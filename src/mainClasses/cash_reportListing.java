package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.cash_report;
public class cash_reportListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String cash_report_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setcash_report_id(String cash_report_id)
{
this.cash_report_id = cash_report_id;
}
public String getcash_report_id(){
return(this.cash_report_id);
}
public List getcash_report()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cash_report_id,balance_code_flag,date,shift_code,counter_code,thousands,fivehundreds,hundreds,fifyts,twntys,tens,fives,twos,ones,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM cash_report "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cash_report(rs.getString("cash_report_id"),rs.getString("balance_code_flag"),rs.getString("date"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("thousands"),rs.getString("fivehundreds"),rs.getString("hundreds"),rs.getString("fifyts"),rs.getString("twntys"),rs.getString("tens"),rs.getString("fives"),rs.getString("twos"),rs.getString("ones"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMcash_report(String cash_report_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cash_report_id,balance_code_flag,date,shift_code,counter_code,thousands,fivehundreds,hundreds,fifyts,twntys,tens,fives,twos,ones,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM cash_report where  cash_report_id = '"+cash_report_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cash_report(rs.getString("cash_report_id"),rs.getString("balance_code_flag"),rs.getString("date"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("thousands"),rs.getString("fivehundreds"),rs.getString("hundreds"),rs.getString("fifyts"),rs.getString("twntys"),rs.getString("tens"),rs.getString("fives"),rs.getString("twos"),rs.getString("ones"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}