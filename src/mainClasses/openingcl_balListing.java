package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.openingcl_bal;

public class openingcl_balListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String emp_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}
public String getemp_id(){
return(this.emp_id);
}
public List getopeningcl_bal()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,mon_yr,value FROM openingcl_bal "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new openingcl_bal(rs.getString("emp_id"),rs.getString("mon_yr"),rs.getString("value")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getopeningcl_bal(String month, String year)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,mon_yr,value FROM openingcl_bal where mon_yr='"+month+" "+year+"' "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new openingcl_bal(rs.getString("emp_id"),rs.getString("mon_yr"),rs.getString("value")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public int getopeningcl_balMonth(String month, String year)
{
	int list = 0;	
	
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) FROM openingcl_bal where mon_yr='"+month+" "+year+"' "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list=Integer.parseInt(rs.getString(1));
	}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getopeningcl_bal(String emp_id,String month, String year)
{
	String list = "";
	if(month.equals("0")){
		month="January";
	}else if(month.equals("1")){
		month="February";
	}else if(month.equals("2")){
		month="March";
	}else if(month.equals("3")){
		month="April";
	}else if(month.equals("4")){
		month="May";
	}else if(month.equals("5")){
		month="June";
	}else if(month.equals("6")){
		month="July";
	}else if(month.equals("7")){
		month="August";
	}else if(month.equals("8")){
		month="September";
	}else if(month.equals("9")){
		month="October";
	}else if(month.equals("10")){
		month="November";
	}else if(month.equals("11")){
		month="December";
	}
	
try {
 // Load the database driver
	
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT value FROM openingcl_bal where emp_id='"+emp_id+"' and mon_yr='"+month+" "+year+"' "; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list=rs.getString("value");
	}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMopeningcl_bal(String emp_id, String mon_yr)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,mon_yr,value FROM openingcl_bal where emp_id='"+emp_id+"' and mon_yr='"+mon_yr+"' "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new openingcl_bal(rs.getString("emp_id"),rs.getString("mon_yr"),rs.getString("value")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List selectAllEmployeeIdBasedOnMonth(String mon_yr)
{
	List l=new ArrayList();
	try {
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT emp_id FROM openingcl_bal where  mon_yr='"+mon_yr+"' "; 
		ResultSet rs = s.executeQuery(selectquery);
		
		while(rs.next())
		{
			l.add(rs.getString(1));
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		e.printStackTrace();
	}
	
	return l;
	
}

}