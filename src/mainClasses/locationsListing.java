package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.locations;
public class locationsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String location_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setlocation_id(String location_id)
{
this.location_id = location_id;
}
public String getlocation_id(){
return(this.location_id);
}
public List getlocations()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT location_id,location_name,narration,emp_id,date,head_account_id,extra1,extra2,extra3,extra4 FROM locations "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new locations(rs.getString("location_id"),rs.getString("location_name"),rs.getString("narration"),rs.getString("emp_id"),rs.getString("date"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMlocations(String location_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT location_id,location_name,narration,emp_id,date,head_account_id,extra1,extra2,extra3,extra4 FROM locations where  location_id = '"+location_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new locations(rs.getString("location_id"),rs.getString("location_name"),rs.getString("narration"),rs.getString("emp_id"),rs.getString("date"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<beans.locations> getMlocationsBasedOnHead(String hoaid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(hoaid.equals("5")){
		subQuery=" where head_account_id='"+hoaid+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT location_id,location_name,narration,emp_id,date,head_account_id,extra1,extra2,extra3,extra4 FROM locations"+subQuery; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new locations(rs.getString("location_id"),rs.getString("location_name"),rs.getString("narration"),rs.getString("emp_id"),rs.getString("date"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<beans.locations> getLocations(String hoaid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
		subQuery=" where head_account_id='"+hoaid+"'";
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT location_id,location_name,narration,emp_id,date,head_account_id,extra1,extra2,extra3,extra4 FROM locations"+subQuery; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new locations(rs.getString("location_id"),rs.getString("location_name"),rs.getString("narration"),rs.getString("emp_id"),rs.getString("date"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getLocationsName(String locId)
{
String name = "";
try {
 // Load the database driver
	String subQuery="";
		subQuery=" where location_id='"+locId+"'";
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT location_id,location_name,narration,emp_id,date,head_account_id,extra1,extra2,extra3,extra4 FROM locations"+subQuery; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	name=rs.getString("location_name");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
}