package mainClasses;

import dbase.sqlcon.SainivasConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.dormitory;
public class dormitoryListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String booking_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setbooking_id(String booking_id)
{
this.booking_id = booking_id;
}
public String getbooking_id(){
return(this.booking_id);
}
public List getdormitory()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT booking_id,first_name,last_name,phone_num,email_id,address,amount,created,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13 FROM dormitory "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new dormitory(rs.getString("booking_id"),rs.getString("first_name"),rs.getString("last_name"),rs.getString("phone_num"),rs.getString("email_id"),rs.getString("address"),rs.getString("amount"),rs.getString("created"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMdormitory(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT booking_id,first_name,last_name,phone_num,email_id,address,amount,created,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13 FROM dormitory where  booking_id = '"+booking_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new dormitory(rs.getString("booking_id"),rs.getString("first_name"),rs.getString("last_name"),rs.getString("phone_num"),rs.getString("email_id"),rs.getString("address"),rs.getString("amount"),rs.getString("created"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getvaliddormitory(String booking_id)
{
String list ="";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM dormitory where  booking_id ='"+booking_id+"'"; 
//System.out.println("selectquery=="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 
	list="yes";
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}