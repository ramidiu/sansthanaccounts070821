package mainClasses;

import dbase.sqlcon.SainivasConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.cancellation_details;
public class cancellation_detailsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String cancel_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setcancel_id(String cancel_id)
{
this.cancel_id = cancel_id;
}
public String getcancel_id(){
return(this.cancel_id);
}
public List getcancellation_details()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cancel_id,booking_id,refund_amount_total,refund_excludetax,refund_includetax,r_vat,r_mvat,r_luxarytax,refund_date,refund_ref_num,refund_trans_num,refund_status,refund_tracking_num,gained_amount_total,gained_amount_excludetax,gained_amount_tax,g_vat,g_mvat,g_luxary_tax,g_service_charge,g_pg_charges,grand_total_paid,entry_date,closed_date,c_x,c_y,c_z FROM cancellation_details "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cancellation_details(rs.getString("cancel_id"),rs.getString("booking_id"),rs.getString("refund_amount_total"),rs.getString("refund_excludetax"),rs.getString("refund_includetax"),rs.getString("r_vat"),rs.getString("r_mvat"),rs.getString("r_luxarytax"),rs.getString("refund_date"),rs.getString("refund_ref_num"),rs.getString("refund_trans_num"),rs.getString("refund_status"),rs.getString("refund_tracking_num"),rs.getString("gained_amount_total"),rs.getString("gained_amount_excludetax"),rs.getString("gained_amount_tax"),rs.getString("g_vat"),rs.getString("g_mvat"),rs.getString("g_luxary_tax"),rs.getString("g_service_charge"),rs.getString("g_pg_charges"),rs.getString("grand_total_paid"),rs.getString("entry_date"),rs.getString("closed_date"),rs.getString("c_x"),rs.getString("c_y"),rs.getString("c_z")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMcancellation_details(String cancel_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cancel_id,booking_id,refund_amount_total,refund_excludetax,refund_includetax,r_vat,r_mvat,r_luxarytax,refund_date,refund_ref_num,refund_trans_num,refund_status,refund_tracking_num,gained_amount_total,gained_amount_excludetax,gained_amount_tax,g_vat,g_mvat,g_luxary_tax,g_service_charge,g_pg_charges,grand_total_paid,entry_date,closed_date,c_x,c_y,c_z FROM cancellation_details where  cancel_id = '"+cancel_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cancellation_details(rs.getString("cancel_id"),rs.getString("booking_id"),rs.getString("refund_amount_total"),rs.getString("refund_excludetax"),rs.getString("refund_includetax"),rs.getString("r_vat"),rs.getString("r_mvat"),rs.getString("r_luxarytax"),rs.getString("refund_date"),rs.getString("refund_ref_num"),rs.getString("refund_trans_num"),rs.getString("refund_status"),rs.getString("refund_tracking_num"),rs.getString("gained_amount_total"),rs.getString("gained_amount_excludetax"),rs.getString("gained_amount_tax"),rs.getString("g_vat"),rs.getString("g_mvat"),rs.getString("g_luxary_tax"),rs.getString("g_service_charge"),rs.getString("g_pg_charges"),rs.getString("grand_total_paid"),rs.getString("entry_date"),rs.getString("closed_date"),rs.getString("c_x"),rs.getString("c_y"),rs.getString("c_z")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getRCallations(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cancel_id,booking_id,refund_amount_total,refund_excludetax,refund_includetax,r_vat,r_mvat,r_luxarytax,refund_date,refund_ref_num,refund_trans_num,refund_status,refund_tracking_num,gained_amount_total,gained_amount_excludetax,gained_amount_tax,g_vat,g_mvat,g_luxary_tax,g_service_charge,g_pg_charges,grand_total_paid,entry_date,closed_date,c_x,c_y,c_z FROM cancellation_details where  booking_id = '"+booking_id+"'"; 
//System.out.println("\n Cancellations == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cancellation_details(rs.getString("cancel_id"),rs.getString("booking_id"),rs.getString("refund_amount_total"),rs.getString("refund_excludetax"),rs.getString("refund_includetax"),rs.getString("r_vat"),rs.getString("r_mvat"),rs.getString("r_luxarytax"),rs.getString("refund_date"),rs.getString("refund_ref_num"),rs.getString("refund_trans_num"),rs.getString("refund_status"),rs.getString("refund_tracking_num"),rs.getString("gained_amount_total"),rs.getString("gained_amount_excludetax"),rs.getString("gained_amount_tax"),rs.getString("g_vat"),rs.getString("g_mvat"),rs.getString("g_luxary_tax"),rs.getString("g_service_charge"),rs.getString("g_pg_charges"),rs.getString("grand_total_paid"),rs.getString("entry_date"),rs.getString("closed_date"),rs.getString("c_x"),rs.getString("c_y"),rs.getString("c_z")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

}