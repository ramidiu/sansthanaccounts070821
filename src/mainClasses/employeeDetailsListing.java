package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.employeeDetails;
import beans.employeeSalary;
import beans.stafftimings;
import dbase.sqlcon.ConnectionHelper;

public class employeeDetailsListing 
{
	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	
	public employeeDetailsListing()
	{
		try {
		 	// Load the database driver
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		}catch(Exception e)
		{
			//this.seterror(e.toString());
			e.printStackTrace();
			System.out.println("Exception is ;"+e);
	}
		
	}
	
	//select all employee id and store in list called by employeeDetailForm
	public List selectAllEmployeeIdOnly()
	{
		List l=new ArrayList();
		try
		{
			
			ResultSet rs=st.executeQuery("select employee_id from employee_details");
			while(rs.next())
			{
				l.add(rs.getString(1));
			
			}
	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return l;	
	}
	
	
	public List selectAllEmployeeDetails()
	{
		List l=new ArrayList();
		try
		{
			
			ResultSet rs=st.executeQuery("select * from employee_details");
			while(rs.next())
			{
				employeeDetails ed=new employeeDetails();
				ed.setEmployee_details_id(rs.getString(1));
				ed.setEmployee_id(rs.getString(2));
				ed.setUnique_registration_no(rs.getString(3));
				ed.setBasic_pay_salary(rs.getString(4));
				ed.setEwf_total_loan_amount(rs.getString(5));
				ed.setEwf_due_amount(rs.getString(6));
				ed.setPersonal_total_loan_amount(rs.getString(7));
				ed.setPersonal_due_amount(rs.getString(8));
				ed.setCreated_date(rs.getString(9));
				ed.setExtra1(rs.getString(10));
				ed.setExtra2(rs.getString(11));
				ed.setExtra3(rs.getString(12));
				ed.setExtra4(rs.getString(13));
				ed.setExtra5(rs.getString(14));
				ed.setCreated_by(rs.getString(15));
				ed.setExtra6(rs.getString(16));
				ed.setExtra7(rs.getString(17));
				ed.setExtra8(rs.getString(18));
				ed.setExtra9(rs.getString(19));
				ed.setExtra10(rs.getString(20));
				
				l.add(ed);
			}
	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return l;	
	}
	
	//list out all the employee details based on depatment ,depatmrnt is there in saisansthan schema..get data from other schema and verify
	public List selectAllEmployeeDetailsBasedUponDepartment(String department,String month) throws SQLException
	{
		//System.out.println("month==="+month);
		List l=new ArrayList();
		try
		{
		//from month we get first 2 character
		/*SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy");
		Date d1=df.parse(month);
		SimpleDateFormat df1=new SimpleDateFormat("yyyy-MM");
		String dateAndMonth=df1.format(d1);*/
		
		//ResultSet rs=st.executeQuery("select * from employee_details");
		String query="SELECT * FROM employee_details where employee_id not in (SELECT employee_id FROM employee_salary where month = '"+month+"' );";
		System.out.println("from employeeDetailsListing.selectAllEmployeeDetailsBasedUponDepartment="+query);
		ResultSet rs=st.executeQuery(query);
		while(rs.next())
		{
			employeeDetails ed=new employeeDetails();
			String staff_id=rs.getString(2);
			stafftimingsListing stlisting=new stafftimingsListing ();
			stafftimings sttiming=stlisting.getStaffDetails(staff_id);
			if(sttiming!=null){
			String staff_type=sttiming.getstaff_type();
			if(staff_type.equals(department))
			{
			  /*System.out.println("1");
				System.out.println(rs.getString(1));
				System.out.println(rs.getString(1));
				System.out.println(rs.getString(2));
				System.out.println(rs.getString(3));
				System.out.println(rs.getString(4));
				System.out.println(rs.getString(5));
				System.out.println(rs.getString(6));
				System.out.println(rs.getString(7));
				System.out.println(rs.getString(8));*/
				ed.setEmployee_details_id(rs.getString(1));
				ed.setEmployee_id(rs.getString(2));
				ed.setUnique_registration_no(rs.getString(3));
				ed.setBasic_pay_salary(rs.getString(4));
				ed.setEwf_total_loan_amount(rs.getString(5));
				ed.setEwf_due_amount(rs.getString(6));
				ed.setPersonal_total_loan_amount(rs.getString(7));
				ed.setPersonal_due_amount(rs.getString(8));
				ed.setCreated_date(rs.getString(9));
				ed.setExtra1(rs.getString(10));
				ed.setExtra2(rs.getString(11));
				ed.setExtra3(rs.getString(12));
				ed.setExtra4(rs.getString(13));
				ed.setExtra5(rs.getString(14));
				ed.setCreated_by(rs.getString(15));
				l.add(ed);
			} }
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	finally{
				if(st!=null){
			st.close();	
		}
			if(rs!=null){
	rs.close();	
		}

		return l;	
	}
	
}
	
	//select single employee details by passing employee_details_id 
	public employeeDetails selectSingleEmployeeDetails(String employee_details_id)
	{
		employeeDetails ed=new employeeDetails();
		try
		{
			
			ResultSet rs=st.executeQuery("select * from employee_details where employee_details_id="+employee_details_id);
			if(rs.next())
			{
				
				ed.setEmployee_details_id(rs.getString(1));
				ed.setEmployee_id(rs.getString(2));
				ed.setUnique_registration_no(rs.getString(3));
				ed.setBasic_pay_salary(rs.getString(4));
				ed.setEwf_total_loan_amount(rs.getString(5));
				ed.setEwf_due_amount(rs.getString(6));
				ed.setPersonal_total_loan_amount(rs.getString(7));
				ed.setPersonal_due_amount(rs.getString(8));
				ed.setCreated_date(rs.getString(9));
				ed.setExtra1(rs.getString(10));
				ed.setExtra2(rs.getString(11));
				ed.setExtra3(rs.getString(12));
				ed.setExtra4(rs.getString(13));
				ed.setExtra5(rs.getString(14));
				ed.setCreated_by(rs.getString(15));
				
				
			}
	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return ed;		
	}
	
	//select single employee details by passing employee_id  not not  by passing employee_detail_id
		public employeeDetails selectSingleEmployeeDetailsBasedUponEmployeeId(String employee_id)
		{
			employeeDetails ed=new employeeDetails();
			try
			{
				c = ConnectionHelper.getConnection();
				st=c.createStatement();
				String q="select * from employee_details where employee_id='"+employee_id+"'";
				//System.out.println(q);
				ResultSet rs=st.executeQuery(q);//+employee_id);
				
				if(rs.next())
				{
					
					ed.setEmployee_details_id(rs.getString(1));
					ed.setEmployee_id(rs.getString(2));
					ed.setUnique_registration_no(rs.getString(3));
					ed.setBasic_pay_salary(rs.getString(4));
					ed.setEwf_total_loan_amount(rs.getString(5));
					ed.setEwf_due_amount(rs.getString(6));
					ed.setPersonal_total_loan_amount(rs.getString(7));
					ed.setPersonal_due_amount(rs.getString(8));
					ed.setCreated_date(rs.getString(9));
					ed.setExtra1(rs.getString(10));//setting eligible_for_hra
					ed.setExtra2(rs.getString(11));//setting eligible_for_medical
					ed.setExtra3(rs.getString(12));//setting eligible_for_ewf_deduction
					ed.setExtra4(rs.getString(13));
					ed.setExtra5(rs.getString(14));
					ed.setCreated_by(rs.getString(15));
					ed.setExtra6(rs.getString(16));//ewf_total_installments
					ed.setExtra7(rs.getString(17));//ewf_balance_installments
					ed.setExtra8(rs.getString(18));//personal_total_installments
					ed.setExtra9(rs.getString(19));//personal_balance_installments
					ed.setExtra10(rs.getString(20));
					ed.setExtra11(rs.getString(21));
					ed.setExtra12(rs.getString(22));
					ed.setExtra13(rs.getString(23));
					ed.setExtra14(rs.getString(24));
					ed.setExtra15(rs.getString(25));
				}
				st.close();
				rs.close();
				c.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return ed;		
		}
		
		//calculating interest and pass to calling file employeeSalarySlipForm.jsp
	public List calculateInterest(String employee_id,float opening_cl,float earned_cl,float closing_cl,float ot)
	{
		List l=new ArrayList();
		
		//getting some data from employeeDetails to calculate 
			employeeDetails details=selectSingleEmployeeDetailsBasedUponEmployeeId(employee_id);
			
			float basic_pay_sal=Float.parseFloat(details.getBasic_pay_salary());
			
			float ewf_total_loan_amount=Float.parseFloat(details.getEwf_total_loan_amount());
			float ewf_due_amount=Float.parseFloat(details.getEwf_due_amount());
			float personal_total_loan_amount=Float.parseFloat(details.getPersonal_total_loan_amount());
			float personal_due_amount=Float.parseFloat(details.getPersonal_due_amount());
			
			String eligible_for_hra=details.getExtra1();//getting eligible for hra conformation
			String eligible_for_ewf_deduction=details.getExtra3();//getting eligible for  ewf deduction conformation
			
		//calculating the data
			float ewf_interest_amount=0.0f;
			float ewf_loan_amount=0.0f;
			float personal_interest_amount=0.0f;
			float personal_loan_amount=0.0f;
			float hra=0.0f;
			float net_salary=0.0f;
			float ot_amount=0.0f;
			float cl_amount_deduction=0.0f;
			float ewf_deduction=0.0f;
			float wa_amount=0;
			float salary_cut_days_amount=0.0f;
			
			if(ewf_due_amount>0.0)
			{
				//ewf_interest_amount=(ewf_due_amount*1)/100;
				if(ewf_total_loan_amount==10000){
					ewf_interest_amount=52.50f;
				}else if(ewf_total_loan_amount==20000){
					ewf_interest_amount=105f;
				}
				ewf_loan_amount=ewf_total_loan_amount/20;
			}
			if(personal_due_amount>0.0)
			{
				personal_interest_amount=0;
				personal_loan_amount=personal_total_loan_amount/20;
			}
			if(eligible_for_hra.equals("eligible_for_hra"))
			{
				if((opening_cl+earned_cl-closing_cl)<10)
				{
					hra=(basic_pay_sal*15)/100;
				}
			}
			
			if(eligible_for_ewf_deduction.equals("eligible_for_ewf_deduction"))
			{
				ewf_deduction=300;
			}
			if(hra>0&&ewf_deduction>0)
			{
				wa_amount=0;
			}
			if(closing_cl<0)
			{
				salary_cut_days_amount=-(closing_cl*basic_pay_sal*12/365);
			}
			ot_amount=(ot*basic_pay_sal)*12/(365*9);
			
			net_salary=basic_pay_sal+hra+ot_amount-ewf_loan_amount-ewf_interest_amount-personal_loan_amount-ewf_deduction-cl_amount_deduction+wa_amount-salary_cut_days_amount;
			
			//setting to employeeSalary and pass to employeeSalarySlipForm.jsp file
			employeeSalary esal=new employeeSalary();
			
			esal.setEmployee_id(employee_id);
			esal.setEwf_interest_amount(new Float(Math.round(ewf_interest_amount)).toString());
			esal.setEwf_loan_amount(new Float(Math.round(ewf_loan_amount)).toString());
			esal.setPersonal_interest_amount(new Float(Math.round(personal_interest_amount)).toString());
			esal.setPersonal_loan_amount(new Float(Math.round(personal_loan_amount)).toString());
			esal.setBasic_pay(new Float(Math.round(basic_pay_sal)).toString());
			esal.setHra(new Float(Math.round(hra)).toString());
			esal.setOtpay(new Float(Math.round(ot_amount)).toString());
			esal.setSalary_pay(new Float(Math.round(net_salary)).toString());
			esal.setExtra8(new Float(Math.round(ewf_deduction)).toString());
			esal.setExtra9(new Float(Math.round(wa_amount)).toString());
			esal.setExtra10(new Float(Math.round(salary_cut_days_amount)).toString());
			l.add(esal);
			
			return l;		
	}
	
	public List calculateInterest2(String employee_id,float opening_cl,float earned_cl,float closing_cl,float ot)
	{
		List l=new ArrayList();
		
		//getting some data from employeeDetails to calculate 
			employeeDetails details=selectSingleEmployeeDetailsBasedUponEmployeeId(employee_id);
			
			float basic_pay_sal=Float.parseFloat(details.getBasic_pay_salary());
			
			float ewf_total_loan_amount=Float.parseFloat(details.getEwf_total_loan_amount());
			float ewf_due_amount=Float.parseFloat(details.getEwf_due_amount());
			float personal_total_loan_amount=Float.parseFloat(details.getPersonal_total_loan_amount());
			float personal_due_amount=Float.parseFloat(details.getPersonal_due_amount());
			float ewf_total_installments=Float.parseFloat(details.getExtra6());
			float personal_total_installments=Float.parseFloat(details.getExtra8());
			
			String eligible_for_hra=details.getExtra1();//getting eligible for hra conformation
			String eligible_for_ewf_deduction=details.getExtra3();//getting eligible for  ewf deduction conformation
			
		//calculating the data
			float ewf_interest_amount=0.0f;
			float ewf_loan_amount=0.0f;
			float personal_interest_amount=0.0f;
			float personal_loan_amount=0.0f;
			float hra=0.0f;
			float net_salary=0.0f;
			float ot_amount=0.0f;
			float cl_amount_deduction=0.0f;
			float ewf_deduction=0.0f;
			float wa_amount=0;
			float salary_cut_days_amount=0.0f;
			
			if(ewf_due_amount>0.0)
			{
				//ewf_interest_amount=(ewf_due_amount*1)/100;
				if(ewf_total_loan_amount==10000){
					ewf_interest_amount=(20/ewf_total_installments)*52.50f;
				}else if(ewf_total_loan_amount==20000){
					ewf_interest_amount=(20/ewf_total_installments)*105f;
				}
				ewf_loan_amount=ewf_total_loan_amount/ewf_total_installments;
			}
			if(personal_due_amount>0.0)
			{
				personal_interest_amount=0;
				personal_loan_amount=personal_total_loan_amount/personal_total_installments;
			}
			if(eligible_for_hra.equals("eligible_for_hra"))
			{
				if((opening_cl+earned_cl-closing_cl)<10)
				{
					hra=(basic_pay_sal*15)/100;
				}
			}
			
			if(eligible_for_ewf_deduction.equals("eligible_for_ewf_deduction"))
			{
				ewf_deduction=300;
			}
			if(hra>0&&ewf_deduction>0)
			{
				wa_amount=0;
			}
			if(closing_cl<0)
			{
				salary_cut_days_amount=-(closing_cl*basic_pay_sal*12/365);
			}
			ot_amount=(ot*basic_pay_sal)*12/(365*9);
			
			net_salary=basic_pay_sal+hra+ot_amount-ewf_loan_amount-ewf_interest_amount-personal_loan_amount-ewf_deduction-cl_amount_deduction+wa_amount-salary_cut_days_amount;
			
			//setting to employeeSalary and pass to employeeSalarySlipForm.jsp file
			employeeSalary esal=new employeeSalary();
			
			esal.setEmployee_id(employee_id);
			esal.setEwf_interest_amount(new Float(Math.round(ewf_interest_amount)).toString());
			esal.setEwf_loan_amount(new Float(Math.round(ewf_loan_amount)).toString());
			esal.setPersonal_interest_amount(new Float(Math.round(personal_interest_amount)).toString());
			esal.setPersonal_loan_amount(new Float(Math.round(personal_loan_amount)).toString());
			esal.setBasic_pay(new Float(Math.round(basic_pay_sal)).toString());
			esal.setHra(new Float(Math.round(hra)).toString());
			esal.setOtpay(new Float(Math.round(ot_amount)).toString());
			esal.setSalary_pay(new Float(Math.round(net_salary)).toString());
			esal.setExtra8(new Float(Math.round(ewf_deduction)).toString());
			esal.setExtra9(new Float(Math.round(wa_amount)).toString());
			esal.setExtra10(new Float(Math.round(salary_cut_days_amount)).toString());
			l.add(esal);
			
			return l;		
	}
	
	
	public boolean checkEidIsAlreadyInserted(String eid)
	{
		boolean b=false;
		
		List l=selectAllEmployeeIdOnly();
		for(int i=0;i<l.size();i++)
		{
			String employeeId=(String)l.get(i);
			if(eid.equals(employeeId))
			{
				b=true;
			}
		}
		return b;	
	}
	
	public String getEmployeeName(String empId)
	{
		String empName="";
		try
		{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			String q="select staff_name from stafftimings where staff_id='"+empId+"'";
			ResultSet rs=st.executeQuery(q);//+employee_id);
			if(rs.next())
			{
				empName=rs.getString("staff_name");
			}
			st.close();
			rs.close();
			c.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return empName;		
	}
	
}
