package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.stafftimings_details;
import dbase.sqlcon.ConnectionHelper;

public class stafftimings_detailsListing {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}

	private String staff_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	 public void setstaff_id(String staff_id)
	{
	this.staff_id = staff_id;
	}
	public String getstaff_id(){
	return(this.staff_id);
	}
	
	
	public List getstafftimings_details()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT staff_id, government_reservation, caste_regligion, dependents, educational_qual, technical_qual, identification_marks, previous_experience, height, physical_disorder, present_pay_scale, basic_pay, reference, graduvite_amount, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9 FROM stafftimings_details "; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new stafftimings_details(rs.getString("staff_id"),rs.getString("government_reservation"),rs.getString("caste_regligion"),rs.getString("dependents"),rs.getString("educational_qual"),rs.getString("technical_qual"),rs.getString("identification_marks"),rs.getString("previous_experience"),rs.getString("height"),rs.getString("physical_disorder"),rs.getString("present_pay_scale"),rs.getString("basic_pay"),rs.getString("reference"),rs.getString("graduvite_amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public stafftimings_details getMstafftimings_details(String staff_id)
	{
		stafftimings_details list = new stafftimings_details();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT * FROM stafftimings_details where  staff_id = '"+staff_id+"'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list=new stafftimings_details(rs.getString("staff_id"),rs.getString("government_reservation"),rs.getString("caste_regligion"),rs.getString("dependents"),rs.getString("educational_qual"),rs.getString("technical_qual"),rs.getString("identification_marks"),rs.getString("previous_experience"),rs.getString("height"),rs.getString("physical_disorder"),rs.getString("present_pay_scale"),rs.getString("basic_pay"),rs.getString("reference"),rs.getString("graduvite_amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public boolean getstafftimings_detailsMId(String staff_id)
	{
		boolean flag=false;
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT * FROM stafftimings_details where  staff_id = '"+staff_id+"'";  
	ResultSet rs = s.executeQuery(selectquery);
	if(rs.next()){
		flag=true;

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return flag;
	}
	
}
