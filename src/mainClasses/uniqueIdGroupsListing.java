package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.UniqueIdGroup;
import dbase.sqlcon.ConnectionHelper;

public class uniqueIdGroupsListing {
	public String error="";

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	private String sssst_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;

	public String getSssst_id() {
		return sssst_id;
	}

	public void setSssst_id(String sssst_id) {
		this.sssst_id = sssst_id;
	}
	public List getUniqueIdGroups(String sssst_id)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	
	String selectquery="SELECT uniqueid, donor, trusties, nad, lta, rds, timeshare, employee, general, createdDate, updatedDate, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9, extra10, extra11, extra12, extra13, extra14, extra15 FROM unique_id_group where  uniqueid = '"+sssst_id+"'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new UniqueIdGroup(rs.getString("uniqueid"),rs.getString("donor"),rs.getString("trusties"),rs.getString("nad"),rs.getString("lta"),rs.getString("rds"),rs.getString("timeshare"),rs.getString("employee"),rs.getString("general"),rs.getString("createdDate"),rs.getString("updatedDate"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.setError(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getUniqueIdGroupsOfEmployeeOrVolunteer()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	
	String selectquery="SELECT uniqueid, donor, trusties, nad, lta, rds, timeshare, employee, general, createdDate, updatedDate, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9, extra10, extra11, extra12, extra13, extra14, extra15 FROM unique_id_group where employee='EMPLOYEE' or extra1='VOLUNTEER'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new UniqueIdGroup(rs.getString("uniqueid"),rs.getString("donor"),rs.getString("trusties"),rs.getString("nad"),rs.getString("lta"),rs.getString("rds"),rs.getString("timeshare"),rs.getString("employee"),rs.getString("general"),rs.getString("createdDate"),rs.getString("updatedDate"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.setError(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getUniqueIdGroupsOfVolunteer()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	
	String selectquery="SELECT uniqueid, donor, trusties, nad, lta, rds, timeshare, employee, general, createdDate, updatedDate, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9, extra10, extra11, extra12, extra13, extra14, extra15 FROM unique_id_group where extra1='VOLUNTEER'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new UniqueIdGroup(rs.getString("uniqueid"),rs.getString("donor"),rs.getString("trusties"),rs.getString("nad"),rs.getString("lta"),rs.getString("rds"),rs.getString("timeshare"),rs.getString("employee"),rs.getString("general"),rs.getString("createdDate"),rs.getString("updatedDate"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.setError(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getUniqueIdGroupsOfEmployees()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	
	String selectquery="SELECT uniqueid, donor, trusties, nad, lta, rds, timeshare, employee, general, createdDate, updatedDate, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9, extra10, extra11, extra12, extra13, extra14, extra15 FROM unique_id_group where employee='EMPLOYEE'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new UniqueIdGroup(rs.getString("uniqueid"),rs.getString("donor"),rs.getString("trusties"),rs.getString("nad"),rs.getString("lta"),rs.getString("rds"),rs.getString("timeshare"),rs.getString("employee"),rs.getString("general"),rs.getString("createdDate"),rs.getString("updatedDate"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.setError(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getUniqueIdGroupsOfDevotees()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	
	String selectquery="SELECT uniqueid, donor, trusties, nad, lta, rds, timeshare, employee, general, createdDate, updatedDate, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9, extra10, extra11, extra12, extra13, extra14, extra15 FROM unique_id_group where employee NOT IN ('EMPLOYEE') AND extra1 NOT IN ('VOLUNTEER') order by SUBSTRING(uniqueid ,5)*1 desc"; 
	//System.out.println("===>"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new UniqueIdGroup(rs.getString("uniqueid"),rs.getString("donor"),rs.getString("trusties"),rs.getString("nad"),rs.getString("lta"),rs.getString("rds"),rs.getString("timeshare"),rs.getString("employee"),rs.getString("general"),rs.getString("createdDate"),rs.getString("updatedDate"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.setError(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getUniqueIdBasedOnGroup(String group)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	
	String addQuery="";
	
	if(group.equals("DONOR"))
	{
		addQuery = "donor = 'DONOR'";
	}
	
	else if(group.equals("TRUSTEES"))
	{
		addQuery = "trusties = 'TRUSTEES'";
	}
	
	else if(group.equals("NAD"))
	{
		addQuery = "nad = 'NAD'";
	}
	
	else if(group.equals("LTA"))
	{
		addQuery = "lta = 'LTA'";
	}
	
	else if(group.equals("RDS"))
	{
		addQuery = "rds = 'RDS'";
	}
	
	else if(group.equals("TIME_SHARE"))
	{
		addQuery = "timeshare = 'TIME_SHARE'";
	}
	
	else if(group.equals("EMPLOYEE"))
	{
		addQuery = "employee = 'EMPLOYEE'";
	}
	
	else if(group.equals("GENERAL"))
	{
		addQuery = "general = 'GENERAL'";
	}
	
	else if(group.equals("VOLUNTEER"))
	{
		addQuery = "extra1 = 'VOLUNTEER'";
	}
	
	String selectquery="SELECT uniqueid, donor, trusties, nad, lta, rds, timeshare, employee, general, createdDate, updatedDate, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9, extra10, extra11, extra12, extra13, extra14, extra15 FROM unique_id_group where "+addQuery; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new UniqueIdGroup(rs.getString("uniqueid"),rs.getString("donor"),rs.getString("trusties"),rs.getString("nad"),rs.getString("lta"),rs.getString("rds"),rs.getString("timeshare"),rs.getString("employee"),rs.getString("general"),rs.getString("createdDate"),rs.getString("updatedDate"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.setError(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getUniqueIdBasedOnGroupDates(String group,String fromDate,String toDate)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	
	String addQuery="";
	
	if(group.equals("DONOR"))
	{
		addQuery = "donor = 'DONOR' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	else if(group.equals("TRUSTEES"))
	{
		addQuery = "trusties = 'TRUSTEES' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	else if(group.equals("NAD"))
	{
		addQuery = "nad = 'NAD' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	else if(group.equals("LTA"))
	{
		addQuery = "lta = 'LTA' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	else if(group.equals("RDS"))
	{
		addQuery = "rds = 'RDS' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	else if(group.equals("TIME_SHARE"))
	{
		addQuery = "timeshare = 'TIME_SHARE' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	else if(group.equals("EMPLOYEE"))
	{
		addQuery = "employee = 'EMPLOYEE' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	else if(group.equals("GENERAL"))
	{
		addQuery = "general = 'GENERAL' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	else if(group.equals("VOLUNTEER"))
	{
		addQuery = "extra1 = 'VOLUNTEER' and createdDate between '"+fromDate+"'and '"+toDate+"'";
	}
	
	String selectquery="SELECT uniqueid, donor, trusties, nad, lta, rds, timeshare, employee, general, createdDate, updatedDate, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9, extra10, extra11, extra12, extra13, extra14, extra15 FROM unique_id_group where "+addQuery;
	
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new UniqueIdGroup(rs.getString("uniqueid"),rs.getString("donor"),rs.getString("trusties"),rs.getString("nad"),rs.getString("lta"),rs.getString("rds"),rs.getString("timeshare"),rs.getString("employee"),rs.getString("general"),rs.getString("createdDate"),rs.getString("updatedDate"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.setError(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
}
