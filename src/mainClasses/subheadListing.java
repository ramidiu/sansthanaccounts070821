package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.subhead;
public class subheadListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String sub_head_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setsub_head_id(String sub_head_id)
{
this.sub_head_id = sub_head_id;
}
public String getsub_head_id(){
return(this.sub_head_id);
}
public List getsubhead()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead "; 
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead order by SUBSTRING(sub_head_id ,2)*1 asc;";
//System.out.println("selectquery:::::"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getsubheadBasedOnHeadOfAccount(String headOfAccount)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead "; 
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where head_account_id='"+headOfAccount+"' order by SUBSTRING(sub_head_id ,2)*1 asc;";
//System.out.println("selectquery:::::"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public subhead getSubheadBasedOnSubheadId(String subHeadId){
	subhead subhead=null;
	try{
		
		
		c=ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectQuery ="Select extra1,major_head_id from subhead where sub_head_id="+subHeadId;
		ResultSet rs=s.executeQuery(selectQuery);
		while(rs.next()){
			subhead=new subhead();
			subhead.setmajor_head_id(rs.getString("major_head_id"));
			subhead.setextra1(rs.getString("extra1"));
		}
		
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	
	return subhead;
}


public List getMsubhead(String sub_head_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where  sub_head_id = '"+sub_head_id+"'"; 
//System.out.println("quesry-->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMsubheadnamesBasedOnMajorhead(String sub_head_id,String majorhead)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where  sub_head_id = '"+sub_head_id+"' and major_head_id='"+majorhead+"'"; 
//System.out.println("quesry-->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMsubheadname(String sub_head_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  sub_head_id = '"+sub_head_id+"'";
//System.out.println("111------->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("name");
 //System.out.println(rs.getString("name"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMsubheadnames(String sub_head_id,String hoid)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  sub_head_id = '"+sub_head_id+"' and head_account_id='"+hoid+"'";
//System.out.println("------->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("name");
 //System.out.println(rs.getString("name"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getMsubheadnames1(String sub_head_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  sub_head_id = '"+sub_head_id+"'";
//System.out.println("------->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("name");
 //System.out.println(rs.getString("name"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getsubheadname(String name,String head_account_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  name like '%"+name+"%' and head_account_id='"+head_account_id+"'";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
 list=rs.getString("sub_head_id")+" "+rs.getString("name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMsubheadnameWithDepartment(String sub_head_id,String hoid)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  sub_head_id = '"+sub_head_id+"' and head_account_id='"+hoid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getSubHeadExpenditure(String sub_head_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  sub_head_id = '"+sub_head_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("description");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMsubheadSearch(String minior,String catid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id, name, description, major_head_id, head_account_id, extra1, extra2, extra3 FROM subhead where  name like '%"+minior+"%' and major_head_id='"+catid+"' ";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMsubheadSearch(String minior,String catid,String subhead,String hoaId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String majorHead="";
	String minorHead="";
	String HOAID="";
	if(hoaId!=null && !hoaId.equals("")){
		HOAID=" and head_account_id='"+hoaId+"'";
	}
	if(catid!=null && !catid.equals("")){
		majorHead=" and major_head_id='"+catid+"'";
	}
	if(minior!=null && !minior.equals("")){
		minorHead=" and extra1='"+minior+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id, name, description, major_head_id, head_account_id, extra1, extra2, extra3 FROM subhead where (sub_head_id like '%"+subhead+"%' || name like '%"+subhead+"%')"+majorHead+minorHead+HOAID;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMsubheadSearchNosubhead(String catid,String subhead,String hoaId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String majorHead="";
	String HOAID="";
	if(hoaId!=null && !hoaId.equals("")){
		HOAID=" and head_account_id='"+hoaId+"'";
	}
	if(catid!=null && !catid.equals("")){
		majorHead=" and major_head_id='"+catid+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id, name, description, major_head_id, head_account_id, extra1, extra2, extra3 FROM subhead where (sub_head_id like '%"+subhead+"%' || name like '%"+subhead+"%')"+majorHead+HOAID;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPoojaTypes(String head_account_id,String major_head_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  head_account_id = '"+head_account_id+"' and major_head_id='"+major_head_id+"'"; 
//System.out.println("offerkindquery...."+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getSubHead(String head_account_id,String major_head_id)
{
	ArrayList list = new ArrayList();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  head_account_id = '"+head_account_id+"' and major_head_id='"+major_head_id+"' order by sub_head_id*1 asc ;"; 
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));
	
	}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
	return list;
}
public List getSubHead2(String head_account_id,String major_head_id)
{
	ArrayList list = new ArrayList();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,extra4 FROM subhead where  head_account_id = '"+head_account_id+"' and major_head_id='"+major_head_id+"' and extra2!='' order by sub_head_id*1 asc ;"; 
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
	while(rs.next())
	{
		subhead s=new subhead();
		s.setsub_head_id(rs.getString("sub_head_id"));
		s.setname(rs.getString("name"));
		s.setdescription(rs.getString("description"));
		s.setmajor_head_id(rs.getString("major_head_id"));
		s.sethead_account_id(rs.getString("head_account_id"));
		s.setextra1(rs.getString("extra1"));
		s.setextra2(rs.getString("extra2"));
		s.setextra3(rs.getString("extra3"));
		s.setextra4(rs.getString("extra4"));
		list.add(s);
	
	}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
	return list;
}


public List checkDuplicateWntry(String sub_head_id,String hoaid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  sub_head_id = '"+sub_head_id+"' and head_account_id='"+hoaid+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List checkDuplicateWntry1(String sub_head_id,String hoaid,String major_head_id,String minor_head_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  sub_head_id = '"+sub_head_id+"' and head_account_id='"+hoaid+"' and major_head_id='"+major_head_id+"' and extra1='"+minor_head_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getSevas(String minorHead,String hoaid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  extra1 = '"+minorHead+"' and (head_account_id='"+hoaid+"' || head_account_id='1')"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getSubheadListMinorhead(String minorHead)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  extra1 = '"+minorHead+"'"; 
//System.out.println("subheadLedgerReport===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getSubheadListMinorhead1(String minorHead,String subhead,String report)
{
//ArrayList list = new ArrayList();
	String list="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where extra1 = '"+minorHead+"' and sub_head_id= '"+subhead+"' and extra5 not like  '%"+report+"%'"; 
//System.out.println("subheadLedgerReport===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 //list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

	list=rs.getString("sub_head_id");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public ArrayList getSubheadListMinorhead2(String minorHead,String report)
{
ArrayList list = new ArrayList();
//	String[] list={};
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where extra1 = '"+minorHead+"' and extra5 not like  '%"+report+"%'"; 
//System.out.println("subheadLedgerReport===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
//int i=0;
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));
	/*list[i]=rs.getString("sub_head_id");
	i+=1;*/
 
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public ArrayList getSubheadListMinorhead3(String majorHead,String report)
{
ArrayList list = new ArrayList();
//	String[] list={};
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where major_head_id = '"+majorHead+"' and extra5 not like  '%"+report+"%'"; 
//System.out.println("subheadLedgerReport===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
//int i=0;
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));
	/*list[i]=rs.getString("sub_head_id");
	i+=1;*/
 
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public ArrayList getSubheadListMinorhead4(String headaccount,String majorhead,String minorhead,String report)
{
ArrayList list = new ArrayList();
//	String[] list={};
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where head_account_id = '"+headaccount+"' and major_head_id= '"+majorhead+"' and extra1= '"+minorhead+"' and extra5 not like  '%"+report+"%'"; 
//System.out.println("subheadLedgerReport===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
//int i=0;
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));
	/*list[i]=rs.getString("sub_head_id");
	i+=1;*/
 
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getSevasPrices(String minorHead,String hoaid,String subhead)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  extra1 = '"+minorHead+"' and (head_account_id='"+hoaid+"' || head_account_id='1') and sub_head_id='"+subhead+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String[] getTestsPrices(String subhead)
{
String[] amount=new String[2];
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra2,extra4 FROM subhead where sub_head_id='"+subhead+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 amount[0]=rs.getString("extra2");
 amount[1]=rs.getString("extra4");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return amount;
}
public List getSubHeadBasedOnHead(String subhead,String hoaId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String HOAID="";
	if(hoaId!=null && !hoaId.equals("")){
		HOAID=" and head_account_id='"+hoaId+"'";
	}
	
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id, name, description, major_head_id, head_account_id, extra1, extra2, extra3,seva_description,extra4,extra5 FROM subhead where (sub_head_id like '%"+subhead+"%' || name like '%"+subhead+"%')"+HOAID;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getOnlyRevenueSubheads(String subhead,String hoaId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String HOAID="";
	if(hoaId!=null && !hoaId.equals("")){
		HOAID=" and head_account_id='"+hoaId+"'";
	}
	
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id, name, description, major_head_id, head_account_id, extra1, extra2, extra3,seva_description,extra4,extra5 FROM subhead where (sub_head_id like '%"+subhead+"%' || name like '%"+subhead+"%')  and major_head_id in(select major_head_id from majorhead where  extra2='revenue'"+HOAID+") ";
System.out.println("getOnlyRevenueSubheads---"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getHeadOFAccountID(String subheadId)
{
String name="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT head_account_id FROM subhead where sub_head_id='"+subheadId+"' ";
String selectquery="SELECT head_account_id FROM subhead where extra1='"+subheadId+"' ";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("head_account_id");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
public String getHeadOFAccountIDOFMinorhead(String extra1)
{
String name="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT head_account_id FROM subhead where extra1='"+extra1+"' "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("head_account_id");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
public List getMajorHeadForRecipts(String hoaid,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="";
String subquery="";
if(hoaid.equals("3")){
	subquery=" and sh.major_head_id='14'";
}
if(hoaid.equals("3")){
	selectquery="SELECT sh.major_head_id FROM subhead sh,customerpurchases cp,products pr where cp.extra1='"+hoaid+"' and sh.head_account_id='"+hoaid+"' and pr.head_account_id='"+hoaid+"'  and cp.productId=pr.ProductId and sh.sub_head_id=pr.sub_head_Id and (date between '"+fromdate+"' and '"+todate+"')"+subquery+" group by sh.major_head_id";
}else{
selectquery="SELECT major_head_id FROM subhead sh,customerpurchases  cp where cp.extra1='"+hoaid+"' and sh.head_account_id='"+hoaid+"' and sh.sub_head_id=cp.productId and (date between '"+fromdate+"' and '"+todate+"') group by sh.major_head_id";
}
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(rs.getString("major_head_id"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMTestNameAndAmount(String subId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where  sub_head_id = '"+subId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMtestAmount(String subheadId)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where  sub_head_id = '"+sub_head_id+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("extra2");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTestNameByCat(String prdid)
{
String list ="";
try {
 // Load the database driver
	String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();
selectquery="SELECT name as productName  FROM subhead where  sub_head_id = '"+prdid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("productName");
}

}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getSuheadName(String subHeadId)
{
	String subHeadName="";
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT name FROM subhead where  sub_head_id = '"+subHeadId+"'";
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			subHeadName=rs.getString("name");
		
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
	return subHeadName;
}
//added by madhav
public String getSuheadNameBasedOnMajorheadandMinorhead(String MajorHeadId,String minorHeadId,String subHeadId)
{
	String subHeadName="";
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT name FROM subhead where  sub_head_id = '"+subHeadId+"' and  major_head_id= '"+MajorHeadId+"'  and extra1 = '"+minorHeadId+"' ";
//        System.out.println("getSuheadNameBasedOnMajorheadandMinorhead===>"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			subHeadName=rs.getString("name");
		
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
	return subHeadName;
}



public List<subhead> getSubHeadBasdOnCompany(String minor_head_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3,seva_description,extra4,extra5 FROM subhead where  extra1 = '"+minor_head_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("seva_description"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getSuheadNameBasedOnHOAandMINHD(String subHeadId,String minorheadId,String HOD)
{
	String subHeadName="";
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT name FROM subhead where  sub_head_id = '"+subHeadId+"' and extra1='"+minorheadId+"' and head_account_id='"+HOD+"'";
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			subHeadName=rs.getString("name");
		
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
	return subHeadName;
}

public List getsubheadBasedOnHeads(String sub_head_id,String minor,String major,String hoaid)
{
ArrayList list = new ArrayList();
String query1 = "";
String query2 = "";
String query3 = "";
if(minor != null && !minor.equals(""))
	query1 = " and extra1 = '"+minor+"'";
if(major != null && !major.equals(""))
	query2 = " and major_head_id = '"+major+"'";
if(hoaid != null && !hoaid.equals(""))
	query3 = " and head_account_id = '"+hoaid+"'";

try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sub_head_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM subhead where  sub_head_id = '"+sub_head_id+"'"+query1+query2+query3; 
//System.out.println("quesry-->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new subhead(rs.getString("sub_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List<subhead> getDiagnosticHeads()
{
  List<subhead> subHeadsList = new ArrayList<subhead>();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select * from subhead where head_account_id='1' and major_head_id='60' and extra1='453'";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	subhead subhead=new subhead();
	subhead.setsub_head_id(rs.getString("sub_head_id"));
	subhead.setname(rs.getString("name"));
	subhead.setextra2(rs.getString("extra2"));
	subhead.setextra4(rs.getString("extra4"));
	subHeadsList.add(subhead);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return subHeadsList;
}

public void updateDiagnosticHeads(String subHeadId,String extra2,String extra4)
{
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String updateQuery="update subhead set extra2='"+extra2+"',extra4='"+extra4+"' where sub_head_id='"+subHeadId+"';";
System.out.println("updateQuery::::"+updateQuery);
s.executeUpdate(updateQuery);
s.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
}

}