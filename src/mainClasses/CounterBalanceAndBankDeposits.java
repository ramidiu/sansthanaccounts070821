package mainClasses;

import beans.bankbalance;
import beans.customerpurchases;
import dbase.sqlcon.ConnectionHelper;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public class CounterBalanceAndBankDeposits
{
  public CounterBalanceAndBankDeposits() {}
  
  Connection con = null;
  Statement statement = null;
  
  public bankbalance getCounterOpeningBalance(String hoaid, String finYear) {
    bankbalance bankbalance = new bankbalance();
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectquery = "SELECT uniq_id, bank_id, type, bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where  type = 'Counter Cash' and extra2 = '" + hoaid + "' and extra1 = '" + finYear + "'";
      
//      System.out.println("getCounterOpeningBalance ====> " + selectquery);
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        bankbalance.setBank_bal(rs.getString("bank_bal"));
        bankbalance.setExtra2(rs.getString("extra2"));
        bankbalance.setExtra3(rs.getString("extra3"));
        bankbalance.setExtra4(rs.getString("extra4"));
        bankbalance.setExtra5(rs.getString("extra5"));
        bankbalance.setExtra10(rs.getString("extra10"));
      }
      rs.close();
      statement.close();
      con.close();
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println("Exception is ;" + e);
    }
    return bankbalance;
  }
  

  public List<customerpurchases> getcustomerpurchasesAfterFinYearToBeforeFromDate(String hoaid, String fromDate, String toDate, String groupBy)
  {
    List<customerpurchases> customerpurchasesList = new java.util.ArrayList();
    String selectquery = "";
    
    String subQuery = "";
    
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subQuery = " group by date(date)";
    }
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      if (hoaid.equals("4")) {
        selectquery = "SELECT sum(totalAmmount) as totalAmmount, date(date) as date  FROM customerpurchases where date >= '" + fromDate + "' and date <= '" + toDate + "' and extra1 = '" + hoaid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='online success' || cash_type='card' || cash_type='phonepe' || cash_type='googlepay')" + subQuery;
      } else if (hoaid.equals("1")) {
        selectquery = "SELECT sum(totalAmmount) as totalAmmount, date(date) as date  FROM customerpurchases where date >= '" + fromDate + "' and date <= '" + toDate + "' and extra1 = '" + hoaid + "' and (cash_type='cash' || cash_type='cheque')" + subQuery;
      }
      else if (hoaid.equals("3")) {
        selectquery = "SELECT sum(totalAmmount) as totalAmmount, date(date) as date  FROM customerpurchases where date >= '" + fromDate + "' and date <= '" + toDate + "' and extra1 = '" + hoaid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='online success' || cash_type='card')" + subQuery;
      }
      else if (hoaid.equals("5")) {
        selectquery = "SELECT sum(totalAmmount) as totalAmmount, date(date) as date  FROM customerpurchases where date >= '" + fromDate + "' and date <= '" + toDate + "' and extra1 = '" + hoaid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='online success' || cash_type='card')" + subQuery;
      }
      System.out.println("getcustomerpurchasesAfterFinYearToBeforeFromDate ====> " + selectquery);
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        if ((rs.getString("totalAmmount") != null) && (!rs.getString("totalAmmount").trim().equals(""))) {
          customerpurchases customerpurchases = new customerpurchases();
          customerpurchases.setdate(rs.getString("date"));
          customerpurchases.settotalAmmount(rs.getString("totalAmmount"));
          customerpurchasesList.add(customerpurchases);
        }
      }
      rs.close();
      statement.close();
      con.close();
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println("Exception is ;" + e);
    }
    return customerpurchasesList;
  }
  
  public List<beans.banktransactions> getbanktransactionsAfterFinYearToBeforeFromDate(String reportType, String reportType1, String fromDate, String toDate, String groupBy)
  {
    List<beans.banktransactions> banktransactions = new java.util.ArrayList();
    String selectquery = "";
    
    String subQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subQuery = " group by date(date)";
    }
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      

      String report = "";
      if ((reportType != null) && (reportType1 != null)) {
        report = " and (extra4 = '" + reportType + "' OR extra4 ='" + reportType1 + "')";
      }
      

      if (reportType.equals("pro-counter")) {
        selectquery = "SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration, date(date) as date, amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13  FROM banktransactions where date >= '" + fromDate + "' and date < '" + toDate + "' " + report + " and type='deposit' and " + "(extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1013' || bank_id='BNK1019' || bank_id='BNK1016' || bank_id='BNK1027' || bank_id='BNK1028')" + subQuery;
      } else if (reportType.equals("Charity-cash")) {
        selectquery = "SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration, date(date) as date, amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13  FROM banktransactions where date >= '" + fromDate + "' and date < '" + toDate + "' " + report + " and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1012' || bank_id='BNK1029')" + subQuery;
      } else if (reportType.equals("poojastore-counter")) {
        selectquery = "SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration, date(date) as date, amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13  FROM banktransactions where date >= '" + fromDate + "' and date < '" + toDate + "' " + report + " and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1015' || bank_id='BNK1030')" + subQuery;
      } else if (reportType.equals("Sainivas")) {
        selectquery = "SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration, date(date) as date, amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13  FROM banktransactions where date >= '" + fromDate + "' and date < '" + toDate + "'" + report + " and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and ( bank_id='BNK1031' || bank_id='BNK1013' || bank_id='BNK1014')" + subQuery;
      }
      
      System.out.println("getbanktransactionsAfterFinYearToBeforeFromDate ====> " + selectquery);
      
      ResultSet rs = statement.executeQuery(selectquery);
      
      while (rs.next()) {
        banktransactions.add(new beans.banktransactions(rs.getString("banktrn_id"), rs.getString("bank_id"), rs.getString("name"), rs.getString("narration"), rs.getString("date"), rs.getString("amount"), rs.getString("type"), rs.getString("createdBy"), rs.getString("editedBy"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("brs_date"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("sub_head_id"), rs.getString("minor_head_id"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13")));
      }
      rs.close();
      statement.close();
      con.close();
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println("Exception is ;" + e);
    }
    return banktransactions;
  }
}