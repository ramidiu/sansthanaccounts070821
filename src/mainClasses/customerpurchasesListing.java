package mainClasses;

import java.sql.SQLException;
import java.sql.ResultSet;
import beans.customerpurchases;
import dbase.sqlcon.ConnectionHelper;
import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Statement;
import java.sql.Connection;
import java.util.ArrayList;

public class customerpurchasesListing
{
    private String error;
    private String purchaseId;
    ArrayList list;
    Connection c;
    Statement s;
    
    public customerpurchasesListing() {
        this.error = "";
        this.list = new ArrayList();
        this.c = null;
        this.s = null;
    }
    
    public void seterror(final String error) {
        this.error = error;
    }
    
    public String geterror() {
        return this.error;
    }
    
    public void setpurchaseId(final String purchaseId) {
        this.purchaseId = purchaseId;
    }
    
    public String getpurchaseId() {
        return this.purchaseId;
    }
    
    public String getWtsp(final String mblnum) throws IOException {
        final URL url = new URL("https://waoptin.aclwhatsapp.com/api/v1/addoptinpost");
        final HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("accept", "application/json");
        final String jsonInputString = "{\"enterpriseId\": \"shrisbuat\", \"msisdn\": \"91339965801\", \"token\": \"602800f0-e3aa-4078-b055-fdde62d621d9\"}";
        final InputStream responseStream = connection.getInputStream();
        return "";
    }
    
    public List getcustomerpurchases() {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where extra1='3'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesBasedOnFromDateToDateAndHoa(final String fromDate, final String toDate, final String hoa) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20,jvType FROM customerpurchases where date>='" + fromDate + "%' and date<='" + toDate + "%' and extra1='" + hoa + "' and cash_type!='offerKind' order by vocharNumber";
            System.out.println("selectquery::" + selectquery);
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20"), rs.getString("jvType")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getAllJvNumbers(final String fromDate, final String toDate, final String hoa) {
        final List<String> list = new ArrayList<String>();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT vocharNumber FROM customerpurchases where date>='" + fromDate + "%' and date<='" + toDate + "%' and extra1='" + hoa + "' and cash_type!='offerKind' and cash_type = 'journalvoucher' group by vocharNumber";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(rs.getString("vocharNumber"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getTotalSaleQuantityBasedHOA(final String productId, final String HOA) {
        String list = "0";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(quantity) as quantity  FROM customerpurchases where productId='" + productId + "' and extra1='" + HOA + "'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("quantity") != null && !rs.getString("quantity").equals("")) {
                    list = rs.getString("quantity");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getMcustomerpurchases(final String purchaseId) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  purchaseId = '" + purchaseId + "'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesList() {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases group by billingId ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnDateAndHoa(final String HOA, final String date) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and  (date like '" + date + "%') and extra1='" + HOA + "' group by billingId ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getCustomerPhoneSearch(final String phoneno) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT * FROM customerpurchases where phoneno like '" + phoneno + "%' and (extra1='1' or extra1='4')";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getSaleDates() {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id FROM customerpurchases group by date ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(rs.getString("date"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getBillDetails(final String billid) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20,extra29,extra30 FROM customerpurchases where  billingId = '" + billid + "'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20"), rs.getString("extra29"), rs.getString("extra30")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getBillDetailsOfferKind(final String billid) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  billingId like '" + billid + "%'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnHOAandDates(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending'  and extra1='" + hoid + "'" + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedDates(final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type='offerKind'" + date + " and productId='" + productId + "' group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnHOAandDatesAndCahsType(final String hoid, final String fromdate, final String todate, final String cashtype) throws SQLException {
        ResultSet rs = null;
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            String SubQuery = "";
            if (cashtype != null && cashtype.equals("creditsale")) {
                SubQuery = " and cash_type!='cash'";
            }
            else if (cashtype != null && cashtype.equals("Jounral")) {
                SubQuery = " and cash_type='journalvoucher'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending'  and extra1='" + hoid + "'" + date + SubQuery + " order by date";
            rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
            return list;
        }
        finally {
            this.s.close();
            rs.close();
            this.c.close();
        }
        this.s.close();
        rs.close();
        this.c.close();
        return list;
    }
    
    public List getDetailsBasedOnReceiptId(final String hoid, final String fromdate, final String todate, final String receiptId) {
        final ArrayList list = new ArrayList();
        try {
            final String date = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending'  and billingId='" + receiptId + "' and  extra1='" + hoid + "'" + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getDetailsBasedOnReceiptIds(final String hoid, final String fromdate, final String todate, final String receiptId) {
        final ArrayList list = new ArrayList();
        try {
            final String date = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT p.purchaseId,date,p.productId,sum(p.quantity) as quantity,p.rate,sum(p.totalAmmount) as totalAmmount,p.billingId,p.vocharNumber,p.cash_type,p.extra1,p.extra2,p.extra3,p.extra4,p.extra5,p.customername,p.phoneno,p.emp_id,p.gothram,p.image,p.extra6,p.extra7,p.extra8,p.extra9,p.extra10,p.offerkind_refid,p.tendered_cash,p.cheque_no,p.name_on_cheque,p.cheque_narration,p.extra11,p.extra12,p.extra13,p.extra14,p.extra15,p.othercash_deposit_status,p.othercash_totalamount,p.extra16,p.extra17,p.extra18,p.extra19,p.extra20,c.phone,c.pancard_no FROM sansthanaccounts.customerpurchases p,customerapplication c where p.billingId=c.billingId and p.cash_type!='offerKind' and p.cash_type!='online pending'  and (p.billingId='" + receiptId + "' OR c.phone='" + receiptId + "') " + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnHOAandDates1(final String hoid) {
        final ArrayList list = new ArrayList();
        try {
            final String date = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as totalAmmount,purchaseId, date, productId, quantity, rate,  billingId, vocharNumber, cash_type, extra1, extra2, extra3, extra4, extra5, customername, phoneno, emp_id, gothram, image, extra6, extra7, extra8, extra9, extra10, offerkind_refid, tendered_cash, cheque_no, name_on_cheque, cheque_narration, extra11, extra12, extra13, extra14, extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  extra14='00' and extra1='" + hoid + "' group by billingId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalSaleReport(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cheque' or cash_type='cash' or  cash_type='online success' or cash_type='card' or  cash_type='googlepay' or cash_type='phonepe') and extra1='" + hoid + "'" + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getList(final String distinct, final String cash_type, final String hod, final String fromdate, final String todate, final String groupby, final String extra1, final String extra2) {
        final ArrayList list = new ArrayList();
        try {
            String distinc = "";
            String cashtype = "";
            String HOD = "";
            String date = "";
            String groupBy = "";
            String query1 = "";
            if (distinct.equals("distinct")) {
                distinc = " distinct productId";
            }
            else {
                distinc = " purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 ";
            }
            if (cash_type.equals("of")) {
                cashtype = " cash_type='offerKind'";
            }
            else {
                cashtype = " cash_type!='offerKind' and cash_type!='online pending' and cash_type!='journalvoucher' and cash_type!='othercash' ";
            }
            if (!hod.equals("")) {
                HOD = " and extra1 = '" + hod + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (!groupby.equals("")) {
                groupBy = " group by " + groupby;
            }
            if (!extra1.equals("")) {
                query1 = " and productId = '" + extra1 + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT " + distinc + " from customerpurchases where " + cashtype + query1 + HOD + date + groupBy + " order by productId*1 asc";
            System.out.println("customerpurchaseslisting getList query."+selectquery);
            final ResultSet rs = this.s.executeQuery(selectquery);
            if (distinct.equals("distinct")) {
                while (rs.next()) {
                    list.add(rs.getString("productId"));
                }
            }
            else {
                while (rs.next()) {
                    list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List oak(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cash' or  cash_type='online success' or cash_type='card' or cash_type='cheque') and extra1='" + hoid + "'" + date + "group by productId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalSaleReportForMaill(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cash' or  cash_type='online success' or cash_type='card' or cash_type='cheque' or  cash_type='googlepay' or  cash_type='phonepe') and extra1='" + hoid + "'" + date + "group by productId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalSaleReportForMail(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cash' or  cash_type='online success') and extra1='" + hoid + "'" + date + "group by productId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalSaleCollectionSummary(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type!='offerKind' and cash_type!='online pending' and cash_type!='othercash'  and cash_type!='journalvoucher') and extra1='" + hoid + "'" + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalOnlineSaleReport(final String hoid, final String fromdate, final String todate, final String paymenttype) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String type = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (!paymenttype.equals("") && paymenttype != null) {
                type = paymenttype;
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20,extra29,extra30 FROM customerpurchases where cash_type='" + type + "' and extra1='" + hoid + "'" + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20"), rs.getString("extra29"), rs.getString("extra30")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalCardSaleReport(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type='card' and extra1='" + hoid + "'" + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnLedgerProduct(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.purchaseId,date,cp.productId,quantity,cp.rate, sum(totalAmmount) as totalAmmount,cp.billingId,cp.vocharNumber,cp.cash_type,cp.extra1,cp.extra2,cp.extra3,cp.extra4,cp.extra5,cp.customername,cp.phoneno,cp.emp_id,cp.gothram,cp.image,cp.extra6,cp.extra7,cp.extra8,cp.extra9,cp.extra10,cp.offerkind_refid,cp.tendered_cash,cp.cheque_no,cp.name_on_cheque,cp.cheque_narration,cp.extra11,cp.extra12,cp.extra13,cp.extra14,cp.extra15,cp.othercash_deposit_status,cp.othercash_totalamount,cp.extra16,cp.extra17,cp.extra18,cp.extra19,cp.extra20 FROM customerpurchases cp,products p where cp.productId=p.productId and  p.sub_head_id='" + hoid + "' and cp.cash_type!='creditsale' " + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnLedgerSubhead(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.purchaseId,date,cp.productId,quantity,cp.rate, sum(totalAmmount) as totalAmmount,cp.billingId,cp.vocharNumber,cp.cash_type,cp.extra1,cp.extra2,cp.extra3,cp.extra4,cp.extra5,cp.customername,cp.phoneno,cp.emp_id,cp.gothram,cp.image,cp.extra6,cp.extra7,cp.extra8,cp.extra9,cp.extra10,cp.offerkind_refid,cp.tendered_cash,cp.cheque_no,cp.name_on_cheque,cp.cheque_narration,cp.extra11,cp.extra12,cp.extra13,cp.extra14,cp.extra15,cp.othercash_deposit_status,cp.othercash_totalamount,cp.extra16,cp.extra17,cp.extra18,cp.extra19,cp.extra20 FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and  s.sub_head_id='" + hoid + "' " + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnLedgerSubhead2(final String hoid, final String mId, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.purchaseId,date,cp.productId,quantity,cp.rate, sum(totalAmmount) as totalAmmount,cp.billingId,cp.vocharNumber,cp.cash_type,cp.extra1,cp.extra2,cp.extra3,cp.extra4,cp.extra5,cp.customername,cp.phoneno,cp.emp_id,cp.gothram,cp.image,cp.extra6,cp.extra7,cp.extra8,cp.extra9,cp.extra10,cp.offerkind_refid,cp.tendered_cash,cp.cheque_no,cp.name_on_cheque,cp.cheque_narration,cp.extra11,cp.extra12,cp.extra13,cp.extra14,cp.extra15,cp.othercash_deposit_status,cp.othercash_totalamount,cp.extra16,cp.extra17,cp.extra18,cp.extra19,cp.extra20 FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and  s.sub_head_id='" + hoid + "' and s.extra1='" + mId + "' " + date + "group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnLedgerSubhead22(final String hoid, final String subhead, final String mId, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.purchaseId,date,cp.productId,quantity,cp.rate, sum(totalAmmount) as totalAmmount,cp.billingId,cp.vocharNumber,cp.cash_type,cp.extra1,cp.extra2,cp.extra3,cp.extra4,cp.extra5,cp.customername,cp.phoneno,cp.emp_id,cp.gothram,cp.image,cp.extra6,cp.extra7,cp.extra8,cp.extra9,cp.extra10,cp.offerkind_refid,cp.tendered_cash,cp.cheque_no,cp.name_on_cheque,cp.cheque_narration,cp.extra11,cp.extra12,cp.extra13,cp.extra14,cp.extra15,cp.othercash_deposit_status,cp.othercash_totalamount,cp.extra16,cp.extra17,cp.extra18,cp.extra19,cp.extra20 FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and  s.sub_head_id='" + subhead + "' and s.extra1='" + mId + "' and cp.extra1='" + hoid + "' " + date + " and (cp.cash_type!='offerKind' and cp.cash_type!='online pending') group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnLedgerSubhead22New(final String hoid, final String subhead, final String mId, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.purchaseId,date,cp.productId,quantity,cp.rate, sum(totalAmmount) as totalAmmount,cp.billingId,cp.vocharNumber,cp.cash_type,cp.extra1,cp.extra2,cp.extra3,cp.extra4,cp.extra5,cp.customername,cp.phoneno,cp.emp_id,cp.gothram,cp.image,cp.extra6,cp.extra7,cp.extra8,cp.extra9,cp.extra10,cp.offerkind_refid,cp.tendered_cash,cp.cheque_no,cp.name_on_cheque,cp.cheque_narration,cp.extra11,cp.extra12,cp.extra13,cp.extra14,cp.extra15,cp.othercash_deposit_status,cp.othercash_totalamount,cp.extra16,cp.extra17,cp.extra18,cp.extra19,cp.extra20 FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and cp.extra1='" + hoid + "' " + date + " and (cp.cash_type='online success') and" + " cp.extra12 = '10'  group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnsubheadInProductType(final String hoid, final String subhead, final String mId, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.purchaseId,date,cp.productId,quantity,cp.rate, sum(totalAmmount) as totalAmmount,cp.billingId,cp.vocharNumber,cp.cash_type,cp.extra1,cp.extra2,cp.extra3,cp.extra4,cp.extra5,cp.customername,cp.phoneno,cp.emp_id,cp.gothram,cp.image,cp.extra6,cp.extra7,cp.extra8,cp.extra9,cp.extra10,cp.offerkind_refid,cp.tendered_cash,cp.cheque_no,cp.name_on_cheque,cp.cheque_narration,cp.extra11,cp.extra12,cp.extra13,cp.extra14,cp.extra15,cp.othercash_deposit_status,cp.othercash_totalamount,cp.extra16,cp.extra17,cp.extra18,cp.extra19,cp.extra20 FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and  s.sub_head_id='" + subhead + "' and s.extra1='" + mId + "' and cp.extra1='" + hoid + "' " + date + " and cp.cash_type!='online pending' group by billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesDetailListBasedOnHOAandDates(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String product = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                product = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where extra1='" + hoid + "'" + date + product + " group by purchaseId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getProdcutWiseSaleslListBasedOnHOAandDates(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String product = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                product = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where extra1='" + hoid + "'" + date + product + " group by date order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getProdcutWiseSalesListBasedOnProduct(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String product = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                product = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where extra1='" + hoid + "'" + date + product + " group by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getTotalSalesAmount(final String hoaId, final String empId, final String date) {
        String Amt = "";
        try {
            String empCon = "";
            String hoaCon = "";
            if (empId != null && !empId.equals("")) {
                empCon = " and emp_id='" + empId + "'";
            }
            if (hoaId != null && hoaId.equals("")) {
                hoaCon = " and extra1='" + hoaId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as totalAmmount FROM customerpurchases where  date<'" + date + "'" + empCon + hoaCon;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                Amt = rs.getString("totalAmmount");
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumDollarAmt(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(othercash_totalamount) as total FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + "  and  cp.extra1='" + hoa + "' and othercash_deposit_status='Deposited' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumDollarAmt1(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.totalAmmount) as total FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + "  and  cp.extra1='" + hoa + "' and othercash_deposit_status='Deposited' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List<String[]> getLedgerSumDollarAmt11(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa, final String extraField, final String... ids) {
        final String Amt = "00";
        String query3 = "";
        final List<String[]> result = new ArrayList<String[]>();
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.extra7,cp.productId, sum(cp.totalAmmount) as total FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + "  and  cp.extra1='" + hoa + "' and othercash_deposit_status='Deposited' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + query3 + " group by cp.productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] ss = new String[3];
                if (rs.getString("extra7") != null && rs.getString("productId") != null && rs.getString("total") != null) {
                    ss[0] = rs.getString("extra7");
                    ss[1] = rs.getString("productId");
                    ss[2] = rs.getString("total");
                    result.add(ss);
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return result;
    }
    
    public String getLedgerSumDollarAmtBasedOnProduct(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(othercash_totalamount) as total FROM customerpurchases cp,products p where cp.productId=p.sub_head_id and " + reprttypq + "  and  cp.extra1='" + hoa + "' and othercash_deposit_status='Deposited' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumDollarAmtLikeDate(final String shId, final String reporttyp, final String date, final String cashtyp, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(othercash_totalamount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and othercash_deposit_status='Deposited' and cp.extra1='" + hod + "' and cp.date like '" + date + "%'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSum(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,products p where cp.productId=p.productId and " + reprttypq + " and cp.extra1='" + hoa + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'" + cashtypq;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSum1(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and p." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.totalAmmount) as total  FROM customerpurchases cp,products p where cp.productId=p.productId and " + reprttypq + " and cp.extra1='" + hoa + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'" + cashtypq + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List<String[]> getLedgerSum11(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa, final String extraField, final String... ids) {
        final String Amt = "00";
        String query3 = "";
        final List<String[]> result = new ArrayList<String[]>();
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and p." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.extra7, cp.productId, sum(cp.totalAmmount) as total  FROM customerpurchases cp,products p where cp.productId=p.productId and " + reprttypq + " and cp.extra1='" + hoa + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'" + cashtypq + query3 + " group by cp.productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] ss = new String[3];
                if (rs.getString("extra7") != null && rs.getString("productId") != null && rs.getString("total") != null) {
                    ss[0] = rs.getString("extra7");
                    ss[1] = rs.getString("productId");
                    ss[2] = rs.getString("total");
                    result.add(ss);
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return result;
    }
    
    public String getLedgerSum1a(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and p." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.totalAmmount) as total  FROM customerpurchases cp,products p where cp.productId=p.productId and " + reprttypq + " and cp.extra1='" + hoa + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "' and cp.cash_type!='offerKind'" + cashtypq + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List<String[]> getLedgerSum1a1(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa, final String extraField, final String... ids) {
        final String Amt = "00";
        String query3 = "";
        final List<String[]> result = new ArrayList<String[]>();
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and p." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.extra7, cp.productId, sum(cp.totalAmmount) as total  FROM customerpurchases cp,products p where cp.productId=p.productId and " + reprttypq + " and cp.extra1='" + hoa + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "' and cp.cash_type!='offerKind'" + cashtypq + query3 + " group by cp.productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] ss = new String[3];
                if (rs.getString("extra7") != null && rs.getString("productId") != null && rs.getString("total") != null) {
                    ss[0] = rs.getString("extra7");
                    ss[1] = rs.getString("productId");
                    ss[2] = rs.getString("total");
                    result.add(ss);
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return result;
    }
    
    public String getLedgerSumOfSubhead(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and cp.extra1='" + hod + "'and cp.date between'" + finyrfrm + "'and'" + finyrto + "'" + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubheads(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hod, final String extraField, final String... ids) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            String query3 = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and cp.extra1='" + hod + "'and cp.date between'" + finyrfrm + "'and'" + finyrto + "'" + cashtypq + " " + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List<String[]> getLedgerSumOfSubheads1(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hod, final String extraField, final String... ids) {
        final String Amt = "00";
        final List<String[]> result = new ArrayList<String[]>();
        try {
            String cashtypq = "";
            String reprttypq = "";
            String query3 = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.extra7, cp.productId, sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and cp.extra1='" + hod + "'and cp.date between'" + finyrfrm + "'and'" + finyrto + "'" + cashtypq + " " + query3 + " group by cp.productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] ss = new String[3];
                if (rs.getString("extra7") != null && rs.getString("productId") != null && rs.getString("total") != null) {
                    ss[0] = rs.getString("extra7");
                    ss[1] = rs.getString("productId");
                    ss[2] = rs.getString("total");
                    result.add(ss);
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return result;
    }
    
    public String getLedgerSumOfExtra12(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hod) {
        String Amt = "00";
        try {
            String reprttypq = "";
            if (!reporttyp.equals("")) {
                reprttypq = " and s." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.extra12) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id " + reprttypq + " and cp.extra1='" + hod + "'and cp.date between'" + finyrfrm + "'and'" + finyrto + "'and cp.cash_type='online success'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead2(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp3, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "' and cp.cash_type!='" + cashtyp3 + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead1(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead1b(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hod, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " " + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List<String[]> getLedgerSumOfSubhead1b1(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hod, final String extraField, final String... ids) {
        final String Amt = "00";
        String query3 = "";
        final List<String[]> result = new ArrayList<String[]>();
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.extra7, cp.productId, sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " " + query3 + " group by cp.productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] ss = new String[3];
                if (rs.getString("extra7") != null && rs.getString("productId") != null && rs.getString("total") != null) {
                    ss[0] = rs.getString("extra7");
                    ss[1] = rs.getString("productId");
                    ss[2] = rs.getString("total");
                    result.add(ss);
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return result;
    }
    
    public String getLedgerSumOfSubhead11(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and (cp.cash_type!='" + cashtyp + "' and cp.cash_type!='" + cashtyp1 + "' and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "')";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead111(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and (cp.cash_type!='" + cashtyp + "' and cp.cash_type!='" + cashtyp1 + "' and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "')";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "'";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " " + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List<String[]> getLedgerSumOfSubhead1111(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod, final String extraField, final String... ids) {
        final String Amt = "00";
        String query3 = "";
        final String extra1 = "00";
        final String sub_head_id = "00";
        final List<String[]> result = new ArrayList<String[]>();
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and (cp.cash_type!='" + cashtyp + "' and cp.cash_type!='" + cashtyp1 + "' and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "')";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "'";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT s.extra1, s.sub_head_id, sum(cp.totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " " + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] ss = new String[3];
                if (rs.getString("extra1") != null && rs.getString("sub_head_id") != null && rs.getString("total") != null) {
                    ss[0] = rs.getString("extra1");
                    ss[1] = rs.getString("sub_head_id");
                    ss[2] = rs.getString("total");
                    result.add(ss);
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return result;
    }
    
    public String getLedgerSumBasedOnJV(final String shId, final String reporttyp, final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp) {
        String Amt = "00";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            String reprttypq = "";
            String fromtoq = " and (date between '" + finyrfrm + "' and '" + finyrto + "') ";
            if (!reporttyp.equals("")) {
                reprttypq = String.valueOf(reporttyp) + "='" + shId + "' and " + reporttype + "='" + mid + "' ";
            }
            if (finyrto != null && finyrto.equals("like")) {
                fromtoq = " and date like '" + finyrfrm + "%' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total FROM customerpurchases where " + reprttypq + " and cash_type='" + cashtyp + "'" + fromtoq;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumBasedOnJVNew(final String shId, final String reporttyp, final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp) {
        String Amt = "00";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            String reprttypq = "";
            String fromtoq = " and (date between '" + finyrfrm + "' and '" + finyrto + "') ";
            if (!reporttyp.equals("")) {
                reprttypq = String.valueOf(reporttyp) + "='" + shId + "' and " + reporttype + "='" + mid + "' ";
            }
            if (finyrto != null && finyrto.equals("like")) {
                fromtoq = " and date like '" + finyrfrm + "%' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total FROM customerpurchases where  " + reprttypq + " and extra9!='' and cash_type='" + cashtyp + "'" + fromtoq;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumBasedOnJV1(final String shId, final String reporttyp, final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            String reprttypq = "";
            String fromtoq = " and (cp.date between '" + finyrfrm + "' and '" + finyrto + "') ";
            if (!reporttyp.equals("")) {
                reprttypq = "cp." + reporttyp + "='" + shId + "' and " + "cp." + reporttype + "='" + mid + "' ";
            }
            if (finyrto != null && finyrto.equals("like")) {
                fromtoq = " and cp.date like '" + finyrfrm + "%' ";
            }
            if (!extraField.equals(null) && !extraField.trim().equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.totalAmmount) as total FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and cp.extra1=s.head_account_id and cash_type='" + cashtyp + "'" + fromtoq + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List<String[]> getLedgerSumBasedOnJV11(final String shId, final String reporttyp, final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String extraField, final String... ids) {
        final String Amt = "00";
        String query3 = "";
        final List<String[]> result = new ArrayList<String[]>();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            String reprttypq = "";
            String fromtoq = " and (cp.date between '" + finyrfrm + "' and '" + finyrto + "') ";
            if (!reporttyp.equals("")) {
                reprttypq = "cp." + reporttyp + "='" + shId + "' and " + "cp." + reporttype + "='" + mid + "' ";
            }
            if (finyrto != null && finyrto.equals("like")) {
                fromtoq = " and cp.date like '" + finyrfrm + "%' ";
            }
            if (!extraField.equals(null) && !extraField.trim().equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.extra7, cp.productId, sum(cp.totalAmmount) as total FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and cp.extra1=s.head_account_id and cash_type='" + cashtyp + "'" + fromtoq + query3 + " group by cp.productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] ss = new String[3];
                if (rs.getString("extra7") != null && rs.getString("productId") != null && rs.getString("total") != null) {
                    ss[0] = rs.getString("extra7");
                    ss[1] = rs.getString("productId");
                    ss[2] = rs.getString("total");
                    result.add(ss);
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return result;
    }
    
    public String getLedgerSumOfSubheadOfComputerChargesAmt(final String shId, final String reporttyp, final String finyrfrm, final String finyrto, final String cashtyp, final String hoa) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(extra12) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + "  and  cp.extra1='" + hoa + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead2(final String shId, final String reporttyp, final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' and s." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead22(final String shId, final String reporttyp, final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'and cp.cash_type!='" + cashtyp1 + "'and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' and s." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " ";
            shId.equals("21464");
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead221(final String shId, final String reporttyp, final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'and cp.cash_type!='" + cashtyp1 + "'and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' and s." + reporttype + "='" + mid + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " " + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead22Extra12(final String shId, final String reporttyp, final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod) {
        String Amt = "00";
        try {
            String reprttypq = "";
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' and s." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.extra12) as total FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "' and cp.cash_type='online success'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumLikeDate(final String shId, final String reporttyp, final String date, final String cashtyp, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,products p where cp.productId=p.productId and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date like '" + date + "%'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumLikeDate2(final String shId, final String reporttyp, final String mid, final String reporttype, final String date, final String cashtyp, final String cashtype, final String cashtype2, final String cashtype3, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and (cp.cash_type='" + cashtyp + "' OR cp.cash_type='" + cashtype + "' OR cp.cash_type='" + cashtype2 + "' OR cp.cash_type='" + cashtype3 + "')";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "'  and  p." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,products p where cp.productId=p.productId and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date like '" + date + "%'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumLikeDate22(final String shId, final String reporttyp, final String mid, final String reporttype, final String date, final String cashtyp, final String cashtype, final String cashtype2, final String cashtype3, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and (cp.cash_type!='" + cashtyp + "' and cp.cash_type!='" + cashtype + "' and cp.cash_type!='" + cashtype2 + "' and cp.cash_type!='" + cashtype3 + "')";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " p." + reporttyp + "='" + shId + "'  and  p." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,products p where cp.productId=p.productId and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date like '" + date + "%'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumLikeDate22Extra12(final String shId, final String reporttyp, final String mid, final String reporttype, final String date, final String cashtyp, final String cashtype, final String cashtype2, final String cashtype3, final String hod) {
        String Amt = "00";
        try {
            String reprttypq = "";
            if (!reporttyp.equals("")) {
                reprttypq = " and p." + reporttyp + "='" + shId + "'  and  p." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.extra12) as total FROM customerpurchases cp,products p where cp.productId=p.productId " + reprttypq + " and cp.extra1='" + hod + "' and cp.date like '" + date + "%' and cp.cash_type='online success'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOFSubheadLikeDate(final String shId, final String reporttyp, final String date, final String cashtyp, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type='" + cashtyp + "'";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date like '" + date + "%'  " + cashtypq + " group by " + reporttyp;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOFSubheadLikeDate2(final String shId, final String reporttyp, final String mid, final String reporttype, final String date, final String cashtyp, final String cashtype, final String cashtype2, final String cashtype3, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and (cp.cash_type='" + cashtyp + "' OR cp.cash_type='" + cashtype + "' OR cp.cash_type='" + cashtype2 + "' OR cp.cash_type='" + cashtype3 + "')";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' and  s." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date like '" + date + "%'  " + cashtypq + "  ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOFSubheadLikeDate22(final String shId, final String reporttyp, final String mid, final String reporttype, final String date, final String cashtyp, final String cashtype, final String cashtype2, final String cashtype3, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and (cp.cash_type!='" + cashtyp + "' and cp.cash_type!='" + cashtype + "' and cp.cash_type!='" + cashtype2 + "' and cp.cash_type!='" + cashtype3 + "')";
            }
            if (!reporttyp.equals("")) {
                reprttypq = " s." + reporttyp + "='" + shId + "' and  s." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date like '" + date + "%'  " + cashtypq + "  ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOFSubheadLikeDate22Extra12(final String shId, final String reporttyp, final String mid, final String reporttype, final String date, final String cashtyp, final String cashtype, final String cashtype2, final String cashtype3, final String hod) {
        String Amt = "00";
        try {
            String reprttypq = "";
            if (!reporttyp.equals("")) {
                reprttypq = " and s." + reporttyp + "='" + shId + "' and  s." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.extra12) as total FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id " + reprttypq + " and cp.extra1='" + hod + "' and cp.date like '" + date + "%' and cp.cash_type='online success'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List getcustomerPurchasesPROCollectionSummary(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending' and cash_type!='journalvoucher' and cash_type!='othercash' and extra1='" + hoid + "'" + date + " group by productId order by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerPurchasesPROCollectionSummaryNew(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending' and cash_type!='journalvoucher' and cash_type!='othercash' and extra1='" + hoid + "'" + date;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerPurchasesPROCollectionSummaryBasedOnProduct(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20,extra29,extra30 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending' and productId='" + productId + "' and extra1='" + hoid + "'" + date + " group by billingId,productId,extra5,cash_type order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20"), rs.getString("extra29"), rs.getString("extra30")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionSummaryOnProduct(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String conHoa = "";
            if (hoid != null && !hoid.equals("") && !hoid.equals("of")) {
                conHoa = " and extra1='" + hoid + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  cash_type='offerKind' and productId='" + productId + "' " + date + conHoa + "group by billingId,productId,extra5,cash_type order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerPurchasesPROCollectionDetailReport(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String pro = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                pro = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending' and extra1='" + hoid + "'" + date + pro + " group by productId,extra5,billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getSEVAS(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String pro = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                pro = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending' and extra1='" + hoid + "'" + date + " group by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getofferkindSEVAS(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String pro = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                pro = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type='offerKind' and extra1='" + hoid + "'" + date + pro + " group by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getofferkindSEVASDaily(final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String pro = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                pro = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type='offerKind'" + date + pro + " group by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getSalesDetailsBasedOnCode(final String productId) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  productId = '" + productId + "'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionSummary(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String conHoa = "";
            if (hoid != null && !hoid.equals("") && !hoid.equals("of")) {
                conHoa = " and extra1='" + hoid + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  cash_type='offerKind' " + date + conHoa + "group by productId,extra5,billingId order by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionSummaryNew(final String hoid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String conHoa = "";
            if (hoid != null && !hoid.equals("") && !hoid.equals("of")) {
                conHoa = " and extra1='" + hoid + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  cash_type='offerKind' " + date + conHoa;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionSummaryList(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String conHoa = "";
            String pro = "";
            if (hoid != null && !hoid.equals("") && !hoid.equals("of")) {
                conHoa = " and extra1='" + hoid + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                pro = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  cash_type='offerKind' " + date + conHoa + pro + "group by productId,extra5,billingId order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionSummaryListMonthly(final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            final String conHoa = "";
            String pro = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (productId != null && !productId.equals("")) {
                pro = " and productId='" + productId + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  cash_type='offerKind' " + date + pro + "group by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionSummaryss(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String conHoa = "";
            if (hoid != null && !hoid.equals("") && !hoid.equals("of")) {
                conHoa = " and extra1='" + hoid + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='offerKind' AND productId='20026') " + date + conHoa + "group by productId,date order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionsSummaryss(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String conHoa = "";
            if (hoid != null && !hoid.equals("") && !hoid.equals("of")) {
                conHoa = " and extra1='" + hoid + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='offerKind' AND productId='20021') " + date + conHoa + "group by productId,date order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionSummarys(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String conHoa = "";
            if (hoid != null && !hoid.equals("") && !hoid.equals("of")) {
                conHoa = " and extra1='" + hoid + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where productId='20032' " + date + conHoa + "group by productId,date order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOfferKindsPROCollectionsSummarys(final String hoid, final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String conHoa = "";
            if (hoid != null && !hoid.equals("") && !hoid.equals("of")) {
                conHoa = " and extra1='" + hoid + "'";
            }
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where productId='20027' " + date + conHoa + "group by productId,date order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getCustomersListForPoojas(final String hoa, final String fromdate, final String todate, final String productCode) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and productId='" + productCode + "' and extra1!='3'" + date + " group by purchaseId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getDevoteesList(final String productCode) {
        final ArrayList list = new ArrayList();
        String orderByQuery = "";
        try {
            if (productCode != null && productCode.equals("20063")) {
                orderByQuery = " order by SUBSTRING(ca.phone ,5)*1 asc ";
            }
            else if (productCode != null && productCode.equals("20092")) {
                orderByQuery = " order by SUBSTRING(ca.phone ,4)*1 asc ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.purchaseId,cp.date,cp.productId,sum(cp.quantity) as quantity,cp.rate,sum(cp.totalAmmount) as totalAmmount,cp.billingId,cp.vocharNumber,cp.cash_type,cp.extra1,cp.extra2,cp.extra3,cp.extra4,cp.extra5,cp.customername,cp.phoneno,cp.emp_id,cp.gothram,cp.image,cp.extra6,cp.extra7,cp.extra8,cp.extra9,cp.extra10,cp.offerkind_refid,cp.tendered_cash,cp.cheque_no,cp.name_on_cheque,cp.cheque_narration,cp.extra11,cp.extra12,cp.extra13,cp.extra14,cp.extra15,cp.othercash_deposit_status,cp.othercash_totalamount,cp.extra16,cp.extra17,cp.extra18,cp.extra19,cp.extra20 FROM customerpurchases cp,customerapplication ca where cp.billingId = ca.billingId and (cp.cash_type!='offerKind' and cp.cash_type!='online pending') and cp.productId='" + productCode + "' and cp.extra1!='3' group by cp.purchaseId " + orderByQuery;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List searchAndGetDevoteesList(final customerpurchases CP, final String productCode) {
        final StringBuilder addQuery = new StringBuilder("");
        if (CP.getcustomername() != null && !CP.getcustomername().trim().equals("") && !CP.getcustomername().trim().equals("null")) {
            addQuery.append(" and cp.customername like '%" + CP.getcustomername() + "%' ");
        }
        if (CP.getgothram() != null && !CP.getgothram().trim().equals("") && !CP.getgothram().trim().equals("null")) {
            addQuery.append(" and cp.gothram like '%" + CP.getgothram() + "%' ");
        }
        if (CP.getExtra20() != null && !CP.getExtra20().trim().equals("") && !CP.getExtra20().trim().equals("null")) {
            addQuery.append(" and ca.last_name like '%" + CP.getExtra20() + "%' ");
        }
        if (CP.getExtra19() != null && !CP.getExtra19().trim().equals("") && !CP.getExtra19().trim().equals("null")) {
            addQuery.append(" and ca.phone like '%" + CP.getExtra19() + "%' ");
        }
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.purchaseId,cp.date,cp.productId,cp.quantity,cp.rate,cp.totalAmmount,cp.billingId,cp.vocharNumber,cp.cash_type,cp.extra1,cp.extra2,cp.extra3,cp.extra4,cp.extra5,cp.customername,cp.phoneno,cp.emp_id,cp.gothram,cp.image,cp.extra6,cp.extra7,cp.extra8,cp.extra9,cp.extra10,cp.offerkind_refid,cp.tendered_cash,cp.cheque_no,cp.name_on_cheque,cp.cheque_narration,cp.extra11,cp.extra12,cp.extra13,cp.extra14,cp.extra15,cp.othercash_deposit_status,cp.othercash_totalamount,cp.extra16,cp.extra17,cp.extra18,cp.extra19,cp.extra20 FROM customerpurchases cp,customerapplication ca where (cp.cash_type!='offerKind' and cp.cash_type!='online pending') and cp.productId='" + productCode + "' and cp.billingId = ca.billingId and cp.extra1!='3'" + (Object)addQuery + " group by purchaseId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List checkCustomerExists(final String name, final String phone, final String email) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where  (SELECT sum(totalAmmount) FROM customerpurchases where extra1!='3' and customername = '" + name + "' and phoneno = '" + phone + "') >'9999' ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getSalesListForVatReport(final String hoid, final String fromdate, final String todate, final String proID, final String vatPer) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String product = "";
            String vat = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (proID != null && !proID.equals("")) {
                product = " and productId='" + proID + "'";
            }
            if (vatPer != null && !vatPer.equals("")) {
                vat = " and productId in ( select productId from products where vat1='" + vatPer + "')";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,sum(ceil(rate+rate*extra13/100)*quantity) as rate,sum(totalAmmount) as totalAmmount,billingId, sum(ceil(rate*extra13/100)*quantity) as  vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and extra1='" + hoid + "'" + date + product + vat + " group by productId,billingId,date order by date ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getSalesListForVatReportNew(final String hoid, final String fromdate, final String todate, final String proID, final String vatPer) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String product = "";
            String vat = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (proID != null && !proID.equals("")) {
                product = " and productId='" + proID + "'";
            }
            if (vatPer != null && !vatPer.equals("")) {
                vat = " and productId in ( select productId from products where vat1='" + vatPer + "')";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity ,sum(rate*quantity) as rate,sum(totalAmmount) as totalAmmount,billingId, sum(rate*quantity) as  vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and extra1='" + hoid + "'" + date + product + vat + " group by productId ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalSalesForVatSummaryReport(final String hoid, final String fromdate, final String todate, final String proID, final String vatPer) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String product = "";
            String vat = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (proID != null && !proID.equals("")) {
                product = " and productId='" + proID + "'";
            }
            if (vatPer != null && !vatPer.equals("")) {
                vat = " and productId in ( select productId from products where vat1='" + vatPer + "')";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,sum((rate+ceil(rate*extra13/100))*quantity) as rate,sum(totalAmmount) as totalAmmount,billingId, sum(ceil(rate*extra13/100)*quantity) as vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and extra1='" + hoid + "'" + date + product + vat;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalSalesForVatSummaryReportNew(final String hoid, final String fromdate, final String todate, final String proID, final String vatPer) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String product = "";
            String vat = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (proID != null && !proID.equals("")) {
                product = " and productId='" + proID + "'";
            }
            if (vatPer != null && !vatPer.equals("")) {
                vat = " and productId in ( select productId from products where vat1='" + vatPer + "')";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,sum(rate*quantity) as rate,sum(totalAmmount) as totalAmmount,billingId, sum(rate*quantity) as vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and extra1='" + hoid + "'" + date + product + vat;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getTotalSalesForVatSummaryReportOld(final String hoid, final String fromdate, final String todate, final String proID, final String vatPer) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            String product = "";
            String vat = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            if (proID != null && !proID.equals("")) {
                product = " and productId='" + proID + "'";
            }
            if (vatPer != null && !vatPer.equals("")) {
                vat = " and productId in ( select productId from products where vat1='" + vatPer + "')";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,sum(totalAmmount-(rate*extra13/100)*quantity) as rate,sum(totalAmmount) as totalAmmount,billingId, sum(rate*quantity) as vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and extra1='" + hoid + "'" + date + product + vat;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getReceipts(final String majorHeadid, final String hoaid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where productId in (select sub_head_id from subhead where major_head_id='" + majorHeadid + "' and head_account_id='" + hoaid + "') and (date between '" + fromdate + "' and '" + todate + "') group by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getReceiptsCounterCash(final String majorHeadid, final String hoaid, final String fromdate) {
        double list = 0.0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where productId in (select sub_head_id from subhead where major_head_id='" + majorHeadid + "' and head_account_id='" + hoaid + "') and (date like '" + fromdate + "%') group by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list += Double.parseDouble(rs.getString("totalAmmount"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getDayWiseIncome(final String hoaid, final String fromdate) {
        double list = 0.0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and  (date like '" + fromdate + "%') and extra1='" + hoaid + "' ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list = Double.parseDouble(rs.getString("totalAmmount"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getDepartmentDayWiseIncome(final String hoaid, final String fromdate) {
        double list = 0.0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type='cash' and  (date like '" + fromdate + "%') and extra1='" + hoaid + "'  ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next() && rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").trim().equals("")) {
                list += Double.parseDouble(rs.getString("totalAmmount"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
        }
        return list;
    }
    
    public double getHundiCollectionNDDayWiseIncome(final String hoaid, final String productid, final String fromdate) {
        double list = 0.0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cash' || cash_type='cheque') and  productId='" + productid + "' and (date like '" + fromdate + "%') and extra1='" + hoaid + "'  ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next() && rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").trim().equals("")) {
                list += Double.parseDouble(rs.getString("totalAmmount"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is  mainClasses.customerpurchasesListing.java(){getDepartmentDayWiseIncome(String hoaid,String fromdate)}==" + e);
        }
        return list;
    }
    
    public double getDayWiseIncomeBasedEmployeID(final String hoaid, final String fromdate, final String employeid) {
        double list = 0.0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type='cash' and  (date like '" + fromdate + "%') and extra1='" + hoaid + "' and emp_id='" + employeid + "' ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list = Double.parseDouble(rs.getString("totalAmmount"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getPoojastoresReceipts(final String majorHeadid, final String hoaid, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.productId,sum(cp.totalAmmount) as totalAmmount,sh.sub_head_id,sh.name FROM customerpurchases cp,subhead sh,products pr where  cp.extra1='" + hoaid + "' and sh.head_account_id='" + hoaid + "' and pr.productId=cp.productId and sh.sub_head_id=pr.sub_head_id and (cp.date between '" + fromdate + "' and '" + todate + "') group by sh.sub_head_id";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("productId"), rs.getString("totalAmmount"), rs.getString("sub_head_id"), rs.getString("name")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getCounterCashReceipts(final String majorHeadid, final String hoaid, final String fromdate) {
        double list = 0.0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.productId,sum(cp.totalAmmount) as totalAmmount,sh.sub_head_id,sh.name FROM customerpurchases cp,subhead sh,products pr where cp.extra1='" + hoaid + "' and sh.head_account_id='" + hoaid + "'  and pr.productId=cp.productId and sh.sub_head_id=pr.sub_head_id and (date like '" + fromdate + "%') group by sh.sub_head_id";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list += Double.parseDouble(rs.getString("totalAmmount"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getProductSaleQty(final String proId, final String fromdate, final String toDate) {
        String list = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and  date >='" + fromdate + "'and date <='" + toDate + "' and productId='" + proId + "' ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list = rs.getString("quantity");
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getProductSaleQty1(final String proId, final String fromdate, final String toDate) {
        double list = 0.0;
        try {
            String subQuery = "";
            if (toDate != null && !toDate.equals("")) {
                subQuery = "and date <='" + toDate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and date >'2014-12-06 23:59:59' and date <'" + fromdate + "'" + subQuery + " and productId='" + proId + "' ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list = Double.parseDouble(rs.getString("quantity"));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getDaySaleQty(final String proId, final String fromdate) {
        double list = 0.0;
        try {
            final String subQuery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and  date like '" + fromdate + "%'" + subQuery + " and productId='" + proId + "' ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("quantity") != null) {
                    list = Double.parseDouble(rs.getString("quantity"));
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getTotalSaleQuantityBasedHOAOnDate(final String productId, final String HOA) {
        String list = "0";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(quantity) as quantity  FROM customerpurchases where productId='" + productId + "' and extra1='" + HOA + "' and date>'2014-12-06 23:59:59' group by productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("quantity") != null && !rs.getString("quantity").equals("")) {
                    list = rs.getString("quantity");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String[] getTotalSaleQuantityBasedHOAOnOfferKind(final String offerkinfId) {
        final String[] list = new String[2];
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT rate,date FROM customerpurchases where offerkind_refid='" + offerkinfId + "'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list[0] = rs.getString("rate");
                list[1] = rs.getString("date");
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getTotAmt(final String fromdate, final String todate) {
        double list = 0.0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT extra14 FROM customerpurchases where  extra1='3' and date >='" + fromdate + "' and date <='" + todate + "' group by billingId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("extra14") != null && !rs.getString("extra14").equals("")) {
                    list += Double.parseDouble(rs.getString("extra14"));
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getTotAmtWithOutRoundOff(final String fromdate, final String todate) {
        double list = 0.0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as totalAmmount FROM customerpurchases where  extra1='3' and date >='" + fromdate + "' and date <='" + todate + "' group by billingId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").equals("")) {
                    list += Double.parseDouble(rs.getString("totalAmmount"));
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public double getTotAmtWithCon(final String fromdate, final String todate, final String cashtype) {
        double list = 0.0;
        try {
            String subQuery = "";
            if (cashtype.equals("cash")) {
                subQuery = " and cash_type='" + cashtype + "'";
            }
            else {
                subQuery = " and cash_type!='" + cashtype + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT extra14 FROM customerpurchases where  extra1='3' and date >='" + fromdate + "' and date <='" + todate + "'" + subQuery + " group by billingId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("extra14") != null && !rs.getString("extra14").equals("")) {
                    list += Double.parseDouble(rs.getString("extra14"));
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getTotalQty(final String proId, final String fromdate) {
        String list = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(quantity) as quantity   FROM customerpurchases where  extra1='3' and date >= '2014-12-07 00:00:01' and date <'" + fromdate + "' and productId='" + proId + "'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("quantity") != null && !rs.getString("quantity").equals("")) {
                    list = String.valueOf(list) + Double.parseDouble(rs.getString("quantity"));
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getkindshawlBefore(final String date, final String productId) {
        String list = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(quantity) as quantity FROM customerpurchases where date < '" + date + "-%' and productId='20026'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list = rs.getString("quantity");
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getkindGoldBefore(final String date, final String productId) {
        String list = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(quantity) as quantity FROM customerpurchases where date < '" + date + "-%' and productId='20021'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list = rs.getString("quantity");
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getsaleshawlbyBefore(final String month, final String productid) {
        String list = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(quantity) as quantity FROM customerpurchases  where date < '" + month + "-%'and productId='20032'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list = rs.getString("quantity");
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getKindSilverbyBefore(final String month, final String productid) {
        String list = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(quantity) as quantity FROM customerpurchases  where date < '" + month + "-%'and productId='20027'";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list = rs.getString("quantity");
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List GetDetailsCustomerBasedOnDates(final String fromdate, final String todate, final String productId) {
        final ArrayList list = new ArrayList();
        final String date = "";
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (productId.equals("20064") || productId.equals("20070") || productId.equals("20050") || productId.equals("20071") || productId.equals("20067") || productId.equals("20093") || productId.equals("20094") || productId.equals("20096") || productId.equals("25303") || productId.equals("21539") || productId.equals("25669")) {
                selectquery = "SELECT * FROM customerpurchases where extra3 between '" + fromdate + "' and '" + todate + "' and productId='" + productId + "'";
            }
            else if (productId.equals("20091") || productId.equals("20054") || productId.equals("20055") || productId.equals("21541") || productId.equals("21456") || productId.equals("25651")) {
                selectquery = "SELECT * FROM customerpurchases where extra3 between '" + fromdate + "' and '" + todate + "' and productId='" + productId + "'";
            }
            else if (productId.equals("21547") || productId.equals("21548") || productId.equals("25304") || productId.equals("21544") || productId.equals("21545") || productId.equals("25302") || productId.equals("21550") || productId.equals("21551") || productId.equals("21553") || productId.equals("21585") || productId.equals("20066") || productId.equals("20095") || productId.equals("25833")) {
                selectquery = "SELECT * FROM customerpurchases where extra3 <= '" + fromdate + "' and extra4 >= '" + todate + "' and productId='" + productId + "'";
            }
            else if (productId.equals("21546") || productId.equals("21543") || productId.equals("21549") || productId.equals("21552")) {
                selectquery = "SELECT * FROM customerpurchases where productId='" + productId + "'";
            }
            else if (!productId.equals("20063") && !productId.equals("20092")) {
                selectquery = "SELECT * FROM customerpurchases where date between '" + fromdate + "' and '" + todate + "' and productId='" + productId + "'";
            }
            if (!selectquery.equals("")) {
                final ResultSet rs = this.s.executeQuery(selectquery);
                while (rs.next()) {
                    list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
                }
                this.s.close();
                rs.close();
                this.c.close();
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List GetDetailsCustomerBasedOnsdateAndEdate(final String fromdate, final String todate, final String productId, final String action) {
        final ArrayList list = new ArrayList();
        final String date = "";
        String query = "";
        try {
            final String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (action.equals("startdate")) {
                query = "SELECT * FROM customerpurchases where extra3 between '" + fromdate + "' and '" + todate + "' and productId='20066'";
            }
            else if (action.equals("enddate")) {
                query = "SELECT * FROM customerpurchases where extra4 between '" + fromdate + "' and '" + todate + "' and productId='20066'";
            }
            final ResultSet rs = this.s.executeQuery(query);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getLedgerSumOfSubhead3(final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'and cp.cash_type!='" + cashtyp1 + "'and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "'";
            }
            if (!reporttype.equals("")) {
                reprttypq = " s." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead31(final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'and cp.cash_type!='" + cashtyp1 + "'and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "'";
            }
            if (!reporttype.equals("")) {
                reprttypq = " s." + reporttype + "='" + mid + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(cp.totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " " + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead4(final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod, final String value) {
        String Amt = "00";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'and cp.cash_type!='" + cashtyp1 + "'and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "'";
            }
            if (!reporttype.equals("")) {
                reprttypq = " s." + reporttype + "='" + mid + "' ";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "'and s.extra5='" + value + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " ";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public String getLedgerSumOfSubhead41(final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod, final String extraField, final String... ids) {
        String Amt = "00";
        String query3 = "";
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'and cp.cash_type!='" + cashtyp1 + "'and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "'";
            }
            if (!reporttype.equals("")) {
                reprttypq = " s." + reporttype + "='" + mid + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " " + query3;
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("total") != null) {
                    Amt = rs.getString("total");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return Amt;
    }
    
    public List<String[]> getLedgerSumOfSubhead411(final String mid, final String reporttype, final String finyrfrm, final String finyrto, final String cashtyp, final String cashtyp1, final String cashtyp2, final String cashtyp3, final String hod, final String extraField, final String... ids) {
        final String Amt = "00";
        String query3 = "";
        final List<String[]> result = new ArrayList<String[]>();
        try {
            String cashtypq = "";
            String reprttypq = "";
            if (!cashtyp.equals("")) {
                cashtypq = " and cp.cash_type!='" + cashtyp + "'and cp.cash_type!='" + cashtyp1 + "'and cp.cash_type!='" + cashtyp2 + "' and cp.cash_type!='" + cashtyp3 + "'";
            }
            if (!reporttype.equals("")) {
                reprttypq = " s." + reporttype + "='" + mid + "' ";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT cp.extra7, cp.productId, sum(totalAmmount) as total  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id  and " + reprttypq + " and cp.extra1='" + hod + "' and cp.date between '" + finyrfrm + "' and '" + finyrto + "'  " + cashtypq + " " + query3 + " group by cp.productId";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] ss = new String[3];
                if (rs.getString("extra7") != null && rs.getString("productId") != null && rs.getString("total") != null) {
                    ss[0] = rs.getString("extra7");
                    ss[1] = rs.getString("productId");
                    ss[2] = rs.getString("total");
                    result.add(ss);
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return result;
    }
    
    public String[] getTotalamountBasedOnHoid(final String hoid, final String fromdate, final String todate) {
        final String[] list = { null, "0.0" };
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (hoid.equals("4")) {
                list[0] = "4";
                selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cash' || cash_type='cheque' || cash_type='card' || cash_type='online success') and extra1='" + hoid + "' " + date + " order by date ";
            }
            else if (hoid.equals("1")) {
                list[0] = "1";
                selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cash' || cash_type='cheque') and extra1='" + hoid + "' " + date + " order by date ";
            }
            else if (hoid.equals("3")) {
                list[0] = "3";
                selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cash' || cash_type='cheque') and extra1='" + hoid + "' " + date + " order by date ";
            }
            else if (hoid.equals("5")) {
                list[0] = "5";
                selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='cash' || cash_type='cheque'|| cash_type='card' || cash_type='online success') and extra1='" + hoid + "' " + date + " order by date ";
            }
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("extra1") != null && !rs.getString("extra1").equals("null")) {
                    list[0] = rs.getString("extra1");
                }
                if (rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").equals("null")) {
                    list[1] = rs.getString("totalAmmount");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String[] getTotalamountBasedOnOnlineCard(final String hoid, final String fromdate, final String todate) {
        final String[] list = { null, "0.0" };
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (hoid.equals("1")) {
                list[0] = "1";
                selectquery = "SELECT purchaseId,date,productId,quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where (cash_type='card' || cash_type='online success') and extra1='" + hoid + "' " + date + " order by date ";
            }
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("extra1") != null && !rs.getString("extra1").equals("null")) {
                    list[0] = rs.getString("extra1");
                }
                if (rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").equals("null")) {
                    list[1] = rs.getString("totalAmmount");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getTotalAmountBeforeFromdate(final String hoid, final String date, final String fromdate) {
        String list = "0";
        String selectquery = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (hoid.equals("4")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='card' || cash_type='online success' || cash_type='googlepay' || cash_type='phonepe')";
            }
            else if (hoid.equals("1")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='cash' || cash_type='cheque')";
            }
            else if (hoid.equals("3")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='online success' || cash_type='card')";
            }
            else if (hoid.equals("5")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='online success' || cash_type='card')";
            }
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").equals("")) {
                    list = rs.getString("totalAmmount");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getTotalAmountBeforeFromdateCardOnline(final String hoid, final String date, final String fromdate) {
        String list = "0";
        String selectquery = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (hoid.equals("1")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='card' || cash_type='online success')";
            }
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").equals("")) {
                    list = rs.getString("totalAmmount");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getTotalAmountBeforeFromdate2(final String hoid, final String date, final String fromdate) {
        String list = "0";
        String selectquery = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (hoid.equals("4")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='online success' || cash_type='card' )";
            }
            else if (hoid.equals("1")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='cash' || cash_type='cheque')";
            }
            else if (hoid.equals("3")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='online success' || cash_type='card')";
            }
            else if (hoid.equals("5")) {
                selectquery = "SELECT sum(totalAmmount) as totalAmmount  FROM customerpurchases where date >='" + date + "' and date<='" + fromdate + "' and extra1='" + hoid + "' and (cash_type='cash' || cash_type='cheque' || cash_type='online success' || cash_type='card')";
            }
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").equals("")) {
                    list = rs.getString("totalAmmount");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public String getTotalAmountBeforeFromdate1(final String hoid, final String date, final String fromdate, final String extraField, final String... ids) {
        String list = "0";
        String selectquery = "";
        String query3 = "";
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (hoid.equals("4")) {
                selectquery = "SELECT sum(cp.totalAmmount) as totalAmmount  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and cp.date >='" + date + "' and cp.date<='" + fromdate + "' and cp.extra1='" + hoid + "' and (cp.cash_type='cash' || cp.cash_type='cheque' || cp.cash_type='online success' || cp.cash_type='card' )";
            }
            else if (hoid.equals("1")) {
                selectquery = "SELECT sum(cp.totalAmmount) as totalAmmount  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and cp.date >='" + date + "' and cp.date<='" + fromdate + "' and cp.extra1='" + hoid + "' and (cp.cash_type='cash' || cp.cash_type='cheque' || cp.cash_type='online success' || cp.cash_type='card')";
            }
            else if (hoid.equals("3")) {
                selectquery = "SELECT sum(cp.totalAmmount) as totalAmmount  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and cp.date >='" + date + "' and cp.date<='" + fromdate + "' and cp.extra1='" + hoid + "' and (cp.cash_type='cash' || cp.cash_type='cheque' || cp.cash_type='online success' || cp.cash_type='card')";
            }
            else if (hoid.equals("5")) {
                selectquery = "SELECT sum(cp.totalAmmount) as totalAmmount  FROM customerpurchases cp,subhead s where cp.productId=s.sub_head_id and cp.date >='" + date + "' and cp.date<='" + fromdate + "' and cp.extra1='" + hoid + "' and (cp.cash_type='cash' || cp.cash_type='cheque' || cp.cash_type='online success' || cp.cash_type='card')";
            }
            if (!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")) {
                for (final String id : ids) {
                    query3 = String.valueOf(query3) + "and s." + extraField + "!='" + id + "'";
                }
                selectquery = String.valueOf(selectquery) + query3;
            }
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                if (rs.getString("totalAmmount") != null && !rs.getString("totalAmmount").equals("")) {
                    list = rs.getString("totalAmmount");
                }
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getDollaramountBasedOnHOA(final String hoid) {
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type='othercash' and othercash_deposit_status!='Deposited' and extra1 = '" + hoid + "' group by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getOnlineBookingsBasedOnDates(final String fromdate, final String todate, final String productId) {
        final String onlyFromDate = fromdate.substring(0, 10);
        final String onlyToDate = todate.substring(0, 10);
        String query = "";
        if (productId.equals("20091")) {
            query = " and rate >= 2500";
        }
        final ArrayList list = new ArrayList();
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            selectquery = "SELECT * FROM customerpurchases where extra11 >= '" + onlyFromDate + "' and extra11 <= '" + onlyToDate + "' and cash_type = 'online success' and productId='" + productId + "'" + query;
            if (!selectquery.equals("")) {
                final ResultSet rs = this.s.executeQuery(selectquery);
                while (rs.next()) {
                    list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
                }
                this.s.close();
                rs.close();
                this.c.close();
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List<String[]> getOfferKindProductList() {
        final ArrayList<String[]> list = new ArrayList<String[]>();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT productId,extra1 FROM customerpurchases where cash_type='offerKind' group by productId order by productId ASC";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                final String[] arr = { rs.getString("productId"), rs.getString("extra1") };
                list.add(arr);
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List<customerpurchases> getOfferKindsListingBasedOnProductId(String fromdate, String todate, final String productId) {
        final ArrayList<customerpurchases> list = new ArrayList<customerpurchases>();
        fromdate = String.valueOf(fromdate) + " 00:00:01";
        todate = String.valueOf(todate) + " 23:59:59";
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            if (!productId.equals("All")) {
                selectquery = "SELECT * FROM customerpurchases where date >= '" + fromdate + "' and date <= '" + todate + "' and cash_type = 'offerKind' and productId='" + productId + "'";
            }
            else {
                selectquery = "SELECT * FROM customerpurchases where date >= '" + fromdate + "' and date <= '" + todate + "' and cash_type = 'offerKind'";
            }
            if (!selectquery.equals("")) {
                final ResultSet rs = this.s.executeQuery(selectquery);
                while (rs.next()) {
                    list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
                }
                this.s.close();
                rs.close();
                this.c.close();
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List<customerpurchases> getLastOneHourList(final String fromdate, final String todate) {
        final ArrayList<customerpurchases> list = new ArrayList<customerpurchases>();
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            selectquery = "SELECT * FROM customerpurchases where date >= '" + fromdate + "' and date <= '" + todate + "'";
            if (!selectquery.equals("")) {
                final ResultSet rs = this.s.executeQuery(selectquery);
                while (rs.next()) {
                    list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
                }
                this.s.close();
                rs.close();
                this.c.close();
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List<customerpurchases> getLastOneHourListReceipts(final String fromdate, final String todate, final String offerKind) {
        final ArrayList<customerpurchases> list = new ArrayList<customerpurchases>();
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            selectquery = "SELECT * FROM customerpurchases where date >= '" + fromdate + "' and date <= '" + todate + "' and cash_type!='" + offerKind + "'";
            System.out.println("selectQuery::::" + selectquery);
            if (!selectquery.equals("")) {
                final ResultSet rs = this.s.executeQuery(selectquery);
                while (rs.next()) {
                    list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
                }
                this.s.close();
                rs.close();
                this.c.close();
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List<customerpurchases> getLastOneHourListOfferkind(final String fromdate, final String todate, final String offerKind) {
        final ArrayList<customerpurchases> list = new ArrayList<customerpurchases>();
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            selectquery = "SELECT * FROM customerpurchases where date >= '" + fromdate + "' and date <= '" + todate + "' and cash_type='" + offerKind + "'";
            if (!selectquery.equals("")) {
                final ResultSet rs = this.s.executeQuery(selectquery);
                while (rs.next()) {
                    list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
                }
                this.s.close();
                rs.close();
                this.c.close();
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List<customerpurchases> getLastOneHourCustomer(final String fromdate, final String todate, final String purchaseId) {
        final ArrayList<customerpurchases> list = new ArrayList<customerpurchases>();
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            selectquery = "SELECT * FROM customerpurchases where date >= '" + fromdate + "' and date <= '" + todate + "' and purchaseId = '" + purchaseId + "'";
            if (!selectquery.equals("")) {
                final ResultSet rs = this.s.executeQuery(selectquery);
                while (rs.next()) {
                    list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
                }
                this.s.close();
                rs.close();
                this.c.close();
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List<customerpurchases> getBillDetailsOfferKindBesedOnFromDateAndToDate(final String pId, final String ofk, final String cashtype, final String offerkindrefid, final String fromDate, final String currentDate) {
        String productId = "";
        System.out.println("Pid is ::: " + pId);
        if (pId.equals("20032")) {
            System.out.println("This is from the sale of shawl:::: ");
            productId = "20026";
        }
        if (pId.equals("25771") || pId.equals("25794")) {
            System.out.println("This is from the SALE OF GOLD and MELTING OF GOLD:::: ");
            productId = "20021";
        }
        if (pId.equals("20033") || pId.equals("25795")) {
            System.out.println("This is from the SALE OF SILVER and MELTING OF SILVER:::: ");
            productId = "20027";
        }
        final ArrayList<customerpurchases> list = new ArrayList<customerpurchases>();
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            selectquery = "SELECT * FROM customerpurchases where productId='" + productId + "' and  billingId like '" + ofk + "%' and cash_type='" + cashtype + "' and offerkind_refid!='" + offerkindrefid + "' and date >='" + fromDate + "' and date <='" + currentDate + "'";
            System.out.println("query.." + selectquery);
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public int updateOfferkindrefidBasedOnBillingId(final String status, final String billingId) {
        int i = 0;
        try {
            String selectquery = "";
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            selectquery = "UPDATE customerpurchases set offerkind_refid='" + status + "' where billingId='" + billingId + "'";
           System.out.println("customerpurchaseslisting updateOfferkindrefidBasedOnBillingId query."+selectquery);
            i = this.s.executeUpdate(selectquery);
            this.s.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            System.out.println("Exception is ;" + e);
        }
        return i;
    }
    
    public List getCpDetailsBsdOnProd(final String ProductId, final String fromdate, final String todate) {
        final ArrayList list = new ArrayList();
        try {
            String date = "";
            if (fromdate != null && todate != null) {
                date = " and date>='" + fromdate + "' and date<='" + todate + "'";
            }
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type != 'online pending' and productId='" + ProductId + "'" + date + " order by date";
            final ResultSet rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
            this.s.close();
            rs.close();
            this.c.close();
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
        }
        return list;
    }
    
    public List getcustomerpurchasesListBasedOnHOAandDatesAndCahsTypeyJVN(final String voucherNo) throws SQLException {
        ResultSet rs = null;
        final ArrayList list = new ArrayList();
        try {
            this.c = ConnectionHelper.getConnection();
            this.s = this.c.createStatement();
            final String selectquery = "SELECT purchaseId,date,productId,quantity,rate,totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20 FROM customerpurchases where cash_type!='offerKind' and cash_type!='online pending'  and extra21='" + voucherNo + "'" + " order by date";
            rs = this.s.executeQuery(selectquery);
            while (rs.next()) {
                list.add(new customerpurchases(rs.getString("purchaseId"), rs.getString("date"), rs.getString("productId"), rs.getString("quantity"), rs.getString("rate"), rs.getString("totalAmmount"), rs.getString("billingId"), rs.getString("vocharNumber"), rs.getString("cash_type"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("customername"), rs.getString("phoneno"), rs.getString("emp_id"), rs.getString("gothram"), rs.getString("image"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("offerkind_refid"), rs.getString("tendered_cash"), rs.getString("cheque_no"), rs.getString("name_on_cheque"), rs.getString("cheque_narration"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13"), rs.getString("extra14"), rs.getString("extra15"), rs.getString("othercash_deposit_status"), rs.getString("othercash_totalamount"), rs.getString("extra16"), rs.getString("extra17"), rs.getString("extra18"), rs.getString("extra19"), rs.getString("extra20")));
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            System.out.println("Exception is ;" + e);
            return list;
        }
        finally {
            this.s.close();
            rs.close();
            this.c.close();
        }
        this.s.close();
        rs.close();
        this.c.close();
        return list;
    }

    public java.util.List getTotalOnlineSaleReport(String hoid, String fromdate, String todate)
    {
      ArrayList list = new ArrayList();
      try
      {
        String date = "";
        String type = "";
        if ((fromdate != null) && (todate != null)) {
          date = 
            " and date>='" + fromdate + "' and date<='" + todate + "'";
        }
       
        

        c = ConnectionHelper.getConnection();
        s = c.createStatement();
        String selectquery = "SELECT purchaseId,date,productId,sum(quantity) as quantity,rate,sum(totalAmmount) as totalAmmount,billingId,vocharNumber,cash_type,extra1,extra2,extra3,extra4,extra5,customername,phoneno,emp_id,gothram,image,extra6,extra7,extra8,extra9,extra10,offerkind_refid,tendered_cash,cheque_no,name_on_cheque,cheque_narration,extra11,extra12,extra13,extra14,extra15,othercash_deposit_status,othercash_totalamount,extra16,extra17,extra18,extra19,extra20,extra29,extra30 FROM customerpurchases where extra1='" + 
          hoid + "'" + date + "group by billingId order by date";
      //  System.out.println("89088080...." + selectquery);
        ResultSet rs = s.executeQuery(selectquery);
        while (rs.next()) {
          list.add(new customerpurchases(rs.getString("purchaseId"), rs
            .getString("date"), rs.getString("productId"), rs
            .getString("quantity"), rs.getString("rate"), rs
            .getString("totalAmmount"), rs.getString("billingId"), 
            rs.getString("vocharNumber"), 
            rs.getString("cash_type"), rs.getString("extra1"), rs
            .getString("extra2"), rs.getString("extra3"), 
            rs.getString("extra4"), rs.getString("extra5"), rs
            .getString("customername"), rs
            .getString("phoneno"), rs.getString("emp_id"), 
            rs.getString("gothram"), rs.getString("image"), rs
            .getString("extra6"), rs.getString("extra7"), 
            rs.getString("extra8"), rs.getString("extra9"), rs
            .getString("extra10"), rs
            .getString("offerkind_refid"), rs
            .getString("tendered_cash"), rs
            .getString("cheque_no"), rs
            .getString("name_on_cheque"), rs
            .getString("cheque_narration"), rs
            .getString("extra11"), rs.getString("extra12"), 
            rs.getString("extra13"), rs.getString("extra14"), rs
            .getString("extra15"), rs
            .getString("othercash_deposit_status"), rs
            .getString("othercash_totalamount"), rs
            .getString("extra16"), rs.getString("extra17"), 
            rs.getString("extra18"), rs.getString("extra19"), rs
            .getString("extra20"),rs.getString("extra29"), rs
            .getString("extra30")));
        }
        
        s.close();
        rs.close();
        c.close();
      } catch (Exception e) {
        seterror(e.toString());
        System.out.println("Exception is ;" + e);
      }
      return list;
    }




}


