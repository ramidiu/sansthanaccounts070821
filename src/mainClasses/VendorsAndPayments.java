package mainClasses;

import beans.bankbalance;
import beans.godwanstock;
import beans.payments;
import beans.productexpenses;
import beans.vendors;
import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class VendorsAndPayments
{
  public VendorsAndPayments() {}
  
  Connection con = null;
  Statement statement = null;
  
  public List<vendors> getAllVendors() {
    List<vendors> vendorList = new ArrayList<vendors>();
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectquery = "SELECT * FROM vendors";
      
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next())
      {
        vendorList.add(new vendors(rs.getString("vendorId"), rs.getString("title"), rs.getString("firstName"), rs.getString("lastName"), rs.getString("gothram"), 
          rs.getString("nomineeName"), rs.getString("agencyName"), rs.getString("address"), rs.getString("city"), rs.getString("pincode"), 
          rs.getString("phone"), rs.getString("email"), rs.getString("description"), rs.getString("headAccountId"), rs.getString("bankName"), 
          rs.getString("bankBranch"), rs.getString("IFSCcode"), rs.getString("accountNo"), rs.getString("creditDays"), rs.getString("extra1"), 
          rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("extra6"), rs.getString("extra7"), 
          rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10")));
      }
      
      statement.close();
      rs.close();
      con.close();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return vendorList;
  }
  

  public vendors getVendorByVendorId(String vendorId)
  {
    vendors vendors = new vendors();
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectquery = "SELECT * FROM vendors where vendorID = '" + vendorId + "'";
      
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next())
      {
        vendors = new vendors(rs.getString("vendorId"), rs.getString("title"), rs.getString("firstName"), rs.getString("lastName"), rs.getString("gothram"), 
          rs.getString("nomineeName"), rs.getString("agencyName"), rs.getString("address"), rs.getString("city"), rs.getString("pincode"), 
          rs.getString("phone"), rs.getString("email"), rs.getString("description"), rs.getString("headAccountId"), rs.getString("bankName"), 
          rs.getString("bankBranch"), rs.getString("IFSCcode"), rs.getString("accountNo"), rs.getString("creditDays"), rs.getString("extra1"), 
          rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("extra6"), rs.getString("extra7"), 
          rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10"));
      }
      
      statement.close();
      rs.close();
      con.close();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return vendors;
  }
  

  public List<bankbalance> getOpeningBalanceForVendors(String vendorId, String finYear, String headOfAccount)
  {
    List<bankbalance> opeingBalance = new java.util.ArrayList();
    
    String subQuery = "";
    
    if ((vendorId != null) && (!vendorId.trim().equals(""))) {
      subQuery = " and extra6 = '" + vendorId + "'";
    }
    if ((headOfAccount != null) && (!headOfAccount.trim().equals(""))) {
      subQuery = subQuery + " and extra2 = '" + headOfAccount + "'";
    }
    

    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      

      String selectQuery = "select * from bankbalance where extra1 = '" + finYear + "' and extra6 != '' and extra2 != '' " + subQuery;
//      System.out.println("getOpeningBalanceForVendors =====> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        bankbalance bankbalance = new bankbalance();
        
        bankbalance.setUniq_id(rs.getString("uniq_id"));
        bankbalance.setBank_id(rs.getString("bank_id"));
        bankbalance.setType(rs.getString("type"));
        bankbalance.setBank_bal(rs.getString("bank_bal"));
        bankbalance.setBank_flag(rs.getString("bank_flag"));
        bankbalance.setCreatedDate(rs.getString("createdDate"));
        bankbalance.setCreatedBy(rs.getString("createdBy"));
        bankbalance.setExtra1(rs.getString("extra1"));
        bankbalance.setExtra2(rs.getString("extra2"));
        bankbalance.setExtra3(rs.getString("extra3"));
        bankbalance.setExtra4(rs.getString("extra4"));
        bankbalance.setExtra5(rs.getString("extra5"));
        bankbalance.setExtra6(rs.getString("extra6"));
        bankbalance.setExtra7(rs.getString("extra7"));
        bankbalance.setExtra8(rs.getString("extra8"));
        bankbalance.setExtra9(rs.getString("extra9"));
        bankbalance.setExtra10(rs.getString("extra10"));
        
        opeingBalance.add(bankbalance);
      }
      
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return opeingBalance;
  }
  
  public List<godwanstock> getMSEEntrysForAllVendors(String vendorId, String head_account_id, String fromDate, String toDate) {
	  System.out.println("This is from the getMSEEntrysForAllVendors");
	  List<godwanstock> godwanstocks = new java.util.ArrayList();
    
    String subQuery = "";
    
    if ((vendorId != null) && (!vendorId.trim().equals(""))) {
      subQuery = " and vendorId = '" + vendorId + "'";
    }
    if ((head_account_id != null) && (!head_account_id.trim().equals(""))) {
      subQuery = subQuery + " and department = '" + head_account_id + "'";
    }
    

    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      

      String selectQuery = "select * from godwanstock where date >= '" + fromDate + "' and date <= '" + toDate + "' " + subQuery + " and vendorId != '' and extra1 like '%MSE%' group by extra1 order by vendorId";
//      System.out.println("getMSEEntrysForAllVendors =====> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next()) {
        godwanstock godwanstock = new godwanstock();
        
        godwanstock.setgsId(rs.getString("gsId"));
        godwanstock.setvendorId(rs.getString("vendorId"));
        godwanstock.setproductId(rs.getString("productId"));
        godwanstock.setdescription(rs.getString("description"));
        godwanstock.setquantity(rs.getString("quantity"));
        godwanstock.setpurchaseRate(rs.getString("purchaseRate"));
        godwanstock.setdate(rs.getString("date"));
        godwanstock.setMRNo(rs.getString("MRNo"));
        godwanstock.setbillNo(rs.getString("billNo"));
        godwanstock.setextra1(rs.getString("extra1"));
        godwanstock.setextra2(rs.getString("extra2"));
        godwanstock.setextra3(rs.getString("extra3"));
        godwanstock.setextra4(rs.getString("extra4"));
        godwanstock.setextra5(rs.getString("extra5"));
        godwanstock.setvat(rs.getString("vat"));
        godwanstock.setvat1(rs.getString("vat1"));
        godwanstock.setprofcharges(rs.getString("profcharges"));
        godwanstock.setemp_id(rs.getString("emp_id"));
        godwanstock.setdepartment(rs.getString("department"));
        godwanstock.setCheckedby(rs.getString("checkedby"));
        godwanstock.setCheckedtime(rs.getString("checkedtime"));
        godwanstock.setApproval_status(rs.getString("approval_status"));
        godwanstock.setExtra6(rs.getString("extra6"));
        godwanstock.setExtra7(rs.getString("extra7"));
        godwanstock.setExtra8(rs.getString("extra8"));
        godwanstock.setExtra9(rs.getString("extra9"));
        godwanstock.setExtra10(rs.getString("extra10"));
        godwanstock.setPo_approved_status(rs.getString("po_approved_status"));
        godwanstock.setPo_approved_by(rs.getString("po_approved_by"));
        godwanstock.setPo_approved_date(rs.getString("po_approved_date"));
        godwanstock.setBill_status(rs.getString("bill_status"));
        godwanstock.setBill_update_by(rs.getString("bill_update_by"));
        godwanstock.setBill_update_time(rs.getString("bill_update_time"));
        godwanstock.setExtra11(rs.getString("extra11"));
        godwanstock.setExtra12(rs.getString("extra12"));
        godwanstock.setExtra13(rs.getString("extra13"));
        godwanstock.setExtra14(rs.getString("extra14"));
        godwanstock.setExtra15(rs.getString("extra15"));
        godwanstock.setMajorhead_id(rs.getString("majorhead_id"));
        godwanstock.setMinorhead_id(rs.getString("minorhead_id"));
        godwanstock.setPayment_status(rs.getString("payment_status"));
        godwanstock.setActualentry_date(rs.getString("actualentry_date"));
        godwanstock.setExtra16(rs.getString("extra16"));
        godwanstock.setExtra17(rs.getString("extra17"));
        godwanstock.setExtra18(rs.getString("extra18"));
        godwanstock.setExtra19(rs.getString("extra19"));
        godwanstock.setExtra20(rs.getString("extra20"));
        

        godwanstocks.add(godwanstock);
      }
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return godwanstocks;
  }
  
  public List<productexpenses> getVendorPaymentCompletedEntrys(String vendorId, String head_account_id, String fromDate, String toDate, String amountGetingType)
  {
	  System.out.println("This is from the getVendorPaymentCompletedEntrys");
    List<productexpenses> productexpenses = new java.util.ArrayList();
    

    String subQuery2 = "";
    



    if ((head_account_id != null) && (!head_account_id.trim().equals(""))) {
      subQuery2 = subQuery2 + " pe.head_account_id = '" + head_account_id + "' and ";
    }
    

    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();

      String selectQuery = "SELECT vendorId, sum(amount) as amount FROM payments where date between '" + fromDate + "' and '" + toDate + "' " + 
        "and vendorId = '" + vendorId + "' and extra3 in (select expinv_id from productexpenses pe where " + subQuery2 + " pe.vendorId = '" + vendorId + "') group by vendorId";
      
      System.out.println("getVendorPaymentCompletedEntrys =====> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        productexpenses productexpenses2 = new productexpenses();
        
        productexpenses2.setvendorId(rs.getString("vendorId"));
        productexpenses2.setamount(rs.getString("amount"));
        
        productexpenses.add(productexpenses2);
      }

      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return productexpenses;
  }

  public List<String[]> getVendorOpeningAndPendingAmount(String vendorId, String head_account_id, String finYear, String fromDate, String toDate, String amountGetingType)
  {
	  System.out.println("vendorId::::"+vendorId);
	  System.out.println("head_account_id::::"+head_account_id);
	  System.out.println("finYear:::::"+finYear);
	  System.out.println("fromDate:::::"+fromDate);
	  System.out.println("toDate::::"+toDate);
	  System.out.println("amountGetingType:::"+amountGetingType);
	  
	  
	 List<vendors> vendorList = getAllVendors();
	 
	 for(int i=0;i<vendorList.size();i++){
		 System.out.println("vendors are---->"+vendorList.get(i).getvendorId());
	 }
    
    List<godwanstock> godwanstockList = getMSEEntrysForAllVendors(vendorId, head_account_id, fromDate, toDate);
    
   
    

    List<String[]> finalListOfVendor = new java.util.ArrayList();
    
    for (int i = 0; i < vendorList.size(); i++)
    {
      vendors vendors = (vendors)vendorList.get(i);
      
      if ((vendors != null) && (!vendors.getvendorId().trim().equals("")))
      {
        String vendorIds = vendors.getvendorId();
        

        if ((vendorId != null) && (!vendorId.trim().equals(""))) {
          vendorIds = vendorId;
        }
        if ((!vendorIds.equals("1001")) && (!vendorIds.equals("1164")) && (!vendorIds.equals("1201")) && (!vendorIds.equals("1202")) && (!vendorIds.equals("1153")) && (!vendorIds.equals("1218")) && (!vendorIds.equals("1219")) && (!vendorIds.equals("1220")) && (!vendorIds.equals("1221")) && (!vendorIds.equals("1222")) && (!vendorIds.equals("1223")) && (!vendorIds.equals("1224")) && (!vendorIds.equals("1225")) && (!vendorIds.equals("1226")) && (!vendorIds.equals("1227")) && (!vendorIds.equals("1165"))) {
          String venderName = vendors.getagencyName();
          
          double openingBal = 0.0D;
          double totalEntrysAmount = 0.0D;
          double paidAmount = 0.0D;
          double pendingAmount = 0.0D;
          
          if ((godwanstockList != null) && (godwanstockList.size() > 0)) {
            for (int j = 0; j < godwanstockList.size(); j++) {
              godwanstock godwanstock = (godwanstock)godwanstockList.get(j);
              if ((godwanstock != null) && (godwanstock.getvendorId() != null) && (vendorIds.trim().equals(godwanstock.getvendorId().trim()))) {
                totalEntrysAmount += Double.parseDouble(godwanstock.getExtra16());
              }
            }
          }
          
          List<productexpenses> paymentsList = getVendorPaymentCompletedEntrys(vendorIds, head_account_id, fromDate, toDate, amountGetingType);
          if ((paymentsList != null) && (paymentsList.size() > 0)) {
            for (int j = 0; j < paymentsList.size(); j++) {
              productexpenses productexpenses = (productexpenses)paymentsList.get(j);
              if ((productexpenses != null) && (productexpenses.getvendorId() != null) && (productexpenses.getvendorId().trim().equals(vendorIds.trim()))) {
                paidAmount += Double.parseDouble(productexpenses.getamount());
              }
            }
          }
          

          pendingAmount = openingBal + totalEntrysAmount - paidAmount;
          



          if ((pendingAmount > 5.0D) || (pendingAmount < -5.0D)) {
            String[] vendorAmountList = new String[6];
            
            vendorAmountList[0] = vendorIds;
            vendorAmountList[1] = venderName;
            vendorAmountList[2] = ""+openingBal;
            vendorAmountList[3] = ""+totalEntrysAmount;
            vendorAmountList[4] = ""+paidAmount;
            vendorAmountList[5] = ""+pendingAmount;
            
            finalListOfVendor.add(vendorAmountList);
          }
        }
      }

      
      if ((vendorId != null) && (!vendorId.trim().equals(""))) {
        break;
      }
    }
    
    
    if ((finalListOfVendor != null) && (finalListOfVendor.size() > 0)) {
      for (int i = 0; i < finalListOfVendor.size(); i++) {
        String[] result = (String[])finalListOfVendor.get(i);
        System.out.println(result[0] + "  " + result[1] + "  " + result[2] + "  " + result[3] + "  " + result[4] + " " + result[5]);
      }
    }
    

    return finalListOfVendor;
  }
  


  public List<payments> getVendorPaymentsByVendorID(String vendorId, String head_account_id, String fromDate, String toDate)
  {
    List<payments> paymentsList = new java.util.ArrayList();
    
    String subQuery = "";
    
    if ((head_account_id != null) && (!head_account_id.trim().equals(""))) {
      subQuery = subQuery + " pe.head_account_id = '" + head_account_id + "' and ";
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT date(p.date) as date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra2, p.extra3, p.extra4 FROM payments p where p.date between '" + 
        fromDate + "' and '" + toDate + "' " + 
        "and p.vendorId = '" + vendorId + "' and p.extra3 in (select pe.expinv_id from productexpenses pe where " + subQuery + " pe.vendorId = '" + vendorId + "')";
      
//      System.out.println("getVendorPaymentsByVendorID =====> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next()) {
        payments payments = new payments();
        
        payments.setdate(rs.getString("date"));
        payments.setbankId(rs.getString("bankId"));
        payments.setnarration(rs.getString("narration"));
        payments.setvendorId(rs.getString("vendorId"));
        payments.setreference(rs.getString("reference"));
        payments.setchequeNo(rs.getString("chequeNo"));
        payments.setamount(rs.getString("amount"));
        payments.setpaymentType(rs.getString("paymentType"));
        payments.setextra2(rs.getString("extra2"));
        payments.setextra3(rs.getString("extra3"));
        payments.setextra4(rs.getString("extra4"));
        
        paymentsList.add(payments);
      }
      
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return paymentsList;
  }
}