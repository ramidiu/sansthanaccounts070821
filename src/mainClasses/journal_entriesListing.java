package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.journal_entries;
public class journal_entriesListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String jv_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setjv_id(String jv_id)
{
this.jv_id = jv_id;
}
public String getjv_id(){
return(this.jv_id);
}
public List getjournal_entries()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT jv_id,created_date,sub_head_id,narration,debit_amount,credit_amount,jv_inv_no,voucher_ref_no,remarks,emp_id,status,head_account_id,extra1,extra2,extra3,extra4,extra5 FROM journal_entries "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new journal_entries(rs.getString("jv_id"),rs.getString("created_date"),rs.getString("sub_head_id"),rs.getString("narration"),rs.getString("debit_amount"),rs.getString("credit_amount"),rs.getString("jv_inv_no"),rs.getString("voucher_ref_no"),rs.getString("remarks"),rs.getString("emp_id"),rs.getString("status"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMjournal_entries(String jv_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT jv_id,created_date,sub_head_id,narration,debit_amount,credit_amount,jv_inv_no,voucher_ref_no,remarks,emp_id,status,head_account_id,extra1,extra2,extra3,extra4,extra5 FROM journal_entries where  jv_id = '"+jv_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new journal_entries(rs.getString("jv_id"),rs.getString("created_date"),rs.getString("sub_head_id"),rs.getString("narration"),rs.getString("debit_amount"),rs.getString("credit_amount"),rs.getString("jv_inv_no"),rs.getString("voucher_ref_no"),rs.getString("remarks"),rs.getString("emp_id"),rs.getString("status"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}