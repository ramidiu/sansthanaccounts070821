package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.banktransactions;
import beans.banktransactionsService2;
public class banktransactionsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String banktrn_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setbanktrn_id(String banktrn_id)
{
this.banktrn_id = banktrn_id;
}
public String getbanktrn_id(){
return(this.banktrn_id);
}
public List getbanktransactions()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getbanktransactionsBasedOnFromDateToDate(String fromDate,String toDate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromDate+"%' and date<='"+toDate+"%'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMbanktransactions(String banktrn_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  banktrn_id = '"+banktrn_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMbanktransactionsId(String bank_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  bank_id = '"+bank_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMbanktransactions1(String banktrn_id)
{
//ArrayList list = new ArrayList();
	String bankId="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  banktrn_id = '"+banktrn_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 //list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));
	bankId=rs.getString("bank_id");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return bankId;
}
public String getEmployeeDepositAmount(String emp_id,String date)
{
String totDeposit="";
try {
 // Load the database driver
	String emPCon="";
	if(emp_id!=null && !emp_id.equals("")){
		emPCon="and  extra2 = '"+emp_id+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where extra4='poojastore-counter'  and date like'%"+date+"%'"+emPCon; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}
public String getTotDepositAmount(String emp_id,String date,String counter)
{
String totDeposit="";
try {
 // Load the database driver
	String emPCon="";
	if(emp_id!=null && !emp_id.equals("")){
		emPCon="and  extra2 = '"+emp_id+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where extra4='"+counter+"'  and date like'%"+date+"%'"+emPCon; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}
public String getEmployeePrevDatesDepositAmount(String emp_id,String date)
{
String totDeposit="";
try {
 // Load the database driver
	String emPCon="";
	if(emp_id!=null && !emp_id.equals("")){
		emPCon="and  extra2 = '"+emp_id+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where extra4='poojastore-counter'  and date<'"+date+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}
public boolean getValidPayment(String paymentid)
{
boolean totDeposit=true;
try {

c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM banktransactions where extra3 like '%"+paymentid+"%'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	totDeposit=false;
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}

public List getOtherDepositsBankTransaction(String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where extra8 = 'other-amount' and date>='"+fromdate+"' and date<='"+todate+"' order by date" ;
//System.out.println("bnktrn===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getBankTransactionBetweenDates(String bank_id,String fromdate,String todate,String reportType)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String report="";
	if(reportType!=null && !reportType.equals("")){
		if(reportType.equals("cashpaid")){
			report=" and (type='cashpaid' || type='pettycash')";
		}else{
		//report=" and type!='cashpaid'";
			report=" and type!='cashpaid' and type != 'pettycash'";	
			}
	}else{
		//report=" and type!='cashpaid'";
		report=" and type!='cashpaid'  and type != 'pettycash'"; //the above lines are kept commented by srinivas on 28/05/2016
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where bank_id = '"+bank_id+"' and date>='"+fromdate+"' and date<='"+todate+"'"+report+" order by date" ;
//System.out.println("bnktrn===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getBankTransactionByCategory(String fromdate,String todate,String depositedCategory)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	/*String report="";
	if(reportType!=null && !reportType.equals("")){
		if(reportType.equals("cashpaid")){
			report=" and (type='cashpaid' || type='pettycash')";
		}else{
		//report=" and type!='cashpaid'";
			report=" and type!='cashpaid' and type != 'pettycash'";	
			}
	}else{
		//report=" and type!='cashpaid'";
		report=" and type!='cashpaid'  and type != 'pettycash'"; //the above lines are kept commented by srinivas on 28/05/2016
	}*/
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  date>='"+fromdate+"' and date<='"+todate+"' and extra4='"+depositedCategory+"' order by date" ;
//System.out.println("bnktrn===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getBankTransactionBetweenDates(String bank_id,String fromdate,String todate,String reportType,boolean BRStype,String type)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String report="";
	String brsinerquery="";
	String typetx="";
	String fromdatequery="";
	if(reportType!=null && !reportType.equals("")){
		if(reportType.equals("cashpaid")){
			report=" and (type='cashpaid' || type='pettycash')";
		}else{
		report=" and type!='cashpaid'";}
	}else{
		report=" and type!='cashpaid'";
	}
	if(BRStype){
		brsinerquery=" and brs_date='' ";
	}
	if(!fromdate.equals("")){
		fromdatequery=" and date>='"+fromdate+"' ";
	}
	if(type.equals("add")){
		typetx=" and type='deposit' ";
	} else if(type.equals("sub")){
		typetx=" and type!='deposit' ";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where bank_id = '"+bank_id+"' "+fromdatequery+" and  date<='"+todate+"'"+report+brsinerquery+typetx+" order by date" ;
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where bank_id = '"+bank_id+"' "+fromdatequery+" and  date<='"+todate+"'"+report+brsinerquery+typetx+" and date NOT LIKE '2015-04-01%' order by date" ;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getBankTransactionBetweenDatesNew(String bank_id,String fromdate,String todate,String reportType,boolean BRStype,String type)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String report="";
	String brsinerquery="";
	String typetx="";
	String fromdatequery="";
	if(reportType!=null && !reportType.equals("")){
		if(reportType.equals("cashpaid")){
			report=" and (type='cashpaid' || type='pettycash')";
		}else{
		report=" and type!='cashpaid'";}
	}else{
		report=" and type!='cashpaid'";
	}
	if(BRStype){
		brsinerquery=" and (brs_date = '' || (brs_date not between '"+fromdate.substring(0, 10)+"' and '"+todate.substring(0, 10)+"'))";
	}
	if(!fromdate.equals("")){
		fromdatequery=" and date >='"+fromdate+"' ";
	}
	if(type.equals("add")){
		typetx=" and type='deposit' ";
	} else if(type.equals("sub")){
		typetx=" and type!='deposit' ";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where bank_id = '"+bank_id+"' "+fromdatequery+" and  date<='"+todate+"'"+report+brsinerquery+typetx+" order by date" ;
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where bank_id = '"+bank_id+"' "+fromdatequery+" and  date<='"+todate+"'"+report+brsinerquery+typetx+" and date NOT LIKE '2015-04-01%' order by date" ;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getBankTransactionBetweenDates(String fromdate,String todate,String type)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String report="";
	String brsinerquery="";
	String typetx="";
	String fromdatequery="";

	if(!fromdate.equals("")){
		fromdatequery=" date>='"+fromdate+"' ";
	}
	
	if(type.equals("add")){
		typetx=" and type='deposit' ";
	} else if(type.equals("sub")){
		typetx=" and type!='deposit' ";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where bank_id = '"+bank_id+"' "+fromdatequery+" and  date<='"+todate+"'"+report+brsinerquery+typetx+" order by date" ;
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where "+fromdatequery+" and  date<='"+todate+"' and type!='cashpaid' and brs_date=''"+typetx+" order by date" ;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactionsService2(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
/*s.close();
rs.close();*/
/*c.close();*/
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String getOpeningBalance(String bank_id,String fromdate,String amountType)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		if(amountType.equals("cashpaid")){
			amtTypeCon=" and type='pettycash'";
		}else{
			amtTypeCon=" and type='deposit'";
		}
	}else{
		amtTypeCon=" and type='deposit'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and date<'"+fromdate+"'"+amtTypeCon; 
//System.out.println("opening2222==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(!rs.getString("amount").equals("NULL") && !rs.getString("amount").trim().equals("")){
	openingBal=rs.getString("amount");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

public String getOpeningBalanceBetweenDates(String bank_id,String fromdate,String todate,String amountType)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		if(amountType.equals("cashpaid")){
			amtTypeCon=" and type='pettycash'";
		}else{
			amtTypeCon=" and type='deposit'";
		}
	}else{
		amtTypeCon=" and type='deposit'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and date >= '"+fromdate+"' and date <= '"+todate+"'"+amtTypeCon; 
//System.out.println("opening2222==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(!rs.getString("amount").equals("NULL") && !rs.getString("amount").trim().equals("")){
	openingBal=rs.getString("amount");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}


public String getBalanceByDeposit(String bank_id,String fromdate,String amountType)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		amtTypeCon=" and type='"+amountType+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and date<='"+fromdate+"'"+amtTypeCon; 
//System.out.println("openingnew==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(!rs.getString("amount").equals("NULL") && !rs.getString("amount").trim().equals("")){
	openingBal=rs.getString("amount");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

public String getBalanceByDepositBetweenDates(String bank_id,String fromdate,String todate,String amountType)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		amtTypeCon=" and type='"+amountType+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and date >='"+fromdate+"' and date<='"+todate+"'"+amtTypeCon; 
//System.out.println("openingnew==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(!rs.getString("amount").equals("NULL") && !rs.getString("amount").trim().equals("")){
	openingBal=rs.getString("amount");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

public String getBalanceByNOTDeposit(String bank_id,String fromdate,String amountType,String amountType1,String amountType2,String amountType3)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		amtTypeCon=" and (type='"+amountType+"' OR type='"+amountType1+"' OR type='"+amountType2+"' OR type='"+amountType3+"')";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and date<='"+fromdate+"'"+amtTypeCon; 
//System.out.println("openingnew111==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(!rs.getString("amount").equals("NULL") && !rs.getString("amount").trim().equals("")){
	openingBal=rs.getString("amount");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

public String getBalanceByNOTDepositBetweenDates(String bank_id,String fromdate,String todate,String amountType,String amountType1,String amountType2,String amountType3)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		amtTypeCon=" and (type='"+amountType+"' OR type='"+amountType1+"' OR type='"+amountType2+"' OR type='"+amountType3+"')";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and date >= '"+fromdate+"' and date <= '"+todate+"'"+amtTypeCon; 
//System.out.println("openingnew111==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(!rs.getString("amount").equals("NULL") && !rs.getString("amount").trim().equals("")){
	openingBal=rs.getString("amount");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

public String getOpeningBalance(String bank_id,String fromdate,String todate,String amountType)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		if(amountType.equals("cashpaid")){
			amtTypeCon=" and type='pettycash'";
		}else{
			amtTypeCon=" and type='deposit'";
		}
	}else{
		amtTypeCon=" and type='deposit'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and ( date between '"+fromdate+"' and '"+todate+"' ) "+amtTypeCon;
//System.out.println("pettyCash >>>>" +selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("amount") !=null && !rs.getString("amount").equals("NULL")){
	openingBal=rs.getString("amount");	
//	System.out.println("openingBal >>>>> "+openingBal);
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}


public String getOpeningBalance1(String bank_id,String fromdate,String todate,String amountType,String extraField,String... ids)
{
String openingBal="00";
String query3="";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		if(amountType.equals("cashpaid")){
			amtTypeCon=" and type='pettycash'";
		}else{
			amtTypeCon=" and type='deposit'";
		}
	}else{
		amtTypeCon=" and type='deposit'";
	}
	if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
		for(String id:ids){
			query3+="and "+extraField+"!='"+id+"'";
		}//for loop ends
	}//if ends
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and ( date between '"+fromdate+"' and '"+todate+"' ) "+amtTypeCon+query3;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(!rs.getString("amount").equals("NULL")){
	openingBal=rs.getString("amount");	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

public String getOpeningBalAmountPaidToVendors(String bank_id,String fromdate,String amountType)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		if(amountType.equals("cashpaid")){
			amtTypeCon=" and (type='cashpaid')";
		}else{
			//amtTypeCon=" and (type='cheque' or type='transfer')";
			amtTypeCon=" and (type='cheque' or type='transfer' or type='withdraw')";
		}
	}else{
		//amtTypeCon=" and (type='cheque' or type='transfer' )";
		amtTypeCon=" and (type='cheque' or type='transfer' or type='withdraw')";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and date<'"+fromdate+"'"+amtTypeCon;
//System.out.println("opening===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(!rs.getString("amount").equals("NULL") && !rs.getString("amount").trim().equals("")){
	openingBal=rs.getString("amount");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

public String getOpeningBalAmountPaidToVendors(String bank_id,String fromdate,String todate,String amountType)
{
String openingBal="00";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		if(amountType.equals("cashpaid")){
			//amtTypeCon=" and type='cashpaid'";
			amtTypeCon=" and (type='cashpaid')";
		}else{
			//amtTypeCon=" and type='cheque' || type='transfer'";
			//amtTypeCon=" and (type='cheque' or type='transfer')";
			amtTypeCon=" and (type='cheque' or type='transfer' or type='withdraw')";
		}
	}else{
		//amtTypeCon=" and type='cheque' || type='transfer'";
		//amtTypeCon=" and (type='cheque' or type='transfer')";
		amtTypeCon=" and (type='cheque' or type='transfer' or type='withdraw')";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"'and ( date between '"+fromdate+"' and '"+todate+"' ) "+amtTypeCon;
//System.out.println("getOpeningBalAmountPaidToVendors ======> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("amount") != null && !rs.getString("amount").equals("NULL")){
	openingBal=rs.getString("amount");
//	System.out.println("openingBal1 >>>>>> "+openingBal);
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

//This method is created for Receipts & Payments to hide some heads instead of getOpeningBalAmountPaidToVendors(-) method
public String getOpeningBalAmountPaidToVendors1(String bank_id,String fromdate,String todate,String amountType,String extraField,String... ids)
{
String openingBal="00";
String query3="";
try {
 // Load the database driver
	String amtTypeCon="";
	if(amountType!=null && !amountType.equals("")){
		if(amountType.equals("cashpaid")){
			//amtTypeCon=" and type='cashpaid'";
			amtTypeCon=" and (type='cashpaid')";
		}//Inner if ends
		else{
			//amtTypeCon=" and type='cheque' || type='transfer'";
			//amtTypeCon=" and (type='cheque' or type='transfer')";
			amtTypeCon=" and (type='cheque' or type='transfer' or type='withdraw')";
		}//inner else ends
	}//if ends
	else{
		//amtTypeCon=" and type='cheque' || type='transfer'";
		//amtTypeCon=" and (type='cheque' or type='transfer')";
		amtTypeCon=" and (type='cheque' or type='transfer' or type='withdraw')";
	}//else ends
	if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
		for(String id:ids){
			query3+="and "+extraField+"!='"+id+"'";
		}//for loop ends
	}//if ends
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"'and ( date between '"+fromdate+"' and '"+todate+"' ) "+amtTypeCon+query3;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("amount") != null && !rs.getString("amount").equals("NULL")){
	openingBal=rs.getString("amount");
	}//if ends
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return openingBal;
}//method ends

public String getCreditBanksTotalAmt(String bank_id,String fromdate,String todate,String flag1,String flag2)
{
String openingBal="00";
try {
 // Load the database driver
	String query1="";
	if(flag1!=null && !flag1.equals("")){
		query1=" and (type='"+flag1+"')";
		
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"'and ( date between '"+fromdate+"' and '"+todate+"' ) "+query1;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("amount") != null && !rs.getString("amount").equals("NULL")){
	openingBal=rs.getString("amount");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return openingBal;
}

public String getCreditOpeningBankBalance(String bank_id,String fromdate,String amountType)
{
String creditBal="00";
try {
 // Load the database driver
	
	String creditamtTypeCon="";
 	if(amountType!=null && !amountType.equals("")){
	if(amountType.equals("cashpaid")){
					creditamtTypeCon=" and type='pettycash' ";
		}else{
					creditamtTypeCon=" and ( type='deposit' or type='pettycash' or  type='cashpaid' or type='cheque' ) ";
		}
	}else{
				creditamtTypeCon=" and ( type='deposit' or type='pettycash' or  type='cashpaid' or type='cheque' ) ";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String creditselectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and brs_date!='' and date<='"+fromdate+"'"+creditamtTypeCon;
ResultSet creditrs = s.executeQuery(creditselectquery);
while(creditrs.next()){
	if(creditrs.getString("amount")!=null){
		creditBal=creditrs.getString("amount");
	}
}
s.close();
creditrs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return creditBal;
}
public String getDebitOpeningBankBalance(String bank_id,String fromdate,String amountType)
{
String debitBal="00";
try {
 // Load the database driver
	String debitamtTypeCon="";
 	if(amountType!=null && !amountType.equals("")){
	if(amountType.equals("cashpaid")){
			debitamtTypeCon=" and type='cashpaid' ";
		
		}else{
			debitamtTypeCon=" and type='deposit' ";
				}
	}else{
		debitamtTypeCon=" and type='deposit' ";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String debitselectquery="SELECT sum(amount) as amount FROM banktransactions where  bank_id = '"+bank_id+"' and brs_date!='' and date<='"+fromdate+"'"+debitamtTypeCon; 
ResultSet debitrs = s.executeQuery(debitselectquery);
while(debitrs.next()){
	if(debitrs.getString("amount")!=null){
		debitBal=debitrs.getString("amount");
	}
}
s.close();
debitrs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return debitBal;
}
public List getPettyCashTransactions(String bank_id,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where type='cashpaid' and bank_id = '"+bank_id+"' and date>='"+fromdate+"' and date<='"+todate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public double getPettyCashTransactions2(String bank_id,String date)
{
double amount = 0.0;	
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where type='cashpaid' and bank_id = '"+bank_id+"' and date like '"+date+"%'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	amount = Double.parseDouble(rs.getString("amount"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return amount;
}

public List getDepositsList(String emp_id,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where type='deposit' and extra2 = '"+emp_id+"' and date>='"+fromdate+"' and date<='"+todate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getStringPaymentType(String expinvId)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra3 = '"+expinvId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list=rs.getString("type");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getStringOtherDeposit(String extra8,String Major_head,String Minor_head, String Sub_Head, String head_account_id,String fromdate,String todate)
{
	
	ArrayList list = new ArrayList();
	String variable=" ";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String condition = "";
if(Minor_head != ""){
	condition = "and minor_head_id='"+Minor_head+"' ";
}
String condition1 = "";
if(Sub_Head != ""){
	condition1 = "and sub_head_id='"+Sub_Head+"' ";
}
String condition2="and date>='"+fromdate+"' and date<='"+todate+"'";
if(todate!=null && todate.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	condition2="  and  date like '"+fromdate+"%' ";
}
String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' "+condition+""+condition1+"and head_account_id='"+head_account_id+"' "+condition2+"" ; 
//String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' and minor_head_id='"+Minor_head+"' and head_account_id='"+head_account_id+"' and date>='"+fromdate+"' and date<='"+todate+"'" ;
ResultSet rs = s.executeQuery(selectquery);
System.out.println("getStringOtherDeposit ====> "+selectquery);
while(rs.next()){
	list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));
	//variable=rs.getString("amount");
	//System.out.println("amounttt.."+variable);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

//This method is created for Income & Expenditure to hide some heads instead of getStringOtherDeposit(-) method
public List<banktransactions> getStringOtherDeposit1(String extra8,String Major_head,String Minor_head, String Sub_Head, String head_account_id,String fromdate,String todate,String extraField,String... ids)
{
	
	ArrayList<banktransactions> list = new ArrayList<banktransactions>();
	String variable=" ";
	String query3="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String condition = "";
if(Minor_head != ""){
	condition = "and minor_head_id='"+Minor_head+"' ";
}//if ends
String condition1 = "";
if(Sub_Head != ""){
	condition1 = "and sub_head_id='"+Sub_Head+"' ";
}//if ends
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+="and "+extraField+"!='"+id+"'";
	}//for loop ends
}//if ends
String condition2="and date>='"+fromdate+"' and date<='"+todate+"'";
if(todate!=null && todate.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	condition2="  and  date like '"+fromdate+"%' ";
}//if ends
String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' "+condition+""+condition1+"and head_account_id='"+head_account_id+"' "+condition2+"" +query3; 
//String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' and minor_head_id='"+Minor_head+"' and head_account_id='"+head_account_id+"' and date>='"+fromdate+"' and date<='"+todate+"'" ;
ResultSet rs = s.executeQuery(selectquery);
//System.out.println("new queruy.."+selectquery);
while(rs.next()){
	list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));
	//variable=rs.getString("amount");
	//System.out.println("amounttt.."+variable);
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return list;
}//method ends





public List<banktransactions> getStringOtherDeposit11(String extra8,String Major_head,String Minor_head, String Sub_Head, String head_account_id,String fromdate,String todate,String extraField,String... ids)
{
	
	ArrayList<banktransactions> list = new ArrayList<banktransactions>();
	String variable=" ";
	String query3="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String condition = "";
if(Minor_head != ""){
	condition = "and minor_head_id='"+Minor_head+"' ";
}//if ends
String condition1 = "";
if(Sub_Head != ""){
	condition1 = "and sub_head_id='"+Sub_Head+"' ";
}//if ends
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+="and "+extraField+"!='"+id+"'";
	}//for loop ends
}//if ends
String condition2="and date>='"+fromdate+"' and date<='"+todate+"'";
if(todate!=null && todate.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	condition2="  and  date like '"+fromdate+"%' ";
}//if ends
String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' "+condition+""+condition1+"and head_account_id='"+head_account_id+"' "+condition2+"" +query3+" group by sub_head_id"; 
//String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' and minor_head_id='"+Minor_head+"' and head_account_id='"+head_account_id+"' and date>='"+fromdate+"' and date<='"+todate+"'" ;
ResultSet rs = s.executeQuery(selectquery);
//System.out.println("new queruy.."+selectquery);
while(rs.next()){
	list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));
	//variable=rs.getString("amount");
	//System.out.println("amounttt.."+variable);
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return list;
}//method ends







/*public List getStringJVDeposit(String extra4,String Major_head,String Minor_head, String Sub_Head, String head_account_id,String fromdate,String todate)
{
	//System.out.println("values..");
	ArrayList list = new ArrayList();
	String variable=" ";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String condition = "";
if(Minor_head != ""){
	condition = "and minor_head_id='"+Minor_head+"' ";
}
String condition1 = "";
if(Sub_Head != ""){
	condition1 = "and sub_head_id='"+Sub_Head+"' ";
}
String condition2="and date>='"+fromdate+"' and date<='"+todate+"'";
if(todate!=null && todate.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	condition2="  and  date like '"+fromdate+"%' ";
}
String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra4 = '"+extra4+"' and major_head_id = '"+Major_head+"' "+condition+""+condition1+"and head_account_id='"+head_account_id+"' "+condition2+"" ; 
//String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' and minor_head_id='"+Minor_head+"' and head_account_id='"+head_account_id+"' and date>='"+fromdate+"' and date<='"+todate+"'" ;
ResultSet rs = s.executeQuery(selectquery);
//System.out.println("new queruy.."+selectquery);
while(rs.next()){
	list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));
	//variable=rs.getString("amount");
	//System.out.println("amounttt.."+variable);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}*/



public List getDepositsListBasedOnHOD(String amtDeposited,String hod,String minorhead,String subhead,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where head_account_id='"+hod+"' and minor_head_id='"+minorhead+"' and sub_head_id='"+subhead+"' and extra8 ='"+amtDeposited+"' and date>='"+fromdate+"' and date<='"+todate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getDepositsListBasedOnHead(String amtDeposited,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where type='deposit' and extra4 = '"+amtDeposited+"' and date>='"+fromdate+"' and date<='"+todate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new banktransactions(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("sub_head_id"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String[] getBankTransactionBasedOnHoid(String reportType,String reportType1,String fromdate,String todate)
{
	String list[]=new String[2];
	list[1]="0.0";
	try {
	 // Load the database driver
		String date="";
		String report="";
		if(fromdate!=null && todate!=null){
			date=" and date>='"+fromdate+"' and date<='"+todate+"'";
		}
		if(reportType!= null && reportType1!=null){
			report=" and (extra4 = '"+reportType+"' OR extra4 ='"+reportType1+"')";
		}
		String selectquery="";
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	if(reportType.equals("pro-counter")){
		list[0]="pro-counter";
		selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1013' || bank_id='BNK1016') order by date" ;
	//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"' and extra4='"+reportType+"' and type='deposit' group by DATE(date)" ;
	}else if(reportType.equals("Charity-cash")){
		list[0]="Charity-cash";
		selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and bank_id='BNK1012' order by date" ;
		//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"' and extra4='"+reportType+"' and type='deposit' group by DATE(date)" ;
	}
	//System.out.println("bnktrn===>"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		if(rs.getString("extra4")!=null && !rs.getString("extra4").equals("null")){
			list[0] = rs.getString("extra4");
		}
		if(rs.getString("amount")!=null && !rs.getString("amount").equals("null")){
			list[1] = rs.getString("amount");
		}
	}
	s.close();
	rs.close();
	c.close();
}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}

public String getTotalamountDepositedBeforeFromdate(String reportType,String reportType1,String date,String fromdate)
{
String totDeposit="";
String selectquery="";
try {
 // Load the database driver
	String report="";
	if(reportType!= null && reportType1!=null){
		report=" and (extra4 = '"+reportType+"' OR extra4 ='"+reportType1+"')";
		//report=" and extra4 = '"+reportType+"'";
	}
	//System.out.println("reporttpe==>"+report);
c = ConnectionHelper.getConnection();
s=c.createStatement();
if(reportType.equals("pro-counter")){
selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1013' || bank_id='BNK1016' || bank_id='BNK1019' || bank_id='BNK1027' || bank_id='BNK1028')"; //       (bank_id='BNK1013' || bank_id='BNK1016' || bank_id='BNK1019' || bank_id='BNK1027' || bank_id='BNK1028' || bank_id='BNK1012' || bank_id='BNK1015' )
}else if(reportType.equals("Charity-cash")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1012' || bank_id='BNK1029')";//  //(bank_id='BNK1012' || bank_id='BNK1019' || bank_id='BNK1029' || bank_id='BNK1013' )
}else if(reportType.equals("poojastore-counter")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1015' || bank_id='BNK1030') ";//  //(bank_id='BNK1015' || bank_id='BNK1030')
}else if(reportType.equals("Sainivas")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and ( bank_id='BNK1031' || bank_id='BNK1013' || bank_id='BNK1014')";//(bank_id='BNK1011' || bank_id='BNK1014' || bank_id='BNK1018')
}

System.out.println("getTotalamountDepositedBeforeFromdate ========> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}


public String getTotalamountDepositedBeforeFromdateCardOnline(String reportType,String reportType1,String date,String fromdate)
{
String totDeposit="";
String selectquery="";
try {
 // Load the database driver
	String report="";
	if(reportType!= null && reportType1!=null){
		report=" and (extra4 = '"+reportType+"' OR extra4 ='"+reportType1+"')";
		//report=" and extra4 = '"+reportType+"'";
	}
	//System.out.println("reporttpe==>"+report);
c = ConnectionHelper.getConnection();
s=c.createStatement();
 if(reportType.equals("Charity-cash")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' ) and (bank_id='BNK1019')";//  //(bank_id='BNK1012' || bank_id='BNK1019' || bank_id='BNK1029' || bank_id='BNK1013' )
}

//System.out.println("========>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}




public String getTotalamountDepositedBeforeFromdateNewBanks(String reportType,String reportType1,String date,String fromdate)
{
String totDeposit="";
String selectquery="";
try {
 // Load the database driver
	String report="";
	if(reportType!= null && reportType1!=null){
		report=" and (extra4 = '"+reportType+"' OR extra4 ='"+reportType1+"')";
		//report=" and extra4 = '"+reportType+"'";
	}
	//System.out.println("reporttpe==>"+report);
c = ConnectionHelper.getConnection();
s=c.createStatement();
if(reportType.equals("pro-counter")){
selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and  (bank_id='BNK1013' || bank_id='BNK1016' || bank_id='BNK1027' || bank_id='BNK1028' )"; //(bank_id='BNK1013' || bank_id='BNK1016'|| bank_id='BNK1027' || bank_id='BNK1028'  )"; //     
}else if(reportType.equals("Charity-cash")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1012' || bank_id='BNK1029') ";//  //(bank_id='BNK1012' || bank_id='BNK1019' || bank_id='BNK1029' || bank_id='BNK1013' )
}else if(reportType.equals("poojastore-counter")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1015' || bank_id='BNK1030') ";//  //(bank_id='BNK1015' || bank_id='BNK1030')
}else if(reportType.equals("Sainivas")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and ( bank_id='BNK1031' || bank_id='BNK1013' || bank_id = 'BNK1014')";//(bank_id='BNK1011' || bank_id='BNK1014' || bank_id='BNK1018')
}

//System.out.println("========>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}



//This method is created for Receipts & Payments to hide some heads instead of getTotalamountDepositedBeforeFromdate(-) method
public String getTotalamountDepositedBeforeFromdate1(String reportType,String reportType1,String date,String fromdate,String extraField,String... ids)
{
String totDeposit="";
String selectquery="";
String query3="";
try {
 // Load the database driver
	String report="";
	if(reportType!= null && reportType1!=null){
		report=" and (extra4 = '"+reportType+"' OR extra4 ='"+reportType1+"')";
		//report=" and extra4 = '"+reportType+"'";
	}//if ends
	if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
		for(String id:ids){
			query3+="and "+extraField+"!='"+id+"'";
		}//for loop ends
	}//if ends
	//System.out.println("reporttpe==>"+report);
c = ConnectionHelper.getConnection();
s=c.createStatement();
if(reportType.equals("pro-counter")){
selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1013' || bank_id='BNK1016')"+query3; 
}//if ends
else if(reportType.equals("Charity-cash")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and bank_id='BNK1012'"+query3;
}//else if ends
else if(reportType.equals("poojastore-counter")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and bank_id='BNK1015'"+query3;
}//else if ends
else if(reportType.equals("Sainivas")){
	selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1011' || bank_id='BNK1014' || bank_id='BNK1018')"+query3;
}//else if ends

//System.out.println("==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return totDeposit;
}//method ends

public String getTotalOnlineDepositedBeforeFromdate(String reportType,String date,String fromdate)
{
String totDeposit="";
String selectquery="";
try {
 // Load the database driver
	String report="";
	if(reportType!= null){
		report=" and (extra4 = '"+reportType+"')";
	}
	//System.out.println("reporttpe==>"+report);
c = ConnectionHelper.getConnection();
s=c.createStatement();
selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and bank_id='BNK1019'"; 
System.out.println("getTotalOnlineDepositedBeforeFromdate ====> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}

//This method is created for Receipts & Payments to hide some heads instead of getTotalOnlineDepositedBeforeFromdate(-) method
public String getTotalOnlineDepositedBeforeFromdate1(String reportType,String date,String fromdate,String extraField,String... ids)
{
String totDeposit="";
String selectquery="";
String query3="";
try {
 // Load the database driver
	String report="";
	if(reportType!= null){
		report=" and (extra4 = '"+reportType+"')";
	}//if ends
	if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
		for(String id:ids){
			query3+="and "+extraField+"!='"+id+"'";
		}//for loop ends
	}//if ends
	//System.out.println("reporttpe==>"+report);
c = ConnectionHelper.getConnection();
s=c.createStatement();
selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<'"+fromdate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and bank_id='BNK1019'"+query3; 
//System.out.println("==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}

public String getTotalJVDepositedBeforeFromdate(String reportType,String date,String fromdate)
{
String totDeposit="";
String selectquery="";
try {
 // Load the database driver
	String report="";
	if(reportType!= null){
		report=" and (extra4 = '"+reportType+"')";
	}
	//System.out.println("reporttpe==>"+report);
	System.out.println("Date..."+date);
c = ConnectionHelper.getConnection();
s=c.createStatement();
selectquery="SELECT sum(amount) as amount FROM banktransactions where date>='"+date+"' and date<='"+fromdate+"'"+report+" and type='withdraw' and (extra8!='dollar-amount' and extra8!='other-amount') and bank_id='BNK1014'"; 
//System.out.println("==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totDeposit=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totDeposit;
}

public String[] getBankTransactionBasedOnHoidNew(String reportType,String reportType1,String fromdate,String todate)
{
	String list[]=new String[2];
	list[1]="0.0";
	try {
	 // Load the database driver
		String date="";
		String report="";
		if(fromdate!=null && todate!=null){
			date=" and date>='"+fromdate+"' and date<='"+todate+"'";
		}
		if(reportType!= null && reportType1!=null){
			report=" and (extra4 = '"+reportType+"' OR extra4 ='"+reportType1+"')";
		}
		String selectquery="";
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	if(reportType.equals("pro-counter")){
		list[0]="pro-counter";
		selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1013' || bank_id='BNK1016' || bank_id='BNK1019' || bank_id='BNK1027' || bank_id='BNK1028' || bank_id='BNK1012' || bank_id='BNK1015' ) order by date" ;
	//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"' and extra4='"+reportType+"' and type='deposit' group by DATE(date)" ;
	}else if(reportType.equals("Charity-cash")){
		list[0]="Charity-cash";
		selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1012' || bank_id='BNK1019' || bank_id='BNK1029' || bank_id='BNK1013' ) order by date" ;
		//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1012' || bank_id='BNK1019' || bank_id='BNK1029') order by date" ;//commented by sagar
		//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"' and extra4='"+reportType+"' and type='deposit' group by DATE(date)" ;
	}
	else if(reportType.equals("poojastore-counter")){
		list[0]="poojastore-counter";
		selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1015' || bank_id='BNK1030') order by date" ;
		//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"' and extra4='"+reportType+"' and type='deposit' group by DATE(date)" ;
	}
	else if(reportType.equals("Sainivas")){
		list[0]="Sainivas";
		selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and ( bank_id='BNK1031' || bank_id='BNK1013' || bank_id='BNK1014') order by date" ;
		//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"' and extra4='"+reportType+"' and type='deposit' group by DATE(date)" ;
	}
	System.out.println("getBankTransactionBasedOnHoidNew  ===>  "+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		if(rs.getString("extra4")!=null && !rs.getString("extra4").equals("null")){
			list[0] = rs.getString("extra4");
		}
		if(rs.getString("amount")!=null && !rs.getString("amount").equals("null")){
			list[1] = rs.getString("amount");
		}
	}
	s.close();
	rs.close();
	c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String[] getBankTransactionBasedOnOnlineCard(String reportType,String reportType1,String fromdate,String todate)
{
	String list[]=new String[2];
	list[1]="0.0";
	try {
	 // Load the database driver
		String date="";
		String report="";
		if(fromdate!=null && todate!=null){
			date=" and date>='"+fromdate+"' and date<='"+todate+"'";
		}
		if(reportType!= null && reportType1!=null){
			report=" and (extra4 = '"+reportType+"' OR extra4 ='"+reportType1+"')";
		}
		String selectquery="";
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	 if(reportType.equals("Charity-cash")){
		list[0]="Charity-cash";
		
		//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1012' || bank_id='BNK1019' || bank_id='BNK1029') order by date" ;//commented by sagar
		//selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"' and extra4='"+reportType+"' and type='deposit' group by DATE(date)" ;
	}
	 
	 selectquery="SELECT banktrn_id,bank_id,name,narration,date,sum(amount) as amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where date>='"+fromdate+"' and date<='"+todate+"'"+report+" and type='deposit' and (extra8!='dollar-amount') and (bank_id='BNK1019' ) order by date" ;
	
	System.out.println("bnktrn===>"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		if(rs.getString("extra4")!=null && !rs.getString("extra4").equals("null")){
			list[0] = rs.getString("extra4");
		}
		if(rs.getString("amount")!=null && !rs.getString("amount").equals("null")){
			list[1] = rs.getString("amount");
		}
	}
	s.close();
	rs.close();
	c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}









public List<banktransactions> getBankTransactionBasedOnHoidNewDayWise(String reportType, String date)
{
	ArrayList<banktransactions> list = new ArrayList<banktransactions>();
	try {
	String selectquery="";
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	if(reportType.equals("pro-counter")){
		selectquery="SELECT * FROM banktransactions where date>='"+date+" 00:00:00' and date<='"+date+" 23:59:59' and (extra4 = '"+reportType+"' OR extra4 ='Development-cash') and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1013' || bank_id='BNK1016' || bank_id='BNK1019' || bank_id='BNK1027' || bank_id='BNK1028'  || bank_id='BNK1012' || bank_id='BNK1015' )";
/*		selectquery="SELECT * FROM banktransactions where date>='"+date+" 00:00:00' and date<='"+date+" 23:59:59' and extra4 = '"+reportType+"' and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1013' || bank_id='BNK1016' || bank_id='BNK1019' )";*/
	}else if(reportType.equals("Charity-cash")){
		selectquery="SELECT * FROM banktransactions where date>='"+date+" 00:00:00' and date<='"+date+" 23:59:59' and extra4 = '"+reportType+"' and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1012' || bank_id='BNK1019' || bank_id='BNK1029' || bank_id='BNK1013' )";
	}
	else if(reportType.equals("poojastore-counter")){
		selectquery="SELECT * FROM banktransactions where date>='"+date+" 00:00:00' and date<='"+date+" 23:59:59' and extra4 = '"+reportType+"' and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and (bank_id='BNK1015' || bank_id='BNK1030')";
	}
	else if(reportType.equals("Sainivas")){
		selectquery="SELECT * FROM banktransactions where date>='"+date+" 00:00:00' and date<='"+date+" 23:59:59' and extra4 = '"+reportType+"' and type='deposit' and (extra8!='dollar-amount' and extra8!='other-amount') and ( bank_id='BNK1031' || bank_id='BNK1013')";
	}
	System.out.println("bnktrn("+date+")===>"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new banktransactions(rs.getString("banktrn_id"), rs.getString("bank_id"), rs.getString("name"), rs.getString("narration"), rs.getString("date"), rs.getString("amount"), rs.getString("type"), rs.getString("createdBy"), rs.getString("editedBy"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("brs_date"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("sub_head_id"), rs.getString("minor_head_id"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13")));
	}
	s.close();
	rs.close();
	c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}





public List<banktransactions> getBankTransactionBasedOnHoidNewDayWiseCardOnline(String reportType, String date)
{
	ArrayList<banktransactions> list = new ArrayList<banktransactions>();
	try {
	String selectquery="";
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	 if(reportType.equals("Charity-cash")){
		selectquery="SELECT * FROM banktransactions where date>='"+date+" 00:00:00' and date<='"+date+" 23:59:59' and extra4 = '"+reportType+"' and type='deposit' and (extra8!='dollar-amount' ) and ( bank_id='BNK1019' )";
	}
	
	System.out.println("bnktrn("+date+")===>"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new banktransactions(rs.getString("banktrn_id"), rs.getString("bank_id"), rs.getString("name"), rs.getString("narration"), rs.getString("date"), rs.getString("amount"), rs.getString("type"), rs.getString("createdBy"), rs.getString("editedBy"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("brs_date"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("sub_head_id"), rs.getString("minor_head_id"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13")));
	}
	s.close();
	rs.close();
	c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public double getPettyCashTotalAmount(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String hoid){
	double amount=0.0;
	// Load the database driver
	try {
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String reprttypq="";
		String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
		List<String> mseids=null;
		String mseid="";
		
		mseids=new ArrayList<String>();
		//String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
		if(!reporttyp.equals(""))
		{
			reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
		}
		if(finyrto!=null && finyrto.equals("like")){
			//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
			fromtoq="  and  p.date like '"+finyrfrm+"%' ";
		}
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT pe.extra5 FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and "+reprttypq+" and pe.head_account_id='"+hoid+"'and pe.extra1='Payment Done'"+fromtoq;
		String selectquery1="";
		//System.out.println("121212====>"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			mseid=rs.getString("extra5");
			//System.out.println("MSE Id====>"+mseid);
			mseids.add(mseid);
		}//while ends
		for(int i=0;i<mseids.size();i++){
			selectquery1="select amount from banktransactions where extra10='"+mseids.get(i)+"'";
			ResultSet rs1 = s.executeQuery(selectquery1);
			if(rs1.next()){
				amount=amount+Double.parseDouble(rs1.getString("amount"));
			}//if block ends
		}//for loop
	}//try block ends 
	catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}//catch block ends
	return amount;
			
}//getPettyCashTotalAmount() ends



public double getPettyCashTotalAmount1(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String hoid,String extraField,String... ids){
	double amount=0.0;
	String query3="";
	// Load the database driver
	try {
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String reprttypq="";
		String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
		List<String> mseids=null;
		String mseid="";
		
		mseids=new ArrayList<String>();
		//String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
		if(!reporttyp.equals(""))
		{
			reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
		}
		if(finyrto!=null && finyrto.equals("like")){
			//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
			fromtoq="  and  p.date like '"+finyrfrm+"%' ";
		}
		if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
			for(String id:ids){
				query3+="and pe."+extraField+"!='"+id+"'";
			}//for loop ends
		}//if ends
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT pe.extra5 FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and "+reprttypq+" and pe.head_account_id='"+hoid+"'and pe.extra1='Payment Done'"+fromtoq+query3;
		String selectquery1="";
		//System.out.println("121212====>"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			mseid=rs.getString("extra5");
			//System.out.println("MSE Id====>"+mseid);
			mseids.add(mseid);
		}//while ends
		for(int i=0;i<mseids.size();i++){
			selectquery1="select amount from banktransactions where extra10='"+mseids.get(i)+"'";
			ResultSet rs1 = s.executeQuery(selectquery1);
			if(rs1.next()){
				amount=amount+Double.parseDouble(rs1.getString("amount"));
			}//if block ends
		}//for loop
	}//try block ends 
	catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}//catch block ends
	return amount;
			
}//getPettyCashTotalAmount() ends
public String getStringOtherDepositByheads(String extra8,String Major_head,String Minor_head, String Sub_Head, String head_account_id,String fromdate,String todate)
{
	
	String Amt ="00";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String condition = "";
if(Minor_head != ""){
	condition = "and minor_head_id='"+Minor_head+"' ";
}
String condition1 = "";
if(Sub_Head != ""){
	condition1 = "and sub_head_id='"+Sub_Head+"' ";
}
String condition2="and date>='"+fromdate+"' and date<='"+todate+"'";
if(todate!=null && todate.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	condition2="  and  date like '"+fromdate+"%' ";
}
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' "+condition+""+condition1+"and head_account_id='"+head_account_id+"' "+condition2+"" ; 
//String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' and minor_head_id='"+Minor_head+"' and head_account_id='"+head_account_id+"' and date>='"+fromdate+"' and date<='"+todate+"'" ;
ResultSet rs = s.executeQuery(selectquery);
//System.out.println("new queruy.."+selectquery);
while(rs.next()){
	if(rs.getString("amount")!=null){
		Amt=rs.getString("amount");
		}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Amt;
}

//This method is created for Income & Expenditure to hide some heads instead of getStringOtherDepositByheads(-) method
public String getStringOtherDepositByheads1(String extra8,String Major_head,String Minor_head, String Sub_Head, String head_account_id,String fromdate,String todate,String extraField,String... ids)
{
	
	String Amt ="00";
	String query3="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String condition = "";
if(Minor_head != ""){
	condition = "and minor_head_id='"+Minor_head+"' ";
}//if ends
String condition1 = "";
if(Sub_Head != ""){
	condition1 = "and sub_head_id='"+Sub_Head+"' ";
}//if ends
String condition2="and date>='"+fromdate+"' and date<='"+todate+"'";
if(todate!=null && todate.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	condition2="  and  date like '"+fromdate+"%' ";
}//if ends
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+="and "+extraField+"!='"+id+"'";
	}//for loop ends
}//if ends
String selectquery="SELECT sum(amount) as amount FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' "+condition+""+condition1+"and head_account_id='"+head_account_id+"' "+condition2+""+query3; 
//String selectquery="SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, extra13 FROM banktransactions where  extra8 = '"+extra8+"' and major_head_id = '"+Major_head+"' and minor_head_id='"+Minor_head+"' and head_account_id='"+head_account_id+"' and date>='"+fromdate+"' and date<='"+todate+"'" ;
ResultSet rs = s.executeQuery(selectquery);
//System.out.println("new queruy.."+selectquery);
while(rs.next()){
	if(rs.getString("amount")!=null){
		Amt=rs.getString("amount");
	}//if ends
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return Amt;
}//method ends

public banktransactions getBankTransactionBsdOnJvAmount(String date,String amount)	{
	ResultSet rs = null;
	String query = "select banktrn_id,date,amount,extra4 from banktransactions where bank_id = 'BNK1019' and date >= '"+date+"' and (extra4 = 'Charity-cash' or extra4 = 'pro-counter') and extra8 = 'other-amount' and  sub_head_id = '25766' and amount= "+amount;
	banktransactions bkt = null;
	try	{
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		rs = s.executeQuery(query);
//		System.out.println("getjvamt---------"+query);
		if(rs.next())	{
			bkt = new banktransactions();
			bkt.setbanktrn_id(rs.getString("banktrn_id"));
			bkt.setdate(rs.getString("date"));
			bkt.setamount(rs.getString("amount"));
			bkt.setextra4(rs.getString("extra4"));
		}
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	catch(Exception e)	{
		e.printStackTrace();
	}
	finally	{
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return bkt;
}

public String getExtra3BsdOnBkt(String bkt)	{
	ResultSet rs = null;
	String query = "select extra3 from banktransactions where banktrn_id='"+bkt+"'";
	String jv = "";
	try	{
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		rs = s.executeQuery(query);
//		System.out.println("getjvno---------"+query);
		if(rs.next())	{
			jv = rs.getString("extra3");
		}
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	catch(Exception e)	{
		e.printStackTrace();
	}
	finally	{
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return jv;
}

} // class