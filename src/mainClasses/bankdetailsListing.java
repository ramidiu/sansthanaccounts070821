package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.bankdetails;

public class bankdetailsListing {
	private String error = "";

	public void seterror(String error) {
		this.error = error;
	}

	public String geterror() {
		return this.error;
	}

	private String bank_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;

	public void setbank_id(String bank_id) {
		this.bank_id = bank_id;
	}

	public String getbank_id() {
		return (this.bank_id);
	}

	public List getbankdetails() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where extra5 = 'Active'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getactivebankdetails() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where extra5 = 'Active'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getdeactivebankdetails() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where extra5 = 'Deactive'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getbankdetailsBasedOnType() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			/*
			 * String selectquery=
			 * "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where extra2!='credit' "
			 * ;
			 */
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where extra2!='credit' and extra5 = 'Active' ";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getMbankdetails(String bank_id) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where  bank_id = '"
					+ bank_id + "' and extra5 = 'Active'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getMbankdetailsAmount(String bank_id) {
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where  bank_id = '"
					+ bank_id + "' and extra5 = 'Active'";
			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("total_amount");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getBanksBasedOnKey(String key, String hoaid, String banktype) {
		ArrayList list = new ArrayList();
		String bankquery = "";
		try {
			String HOA = "";
			if (hoaid != null && !hoaid.equals("")) {
				HOA = " and headAccountId='" + hoaid + "'";
			}
			if ("credit".equals(banktype)) {
				bankquery = " and extra2!='" + banktype + "' ";
			}
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where (bank_id like '%"
					+ key
					+ "%' || bank_name like '%"
					+ key
					+ "%') and extra5 = 'Active'" + HOA + bankquery;
			// System.out.println("query===>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getBanksBasedOnKeys(String key, String hoaid, String banktype,
			String type) {
		ArrayList list = new ArrayList();
		String bankquery = "";
		try {
			String HOA = "";
			if (hoaid != null && !hoaid.equals("")) {
				HOA = " and headAccountId='" + hoaid + "'";
			}
			if ("credit".equals(banktype)) {
				bankquery = " and extra2!='" + banktype + "' ";
			}
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where extra2='"
					+ type
					+ "' and (bank_id like '%"
					+ key
					+ "%' || bank_name like '%"
					+ key
					+ "%') and extra5 = 'Active'" + HOA + bankquery;
			// System.out.println("query===>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getNormalAndLoanBanksBasedOnKeys(String key, String hoaid) {
		ArrayList list = new ArrayList();
		try {
			String HOA = "";
			if (hoaid != null && !hoaid.equals("")) {
				HOA = " and headAccountId='" + hoaid + "'";
			}
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where extra2 in ('debit','credit') and extra5 = 'Active' and (bank_id like '%"
					+ key + "%' || bank_name like '%" + key + "%') " + HOA;
			// System.out.println("query===>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getBankID(String headId) {
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where  headAccountId = '"
					+ headId + "' and extra5 = 'Active'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("bank_id");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getPettyCashAmount(String bank_id) {
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT extra1 FROM bankdetails where  bank_id = '"
					+ bank_id + "' and extra5 = 'Active'";
			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("extra1");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getBankName(String bank_id) {
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_name FROM bankdetails where  bank_id = '"
					+ bank_id + "' and extra5 = 'Active'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("bank_name");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getBanksBasedOnHOA(String hoaid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where headAccountId='"
					+ hoaid + "'and extra2!='credit' and extra5 = 'Active'";
			// System.out.println("11111...."+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getBanksBasedOnBank(String bankname) {
		ArrayList list = new ArrayList();
		// String head_account="";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where bank_name='"
					+ bankname + "' and extra2!='credit' and extra5 = 'Active'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));
				// head_account=rs.getString("headAccountId");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getCreditBanksBasedOnHOA(String hoaid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where headAccountId='"
					+ hoaid + "' and extra2='credit' and extra5 = 'Active'";
			// System.out.println("22222==>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getBanksBasedOnHOA(String hoaid, String banktype,
			String departmenttype) {
		ArrayList list = new ArrayList();
		StringBuilder departmentquery = new StringBuilder();
		if (!departmenttype.equals("")) {
			departmentquery.append(" and extra4='" + departmenttype + "' ");
		}
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where headAccountId='"
					+ hoaid
					+ "' and extra5 = 'Active' and extra2!='credit'and extra3='"
					+ banktype + "' " + departmentquery.toString();
			/* System.out.println(selectquery); */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new bankdetails(rs.getString("bank_id"), rs
						.getString("bank_name"), rs.getString("bank_branch"),
						rs.getString("account_holder_name"), rs
								.getString("account_number"), rs
								.getString("ifsc_code"), rs
								.getString("total_amount"), rs
								.getString("headAccountId"), rs
								.getString("createdBy"), rs
								.getString("editedBy"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3"), rs
								.getString("extra4"), rs.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public double getBanksAmountBasedOnHOA(String hoaid, String banktype,
			String departmenttype) {
		double list = 0.0;
		StringBuilder departmentquery = new StringBuilder();
		if (!departmenttype.equals("")) {
			departmentquery.append(" and extra4='" + departmenttype + "' ");
		}
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT bank_id,bank_name,bank_branch,account_holder_name,account_number,ifsc_code,total_amount,headAccountId,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5 FROM bankdetails where headAccountId='"
					+ hoaid
					+ "' and extra5 = 'Active' and extra2!='credit'and extra3='"
					+ banktype + "' " + departmentquery.toString();
			/* System.out.println(selectquery); */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = Double.parseDouble((rs.getString("total_amount")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List<bankdetails> getPettyCashBasedOnHeadAccountId(
			String headAccountId) {
		String selectQuery = "";
		List<bankdetails> bankdetailsList = new ArrayList<bankdetails>();
		bankdetails Bankdetails = null;
		try {
			c = ConnectionHelper.getConnection();

			s = c.createStatement();
			if (headAccountId == null || headAccountId.equals("")) {
				selectQuery = "SELECT bank_id,bank_name FROM bankdetails ";
			} else {
				selectQuery = "SELECT bank_id,bank_name FROM bankdetails  where headAccountId='"
						+ headAccountId + "'";
			}
			ResultSet rs = s.executeQuery(selectQuery);

			while (rs.next()) {
				Bankdetails = new bankdetails();
				Bankdetails.setbank_id(rs.getString("bank_id"));
				Bankdetails.setbank_name(rs.getString("bank_name"));
				bankdetailsList.add(Bankdetails);

			}
		} catch (Exception e) {

		}

		return bankdetailsList;
	}
}