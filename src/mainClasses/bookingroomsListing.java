package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.SainivasConnectionHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import beans.booking_roomsdetails;
import beans.bookingrooms;
public class bookingroomsListing 
 {
private String error="";
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
java.text.SimpleDateFormat parse = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String booking_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setbooking_id(String booking_id)
{
this.booking_id = booking_id;
}
public String getbooking_id(){
return(this.booking_id);
}
public List getbookingrooms()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM bookingrooms"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List gettopbookingids()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM bookingrooms"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMbookingrooms(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT booking_id,checkindate,checkin_time,checkoutdate,checkouttime,current_date,total_amount,total_noof_rooms,total_noof_persons,booked_person_name,id_proof,document_number,issue_date,expiry_date,second_proof,secondproof_desc,mobile_number,address,vechile_flag,vechile_number,transportaion_flag,transportation_charges,`from`,`to`,room_transfer_flag,image_person,scan_document,trans,up,downs,shift,counter,service_charges,internet_charges,card_number,trsn_tracking_num,booking_confirmation_number,bank_res,bank_res_2,bank_stat_number,new_1,booked_flag,checkin_flag,checkout_flag,paymentstauts,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra1,extra11,extra10,ip_address,extra_bed_required,extra_bed_charges FROM bookingrooms where  booking_id = '"+booking_id+"'";  
//System.out.println("Booking_id ** =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMbookingamount(String booking_id)
{
String list ="";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT total_amount FROM bookingrooms where  booking_id = '"+booking_id+"'";  
//System.out.println("Booking_id ** =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list=rs.getString("total_amount");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMbookingextrabedcount(String booking_id)
{
String list ="";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra_bed_required FROM bookingrooms where  booking_id = '"+booking_id+"'";  
//System.out.println("Booking_id ** =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list=rs.getString("extra_bed_required");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getNoofDaysLoginid(String login_id,String fromdate,String todate)
{
String list ="0";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(extra8) as noofdays FROM shrisainivas.bookingrooms where booked_person_name='"+login_id+"' and (checkindate between '"+fromdate+"' and '"+todate+"')";  
//System.out.println("Booking_id ** =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("noofdays")!=null){
	list=rs.getString("noofdays"); }
	
		

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalAmount(String paymentstatus,String fromdate,String todate)
{
String list ="0";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(total_amount) as amount FROM shrisainivas.bookingrooms where paymentstauts='"+paymentstatus+"' and (checkindate between '"+fromdate+"' and '"+todate+"')";  
System.out.println("Booking_id ** =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("amount")!=null){
	list=rs.getString("amount"); }
	
		

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public String getMbookingextrabedcharges(String booking_id)
{
String list ="";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra_bed_charges FROM bookingrooms where  booking_id = '"+booking_id+"'";  
//System.out.println("Booking_id ** =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list=rs.getString("extra_bed_charges");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/////////////////////////Get Dummy/Blocked Rooms////////////////

public List getDummyBookings()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT booking_id,checkindate,checkin_time,checkoutdate,checkouttime,current_date,total_amount,total_noof_rooms,total_noof_persons,booked_person_name,id_proof,document_number,issue_date,expiry_date,second_proof,secondproof_desc,mobile_number,address,booked_flag,checkin_flag,checkout_flag,paymentstauts,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra1,extra11,extra10,up,downs,shift,service_charges,internet_charges,extra_bed_required,extra_bed_charges FROM bookingrooms where extra2='dummy'"; 
//System.out.println("Dummy :: "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}


/*For availability check*/
public String getavailabilityRooms(String checkin,String checkout,String roomtype)
{
String list ="";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select noof_rooms,booking_id from booking_roomsdetails where roomtype_id='"+roomtype+"'  and booking_id in (select booking_id from bookingrooms where '"+checkin+"'<=`checkoutdate` and `checkindate` <='"+checkout+"' and booked_flag='booked' and b.checkout_flag='no' and paymentstauts='success')"; 
//System.out.println("availability ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//System.out.println("****"+rs.getString("booking_id")+"::"+rs.getString("noof_rooms")+"<br>");
//String noofrooms=rs.getString(1);
 list=rs.getString("booking_id")+"::"+rs.getString("noof_rooms");
//System.out.println("Fianl ="+list);
}

}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

//********In a single loop

public List getavailabilityRooms_single(String roomtype)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkoutdate,b.checkout_flag,b.document_number,br.noof_rooms FROM bookingrooms b,booking_roomsdetails br,booked_room bkr where b.booking_id=br.booking_id and b.booking_id=bkr.booking_id and b.booking_id in(select booking_id from booking_roomsdetails where roomtype_id='"+roomtype+"' and (booked_flag='booked' or booked_flag='postpre') and ((b.checkout_flag='no') or (b.checkout_flag='yes' and bkr.extra1='') or (b.checkout_flag='yes' and bkr.extra1='Dirty')) and paymentstauts='success')"; 
//System.out.println("*****************Single** ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	
	
list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("checkout_flag"),rs.getString("document_number"),rs.getString("noof_rooms")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getavailabilityRooms_single_withrooms(String roomtype)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkoutdate,b.checkout_flag,b.document_number,br.noof_rooms,bkr.room_number FROM bookingrooms b,booking_roomsdetails br,booked_room bkr where b.booking_id=br.booking_id and b.booking_id=bkr.booking_id  and b.booking_id in(select booking_id from booking_roomsdetails where roomtype_id='"+roomtype+"' and (booked_flag='booked' or booked_flag='postpre') and ((b.checkout_flag='no') or (b.checkout_flag='yes' and bkr.extra1='') or (b.checkout_flag='yes' and bkr.extra1='Dirty')) and paymentstauts='success')"; 
//System.out.println("*****************Single** ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	
	
list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("checkout_flag"),rs.getString("document_number"),rs.getString("noof_rooms"),rs.getString("room_number")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


//*************************all the Bookings which are not checked in for today*******************************************
public List getBookingNotCheckinsForToday(String todaystart,String todayend)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='postpre') and (checkin_flag='no' or checkin_flag='yes') and paymentstauts='success' AND checkindate between '"+todaystart+"' and '"+todayend+"' and b.extra2 not in ('dummy') order by `checkindate` asc"; 
//System.out.println("getBookingNotCheckins ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("\n"+rs.getString("booking_id"));
	//System.out.println("\n"+rs.getString("checkindate"));
	//System.out.println("\n"+rs.getString("checkin_time"));
	//System.out.println("\n"+rs.getString("checkoutdate"));
	//System.out.println("\n"+rs.getString("checkouttime"));
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getBookingNotCheckinsForTodayNoRT(String todaystart,String todayend)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre') and (checkin_flag='no' or checkin_flag='yes') and paymentstauts='success' AND checkindate between '"+todaystart+"' and '"+todayend+"' and b.extra2 not in ('dummy') order by `checkindate` asc"; 
//System.out.println("getBookingNotCheckinsForTodayNoRT ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getBookingNotCheckinsForTodayNoRTOfRoomNo(String todaystart,String todayend,String roomno)
{
	String list = "";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booked_room br where ( br.booking_id=b.booking_id) and(booked_flag='booked' or booked_flag='postpre') and (checkin_flag='no' or checkin_flag='yes') and paymentstauts='success' AND (checkindate between '"+todaystart+"' and '"+todayend+"' )and b.extra2 not in ('dummy') and (br.room_number='"+roomno+"')  order by `checkindate` asc";
                    
//System.out.println("getBookingNotCheckinsForTodayNoRT ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list=rs.getString("booking_id");

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
//***********************************All the bookings****************************//

public List getAllBookingsNotCheckedins()
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='postpre') and checkin_flag='no' and paymentstauts='success' and b.extra2 not in ('dummy')"; 
//System.out.println("getBookingNotCheckins ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("\n"+rs.getString("booking_id"));
	//System.out.println("\n"+rs.getString("checkindate"));
	//System.out.println("\n"+rs.getString("checkin_time"));
	//System.out.println("\n"+rs.getString("checkoutdate"));
	//System.out.println("\n"+rs.getString("checkouttime"));
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getAllBookingsNotCheckedinsNotRT()
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra3,b.extra4,b.extra7,b.extra2 FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre') and checkin_flag='no' and paymentstauts='success' and b.extra2 not in ('dummy')"; 
//System.out.println("getBookingNotCheckins ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("\n"+rs.getString("booking_id"));
//	System.out.println("\n"+rs.getString("checkindate"));
//	System.out.println("\n"+rs.getString("checkin_time"));
//	System.out.println("\n"+rs.getString("checkoutdate"));
//	System.out.println("\n"+rs.getString("checkouttime"));
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getAllBookingsNotCheckedInsNotRT()
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select * FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre') and checkin_flag='no' and paymentstauts='success' and b.extra2 not in ('dummy')"; 
//System.out.println("getAllBookingsNotCheckedInsNotRT ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("\n"+rs.getString("booking_id"));
//	System.out.println("\n"+rs.getString("checkindate"));
//	System.out.println("\n"+rs.getString("checkin_time"));
//	System.out.println("\n"+rs.getString("checkoutdate"));
//	System.out.println("\n"+rs.getString("checkouttime"));
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getAllBookingsNotCheckedInsNotRTbydate(String s1,String s2)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select * FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre')  and (checkindate between '"+s1+"' and '"+s2+"') and checkin_flag='no' and paymentstauts='success' and b.extra2 not in ('dummy')"; 
//System.out.println("getAllBookingsNotCheckedInsNotRTbydate ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("\n"+rs.getString("booking_id"));
//	System.out.println("\n"+rs.getString("checkindate"));
//	System.out.println("\n"+rs.getString("checkin_time"));
//	System.out.println("\n"+rs.getString("checkoutdate"));
//	System.out.println("\n"+rs.getString("checkouttime"));
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getAllBookingsNotCheckedInsNotRTbydate1(String s1,String s2)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select * FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre')  and (checkindate between '"+s1+"' and '"+s2+"')  and paymentstauts='success' and b.extra2 not in ('dummy')"; 
//System.out.println("getAllBookingsNotCheckedInsNotRTbydate ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("\n"+rs.getString("booking_id"));
//	System.out.println("\n"+rs.getString("checkindate"));
//	System.out.println("\n"+rs.getString("checkin_time"));
//	System.out.println("\n"+rs.getString("checkoutdate"));
//	System.out.println("\n"+rs.getString("checkouttime"));
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getBookingsForMontly(String todaystart,String todayend)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked' AND b.current_date between '"+todaystart+"' and '"+todayend+"' and b.extra2 not in ('dummy')"; 
//System.out.println("getBookingsForMontly ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));
}
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getBookingsForMontlyNotRT(String todaystart,String todayend)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra3,b.extra4,b.extra7 FROM bookingrooms b where b.booked_flag='booked' AND b.current_date between '"+todaystart+"' and '"+todayend+"' and b.extra2 not in ('dummy')"; 
//System.out.println("getBookingsForMontly ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));
}
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
///******************************Cancellations Montly**********************//

public List getCancellationsForMontly(String todaystart,String todayend)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and booked_flag='cancelled' AND issue_date between '"+todaystart+"' and '"+todayend+"'"; 
//System.out.println("getCancellationsForMontly ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));
}
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/************************To get All the details of booking_id******************************************/
public List getAllDetailsOnbookingID(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  AND b.booking_id='"+booking_id+"'";
//String selectquery="select checkindate FROM bookingrooms where booking_id='"+booking_id+"'";
//System.out.println("getAllDetailsOnbookingID ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//System.out.println("\n@@@"+sdf.format(parse.parse(rs.getString("checkindate"))));
	//list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),sdf.format(parse.parse(rs.getString("checkindate"))),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getAllDetailsOfNegativeBookings(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  AND b.booking_id='"+booking_id+"' and b.extra2='guestpay'";
//String selectquery="select checkindate FROM bookingrooms where booking_id='"+booking_id+"'";
//System.out.println("getAllDetailsOnbookingID ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//System.out.println("\n@@@"+sdf.format(parse.parse(rs.getString("checkindate"))));
	//list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),sdf.format(parse.parse(rs.getString("checkindate"))),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getGuestPayCancelBooking(String booked_person_name)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  AND b.booked_person_name='"+booked_person_name+"' and b.booked_flag='cancelled' and b.extra2='guestpay'"; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("@@@"+rs.getString("extra6"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getGuestPayCancelBooking()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  and b.booked_flag='cancelled' and b.extra2='guestpay'"; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("@@@"+rs.getString("extra6"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getchecincrossedGuestPay(String booked_person_name,String checkindate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  AND b.booked_person_name='"+booked_person_name+"' and b.booked_flag='booked' and b.checkin_flag='no' and b.extra2='guestpay' and `checkindate` <='"+checkindate+"'" ;
//System.out.println("\ngetchecincrossedGuestPay ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("@@@"+rs.getString("extra6"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getchecincrossedGuestPay(String checkindate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked' and b.checkin_flag='no' and b.extra2='guestpay' and `checkindate` <='"+checkindate+"'" ;
//System.out.println("\ngetchecincrossedGuestPay ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("@@@"+rs.getString("extra6"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getAllBookingOfCustomer(String booked_person_name)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  AND b.booked_person_name='"+booked_person_name+"' and b.paymentstauts='success'"; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("@@@"+rs.getString("extra6"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getParkedVechiles()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  AND b.checkin_flag='yes' and b.checkout_flag='no' and b.vechile_flag='yes'"; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("@@@"+rs.getString("extra6"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public List getAllDetailsOnbookingIDNotRT(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b where b.booking_id='"+booking_id+"'"; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("@@@"+rs.getString("extra6"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////


public String getMerchantEmailAddress(String booking_id)
{
String list = "";//new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select address FROM bookingrooms where booking_id='"+booking_id+"'"; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list=rs.getString("address");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
public List getOnlyBookings(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  AND b.booking_id='"+booking_id+"' "; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("@@@"+rs.getString("extra6"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


/**********************************to get all details based on Room**************************/

public List getAllDetailsOfRoom(String room_number)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='postpre') AND checkin_flag='yes' and checkout_flag='no'  AND b.booking_id in (SELECT booking_id FROM booked_room b where room_number='"+room_number+"')"; 
//System.out.println("getAllDetailsOfRoom ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getDirtyRoomsOnRoomNumber(String room_number)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8 FROM bookingrooms b,booking_roomsdetails br,booked_room bkr where b.booking_id=br.booking_id and b.booking_id=bkr.booking_id and (booked_flag='booked' or booked_flag='postpre') AND checkin_flag='yes' and checkout_flag='yes' AND b.booking_id in (SELECT booking_id FROM booked_room b where room_number='"+room_number+"' and b.extra1='Dirty')"; 
//System.out.println("getAllDetailsOfRoom ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getDummyRooms(String room_number,String startdate,String enddate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id AND checkindate between '"+startdate+"' and '"+enddate+"' and (booked_flag='booked' or booked_flag='postpre') and b.extra2='dummy'  AND b.booking_id in (SELECT booking_id FROM booked_room b where room_number='"+room_number+"')"; 
//System.out.println("getAllDetailsOfRoom ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getDummyRooms_onthisTime(String startdate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select bkr.room_number FROM bookingrooms b,booking_roomsdetails br,booked_room bkr where b.booking_id=br.booking_id and b.booking_id=bkr.booking_id AND ( ('"+startdate+"'<=`checkoutdate`)  and (`checkindate` <='"+startdate+"')) and (booked_flag='booked' or booked_flag='postpre') and b.extra2='dummy' "; 
//System.out.println("\ngetDummyRooms_onthisTime ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(rs.getString("room_number"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getDummyRooms_onthisTime(String roomtype_id,String checkindate,String checkoutdate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.total_noof_rooms,b.booked_person_name,br.roomtype_id,bkr.room_number FROM bookingrooms b,booking_roomsdetails br,booked_room bkr where b.booking_id=br.booking_id and b.booking_id=bkr.booking_id AND ( ('"+checkindate+"'<=`checkoutdate`)  and (`checkindate` <='"+checkoutdate+"')) and (booked_flag='booked' or booked_flag='postpre') and b.extra2='dummy' and roomtype_id='"+roomtype_id+"'"; 
//System.out.println("\ngetDummyRooms_onthisTime ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(rs.getString("room_number"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}




/************************To cancel the booking_id******************************************/
public List getCancelBooking(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  AND b.booking_id='"+booking_id+"' and checkin_flag='no' and (booked_flag='booked' or booked_flag='postpre' or booked_flag='cancelled') and paymentstauts='success' and trans!='wholebooking'"; 
//System.out.println("getCancelBooking ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),sdf.format(parse.parse(rs.getString("checkindate"))),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

/**************************Cancel Booking List ***************************************/
public List getCancelBookingList()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  and booked_flag='cancelled' order by `issue_date` desc "; 
//System.out.println("getCancelBooking ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
//	System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

////////////////////

public List getCancelBookingListPending()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7,b.internet_charges,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res_2 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.extra4='Pending'  and booked_flag='cancelled'  order by `issue_date` desc ";
 
//System.out.println("getCancelBooking ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res_2")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getCancelBookingListPendingProcessed()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7,b.internet_charges,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res_2 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.extra4='Pending'  and booked_flag='cancelled' and br.extra2='beingprocessed' order by `issue_date` desc ";
 
//System.out.println("getCancelBooking ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res_2")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getCancelBookingListToBeProcossed()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7,b.internet_charges,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res_2 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.extra4='Pending'  and booked_flag='cancelled' and br.extra2!='completed' and br.extra2!='beingprocessed'  order by `issue_date` desc ";
 
//System.out.println("getCancelBooking ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res_2")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getCancelBookingListPendingbydate(String from,String to)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7,b.internet_charges,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res_2 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.extra4='Pending'  and booked_flag='cancelled' and issue_date>='"+from+"' and issue_date<='"+to+"'  order by `issue_date` desc ";
 
//System.out.println("getCancelBooking ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res_2")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

//////////////////////

public List getCancelBookingListSuccess()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7,b.internet_charges,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res_2 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.extra4='success' and booked_flag='cancelled' order by `issue_date` desc "; 
//System.out.println("getCancelBookingSucess ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res_2")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getCancelBookingListSuccessbydate(String from,String to)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7,b.internet_charges,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res_2 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.extra4='success' and booked_flag='cancelled' and issue_date>='"+from+"' and issue_date<='"+to+"'  order by `issue_date` desc "; 
//System.out.println("getCancelBookingSucess ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res_2")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getCancelBookingListSuccessbydateandroom(String roomid,String to)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,c.cancel_id as id_proof ,c.refund_amount_total as document_number,b.issue_date,b.expiry_date,c.gained_amount_excludetax as second_proof,c.gained_amount_tax as secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,c.g_service_charge as extra4,b.extra7,b.internet_charges,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res_2 FROM bookingrooms b,booking_roomsdetails br,cancellation_details c where b.booking_id=br.booking_id and c.booking_id=br.booking_id and b.extra4='success' and br.roomtype_id='"+roomid+"' and booked_flag='cancelled' and issue_date like '"+to+"%'  group by br.booking_id order by `issue_date` desc"; 
//System.out.println("getCancelBookingSucess ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res_2")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getCancelBookingListPendingbydateandroom(String roomid,String to)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,c.cancel_id as id_proof ,c.refund_amount_total as document_number,b.issue_date,b.expiry_date,c.gained_amount_excludetax as second_proof,c.gained_amount_tax as secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,c.g_service_charge as extra4,b.extra7,b.internet_charges,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res_2 FROM bookingrooms b,booking_roomsdetails br,cancellation_details c where b.booking_id=br.booking_id  and c.booking_id=br.booking_id and b.extra4='Pending' and br.roomtype_id='"+roomid+"'  and booked_flag='cancelled' and issue_date like '"+to+"%'  group by br.booking_id order by `issue_date` desc"; 
//System.out.println("getCancelBookingSucess ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res_2")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getschemeBookings(String booked_person_name)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked'  and b.paymentstauts='success'  and b.booked_person_name='"+booked_person_name+"' order by `checkindate` desc "; 
//System.out.println("getschemeBookings ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getschemeBookingsDateWise(String booked_person_name,String from,String to)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked'  and (b.current_date between '"+from+"' and '"+to+"') and b.paymentstauts='success'  and b.booked_person_name='"+booked_person_name+"' order by `checkindate` desc "; 
//System.out.println("getschemeBookings ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("extra3"+rs.getString("extra3"));
//	System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getmemberBookingsDateWise(String booked_person_name,String from,String to)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked'  and (b.current_date between '"+from+"' and '"+to+"') and b.paymentstauts='success'  and b.booked_person_name='"+booked_person_name+"' order by `checkindate` desc ";
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br,counter_balance_update cb where b.booking_id=br.booking_id and b.booking_id=cb.booking_id and b.booked_flag='booked'  and (b.current_date between '"+from+"' and '"+to+"') and b.paymentstauts='success' and cb.login_id='"+booked_person_name+"' order by `checkindate` desc  ";
//System.out.println("getmemberBookingsDateWise ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("extra3"+rs.getString("extra3"));
//	System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public List getschemeBookingsSuperadmin(String booked_person_name)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked' and b.paymentstauts='success'  and b.booked_person_name='"+booked_person_name+"' and b.extra2!='dummy' order by `checkindate` desc "; 
//System.out.println("getschemeBookings ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//	System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getschemeBookingsSuperadminDAteWise(String booked_person_name,String from,String to)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked' and b.paymentstauts='success' and (b.current_date between '"+from+"' and '"+to+"')  and b.booked_person_name='"+booked_person_name+"' and b.extra2!='dummy' order by `checkindate` desc "; 
//System.out.println("getschemeBookings ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getschemeBookingsPending(String booked_person_name)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked' and b.paymentstauts='pending'  and b.booked_person_name='"+booked_person_name+"' order by `checkindate` desc "; 
//System.out.println("getschemeBookings ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getschemeBookings_cancel(String booked_person_name)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='cancelled'  and b.booked_person_name='"+booked_person_name+"' order by `checkindate` desc "; 
//System.out.println("getschemeBookings ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("extra3"+rs.getString("extra3"));
	//System.out.println("extra4"+rs.getString("extra4"));
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/*********************************Today Checkouts*******************************************/

// *****************************************************************Getting all dirty rooms for listing.....
public List getDirtyRooms()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.up,b.downs,b.shift,b.service_charges,b.internet_charges FROM bookingrooms b,booked_room bkr,booking_roomsdetails br where b.booking_id=br.booking_id and b.booking_id=bkr.booking_id and bkr.extra1='Dirty' order by checkoutdate asc "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}    

//*******************************************************Getting all dirty rooms for listing.....
public List getCleanRooms()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.up,b.downs,b.shift,b.service_charges,b.internet_charges FROM bookingrooms b,booked_room bkr,booking_roomsdetails br where b.booking_id=br.booking_id and b.booking_id=bkr.booking_id and bkr.extra1='Clean'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}    
/*****************************get All The Checkins***************************************/
public List getAllCheckins()
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked'" +
		"or booked_flag='postpre' or booked_flag='checkedout') and checkin_flag='yes' and paymentstauts='success'  order by checkoutdate asc"; 
//System.out.println("getAllCheckins ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
		
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getAllCheckinsNotRT()
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra3,b.extra4,b.extra7 FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre' or booked_flag='checkedout') and checkin_flag='yes' and paymentstauts='success'  order by checkoutdate asc"; 
//System.out.println("getAllCheckins ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
		
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}


public List getAllCheckinsNotRTbydate(String s1,String s2)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra3,b.extra4,b.extra7 FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre' or booked_flag='checkedout')  and (checkoutdate between '"+s1+"' and '"+s2+"') and checkin_flag='yes' and paymentstauts='success'  order by checkoutdate asc"; 
//System.out.println("getAllCheckins ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
		
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}


/***********************************To day Checkouts******************************************/

public List getTodayCheckouts(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='postpre' or booked_flag='checkedout') and checkin_flag='yes' and paymentstauts='success' and checkoutdate between '"+startdate+"' and '"+enddate+"' order by checkoutdate asc"; 
//System.out.println("getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getTodayCheckoutsNotRT(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre' or booked_flag='checkedout') and checkin_flag='yes' and paymentstauts='success' and checkoutdate between '"+startdate+"' and '"+enddate+"' order by checkoutdate asc"; 
//System.out.println("getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}
/**************************************************************************************************/
public List getdelayedcheckout(String startdate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b where (booked_flag='booked' or booked_flag='postpre' or booked_flag='checkedout') and checkin_flag='yes' and checkout_flag='no' and paymentstauts='success' and checkoutdate <='"+startdate+"' order by checkoutdate asc"; 
//System.out.println("getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}
/***********************************Get Online Reservations******************************************/

public List getOnlineReservations()
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='postpre') and paymentstauts='success' and b.extra5='onlineuser'"; 
//System.out.println("\ngetOnlineReservations ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/***************************************************************/
public List getcurrentdatereservations(String from_date_today)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='postpre') and paymentstauts='success' and b.extra5='onlineuser' and  checkindate>='"+from_date_today+"' order by checkindate"; 
//System.out.println("\ngetOnlineReservations ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/**************8No Show List***************************************************/
public List getolddatereservations(String from)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery=" select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='postpre') and paymentstauts='success' and b.extra5='onlineuser' and  checkindate<'"+from+"' order by checkindate "; 
//System.out.println("\ngetOnlineReservations ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/****************************************************************************/
public List getNoShowList(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (b.booked_flag='booked' or b.booked_flag='postpre') and b.paymentstauts='success' and b.checkin_flag='no'  and b.checkindate between '"+startdate+"' and '"+enddate+"' and b.extra2 not in ('dummy')";  
//System.out.println("\ngetNoShowList ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getNoShowListNotRT(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges,b.extra9,b.trsn_tracking_num,b.new_1,b.ip_address FROM bookingrooms b where (b.booked_flag='booked' or b.booked_flag='postpre') and b.paymentstauts='success' and b.checkin_flag='no'  and b.checkindate between '"+startdate+"' and '"+enddate+"' and b.extra2 not in ('dummy')";  
//System.out.println("\ngetNoShowList ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges"),rs.getString("extra9"),rs.getString("trsn_tracking_num"),rs.getString("new_1"),rs.getString("ip_address")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

/**************Checkout Pending***************************************************/
public List getCheckOutPendingList(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (b.booked_flag='booked'  or b.booked_flag='postpre') and b.paymentstauts='success' and b.checkin_flag='yes' and b.checkout_flag='no'  and b.checkoutdate between '"+startdate+"' and '"+enddate+"'";  
//System.out.println("\ngetCheckOutPendingList="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getCheckOutPendingListNotRT(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b where  (b.booked_flag='booked'  or b.booked_flag='postpre') and b.paymentstauts='success' and b.checkin_flag='yes' and b.checkout_flag='no'  and b.checkoutdate between '"+startdate+"' and '"+enddate+"'";  
//System.out.println("\ngetCheckOutPendingList="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

/***********************************Get Online Reservations******************************************/

public List getPaymentFailTransactions(String from_date,String to_date)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra3,b.extra4,b.extra7 FROM bookingrooms b,booking_roomsdetails br where (b.current_date between '"+from_date+"' and '"+to_date+"') and ( b.booking_id=br.booking_id) and (booked_flag='booked' or booked_flag='postpre') and paymentstauts='pending' order by `current_date` desc"; 
//System.out.println("\ngetOnlineReservations ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/************************Daily Cash Report***************************/

public List getDailyCashReport_bookings(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"'"; //this query calculate even room upgrade details 
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"'";
//System.out.println("getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDailyCashReport_cancellations(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and booked_flag='cancelled' and b.paymentstauts='success'  and b.issue_date between '"+startdate+"' and '"+enddate+"' and (b.extra2!='dummy' and b.extra2!='guestpay')"; 
//System.out.println("getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDailyCashReport_postpre(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and booked_flag='postpre' and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"'"; 
//System.out.println("getDailyCashReport_postpre ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDailyCashReport_bookings_login(String startdate,String enddate,String login_id)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked' and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"' and b.booked_person_name='"+login_id+"'"; //this query calculate even room upgrade details

String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.booked_flag='booked' and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"' and b.booked_person_name='"+login_id+"'";
//System.out.println("\n*****************getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDailyCashReport_cancellations_login(String startdate,String enddate,String login_id)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='cancelled' and b.paymentstauts='success'  and b.issue_date between '"+startdate+"' and '"+enddate+"' and b.booked_person_name='"+login_id+"'";//this query calculate even room upgrade details 
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.booked_flag='cancelled' and b.paymentstauts='success'  and b.issue_date between '"+startdate+"' and '"+enddate+"' and b.booked_person_name='"+login_id+"'";
//System.out.println("\n*********getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getDailyOnlineBookings(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"' and b.extra2='netbanking'"; // this query calculate even room upgrade details
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"' and b.extra2='netbanking' GROUP BY b.booking_id";
//System.out.println("\n*********getDailyOnlineBookings ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public double getDayOnlineBookings(String fromdate)
{
	double list=0.00;
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id  and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"' and b.extra2='netbanking'"; // this query calculate even room upgrade details
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,sum(b.total_amount) as totalamount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.paymentstauts='success'  and b.current_date like '"+fromdate+"%' and b.extra2='netbanking'";
//System.out.println("\n*********getDailyOnlineBookings ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list=Double.parseDouble(rs.getString("totalamount"));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getDailyOnlineCancellations(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='cancelled' and b.paymentstauts='success'  and b.issue_date between '"+startdate+"' and '"+enddate+"' and b.extra2='netbanking' and b.extra4='success'";//this query calculate even room upgrade details
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.booked_flag='cancelled' and b.paymentstauts='success'  and b.issue_date between '"+startdate+"' and '"+enddate+"' and b.extra2='netbanking' and b.extra4='success'";
//System.out.println("\n*********getDailyOnlineBookings ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public List getDailyOnlineBookingsCash(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked' and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"' and b.extra2='cash'";// this query calculate even room upgrade details
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.booked_flag='booked' and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"' and (b.extra2='cash' or b.extra2='card')";
System.out.println("\n*********Sujith tested ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public double getDayOnlineBookingsCash(String role,String fromdate)
{
	double list=0.00;
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='booked' and b.paymentstauts='success'  and b.current_date between '"+startdate+"' and '"+enddate+"' and b.extra2='cash'";// this query calculate even room upgrade details
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,sum(b.total_amount) as totalamount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.booked_flag='booked' and b.paymentstauts='success'  and b.current_date like '"+fromdate+"%' and b.extra2='cash'";
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,sum(b.total_amount) as totalamount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br,logins lg where b.booking_id=br.booking_id and b.booked_person_name=lg.login_id and br.total_cost!='00' and b.booked_flag='booked' and b.paymentstauts='success'  and b.current_date like '"+fromdate+"%'  and b.extra2='cash' and lg.role='"+role+"'";
//System.out.println("\n*********Sujith tested ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list=Double.parseDouble(rs.getString("totalamount"));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getDailyOnlineCancellationsCash(String startdate,String enddate)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.booked_flag='cancelled' and b.paymentstauts='success'  and b.issue_date between '"+startdate+"' and '"+enddate+"' and b.extra2='cash'"; //this query calculate even room upgrade details
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.total_cost!='00' and b.booked_flag='cancelled' and b.paymentstauts='success'  and b.issue_date between '"+startdate+"' and '"+enddate+"' and b.extra2='cash'";
//System.out.println("\n*********getDailyOnlineBookings ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}




public List getDailyCashReport_postpre_login(String startdate,String enddate,String login_id)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,br.roomtype_id,b.extra2,b.extra3,b.extra4,b.extra7,b.internet_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and booked_flag='postpre' and b.paymentstauts='success'  and b.issue_date between '"+startdate+"' and '"+enddate+"' and b.booked_person_name='"+login_id+"'"; 
//System.out.println("\n***********getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("roomtype_id"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra7"),rs.getString("internet_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getGrossAmount(String roomtype_id)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.current_date,b.checkindate,b.checkoutdate,b.shift,b.service_charges,b.extra_bed_charges,b.counter,b.downs,b.image_person,b.total_amount FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='cancelled') and b.paymentstauts='success' and (b.extra2!='dummy' or b.extra2!='previlized' or b.extra2!='guestpay') and br.roomtype_id='"+roomtype_id+"'"; 
//System.out.println("\n***********getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_charges"),rs.getString("counter"),rs.getString("downs"),rs.getString("image_person"),rs.getString("total_amount")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getNetAmount(String roomtype_id,String from,String to)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.current_date,b.checkindate,b.checkoutdate,b.shift,b.service_charges,b.extra_bed_charges,b.counter,b.downs,b.image_person,b.total_amount FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and (booked_flag='booked' or booked_flag='cancelled') and b.paymentstauts='success' and (b.checkindate between '"+from+"' and '"+to+"') and (b.extra2!='dummy' and b.extra2!='previlized' and b.extra2!='guestpay') and br.roomtype_id='"+roomtype_id+"'"; 
//System.out.println("\n***********getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_charges"),rs.getString("counter"),rs.getString("downs"),rs.getString("image_person"),rs.getString("total_amount")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getFocusSales(String from,String to)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and booked_flag='booked' and b.paymentstauts='success' and (b.current_date between '"+from+"' and '"+to+"') and (b.extra2!='dummy' and b.extra2!='previlized' and b.extra2!='guestpay')"; 
//System.out.println("\n***********getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getFoucsReceipts(String from,String to)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver 
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,b.current_date,b.extra2,b.booked_person_name,b.total_amount FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and booked_flag='booked' and b.paymentstauts='success' and (b.current_date between '"+from+"' and '"+to+"') and (b.extra2!='dummy' and b.extra2!='previlized' and b.extra2!='guestpay')"; 
//System.out.println("\n***********getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("current_date"),rs.getString("extra2"),rs.getString("booked_person_name"),rs.getString("total_amount")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public List getFocusPayments(String from,String to)
{
	ArrayList<bookingrooms> list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and booked_flag='cancelled' and b.paymentstauts='success' and (b.issue_date between '"+from+"' and '"+to+"') and (b.extra2!='dummy' and b.extra2!='previlized' and b.extra2!='guestpay')"; 
//System.out.println("\n***********getTodayCheckouts ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

}


}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getPrivilegeBookings()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and b.extra2='previlized'"; 
//System.out.println("getPrivilegeBookings ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getRTGSlist(String startdate,String enddate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();

String selectquery="SELECT * FROM bookingrooms  where  current_date between '"+startdate+"' and '"+enddate+"' and extra2 =' RTGS' "; 

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDueamountlist(String loginid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();

String selectquery="SELECT * FROM bookingrooms where booked_person_name='"+loginid+"'  and extra2='guestpay'";
//System.out.println(" hhh selectquery"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getvalidbookingid(String booking_id)
{
String list = "";//new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select count(*) as num FROM bookingrooms where booking_id='"+booking_id+"'"; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list=rs.getString("num");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}
public List getonlinebookingbydate(String s1,String s2)
{
	ArrayList list = new ArrayList();
	try {
		
		
		//s1="2014-01-01 12:14:57";
		//s2="2014-01-05 12:14:57";
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT booking_id,checkindate,checkin_time,checkoutdate,checkouttime,b.current_date,total_amount,total_noof_rooms,total_noof_persons,booked_person_name,id_proof,document_number,issue_date,expiry_date,second_proof,secondproof_desc,mobile_number,address,booked_flag,checkin_flag,checkout_flag,paymentstauts,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra1,extra11,extra10,up,downs,shift,service_charges,internet_charges,extra_bed_required,extra_bed_charges FROM bookingrooms b where b.extra5='onlineuser' and b.booked_flag='booked' and  (b.current_date  between '"+s1+"' and '"+s2+"' )"; 
	//System.out.println("@Dummy :: "+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	//System.out.println("Exception is ;"+e);
	}
	return list;
}
public List getonlinebookingcancelledbydate(String s1,String s2)
{
	ArrayList list = new ArrayList();
	try {
		
		
		//s1="2014-01-01 12:14:57";
		//s2="2014-01-05 12:14:57";
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT booking_id,checkindate,checkin_time,checkoutdate,checkouttime,b.current_date,total_amount,total_noof_rooms,total_noof_persons,booked_person_name,id_proof,document_number,issue_date,expiry_date,second_proof,secondproof_desc,mobile_number,address,booked_flag,checkin_flag,checkout_flag,paymentstauts,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra1,extra11,extra10,up,downs,shift,service_charges,internet_charges,extra_bed_required,extra_bed_charges FROM bookingrooms b where b.extra5='onlineuser' and b.booked_flag='cancelled' and  (b.current_date  between '"+s1+"' and '"+s2+"' )"; 
	//System.out.println("@Dummy :: "+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	//System.out.println("Exception is ;"+e);
	}
	return list;
}

public String getbookedornot(String loginid)
{
String list = "";//new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM shrisainivas.bookingrooms where booked_person_name='"+loginid+"'"; 
//System.out.println("getAllDetailsOnbookingID ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

	list=rs.getString("no");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
//System.out.println("Exception is ;"+e);
}
return list;
}
//no of bookings for month
public String getnoofbooked(String lgid,String month)
{
String list ="";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(booking_id)as no  FROM bookingrooms b where (b.booked_person_name='"+lgid+"') and ( b.current_date like '"+month+"-%') "; 
//System.out.println("availability ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
//System.out.println("****"+rs.getString("booking_id")+"::"+rs.getString("noof_rooms")+"<br>");
//String noofrooms=rs.getString(1);
 list=rs.getString("no")+"";
//System.out.println("Fianl ="+list);
}

}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

/*public List getRTGScancellationslist(String startdate,String enddate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();

String selectquery="SELECT * FROM bookingrooms  where  current_date between '"+startdate+"' and '"+enddate+"' and extra2 =' RTGS' and booked_flag='cancelled'"; 

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}*/
public HashMap getOnlineBookingList(String startdate,String enddate)
{

HashMap both = new HashMap();	
ArrayList listEbs = new ArrayList();
ArrayList listpunjab = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();

String selectquery="SELECT * FROM bookingrooms b WHERE b.current_date BETWEEN '"+startdate+"' AND '"+enddate+"' and extra2='netbanking' and bank_stat_number!='' and bank_stat_number!='0' and paymentstauts='success'"; 
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	listEbs.add(new bookingrooms(rs.getString("booking_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("internet_charges")));
}
String selectquery1="SELECT * FROM bookingrooms b WHERE b.current_date BETWEEN '"+startdate+"' AND '"+enddate+"' and extra2='netbanking' and bank_stat_number='' and paymentstauts='success'"; 
System.out.println(selectquery1);
ResultSet rs1 = s.executeQuery(selectquery1);

while(rs1.next()){
	listpunjab.add(new bookingrooms(rs1.getString("booking_id"),rs1.getString("checkindate"),rs1.getString("checkin_time"),rs1.getString("checkoutdate"),rs1.getString("checkouttime"),rs1.getString("current_date"),rs1.getString("total_amount"),rs1.getString("total_noof_rooms"),rs1.getString("total_noof_persons"),rs1.getString("booked_person_name"),rs1.getString("id_proof"),rs1.getString("document_number"),rs1.getString("issue_date"),rs1.getString("expiry_date"),rs1.getString("second_proof"),rs1.getString("secondproof_desc"),rs1.getString("mobile_number"),rs1.getString("address"),rs1.getString("booked_flag"),rs1.getString("checkin_flag"),rs1.getString("checkout_flag"),rs1.getString("paymentstauts"),rs1.getString("extra2"),rs1.getString("extra3"),rs1.getString("extra4"),rs1.getString("extra5"),rs1.getString("extra6"),rs1.getString("extra7"),rs1.getString("extra8"),rs1.getString("extra9"),rs1.getString("extra1"),rs1.getString("extra11"),rs1.getString("extra10"),rs1.getString("up"),rs1.getString("downs"),rs1.getString("shift"),rs1.getString("service_charges"),rs1.getString("internet_charges")));
}

both.put("1", listEbs);
both.put("2",listpunjab);
s.close();
rs.close();
rs1.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is from the method "+e);
}
return both;
}
public bookingrooms getTotalAmounts(String roomtype_id,String StartTime, String EndTime) {
	
	bookingrooms Bookingrooms  = new bookingrooms();
	try {
	
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery = "SELECT sum(shift) as shift,sum(service_charges) as service_charges ,sum(trans) as trans,sum(total_amount) as total_amount"
			+ " FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+roomtype_id+"' "
			+ "and b.checkoutdate between '"+StartTime+"' and '"+EndTime+"' and b.paymentstauts='success' "
			+ "and b.booked_flag!='cancelled'  and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') "
			+ " and br.total_cost!='00' ";
	System.out.println("getTotalAmounts  "+selectquery);
	ResultSet rs = s.executeQuery(selectquery);

	while(rs.next()){
	
		Bookingrooms.settrans(rs.getString("trans"));
		Bookingrooms.settotal_amount(rs.getString("total_amount"));
		Bookingrooms.setshift(rs.getString("shift"));
		Bookingrooms.setservice_charges(rs.getString("service_charges"));
		
		}
	}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	
	System.out.println("getshift ====> "+Bookingrooms.getshift());
	System.out.println("getservice_charges ====> "+Bookingrooms.getservice_charges());
	
		return Bookingrooms;

 }
public bookingrooms getTotalAmountsForRooms(String roomtype_id,String StartTime, String EndTime) {
	
	bookingrooms Bookingrooms  = new bookingrooms();
	try {
	
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery = "SELECT sum(shift) as shift,sum(service_charges) as service_charges ,sum(trans) as trans,sum(total_amount) as total_amount"
			+ " FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+roomtype_id+"' "
			+ "and b.checkoutdate between '"+StartTime+"' and '"+EndTime+"' and b.paymentstauts='success' "
			+ "and b.booked_flag!='cancelled'  and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') "
			+ " and br.total_cost!='00' ";
	System.out.println("getTotalAmounts  "+selectquery);
	ResultSet rs = s.executeQuery(selectquery);

	while(rs.next()){
	
		Bookingrooms.settrans(rs.getString("trans"));
		Bookingrooms.settotal_amount(rs.getString("total_amount"));
		Bookingrooms.setshift(rs.getString("shift"));
		Bookingrooms.setservice_charges(rs.getString("service_charges"));
		
		}
	}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	
	System.out.println("getshift ====> "+Bookingrooms.getshift());
	System.out.println("getservice_charges ====> "+Bookingrooms.getservice_charges());
	
		return Bookingrooms;

 }
public List getBookingRoomListing(String roomtype_id,String fromDate, String toDate) {
	System.out.println("this is form praveen created pate:::");
	ArrayList list = new ArrayList();
	try {
	
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	/*String selectquery = "SELECT b.booking_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name"
			+ "* FROM bookingrooms b WHERE b.checkindate >= '"+fromDate+"' AND b.checkoutdate<='"+toDate+"' AND b.";*/
	/*String selectquery = "SELECT b.booking_id,b.trans FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+roomtype_id+"' AND b.checkindate >= '"+fromDate+"' AND b.checkoutdate<='"+toDate+"'"
			+ "and b.paymentstauts='success' AND b.booked_flag!='cancelled'  and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') "
			+ " and br.total_cost!='00' ";*/
	String selectquery="SELECT b.booking_id,br.roomtype_id,b.checkindate,b.checkin_time,b.checkoutdate,b.checkouttime,b.current_date,b.total_amount,b.total_noof_rooms,b.total_noof_persons,b.booked_person_name,b.id_proof,b.document_number,b.issue_date,b.expiry_date,b.second_proof,b.secondproof_desc,b.mobile_number,b.address,b.vechile_flag,b.vechile_number,b.transportaion_flag,b.transportation_charges,b.from,b.to,b.room_transfer_flag,b.image_person,b.scan_document,b.trans,b.up,b.downs,b.shift,b.counter,b.service_charges,b.internet_charges,b.card_number,b.trsn_tracking_num,b.booking_confirmation_number,b.bank_res,b.bank_res_2,b.bank_stat_number,b.new_1,b.booked_flag,b.checkin_flag,b.checkout_flag,b.paymentstauts,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra1,b.extra11,b.extra10,b.ip_address,b.extra_bed_required,b.extra_bed_charges FROM bookingrooms b,booking_roomsdetails br "
			+ "WHERE b.booking_id=br.booking_id and br.roomtype_id='"+roomtype_id+"' AND b.checkindate >= '"+fromDate+"' AND b.checkoutdate<='"+toDate+"' and b.paymentstauts='success' AND b.booked_flag!='cancelled'  AND (b.extra2!='dummy' AND b.extra2!='guestpay' AND b.extra2!='previlized') AND br.total_cost!='00'";
	System.out.println("getTotalAmounts  "+selectquery);
	ResultSet rs = s.executeQuery(selectquery);

	while(rs.next()){
	list.add(new bookingrooms(rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("checkindate"),rs.getString("checkin_time"),rs.getString("checkoutdate"),rs.getString("checkouttime"),rs.getString("current_date"),rs.getString("total_amount"),rs.getString("total_noof_rooms"),rs.getString("total_noof_persons"),rs.getString("booked_person_name"),rs.getString("id_proof"),rs.getString("document_number"),rs.getString("issue_date"),rs.getString("expiry_date"),rs.getString("second_proof"),rs.getString("secondproof_desc"),rs.getString("mobile_number"),rs.getString("address"),rs.getString("vechile_flag"),rs.getString("vechile_number"),rs.getString("transportaion_flag"),rs.getString("transportation_charges"),rs.getString("from"),rs.getString("to"),rs.getString("room_transfer_flag"),rs.getString("image_person"),rs.getString("scan_document"),rs.getString("trans"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("counter"),rs.getString("service_charges"),rs.getString("internet_charges"),rs.getString("card_number"),rs.getString("trsn_tracking_num"),rs.getString("booking_confirmation_number"),rs.getString("bank_res"),rs.getString("bank_res_2"),rs.getString("bank_stat_number"),rs.getString("new_1"),rs.getString("booked_flag"),rs.getString("checkin_flag"),rs.getString("checkout_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra1"),rs.getString("extra11"),rs.getString("extra10"),rs.getString("ip_address"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges")));
		}
	}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	
return list;

 }
 }
