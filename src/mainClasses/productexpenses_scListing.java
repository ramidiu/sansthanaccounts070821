package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.productexpenses_sc;

import dbase.sqlcon.ConnectionHelper;

public class productexpenses_scListing {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String exp_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
public String getExp_id() {
	return exp_id;
}
public void setExp_id(String exp_id) {
	this.exp_id = exp_id;
}
public String getCountOfRecords(String expId)
{
String count="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as count FROM productexpenses_sc where exp_id ='"+expId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	count=rs.getString("count");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return count;
}

public String getMaxDateForRecord(String expId)
{
String date="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT max(entry_date) as date FROM productexpenses_sc where exp_id ='"+expId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	date=rs.getString("date");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return date;
}

public String getCountOfRecordsBasedOnExpinId(String expInvId)
{
String count="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as count FROM productexpenses_sc where expinv_id ='"+expInvId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	count=rs.getString("count");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return count;
}

public String getMaxDateForRecordBasedOnExpinId(String expInvId)
{
String date="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT max(entry_date) as date FROM productexpenses_sc where expinv_id ='"+expInvId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	date=rs.getString("entry_date");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return date;
}
public List getpaymentsByDateByHOA(String head_account_id,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(p.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where pe.entry_date<'"+entry_date+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";  //commented by pradeep
String selectquery="SELECT sum(amount) as TBECResolutionNO,exp_id, vendorId, narration, entry_date, major_head_id, minorhead_id, sub_head_id, expinv_id, emp_id, extra1, extra2, extra3, extra4, extra5, head_account_id, enter_by, enter_date, extra6, extra7, extra8, extra9, extra10 FROM productexpenses_sc where entry_date>='"+fromdate+"' and entry_date <='"+todate+"' and vendorId!='1001' and  head_account_id='"+head_account_id+"' and extra1='Payment Done'  group by vendorId ";
System.out.println("paymentlist==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses_sc(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

}
