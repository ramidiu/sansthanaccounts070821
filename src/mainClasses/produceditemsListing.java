package mainClasses;
import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.produceditems;
public class produceditemsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String pro_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setpro_id(String pro_id)
{
this.pro_id = pro_id;
}
public String getpro_id(){
return(this.pro_id);
}
public List getproduceditems()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pro_id,sstid,productId,produced_date,description,trans_id,status,quantity,emp_id,sale_price,head_account_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM produceditems "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new produceditems(rs.getString("pro_id"),rs.getString("sstid"),rs.getString("productId"),rs.getString("produced_date"),rs.getString("description"),rs.getString("trans_id"),rs.getString("status"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("sale_price"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMproduceditems(String pro_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pro_id,sstid,productId,produced_date,description,trans_id,status,quantity,emp_id,sale_price,head_account_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM produceditems where  pro_id = '"+pro_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new produceditems(rs.getString("pro_id"),rs.getString("sstid"),rs.getString("productId"),rs.getString("produced_date"),rs.getString("description"),rs.getString("trans_id"),rs.getString("status"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("sale_price"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproduceditemsKOT()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pro_id,sstid,productId,produced_date,description,trans_id,status,quantity,emp_id,sale_price,head_account_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM produceditems  where sstid not in(SELECT extra1 FROM consumption_entries  group by extra1)group by sstid "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new produceditems(rs.getString("pro_id"),rs.getString("sstid"),rs.getString("productId"),rs.getString("produced_date"),rs.getString("description"),rs.getString("trans_id"),rs.getString("status"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("sale_price"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproduceditems(String sstid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pro_id,sstid,pi.productId,produced_date,pi.description,trans_id,status,quantity,emp_id,sale_price,pi.head_account_id,pi.extra1,pi.extra2,pi.extra3,pi.extra4,pi.extra5,pi.extra6,pi.extra7 FROM sansthanaccounts.produceditems pi  where  sstid = '"+sstid+"'  group by pro_id "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new produceditems(rs.getString("pro_id"),rs.getString("sstid"),rs.getString("productId"),rs.getString("produced_date"),rs.getString("description"),rs.getString("trans_id"),rs.getString("status"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("sale_price"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List GetProducedItemBasedOnHeadOfAccount(String FromDate,String ToDate,String HeadOfAccount)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String InnerQuery="";
if(!HeadOfAccount.equals("")){
	InnerQuery=" and head_account_id='"+HeadOfAccount+"' ";
}
String selectquery="SELECT pro_id,sstid,productId,produced_date,description,trans_id,status,quantity,emp_id,sale_price,head_account_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM produceditems where  produced_date between '"+FromDate+"' and '"+ToDate+"'"+InnerQuery;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new produceditems(rs.getString("pro_id"),rs.getString("sstid"),rs.getString("productId"),rs.getString("produced_date"),rs.getString("description"),rs.getString("trans_id"),rs.getString("status"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("sale_price"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public double getDayTransferQty(String productID,String fromDate,String toDate)
{
double list =0.00;
try {
 // Load the database driver	
		
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pro_id,sstid,productId,produced_date,description,trans_id,status,sum(quantity) as quantity,emp_id,sale_price,head_account_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM produceditems where productId='"+productID+"' and produced_date >= '"+fromDate+"' and produced_date <= '"+toDate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null)
 list=Double.parseDouble(rs.getString("quantity"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


}