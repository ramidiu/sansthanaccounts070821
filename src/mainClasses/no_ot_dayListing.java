package mainClasses;
import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.*;
public class no_ot_dayListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String s_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void sets_id(String s_id)
{
this.s_id = s_id;
}
public String gets_id(){
return(this.s_id);
}
public List getno_ot_day()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT s_id,no_ot_date FROM no_ot_day "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new no_ot_day(rs.getString("s_id"),rs.getString("no_ot_date")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMno_ot_day(String s_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT s_id,no_ot_date FROM no_ot_day where  s_id = '"+s_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new no_ot_day(rs.getString("s_id"),rs.getString("no_ot_date")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getNno_ot_day(String no_ot_date)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT s_id,no_ot_date FROM no_ot_day where  no_ot_date = '"+no_ot_date+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new no_ot_day(rs.getString("s_id"),rs.getString("no_ot_date")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}