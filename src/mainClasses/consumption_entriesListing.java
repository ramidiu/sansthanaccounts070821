package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.consumption_entries;
public class consumption_entriesListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String consumption_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setconsumption_id(String consumption_id)
{
this.consumption_id = consumption_id;
}
public String getconsumption_id(){
return(this.consumption_id);
}
public List getconsumption_entries()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT consumption_id,entry_date,consumption_category,con_sub_category,head_account_id,narration,items_qty,devotees_qty,damage_qty,leftover_qty,emp_id,consumption_invoice_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM consumption_entries "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new consumption_entries(rs.getString("consumption_id"),rs.getString("entry_date"),rs.getString("consumption_category"),rs.getString("con_sub_category"),rs.getString("head_account_id"),rs.getString("narration"),rs.getString("items_qty"),rs.getString("devotees_qty"),rs.getString("damage_qty"),rs.getString("leftover_qty"),rs.getString("emp_id"),rs.getString("consumption_invoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getConsumptionReportBasedOnDates(String hoaid,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT consumption_id,entry_date,consumption_category,con_sub_category,head_account_id,narration,items_qty,devotees_qty,damage_qty,leftover_qty,emp_id,consumption_invoice_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM consumption_entries where entry_date>='"+fromdate+"' and entry_date<='"+todate+"' "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new consumption_entries(rs.getString("consumption_id"),rs.getString("entry_date"),rs.getString("consumption_category"),rs.getString("con_sub_category"),rs.getString("head_account_id"),rs.getString("narration"),rs.getString("items_qty"),rs.getString("devotees_qty"),rs.getString("damage_qty"),rs.getString("leftover_qty"),rs.getString("emp_id"),rs.getString("consumption_invoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMconsumption_entries(String consumption_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT consumption_id,entry_date,consumption_category,con_sub_category,head_account_id,narration,items_qty,devotees_qty,damage_qty,leftover_qty,emp_id,consumption_invoice_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM consumption_entries where  consumption_id = '"+consumption_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new consumption_entries(rs.getString("consumption_id"),rs.getString("entry_date"),rs.getString("consumption_category"),rs.getString("con_sub_category"),rs.getString("head_account_id"),rs.getString("narration"),rs.getString("items_qty"),rs.getString("devotees_qty"),rs.getString("damage_qty"),rs.getString("leftover_qty"),rs.getString("emp_id"),rs.getString("consumption_invoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getConsumption_entriesBasedKOT(String kotid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT consumption_id,entry_date,consumption_category,con_sub_category,head_account_id,narration,items_qty,devotees_qty,damage_qty,leftover_qty,emp_id,consumption_invoice_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7 FROM consumption_entries where  extra1 = '"+kotid+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new consumption_entries(rs.getString("consumption_id"),rs.getString("entry_date"),rs.getString("consumption_category"),rs.getString("con_sub_category"),rs.getString("head_account_id"),rs.getString("narration"),rs.getString("items_qty"),rs.getString("devotees_qty"),rs.getString("damage_qty"),rs.getString("leftover_qty"),rs.getString("emp_id"),rs.getString("consumption_invoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}