package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import beans.GatePassEntry;
import dbase.sqlcon.ConnectionHelper;

public class GatePassService {

	Connection connection = null;
	Statement statement = null;

	public void saveGatePassEntryDetails(GatePassEntry gatePassEntry) {
		try {
			connection = ConnectionHelper.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from no_genarator where table_name='gatepassentry'");
			resultSet.next();
			int gatepassentryNumber = resultSet.getInt("table_id");
			String prefix = resultSet.getString("id_prefix");
			resultSet.close();
			int incrementdgatepassentryId = gatepassentryNumber + 1;
			String gatePassEntryId = prefix + incrementdgatepassentryId;
			String query = "insert into gatepassentry  values('" + gatePassEntryId + "','" + gatePassEntry.getStaffId()
					+ "','" + gatePassEntry.getStaffName() + "','" + gatePassEntry.getDepartment() + "','"
					+ gatePassEntry.getDescription() + "','" + gatePassEntry.getCreatedDate() + "','"
					+ gatePassEntry.getOuttime() + "','','"+gatePassEntry.getInTime()+"','','','"+gatePassEntry.getGatePassType()+"')";
			statement.executeUpdate(query);
			statement.executeUpdate("update no_genarator set table_id=" + incrementdgatepassentryId
					+ " where table_name='gatepassentry'");
			statement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<GatePassEntry> gatePassEntryList() {
		List<GatePassEntry> gatepassEntryList = new ArrayList<GatePassEntry>();
		try {
			connection = ConnectionHelper.getConnection();
			statement = connection.createStatement();
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String fromDate = sdf.format(date);
			String selectquery = "SELECT * FROM gatepassentry where intime!='' and outtime!='' order by createddate";
			ResultSet resultSet = statement.executeQuery(selectquery);
			while (resultSet.next()) {
				GatePassEntry gatePassEntry = new GatePassEntry();
				gatePassEntry.setGatePassEntryId(resultSet.getString("gatepassentry_id"));
				gatePassEntry.setStaffId(resultSet.getString("staffid"));
				gatePassEntry.setStaffName(resultSet.getString("staffname"));
				gatePassEntry.setDepartment(resultSet.getString("department"));
				gatePassEntry.setDescription(resultSet.getString("description"));
				gatePassEntry.setOuttime(resultSet.getString("outtime"));
				gatePassEntry.setInTime(resultSet.getString("intime"));
				gatePassEntry.setGatePassType(resultSet.getString("gatepasstype"));
				gatepassEntryList.add(gatePassEntry);
			}
			statement.close();
			resultSet.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return gatepassEntryList;
	}

	public void updateGatePassEntry(String updatedDate, String intime, String gatePassEntryId) {
		try {
			connection = ConnectionHelper.getConnection();
			statement = connection.createStatement();
			statement.executeUpdate("update gatepassentry set updateddate='" + updatedDate + "',intime='" + intime
					+ "' where gatepassentry_id='" + gatePassEntryId + "'");
			statement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<GatePassEntry> gateUpdatedPassEntryList() {
		List<GatePassEntry> gatepassEntryList = new ArrayList<GatePassEntry>();
		try {
			connection = ConnectionHelper.getConnection();
			statement = connection.createStatement();
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String fromDate = sdf.format(date);
			String selectquery = "SELECT * FROM gatepassentry where createddate between '" + fromDate
					+ " 00:00:00' and '" + fromDate
					+ " 23:59:59' and updateddate!='' and intime!='' order by createddate";
			ResultSet resultSet = statement.executeQuery(selectquery);
			while (resultSet.next()) {
				GatePassEntry gatePassEntry = new GatePassEntry();
				gatePassEntry.setGatePassEntryId(resultSet.getString("gatepassentry_id"));
				gatePassEntry.setStaffId(resultSet.getString("staffid"));
				gatePassEntry.setStaffName(resultSet.getString("staffname"));
				gatePassEntry.setDepartment(resultSet.getString("department"));
				gatePassEntry.setDescription(resultSet.getString("description"));
				gatePassEntry.setInTime(resultSet.getString("inTime"));
				gatePassEntry.setOuttime(resultSet.getString("outtime"));
				gatePassEntry.setUpdatedDate(resultSet.getString("updateddate"));
				gatePassEntry.setCreatedDate(resultSet.getString("createddate"));
				gatepassEntryList.add(gatePassEntry);
			}
			statement.close();
			resultSet.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return gatepassEntryList;
	}

	public List<GatePassEntry> gateUpdatedPassEntryListBasedOnDate(String fromDate, String toDate) {
		List<GatePassEntry> gatepassEntryList = new ArrayList<GatePassEntry>();
		try {
			connection = ConnectionHelper.getConnection();
			statement = connection.createStatement();
			String selectquery = "SELECT * FROM gatepassentry where createddate between '" + fromDate + "' and '"
					+ toDate
					+ "'  and clnumber='' and cltype=''  order by createddate";
			ResultSet resultSet = statement.executeQuery(selectquery);
			while (resultSet.next()) {
				GatePassEntry gatePassEntry = new GatePassEntry();
				gatePassEntry.setGatePassEntryId(resultSet.getString("gatepassentry_id"));
				gatePassEntry.setStaffId(resultSet.getString("staffid"));
				gatePassEntry.setStaffName(resultSet.getString("staffname"));
				gatePassEntry.setDepartment(resultSet.getString("department"));
				gatePassEntry.setDescription(resultSet.getString("description"));
				gatePassEntry.setInTime(resultSet.getString("inTime"));
				gatePassEntry.setOuttime(resultSet.getString("outtime"));
				gatePassEntry.setUpdatedDate(resultSet.getString("updateddate"));
				gatePassEntry.setCreatedDate(resultSet.getString("createddate"));
				gatePassEntry.setGatePassType(resultSet.getString("gatepasstype"));

				gatepassEntryList.add(gatePassEntry);
			}
			statement.close();
			resultSet.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return gatepassEntryList;
	}

	public void updateGatePassEntryCls(String gatePassEntryId, double clnumber, String clType) {
		try {
			connection = ConnectionHelper.getConnection();
			statement = connection.createStatement();
			statement.executeUpdate("update gatepassentry set clnumber='" + clnumber + "',cltype='" + clType
					+ "' where gatepassentry_id='" + gatePassEntryId + "'");
			statement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<GatePassEntry> gateUpdatedPassEntryApprovedList(String fromDate, String toDate) {
		List<GatePassEntry> gatepassEntryList = new ArrayList<GatePassEntry>();
		try {
			connection = ConnectionHelper.getConnection();
			statement = connection.createStatement();
			String selectquery = "select *  from gatepassentry where createddate>='"+fromDate+"' and createddate<='"+toDate+"' and clnumber!=''  and cltype!=''  order by staffname";
			ResultSet resultSet = statement.executeQuery(selectquery);
			while (resultSet.next()) {
				GatePassEntry gatePassEntry = new GatePassEntry();
				gatePassEntry.setStaffId(resultSet.getString("staffid"));
				gatePassEntry.setStaffName(resultSet.getString("staffname"));
				gatePassEntry.setDepartment(resultSet.getString("department"));
				gatePassEntry.setClnumber(resultSet.getDouble("clnumber"));
				gatePassEntry.setCreatedDate(resultSet.getString("createdDate"));
				gatePassEntry.setInTime(resultSet.getString("intime"));
				gatePassEntry.setOuttime(resultSet.getString("outtime"));
				gatePassEntry.setClType(resultSet.getString("cltype"));
				gatePassEntry.setGatePassType(resultSet.getString("gatepasstype"));
				gatepassEntryList.add(gatePassEntry);
			}
			statement.close();
			resultSet.close();
			connection.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return gatepassEntryList;
	}
}
