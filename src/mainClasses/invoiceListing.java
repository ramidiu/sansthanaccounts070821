package mainClasses;


import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.invoice;
public class invoiceListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String inv_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setinv_id(String inv_id)
{
this.inv_id = inv_id;
}
public String getinv_id(){
return(this.inv_id);
}
public List getinvoice()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT inv_id,headAccountId,vendorid,amount,paid,pendingamount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,emp_id FROM invoice "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new invoice(rs.getString("inv_id"),rs.getString("headAccountId"),rs.getString("vendorid"),rs.getString("amount"),rs.getString("paid"),rs.getString("pendingamount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalInvoicesAmount(String vendorId)
{
	String totAmt="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as total FROM invoice where  vendorId = '"+vendorId+"'";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totAmt=rs.getString("total");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totAmt;
}




public String getTotalInvoicesAmount(String vendorId, String date1, String date2)
{
	System.out.println("date1.."+date1+".."+date2);
	String totAmt="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement(); 
String selectquery="SELECT sum(amount) as total FROM invoice where date(createdBy) between '"+date1+"' and '"+date2+"' and vendorId = '"+vendorId+"'";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totAmt=rs.getString("total");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totAmt;
}
public List getMinvoice(String inv_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT inv_id,headAccountId,vendorid,amount,paid,pendingamount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,emp_id FROM invoice where  inv_id = '"+inv_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new invoice(rs.getString("inv_id"),rs.getString("headAccountId"),rs.getString("vendorid"),rs.getString("amount"),rs.getString("paid"),rs.getString("pendingamount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMinvoiceAmount(String inv_id)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT inv_id,headAccountId,vendorid,amount,paid,pendingamount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,emp_id FROM invoice where  inv_id = '"+inv_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("amount");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMinvoiceList(String inv_id,String head_id,String vendorid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT inv_id,headAccountId,vendorid,amount,paid,pendingamount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,emp_id FROM invoice where  inv_id like '%"+inv_id+"%' and headAccountId ='"+head_id+"'  and vendorid ='"+vendorid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new invoice(rs.getString("inv_id"),rs.getString("headAccountId"),rs.getString("vendorid"),rs.getString("amount"),rs.getString("paid"),rs.getString("pendingamount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMNOTAdminApprovedinvoiceList(String vendorId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select * from invoice where invoice.inv_id NOT IN (select extra5 from productexpenses) and vendorid ='"+vendorId+"' and vendorid !=''";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new invoice(rs.getString("inv_id"),rs.getString("headAccountId"),rs.getString("vendorid"),rs.getString("amount"),rs.getString("paid"),rs.getString("pendingamount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getTotalOtherChargesAmount(String vendorId)
{
	String totAmt="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(extra5) as total FROM invoice where  vendorId = '"+vendorId+"'";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totAmt=rs.getString("total");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totAmt;
}

public List getMNOTAdminApprovedinvoiceList(String vendorId,String date1, String date2)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select * from invoice where invoice.inv_id NOT IN (select extra5 from productexpenses where date(entry_date) between '"+date1+"' and '"+date2+"') and vendorid ='"+vendorId+"' and vendorid !='' and date(createdBy) between '"+date1+"' and '"+date2+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new invoice(rs.getString("inv_id"),rs.getString("headAccountId"),rs.getString("vendorid"),rs.getString("amount"),rs.getString("paid"),rs.getString("pendingamount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}