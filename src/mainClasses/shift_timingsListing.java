package mainClasses;
import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.*;
public class shift_timingsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String shtime_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setshtime_id(String shtime_id)
{
this.shtime_id = shtime_id;
}
public String getshtime_id(){
return(this.shtime_id);
}
public List getshift_timings()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT shtime_id,shift_name,shift_starttitming,shift_endtiming,shift_code,extra2,extra3 FROM shift_timings "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shift_timings(rs.getString("shtime_id"),rs.getString("shift_name"),rs.getString("shift_starttitming"),rs.getString("shift_endtiming"),rs.getString("shift_code"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMshift_timings(String shtime_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT shtime_id,shift_name,shift_starttitming,shift_endtiming,shift_code,extra2,extra3 FROM shift_timings where  shtime_id = '"+shtime_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shift_timings(rs.getString("shtime_id"),rs.getString("shift_name"),rs.getString("shift_starttitming"),rs.getString("shift_endtiming"),rs.getString("shift_code"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String  getTimings(String shift_code)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT shift_starttitming,shift_endtiming FROM shift_timings where  shift_code = '"+shift_code+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("shift_starttitming")+"To"+rs.getString("shift_endtiming");

}
s.close();
rs.close();
//c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}