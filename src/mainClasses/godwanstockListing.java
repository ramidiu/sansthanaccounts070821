package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.godwanstock;
import beans.godwanstock_scService;
import dbase.sqlcon.ConnectionHelper;
public class godwanstockListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String gsId;
ArrayList<?> list = new ArrayList<Object>();
Connection c = null;
Statement s = null;
 public void setgsId(String gsId)
{
this.gsId = gsId;
}
public String getgsId(){
return(this.gsId);
}
public List<godwanstock> getgodwanstock()
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department FROM godwanstock "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockBasedOnFromDateToDateAndHOA(String fromDate,String toDate,String hoa)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,extra16,extra21,payment_status FROM godwanstock where date>='"+fromDate+"%' and date<='"+toDate+"%' and department='"+hoa+"' and billNo not like 'OFK%'"; 

System.out.println("gd selectquery::::=====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("extra16"),rs.getString("extra21"),rs.getString("payment_status")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getMSEbyVendorByDate(String department,String date )
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,productId,vendorId, description, quantity, purchaseRate, date, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15, sum(quantity*purchaseRate) as MRNo  FROM godwanstock  where date<'"+date+"' and vendorId!='1001' and  department='"+department+"'  group by vendorId ";
//System.out.println("vendorlist===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getMSEbyVendorBtwDates(String department,String fromdate,String todate)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,productId,vendorId, description, quantity, purchaseRate, date, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15, sum(quantity*purchaseRate) as MRNo  FROM godwanstock  where date >= '"+fromdate+"' and date <= '"+todate+"' and vendorId!='1001' and  department='"+department+"'  group by vendorId ";
//System.out.println("vendorlist===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
//System.out.println("rs.getString(MRNo) "+rs.getString("MRNo"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getMSEbyVendorBtwDates1(String department,String fromdate,String todate)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
ArrayList<String> vendorId  = new ArrayList<String>();
ArrayList<String> total = new ArrayList<String>();
String vendorid="";
double amount = 0.00; 
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//System.out.println("===========================================================================================");
String selectquery="SELECT vendorId  FROM godwanstock  where date >= '"+fromdate+"' and date <= '"+todate+"' and vendorId!='1001' and vendorId!='1095' and vendorId != '1201' and vendorId!='1208' and vendorId!='1165' and vendorId!='1164' and vendorId!='1153' and vendorId!='' and  department='"+department+"'  group by vendorId ";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	vendorId.add(rs.getString("vendorId"));
}
//System.out.println("SIZE ------ > "+vendorId.size());
for(int i = 0; i< vendorId.size(); i++){
//	System.out.println(i+1);
String query = "SELECT vendorId, extra16,extra1  FROM godwanstock  where date >= '"+fromdate+"' and date <= '"+todate+"' and  department='"+department+"' and vendorId ='"+vendorId.get(i)+"' group by extra1"; 
//System.out.println("vendor,....."+query);
rs = s.executeQuery(query);
amount = 0.00;
vendorid="";
while(rs.next()){
	vendorid = rs.getString("vendorId");
//	System.out.println(vendorid +" ----- >  "+ rs.getString("extra1"));
	amount = amount + (Double.parseDouble(rs.getString("extra16")));
	if(vendorid.equals("1435")){
		System.out.println("vendor  amount,....."+amount);
	}
}

list.add(new godwanstock(vendorid.trim(),String.valueOf(amount)));
//System.out.println(vendorid +" ---- > "+ amount);
}
//System.out.println("INVOICE -----> "+ list.toString());
//System.out.println("===============================================================================================");
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getgodwanstockbillApprovePending(String status,String hoaid)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
	String con="";
	if(hoaid!=null && !hoaid.equals("")){
		con=" and department='"+hoaid+"'";
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(quantity*(purchaseRate*(1+vat/100))) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra17,extra18,extra19,extra20 FROM godwanstock where extra1!='INV99' and approval_status='"+status+"'"+con+" and extra1 not in (select extra5 from productexpenses where extra5!='Payment Done')   group by extra1"; 
//System.out.println("godwanstocklisting===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next())
{
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getgodwanstockbillApprovePending1(String status,String hoaid)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
	String con="";
	if(hoaid!=null && !hoaid.equals("")){
		con=" and department='"+hoaid+"'";
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(quantity*(purchaseRate+extra18+extra19+extra20)) as purchaseRate,date,(purchaseRate+extra18+extra19+extra20) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra17,extra18,extra19,extra20 FROM godwanstock where extra1!='INV99'  and billNo not like 'OFK%' and approval_status='"+status+"'"+con+" and extra1 not in (select extra5 from productexpenses where extra5!='Payment Done')   group by extra1";
/* System.out.println("godwanstocklisting:::===>"+selectquery); */
ResultSet rs = s.executeQuery(selectquery);
while(rs.next())
{
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List<godwanstock> getgodwanstockbillApprovePendingBasedOnDepartment(String status,String finYear,String department,String superadminId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
String subquery="";
String subquery1="";
String fiYear[]=finYear.split("-");
System.out.println("fiYear...."+fiYear[0]+"sdfgn"+fiYear[1]);
if(!department.equals("")){
	//subquery="and idapp.admin_status='approved' and idapp.admin_id='"+superadminId+"' and  gs.department='"+department+"' ";
	if(department.equals("1"))
	{
		/*subquery="and idapp.admin_status='approved' and (gs.department='"+department+"'  and   mrno !='Medicine') ";*/
		/*subquery="and idapp.admin_status='approved' and (gs.department='"+department+"'  and   mrno !='Medicine' and mrno !='Education' and mrno !='Library') ";*/
		subquery="and idapp.admin_status='approved' and (gs.department='"+department+"' and gs.majorhead_id !='58' and  gs.majorhead_id !='59' and  gs.majorhead_id !='60' and gs.majorhead_id !='61' and   mrno !='Medicine' and mrno !='Education' and mrno !='Library') ";
	}
	else if(department.equals("3")){
		/*subquery="and idapp.admin_status='approved' and (gs.department='"+department+"' ||  mrno ='Medicine') ";*/
		/*subquery="and idapp.admin_status='approved' and (gs.department='"+department+"' ||  mrno ='Medicine' ||  mrno ='Education' ||  mrno ='Library') ";*/
		subquery="and idapp.admin_status='approved' and (gs.department='"+department+"' ||  gs.majorhead_id='58' ||  gs.majorhead_id='59' ||  gs.majorhead_id='60' || gs.majorhead_id='61' ||  mrno ='Medicine' ||  mrno ='Education' ||  mrno ='Library') ";
	}
	else
	{
		subquery="and idapp.admin_status='approved'  and  gs.department='"+department+"' ";
	}
	

}
if(!department.equals("")){
	if(department.equals("1"))
	{
		/*subquery1="and ( gs.department='"+department+"' and mrno !='Medicine')";*/
		/*subquery1="and ( gs.department='"+department+"' and mrno !='Medicine' and mrno !='Education' and mrno !='Library')";*/
		subquery1="and ( gs.department='"+department+"'and gs.majorhead_id !='58' and  gs.majorhead_id !='59' and  gs.majorhead_id !='60' and gs.majorhead_id !='61' and mrno !='Medicine' and mrno !='Education' and mrno !='Library')";
	}
	else if(department.equals("3")){
		/*subquery1="and ( gs.department='"+department+"' || mrno ='Medicine')";*/
		/*subquery1="and ( gs.department='"+department+"' || mrno ='Medicine' ||  mrno ='Education' ||  mrno ='Library')";*/
		subquery1="and ( gs.department='"+department+"' ||  gs.majorhead_id='58' ||  gs.majorhead_id='59' ||  gs.majorhead_id='60' || gs.majorhead_id='61' || mrno ='Medicine' ||  mrno ='Education' ||  mrno ='Library')";
	}
	else
	{
	subquery1="and  gs.department='"+department+"'";
	}
}
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="";

if(status.equals("Not-Approve")){
	selectquery="SELECT gs.gsId,gs.vendorId,gs.productId,gs.description,gs.quantity,sum(gs.quantity*(gs.purchaseRate*(1+gs.vat/100))) as purchaseRate,gs.date,(gs.purchaseRate*(1+gs.vat/100)) as MRNo,gs.billNo,gs.extra1,gs.extra2,gs.extra3,gs.extra4,gs.extra5,gs.vat,gs.vat1,gs.profcharges,gs.emp_id,gs.department,gs.checkedby, gs.checkedtime, gs.approval_status, gs.extra6, gs.extra7, gs.extra8,gs.extra9, gs.extra10 FROM godwanstock gs,indentapprovals idapp  where gs.profcharges='checked' and gs.date>='"+fiYear[0]+"-04-01 00:00:00' and gs.date<'"+fiYear[1]+"03-31 23:59:59'  and gs.approval_status!='Approved' and gs.vendorId!='1208' and  gs.extra1!=idapp.indentinvoice_id "+subquery+" group by gs.extra1 order by gs.date desc";
}else if(status.equals("Approved")){
	selectquery="SELECT gs.gsId,gs.vendorId,gs.productId,gs.description,gs.quantity,sum(gs.quantity*(gs.purchaseRate*(1+gs.vat/100))) as purchaseRate,gs.date,(gs.purchaseRate*(1+gs.vat/100)) as MRNo,gs.billNo,gs.extra1,gs.extra2,gs.extra3,gs.extra4,gs.extra5,gs.vat,gs.vat1,gs.profcharges,gs.emp_id,gs.department,gs.checkedby, gs.checkedtime, gs.approval_status, gs.extra6, gs.extra7, gs.extra8, gs.extra9, gs.extra10 FROM godwanstock gs,indentapprovals idapp where gs.profcharges='checked' and gs.date>='"+fiYear[0]+"-04-01 00:00:00' and gs.date<'"+fiYear[1]+"03-31 23:59:59'   and gs.approval_status='Approved' and gs.vendorId!='1208' and  gs.extra1=idapp.indentinvoice_id "+subquery+" group by gs.extra1 order by  gs.date desc";
}
else if(status.equals("billdisapproved")){
	selectquery="SELECT gs.gsId,gs.vendorId,gs.productId,gs.description,gs.quantity,sum(gs.quantity*(gs.purchaseRate*(1+gs.vat/100))) as purchaseRate,gs.date,(gs.purchaseRate*(1+gs.vat/100)) as MRNo,gs.billNo,gs.extra1,gs.extra2,gs.extra3,gs.extra4,gs.extra5,gs.vat,gs.vat1,gs.profcharges,gs.emp_id,gs.department,gs.checkedby, gs.checkedtime, gs.approval_status, gs.extra6, gs.extra7, gs.extra8, gs.extra9, gs.extra10 FROM godwanstock gs,indentapprovals idapp where gs.profcharges='checked' and gs.date>='"+fiYear[0]+"-04-01 00:00:00' and gs.date<'"+fiYear[1]+"03-31 23:59:59' and gs.approval_status='Not-Approve' and gs.vendorId!='1208' and  gs.extra1=idapp.indentinvoice_id and idapp.admin_status='billdisapproved' "+subquery1+" group by gs.extra1 order by  gs.date desc";
}
else{
	selectquery="SELECT gs.gsId,gs.vendorId,gs.productId,gs.description,gs.quantity,sum(gs.quantity*(gs.purchaseRate*(1+gs.vat/100))) as purchaseRate,gs.date,(gs.purchaseRate*(1+gs.vat/100)) as MRNo,gs.billNo,gs.extra1,gs.extra2,gs.extra3,gs.extra4,gs.extra5,gs.vat,gs.vat1,gs.profcharges,gs.emp_id,gs.department,gs.checkedby, gs.checkedtime, gs.approval_status, gs.extra6, gs.extra7, gs.extra8, gs.extra9, gs.extra10 FROM godwanstock gs,indentapprovals idapp where gs.profcharges='checked' and gs.date>='"+fiYear[0]+"-04-01 00:00:00' and gs.date<'"+fiYear[1]+"03-31 23:59:59' and gs.approval_status='Not-Approve' and gs.vendorId!='1208' and gs.extra1=idapp.indentinvoice_id "+subquery1+" group by gs.extra1 order by  gs.date desc";
	//System.out.println(selectquery);
}
System.out.println("test:::"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getgodwanstockbillApprovePendingBasedOnDepartmentBtwDates(String status,String department,String superadminId,String fdate,String tdate)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
String subquery="";
String subquery1="";
String subquery2="";

if(!department.equals("")){
	//subquery="and idapp.admin_status='approved' and idapp.admin_id='"+superadminId+"' and  gs.department='"+department+"' ";
	
	
	if(department.equals("1"))
	{
		/*subquery="and idapp.admin_status='approved' and (gs.department='"+department+"'  and   mrno !='Medicine') ";*/
		subquery="and idapp.admin_status='approved' and (gs.department='"+department+"'  and   mrno !='Medicine' and mrno !='Education' and mrno !='Library') ";
	}
	else if(department.equals("3")){
		/*subquery="and idapp.admin_status='approved' and (gs.department='"+department+"' ||  mrno ='Medicine') ";*/
		subquery="and idapp.admin_status='approved' and (gs.department='"+department+"' ||  mrno ='Medicine' ||  mrno ='Education' ||  mrno ='Library') ";
	}
	else
	{
		subquery="and idapp.admin_status='approved'  and  gs.department='"+department+"' ";
	}
	

}
if(!department.equals("")){
	if(department.equals("1"))
	{
		/*subquery1="and ( gs.department='"+department+"' and mrno !='Medicine')";*/
		subquery1="and ( gs.department='"+department+"' and mrno !='Medicine' and mrno !='Education' and mrno !='Library')";
	}
	else if(department.equals("3")){
		/*subquery1="and ( gs.department='"+department+"' || mrno ='Medicine')";*/
		subquery1="and ( gs.department='"+department+"' || mrno ='Medicine' ||  mrno ='Education' ||  mrno ='Library')";
	}
	else
	{
	subquery1="and  gs.department='"+department+"'";
	}
}

if(fdate != null && !fdate.equals("") && tdate != null && !tdate.equals(""))
{
	subquery2 = " and gs.date >= '"+fdate+"' and gs.date <= '"+tdate+"'";
}

try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="";

if(status.equals("Not-Approve")){
	selectquery="SELECT gs.gsId,gs.vendorId,gs.productId,gs.description,gs.quantity,sum(gs.quantity*(gs.purchaseRate*(1+gs.vat/100))) as purchaseRate,gs.date,(gs.purchaseRate*(1+gs.vat/100)) as MRNo,gs.billNo,gs.extra1,gs.extra2,gs.extra3,gs.extra4,gs.extra5,gs.vat,gs.vat1,gs.profcharges,gs.emp_id,gs.department,gs.checkedby, gs.checkedtime, gs.approval_status, gs.extra6, gs.extra7, gs.extra8,gs.extra9, gs.extra10 FROM godwanstock gs,indentapprovals idapp  where gs.profcharges='checked'  and gs.approval_status!='Approved' and gs.vendorId!='1208' and  gs.extra1!=idapp.indentinvoice_id "+subquery+subquery2+" group by gs.extra1 order by gs.date desc";
}else if(status.equals("Approved")){
	selectquery="SELECT gs.gsId,gs.vendorId,gs.productId,gs.description,gs.quantity,sum(gs.quantity*(gs.purchaseRate*(1+gs.vat/100))) as purchaseRate,gs.date,(gs.purchaseRate*(1+gs.vat/100)) as MRNo,gs.billNo,gs.extra1,gs.extra2,gs.extra3,gs.extra4,gs.extra5,gs.vat,gs.vat1,gs.profcharges,gs.emp_id,gs.department,gs.checkedby, gs.checkedtime, gs.approval_status, gs.extra6, gs.extra7, gs.extra8, gs.extra9, gs.extra10 FROM godwanstock gs,indentapprovals idapp where gs.profcharges='checked'  and gs.approval_status='Approved' and gs.vendorId!='1208' and  gs.extra1=idapp.indentinvoice_id "+subquery+subquery2+" group by gs.extra1 order by  gs.date desc";
}
else if(status.equals("billdisapproved")){
	selectquery="SELECT gs.gsId,gs.vendorId,gs.productId,gs.description,gs.quantity,sum(gs.quantity*(gs.purchaseRate*(1+gs.vat/100))) as purchaseRate,gs.date,(gs.purchaseRate*(1+gs.vat/100)) as MRNo,gs.billNo,gs.extra1,gs.extra2,gs.extra3,gs.extra4,gs.extra5,gs.vat,gs.vat1,gs.profcharges,gs.emp_id,gs.department,gs.checkedby, gs.checkedtime, gs.approval_status, gs.extra6, gs.extra7, gs.extra8, gs.extra9, gs.extra10 FROM godwanstock gs,indentapprovals idapp where gs.profcharges='checked'  and gs.approval_status='Not-Approve' and gs.vendorId!='1208' and  gs.extra1=idapp.indentinvoice_id and idapp.admin_status='billdisapproved' "+subquery1+subquery2+" group by gs.extra1 order by  gs.date desc";
}
else{
	selectquery="SELECT gs.gsId,gs.vendorId,gs.productId,gs.description,gs.quantity,sum(gs.quantity*(gs.purchaseRate*(1+gs.vat/100))) as purchaseRate,gs.date,(gs.purchaseRate*(1+gs.vat/100)) as MRNo,gs.billNo,gs.extra1,gs.extra2,gs.extra3,gs.extra4,gs.extra5,gs.vat,gs.vat1,gs.profcharges,gs.emp_id,gs.department,gs.checkedby, gs.checkedtime, gs.approval_status, gs.extra6, gs.extra7, gs.extra8, gs.extra9, gs.extra10 FROM godwanstock gs,indentapprovals idapp where gs.profcharges='checked'  and gs.approval_status='Not-Approve' and gs.vendorId!='1208' and gs.extra1=idapp.indentinvoice_id "+subquery1+subquery2+" group by gs.extra1 order by  gs.date desc";
	//System.out.println(selectquery);
}
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getgodwanbillApprove(String adminid)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM godwanstock where extra1 not in(select indentinvoice_id from indentapprovals where admin_id='"+adminid+"') group by extra1"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public boolean validProductOfSecondOpeningBalance(String productid)
{
boolean list =false;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM godwanstock where productid='"+productid+"' and extra1='INV99' and department='3'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=true;
 }
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getgodwanstockbillApprovePendingBasedDepartment(String status,String department)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
String depq="";
if(department!=null)
{
	depq="and department='"+department+"'";
}
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(quantity*(purchaseRate*(1+vat/100))) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where approval_status='"+status+"' "+depq+" group by extra1";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockByinvoiceId(String invoiceId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,(purchaseRate*(1+vat/100)) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate*(1+vat/100))) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra17,extra18,extra19,extra20,extra21,extra25 FROM godwanstock where extra1='"+invoiceId+"' ";
System.out.println("getgodwanstock ========> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));
	godwanstock godwanstock = new godwanstock();
	godwanstock.setgsId(rs.getString("gsId"));
	godwanstock.setvendorId(rs.getString("vendorId"));
	godwanstock.setproductId(rs.getString("productId"));
	godwanstock.setdescription("description");
	godwanstock.setquantity(rs.getString("quantity"));
	godwanstock.setpurchaseRate(rs.getString("purchaseRate"));
	godwanstock.setdate(rs.getString("date"));
	godwanstock.setMRNo(rs.getString("MRNo"));
	godwanstock.setbillNo(rs.getString("billNo"));
	godwanstock.setextra1(rs.getString("extra1"));
	godwanstock.setextra2(rs.getString("extra2"));
	godwanstock.setextra3(rs.getString("extra3"));
	godwanstock.setextra4(rs.getString("extra4"));
	godwanstock.setextra5(rs.getString("extra5"));
	godwanstock.setvat(rs.getString("vat"));
	godwanstock.setvat1(rs.getString("vat1"));
	godwanstock.setprofcharges(rs.getString("profcharges"));
	godwanstock.setemp_id(rs.getString("emp_id"));
	godwanstock.setdepartment(rs.getString("department"));
	godwanstock.setCheckedby(rs.getString("checkedby"));
	godwanstock.setCheckedtime(rs.getString("checkedtime"));
	godwanstock.setApproval_status(rs.getString("approval_status"));
	godwanstock.setExtra6(rs.getString("extra6"));
	godwanstock.setExtra7(rs.getString("extra7"));
	godwanstock.setExtra8(rs.getString("extra8"));
	godwanstock.setExtra9(rs.getString("extra9"));
	godwanstock.setExtra10(rs.getString("extra10"));
	godwanstock.setPo_approved_status(rs.getString("po_approved_status"));
	godwanstock.setPo_approved_by(rs.getString("po_approved_by"));
	godwanstock.setPo_approved_date(rs.getString("po_approved_date"));
	godwanstock.setBill_status(rs.getString("bill_status"));
	godwanstock.setBill_update_by(rs.getString("bill_update_by"));
	godwanstock.setBill_update_time(rs.getString("bill_update_time"));
	godwanstock.setExtra11(rs.getString("extra11"));
	godwanstock.setExtra12(rs.getString("extra12"));
	godwanstock.setExtra13(rs.getString("extra13"));
	godwanstock.setExtra14(rs.getString("extra14"));
	godwanstock.setExtra15(rs.getString("extra15"));
	/*godwanstock.setExtra16(rs.getString("extra16"));*/
	godwanstock.setExtra17(rs.getString("extra17"));
	godwanstock.setExtra18(rs.getString("extra18"));
	godwanstock.setExtra19(rs.getString("extra19"));
	godwanstock.setExtra20(rs.getString("extra20"));
	godwanstock.setExtra21(rs.getString("extra21"));
	godwanstock.setExtra25(rs.getString("extra25")); // added by gowri shankar for getting dc number
	list.add(godwanstock);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}





public List<godwanstock> getgodwanstockByExpoinvoiceId(String ExpinvoiceId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,(purchaseRate*(1+vat/100)) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate*(1+vat/100))) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra17,extra18,extra19,extra20,extra21,extra25 FROM godwanstock where extra21='"+ExpinvoiceId+"' ";
System.out.println("getgodwanstock ========> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));
	godwanstock godwanstock = new godwanstock();
	godwanstock.setgsId(rs.getString("gsId"));
	godwanstock.setvendorId(rs.getString("vendorId"));
	godwanstock.setproductId(rs.getString("productId"));
	godwanstock.setdescription("description");
	godwanstock.setquantity(rs.getString("quantity"));
	godwanstock.setpurchaseRate(rs.getString("purchaseRate"));
	godwanstock.setdate(rs.getString("date"));
	godwanstock.setMRNo(rs.getString("MRNo"));
	godwanstock.setbillNo(rs.getString("billNo"));
	godwanstock.setextra1(rs.getString("extra1"));
	godwanstock.setextra2(rs.getString("extra2"));
	godwanstock.setextra3(rs.getString("extra3"));
	godwanstock.setextra4(rs.getString("extra4"));
	godwanstock.setextra5(rs.getString("extra5"));
	godwanstock.setvat(rs.getString("vat"));
	godwanstock.setvat1(rs.getString("vat1"));
	godwanstock.setprofcharges(rs.getString("profcharges"));
	godwanstock.setemp_id(rs.getString("emp_id"));
	godwanstock.setdepartment(rs.getString("department"));
	godwanstock.setCheckedby(rs.getString("checkedby"));
	godwanstock.setCheckedtime(rs.getString("checkedtime"));
	godwanstock.setApproval_status(rs.getString("approval_status"));
	godwanstock.setExtra6(rs.getString("extra6"));
	godwanstock.setExtra7(rs.getString("extra7"));
	godwanstock.setExtra8(rs.getString("extra8"));
	godwanstock.setExtra9(rs.getString("extra9"));
	godwanstock.setExtra10(rs.getString("extra10"));
	godwanstock.setPo_approved_status(rs.getString("po_approved_status"));
	godwanstock.setPo_approved_by(rs.getString("po_approved_by"));
	godwanstock.setPo_approved_date(rs.getString("po_approved_date"));
	godwanstock.setBill_status(rs.getString("bill_status"));
	godwanstock.setBill_update_by(rs.getString("bill_update_by"));
	godwanstock.setBill_update_time(rs.getString("bill_update_time"));
	godwanstock.setExtra11(rs.getString("extra11"));
	godwanstock.setExtra12(rs.getString("extra12"));
	godwanstock.setExtra13(rs.getString("extra13"));
	godwanstock.setExtra14(rs.getString("extra14"));
	godwanstock.setExtra15(rs.getString("extra15"));
	/*godwanstock.setExtra16(rs.getString("extra16"));*/
	godwanstock.setExtra17(rs.getString("extra17"));
	godwanstock.setExtra18(rs.getString("extra18"));
	godwanstock.setExtra19(rs.getString("extra19"));
	godwanstock.setExtra20(rs.getString("extra20"));
	godwanstock.setExtra21(rs.getString("extra21"));
	godwanstock.setExtra25(rs.getString("extra25")); // added by gowri shankar for getting dc number
	list.add(godwanstock);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}





























public List<godwanstock> getgodwanstockByinvoiceId1(String invoiceId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,(purchaseRate) as purchaseRate,date,(purchaseRate) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate+extra18+extra19+extra20)) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra17,extra18,extra19,extra20 FROM godwanstock where extra1='"+invoiceId+"' ";
System.out.println("getgodwanstock ========> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockUnapproved(String po)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department FROM godwanstock where profcharges='unchecked' and extra1='"+po+"'"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockUnapprovedByPO1(String po)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,((purchaseRate+extra18+extra19+extra20)*quantity) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,extra17,extra18,extra19,extra20 FROM godwanstock where profcharges='UncheckedByPO' and extra1='"+po+"'"; 
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getgodwanstockUnapprovedByPO(String po)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,((purchaseRate*(1+vat/100))*quantity) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,extra17,extra18,extra19,extra20 FROM godwanstock where profcharges='UncheckedByPO' and extra1='"+po+"'"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockAllApproved(String po)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,((purchaseRate*(1+vat/100))*quantity) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where  extra1='"+po+"'"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockUnapprovedList(String department)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
	String con="";
	if(department.equals("3") || department.equals("5") ){
		con="and g.department='"+department+"' group by g.extra1";
	}else if(department.equals("4") || department.equals("1")){
		con="and (g.department='1' || g.department='4') group by g.extra1";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM godwanstock g,vendors v,products pr where g.profcharges='unchecked' and v.vendorId=g.vendorId  and pr.productId=g.productId and pr.major_head_id!='60' "+con; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra25")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getMedicalUnapprovedList(String department)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
	String con="";
	if(department.equals("3") || department.equals("5") ){
		con="and department='"+department+"' group by extra1";
	}else if(department.equals("1")){
		con="and (g.department='1') group by g.extra1";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM godwanstock g,vendors v,products pr where g.profcharges='unchecked' and v.vendorId=g.vendorId  and pr.productId=g.productId and pr.major_head_id='60' "+con; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockUnapprovedListByPO(String department)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
	String con="";
	if(department.equals("3") || department.equals("5") ){
		con="and department='"+department+"' ";
	}else if(department.equals("4")){
		con="and (department='1' || department='4') ";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,g.vendorId,productId,v.firstname as description,quantity,purchaseRate,date,MRNo,billNo,g.extra1,g.extra2,g.extra3,g.extra4,g.extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock g,vendors v where profcharges='UncheckedByPO' and v.vendorId=g.vendorId "+con+" group by extra1"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockUnapprovedListByPO1(String department)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
	String con="";
	if(department.equals("3") || department.equals("5") ){
		con="and department='"+department+"' ";
	}else if(department.equals("4")){
		con="and (department='1' || department='4') ";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,g.vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,g.extra1,g.extra2,g.extra3,g.extra4,g.extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock g where profcharges='UncheckedByPO'  "+con+" group by extra1"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getgodwanstockBefore(String month,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.date < '"+month+"-%' and g.productid='"+productid+"'";
System.out.println("1111======="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getgodwanstockbymonth(String month,String productid)
{
String list = "";
//System.out.println("111111");
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.date like '"+month+"-%'and g.productid='"+productid+"'";
//System.out.println("getgodwanstockbymonth===========>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getgodwanstockbyDate(String fromDate,String toDate,String productId)
{
String list = "";
//System.out.println("111111");
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.date between '"+fromDate+"' and '"+toDate+"' and g.productid='"+productId+"'";
System.out.println("inward:::"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String getgodwanstockbyENDDate(String fromDate,String toDate,String productId)
{
String list = "";
//System.out.println("111111");
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.date between '"+fromDate+"' and '"+toDate+"' and g.productid='"+productId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String getgodwanstockbyDateMedicalStock1(String fromDate,String toDate,String productId)
{
String list = "";
//System.out.println("111111");
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.extra5='' and g.date between '"+fromDate+"' and '"+toDate+"' and g.productid='"+productId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getgodwanstockbyDateMedicalStock2(String fromDate,String toDate,String productId)
{
String list = "";
//System.out.println("111111");
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(extra5) as quantity FROM godwanstock g where g.extra5!='' and g.date between '"+fromDate+"' and '"+toDate+"' and g.productid='"+productId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String[] getgodwanstockShopStockOpeningBal(String fromDate,String toDate,String productId)
{
String[] value=new String[2];
//System.out.println("111111");
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select sum(gd.quantity) as godwanQuantity,sum(s.quantity) as shopStockQuantity from godwanstock gd,shopstock s where gd.productId=s.productId and s.forwardedDate between '"+fromDate+"' and '"+toDate+"' and gd.date between '"+fromDate+"' and '"+toDate+"' and gd.productId='"+productId+"'";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
value[0]=rs.getString("godwanQuantity");
value[1]=rs.getString("shopStockQuantity");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return value;
}



public String getgodwanstockbymonthBasedOnvendor(String month,String vendorid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity*purchaseRate) as quantity FROM godwanstock g where g.date like '"+month+"%'and g.vendorId='"+vendorid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getgodwanstockbyDay(String day,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(quantity) as quantity FROM sansthanaccounts.godwanstock g where g.date ='"+day+"'and g.productid='"+productid+"'"; 
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.date like'"+day+"%'and g.productid='"+productid+"'";
//System.out.println("inwards godwanstock stockbyDAY ==========>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getMgodwanstock(String gsId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department FROM godwanstock where  gsId = '"+gsId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getStockListBasedOnIndentId()
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department FROM godwanstock  group by extra2 "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getBillsBasedOnIndentId(String indentinv_id)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department FROM godwanstock where extra2='"+indentinv_id+"'  group by extra1 "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getBillsBasedOnPurchaseOrderId(String indentinv_id)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby,checkedtime FROM godwanstock where extra2='"+indentinv_id+"'"; 
//sSystem.out.println("getgdstkpo--"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 godwanstock gd = new godwanstock();
 gd.setGsId(rs.getString("gsId"));
 gd.setVendorId(rs.getString("vendorId"));
 gd.setProductId(rs.getString("productId"));
 gd.setDescription(rs.getString("description"));
 gd.setQuantity(rs.getString("quantity"));
 gd.setPurchaseRate(rs.getString("purchaseRate"));
 gd.setDate(rs.getString("date"));
 gd.setMRNo(rs.getString("MRNo"));
 gd.setBillNo(rs.getString("billNo"));
 gd.setExtra1(rs.getString("extra1"));
 gd.setExtra2(rs.getString("extra2"));
 gd.setExtra3(rs.getString("extra3"));
 gd.setExtra4(rs.getString("extra4"));
 gd.setExtra5(rs.getString("extra5"));
 gd.setVat(rs.getString("vat"));
 gd.setVat1(rs.getString("vat1"));
 gd.setProfcharges(rs.getString("profcharges"));
 gd.setEmp_id(rs.getString("emp_id"));
 gd.setDepartment(rs.getString("department"));
 gd.setCheckedby(rs.getString("checkedby"));
 gd.setCheckedtime(rs.getString("checkedtime"));
 list.add(gd);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getStockDetailsBasedOnInvoice(String invId)
{
	ArrayList<godwanstock> list = new ArrayList<godwanstock>();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(quantity*purchaseRate*(1+vat/100)+extra8) as purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15 FROM godwanstock where  extra1 = '"+invId+"'";
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next() && rs.getString("gsId")!=null){
	 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
	
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		e.printStackTrace();
		System.out.println("Exception is ;"+e);
	}
	return list;
}
public List<godwanstock> getInvoiceDetails(String invId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,extra17,extra18,extra19,extra20 FROM godwanstock where  extra1 = '"+invId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getInvoiceDetailsBasedTrackingNo(String tracking_id)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where  extra9 = '"+tracking_id+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getInvoiceDetailsBasedTrackingNo1(String tracking_id)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,po_approved_by,bill_update_by,extra17,extra18,extra19,extra20 FROM godwanstock where  extra9 = '"+tracking_id+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_by"),rs.getString("bill_update_by"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getStockDetailsBasedOnInvoiceId(String invId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,extra17,extra18,extra19,extra20 FROM godwanstock where  extra1 = '"+invId+"'";
System.out.println("query-->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//System.out.println("date from godwanstock table===========>"+rs.getString("date"));
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
//System.out.println("returnin godwan stcok list");
return list;
}

public List<godwanstock> getStockVendorDetailsBasedOnDate(String fromDate,String toDate)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(purchaseRate) as price,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,extra17,sum(extra18) as CGST,sum(extra19) as SGST,sum(extra20) as IGST FROM godwanstock where  date between '"+fromDate+"' and '"+toDate+"' and extra18>='0' group by extra1";
System.out.println("query-->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("price"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra17"),rs.getString("CGST"),rs.getString("SGST"),rs.getString("IGST")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getStockVendorDetailsBasedOnDateAndHeadId(String fromDate,String toDate,String headAccountId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
/*String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(purchaseRate) as price,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,extra17,sum(extra18) as CGST,sum(extra19) as SGST,sum(extra20) as IGST FROM godwanstock where  date between '"+fromDate+"' and '"+toDate+"' and department = '"+headAccountId+"' and extra18>'0' and extra19>'0' group by extra1";*/
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(purchaseRate) as price,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10,extra17,sum(extra18) as CGST,sum(extra19) as SGST,sum(extra20) as IGST FROM godwanstock where  date between '"+fromDate+"' and '"+toDate+"' and department = '"+headAccountId+"' group by extra1";

System.out.println("query-->"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("price"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra17"),rs.getString("CGST"),rs.getString("SGST"),rs.getString("IGST")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String getPurchStockByInv(String poinv_id )
{
String list ="";
try {
 // Load the database driver
	String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();
	selectquery="SELECT quantity FROM godwanstock where  extra2 = '"+poinv_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");
}

}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public boolean getPurchStockStauts(String poinv_id )
{
boolean list =false;
try {
 // Load the database driver
	String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();
	selectquery="SELECT quantity FROM godwanstock where  extra2 = '"+poinv_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list =true;
}

}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalReceivedQuantity(String productId,String poId)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.productId ='"+productId+"'and g.extra2='"+poId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalReceivedQuantityBasedHOA(String productId,String HOA)
{
String list = "0";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.productId ='"+productId+"'and g.department='"+HOA+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null && !rs.getString("quantity").equals("")){
 list=rs.getString("quantity");
	}

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalReceivedQuantityBasedHOAOfSecondOpenBal(String productId,String HOA)
{
String list = "0";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.productId ='"+productId+"'and g.department='"+HOA+"' and extra1='INV99' and date>'2014-12-06 23:59:59' group by productId";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null && !rs.getString("quantity").equals("")){
 list=rs.getString("quantity");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalReceivedQuantityEntryHOA(String productId,String HOA)
{
String list = "0";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock g where g.productId ='"+productId+"'and g.department='"+HOA+"' and extra1!='INV99' and date>'2014-12-06 23:59:59' and profcharges='checked' group by productId";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null && !rs.getString("quantity").equals("")){
 list=rs.getString("quantity");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getPurchaseProductsForVatReport(String hoid,String fromdate,String todate,String proID,String vatPer)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String date="";
String product="";
String vat="";
if(fromdate!=null && todate!=null){
	date=" and date>='"+fromdate+"' and date<='"+todate+"'";
}
if(proID!=null && !proID.equals("")){
	product=" and productId='"+proID+"'";
}
if(vatPer!=null && !vatPer.equals("")){
	vat=" and vat='"+vatPer+"'";
}
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(purchaseRate*quantity) as purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department FROM godwanstock where quantity!='0' and department='"+hoid+"'"+date+product+vat+" group by productId,vat,date order by date"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getPurchaseProductsForVatSummaryReport(String hoid,String fromdate,String todate,String proID,String vatPer)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String date="";
String product="";
String vat="";
if(fromdate!=null && todate!=null){
	date=" and date>='"+fromdate+"' and date<='"+todate+"'";
}
if(proID!=null && !proID.equals("")){
	product=" and productId='"+proID+"'";
}
if(vatPer!=null && !vatPer.equals("")){
	vat=" and vat='"+vatPer+"'";
}
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(purchaseRate*quantity) as purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department FROM godwanstock where quantity!='0' and department='"+hoid+"'"+date+product+vat; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getMasterStockEntryReport(String hoid,String fromdate,String todate)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String date="";
if(fromdate!=null && todate!=null){
	date=" and date>='"+fromdate+"' and date<='"+todate+"'";
}
String subQuery="";
if(hoid!=null && !hoid.equals("")){
	subQuery="and department='"+hoid+"'";
}
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(purchaseRate*quantity) as purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where quantity!='0' and extra1 NOT LIKE  'OFK%' "+date+subQuery+" group by gsId"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getSansthanStoreMSE(String hoid,String fromdate,String todate)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String date="";
if(fromdate!=null && todate!=null){
	date=" and date>='"+fromdate+"' and date<='"+todate+"'";
}
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where quantity!='0' and extra7!='serviceBill' and department='"+hoid+"'"+date+" group by gsId"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getPurchasedStock(String hoid,String fromdate,String todate)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String date="";
if(fromdate!=null && todate!=null){
	date=" and date>='"+fromdate+"' and date<='"+todate+"'";
}
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where quantity!='0' and department='"+hoid+"'"+date+" group by gsId"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<String> getTotalPendingBills(String hoid,String fromdate,String todate)
{
	ArrayList<String> list = new ArrayList<String>();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String date="";
		if(fromdate!=null && todate!=null && !fromdate.equals("") && !todate.equals("")){
			date=" and gs.date>='"+fromdate+"' and gs.date<='"+todate+"'";
		}
		String selectquery="SELECT gs.extra1 as extra1  FROM godwanstock gs,productexpenses pe where gs.extra1!='INV99' and gs.department='"+hoid+"' and pe.extra1!='Payment Done'"+date+" group by gs.extra1"; 
		//System.out.println("with kot=="+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list.add(rs.getString("extra1"));
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
/** @Aditya*/
public List<String> getTotalPendingBillsWithOutKOT(String hoid,String fromdate,String todate)
{
	ArrayList<String> list = new ArrayList<String>();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String date="";
		if(fromdate!=null && todate!=null && !fromdate.equals("") && !todate.equals("")){
			date=" and gs.date>='"+fromdate+"' and gs.date<='"+todate+"'";
		}
		String selectquery="SELECT gs.extra1 as extra1  FROM godwanstock gs,productexpenses pe where gs.extra1!='INV99' and gs.extra1 NOT LIKE  'OFK%' and gs.department='"+hoid+"' and pe.extra1!='Payment Done'"+date+" group by gs.extra1"; 
		System.out.println("with kot=="+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list.add(rs.getString("extra1"));
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
//select all pending bills which is not 'KOT' 
/*public List getTotalPendingBillsNotKot(String hoid,String fromdate,String todate)
{
	ArrayList list = new ArrayList();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String date="";
		if(fromdate!=null && todate!=null && !fromdate.equals("") && !todate.equals("")){
			date=" and gs.date>='"+fromdate+"' and gs.date<='"+todate+"'";
		}
		String selectquery="SELECT gs.extra1 as extra1  FROM godwanstock gs,productexpenses pe where gs.extra1!='INV99' and pe.extra5=gs.extra1 and  gs.extra1  NOT LIKE  'OFK%' and gs.department='"+hoid+"' and pe.extra1!='Payment Done'"+date+" group by gs.extra1"; 
		System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list.add(rs.getString("extra1"));
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}*/
public ArrayList<String> getTotalPendingBillsNotKot(String hoid,String fromdate,String todate)
{
	ArrayList<String> list = new ArrayList<String>();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String date="";
		if(fromdate!=null && todate!=null && !fromdate.equals("") && !todate.equals("")){
			date=" and gs.date>='"+fromdate+"' and gs.date<='"+todate+"'";
		}
		String selectquery="SELECT gs.extra1 as extra1,gs.extra9 FROM godwanstock gs where gs.extra1!='INV99' and gs.extra1 NOT LIKE  'OFK%' and  gs.department='"+hoid+"'  and gs.extra1 not in(SELECT extra5  FROM productexpenses where head_Account_id ='"+hoid+"' group by extra5) "+date+"  group by extra1";
		//System.out.println("Inner--not kot=="+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list.add(rs.getString("extra1"));
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}

public List<String> getTotalPendingPayments(String hoid,String fromdate,String todate,ArrayList<String> Bills)
{
	ArrayList<String> list = Bills;
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String date="";
		if(fromdate!=null && todate!=null && !fromdate.equals("") && !todate.equals("")){
			date=" and gs.date>='"+fromdate+"' and gs.date<='"+todate+"'";
		}
		String selectquery="SELECT extra5 FROM productexpenses where head_Account_id ='"+hoid+"' and extra1!='Payment Done' and extra1!='journalvoucher'"+date+" group by extra5";
		//System.out.println("outer=="+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
			list.add(rs.getString("extra5"));
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	//System.out.println("list.SIZE=="+list.size());
	return list;
}
public String getReceivedQty(String poid,String productID)
{
String qty = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock where extra2='"+poid+"' and productId='"+productID+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	qty=rs.getString("quantity");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return qty;
}
public String getReceivedQty(String productID,String fromDate,String toDate)
{
String qty = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock where productId='"+productID+"' and date >='"+fromDate+"' and date<='"+toDate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	qty=rs.getString("quantity");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return qty;
}
public String getTotalReceivedQty(String productID,String fromDate)
{
String qty = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM godwanstock where extra1!='INV99' and productId='"+productID+"' and date > '2014-12-06 11:55:00' and date <'"+fromDate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	qty=rs.getString("quantity");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return qty;
}
public List<godwanstock> getgodwanstockGroupByInvoice()
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department FROM godwanstock group by extra1"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockGroupByInvoiceID(String Poid)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where extra2='"+Poid+"' group by extra1"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstockGroupByInvoiceIDWithoutIndent(String department)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
	String inquery="";
	if(!department.equals("")){
		inquery=" and department='"+department+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();

String selectquery="SELECT * FROM godwanstock  where  extra2='' "+inquery+" group by extra1"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public int getgodwanstockGroupByInvoiceIDSize(String indentid)
{
	int list =1;
	int no=0;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no  FROM godwanstock g,purchaseorder p where g.extra2=p.poinv_id and p.indentinvoice_id='"+indentid+"' group by g.extra1"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public int getgodwanstockGroupByInvoiceIDSizeWithUniqueId(String uniqueid)
{
	int list =1;
	int no=0;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no  FROM godwanstock g where g.extra9='"+uniqueid+"' group by g.extra1"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getClosedPOSize()
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM purchaseorder p,godwanstock g where g.extra2=p.poinv_id and g.productId=p.product_id and (g.quantity)>=p.quantity group by p.poinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<String> getValidateBillNum(String vendorId,String billNum)
{
ArrayList<String>  list =new ArrayList<String>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM godwanstock where vendorId='"+vendorId+"' and billNo='"+billNum+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(rs.getString("billNo"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getBillTOT(String invoiceId)
{
String  list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(quantity*(purchaseRate+extra18+extra19+extra20)*(1+vat/100)) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate*(1+vat/100))) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where extra1='"+invoiceId+"' ";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
 list=(Double.parseDouble(rs.getString("purchaseRate"))+Double.parseDouble(rs.getString("extra8")))+"";
	} else {
		list=rs.getString("purchaseRate");
	}

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getBillTOTAL(String invoiceId)
{
String  list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(quantity*(purchaseRate+extra18+extra19+extra20)) as purchaseRate,date,(purchaseRate+extra18+extra19+extra20) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate+extra18+extra19+extra20)) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where extra1='"+invoiceId+"' ";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
 list=(Double.parseDouble(rs.getString("purchaseRate"))+Double.parseDouble(rs.getString("extra8")))+"";
 System.out.println("list....1..."+list);
	} else {
		list=rs.getString("purchaseRate");
		 System.out.println("list....2..."+list);
	}

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getGodwannoBasedUniqueno(String id)
{
String  list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra1 FROM godwanstock where extra9='"+id+"'";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
		list=rs.getString("extra1");


}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getGodwannoBasedUniquenoOrMSEno(String id,String Billtype)
{
ArrayList<godwanstock>  list = new ArrayList<godwanstock>();
try {
 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String InnerQuery="";
	if(Billtype.equals("MSE")){
		InnerQuery="where extra1='"+id+"'  and profcharges='checked'  ";
	} else if(Billtype.equals("UniqueId")){
		InnerQuery="where extra9='"+id+"' and profcharges='checked' ";
	}

String selectquery="SELECT gsId,vendorId,productId,description,quantity,purchaseRate,date,((purchaseRate*(1+vat/100))*quantity) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock "+InnerQuery+" group by extra1";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getgodwanstock(String status,String department,String date,String datetype)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
String subquery="";
String subquery1="";
String stausquery="";
if(!status.equals("") && status.equals("pending")){
	stausquery="and approval_status='Not-Approve'";
}
else {
	
}
if(!department.equals("")){
	subquery1="and  department='"+department+"'";
}
if(!date.equals("") && !datetype.equals("") && datetype.equals("Before")){
	subquery=" and checkedtime<'"+date+"' ";
}
else if(!date.equals("") && !datetype.equals("Uptonow")){
	subquery=" and checkedtime<='"+date+"' ";
}
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="";


	selectquery="SELECT * FROM godwanstock where profcharges='checked' "+subquery+stausquery+" group by extra1"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getBillNumber(String invoiceId)
{
String list = "";
try {
// Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,(purchaseRate*(1+vat/100)) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate*(1+vat/100))) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where extra1='"+invoiceId+"' ";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
list=rs.getString("billNo");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock_scService> getgodwanstockListBetweenDates(String fromdate,String todate)
{
ArrayList<godwanstock_scService> list = new ArrayList<godwanstock_scService>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select gsId, vendorId, productId, description, quantity, purchaseRate, date, MRNo, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15, majorhead_id, minorhead_id, payment_status, actualentry_date, extra16, extra17, extra18, extra19, extra20 from godwanstock where (date between '"+fromdate+"' and '"+todate+"') and payment_status!='Payment Done'";
String selectquery="select gsId, vendorId, productId, description, quantity, purchaseRate, date, MRNo, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15, majorhead_id, minorhead_id, payment_status, actualentry_date, extra16, extra17, extra18, extra19, extra20 from godwanstock where payment_status!='Payment Done'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next())
{
 list.add(new godwanstock_scService(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("majorhead_id"),rs.getString("minorhead_id"),rs.getString("payment_status"),rs.getString("actualentry_date"),rs.getString("extra16"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<godwanstock> getgodwanstockBetweenDates(String fromdate,String todate)
{
List<godwanstock> list = new ArrayList<godwanstock>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="select gsId, vendorId, productId, description, quantity, purchaseRate, date, MRNo, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15, majorhead_id, minorhead_id, payment_status, actualentry_date, extra16, extra17, extra18, extra19, extra20 from godwanstock where (date between '"+fromdate+"' and '"+todate+"') and payment_status!='Payment Done'";
String selectquery="select gsId, vendorId, productId, description, sum(quantity) as quantity, purchaseRate,date, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra16 from godwanstock where date between '"+fromdate+"' and '"+todate+"' group by productId";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next())
{
 list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra16")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public String getTotalPurchasesAmount(String vendorId)
{
	String totAmt="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity*purchaseRate) as total FROM godwanstock where  vendorId = '"+vendorId+"'";
//System.out.println("==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	totAmt=rs.getString("total");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return totAmt;
}
public List<godwanstock> getMSEbyVendorBtwDatesBasedOnVendor(String department,String fromdate,String todate,String vendorId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,productId,vendorId, description, quantity, purchaseRate, date, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra16 FROM godwanstock  where date >= '"+fromdate+"' and date <= '"+todate+"' and vendorId='"+vendorId+"' and  department='"+department+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),"",rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra16")));
//System.out.println("purchaseRatepurchaseRatepurchaseRatepurchaseRate : "+rs.getString("purchaseRate"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getMSEbyVendorBtwDatesBasedOnVendorWithMSCId(String department,String fromdate,String todate,String vendorId,String mscId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,productId,vendorId, description, quantity, purchaseRate, date, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra16,extra17,extra18,extra19,extra20 FROM godwanstock  where date >= '"+fromdate+"' and date <= '"+todate+"' and vendorId='"+vendorId+"' and  department='"+department+"'"+" and extra1='"+mscId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),"",rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra16"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));
//System.out.println("purchaseRatepurchaseRatepurchaseRatepurchaseRate : "+rs.getString("purchaseRate"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<godwanstock> getMSEbyVendorBtwDatesBasedOnVendorWithGroupBy(String department,String fromdate,String todate,String vendorId)
{
ArrayList<godwanstock> list = new ArrayList<godwanstock>();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,productId,vendorId, description, quantity, purchaseRate, date, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15,extra16,extra17,extra18,extra19,extra20 FROM godwanstock  where date >= '"+fromdate+"' and date <= '"+todate+"' and vendorId='"+vendorId+"' and  department='"+department+"'"+"  group by extra1";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new godwanstock(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),"",rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra16"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));
//System.out.println("purchaseRatepurchaseRatepurchaseRatepurchaseRate : "+rs.getString("purchaseRate"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public double getBillTOT(String vendorId, String date1,String date2)
{
double  list = 0.0;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,vendorId,productId,description,quantity,sum(quantity*purchaseRate*(1+vat/100)) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate*(1+vat/100))) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where vendorId='"+vendorId+"' and date between '"+date1+"' and '"+date2+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("purchaseRate")!=null){
	if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
 list=(Double.parseDouble(rs.getString("purchaseRate"))+Double.parseDouble(rs.getString("extra8")));
	} else {
		list=Double.parseDouble(rs.getString("purchaseRate"));
	}
	}else{
		list = 0.0;
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String totalBillByVendorId(String vendorId ,String date1,String date2){
	String total="0.0";
	List<String> list = new ArrayList<String>();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		
		String query = "SELECT extra1 FROM godwanstock where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"' group by extra1";
		//System.out.println(query);
		ResultSet rs = s.executeQuery(query);
		while(rs.next()){
			list.add(rs.getString("extra1"));
		}
		//System.out.println("LIST SIZE: "+list.size());
		if(list.size()>0){
		int i = 0;
		for(i=0;i<list.size();i++){
			
		//	System.out.println("MSE ID = "+list.get(i));
		String selectquery="SELECT sum(quantity*purchaseRate*(1+vat/100)) as purchaseRate, extra8 FROM godwanstock where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"' and extra1 = '"+list.get(i)+"'";
		//System.out.println(selectquery);
		rs = s.executeQuery(selectquery);
		
		while(rs.next()){
			if(rs.getString("purchaseRate")!=null && !rs.getString("purchaseRate").equals("")){
			if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
				total=String.valueOf(Double.parseDouble(total) + (Double.parseDouble(rs.getString("purchaseRate"))+Double.parseDouble(rs.getString("extra8"))));
			} else {
				total=String.valueOf(Double.parseDouble(total) + Double.parseDouble(rs.getString("purchaseRate")));
			}
		}
		}
		}
		s.close();
		rs.close();
		c.close();
		
	}
	}
		catch(Exception e){
		this.seterror(e.toString());
		
		System.out.println("Exception is ;"+e);
		
		}
	return total;
}
public String totalBillByVendorId(String vendorId){
	String total=null;
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT sum(quantity*purchaseRate*(1+vat/100)) as purchaseRate, extra8 FROM godwanstock where vendorId='"+vendorId+"'";
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
				total=String.valueOf((Double.parseDouble(rs.getString("purchaseRate"))+Double.parseDouble(rs.getString("extra8"))));
			} else {
				total=String.valueOf(Double.parseDouble(rs.getString("purchaseRate")));
			}

		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	return total;
}
public String pendingAmountByVendorIdAndDate(String vendorId ,String date1,String date2){
	String total=null;
	double amount = 0.0;
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String query = "SELECT sum(amount) as amount,expinv_id,extra5 FROM productexpenses where date(entry_date) between '"+date1+"' and '"+date2+"' and vendorId='"+vendorId+"' and expinv_id not in(select extra3 from payments where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"') group by expinv_id";
		//System.out.println(query);
		ResultSet rs = s.executeQuery(query);
		while(rs.next()){
			if(rs.getString("amount")!=null && !rs.getString("amount").equals("")){
				amount = amount + Double.parseDouble(rs.getString("amount"));	
			}
		}
		
		String query2 = "select quantity*purchaseRate as amount,extra1,extra8 from godwanstock where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"' and extra1 not in (select extra5 from sansthanaccounts.productexpenses where vendorId='"+vendorId+"' and date(entry_date) between '"+date1+"' and '"+date2+"')";
		
		rs = s.executeQuery(query2);
		while(rs.next()){
			if(rs.getString("amount")!=null && !rs.getString("amount").equals("")){
				amount = amount + Double.parseDouble(rs.getString("amount"));
				if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
					amount = amount + Double.parseDouble(rs.getString("extra8"));
				}
			}
		}
		
	}catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	total = String.valueOf(amount);
	return total;
}
public String pendingAmountByVendorIdAndDate(String vendorId){
	String total=null;
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT sum(quantity*purchaseRate*(1+vat/100)) as purchaseRate, extra8 FROM godwanstock where vendorId='"+vendorId+"' and payment_status!='Payment Done'";
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			if(rs.getString("purchaseRate")!=null){
			if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
				total=String.valueOf((Double.parseDouble(rs.getString("purchaseRate"))+Double.parseDouble(rs.getString("extra8"))));
			} else {
				total=String.valueOf(Double.parseDouble(rs.getString("purchaseRate")));
			}
			}else{
				total = "0.0";
			}

		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	//System.out.println("total: "+total);
	return total;
}
public List<godwanstock> getPendingAmountRecords(String vendorID, String date1,String date2){
	ArrayList<godwanstock> pendingAmount = new ArrayList<godwanstock>();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery = null;
		
		selectquery="SELECT productID,quantity,purchaseRate,(1+vat/100) as vat, extra8,extra1,date,profcharges,approval_status, extra16, department FROM godwanstock where vendorId='"+vendorID+"' and date(date) between '"+date1+"' and '"+date2+"' and extra1 not in(select extra5 from productexpenses where date(entry_date) between '"+date1+"' and '"+date2+"')";
	
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			godwanstock gsw = new godwanstock();
			gsw.setproductId(rs.getString("productID"));
			gsw.setextra1(rs.getString("extra1"));
			gsw.setdate(rs.getString("date"));
			gsw.setExtra16(rs.getString("extra16"));
			gsw.setprofcharges(rs.getString("profcharges"));
			gsw.setdepartment(rs.getString("department"));
			gsw.setApproval_status(rs.getString("approval_status"));
			if(rs.getString("purchaseRate")!=null && !rs.getString("purchaseRate").equals("")){				
				if(rs.getString("quantity")!=null && !rs.getString("quantity").equals("")){					
					double rate = Double.parseDouble(rs.getString("quantity"))*Double.parseDouble(rs.getString("purchaseRate"));				
					if(rs.getString("vat")!=null && !rs.getString("vat").equals("")){						
						rate = rate*Double.parseDouble(rs.getString("vat"));					
					}
				if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
					gsw.setpurchaseRate(String.valueOf(rate+Double.parseDouble(rs.getString("extra8"))));
				} else {
					gsw.setpurchaseRate(String.valueOf(rate));
				}
				}
				}else{
					gsw.setpurchaseRate("0.0");
				}
			pendingAmount.add(gsw);
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	
	return pendingAmount;
}
public String thisYearPendingAmountByVendorIdAndDate(String vendorId,String date1,String date2){
	String total=null;
	double amount = 0.0;
	String mse1  = "";
	double MRNo = 0.0;
	String vendor2 = "";
	ArrayList<String> al = new ArrayList<String>();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String query = "SELECT extra5 FROM productexpenses where date(entry_date) between '"+date1+"' and '"+date2+"' and vendorId='"+vendorId+"' and expinv_id not in(select extra3 from payments where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"') group by extra5";
		//String query = "SELECT extra5 FROM productexpenses where entry_date between '2016-03-28 00:00:01' and '2017-03-31 23:59:59' and vendorId='"+vendorId+"' group by expinv_id";
		System.out.println("query12345..............."+query);
		ResultSet rs = s.executeQuery(query);
		while(rs.next()){
			al.add(rs.getString("extra5"));
		}
		//System.out.println("al.size -----> "+al.size());
		String query2 = "select extra16 from godwanstock where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"' and extra1 not in (select extra5 from productexpenses where vendorId='"+vendorId+"' and date(entry_date) between '"+date1+"' and '"+date2+"') group by extra1";
		System.out.println("query5431hhh..............."+query2);
		//System.out.println(query2);
		rs = s.executeQuery(query2);
		while(rs.next()){	
					amount = amount + Double.parseDouble(rs.getString("extra16"));
					System.out.println("amount.......11........"+amount);
		}
		//System.out.println("al.size -----> "+al.size());
		for(int i=0;i<al.size();i++){
		
			String query1 = "SELECT extra16 FROM godwanstock where extra1='"+al.get(i)+"' group by extra1";
			System.out.println("query1.......11........"+query1);
			rs = s.executeQuery(query1);
			//System.out.println(query1);
			while(rs.next()){
			amount = amount + Double.parseDouble(rs.getString("extra16"));
		}
			//System.out.println("hello");
		}
		//System.out.println("HELLO");
		
	//	System.out.println("amount  ----- >  "+amount);
	}catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	total = String.valueOf(amount);
	return total;
}
public String openingPendingAmountByVendorIdAndDate(String vendorId,String date1,String date2){
	String total=null;
	double amount = 0.0;
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String query = "SELECT sum(amount) as amount,expinv_id,extra5 FROM productexpenses where date(entry_date) between '"+date1+"' and '"+date2+"' and vendorId='"+vendorId+"' and extra1!='Payment Done'";
		//System.out.println(query);
		ResultSet rs = s.executeQuery(query);
		while(rs.next()){
			if(rs.getString("amount")!=null && !rs.getString("amount").equals("")){
				amount = amount + Double.parseDouble(rs.getString("amount"));	
			}
		}
		//System.out.println("IN GODWANSTOCKLISTING");
		String query2 = "select quantity*purchaseRate*(1+vat/100) as amount,extra1,extra8 from godwanstock where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"' and approval_status !='Approved' and extra1 not in (select extra5 from sansthanaccounts.productexpenses where vendorId='"+vendorId+"' and extra1='Payment Done')";
		//System.out.println(query2);
		rs = s.executeQuery(query2);
		while(rs.next()){
			if(rs.getString("amount")!=null && !rs.getString("amount").equals("")){
				amount = amount + Double.parseDouble(rs.getString("amount"));
				if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
					amount = amount + Double.parseDouble(rs.getString("extra8"));
				}
			}
		}
		
	}catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	total = String.valueOf(amount);
	return total;
}
public String thisyearOpeningPendingBalance(String vendorId,String date1,String date2){
	String total=null;
	ArrayList<String> al = new ArrayList<String>();
	double amount = 0.0;
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		//System.out.println("THIS YEAR OPENING BALANCE");
		//String query = "SELECT sum(amount) as amount,expinv_id,extra5 FROM productexpenses where date(entry_date) between '"+date1+"' and '"+date2+"' and vendorId='"+vendorId+"' and extra1!='Payment Done'";
		String selectQuery = "SELECT extra5 FROM productexpenses where vendorId='"+vendorId+"' and date(entry_date) between '"+date1+"' and '"+date2+"' and expinv_id not in (SELECT extra3 FROM sansthanaccounts.payments where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"') group by extra5";
	//	System.out.println(selectQuery);
		ResultSet rs = s.executeQuery(selectQuery);
		while(rs.next()){
			al.add(rs.getString("extra5"));
		}
		for(int i=0;i<al.size();i++){
		String selectQuery1 = "select extra16 from godwanstock where extra1='"+al.get(i)+"' group by extra1";
	//	System.out.println(selectQuery1);
		rs = s.executeQuery(selectQuery1);
		while(rs.next()){
			amount = amount + Double.parseDouble(rs.getString("extra16"));
		}
		}
		String selectQuery2 = "SELECT extra16 FROM godwanstock where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"' and extra1 not in (SELECT extra5 FROM sansthanaccounts.productexpenses where vendorId='"+vendorId+"' and date(entry_date) between '"+date1+"' and '"+date2+"' ) group by extra1" ;
		//System.out.println(selectQuery2);
		rs = s.executeQuery(selectQuery2);
		while(rs.next()){	
					amount = amount + Double.parseDouble(rs.getString("extra16"));			
		}
		
	}catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	total = String.valueOf(amount);
	return total;
}
public String OpeningPendingBalanceByDept(String vendorId,String date1,String date2,String department){
	String total=null;
	ArrayList<String> al = new ArrayList<String>();
	double amount = 0.0;
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		//String query = "SELECT sum(amount) as amount,expinv_id,extra5 FROM productexpenses where date(entry_date) between '"+date1+"' and '"+date2+"' and vendorId='"+vendorId+"' and extra1!='Payment Done'";
		String selectQuery = "SELECT extra5 FROM productexpenses where vendorId='"+vendorId+"' and date(entry_date) between '"+date1+"' and '"+date2+"' and head_account_id='"+department+"' and expinv_id not in (SELECT extra3 FROM sansthanaccounts.payments where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"') group by extra5";
		ResultSet rs = s.executeQuery(selectQuery);
		while(rs.next()){
			al.add(rs.getString("extra5"));
		}
		for(int i=0;i<al.size();i++){
		String selectQuery1 = "select extra16 from godwanstock where extra1='"+al.get(i)+"' group by extra1";
		if(vendorId !=null && vendorId.equals("1435")){
			System.out.println("opening balance 2222..."+selectQuery1);
		}
		rs = s.executeQuery(selectQuery1);
		while(rs.next()){
			amount = amount + Double.parseDouble(rs.getString("extra16"));
		}
		}
		String selectQuery2 = "SELECT extra16 FROM godwanstock where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"' and department = '"+department+"' and extra1 not in (SELECT extra5 FROM sansthanaccounts.productexpenses where vendorId='"+vendorId+"' and date(entry_date) between '"+date1+"' and '"+date2+"' ) group by extra1" ;
		rs = s.executeQuery(selectQuery2);
		while(rs.next()){	
					amount = amount + Double.parseDouble(rs.getString("extra16"));			
		}
//		System.out.println("opening balance amount..."+amount);
	}catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	total = String.valueOf(amount);
	if(vendorId !=null && vendorId.equals("1435")){
		System.out.println("total===== > "+total);
	}
	return total;
}
public String getTotalAmount(String mse_id){
	String total="";
	double amount=0.00;
	String extraCharges = "";
	try{
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		
		String selectQuery = "Select quantity,purchaseRate,extra1,vat,extra8 FROM godwanstock where extra1='"+mse_id+"'";
//		System.out.println(selectQuery);
		ResultSet rs = s.executeQuery(selectQuery);
		while(rs.next()){
			amount = amount + Double.parseDouble(rs.getString("quantity")) * Double.parseDouble(rs.getString("purchaseRate")) * (1+(Double.parseDouble(rs.getString("vat"))/100));
			extraCharges = rs.getString("extra8");
		}
		total = String.valueOf(amount+Double.parseDouble(extraCharges));
		String selectQuery2 = "update godwanstock set extra16 = '"+total+"' where extra1='"+mse_id+"'";
//		System.out.println(selectQuery2);
		int i = s.executeUpdate(selectQuery2);
	}catch(Exception e){
		
	}
	
	return total;
}

public String getTotalAmount1(String mse_id){
	String total="";
	double amount=0.00;
	String extraCharges = "";
	try{
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		
		String selectQuery = "Select quantity,purchaseRate,extra1,vat,extra8 FROM godwanstock where extra1='"+mse_id+"'";
//		System.out.println(selectQuery);
		ResultSet rs = s.executeQuery(selectQuery);
		while(rs.next()){
			amount = amount + Double.parseDouble(rs.getString("quantity")) * Double.parseDouble(rs.getString("purchaseRate")) * (1+(Double.parseDouble(rs.getString("vat"))/100));
			extraCharges = rs.getString("extra8");
		}
		total = String.valueOf(amount+Double.parseDouble(extraCharges));
		
	}catch(Exception e){
		
	}
	
	return total;
}
//this method is written by madhav to get the Status of stock approval by kitchen or not after Approved by po
public String getProfCharges(String proId )
{
String list ="";
try {
// Load the database driver
	String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();
	selectquery="SELECT profcharges FROM godwanstock where  productId = '"+proId+"'"; 
//System.out.println("profcharges= godwanstock======>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
list=rs.getString("profcharges");
}

}
catch(Exception e){
e.printStackTrace();
System.out.println("Exception is ;"+e);
}
return list;
}

	public List<String> getGSTs() {
		List<String> list = new ArrayList<String>();
		String selectQuery = "SELECT extra17 FROM godwanstock group by extra17";

		try {
			c = ConnectionHelper.getConnection();
			s = c.createStatement();

			ResultSet rs = s.executeQuery(selectQuery);
			while (rs.next()) {
				list.add(rs.getString("extra17"));
			}

			rs.close();
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<godwanstock> getgdwnStkBetweenDatesBsdOnHoa(String fromdate,String todate,String hoa,String productid,String condition)	{
		List<godwanstock> gdList = new ArrayList<godwanstock>();
		ResultSet rs = null;
		String subQuery = " ",query="";
		System.out.println("head---"+hoa);
		System.out.println("pro---"+productid);
		if (!productid.equals(""))	{
			subQuery += "and productId="+productid+" ";
		}
		else {
			subQuery += " group by productId order by date,productId ";
		}
		if(!condition.trim().equals(""))	{
			query = "select sum(quantity) as quantity,date,productId,majorhead_id,extra3 from godwanstock where date >= '"+fromdate+"' and date < '"+todate+"' and department ="+hoa+subQuery;
		}
		else	{
			query = "select sum(quantity) as quantity,date,productId,majorhead_id,extra3 from godwanstock where date >= '"+fromdate+"' and date <= '"+todate+"' and department ="+hoa+subQuery;
		}
		try	{
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);
			
			while(rs.next())	{
				godwanstock gd = new godwanstock();
				gd.setQuantity(rs.getString("quantity"));
				gd.setDate(rs.getString("date"));
				gd.setProductId(rs.getString("productId"));
				gd.setMajorhead_id(rs.getString("majorhead_id"));
				gd.setextra3(rs.getString("extra3"));
				gdList.add(gd);
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		finally	{
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return gdList;
	}
	
	public List<godwanstock> getOlddcEntries(String fromdate,String todate)	{
		List<godwanstock> list = new ArrayList<godwanstock>();
		ResultSet rs = null;
		String suqry = " ";
		if (fromdate != null && !fromdate.equals("") && todate != null && !todate.equals(""))	{
			suqry += "and g.date >= '"+fromdate+"' and g.date <= '"+todate+"' ";
		}
		String query = "SELECT g.extra1,g.extra21,g.date,g.extra9,g.extra25,v.agencyName,v.vendorid,g.department,g.billNo,h.name FROM godwanstock g,vendors v,headofaccounts h where approval_status='dcnotapproved' and (g.vendorid = v.vendorid) and (g.department = h.head_account_id)"+suqry+" group by g.extra25 order by g.extra1";
//		System.out.println("get old dc vendors----"+query);
		try	{
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);
			while(rs.next())	{
				godwanstock gd = new godwanstock();
				gd.setExtra1(rs.getString("g.extra1"));
				gd.setExtra21(rs.getString("g.extra21"));
				gd.setDate(rs.getString("g.date"));
				gd.setExtra9(rs.getString("g.extra9"));
				gd.setExtra25(rs.getString("g.extra25"));
				gd.setExtra10(rs.getString("v.agencyName"));
				gd.setVendorId(rs.getString("v.vendorid"));
				gd.setDepartment(rs.getString("g.department"));
				gd.setBillNo(rs.getString("g.billNo"));
				gd.setExtra12(rs.getString("h.name"));
				list.add(gd);
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		finally	{
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public List<godwanstock> getDcEntriesBetweenDates(String fromdate,String todate,String dcNo,String status)	{
	List<godwanstock> list = new ArrayList<godwanstock>();
		ResultSet rs = null;
		String suqry = " ",suqry1= " ",subqry2=" ";
		if (fromdate != null && !fromdate.equals("") && todate != null)	{
			suqry += " and g.date >= '"+fromdate+"' and g.date <= '"+todate+"' ";
		}
		if (dcNo != null && !dcNo.trim().equals(""))	{
			suqry1 += " and g.extra25 = '"+dcNo+"' ";
		}
		else	{
			suqry1 += " and g.extra25 like 'DC%' ";
		}
		if (status != null && !status.equals(""))	{
			subqry2 += " g.approval_status ='Approved'";
		}
		else	{
			subqry2 += " g.approval_status !='Approved'";
		}
		String query = "SELECT g.extra1,g.extra21,DATE_FORMAT(g.date,'%Y-%m-%d') as gdate,g.extra9,g.extra25,g.extra24,g.extra16,v.agencyName,v.vendorid,g.department,p.productId,p.productName,mh.major_head_id,mh.name,mn.minorhead_id,mn.name,s.sub_head_id,s.name,g.quantity,g.purchaseRate,h.name FROM godwanstock g left outer join vendors v on g.vendorid = v.vendorid left outer join products p on (g.productId = p.productId and g.department = p.head_account_id and g.majorhead_id = p.major_head_id and g.minorhead_id = p.extra3 and g.extra23 = p.sub_head_id) left outer join majorhead mh on (g.majorhead_id = mh.major_head_id and g.department = mh.head_account_id) left outer join minorhead mn on (g.minorhead_id = mn.minorhead_id and g.majorhead_id = mn.major_head_id and g.department = mn.head_account_id) left outer join subhead s on (g.extra23 = s.sub_head_id and g.minorhead_id = s.extra1 and g.majorhead_id = s.major_head_id and g.department = s.head_account_id) left outer join headofaccounts h on (g.department = h.head_account_id) where "+subqry2+suqry1+suqry+" and g.expensesEntryDate = '' order by g.extra1,g.extra25";
		
		System.out.println("sq--"+query);
		try	{
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);
			while(rs.next())	{
				godwanstock gd = new godwanstock();
				gd.setExtra1(rs.getString("g.extra1"));
				gd.setExtra21(rs.getString("g.extra21"));
				gd.setDate(rs.getString("gdate"));
				gd.setExtra9(rs.getString("g.extra9"));
				gd.setExtra25(rs.getString("g.extra25"));
				gd.setExtra10(rs.getString("v.agencyName"));
				gd.setVendorId(rs.getString("v.vendorid"));
				gd.setDepartment(rs.getString("g.department"));
				gd.setProductId(rs.getString("p.productId"));
				gd.setExtra29(rs.getString("p.productName"));
				gd.setMajorhead_id(rs.getString("mh.major_head_id"));
				gd.setExtra26(rs.getString("mh.name")); // major head name
				gd.setMinorhead_id(rs.getString("mn.minorhead_id"));
				gd.setExtra27(rs.getString("mn.name")); // minor head name
				gd.setExtra23(rs.getString("s.sub_head_id"));
				gd.setExtra28(rs.getString("s.name")); // sub head name
				gd.setExtra24(rs.getString("g.extra24")); // individual product amount 
				gd.setExtra16(rs.getString("g.extra16")); // total mse entry amount
				gd.setQuantity(rs.getString("g.quantity"));
				gd.setPurchaseRate(rs.getString("g.purchaseRate"));
				gd.setExtra30(rs.getString("h.name"));
				list.add(gd);
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		finally	{
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	public List<String> getDcVendors(String appStatus)	{
		List<String> vendors = null;
		ResultSet rs = null;
		String query = "";
		if (!appStatus.trim().equals(""))	{
			query = "select g.vendorId,v.agencyName,g.extra25 from godwanstock g,vendors v where g.extra25 like 'DC%' and g.expensesEntryDate = '' and (g.vendorId = v.vendorId) and g.approval_status = '"+appStatus+"' group by g.vendorId";
		}
		else	{
			query = "select g.vendorId,v.agencyName,g.extra25 from godwanstock g,vendors v where g.extra25 like 'DC%' and g.expensesEntryDate = '' and (g.vendorId = v.vendorId) group by g.vendorId";
		}
		String vid = "";
//		System.out.println("venndor----"+query);
		try	{
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);
			vendors = new ArrayList<String>();
			while (rs.next())	{
				vid = rs.getString("g.vendorId")+":"+rs.getString("v.agencyName")+":"+rs.getString("g.extra25");
				vendors.add(vid);
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		finally	{
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return	vendors;
	}
	
	// stock not approved by kitchen tore supervisor
	public List<godwanstock> getUnapprovedDcBills(String dcNumber,String fromdate,String todate)	{
		List<godwanstock> gdl = null;
		ResultSet rs = null;
		String subQuery = " ";
//		System.out.println("dcNumber---"+dcNumber);
		if (!dcNumber.equals(""))	{
			subQuery += "and extra25 = '"+dcNumber+"' ";
		}
		else	{
			subQuery += "and extra25 like 'DC%' ";
		}
		if (!fromdate.equals("") && !todate.equals(""))	{
				subQuery += "and date >= '"+fromdate+"' and date <= '"+todate+"'";
		}
		String query = "select vendorId,productId,quantity,purchaseRate,date,extra1,emp_id,department,approval_status,majorhead_id,minorhead_id,extra16,extra21,extra23,extra24,extra25 from godwanstock where approval_status !='Not-Approve'"+subQuery;
//		System.out.println("query----"+query);
		try	{
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);
			gdl = new ArrayList<godwanstock>();
			while (rs.next())	{
				godwanstock gd = new godwanstock();
				gd.setVendorId(rs.getString("vendorId"));
				gd.setProductId(rs.getString("productId"));
				gd.setQuantity(rs.getString("quantity"));
				gd.setPurchaseRate(rs.getString("purchaseRate"));
				gd.setDate(rs.getString("date"));
				gd.setExtra1(rs.getString("extra1"));
				gd.setEmp_id(rs.getString("emp_id"));
				gd.setDepartment(rs.getString("department"));
				gd.setApproval_status(rs.getString("approval_status"));
				gd.setMajorhead_id(rs.getString("majorhead_id"));
				gd.setMinorhead_id(rs.getString("minorhead_id"));
				gd.setExtra16(rs.getString("extra16"));
				gd.setExtra21(rs.getString("extra21"));
				gd.setExtra23(rs.getString("extra23"));
				gd.setExtra24(rs.getString("extra24"));
				gd.setExtra25(rs.getString("extra25"));
				
				gdl.add(gd);
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		finally	{
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return	gdl;
	}
	
	
	public List<godwanstock> getGodownStockId(String expensesId)	{
		List godwnstockidList = null;
		ResultSet rs = null;
		godwanstock gstock=null;
		String query = "";
		String godownid="";
			query = "SELECT gsid,extra21 FROM godwanstock where extra21='"+expensesId+"'";
			
	
		try	{
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);
			godwnstockidList = new ArrayList<String>();
			while (rs.next())	{
				gstock=new godwanstock();
				gstock.setgsId(rs.getString("gsid"));
				gstock.setExtra21(rs.getString("extra21")); 
				godwnstockidList.add(gstock);
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		finally	{
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return	godwnstockidList;
	}
	public List<godwanstock> getMSEsBetweenDates(String fromDate,String toDate){
		System.out.println("Hiii");
		List<godwanstock> godwnstockList=new ArrayList<godwanstock>();
		String selectQuery="select * from godwanstock where date>='"+fromDate+"' and date<='"+toDate+"'";
		try {
		 c=ConnectionHelper.getConnection();
		 s=c.createStatement();
		 ResultSet rs=s.executeQuery(selectQuery);
		 while(rs.next()) {
		 godwanstock gs=new godwanstock();
		 gs.setgsId(rs.getString("gsId"));
		 gs.setvendorId(rs.getString("vendorId"));
		 gs.setproductId(rs.getString("productId"));
		 gs.setQuantity(rs.getString("quantity"));
		 gs.setPurchaseRate(rs.getString("purchaseRate"));
		 gs.setDate(rs.getString("date"));
		 gs.setextra1(rs.getString("extra1"));
		 gs.setdepartment(rs.getString("department"));
		 gs.setExtra16(rs.getString("extra16"));
		 gs.setExtra21(rs.getString("extra21"));
		 godwnstockList.add(gs);
		 }
		 rs.close();
		 c.close();
		 
		}catch(Exception e){
		e.printStackTrace();
		}
		return godwnstockList;
	}
	public List<godwanstock> getMSEsBetweenDatesBasedOnHeadOfAccount(String fromDate,String toDate,String headOfAccount){
		List<godwanstock> godwnstockList=new ArrayList<godwanstock>();
		String selectQuery="select * from godwanstock where date>='"+fromDate+"' and date<='"+toDate+"'and extra1 like 'MSE%' and department='"+headOfAccount+"' group by extra1 order by extra1";
		try {
		 c=ConnectionHelper.getConnection();
		 s=c.createStatement();
		 ResultSet rs=s.executeQuery(selectQuery);
		 while(rs.next()) {
		 godwanstock gs=new godwanstock();
		 gs.setgsId(rs.getString("gsId"));
		 gs.setvendorId(rs.getString("vendorId"));
		 gs.setproductId(rs.getString("productId"));
		 gs.setQuantity(rs.getString("quantity"));
		 gs.setPurchaseRate(rs.getString("purchaseRate"));
		 gs.setDate(rs.getString("date"));
		 gs.setextra1(rs.getString("extra1"));
		 gs.setdepartment(rs.getString("department"));
		 gs.setExtra16(rs.getString("extra16"));
		 gs.setExtra21(rs.getString("extra21"));
		 godwnstockList.add(gs);
		 }
		 rs.close();
		 c.close();
		 
		}catch(Exception e){
		e.printStackTrace();
		}
		return godwnstockList;
	}
	public List<godwanstock> getMSEDetails(String mseId){
		List<godwanstock> godwnstockList=new ArrayList<godwanstock>();
		String selectQuery="select * from godwanstock where extra1='"+mseId+"'";
		try {
		 c=ConnectionHelper.getConnection();
		 s=c.createStatement();
		 ResultSet rs=s.executeQuery(selectQuery);
		 while(rs.next()) {
		 godwanstock gs=new godwanstock();
		 gs.setgsId(rs.getString("gsId"));
		 gs.setvendorId(rs.getString("vendorId"));
		 gs.setproductId(rs.getString("productId"));
		 gs.setQuantity(rs.getString("quantity"));
		 gs.setPurchaseRate(rs.getString("purchaseRate"));
		 gs.setDate(rs.getString("date"));
		 gs.setextra1(rs.getString("extra1"));
		 gs.setdepartment(rs.getString("department"));
		 gs.setExtra16(rs.getString("extra16"));
		 gs.setExtra21(rs.getString("extra21"));
		 gs.setExtra18(rs.getString("extra18"));
		 gs.setExtra19(rs.getString("extra19"));
		 gs.setExtra20(rs.getString("extra20"));
		 godwnstockList.add(gs);
		 }
		 rs.close();
		 c.close();
		 
		}catch(Exception e){
		e.printStackTrace();
		}
		return godwnstockList;
	}
	
	public void updateGodwanstockDetails(String productId,String vendorId,String quantity,String purchaseRate,String cgst,String sgst,String igst,String totalAmount,String godwanStockId) {
		String updateQuery="update godwanstock set productId='"+productId+"',vendorId='"+vendorId+"',quantity='"+quantity+"', purchaseRate='"+purchaseRate+"',extra18='"+cgst+"', extra19='"+sgst+"', extra20='"+igst+"',extra16='"+totalAmount+"' where gsid='"+godwanStockId+"'";
		try {
			 c=ConnectionHelper.getConnection();
			 s=c.createStatement();
			 s.executeUpdate(updateQuery);
			 s.close();
			 c.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}