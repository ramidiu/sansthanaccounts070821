package mainClasses;


import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.majorhead;
public class majorheadListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String major_head_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setmajor_head_id(String major_head_id)
{
this.major_head_id = major_head_id;
}
public String getmajor_head_id(){
return(this.major_head_id);
}
public List getmajorhead()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMajorHeadNameBasedOnId(String majorHeadId){
	String name="";
	try{
		c=ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectQuery="SELECT name from majorhead where major_head_id="+majorHeadId;
		ResultSet rs=s.executeQuery(selectQuery);
		while(rs.next()){
			name=rs.getString("name");
		}
	}catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	
	return name;
}



public List getMmajorhead(String major_head_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  major_head_id = '"+major_head_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMmajorhead1(String head_account_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  head_account_id = '"+head_account_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<majorhead> getMajorHeadBasdOnCompany(String head_account_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  head_account_id = '"+head_account_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<majorhead> getMajorHeadBasdOnCompanyAndRevenueType(String head_account_id,String RevenueType)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  head_account_id = '"+head_account_id+"' and extra2='"+RevenueType+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<majorhead> getMajorHeadBasdOnCompanyAndRevenueType(String head_account_id,String RevenueType,String headsgroup)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
String innerquery="";

/*if(!headsgroup.equals("")){
	innerquery=" and extra1='"+headsgroup+"' ";
}*/

if(!headsgroup.equals("") && head_account_id.equals("4")){
	innerquery=" and (extra1='1' || extra1 = '2') ";
}
else if(!headsgroup.equals("")){
	innerquery=" and extra1='"+headsgroup+"' ";
}

c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  head_account_id = '"+head_account_id+"'"+innerquery+" and extra2='"+RevenueType+"' "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<majorhead> getMajorHeadBasdOnCompanyAndRevenueTypeNew(String head_account_id,String RevenueType,String headsgroup)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
String innerquery="";
String selectquery="";
if(!headsgroup.equals("") && head_account_id.equals("4")){
	innerquery=" and (extra1='1' || extra1 = '2') ";
}
else if(!headsgroup.equals("")){
	innerquery=" and extra1='"+headsgroup+"' ";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
if(head_account_id!=null&&!head_account_id.equals("")){
selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  head_account_id = '"+head_account_id+"'"+innerquery+" and extra2='"+RevenueType+"' "; 

}
else{
	if(RevenueType.equals("expenses")){
	selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where extra2='"+RevenueType+"' order by extra1 " ;	
	}
	else{
		selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where extra2='"+RevenueType+"'";	
	}
}

System.out.println("getMajorHeadBasdOnCompanyAndRevenueTypeNew ====> "+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<majorhead> getMajorHeadBasdOnCompanyAndRevenueTypeNew1(String head_account_id,String RevenueType,String headsgroup,String report)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
String innerquery="";
String selectquery="";
if(!headsgroup.equals("") && head_account_id.equals("4")){
	innerquery=" and (extra1='1' || extra1 = '2') ";
}
else if(!headsgroup.equals("")){
	innerquery=" and extra1='"+headsgroup+"' ";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
if(head_account_id!=null&&!head_account_id.equals("")){
selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  head_account_id = '"+head_account_id+"'"+innerquery+" and extra2='"+RevenueType+"'  and extra3 like  '%"+report+"%'";

}
else{
	if(RevenueType.equals("expenses")){
	selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where extra2='"+RevenueType+"' order by extra1 and extra3 like  '%"+report+"%' " ;	
	}
	else{
		selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where extra2='"+RevenueType+"' and extra3 like  '%"+report+"%'";	
	}
}
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getHeadId(String major_head_id)
{
//ArrayList list = new ArrayList();
	String headId="";
try {
 // Load the database driver
String innerquery="";
String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();

selectquery="SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  major_head_id = '"+major_head_id+"' "; 

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 //list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));
headId=rs.getString("head_account_id");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return headId;
}


public String getmajorheadName(String major_head_id)
{
String name="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT name FROM majorhead where major_head_id='"+major_head_id+"' "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
public String getmajorheadname(String major_head_name,String head_account_id)
{
String name="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id,name FROM majorhead where name like '%"+major_head_name+"%' and head_account_id='"+head_account_id+"'"; 

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("major_head_id")+" "+rs.getString("name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}



public String getMajorHeadName(String majorHeadId,String head_account_id)
{
String name="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id,name FROM majorhead where major_head_id = "+majorHeadId+" and head_account_id='"+head_account_id+"'"; 

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("major_head_id")+" "+rs.getString("name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}





public String getheadgroup(String sub_head_id,String head_account_id)
{
String headgroup="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT m.extra1 as extra1 FROM subhead s,majorhead m where s.sub_head_id = '"+sub_head_id+"' and s.head_account_id ='"+head_account_id+"' and s.major_head_id = m.major_head_id and m.head_account_id = '"+head_account_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	headgroup=rs.getString("extra1");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return headgroup;
}

public String getheadgroup2(String major_head_id,String head_account_id)
{
String headgroup="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra1 from majorhead where major_head_id = '"+major_head_id+"' and head_account_id ='"+head_account_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	headgroup=rs.getString("extra1");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return headgroup;
}


public List getMmajorheadSearch(String major,String catid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT major_head_id, name, description, head_account_id, extra1, extra2, extra3 FROM majorhead where ( major_head_id like '%"+major+"%' || name like '%"+major+"%') and head_account_id='"+catid+"' ";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new majorhead(rs.getString("major_head_id"),rs.getString("name"),rs.getString("description"),rs.getString("head_account_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMajorHeadNameByCat1(String mjrheadid,String Category)
{
String list ="";
try {
 // Load the database driver
	String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();

	selectquery="SELECT name FROM majorhead where  major_head_id = '"+mjrheadid+"'";	

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("name");
}

}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

}