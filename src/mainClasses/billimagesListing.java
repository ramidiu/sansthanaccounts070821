package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import dbase.sqlcon.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.billimages;
public class billimagesListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String biId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setbiId(String biId)
{
this.biId = biId;
}
public String getbiId(){
return(this.biId);
}
public List getbillimages()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT biId,referenceId,Type,date,emp_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM billimages "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new billimages(rs.getString("biId"),rs.getString("referenceId"),rs.getString("Type"),rs.getString("date"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMbillimages(String biId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT biId,referenceId,Type,date,emp_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM billimages where  biId = '"+biId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new billimages(rs.getString("biId"),rs.getString("referenceId"),rs.getString("Type"),rs.getString("date"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getListImage(String refId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT biId,referenceId,Type,date,emp_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM billimages where  referenceId = '"+refId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new billimages(rs.getString("biId"),rs.getString("referenceId"),rs.getString("Type"),rs.getString("date"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getImageName(String refId)
{
	String name = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT biId,referenceId,Type,date,emp_id,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM billimages where  referenceId = '"+refId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	name=rs.getString("extra1");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
}