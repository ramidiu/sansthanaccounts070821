package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.headofaccounts;

public class headofaccountsListing {
	private String error = "";

	public void seterror(String error) {
		this.error = error;
	}

	public String geterror() {
		return this.error;
	}

	private String head_account_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;

	public void sethead_account_id(String head_account_id) {
		this.head_account_id = head_account_id;
	}

	public String gethead_account_id() {
		return (this.head_account_id);
	}

	public List getheadofaccounts() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT head_account_id,name,phone,address,description,extra1,extra2,extra3,extra4,extra5 FROM headofaccounts  where extra1!='InActive' order by name";
			 //System.out.println("HOA::"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new headofaccounts(rs.getString("head_account_id"), rs
						.getString("name"), rs.getString("phone"), rs
						.getString("address"), rs.getString("description"), rs
						.getString("extra1"), rs.getString("extra2"), rs
						.getString("extra3"), rs.getString("extra4"), rs
						.getString("extra5")));
 
			}
			s.close(); 
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getActiveHOA() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT head_account_id,name,phone,address,description,extra1,extra2,extra3,extra4,extra5 FROM headofaccounts where extra1!='InActive' ";
			// System.out.println("HOA::"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new headofaccounts(rs.getString("head_account_id"), rs
						.getString("name"), rs.getString("phone"), rs
						.getString("address"), rs.getString("description"), rs
						.getString("extra1"), rs.getString("extra2"), rs
						.getString("extra3"), rs.getString("extra4"), rs
						.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getHOA() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT head_account_id,name,phone,address,description,extra1,extra2,extra3,extra4,extra5 FROM headofaccounts where head_account_id='3' ";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new headofaccounts(rs.getString("head_account_id"), rs
						.getString("name"), rs.getString("phone"), rs
						.getString("address"), rs.getString("description"), rs
						.getString("extra1"), rs.getString("extra2"), rs
						.getString("extra3"), rs.getString("extra4"), rs
						.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getMheadofaccounts(String head_account_id) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT head_account_id,name,phone,address,description,extra1,extra2,extra3,extra4,extra5 FROM headofaccounts where  head_account_id = '"
					+ head_account_id + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new headofaccounts(rs.getString("head_account_id"), rs
						.getString("name"), rs.getString("phone"), rs
						.getString("address"), rs.getString("description"), rs
						.getString("extra1"), rs.getString("extra2"), rs
						.getString("extra3"), rs.getString("extra4"), rs
						.getString("extra5")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getHeadofAccountName(String head_account_id) {
		String name = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT name FROM headofaccounts where  head_account_id = '"
					+ head_account_id + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				name = rs.getString("name");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}

}