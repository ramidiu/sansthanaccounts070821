package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.poojasaaman;
public class poojasaamanListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String saman_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setsaman_id(String saman_id)
{
this.saman_id = saman_id;
}
public String getsaman_id(){
return(this.saman_id);
}
public List getpoojasaaman()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT saman_id,sub_head_id,productId,qty,extra1,extra2,extra3,extra4,extra5 FROM poojasaaman "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new poojasaaman(rs.getString("saman_id"),rs.getString("sub_head_id"),rs.getString("productId"),rs.getString("qty"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMpoojasaaman(String saman_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT saman_id,sub_head_id,productId,qty,extra1,extra2,extra3,extra4,extra5 FROM poojasaaman where  saman_id = '"+saman_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new poojasaaman(rs.getString("saman_id"),rs.getString("sub_head_id"),rs.getString("productId"),rs.getString("qty"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPoojas()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT saman_id,sub_head_id,productId,qty,extra1,extra2,extra3,extra4,extra5 FROM poojasaaman group by sub_head_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new poojasaaman(rs.getString("saman_id"),rs.getString("sub_head_id"),rs.getString("productId"),rs.getString("qty"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getProductsBasedOnSubhead(String sub_head_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT saman_id,sub_head_id,productId,qty,extra1,extra2,extra3,extra4,extra5 FROM poojasaaman where  sub_head_id = '"+sub_head_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new poojasaaman(rs.getString("saman_id"),rs.getString("sub_head_id"),rs.getString("productId"),rs.getString("qty"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}