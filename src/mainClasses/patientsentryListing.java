package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.patientsentry;
public class patientsentryListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String patientId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setpatientId(String patientId)
{
this.patientId = patientId;
}
public String getpatientId(){
return(this.patientId);
}
public List getpatientsentry()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT patientId,patientName,address,phone,extra1,extra2,extra3,extra4,extra5 FROM patientsentry "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new patientsentry(rs.getString("patientId"),rs.getString("patientName"),rs.getString("address"),rs.getString("phone"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getpatientsentryBetweenDates(String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT patientId,patientName,address,phone,extra1,extra2,extra3,extra4,extra5 FROM patientsentry where extra2 >= '"+fromdate+"' and extra2 <= '"+todate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new patientsentry(rs.getString("patientId"),rs.getString("patientName"),rs.getString("address"),rs.getString("phone"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMpatientsentry(String patientId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT patientId,patientName,address,phone,extra1,extra2,extra3,extra4,extra5 FROM patientsentry where  patientId = '"+patientId+"'"; 
//System.out.println("patientsentry=>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new patientsentry(rs.getString("patientId"),rs.getString("patientName"),rs.getString("address"),rs.getString("phone"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPatientsByKey(String key)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT patientId,patientName,address,phone,extra1,extra2,extra3,extra4,extra5 FROM patientsentry where  patientName like '%"+key+"%' or patientId like '%"+key+"%' or phone like '%"+key+"%' group by patientId"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new patientsentry(rs.getString("patientId"),rs.getString("patientName"),rs.getString("address"),rs.getString("phone"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getPatientName(String pid)
{
String Name="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT patientName FROM patientsentry where patientId='"+pid+"' "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	Name=rs.getString("patientName");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Name;
}
//get
public List getPatientAndDoctorDetailBasedOnAppoinmentId(String oldAppointmentId)
{
	ArrayList list = new ArrayList();
	/*String patientName="";
	String doctorName="";
	String purpose="";
	String phone="";
	String extra3="";
	String extra1="";
	String address="";
	String patientId="";*/
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="select p.patientName,dd.doctorName,a.purpose,p.extra5,p.phone,p.extra3,p.extra1,p.address,p.patientId FROM patientsentry p,appointments a,doctordetails dd where p.patientId=a.patientId and a.doctorId=dd.doctorId and p.phone='"+oldAppointmentId+"';"; 
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
			list.add(rs.getString("p.patientName"));
			list.add(rs.getString("dd.doctorName"));
			list.add(rs.getString("a.purpose"));
			list.add(rs.getString("p.phone"));
			list.add(rs.getString("p.extra3"));  //age
			list.add(rs.getString("p.extra1"));   //gender
			list.add(rs.getString("p.address"));
			list.add(rs.getString("p.patientId"));
			list.add(rs.getString("p.extra5"));
			/*patientName=rs.getString("p.patientName");
			doctorName=rs.getString("dd.doctorName");
			purpose=rs.getString("a.purpose");
			phone=rs.getString("p.phone");
			extra3=rs.getString("p.extra3");  //age
			extra1=rs.getString("p.extra1");	 //gender
			address=rs.getString("p.address");
			patientId=rs.getString("p.patientId");*/
		}
		/*list.add(patientName);
		list.add(doctorName);
		list.add(purpose);
		list.add(phone);
		list.add(extra3);
		list.add(extra1);
		list.add(address);
		list.add(patientId);*/
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
	return list;
}
public List getPatientAndDoctorDetailBasedOnAppoinmentIdd(String oldAppointmentId)
{
	ArrayList list = new ArrayList();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="select p.patientName,dd.doctorName,a.purpose,p.phone,p.extra3,p.extra1,p.extra4,p.extra5,p.address,p.patientId FROM patientsentry p,diagnosticappointments a,doctordetails dd where p.patientId=a.patientId and a.doctorId=dd.doctorId and a.appointmnetId='"+oldAppointmentId+"';"; 
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
			list.add(rs.getString("p.patientName"));
			list.add(rs.getString("dd.doctorName"));
			list.add(rs.getString("a.purpose"));
			list.add(rs.getString("p.phone"));
			list.add(rs.getString("p.extra3"));  //age
			list.add(rs.getString("p.extra1"));	 //gender
			list.add(rs.getString("p.extra4"));	 //test name
			list.add(rs.getString("p.extra5"));	 //amount
			list.add(rs.getString("p.address"));
			list.add(rs.getString("p.patientId"));
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
	return list;
}

}