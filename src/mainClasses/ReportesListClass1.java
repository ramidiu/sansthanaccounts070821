package mainClasses;

import beans.bankbalance;
import beans.bankdetails;
import beans.banktransactions;
import beans.customerpurchases;
import beans.godwanstock;
import beans.majorhead;
import beans.minorhead;
import beans.productexpenses;
import beans.subhead;
import beans.vendors;
import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class ReportesListClass1
{
  Connection con = null;
  Statement statement = null;
  
  public ReportesListClass1() {}
  
  public List<majorhead> getMajorHeadsBasedOnHOA(String head_of_account) {
	  List<majorhead> majorHeadList = new ArrayList<majorhead>();
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectquery = "SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  head_account_id = '" + head_of_account + "'";
      
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        majorHeadList.add(new majorhead(rs.getString("major_head_id"), rs.getString("name"), rs.getString("description"), rs.getString("head_account_id"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3")));
      }
      statement.close();
      rs.close();
      con.close();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return majorHeadList;
  }
  

  public List<majorhead> getMajorHeadesBasedOnNatcherAndHeadOfAccount(String head_of_account, String natcher)
  {
    List<majorhead> majorHeadList = new ArrayList<majorhead>();
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectquery = "SELECT * FROM majorhead where  head_account_id = '" + head_of_account + "' and extra2 = '" + natcher + "'";
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        majorHeadList.add(new majorhead(rs.getString("major_head_id"), rs.getString("name"), rs.getString("description"), rs.getString("head_account_id"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3")));
      }
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return majorHeadList;
  }
  
  public List<minorhead> getMinorHeadsByMajorHeadAndHeadOfAccountId(String head_of_account, String majorHeadId)
  {
    List<minorhead> minorheadList = new ArrayList<minorhead>();
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectquery = "SELECT * FROM minorhead where  head_account_id = '" + head_of_account + "' and major_head_id = '" + majorHeadId + "'";
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        minorheadList.add(new minorhead(rs.getString("minorhead_id"), rs.getString("name"), rs.getString("description"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3")));
      }
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return minorheadList;
  }
  
  public List<subhead> getSubheadsByMinorHeadAndMajorHeadAndHeadOfAccount(String head_of_account, String majorHeadId, String minorHeadId) {
    List<subhead> subheadList = new ArrayList<subhead>();
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectquery = "SELECT * FROM subhead where  head_account_id = '" + head_of_account + "' and major_head_id = '" + majorHeadId + "' and extra1 = '" + minorHeadId + "'";
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        subheadList.add(new subhead(rs.getString("sub_head_id"), rs.getString("name"), rs.getString("description"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3")));
      }
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return subheadList;
  }
  


  public List<banktransactions> getOtherDepositsAmountGroupBySubheadId(String type, String hoa, String majorHeadId, String minorheadId, String subHeadId, String fromDate, String toDate, String groupBy, String dayOf, String bankType)
  {
    List<banktransactions> banktransactions = new ArrayList<banktransactions>();
    
    String subQuery = "";
    
    if(majorHeadId != null && !majorHeadId.trim().equals("")){
    	 subQuery = " and major_head_id = '" + majorHeadId + "'";
    }
    
    if ((minorheadId != null) && (!minorheadId.equals(""))) {
      subQuery = " and minor_head_id = '" + minorheadId + "'";
    }
    
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and sub_head_id = '" + subHeadId + "'";
    }
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    String loanBanks = "";
    if(bankType != null && bankType.trim().equals("not a loan bank")){
    	loanBanks = " and bank_id != 'BNK1021' and bank_id != 'BNK1022' and bank_id != 'BNK1024' and bank_id != 'BNK1025' ";
    }else if(bankType != null && bankType.trim().equals("loan bank")){
    	loanBanks = " and ( bank_id = 'BNK1021' or bank_id = 'BNK1022' or bank_id = 'BNK1024' or bank_id = 'BNK1025') ";
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectQuery = "SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration, date(date) as date, amount,type,createdBy,editedBy,extra1,extra2,extra3,"
      		+ "extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, " + 
        dayOf + "(date) as extra13 FROM banktransactions where brs_date >= '" + fromDate + "' and brs_date <= '" + toDate + "' "+loanBanks+" and extra8 = '" + type + "' "
        		+ "and head_account_id = '" + hoa + "'" + subQuery + subGroupQuery + " order by date";
      
//      System.out.println("getOtherDepositsAmountGroupBySubheadId =====> " + selectQuery);
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next()) {
        banktransactions.add(new banktransactions(rs.getString("banktrn_id"), rs.getString("bank_id"), rs.getString("name"), rs.getString("narration"), rs.getString("date"), rs.getString("amount"), rs.getString("type"), rs.getString("createdBy"), rs.getString("editedBy"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("brs_date"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("sub_head_id"), rs.getString("minor_head_id"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13")));
      }
      
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    

    return banktransactions;
  }
  

  public List<bankbalance> getMajorOrMinorOrSubheadOpeningBal(String hoa, String majorHeadId, String minorHeadId, String subHeadId, String finYr, String assetsORliabilites, String debitORcredit, String groupBy, String dayOf)
  {
    List<bankbalance> bankbalance = new ArrayList<bankbalance>();
    String subQuery = "";
    
    
   
   
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and extra3 = '" + majorHeadId + "'";
    }
    if ((minorHeadId != null) && (!minorHeadId.equals(""))) {
      subQuery = subQuery + " and extra4 = '" + minorHeadId + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and extra5 = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT uniq_id, bank_id, type, sum(bank_bal) as bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9, " + 
        dayOf + "(createdDate) as extra10 FROM bankbalance where type = '" + assetsORliabilites + "' and extra1 = '" + finYr + "'" + 
        " and extra7 = '" + debitORcredit + "' and extra2 = '" + hoa + "'" + subQuery + subGroupQuery + " order by createdDate";
      
//      System.out.println("getMajorOrMinorOrSubheadOpeningBal ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        bankbalance bankbalance2 = new bankbalance();
        
        bankbalance2.setBank_bal(rs.getString("bank_bal"));
        bankbalance2.setExtra2(rs.getString("extra2"));
        bankbalance2.setExtra3(rs.getString("extra3"));
        bankbalance2.setExtra4(rs.getString("extra4"));
        bankbalance2.setExtra5(rs.getString("extra5"));
        bankbalance2.setExtra10(rs.getString("extra10"));
        
        bankbalance.add(bankbalance2);
      }
      

      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bankbalance;
  }
  
  public List<productexpenses> getSumOfAmountFromProductexpenses(String hoa, String majorHeadId, String minorHeadId, String subHeadId, String fromDate, String toDate, String groupBy, String dayOf) {
    List<productexpenses> productexpenses = new ArrayList<productexpenses>();
    String subQuery = "";
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and pe.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadId != null) && (!minorHeadId.equals(""))) {
      subQuery = subQuery + " and pe.minorhead_id = '" + minorHeadId + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and pe.sub_head_id = '" + subHeadId + "'";
    }
    
//    String subGroupQuery = "";
//    if(groupBy != null && !groupBy.trim().equals("")){
//    	subGroupQuery = " group by "+groupBy;
//    }
  
    
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
//      String selectQuery = "SELECT pe.head_account_id, pe.major_head_id, pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total, " + dayOf + "(p.date) as extra10,"
//      		+ " pe.expinv_id, pe.narration FROM productexpenses pe,payments p where p.extra3 = pe.expinv_id and " + 
//        "pe.head_account_id = '" + hoa + "' " + subQuery + " and pe.extra1='Payment Done' and p.date >= '" + fromDate + "' and p.date <= '" + toDate+ "' "
//        		+ "and p.extra3 in ( select extra3 from banktransactions where brs_date >= '"+fromDate+"' and brs_date <= '"+toDate+"')" + subGroupQuery  + " order by p.date";

      String selectQuery = "SELECT pe.head_account_id, pe.major_head_id, pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total, " + dayOf + "(pe.extra10) as extra10,"
        		+ " pe.expinv_id, pe.narration FROM productexpenses pe where pe.head_account_id = '" + hoa + "' " + subQuery + " and pe.extra1='Payment Done' and pe.extra10 >= '" + fromDate.split(" ")[0] + "' and pe.extra10 <= '" + toDate.split(" ")[0] + "' group by expinv_id order by pe.extra10";

      
//      System.out.println("getSumOfAmountFromProductexpenses 11111 ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      
      while (rs.next())
      {
    	  
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
		        productexpenses productexpenses2 = new productexpenses();
		        
		        productexpenses2.setamount(rs.getString("total"));
		        productexpenses2.sethead_account_id(rs.getString("head_account_id"));
		        productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
		        productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
		        productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
		        productexpenses2.setExtra10(rs.getString("extra10"));
		        productexpenses2.setexpinv_id(rs.getString("expinv_id"));
		        productexpenses2.setnarration(rs.getString("narration"));
		        productexpenses.add(productexpenses2);
    	  }
       
      }
      
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return productexpenses;
  }
  
  
  public List<bankdetails> getBankDetailsByHoaId(String hoaId){
	  List<bankdetails> bankdetailsList = new ArrayList<bankdetails>();
	  
	  try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	     
	      String selectQuery = "select * from bankdetails where headAccountId = '"+hoaId+"'";
	      
	      ResultSet rs = statement.executeQuery(selectQuery);
	      while(rs.next()){
	    	  
	    	  bankdetails bankdetails = new bankdetails();
	    	  bankdetails.setbank_id(rs.getString("bank_id"));
	    	  bankdetails.setbank_name(rs.getString("bank_name"));
	    	  
	    	  bankdetailsList.add(bankdetails);
	      }
	      
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	  
	 return bankdetailsList; 
	  
  }
  
  public List<customerpurchases> getJVAmountsFromcustomerpurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadId, String fromDate, String toDate, String cashType, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and extra6 = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and extra7 = '" + minorHeadID + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and productId = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectQuery = "SELECT extra1, extra6, extra7, productId, sum(totalAmmount) as total, " + dayOf + "(date) as extra20, billingId, cash_type, vocharNumber, extra2, quantity FROM customerpurchases where " + 
        "extra1 = '" + hoa + "' " + subQuery + " and cash_type='" + cashType + "' and date between '" + fromDate + "' and '" + toDate + "'" + subGroupQuery  + " order by date";
      
//      System.out.println("getJVAmountsFromcustomerpurchases JV DEBIT =======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
		        customerpurchases customerpurchases2 = new customerpurchases();
		        customerpurchases2.setextra1(rs.getString("extra1"));
		        customerpurchases2.setextra6(rs.getString("extra6"));
		        customerpurchases2.setextra7(rs.getString("extra7"));
		        customerpurchases2.setproductId(rs.getString("productId"));
		        customerpurchases2.settotalAmmount(rs.getString("total"));
		        customerpurchases2.setExtra20(rs.getString("extra20"));
		        customerpurchases2.setbillingId(rs.getString("billingId"));
		        customerpurchases2.setcash_type(rs.getString("cash_type"));
		        customerpurchases2.setvocharNumber(rs.getString("vocharNumber"));
		        customerpurchases2.setquantity(rs.getString("quantity"));
		        customerpurchases2.setextra2(rs.getString("extra2"));
		        customerpurchases.add(customerpurchases2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return customerpurchases;
  }
  

  public List<customerpurchases> getTotalAmountFromcustomerpurchaseswithoutOfferKind(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String cash_type, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and p.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and p.extra3 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and p.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT cp.extra1, p.major_head_id, p.extra3, cp.productId, sum(totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.extra2  FROM customerpurchases cp," + 
        "products p where cp.productId = p.productId and cp.extra1='" + hoa + "' and p.head_account_id = '"+hoa+"' " + subQuery + " and cp.date between '" + fromDate + "' and '" + toDate + "' " + 
        "and cash_type != '" + cash_type + "'" + subGroupQuery  + " order by cp.date";
      
//      System.out.println(" getTotalAmountFromcustomerpurchaseswithproduct ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
	        customerpurchases customerpurchases2 = new customerpurchases();
	        customerpurchases2.setextra1(rs.getString("extra1"));
	        customerpurchases2.setextra6(rs.getString("major_head_id"));
	        customerpurchases2.setextra7(rs.getString("extra3"));
	        customerpurchases2.setproductId(rs.getString("productId"));
	        customerpurchases2.settotalAmmount(rs.getString("total"));
	        customerpurchases2.setExtra20(rs.getString("extra20"));
	        customerpurchases2.setbillingId(rs.getString("billingId"));
	        customerpurchases2.setcash_type(rs.getString("cash_type"));
	        customerpurchases2.setextra2(rs.getString("extra2"));
	        
	        customerpurchases.add(customerpurchases2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return customerpurchases;
  }
  

  public List<customerpurchases> gettoatlAmountfromcustomerpurchaseswithsubhead(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(cp.totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.extra2 " + 
        "FROM customerpurchases cp, subhead s where cp.productId = s.sub_head_id " + subQuery + " and cp.extra1='" + hoa + "' and s.head_account_id = '"+hoa+"' " + 
        "and cp.date between '" + fromDate + "' and '" + toDate + "' and cash_type != '' " + subGroupQuery  + " order by cp.date";
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
	        customerpurchases customerpurchases2 = new customerpurchases();
	        customerpurchases2.setextra1(rs.getString("head_account_id"));
	        customerpurchases2.setextra6(rs.getString("major_head_id"));
	        customerpurchases2.setextra7(rs.getString("extra1"));
	        customerpurchases2.setproductId(rs.getString("sub_head_id"));
	        customerpurchases2.settotalAmmount(rs.getString("total"));
	        customerpurchases2.setExtra20(rs.getString("extra20"));
	        customerpurchases2.setbillingId(rs.getString("billingId"));
	        customerpurchases2.setcash_type(rs.getString("cash_type"));
	        customerpurchases2.setextra2(rs.getString("extra2"));
	        
	        customerpurchases.add(customerpurchases2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    

    return customerpurchases;
  }
  
  public List<productexpenses> getJVAmountsFromproductexpenses(String hoa, String majorHeadId, String minorHeadId, String subHeadId, String cashTyppe, String fromDate, String toDate, String groupBy, String dayOf, String creditOrDebit)
  {
    List<productexpenses> productexpenses = new ArrayList<productexpenses>();
    
    String subQuery = "";
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and pe.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadId != null) && (!minorHeadId.equals(""))) {
      subQuery = subQuery + " and pe.minorhead_id = '" + minorHeadId + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and pe.sub_head_id = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    String creditORDebitQuery = "";
//    if(creditOrDebit != null && !creditOrDebit.trim().equals("")){
//    	
//	    	if(creditOrDebit.equals("debit")){
//	    		creditORDebitQuery = " pe.extra5 = bt.extra3 and";
//	    	}else{
//	    		creditORDebitQuery = " pe.extra5 != bt.extra3 and";
//	    	}
//    	
//    }
    
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT pe.head_account_id, pe.major_head_id, pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total, " + dayOf + "(pe.entry_date) as extra10" + 
        " , pe.expinv_id, pe.extra5, pe.narration FROM productexpenses pe where "+creditORDebitQuery+" pe.head_account_id = '" + hoa + "' " + subQuery + " and pe.extra1='" + cashTyppe + "' "
        		+ "and pe.entry_date between '" + fromDate + "' and '" + toDate + "'" + subGroupQuery  + " order by pe.entry_date";
      
//      System.out.println("getJVAmountsFromproductexpenses JV CREDIT ====> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
	        productexpenses productexpenses2 = new productexpenses();
	        
	        productexpenses2.setamount(rs.getString("total"));
	        productexpenses2.sethead_account_id(rs.getString("head_account_id"));
	        productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
	        productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
	        productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
	        productexpenses2.setExtra10(rs.getString("extra10"));
	        productexpenses2.setexpinv_id(rs.getString("expinv_id"));
	        productexpenses2.setextra5(rs.getString("extra5"));
	        productexpenses2.setnarration(rs.getString("narration"));
	        
	        productexpenses.add(productexpenses2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return productexpenses;
  }
  

  public List<customerpurchases> getSumOfDollorAmountfromcustomerpurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String cashType, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
    
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(othercash_totalamount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.quantity, cp.extra2 FROM" + 
        " customerpurchases cp,subhead s where cp.productId = s.sub_head_id and  cp.extra1='" + hoa + "' and s.head_account_id = '"+hoa+"' " + subQuery + " and othercash_deposit_status='Deposited'" + 
        " and cp.extra16 between '" + fromDate + "' and '" + toDate + "' and cash_type = '" + cashType + "'" + subGroupQuery  + " order by cp.extra16";
      
//      System.out.println("getSumOfDollorAmountfromcustomerpurchases  ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
	        customerpurchases customerpurchases2 = new customerpurchases();
	        customerpurchases2.setextra1(rs.getString("head_account_id"));
	        customerpurchases2.setextra6(rs.getString("major_head_id"));
	        customerpurchases2.setextra7(rs.getString("extra1"));
	        customerpurchases2.setproductId(rs.getString("sub_head_id"));
	        customerpurchases2.settotalAmmount(rs.getString("total"));
	        customerpurchases2.setExtra20(rs.getString("extra20"));
	        customerpurchases2.setbillingId(rs.getString("billingId"));
	        customerpurchases2.setcash_type(rs.getString("cash_type"));
	        customerpurchases2.setquantity(rs.getString("quantity"));
	        customerpurchases2.setextra2(rs.getString("extra2"));
	
	        customerpurchases.add(customerpurchases2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    

    return customerpurchases;
  }
  
  public List<customerpurchases> getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
    
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
//      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.quantity, cp.extra2 FROM" + 
//        " customerpurchases cp,subhead s where cp.productId = s.sub_head_id and  cp.extra1='" + hoa + "' and s.head_account_id = '"+hoa+"' " + subQuery + " and cp.date between '" + fromDate + "' and '" + toDate + "' " + 
//        " and ( cash_type = 'card' or cash_type = 'cash' or cash_type = 'cheque' or cash_type = 'online success' ) "+ subGroupQuery + " order by cp.date";
//      
      
      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.quantity, cp.extra2 FROM" + 
    	        " customerpurchases cp,subhead s where cp.productId = s.sub_head_id and  cp.extra1='" + hoa + "' and s.head_account_id = '"+hoa+"' " + subQuery + " and cp.date between '" + fromDate + "' and '" + toDate + "' " + 
    	        " and ( cash_type = 'cash' ) "+ subGroupQuery + " order by cp.date";
    	      
      
//      System.out.println("getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
		        customerpurchases customerpurchases2 = new customerpurchases();
		        customerpurchases2.setextra1(rs.getString("head_account_id"));
		        customerpurchases2.setextra6(rs.getString("major_head_id"));
		        customerpurchases2.setextra7(rs.getString("extra1"));
		        customerpurchases2.setproductId(rs.getString("sub_head_id"));
		        customerpurchases2.settotalAmmount(rs.getString("total"));
		        customerpurchases2.setExtra20(rs.getString("extra20"));
		        customerpurchases2.setbillingId(rs.getString("billingId"));
		        customerpurchases2.setcash_type(rs.getString("cash_type"));
		        customerpurchases2.setquantity(rs.getString("quantity"));
		        customerpurchases2.setextra2(rs.getString("extra2"));
		        
		        customerpurchases.add(customerpurchases2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return customerpurchases;
  }
  
  public List<customerpurchases> getJVCreditAmountFromCustomerPurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String groupBy, String dayOf){
	  List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
	    
	    String subQuery = "";
	    
	    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
	      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
	    }
	    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
	      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
	    }
	    if ((subHeadID != null) && (!subHeadID.equals(""))) {
	      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
	    }
	    
	    String subGroupQuery = "";
	    if(groupBy != null && !groupBy.trim().equals("")){
	    	subGroupQuery = " group by "+groupBy;
	    }
	    
	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
	      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.extra2 FROM" + 
	        " customerpurchases cp,subhead s where cp.productId = s.sub_head_id and  cp.extra1='" + hoa + "' and s.head_account_id = '"+hoa+"' " + subQuery + " and cp.date between '" + fromDate + "' and '" + toDate + "' " + 
	        " and cash_type = 'journalvoucher' and extra9 like 'BNK%' "+ subGroupQuery + " order by cp.date";
	      
//	      System.out.println("getJVCreditAmountFromCustomerPurchases ======> " + selectQuery);
	      
	      ResultSet rs = statement.executeQuery(selectQuery);
	      while (rs.next())
	      {
	    	  String total = rs.getString("total");
	    	  if(total != null && !total.trim().equals("")){
			        customerpurchases customerpurchases2 = new customerpurchases();
			        customerpurchases2.setextra1(rs.getString("head_account_id"));
			        customerpurchases2.setextra6(rs.getString("major_head_id"));
			        customerpurchases2.setextra7(rs.getString("extra1"));
			        customerpurchases2.setproductId(rs.getString("sub_head_id"));
			        customerpurchases2.settotalAmmount(rs.getString("total"));
			        customerpurchases2.setExtra20(rs.getString("extra20"));
			        customerpurchases2.setbillingId(rs.getString("billingId"));
			        customerpurchases2.setcash_type(rs.getString("cash_type"));
			        customerpurchases2.setextra2(rs.getString("extra2"));
			        
			        customerpurchases.add(customerpurchases2);
	    	  }
	      }
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	    return customerpurchases;
  }
  
  
  public List<customerpurchases> getWithBankJVAmountsFromcustomerpurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadId, String fromDate, String toDate, String cashType, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and c.extra6 = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and c.extra7 = '" + minorHeadID + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and c.productId = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectQuery = "SELECT c.extra1, c.extra6, c.extra7, c.productId, sum(c.totalAmmount) as total, " + dayOf + "(c.date) as extra20, c.billingId, c.cash_type, c.vocharNumber, c.extra2, c.quantity FROM customerpurchases c where " + 
        "extra1 = '" + hoa + "' " + subQuery + " and cash_type='" + cashType + "' and date between '" + fromDate + "' and '" + toDate + "' and c.vocharNumber in (SELECT extra3 FROM banktransactions where extra4 = '"+cashType+"' and date between '"+fromDate+"' and '"+toDate+"') " + subGroupQuery  + " order by date";
      
//      System.out.println("getWithBankJVAmountsFromcustomerpurchases JV DEBIT =======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
		        customerpurchases customerpurchases2 = new customerpurchases();
		        customerpurchases2.setextra1(rs.getString("extra1"));
		        customerpurchases2.setextra6(rs.getString("extra6"));
		        customerpurchases2.setextra7(rs.getString("extra7"));
		        customerpurchases2.setproductId(rs.getString("productId"));
		        customerpurchases2.settotalAmmount(rs.getString("total"));
		        customerpurchases2.setExtra20(rs.getString("extra20"));
		        customerpurchases2.setbillingId(rs.getString("billingId"));
		        customerpurchases2.setcash_type(rs.getString("cash_type"));
		        customerpurchases2.setvocharNumber(rs.getString("vocharNumber"));
		        customerpurchases2.setquantity(rs.getString("quantity"));
		        customerpurchases2.setextra2(rs.getString("extra2"));
		        customerpurchases.add(customerpurchases2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return customerpurchases;
  }
  
  public List<productexpenses> getWithBankJVAmountsFromproductexpenses(String hoa, String majorHeadId, String minorHeadId, String subHeadId, String cashTyppe, String fromDate, String toDate, String groupBy, String dayOf, String creditOrDebit)
  {
    List<productexpenses> productexpenses = new ArrayList<productexpenses>();
    
    String subQuery = "";
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and pe.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadId != null) && (!minorHeadId.equals(""))) {
      subQuery = subQuery + " and pe.minorhead_id = '" + minorHeadId + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and pe.sub_head_id = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT pe.head_account_id, pe.major_head_id, pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total, " + dayOf + "(pe.entry_date) as extra10" + 
        " , pe.expinv_id, pe.extra5, pe.narration FROM productexpenses pe where pe.head_account_id = '" + hoa + "' " + subQuery + " and pe.extra1='" + cashTyppe + "' "
        		+ "and pe.entry_date between '" + fromDate + "' and '" + toDate + "' and pe.extra5 in (SELECT extra3 FROM banktransactions where extra4 = '"+cashTyppe+"' and date between '"+fromDate+"' and '"+toDate+"') " + subGroupQuery  + " order by pe.entry_date";
      
//      System.out.println("getWithBankJVAmountsFromproductexpenses JV CREDIT ====> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
	        productexpenses productexpenses2 = new productexpenses();
	        
	        productexpenses2.setamount(rs.getString("total"));
	        productexpenses2.sethead_account_id(rs.getString("head_account_id"));
	        productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
	        productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
	        productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
	        productexpenses2.setExtra10(rs.getString("extra10"));
	        productexpenses2.setexpinv_id(rs.getString("expinv_id"));
	        productexpenses2.setextra5(rs.getString("extra5"));
	        productexpenses2.setnarration(rs.getString("narration"));
	        
	        productexpenses.add(productexpenses2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return productexpenses;
  }
  
  public double getCardAndOnlineSalesBasedOnHeadOfAccount(String hoaid, String fromDate, String toDate){

//	    List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
	    
	    
	    double amount = 0.0;
	    
	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
	      String selectQuery = "SELECT sum(totalAmmount) as total FROM" + 
	        " customerpurchases cp where  cp.extra1='" + hoaid + "' and cp.date between '" + fromDate + "' and '" + toDate + "' and ( cash_type = 'card' or cash_type = 'online success' ) order by cp.date";
	      
//	      System.out.println("getCardAndOnlineSalesBasedOnHeadOfAccount ======> " + selectQuery);
	      
	      ResultSet rs = statement.executeQuery(selectQuery);
	      while (rs.next())
	      {
	    	  String total = rs.getString("total");
	    	  if(total != null && !total.trim().equals("")){
	    		  amount = Double.parseDouble(total);
//			        customerpurchases customerpurchases2 = new customerpurchases();
//			        customerpurchases2.setextra1(rs.getString("head_account_id"));
//			        customerpurchases2.setextra6(rs.getString("major_head_id"));
//			        customerpurchases2.setextra7(rs.getString("extra1"));
//			        customerpurchases2.setproductId(rs.getString("sub_head_id"));
//			        customerpurchases2.settotalAmmount(rs.getString("total"));
//			        customerpurchases2.setExtra20(rs.getString("extra20"));
//			        customerpurchases2.setbillingId(rs.getString("billingId"));
//			        customerpurchases2.setcash_type(rs.getString("cash_type"));
//			        customerpurchases2.setquantity(rs.getString("quantity"));
//			        customerpurchases2.setextra2(rs.getString("extra2"));
			        
//			        customerpurchases.add(customerpurchases2);
	    	  }
	      }
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	    return amount;
	  
  }
  
  
  public List<customerpurchases> getcardChequeJvOnlineSuccessAmountFromcustomerpurchasesByDepositedDate(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList<customerpurchases>();
    
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if(groupBy != null && !groupBy.trim().equals("")){
    	subGroupQuery = " group by "+groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.quantity, cp.extra2 FROM" + 
        " customerpurchases cp,subhead s where cp.productId = s.sub_head_id and  cp.extra1='" + hoa + "' and s.head_account_id = '"+hoa+"' " + subQuery + " and cp.depositedDate between '" + fromDate + "' and '" + toDate + "' " + 
        " and ( cash_type = 'card' or cash_type = 'cheque' or cash_type = 'online success' ) "+ subGroupQuery + " order by cp.date";
      
//      System.out.println("getcardChequeJvOnlineSuccessAmountFromcustomerpurchasesByDepositedDate ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
    	  String total = rs.getString("total");
    	  if(total != null && !total.trim().equals("")){
		        customerpurchases customerpurchases2 = new customerpurchases();
		        customerpurchases2.setextra1(rs.getString("head_account_id"));
		        customerpurchases2.setextra6(rs.getString("major_head_id"));
		        customerpurchases2.setextra7(rs.getString("extra1"));
		        customerpurchases2.setproductId(rs.getString("sub_head_id"));
		        customerpurchases2.settotalAmmount(rs.getString("total"));
		        customerpurchases2.setExtra20(rs.getString("extra20"));
		        customerpurchases2.setbillingId(rs.getString("billingId"));
		        customerpurchases2.setcash_type(rs.getString("cash_type"));
		        customerpurchases2.setquantity(rs.getString("quantity"));
		        customerpurchases2.setextra2(rs.getString("extra2"));
		        
		        customerpurchases.add(customerpurchases2);
    	  }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return customerpurchases;
  }
  
  
  
  
  
  public List<godwanstock> getVendorPendeingPayment(String hoaid, String vendorId, String fromDate, String toDate, String groupBy){
	  
	  List<godwanstock> godwanstocksList = new ArrayList<godwanstock>();
	  
	  String subQuery1 = "";
	  String subQuery = "";
	  
	  
	  String notComeVendor = " and vendorId != '1001' and vendorId != '1153' and vendorId != '1164' and vendorId != '1165' and vendorId != '1201' "
	  		+ "and vendorId != '1202' and vendorId != '1218' and vendorId != '1219' and vendorId != '1220' and vendorId != '1221' and vendorId != '1222'"
	  		+ " and vendorId != '1223' and vendorId != '1224' and vendorId != '1225' and vendorId != '1226' and vendorId != '1227' ";
	  
	  
	  if(vendorId != null && !vendorId.trim().equals("")){
		  subQuery = " and vendorId = '"+vendorId+"' ";
	  }
	    
	  
	    if(groupBy != null && !groupBy.trim().equals("")){
	    	subQuery1 = " group by "+groupBy;
	    }
	    
	    
	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
		    String selectQuery = "select department, majorhead_id, minorhead_id, productId, extra16, extra1, vendorId from godwanstock where date between '"+fromDate+"' "
		    		+ "and '"+toDate+"' and extra1 like '%MSE%' "+subQuery+" "+notComeVendor+" and department = '"+hoaid+"' and extra1 not in "
		    				+ " (select extra5 from productexpenses where entry_date between '"+fromDate+"' and '"+toDate+"')  "+subQuery1;
		    
		   // select * from godwanstock where date between '2017-04-01 00:00:00' and '2017-04-04 23:59:59' and extra1 like '%MSE%' 
		  //  and extra1 not in (select extra5 from productexpenses where entry_date between '2017-04-01 00:00:00' and '2017-04-04 23:59:59')
		    
		    
		//    select * from godwanstock where date between '2017-04-01 00:00:00' and '2017-04-04 23:59:59' and extra1 like '%MSE%' 
		//    and extra1  in (select extra5 from productexpenses where entry_date between '2017-04-01 00:00:00' and '2017-04-04 23:59:59' 
		//    		and extra5 like '%MSE%' and expinv_id not in (select extra3 from payments where date between '2017-04-01 00:00:00' and '2017-04-04 23:59:59' ))
		    
//		    System.out.println("getVendorPendeingPayment 1111 ====> "+selectQuery);
		    
		    ResultSet rs = statement.executeQuery(selectQuery);
		      while (rs.next()){
		    	  godwanstock godwanstock = new godwanstock();
		    	  godwanstock.setvendorId(rs.getString("vendorId"));
		    	  godwanstock.setextra1(rs.getString("extra1"));
		    	  godwanstock.setdepartment(rs.getString("department"));
		    	  godwanstock.setMajorhead_id(rs.getString("majorhead_id"));
		    	  godwanstock.setMinorhead_id(rs.getString("minorhead_id"));
		    	  godwanstock.setproductId(rs.getString("productId"));
		    	  godwanstock.setExtra16(rs.getString("extra16"));
		    	  godwanstocksList.add(godwanstock);
		      }
		      
		      selectQuery = "select * from godwanstock where date between '"+fromDate+"' and '"+toDate+"' and extra1 like '%MSE%' "+subQuery+" "
		      		+ " "+notComeVendor+" and department = '"+hoaid+"' and extra1  in "
		      		+ " (select extra5 from productexpenses where entry_date between '"+fromDate+"' and '"+toDate+"' and head_account_id = '"+hoaid+"' and extra5 like '%MSE%' "
		      				+ " and expinv_id not in (select extra3 from payments where date between '"+fromDate+"' and '"+toDate+"' )) "+subQuery1;
//		     
		      
//		      selectQuery = "select * from godwanstock where date between '"+fromDate+"' and '"+toDate+"' and extra1 like '%MSE%' "+subQuery+" "
//			      		+ " "+notComeVendor+" and department = '"+hoaid+"' and extra1  in "
//			      		+ " (select extra5 from productexpenses where entry_date between '"+fromDate+"' and '"+toDate+"' and head_account_id = '"+hoaid+"' and extra5 like '%MSE%' "
//			      				+ " and expinv_id not in (SELECT extra3 FROM banktransactions where brs_date >= '"+fromDate.split(" ")[0]+"' and  brs_date <= '"+toDate.split(" ")[0]+"' )) "+subQuery1;
//			     
		      
//		      System.out.println("getVendorPendeingPayment 2222 ====> "+selectQuery);
		      ResultSet rs1 = statement.executeQuery(selectQuery);
		      while (rs1.next()){
		    	  godwanstock godwanstock = new godwanstock();
		    	  godwanstock.setvendorId(rs1.getString("vendorId"));
		    	  godwanstock.setextra1(rs1.getString("extra1"));
		    	  godwanstock.setdepartment(rs1.getString("department"));
		    	  godwanstock.setMajorhead_id(rs1.getString("majorhead_id"));
		    	  godwanstock.setMinorhead_id(rs1.getString("minorhead_id"));
		    	  godwanstock.setproductId(rs1.getString("productId"));
		    	  godwanstock.setExtra16(rs1.getString("extra16"));
		    	  godwanstocksList.add(godwanstock);
		      }
		      
		      
		      
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
		  
	  return godwanstocksList;
  }
  
  
  
  public List<banktransactions> getBankBalanceByBankIdAndBrsDate(String bankId, String brsFromDate, String brsToDate, String amountType, String depositeOrWithdraw){
	  
	  List<banktransactions> banktransactionsList = new ArrayList<banktransactions>();
	  
	  String subQuery = "";
	  
	  if(depositeOrWithdraw != null && (depositeOrWithdraw.equals("withdraw") || depositeOrWithdraw.equals("deposit"))){
		  if(depositeOrWithdraw.equals("withdraw")){
			  if(amountType != null && !amountType.trim().equals("")){
						if(amountType.equals("cashpaid")){
							subQuery = " and (type = 'cashpaid')";
						}else{
							subQuery = " and (type = 'cheque' or type = 'transfer' or type = 'withdraw')";
						}
			  }else{
				  subQuery = " and (type = 'cheque' or type = 'transfer' or type = 'withdraw')";
			  }
		  }else{
			  if(amountType != null && !amountType.trim().equals("")){
					if(amountType.equals("cashpaid")){
						subQuery = " and type = 'pettycash'";
					}else{
						subQuery = " and type = 'deposit'";
					}
				}else{
					subQuery = " and type = 'deposit'";
				}
		  }
	  }else if(depositeOrWithdraw != null && depositeOrWithdraw.trim().equals("")){
		  
		  if(amountType != null && !amountType.equals("")){
			  
				if(amountType.equals("cashpaid")){
					subQuery = " and (type = 'cashpaid' || type = 'pettycash')";
				}else{
					subQuery = " and type != 'cashpaid' and type != 'pettycash'";	
				}
			}else{
				subQuery = " and type != 'cashpaid'  and type != 'pettycash'";
			}
		  
	  }
	  
	  try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	  
	      	String selectQuery = "select * from banktransactions where bank_id = '"+bankId+"' and brs_date >= '"+brsFromDate+"' and brs_date <= '"+brsToDate+"'"+subQuery+" order by brs_date";
	      	
//	      	 System.out.println("getBankBalanceByBankIdAndBrsDate =====> "+selectQuery);
	      	
	      	 ResultSet rs = statement.executeQuery(selectQuery);
	      	 
	      	
	      	 
	      	 while(rs.next()){
	      		 
	      		banktransactions banktransactions = new banktransactions();
	      		
	      		banktransactions.setbanktrn_id(rs.getString("banktrn_id"));
	      		banktransactions.setbank_id(rs.getString("bank_id"));
	      		banktransactions.setnarration(rs.getString("narration"));
	      		banktransactions.setdate(rs.getString("date"));
	      		banktransactions.setamount(rs.getString("amount"));
	      		banktransactions.settype(rs.getString("type"));
	      		banktransactions.setextra3(rs.getString("extra3"));
	      		banktransactions.setextra4(rs.getString("extra4"));
	      		banktransactions.setbrs_date(rs.getString("brs_date"));
	      		banktransactions.setHead_account_id(rs.getString("head_account_id"));
	      		banktransactions.setMajor_head_id(rs.getString("major_head_id"));
	      		banktransactions.setMinor_head_id(rs.getString("minor_head_id"));
	      		banktransactions.setSub_head_id(rs.getString("sub_head_id"));
	      		
	      		banktransactionsList.add(banktransactions);
	      		
	      	 }
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	  
	  return banktransactionsList;
  }
  
  public bankbalance getBankOpeningBalanceByBankId(String bankId, String type, String finYear){
	  
	  bankbalance bankbalance = new bankbalance();
	  
	  String newType = "";
	  
		if(type.equals("bank"))
		{
			newType = "Bank";
		}
		else if(type.equals("cashpaid"))
		{
			newType = "Petty Cash";
		}
		  
		  try
		    {
		      con = ConnectionHelper.getConnection();
		      statement = con.createStatement();
		  
		      	String selectQuery = "SELECT * FROM bankbalance where  bank_id = '"+bankId+"' and type = '"+newType+"' and extra1 = '"+finYear+"'";
		      	
//		      	System.out.println("getBankOpeningBalanceByBankId =====> "+selectQuery);
		      	
		      	 ResultSet rs = statement.executeQuery(selectQuery);
		      	 
		      	 while(rs.next()){
		      		
		      		bankbalance.setBank_id(rs.getString("bank_id"));
		      		bankbalance.setType(rs.getString("type"));
		      		bankbalance.setBank_bal(rs.getString("bank_bal"));
		      		bankbalance.setBank_flag(rs.getString("bank_flag"));
		      		bankbalance.setCreatedDate(rs.getString("createdDate"));
		      		bankbalance.setExtra1(rs.getString("extra1"));
		      		bankbalance.setExtra2(rs.getString("extra2"));
		      		bankbalance.setExtra3(rs.getString("extra3"));
		      		bankbalance.setExtra4(rs.getString("extra4"));
		      		bankbalance.setExtra5(rs.getString("extra5"));
		      		bankbalance.setExtra7(rs.getString("extra7"));
		      		
		      	 }
		      	
		    }
		    catch (Exception e) {
		      e.printStackTrace();
		    }
	  return bankbalance;
  }
  
  public List<godwanstock> getMseEntrysByDateAndHoaid(String hoaId, String vendorId, String fromDate, String toDate, String groupBy){
	  
 List<godwanstock> godwanstocksList = new ArrayList<godwanstock>();
	  
	  String subQuery1 = "";
	  String subQuery = "";
	  
	  
	  String notComeVendor = " and vendorId != '1001' and vendorId != '1153' and vendorId != '1164' and vendorId != '1165' and vendorId != '1201' "
	  		+ "and vendorId != '1202' and vendorId != '1218' and vendorId != '1219' and vendorId != '1220' and vendorId != '1221' and vendorId != '1222'"
	  		+ " and vendorId != '1223' and vendorId != '1224' and vendorId != '1225' and vendorId != '1226' and vendorId != '1227' ";
	  
	  
	  if(vendorId != null && !vendorId.trim().equals("")){
		  subQuery = " and vendorId = '"+vendorId+"' ";
	  }
	    
	  
	    if(groupBy != null && !groupBy.trim().equals("")){
	    	subQuery1 = " group by "+groupBy;
	    }
	    
	    
	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
		    String selectQuery = "select department, majorhead_id, minorhead_id, productId, extra16, extra1, vendorId, extra21 from godwanstock where date between '"+fromDate+"' "
		    		+ "and '"+toDate+"' and extra1 like '%MSE%' "+subQuery+" "+notComeVendor+" and department = '"+hoaId+"' "+subQuery1;
		    
//		    System.out.println("getMseEntrysByDateAndHoaid ====> "+selectQuery);
		    
		    ResultSet rs = statement.executeQuery(selectQuery);
		      while (rs.next()){
		    	  godwanstock godwanstock = new godwanstock();
		    	  godwanstock.setvendorId(rs.getString("vendorId"));
		    	  godwanstock.setextra1(rs.getString("extra1"));
		    	  godwanstock.setdepartment(rs.getString("department"));
		    	  godwanstock.setMajorhead_id(rs.getString("majorhead_id"));
		    	  godwanstock.setMinorhead_id(rs.getString("minorhead_id"));
		    	  godwanstock.setproductId(rs.getString("productId"));
		    	  godwanstock.setExtra16(rs.getString("extra16"));
		    	  godwanstock.setExtra21(rs.getString("extra21"));
		    	  godwanstocksList.add(godwanstock);
		      }
		      
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
		  
	  return godwanstocksList;
  }
  
  public List<productexpenses> getproductexpensesListByMseId(String fromDate, String toDate, String MseIdsSubQuery){
	  
	  List<productexpenses> productexpensesList = new ArrayList<productexpenses>();
	  
	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
	      String selectQuery = "select * from productexpenses where entry_date between '"+fromDate+"' and '"+toDate+"' "+MseIdsSubQuery+" group by extra5";
	      
//	      System.out.println("getproductexpensesListByMseId ===== > "+selectQuery);
	      
	      ResultSet rs = statement.executeQuery(selectQuery);
	      while (rs.next()){
		      productexpenses productexpenses2 = new productexpenses();
		        
		        productexpenses2.setamount(rs.getString("amount"));
		        productexpenses2.sethead_account_id(rs.getString("head_account_id"));
		        productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
		        productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
		        productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
		        productexpenses2.setvendorId(rs.getString("vendorId"));
		        productexpenses2.setextra1(rs.getString("extra1"));
		        productexpenses2.setexpinv_id(rs.getString("expinv_id"));
		        productexpenses2.setnarration(rs.getString("narration"));
		        productexpensesList.add(productexpenses2);
	      }
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
		 
	  
	  return productexpensesList;
  }
  
  public List<banktransactions> getbanktransactionsByExpinv_idAndBRSDate(String fromDate, String toDate, String expinv_idSubQuery){
	  
	  List<banktransactions> banktransactionsList = new ArrayList<banktransactions>();
	  
	  try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
	      
	      String selectQuery = "select * from banktransactions where brs_date >= '"+fromDate+"' and brs_date <= '"+toDate+"' "+expinv_idSubQuery;
	      
//	      System.out.println("getbanktransactionsByExpinv_idAndBRSDate =====> "+selectQuery);
	      
	      ResultSet rs = statement.executeQuery(selectQuery);
	      while (rs.next()){
	    	  
	    	  banktransactions banktransactions = new banktransactions();
	    	  
	    	  banktransactions.setbanktrn_id(rs.getString("banktrn_id"));
	    	  banktransactions.setbank_id(rs.getString("bank_id"));
	    	  banktransactions.setname(rs.getString("name"));
	    	  banktransactions.setnarration(rs.getString("narration"));
	    	  banktransactions.setdate(rs.getString("date"));
	    	  banktransactions.setamount(rs.getString("amount"));
	    	  banktransactions.settype(rs.getString("type"));
	    	  banktransactions.setextra3(rs.getString("extra3"));
	    	  banktransactions.setextra5(rs.getString("extra5"));
	    	  banktransactions.setbrs_date(rs.getString("brs_date"));
	    	  
	    	  banktransactionsList.add(banktransactions);
	      }
	      
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	  
	  return banktransactionsList;
  }
  
  
  public List<godwanstock> getVendorPendingAmount(String fromDate, String toDate, String vendorId, String hoaId){
	  
	  
	  
List<godwanstock> godwanstocksList = new ArrayList<godwanstock>();
	  
	  String subQuery = "";
	  
	  
	  String notComeVendor = " and vendorId != '' and vendorId != '1001' and vendorId != '1153' and vendorId != '1164' and vendorId != '1165' and vendorId != '1201' "
	  		+ "and vendorId != '1202' and vendorId != '1218' and vendorId != '1219' and vendorId != '1220' and vendorId != '1221' and vendorId != '1222'"
	  		+ " and vendorId != '1223' and vendorId != '1224' and vendorId != '1225' and vendorId != '1226' and vendorId != '1227' ";
	  
	  
	  if(vendorId != null && !vendorId.trim().equals("")){
		  subQuery = " and vendorId = '"+vendorId+"' ";
	  }else{
		  subQuery = notComeVendor;
	  }
	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
		    String selectQuery = "select department, majorhead_id, minorhead_id, productId, extra16, extra1, vendorId, extra21 from godwanstock "
		    		+ "where department = '"+hoaId+"' and date between '"+fromDate+"' and '"+toDate+"' and brsDate not between '"+fromDate.split(" ")[0]+"'"
		    				+ " and '"+toDate.split(" ")[0]+"' and extra1 like '%MSE%' "+subQuery+" group by extra1 order by vendorId";
		    
//		    System.out.println("getVendorPendingAmount ====> "+selectQuery);
		    
		    ResultSet rs = statement.executeQuery(selectQuery);
		      while (rs.next()){
		    	  godwanstock godwanstock = new godwanstock();
		    	  godwanstock.setvendorId(rs.getString("vendorId"));
		    	  godwanstock.setextra1(rs.getString("extra1"));
		    	  godwanstock.setdepartment(rs.getString("department"));
		    	  godwanstock.setMajorhead_id(rs.getString("majorhead_id"));
		    	  godwanstock.setMinorhead_id(rs.getString("minorhead_id"));
		    	  godwanstock.setproductId(rs.getString("productId"));
		    	  godwanstock.setExtra16(rs.getString("extra16"));
		    	  godwanstock.setExtra21(rs.getString("extra21"));
		    	  godwanstocksList.add(godwanstock);
		      }
		      
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	    List<godwanstock> godwanstocksList1 = new ArrayList<godwanstock>();
	    
	    if(godwanstocksList != null && godwanstocksList.size() > 0){
	    	
	    	String vendorId1 = "";
	    	double totalAmount = 0.0;
	    	godwanstock godwanstock1 = new godwanstock();
	    	for(int i= 0 ; i < godwanstocksList.size(); i++){
	    		
	    		godwanstock godwanstock = godwanstocksList.get(i);
	    		
	    		
	    		
	    		if(vendorId1.equals("")){
	    			vendorId1 = godwanstock.getvendorId().trim();
	    			totalAmount = Double.parseDouble(godwanstock.getExtra16()); 
	    		}else if(vendorId1.equals(godwanstock.getvendorId().trim())){
	    			totalAmount = totalAmount + Double.parseDouble(godwanstock.getExtra16());
	    		}else{
	    			godwanstock1.setExtra16(""+totalAmount);
	    			totalAmount = 0.0;
	    			godwanstocksList1.add(godwanstock1);
	    			vendorId1 = godwanstock.getvendorId().trim();
	    			totalAmount = Double.parseDouble(godwanstock.getExtra16());
	    		}
	    		
	    		godwanstock1 = godwanstock;
	    		
	    		if(i == (godwanstocksList.size() - 1)){
	    			godwanstock1.setExtra16(""+totalAmount);
	    			totalAmount = 0.0;
	    			godwanstocksList1.add(godwanstock1);
	    		}
	    		
	    	}
	    	
	    }
	    
	    VendorsAndPayments1 vendorsAndPayments = new VendorsAndPayments1();
	    List<vendors> vendorList = vendorsAndPayments.getAllVendors();
	    
	    if(godwanstocksList1 != null && godwanstocksList1.size() > 0){
	    	
	    	for(int i = 0 ; i < godwanstocksList1.size(); i++){
	    		godwanstock godwanstock = godwanstocksList1.get(i);
	    		for(int j = 0 ; j < vendorList.size(); j++){
	    			vendors vendors = vendorList.get(j);
	    			if(godwanstock.getvendorId().trim().equals(vendors.getvendorId())){
	    				godwanstock.setMRNo(vendors.getagencyName());
	    				godwanstocksList1.set(i, godwanstock);
	    				break;
	    			}
	    		}
	    	}
	    }
	  return godwanstocksList1;
  }
  
  
  
  
  
  
  public List<godwanstock> getAllgodwanstockMse(String limitfrom, String limit){
	  
	  List<godwanstock> godwanstocksList = new ArrayList<godwanstock>();
	  
	  try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
		    String selectQuery = "select * from godwanstock where extra1 like '%MSE%' group by extra1 limit "+limitfrom+", "+limit+"";
		    
//		    System.out.println("getAllgodwanstockMse ====> "+selectQuery);
		    
		    ResultSet rs = statement.executeQuery(selectQuery);
		      while (rs.next()){
		    	  godwanstock godwanstock = new godwanstock();
		    	  godwanstock.setvendorId(rs.getString("vendorId"));
		    	  godwanstock.setextra1(rs.getString("extra1"));
		    	  godwanstock.setdepartment(rs.getString("department"));
		    	  godwanstock.setMajorhead_id(rs.getString("majorhead_id"));
		    	  godwanstock.setMinorhead_id(rs.getString("minorhead_id"));
		    	  godwanstock.setproductId(rs.getString("productId"));
		    	  godwanstock.setExtra16(rs.getString("extra16"));
		    	  godwanstock.setExtra21(rs.getString("extra21"));
		    	  godwanstock.setBanktrn_id(rs.getString("banktrn_id"));
		    	  godwanstock.setBrsDate(rs.getString("brsDate"));
		    	  godwanstock.setChecKPrintDate(rs.getString("checkPrintDate"));
		    	  
		    	  godwanstocksList.add(godwanstock);
		      }
		      
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
		  
	  return godwanstocksList;
  }
  
  public List<productexpenses> getAllproductexpensesMses(String MSEIDS){
	  
	  List<productexpenses> productexpensesList = new ArrayList<productexpenses>();
	  
	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
	      String selectQuery = "select * from productexpenses where "+MSEIDS+" group by extra5";
	      
	      System.out.println("getAllproductexpensesMses ===== > "+selectQuery);
	      
	      ResultSet rs = statement.executeQuery(selectQuery);
	      while (rs.next()){
		      productexpenses productexpenses2 = new productexpenses();
		        
		        productexpenses2.setamount(rs.getString("amount"));
		        productexpenses2.sethead_account_id(rs.getString("head_account_id"));
		        productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
		        productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
		        productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
		        productexpenses2.setvendorId(rs.getString("vendorId"));
		        productexpenses2.setextra5(rs.getString("extra5"));
		        productexpenses2.setexpinv_id(rs.getString("expinv_id"));
		        productexpenses2.setnarration(rs.getString("narration"));
		        productexpenses2.setExtra9(rs.getString("extra9"));
		        productexpenses2.setExtra10(rs.getString("extra10"));
		        productexpenses2.setBanktrn_id(rs.getString("banktrn_id"));
		        productexpensesList.add(productexpenses2);
	      }
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
		 
	  
	  return productexpensesList;
  }
  
 
  public int updateUniqueIdForMSE(String mseId, String expinv_id, String checkPrintDate, String brsDate, String bkt){

	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
	      String update = "update godwanstock set extra21 = '"+expinv_id+"', checkPrintDate = '"+checkPrintDate+"', brsDate = '"+brsDate+"', banktrn_id = '"+bkt+"' where extra1 = '"+mseId+"'";
	      
	      statement.executeUpdate(update);
	      statement.close();
	      con.close();
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	  
	  return 0;
  }
  
  
  
  public List<productexpenses> getproductexpensesList(String limitFrom, String limit){
	  List<productexpenses> productexpensesList = new ArrayList<productexpenses>();
	  
	    try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
	      String selectQuery = "select * from productexpenses group by expinv_id order by expinv_id limit "+limitFrom+", "+limit;
	      
//	      System.out.println("getAllproductexpensesMses ===== > "+selectQuery);
	      
	      ResultSet rs = statement.executeQuery(selectQuery);
	      while (rs.next()){
		      productexpenses productexpenses2 = new productexpenses();
		        
		        productexpenses2.setamount(rs.getString("amount"));
		        productexpenses2.sethead_account_id(rs.getString("head_account_id"));
		        productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
		        productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
		        productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
		        productexpenses2.setvendorId(rs.getString("vendorId"));
		        productexpenses2.setextra5(rs.getString("extra5"));
		        productexpenses2.setexpinv_id(rs.getString("expinv_id"));
		        productexpenses2.setnarration(rs.getString("narration"));
		        productexpenses2.setExtra9(rs.getString("extra9"));
		        productexpenses2.setExtra10(rs.getString("extra10"));
		        productexpenses2.setBanktrn_id(rs.getString("banktrn_id"));
		        productexpensesList.add(productexpenses2);
	      }
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
		 
	  
	  return productexpensesList;
  }
  
  
 public List<banktransactions> getbanktransactionsByExpinv_id(String expinv_idSubQuery){
	  
	  List<banktransactions> banktransactionsList = new ArrayList<banktransactions>();
	  
	  try
	    {
	      con = ConnectionHelper.getConnection();
	      statement = con.createStatement();
	      
	      
	      String selectQuery = "select * from banktransactions where "+expinv_idSubQuery+" order by extra3 ";
	      
//	      System.out.println("getbanktransactionsByExpinv_idAndBRSDate =====> "+selectQuery);
	      
	      ResultSet rs = statement.executeQuery(selectQuery);
	      while (rs.next()){
	    	  
	    	  banktransactions banktransactions = new banktransactions();
	    	  
	    	  banktransactions.setbanktrn_id(rs.getString("banktrn_id"));
	    	  banktransactions.setbank_id(rs.getString("bank_id"));
	    	  banktransactions.setname(rs.getString("name"));
	    	  banktransactions.setnarration(rs.getString("narration"));
	    	  banktransactions.setdate(rs.getString("date"));
	    	  banktransactions.setamount(rs.getString("amount"));
	    	  banktransactions.settype(rs.getString("type"));
	    	  banktransactions.setextra3(rs.getString("extra3"));
	    	  banktransactions.setextra5(rs.getString("extra5"));
	    	  banktransactions.setbrs_date(rs.getString("brs_date"));
	    	  
	    	  banktransactionsList.add(banktransactions);
	      }
	      
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	  
	  return banktransactionsList;
  }
 
 
	 public int updateproductexpenses(String expinv_id, String paymentDate, String brs_date, String bkt){
		 
		 try
		    {
		      con = ConnectionHelper.getConnection();
		      statement = con.createStatement();
		      
		      String update = "update productexpenses set extra9 = '"+paymentDate+"', extra10 = '"+brs_date+"', banktrn_id = '"+bkt+"' where expinv_id = '"+expinv_id+"'";
		 
		      statement.executeUpdate(update);
		      
		      statement.close();
		      con.close();
		      
		    }
		    catch (Exception e) {
		      e.printStackTrace();
		    }
		 return 0;
	 }
	  
  
}