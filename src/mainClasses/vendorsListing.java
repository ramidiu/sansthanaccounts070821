package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import beans.vendors;
public class vendorsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String vendorId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setvendorId(String vendorId)
{
this.vendorId = vendorId;
}
public String getvendorId(){
return(this.vendorId);
}
public List getvendors()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT vendorId,title,firstName,lastName,gothram,nomineeName,agencyName,address,city,pincode,phone,email,description,headAccountId,bankName,bankBranch,IFSCcode,accountNo,creditDays,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM vendors "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new vendors(rs.getString("vendorId"),rs.getString("title"),rs.getString("firstName"),rs.getString("lastName"),rs.getString("gothram"),rs.getString("nomineeName"),rs.getString("agencyName"),rs.getString("address"),rs.getString("city"),rs.getString("pincode"),rs.getString("phone"),rs.getString("email"),rs.getString("description"),rs.getString("headAccountId"),rs.getString("bankName"),rs.getString("bankBranch"),rs.getString("IFSCcode"),rs.getString("accountNo"),rs.getString("creditDays"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getActivevendors()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT vendorId,title,firstName,lastName,gothram,nomineeName,agencyName,address,city,pincode,phone,email,description,headAccountId,bankName,bankBranch,IFSCcode,accountNo,creditDays,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM vendors where extra6 = 'Active'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new vendors(rs.getString("vendorId"),rs.getString("title"),rs.getString("firstName"),rs.getString("lastName"),rs.getString("gothram"),rs.getString("nomineeName"),rs.getString("agencyName"),rs.getString("address"),rs.getString("city"),rs.getString("pincode"),rs.getString("phone"),rs.getString("email"),rs.getString("description"),rs.getString("headAccountId"),rs.getString("bankName"),rs.getString("bankBranch"),rs.getString("IFSCcode"),rs.getString("accountNo"),rs.getString("creditDays"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getDeactivevendors()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT vendorId,title,firstName,lastName,gothram,nomineeName,agencyName,address,city,pincode,phone,email,description,headAccountId,bankName,bankBranch,IFSCcode,accountNo,creditDays,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM vendors where extra6 = 'Deactive' "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new vendors(rs.getString("vendorId"),rs.getString("title"),rs.getString("firstName"),rs.getString("lastName"),rs.getString("gothram"),rs.getString("nomineeName"),rs.getString("agencyName"),rs.getString("address"),rs.getString("city"),rs.getString("pincode"),rs.getString("phone"),rs.getString("email"),rs.getString("description"),rs.getString("headAccountId"),rs.getString("bankName"),rs.getString("bankBranch"),rs.getString("IFSCcode"),rs.getString("accountNo"),rs.getString("creditDays"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMvendors(String vendorId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT vendorId,title,firstName,lastName,gothram,nomineeName,agencyName,address,city,pincode,phone,email,description,headAccountId,bankName,bankBranch,IFSCcode,accountNo,creditDays,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM vendors where  vendorId = '"+vendorId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new vendors(rs.getString("vendorId"),rs.getString("title"),rs.getString("firstName"),rs.getString("lastName"),rs.getString("gothram"),rs.getString("nomineeName"),rs.getString("agencyName"),rs.getString("address"),rs.getString("city"),rs.getString("pincode"),rs.getString("phone"),rs.getString("email"),rs.getString("description"),rs.getString("headAccountId"),rs.getString("bankName"),rs.getString("bankBranch"),rs.getString("IFSCcode"),rs.getString("accountNo"),rs.getString("creditDays"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMvendorsAgenciesName(String vendorId)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT agencyName FROM vendors where  vendorId = '"+vendorId+"'";
//System.out.println("vendors............"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("agencyName");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getVendorEmail(String vendorId)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT email FROM vendors where  vendorId = '"+vendorId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("email");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTINnum(String vendorId)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra3 FROM vendors where  vendorId = '"+vendorId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("extra3");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getGSTNum(String vendorId)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra8 FROM vendors where  vendorId = '"+vendorId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("extra8");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMvendorsSearch(String vendor,String catid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String hoa="";
	if(catid!=null){
		hoa= "and headAccountId='"+catid+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT vendorId,title,firstName,lastName,gothram,nomineeName,agencyName,address,city,pincode,phone,email,description,headAccountId,bankName,bankBranch,IFSCcode,accountNo,creditDays,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM vendors where (vendorId like '%"+vendor+"%' || agencyName like '%"+vendor+"%')";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new vendors(rs.getString("vendorId"),rs.getString("title"),rs.getString("firstName"),rs.getString("lastName"),rs.getString("gothram"),rs.getString("nomineeName"),rs.getString("agencyName"),rs.getString("address"),rs.getString("city"),rs.getString("pincode"),rs.getString("phone"),rs.getString("email"),rs.getString("description"),rs.getString("headAccountId"),rs.getString("bankName"),rs.getString("bankBranch"),rs.getString("IFSCcode"),rs.getString("accountNo"),rs.getString("creditDays"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public Map<String, vendors> getVendorsMap(){
	Map<String, vendors> vendorsMap = new HashMap<String, vendors>();
	String selectQuery = "select * from vendors";
	try {
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		ResultSet rs = s.executeQuery(selectQuery);
		
		while(rs.next()){
			vendorsMap.put(rs.getString("vendorId"),new vendors(rs.getString("vendorId"),rs.getString("title"),rs.getString("firstName"),rs.getString("lastName"),rs.getString("gothram"),rs.getString("nomineeName"),rs.getString("agencyName"),rs.getString("address"),rs.getString("city"),rs.getString("pincode"),rs.getString("phone"),rs.getString("email"),rs.getString("description"),rs.getString("headAccountId"),rs.getString("bankName"),rs.getString("bankBranch"),rs.getString("IFSCcode"),rs.getString("accountNo"),rs.getString("creditDays"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
		}
		
		rs.close();
		s.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return vendorsMap;
	
}
}