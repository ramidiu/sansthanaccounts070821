package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import dbase.sqlcon.SainivasConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.counters;
public class countersListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String counter_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setcounter_id(String counter_id)
{
this.counter_id = counter_id;
}
public String getcounter_id(){
return(this.counter_id);
}
public List getcounters()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT counter_id,counter_code,counter_name,extra1,extra2,extra3,extra4 FROM counters "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new counters(rs.getString("counter_id"),rs.getString("counter_code"),rs.getString("counter_name"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMcounters(String counter_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT counter_id,counter_code,counter_name,extra1,extra2,extra3,extra4 FROM counters where  counter_id = '"+counter_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new counters(rs.getString("counter_id"),rs.getString("counter_code"),rs.getString("counter_name"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getName(String counter_id)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT counter_name FROM counters where  counter_id = '"+counter_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("counter_name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getShrisainivasName(String counter_id)
{
String list = "";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT counter_name FROM counters where  counter_id = '"+counter_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("counter_name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}