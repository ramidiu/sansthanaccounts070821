package mainClasses;

import dbase.sqlcon.SainivasConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.productbill;
public class productbillListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String productbill_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setproductbill_id(String productbill_id)
{
this.productbill_id = productbill_id;
}
public String getproductbill_id(){
return(this.productbill_id);
}
public List getproductbill()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT productbill_id,total,discount,afterdiscount,login,tables,servicetax,servicecharges,createddate,status,remarks,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8 FROM productbill "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductbillwithbooking(String bookingid,String resid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT productbill_id,total,discount,afterdiscount,login,tables,servicetax,servicecharges,createddate,status,remarks,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8 FROM productbill where tables='"+bookingid+"' and extra3='"+resid+"' order by productbill_id ";
/*System.out.println("selectquery ="+selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductbillsfromtodate(String from,String to,String resid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT productbill_id,total,discount,afterdiscount,login,tables,servicetax,servicecharges,createddate,status,remarks,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8 FROM productbill where ( extra1 between '"+from+"'and '"+to+"') and extra3='"+resid+"'  order by extra1 asc ";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductbillsfromtodate(String to,String resid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT productbill_id,total,discount,afterdiscount,login,tables,servicetax,servicecharges,createddate,status,remarks,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8 FROM productbill where ( extra1 like '%"+to+"%') and extra3='"+resid+"' and status='success'  order by extra1 asc ";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductbillsfromtodatelike(String to,String resid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT  productbill_id,total,discount,afterdiscount,login,tables,servicetax,servicecharges,createddate,status,remarks,extra1,extra2,extra3,count(productbill_id) as  extra4,sum(afterdiscount) as extra5,extra6,extra7,extra8 FROM productbill where ( extra1 like '%"+to+"%')   and extra3='"+resid+"'   group  by productbill_id desc";
/*System.out.println(selectquery);*/

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getvalidproductbill(String prdbil_id,String res_Id)
{
String list ="";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM productbill where productbill_id='"+prdbil_id+"' and extra3='"+res_Id+"'"; 
//System.out.println("selectquery=="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 
	list="yes";
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPendingproductbill()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT b.productbill_id,b.total,b.discount,b.afterdiscount,b.login,b.tables,b.servicetax,b.servicecharges,b.createddate,b.status,s.waiterno as remarks,b.extra1,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8 FROM productbill b,productsale s where s.productbill_id=b.productbill_id and b.status='Pending' and b.extra3='RES101' group by b.productbill_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPendingproductbillGroupbyRoom()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT b.productbill_id,b.total,b.discount,b.afterdiscount,b.login,b.tables,b.servicetax,b.servicecharges,b.createddate,b.status,s.waiterno as remarks,b.extra1,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8 FROM productbill b,productsale s where s.productbill_id=b.productbill_id and b.status='Pending' group by s.waiterno"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPendingproductbillwithdate(String fromdate,String todate,String resid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT b.productbill_id,b.total,b.discount,b.afterdiscount,b.login,b.tables,b.servicetax,b.servicecharges,b.createddate,b.status,s.waiterno as remarks,b.extra1,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8 FROM productbill b,productsale s where s.productbill_id=b.productbill_id and b.status='success' and s.extra4='"+resid+"'and  b.extra1 between '"+fromdate+"' and '"+todate+"' group by b.productbill_id";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMproductbill(String productbill_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT productbill_id,total,discount,afterdiscount,login,tables,servicetax,servicecharges,createddate,status,remarks,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8 FROM productbill where  productbill_id = '"+productbill_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMproductbillofLogin(String fromdate,String todate,String userid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT productbill_id,total,discount,afterdiscount,login,tables,servicetax,servicecharges,createddate,status,remarks,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8 FROM productbill where  login = '"+userid+"' and   extra1 between '"+fromdate+"' and '"+todate+"' and  status='success' ";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productbill(rs.getString("productbill_id"),rs.getString("total"),rs.getString("discount"),rs.getString("afterdiscount"),rs.getString("login"),rs.getString("tables"),rs.getString("servicetax"),rs.getString("servicecharges"),rs.getString("createddate"),rs.getString("status"),rs.getString("remarks"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public boolean getValidBill(String productbill_id)
{
	boolean list =false;
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM productbill where  productbill_id = '"+productbill_id+"'"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list =true;

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public boolean getValidBookingid(String bookingid)
{
	boolean list =false;
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM productbill where  tables = '"+bookingid+"' and status='Pending' "; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list =true;

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getBillidforBookingid(String bookingid)
{
	String list ="";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM productbill where  tables = '"+bookingid+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list =rs.getString("productbill_id");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}