package mainClasses;

import beans.bankbalance;
import beans.banktransactions;
import beans.customerpurchases;
import beans.godwanstock;
import beans.majorhead;
import beans.minorhead;
import beans.productexpenses;
import beans.subhead;
import beans.vendors;
import dbase.sqlcon.ConnectionHelper;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ReportesListClass
{
  Connection con = null;
  Statement statement = null;
  
  public ReportesListClass() {}
  
  public List<majorhead> getMajorHeadsBasedOnHOA(String head_of_account) {
    List<majorhead> majorHeadList = new ArrayList();
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectquery = "SELECT major_head_id,name,description,head_account_id,extra1,extra2,extra3 FROM majorhead where  head_account_id = '" + head_of_account + "'";
      
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        majorHeadList.add(new majorhead(rs.getString("major_head_id"), rs.getString("name"), rs.getString("description"), rs.getString("head_account_id"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3")));
      }
      statement.close();
      rs.close();
      con.close();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return majorHeadList;
  }
  

  public List<majorhead> getMajorHeadesBasedOnNatcherAndHeadOfAccount(String head_of_account, String natcher)
  {
    List<majorhead> majorHeadList = new ArrayList();
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectquery = "SELECT * FROM majorhead where  head_account_id = '" + head_of_account + "' and extra2 = '" + natcher + "'";
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        majorHeadList.add(new majorhead(rs.getString("major_head_id"), rs.getString("name"), rs.getString("description"), rs.getString("head_account_id"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3")));
      }
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return majorHeadList;
  }
  
  public List<minorhead> getMinorHeadsByMajorHeadAndHeadOfAccountId(String head_of_account, String majorHeadId)
  {
    List<minorhead> minorheadList = new ArrayList();
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectquery = "SELECT * FROM minorhead where  head_account_id = '" + head_of_account + "' and major_head_id = '" + majorHeadId + "'";
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        minorheadList.add(new minorhead(rs.getString("minorhead_id"), rs.getString("name"), rs.getString("description"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3")));
      }
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return minorheadList;
  }
  
  public List<subhead> getSubheadsByMinorHeadAndMajorHeadAndHeadOfAccount(String head_of_account, String majorHeadId, String minorHeadId) {
    List<subhead> subheadList = new ArrayList();
    try {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectquery = "SELECT * FROM subhead where  head_account_id = '" + head_of_account + "' and major_head_id = '" + majorHeadId + "' and extra1 = '" + minorHeadId + "'";
      ResultSet rs = statement.executeQuery(selectquery);
      while (rs.next()) {
        subheadList.add(new subhead(rs.getString("sub_head_id"), rs.getString("name"), rs.getString("description"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3")));
      }
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return subheadList;
  }
  


  public List<banktransactions> getOtherDepositsAmountGroupBySubheadId(String type, String hoa, String majorHeadId, String minorheadId, String subHeadId, String fromDate, String toDate, String groupBy, String dayOf, String bankType)
  {
    List<banktransactions> banktransactions = new ArrayList();
    
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.trim().equals(""))) {
      subQuery = " and major_head_id = '" + majorHeadId + "'";
    }
    
    if ((minorheadId != null) && (!minorheadId.equals(""))) {
      subQuery = " and minor_head_id = '" + minorheadId + "'";
    }
    
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and sub_head_id = '" + subHeadId + "'";
    }
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    
    String loanBanks = "";
    if ((bankType != null) && (bankType.trim().equals("not a loan bank"))) {
      loanBanks = " and bank_id != 'BNK1021' and bank_id != 'BNK1022' and bank_id != 'BNK1024' and bank_id != 'BNK1025' ";
    } else if ((bankType != null) && (bankType.trim().equals("loan bank"))) {
      loanBanks = " and ( bank_id = 'BNK1021' or bank_id = 'BNK1022' or bank_id = 'BNK1024' or bank_id = 'BNK1025') ";
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectQuery = "SELECT sum(amount) as amount,banktrn_id,bank_id,name,narration, date(date) as date, amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5,brs_date, extra6, extra7, extra8, sub_head_id, minor_head_id, major_head_id, head_account_id, extra9, extra10, extra11, extra12, " + 
      
        dayOf + "(date) as extra13 FROM banktransactions where date >= '" + fromDate + " 00:00:00' and date <= '" + toDate + " 23:59:59' " + loanBanks + " and extra8 = '" + type + "' " + 
        "and head_account_id = '" + hoa + "'" + subQuery + subGroupQuery + " order by date";
      
     //  System.out.println("select query for other amount For Loan Bank is:::=====> " + selectQuery);
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next()) {
        banktransactions.add(new banktransactions(rs.getString("banktrn_id"), rs.getString("bank_id"), rs.getString("name"), rs.getString("narration"), rs.getString("date"), rs.getString("amount"), rs.getString("type"), rs.getString("createdBy"), rs.getString("editedBy"), rs.getString("extra1"), rs.getString("extra2"), rs.getString("extra3"), rs.getString("extra4"), rs.getString("extra5"), rs.getString("brs_date"), rs.getString("extra6"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("sub_head_id"), rs.getString("minor_head_id"), rs.getString("major_head_id"), rs.getString("head_account_id"), rs.getString("extra9"), rs.getString("extra10"), rs.getString("extra11"), rs.getString("extra12"), rs.getString("extra13")));
      }
      
      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    

    return banktransactions;
  }
  

  public List<bankbalance> getMajorOrMinorOrSubheadOpeningBal(String hoa, String majorHeadId, String minorHeadId, String subHeadId, String finYr, String assetsORliabilites, String debitORcredit, String groupBy, String dayOf)
  {
    List<bankbalance> bankbalance = new ArrayList();
    String subQuery = "";
    




    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and extra3 = '" + majorHeadId + "'";
    }
    if ((minorHeadId != null) && (!minorHeadId.equals(""))) {
      subQuery = subQuery + " and extra4 = '" + minorHeadId + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and extra5 = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT uniq_id, bank_id, type, sum(bank_bal) as bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9, " + 
        dayOf + "(createdDate) as extra10 FROM bankbalance where type = '" + assetsORliabilites + "' and extra1 = '" + finYr + "'" + 
        " and extra7 = '" + debitORcredit + "' and extra2 = '" + hoa + "'" + subQuery + subGroupQuery + " order by createdDate";
      
//       System.out.println("getMajorOrMinorOrSubheadOpeningBal ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        bankbalance bankbalance2 = new bankbalance();
        
        bankbalance2.setBank_bal(rs.getString("bank_bal"));
        bankbalance2.setExtra2(rs.getString("extra2"));
        bankbalance2.setExtra3(rs.getString("extra3"));
        bankbalance2.setExtra4(rs.getString("extra4"));
        bankbalance2.setExtra5(rs.getString("extra5"));
        bankbalance2.setExtra10(rs.getString("extra10"));
        
        bankbalance.add(bankbalance2);
      }
      

      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bankbalance;
  }
  
  public List<productexpenses> getSumOfAmountFromProductexpenses(String hoa, String majorHeadId, String minorHeadId, String subHeadId, String fromDate, String toDate, String groupBy, String dayOf) {
    List<productexpenses> productexpenses = new ArrayList();
    String subQuery = "";
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and pe.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadId != null) && (!minorHeadId.equals(""))) {
      subQuery = subQuery + " and pe.minorhead_id = '" + minorHeadId + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and pe.sub_head_id = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT pe.head_account_id, pe.major_head_id, pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total, " + dayOf + "(p.date) as extra10, pe.expinv_id, pe.narration FROM productexpenses pe,payments p where p.extra3 = pe.expinv_id and " + 
        "pe.head_account_id = '" + hoa + "' " + subQuery + " and pe.extra1='Payment Done' and p.date >= '" + fromDate + "' and p.date <= '" + toDate + "'" + subGroupQuery + " order by p.date";
      
       //System.out.println("getSumOfAmountFromProductexpenses ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {

        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          productexpenses productexpenses2 = new productexpenses();
          
          productexpenses2.setamount(rs.getString("total"));
          productexpenses2.sethead_account_id(rs.getString("head_account_id"));
          productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
          productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
          productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
          productexpenses2.setExtra10(rs.getString("extra10"));
          productexpenses2.setexpinv_id(rs.getString("expinv_id"));
          productexpenses2.setnarration(rs.getString("narration"));
          productexpenses.add(productexpenses2);
        }
      }
      

      statement.close();
      rs.close();
      con.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return productexpenses;
  }
  
  public List<customerpurchases> getJVAmountsFromcustomerpurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadId, String fromDate, String toDate, String cashType, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList();
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and extra6 = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and extra7 = '" + minorHeadID + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and productId = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectQuery = "SELECT extra1, extra6, extra7, productId, sum(totalAmmount) as total, " + dayOf + "(date) as extra20, billingId, cash_type, vocharNumber, extra2, quantity FROM customerpurchases where " + 
        "extra1 = '" + hoa + "' " + subQuery + " and cash_type='" + cashType + "' and date between '" + fromDate + "' and '" + toDate + "'" + subGroupQuery + " order by date";
      
       System.out.println("getJVAmountsFromcustomerpurchases JV DEBIT from majorhead print.568.! =======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          customerpurchases customerpurchases2 = new customerpurchases();
          customerpurchases2.setextra1(rs.getString("extra1"));
          customerpurchases2.setextra6(rs.getString("extra6"));
          customerpurchases2.setextra7(rs.getString("extra7"));
          customerpurchases2.setproductId(rs.getString("productId"));
          customerpurchases2.settotalAmmount(rs.getString("total"));
          customerpurchases2.setExtra20(rs.getString("extra20"));
          customerpurchases2.setbillingId(rs.getString("billingId"));
          customerpurchases2.setcash_type(rs.getString("cash_type"));
          customerpurchases2.setvocharNumber(rs.getString("vocharNumber"));
          customerpurchases2.setquantity(rs.getString("quantity"));
          customerpurchases2.setextra2(rs.getString("extra2"));
          customerpurchases.add(customerpurchases2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return customerpurchases;
  }
  

  public List<customerpurchases> getTotalAmountFromcustomerpurchaseswithoutOfferKind(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String cash_type, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList();
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and p.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and p.extra3 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and p.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT cp.extra1, p.major_head_id, p.extra3, cp.productId, sum(totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.extra2  FROM customerpurchases cp," + 
        "products p where cp.productId = p.productId and cp.extra1='" + hoa + "' and p.head_account_id = '" + hoa + "' " + subQuery + " and cp.date between '" + fromDate + "' and '" + toDate + "' " + 
        "and cash_type != '" + cash_type + "'" + subGroupQuery + " order by cp.date";
      
//       System.out.println(" getTotalAmountFromcustomerpurchaseswithproduct ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          customerpurchases customerpurchases2 = new customerpurchases();
          customerpurchases2.setextra1(rs.getString("extra1"));
          customerpurchases2.setextra6(rs.getString("major_head_id"));
          customerpurchases2.setextra7(rs.getString("extra3"));
          customerpurchases2.setproductId(rs.getString("productId"));
          customerpurchases2.settotalAmmount(rs.getString("total"));
          customerpurchases2.setExtra20(rs.getString("extra20"));
          customerpurchases2.setbillingId(rs.getString("billingId"));
          customerpurchases2.setcash_type(rs.getString("cash_type"));
          customerpurchases2.setextra2(rs.getString("extra2"));
          
          customerpurchases.add(customerpurchases2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return customerpurchases;
  }
  

  public List<customerpurchases> gettoatlAmountfromcustomerpurchaseswithsubhead(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList();
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(cp.totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.extra2 " + 
        "FROM customerpurchases cp, subhead s where cp.productId = s.sub_head_id " + subQuery + " and cp.extra1='" + hoa + "' and s.head_account_id = '" + hoa + "' " + 
        "and cp.date between '" + fromDate + "' and '" + toDate + "' and cash_type != '' " + subGroupQuery + " order by cp.date";
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          customerpurchases customerpurchases2 = new customerpurchases();
          customerpurchases2.setextra1(rs.getString("head_account_id"));
          customerpurchases2.setextra6(rs.getString("major_head_id"));
          customerpurchases2.setextra7(rs.getString("extra1"));
          customerpurchases2.setproductId(rs.getString("sub_head_id"));
          customerpurchases2.settotalAmmount(rs.getString("total"));
          customerpurchases2.setExtra20(rs.getString("extra20"));
          customerpurchases2.setbillingId(rs.getString("billingId"));
          customerpurchases2.setcash_type(rs.getString("cash_type"));
          customerpurchases2.setextra2(rs.getString("extra2"));
          
          customerpurchases.add(customerpurchases2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    

    return customerpurchases;
  }
  
  public List<productexpenses> getJVAmountsFromproductexpenses(String hoa, String majorHeadId, String minorHeadId, String subHeadId, String cashTyppe, String fromDate, String toDate, String groupBy, String dayOf, String creditOrDebit)
  {
    List<productexpenses> productexpenses = new ArrayList();
    
    String subQuery = "";
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and pe.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadId != null) && (!minorHeadId.equals(""))) {
      subQuery = subQuery + " and pe.minorhead_id = '" + minorHeadId + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and pe.sub_head_id = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    String creditORDebitQuery = "";

    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT pe.head_account_id, pe.major_head_id, pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total, " + dayOf + "(pe.entry_date) as extra10" + 
        " , pe.expinv_id, pe.extra5, pe.narration FROM productexpenses pe where " + creditORDebitQuery + " pe.head_account_id = '" + hoa + "' " + subQuery + " and pe.extra1='" + cashTyppe + "' " + 
        "and pe.entry_date between '" + fromDate + "' and '" + toDate + "'" + subGroupQuery + " order by pe.entry_date";
      
       //System.out.println("getJVAmountsFromproductexpenses JV CREDIT ====> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          productexpenses productexpenses2 = new productexpenses();
          
          productexpenses2.setamount(rs.getString("total"));
          productexpenses2.sethead_account_id(rs.getString("head_account_id"));
          productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
          productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
          productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
          productexpenses2.setExtra10(rs.getString("extra10"));
          productexpenses2.setexpinv_id(rs.getString("expinv_id"));
          productexpenses2.setextra5(rs.getString("extra5"));
          productexpenses2.setnarration(rs.getString("narration"));
          
          productexpenses.add(productexpenses2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return productexpenses;
  }
  

  public List<customerpurchases> getSumOfDollorAmountfromcustomerpurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String cashType, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList();
    
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(othercash_totalamount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.quantity, cp.extra2 FROM" + 
        " customerpurchases cp,subhead s where cp.productId = s.sub_head_id and  cp.extra1='" + hoa + "' and s.head_account_id = '" + hoa + "' " + subQuery + " and othercash_deposit_status='Deposited'" + 
        " and cp.extra16 between '" + fromDate + "' and '" + toDate + "' and cash_type = '" + cashType + "'" + subGroupQuery + " order by cp.date";
      
//      System.out.println("getSumOfDollorAmountfromcustomerpurchases  ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          customerpurchases customerpurchases2 = new customerpurchases();
          customerpurchases2.setextra1(rs.getString("head_account_id"));
          customerpurchases2.setextra6(rs.getString("major_head_id"));
          customerpurchases2.setextra7(rs.getString("extra1"));
          customerpurchases2.setproductId(rs.getString("sub_head_id"));
          customerpurchases2.settotalAmmount(rs.getString("total"));
          customerpurchases2.setExtra20(rs.getString("extra20"));
          customerpurchases2.setbillingId(rs.getString("billingId"));
          customerpurchases2.setcash_type(rs.getString("cash_type"));
          customerpurchases2.setquantity(rs.getString("quantity"));
          customerpurchases2.setextra2(rs.getString("extra2"));
          
          customerpurchases.add(customerpurchases2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    

    return customerpurchases;
  }
  
  public List<customerpurchases> getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList();
    
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.quantity, cp.extra2 FROM" + 
        " customerpurchases cp,subhead s where cp.productId = s.sub_head_id and  cp.extra1='" + hoa + "' and s.head_account_id = '" + hoa + "' " + subQuery + " and cp.date between '" + fromDate + "' and '" + toDate + "' " + 
        " and ( cash_type = 'card' or cash_type = 'cash' or cash_type = 'cheque' or cash_type = 'online success' or cash_type = 'googlepay' or cash_type = 'phonepe' ) " + subGroupQuery + " order by cp.date";
      
//       System.out.println("getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases ======> " + selectQuery);
     
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          customerpurchases customerpurchases2 = new customerpurchases();
          customerpurchases2.setextra1(rs.getString("head_account_id"));
          customerpurchases2.setextra6(rs.getString("major_head_id"));
          customerpurchases2.setextra7(rs.getString("extra1"));
          customerpurchases2.setproductId(rs.getString("sub_head_id"));
          customerpurchases2.settotalAmmount(rs.getString("total"));
          customerpurchases2.setExtra20(rs.getString("extra20"));
          customerpurchases2.setbillingId(rs.getString("billingId"));
          customerpurchases2.setcash_type(rs.getString("cash_type"));
          customerpurchases2.setquantity(rs.getString("quantity"));
          customerpurchases2.setextra2(rs.getString("extra2"));
          
          customerpurchases.add(customerpurchases2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return customerpurchases;
  }
  
  public List<customerpurchases> getJVCreditAmountFromCustomerPurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadID, String fromDate, String toDate, String groupBy, String dayOf) {
    List<customerpurchases> customerpurchases = new ArrayList();
    
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and s.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and s.extra1 = '" + minorHeadID + "'";
    }
    if ((subHeadID != null) && (!subHeadID.equals(""))) {
      subQuery = subQuery + " and s.sub_head_id = '" + subHeadID + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT s.head_account_id, s.major_head_id, s.extra1, s.sub_head_id, sum(totalAmmount) as total, " + dayOf + "(cp.date) as extra20, cp.billingId, cp.cash_type, cp.extra2 FROM" + 
        " customerpurchases cp,subhead s where cp.productId = s.sub_head_id and  cp.extra1='" + hoa + "' and s.head_account_id = '" + hoa + "' " + subQuery + " and cp.date between '" + fromDate + "' and '" + toDate + "' " + 
        " and cash_type = 'journalvoucher' and extra9 like 'BNK%' " + subGroupQuery + " order by cp.date";
      
      // System.out.println("getJVCreditAmountFromCustomerPurchases ======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          customerpurchases customerpurchases2 = new customerpurchases();
          customerpurchases2.setextra1(rs.getString("head_account_id"));
          customerpurchases2.setextra6(rs.getString("major_head_id"));
          customerpurchases2.setextra7(rs.getString("extra1"));
          customerpurchases2.setproductId(rs.getString("sub_head_id"));
          customerpurchases2.settotalAmmount(rs.getString("total"));
          customerpurchases2.setExtra20(rs.getString("extra20"));
          customerpurchases2.setbillingId(rs.getString("billingId"));
          customerpurchases2.setcash_type(rs.getString("cash_type"));
          customerpurchases2.setextra2(rs.getString("extra2"));
          
          customerpurchases.add(customerpurchases2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return customerpurchases;
  }
  

  public List<customerpurchases> getWithBankJVAmountsFromcustomerpurchases(String hoa, String majorHeadId, String minorHeadID, String subHeadId, String fromDate, String toDate, String cashType, String groupBy, String dayOf)
  {
    List<customerpurchases> customerpurchases = new ArrayList();
    String subQuery = "";
    
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and c.extra6 = '" + majorHeadId + "'";
    }
    if ((minorHeadID != null) && (!minorHeadID.equals(""))) {
      subQuery = subQuery + " and c.extra7 = '" + minorHeadID + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and c.productId = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    
    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      String selectQuery = "SELECT c.extra1, c.extra6, c.extra7, c.productId, sum(c.totalAmmount) as total, " + dayOf + "(c.date) as extra20, c.billingId, c.cash_type, c.vocharNumber, c.extra2, c.quantity FROM customerpurchases c where " + 
        "extra1 = '" + hoa + "' " + subQuery + " and cash_type='" + cashType + "' and date between '" + fromDate + "' and '" + toDate + "' and c.vocharNumber in (SELECT extra3 FROM banktransactions where extra4 = '" + cashType + "' and date between '" + fromDate + "' and '" + toDate + "') " + subGroupQuery + " order by date";
      
     //  System.out.println("getWithBankJVAmountsFromcustomerpurchases JV DEBIT =======> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          customerpurchases customerpurchases2 = new customerpurchases();
          customerpurchases2.setextra1(rs.getString("extra1"));
          customerpurchases2.setextra6(rs.getString("extra6"));
          customerpurchases2.setextra7(rs.getString("extra7"));
          customerpurchases2.setproductId(rs.getString("productId"));
          customerpurchases2.settotalAmmount(rs.getString("total"));
          customerpurchases2.setExtra20(rs.getString("extra20"));
          customerpurchases2.setbillingId(rs.getString("billingId"));
          customerpurchases2.setcash_type(rs.getString("cash_type"));
          customerpurchases2.setvocharNumber(rs.getString("vocharNumber"));
          customerpurchases2.setquantity(rs.getString("quantity"));
          customerpurchases2.setextra2(rs.getString("extra2"));
          customerpurchases.add(customerpurchases2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    return customerpurchases;
  }
  
  public List<productexpenses> getWithBankJVAmountsFromproductexpenses(String hoa, String majorHeadId, String minorHeadId, String subHeadId, String cashTyppe, String fromDate, String toDate, String groupBy, String dayOf, String creditOrDebit)
  {
    List<productexpenses> productexpenses = new ArrayList();
    
    String subQuery = "";
    if ((majorHeadId != null) && (!majorHeadId.equals(""))) {
      subQuery = " and pe.major_head_id = '" + majorHeadId + "'";
    }
    if ((minorHeadId != null) && (!minorHeadId.equals(""))) {
      subQuery = subQuery + " and pe.minorhead_id = '" + minorHeadId + "'";
    }
    if ((subHeadId != null) && (!subHeadId.equals(""))) {
      subQuery = subQuery + " and pe.sub_head_id = '" + subHeadId + "'";
    }
    
    String subGroupQuery = "";
    if ((groupBy != null) && (!groupBy.trim().equals(""))) {
      subGroupQuery = " group by " + groupBy;
    }
    


    try
    {
      con = ConnectionHelper.getConnection();
      statement = con.createStatement();
      
      String selectQuery = "SELECT pe.head_account_id, pe.major_head_id, pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total, " + dayOf + "(pe.entry_date) as extra10" + 
        " , pe.expinv_id, pe.extra5, pe.narration FROM productexpenses pe where pe.head_account_id = '" + hoa + "' " + subQuery + " and pe.extra1='" + cashTyppe + "' " + 
        "and pe.entry_date between '" + fromDate + "' and '" + toDate + "' and pe.extra5 in (SELECT extra3 FROM banktransactions where extra4 = '" + cashTyppe + "' and date between '" + fromDate + "' and '" + toDate + "') " + subGroupQuery + " order by pe.entry_date";
      
       //System.out.println("Product Expenses JV CREDIT ====> " + selectQuery);
      
      ResultSet rs = statement.executeQuery(selectQuery);
      while (rs.next())
      {
        String total = rs.getString("total");
        if ((total != null) && (!total.trim().equals(""))) {
          productexpenses productexpenses2 = new productexpenses();
          
          productexpenses2.setamount(rs.getString("total"));
          productexpenses2.sethead_account_id(rs.getString("head_account_id"));
          productexpenses2.setmajor_head_id(rs.getString("major_head_id"));
          productexpenses2.setminorhead_id(rs.getString("minorhead_id"));
          productexpenses2.setsub_head_id(rs.getString("sub_head_id"));
          productexpenses2.setExtra10(rs.getString("extra10"));
          productexpenses2.setexpinv_id(rs.getString("expinv_id"));
          productexpenses2.setextra5(rs.getString("extra5"));
          productexpenses2.setnarration(rs.getString("narration"));
          
          productexpenses.add(productexpenses2);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return productexpenses;
  }
  
  public List<godwanstock> getVendorPendingAmountOld(String fromDate, String toDate, String vendorId, String hoaId){
	  
	  
	  
	  List<godwanstock> godwanstocksList = new ArrayList<godwanstock>();
	  	  
	  	  String subQuery = "";
	  	  
	  	  String notComeVendor = " and vendorId != '' and vendorId != '1001' and vendorId != '1153' and vendorId != '1164' and vendorId != '1165' and vendorId != '1201' "
	  	  		+ "and vendorId != '1202' and vendorId != '1218' and vendorId != '1219' and vendorId != '1220' and vendorId != '1221' and vendorId != '1222'"
	  	  		+ " and vendorId != '1223' and vendorId != '1224' and vendorId != '1225' and vendorId != '1226' and vendorId != '1227' ";
	  	  
	  	  
	  	  if(vendorId != null && !vendorId.trim().equals("")){
	  		  subQuery = " and vendorId = '"+vendorId+"' ";
	  	  }else{
	  		  subQuery = notComeVendor;
	  	  }
	  	    try
	  	    {
	  	      con = ConnectionHelper.getConnection();
	  	      statement = con.createStatement();
	  	      
	  		    String selectQuery = "select department, date, majorhead_id, minorhead_id, productId, extra16, extra1, vendorId, extra21 from godwanstock "
	  		    		+ "where department = '"+hoaId+"' and date between '"+fromDate+"' and '"+toDate+"' and checKPrintDate not between '"+fromDate+"'"
	  		    				+ " and '"+toDate+"' and extra1 like '%MSE%' "+subQuery+" group by extra1 order by vendorId";
	  		    
//	  		    System.out.println("getVendorPendingAmount ====> "+selectQuery);
	  		    
	  		    ResultSet rs = statement.executeQuery(selectQuery);
	  		      while (rs.next()){
	  		    	  godwanstock godwanstock = new godwanstock();
	  		    	  godwanstock.setvendorId(rs.getString("vendorId"));
	  		    	  godwanstock.setextra1(rs.getString("extra1"));
	  		    	  godwanstock.setdate(rs.getString("date"));
	  		    	  godwanstock.setdepartment(rs.getString("department"));
	  		    	  godwanstock.setMajorhead_id(rs.getString("majorhead_id"));
	  		    	  godwanstock.setMinorhead_id(rs.getString("minorhead_id"));
	  		    	  godwanstock.setproductId(rs.getString("productId"));
	  		    	  godwanstock.setExtra16(rs.getString("extra16"));
	  		    	  godwanstock.setExtra21(rs.getString("extra21"));
	  		    	  godwanstocksList.add(godwanstock);
	  		      }
	  		      
	  	    }
	  	    catch (Exception e) {
	  	      e.printStackTrace();
	  	    }
	  	    List<godwanstock> godwanstocksList1 = new ArrayList<godwanstock>();
	  	    
	  	    if(godwanstocksList != null && godwanstocksList.size() > 0){
	  	    	
	  	    	String vendorId1 = "";
	  	    	double totalAmount = 0.0;
	  	    	godwanstock godwanstock1 = new godwanstock();
	  	    	for(int i= 0 ; i < godwanstocksList.size(); i++){
	  	    		
	  	    		godwanstock godwanstock = godwanstocksList.get(i);
	  	    		
	  	    		
	  	    		
	  	    		if(vendorId1.equals("")){
	  	    			vendorId1 = godwanstock.getvendorId().trim();
	  	    			totalAmount = Double.parseDouble(godwanstock.getExtra16()); 
	  	    		}else if(vendorId1.equals(godwanstock.getvendorId().trim())){
	  	    			totalAmount = totalAmount + Double.parseDouble(godwanstock.getExtra16());
	  	    		}else{
	  	    			godwanstock1.setExtra16(""+totalAmount);
	  	    			totalAmount = 0.0;
	  	    			godwanstocksList1.add(godwanstock1);
	  	    			vendorId1 = godwanstock.getvendorId().trim();
	  	    			totalAmount = Double.parseDouble(godwanstock.getExtra16());
	  	    		}
	  	    		
	  	    		godwanstock1 = godwanstock;
	  	    		
	  	    		if(i == (godwanstocksList.size() - 1)){
	  	    			godwanstock1.setExtra16(""+totalAmount);
	  	    			totalAmount = 0.0;
	  	    			godwanstocksList1.add(godwanstock1);
	  	    		}
	  	    		
	  	    	}
	  	    	
	  	    }
	  	    
	  	    VendorsAndPayments vendorsAndPayments = new VendorsAndPayments();
	  	    List<vendors> vendorList = vendorsAndPayments.getAllVendors();
	  	    
	  	    if(godwanstocksList1 != null && godwanstocksList1.size() > 0){
	  	    	
	  	    	for(int i = 0 ; i < godwanstocksList1.size(); i++){
	  	    		godwanstock godwanstock = godwanstocksList1.get(i);
	  	    		for(int j = 0 ; j < vendorList.size(); j++){
	  	    			vendors vendors = vendorList.get(j);
	  	    			if(godwanstock.getvendorId().trim().equals(vendors.getvendorId())){
	  	    				godwanstock.setMRNo(vendors.getagencyName());
	  	    				godwanstocksList1.set(i, godwanstock);
	  	    				break;
	  	    			}
	  	    		}
	  	    	}
	  	    }
	  	  return godwanstocksList1;
	    }
	    
 public List<godwanstock> getVendorSuccessOpeningEntrysAmount(String fromDate, String toDate, String vendorId, String hoaId){
	  
	  List<godwanstock> godwanstocksList = new ArrayList<godwanstock>();
	  	  
	  	  String subQuery = "";
	  	  
	  	  String notComeVendor = " and vendorId != '' and vendorId != '1001' and vendorId != '1153' and vendorId != '1164' and vendorId != '1165' and vendorId != '1201' "
	  	  		+ "and vendorId != '1202' and vendorId != '1218' and vendorId != '1219' and vendorId != '1220' and vendorId != '1221' and vendorId != '1222'"
	  	  		+ " and vendorId != '1223' and vendorId != '1224' and vendorId != '1225' and vendorId != '1226' and vendorId != '1227' ";
	  	  
	  	  if(vendorId != null && !vendorId.trim().equals("")){
	  		  subQuery = " and vendorId = '"+vendorId+"' ";
	  	  }else{
	  		  subQuery = notComeVendor;
	  	  }
	  	    try
	  	    {
	  	      con = ConnectionHelper.getConnection();
	  	      statement = con.createStatement();
	  	      
	  		    String selectQuery = "select department, date, majorhead_id, minorhead_id, productId, extra16, extra1, vendorId, extra21 from godwanstock "
	  		    		+ "where department = '"+hoaId+"' and date < '"+fromDate+"' and checKPrintDate between '"+fromDate+"'"
	  		    				+ " and '"+toDate+"' and extra1 like '%MSE%' "+subQuery+" group by extra1 order by vendorId";
	  		    
//	  		    System.out.println("getVendorSuccessOpeningEntrysAmount ====> "+selectQuery);
	  		    
	  		    ResultSet rs = statement.executeQuery(selectQuery);
	  		      while (rs.next()){
	  		    	  godwanstock godwanstock = new godwanstock();
	  		    	  godwanstock.setvendorId(rs.getString("vendorId"));
	  		    	  godwanstock.setextra1(rs.getString("extra1"));
	  		    	  godwanstock.setdate(rs.getString("date"));
	  		    	  godwanstock.setdepartment(rs.getString("department"));
	  		    	  godwanstock.setMajorhead_id(rs.getString("majorhead_id"));
	  		    	  godwanstock.setMinorhead_id(rs.getString("minorhead_id"));
	  		    	  godwanstock.setproductId(rs.getString("productId"));
	  		    	  godwanstock.setExtra16(rs.getString("extra16"));
	  		    	  godwanstock.setExtra21(rs.getString("extra21"));
	  		    	  godwanstocksList.add(godwanstock);
	  		      }
	  		      
	  	    }
	  	    catch (Exception e) {
	  	      e.printStackTrace();
	  	    }
	  	    List<godwanstock> godwanstocksList1 = new ArrayList<godwanstock>();
	  	    
	  	    if(godwanstocksList != null && godwanstocksList.size() > 0){
	  	    	
	  	    	String vendorId1 = "";
	  	    	double totalAmount = 0.0;
	  	    	godwanstock godwanstock1 = new godwanstock();
	  	    	for(int i= 0 ; i < godwanstocksList.size(); i++){
	  	    		
	  	    		godwanstock godwanstock = godwanstocksList.get(i);
	  	    		
	  	    		
	  	    		
	  	    		if(vendorId1.equals("")){
	  	    			vendorId1 = godwanstock.getvendorId().trim();
	  	    			totalAmount = Double.parseDouble(godwanstock.getExtra16()); 
	  	    		}else if(vendorId1.equals(godwanstock.getvendorId().trim())){
	  	    			totalAmount = totalAmount + Double.parseDouble(godwanstock.getExtra16());
	  	    		}else{
	  	    			godwanstock1.setExtra16(""+totalAmount);
	  	    			totalAmount = 0.0;
	  	    			godwanstocksList1.add(godwanstock1);
	  	    			vendorId1 = godwanstock.getvendorId().trim();
	  	    			totalAmount = Double.parseDouble(godwanstock.getExtra16());
	  	    		}
	  	    		
	  	    		godwanstock1 = godwanstock;
	  	    		
	  	    		if(i == (godwanstocksList.size() - 1)){
	  	    			godwanstock1.setExtra16(""+totalAmount);
	  	    			totalAmount = 0.0;
	  	    			godwanstocksList1.add(godwanstock1);
	  	    		}
	  	    		
	  	    	}
	  	    	
	  	    }
	  	    
	  	    VendorsAndPayments vendorsAndPayments = new VendorsAndPayments();
	  	    List<vendors> vendorList = vendorsAndPayments.getAllVendors();
	  	    
	  	    if(godwanstocksList1 != null && godwanstocksList1.size() > 0){
	  	    	
	  	    	for(int i = 0 ; i < godwanstocksList1.size(); i++){
	  	    		godwanstock godwanstock = godwanstocksList1.get(i);
	  	    		for(int j = 0 ; j < vendorList.size(); j++){
	  	    			vendors vendors = vendorList.get(j);
	  	    			if(godwanstock.getvendorId().trim().equals(vendors.getvendorId())){
	  	    				godwanstock.setMRNo(vendors.getagencyName());
	  	    				godwanstocksList1.set(i, godwanstock);
	  	    				break;
	  	    			}
	  	    		}
	  	    	}
	  	    }
	  	  return godwanstocksList1;
	    }
	    
  
  
 public List<godwanstock> getRecordsBetweenDates(final String fromdate, final String todate, final String mjId, final String dept, final String mnId, final String subId, final boolean isGroupBy) {
     String subQry = "";
     if (!mnId.trim().equals("") && subId.trim().equals("")) {
         subQry = String.valueOf(subQry) + "and minorhead_id = " + mnId;
         if (isGroupBy) {
             subQry = String.valueOf(subQry) + " group by minorhead_id";
         }
     }
     else if (!subId.trim().equals("") && mnId.trim().equals("")) {
         subQry = String.valueOf(subQry) + "and productId = " + subId;
         if (isGroupBy) {
             subQry = String.valueOf(subQry) + " group by productId";
         }
     }
     else if (!subId.trim().equals("") && !mnId.trim().equals("")) {
         subQry = String.valueOf(subQry) + "and minorhead_id = " + mnId + " and productId = " + subId;
         if (isGroupBy) {
             subQry = String.valueOf(subQry) + " group by productId";
         }
     }
     final String selectQuery = "select gsId,vendorId,productId,description,quantity,purchaseRate,date,MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,emp_id,department,checkedby,checkedtime,approval_status,extra6,extra8,extra9,po_approved_status,po_approved_by,po_approved_date,bill_status,bill_update_by,bill_update_time,extra11,extra12,extra13,extra14,extra15,majorhead_id,minorhead_id,payment_status,actualentry_date,extra16,extra17,extra18,extra19,extra20,extra21,extra22,extra23 from godwanstock where department = " + dept + " and date between '" + fromdate + "' and  '" + todate + "' and majorhead_id =" + mjId + " and extra1 like '%MSE%' " + subQry + " order by date";
     List<godwanstock> godwanStockList = null;
     ResultSet resultSet = null;
     try {
         this.con = ConnectionHelper.getConnection();
         this.statement = this.con.createStatement();
         godwanStockList = new ArrayList<godwanstock>();
         final Statement statement = this.con.createStatement();
         resultSet = statement.executeQuery(selectQuery);
         while (resultSet.next()) {
             final godwanstock godwanStock = new godwanstock();
             godwanStock.setGsId(resultSet.getString("gsId"));
             godwanStock.setVendorId(resultSet.getString("vendorId"));
             godwanStock.setProductId(resultSet.getString("productId"));
             godwanStock.setDescription(resultSet.getString("description"));
             godwanStock.setQuantity(resultSet.getString("quantity"));
             godwanStock.setPurchaseRate(resultSet.getString("purchaseRate"));
             godwanStock.setDate(resultSet.getString("date"));
             godwanStock.setMRNo(resultSet.getString("MRNo"));
             godwanStock.setBillNo(resultSet.getString("billNo"));
             godwanStock.setExtra1(resultSet.getString("extra1"));
             godwanStock.setExtra2(resultSet.getString("extra2"));
             godwanStock.setExtra3(resultSet.getString("extra3"));
             godwanStock.setExtra4(resultSet.getString("extra4"));
             godwanStock.setExtra5(resultSet.getString("extra5"));
             godwanStock.setVat(resultSet.getString("vat"));
             godwanStock.setVat1(resultSet.getString("vat1"));
             godwanStock.setProfcharges(resultSet.getString("profcharges"));
             godwanStock.setEmp_id(resultSet.getString("emp_id"));
             godwanStock.setDepartment(resultSet.getString("department"));
             godwanStock.setCheckedby(resultSet.getString("checkedby"));
             godwanStock.setCheckedtime(resultSet.getString("checkedtime"));
             godwanStock.setApproval_status(resultSet.getString("approval_status"));
             godwanStock.setExtra6(resultSet.getString("extra6"));
             godwanStock.setExtra8(resultSet.getString("extra8"));
             godwanStock.setExtra9(resultSet.getString("extra9"));
             godwanStock.setPo_approved_status(resultSet.getString("po_approved_status"));
             godwanStock.setPo_approved_by(resultSet.getString("po_approved_by"));
             godwanStock.setPo_approved_date(resultSet.getString("po_approved_date"));
             godwanStock.setBill_status(resultSet.getString("bill_status"));
             godwanStock.setBill_update_by(resultSet.getString("bill_update_by"));
             godwanStock.setBill_update_time(resultSet.getString("bill_update_time"));
             godwanStock.setExtra11(resultSet.getString("extra11"));
             godwanStock.setExtra12(resultSet.getString("extra12"));
             godwanStock.setExtra13(resultSet.getString("extra13"));
             godwanStock.setExtra14(resultSet.getString("extra14"));
             godwanStock.setExtra15(resultSet.getString("extra15"));
             godwanStock.setMajorhead_id(resultSet.getString("majorhead_id"));
             godwanStock.setMinorhead_id(resultSet.getString("minorhead_id"));
             godwanStock.setPayment_status(resultSet.getString("payment_status"));
             godwanStock.setActualentry_date(resultSet.getString("actualentry_date"));
             godwanStock.setExtra16(resultSet.getString("extra16"));
             godwanStock.setExtra17(resultSet.getString("extra17"));
             godwanStock.setExtra18(resultSet.getString("extra18"));
             godwanStock.setExtra19(resultSet.getString("extra19"));
             godwanStock.setExtra20(resultSet.getString("extra20"));
             godwanStock.setExtra21(resultSet.getString("extra21"));
             godwanStock.setExtra23(resultSet.getString("extra23"));
             godwanStockList.add(godwanStock);
         }
         resultSet.close();
         statement.close();
     }
     catch (SQLException e) {
         e.printStackTrace();
     }
     return godwanStockList;
 }
 
 
}