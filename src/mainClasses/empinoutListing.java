package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.*;
public class empinoutListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String emplo_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setemplo_id(String emplo_id)
{
this.emplo_id = emplo_id;
}
public String getemplo_id(){
return(this.emplo_id);
}
public List getempinout()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emplo_id,emp_timings,extra1,extra2,extra3,extra4,extra5 FROM empinout "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new empinout(rs.getString("emplo_id"),rs.getString("emp_timings"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getEmpdateexits(String empdate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emplo_id,emp_timings,extra1,extra2,extra3,extra4,extra5 FROM empinout  where date(emp_timings)='"+empdate+"'";
//System.out.println("a@selectquery=>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new empinout(rs.getString("emplo_id"),rs.getString("emp_timings"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getEmpAttendenceHours(String yestdays,String emplo_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emplo_id,emp_timings,extra1,extra2,extra3,extra4,extra5 FROM empinout where DATE(emp_timings)='"+yestdays+"' and emplo_id='"+emplo_id+"'  order by emp_timings asc"; 
/*System.out.println("b@getEmpAttendenceHours"+selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new empinout(rs.getString("emplo_id"),rs.getString("emp_timings"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}