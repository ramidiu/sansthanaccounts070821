package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.integerToRomanNumber;
import beans.productexpenses;
import beans.productexpensesService;
import beans.productexpenses_scService;

@SuppressWarnings("all")
public class productexpensesListing
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String exp_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setexp_id(String exp_id)
{
this.exp_id = exp_id;
}
public String getexp_id(){
return(this.exp_id);
}
public List getproductexpenses()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesBasedOnFromDateToDateAndHoa(String fromDate,String toDate,String hoa)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where entry_date>='"+fromDate+"%' and entry_date<='"+toDate+"%' and head_account_id='"+hoa+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getPaymentTOT(String expId)
{
String  list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as amount FROM productexpenses where expinv_id='"+expId+"'group by expinv_id";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){

		list=rs.getString("amount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesNotApprovedByManager()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,enter_date as narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where  extra1='WaitingMgrApproval' group by expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesNotApprovedByManagerByHoa(String hoa)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if( hoa!=null&& !hoa.equals("")){
		subQuery="and head_account_id='"+hoa+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,enter_date as narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where  extra1='WaitingMgrApproval'"+subQuery+"  group by expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/***Productexp subhead id based on banktransaction**/
public String  getproductexpensesSubheadonBank(String paymentid)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pe.sub_head_id,s.name FROM productexpenses pe,payments p,subhead s where pe.expinv_id=p.extra3 and s.sub_head_id=pe.sub_head_id and p.extra4='"+paymentid+"' group by pe.sub_head_id";
//String selectquery="SELECT pe.sub_head_id,s.name FROM productexpenses pe,payments p,subhead s where pe.expinv_id=p.extra3 and s.sub_head_id=pe.sub_head_id and s.head_account_id=pe.head_account_id and p.extra4='"+paymentid+"' group by pe.sub_head_id";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(list.equals("")){
 list="("+rs.getString("sub_head_id")+")"+rs.getString("name");
	} else{
		list=list+",("+rs.getString("sub_head_id")+")"+rs.getString("name");	
	}

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String  getproductexpensesSubheadonBanks(String paymentid)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT pe.sub_head_id,s.name FROM productexpenses pe,payments p,subhead s where pe.expinv_id=p.extra3 and s.sub_head_id=pe.sub_head_id and p.extra4='"+paymentid+"' group by pe.sub_head_id";
String selectquery="SELECT pe.sub_head_id,s.name FROM productexpenses pe,payments p,subhead s where pe.expinv_id=p.extra3 and s.sub_head_id=pe.sub_head_id and s.head_account_id=pe.head_account_id and p.extra4='"+paymentid+"' group by pe.sub_head_id";
//System.out.println("1111....."+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(list.equals("")){
 list="("+rs.getString("sub_head_id")+")"+rs.getString("name");
	} else{
		list=list+",("+rs.getString("sub_head_id")+")"+rs.getString("name");	
	}

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesBasedInvoice(String InvoiceId)
{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where extra5='"+InvoiceId+"' group by expinv_id"; 
	//System.out.println("get Values"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
		e.printStackTrace();
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	//System.out.println("from db=="+list);
	return list;
}

public List getproductexpensesBasedExpInvId(String InvoiceId)
{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where expinv_id='"+InvoiceId+"' group by expinv_id"; 
	//System.out.println("get Values"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
		e.printStackTrace();
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	//System.out.println("from db=="+list);
	return list;
}

public int getproductexpensesBasedIndentSize(String indentid)
{
	int list =1;
	int no=0;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM productexpenses px,godwanstock g,purchaseorder p where px.extra5=g.extra1 and g.extra2=p.poinv_id and p.indentinvoice_id='"+indentid+"' group by px.expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public int getproductexpensesBasedWithoutIndentSize(String mseid)
{
	int list =1;
	int no=0;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM productexpenses px  where px.extra5='"+mseid+"' group by px.expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public int getproductexpensesBasedUniqueIdSize(String uniqueid)
{
	int list =1;
	int no=0;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM productexpenses px  where px.extra6='"+uniqueid+"' group by px.expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public int getproductexpensesBasedInvoiceSize(String invoiceid)
{
	int list =1;
	int no=0;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM productexpenses px,godwanstock g where px.extra5='"+invoiceid+"'  group by px.expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesApprovepending(String adminid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM productexpenses where expinv_id not in(select indentinvoice_id from indentapprovals where admin_id='"+adminid+"') group by expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMproductexpenses(String exp_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where  exp_id = '"+exp_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMproductexpensesBasedOnExpInvoiceID(String expinv_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,enter_by,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where  expinv_id = '"+expinv_id+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMproductexpensesBasedOnExpInvoiceIDAndSubId(String expinv_id,String subhead_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where  expinv_id = '"+expinv_id+"' and sub_head_id = '"+subhead_id+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getProductExpensesReportForDaily(String expinv_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='Payment Done' and  expinv_id = '"+expinv_id+"'";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMproductexpensesBasedTrackingNo(String trackingno)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where  extra6 = '"+trackingno+"'";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getExpenseAmountByMonth(String month,String subHead)
{
String  total ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as totalAmount FROM productexpenses where entry_date like '"+month+"%'and sub_head_id='"+subHead+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 total=rs.getString("totalAmount");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return total;
}
public String getPresentExpenditure(String expinvid,String subHead,String status,String date,String AmtCalSt)
{
String  total ="";

String ddt=date.substring(0, 4);
String mnth=date.substring(5, 7);

int month=Integer.parseInt(mnth);
int yr=Integer.parseInt(ddt);
int finyr=0;
if(month>=4) {
	finyr=yr;
	System.out.println("month..."+finyr);
}
else if(month<=4) {
	
    finyr=yr-1;
    System.out.println("month of .."+finyr);
}
String currentYr=String.valueOf(finyr)+"-04-01";
System.out.println("currentYr...."+currentYr);


try {
 // Load the database driver
	String con="";
	if(AmtCalSt.equals("present")){
		con=" and entry_date like'"+date+"%' and expinv_id= '"+expinvid+"'";
	}else if(AmtCalSt.equals("previous")){
		con="and entry_date>='"+currentYr+"' and entry_date<'"+date+"' ";
	}
	//current yr added by vamshi to get "previous expenditure" based on financial year
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount+extra7) as totalAmount FROM productexpenses where extra1='"+status+"' and sub_head_id='"+subHead+"'"+con; 
/*System.out.println(AmtCalSt+" "+selectquery);*/
System.out.println("selectquery..."+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 total=rs.getString("totalAmount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return total;
}
public String getExpenseAmountByMonthBasedOnSubhead(String month,String HeadId,String sortType)
{
String  total ="";
try {
 // Load the database driver
	String conType="";
	if(sortType.equals("majorHead")){
		conType=" and major_head_id='"+HeadId+"'";
	}
	if(sortType.equals("minorHead")){
		conType=" and minorhead_id='"+HeadId+"'";
	}
	if(sortType.equals("subHead")){
		conType=" and sub_head_id='"+HeadId+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as totalAmount FROM productexpenses where entry_date like '"+month+"-%'"+conType; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 total=rs.getString("totalAmount");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return total;
}
public String getPreviousExpensesAmount(String month,String HeadId,String sortType)
{
String  total ="";
String conType="";
if(sortType.equals("majorHead")){
	conType=" and major_head_id='"+HeadId+"'";
}
if(sortType.equals("minorHead")){
	conType=" and minorhead_id='"+HeadId+"'";
}
if(sortType.equals("subHead")){
	conType=" and sub_head_id='"+HeadId+"'";
}
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as totalAmount FROM productexpenses where entry_date like '"+month+"%'"+conType; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 total=rs.getString("totalAmount");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return total;
}
public String getPreviousClosingExpensesAmount(String month,String HeadId,String sortType)
{
String  total ="";
String conType="";
if(sortType.equals("majorHead")){
	conType=" and major_head_id='"+HeadId+"'";
}
if(sortType.equals("minorHead")){
	conType=" and minorhead_id='"+HeadId+"'";
}
if(sortType.equals("subHead")){
	conType=" and sub_head_id='"+HeadId+"'";
}
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as totalAmount FROM productexpenses where entry_date < '"+month+"%'"+conType; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 total=rs.getString("totalAmount");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return total;
}
public boolean getPRoductExpensForInvoice(String invoice)
{
	boolean  total =false;

try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM productexpenses where extra5='"+invoice+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	total =true;
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return total;
}
public String getPRoductExpensForInvoiceWithCon(String invoice)
{
	String  total ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM productexpenses where extra5='"+invoice+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	total =rs.getString("extra5");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return total;
}
public List getExpensesDetails(String month,String HeadId,String sortType)

{
	String conType="";
	if(sortType.equals("majorHead")){
		conType=" and major_head_id='"+HeadId+"'";
	}
	if(sortType.equals("minorHead")){
		conType=" and minorhead_id='"+HeadId+"'";
	}
	if(sortType.equals("subHead")){
		conType=" and sub_head_id='"+HeadId+"'";
	}
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where entry_date like '"+month+"%'"+conType; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getExpensesListGroupByInvoice(String status)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id, vendorId, narration, entry_date, major_head_id, minorhead_id, sub_head_id, sum(amount) as amount, expinv_id, emp_id, extra1, extra2, extra3, extra4, extra5, head_account_id, enter_by, enter_date, extra6, extra7, extra8, extra9, extra10 FROM productexpenses where extra1='"+status+"' group by expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getExpensesListGroupByInvoiceAndHOOA(String status,String hoa)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(hoa!=null && !hoa.equals("")){
		subQuery="and head_account_id='"+hoa+"'";
	}
			
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id, vendorId, narration, entry_date, major_head_id, minorhead_id, sub_head_id, sum(amount) as amount, expinv_id, emp_id, extra1, extra2, extra3, extra4, extra5, head_account_id, enter_by, enter_date, extra6, extra7, extra8, extra9, extra10 FROM productexpenses where extra1='"+status+"'"+subQuery+"  group by expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getExpensesListGroupByInvoiceAndHOA(String status,String hoa,String role,String userId,String userId1)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery1="";
	String subQuery="";
	String rolequery="";
	String adminquery="";
	String adminId="";
	 if(role.equals("2")){
		rolequery=" and p.extra4='BoardApprove' ";
			}
	if(hoa!=null && !hoa.equals("")){
		subQuery=" p.head_account_id='"+hoa+"' and ";
	}
	if(role.equals("1")) // GS, CHAIRMAN
	{
		adminId=" (admin_id='"+userId+"' or admin_id='"+userId1+"')"; // GS, CHAIRMAN
	}else if(role.equals("2")){  // FS
		adminId=" admin_id='"+userId+"'"; // FS
	}
		
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="";
if(status.equals("Approved")){
selectquery="SELECT * FROM productexpenses p,indentapprovals i where "+subQuery+" (p.extra1!='Not-Approve' and p.extra1!='WaitingMgrApproval' and p.extra1='approved'  ) and p.expinv_id=i.indentinvoice_id and i.admin_status='approved'  group by p.expinv_id order by p.entry_date desc ";
} 
else if(status.equals("Not-Approve")){
	selectquery="SELECT * FROM productexpenses p  where  expinv_id not in (  SELECT indentinvoice_id  FROM indentapprovals  where  "+adminId+" ) and "+subQuery+" ( extra1!='WaitingMgrApproval' and  extra1='Not-Approve' and extra1!='approved' "+rolequery+" )  group by expinv_id order by entry_date desc ";
}
else if(status.equals("Partially-Approved")){
	/*selectquery="SELECT * FROM productexpenses p,indentapprovals i where  "+subQuery+" ( p.extra1='Not-Approve' and p.extra1!='WaitingMgrApproval'  and p.extra1!='approved') and p.expinv_id=i.indentinvoice_id group by p.expinv_id order by p.entry_date desc ";*/
	selectquery="SELECT * FROM productexpenses p,indentapprovals i where  "+subQuery+" ( p.extra1='Not-Approve' and p.extra1!='WaitingMgrApproval'  and p.extra1!='approved') and p.expinv_id=i.indentinvoice_id and i.admin_status!='postpone' group by p.expinv_id order by p.entry_date desc ";
}
else if(status.equals("Postpone")){
	/*selectquery="SELECT * FROM productexpenses p,indentapprovals i where   "+subQuery+" (p.extra1='Not-Approve' and p.extra1!='WaitingMgrApproval' and p.extra1!='approved' ) and p.expinv_id=i.indentinvoice_id and i.admin_status='postpone'   group by p.expinv_id order by p.entry_date desc ";*/
	selectquery="SELECT * FROM productexpenses p,indentapprovals i where   "+subQuery+" (p.extra1='Not-Approve' and p.extra1!='WaitingMgrApproval' and p.extra1!='approved' "+rolequery+" ) and p.expinv_id=i.indentinvoice_id and (i.admin_status='postpone'  and i.admin_status!='approved' and "+adminId+") group by p.expinv_id order by p.entry_date desc ";
}
else if(status.equals("DisApprove")){
	selectquery="SELECT * FROM productexpenses p,indentapprovals i where  "+subQuery+" (p.extra1='Not-Approve' and p.extra1!='WaitingMgrApproval' and p.extra1!='approved') and p.expinv_id=i.indentinvoice_id and i.admin_status='disapproved'   group by p.expinv_id order by p.entry_date desc ";	
}
System.out.println("productexpenseslisting===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPaymentsGroupByExpenseId(String hoaid,String fromdate,String todate,String Type) throws SQLException
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='"+Type+"' and head_account_id='"+hoaid+"' and (entry_date between '"+fromdate+"' and '"+todate+"' ) ";
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,"
		+ "extra5,head_account_id FROM productexpenses where extra1='"+Type+"' and head_account_id='"+hoaid+"'"
				+ " and (entry_date between '"+fromdate+"' and '"+todate+"' ) ";
//System.out.println("get journal pe---------"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next() && rs.getString("exp_id")!=null){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
rs.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
finally{
	s.close();
	c.close();
}
return list;
}
public double getExpensesAmt(String hoaid,String fromdate)
{
double list =0.00;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='Payment Done' and head_account_id='"+hoaid+"' and (entry_date like '"+fromdate+"%') "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list=Double.parseDouble(rs.getString("amount"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getExpensesAmtList(String hoaid,String fromdate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='"+Type+"' and head_account_id='"+hoaid+"' and (entry_date between '"+fromdate+"' and '"+todate+"' ) ";
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='Payment Done' and head_account_id='"+hoaid+"' and (entry_date like '"+fromdate+"%') group by extra5 order by entry_date";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
rs.close();
s.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public double getOtherChargesTotAmt(String hoaid,String fromdate)
{
double list =0.00;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select extra8 from godwanstock where extra1 in (SELECT extra5 FROM productexpenses where extra1='Payment Done' and head_account_id='"+hoaid+"' and (entry_date like '"+fromdate+"%') group by extra5) group by extra1 "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("extra8")!=null && !rs.getString("extra8").equals(""))
		list=list+Double.parseDouble(rs.getString("extra8"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public double getOtherChargesTotAmt2(String mseId)
{
double list =0.00;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select extra8 from godwanstock where extra1 = '"+mseId+"' group by extra1"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("extra8")!=null && !rs.getString("extra8").equals(""))
		list=list+Double.parseDouble(rs.getString("extra8"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getLedgerSum(String shId,String reporttyp,String finyrfrm,String finyrto,String hoid)
{
	String Amt ="00";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
//String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" p."+reporttyp+"='"+shId+"' ";
}
/*if(finyrto!=null && finyrto.equals("like")){
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}*/
if(finyrto!=null && finyrto.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and pe."+reporttyp+"='"+shId+"'and pe.head_account_id = '"+hoid+"' and pe.extra1='Payment Done' "+fromtoq;  
System.out.println("getLedgerSum ====> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Amt;
}

//This method is created for Income & Expenditure to hide some heads instead of getLedgerSum(-) method
public String getLedgerSum1(String shId,String reporttyp,String finyrfrm,String finyrto,String hoid,String extraField,String... ids)
{
	String Amt ="00";
	String query3="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
//String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" p."+reporttyp+"='"+shId+"' ";
}//if ends
/*if(finyrto!=null && finyrto.equals("like")){
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}*/
if(finyrto!=null && finyrto.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}//if ends
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+="and pe."+extraField+"!='"+id+"'";
	}//for loop ends
}//if ends
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and pe."+reporttyp+"='"+shId+"'and pe.head_account_id = '"+hoid+"' and pe.extra1='Payment Done' "+fromtoq+query3;  
System.out.println("getLedgerSum1  ====>  "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}//if ends
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return Amt;
}//method ends


public List<String[]> getLedgerSum11(String shId,String reporttyp,String finyrfrm,String finyrto,String hoid,String extraField,String... ids)
{
	String Amt = "00";
	String extra1 = "00";
	String sub_head_id = "00";
	String query3="";
	List<String[]> result = new ArrayList<String[]>();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" p."+reporttyp+"='"+shId+"' ";
}

if(finyrto!=null && finyrto.equals("like")){
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+="and pe."+extraField+"!='"+id+"'";
	}
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and pe."+reporttyp+"='"+shId+"'and pe.head_account_id = '"+hoid+"' and pe.extra1='Payment Done' "+fromtoq+query3+" group by pe.sub_head_id";  
System.out.println("getLedgerSum11  ====>  "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	String[] ss = new String[3];
	if( rs.getString("minorhead_id") != null && rs.getString("sub_head_id") != null && rs.getString("total") != null){
		ss[0] = rs.getString("minorhead_id");
		ss[1] = rs.getString("sub_head_id");
		ss[2] = rs.getString("total");
		result.add(ss);
	}
	
	
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return result;
}//method ends



public String getLedgerSum0(String shId,String reporttyp,String finyrfrm,String finyrto,String hoid)
{
	String Amt ="00";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
//String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" p."+reporttyp+"='"+shId+"' ";
}
/*if(finyrto!=null && finyrto.equals("like")){
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}*/
if(finyrto!=null && finyrto.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";;
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and pe."+reporttyp+"='"+shId+"'and pe.head_account_id = '"+hoid+"' and pe.extra1='Payment Done' "+fromtoq+" and pe.minorhead_id!='281'"; 
//System.out.println("555555&&&666====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Amt;
}
public String getLedgerSum01(String shId,String reporttyp,String finyrfrm,String finyrto,String hoid,String extraField,String... ids)
{
	String Amt ="00";
	String query3="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
//String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" p."+reporttyp+"='"+shId+"' ";
}
/*if(finyrto!=null && finyrto.equals("like")){
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}*/
if(finyrto!=null && finyrto.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";;
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+=" and pe."+extraField+"!='"+id+"'";
	}//for loop ends
}//if ends
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and pe."+reporttyp+"='"+shId+"'and pe.head_account_id = '"+hoid+"' and pe.extra1='Payment Done' "+fromtoq+" and pe.minorhead_id!='281'"+query3; 
//System.out.println("555555&&&666====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Amt;
}

public String getLedgerSum2(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String hoid)
{
	String Amt ="00";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
//String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
}
if(finyrto!=null && finyrto.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and "+reprttypq+" and pe.head_account_id='"+hoid+"'and pe.extra1='Payment Done'"+fromtoq;
//String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq;
System.out.println("00000 balance sheet ====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Amt;
}

//This method is created for Income & Expenditure to hide some heads instead of getLedgerSum2(-) method
public String getLedgerSum21(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String hoid,String extraField,String... ids)
{
	String Amt ="00";
	String query3="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
//String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
}//if ends
if(finyrto!=null && finyrto.equals("like")){
	//fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}//if ends
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+="and pe."+extraField+"!='"+id+"'";
	}//for loop ends
}//if ends
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and "+reprttypq+" and pe.head_account_id='"+hoid+"'and pe.extra1='Payment Done'"+fromtoq+query3;
//String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq;
//System.out.println("121212====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}//if ends
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return Amt;
}//method ends

public List getList(String distinct,String cash_type,String hod,String fromdate,String todate,String groupby,String extra1,String extra2)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver 
	String distinc = "";
	//String cashtype = "";
	String HOD = "";
	String date="";
	String groupBy = "";
	String query1 = "";
	
	if(distinct.equals("distinct"))
	{
		distinc = " distinct pe.sub_head_id";
	}
	else
	{
		distinc = " pe.exp_id, pe.vendorId, pe.narration, pe.entry_date, pe.major_head_id, pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as amount, pe.expinv_id, pe.emp_id, pe.extra1, pe.extra2, pe.extra3, pe.extra4, pe.extra5, pe.head_account_id";
	}
	
	/*if(cash_type.equals("of"))
	{
		cashtype=" cash_type='offerKind'";
	}
	else
	{
		cashtype=" cash_type!='offerKind' and cash_type!='online pending' and cash_type!='journalvoucher' and cash_type!='othercash' ";
	}*/
	
	if(!hod.equals(""))
	{
		HOD = " and pe.head_account_id = '"+hod+"'";
	}
	
	if(fromdate!=null && todate!=null){
		date=" and p.date>='"+fromdate+"' and p.date<='"+todate+"'";
	}
	
	if(!groupby.equals(""))
	{
		groupBy = " group by pe."+groupby;
	}
	
	if(!extra1.equals(""))
	{
		query1 = " and pe.sub_head_id = '"+extra1+"'";
	}
	
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT "+distinc+" from productexpenses pe,payments p where p.extra3=pe.expinv_id "+query1+HOD+date+groupBy+" order by pe.sub_head_id*1 asc"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(distinct.equals("distinct"))
{
	while(rs.next())
	{
		list.add(rs.getString("sub_head_id"));
	}
}
else{
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));
}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String getLedgerSumBasedOnJV(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String cashtyp)
{
	String Amt ="00";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
}
if(finyrto!=null && finyrto.equals("like")){
	fromtoq=" and pe.entry_date like '"+finyrfrm+"%' ";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and pe.extra1='"+cashtyp+"'"+fromtoq;
//String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq;
System.out.println("getLedgerSumBasedOnJV ====> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Amt;
}



public String getLedgerSumBasedOnJVNew(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String cashtyp)
{
	String Amt ="00";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
}
if(finyrto!=null && finyrto.equals("like")){
	fromtoq=" and pe.entry_date like '"+finyrfrm+"%' ";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe,banktransactions b where pe.extra5=b.extra3 and "+reprttypq+" and pe.extra1='"+cashtyp+"'"+fromtoq;
//String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq;
System.out.println("111111.... balance sheet====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Amt;
}







//This method is created for Income & Expenditure to hide some heads instead of getLedgerSumBasedOnJV(-) method
public String getLedgerSumBasedOnJV1(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String cashtyp,String extraField,String... ids)
{
	String Amt ="00";
	String query3="";

try {
// Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
}//if ends
if(finyrto!=null && finyrto.equals("like")){
	fromtoq=" and pe.entry_date like '"+finyrfrm+"%' ";
}//if ends
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+="and pe."+extraField+"!='"+id+"'";
	}//for loop ends
}//if ends
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and pe.extra1='"+cashtyp+"'"+fromtoq+query3;
//String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq;
System.out.println("getLedgerSumBasedOnJV1 =====>  "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}//if ends
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return Amt;
}//method ends


public List<String[]> getLedgerSumBasedOnJV11(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String cashtyp,String extraField,String... ids)
{
	String Amt ="00";
	String query3="";
	List<String[]> result = new ArrayList<String[]>();
try {
// Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
}//if ends
if(finyrto!=null && finyrto.equals("like")){
	fromtoq=" and pe.entry_date like '"+finyrfrm+"%' ";
}//if ends
if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
	for(String id:ids){
		query3+="and pe."+extraField+"!='"+id+"'";
	}//for loop ends
}//if ends
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pe.minorhead_id, pe.sub_head_id, sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and pe.extra1='"+cashtyp+"'"+fromtoq+query3;
//String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq;
System.out.println("getLedgerSumBasedOnJV11 =====>  "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	String[] ss = new String[3];
	if( rs.getString("minorhead_id") != null && rs.getString("sub_head_id") != null && rs.getString("total") != null){
		ss[0] = rs.getString("minorhead_id");
		ss[1] = rs.getString("sub_head_id");
		ss[2] = rs.getString("total");
		result.add(ss);
	}
}//while loop ends
s.close();
rs.close();
c.close();
}//try block ends
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}//catch block ends
return result;
}//method ends


public String getLedgerSumBasedOnPE(String shId,String reporttyp,String mid,String reporttype,String finyrfrm,String finyrto,String cashtyp)
{
	String Amt ="00";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe."+reporttype+"='"+mid+"' ";
}
if(finyrto!=null && finyrto.equals("like")){
	fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq;
String selectquery="SELECT sum(pe.amount) as total FROM productexpenses pe where "+reprttypq+" and pe.extra1='Payment Done' "+fromtoq;
//System.out.println("212121====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("total")!=null){
	Amt=rs.getString("total");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return Amt;
}
public List getLedgerSumDetails(String shId,String hoid,String reporttyp,String finyrfrm,String finyrto,String cashtyp)
{
	ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
/*if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"'";
}*/
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe.head_account_id='"+hoid+"'";
}
if(finyrto!=null && finyrto.equals("like")){
	fromtoq="  and  pe.entry_date like '"+finyrfrm+"%' ";
}
/*if(cashtyp.equals("ById")){
	fromtoq=fromtoq+" group by major_head_id";
			
}*/
if(!cashtyp.equals("")){
	reprttypq = reprttypq +" and pe.minorhead_id ='"+cashtyp+"'";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT pe.exp_id,pe.vendorId,pe.narration,pe.entry_date,pe.major_head_id,pe.minorhead_id,pe.sub_head_id,sum(pe.amount) as amount,pe.expinv_id,pe.emp_id,pe.extra1,pe.extra2,pe.extra3,pe.extra4,pe.extra5,pe.head_account_id  FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and "+reprttypq+" and pe.extra1='Payment Done' "+fromtoq; 
String selectquery="SELECT pe.exp_id,pe.vendorId,pe.narration,pe.entry_date,pe.major_head_id,pe.minorhead_id,pe.sub_head_id,sum(pe.amount) as amount,pe.expinv_id,pe.emp_id,pe.extra1,pe.extra2,pe.extra3,pe.extra4,pe.extra5,pe.head_account_id  FROM productexpenses pe where "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq+" group by pe.expinv_id";
//System.out.println("dateLedgerReport33333=====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getLedgerSumDetailsNew(String shId,String hoid,String reporttyp,String finyrfrm,String finyrto,String cashtyp)
{
	ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( p.date between '"+finyrfrm+"' and '"+finyrto+"') ";
/*if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"'";
}*/
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe.head_account_id='"+hoid+"'";
}
if(finyrto!=null && finyrto.equals("like")){
	fromtoq="  and  p.date like '"+finyrfrm+"%' ";
}
/*if(cashtyp.equals("ById")){
	fromtoq=fromtoq+" group by major_head_id";
			
}*/
if(!cashtyp.equals("")){
	reprttypq = reprttypq +" and pe.minorhead_id ='"+cashtyp+"'";
}
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT pe.exp_id,pe.vendorId,pe.narration,pe.entry_date,pe.major_head_id,pe.minorhead_id,pe.sub_head_id,sum(pe.amount) as amount,pe.expinv_id,pe.emp_id,pe.extra1,pe.extra2,pe.extra3,pe.extra4,pe.extra5,pe.head_account_id  FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and "+reprttypq+" and pe.extra1='Payment Done' "+fromtoq; 
String selectquery="SELECT pe.exp_id,pe.vendorId,pe.narration,pe.entry_date,pe.major_head_id,pe.minorhead_id,pe.sub_head_id,sum(pe.amount) as amount,pe.expinv_id,pe.emp_id,pe.extra1,pe.extra2,pe.extra3,pe.extra4,pe.extra5,pe.head_account_id  FROM productexpenses pe,payments p where pe.expinv_id = p.extra3 and "+reprttypq+" and (pe.extra1='Payment Done' || pe.extra1='journalvoucher') "+fromtoq+" group by pe.expinv_id";
//System.out.println("dateLedgerReport33333=====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getLedgerSumDetailsNewJV(String shId,String hoid,String reporttyp,String finyrfrm,String finyrto,String cashtyp)
{
	ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String reprttypq="";
String fromtoq=" and ( pe.entry_date between '"+finyrfrm+"' and '"+finyrto+"') ";
/*if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"'";
}*/
if(!reporttyp.equals(""))
{
	reprttypq=" pe."+reporttyp+"='"+shId+"' and pe.head_account_id='"+hoid+"'";
}

if(!cashtyp.equals("")){
	reprttypq = reprttypq +" and pe.minorhead_id ='"+cashtyp+"'";
}

c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT pe.exp_id,pe.vendorId,pe.narration,pe.entry_date,pe.major_head_id,pe.minorhead_id,pe.sub_head_id,sum(pe.amount) as amount,pe.expinv_id,pe.emp_id,pe.extra1,pe.extra2,pe.extra3,pe.extra4,pe.extra5,pe.head_account_id  FROM productexpenses pe,payments p where p.extra3=pe.expinv_id and "+reprttypq+" and pe.extra1='Payment Done' "+fromtoq; 
String selectquery="SELECT pe.exp_id,pe.vendorId,pe.narration,pe.entry_date,pe.major_head_id,pe.minorhead_id,pe.sub_head_id,sum(pe.amount) as amount,pe.expinv_id,pe.emp_id,pe.extra1,pe.extra2,pe.extra3,pe.extra4,pe.extra5,pe.head_account_id  FROM productexpenses pe where "+reprttypq+" and pe.extra1='journalvoucher' "+fromtoq+" group by pe.expinv_id";
//System.out.println("dateLedgerReport33333=====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getExpensesListGroupByInvoiceAndVendor(String status,String vendor,String hoid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(vendor.equals("Other Bills")){
		subQuery="and vendorId=''";
	}else{
		subQuery="and vendorId='"+vendor+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id, vendorId, narration, entry_date, major_head_id, minorhead_id, sub_head_id, sum(amount) as amount, expinv_id, emp_id, extra1, extra2, extra3, extra4, extra5, head_account_id, enter_by, enter_date, extra6, extra7, extra8, extra9, extra10, extra11 FROM productexpenses where extra1='"+status+"' and head_account_id='"+hoid+"'"+subQuery+"  group by expinv_id"; 
System.out.println("explist-----------"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getOtherCharges(String exp_id)
{
String list ="0";
try {

c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra7  FROM productexpenses where exp_id='"+exp_id+"' group by expinv_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("extra7");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getExpensesListGroupByInvoice1(String status)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='"+status+"' and extra4='BoardApprove'  group by expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getExpListBasedOnInvoiceID(String expinv_id,String role)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String qrole="";
	if(role!=null && role.equals("FS")){
		
		qrole=" and extra4='BoardApprove'";
	}else if(role!=null && role.equals("ADMIN")){
		
		qrole=" and extra1='Not-Approve'";
	}
	
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id, enter_by, enter_date, extra6, extra7, extra8, extra9, extra10 FROM productexpenses where  expinv_id = '"+expinv_id+"' "+qrole;
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getExpensesInvoiceTotAmt(String id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where expinv_id='"+id+"' group by expinv_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("amount");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getVendors(String hoid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(hoid!=null && !hoid.equals("")){
		subQuery="and head_account_id='"+hoid+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where extra1='Approved'"+subQuery+" group by vendorId"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPaymentsBasedOnMajorheadId(String hoaid,String majorHead,String fromDate,String toDate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='Payment Done' and head_account_id='"+hoaid+"' and major_head_id='"+majorHead+"' and (entry_date >= '"+fromDate+"' and  entry_date<= '"+toDate+"' ) group by minorhead_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(rs.getString("minorhead_id"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPaymentsBasedOnMajorheadIdChequeissue(String hoaid,String majorHead,String fromDate,String toDate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='Payment Done' and head_account_id='"+hoaid+"' and major_head_id='"+majorHead+"' and (entry_date >= '"+fromDate+"' and  entry_date<= '"+toDate+"' ) group by minorhead_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(rs.getString("minorhead_id"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPaymentsBasedOnMinorheadId(String hoaid,String minorHead,String fromDate,String toDate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where extra1='Payment Done' and head_account_id='"+hoaid+"' and minorhead_id='"+minorHead+"' and (entry_date >= '"+fromDate+"' and  entry_date<='"+toDate+"' )  group by sub_head_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesBasedOnVendor(String vendor)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where extra1='Payment Done' and vendorId='"+vendor+"' group by expinv_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesBasedOnVendorAndStatus(String vendor,String status)
{
ArrayList list = new ArrayList();
try {
// Load the database driver
	String subQuery="";
	if(status!=null &&status.equals("Payment Done")){
		subQuery=" and extra1='Payment Done'";
	}else{
		subQuery=" and extra1!='Payment Done'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where  vendorId='"+vendor+"'"+subQuery+" group by expinv_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getExpensesListGroupByInvoice(String date,String datetype,String paymenttype)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	String rolequery="";
	String datequery="";
	 
	 if(!date.equals("") && datetype.equals("Today")){
			datequery=" and entry_date >='"+date+"' ";
		}
	 else if(!date.equals("") && !datetype.equals("") && datetype.equals("Before")){
		datequery=" and entry_date<'"+date+"'  ";
	}
	else if(!date.equals("") && !datetype.equals("Uptonow")){
		datequery=" and entry_date<='"+date+"' ";
	}

c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="";
if(paymenttype.equals("PaymentDone")){
	selectquery="SELECT * FROM productexpenses p where ( p.extra1='approved' or p.extra1='Payment Done') and  extra4='BoardApprove' "+datequery+" group by extra6";
} else {
	selectquery="SELECT * FROM productexpenses p where p.extra1!='WaitingMgrApproval' and p.extra1!='approved' and  extra4!='BoardApprove' "+datequery+" group by extra6";	
}
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPaymentsBetweenDates(String fromdate,String todate) throws SQLException
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where (entry_date between '"+fromdate+"' and '"+todate+"' ) and (extra1 != 'Payment Done' and extra1 != 'journalvoucher') ";
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where extra1 != 'Payment Done' and extra1 != 'journalvoucher' ";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next() && rs.getString("exp_id")!=null){
	list.add(new productexpenses_scService(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
rs.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
finally{
	s.close();
	c.close();
}
return list;
}

public String getMseIdBasedOnExpInvId(String expinv_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id FROM productexpenses where expinv_id='"+expinv_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("extra5");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPettyCashIssueList(String hoid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(hoid!=null && !hoid.equals("")){
		subQuery="and head_account_id='"+hoid+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where extra1='Payment Done' and extra2 in ('21452','21450','21405') and extra8 = ''"+subQuery; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesBasedOnVendorAndStatusAndDate(String vendor,String status ,String date1, String date2)
{
ArrayList list = new ArrayList();
try {
// Load the database driver
	String subQuery="";
	if(status!=null &&status.equals("Payment Done")){
		subQuery=" and extra1='Payment Done'";
	}else{
		subQuery=" and extra1!='Payment Done'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where date(entry_date) between '"+date1+"' and '"+date2+"'  and vendorId='"+vendor+"'and expinv_id not in(select extra3 from payments where date(date) between '"+date1+"' and '"+date2+"') group by expinv_id"; 
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getproductexpensesBasedOnVendorAndStatusAndExpinvId(String vendor,String status ,String expinv_id)
{
ArrayList list = new ArrayList();
try {
// Load the database driver
	String subQuery="";
	if(status!=null &&status.equals("Payment Done")){
		subQuery=" and extra1='Payment Done'";
	}else{
		subQuery=" and extra1!='Payment Done'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT exp_id,vendorId,narration,entry_date,major_head_id,minorhead_id,sub_head_id,sum(amount) as amount,expinv_id,emp_id,extra1,extra2,extra3,extra4,extra5,head_account_id,enter_by,enter_date,extra6,extra7,extra8,extra9,extra10 FROM productexpenses where expinv_id = '"+expinv_id+"' and vendorId='"+vendor+"'"+subQuery+" group by expinv_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getExpensesInvoiceTotAmt1(String expinv_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT expinv_id,emp_id,extra5 FROM productexpenses where expinv_id='"+expinv_id+"' group by expinv_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	String invoice_id = rs.getString("extra5");
String query = "SELECT gsId,vendorId,productId,description,quantity,sum(quantity*(purchaseRate+extra18+extra19+extra20)*(1+vat/100)) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate*(1+vat/100))) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where extra1='"+invoice_id+"' ";
rs = s.executeQuery(query);
while(rs.next()){
	if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
 list=(Double.parseDouble(rs.getString("purchaseRate"))+Double.parseDouble(rs.getString("extra8")))+"";
	} else {
		list=rs.getString("purchaseRate");
	}
}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getExpensesInvoiceTotAmtNew(String expinv_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT expinv_id,emp_id,extra5 FROM productexpenses where expinv_id='"+expinv_id+"' group by expinv_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	String invoice_id = rs.getString("extra5");
String query = "SELECT gsId,vendorId,productId,description,quantity,sum(quantity*(purchaseRate+extra18+extra19+extra20)*(1+vat/100)) as purchaseRate,date,(purchaseRate*(1+vat/100)) as MRNo,billNo,extra1,extra2,extra3,extra4,extra5,vat,(quantity*(purchaseRate*(1+vat/100))) as vat1,profcharges,emp_id,department,checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10 FROM godwanstock where extra1='"+invoice_id+"' ";
rs = s.executeQuery(query);
while(rs.next()){
	if(rs.getString("extra8")!=null && !rs.getString("extra8").equals("")){
 list=(Double.parseDouble(rs.getString("purchaseRate"))+Double.parseDouble(rs.getString("extra8")))+"";
	} else {
		list=rs.getString("purchaseRate");
	}
}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getExpensesInvoice(String expinv_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pe.exp_id,pe.vendorId,pe.enter_date as narration,pe.entry_date,pe.major_head_id,pe.minorhead_id,pe.sub_head_id,sum(pe.amount) as amount,pe.expinv_id,pe.emp_id,pe.extra1,pe.extra2,pe.extra3,pe.extra4,pe.extra5,pe.head_account_id,pe.enter_by,pe.enter_date,pe.extra6,pe.extra7,pe.extra8,pe.extra9,pe.extra10 FROM productexpenses pe,payments py where pe.expinv_id!=py.extra3 and pe.extra1!='Payment Done' and pe.extra1!='journalvoucher' and pe.extra1!='Not-Approve' and pe.extra1!='WaitingMgrApproval' and pe.expinv_id like'"+expinv_id+"%' group by expinv_id"; 
ResultSet rs = s.executeQuery(selectquery);
//System.out.println("query....."+selectquery);
while(rs.next()){
	list.add(new productexpenses(rs.getString("exp_id"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("entry_date"),rs.getString("major_head_id"),rs.getString("minorhead_id"),rs.getString("sub_head_id"),rs.getString("amount"),rs.getString("expinv_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("head_account_id"),rs.getString("enter_by"),rs.getString("enter_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String getUniqueIdBasedOnExpInvId(String expinvid)
{
	System.out.println("expinvid =====> "+expinvid);
	
	String uniqueId="";
	String selectquery="";
	String expinvIdQuery="";
	String[] expinvIdArray=expinvid.split(",");
	if(expinvid!=null && !expinvid.equals("null") && !expinvid.equals("")){
	for(int i=0;i<expinvIdArray.length;i++){
		if(i!=expinvIdArray.length-1){
		expinvIdQuery=expinvIdQuery+"expinv_id ='"+expinvIdArray[i]+"' or ";
		}
		else{
			expinvIdQuery=expinvIdQuery+"expinv_id ='"+expinvIdArray[i]+"'";	
		}
	}}
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
if(expinvid!=null && !expinvid.equals("null") && !expinvid.equals(""))
selectquery="SELECT extra6 FROM productexpenses where "+expinvIdQuery+" group by extra6"; 
System.out.println("selectquery ======> "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	uniqueId=uniqueId+rs.getString("extra6")+",";
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is 111111 ;"+e);
}
return uniqueId;
}

public List<productexpenses> getDcProdExpenses()	{
	List<productexpenses> plist = null;
	String query = "select pe.entry_date,pe.expinv_id,pe.amount,pe.extra11,pe.extra5,pe.vendorId,v.agencyName,pe.major_head_id,mh.name,pe.minorhead_id,mn.name,pe.sub_head_id,s.name,pe.extra2,p.productName,pe.head_account_id,h.name,g.quantity,g.purchaserate,pe.exp_id  from productexpenses pe left outer join vendors v on pe.vendorId = v.vendorid left outer join products p on (pe.extra2 = p.productId and pe.head_account_id = p.head_account_id and pe.major_head_id = p.major_head_id and pe.minorhead_id = p.extra3 and pe.sub_head_id = p.sub_head_id) left outer join majorhead mh on (pe.major_head_id = mh.major_head_id and pe.head_account_id = mh.head_account_id) left outer join minorhead mn on (pe.minorhead_id = mn.minorhead_id and pe.major_head_id = mn.major_head_id and pe.head_account_id = mn.head_account_id) left outer join subhead s on (pe.sub_head_id = s.sub_head_id and pe.minorhead_id = s.extra1 and pe.major_head_id = s.major_head_id and pe.head_account_id = s.head_account_id) left outer join headofaccounts h on (pe.head_account_id = h.head_account_id) left outer join godwanstock g on (g.extra21 = pe.expinv_id and pe.major_head_id = g.majorhead_id and pe.minorhead_id = g.minorhead_id and pe.extra2 = g.productId)  where (pe.extra4 = '' or pe.extra4 = 'BoardApprove') and pe.extra11 like 'DC%' and pe.extra1 = 'Not-Approve'";
	ResultSet rs = null;
	try	{
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		rs = s.executeQuery(query);
//		System.out.println("get prod exps-=----"+query);
		plist = new ArrayList<productexpenses>();
		while (rs.next())	{
			productexpenses pe = new productexpenses();
			pe.setentry_date(rs.getString("pe.entry_date"));
			pe.setexpinv_id(rs.getString("pe.expinv_id"));
			pe.setamount(rs.getString("pe.amount"));
			pe.setExtra11(rs.getString("pe.extra11").trim());
			pe.setextra5(rs.getString("pe.extra5"));
			pe.setvendorId(rs.getString("pe.vendorId"));
			pe.setExtra13(rs.getString("v.agencyName"));
			pe.setmajor_head_id(rs.getString("pe.major_head_id"));
			pe.setExtra14(rs.getString("mh.name"));
			pe.setminorhead_id(rs.getString("pe.minorhead_id"));
			pe.setExtra15(rs.getString("mn.name"));
			pe.setsub_head_id(rs.getString("pe.sub_head_id"));
			pe.setExtra16(rs.getString("s.name"));
			pe.setextra2(rs.getString("pe.extra2"));
			pe.setExtra17(rs.getString("p.productName"));
			pe.sethead_account_id(rs.getString("pe.head_account_id"));
			pe.setExtra18(rs.getString("h.name"));
			pe.setExtra19(rs.getString("g.quantity"));
			pe.setExtra20(rs.getString("g.purchaserate"));
			pe.setexp_id(rs.getString("pe.exp_id"));
			plist.add(pe);
		}
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	catch(Exception e)	{
		e.printStackTrace();
	}
	finally	{
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return plist;
}


public List<productexpenses> getProdExpensesDetails(String mseId)	{
	List<productexpenses> plist = null;
	String query = "SELECT * FROM productexpenses where extra5='"+mseId+"'";
	ResultSet rs = null;
	try	{
		  c = ConnectionHelper.getConnection();
		  s=c.createStatement();
		  rs = s.executeQuery(query);
		  plist = new ArrayList<productexpenses>();
		  while (rs.next())	{
			productexpenses pe = new productexpenses();
			pe.setentry_date(rs.getString("entry_date"));
			pe.setexpinv_id(rs.getString("expinv_id"));
			pe.setamount(rs.getString("amount"));
			pe.setExtra11(rs.getString("extra11").trim());
			pe.setextra5(rs.getString("extra5"));
			pe.setvendorId(rs.getString("vendorId"));
			pe.setmajor_head_id(rs.getString("major_head_id"));
			pe.setminorhead_id(rs.getString("minorhead_id"));
			pe.setsub_head_id(rs.getString("sub_head_id"));
			pe.setextra2(rs.getString("extra2"));
			pe.sethead_account_id(rs.getString("head_account_id"));
			pe.setexp_id(rs.getString("exp_id"));
			plist.add(pe);
		}
		  s.close();
		  c.close();
	}
	
	catch(Exception e)	{
		e.printStackTrace();
	}
	
	return plist;
}

public void updateExpenseEntryData(String amount,String expensesId) {
	String updatequery="update productexpenses set amount='"+amount+"' where exp_id='"+expensesId+"' ";
	try {
		 c = ConnectionHelper.getConnection();
		  s=c.createStatement();
		  s.executeUpdate(updatequery);
		  s.close();
		  c.close();
	}catch(Exception  e){
		e.printStackTrace();
	}
}
}