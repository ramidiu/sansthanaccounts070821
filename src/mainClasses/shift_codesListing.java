package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.shift_codes;
public class shift_codesListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String shift_code_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setshift_code_id(String shift_code_id)
{
this.shift_code_id = shift_code_id;
}
public String getshift_code_id(){
return(this.shift_code_id);
}
public List getshift_codes()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT shift_code_id,shift_code,from_time,to_time,extra1,extra2,extra3,extra4,extra5 FROM shift_codes "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shift_codes(rs.getString("shift_code_id"),rs.getString("shift_code"),rs.getString("from_time"),rs.getString("to_time"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMshift_codes(String shift_code_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT shift_code_id,shift_code,from_time,to_time,extra1,extra2,extra3,extra4,extra5 FROM shift_codes where  shift_code_id = '"+shift_code_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shift_codes(rs.getString("shift_code_id"),rs.getString("shift_code"),rs.getString("from_time"),rs.getString("to_time"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}