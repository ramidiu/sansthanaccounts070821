package mainClasses;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor.BLUE;

import com.lowagie.text.Anchor;
import com.lowagie.text.Annotation;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.CMYKColor;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import beans.customerpurchasesService;
import dbase.sqlcon.ConnectionHelper;

public class WtspNotification{

	customerpurchasesService csp=new customerpurchasesService();	
	public void offerKindsWtspNotification(HttpServletRequest rquest,HttpServletResponse response,String type)throws ServletException, IOException {      
		 ServletOutputStream os = response.getOutputStream();
		  //set the response content type to PDF
		  response.setContentType("application/pdf"); 
		  String ss="                                   SHRI SHIRIDI SAI BABA SANSTHAN TRUST                       ";
		  String productId="";
		  String billingId="";
		  String customerName="";
		  String gothram="";
		  String quantity="";
		  String totalAmount="";
		  String bookingDate="";
		  String phoneNo="";
		  String subHeadName="";
		  String hoa="";
		  Document document = new Document();
		    ResultSet rs=null;  
		    String purchaseId="";
		    SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy"); // 
		    SimpleDateFormat SJDF=new SimpleDateFormat("yyyy-MM-dd"); // 
		    try
	      {
			    Connection c = ConnectionHelper.getConnection();
			    Statement st = c.createStatement();
	            //getting purchaseId
			    (rs = st.executeQuery("select * from no_genarator where table_name='customerpurchases'")).next();
	            final int ticket = rs.getInt("table_id");
	            final String PriSt = rs.getString("id_prefix");
	            rs.close();
	            final int ticketm = ticket;
	            purchaseId = String.valueOf(PriSt) + ticketm;
		        System.out.println("purchaseId..in testing.."+purchaseId);
			    //getting receipt needed information
		        String selectquery="SELECT productId,billingId,customername,gothram,quantity,totalAmmount,date,phoneno,extra1 FROM customerpurchases where  purchaseId = '"+purchaseId+"'"; 
			    ResultSet rs1 = st.executeQuery(selectquery);
			    while(rs1.next()) {
			    productId=rs1.getString("productId");
			    billingId=rs1.getString("billingId");
			  	customerName=rs1.getString("customername");
			  	gothram=rs1.getString("gothram");
			  	quantity=rs1.getString("quantity");
			  	totalAmount=rs1.getString("totalAmmount");
			  	bookingDate=rs1.getString("date");
			    phoneNo=rs1.getString("phoneno");
			    hoa=rs1.getString("extra1");
			    }
			    
			    bookingDate=CDF.format(SJDF.parse(bookingDate));
			    rs1.close();
			    
			    System.out.println("sort by type..!"+type);
			    if(type.equals("subhead")) {
			    //getting receipt needed information
		        String selectquery1="SELECT name FROM subhead where  sub_head_id = '"+productId+"' and head_account_id = '"+hoa+"'"; 
			    ResultSet rs2 = st.executeQuery(selectquery1);
			    while(rs2.next()) {
			    	subHeadName=rs2.getString("name");
			    }
			    rs2.close();
			    System.out.println("subhead suhead name.."+subHeadName);
			    }
			    else {
			    	//getting receipt needed information
			        String selectquery1="SELECT productName FROM products where  productId = '"+productId+"'"; 
				    ResultSet rs2 = st.executeQuery(selectquery1);
				    while(rs2.next()) {
				    	subHeadName=rs2.getString("productName");
				    }
				    rs2.close();
			    System.out.println("products product name.."+subHeadName);	
			    }
			    
			    st.close();
			    c.close();
 System.out.println("values by result set.."+" productId "+productId+" subHeadName "+subHeadName +" b "+billingId+"c "+customerName+" g "+gothram+" q "+quantity+" to "+totalAmount+" da "+bookingDate +" ph "+phoneNo);
			  
    	     Font blueFont1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.BOLD, new CMYKColor(0, 0, 0, 255));
 	         Font blueFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, new CMYKColor(0, 0, 0, 255));
			  //local//PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\cmahesh\\Downloads\\"+billingId+".pdf"));
			  PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("/var/www/accounts/data/www/accounts.saisansthan.in/ROOT/static/"+billingId+".pdf"));
			 document.open();
			    //local// Image image1 = Image.getInstance("C:\\Users\\cmahesh\\Downloads\\saibaba.jpeg");
			     Image image1 = Image.getInstance("http://accounts.saisansthan.in/static/saibaba.jpeg");
			     //Fixed Positioning
		         image1.setAbsolutePosition(38f, 700f);
		        
		         //Scale to new height and new width of image
		         image1.scaleAbsolute(92, 100);
		         //Add to document
		         document.add(image1);
		         //local//Image image2 = Image.getInstance("C:\\Users\\cmahesh\\Downloads\\saibaba2.jpeg");
		         Image image2 = Image.getInstance("http://accounts.saisansthan.in/static/saibaba2.jpeg");
		            
		         //Fixed Positioning
		         image2.setAbsolutePosition(470f, 700f);
		         //Scale to new height and new width of image
		         image2.scaleAbsolute(92, 100);
		         //Add to document
		         document.add(image2);   
			 document.add(new Paragraph(ss, blueFont1));
	         //document.add(new Paragraph("                                          SHRI SHIRIDI SAI BABA SANSTHAN TRUST                                    "));
	         document.add(new Paragraph("                                                             Regd. No. 646/92                                           "));
	         document.add(new Paragraph("                                                Dilsukhnagar, Hyderabad,TS-500 060                                    "));
	         document.add(new Paragraph("                                                               Ph:24066566.                                            "));
	         document.add(new Paragraph("                                               Contact us on:-8500966566 (Phone Pay).            "));
	         document.add(new Paragraph("                                                  website url: "+new Chunk("https://saisansthan.in/").setAnchor("https://saisansthan.in/")        ));
	         document.add(new Paragraph("                                                                                                                    "));  
	         document.add(new Paragraph("MyBookings                                                                                                          ",blueFont));
        
	         PdfPTable table = new PdfPTable(2); // 2 columns.
	         table.setWidthPercentage(100); //Width 100%
	         table.setSpacingBefore(10f); //Space before table
	         table.setSpacingAfter(10f); //Space after table
	  
	         //Set Column widths
	         float[] columnWidths = {2f, 2f};
	         table.setWidths(columnWidths);
	  
	         PdfPCell cell1 = new PdfPCell(new Paragraph(" Receipt Number: ",blueFont));
	         cell1.setPaddingLeft(10);
	         cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         
	  
	         PdfPCell cell2 = new PdfPCell(new Paragraph(billingId,blueFont));
	         cell2.setPaddingLeft(10);
	         cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell1);
	         table.addCell(cell2);
	             
	         PdfPCell cell3 = new PdfPCell(new Paragraph(" Name: ",blueFont));
	         cell3.setPaddingLeft(10);
	         cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell4 = new PdfPCell(new Paragraph(customerName,blueFont));
	         cell4.setPaddingLeft(10);
	         cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell3);
	         table.addCell(cell4);
	         
	         PdfPCell cell5 = new PdfPCell(new Paragraph(" Gothram: ",blueFont));
	         cell5.setPaddingLeft(10);
	         cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell6 = new PdfPCell(new Paragraph(gothram,blueFont));
	         cell6.setPaddingLeft(10);
	         cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell5);
	         table.addCell(cell6);
	         
	         PdfPCell cell7 = new PdfPCell(new Paragraph(" Booking For: ",blueFont));
	         cell7.setPaddingLeft(10);
	         cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell8 = new PdfPCell(new Paragraph(subHeadName,blueFont));
	         cell8.setPaddingLeft(10);
	         cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell7);
	         table.addCell(cell8);
	         
	         PdfPCell cell9 = new PdfPCell(new Paragraph(" Ticket Qty: ",blueFont));
	         cell9.setPaddingLeft(10);
	         cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell10 = new PdfPCell(new Paragraph(quantity,blueFont));
	         cell10.setPaddingLeft(10);
	         cell10.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell10.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell9);
	         table.addCell(cell10);
	         
	         PdfPCell cell11 = new PdfPCell(new Paragraph(" Total Amount: ",blueFont));
	         cell11.setPaddingLeft(10);
	         cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell12 = new PdfPCell(new Paragraph(totalAmount,blueFont));
	         cell12.setPaddingLeft(10);
	         cell12.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell11);
	         table.addCell(cell12);
	         
	         PdfPCell cell13 = new PdfPCell(new Paragraph(" Booking Date: ",blueFont));
	         cell13.setPaddingLeft(10);
	         cell13.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell13.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell14 = new PdfPCell(new Paragraph(bookingDate,blueFont));//bookingDate.substring(0, 10)));
	         cell14.setPaddingLeft(10);
	         cell14.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell14.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell13);
	         table.addCell(cell14);
	       	         
	         PdfPCell cell17 = new PdfPCell(new Paragraph(" Phone Number: ",blueFont));
	         cell17.setPaddingLeft(10);
	         cell17.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell17.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell18 = new PdfPCell(new Paragraph(phoneNo,blueFont));
	         cell18.setPaddingLeft(10);
	         cell18.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell18.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell17);
	         table.addCell(cell18);
	         document.add(table);
	         document.add(new Paragraph("                                                    *** Thank You Visit Again! ***                                           ",blueFont));
	         document.add(new Paragraph("Note : We sent an acknowledgment mail to your mail id.If you do not find our mail in your inbox please once check in your spam folder.Thank you",blueFont));        
	         document.add(new Paragraph("                                                                                              "));
	         document.add(new Paragraph("Please Subscribe & follow us on:"));
	         document.add(new Paragraph("Facebook: "+new Chunk("https://www.facebook.com/dilsuknagarsaibabatemple").setAnchor("https://www.facebook.com/dilsuknagarsaibabatemple/")));
	         document.add(new Paragraph("Youtube : "+new Chunk("https://www.youtube.com/channel/UCtSaXmy5KswqzjJNuVsz_PA").setAnchor("https://www.youtube.com/channel/UCtSaXmy5KswqzjJNuVsz_PA/")));
	         document.add(new Paragraph("Instagram: "+new Chunk("https://instagram.com/dilsukhnagar_sai_baba_temple?utm_medium=copy_link").setAnchor("https://instagram.com/dilsukhnagar_sai_baba_temple?utm_medium=copy_link/")));
	         document.add(new Paragraph("To Receive daily updates please provide your details on the following links. "+new Chunk("https://forms.gle/cr8HJGwCRaryQZWn9").setAnchor("https://forms.gle/cr8HJGwCRaryQZWn9/")));
	         document.close();	        
	         writer.close();
	    	 URL url = new URL("https://push.aclwhatsapp.com/pull-platform-receiver/wa/messages/");
		     	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		     	connection.setDoOutput(true);
		     	connection.setRequestMethod("POST");
		     	connection.setRequestProperty("Content-Type", "application/json");
		     	connection.setRequestProperty("user", "shrisbuat");
		     	connection.setRequestProperty("pass", "shrisbuat23");
	            String jsonString="{\"messages\": [{\"sender\": \"918297966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"mediaTemplate\",\"mediaTemplate\": {\"mediaUrl\": \"http://accounts.saisansthan.in/static/"+billingId+".pdf\",\"contentType\":\"application/pdf\",\"filename\": \"seva-receipt\",\"template\": \"receipt_for_seva_booking\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\",\"3\": \"var3\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
		     	//String jsonString="{\"messages\": [{\"sender\": \"918297966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"mediaTemplate\",\"mediaTemplate\": {\"mediaUrl\": \"https://file-examples-com.github.io/uploads/2017/10/file-sample_150kB.pdf\",\"contentType\":\"application/pdf\",\"filename\": \"seva-receipt\",\"template\": \"receipt_for_seva_booking\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\",\"3\": \"var3\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
		     	jsonString=jsonString.replace("pNo",  phoneNo);
	            jsonString=jsonString.replace("var1", customerName);
	            jsonString=jsonString.replace("var2", subHeadName);
	            jsonString=jsonString.replace("var3", bookingDate);
		        OutputStream os1 = connection.getOutputStream();
		     	byte[] input = jsonString.getBytes("utf-8");
		     	os1.write(input, 0, input.length);			
		     	int code = connection.getResponseCode();
		     	System.out.println("status code.in seva receipt.document..!"+code);		    	 
	      } catch (DocumentException e)
	      {
	         e.printStackTrace();
	      } catch (FileNotFoundException e)
	      {
	         e.printStackTrace();
	      }
		  catch (SQLException e2) {
              e2.printStackTrace();
          }
		  catch (Exception e3) {
	          e3.printStackTrace();
	      }

	         
	         
	         
	}
	
	public void makeDocument(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 System.out.println("calling from wtsp triggering messages.........!");		
         
		 ServletOutputStream os = response.getOutputStream();
		  //set the response content type to PDF
		  response.setContentType("application/pdf"); 
		  String ss="                                   SHRI SHIRIDI SAI BABA SANSTHAN TRUST                       ";
		  String productId="";
		  String billingId="";
		  String customerName="";
		  String gothram="";
		  String quantity="";
		  String totalAmount="";
		  String bookingDate="";
		  String performedDate="";
		  String phoneNo="";
		  String subHeadName="";
		  String hoa="";
		  Document document = new Document();
		    ResultSet rs=null;  
		
		    String purchaseId="";
		    SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy"); // 
		    SimpleDateFormat SJDF=new SimpleDateFormat("yyyy-MM-dd"); // 
		    try
	      {
			    Connection c = ConnectionHelper.getConnection();
			    Statement st = c.createStatement();
	            //getting purchaseId
			    (rs = st.executeQuery("select * from no_genarator where table_name='customerpurchases'")).next();
	            final int ticket = rs.getInt("table_id");
	            final String PriSt = rs.getString("id_prefix");
	            rs.close();
	            final int ticketm = ticket;
	            purchaseId = String.valueOf(PriSt) + ticketm;
		        System.out.println("purchaseId..in testing.."+purchaseId);
			    //getting receipt needed information
		        String selectquery="SELECT productId,billingId,customername,gothram,quantity,totalAmmount,date,extra1,extra3,phoneno FROM customerpurchases where  purchaseId = '"+purchaseId+"'"; 
			    ResultSet rs1 = st.executeQuery(selectquery);
			    while(rs1.next()) {
			    productId=rs1.getString("productId");
			    billingId=rs1.getString("billingId");
			  	customerName=rs1.getString("customername");
			  	gothram=rs1.getString("gothram");
			  	quantity=rs1.getString("quantity");
			  	totalAmount=rs1.getString("totalAmmount");
			  	bookingDate=rs1.getString("date");
			  	performedDate=rs1.getString("extra3");
			    phoneNo=rs1.getString("phoneno");
			    hoa=rs1.getString("extra1");
			    }
			    
			    bookingDate=CDF.format(SJDF.parse(bookingDate));
			    if(!performedDate.equals("") && performedDate!=null) {
			    	 performedDate=CDF.format(SJDF.parse(performedDate));
			    }else {
			    	performedDate="-------";
			    }
			   
			    rs1.close();
			    
			    //getting receipt needed information
		        String selectquery1="SELECT name FROM subhead where  sub_head_id = '"+productId+"' and head_account_id = '"+hoa+"'"; 
			    ResultSet rs2 = st.executeQuery(selectquery1);
			    while(rs2.next()) {
			    	subHeadName=rs2.getString("name");
			    }
			    rs2.close();
			 
			    
			    st.close();
			    c.close();
			 
			    Font fuente5 = new Font();
			    fuente5.setColor(new CMYKColor(355,100,0,4));
			    fuente5.setStyle(Font.UNDERLINE);
			    String sss="hello";
			     System.out.println("values by result set.."+" productId "+productId+" subHeadName "+subHeadName +" b "+billingId+"c "+customerName+" g "+gothram+" q "+quantity+" to "+totalAmount+" da "+bookingDate +" p "+performedDate+" ph "+phoneNo);
			     Font link = FontFactory.getFont("Arial", 12, Font.UNDERLINE, new Color(0, 0, 255));
			     Font blueFont2 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.UNDERLINE, new CMYKColor(0, 0, 255, 0));	     
			     Font blueFont1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.BOLD, new CMYKColor(0, 0, 0, 255));
			 		    Font blueFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.BOLD, new CMYKColor(0, 0, 0, 255));
			  //PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\cmahesh\\Downloads\\"+billingId+".pdf"));
			 PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("/var/www/accounts/data/www/accounts.saisansthan.in/ROOT/static/"+billingId+".pdf"));
			 document.open();
			     //Image image1 = Image.getInstance("C:\\Users\\cmahesh\\Downloads\\saibaba.jpeg");
			 	Image image1 = Image.getInstance("http://accounts.saisansthan.in/static/saibaba.jpeg");    
			 	
			     //Fixed Positioning
		         image1.setAbsolutePosition(38f, 700f);
		        
		         //Scale to new height and new width of image
		         image1.scaleAbsolute(92, 100);
		         //Add to document
		         document.add(image1);
		         //Image image2 = Image.getInstance("C:\\Users\\cmahesh\\Downloads\\saibaba2.jpeg");
		         Image image2 = Image.getInstance("http://accounts.saisansthan.in/static/saibaba2.jpeg"); 
		         //Fixed Positioning
		         image2.setAbsolutePosition(470f, 700f);
		         //Scale to new height and new width of image
		         image2.scaleAbsolute(92, 100);
		         //Add to document
		         document.add(image2);   
			
		         document.add(new Paragraph(ss, blueFont1));
	       
			 //document.add(new Paragraph("                                          SHRI SHIRIDI SAI BABA SANSTHAN TRUST                                    "));
	         document.add(new Paragraph("                                                             Regd. No. 646/92                                           "));
	         document.add(new Paragraph("                                                Dilsukhnagar, Hyderabad,TS-500 060                                    "));
	         document.add(new Paragraph("                                                               Ph:24066566.                                            "));
	         document.add(new Paragraph("                                               Contact us on:-8500966566 (Phone Pay).            "));
	         document.add(new Paragraph("                                                  website url: "+new Chunk("https://saisansthan.in/").setAnchor("https://saisansthan.in/")        ));
	         document.add(new Paragraph("                                                                                                                    "));  
	         document.add(new Paragraph("MyBookings                                                                                                          ",blueFont));
	
	         PdfPTable table = new PdfPTable(2); // 2 columns.
	         table.setWidthPercentage(100); //Width 100%
	         table.setSpacingBefore(10f); //Space before table
	         table.setSpacingAfter(10f); //Space after table
	  
	         //Set Column widths
	         float[] columnWidths = {2f, 2f};
	         table.setWidths(columnWidths);
	  
	         PdfPCell cell1 = new PdfPCell(new Paragraph(" Receipt Number: ",blueFont));
	         cell1.setPaddingLeft(10);
	         cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         
	  
	         PdfPCell cell2 = new PdfPCell(new Paragraph(billingId,blueFont));
	         cell2.setPaddingLeft(10);
	         cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell1);
	         table.addCell(cell2);
	             
	         PdfPCell cell3 = new PdfPCell(new Paragraph(" Name: ",blueFont));
	         cell3.setPaddingLeft(10);
	         cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell4 = new PdfPCell(new Paragraph(customerName,blueFont));
	         cell4.setPaddingLeft(10);
	         cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell3);
	         table.addCell(cell4);
	         
	         PdfPCell cell5 = new PdfPCell(new Paragraph(" Gothram: ",blueFont));
	         cell5.setPaddingLeft(10);
	         cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell6 = new PdfPCell(new Paragraph(gothram,blueFont));
	         cell6.setPaddingLeft(10);
	         cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell5);
	         table.addCell(cell6);
	         
	         PdfPCell cell7 = new PdfPCell(new Paragraph(" Seva Name: ",blueFont));
	         cell7.setPaddingLeft(10);
	         cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell8 = new PdfPCell(new Paragraph(subHeadName,blueFont));
	         cell8.setPaddingLeft(10);
	         cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell7);
	         table.addCell(cell8);
	         
	         PdfPCell cell9 = new PdfPCell(new Paragraph(" Ticket Qty: ",blueFont));
	         cell9.setPaddingLeft(10);
	         cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell10 = new PdfPCell(new Paragraph(quantity,blueFont));
	         cell10.setPaddingLeft(10);
	         cell10.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell10.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell9);
	         table.addCell(cell10);
	         
	         PdfPCell cell11 = new PdfPCell(new Paragraph(" Total Amount: ",blueFont));
	         cell11.setPaddingLeft(10);
	         cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell12 = new PdfPCell(new Paragraph(totalAmount,blueFont));
	         cell12.setPaddingLeft(10);
	         cell12.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell11);
	         table.addCell(cell12);
	         
	         PdfPCell cell13 = new PdfPCell(new Paragraph(" Seva Booking Date: ",blueFont));
	         cell13.setPaddingLeft(10);
	         cell13.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell13.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell14 = new PdfPCell(new Paragraph(bookingDate,blueFont));//bookingDate.substring(0, 10)));
	         cell14.setPaddingLeft(10);
	         cell14.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell14.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell13);
	         table.addCell(cell14);
	         
	         
	         PdfPCell cell15 = new PdfPCell(new Paragraph(" Seva Performed Date: ",blueFont));
	         cell15.setPaddingLeft(10);
	         cell15.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell15.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell16 = new PdfPCell(new Paragraph(performedDate,blueFont));
	         cell16.setPaddingLeft(10);
	         cell16.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell16.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell15);
	         table.addCell(cell16);
	         
	         PdfPCell cell17 = new PdfPCell(new Paragraph(" Phone Number: ",blueFont));
	         cell17.setPaddingLeft(10);
	         cell17.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell17.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  
	         PdfPCell cell18 = new PdfPCell(new Paragraph(phoneNo,blueFont));
	         cell18.setPaddingLeft(10);
	         cell18.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell18.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         table.addCell(cell17);
	         table.addCell(cell18);
	        
	      
	     	         
	         document.add(table);
	         
	         document.add(new Paragraph("                                                    *** Thank You Visit Again! ***                                           ",blueFont));
	         document.add(new Paragraph("Note : We sent an acknowledgment mail to your mail id.If you do not find our mail in your inbox please once check in your spam folder.Thank you",blueFont));        
	         document.add(new Paragraph("                                                                                              "));
	         document.add(new Paragraph("Please Subscribe & follow us on:"));
	         document.add(new Paragraph("Facebook: "+new Chunk("https://www.facebook.com/dilsuknagarsaibabatemple").setAnchor("https://www.facebook.com/dilsuknagarsaibabatemple/")));
	         document.add(new Paragraph("Youtube : "+new Chunk("https://www.youtube.com/channel/UCtSaXmy5KswqzjJNuVsz_PA").setAnchor("https://www.youtube.com/channel/UCtSaXmy5KswqzjJNuVsz_PA/")));
	         document.add(new Paragraph("Instagram: "+new Chunk("https://instagram.com/dilsukhnagar_sai_baba_temple?utm_medium=copy_link").setAnchor("https://instagram.com/dilsukhnagar_sai_baba_temple?utm_medium=copy_link/")));
	         document.add(new Paragraph("To Receive daily updates please provide your details on the following links. "+new Chunk("https://forms.gle/cr8HJGwCRaryQZWn9").setAnchor("https://forms.gle/cr8HJGwCRaryQZWn9/")));
	         document.close();	        
	         writer.close();
	        System.out.println("productId.."+productId);
	        int a=Integer.parseInt(productId);
	             if(a==21539 || a==20054 || a==25669 ||a==20070 ||a==20071||a==20064) {
	        	 System.out.println("customername.."+customerName+" sheadname.."+subHeadName+" performdDate.."+performedDate+" phone.."+phoneNo);	
	        		sevaReceipt(phoneNo, customerName, subHeadName, performedDate,billingId);	    	 
			     }else{
	                 donation(phoneNo, customerName, subHeadName, totalAmount,billingId);			     
			     }		    	 			    	 
			     
			     
			     
	
	         
	         
	         
	         
	     
	      } catch (DocumentException e)
	      {
	         e.printStackTrace();
	      } catch (FileNotFoundException e)
	      {
	         e.printStackTrace();
	      }
		  catch (SQLException e2) {
              e2.printStackTrace();
          }
		  catch (Exception e3) {
	          e3.printStackTrace();
	      }

	  
  
	     	}
	
	public void sevaReceipt(String phoneNo,String customerName,String subHeadName,String performedDate,String billingId)throws ServletException, IOException {
		 URL url = new URL("https://push.aclwhatsapp.com/pull-platform-receiver/wa/messages/");
	     	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	     	connection.setDoOutput(true);
	     	connection.setRequestMethod("POST");
	     	connection.setRequestProperty("Content-Type", "application/json");
	     	connection.setRequestProperty("user", "shrisbpd");
	     	connection.setRequestProperty("pass", "S{hvR9$Z6st");
         String jsonString="{\"messages\": [{\"sender\": \"9052966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"mediaTemplate\",\"mediaTemplate\": {\"mediaUrl\": \"http://accounts.saisansthan.in/static/"+billingId+".pdf\",\"contentType\":\"application/pdf\",\"filename\": \"seva-receipt\",\"template\": \"receipt_for_seva_booking\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\",\"3\": \"var3\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
	     	//String jsonString="{\"messages\": [{\"sender\": \"9052966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"mediaTemplate\",\"mediaTemplate\": {\"mediaUrl\": \"https://file-examples-com.github.io/uploads/2017/10/file-sample_150kB.pdf\",\"contentType\":\"application/pdf\",\"filename\": \"seva-receipt\",\"template\": \"receipt_for_seva_booking\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\",\"3\": \"var3\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
	     	System.out.println("json string.."+jsonString);
	     	jsonString=jsonString.replace("pNo",  phoneNo);
         jsonString=jsonString.replace("var1", customerName);
         jsonString=jsonString.replace("var2", subHeadName);
         jsonString=jsonString.replace("var3", performedDate);
         System.out.println("json string.."+jsonString);
	        OutputStream os1 = connection.getOutputStream();
	     	byte[] input = jsonString.getBytes("utf-8");
	     	os1.write(input, 0, input.length);			
	     	int code = connection.getResponseCode();
	     	System.out.println("status code.in seva receipt.document..!"+code);		    	 

	}
	public void donation(String phoneNo,String customerName,String subHeadName,String totalAmount,String billingId)throws ServletException, IOException {
		
	 	 System.out.println("customername.."+customerName+" sheadname.."+subHeadName+" totalAmount.."+totalAmount+" phone.."+phoneNo);			    	 
      	URL url = new URL("https://push.aclwhatsapp.com/pull-platform-receiver/wa/messages/");
	     	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	     	connection.setDoOutput(true);
	     	connection.setRequestMethod("POST");
	     	connection.setRequestProperty("Content-Type", "application/json");
	     	connection.setRequestProperty("user", "shrisbpd");
	     	connection.setRequestProperty("pass", "S{hvR9$Z6st");
         String jsonString="{\"messages\": [{\"sender\": \"9052966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"mediaTemplate\",\"mediaTemplate\": {\"mediaUrl\": \"http://accounts.saisansthan.in/static/"+billingId+".pdf\",\"contentType\":\"application/pdf\",\"filename\": \"seva-receipt\",\"template\": \"receipt_for_donation\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\",\"3\": \"var3\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
	     	//String jsonString="{\"messages\": [{\"sender\": \"918297966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"mediaTemplate\",\"mediaTemplate\": {\"mediaUrl\": \"https://file-examples-com.github.io/uploads/2017/10/file-sample_150kB.pdf\",\"contentType\":\"application/pdf\",\"filename\": \"receipt\",\"template\": \"receipt_for_donation\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\",\"3\": \"var3\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
	     	System.out.println("json string.."+jsonString);
	     	jsonString=jsonString.replace("pNo",  phoneNo);
         jsonString=jsonString.replace("var1", customerName);
         jsonString=jsonString.replace("var2", subHeadName);
         jsonString=jsonString.replace("var3", totalAmount);
         System.out.println("json string.."+jsonString);
	        OutputStream os1 = connection.getOutputStream();
	     	byte[] input = jsonString.getBytes("utf-8");
	     	os1.write(input, 0, input.length);			
	     	int code = connection.getResponseCode();
	     	System.out.println("status code.receipt for donation.document..!"+code);		    	 

	}
	
	
	public void birthdayWishes(String username,String wish,String mobileNumber)throws IOException {
		try {
		csp.addToOptin(mobileNumber);
        URL url = new URL("https://push.aclwhatsapp.com/pull-platform-receiver/wa/messages/");
     	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
     	connection.setDoOutput(true);
     	connection.setRequestMethod("POST");
     	connection.setRequestProperty("Content-Type", "application/json");
     	connection.setRequestProperty("user", "shrisbpd");
     	connection.setRequestProperty("pass", "S{hvR9$Z6st");
     	//local//String jsonString="{\"messages\": [{\"sender\": \"918297966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"template\",\"template\": {\"body\": [],\"mediaUrl\": \"https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg\",\"contentType\":\"image/jpeg\",\"template\": \"birthday_anniversary_greetings\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
     	String jsonString="{\"messages\": [{\"sender\": \"\r\n"
     			+ "9052966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"template\",\"template\": {\"body\": [],\"mediaUrl\": \"http://accounts.saisansthan.in/static/saibaba2.jpeg\",\"contentType\":\"image/jpeg\",\"template\": \"birthday_anniversary_greetings\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
		jsonString=jsonString.replace("pNo",  mobileNumber);
        jsonString=jsonString.replace("var1", username);
        jsonString=jsonString.replace("var2", wish);
    	System.out.println("json string..in birthday wishes..document..!"+jsonString);
        OutputStream os1 = connection.getOutputStream();
     	byte[] input = jsonString.getBytes("utf-8");
     	os1.write(input, 0, input.length);			
     	int code = connection.getResponseCode();
     	System.out.println("status code..in birthday wishes..document..!"+code);
		}
		 catch(Exception ie) {
	            ie.printStackTrace();
	        } 
	}

	public void getNdaLtaWishes(String username,String sevaname,String date,String mobileNumber)throws IOException {
		csp.addToOptin(mobileNumber);
        URL url = new URL("https://push.aclwhatsapp.com/pull-platform-receiver/wa/messages/");
     	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
     	connection.setDoOutput(true);
     	connection.setRequestMethod("POST");
     	connection.setRequestProperty("Content-Type", "application/json");
     	connection.setRequestProperty("user", "\r\n"
     			+ "shrisbpd");
     	connection.setRequestProperty("pass", "S{hvR9$Z6st");
     	//local//String jsonString="{\"messages\": [{\"sender\": \"918297966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"template\",\"template\": {\"body\": [],\"mediaUrl\": \"http://accounts.saisansthan.in/static/saibaba.jpeg\",\"contentType\":\"image/jpeg\",\"template\": \"lta_nad_ltm\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\",\"3\": \"var3\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
     	String jsonString="{\"messages\": [{\"sender\": \"\r\n"
     			+ "9052966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"template\",\"template\": {\"body\": [],\"mediaUrl\": \"https://file-examples-com.github.io/uploads/2017/10/file_example_JPG_100kB.jpg\",\"contentType\":\"image/jpeg\",\"template\": \"lta_nad_ltm\",\"parameters\": {\"1\": \"var1\",\"2\": \"var2\",\"3\": \"var3\"},\"langCode\": \"en\"}}],\"responseType\": \"json\"}";
		jsonString=jsonString.replace("pNo",  mobileNumber);
        jsonString=jsonString.replace("var1", username);
        jsonString=jsonString.replace("var2", sevaname);
        jsonString=jsonString.replace("var3", date);
        System.out.println("json string..in nda lta wishes..document..!"+jsonString);
        OutputStream os1 = connection.getOutputStream();
     	byte[] input = jsonString.getBytes("utf-8");
     	os1.write(input, 0, input.length);			
     	int code = connection.getResponseCode();
     	System.out.println("status code.in lda-lta.document..!"+code);
     
	}

	
	
	
}
