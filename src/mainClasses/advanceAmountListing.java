package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.advance_amount;
import dbase.sqlcon.ConnectionHelper;

public class advanceAmountListing 
{
	Connection con=null;
	Statement st=null;
	ResultSet rs=null;
	public advanceAmountListing()
	{
		try 
		{
			 // Load the database driver
			con = ConnectionHelper.getConnection();
			st=con.createStatement();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public List selectAllAdvanceAmount()
	{
		List l=new ArrayList();
		try
		{
			
			String query="select * from advanceamount";
			rs=st.executeQuery(query);
			while(rs.next())
			{
				advance_amount aa=new advance_amount();
				
				aa.setAdvanceamount_id(rs.getString(1));
				aa.setDatetime(rs.getString(2));
				aa.setMaster_suite(rs.getString(3));
				aa.setExecutive_suite(rs.getString(4));
				aa.setDeluxe_suite(rs.getString(5));
				aa.setSuite(rs.getString(6));
				aa.setDeluxe_room_ac(rs.getString(7));
				aa.setSpecial_room_nonac(rs.getString(8));
				aa.setDeluxe_room_nonac(rs.getString(9));
				aa.setDeluxe_room_ac_2beds(rs.getString(10));
				aa.setBanquet_hall_5th_floor(rs.getString(11));
				aa.setBanquet_hall_4th_floor(rs.getString(12));
				aa.setExtra1(rs.getString(13));
				aa.setExtra1(rs.getString(14));
				aa.setExtra1(rs.getString(15));
				aa.setExtra1(rs.getString(16));
				aa.setExtra1(rs.getString(17));
				
				l.add(aa);
			}
			
			st.close();
			con.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return l;
		
	}

}
