package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.minorhead;

public class minorheadListing {
	private String error = "";

	public void seterror(String error) {
		this.error = error;
	}

	public String geterror() {
		return this.error;
	}

	private String minorhead_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;

	public void setminorhead_id(String minorhead_id) {
		this.minorhead_id = minorhead_id;
	}

	public String getminorhead_id() {
		return (this.minorhead_id);
	}

	public List getminorhead() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM minorhead order by SUBSTRING(minorhead_id ,1)*1 asc";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"), rs
						.getString("name"), rs.getString("description"), rs
						.getString("major_head_id"), rs
						.getString("head_account_id"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getMinorHeadNameBasedOnMinorHeadId(String minorHeadId){
		String name="";
		try{
			c=ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectQuery="SELECT name from minorhead where minorhead_id="+minorHeadId;
			ResultSet rs=s.executeQuery(selectQuery);
			while(rs.next()){
				name=rs.getString("name");
			}
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}
	
	
	
	public String getminorheadname(String minorheadname, String head_account_id) {
		String name = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name FROM minorhead where name like '%"
					+ minorheadname
					+ "%' and head_account_id='"
					+ head_account_id + "'";
			ResultSet rs = s.executeQuery(selectquery);
			System.out.println();
			System.out.println("slect query======>"+selectquery);
			if (rs.next()) {
				name = rs.getString("minorhead_id") + " "
						+ rs.getString("name");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}

	
	public String getminorheadname(String minorId,String majorId ,String head_account_id) {
		String name = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name FROM minorhead where major_head_id = "+ majorId + " and head_account_id="+ head_account_id + " and minorhead_id="+minorId;
			ResultSet rs = s.executeQuery(selectquery);
			System.out.println();
			System.out.println("slect query======>"+selectquery);
			if (rs.next()) {
				name = rs.getString("minorhead_id") + " "
						+ rs.getString("name");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}
	
	
	
	public List getMminheadSearch(String minior, String catid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			String majorHead = "";
			if (catid != null && !catid.equals("")) {
				majorHead = "and major_head_id='" + catid + "'";
			}
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3  FROM minorhead where  (minorhead_id like '%"
					+ minior
					+ "%' || name like '%"
					+ minior
					+ "%')"
					+ majorHead;
			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"), rs
						.getString("name"), rs.getString("description"), rs
						.getString("major_head_id"), rs
						.getString("head_account_id"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getminorheadName(String minorhead_id) {
		String name = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT name FROM minorhead where minorhead_id='"
					+ minorhead_id + "' ";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				name = rs.getString("name");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}

	public List<minorhead> getMinorHeadBasdOnCompany(String major_head_id) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM minorhead  where  major_head_id = '"
					+ major_head_id + "' group by minorhead_id ";
			System.out.println("66666.....=====>" + selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"), rs
						.getString("name"), rs.getString("description"), rs
						.getString("major_head_id"), rs
						.getString("head_account_id"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List<minorhead> getMinorHeadBasdOnCompany1(String major_head_id,
			String minor_head, String report) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM minorhead  where  major_head_id = '"
					+ major_head_id
					+ "' and minorhead_id='"
					+ minor_head
					+ "' and extra1 like  '%" + report + "%'";
			// System.out.println("44444=====>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"), rs
						.getString("name"), rs.getString("description"), rs
						.getString("major_head_id"), rs
						.getString("head_account_id"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getminorhead(String minorhead_id) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM minorhead where   minorhead_id = '"
					+ minorhead_id + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"), rs
						.getString("name"), rs.getString("description"), rs
						.getString("major_head_id"), rs
						.getString("head_account_id"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getminorhead1(String majorhead_id, String report) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM minorhead where  major_head_id = '"
					+ majorhead_id + "' and extra3 not like '%" + report + "%'";
			ResultSet rs = s.executeQuery(selectquery);
			// System.out.println("query is ;"+selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"), rs
						.getString("name"), rs.getString("description"), rs
						.getString("major_head_id"), rs
						.getString("head_account_id"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getminorheadValidate(String minorhead_id, String hoid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM minorhead where   minorhead_id = '"
					+ minorhead_id + "' and head_account_id='" + hoid + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"), rs
						.getString("name"), rs.getString("description"), rs
						.getString("major_head_id"), rs
						.getString("head_account_id"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getMminorhead(String minorhead_id) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM minorhead where  minorhead_id = '"
					+ minorhead_id + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"),
						rs.getString("name"),
						rs.getString("description"),
						rs.getString("major_head_id"), 
						rs.getString("head_account_id"),
						rs.getString("extra1"),
						rs.getString("extra2"),
						rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getMminorheadsBasedONHOA(String hoa) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT minorhead_id,name,description,major_head_id,head_account_id,extra1,extra2,extra3 FROM minorhead where  head_account_id = '"
					+ hoa + "' || head_account_id='1'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new minorhead(rs.getString("minorhead_id"), rs
						.getString("name"), rs.getString("description"), rs
						.getString("major_head_id"), rs
						.getString("head_account_id"), rs.getString("extra1"),
						rs.getString("extra2"), rs.getString("extra3")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getHeadOFAccountID(String minorhead_id) {
		String name = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT head_account_id FROM minorhead where minorhead_id='"
					+ minorhead_id + "' ";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				name = rs.getString("head_account_id");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}

	public String getHeadOFAccountIDOFMajorhead_id(String major_head_id) {
		String name = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT head_account_id FROM minorhead where major_head_id='"
					+ major_head_id + "' ";
			// System.out.println("3333333====>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				name = rs.getString("head_account_id");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}

	public String getMinorHeadNameByCat1(String minrheadid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();

			selectquery = "SELECT name FROM minorhead where  minorhead_id = '"
					+ minrheadid + "'";

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("name");
			}

		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}
}