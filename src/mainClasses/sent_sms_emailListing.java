package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.sent_sms_email;

public class sent_sms_emailListing 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String uniqId;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	
	public String getUniqId() {
		return uniqId;
	}
	public void setUniqId(String uniqId) {
		this.uniqId = uniqId;
	}
	
	public List getSentSmsEmailBasedOnDates(String fromdate,String todate,String type)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver 
		String date="";
		String query1="";
		if(fromdate!=null && todate!=null){
			date=" createdDate >= '"+fromdate+"' and createdDate <= '"+todate+"'";
		}
		
		if(!type.equals("ALL"))
		{
			query1 = " and purpose = '"+type+"'";
		}
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT uniqId,regId,name,purpose,daysremainder,email,phone,createdDate,extra1,extra2,extra3,extra4,extra5 FROM sent_sms_email where "+date+query1+" order by createdDate"; 
	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new sent_sms_email(rs.getString("uniqId"),rs.getString("regId"),rs.getString("name"),rs.getString("purpose"),rs.getString("daysremainder"),rs.getString("email"),rs.getString("phone"),rs.getString("createdDate"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));
				}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
}
