package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import dbase.sqlcon.ConnectionHelper;
import java.util.ArrayList;
import java.util.List;

import beans.customerapplication;
public class customerapplicationListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String cust_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setcust_id(String cust_id)
{
this.cust_id = cust_id;
}
public String getcust_id(){
return(this.cust_id);
}
public List getcustomerapplication()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cust_id,billingId,title,first_name,middle_name,last_name,email_id,phone,gender,dateof_birth,address1,address2,pincode,city,gothram,from_date,to_date,image,idproof1,idproof2,pancard_no,extra1,extra2,extra3,extra4 FROM customerapplication "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMcustomerapplication(String cust_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cust_id,billingId,title,first_name,middle_name,last_name,email_id,phone,gender,dateof_birth,address1,address2,pincode,city,gothram,from_date,to_date,image,idproof1,idproof2,pancard_no,extra1,extra2,extra3,extra4 FROM customerapplication where  cust_id = '"+cust_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMcustomerDetailsBasedOnBillingId(String billingID)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cust_id,billingId,title,first_name,middle_name,last_name,email_id,phone,gender,dateof_birth,address1,address2,pincode,city,gothram,from_date,to_date,image,idproof1,idproof2,pancard_no,extra1,extra2,extra3,extra4 FROM customerapplication where  billingId = '"+billingID+"'";
//System.out.println("customer application......"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMcustomerDetailsBasedOnBillingIdDateWise(String billingID,String performedDate)
{
	ArrayList list = new ArrayList();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT cust_id,billingId,title,first_name,middle_name,last_name,email_id,phone,gender,dateof_birth,address1,address2,pincode,city,gothram,from_date,to_date,image,idproof1,idproof2,pancard_no,extra1,extra2,extra3,extra4 FROM customerapplication where  extra1='"+performedDate+"' and billingId = '"+billingID+"'"; 
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));
		
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
	return list;
}


public String getPerformerName(String billID)
{
String name ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cust_id,billingId,title,first_name,middle_name,last_name,email_id,phone,gender,dateof_birth,address1,address2,pincode,city,gothram,from_date,to_date,image,idproof1,idproof2,pancard_no,extra1,extra2,extra3,extra4 FROM customerapplication where  billingId = '"+billID+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	name=rs.getString("last_name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
public String getPancardNum(String billID)
{
String name ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cust_id,billingId,title,first_name,middle_name,last_name,email_id,phone,gender,dateof_birth,address1,address2,pincode,city,gothram,from_date,to_date,image,idproof1,idproof2,pancard_no,extra1,extra2,extra3,extra4 FROM customerapplication where  billingId = '"+billID+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	name=rs.getString("pancard_no");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
public List GetDailyCustomerPerformedDate(String MonthDay)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT ca.cust_id,ca.billingId,ca.title,ca.first_name,ca.middle_name,ca.last_name,ca.email_id,ca.phone,gender,ca.dateof_birth,ca.address1,ca.address2,ca.pincode,ca.city,ca.gothram,from_date,to_date,cp.productId as image,idproof1,idproof2,pancard_no,ca.extra1,ca.extra2,ca.extra3,ca.extra4 FROM customerapplication ca,customerpurchases cp where cp.billingId=ca.billingId and ca.extra1='"+MonthDay+"' and ca.email_id!='' and cp.cash_type!='online pending' group by billingId"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List GetDetailsCustomerPerformedDate(String fromdate,String todate,String productId)
{
	ArrayList list=new ArrayList();
	/*String date="";*/
	try
	{
		/*if(fromdate!=null && todate!=null){
			date=" date>='"+fromdate+"' and date<='"+todate+"'";
			//System.out.println("from db"+fromdate+"===="+todate);
		}*/
		String selectquery="";
		c=ConnectionHelper.getConnection();
		s=c.createStatement();
		selectquery="SELECT c.cust_id,c.billingId,c.title,c.first_name,c.middle_name,c.last_name,c.email_id,c.phone,gender,c.dateof_birth,c.address1,c.address2,c.pincode,c.city,c.gothram,from_date,to_date,p.productId as image,p.customername, idproof1,idproof2,pancard_no,c.extra1,p.extra2 as extra2,c.extra3,c.extra4 FROM sansthanaccounts.customerapplication c,customerpurchases p where p.billingId=c.billingId and c.extra1 between '"+fromdate+"' and '"+todate+"'  and  p.productId='"+productId+"' and p.cash_type!='online pending'";
		//System.out.println("======>"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
		 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));
		 //System.out.println(rs.getString("extra1"));
		}
		
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		e.printStackTrace();
		//System.out.println("Exception is ;"+e);
	}
	return list;
}
public List GetDetailsCustomerBasedOnDate(String fromdate,String todate,String productId)
{
	ArrayList list=new ArrayList();
	String date="";
	try
	{
		if(fromdate!=null && todate!=null){
			date=" date>='"+fromdate+"' and date<='"+todate+"'";
			//System.out.println("from db"+fromdate+"===="+todate);
		}
		String selectquery="";
		c=ConnectionHelper.getConnection();
		s=c.createStatement();
		/*if(productId.equals("20064") ||productId.equals("20066") ||productId.equals("20067") ||productId.equals("20070") ||productId.equals("20071") ||productId.equals("20091"))
		{*/
		selectquery="SELECT c.cust_id,c.billingId,c.title,c.first_name,c.middle_name,c.last_name,c.email_id,c.phone,gender,c.dateof_birth,c.address1,c.address2,c.pincode,c.city,c.gothram,from_date,to_date,p.productId as image,p.customername, idproof1,idproof2,pancard_no,c.extra1,date,p.extra2 as extra2,c.extra3,c.extra4 FROM sansthanaccounts.customerapplication c,customerpurchases p where "+date+" and p.billingId=c.billingId and p.productId='"+productId+"' ";
		/*}*/
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
		 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));
		/* System.out.println(date);*/
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		e.printStackTrace();
		System.out.println("Exception is ;"+e);
	}
	return list;
}

public List getRCTtwo()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cust_id,billingId,title,first_name,middle_name,last_name,email_id,phone,gender,dateof_birth,address1,address2,pincode,city,gothram,from_date,to_date,image,idproof1,idproof2,pancard_no,extra1,extra2,extra3,extra4 FROM customerapplication where billingId like '%RCT2%'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}





/*public List GetDetailsCustomerPerformedDates(String fromdate,String todate,String productId)
{
	ArrayList list=new ArrayList();
	String date="";
	try
	{
		if(fromdate!=null && todate!=null){
			date=" date>='"+fromdate+"' and date<='"+todate+"'";
			//System.out.println("from db"+fromdate+"===="+todate);
		}
		String selectquery="";
		c=ConnectionHelper.getConnection();
		s=c.createStatement();
		selectquery="SELECT c.cust_id,c.billingId,c.title,c.first_name,c.middle_name,c.last_name,c.email_id,c.phone,gender,c.dateof_birth,c.address1,c.address2,c.pincode,c.city,c.gothram,from_date,to_date,p.productId as image,p.customername, idproof1,idproof2,pancard_no,c.extra1,p.extra2 as extra2,c.extra3,c.extra4 FROM sansthanaccounts.customerapplication c,customerpurchases p where p.billingId=c.billingId and c.extra1 between '"+fromdate+"' and '"+todate+"'  and  p.productId='"+productId+"'";
		
		
		System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
			
			
		 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));
		//System.out.println(rs.getString("extra1"));
		}
		
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		e.printStackTrace();
		//System.out.println("Exception is ;"+e);
	}
	return list;
}*/

public List getcustomerDetailsBasedOnUniqueId(String uniqueId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT ca.cust_id,ca.billingId,ca.title,ca.first_name,ca.middle_name,ca.last_name,ca.email_id,ca.phone,ca.gender,ca.dateof_birth,ca.address1,ca.address2,ca.pincode,ca.city,ca.gothram,ca.from_date,ca.to_date,ca.image,ca.idproof1,ca.idproof2,ca.pancard_no,ca.extra1,ca.extra2,ca.extra3,ca.extra4 FROM customerapplication ca,customerpurchases cp where  ca.billingId = cp.billingId and cp.extra15 = '"+uniqueId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getNADLTAhavingEmail(String productId,String emails,String fromdate,String todate)
{
ArrayList list = new ArrayList();
String emailQuery = "";
String dateQuery = "";
try {
	if(!fromdate.equals("") && !todate.equals(""))
	{
		dateQuery = " and ca.extra1 between '"+fromdate+"' and '"+todate+"'";
	}
	else if(!fromdate.equals("") && todate.equals(""))
	{
		dateQuery = " and cp.extra7 like '%"+fromdate+"%'";
	}
	if(emails.equals("WithEmail")){
		emailQuery = " and trim(cp.extra8) != ''";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(emails.equals("WithOutEmail")){
		emailQuery = " and trim(cp.extra8) = ''";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(emails.equals("WithMobile")){
		emailQuery = " and trim(cp.phoneno) != ''";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(emails.equals("WithOutMobile")){
		emailQuery = " and trim(cp.phoneno) = ''";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(emails.equals("WithEmail&WithMobile")){
		emailQuery = " and trim(cp.extra8) != '' and trim(cp.phoneno) != ''";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(emails.equals("WithEmail&WithOutMobile")){
		emailQuery = " and trim(cp.extra8) != '' and trim(cp.phoneno) = ''";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(emails.equals("WithOutEmail&WithMobile")){
		emailQuery = " and trim(cp.extra8) = '' and trim(cp.phoneno) != ''";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(emails.equals("WithOutEmail&WithOutMobile")){
		emailQuery = " and trim(cp.extra8) = '' and trim(cp.phoneno) = ''";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select ca.cust_id, ca.billingId, ca.title, ca.first_name, ca.middle_name, ca.last_name, ca.email_id, ca.phone, ca.gender, ca.dateof_birth, ca.address1, ca.address2, ca.pincode, ca.city, ca.gothram, ca.from_date, ca.to_date, ca.image, ca.idproof1, ca.idproof2, ca.pancard_no, ca.extra1, ca.extra2, ca.extra3, ca.extra4 from customerapplication ca,customerpurchases cp where ca.billingId = cp.billingId and cp.productId = '"+productId+"'"+dateQuery+emailQuery+" and cp.cash_type!='online pending'";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getNADLTAhavingEmailOrMobile(String productId)
{
ArrayList list = new ArrayList();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select ca.cust_id, ca.billingId, ca.title, ca.first_name, ca.middle_name, ca.last_name, ca.email_id, ca.phone, ca.gender, ca.dateof_birth, ca.address1, ca.address2, ca.pincode, ca.city, ca.gothram, ca.from_date, ca.to_date, ca.image, ca.idproof1, ca.idproof2, ca.pancard_no, ca.extra1, ca.extra2, ca.extra3, ca.extra4 from customerapplication ca,customerpurchases cp where ca.billingId = cp.billingId and cp.productId = '"+productId+"' and cp.cash_type!='online pending' and (trim(cp.phoneno) != '' || trim(cp.extra8) != '')";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getNADLTAhavingEmailOrMobileAndDate(String productId,String fromDate,String toDate)
{
ArrayList list = new ArrayList();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select ca.cust_id, ca.billingId, ca.title, ca.first_name, ca.middle_name, ca.last_name, ca.email_id, ca.phone, ca.gender, ca.dateof_birth, ca.address1, ca.address2, ca.pincode, ca.city, ca.gothram, ca.from_date, ca.to_date, ca.image, ca.idproof1, ca.idproof2, ca.pancard_no, ca.extra1, ca.extra2, ca.extra3, ca.extra4 from customerapplication ca,customerpurchases cp where ca.billingId = cp.billingId and cp.productId = '"+productId+"' and cp.date between '"+fromDate+"' and '"+toDate+"' and cp.cash_type!='online pending' and (trim(cp.phoneno) != '' || trim(cp.extra8) != '')";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getNADLTABasedOnSpecialDay(String specialDay)
{
ArrayList list = new ArrayList();
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select ca.cust_id, ca.billingId, ca.title, ca.first_name, ca.middle_name, ca.last_name, ca.email_id, ca.phone, ca.gender, ca.dateof_birth, ca.address1, ca.address2, ca.pincode, ca.city, ca.gothram, ca.from_date, ca.to_date, ca.image, ca.idproof1, ca.idproof2, ca.pancard_no, ca.extra1, ca.extra2, ca.extra3, ca.extra4 from customerapplication ca,customerpurchases cp where ca.billingId = cp.billingId and cp.extra7 like '%"+specialDay+"%' and cp.cash_type!='online pending'";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getNADLTABasedOnSpecialDayNew(String specialDay,String nadlta)
{
ArrayList list = new ArrayList();
String query = "";
if(nadlta.equals("nadlta"))
{
	query = " and (cp.productId = '20092' || cp.productId = '20063')";
}
else if(nadlta.equals("NAD"))
{
	query = " and cp.productId = '20092'";
}
else if(nadlta.equals("LTA"))
{
	query = " and cp.productId = '20063'";
}

try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select ca.cust_id, ca.billingId, ca.title, ca.first_name, ca.middle_name, ca.last_name, ca.email_id, ca.phone, ca.gender, ca.dateof_birth, ca.address1, ca.address2, ca.pincode, ca.city, ca.gothram, ca.from_date, ca.to_date, ca.image, ca.idproof1, ca.idproof2, ca.pancard_no, ca.extra1, ca.extra2, ca.extra3, ca.extra4 from customerapplication ca,customerpurchases cp where ca.billingId = cp.billingId and cp.extra7 like '%"+specialDay+"%' and cp.cash_type!='online pending'"+query;
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getNADLTABasedOnSpecialDayNewAndDate(String specialDay,String nadlta,String fromDate,String toDate)
{
ArrayList list = new ArrayList();
String query = "";
if(nadlta.equals("nadlta"))
{
	query = " and (cp.productId = '20092' || cp.productId = '20063') and cp.date between  '"+fromDate+"'and '"+toDate+"'";
}
else if(nadlta.equals("NAD"))
{
	query = " and cp.productId = '20092' and cp.date between '"+fromDate+"' and '"+toDate+"'";
}
else if(nadlta.equals("LTA"))
{
	query = " and cp.productId = '20063' and cp.date between '"+fromDate+"'and '"+toDate+"'";
}

try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select ca.cust_id, ca.billingId, ca.title, ca.first_name, ca.middle_name, ca.last_name, ca.email_id, ca.phone, ca.gender, ca.dateof_birth, ca.address1, ca.address2, ca.pincode, ca.city, ca.gothram, ca.from_date, ca.to_date, ca.image, ca.idproof1, ca.idproof2, ca.pancard_no, ca.extra1, ca.extra2, ca.extra3, ca.extra4 from customerapplication ca,customerpurchases cp where ca.billingId = cp.billingId and cp.extra7 like '%"+specialDay+"%' and cp.cash_type!='online pending'"+query;
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}




public List getListForDOBOrAnnivOrNADLTAReminder(String columnName,String value,String likeOperator,String withPhone,String extra)
{
ArrayList list = new ArrayList();
String query1 = "";
String query2 = "";
String query3 = "";
try {
	if(likeOperator.equals("like")){
		query1 = " and ca."+columnName+" like '"+value+"'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	if(likeOperator.equals("")){
		query1 = " and trim(ca."+columnName+") = '"+value+"'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	if(withPhone.equals("withPhone")){
		query2 = " and (trim(cp.phoneno) != '' || trim(cp.extra8) != '')";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	
	if(extra.equals("NAD")){
		query3 = " and trim(cp.productId) = '20092'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(extra.equals("LTA")){
		query3 = " and trim(cp.productId) = '20063'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(extra.equals("NAD AND LTA")){
		query3 = " and (trim(cp.productId) = '20092' || trim(cp.productId) = '20063')";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select ca.cust_id, ca.billingId, ca.title, ca.first_name, ca.middle_name, ca.last_name, ca.email_id, ca.phone, ca.gender, ca.dateof_birth, ca.address1, ca.address2, ca.pincode, ca.city, ca.gothram, ca.from_date, ca.to_date, ca.image, ca.idproof1, ca.idproof2, ca.pancard_no, ca.extra1, ca.extra2, ca.extra3, ca.extra4 from customerapplication ca,customerpurchases cp where ca.billingId = cp.billingId "+query1+query2+query3+" and cp.cash_type!='online pending' group by ca.first_name";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getListForDOBOrAnnivOrNADLTAReminderDate(String columnName,String value,String likeOperator,String withPhone,String extra,String fromDate,String toDate)
{
ArrayList list = new ArrayList();
String query1 = "";
String query2 = "";
String query3 = "";
try {
	if(likeOperator.equals("like")){
		query1 = " and ca."+columnName+" like '"+value+"'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	if(likeOperator.equals("")){
		query1 = " and trim(ca."+columnName+") = '"+value+"'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	if(withPhone.equals("withPhone")){
		query2 = " and (trim(cp.phoneno) != '' || trim(cp.extra8) != '')";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	
	if(extra.equals("NAD")){
		query3 = " and trim(cp.productId) = '20092'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(extra.equals("LTA")){
		query3 = " and trim(cp.productId) = '20063'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	else if(extra.equals("NAD AND LTA")){
		query3 = " and (trim(cp.productId) = '20092' || trim(cp.productId) = '20063')";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select ca.cust_id, ca.billingId, ca.title, ca.first_name, ca.middle_name, ca.last_name, ca.email_id, ca.phone, ca.gender, ca.dateof_birth, ca.address1, ca.address2, ca.pincode, ca.city, ca.gothram, ca.from_date, ca.to_date, ca.image, ca.idproof1, ca.idproof2, ca.pancard_no, ca.extra1, ca.extra2, ca.extra3, ca.extra4 from customerapplication ca,customerpurchases cp where ca.billingId = cp.billingId "+query1+query2+query3+" and cp.cash_type!='online pending' and cp.date between '"+fromDate+"' and '"+toDate+"' group by ca.first_name";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new customerapplication(rs.getString("cust_id"),rs.getString("billingId"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("email_id"),rs.getString("phone"),rs.getString("gender"),rs.getString("dateof_birth"),rs.getString("address1"),rs.getString("address2"),rs.getString("pincode"),rs.getString("city"),rs.getString("gothram"),rs.getString("from_date"),rs.getString("to_date"),rs.getString("image"),rs.getString("idproof1"),rs.getString("idproof2"),rs.getString("pancard_no"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}