package mainClasses;

import dbase.sqlcon.SainivasConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.roomtype;
public class roomtypeListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String roomtype_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setroomtype_id(String roomtype_id)
{
this.roomtype_id = roomtype_id;
}
public String getroomtype_id(){
return(this.roomtype_id);
}
public List getroomtype()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM roomtype "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new roomtype(rs.getString("roomtype_id"),rs.getString("roomtype_name"),rs.getString("adults"),rs.getString("childerns"),rs.getString("default_price"),rs.getString("allow_extrabed"),rs.getString("extrabed_charge"),rs.getString("noof_rooms"),rs.getString("image"),rs.getString("image_name"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("charges"),rs.getString("priority"),rs.getString("rty"),rs.getString("newr"),rs.getString("newextra1")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getroomtypeRoomsOnly()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM roomtype where rty='rooms'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new roomtype(rs.getString("roomtype_id"),rs.getString("roomtype_name"),rs.getString("adults"),rs.getString("childerns"),rs.getString("default_price"),rs.getString("allow_extrabed"),rs.getString("extrabed_charge"),rs.getString("noof_rooms"),rs.getString("image"),rs.getString("image_name"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("charges"),rs.getString("priority"),rs.getString("rty"),rs.getString("newr"),rs.getString("newextra1")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getPriority(String roomtype_id)
{

	String list = "";//new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT priority FROM roomtype where roomtype_id='"+roomtype_id+"'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list=rs.getString("priority");

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
///////////////////////////////////////////////////////////////////
public List getroomtypePriority(String priority)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT roomtype_id,roomtype_name,adults,childerns,default_price,allow_extrabed,extrabed_charge,noof_rooms,image,image_name,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM roomtype where priority<='"+priority+"'  order by priority DESC "; 
System.out.println("getroomtypePriority = "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new roomtype(rs.getString("roomtype_id"),rs.getString("roomtype_name"),rs.getString("adults"),rs.getString("childerns"),rs.getString("default_price"),rs.getString("allow_extrabed"),rs.getString("extrabed_charge"),rs.getString("noof_rooms"),rs.getString("image"),rs.getString("image_name"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
//////////////////////////////////////////////////////////////////
public List getMroomtype(String roomtype_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM roomtype where  roomtype_id = '"+roomtype_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new roomtype(rs.getString("roomtype_id"),rs.getString("roomtype_name"),rs.getString("adults"),rs.getString("childerns"),rs.getString("default_price"),rs.getString("allow_extrabed"),rs.getString("extrabed_charge"),rs.getString("noof_rooms"),rs.getString("image"),rs.getString("image_name"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("charges"),rs.getString("priority"),rs.getString("rty"),rs.getString("newr"),rs.getString("newextra1")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getName(String roomtype_id)
{
String list = "";//new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT roomtype_name FROM roomtype where  roomtype_id = '"+roomtype_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
list=rs.getString("roomtype_name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getExtrabedCharge(String roomtype_id)
{
String list = "";//new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT rt.extrabed_charge FROM room r,roomtype rt where r.room_id='"+roomtype_id+"' and rt.roomtype_id=r.roomtype_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
list=rs.getString("extrabed_charge");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getRoomTypeRent(String room_number)
{
String default_price = "";//new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select default_price FROM roomtype where roomtype_id in(SELECT roomtype_id FROM room where room_number='"+room_number+"')"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	default_price=rs.getString("default_price");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return default_price;
}

public String RoomTypeRent(String roomtype_id)
{
String default_price = "";//new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select default_price FROM roomtype where roomtype_id ='"+roomtype_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	default_price=rs.getString("default_price");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return default_price;
}




public String getRoomTypeName(String room_number)
{
String default_price = "";//new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select roomtype_name FROM roomtype where roomtype_id in(SELECT roomtype_id FROM room where room_number='"+room_number+"')"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	default_price=rs.getString("roomtype_name");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return default_price;
}
public List<roomtype> getroomtypeID(String active){
List<roomtype> list = new ArrayList<roomtype>();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM roomtype where newextra2 = '"+active+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new roomtype(rs.getString("roomtype_id"),rs.getString("roomtype_name"),rs.getString("adults"),rs.getString("childerns"),rs.getString("default_price"),rs.getString("allow_extrabed"),rs.getString("extrabed_charge"),rs.getString("noof_rooms"),rs.getString("image"),rs.getString("image_name"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("charges"),rs.getString("priority"),rs.getString("rty"),rs.getString("newr"),rs.getString("newextra1")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}