package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.AdvanceService;
import dbase.sqlcon.ConnectionHelper;

public class advance_Listing {
		Connection con=null;
		Statement st=null;
		ResultSet rs=null;
		
		public List<AdvanceService> getInvoiceId(String headAccountId){
			List<AdvanceService> list=new ArrayList<AdvanceService>();
			
	
			try {
				con=ConnectionHelper.getConnection();
				st=con.createStatement();
				String query="select invoiceId from advance where extra1='"+headAccountId+"' and amount!=extra2";
				rs=st.executeQuery(query);
				System.out.println("query..."+query);
				while(rs.next()){
					list.add(new AdvanceService(rs.getString("invoiceId")));
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return list;
		}
		
		public List<AdvanceService> getTotalDetails(String headAccountId,String fromDate,String todate){
			List<AdvanceService> list=new ArrayList<AdvanceService>();
			try {
				con=ConnectionHelper.getConnection();
				st=con.createStatement();
				String query="";
				
					
				if(headAccountId== ""){
				query="select AdvId,vendorId,narration,date,amount,majorHeadId,minorHeadId,subHeadId,MSEId,bankId,uniqueId,invoiceId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra12,extra13,extra14,extra15 from advance where extra1!='"+headAccountId+"' and date between '"+fromDate+"' and '"+todate+"'";
				}
				else{
					query="select AdvId,vendorId,narration,date,amount,majorHeadId,minorHeadId,subHeadId,MSEId,bankId,uniqueId,invoiceId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra12,extra13,extra14,extra15 from advance where extra1='"+headAccountId+"' and date between '"+fromDate+"' and '"+todate+"'";	
				}
				rs=st.executeQuery(query);
				System.out.println("query..."+query);
				while(rs.next()){
					list.add(new AdvanceService(rs.getString("AdvId"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("majorHeadId"),rs.getString("minorHeadId"),rs.getString("subHeadId"),rs.getString("MSEId"),rs.getString("bankId"),rs.getString("uniqueId"),rs.getString("invoiceId"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return list;
		}
		
		public List<AdvanceService> getTotalDetailsBasedOnID(String Invoice,String MSEId){
			List<AdvanceService> list=new ArrayList<AdvanceService>();
			try {
				con=ConnectionHelper.getConnection();
				st=con.createStatement();
				String query="";
				
					
				
				query="select AdvId,vendorId,narration,date,amount,majorHeadId,minorHeadId,subHeadId,MSEId,bankId,uniqueId,invoiceId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra12,extra13,extra14,extra15 from advance where invoiceId='"+Invoice+"' and MSEId='"+MSEId+"'";
				
				rs=st.executeQuery(query);
				System.out.println("query..."+query);
				while(rs.next()){
					list.add(new AdvanceService(rs.getString("AdvId"),rs.getString("vendorId"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("majorHeadId"),rs.getString("minorHeadId"),rs.getString("subHeadId"),rs.getString("MSEId"),rs.getString("bankId"),rs.getString("uniqueId"),rs.getString("invoiceId"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return list;
		}
		
		public String getInvoiceIdAmount(String InvoiceId){
			String amount="";
			try {
				con=ConnectionHelper.getConnection();
				st=con.createStatement();
				String query="SELECT (amount-extra2) as amount FROM advance where invoiceId='"+InvoiceId+"'";
				rs=st.executeQuery(query);
				System.out.println("query..."+query);
				while(rs.next()){
					amount=rs.getString("amount");
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return amount;
		}
		
	
}
