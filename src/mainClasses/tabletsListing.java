package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.tablets;
public class tabletsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String tabletId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void settabletId(String tabletId)
{
this.tabletId = tabletId;
}
public String gettabletId(){
return(this.tabletId);
}
public List gettablets()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT tabletId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM tablets "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new tablets(rs.getString("tabletId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMtablets(String tabletId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT tabletId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM tablets where  tabletId = '"+tabletId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new tablets(rs.getString("tabletId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMtabletStock(String tabletId)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT tabletId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM tablets where  tabletId = '"+tabletId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMtabletName(String tabletId)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT tabletId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM tablets where  tabletId = '"+tabletId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("tabletName");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMtabletsSearch(String tabletName)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT tabletId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM tablets where  tabletName like '%"+tabletName+"%'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new tablets(rs.getString("tabletId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMtabletsSearchStock(String tabletName)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT tabletId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM tablets where  tabletName like '%"+tabletName+"%' and quantity >0 "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new tablets(rs.getString("tabletId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getinvociceno()
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
ResultSet rs=s.executeQuery("select * from no_genarator where table_name='invoice'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
list=PriSt+ticketm;
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}