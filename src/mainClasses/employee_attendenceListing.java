package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.*;
public class employee_attendenceListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String emp_atid;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setemp_atid(String emp_atid)
{
this.emp_atid = emp_atid;
}
public String getemp_atid(){
return(this.emp_atid);
}
public List getemployee_attendence()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_atid,staff_id,staff_intime,staff_outtime,late_hours,late_hourpermision,date,conform_status,admin_id,mager_id,extra5,extra6,extar7,extra8,extra9,extra10,extra11,extra12 FROM employee_attendence "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employee_attendence(rs.getString("emp_atid"),rs.getString("staff_id"),rs.getString("staff_intime"),rs.getString("staff_outtime"),rs.getString("late_hours"),rs.getString("late_hourpermision"),rs.getString("date"),rs.getString("conform_status"),rs.getString("admin_id"),rs.getString("mager_id"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extar7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

//include untime and out time of employee
public String getduplicates(String staff_id,String yestdays,String staff_intime,String staff_outtime,String mager_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT staff_id FROM employee_attendence  where staff_id='"+staff_id+"' and date='"+yestdays+ "' and staff_intime='"+staff_intime+"' and staff_outtime='"+staff_outtime+"' and mager_id='"+mager_id+"'"; 
//System.out.println("getduplicates"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 //list.add(new employee_attendence(rs.getString("emp_atid"),rs.getString("staff_id"),rs.getString("staff_intime"),rs.getString("staff_outtime"),rs.getString("late_hours"),rs.getString("late_hourpermision"),rs.getString("date"),rs.getString("conform_status"),rs.getString("admin_id"),rs.getString("mager_id"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extar7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12")));
list=rs.getString("staff_id");//System.out.println("list is ;"+list);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getEmployeeAttendceMonth(String staff_id,String month)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_atid,staff_id,staff_intime,staff_outtime,late_hours,late_hourpermision,date,conform_status,admin_id,mager_id,extra5,extra6,extar7,extra8,extra9,extra10,extra11,extra12 FROM employee_attendence where staff_id='"+staff_id+"' and date like '"+month+"%'  and conform_status='confirm' group by date"; 

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employee_attendence(rs.getString("emp_atid"),rs.getString("staff_id"),rs.getString("staff_intime"),rs.getString("staff_outtime"),rs.getString("late_hours"),rs.getString("late_hourpermision"),rs.getString("date"),rs.getString("conform_status"),rs.getString("admin_id"),rs.getString("mager_id"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extar7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
//exclude intime and out time
public String getduplicates(String staff_id,String yestdays,String mager_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT staff_id FROM employee_attendence  where staff_id='"+staff_id+"' and date='"+yestdays+ "' and mager_id='"+mager_id+"'"; 
//System.out.println("4#getduplicates"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 //list.add(new employee_attendence(rs.getString("emp_atid"),rs.getString("staff_id"),rs.getString("staff_intime"),rs.getString("staff_outtime"),rs.getString("late_hours"),rs.getString("late_hourpermision"),rs.getString("date"),rs.getString("conform_status"),rs.getString("admin_id"),rs.getString("mager_id"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extar7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12")));
list=rs.getString("staff_id");//System.out.println("list is ;"+list);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getInOut(String staff_id,String dateS)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_atid,staff_id,staff_intime,staff_outtime,late_hours,late_hourpermision,date,conform_status,admin_id,mager_id,extra5,extra6,extar7,extra8,extra9,extra10,extra11,extra12 FROM employee_attendence where staff_id='"+staff_id+"' AND date='"+dateS+"'";
//System.out.println("6(289)@selectquery=>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employee_attendence(rs.getString("emp_atid"),rs.getString("staff_id"),rs.getString("staff_intime"),rs.getString("staff_outtime"),rs.getString("late_hours"),rs.getString("late_hourpermision"),rs.getString("date"),rs.getString("conform_status"),rs.getString("admin_id"),rs.getString("mager_id"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extar7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getEmployeeMonthLeavese(String staff_id,String startdate,String enddate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_atid,staff_id,staff_intime,staff_outtime,late_hours,late_hourpermision,date,conform_status,admin_id,mager_id,extra5,extra6,extar7,extra8,extra9,extra10,extra11,extra12 FROM employee_attendence where staff_id='"+staff_id+"' and (date between '"+startdate+ "' and '"+ enddate+"') and conform_status='confirm'"; 
//System.out.println("selectquery: "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employee_attendence(rs.getString("emp_atid"),rs.getString("staff_id"),rs.getString("staff_intime"),rs.getString("staff_outtime"),rs.getString("late_hours"),rs.getString("late_hourpermision"),rs.getString("date"),rs.getString("conform_status"),rs.getString("admin_id"),rs.getString("mager_id"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extar7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

//check emp attendence have  enter dateor not
public List getStaffpermissionAttendence(String leave_date)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//System.out.println("inside the method");
// String selectquery="SELECT * FROM employee_attendence where date in(SELECT leave_date FROM leavepermission where  leave_date='"+leave_date+"')";
String selectquery="SELECT * FROM employee_attendence where date ="+leave_date;
System.out.println("Leave attendence edit: "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	
 list.add(new employee_attendence(rs.getString("emp_atid"),rs.getString("staff_id"),rs.getString("staff_intime"),rs.getString("staff_outtime"),rs.getString("late_hours"),rs.getString("late_hourpermision"),rs.getString("date"),rs.getString("conform_status"),rs.getString("admin_id"),rs.getString("mager_id"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extar7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12")));
 
}

s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String getEmployeeLPOrLA(String staff_id,String LpOrLA,String fromDate,String toDate)
{ 
	System.out.println("LpOrLA::::"+LpOrLA);
	System.out.println("fromDate::::"+fromDate);
	System.out.println("fromDate::::"+toDate);
String lpOrLACount ="";
try {
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(extra5) as total FROM employee_attendence  where staff_id='"+staff_id+"' and extra5='"+LpOrLA+ "' and date between'"+fromDate+"' and '"+toDate+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	lpOrLACount=rs.getString("total");
	
	System.out.println("lpOrLACount::::::::"+lpOrLACount);
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return lpOrLACount;
}

}