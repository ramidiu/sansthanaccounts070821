package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import beans.employeeSalary;
import dbase.sqlcon.ConnectionHelper;

public class employeeSalarySlipListing 
{

	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	
	public employeeSalarySlipListing()
	{
		try 
		{
		 	// Load the database driver
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}//constructor ended
	
	
	//select All the empployee salary slip no condition here 
	public List<employeeSalary> selectAllSalarySlip()
	{
		List<employeeSalary> l=new ArrayList<employeeSalary>();
		try
		{
			
			ResultSet rs=st.executeQuery("select * from employee_salary");
			while(rs.next())
			{
				employeeSalary empSal=new employeeSalary();
				empSal.setEmployee_salary_id(rs.getString(1));
				empSal.setEmployee_id(rs.getString(2));
				empSal.setMonth(rs.getString(3));
				empSal.setEwf_loan_amount(rs.getString(4));
				empSal.setEwf_interest_amount(rs.getString(5));
				empSal.setPersonal_loan_amount(rs.getString(6));
				empSal.setPersonal_interest_amount(rs.getString(7));
				empSal.setBasic_pay(rs.getString(8));
				empSal.setHra(rs.getString(9));
				empSal.setOtpay(rs.getString(10));
				empSal.setMedical_insurance(rs.getString(11));
				empSal.setSalary_pay(rs.getString(12));
				empSal.setExtra1(rs.getString(13));
				empSal.setExtra2(rs.getString(14));
				empSal.setExtra3(rs.getString(15));
				empSal.setExtra4(rs.getString(16));
				empSal.setExtra5(rs.getString(17));
				empSal.setExtra6(rs.getString(18));
				empSal.setExtra7(rs.getString(19));
				empSal.setExtra8(rs.getString(20));
				empSal.setExtra9(rs.getString(21));
				empSal.setExtra10(rs.getString(22));
				
				l.add(empSal);
			}
	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return l;
	}

//select All the salary slip based on month
public List<employeeSalary> selectAllSalarySlipBasedOnMonth(String month)
{
	List<employeeSalary> l=new ArrayList<employeeSalary>();
	try
	{
		String query="select * from employee_salary where month='"+month+"'";
		//System.out.println("------------------"+query);
		ResultSet rs=st.executeQuery(query);
		while(rs.next())
		{
			employeeSalary empSal=new employeeSalary();
			empSal.setEmployee_salary_id(rs.getString(1));
			empSal.setEmployee_id(rs.getString(2));
			empSal.setMonth(rs.getString(3));
			empSal.setEwf_loan_amount(rs.getString(4));
			empSal.setEwf_interest_amount(rs.getString(5));
			empSal.setPersonal_loan_amount(rs.getString(6));
			empSal.setPersonal_interest_amount(rs.getString(7));
			empSal.setBasic_pay(rs.getString(8));
			empSal.setHra(rs.getString(9));
			empSal.setOtpay(rs.getString(10));
			empSal.setMedical_insurance(rs.getString(11));
			empSal.setSalary_pay(rs.getString(12));
			empSal.setExtra1(rs.getString(13));
			empSal.setExtra2(rs.getString(14));
			empSal.setExtra3(rs.getString(15));
			empSal.setExtra4(rs.getString(16));
			empSal.setExtra5(rs.getString(17));
			empSal.setExtra6(rs.getString(18));
			empSal.setExtra7(rs.getString(19));
			empSal.setExtra8(rs.getString(20));
			empSal.setExtra9(rs.getString(21));
			empSal.setExtra10(rs.getString(22));
			empSal.setOther_deductions(rs.getString(23));
			empSal.setOther_allowances(rs.getString(24));
			empSal.setExtra11(rs.getString(25));
			empSal.setExtra12(rs.getString(26));
			empSal.setExtra13(rs.getString(27));
			empSal.setExtra14(rs.getString(28));
			empSal.setExtra15(rs.getString(29));
			
			l.add(empSal);
		}

	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return l;
}

//select All the salary slip based on month
public List<employeeSalary> selectAllSalarySlipBasedOnMonthAndId(String month,String employee_id)
{
	
	
	List<employeeSalary> l=new ArrayList<employeeSalary>();
	try
	{
		/*SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		Date d1=df.parse(month);
		SimpleDateFormat df1=new SimpleDateFormat("yyyy-MM");
		String dateAndMonth=df1.format(d1);*/
		
		
		
		String query="select * from employee_salary where month ='"+month+"'"+"and employee_id='"+employee_id+"'";	
		//System.out.println(query);
		ResultSet rs=st.executeQuery(query);
		while(rs.next())
		{
			employeeSalary empSal=new employeeSalary();
			empSal.setEmployee_salary_id(rs.getString(1));
			empSal.setEmployee_id(rs.getString(2));
			empSal.setMonth(rs.getString(3));
			empSal.setEwf_loan_amount(rs.getString(4));
			empSal.setEwf_interest_amount(rs.getString(5));
			empSal.setPersonal_loan_amount(rs.getString(6));
			empSal.setPersonal_interest_amount(rs.getString(7));
			empSal.setBasic_pay(rs.getString(8));
			empSal.setHra(rs.getString(9));
			empSal.setOtpay(rs.getString(10));
			empSal.setMedical_insurance(rs.getString(11));
			empSal.setSalary_pay(rs.getString(12));
			empSal.setExtra1(rs.getString(13));
			empSal.setExtra2(rs.getString(14));
			empSal.setExtra3(rs.getString(15));
			empSal.setExtra4(rs.getString(16));
			empSal.setExtra5(rs.getString(17));
			empSal.setExtra6(rs.getString(18));
			empSal.setExtra7(rs.getString(19));
			empSal.setExtra8(rs.getString(20));
			empSal.setExtra9(rs.getString(21));
			empSal.setExtra10(rs.getString(22));
			empSal.setOther_deductions(rs.getString("other_deductions"));
			empSal.setOther_allowances(rs.getString("other_allowances"));
			
			
			l.add(empSal);
		}

	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return l;
}

/**
 * @author Aaditya Raj
 * @param monthAndYear  pass month and year is specified format like : October@2015
 * @return list of employee salary data which is use to generate salary slip etc..
 */
public List<employeeSalary> getSalarySlip(String monthAndYear)
{
	
	
	List<employeeSalary> l=new ArrayList<employeeSalary>();
	try
	{
		String query="select * from employee_salary where month ='"+monthAndYear+"'";	
		//System.out.println(query);
		ResultSet rs=st.executeQuery(query);
		while(rs.next())
		{
			employeeSalary empSal=new employeeSalary();
			empSal.setEmployee_salary_id(rs.getString(1));
			empSal.setEmployee_id(rs.getString(2));
			empSal.setMonth(rs.getString(3));
			empSal.setEwf_loan_amount(rs.getString(4));
			empSal.setEwf_interest_amount(rs.getString(5));
			empSal.setPersonal_loan_amount(rs.getString(6));
			empSal.setPersonal_interest_amount(rs.getString(7));
			empSal.setBasic_pay(rs.getString(8));
			empSal.setHra(rs.getString(9));
			empSal.setOtpay(rs.getString(10));
			empSal.setMedical_insurance(rs.getString(11));
			empSal.setSalary_pay(rs.getString(12));
			empSal.setExtra1(rs.getString(13));
			empSal.setExtra2(rs.getString(14));
			empSal.setExtra3(rs.getString(15));
			empSal.setExtra4(rs.getString(16));
			empSal.setExtra5(rs.getString(17));
			empSal.setExtra6(rs.getString(18));
			empSal.setExtra7(rs.getString(19));
			empSal.setExtra8(rs.getString(20));
			empSal.setExtra9(rs.getString(21));
			empSal.setExtra10(rs.getString(22));
			empSal.setOther_deductions(rs.getString("other_deductions"));
			empSal.setOther_allowances(rs.getString("other_allowances"));
			
			l.add(empSal);
		}

	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return l;
}

//validate the employee id and month if it already inserted then return true to calling method........ 
public boolean validateEmpIdAndMonth(String employee_id,String month)
{
	boolean flag=false;
	
		List<employeeSalary> list=selectAllSalarySlip();
		Iterator<employeeSalary> itr=list.iterator();
		while(itr.hasNext())
		{
			employeeSalary esal=itr.next();
			if(esal.getEmployee_id().equals(employee_id) && esal.getMonth().equals(month))
			{
				flag=true;
			}
		}
		
	
	return flag;	
}

public String getEWFAmount(String employeeId)
{
String list = "0";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
String selectquery="SELECT sum(extra8) as totalamount  FROM employee_salary where employee_id='"+employeeId+"'";
//System.out.println(selectquery);
ResultSet rs = st.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("totalamount")!=null && !rs.getString("totalamount").equals("")){
 list=rs.getString("totalamount");
	}

}

}
catch(Exception e){
e.printStackTrace();
}
return list;
}
}//end of the class
