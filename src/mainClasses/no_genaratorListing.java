package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.no_genarator;

public class no_genaratorListing {
	private String error = "";

	public void seterror(String error) {
		this.error = error;
	}

	public String geterror() {
		return this.error;
	}

	private String id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;

	public void setid(String id) {
		this.id = id;
	}

	public String getid() {
		return (this.id);
	}

	public List getno_genarator() throws SQLException {
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT id,table_name,table_id,id_prefix FROM no_genarator ";
			rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new no_genarator(rs.getInt("id"), rs
						.getString("table_name"), rs.getInt("table_id"), rs
						.getString("id_prefix")));

			}
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		} finally {
			s.close();
			rs.close();
			c.close();
		}
		return list;
	}

	public List getMno_genarator(String id) throws SQLException {
		ResultSet rs = null;
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT id,table_name,table_id,id_prefix FROM no_genarator where  id = '"
					+ id + "'";
			rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new no_genarator(rs.getInt("id"), rs
						.getString("table_name"), rs.getInt("table_id"), rs
						.getString("id_prefix")));

			}
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		} finally {
			s.close();
			rs.close();
			c.close();
		}
		return list;
	}

	// ////////

	public String getid(String table) throws SQLException {
		ResultSet rs = null;
		String list = "";
		String id = "";
		
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT table_id,id_prefix FROM no_genarator where  table_name = '"
					+ table + "'";
			
			rs = s.executeQuery(selectquery);
			while (rs.next()) {
				String id_pre = rs.getString("id_prefix");
				id = Integer.toString(rs.getInt("table_id") + 1);
				list = id_pre + id;
				
				// System.out.println(""list);
			}
			String query="update no_genarator set table_id='"+id+"' where table_name='"+table+"'";
			s.executeUpdate(query);
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		} finally {
			s.close();
			rs.close();
			c.close();
		}
		return list;
	}
	
	
	public String getidWithoutUpdatingNoGen(String table) throws SQLException {
		ResultSet rs = null;
		String list = "";
		String id = "";
		
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT table_id,id_prefix FROM no_genarator where  table_name = '"+ table + "'";
			
			rs = s.executeQuery(selectquery);
			while (rs.next()) {
				String id_pre = rs.getString("id_prefix");
				id = Integer.toString(rs.getInt("table_id") + 1);
				list = id_pre + id;
				
				// System.out.println(""list);
			}
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		} finally {
			s.close();
			rs.close();
			c.close();
		}
		return list;
	}
	
	
	
	public String getId(String table) throws SQLException	{
		ResultSet rs = null;
		String jvnum = "";
		int num=0;
		int res=0;
		try	{
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery="SELECT table_id,id_prefix FROM no_genarator where  table_name = '"
					+ table + "'";
			System.out.println("selectquery===================="+selectquery);
			rs=s.executeQuery(selectquery);
			while(rs.next())	{
				System.out.println("result set not empty");
				String id_pref=rs.getString("id_prefix");
				 num=rs.getInt("table_id");
				 jvnum=id_pref+Integer.toString(num);
			}
			String updatequery="update no_genarator set table_id='"+(num+1)+"' where table_name='"+table+"'";
			System.out.println("update no gen==========="+updatequery);
			res=s.executeUpdate(updatequery);
			System.out.println("result===========>"+res);
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
	return jvnum;
	}
	
	
	
	public String getidBasedOnExpid(String expid) throws SQLException {
		ResultSet rs = null;
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String headofaccount = "";
			String devflag = "";
			String tablename = "";
			String selectquery = "SELECT m.head_account_id,m.extra1 FROM productexpenses p,majorhead m where  m.major_head_id=p.major_head_id and   p.expinv_id='"
					+ expid + "'";
			rs = s.executeQuery(selectquery);
			while (rs.next()) {
				headofaccount = rs.getString("head_account_id");
				devflag = rs.getString("extra1");
			}
			if (headofaccount.equals("1")) {
				tablename = "charityvoucher";
			}
			if (headofaccount.equals("3")) {
				tablename = "poojastorevoucher";
			}
			if (headofaccount.equals("4")) {
				if (devflag.equals("1")) {
					tablename = "sansthannondevvoucher";
				} else {
					tablename = "sansthandevvoucher";
				}
			}
			if (headofaccount.equals("5")) {
				tablename = "sainivasvoucher";
			}

			selectquery = "SELECT table_id,id_prefix FROM no_genarator where  table_name = '"
					+ tablename + "'";
			rs = s.executeQuery(selectquery);
			while (rs.next()) {
				String id_pre = rs.getString("id_prefix");
				String id = Integer.toString(rs.getInt("table_id") + 1);
				list = tablename + "@" + id_pre + id;
				// System.out.println(list);
			}
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		} finally {
			s.close();
			rs.close();
			c.close();
		}
		return list;
	}

	public String getidBasedOnExpidAndPaymentTpye(String paymentType,
			String expid) throws SQLException {
		ResultSet rs = null;
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String headofaccount = "";
			String devflag = "";
			String tablename = "";
			String selectquery = "SELECT m.head_account_id,m.extra1 FROM productexpenses p,majorhead m where  m.major_head_id=p.major_head_id and   p.expinv_id='"
					+ expid + "'";
			System.out.println(selectquery);
			rs = s.executeQuery(selectquery);
			while (rs.next()) {
				headofaccount = rs.getString("head_account_id");
				devflag = rs.getString("extra1");
			}

			if (paymentType.equals("cheque")) {
				if (headofaccount.equals("1")) {
					tablename = "charityvoucher";
				}
				if (headofaccount.equals("3")) {
					tablename = "poojastorevoucher";
				}
				if (headofaccount.equals("4")) {
					if (devflag.equals("1")) {
						tablename = "sansthannondevvoucher";
					} else {
						tablename = "sansthandevvoucher";
					}
				}
				if (headofaccount.equals("5")) {
					tablename = "sainivasvoucher";
				}
				if (headofaccount.equals("10")) {
					tablename = "ewfvoucher";
				}
			}

			else if (paymentType.equals("cashpaid")) {
				if (headofaccount.equals("1")) {
					tablename = "charityvoucherp";
				}
				if (headofaccount.equals("3")) {
					tablename = "poojastorevoucherp";
				}
				if (headofaccount.equals("4")) {
					if (devflag.equals("1")) {
						tablename = "sansthannondevvoucherp";
					} else {
						tablename = "sansthandevvoucherp";
					}
				}
				if (headofaccount.equals("5")) {
					tablename = "sainivasvoucherp";
				}
				if (headofaccount.equals("10")) {
					tablename = "ewfvoucherp";
				}
			}

			selectquery = "SELECT table_id,id_prefix FROM no_genarator where  table_name = '"
					+ tablename + "'";
			rs = s.executeQuery(selectquery);
			while (rs.next()) {
				String id_pre = rs.getString("id_prefix");
				String id = Integer.toString(rs.getInt("table_id") + 1);
				list = tablename + "@" + id_pre + id;
				// System.out.println(list);
			}
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		} finally {
			s.close();
			rs.close();
			c.close();
		}
		return list;
	}

	public List<no_genarator> getNoGeneratorList(String query) {

		ResultSet rs = null;
		List<no_genarator> list = new ArrayList<no_genarator>();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);
			while (rs.next()) {
				list.add(new no_genarator(rs.getInt("id"), rs
						.getString("table_name"), rs.getInt("table_id"), rs
						.getString("id_prefix")));

			}
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}

		return list;

	}
	
	public List<no_genarator> getNoGeneratorList1(String query) {
		ResultSet rs = null;
		List<no_genarator> list = new ArrayList<no_genarator>();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);
			while (rs.next()) {
				no_genarator no = new no_genarator();
				no.settable_id(rs.getInt("table_id"));
				no.settable_name(rs.getString("table_name"));
				list.add(no);
			}
		} catch (Exception e) {
		e.printStackTrace();
		}
		finally	{
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	public int updateNoGenerator(String query) {
		int i = 0;
		try {
			c = ConnectionHelper.getConnection();
			s = c.createStatement();

			i = s.executeUpdate(query);

		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return i;
	}
	public String getTableId(String tablename) {
		String selectQuery="select concat(id_prefix,table_id) from no_genarator where table_name='"+tablename+"'";
		System.out.println();
		System.out.println("getting table id from no ganartor table========"+selectQuery);
		String result="";
		try	{
			c = ConnectionHelper.getConnection();
			PreparedStatement ps=c.prepareStatement(selectQuery); 
			ResultSet rs=ps.executeQuery();
			System.out.println("result set for no_genarator   "+rs);
			if(rs.next())	{
				result=rs.getString(1);
			}
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		return result;
	}
	
	public String incrementTableId( String jvnum) {
		System.out.println();
		System.out.println("incrementing jvid-------------------");
		String[] jvid=jvnum.split("N");
		int result=Integer.parseInt(jvid[1])+1;
		return String.valueOf("JVN"+result);
	}
	
	public int updateNoGenerator(String tablename, String jvid) {
		System.out.println("updating jv id in no genarator------------");
		String updateQuery = "UPDATE no_genarator SET table_id=? WHERE table_name=?";
		int result=0;
		String[] emppid=jvid.split("N");
		int output=Integer.parseInt(emppid[1])+1;
		try	{
			c = ConnectionHelper.getConnection();
			PreparedStatement ps=c.prepareStatement(updateQuery);
			ps.setInt(1, output);
			ps.setString(2,tablename);
			System.out.println("update query for no genarator==========="+ps);
			result=ps.executeUpdate();	
		}
		catch(Exception se)	{
			se.printStackTrace();
		}
		return result;
	}

}