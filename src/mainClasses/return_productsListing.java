package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.return_products;
public class return_productsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String return_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setreturn_id(String return_id)
{
this.return_id = return_id;
}
public String getreturn_id(){
return(this.return_id);
}
public List getreturn_products()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT return_id,product_id,return_qty,return_date,mse_inv_id,vendor_billno,return_inv_id,entered_by,approved_by,approved_date,narration,vendor_id,head_account_id,status,extra1,extra2,extra3,extra4,extra5 FROM return_products "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new return_products(rs.getString("return_id"),rs.getString("product_id"),rs.getString("return_qty"),rs.getString("return_date"),rs.getString("mse_inv_id"),rs.getString("vendor_billno"),rs.getString("return_inv_id"),rs.getString("entered_by"),rs.getString("approved_by"),rs.getString("approved_date"),rs.getString("narration"),rs.getString("vendor_id"),rs.getString("head_account_id"),rs.getString("status"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMreturn_products(String return_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT return_id,product_id,return_qty,return_date,mse_inv_id,vendor_billno,return_inv_id,entered_by,approved_by,approved_date,narration,vendor_id,head_account_id,status,extra1,extra2,extra3,extra4,extra5 FROM return_products where  return_id = '"+return_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new return_products(rs.getString("return_id"),rs.getString("product_id"),rs.getString("return_qty"),rs.getString("return_date"),rs.getString("mse_inv_id"),rs.getString("vendor_billno"),rs.getString("return_inv_id"),rs.getString("entered_by"),rs.getString("approved_by"),rs.getString("approved_date"),rs.getString("narration"),rs.getString("vendor_id"),rs.getString("head_account_id"),rs.getString("status"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}