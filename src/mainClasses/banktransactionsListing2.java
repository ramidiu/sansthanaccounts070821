package mainClasses;


import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.banktransactions2;

public class banktransactionsListing2 {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	private String banktrn_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	 public void setbanktrn_id(String banktrn_id)
	{
	this.banktrn_id = banktrn_id;
	}
	public String getbanktrn_id(){
	return(this.banktrn_id);
	}
	
	public String getCountOfRecords(String banktrnid)
	{
	String count="";
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT count(*) as count FROM banktransactions2 where banktrn_id ='"+banktrnid+"'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	if(rs.next()){
		count=rs.getString("count");
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return count;
	}
	
	public String getMaxDateForRecord(String banktrnid)
	{
	String date="";
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT max(date) as date FROM banktransactions2 where banktrn_id ='"+banktrnid+"'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	if(rs.next()){
		date=rs.getString("date");
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return date;
	}
	
	public List getBankTransactionBetweenDatesFromBKT2(String bank_id,String fromdate,String todate,String reportType,boolean BRStype,String type)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
		String report="";
		String brsinerquery="";
		String typetx="";
		String fromdatequery="";
		if(reportType!=null && !reportType.equals("")){
			if(reportType.equals("cashpaid")){
				report=" and (type='cashpaid' || type='pettycash')";
			}else{
			report=" and type!='cashpaid'";}
		}else{
			report=" and type!='cashpaid'";
		}
		if(BRStype){
			brsinerquery=" and brs_date='' ";
		}
		if(!fromdate.equals("")){
			fromdatequery=" and date>='"+fromdate+"' ";
		}
		if(type.equals("add")){
			typetx=" and type='deposit' ";
		} else if(type.equals("sub")){
			typetx=" and type!='deposit' ";
		}
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	//String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions where bank_id = '"+bank_id+"' "+fromdatequery+" and  date<='"+todate+"'"+report+brsinerquery+typetx+" order by date" ;
	String selectquery="SELECT banktrn_id,bank_id,name,narration,date,amount,type,createdBy,editedBy,extra1,extra2,extra3,extra4,extra5, brs_date, extra6, extra7, extra8 FROM banktransactions2 where bank_id = '"+bank_id+"' "+fromdatequery+" and  date<='"+todate+"'"+report+brsinerquery+typetx+" and date NOT LIKE '2015-04-01%' order by date" ;
	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new banktransactions2(rs.getString("banktrn_id"),rs.getString("bank_id"),rs.getString("name"),rs.getString("narration"),rs.getString("date"),rs.getString("amount"),rs.getString("type"),rs.getString("createdBy"),rs.getString("editedBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("brs_date"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
}
