package mainClasses;


import dbase.sqlcon.ConnectionHelper;
import dbase.sqlcon.SainivasConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import beans.cashier_report;
public class cashier_reportListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String cashier_report_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setcashier_report_id(String cashier_report_id)
{
this.cashier_report_id = cashier_report_id;
}
public String getcashier_report_id(){
return(this.cashier_report_id);
}
public List getcashier_report()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getOpenedCountersList()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where logout_time='' && cash2=''"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getCounterBalanceNotDepositedList(String headOFAccountId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where cash6!='Deposited' and closing_balance>'00' and cash1>'2014-05-19' and login_id in(select emp_id from employees where headAccountId='"+headOFAccountId+"') "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getSainivasCounterBalanceNotDepositedList(String dateofcounter)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id, shift_code, counter_code, c.login_id, opening_balance, counter_balance, closing_balance, total_amount, logoin_time, logout_time, cash1, cash2, cash3, cash4, cash5, l.name as cash6, cash7, cash8, cash9, cash10 FROM cashier_report c,logins l where l.login_id=c.login_id and cash6!='Deposited' and cash1 like '"+dateofcounter+"%'  and (cash3='receptionist' or cash3='dsnradmin' ) group by c.login_id";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getSainivasCounterBalanceNotDeposited()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where cash6!='Deposited' and closing_balance>'00' and cash2>'2015-05-19' group by  cashier_report_id  order by cash2 asc ";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public double getSainivasCounterBalance(String FromDate)
{
	double list = 0.0;
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where cash1='"+FromDate+"'  group by  cashier_report_id  ";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=list+Double.parseDouble( rs.getString("total_amount"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getcashier_reportToday(String logindate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where cash1='"+logindate+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getcashier_report(String cashier_report_id) throws SQLException
{
ArrayList CounterList=new ArrayList();
ResultSet rs=null;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10,due_banktrnsid, edit_by, update_time, extra11, extra12 FROM cashier_report where  cashier_report_id = '"+cashier_report_id+"'"; 
System.out.println("selectquery....."+selectquery); 
rs = s.executeQuery(selectquery);
while(rs.next()){
	CounterList.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10"),rs.getString("due_banktrnsid"),rs.getString("edit_by"),rs.getString("update_time"),rs.getString("extra11"),rs.getString("extra12")));


}
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
finally{
	s.close();
	rs.close();
	c.close();	
}
return CounterList;
}
/**
 * @author Pruthvi
 * 
 *         tags
 * @throws SQLException 
 */
public HashMap<String, beans.cashier_report> getMcashier_report(String cashier_report_id) throws SQLException
{
HashMap<String, beans.cashier_report> CounterList=new HashMap<String, cashier_report>();
ResultSet rs=null;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10,due_banktrnsid, edit_by, update_time, extra11, extra12 FROM cashier_report where  cashier_report_id = '"+cashier_report_id+"'"; 
 rs = s.executeQuery(selectquery);
while(rs.next()){
	CounterList.put("counter", new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10"),rs.getString("due_banktrnsid"),rs.getString("edit_by"),rs.getString("update_time"),rs.getString("extra11"),rs.getString("extra12")));


}
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
finally{
	s.close();
	rs.close();
	c.close();	
}
return CounterList;
}
public HashMap<String, beans.cashier_report> getSainivascashier_report(String cashier_report_id) throws SQLException
{
HashMap<String, beans.cashier_report> CounterList=new HashMap<String, cashier_report>();
ResultSet rs=null;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cashier_report_id = '"+cashier_report_id+"'"; 
 rs = s.executeQuery(selectquery);
while(rs.next()){
	CounterList.put("counter", new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));


}
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
finally{
	s.close();
	rs.close();
	c.close();	
}
return CounterList;
}
public List getchashreportOnDate(String cash1,String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash1 = '"+cash1+"'  and login_id='"+login_id+"' order by logoin_time desc"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

/********************************/
public List getReportForToday(String cash1,String cash3,String logout_time)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash1 = '"+cash1+"'  and cash3='"+cash3+"' and logout_time='"+logout_time+"' "; 

//System.out.println("\ngetReportForToday == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/*****************************/
public List getReportsByLoginId(String cash1,String cash4)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash1 = '"+cash1+"'  and cash4='"+cash4+"'"; 

System.out.println("\ngetReportForToday == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/*****************************/
public List getchashreportOnOnlyDate(String cash1)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash1 = '"+cash1+"' order by logoin_time desc"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getchashreportOnOnlyDateLogin(String cash1,String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash1 = '"+cash1+"' and login_id='"+login_id+"' order by logoin_time desc"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getchashreportDateWise(String from,String to,String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  (cash1 between '"+from+"' and '"+to+"') and login_id='"+login_id+"' order by logoin_time desc"; 

//System.out.println("\ngetchashreportDateWise == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public String getchashreportDateWiseString(String from,String to,String login_id)
{
String list = "";String selectquery="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();

selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  (cash1 between '"+from+"' and '"+to+"') and login_id='"+login_id+"' order by logoin_time desc"; 

//System.out.println("\ngetchashreportDateWise == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 //list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));
//list=selectquery;
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return selectquery;
}


public List getchashreportOnOnlyDate_2(String cash1)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash1 = '"+cash1+"' order by logoin_time desc"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getchashreportOnOnlyDate_2(String cash1,String cash3)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash1 = '"+cash1+"' and cash3='"+cash3+"' order by logoin_time desc"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getLogoutList(String logout_time)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  logout_time = '"+logout_time+"' "; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getLoginReport(String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  login_id = '"+login_id+"' and  logout_time!=''"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getIndividualReport(String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  login_id = '"+login_id+"' and  logout_time='' order by cash1 desc"; 

System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
////////////////////////////////////////////////////
public String getCounterBalance(String login_id,String logout_time)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  login_id = '"+login_id+"' and  logout_time = '"+logout_time+"'"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("counter_balance");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getCounterBalance(String cashier_report_id)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cashier_report_id = '"+cashier_report_id+"' "; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
 list=rs.getString("cash6");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
///////////////////////////////////////////////////////

////////////////////////////////////////////////////
public List getCounter(String login_id,String logout_time)
{
	ArrayList list = new ArrayList();
try {
// Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  login_id = '"+login_id+"' and  logout_time = '"+logout_time+"'"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
///////////////////////////////////////////////////////
public List getUnApproveBank()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash9='unapporve'"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getApproveBank()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT cashier_report_id,shift_code,counter_code,login_id,opening_balance,counter_balance,closing_balance,total_amount,logoin_time,logout_time,cash1,cash2,cash3,cash4,cash5,cash6,cash7,cash8,cash9,cash10 FROM cashier_report where  cash9='approved'"; 

//System.out.println("\ngetchashreportOnDate == "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new cashier_report(rs.getString("cashier_report_id"),rs.getString("shift_code"),rs.getString("counter_code"),rs.getString("login_id"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("total_amount"),rs.getString("logoin_time"),rs.getString("logout_time"),rs.getString("cash1"),rs.getString("cash2"),rs.getString("cash3"),rs.getString("cash4"),rs.getString("cash5"),rs.getString("cash6"),rs.getString("cash7"),rs.getString("cash8"),rs.getString("cash9"),rs.getString("cash10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

}