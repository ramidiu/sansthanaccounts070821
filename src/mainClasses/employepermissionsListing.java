package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.employepermissions;
public class employepermissionsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String permission_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setpermission_id(String permission_id)
{
this.permission_id = permission_id;
}
public String getpermission_id(){
return(this.permission_id);
}
public List getemployepermissions()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT permission_id,emp_id,head_account_id,emp_permissions,description,extra1,extra2,extra3,extra4 FROM employepermissions "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employepermissions(rs.getString("permission_id"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("emp_permissions"),rs.getString("description"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMemployepermissions(String permission_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT permission_id,emp_id,head_account_id,emp_permissions,description,extra1,extra2,extra3,extra4 FROM employepermissions where  permission_id = '"+permission_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employepermissions(rs.getString("permission_id"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("emp_permissions"),rs.getString("description"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getEmployeDetratmentpermissions(String emp_id,String head_account_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT permission_id,emp_id,head_account_id,emp_permissions,description,extra1,extra2,extra3,extra4 FROM employepermissions where  emp_id = '"+emp_id+"' and head_account_id='"+head_account_id+"' "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employepermissions(rs.getString("permission_id"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("emp_permissions"),rs.getString("description"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

}