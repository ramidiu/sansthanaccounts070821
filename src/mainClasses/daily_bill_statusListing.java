package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.daily_bill_status;

public class daily_bill_statusListing 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String bill_id;
	
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	public String getBill_id() {
		return bill_id;
	}
	public void setBill_id(String bill_id) {
		this.bill_id = bill_id;
	}
	
	public List getDailyBillStatusBetweenDates(String fromdate,String todate)
	{
	ArrayList list = new ArrayList();
	try {
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT bill_id, createdBy, createdDate, fileName, extra1, extra2, extra3, extra4, extra5 FROM daily_bill_status where createdDate >='"+fromdate+"' and createdDate <='"+todate+"' order by createdDate";
	System.out.println("billList===>"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new daily_bill_status(rs.getString("bill_id"),rs.getString("createdBy"),rs.getString("createdDate"),rs.getString("fileName"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getDailyBillStatusOfParticularDate(String date)
	{
	ArrayList list = new ArrayList();
	try {
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT bill_id, createdBy, createdDate, fileName, extra1, extra2, extra3, extra4, extra5 FROM daily_bill_status where createdDate like '"+date+"%'";
	System.out.println("billList===>"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new daily_bill_status(rs.getString("bill_id"),rs.getString("createdBy"),rs.getString("createdDate"),rs.getString("fileName"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}

}
