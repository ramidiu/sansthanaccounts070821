package mainClasses;
import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.*;
public class stafftimingsListing 
{
	private String error="";
	public void seterror(String error)
	{
		this.error=error;
	}
	public String geterror()
	{
		return this.error;
	}

	private String staff_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	public void setstaff_id(String staff_id)
	{
		this.staff_id = staff_id;
	}
	public String getstaff_id(){
		return(this.staff_id);
	}

	//get All staff id from database.....
	public List getAllStaffId()
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  staff_id  FROM stafftimings"; 
			//System.out.println("getStaffYearList is "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				list.add(rs.getString(1));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e)
		{
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}//getAllStaffId()

	//to get all details of staff based upon its id....
	public stafftimings getStaffDetails(String staff_id)
	{

		stafftimings st=null;
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT staff_id,staff_name,designation,status,starttime,start_period,endtime,end_period,staff_type,staff_year,extra1,extra2,extra3,extra4 FROM stafftimings where  staff_id = '"+staff_id+"'"; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				st=new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return st;
	}

	//get all the staff_type only
	public List getAllDistinctStaffType()
	{
		ArrayList list = new ArrayList();

		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT distinct staff_type FROM stafftimings "; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				stafftimings st=new stafftimings();
				st.setstaff_type(rs.getString("staff_type"));
				list.add(st);

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}









	public List getstafftimings()
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT staff_id,staff_name,designation,status,starttime,start_period,endtime,end_period,staff_type,staff_year,extra1,extra2,extra3,extra4 FROM stafftimings "; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}


	public List getMstafftimings(String staff_id)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT staff_id,staff_name,designation,status,starttime,start_period,endtime,end_period,staff_type,staff_year,extra1,extra2,extra3,extra4, blood_group, mobile_no, emergency_no, address, DOB, staff_nid, extra5, extra6, next_week_shift, extra7, extra8, extra9, extra10 FROM stafftimings where  staff_id = '"+staff_id+"'"; 
			System.out.println("selectquery--->"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("blood_group"),rs.getString("mobile_no"),rs.getString("emergency_no"),rs.getString("address"),rs.getString("DOB"),rs.getString("staff_nid"),rs.getString("extra5"), rs.getString("extra6"), rs.getString("next_week_shift"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	//To get all records from stafftimings
	public List getAllStaffTimings()
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings";
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("blood_group"),rs.getString("mobile_no"),rs.getString("emergency_no"),rs.getString("address"),rs.getString("DOB"),rs.getString("staff_nid"),rs.getString("extra5"), rs.getString("extra6"), rs.getString("next_week_shift"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	//geting  distinct   year list
	public List getStaffYearList()
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  distinct year(staff_year)  FROM stafftimings  order by staff_year desc"; 
			//System.out.println("getStaffYearList is "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	//geting  distinct   year list
	public List getStaffYearList(String years)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  distinct year(staff_year)  FROM stafftimings  where  year(staff_year)='"+years+"'order by staff_year asc"; 
			//System.out.println("getStaffYearList is "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	public List getStaffList(String years)
	{
		ArrayList list = new ArrayList();
		try {

			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  distinct year(staff_year)  FROM stafftimings  where  year(staff_year)='"+years+"'order by staff_year asc"; 
			//System.out.println("1@getStaffYearList is "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				list.add(new stafftimings(rs.getString(1)));
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);  
		}
		return list;
	}
	public List getStaffList1(String years)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  distinct year(staff_year)  FROM stafftimings  where  year(staff_year)>'"+years+"'order by staff_year asc"; 
			//System.out.println("1@getStaffYearList is "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	public List getStaffcategoryList(String year)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			System.out.println(year +" getStaff$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			String selectquery="SELECT  distinct staff_type FROM stafftimings where year(staff_year)='"+year+"'order by staff_year asc"; 
			//System.out.println("2@getStaffcategoryList "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	public List getStaffcategoryList()
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  distinct staff_type FROM stafftimings order by staff_year asc"; 
			//System.out.println("getStaffcategoryList "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}


	public List getStaffDepartmentList1(String year)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  distinct staff_type FROM stafftimings where year(staff_year)='"+year+"'order by staff_year asc"; 
			System.out.println("getStaffcategoryList "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(rs.getString(1));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List getStaffDepartmentList(String year)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  distinct staff_type FROM stafftimings where year(staff_year)>='"+year+"'order by staff_year asc"; 
			//System.out.println("getStaffcategoryList "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(rs.getString(1));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List getStaffcategoryList(String staff_type,String year)
	{
		ArrayList list = new ArrayList();
		try {
			
			//System.out.println("in StaffimimgsListing get SFTw.getstaff().toString()"+year);
			//System.out.println("in StaffimimgsListing get SFTw.getstaff_type().toString()"+staff_type);
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			//String selectquery="SELECT   distinct staff_type FROM stafftimings where year(staff_year)>='"+year+"' and (staff_type='SHRI SAINIVAS MEGHA DHARMASALA' or staff_type='RESTAURANT') order by staff_year asc";
			String selectquery="SELECT   distinct staff_type FROM stafftimings where year(staff_year)>='"+year+"' and staff_type='"+staff_type+"' order by staff_year asc";
			//System.out.println("getStaffcategoryList "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	public List getEmpCategoryList(String year,String datess)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT  distinct s.staff_type FROM stafftimings s,employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_id=e.staff_id  and e.date='"+datess+"'order by s.staff_year asc"; 
			/*System.out.println("getEmpCategoryList "+selectquery);*/
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	public List getEmpCategoryList2(String year,String datess)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			/*String selectquery="SELECT  distinct s.staff_type FROM stafftimings s,employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_id=e.staff_id  and e.date='"+datess+"'order by s.staff_year asc";*/
			/*String selectquery="SELECT  distinct s.staff_type FROM stafftimings s,employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_id=e.staff_id  and e.date='"+datess+"' and  s.staff_type != 'PRIVATE SECURITY' order by s.staff_year asc " ;*/
			/*String selectquery="SELECT  distinct s.staff_type FROM stafftimings s,employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_id=e.staff_id  and e.date='"+datess+"' and  s.staff_type NOT IN ('PRIVATE SECURITY', 'SANSTHAN OUTSOURCE EMPLOYEES') order by s.staff_year asc " ;*/
			String selectquery="SELECT  distinct s.staff_type FROM stafftimings s,employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_id=e.staff_id  and e.date='"+datess+"' and  s.staff_type NOT IN ('PRIVATE SECURITY', 'SANSTHAN OUTSOURCE EMPLOYEES','EDUCATION','DOCTOR','VEDHAPANDIT','LADY SECURITY GAURDS','SANSTHAN SECURITY GUARDS','SERVICES (C-1102)','SANSTHAN -(C-1100)','SAIPRASAD (C-1101)','GENERAL ADMINISTRATION DEPARTMENT (C-1103)','ACCOUNTS DEPARTMENT (C-1104)','SECURITY AGENCY (C-1105)') order by s.staff_year asc " ;
			//System.out.println("getEmpCategoryList "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	public List getEmpCategoryList3(String year,String datess)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT    distinct s.staff_type FROM stafftimings s , employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_type IN ('PRIVATE SECURITY', 'SANSTHAN OUTSOURCE EMPLOYEES','EDUCATION','DOCTOR','VEDHAPANDIT','LADY SECURITY GAURDS','SANSTHAN SECURITY GUARDS','SERVICES (C-1102)','SANSTHAN -(C-1100)','SAIPRASAD (C-1101)','GENERAL ADMINISTRATION DEPARTMENT (C-1103)','ACCOUNTS DEPARTMENT (C-1104)','SECURITY AGENCY (C-1105)'"
					+ ""
					+ ") and s.staff_id=e.staff_id and e.date='"+datess+"'order by s.staff_year asc";
			/*String selectquery="SELECT    distinct s.staff_type FROM stafftimings s , employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_type='"+staff_type+"'and s.staff_id=e.staff_id and e.date='"+datess+"'order by s.staff_year asc";*/
			/*String selectquery="SELECT distinct s.staff_type FROM stafftimings s , employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_type='"+staff_type+"'and s.staff_id=e.staff_id and e.date='"+datess+"' and s.staff_type NOT IN ('PRIVATE SECURITY') order by s.staff_year asc"; */
			//System.out.println("getEmpCategoryList "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	public List getEmpCategoryList(String staff_type,String year,String datess)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT    distinct s.staff_type FROM stafftimings s , employee_attendence e where year(s.staff_year)='"+year+"' and s.staff_type='"+staff_type+"'and s.staff_id=e.staff_id and e.date='"+datess+"'order by s.staff_year asc"; 
			//System.out.println("getEmpCategoryList "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString(1)));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	////////////////////////////////////////
	public List getALLByStaffcategory(String staff_year )
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings   where  year(staff_year)='"+staff_year+"'"; 
			//System.out.println("getMmeetingmonth "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;

	}
	public List getALLByStaffcategory(String staff_type,String staff_year )
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings   where staff_type='"+staff_type+"' and year(staff_year)='"+staff_year+"'"; 
			//System.out.println("3@getMmeetingmonth "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("blood_group"),rs.getString("mobile_no"),rs.getString("emergency_no"),rs.getString("address"),rs.getString("DOB"),rs.getString("staff_nid"),rs.getString("extra5"),rs.getString("extra6"), rs.getString("next_week_shift"), rs.getString("extra7"), rs.getString("extra8"), rs.getString("extra9"), rs.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;

	}

	public List getALLByStaffCategory(String staff_type)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings   where staff_type='"+staff_type+"' "; 
			System.out.println("getMmeetingmonth "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;

	}

	public List getALLStaffForAttendence(String year)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings   where year(staff_year)='"+year+"' order by designation"; 
			///System.out.println("getMmeetingmonth "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;

	}
	public List getALLStaffForAttendencestatic(String year)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings   where year(staff_year)>='"+year+"' order by designation"; 
			//System.out.println("getMmeetingmonth "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;

	}
	public List getALLStaffForBasedonDepartment(String year,String department)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings   where year(staff_year)>='"+year+"'  and staff_type='"+department+"' order by designation"; 
			//System.out.println("getMmeetingmonth "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;

	}
	public List getALLStaffForAttendence(String year,String staff)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings   where year(staff_year)>='"+year+"' and staff_type='"+staff+"' order by designation"; 
			//System.out.println("salary sheet deept wise ::::"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;

	}
	public List getALLStaffForAttendence1(String year,String staff)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM stafftimings where year(staff_year)='"+year+"' and staff_type='"+staff+"' order by designation"; 
			System.out.println("salary sheet deept wise ::::"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;

	}
	// Load the database driver
	public String getstafName(String staff_id)
	{
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT staff_name FROM stafftimings where staff_id='"+staff_id+"'"; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list=rs.getString("staff_name");
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public String getStafCategoryName(String staff_id)
	{
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT staff_type FROM stafftimings where staff_id='"+staff_id+"'"; 
			ResultSet rs = s.executeQuery(selectquery);
			//System.out.println(selectquery);
			while(rs.next()){
				list=rs.getString("staff_type");
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	//select all the staff id which belongs to particular department
	public String listOfAllEmployeeIdBelongsToDepartment(String department)
	{
		StringBuffer ids=new StringBuffer("");
		ids=ids.append("&employee_id=");
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT staff_id FROM stafftimings   where staff_type='"+department+"'"; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				ids.append(" ").append(rs.getString(1));
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}

		return ids.toString();
	}

	public List getUnApprovedStaffs(String month_year)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();

			//String selectquery=SELECT * FROM stafftimings  where staff_id not in (SELECT emp_id FROM openingcl_bal  WHERE mon_yr='April 2015' group by emp_id)  group by staff_id 

			String selectquery="SELECT * FROM stafftimings  where staff_id not in (SELECT emp_id FROM openingcl_bal  WHERE mon_yr='"+month_year+"' group by emp_id)  group by staff_id"; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				list.add(new stafftimings(rs.getString("staff_id"),rs.getString("staff_name"),rs.getString("designation"),rs.getString("status"),rs.getString("starttime"),rs.getString("start_period"),rs.getString("endtime"),rs.getString("end_period"),rs.getString("staff_type"),rs.getString("staff_year"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

			}
			s.close();
			rs.close();
			c.close();
		}

		catch(Exception e)
		{
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

}