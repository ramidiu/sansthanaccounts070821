package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.headsgroup;
public class headsgroupListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String group_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setgroup_id(String group_id)
{
this.group_id = group_id;
}
public String getgroup_id(){
return(this.group_id);
}
public List getheadsgroup()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT group_id,group_name,narration,created_date,head_account_id,emp_id,extra1,extra2,extra3 FROM headsgroup ";
String selectquery="SELECT group_id,group_name,narration,created_date,head_account_id,emp_id,extra1,extra2,extra3 FROM headsgroup order by SUBSTRING(group_id ,1)*1 asc;";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new headsgroup(rs.getString("group_id"),rs.getString("group_name"),rs.getString("narration"),rs.getString("created_date"),rs.getString("head_account_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMheadsgroup(String group_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT group_id,group_name,narration,created_date,head_account_id,emp_id,extra1,extra2,extra3 FROM headsgroup where  group_id = '"+group_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new headsgroup(rs.getString("group_id"),rs.getString("group_name"),rs.getString("narration"),rs.getString("created_date"),rs.getString("head_account_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getGroupsBasedOnHead(String headid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT group_id,group_name,narration,created_date,head_account_id,emp_id,extra1,extra2,extra3 FROM headsgroup"; 
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new headsgroup(rs.getString("group_id"),rs.getString("group_name"),rs.getString("narration"),rs.getString("created_date"),rs.getString("head_account_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}