package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.superadmin;
public class superadminListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String admin_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setadmin_id(String admin_id)
{
this.admin_id = admin_id;
}
public String getadmin_id(){
return(this.admin_id);
}
public List getsuperadmin()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT admin_id,admin_name,user_name,password,extra1,extra2,extra3,extra4,extra5,image,start_date,end_date,last_login,ip_address,secret_ans,extra6,extra7,extra8,extra9 FROM superadmin "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new superadmin(rs.getString("admin_id"),rs.getString("admin_name"),rs.getString("user_name"),rs.getString("password"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("image"),rs.getString("start_date"),rs.getString("end_date"),rs.getString("last_login"),rs.getString("ip_address"),rs.getString("secret_ans"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMsuperadmin(String admin_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT admin_id,admin_name,user_name,password,extra1,extra2,extra3,extra4,extra5,image,start_date,end_date,last_login,ip_address,secret_ans,extra6,extra7,extra8,extra9 FROM superadmin where  admin_id = '"+admin_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new superadmin(rs.getString("admin_id"),rs.getString("admin_name"),rs.getString("user_name"),rs.getString("password"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("image"),rs.getString("start_date"),rs.getString("end_date"),rs.getString("last_login"),rs.getString("ip_address"),rs.getString("secret_ans"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getSuperadmin(String user_name,String password)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT admin_id,admin_name,user_name,password,extra1,extra2,extra3,extra4,extra5 FROM superadmin where user_name='"+user_name+"' and password='"+password+"' and extra5!='DEACTIVE'";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new superadmin(rs.getString("admin_id"),rs.getString("admin_name"),rs.getString("user_name"),rs.getString("password"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getSuperadmin(String user_name)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT admin_id,admin_name,user_name,password,extra1,extra2,extra3,extra4,extra5,image,start_date,end_date,last_login,ip_address,secret_ans,extra6,extra7,extra8,extra9 FROM superadmin where user_name='"+user_name+"' and extra5!='DEACTIVE'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new superadmin(rs.getString("admin_id"),rs.getString("admin_name"),rs.getString("user_name"),rs.getString("password"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("image"),rs.getString("start_date"),rs.getString("end_date"),rs.getString("last_login"),rs.getString("ip_address"),rs.getString("secret_ans"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMsuperadminWithHOA(String HOA)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT admin_id,admin_name,user_name,password,extra1,extra2,extra3,extra4,extra5 FROM superadmin where extra5='ACTIVE'  and  extra2 = '"+HOA+"'"; 
System.out.println("selectQuery................................"+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new superadmin(rs.getString("admin_id"),rs.getString("admin_name"),rs.getString("user_name"),rs.getString("password"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMsuperadminPassword(String admin_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT admin_id,admin_name,user_name,password,extra1,extra2,extra3,extra4,extra5 FROM superadmin where  admin_id = '"+admin_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("password");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getSuperadminname(String admin_id)
{
String name= "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT admin_name FROM superadmin where  admin_id = '"+admin_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("admin_name");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
public String getSuperadminnameRole(String admin_id)
{
String name= "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT extra4 FROM superadmin where  admin_id = '"+admin_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("extra4");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
public String getPassword(String uname)
{
String name= "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT password FROM superadmin where  user_name = '"+uname+"' and extra5!='DEACTIVE'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("password");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}

public boolean getEmergencyApproval(String superadminId)
{
	boolean flag=false;
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT extra6 FROM superadmin where admin_id='"+superadminId+"' ";
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
			if(rs.getString("extra6").equals("EmergencyApproval"))
			{
				System.out.println(rs.getString("extra6")+"  "+superadminId);
				flag=true;
			}
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	
	return flag;
}
public List getSuperAdminIdAndName(String HOA)
{
	ArrayList list = new ArrayList();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT admin_id,admin_name FROM superadmin where extra5!='DEACTIVE' and extra2 = '"+HOA+"'"; 
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			list.add(rs.getString("admin_id"));
			list.add(rs.getString("admin_name"));
		}
		s.close();
		rs.close();
		c.close();
	  }
		catch(Exception e){
			e.printStackTrace();
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
	  }
	return list;
}

public String getAdminIdBasedOnRole(String role)
{
String name= "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT admin_id FROM superadmin where  extra1 = '"+role+"' and extra5!='DEACTIVE'"; 
//System.out.println("==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 name=rs.getString("admin_id");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return name;
}
}