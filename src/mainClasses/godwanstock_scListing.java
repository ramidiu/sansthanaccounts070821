package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.godwanstock_sc;

import dbase.sqlcon.ConnectionHelper;

public class godwanstock_scListing {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String gsId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setgsId(String gsId)
{
this.gsId = gsId;
}
public String getgsId(){
return(this.gsId);
}

public String getCountOfRecords(String gsId)
{
String count="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as count FROM godwanstock_sc where gsId ='"+gsId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	count=rs.getString("count");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return count;
}
public String getCountOfRecordsBasedOnMseId(String mseId)
{
String count="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as count FROM godwanstock_sc where extra1 ='"+mseId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	count=rs.getString("count");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return count;
}
public String getMaxDateForRecord(String gsId)
{
String date="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT max(date) as date FROM godwanstock_sc where gsId ='"+gsId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	date=rs.getString("date");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return date;
}
public String getMaxDateForRecordBasedOnMseId(String mseId)
{
String date="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT max(date) as date FROM godwanstock_sc where extra1 ='"+mseId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	date=rs.getString("date");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return date;
}
public List getMSEbyVendorBasedOnDates(String department,String fromdate,String todate )
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gsId,productId,vendorId, description, quantity, purchaseRate, date, billNo, extra1, extra2, extra3, extra4, extra5, vat, vat1, profcharges, emp_id, department, checkedby, checkedtime, approval_status, extra6, extra7, extra8, extra9, extra10, po_approved_status, po_approved_by, po_approved_date, bill_status, bill_update_by, bill_update_time, extra11, extra12, extra13, extra14, extra15, sum(quantity*purchaseRate) as MRNo  FROM godwanstock_sc  where date>='"+fromdate+"' and date <='"+todate+"' and vendorId!='1001' and  department='"+department+"'  group by vendorId ";
//System.out.println("vendorlist===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new godwanstock_sc(rs.getString("gsId"),rs.getString("vendorId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("purchaseRate"),rs.getString("date"),rs.getString("MRNo"),rs.getString("billNo"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("vat"),rs.getString("vat1"),rs.getString("profcharges"),rs.getString("emp_id"),rs.getString("department"),rs.getString("checkedby"),rs.getString("checkedtime"),rs.getString("approval_status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("po_approved_status"),rs.getString("po_approved_by"),rs.getString("po_approved_date"),rs.getString("bill_status"),rs.getString("bill_update_by"),rs.getString("bill_update_time"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("majorhead_id"),rs.getString("minorrhead_id"),rs.getString("payment_status"),rs.getString("actualentry_date"),rs.getString("extra16"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getNoDatesForSelected(String todate,String fromdate)
{
String noofdaysselected="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT DATEDIFF( '"+todate+"', '"+fromdate+"') as noofdays"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
if(rs.next()){
	noofdaysselected=rs.getString("noofdays");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return noofdaysselected;
}
}
