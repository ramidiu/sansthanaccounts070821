package mainClasses;
import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.purchaseorder;
public class purchaseorderListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String po_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setpo_id(String po_id)
{
this.po_id = po_id;
}
public String getpo_id(){
return(this.po_id);
}
public List getpurchaseorder()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getpurchaseordersBasedOnVendor()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder group by poinv_id order by poinv_id DESC"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getpurchaseordersBasedOnVendorAndHoa(String hoaid,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(hoaid !=null && !hoaid.trim().equals("")){
		subQuery=" where head_account_id='"+hoaid+"' ";
	}
	if (fromdate != null && !fromdate.equals("") && todate != null && !todate.equals(""))	{
		if (!subQuery.trim().equals(""))	{
			subQuery += " and created_date >='"+fromdate+"' and created_date <='"+todate+"' "; 
		}
		else	{
			subQuery += " where created_date >='"+fromdate+"' and created_date <='"+todate+"' "; 
		}
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder"+subQuery+" group by poinv_id order by poinv_id DESC"; 
//System.out.println("get po orders---"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getpurchaseordersBasedIndentId(String indentinvoice_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder where indentinvoice_id='"+indentinvoice_id+"' group by poinv_id order by poinv_id DESC";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public int getpurchaseordersBasedIndentIdSize(String indentinvoice_id)
{
int list =1;
int no=0;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM purchaseorder where indentinvoice_id='"+indentinvoice_id+"' group by poinv_id order by poinv_id DESC"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMpurchaseorder(String po_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder where  po_id = '"+po_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMpurchaseorderBasedONPoINVID(String poinv_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder where  poinv_id = '"+poinv_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMpurchaseorderBasedTrackingno(String tracking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder where  extra1 = '"+tracking_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getPurchStockByInv(String poinv_id )
{
String list ="";
try {
 // Load the database driver
	String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();
	selectquery="SELECT quantity FROM purchaseorder where  poinv_id = '"+poinv_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");
}

}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getPurchSOrderDarw(String poinv_id )
{
String list ="";
try {
 // Load the database driver
	String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();
	selectquery="SELECT created_date FROM purchaseorder where  poinv_id = '"+poinv_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("created_date");
}

}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMpurchaseorderInvoiceIDs(String vendorid,String hoid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder where  vendor_id = '"+vendorid+"' and head_account_id='"+hoid+"' group by poinv_id order by poinv_id DESC"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMpurchaseorderInvoiceIDsNew(String vendorid,String hoid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT p.po_id,p.created_date,p.description,p.status,p.indentinvoice_id,p.requiremrnt,p.required_date,p.head_account_id,p.product_id,p.tablet_id,p.purchase_price,p.quantity,p.vendor_id,p.emp_id,p.poinv_id,p.extra1,p.extra2,p.extra3,p.extra4,p.extra5 FROM purchaseorder p,invoice i where  p.vendor_id = '"+vendorid+"' and p.head_account_id='"+hoid+"' and p.poinv_id not in(SELECT extra1 FROM invoice where  vendorid = '"+vendorid+"' and headAccountId='"+hoid+"' and extra1 != '') group by poinv_id order by poinv_id DESC"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getPurchaseOrderBAsedOnHOID(String hoid,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
	String subQuery="";
	if(hoid!=null && !hoid.equals("") && hoid.equals("5")){
		subQuery=" where head_account_id='"+hoid+"'";
	}
	if (fromdate != null && !fromdate.equals("") && todate != null && !todate.equals(""))	{
		if (!subQuery.trim().equals(""))	{
			subQuery += " and created_date >='"+fromdate+"' and created_date <='"+todate+"' "; 
		}
		else	{
			subQuery += " where created_date >='"+fromdate+"' and created_date <='"+todate+"' "; 
		}
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT po_id,created_date,description,status,indentinvoice_id,requiremrnt,required_date,head_account_id,product_id,tablet_id,purchase_price,quantity,vendor_id,emp_id,poinv_id,extra1,extra2,extra3,extra4,extra5 FROM purchaseorder "+subQuery+" group by poinv_id order by poinv_id DESC"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new purchaseorder(rs.getString("po_id"),rs.getString("created_date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requiremrnt"),rs.getString("required_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("purchase_price"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("poinv_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}