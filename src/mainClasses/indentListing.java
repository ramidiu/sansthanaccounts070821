package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.indent;
public class indentListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String indent_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setindent_id(String indent_id)
{
this.indent_id = indent_id;
}
public String getindent_id(){
return(this.indent_id);
}
public List getindent()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getindentGroupByIndentinvoice_id (String HOD,String id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String inquery="";
	String IDinquery="";
	if(!HOD.equals("")){
		inquery= " and head_account_id='"+HOD+"' ";
	}
	if(!id.equals("")){
		IDinquery= " and indentinvoice_id='"+id+"' ";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+inquery+" "+IDinquery+"  group by indentinvoice_id";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getindentGroupByIndentinvoice_idWithDate(String HOD,String id,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String inquery="";
	String IDinquery="";
	String dateQuery="";
	if(!HOD.equals("")){
		inquery= " and head_account_id='"+HOD+"' ";
	}
	if(!id.equals("")){
		IDinquery= " and indentinvoice_id='"+id+"' ";
	}
	if(!fromdate.equals("")){
		dateQuery= " and date >='"+fromdate+"' and date <='"+todate+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+inquery+" "+IDinquery+dateQuery+"  group by indentinvoice_id";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getindentGroupByIndentinvoice_id ()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver

c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending'   group by indentinvoice_id"; 
//System.out.println("indentlisting====>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getindentGroupByIndentinvoice_idBasedOnDeaprtment(String department)
{
ArrayList list = new ArrayList();
String subquery="";
if(!department.equals("")){
	subquery=" and head_account_id='"+department+"' ";
}
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+" group by indentinvoice_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getindentnoBasedonMaster(String id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indentinvoice_id  FROM invoice i,purchaseorder p where p.poinv_id=i.extra1 and  i.inv_id='"+id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("indentinvoice_id");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getindentnoBasedonUniqueId(String id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indentinvoice_id  FROM indent  where extra4='"+id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("indentinvoice_id");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getEntryDateUniqueId(String id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT date  FROM indent  where extra4='"+id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("date");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getIndentsBasedOnDeptAndStatus(String department,String status)
{
ArrayList list = new ArrayList();
String subquery="";
if(!department.equals("")){
	subquery=" and head_account_id='"+department+"' ";
}
String condition="";
if(status!=null && !status.equals("")){
	if(status.equals("pending")){
		condition=" and (status='reportRaised' || status='2 members approved' || status='1 members approved' )";
	}else if(status.equals("closed")){
		condition=" and (status='3indentApproved' || status='4indentApproved')";
	}
}
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+condition+" group by indentinvoice_id order by date desc";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getIndentsBasedOnDeptAndStatus1(String department,String status,String superadminId)
{
System.out.println("depst----"+department);
	System.out.println("status....."+status);
ArrayList list = new ArrayList();
String subquery="";
String subquery1="";
String subquery2="";
if(!department.equals(""))
{
	if(department.equals("1"))
	{
		/*subquery1="and head_account_id='"+department+"' and tablet_id !='Medicine'";*/
		subquery1="and head_account_id='"+department+"' and tablet_id not in ('Medicine','Education','Library')";
	}
	else if(department.equals("3"))
	{
		/*subquery2="and head_account_id='"+department+"' OR tablet_id = 'Medicine'";*/
		subquery2="and (head_account_id='"+department+"' OR tablet_id in ('Medicine','Education','Library'))";
	}
	else{
	subquery=" and head_account_id='"+department+"' ";
	}
}


String condition="";
if(status!=null && !status.equals("")){
	if(status.equals("pending") ){
		/*condition=" and (status='reportRaised' || status='2 members approved' || status='1 members approved' )";*/
		condition=" and (status='reportRaised' || status='2 members approved' || status='1 members approved' || status='0 members approved' )";
	}else if(status.equals("partially")){
		condition=" and (status='2 members approved' || status='1 members approved')";
	}else if(status.equals("closed")){
		condition=" and (status='3indentApproved' || status='4indentApproved' || status='EmergencyApproved')";
	}
	else if(status.equals("EmergencyApproval"))
	{
		//System.out.println("1");
		condition=" and status='reportRaised' and requirement='EmergencyApproval'";
	}
}
try {
	// Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="";

if(status.equals("closed") || status.equals("EmergencyApproval")){
	//System.out.println("2");
	selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+subquery1+subquery2+condition+" group by indentinvoice_id order by date desc";
}else if(status.equals("partially")){
	//System.out.println("3");
	selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+subquery1+subquery2+condition+" group by indentinvoice_id order by date desc";
}else if(status.equals("pending")){
	//System.out.println("4");
	selectquery="SELECT i.indent_id,i.date,i.description,i.status,i.indentinvoice_id,i.requirement,i.require_date,i.head_account_id,i.product_id,i.tablet_id,i.quantity,i.vendor_id,i.emp_id,i.extra1,i.extra2,i.extra3,i.extra4,i.extra5 FROM indent i,indentapprovals ia where status!='reportPending' and ia.extra3 not in ('FS','CM','GS') "+subquery+subquery1+subquery2+condition+" group by indentinvoice_id order by date desc";
}
else if(status.equals("disapproved")){
	//System.out.println("4");
	selectquery="SELECT i.indent_id,i.date,i.description,i.status,i.indentinvoice_id,i.requirement,i.require_date,i.head_account_id,i.product_id,i.tablet_id,i.quantity,i.vendor_id,i.emp_id,i.extra1,i.extra2,i.extra3,i.extra4,i.extra5 FROM indent i,indentapprovals ia where i.status!='reportPending' and ia.admin_status = 'disapproved' and i.indentinvoice_id = ia.indentinvoice_id "+subquery+subquery1+subquery2+condition+" group by i.indentinvoice_id order by i.date desc";
}
System.out.println("indent approcals query-----"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next())
{
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
e.printStackTrace();
}
return list;
}

/*public List getIndentsBasedOnDeptAndStatus2(String department,String status,String superadminId)
{
	System.out.println("status....."+status);
ArrayList list = new ArrayList();
String subquery="";
String subquery1="";
String subquery2="";
if(!department.equals(""))
{
	if(department.equals("1"))
	{
		subquery1="and head_account_id='"+department+"' and tablet_id !='Medicine'";
		subquery1="and head_account_id='"+department+"' and tablet_id not in ('Medicine','Education','Library')";
	}
	else if(department.equals("3"))
	{
		subquery2="and head_account_id='"+department+"' OR tablet_id = 'Medicine'";
		subquery2="and (head_account_id='"+department+"' OR tablet_id in ('Medicine','Education','Library'))";
	}
	else{
	subquery=" and head_account_id='"+department+"' ";
	}
}


String condition="";
if(status!=null && !status.equals("")){
	if(status.equals("pending") ){
		condition=" and (status='reportRaised' || status='2 members approved' || status='1 members approved' )";
		condition=" and (status='reportRaised' || status='2 members approved' || status='1 members approved' || status='0 members approved' )";
	}else if(status.equals("partially")){
		condition=" and (status='2 members approved' || status='1 members approved')";
	}else if(status.equals("closed")){
		condition=" and (status='3indentApproved' || status='4indentApproved' || status='EmergencyApproved')";
	}
	else if(status.equals("EmergencyApproval"))
	{
		//System.out.println("1");
		condition=" and status='reportRaised' and requirement='EmergencyApproval'";
	}
}
try {
	// Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="";

if(status.equals("closed") || status.equals("EmergencyApproval")){
	//System.out.println("2");
	selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+subquery1+subquery2+condition+" group by indentinvoice_id order by date desc";
}else if(status.equals("partially")){
	//System.out.println("3");
	selectquery="SELECT i.indent_id,i.date,i.description,i.status,i.indentinvoice_id,i.requirement,i.require_date,i.head_account_id,i.product_id,i.tablet_id,i.quantity,i.vendor_id,i.emp_id,i.extra1,i.extra2,i.extra3,i.extra4,i.extra5 FROM indent i,indentapprovals ia where i.status!='reportPending' and ia.admin_status!= 'disapproved' and i.indentinvoice_id = ia.indentinvoice_id "+subquery+subquery1+subquery2+condition+" group by i.indentinvoice_id order by i.date desc";
}else if(status.equals("pending")){
	//System.out.println("4");
	selectquery="SELECT i.indent_id,i.date,i.description,i.status,i.indentinvoice_id,i.requirement,i.require_date,i.head_account_id,i.product_id,i.tablet_id,i.quantity,i.vendor_id,i.emp_id,i.extra1,i.extra2,i.extra3,i.extra4,i.extra5 FROM indent i,indentapprovals ia where i.status!='reportPending' and ia.admin_status!= 'disapproved' and i.indentinvoice_id = ia.indentinvoice_id "+subquery+subquery1+subquery2+condition+" group by i.indentinvoice_id order by i.date desc";
}
else if(status.equals("disapproved")){
	//System.out.println("4");
	selectquery="SELECT i.indent_id,i.date,i.description,i.status,i.indentinvoice_id,i.requirement,i.require_date,i.head_account_id,i.product_id,i.tablet_id,i.quantity,i.vendor_id,i.emp_id,i.extra1,i.extra2,i.extra3,i.extra4,i.extra5 FROM indent i,indentapprovals ia where i.status!='reportPending' and ia.admin_status = 'disapproved' and i.indentinvoice_id = ia.indentinvoice_id "+subquery+subquery1+subquery2+condition+" group by i.indentinvoice_id order by i.date desc";
}
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next())
{
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
e.printStackTrace();
}
return list;
}*/

public List getIndentsBasedOnDeptAndStatus1AndBtwDates(String department,String status,String superadminId,String fdate,String tdate)
{
	
ArrayList list = new ArrayList();
String subquery="";
String subquery1="";
String subquery2="";
String subquery3="";

if(!department.equals(""))
{
	if(department.equals("1"))
	{
		/*subquery1="and head_account_id='"+department+"' and tablet_id !='Medicine'";*/
		subquery1="and head_account_id='"+department+"' and tablet_id not in ('Medicine','Education','Library')";
	}
	else if(department.equals("3"))
	{
		/*subquery2="and head_account_id='"+department+"' OR tablet_id = 'Medicine'";*/
		subquery2="and (head_account_id='"+department+"' OR tablet_id in ('Medicine','Education','Library'))";
	}
	else{
	subquery=" and head_account_id='"+department+"' ";
	}
}

if(fdate != null && !fdate.equals("") && tdate != null && !tdate.equals(""))
{
	subquery3 = " and date >= '"+fdate+"' and date <= '"+tdate+"'";
}

String condition="";
if(status!=null && !status.equals("")){
	if(status.equals("pending") ){
		/*condition=" and (status='reportRaised' || status='2 members approved' || status='1 members approved' )";*/
		condition=" and (status='reportRaised' || status='2 members approved' || status='1 members approved' || status='0 members approved' )";
	}else if(status.equals("partially")){
		condition=" and (status='2 members approved' || status='1 members approved')";
	}else if(status.equals("closed")){
		condition=" and (status='3indentApproved' || status='4indentApproved' || status='EmergencyApproved')";
	}
	else if(status.equals("EmergencyApproval"))
	{
		//System.out.println("1");
		condition=" and status='reportRaised' and requirement='EmergencyApproval'";
	}
}
try {
	// Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="";

if(status.equals("closed") || status.equals("EmergencyApproval")){
	//System.out.println("2");
	selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+subquery1+subquery2+condition+subquery3+" group by indentinvoice_id";
}else if(status.equals("partially")){
	//System.out.println("3");
	selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+subquery1+subquery2+condition+subquery3+" group by indentinvoice_id";
}else if(status.equals("pending")){
	//System.out.println("4");
	selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+subquery1+subquery2+condition+subquery3+" group by indentinvoice_id";
}
else if(status.equals("disapproved")){
	//System.out.println("4");
	selectquery="SELECT i.indent_id,i.date,i.description,i.status,i.indentinvoice_id,i.requirement,i.require_date,i.head_account_id,i.product_id,i.tablet_id,i.quantity,i.vendor_id,i.emp_id,i.extra1,i.extra2,i.extra3,i.extra4,i.extra5 FROM indent i,indentapprovals ia where i.status!='reportPending' and ia.admin_status = 'disapproved' and i.date >= '"+fdate+"' and i.date <= '"+tdate+"' and i.indentinvoice_id = ia.indentinvoice_id "+subquery+subquery1+subquery2+condition+" group by i.indentinvoice_id";
}
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next())
{
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
e.printStackTrace();
}
return list;
}

public List getindentsbasedOnHOA (String hoa_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String headCon="";
	if(!hoa_id.equals("0")&& !hoa_id.equals("3")){
		headCon=" where head_account_id='"+hoa_id+"' and status!='reportPending'";
	} else{
		headCon=" where  status!='reportPending'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent "+headCon+"  group by indentinvoice_id order by date desc";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPendingIndents()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status='reportRaised' group by indentinvoice_id "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getUnApproveIndents()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status='reportPending' group by indentinvoice_id ";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMindent(String indent_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where  indent_id = '"+indent_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMindentsByIndentinvoice_id(String indentinvoice_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where  indentinvoice_id = '"+indentinvoice_id+"'"; 
//System.out.println("getindentdetls====="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMindentsByIndentinvoice_ids(String indentinvoice_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT i.indent_id,i.date,i.description,i.status,i.indentinvoice_id,i.requirement,i.require_date,i.head_account_id,i.product_id,i.tablet_id,i.quantity,i.vendor_id,i.emp_id,i.extra1,i.extra2,i.extra3,i.extra4,i.extra5,pe.expinv_id,pe.extra6 FROM indent i,productexpenses pe where  i.extra4=pe.extra6 and pe.expinv_id = '"+indentinvoice_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMindentsByTrackingId(String trackingid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where  extra4 = '"+trackingid+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMindentsGroupByVendor(String indentinvoice_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where  indentinvoice_id = '"+indentinvoice_id+"' group by vendor_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMindentProductsDetails(String indentinvoice_id,String vendorid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where  indentinvoice_id = '"+indentinvoice_id+"' and vendor_id='"+vendorid+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMindentBasedOnVendor(String vendor_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where  vendor_id = '"+vendor_id+"' group by indentinvoice_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPendingIndentsBasedOnHod(String head_id)
{
ArrayList list = new ArrayList();
try {
	String headCon="";
	if(!head_id.equals("0")){
		headCon="and head_account_id='"+head_id+"'";
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status='reportRaised'   group by indentinvoice_id "; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getIndentsBasedOnDeptAndStatusAndDate(String department,String status,String date,String datetype)
{
ArrayList list = new ArrayList();
String subquery="";
String datequery="";
String condition="";
if(!department.equals("")){
	subquery=" and head_account_id='"+department+"' ";
}
if(!date.equals("") && !datetype.equals("") && datetype.equals("Before")){
	datequery=" and date<'"+date+"' ";
}
else if(!date.equals("") && !datetype.equals("Uptonow")){
	datequery=" and date<='"+date+"' ";
}
if(status!=null && !status.equals("")){
	if(status.equals("pending")){
		condition=" and (status='reportRaised' || status='2 members approved' || status='1 members approved' )";
	}else if(status.equals("closed")){
		condition=" and (status='3indentApproved' || status='4indentApproved')";
	}
	
}
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT indent_id,date,description,status,indentinvoice_id,requirement,require_date,head_account_id,product_id,tablet_id,quantity,vendor_id,emp_id,extra1,extra2,extra3,extra4,extra5 FROM indent where status!='reportPending' "+subquery+datequery+condition+" group by indentinvoice_id";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}