package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.gatepass_entries;
public class gatepass_entriesListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String gp_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setgp_id(String gp_id)
{
this.gp_id = gp_id;
}
public String getgp_id(){
return(this.gp_id);
}
public List getgatepass_entries()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gp_id,entry_date,productId,qty,head_account_id,gatepass_number,entered_emp_id,status,narration,remarks,gatepass_category,updated_status,updated_date,updated_emp_id,return_qty,gatepass_takenby,extra1,extra2,extra3,extra4,extra5 FROM gatepass_entries "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new gatepass_entries(rs.getString("gp_id"),rs.getString("entry_date"),rs.getString("productId"),rs.getString("qty"),rs.getString("head_account_id"),rs.getString("gatepass_number"),rs.getString("entered_emp_id"),rs.getString("status"),rs.getString("narration"),rs.getString("remarks"),rs.getString("gatepass_category"),rs.getString("updated_status"),rs.getString("updated_date"),rs.getString("updated_emp_id"),rs.getString("return_qty"),rs.getString("gatepass_takenby"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getGatepassesEntryReport(String hoid,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gp_id,entry_date,productId,qty,head_account_id,gatepass_number,entered_emp_id,status,narration,remarks,gatepass_category,updated_status,updated_date,updated_emp_id,return_qty,gatepass_takenby,extra1,extra2,extra3,extra4,extra5 FROM gatepass_entries where entry_date>='"+fromdate+"' and entry_date <='"+todate+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new gatepass_entries(rs.getString("gp_id"),rs.getString("entry_date"),rs.getString("productId"),rs.getString("qty"),rs.getString("head_account_id"),rs.getString("gatepass_number"),rs.getString("entered_emp_id"),rs.getString("status"),rs.getString("narration"),rs.getString("remarks"),rs.getString("gatepass_category"),rs.getString("updated_status"),rs.getString("updated_date"),rs.getString("updated_emp_id"),rs.getString("return_qty"),rs.getString("gatepass_takenby"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMgatepass_entries(String gp_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT gp_id,entry_date,productId,qty,head_account_id,gatepass_number,entered_emp_id,status,narration,remarks,gatepass_category,updated_status,updated_date,updated_emp_id,return_qty,gatepass_takenby,extra1,extra2,extra3,extra4,extra5 FROM gatepass_entries where  gp_id = '"+gp_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new gatepass_entries(rs.getString("gp_id"),rs.getString("entry_date"),rs.getString("productId"),rs.getString("qty"),rs.getString("head_account_id"),rs.getString("gatepass_number"),rs.getString("entered_emp_id"),rs.getString("status"),rs.getString("narration"),rs.getString("remarks"),rs.getString("gatepass_category"),rs.getString("updated_status"),rs.getString("updated_date"),rs.getString("updated_emp_id"),rs.getString("return_qty"),rs.getString("gatepass_takenby"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}