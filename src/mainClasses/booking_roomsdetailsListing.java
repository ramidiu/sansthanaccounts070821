package mainClasses;

import dbase.sqlcon.SainivasConnectionHelper;
import dbase.sqlcon.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.booking_roomsdetails;
import beans.bookingrooms;
public class booking_roomsdetailsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String roomdetails_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setroomdetails_id(String roomdetails_id)
{
this.roomdetails_id = roomdetails_id;
}
public String getroomdetails_id(){
return(this.roomdetails_id);
}
public List getbooking_roomsdetails()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT roomdetails_id,booking_id,roomtype_id,noof_rooms,total_cost,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13 FROM booking_roomsdetails "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getPK(String booking_id,String roomtype_id)
{
	ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT roomdetails_id,booking_id,roomtype_id,noof_rooms,total_cost,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13 FROM booking_roomsdetails where booking_id='"+booking_id+"' and roomtype_id='"+roomtype_id+"' "; 
//System.out.println("\nGetPK ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getRelbooking_roomsdetails(String booking_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT roomdetails_id,booking_id,roomtype_id,noof_rooms,total_cost,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13 FROM booking_roomsdetails where  booking_id = '"+booking_id+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public List getOtherBookingUpDown(String booking_id,String roomtype_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT roomdetails_id,booking_id,roomtype_id,noof_rooms,total_cost,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13 FROM booking_roomsdetails where booking_id = '"+booking_id+"' and roomtype_id='"+roomtype_id+"'"; 
//System.out.println("\n getOtherBookingUpDown*** ="+selectquery);

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getMbooking_roomsdetails(String roomdetails_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT roomdetails_id,booking_id,roomtype_id,noof_rooms,total_cost,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13 FROM booking_roomsdetails where  roomdetails_id = '"+roomdetails_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List Report_by_RoomType(String room_type,String from,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	//String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.current_date between '"+from+"' and '"+to+"' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id "; //this query calculate even room upgrade details
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and br.total_cost!='00' and b.current_date between '"+from+"' and '"+to+"' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id ";
	
	System.out.println("\nReport_by_RoomType ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
public List Report_by_RoomTypelike(String room_type,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	//String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.current_date between '"+from+"' and '"+to+"' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id "; //this query calculate even room upgrade details
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and br.total_cost!='00' and b.checkindate like '%"+to+"%' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') and b.checkin_flag='yes' and b.checkout_flag='yes' GROUP BY b.booking_id";
	//System.out.println("\nReport_by_RoomType ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
public List Report_by_RoomTypelikegross(String room_type,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	//String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.current_date between '"+from+"' and '"+to+"' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id "; //this query calculate even room upgrade details
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and br.total_cost!='00' and b.checkoutdate like '%"+to+"%' and b.paymentstauts='success'  GROUP BY b.booking_id";
/*System.out.println("\nReport_by_RoomType ="+selectquery);*/
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}

public List Report_by_RoomType_checkindate(String room_type,String from,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.checkindate between '"+from+"' and '"+to+"' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay')"; 
	//System.out.println("\n@@@@@@@@@@@@@Report_by_RoomType ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}

public List Report_by_RoomType_checkindate_yes(String room_type,String from,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.checkindate between '"+from+"' and '"+to+"' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') and b.checkin_flag='yes' and b.checkout_flag='yes' GROUP BY b.booking_id"; 
	//System.out.println("\n@@@@@@@@@@@@@Report_by_RoomType ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
public List Report_by_RoomType_checkindate_yeslike(String room_type,String date)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.checkindate like '"+date+"%' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') and b.checkin_flag='yes' and b.checkout_flag='yes' GROUP BY b.booking_id"; 
	//System.out.println("\n@@@@@@@@@@@@@Report_by_RoomType ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
public List Report_by_RoomTypenet_checkindate_yeslike(String room_type,String date)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,COUNT(DISTINCT br.booking_id) as  noof_rooms,sum(total_amount) as total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.checkindate like '"+date+"%' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') and b.checkin_flag='yes' and b.checkout_flag='yes' GROUP BY br.roomtype_id"; 
	//System.out.println("\n@@@@@@@@@@@@@Report_by_RoomType ="+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
public List Report_by_RoomTypegross_checkindate_yeslike(String room_type,String date)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,COUNT(DISTINCT br.booking_id) as  noof_rooms,sum(total_amount) as total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.checkoutdate like '"+date+"%' and b.paymentstauts='success' GROUP BY br.roomtype_id"; 
/*System.out.println("\n@@@@@@@@@@@@@Report_by_RoomType ="+selectquery);*/
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
public List getGrossBookings(String room_type,String from,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
		// Load the database driver
		c = SainivasConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM" +
				" booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.current_date between '"+from+"' and '"+to+"' and b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id "; 
		
		String q2="select b.booking_id from bookingrooms b,shrisainivas.booking_roomsdetails br where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.current_date between '"+from+"' and '"+to+"' and b.checkindate NOT BETWEEN '"+from+"' and '"+to+"'";
		//String q2="select b.booking_id from bookingrooms b where b.current_date between '"+from+"' and '"+to+"' and b.checkindate NOT BETWEEN '"+from+"' and '"+to+"'";
		/*System.out.println("getGrossBookings ="+selectquery);*/
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
			list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e)
		{
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
	return list;
}

public List getAdvanceBookingsAmount(String room_type,String from,String to)
{
	ArrayList list = new ArrayList();
	try {
		// Load the database driver
		c = SainivasConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.current_date between '"+from+"' and '"+to+"' and b.checkindate NOT BETWEEN '"+from+"' and '"+to+"'";
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next())
		{
			list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e)
		{
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
	return list;
}

public String getAdvanceAmount(String roomtype,String date1)
{
	String sumOfAdv="";
	try {
			// Load the database driver
			c = SainivasConnectionHelper.getConnection();
			s=c.createStatement();
			
			String q="select sum(b.total_amount) from bookingrooms b,booking_roomsdetails br where b.booking_id=br.booking_id AND ( (b.checkindate>'"+date1+"'AND b.checkin_flag='no') OR (checkoutdate>'"+date1+"' AND b.checkout_flag='no' ) ) AND br.roomtype_id='"+roomtype+"'";
			System.out.println(q);
			ResultSet rs = s.executeQuery(q);
			while(rs.next())
			{
				
				sumOfAdv=rs.getString("sum(b.total_amount)");
			
			}
			
				s.close();
				rs.close();
				c.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	return sumOfAdv;
}






public List getGrossBookingslike(String room_type,String date)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.current_date like '"+date+"-%' and  b.paymentstauts='success' and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id "; 
	//System.out.println("\n@@@@@@@@@@@@@getGrossBookings ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}


public List Report_by_RoomType_cancellations(String room_type,String from,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.issue_date between '"+from+"' and '"+to+"'  and b.paymentstauts='success'  and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id"; 
//System.out.println("\n##############Report_by_RoomType_cancella ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next())
	{
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}

public List Report_by_RoomType_cancellationslike(String room_type,String date)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.issue_date like '"+date+"-%'   and b.paymentstauts='success'  and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id"; 
	//System.out.println("\n##############Report_by_RoomType_cancella ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}


public List getGrossCancellations(String room_type,String from,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.issue_date between '"+from+"' and '"+to+"' and b.extra4='success' and b.paymentstauts='success'  and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id"; 
	//System.out.println("\n##############Report_by_RoomType_cancella ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}
public List getGrossCancellationslike(String room_type,String date)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.issue_date like '"+date+"-%' and b.extra4='success' and b.paymentstauts='success'  and (b.extra2!='dummy' and b.extra2!='guestpay' and b.extra2!='previlized') GROUP BY b.booking_id"; 
	//System.out.println("\n##############Report_by_RoomType_cancella ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}



public List Report_by_RoomType_checkoutdate(String room_type,String from,String to)
{
	//SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='RTY1000023' and b.current_date between '2012-11-10 00:00:00' and '2012-11-17 23:59:59'
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT br.roomdetails_id,br.booking_id,br.roomtype_id,br.noof_rooms,br.total_cost,b.current_date,b.checkindate,b.checkoutdate,b.booked_person_name,b.booked_flag,b.paymentstauts,b.extra2,b.total_amount,b.extra3,b.internet_charges,b.up,b.downs,b.shift,b.service_charges,b.extra_bed_required,b.extra_bed_charges,b.issue_date,b.counter,b.image_person,b.extra4 FROM booking_roomsdetails br,bookingrooms b where b.booking_id=br.booking_id and br.roomtype_id='"+room_type+"' and b.checkoutdate between '"+from+"' and '"+to+"' and b.paymentstauts='success' "; 
	//System.out.println("\nReport_by_RoomType_checkoutdate ="+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new booking_roomsdetails(rs.getString("roomdetails_id"),rs.getString("booking_id"),rs.getString("roomtype_id"),rs.getString("noof_rooms"),rs.getString("total_cost"),rs.getString("current_date"),rs.getString("checkindate"),rs.getString("checkoutdate"),rs.getString("booked_person_name"),rs.getString("booked_flag"),rs.getString("paymentstauts"),rs.getString("extra2"),rs.getString("total_amount"),rs.getString("extra3"),rs.getString("internet_charges"),rs.getString("up"),rs.getString("downs"),rs.getString("shift"),rs.getString("service_charges"),rs.getString("extra_bed_required"),rs.getString("extra_bed_charges"),rs.getString("issue_date"),rs.getString("counter"),rs.getString("image_person"),rs.getString("extra4")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
}



}