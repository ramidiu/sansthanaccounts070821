package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.doctordetails;
public class doctordetailsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String doctorId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setdoctorId(String doctorId)
{
this.doctorId = doctorId;
}
public String getdoctorId(){
return(this.doctorId);
}
public List getdoctordetails()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT doctorId,doctorName,doctorQualification,phoneNo,address,extra1,extra2,extra3,extra4,extra5 FROM doctordetails "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new doctordetails(rs.getString("doctorId"),rs.getString("doctorName"),rs.getString("doctorQualification"),rs.getString("phoneNo"),rs.getString("address"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getdoctorDetailsGroupBySpecialist()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT doctorId,doctorName,doctorQualification,phoneNo,address,extra1,extra2,extra3,extra4,extra5 FROM doctordetails group by extra3"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new doctordetails(rs.getString("doctorId"),rs.getString("doctorName"),rs.getString("doctorQualification"),rs.getString("phoneNo"),rs.getString("address"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMdoctordetails(String doctorId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT doctorId,doctorName,doctorQualification,phoneNo,address,extra1,extra2,extra3,extra4,extra5 FROM doctordetails where  doctorId = '"+doctorId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new doctordetails(rs.getString("doctorId"),rs.getString("doctorName"),rs.getString("doctorQualification"),rs.getString("phoneNo"),rs.getString("address"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDoctorSearch(String name)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT doctorId,doctorName,doctorQualification,phoneNo,address,extra1,extra2,extra3,extra4,extra5 FROM doctordetails where  (doctorId like '%"+name+"%' || doctorName like '%"+name+"%')"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new doctordetails(rs.getString("doctorId"),rs.getString("doctorName"),rs.getString("doctorQualification"),rs.getString("phoneNo"),rs.getString("address"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDoctorsBasedOnSpecialist(String Specialist)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT doctorId,doctorName,doctorQualification,phoneNo,address,extra1,extra2,extra3,extra4,extra5 FROM doctordetails where  extra3='"+Specialist+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new doctordetails(rs.getString("doctorId"),rs.getString("doctorName"),rs.getString("doctorQualification"),rs.getString("phoneNo"),rs.getString("address"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getDoctorName(String docId)
{
String doctorName="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT doctorName FROM doctordetails where doctorId='"+docId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	doctorName=rs.getString("doctorName");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return doctorName;
}

}