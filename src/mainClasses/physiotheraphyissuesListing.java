package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.physiotheraphyissues;
import beans.doctordetails;
import beans.patientsentry;
import dbase.sqlcon.ConnectionHelper;

public class physiotheraphyissuesListing 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String physiotheraphyId;
	
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	
	public String getPhysiotheraphyId() {
		return physiotheraphyId;
	}
	public void setPhysiotheraphyId(String physiotheraphyId) {
		this.physiotheraphyId = physiotheraphyId;
	}
	
	public List getMtestissueBasedOnAppoitment(String apptid)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT physiotheraphyId, datee, appointmnetId, machineName, testName, extra1, extra2, extra3, extra4, extra5 FROM physiotheraphy_services where  appointmnetId = '"+apptid+"'"; 
	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new physiotheraphyissues(rs.getString("physiotheraphyId"),rs.getString("datee"),rs.getString("appointmnetId"),rs.getString("machineName"),rs.getString("testName"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List<doctordetails> getDoctorIdBetweenDates(String formdate,String todate)
	{
		List<doctordetails> list = new ArrayList<doctordetails>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT dd.doctorName,dd.doctorId  FROM appointments a,physiotheraphy_services p,doctordetails dd where a.appointmnetId=p.appointmnetId and dd.doctorId=a.doctorId  and p.datee between '"+formdate+"' and '"+todate+"' group by a.doctorId"; 
			//System.out.println("===="+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				doctordetails dd=new doctordetails();
				dd.setdoctorId(rs.getString("dd.doctorId"));
				dd.setdoctorName(rs.getString("dd.doctorName"));
				list.add(dd);
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	public List<patientsentry> getPatientIdBasedOnDoctorId(String formdate,String todate,String doctorId)
	{
		List<patientsentry> list = new ArrayList<patientsentry>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT pe.patientId,pe.patientName FROM appointments a,patientsentry pe,physiotheraphy_services p,doctordetails dd where a.appointmnetId=p.appointmnetId and dd.doctorId=a.doctorId and a.patientId=pe.patientId and p.datee between '"+formdate+"' and '"+todate+"' and dd.doctorId='"+doctorId+"'group by pe.patientId"; 
			//System.out.println("===="+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				patientsentry pe=new patientsentry();
				pe.setpatientId(rs.getString("pe.patientId"));
				pe.setpatientName(rs.getString("pe.patientName"));
				list.add(pe);
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	public List<physiotheraphyissues> getPhysiotheraphyBasedOnPatient(String formdate,String todate,String doctorId,String patientId)
	{
		List<physiotheraphyissues> list = new ArrayList<physiotheraphyissues>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT p.physiotheraphyId,p.datee,p.appointmnetId,p.machineName,p.testName,p.extra1,p.extra2,p.extra3,p.extra4,p.extra5 FROM physiotheraphy_services p,appointments a where a.appointmnetId=p.appointmnetId and a.patientId='"+patientId+"' and a.doctorId='"+doctorId+"' and p.datee between '"+formdate+"' and '"+todate+"'"; 
			//System.out.println("===="+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
			 list.add(new physiotheraphyissues(rs.getString("physiotheraphyId"),rs.getString("datee"),rs.getString("appointmnetId"),rs.getString("machineName"),rs.getString("testName"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));
		
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	public List<physiotheraphyissues> getPhysiotheraphyBetweenDates(String formdate,String todate,String doctorId)
	{
		List<physiotheraphyissues> list = new ArrayList<physiotheraphyissues>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT p.physiotheraphyId,p.datee,p.appointmnetId,p.machineName,p.testName,p.extra1,p.extra2,p.extra3,p.extra4,p.extra5 FROM physiotheraphy_services p,appointments a where a.appointmnetId=p.appointmnetId and a.doctorId='"+doctorId+"' and p.datee between '"+formdate+"' and '"+todate+"'"; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new physiotheraphyissues(rs.getString("physiotheraphyId"),rs.getString("datee"),rs.getString("appointmnetId"),rs.getString("machineName"),rs.getString("testName"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));	
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
}
