package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import beans.products;
import dbase.sqlcon.ConnectionHelper;

public class productsListing {
	private String error = "";

	public void seterror(String error) {
		this.error = error;
	}

	public String geterror() {
		return this.error;
	}

	private String productId;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;

	public void setproductId(String productId) {
		this.productId = productId;
	}

	public String getproductId() {
		return (this.productId);
	}

	public List getproducts() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products  where extra10!='Deactive' order by productId ";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getMproducts(String productId) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where  productId = '"
					+ productId + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductName(String productId, String headId) {
		String name = "";
		String selectquery = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			selectquery = "SELECT productName FROM products where  productId = '"
					+ productId + "' and head_account_id='" + headId + "'";
			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				/*
				 * list.add(new
				 * products(rs.getString("productId"),rs.getString("productName"
				 * )
				 * ,rs.getString("description"),rs.getString("purchaseAmmount"),
				 * rs
				 * .getString("sellingAmmount"),rs.getString("head_account_id")
				 * ,rs
				 * .getString("major_head_id"),rs.getString("sub_head_id"),rs.
				 * getString
				 * ("reorderLevel"),rs.getString("balanceQuantityStore")
				 * ,rs.getString
				 * ("balanceQuantityGodwan"),rs.getString("extra1"),
				 * rs.getString(
				 * "extra2"),rs.getString("extra3"),rs.getString("extra4"
				 * ),rs.getString
				 * ("extra5"),rs.getString("vat"),rs.getString("vat1"
				 * ),rs.getString
				 * ("profcharges"),rs.getString("units"),rs.getString
				 * ("extra6"),rs
				 * .getString("extra7"),rs.getString("extra8"),rs.getString
				 * ("extra9"),rs.getString("extra10")));
				 */
				name = rs.getString("productName");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}

	public List getMproducts1(String productId, String hod, String majorhead, String minorhead, String subhead) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where  productId = '"
					+ productId
					+ "' and head_account_id = '"
					+ hod
					+ "' and major_head_id = '"
					+ majorhead
					+ "' and extra3 = '"
					+ minorhead
					+ "' and sub_head_id = '"
					+ subhead + "'";
			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getMproductsBasedOnHoid(String productId, String hoid) {
		ArrayList list = new ArrayList();
		try {
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where  productId = '"
					+ productId + "' and head_account_id='" + hoid + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getProductsBasedOnKey(String key, String hoaid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			String con = "";
			if (hoaid != null && !hoaid.equals("")) {
				con = " and head_account_id='" + hoaid + "' ";
			}
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,TRUNCATE(balanceQuantityStore,3) as balanceQuantityStore,TRUNCATE(balanceQuantityGodwan,3) as balanceQuantityGodwan,extra1,(sellingAmmount*(1+(vat1/100))) as extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where  (productName like '%"
					+ key + "%' || productId like '%" + key + "%')" + con;
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getProductsBasedOnKeyAndMajorHead(String key, String hoaid, String majorhead) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			String con = "";
			String majorinquery = "";
			if (hoaid != null && !hoaid.equals("")) {
				con = " and head_account_id='" + hoaid + "' ";
			}
			if (majorhead != null && !majorhead.equals("")) {
				majorinquery = " and major_head_id ='" + majorhead + "' ";
			}
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,(sellingAmmount*(1+(vat1/100))) as extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where  (productName like '%"
					+ key
					+ "%' || productId like '%"
					+ key
					+ "%')"
					+ con
					+ majorinquery;
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getProductsBasedOnKeyAndSubHead(String key, String hoaid, String majorhead, String minorhead, String subhead) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			String con = "";
			String majorinquery = "";
			String minorinquery = "";
			String subheadinquery = "";

			if (hoaid != null && !hoaid.equals("")) {
				con = " and head_account_id='" + hoaid + "' ";
			}
			if (majorhead != null && !majorhead.equals("")) {
				majorinquery = " and major_head_id ='" + majorhead + "' ";
			}
			if (minorhead != null && !minorhead.equals("")) {
				minorinquery = " and extra3 ='" + minorhead + "' ";
			}
			if (subhead != null && !subhead.equals("")) {
				subheadinquery = " and sub_head_id ='" + subhead + "' ";
			}
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,(sellingAmmount*(1+(vat1/100))) as extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where  (productName like '%"
					+ key
					+ "%' || productId like '%"
					+ key
					+ "%')"
					+ con
					+ majorinquery + minorinquery + subheadinquery;
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getProductsBasedOnKeyAndHeadId(String key, String hid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "";
			selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,TRUNCATE(balanceQuantityStore,3) as balanceQuantityStore,TRUNCATE(balanceQuantityGodwan,3) as balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where (productId like '%"
					+ key
					+ "%' || productName like '%"
					+ key
					+ "%') and head_account_id='" + hid + "'";
			System.out.println("selectQuery=====>" + selectquery);
			/*
			 * selectquery=
			 * "SELECT tabletId as productId,tabletName productName,quantity as balanceQuantityGodwan   FROM tablets where ( tabletId like '%"
			 * +key+"%' || tabletName like '%"+key+"%')";
			 */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {

				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}
	public List getMedicalProductsBasedOnKeyAndHeadId(String key, String hid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "";
			selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,TRUNCATE(balanceQuantityStore,3) as balanceQuantityStore,TRUNCATE(balanceQuantityGodwan,3) as balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where (productId like '%"
					+ key
					+ "%' || productName like '%"
					+ key
					+ "%') and head_account_id='" + hid + "' and major_head_id='60' and extra3='447'";
			System.out.println("selectQuery=====>" + selectquery);
			/*
			 * selectquery=
			 * "SELECT tabletId as productId,tabletName productName,quantity as balanceQuantityGodwan   FROM tablets where ( tabletId like '%"
			 * +key+"%' || tabletName like '%"+key+"%')";
			 */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {

				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}
	public List getKindProductsBasedOnKeyAndHeadId(String key, String hid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "";
			selectquery = "SELECT productId,productName,balanceQuantityGodwan FROM products where (productId like '%"
					+ key
					+ "%' || productName like '%"
					+ key
					+ "%') and head_account_id='" + hid + "'";
			// System.out.println(selectquery);
			/*
			 * selectquery=
			 * "SELECT tabletId as productId,tabletName productName,quantity as balanceQuantityGodwan   FROM tablets where ( tabletId like '%"
			 * +key+"%' || tabletName like '%"+key+"%')";
			 */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs
						.getString("balanceQuantityGodwan")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsStockGodwan(String prdid) {
		String list = "0";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT balanceQuantityGodwan FROM products where  productId = '"
					+ prdid + "'";
			/* System.out.println(selectquery); */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				if (!rs.getString("balanceQuantityGodwan").equals("")) {
					list = rs.getString("balanceQuantityGodwan");
				}
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsStockGodwanWithHOA(String prdid, String hoa) {
		String list = "0";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT balanceQuantityGodwan FROM products where  productId = '"
					+ prdid
					+ "' and head_account_id='"
					+ hoa
					+ "' order by balanceQuantityGodwan desc";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			if (rs.next()) {
				if (!rs.getString("balanceQuantityGodwan").equals("")) {
					list = rs.getString("balanceQuantityGodwan");
				}
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsStockStore(String prdid) {
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT balanceQuantityStore FROM products where  productId = '"
					+ prdid + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("balanceQuantityStore");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsNameByCat(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// System.out.println("-----"+Category);
			if (Category.equals("4") || Category.equals("1")
					|| Category.equals("5") || Category.equals("3")) {

				selectquery = "SELECT name as productName  FROM subhead where  sub_head_id = '"
						+ prdid + "' and head_account_id='" + Category + "'";
//				System.out.println();
//				System.out.println("select query for getProductsNameByCat if equals empty(subhead table)222222222222222222222222222222"+selectquery);
			} else {
				selectquery = "SELECT productName FROM products where  productId = '"
						+ prdid + "' and head_account_id='" + Category + "'";
//				System.out.println();
//				System.out.println("select query for getProductsNameByCat if equals empty(products table)222222222222222222222222222222"+selectquery);

			}
			// System.out.println("======>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
				// System.out.println(rs.getString("productName"));
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductNameByCat(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// System.out.println("-----"+Category);
			if (Category.equals("4") || Category.equals("1") || Category.equals("5") || Category.equals("3")) {
				selectquery = "SELECT productName FROM products where  productId = '"+ prdid + "' and head_account_id='" + Category + "'";
			}
			// System.out.println("======>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
				// System.out.println(rs.getString("productName"));
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	
	
	public String getProductNameByHeads(String Category,String majorHead,String minorHead,String subHead,String prdid) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
				selectquery = "SELECT productName FROM products where  productId ="+ prdid + " and head_account_id=" + Category + " and major_head_id="+majorHead+" and extra3="+minorHead+" and sub_head_id="+subHead;
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}
	
	
	
	
	public String getProductNameByCat1(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// System.out.println("-----"+Category);
			if (Category.equals("4") || Category.equals("1")
					|| Category.equals("5") || Category.equals("3")) {

				selectquery = "SELECT name FROM subhead where sub_head_id = '"
						+ prdid + "' and head_account_id='" + Category + "'";
			}

			// System.out.println("======>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("name");
				// System.out.println(rs.getString("productName"));
			}

			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsNameByCats(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// System.out.println("Category-----"+Category);
			if (Category.equals("4") || Category.equals("1")) {

				selectquery = "SELECT name as productName  FROM subhead where  sub_head_id = '"
						+ prdid + "'";
			} else {
				selectquery = "SELECT productName FROM products where  productId = '"
						+ prdid + "'";
			}
			// System.out.println("--->"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
				// System.out.println(rs.getString("productName"));
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductNameByCats(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// System.out.println("-----"+Category);
			if (Category.equals("4") || Category.equals("1")) {
				selectquery = "SELECT productName FROM products where  productId = '"
						+ prdid + "'";
			} else {
				selectquery = "SELECT name as productName  FROM subhead where  sub_head_id = '"
						+ prdid + "'";
			}
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
				// System.out.println(rs.getString("productName"));
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsNameByCatsn(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// System.out.println("-----"+Category);
			if (Category.equals("4") || Category.equals("1")
					|| Category.equals("5")) {

				selectquery = "SELECT name as productName  FROM subhead where  sub_head_id = '"
						+ prdid + "'";
			} else {
				selectquery = "SELECT productName FROM products where  productId = '"
						+ prdid + "'";
			}

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
				// System.out.println(rs.getString("productName"));
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsNameByHid(String prdid, String hoaid) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			String con = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			if (hoaid != null && !hoaid.equals("")) {
				con = " and head_account_id='" + hoaid + "' ";
			}
			selectquery = "SELECT productName FROM products where  productId = '"
					+ prdid + "'" + con;

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsNameByCat1(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();

			selectquery = "SELECT productName FROM products where  productId = '"
					+ prdid + "'";

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsNameByCat2(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();

			selectquery = "SELECT productName FROM products where  productId = '"
					+ prdid + "' and head_account_id='" + Category + "'";

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("productName");
				// System.out.println(rs.getString("productName"));
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsNameByCatUnits(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();

			selectquery = "SELECT units FROM products where  productId = '"
					+ prdid + "'";

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("units");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsStockByCat(String prdid, String Cat) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();

			selectquery = "SELECT balanceQuantityGodwan FROM products where  productId ='"
					+ prdid + "'";

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("balanceQuantityGodwan");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsStockShop(String prdid) {
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT balanceQuantityStore FROM products where  productId = '"
					+ prdid + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("balanceQuantityStore");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsStockShopWithHOA(String prdid, String hoa) {
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT balanceQuantityStore FROM products where  productId = '"
					+ prdid
					+ "'  and head_account_id='"
					+ hoa
					+ "' order by balanceQuantityStore desc";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			if (rs.next()) {
				list = rs.getString("balanceQuantityStore");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductPrice(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			if (Category.equals("8")) {
				selectquery = "SELECT extra3 as purchaseAmmount  FROM tablets where  tabletId = '"
						+ prdid + "'";
			} else {
				selectquery = "SELECT purchaseAmmount FROM products where  productId = '"
						+ prdid + "'";
			}

			 System.out.println("get product price-->"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				// list=rs.getString("productName");
				list = rs.getString("purchaseAmmount");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public boolean getvalidProduct(String productid) {
		boolean list = false;
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM products where productId='"
					+ productid + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = true;

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getvalidProductWithHOAOnlyNegtiv(String productid, String HOA) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM products where productId='"
					+ productid + "' and head_account_id='" + HOA + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public boolean getvalidProductWithHOA(String productid, String HOA) {
		boolean list = false;
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM products where productId='"
					+ productid + "' and head_account_id='" + HOA + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = true;

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public boolean getvalidVendor(String vendorid) {
		boolean list = false;
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM vendors where vendorId='"
					+ vendorid + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = true;

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public boolean getvalidSubHead(String subHead) {
		boolean list = false;
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM subhead where sub_head_id='"
					+ subHead + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = true;

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPoojaList(String subheadId, String hoid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 where  head_account_id = '"
					+ hoid + "' and sub_head_id='" + subheadId + "'";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPoojaPrice(String subheadId, String hoid, String proId) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where  head_account_id = '"
					+ hoid
					+ "' and sub_head_id='"
					+ subheadId
					+ "' and productId='" + proId + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getMedicalStore(String hoid) {
		ArrayList list = new ArrayList();
		try {
			String con = "";
			if (hoid != null && !hoid.equals("")) {
				con = " where head_account_id='" + hoid + "'";
			}
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM products" + con;
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getMedicalStoreStock(String hoid) {
		ArrayList list = new ArrayList();
		try {
			String con = "";
			if (hoid != null && !hoid.equals("")) {
				con = " and  head_account_id='" + hoid + "'";
			}
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM products where major_head_id='60'"
					+ con;
//			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsAmount(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			if (Category.equals("1")) {
				selectquery = "SELECT tabletName as price  FROM tablets where  tabletId = '"
						+ prdid + "'";
			} else {
				selectquery = "SELECT sellingAmmount as price FROM products where  productId = '"
						+ prdid + "'";
			}

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("price");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsBalanceQty(String prdid, String Category) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			if (Category.equals("1")) {
				selectquery = "SELECT tabletName as price  qty tablets where  tabletId = '"
						+ prdid + "'";
			} else {
				selectquery = "SELECT balanceQuantityStore as qty FROM products where  productId = '"
						+ prdid + "'";
			}

			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("qty");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getHeadOFAccountID(String productId) {
		String name = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT head_account_id FROM products where productId='"
					+ productId + "' ";
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				name = rs.getString("head_account_id");

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return name;
	}

	public List getPresentShopStock(String productID, String hoid) {
		ArrayList list = new ArrayList();
		try {
			String product = "";
			String hodinnerquey = "";
			if (productID != null && !productID.equals("")) {
				// System.out.println(productID);
				String[] pro = productID.split(" ");
				product = " and productId='" + pro[0] + "'";
			}
			if (hoid != null && !hoid.equals("")
					&& (hoid.equals("1") || hoid.equals("4"))) {
				hodinnerquey = " (head_account_id='1' or  head_account_id='4')";
			} else {
				hodinnerquey = " head_account_id='" + hoid + "' ";
			}
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM products where  "
					+ hodinnerquey + " " + product;
//			System.out.println("product lising getPresentShopStock------"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPresentShopStock1() {
		ArrayList list = new ArrayList();
		try {

			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM products where (head_account_id='1' or  head_account_id='4')";
//			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPresentShopStockAssets(String[] majorHeads) {
		ArrayList list = new ArrayList();
		try {

			String major = "";
			String majorHD = "";
//			System.out.println("majorHeads......." + majorHeads.length);
			for (int i = 0; i < majorHeads.length; i++) {
				if (i != majorHeads.length - 1) {
					major = majorHeads[i] + "' or major_head_id='";
				} else {
					major = majorHeads[i] + "'";
				}
				majorHD = majorHD + major;
			}
//			System.out.println("major......." + majorHD);

			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// String
			// selectquery="SELECT * FROM products where (head_account_id='1')"
			// ;
			String selectquery = "SELECT * FROM products where major_head_id='"
					+ majorHD + " and head_account_id!='5'";
		System.out.println("selectquery:::::::"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPresentShopStockConsumer(String[] productId) {
		ArrayList list = new ArrayList();
		try {

			String product = "";
			String productD = "";
//			System.out.println("majorHeads......." + productId.length);
			for (int i = 0; i < productId.length; i++) {
				if (i != productId.length - 1) {
					product = productId[i] + "' or productId='";
				} else {
					product = productId[i] + "'";
				}
				productD = productD + product;
			}
//			System.out.println("product......." + productD);

			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// String
			// selectquery="SELECT * FROM products where (head_account_id='1')"
			// ;
			String selectquery = "SELECT * FROM products where productId='"
					+ productD;
//			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPresentShopStockAssets1(String[] majorHeads, String productId1, String productId2) {
		ArrayList list = new ArrayList();
		try {

			String major = "";
			String majorHD = "";
//			System.out.println("majorHeads......." + majorHeads.length);
			for (int i = 0; i < majorHeads.length; i++) {
				if (i != majorHeads.length - 1) {
					major = majorHeads[i] + "' or major_head_id='";
				} else {
					major = majorHeads[i] + "'";
				}
				majorHD = majorHD + major;
			}
//			System.out.println("major......." + majorHD);

			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// String
			// selectquery="SELECT * FROM products where (head_account_id='1')"
			// ;
			String selectquery = "SELECT * FROM products where productId not between'"
					+ productId1
					+ "' and '"
					+ productId2
					+ "' and major_head_id='" + majorHD;
//			System.out.println("111=====> " + selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPresentShopStockAssetsName(String ProductId1, String ProductId2) {
		ArrayList list = new ArrayList();
		try {

			/*
			 * String major=""; String majorHD="";
			 * System.out.println("ProductName......."+ProductName.length);
			 * for(int i=0;i<ProductName.length;i++){
			 * if(i!=ProductName.length-1){ major= ProductName[i] +
			 * "%' or productName like '%"; }else{ major=ProductName[i]+"%'"; }
			 * majorHD=majorHD+major; }
			 * System.out.println("major......."+majorHD);
			 */

			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// String
			// selectquery="SELECT * FROM products where (head_account_id='1')"
			// ;
			String selectquery = "SELECT * FROM products where productId between '"
					+ ProductId1 + "' and '" + ProductId2 + "'";
//			System.out.println("getPresentShopStockAssetsName >>>>>>>> "+ selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPresentShopStocks(String productID, String hoid) {
		ArrayList list = new ArrayList();
		try {
			String product = "";
			String hodinnerquey = "";
			if (productID != null && !productID.equals("")) {
				// System.out.println(productID);
				String[] pro = productID.split(" ");
				product = " and productId='" + pro[0] + "'";
			}

			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			// String
			// selectquery="SELECT * FROM products where  head_account_id = '"+hoid+"' "+product;
			String selectquery = "SELECT * FROM products where  head_account_id = '"
					+ hoid + "' " + product + " and major_head_id!='60'";
//			System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getSellingVat(String prdid) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			selectquery = "SELECT vat1  FROM products where  productId = '"
					+ prdid + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("vat1");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getSellingRate(String prdid) {
		String list = "";
		try {
			// Load the database driver
			String selectquery = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			selectquery = "SELECT sellingAmmount   FROM products where  productId = '"
					+ prdid + "'";
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list = rs.getString("sellingAmmount");
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getProductEqualCon(String key, String hoaid) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			String con = "";
			if (hoaid != null && !hoaid.equals("")) {
				con = " and head_account_id='" + hoaid + "' ";
			}
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,TRUNCATE(balanceQuantityStore,3) as balanceQuantityStore,TRUNCATE(balanceQuantityGodwan,3) as balanceQuantityGodwan,extra1,(sellingAmmount*(1+(vat1/100))) as extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where   productId='"
					+ key + "'" + con;
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getNegativeProductReport(String hoid) {
		ArrayList list = new ArrayList();
		try {
			String con = "";
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * FROM products where head_account_id='"
					+ hoid
					+ "' and (balanceQuantityStore <'0' or  balanceQuantityGodwan<'0' ) group by productId";
			/* System.out.println(selectquery); */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getproductsVatReport() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT p.productId,p.productName,p.description,p.purchaseAmmount,p.sellingAmmount,p.head_account_id,p.major_head_id,p.sub_head_id,p.reorderLevel,p.balanceQuantityStore,p.balanceQuantityGodwan,p.extra1,p.extra2,p.extra3,p.extra4,p.extra5,p.vat,p.vat1,p.profcharges,p.units,p.extra6,p.extra7,p.extra8,p.extra9,p.extra10 FROM customerpurchases c,products p  where   p.productId=c.productId and c.extra1='3'  and c.extra13='' group by p.productId ";
			/* System.out.println(selectquery); */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public List getPoojaStoreStockWithVat(String hoid, String vat, String con) {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			String subQuery = "";
			if (con.equals("godown")) {
				subQuery = "and balanceQuantityGodwan!='null' and balanceQuantityGodwan!=' ' and balanceQuantityGodwan!='0.0' and  balanceQuantityGodwan!='0'";
			} else if (con.equals("poojastore")) {
				subQuery = "and balanceQuantityStore!='null' and balanceQuantityStore!=' ' and balanceQuantityStore!='0.0' and balanceQuantityStore!='0'";
			}
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 from products where head_account_id = '"
					+ hoid
					+ "' and vat='"
					+ vat
					+ "' and purchaseAmmount!=' ' and purchaseAmmount!='0' and purchaseAmmount!='null' and sellingAmmount!='null' and sellingAmmount!='0' and sellingAmmount!=' ' and vat!=' ' and vat!='null' and vat1!=' ' and vat1!='null'"
					+ subQuery;
			// System.out.println(selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	public String getProductsBasedOnmedicines(String productId) {
		String product = "";
		try {
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT productId,productName,description,purchaseAmmount,sellingAmmount,head_account_id,major_head_id,sub_head_id,reorderLevel,balanceQuantityStore,balanceQuantityGodwan,extra1,extra2,extra3,extra4,extra5,vat,vat1,profcharges,units,extra6,extra7,extra8,extra9,extra10 FROM products where productId='"
					+ productId + "'";
			// System.out.println("#######>"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				if ("447".equals(rs.getString("extra3"))) {
					product = "Medicine";
				} else if ("58".equals(rs.getString("major_head_id"))) {
					product = "Education";
				} else if ("59".equals(rs.getString("major_head_id"))) {
					product = "Library";
				}
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			// System.out.println("Exception is ;"+e);
		}
		return product;
	}

	// this method is written by madhav
	public String getProductsExtra6(String prdid) {
		String list = "0";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT extra6 FROM products where  productId = '"
					+ prdid + "'";
//			System.out.println("opening bal 111===" + selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				if (!rs.getString("extra6").equals("")) {
					list = rs.getString("extra6");
				}
			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return list;
	}

	// this method is written by madhav
	public int updateExtra6BasedOnProductId(String balQtyStore, String prdid) {

		int i = 0;
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String updatequery = "update products set extra6='" + balQtyStore
					+ "' where  productId = '" + prdid + "'";
//			System.out.println("updatequery================>" + updatequery);
			i = s.executeUpdate(updatequery);
			s.close();
			c.close();
		} catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		return i;
	}

	// below method is added by madhav
	public List getAllProducts() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * from products ";
			/* System.out.println(selectquery); */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception is ;" + e);
		}
		return list;
	}
	
	
	public List getAllProductsGroupWithProductId() {
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s = c.createStatement();
			String selectquery = "SELECT * from products where head_account_id='1' group by productId";
			/* System.out.println(selectquery); */
			ResultSet rs = s.executeQuery(selectquery);
			while (rs.next()) {
				list.add(new products(rs.getString("productId"), rs
						.getString("productName"), rs.getString("description"),
						rs.getString("purchaseAmmount"), rs
								.getString("sellingAmmount"), rs
								.getString("head_account_id"), rs
								.getString("major_head_id"), rs
								.getString("sub_head_id"), rs
								.getString("reorderLevel"), rs
								.getString("balanceQuantityStore"), rs
								.getString("balanceQuantityGodwan"), rs
								.getString("extra1"), rs.getString("extra2"),
						rs.getString("extra3"), rs.getString("extra4"), rs
								.getString("extra5"), rs.getString("vat"), rs
								.getString("vat1"),
						rs.getString("profcharges"), rs.getString("units"), rs
								.getString("extra6"), rs.getString("extra7"),
						rs.getString("extra8"), rs.getString("extra9"), rs
								.getString("extra10")));

			}
			s.close();
			rs.close();
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception is ;" + e);
		}
		return list;
	}
	
public List<products> getProductsBasedOnData(String hoa,String data)	{
	String query = "SELECT p.productId,p.productName,p.balanceQuantityGodwan,p.extra7,p.units FROM products p where (p.productName like '%"+data+"%') and p.head_account_id= "+hoa+" group by p.productname";
	ArrayList<products> prodList = null;
//	System.out.println("gdddddddd"+query);
	try	{
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		ResultSet rs = s.executeQuery(query);
		prodList = new ArrayList<products>();
		while(rs.next())	{
			products prod = new products();
			prod.setproductId(rs.getString("p.productId"));
			prod.setproductName(rs.getString("p.productName"));
			prod.setbalanceQuantityGodwan(rs.getString("p.balanceQuantityGodwan"));
			prod.setextra7(rs.getString("p.extra7"));
			prod.setunits(rs.getString("p.units"));
			prodList.add(prod);
		}
	}
	catch(SQLException e)	{
	e.printStackTrace();
	}
	return prodList;
}

public List<products> getProductsListbsdOnHoa(String hoa)	{
	String query = "";
	String condition = " ";
	if (!hoa.trim().equals(""))	{
		condition += "where head_account_id="+hoa+" ";
	}
	query = "select * from products"+condition+"group by productid order by productid";
	ResultSet rs = null;
	List<products> productsList = null;
	try	{
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		rs = s.executeQuery(query);
//		System.out.println("prodlist----"+query);
		productsList = new ArrayList<products>();
		while(rs.next())	{
			products product = new products();
			product.setproductId(rs.getString("productId"));
			product.setproductName(rs.getString("productName"));
			product.setdescription(rs.getString("description"));
			product.setpurchaseAmmount(rs.getString("purchaseAmmount"));
			product.setsellingAmmount(rs.getString("sellingAmmount"));
			product.sethead_account_id(rs.getString("head_account_id"));
			product.setmajor_head_id(rs.getString("major_head_id"));
			product.setsub_head_id(rs.getString("sub_head_id"));
			product.setreorderLevel(rs.getString("reorderLevel"));
			product.setbalanceQuantityStore(rs.getString("balanceQuantityStore"));
			product.setbalanceQuantityGodwan(rs.getString("balanceQuantityGodwan"));
			product.setprofcharges(rs.getString("profcharges"));
			product.setunits(rs.getString("units"));
			product.setextra7(rs.getString("extra7"));
			
			productsList.add(product);
		}
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	finally	{
		try {
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return productsList;
}
public List<products> getProductsBasedOnKeyAndHoa(String key, String hid) {
	ArrayList list = new ArrayList();
	try {
		// Load the database driver
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		String selectquery = "";
		selectquery = "SELECT p.productId,p.productName,p.major_head_id,p.sub_head_id,TRUNCATE(p.balanceQuantityStore,3) as balanceQuantityStore,TRUNCATE(p.balanceQuantityGodwan,3) as balanceQuantityGodwan,p.extra3,p.units,p.extra6,p.extra7,m.name as mjname,mn.name as mnname,s.name as sname FROM products p,majorhead m,subhead s,minorhead mn where (p.productId like '%"
				+ key
				+ "%' || p.productName like '%"
				+ key
				+ "%') and p.head_account_id='" + hid + "' and (m.major_head_id = p.major_head_id) and (mn.minorhead_id = p.extra3) and (p.sub_head_id = s.sub_head_id)  group by p.productid";
//		System.out.println("selectQuery=====>" + selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while (rs.next()) {
			products prod = new products();
			prod.setproductId(rs.getString("p.productId"));
			prod.setproductName(rs.getString("p.productName"));
			prod.setmajor_head_id(rs.getString("p.major_head_id"));
			prod.setsub_head_id(rs.getString("p.sub_head_id"));
			prod.setbalanceQuantityStore(rs.getString("balanceQuantityStore"));
			prod.setbalanceQuantityGodwan(rs.getString("balanceQuantityGodwan"));
			prod.setextra3(rs.getString("p.extra3"));
			prod.setextra8(rs.getString("mjname"));
			prod.setextra9(rs.getString("mnname"));
			prod.setextra10(rs.getString("sname"));
			list.add(prod);
		}
		s.close();
		rs.close();
		c.close();
	} catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;" + e);
	}
	return list;
}

public List<String> getProductCategories() {
	ArrayList<String> list = new ArrayList<String>();
	try {
		// Load the database driver
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		String selectquery = "select distinct(extra8) as category from products where extra8 != ''";
		ResultSet rs = s.executeQuery(selectquery);
		while (rs.next()) {
			list.add(rs.getString("category"));
		}
		s.close();
		rs.close();
		c.close();
	} 
	catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;" + e);
	}
	return list;
} // method

public List<String> getProductSubCategories(String category) {
	ArrayList<String> list = new ArrayList<String>();
	try {
		
		// Load the database driver
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		String selectquery = "select distinct(extra9) as subcategory from products";
		if(!category.trim().equals(""))	{
			selectquery += " where extra8 = '"+category.trim()+"'";
		}
//		System.out.println("subcat--"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while (rs.next()) {
			if (!rs.getString("subcategory").trim().equals(""))
			list.add(rs.getString("subcategory"));
		}
		s.close();
		rs.close();
		c.close();
	} 
	catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;" + e);
	}
	return list;
} // method

public List<products> getProductsBasedOnHoaAndCategories(String category,String subcategory,String hid) {
	ArrayList list = new ArrayList();
	try {
		// Load the database driver
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		String selectquery = "",subquery=" ";
		if (!category.equals(""))	{
			subquery += "and p.extra8 = '"+category.trim()+"'";
		}
		if (!subcategory.equals(""))	{
			subquery += "and p.extra9 = '"+subcategory.trim()+"'";
		}
		selectquery = "SELECT p.productId,p.productName,p.major_head_id,p.sub_head_id,TRUNCATE(p.balanceQuantityStore,3) as balanceQuantityStore,TRUNCATE(p.balanceQuantityGodwan,3) as balanceQuantityGodwan,p.extra3,p.units,p.extra6,p.extra7,p.extra8 as category,p.extra9 as subcategory,m.name as mjname,mn.name as mnname,s.name as sname from products p,majorhead m,subhead s,minorhead mn where p.head_account_id='" + hid + "'"+subquery+" and (m.major_head_id = p.major_head_id) and (mn.minorhead_id = p.extra3) and (p.sub_head_id = s.sub_head_id)";
//		System.out.println("getProductsBasedOnHoaAndCategories=====>" + selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while (rs.next()) {
			products prod = new products();
			prod.setproductId(rs.getString("p.productId"));
			prod.setproductName(rs.getString("p.productName"));
			prod.setmajor_head_id(rs.getString("p.major_head_id"));
			prod.setsub_head_id(rs.getString("p.sub_head_id"));
			prod.setbalanceQuantityStore(rs.getString("balanceQuantityStore"));
			prod.setbalanceQuantityGodwan(rs.getString("balanceQuantityGodwan"));
			prod.setextra3(rs.getString("p.extra3"));
			prod.setextra8(rs.getString("mjname"));
			prod.setextra9(rs.getString("mnname"));
			prod.setextra10(rs.getString("sname"));
			list.add(prod);
		}
		s.close();
		rs.close();
		c.close();
	} catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;" + e);
	}
	return list;
}
public products getMajorMinorSubHeads(String headOfAccount,String productId){
	 products prod=null;
	 
		try{
			c=ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectQuery="SELECT p.productid,p.productname,p.major_head_id,p.extra3,p.sub_head_id,s.name,mi.name,mj.name FROM products p,subhead s,minorhead mi,majorhead mj where p.head_account_id='"+headOfAccount+"' and  p.productid='"+productId+"' and p.sub_head_id=s.sub_head_id  and p.extra3=mi.minorhead_id and p.major_head_id=mj.major_head_id";
			ResultSet rs=s.executeQuery(selectQuery);
			while(rs.next()){
				prod=new products();
				prod.setproductId(rs.getString("productid"));
				prod.setproductName(rs.getString("productname"));
				prod.setmajor_head_id(rs.getString("major_head_id"));
				prod.setMajorHeadName(rs.getString("mj.name"));
				prod.setextra3(rs.getString("extra3"));
				prod.setMinorHeadName(rs.getString("mi.name"));
				prod.setsub_head_id(rs.getString("sub_head_id"));
				prod.setSubHeadName(rs.getString("s.name"));
			}
			s.close();
			rs.close();
			c.close();
		}catch (Exception e) {
			this.seterror(e.toString());
			System.out.println("Exception is ;" + e);
		}
		
		
	return prod;	
}



public List<products> getProductsListbsdOnHeadOfAccount(String hoa)	{
	String query = "";
	String condition = " ";
	if (!hoa.trim().equals(""))	{
		condition += "where head_account_id="+hoa+" ";
	}
	query = "select * from products"+condition;
	ResultSet rs = null;
	List<products> productsList = null;
	try	{
		c = ConnectionHelper.getConnection();
		
		s = c.createStatement();
		rs = s.executeQuery(query);
//		System.out.println("prodlist----"+query);
		productsList = new ArrayList<products>();
		
		while(rs.next())	{
			products product = new products();
			product.setproductId(rs.getString("productId"));
			product.setproductName(rs.getString("productName"));
			product.setdescription(rs.getString("description"));
			product.setpurchaseAmmount(rs.getString("purchaseAmmount"));
			product.setsellingAmmount(rs.getString("sellingAmmount"));
			product.sethead_account_id(rs.getString("head_account_id"));
			product.setmajor_head_id(rs.getString("major_head_id"));
			product.setextra3(rs.getString("extra3"));
			product.setsub_head_id(rs.getString("sub_head_id"));
			product.setreorderLevel(rs.getString("reorderLevel"));
			product.setbalanceQuantityStore(rs.getString("balanceQuantityStore"));
			product.setbalanceQuantityGodwan(rs.getString("balanceQuantityGodwan"));
			product.setprofcharges(rs.getString("profcharges"));
			product.setunits(rs.getString("units"));
			product.setextra7(rs.getString("extra7"));
			
			productsList.add(product);
		}
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	finally	{
		try {
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return productsList;
}
public List getDeactivateProducts()
{
  ArrayList list = new ArrayList();
  try
  {
    this.c = ConnectionHelper.getConnection();
    this.s = this.c.createStatement();
    String selectquery = "SELECT * FROM products  where  extra10 = 'Deactive'";
    ResultSet rs = this.s.executeQuery(selectquery);
    while (rs.next()) {
    	products product = new products();
		product.setproductId(rs.getString("productId"));
		product.setproductName(rs.getString("productName"));
		product.setdescription(rs.getString("description"));
		product.setpurchaseAmmount(rs.getString("purchaseAmmount"));
		product.setsellingAmmount(rs.getString("sellingAmmount"));
		product.sethead_account_id(rs.getString("head_account_id"));
		product.setmajor_head_id(rs.getString("major_head_id"));
		product.setsub_head_id(rs.getString("sub_head_id"));
		product.setreorderLevel(rs.getString("reorderLevel"));
		product.setbalanceQuantityStore(rs.getString("balanceQuantityStore"));
		product.setbalanceQuantityGodwan(rs.getString("balanceQuantityGodwan"));
		product.setprofcharges(rs.getString("profcharges"));
		product.setunits(rs.getString("units"));
		product.setextra7(rs.getString("extra7"));
		list.add(product);
    }
    this.s.close();
    rs.close();
    this.c.close();
  }
  catch (Exception e)
  {
    seterror(e.toString());
    System.out.println("Exception is ;" + e);
  }
  return list;
}

public products getMajorMinorAndSubHeadForServiceBillApproval(String productId,String headOfAccount){
	products prod=null;
	try{
		c=ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectQuery="SELECT g.productId,g.minorhead_id,g.majorhead_id,s.name,mj.name,mi.name from godwanstock g,majorhead mj,minorhead mi,subhead s WHERE g.productId='"+productId+"' AND g.productId=s.sub_head_id AND g.majorhead_id=mj.major_head_id AND g.minorhead_id=mi.minorhead_id AND g.department='"+headOfAccount+"'";
		ResultSet rs=s.executeQuery(selectQuery);
		while(rs.next()){
			prod=new products();
			prod.setproductId(rs.getString("g.productId"));
			prod.setmajor_head_id(rs.getString("g.majorhead_id"));
			prod.setextra3(rs.getString("g.minorhead_id"));
			prod.setsub_head_id(rs.getString("g.productId"));
			prod.setMajorHeadName(rs.getString("mj.name"));
			prod.setMinorHeadName(rs.getString("mi.name"));
			prod.setSubHeadName(rs.getString("s.name"));
		}
		
	}catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;" + e);
	}
	
	
return prod;	
}
public products getMajorMinorSubheadName(String productId,String headOfAccount){
	products prod=null;
	try {
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		
		String selectquery = "";
		selectquery = "SELECT  p.productId,p.major_head_id,p.extra3,p.sub_head_id,mj.name,mi.name,s.name FROM products p,majorhead mj,minorhead mi,subhead s where   p.productId='"+productId+"' and p.head_account_id='"+headOfAccount+"'and  p.sub_head_id=s.sub_head_id and mi.minorhead_id=s.extra1 and mj.major_head_id=mi.major_head_id";
//		System.out.println("new selectquery"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while (rs.next()) {
			prod = new products();
			prod.setproductId(rs.getString("p.productId"));
			prod.setmajor_head_id(rs.getString("p.major_head_id"));
			prod.setextra3(rs.getString("p.extra3"));
			prod.setsub_head_id(rs.getString("p.sub_head_id"));
			prod.setMajorHeadName(rs.getString("mj.name"));
			prod.setMinorHeadName(rs.getString("mi.name"));
			prod.setSubHeadName(rs.getString("s.name"));
		}
		s.close();
		rs.close();
		c.close();
	} catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;" + e);
	}
	return prod;
}
public Map<String,products> getMedicineProdutsMap() {
	Map<String,products> productsMap=new HashMap<String,products>();
	try {
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		
		String selectquery = "";
		selectquery = "SELECT * FROM products where major_head_id='60' and head_account_id='1'";
		ResultSet rs = s.executeQuery(selectquery);
		while (rs.next()) {
			products prod = new products();
			prod.setproductId(rs.getString("productId"));
			prod.setproductName(rs.getString("productName"));
			prod.setmajor_head_id(rs.getString("major_head_id"));
			prod.setextra3(rs.getString("extra3"));
			prod.setsub_head_id(rs.getString("sub_head_id"));
			prod.setbalanceQuantityGodwan(rs.getString("balanceQuantityGodwan"));
			productsMap.put(prod.getproductId(),prod);
		}
		s.close();
		rs.close();
		c.close();
	} catch (Exception e) {
		this.seterror(e.toString());
		System.out.println("Exception is ;" + e);
	}
	return productsMap;
}
 public List<products> getProductsBasedOnCategoryAndSubCategory(String category,String subCategory){
	 List<products> productsList=new ArrayList<products>();
	 try {
		   c=ConnectionHelper.getConnection();
		   s=c.createStatement();
		   String query="select * from products where extra8='"+category+"' and extra9='"+subCategory+"' and head_account_id='1'";
		   ResultSet rs=s.executeQuery(query);
		   while(rs.next()) {
			   products products=new products();
			   products.setproductId(rs.getString("productid"));
			   products.setproductName(rs.getString("productName"));
			   products.sethead_account_id(rs.getString("head_account_id"));
			   products.setmajor_head_id(rs.getString("major_head_id"));
			   products.setextra3(rs.getString("extra3"));
			   products.setsub_head_id(rs.getString("sub_head_id"));
			   productsList.add(products);
		   }
		   s.close();
			rs.close();
			c.close();
	 }catch(Exception e) {
		 e.printStackTrace();
	 }
	 return productsList;
 }
 public List<products> getProductsBasedOnCategory(String category,String subcat1){
	 List<products> productsList=new ArrayList<products>();
	 try {
		   c=ConnectionHelper.getConnection();
		   s=c.createStatement();
		   String query="select * from products where extra8='"+category+"' and extra9='"+subcat1+"' and head_account_id='1'";
		   ResultSet rs=s.executeQuery(query);
		   while(rs.next()) {
			   products products=new products();
			   products.setproductId(rs.getString("productid"));
			   products.setproductName(rs.getString("productName"));
			   products.sethead_account_id(rs.getString("head_account_id"));
			   products.setmajor_head_id(rs.getString("major_head_id"));
			   products.setextra3(rs.getString("extra3"));
			   products.setsub_head_id(rs.getString("sub_head_id"));
			   productsList.add(products);
		   }
		   s.close();
			rs.close();
			c.close();
	 }catch(Exception e) {
		 e.printStackTrace();
	 }
	 return productsList;
 }
}