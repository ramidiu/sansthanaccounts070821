package mainClasses;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.banktransactions;
import beans.banktransactionsService;
import beans.customerpurchasesService;
@WebServlet("/brsurl")
public class BrsServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 System.out.println("calling from wtsp triggering messages.........!");		
		HttpSession ses=request.getSession(false);
		String empid=(String)ses.getAttribute("adminId");
		String type=request.getParameter("type");
		System.out.println("type...!"+type);
		String checkedboxes[]=request.getParameterValues("check");
		System.out.println("request.getParameterValues(\"check\")...!"+request.getParameterValues("check"));
		String hoa=request.getParameter("head");
		int result=0;
		String jvNum="";
		int res=0;
		customerpurchasesService cpservice= new customerpurchasesService();
		banktransactionsListing bktService = new banktransactionsListing();
		banktransactionsService bktrns = new banktransactionsService();
		no_genaratorListing NG_l=new no_genaratorListing();
		if (checkedboxes != null && checkedboxes.length > 0)	{
			for(int i=0;i<checkedboxes.length;i++)	{
				String substring=checkedboxes[i].substring(checkedboxes[i].lastIndexOf("e")+1);
				System.out.println("substring in rs update...!"+substring);
				String date=request.getParameter("hid2"+substring);
				System.out.println("date...!"+date);
				String depdate=request.getParameter("depositdate"+substring);
				System.out.println("depositdate in rs update...!"+depdate);
				
				String jvnumber=request.getParameter("jv"+substring);
				System.out.println("jvnumber...!"+jvnumber);
				String amount=request.getParameter("amount"+substring);
				System.out.println("amount...!"+amount);
				if(date!=null && !date.equals("") && depdate!=null && !depdate.equals("") && type!=null && !type.equals("") && jvnumber.equals("") && hoa.equals("1")) {
//					System.out.println("1"); 
					if(type.equals("card") || type.equals("online success") || type.equals("googlepay") || type.equals("phonepe"))	{
//						System.out.println("2");
						try	{
							 jvNum=NG_l.getid("journal_voucher_no");
							 System.out.println("jv number--->"+jvNum);
						 }
						 catch(Exception e){
							 e.printStackTrace();
						 }
						res=cpservice.insertJvInCustomerPurchases(amount,depdate,hoa,jvNum,empid);
						 System.out.println("res--->"+res);
						// checking for already deposited entries
						banktransactions bkt = bktService.getBankTransactionBsdOnJvAmount(date,amount);
						 System.out.println("bkt--->"+bkt);
						if (bkt != null)	{
//							System.out.println("3");
							bktrns.updateExtra3AndBrsDateBasdOnBkt(jvNum,bkt.getbanktrn_id(),depdate);
						}
						else	{
//							System.out.println("4");
							Calendar cal=Calendar.getInstance();
							DateFormat df= new SimpleDateFormat("HH:mm:ss");
							String subdate=df.format(cal.getTime());
							System.out.println("inerting in bank...!");
							banktransactions bankt = new banktransactions();
							bankt.setbank_id("BNK1019");
							bankt.setdate(depdate);
							bankt.setamount(amount);
							bankt.settype("deposit");
							bankt.setcreatedBy(empid);
							bankt.setextra2("null");
							bankt.setextra3(jvNum);
							bankt.setextra4("Charity-cash");
							bankt.setExtra7(depdate+" "+subdate);
							bankt.setExtra8("other-amount");
							bankt.setSub_head_id("25766");
							bankt.setMinor_head_id("690");
							bankt.setMajor_head_id("103");
							bankt.setHead_account_id("4");
							bankt.setbrs_date(depdate.split(" ")[0]);
							bankt.setname("reconciliation");
							bktrns.saveBankTransaction(bankt);
						}
						result=cpservice.updateDepositedDate(date,depdate,type,jvNum,hoa);
					} // if
					else if(type.equals("cash") || type.equals("cheque"))	{
//						System.out.println("5");
						String jnum=jvNum;
						jnum="";
						result=cpservice.updateDepositedDate(date,depdate,type,jnum,hoa);
					} // else if
				} // if
				else if(date!=null && !date.equals("") && depdate!=null && !depdate.equals("") && type!=null && !type.equals("") && !jvnumber.equals(""))	{
//					System.out.println("6");
					res=cpservice.updateJvDate(depdate,jvnumber);
						bktrns.updateJvAndBrsDate(depdate,jvnumber);
					result=cpservice.updateDepositedDate(date,depdate,type,jvnumber,hoa); 
				} // else if
				else if(date!=null && !date.equals("") && depdate.equals("") && !jvnumber.equals("") && hoa.equals("1"))	{
//					System.out.println("7");
					res=cpservice.deleteJv(jvnumber);
					 bktrns.deleteBankTxn(jvnumber);
					String jnum=jvnumber;
					jnum="";
					result=cpservice.updateDepositedDate(date,depdate,type,jnum,hoa);
				} // else if
				else if(date!=null && !date.equals("") && jvnumber.equals("") && depdate.equals(""))	{
//					System.out.println("8");
					result=cpservice.updateDepositedDate(date,depdate,type,jvnumber,hoa);
				} // else if
				if(date!=null && !date.equals("") && depdate!=null && !depdate.equals("") && type!=null && !type.equals("") && jvnumber.equals("") && !hoa.equals("1")) {
//					System.out.println("9");
					String jnum=jvNum;
					jnum="";
					result=cpservice.updateDepositedDate(date,depdate,type,jnum,hoa);
				} // if
			} // for
		}
		response.sendRedirect("admin/adminPannel.jsp?page=cardCheckonlineBrsdateupdate");
	}
}
