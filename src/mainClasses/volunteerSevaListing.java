package mainClasses;
import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.volunteerseva;

public class volunteerSevaListing {
	
	private String productId;
	
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	
	public List getVolunteerSevasListBasedOnDate(String role)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="";
	System.out.println("role value.."+role);
	if(role.equals("all")){
		selectquery="SELECT volseva_id,seva_name,sssst_id,datee,created_date,extra1,extra2,extra3,extra4,extra5 FROM volunteer_seva"; 
	}
	else{
	selectquery="SELECT volseva_id,seva_name,sssst_id,datee,created_date,extra1,extra2,extra3,extra4,extra5 FROM volunteer_seva where seva_name like'%"+ role+"%'"; 
	}
	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new volunteerseva(rs.getString("volseva_id"),rs.getString("seva_name"),rs.getString("sssst_id"),rs.getString("datee"),rs.getString("created_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
		e.printStackTrace();
	}
	return list;
	}

}
