package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.payments;
public class paymentsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String paymentId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setpaymentId(String paymentId)
{
this.paymentId = paymentId;
}
public String getpaymentId(){
return(this.paymentId);
}
public List getpayments()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT paymentId,date,bankId,narration,partyName,vendorId,reference,chequeNo,amount,remarks,paymentType,extra1,extra2,extra3,extra4,extra5 FROM payments ORDER by date"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMpayments(String paymentId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT paymentId,date,bankId,narration,partyName,vendorId,reference,chequeNo,amount,remarks,paymentType,extra1,extra2,extra3,extra4,extra5,TBECResolutionNO,TBECDate,status,extra6,extra7,extra8,extra9 FROM payments where  paymentId = '"+paymentId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("TBECResolutionNO"),rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getpaymentsByDateByHOA(String head_account_id,String entry_date)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(p.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where pe.entry_date<'"+entry_date+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";  //commented by pradeep
String selectquery="SELECT sum(pe.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where pe.entry_date<'"+entry_date+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";
System.out.println("paymentlist==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("TBECResolutionNO"),rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getpaymentsBtwDatesByHOA(String head_account_id,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(p.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where pe.entry_date<'"+entry_date+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";  //commented by pradeep
/*String selectquery="SELECT sum(pe.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where pe.entry_date >= '"+fromdate+"' and pe.entry_date <= '"+todate+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";*/
String selectquery="SELECT sum(pe.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where p.date >= '"+fromdate+"' and p.date <= '"+todate+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";
//System.out.println("paymentlist==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("TBECResolutionNO"),rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getpaymentsBtwDatesByHOA1(String head_account_id,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(p.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where pe.entry_date<'"+entry_date+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";  //commented by pradeep
//String selectquery="SELECT sum(pe.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where pe.entry_date >= '"+fromdate+"' and pe.entry_date <= '"+todate+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";
//String selectquery="SELECT sum(pe.amount) as TBECResolutionNO,p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where p.date >= '"+fromdate+"' and p.date <= '"+todate+"' and p.extra3=pe.expinv_id and pe.vendorId!='1001' and  pe.head_account_id='"+head_account_id+"'  group by pe.vendorId ";
//System.out.println("paymentlist==>"+selectquery);

String selectquery  = "SELECT vendorId, sum(amount) as amount FROM payments where date between '"+fromdate+"' and '"+todate+"' and vendorId!='1001' and vendorId!='1165' and vendorId!='1356' and vendorId!='1164' and vendorId!='1153' and vendorId!='1095' and vendorId!='' and extra3 in(select expinv_id from productexpenses where head_account_id='"+head_account_id+"') group by vendorId;";

ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	//list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("TBECResolutionNO"),rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));
	payments pay = new payments();
	pay.setvendorId(rs.getString("vendorId"));
	pay.setamount(rs.getString("amount"));
	if(rs.getString("vendorId").equals("1435")){
		System.out.println("payments..amount......."+rs.getString("amount"));
	}
	
	
	list.add(pay);
	//System.out.println(rs.getString("vendorId")+" ----- > "+rs.getString("amount"));
}

s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getChequeNo(String payInvId)
{
	String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT paymentId,date,bankId,narration,partyName,vendorId,reference,chequeNo,amount,remarks,paymentType,extra1,extra2,extra3,extra4,extra5,TBECResolutionNO,TBECDate,status,extra6,extra7,extra8,extra9 FROM payments where  extra4 = '"+payInvId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("chequeNo");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMpaymentsBasedOnExpID(String ExpID)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM payments where extra3='"+ExpID+"' group by extra4"; 
//System.out.println("===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getAllPaymentsBasedOnFromDateToDate(String fromDate,String toDate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM payments where date>='"+fromDate+"%' and date<='"+toDate+"%'"; 
System.out.println("===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMpaymentsBasedOnExpID(String vendorId, String date1,String date2)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM payments where vendorId='"+vendorId+"' and date(date) between '"+date1+"' and '"+date2+"' group by extra3"; 
//System.out.println("===>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getSuccesfulPaymentsBasedOnExpID(String ExpID)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM payments where extra3 in (select expinv_id from productexpenses where extra1='Payment Done' and expinv_id='"+ExpID+"')  group by extra4"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMpaymentsBasedOnExpIDSize(String MSEID)
{
	int list =1;
	int no=0;
	String rows="no";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM payments ps,productexpenses px,godwanstock g where ps.extra3=px.expinv_id and  px.extra5='"+MSEID+"' group by ps.extra4"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	rows="yes";
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
rows=list+" "+rows;
return rows;
}
public String getMpaymentsWithOutIndentSize(String indentid)
{
	int list =1;
	int no=0;
	String rows="no";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM payments ps,productexpenses px,godwanstock g,purchaseorder p  where ps.extra3=px.expinv_id and  px.extra5=g.extra1 and g.extra2=p.poinv_id and p.indentinvoice_id='"+indentid+"' group by ps.extra4"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	rows="yes";
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
rows=list+" "+rows;
return rows;
}
public String getMpaymentsBasedOnUniqueIdSize(String uniqueid)
{
	int list =1;
	int no=0;
	String rows="no";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(*) as no FROM payments ps,productexpenses px  where ps.extra3=px.expinv_id and  px.extra6='"+uniqueid+"' group by ps.extra4"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	no=no+1;
 }
if(no>0){
	list=no;
	rows="yes";
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
rows=list+" "+rows;
return rows;
}
public List getMpaymentsBasedOnPaymentInvoice(String paymentInvId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT paymentId,date,bankId,narration,partyName,vendorId,reference,chequeNo,sum(amount) as amount,extra6 as remarks,paymentType,extra1,extra2,extra3,extra4,extra5,TBECResolutionNO,TBECDate,status,extra6,extra7,extra8,extra9,service_tax_amount, service_cheque_name, service_cheque_no, other_amount, other_cheque_name, other_cheque_no, extra10, extra11, extra12, extra13, extra14 FROM payments where  extra4 = '"+paymentInvId+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 //list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));
	list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("TBECResolutionNO"),rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));
	//System.out.println("from db"+rs.getString("extra7"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDailyPaymentsReport(String hoid,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String subquery="";
if(hoid!=null && !hoid.equals("")){
	subquery=" and pe.head_account_id='"+hoid+"' ";
}
String selectquery="SELECT p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.extra6 as remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5, p.TBECResolutionNO, p.TBECDate, p.status, pe.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11,p.extra12, p.extra13, p.extra14 FROM payments p,productexpenses pe where  p.date >= '"+fromdate+"' and p.date<='"+todate+"' and p.extra3=pe.expinv_id"+subquery+"  group by p.extra3 order by p.date"; 
System.out.println(hoid+"ss"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("paymentId")!=null){
		list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("TBECResolutionNO"),rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPassOrderInfo(String paymentId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT paymentId,date,bankId,narration,partyName,vendorId,reference,chequeNo,sum(amount) as amount,remarks,paymentType,extra1,extra2,extra3,extra4,extra5,TBECResolutionNO,TBECDate,status,extra6,extra7,extra8,extra9,service_tax_amount, service_cheque_name, service_cheque_no, other_amount, other_cheque_name, other_cheque_no, extra10, extra11, extra12, extra13, extra14 FROM payments where  paymentId = '"+paymentId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("TBECResolutionNO"),rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPaymentsByDate(String fromdate ,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5, p.TBECResolutionNO, p.TBECDate, p.status,pe.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no,p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM payments p,productexpenses pe  where  ( date  between '"+fromdate+"' and '"+todate+"' ) and pe.expinv_id=p.extra3  and  (p.extra5>1 or p.extra7 >1) group by extra3"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("TBECResolutionNO"),rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getPaidAmountToVendor(String vendor)
{
	String paidAmt ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(amount) as paidAmount FROM payments where vendorId='"+vendor+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	paidAmt=rs.getString("paidAmount");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return paidAmt;
}
public String getPaidAmountToVendor(String vendor ,String date1,String date2)
{
	String paidAmt ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();

String selectquery="SELECT sum(amount) as paidAmount FROM payments where vendorId='"+vendor+"' and date(date) between '"+date1+"' and '"+date2+"' order by extra3";
//System.out.println("selectquery---->"+selectquery);
//String selectquery= "SELECT sum(amount) as paidAmount FROM payments where vendorId='"+vendor+"' and date(date) between '"+date1+"' and '"+date2+"' and extra3 in (SELECT expinv_id FROM productexpenses where vendorId='"+vendor+"' and date(entry_date) between '"+date1+"' and '"+date2+"' and extra5 in (SELECT extra1 FROM godwanstock where vendorId='"+vendor+"' and date(date) between '"+date1+"' and '"+date2+"') )";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	paidAmt=rs.getString("paidAmount");
}

//System.out.println("paidAmt----->"+paidAmt);
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return paidAmt;
}
public String[] getTotalExpensesMonth(String month,String vendorId)
{
String[]  total = new String[2];
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT pe.extra5, sum(p.amount) as paidAmount FROM payments p, productexpenses pe where  p.date like '"+month+"%' and pe.expinv_id=p.extra3 and p.vendorId='"+vendorId+"'"; 
//System.out.println("getTotalExpensesMonth=>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 total[0]=rs.getString("paidAmount");
 total[1]=rs.getString("extra5");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return total;
}

public List<payments> getpaymentsBtwDatesByHOABasedOnVendor(String head_account_id,String fromdate,String todate,String vendorId)
{
ArrayList<payments> list = new ArrayList<payments>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where p.date >= '"+fromdate+"' and p.date <= '"+todate+"' and p.extra3=pe.expinv_id and pe.vendorId='"+vendorId+"' and  pe.head_account_id='"+head_account_id+"' group by p.paymentId";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),"",rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public List<payments> getpaymentsBtwDates(String head_account_id,String fromdate,String todate)
{
ArrayList<payments> list = new ArrayList<payments>();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT p.paymentId, p.date, p.bankId, p.narration, p.partyName, p.vendorId, p.reference, p.chequeNo, p.amount, p.remarks, p.paymentType, p.extra1, p.extra2, p.extra3, p.extra4, p.extra5,  p.TBECDate, p.status, p.extra6, p.extra7, p.extra8, p.extra9, p.service_tax_amount, p.service_cheque_name, p.service_cheque_no, p.other_amount, p.other_cheque_name, p.other_cheque_no, p.extra10, p.extra11, p.extra12, p.extra13, p.extra14 FROM productexpenses pe,payments p where p.date >= '"+fromdate+"' and p.date <= '"+todate+"' and p.extra3=pe.expinv_id group by p.paymentId";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new payments(rs.getString("paymentId"),rs.getString("date"),rs.getString("bankId"),rs.getString("narration"),rs.getString("partyName"),rs.getString("vendorId"),rs.getString("reference"),rs.getString("chequeNo"),rs.getString("amount"),rs.getString("remarks"),rs.getString("paymentType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),"",rs.getString("TBECDate"),rs.getString("status"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("service_tax_amount"),rs.getString("service_cheque_name"),rs.getString("service_cheque_no"),rs.getString("other_amount"),rs.getString("other_cheque_name"),rs.getString("other_cheque_no"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


}