package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.*;
public class leavepermissionListing 
{
	private String error="";
	public void seterror(String error)
	{
		this.error=error;
	}
	public String geterror()
	{
		return this.error;
	}

	private String leavper_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	public void setleavper_id(String leavper_id)
	{
		this.leavper_id = leavper_id;
	}
	public String getleavper_id(){
		return(this.leavper_id);
	}
	public List getleavepermission()
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT leavper_id,leave_date,leave_type,leave_askeddate,leave_status,employee_id,extra1,extra2 FROM leavepermission "; 
			//System.out.println("getleavepermission : "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new leavepermission(rs.getString("leavper_id"),rs.getString("leave_date"),rs.getString("leave_type"),rs.getString("leave_askeddate"),rs.getString("leave_status"),rs.getString("employee_id"),rs.getString("extra1"),rs.getString("extra2")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List getMleavepermission(String leavper_id)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT leavper_id,leave_date,leave_type,leave_askeddate,leave_status,employee_id,extra1,extra2 FROM leavepermission where  leavper_id = '"+leavper_id+"'"; 
			//System.out.println("getMleavepermission : "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new leavepermission(rs.getString("leavper_id"),rs.getString("leave_date"),rs.getString("leave_type"),rs.getString("leave_askeddate"),rs.getString("leave_status"),rs.getString("employee_id"),rs.getString("extra1"),rs.getString("extra2")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}

	public String getLeavePermission(String leave_date,String employee_id)
	{
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
		//	System.out.println("in LeavePermissionListing.java leave date "+leave_date);
		//	System.out.println("in LeavePermissionListing.java employee_id "+employee_id);
			String selectquery="SELECT leave_type FROM leavepermission where  employee_id = '"+employee_id+"' and leave_date='"+leave_date+"' and leave_status='yes'"; 
			//System.out.println("getLeavePermission"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list=rs.getString("leave_type");

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public String getLeavePermission(String leave_date,String employee_id, String leave_type)
	{
		String list = "";
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT extra1 FROM leavepermission where  employee_id = '"+employee_id+"' and leave_date='"+leave_date+"' and leave_status='yes' and leave_type='"+leave_type+"' "; 
		//System.out.println("getLeavePermission"+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list=rs.getString("extra1");

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List getLeavePermission(String leave_date)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM leavepermission where  leave_date='"+leave_date+"'";
			//System.out.println("getLeavePermission : "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new leavepermission(rs.getString("leavper_id"),rs.getString("leave_date"),rs.getString("leave_type"),rs.getString("leave_askeddate"),rs.getString("leave_status"),rs.getString("employee_id"),rs.getString("extra1"),rs.getString("extra2")));

			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List getLeavePermissionBasedOnDateAndId(String today_date , String employee_id)
	{
		ArrayList list = new ArrayList();
		try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT * FROM leavepermission where  employee_id = '"+employee_id+"' and leave_date = '"+today_date+"'" ; 
			//String selectquery="SELECT * FROM leavepermission where  employee_id = '"+employee_id+"' and leave_date='"+today_date+"'";
			//System.out.println("getLeavePermissionBasedOnDateAndId : "+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				list.add(new leavepermission(rs.getString("leavper_id"),rs.getString("leave_date"),rs.getString("leave_type"),rs.getString("leave_askeddate"),rs.getString("leave_status"),rs.getString("employee_id"),rs.getString("extra1"),rs.getString("extra2")));
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public String maxDateInLeavePermission(){
		
		String last_date = "";
		try {
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String query = "SELECT max(leave_date) FROM sansthanaccounts.leavepermission";
	//	System.out.println("maxDateInLeavePermission : "+query);
		ResultSet rs = s.executeQuery(query);
		while(rs.next()){
			last_date = rs.getString("max(leave_date)");
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
		return last_date;
	}
public String getCountofTwoDates(String date1 , String date2){
		
		String count = "";
		try {
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String query = "SELECT count(leave_date) FROM sansthanaccounts.leavepermission where leave_date between '"+date1+"' and '"+date2+"'";
		//System.out.println("getCountofTwoDates : "+query);
		ResultSet rs = s.executeQuery(query);
		while(rs.next()){
			count = rs.getString("count(leave_date)");
		}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
	}
		return count;
	}
}