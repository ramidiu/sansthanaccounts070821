package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dbase.sqlcon.ConnectionHelper;

import beans.duesService;

public class dueListing {

	Connection c=null;
	Statement st=null;
	ResultSet rs=null;
	
	public List<duesService> getDueDetails(String AdvInvoice){
		//System.out.println("111111111..."+AdvInvoice);
		List<duesService> dues=new ArrayList<duesService>();
		try {
			c=ConnectionHelper.getConnection();
			st=c.createStatement();
			String query="select DueId,date,finYear,headAccountId,advanceInvId,majorHeadId,minorHeadId,subHeadId,invoiceId,amount,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15,extra16,extra17,extra18,extra19,extra20 from dues where advanceInvId='"+AdvInvoice+"'";
			rs=st.executeQuery(query);
			System.out.println("111....query.."+query);
			while(rs.next()){
				dues.add(new duesService(rs.getString("DueId"),rs.getString("date"),rs.getString("finYear"),rs.getString("headAccountId"),rs.getString("advanceInvId"),rs.getString("majorHeadId"),rs.getString("minorHeadId"),rs.getString("subHeadId"),rs.getString("invoiceId"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15"),rs.getString("extra16"),rs.getString("extra17"),rs.getString("extra18"),rs.getString("extra19"),rs.getString("extra20")));
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return dues;
	}
}
