package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.sssst_registrations;
public class sssst_registrationsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String sssst_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setsssst_id(String sssst_id)
{
this.sssst_id = sssst_id;
}
public String getsssst_id(){
return(this.sssst_id);
}
public List getsssst_registrations()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4 FROM sssst_registrations"; 
String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4,father_name,mother_name,mothertongue,bloodgroup,education_qualification,previous_experience,date_of_appointed,dependents,designation,identification_marks,height,weight,basic_pay,present_pay_scale,occupation,permanent_address,present_address,service_experinece_in_sansthan_trust,technical_qualification,caste_religion,physical_disorder,govt_reservation,enclosures,doa,reservation_cat,percent_of_disorder,ec_resolution_num,ec_resolution_date,tb_resolution_num,tb_resolution_date,employee_identification_num,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15 FROM sssst_registrations order by SUBSTRING(sssst_id ,5)*1 desc"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getMsssst_registrations(String sssst_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4,father_name,mother_name,mothertongue,bloodgroup,education_qualification,previous_experience,date_of_appointed,dependents,designation,identification_marks,height,weight,basic_pay,present_pay_scale,occupation,permanent_address,present_address,service_experinece_in_sansthan_trust,technical_qualification,caste_religion,physical_disorder,govt_reservation,enclosures,doa,reservation_cat,percent_of_disorder,ec_resolution_num,ec_resolution_date,tb_resolution_num,tb_resolution_date,employee_identification_num,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15 FROM sssst_registrations where  sssst_id = '"+sssst_id+"'";
System.out.println("selectquery....."+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List searchRegistrations(String sssst_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4 FROM sssst_registrations where  sssst_id like '%"+sssst_id+"%'"; 
String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4,father_name,mother_name,mothertongue,bloodgroup,education_qualification,previous_experience,date_of_appointed,dependents,designation,identification_marks,height,weight,basic_pay,present_pay_scale,occupation,permanent_address,present_address,service_experinece_in_sansthan_trust,technical_qualification,caste_religion,physical_disorder,govt_reservation,enclosures,doa,reservation_cat,percent_of_disorder,ec_resolution_num,ec_resolution_date,tb_resolution_num,tb_resolution_date,employee_identification_num,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15 FROM sssst_registrations where  mobile like '%"+sssst_id+"%'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getOnlinesssstregistrations(String fromDate,String toDate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4,father_name,mother_name,mothertongue,bloodgroup,education_qualification,previous_experience,date_of_appointed,dependents,designation,identification_marks,height,weight,basic_pay,present_pay_scale,occupation,permanent_address,present_address,service_experinece_in_sansthan_trust,technical_qualification,caste_religion,physical_disorder,govt_reservation,enclosures,doa,reservation_cat,percent_of_disorder,ec_resolution_num,ec_resolution_date,tb_resolution_num,tb_resolution_date,employee_identification_num,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15 FROM sssst_registrations where refer_by = 'online' and register_date between '"+fromDate+"' and '"+toDate+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getOnlinesssstregistrations()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4,father_name,mother_name,mothertongue,bloodgroup,education_qualification,previous_experience,date_of_appointed,dependents,designation,identification_marks,height,weight,basic_pay,present_pay_scale,occupation,permanent_address,present_address,service_experinece_in_sansthan_trust,technical_qualification,caste_religion,physical_disorder,govt_reservation,enclosures,doa,reservation_cat,percent_of_disorder,ec_resolution_num,ec_resolution_date,tb_resolution_num,tb_resolution_date,employee_identification_num,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15 FROM sssst_registrations"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getOnlinesssstregistrationsBasedOnDate(String fromDate,String toDate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4,father_name,mother_name,mothertongue,bloodgroup,education_qualification,previous_experience,date_of_appointed,dependents,designation,identification_marks,height,weight,basic_pay,present_pay_scale,occupation,permanent_address,present_address,service_experinece_in_sansthan_trust,technical_qualification,caste_religion,physical_disorder,govt_reservation,enclosures,doa,reservation_cat,percent_of_disorder,ec_resolution_num,ec_resolution_date,tb_resolution_num,tb_resolution_date,employee_identification_num,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15 FROM sssst_registrations where refer_by = 'online' and  register_date between '"+fromDate+"'  and '"+toDate+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getDevoteesWhoHaveEnteredDOB()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4,father_name,mother_name,mothertongue,bloodgroup,education_qualification,previous_experience,date_of_appointed,dependents,designation,identification_marks,height,weight,basic_pay,present_pay_scale,occupation,permanent_address,present_address,service_experinece_in_sansthan_trust,technical_qualification,caste_religion,physical_disorder,govt_reservation,enclosures,doa,reservation_cat,percent_of_disorder,ec_resolution_num,ec_resolution_date,tb_resolution_num,tb_resolution_date,employee_identification_num,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15 FROM sssst_registrations where dob != ''"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getSSSST(String firstname,String add1,String add2,String country,String gothram)
{
String uniqueId = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sssst_id,title,first_name,middle_name,last_name,spouce_name,gender,dob,address_1,address_2,country,state,city,pincode,mobile,idproof_1,idproof1_num,idproof_2,idproof2_num,image,pancard_no,email_id,updates_receive_via,gothram,remarks,refer_by,register_date,extra1,extra2,extra3,extra4,father_name,mother_name,mothertongue,bloodgroup,education_qualification,previous_experience,date_of_appointed,dependents,designation,identification_marks,height,weight,basic_pay,present_pay_scale,occupation,permanent_address,present_address,service_experinece_in_sansthan_trust,technical_qualification,caste_religion,physical_disorder,govt_reservation,enclosures,doa,reservation_cat,percent_of_disorder,ec_resolution_num,ec_resolution_date,tb_resolution_num,tb_resolution_date,employee_identification_num,extra5,extra6,extra7,extra8,extra9,extra10,extra11,extra12,extra13,extra14,extra15 FROM sssst_registrations where first_name like '%"+firstname.trim()+"%' and address_1 like '%"+add1.trim()+"%' and address_2 like '%"+add2.trim()+"%' and country like '%"+country.trim()+"%' and gothram like '%"+gothram.trim()+"%'"; 
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	uniqueId = rs.getString("sssst_id");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return uniqueId;
}

public List searchAndGetDevoteesRegList(sssst_registrations SSSST)
{
	StringBuilder addQuery=new StringBuilder("");
	if(SSSST.getsssst_id()!=null && !SSSST.getsssst_id().trim().equals("") && !SSSST.getsssst_id().trim().equals("null"))
	{
		 addQuery.append(" and s.sssst_id like '%"+SSSST.getsssst_id()+"%' ");
	}
	if(SSSST.getfirst_name()!=null && !SSSST.getfirst_name().trim().equals("") && !SSSST.getfirst_name().trim().equals("null"))
	{
		 addQuery.append(" and s.first_name like '%"+SSSST.getfirst_name()+"%' ");
	}
	if(SSSST.getmobile()!=null && !SSSST.getmobile().trim().equals("") && !SSSST.getmobile().trim().equals("null"))
	{
		 addQuery.append(" and s.mobile like '%"+SSSST.getmobile()+"%' ");
	}
	if(SSSST.getgothram()!=null && !SSSST.getgothram().trim().equals("") && !SSSST.getgothram().trim().equals("null"))
	{
		 addQuery.append(" and s.gothram like '%"+SSSST.getgothram()+"%' ");
	}
ArrayList list = new ArrayList();
try {
 // Load the database driver 
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT s.sssst_id,s.title,s.first_name,s.middle_name,s.last_name,s.spouce_name,s.gender,s.dob,s.address_1,s.address_2,s.country,s.state,s.city,s.pincode,s.mobile,s.idproof_1,s.idproof1_num,s.idproof_2,s.idproof2_num,s.image,s.pancard_no,s.email_id,s.updates_receive_via,s.gothram,s.remarks,s.refer_by,s.register_date,s.extra1,s.extra2,s.extra3,s.extra4,s.father_name,s.mother_name,s.mothertongue,s.bloodgroup,s.education_qualification,s.previous_experience,s.date_of_appointed,s.dependents,s.designation,s.identification_marks,s.height,s.weight,s.basic_pay,s.present_pay_scale,s.occupation,s.permanent_address,s.present_address,s.service_experinece_in_sansthan_trust,s.technical_qualification,s.caste_religion,s.physical_disorder,s.govt_reservation,s.enclosures,s.doa,s.reservation_cat,s.percent_of_disorder,s.ec_resolution_num,s.ec_resolution_date,s.tb_resolution_num,s.tb_resolution_date,s.employee_identification_num,s.extra5,s.extra6,s.extra7,s.extra8,s.extra9,s.extra10,s.extra11,s.extra12,s.extra13,s.extra14,s.extra15 FROM sssst_registrations s,unique_id_group u where s.sssst_id = u.uniqueid "+addQuery+" and u.employee != 'EMPLOYEE' and u.extra1 != 'VOLUNTEER'";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getSSSSTListForDOBOrAnniv(String columnName,String value,String likeOperator,String withPhone,String extra)
{
ArrayList list = new ArrayList();
String query1 = "";
String query2 = "";
try {
	if(likeOperator.equals("like")){
		query1 = " and trim(s."+columnName+") like '"+value+"'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	if(likeOperator.equals("")){
		query1 = " and trim(s."+columnName+") = '"+value+"'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	if(withPhone.equals("withPhone")){
		query2 = " and (trim(s.mobile) != '' || trim(s.email_id) != '')";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select s.sssst_id,s.title,s.first_name,s.middle_name,s.last_name,s.spouce_name,s.gender,s.dob,s.address_1,s.address_2,s.country,s.state,s.city,s.pincode,s.mobile,s.idproof_1,s.idproof1_num,s.idproof_2,s.idproof2_num,s.image,s.pancard_no,s.email_id,s.updates_receive_via,s.gothram,s.remarks,s.refer_by,s.register_date,s.extra1,s.extra2,s.extra3,s.extra4,s.father_name,s.mother_name,s.mothertongue,s.bloodgroup,s.education_qualification,s.previous_experience,s.date_of_appointed,s.dependents,s.designation,s.identification_marks,s.height,s.weight,s.basic_pay,s.present_pay_scale,s.occupation,s.permanent_address,s.present_address,s.service_experinece_in_sansthan_trust,s.technical_qualification,s.caste_religion,s.physical_disorder,s.govt_reservation,s.enclosures,s.doa,s.reservation_cat,s.percent_of_disorder,s.ec_resolution_num,s.ec_resolution_date,s.tb_resolution_num,s.tb_resolution_date,s.employee_identification_num,s.extra5,s.extra6,s.extra7,s.extra8,s.extra9,s.extra10,s.extra11,s.extra12,s.extra13,s.extra14,s.extra15 FROM sssst_registrations s,unique_id_group u where s.sssst_id = u.uniqueid "+query1+query2+" and (u.trusties = 'TRUSTEES' || u.employee = 'EMPLOYEE' || u.extra1 = 'VOLUNTEER' || u.extra2 = 'ONLINE') group by s.first_name";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getSSSSTListForDOBOrAnnivDates(String columnName,String value,String likeOperator,String withPhone,String extra,String fromDate,String toDate)
{
ArrayList list = new ArrayList();
String query1 = "";
String query2 = "";
try {
	if(likeOperator.equals("like")){
		query1 = " and trim(s."+columnName+") like '"+value+"'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	if(likeOperator.equals("")){
		query1 = " and trim(s."+columnName+") = '"+value+"'";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
	if(withPhone.equals("withPhone")){
		query2 = " and (trim(s.mobile) != '' || trim(s.email_id) != '')";
		//System.out.println("from db"+fromdate+"===="+todate);
	}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="select s.sssst_id,s.title,s.first_name,s.middle_name,s.last_name,s.spouce_name,s.gender,s.dob,s.address_1,s.address_2,s.country,s.state,s.city,s.pincode,s.mobile,s.idproof_1,s.idproof1_num,s.idproof_2,s.idproof2_num,s.image,s.pancard_no,s.email_id,s.updates_receive_via,s.gothram,s.remarks,s.refer_by,s.register_date,s.extra1,s.extra2,s.extra3,s.extra4,s.father_name,s.mother_name,s.mothertongue,s.bloodgroup,s.education_qualification,s.previous_experience,s.date_of_appointed,s.dependents,s.designation,s.identification_marks,s.height,s.weight,s.basic_pay,s.present_pay_scale,s.occupation,s.permanent_address,s.present_address,s.service_experinece_in_sansthan_trust,s.technical_qualification,s.caste_religion,s.physical_disorder,s.govt_reservation,s.enclosures,s.doa,s.reservation_cat,s.percent_of_disorder,s.ec_resolution_num,s.ec_resolution_date,s.tb_resolution_num,s.tb_resolution_date,s.employee_identification_num,s.extra5,s.extra6,s.extra7,s.extra8,s.extra9,s.extra10,s.extra11,s.extra12,s.extra13,s.extra14,s.extra15 FROM sssst_registrations s,unique_id_group u where s.sssst_id = u.uniqueid "+query1+query2+" and (u.trusties = 'TRUSTEES' || u.employee = 'EMPLOYEE' || u.extra1 = 'VOLUNTEER' || u.extra2 = 'ONLINE') and s.register_date between '"+fromDate+"' and '"+toDate+"' group by s.first_name";
System.out.println("ssssssss:::::"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new sssst_registrations(rs.getString("sssst_id"),rs.getString("title"),rs.getString("first_name"),rs.getString("middle_name"),rs.getString("last_name"),rs.getString("spouce_name"),rs.getString("gender"),rs.getString("dob"),rs.getString("address_1"),rs.getString("address_2"),rs.getString("country"),rs.getString("state"),rs.getString("city"),rs.getString("pincode"),rs.getString("mobile"),rs.getString("idproof_1"),rs.getString("idproof1_num"),rs.getString("idproof_2"),rs.getString("idproof2_num"),rs.getString("image"),rs.getString("pancard_no"),rs.getString("email_id"),rs.getString("updates_receive_via"),rs.getString("gothram"),rs.getString("remarks"),rs.getString("refer_by"),rs.getString("register_date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("father_name"),rs.getString("mother_name"),rs.getString("mothertongue"),rs.getString("bloodgroup"),rs.getString("education_qualification"),rs.getString("previous_experience"),rs.getString("date_of_appointed"),rs.getString("dependents"),rs.getString("designation"),rs.getString("identification_marks"),rs.getString("height"),rs.getString("weight"),rs.getString("basic_pay"),rs.getString("present_pay_scale"),rs.getString("occupation"),rs.getString("permanent_address"),rs.getString("present_address"),rs.getString("service_experinece_in_sansthan_trust"),rs.getString("technical_qualification"),rs.getString("caste_religion"),rs.getString("physical_disorder"),rs.getString("govt_reservation"),rs.getString("enclosures"),rs.getString("doa"),rs.getString("reservation_cat"),rs.getString("percent_of_disorder"),rs.getString("ec_resolution_num"),rs.getString("ec_resolution_date"),rs.getString("tb_resolution_num"),rs.getString("tb_resolution_date"),rs.getString("employee_identification_num"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10"),rs.getString("extra11"),rs.getString("extra12"),rs.getString("extra13"),rs.getString("extra14"),rs.getString("extra15")));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



}