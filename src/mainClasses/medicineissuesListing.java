package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.medicineissues;
public class medicineissuesListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String documentNo;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setdocumentNo(String documentNo)
{
this.documentNo = documentNo;
}
public String getdocumentNo(){
return(this.documentNo);
}
public List getmedicineissues()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT documentNo,date,appointmnetId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM medicineissues "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new medicineissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getmedicineissues(String formdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT documentNo,date,appointmnetId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM medicineissues where date between '"+formdate+"' and '"+todate+"' group by appointmnetId"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new medicineissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getmedicineissuesByMonth(String month,String pid)
{
	String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM medicineissues m where m.date like '"+month+"-%'and m.tabletName='"+pid+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list=rs.getString("quantity");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getmedicineissuesByDay(String day,String pid)
{
	String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM medicineissues m where m.date like '"+day+"%'and m.tabletName='"+pid+"'"; 
System.out.println("outword from medicineissues =============>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list=rs.getString("quantity");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getDailymedicineissues(String formdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT documentNo,date,appointmnetId,tabletName,sum(quantity) as quantity,extra1,extra2,extra3,extra4,extra5 FROM medicineissues where date between '"+formdate+"' and '"+todate+"' group by tabletName"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new medicineissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDailymedicineissuesBasedonDotor(String formdate,String todate,String docotrid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT documentNo,m.date,m.appointmnetId,tabletName,sum(quantity) as quantity,m.extra1,p.productName as extra2,p.purchaseAmmount as extra3,p.balanceQuantityGodwan as extra4,m.extra5 FROM medicineissues m,appointments a,products p where a.date between '"+formdate+"' and '"+todate+"' and m.appointmnetId=a.appointmnetId and p.productId=tabletName and p.major_head_id='60' and a.doctorId='"+docotrid+"' group by tabletName"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new medicineissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getgMedicinestockbyDate(String fromDate,String toDate,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//SELECT sum(quantity) as quantity FROM medicineissues where m.date between '2014-04-01' and '2017-10-15'and m.tabletName='6001' 
String selectquery="SELECT sum(quantity) as quantity FROM medicineissues m where m.date between '"+fromDate+"' and '"+toDate+"'and m.tabletName='"+productid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getMedicinestockbyENDDate(String fromDate,String toDate,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//SELECT sum(quantity) as quantity FROM medicineissues m where m.date between '2014-04-01' and '2017-10-15'and m.tabletName='6001' 
String selectquery="SELECT sum(quantity) as quantity FROM medicineissues m where m.date between '"+fromDate+"' and '"+toDate+"'and m.tabletName='"+productid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}






public List getDoctors(String formdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT documentNo,date,appointmnetId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM medicineissues where date between '"+formdate+"' and '"+todate+"' group by appointmnetId"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new medicineissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMmedicineissues(String documentNo)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT documentNo,date,appointmnetId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM medicineissues where  documentNo = '"+documentNo+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new medicineissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMmedicineissuesBasedOnAppoitment(String apptid)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT documentNo,date,appointmnetId,tabletName,quantity,extra1,extra2,extra3,extra4,extra5 FROM medicineissues where  appointmnetId = '"+apptid+"'"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new medicineissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<medicineissues> getIssuedMedicineDetails(String fromDate,String toDate){
	List<medicineissues> medicineissuesList=new ArrayList<medicineissues>();
	try {
		  c = ConnectionHelper.getConnection();
		  s=c.createStatement();
		String selectquery="SELECT m.tabletname,sum(m.quantity) as consumption FROM medicineissues m where date>='"+fromDate+"' and date<='"+toDate+"' group by m.tabletname";
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			medicineissues issuedMedicine=new medicineissues();
			issuedMedicine.settabletName(rs.getString("tabletname"));
			issuedMedicine.setConsumption(rs.getDouble("consumption"));
			medicineissuesList.add(issuedMedicine);
		}
		s.close();
		rs.close();
		c.close();
	}catch(Exception e) {
		e.printStackTrace();
	}
	return medicineissuesList;
}
}