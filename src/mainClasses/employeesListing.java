package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.employees;
public class employeesListing 
 {
	private String error="";
	
	public void seterror(String error)
	{
		this.error=error;
	}
	public String geterror()
	{
		return this.error;
	}

	private String emp_id;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	
 public void setemp_id(String emp_id)
 {
	 this.emp_id = emp_id;
 }
 public String getemp_id()
 {
	 return(this.emp_id);
 }
 public List getemployees()
 {
	 ArrayList list = new ArrayList();
	 try 
	 {
		 // Load the database driver
		 	c = ConnectionHelper.getConnection();
		 	s=c.createStatement();
		 	String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees "; 
		 	ResultSet rs = s.executeQuery(selectquery);
		 	while(rs.next())
		 	{
		 		list.add(new employees(rs.getString("emp_id"),rs.getString("firstname"),rs.getString("email"),rs.getString("user_name"),rs.getString("password"),rs.getString("lastname"),rs.getString("join_date"),rs.getString("emp_sal"),rs.getString("emp_address"),rs.getString("emp_phone"),rs.getString("headAccountId"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

		 	}
		 	s.close();
		 	rs.close();
		 	c.close();
	 }
	 catch(Exception e)
	 {
		 this.seterror(e.toString());
		 System.out.println("Exception is ;"+e);
	 }
	 return list;
}
 
public List getMemployees(String emp_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees where  emp_id = '"+emp_id+"'";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employees(rs.getString("emp_id"),rs.getString("firstname"),rs.getString("email"),rs.getString("user_name"),rs.getString("password"),rs.getString("lastname"),rs.getString("join_date"),rs.getString("emp_sal"),rs.getString("emp_address"),rs.getString("emp_phone"),rs.getString("headAccountId"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMemployeesName(String emp_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees where  emp_id = '"+emp_id+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("firstname");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMemployeesRole(String emp_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees where  emp_id = '"+emp_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("extra3");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getMEmployeePassword(String emp_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees where  emp_id = '"+emp_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("password");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getEmployee(String user_name,String password)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees where  user_name = '"+user_name+"' and password='"+password+"' and extra1!='Manger' ";
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employees(rs.getString("emp_id"),rs.getString("firstname"),rs.getString("email"),rs.getString("user_name"),rs.getString("password"),rs.getString("lastname"),rs.getString("join_date"),rs.getString("emp_sal"),rs.getString("emp_address"),rs.getString("emp_phone"),rs.getString("headAccountId"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getAdminEmployee(String user_name,String password)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees where  user_name = '"+user_name+"' and password='"+password+"' and (extra1='Manger' or extra1='AsstManger' ) ";
//	System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employees(rs.getString("emp_id"),rs.getString("firstname"),rs.getString("email"),rs.getString("user_name"),rs.getString("password"),rs.getString("lastname"),rs.getString("join_date"),rs.getString("emp_sal"),rs.getString("emp_address"),rs.getString("emp_phone"),rs.getString("headAccountId"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getAdminEmployeeAndAudiotrs(String user_name,String password)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees where  user_name = '"+user_name+"' and password='"+password+"' and (extra1='Manger' or extra1='AsstManger' or extra1='Auditor' ) ";
//	System.out.println("vlidate admin--"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new employees(rs.getString("emp_id"),rs.getString("firstname"),rs.getString("email"),rs.getString("user_name"),rs.getString("password"),rs.getString("lastname"),rs.getString("join_date"),rs.getString("emp_sal"),rs.getString("emp_address"),rs.getString("emp_phone"),rs.getString("headAccountId"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public String getMemployeesHOA(String emp_id)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT emp_id,firstname,email,user_name,password,lastname,join_date,emp_sal,emp_address,emp_phone,headAccountId,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM employees where  emp_id = '"+emp_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("headAccountId");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


//select All Employee Id from  employees table and this method is calling by employeeDetailsForm.jsp file
public List selectAllEmployeesId()
{
	List l=new ArrayList();
	
	try 
	 {
		 // Load the database driver
		 	c = ConnectionHelper.getConnection();
		 	s=c.createStatement();
		 	String selectquery="SELECT emp_id FROM employees "; 
		 	ResultSet rs = s.executeQuery(selectquery);
		 	while(rs.next())
		 	{
		 		l.add(rs.getString("emp_id"));

		 	}
		 	s.close();
		 	rs.close();
		 	c.close();
	 }
	 catch(Exception e)
	 {
		 this.seterror(e.toString());
		 System.out.println("Exception is ;"+e);
	 }
	 
	return l;
	
}










}