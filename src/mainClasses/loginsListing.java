package mainClasses;

import dbase.sqlcon.SainivasConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.logins;
public class loginsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String login_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setlogin_id(String login_id)
{
this.login_id = login_id;
}
public String getlogin_id(){
return(this.login_id);
}
public List getlogins()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMlogins(String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  login_id = '"+login_id+"'"; 
//System.out.println("getMlogins ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

////////////////
public List getRoleEmp(String role)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  role = '"+role+"'"; 
//System.out.println("getMlogins ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List gettopbookingids()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT DISTINCT (l.login_id),l.name,l.email_id,l.mobile_number,count(booking_id) as no FROM bookingrooms b,logins l where b.booked_person_name=l.login_id group by l.login_id  order by no DESC"; 
ResultSet rs = s.executeQuery(selectquery);
//System.out.println("selectquery=>"+selectquery);
while(rs.next()){
	 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("no")));

	}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getRoleEmpDESC(String role)
{
ArrayList list = new ArrayList();
try {
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  role = '"+role+"' order by login_id desc "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getRoleEmpDESCRIPTION(String role)
{
ArrayList list = new ArrayList();
try {
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  role = '"+role+"' order by login_id desc "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
///////////////
public List getMvalidation(String username,String password)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  username = '"+username+"' and password = '"+password+"'";
//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

///////////////////////////
public List getMvalidationRestaurant(String username,String password)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where ( role='restaurantcounter' or role='restaurantmanager') and ( username = '"+username+"' and password = '"+password+"')";
//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getUnApprove(String extra8)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  extra8='"+extra8+"'";
//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

///////////////////////////

public List getSchemeApprove(String extra8,String role)
{
ArrayList list = new ArrayList();
try {
// Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  extra8='"+extra8+"' and role='"+role+"' ";
//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getSchemeApprove(String extra8,String role,String login_id)
{
ArrayList list = new ArrayList();
try {
// Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  extra8='"+extra8+"' and role='"+role+"' and login_id='"+login_id+"' ";
//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


/*******************Get Name**********************************/
public String getName(String login_id)
{
String list = "";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT name FROM logins where  login_id = '"+login_id+"'";
//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 //list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));
	list=rs.getString("name");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getNameRole(String login_id)
{
String list = "";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT name,role FROM logins where  login_id = '"+login_id+"'";
//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("role").equals("onlineuser")){
		list=rs.getString("name");	
	} else if(rs.getString("role").equals("receptionist")){
		list="SHRI SAINIVAS MEGA RESIDENCY";
	}  else{
		list="Saisansthan Trust,Dilsukhnagar.";
	}
	
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
/*******************Get Role**********************************/
public String getRole(String login_id)
{
String list = "";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT role FROM logins where  login_id = '"+login_id+"'";
//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 //list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));
	list=rs.getString("role");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

//------------- Getting Cleaners List --------------
public List getCleaners()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where role='cleaner' "; 
//System.out.println("\nHello : "+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getPassword(String username,String mobile_number )
{
String list = "";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT password FROM logins where  username = '"+username+"' and mobile_number='"+mobile_number+"'";

//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 //list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));
	list=rs.getString("password");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getpwd(String username)
{
String list = "";
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
/*String selectquery="SELECT password FROM logins where  username = '"+username+"' and mobile_number='"+mobile_number+"'";
*/String selectquery="SELECT password FROM logins where  username = '"+username+"' ";

//System.out.println("selectquery="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 //list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));
	list=rs.getString("password");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getmailtomanagementlist(String housekeeping,String dsnradmin,String receptionist,String stockmanagement)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where  role = '"+housekeeping+"' or role='"+dsnradmin+"' or role='"+receptionist+"' or '"+stockmanagement+"'"; 
//System.out.println("getMlogins ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getloginlistbydate(String s1,String s2)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id,name,email_id,mobile_number,username,password,role,address,branch,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,gender,qualification,doj,salary FROM logins where role='onlineuser' and ( doj between '"+s1+"' and '"+s2+"') order by doj"; 
//System.out.println("getMlogins ="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new logins(rs.getString("login_id"),rs.getString("name"),rs.getString("email_id"),rs.getString("mobile_number"),rs.getString("username"),rs.getString("password"),rs.getString("role"),rs.getString("address"),rs.getString("branch"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("gender"),rs.getString("qualification"),rs.getString("doj"),rs.getString("salary")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



}