package mainClasses;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import beans.staff_designation;

import dbase.sqlcon.ConnectionHelper;
public class staff_designationListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String stdes_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setstdes_id(String stdes_id)
{
this.stdes_id = stdes_id;
}
public String getstdes_id(){
return(this.stdes_id);
}
public List getstaff_designation()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT stdes_id,designation_name,extra1,extra2 FROM staff_designation "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new staff_designation(rs.getString("stdes_id"),rs.getString("designation_name"),rs.getString("extra1"),rs.getString("extra2")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMstaff_designation(String stdes_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT stdes_id,designation_name,extra1,extra2 FROM staff_designation where  stdes_id = '"+stdes_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new staff_designation(rs.getString("stdes_id"),rs.getString("designation_name"),rs.getString("extra1"),rs.getString("extra2")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}