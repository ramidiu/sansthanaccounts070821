package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.inventory_products_locations;
public class inventory_products_locationsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String inventory_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setinventory_id(String inventory_id)
{
this.inventory_id = inventory_id;
}
public String getinventory_id(){
return(this.inventory_id);
}
public List getinventory_products_locations()
{
ArrayList list = new ArrayList();
try {
	// Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT inventory_id,inventory_name,product_id,quantity,location_id,created_by,created_date,updated_by,updated_date,narration,status,head_account_id,image,document,extra1,extra2,extra3,extra4 FROM inventory_products_locations "; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next())
	{
		list.add(new inventory_products_locations(rs.getString("inventory_id"),rs.getString("inventory_name"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("location_id"),rs.getString("created_by"),rs.getString("created_date"),rs.getString("updated_by"),rs.getString("updated_date"),rs.getString("narration"),rs.getString("status"),rs.getString("head_account_id"),rs.getString("image"),rs.getString("document"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));
	}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMinventory_products_locations(String inventory_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT inventory_id,inventory_name,product_id,quantity,location_id,created_by,created_date,updated_by,updated_date,narration,status,head_account_id,image,document,extra1,extra2,extra3,extra4 FROM inventory_products_locations where  inventory_id = '"+inventory_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new inventory_products_locations(rs.getString("inventory_id"),rs.getString("inventory_name"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("location_id"),rs.getString("created_by"),rs.getString("created_date"),rs.getString("updated_by"),rs.getString("updated_date"),rs.getString("narration"),rs.getString("status"),rs.getString("head_account_id"),rs.getString("image"),rs.getString("document"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getProductLocationSBasedOnHoa(String hoaid)
{
	ArrayList list = new ArrayList();
	try {
		// Load the database driver
		String subQuery="";
		if(hoaid.equals("5")){
			subQuery=" where head_account_id='"+hoaid+"'";
		}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT inventory_id,inventory_name,product_id,quantity,location_id,created_by,created_date,updated_by,updated_date,narration,status,head_account_id,image,document,extra1,extra2,extra3,extra4 FROM inventory_products_locations"+subQuery; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new inventory_products_locations(rs.getString("inventory_id"),rs.getString("inventory_name"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("location_id"),rs.getString("created_by"),rs.getString("created_date"),rs.getString("updated_by"),rs.getString("updated_date"),rs.getString("narration"),rs.getString("status"),rs.getString("head_account_id"),rs.getString("image"),rs.getString("document"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


//method is defined  
public List selectAllDistinctHeadOFAccount()
{
	List l=new ArrayList();
	try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT DISTINCT  head_account_id  FROM inventory_products_locations "; 
			ResultSet rs = s.executeQuery(selectquery);
			
			while(rs.next())
			{
				inventory_products_locations loc=new inventory_products_locations();
				loc.sethead_account_id(rs.getString(1));
				l.add(loc);
	
			}
	}catch(Exception e)
	{
		
	}
	return l;
}

public List selectAllDistinctStatus()
{
	List l=new ArrayList();
	try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT DISTINCT  status  FROM inventory_products_locations "; 
			ResultSet rs = s.executeQuery(selectquery);
			
			while(rs.next())
			{
				inventory_products_locations loc=new inventory_products_locations();
				loc.setstatus(rs.getString(1));
				l.add(loc);
	
			}
	}catch(Exception e)
	{
		
	}
	return l;
}

//
public List selectAllDataBasedUponStatusAndHeadOfAccountId(String head_of_account_id,String status)
{
	List l=new ArrayList();
	try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT *  FROM inventory_products_locations where head_account_id='"+head_of_account_id+"' AND status='"+status+"'"; 
			ResultSet rs = s.executeQuery(selectquery);
			
			while(rs.next())
			{
				inventory_products_locations loc=new inventory_products_locations();
				loc.setinventory_id(rs.getString(1));
				loc.setinventory_name(rs.getString(2));
				loc.setproduct_id(rs.getString(3));
				loc.setquantity(rs.getString(4));
				loc.setlocation_id(rs.getString(5));
				loc.setcreated_by(rs.getString(6));
				loc.setcreated_date(rs.getString(7));
				loc.setupdated_by(rs.getString(8));
				loc.setupdated_date(rs.getString(9));
				loc.setnarration(rs.getString(10));
				
				loc.setstatus(rs.getString(11));
				loc.sethead_account_id(rs.getString(12));
				loc.setimage(rs.getString(13));
				loc.setdocument(rs.getString(14));
				loc.setextra1(rs.getString(15));
				loc.setextra2(rs.getString(16));
				loc.setextra3(rs.getString(17));
				loc.setextra4(rs.getString(18));
				loc.setRate(rs.getString(19));
				loc.setAmount(rs.getString(20));
				loc.setWarranty(rs.getString(21));
				loc.setDamaged_amount(rs.getString(22));
				
				l.add(loc);
	
			}
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	return l;
}

}




