package mainClasses;
import dbase.sqlcon.SainivasConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Room;
import beans.roomtype;

public class RoomListing {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	
	
	public List getRooms(String roomtype_id)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = SainivasConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT * FROM room WHERE roomtype_id='"+roomtype_id+"'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new Room(rs.getString("room_id"),rs.getString("roomtype_id"),rs.getString("room_number"),rs.getString("aminities"),rs.getString("status"),rs.getString("priority"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	
}
