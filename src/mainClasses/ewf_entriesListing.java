package mainClasses;

import beans.ewf_entries;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;

import dbase.sqlcon.ConnectionHelper;
import java.util.ArrayList;
import java.util.List;
public class ewf_entriesListing 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String ewf_id;
	
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	
	public String getEwf_id() {
		return (this.ewf_id);
	}
	public void setEwf_id(String ewf_id) {
		this.ewf_id = ewf_id;
	}
	
	public List getewf_entries()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT ewf_id,sub_head_id,entry_date,narration,enter_by,minor_head_id,major_head_id,head_account_id,amount,credit_or_debit,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM ewf_entries"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new ewf_entries(rs.getString("ewf_id"),rs.getString("sub_head_id"),rs.getString("entry_date"),rs.getString("narration"),rs.getString("enter_by"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("amount"),rs.getString("credit_or_debit"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getewf_entriesBasedOnDates(String fromdate,String todate)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT ewf_id,sub_head_id,entry_date,narration,enter_by,minor_head_id,major_head_id,head_account_id,amount,credit_or_debit,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM ewf_entries where entry_date >='"+fromdate+"' and entry_date <= '"+todate+"'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new ewf_entries(rs.getString("ewf_id"),rs.getString("sub_head_id"),rs.getString("entry_date"),rs.getString("narration"),rs.getString("enter_by"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("amount"),rs.getString("credit_or_debit"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	
	public List getOpeningBalRecordsForCurrentFinYear()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT ewf_id,sub_head_id,entry_date,narration,enter_by,minor_head_id,major_head_id,head_account_id,amount,credit_or_debit,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM ewf_entries where extra2 = 'openingbal'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		list.add(new ewf_entries(rs.getString("ewf_id"),rs.getString("sub_head_id"),rs.getString("entry_date"),rs.getString("narration"),rs.getString("enter_by"),rs.getString("minor_head_id"),rs.getString("major_head_id"),rs.getString("head_account_id"),rs.getString("amount"),rs.getString("credit_or_debit"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public String getCurrentFinYearOpeningBalBasedOnSubHead(String sub_head_id)
	{
		String openingBal = "";
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT amount FROM ewf_entries where sub_head_id = '"+sub_head_id+"' and extra2 = 'openingbal'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		openingBal = rs.getString("amount");

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return openingBal;
	}
	
	public String getSumOfDebitsBasedOnSubHead(String sub_head_id,String fromdate,String todate)
	{
	String sumOfDebits = "0";
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT sum(amount) as amount FROM ewf_entries where sub_head_id = '"+sub_head_id+"' and entry_date >='"+fromdate+"' and entry_date <= '"+todate+"' and credit_or_debit = 'debit' and extra2 != 'openingbal'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next())
	{
		if(rs.getString("amount") != null && !rs.getString("amount").equals(""))
		{	
			sumOfDebits = rs.getString("amount");
		}
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return sumOfDebits;
	}
	
	public String getSumOfCreditsBasedOnSubHead(String sub_head_id,String fromdate,String todate)
	{
	String sumOfCredits = "0";
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT sum(amount) as amount FROM ewf_entries where sub_head_id = '"+sub_head_id+"' and entry_date >='"+fromdate+"' and entry_date <= '"+todate+"' and credit_or_debit = 'credit' and extra2 != 'openingbal'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		if(rs.getString("amount") != null && !rs.getString("amount").equals(""))
		{
			sumOfCredits = rs.getString("amount");
		}
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return sumOfCredits;
	}

}
