package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.shopstock;
public class shopstockListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String sstId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setsstId(String sstId)
{
this.sstId = sstId;
}
public String getsstId(){
return(this.sstId);
}
public List getshopstock()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getgShopstockbyBefore(String month,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate < '"+month+"-%'and s.productid='"+productid+"'  and stockType='shop'"; 
String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate < '"+month+"-%'and s.productid='"+productid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getTotalTransferQuantityBasedHOA(String productId,String HOA)
{
String list = "0";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity  FROM shopstock where productId='"+productId+"' and extra1='"+HOA+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null && !rs.getString("quantity").equals("")){
 list=rs.getString("quantity");
	}

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalTransferQuantityBasedDate(String productId,String HOA)
{
String list = "0";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity  FROM shopstock where productId='"+productId+"' and extra1='"+HOA+"' and forwardedDate>'2014-12-06' and extra3!='STI99' group by productId";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null && !rs.getString("quantity").equals("")){
 list=rs.getString("quantity");
	}

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getgShopstockbymonth(String month,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate like '"+month+"-%'and s.productid='"+productid+"'  and stockType='shop'"; 
String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate like '"+month+"-%'and s.productid='"+productid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getgShopstockbyDate(String fromDate,String toDate,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate like '"+month+"-%'and s.productid='"+productid+"'  and stockType='shop'"; 
String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate between '"+fromDate+"' and '"+toDate+"'and s.productid='"+productid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getgShopstockbyENDDate(String fromDate,String toDate,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate like '"+month+"-%'and s.productid='"+productid+"'  and stockType='shop'"; 
String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate between '"+fromDate+"' and '"+toDate+"'and s.productid='"+productid+"'";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getgShopstockbyDay(String day,String productid)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
//String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate ='"+day+"'and s.productid='"+productid+"'  and stockType='shop'"; 
String selectquery="SELECT sum(quantity) as quantity FROM shopstock s where s.forwardedDate  like '"+day+"'and s.productid='"+productid+"'";
System.out.println(" outword from shopStock =========>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getgShopstockTransfer(String formdate,String toDate,String hoid)
{
ArrayList list =new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(hoid!=null && !hoid.equals("")){
		if(hoid.equals("4") ||hoid.equals("1") ){
			subQuery="and (extra1='4' || extra1='1') ";
		}else{
			subQuery="and extra1='"+hoid+"'";
		}
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId, productId, description, quantity, forwardedDate, stockType, extra1, extra2, extra3, extra4, extra5, emp_id FROM shopstock s where ( s.forwardedDate >='"+formdate+"'and  s.forwardedDate<='"+toDate+"' )"+subQuery+" group by extra3 order by forwardedDate ";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getgShopstockTransferBasedMajorHead(String formdate,String toDate,String hoid,String majorheadid)
{
ArrayList list =new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(hoid!=null && !hoid.equals("")){
		if(hoid.equals("4") ||hoid.equals("1") ){
			subQuery="and (s.extra1='4' || s.extra1='1') ";
		}else{
			subQuery="and s.extra1='"+hoid+"'";
		}
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId, s.productId, s.description, quantity, forwardedDate, stockType, s.extra1, s.extra2, s.extra3, s.extra4, s.extra5, s.emp_id FROM  shopstock s,products p where p.productId=s.productId and p.major_head_id='81' and  ( s.forwardedDate >='"+formdate+"'and  s.forwardedDate<='"+toDate+"' )"+subQuery+" group by s.extra3 order by forwardedDate";
/*System.out.println(selectquery);
*/ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getStockDailyIssuses(String formdate,String toDate,String hoid)
{
ArrayList list =new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId, productId, description, sum(quantity) as quantity, forwardedDate, stockType, extra1, extra2, extra3, extra4, extra5, emp_id FROM shopstock s where ( s.forwardedDate >='"+formdate+"'and  s.forwardedDate<='"+toDate+"' ) and extra1='"+hoid+"' group by productId";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getStockDailyIssusesBasedOnInvoice(String formdate,String toDate,String hoid)
{
ArrayList list =new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId, productId, description, quantity, forwardedDate, stockType, extra1, extra2, extra3, extra4, extra5, emp_id FROM shopstock s where ( s.forwardedDate >='"+formdate+"'and  s.forwardedDate<='"+toDate+"' ) and extra1='"+hoid+"' group by productId,extra3 order by forwardedDate";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMshopstock(String sstId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where  sstId = '"+sstId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getStockTransferIDS(String hoa)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String con="";
	if(hoa.equals("4")){
		con=("extra1='1' || extra1='4'");
	}else{
		con="extra1='"+hoa+"'";
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where "+con+" group by extra3"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getpresentShopStock(String productID)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
	String product="";
	if(productID!=null && !productID.equals("")){
		//System.out.println(productID);
		String[] pro=productID.split(" ");
		product=" and productId='"+pro[0]+"'";
	}
	
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,sum(quantity) as quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where stockType='shop'"+product+" group by productId"; 
//System.out.println("getpresentShopStock-----"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getProductTransferQty(String productID,String fromDate,String toDate)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,sum(quantity) as quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where stockType='shop' and productId='"+productID+"' and forwardedDate>='"+fromDate+"' and forwardedDate<='"+toDate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalProductTransferQty(String productID,String fromDate)
{
String list ="";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,sum(quantity) as quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where extra3!='STI00' and stockType='shop' and productId='"+productID+"' and forwardedDate >= '2014-12-07' and forwardedDate < '"+fromDate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("quantity");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public double getStockValuationReport(String productID,String fromDate,String toDate)
{
double list =0.00;
try {
 // Load the database driver
	String subQuery="";
	if(toDate!=null && !toDate.equals("")){
		subQuery="and forwardedDate<='"+toDate+"'";
	}
		
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,sum(quantity) as quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where stockType='shop' and productId='"+productID+"' and forwardedDate >'2014-12-06' and forwardedDate <'"+fromDate+"'"+subQuery; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=Double.parseDouble(rs.getString("quantity"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public double getStockValuationReport2(String productID,String fromDate,String toDate)
{
double list =0.00;
try {	
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,sum(quantity) as quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where productId='"+productID+"' and forwardedDate >='"+fromDate+"' and forwardedDate <'"+toDate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null)
 list=Double.parseDouble(rs.getString("quantity"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public double getDayTransferQty(String productID,String fromDate)
{
double list =0.00;
try {
 // Load the database driver
	String subQuery="";

		
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,sum(quantity) as quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where stockType='shop' and productId='"+productID+"' and forwardedDate like '"+fromDate+"%'"+subQuery; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null)
 list=Double.parseDouble(rs.getString("quantity"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public double getDayTransferQty2(String productID,String fromDate)
{
double list =0.00;
try {
 // Load the database driver	
		
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,sum(quantity) as quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where productId='"+productID+"' and forwardedDate = '"+fromDate+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null)
 list=Double.parseDouble(rs.getString("quantity"));
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getTransferReportBasedonStockTransaferInvoiceId(String TransInvoice)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where  extra3 = '"+TransInvoice+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getTransferReportBasedonStockTransaferInvoiceIdAndEmp(String TransInvoice,String empId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where  extra3 = '"+TransInvoice+"' and emp_id='"+empId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getDate(String id)
{
String list = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT forwardedDate FROM shopstock  where extra3='"+id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("forwardedDate");
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public boolean validProductOfSecondOpeningBalance(String productid)
{
boolean list =false;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM shopstock where productid='"+productid+"' and extra3='STI99'  and extra1='3'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=true;
 }
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalReceivedQuantityBasedHOAOfSecondOpenBal(String productId,String HOA)
{
String list = "0";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM shopstock where productId ='"+productId+"'and extra1='"+HOA+"' and extra3='STI99' and forwardedDate>'2014-12-06 23:59:59' group by productId";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null && !rs.getString("quantity").equals("")){
 list=rs.getString("quantity");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getTotalReceivedQuantityEntryHOA(String productId,String HOA)
{
String list = "0";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sum(quantity) as quantity FROM shopstock where productId ='"+productId+"'and extra1='"+HOA+"' and extra3!='STI99' and forwardedDate>'2014-12-06 23:59:59' group by productId";
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("quantity")!=null && !rs.getString("quantity").equals("")){
 list=rs.getString("quantity");
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getgShopstockTransferBased(String formdate,String toDate,String hoid)
{
ArrayList list =new ArrayList();
try {
 // Load the database driver
	String subQuery="";
	if(hoid!=null && !hoid.equals("")){
		if(hoid.equals("4") ||hoid.equals("1") ){
			subQuery="and (extra1='4' || extra1='1') ";
		}else{
			subQuery="and extra1='"+hoid+"'";
		}
	}
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId, productId, description, quantity, forwardedDate, stockType, extra1, extra2, extra3, extra4, extra5, emp_id FROM shopstock s where ( s.forwardedDate >='"+formdate+"'and  s.forwardedDate<='"+toDate+"' )"+subQuery+" group by extra3 order by forwardedDate ";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	 list.add(new shopstock(rs.getString("sstId"),rs.getString("productId"),rs.getString("description"),rs.getString("quantity"),rs.getString("forwardedDate"),rs.getString("stockType"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("emp_id")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getHeadOfAccount(String TransInvoice)
{
String  hoa = "";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT sstId,productId,description,quantity,forwardedDate,stockType,extra1,extra2,extra3,extra4,extra5,emp_id FROM shopstock where  extra3 = '"+TransInvoice+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	hoa=rs.getString("extra1");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return hoa;
}

public List<shopstock> getShopStkListBsdOnDatesAndIds(String fromdate,String todate,String prodid,String hoa,String condition)	{
	String selectQuery = "";
	ResultSet rs = null;
	ArrayList<shopstock> stocklist = new ArrayList<shopstock>();
	if (!prodid.trim().equals("")) {
		if(!condition.trim().equals(""))	{
			selectQuery = "select productId,sum(quantity) as quantity,forwardedDate,stockType from shopstock where extra1= "+hoa+" and forwardedDate >= '"+fromdate+"' and forwardedDate < '"+todate+"' and productid = "+prodid+" ";
		}
		else	{
			selectQuery = "select productId,sum(quantity) as quantity,forwardedDate,stockType from shopstock where extra1= "+hoa+" and forwardedDate >= '"+fromdate+"' and forwardedDate <= '"+todate+"' and productid = "+prodid+" ";
		}
	}
	else if (prodid.trim().equals(""))	{
		if (!condition.trim().equals(""))	{
			selectQuery = "select productId,sum(quantity) as quantity,forwardedDate,stockType from shopstock where extra1= "+hoa+" and forwardedDate >= '"+fromdate+"' and forwardedDate < '"+todate+"' group by productId order by forwardeddate,productid";
		}
		else	{
			selectQuery = "select productId,sum(quantity) as quantity,forwardedDate,stockType from shopstock where extra1= "+hoa+" and forwardedDate >= '"+fromdate+"' and forwardedDate <= '"+todate+"' group by productId order by forwardeddate,productid";
		}
	}
	try	{
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		rs = s.executeQuery(selectQuery);
		System.out.println("get from shopstk-----"+selectQuery);
		while(rs.next())	{
			shopstock stock = new shopstock();
			stock.setproductId(rs.getString("productId"));
			stock.setquantity(rs.getString("quantity"));
			stock.setforwardedDate(rs.getString("forwardedDate"));
			stock.setstockType(rs.getString("stockType"));
			
			stocklist.add(stock);
		}
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	finally	{
		try {
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return stocklist;
}

}