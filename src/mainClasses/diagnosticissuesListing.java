package mainClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.diagnosticissues;
import beans.doctordetails;
import beans.patientsentry;
import dbase.sqlcon.ConnectionHelper;

public class diagnosticissuesListing {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}

	private String documentNo;
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	 public void setdocumentNo(String documentNo)
	{
	this.documentNo = documentNo;
	}
	public String getdocumentNo(){
	return(this.documentNo);
	}
	public List gettestissues()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,date,appointmnetId,testName,amount,extra1,extra2,extra3,extra4,extra5 FROM diagnosticissues "; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public List gettestissues(String formdate,String todate)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,date,appointmnetId,testName,amount,extra1,extra2,extra3,extra4,extra5 FROM diagnosticissues where date between '"+formdate+"' and '"+todate+"' group by appointmnetId"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public List getDailytestissues(String formdate,String todate)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,date,appointmnetId,testName,sum(amount) as quantity,extra1,extra2,extra3,extra4,extra5 FROM diagnosticissues where date between '"+formdate+"' and '"+todate+"' group by testName"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	/*public List getDailytestissuesBasedonDotor(String formdate,String todate,String docotrid)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,m.date,m.appointmnetId,testName,sum(amount) as quantity,m.extra1,p.productName as extra2,p.purchaseAmmount as extra3,p.balanceQuantityGodwan as extra4,m.extra5 FROM medicineissues m,appointments a,products p where a.date between '"+formdate+"' and '"+todate+"' and m.appointmnetId=a.appointmnetId and p.productId=tabletName and p.major_head_id='60' and a.doctorId='"+docotrid+"' group by tabletName"; 
	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new medicineissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("tabletName"),rs.getString("quantity"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}*/
	public List getDoctors(String formdate,String todate)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,date,appointmnetId,testtName,amount,extra1,extra2,extra3,extra4,extra5 FROM diagnosticissues where date between '"+formdate+"' and '"+todate+"' group by appointmnetId"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public List getMtestissues(String documentNo)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,date,appointmnetId,testName,amount,extra1,extra2,extra3,extra4,extra5 FROM diagnosticissues where  documentNo = '"+documentNo+"'"; 
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public List getMtestissuesBasedOnAppoitment(String apptid)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,date,appointmnetId,testName,amount,extra1,extra2,extra3,extra4,extra5 FROM diagnosticissues where  appointmnetId = '"+apptid+"'"; 
	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public List<diagnosticissues> getPaidFreeDiagnosticBetweenDates(String formdate,String todate,String paidFree,String doctorId)
	{
		List<diagnosticissues> list = new ArrayList<diagnosticissues>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT d.documentNo,d.date,d.appointmnetId,d.testName,d.amount,d.extra1,d.extra2,d.extra3,d.extra4,d.extra5 FROM diagnosticissues d,appointments a where a.appointmnetId=d.appointmnetId and a.doctorId='"+doctorId+"' and d.date between '"+formdate+"' and '"+todate+"' and d.extra4='"+paidFree+"'"; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
			 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));
		
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List<diagnosticissues> getPaidFreeDiagnosticBasedOnPatient(String formdate,String todate,String paidFree,String doctorId,String patientId)
	{
		List<diagnosticissues> list = new ArrayList<diagnosticissues>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT d.documentNo,d.date,d.appointmnetId,d.testName,d.amount,d.extra1,d.extra2,d.extra3,d.extra4,d.extra5 FROM diagnosticissues d,appointments a where a.appointmnetId=d.appointmnetId and a.patientId='"+patientId+"' and a.doctorId='"+doctorId+"' and d.date between '"+formdate+"' and '"+todate+"' and d.extra4='"+paidFree+"'"; 
			System.out.println("===="+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
			 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));
		
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List<doctordetails> getDoctorIdBetweenDates(String formdate,String todate,String paidFree)
	{
		List<doctordetails> list = new ArrayList<doctordetails>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT dd.doctorName,dd.doctorId  FROM appointments a,diagnosticissues d,doctordetails dd where a.appointmnetId=d.appointmnetId and dd.doctorId=a.doctorId  and d.date between '"+formdate+"' and '"+todate+"' and d.extra4='"+paidFree+"' group by a.doctorId"; 
			System.out.println("===="+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				doctordetails dd=new doctordetails();
				dd.setdoctorId(rs.getString("dd.doctorId"));
				dd.setdoctorName(rs.getString("dd.doctorName"));
				list.add(dd);
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List<patientsentry> getPatientIdBasedOnDoctorId(String formdate,String todate,String paidFree,String doctorId)
	{
		List<patientsentry> list = new ArrayList<patientsentry>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="SELECT pe.patientId,pe.patientName FROM appointments a,patientsentry pe,diagnosticissues d,doctordetails dd where a.appointmnetId=d.appointmnetId and dd.doctorId=a.doctorId and a.patientId=pe.patientId and d.date between '"+formdate+"' and '"+todate+"' and d.extra4='"+paidFree+"' and dd.doctorId='"+doctorId+"'group by pe.patientId"; 
			//System.out.println("===="+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next())
			{
				patientsentry pe=new patientsentry();
				pe.setpatientId(rs.getString("pe.patientId"));
				pe.setpatientName(rs.getString("pe.patientName"));
				list.add(pe);
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		return list;
	}
	public List getMamountBasedOnAppoitment(String apptid)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,date,appointmnetId,testName,amount,extra1,extra2,sum(extra3) as extra3,extra4,extra5 FROM diagnosticissues where  appointmnetId = '"+apptid+"'"; 
	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public List getMtestissueBasedOnAppoitment(String apptid)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT documentNo,date,appointmnetId,testName,amount,extra1,extra2,extra3,extra4,extra5 FROM diagnosticissues where  appointmnetId = '"+apptid+"'"; 
	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new diagnosticissues(rs.getString("documentNo"),rs.getString("date"),rs.getString("appointmnetId"),rs.getString("testName"),rs.getString("amount"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	
	public List<diagnosticissues> getDailyDaignosticIssueBasedOnDoctor(String formdate,String todate,String doctorId)
	{
		List<diagnosticissues> list = new ArrayList<diagnosticissues>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="select d.testName,count(d.documentNo),d.amount,sum(d.extra3),d.extra2 from diagnosticissues d,appointments a where d.appointmnetId=a.appointmnetId and d.date>'"+formdate+"' and d.date<'"+todate+"' and a.doctorId='"+doctorId+"' group by testName,extra2"; 
			System.out.println("===="+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				diagnosticissues d=new diagnosticissues();
				d.setTestName(rs.getString("d.testName"));
				d.setDocumentNo(rs.getString("count(d.documentNo)"));
				d.setAmount(rs.getString("d.amount"));
				d.setExtra3(rs.getString("sum(d.extra3)"));
				d.setExtra2(rs.getString("d.extra2"));
				list.add(d);
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception in  diagnosticissuesListing.getDailyDaignosticIssueBasedOnDoctor() ;"+e);
		}
		return list;
	}
	public List<diagnosticissues> getDailyDaignosticIssueNameBasedOnDoctor(String formdate,String todate,String doctorId)
	{
		List<diagnosticissues> list = new ArrayList<diagnosticissues>();
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="select d.testName,count(d.documentNo),d.amount,sum(d.extra3),d.extra2,s.name as extra5 from diagnosticissues d,appointments a,subhead s where d.appointmnetId=a.appointmnetId and s.sub_head_id=testName and d.date>'"+formdate+"' and d.date<'"+todate+"' and a.doctorId='"+doctorId+"' group by testName,extra2"; 
			System.out.println("===="+selectquery);
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				diagnosticissues d=new diagnosticissues();
				d.setTestName(rs.getString("d.testName"));
				d.setDocumentNo(rs.getString("count(d.documentNo)"));
				d.setAmount(rs.getString("d.amount"));
				d.setExtra3(rs.getString("sum(d.extra3)"));
				d.setExtra2(rs.getString("d.extra2"));
				/*d.setExtra5(rs.getString("d.extra5"));*/
				d.setExtra5(rs.getString("extra5"));
				list.add(d);
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception in  diagnosticissuesListing.getDailyDaignosticIssueBasedOnDoctor() ;"+e);
		}
		return list;
	}
	public String getTotalAppointment(String formdate,String todate)
	{
		String  totalAppointment = "0";
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="select count(appointmnetId) from diagnosticissues d where  d.date>'"+formdate+"' and d.date<'"+todate+"' "; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				totalAppointment=rs.getString("count(appointmnetId)");
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception in diagnosticissuesListing.getTotalAppointment() ;"+e);
		}
		return totalAppointment;
	}
	public String getTotalTest(String formdate,String todate)
	{
		String  totalDocumentNo = "0";
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="select count(documentNo) from diagnosticissues d where d.date>'"+formdate+"' and d.date<'"+todate+"' "; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				totalDocumentNo=rs.getString("count(documentNo)");
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception in diagnosticissuesListing.getTotalTest() ;"+e);
		}
		return totalDocumentNo;
	}
	public String getTotalAmount(String formdate,String todate)
	{
		String  totalAmount = "0";
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			s=c.createStatement();
			String selectquery="select sum(d.extra3) from diagnosticissues d where d.date>'"+formdate+"' and d.date<'"+todate+"' "; 
			ResultSet rs = s.executeQuery(selectquery);
			while(rs.next()){
				totalAmount=rs.getString("sum(d.extra3)");
			}
			s.close();
			rs.close();
			c.close();
		}
		catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception in diagnosticissuesListing.getTotalAmount() ;"+e);
		}
		return totalAmount;
	}
	
	public List<diagnosticissues> getDiagnosticTestIssuesBasedOnDates(String fromDate,String toDate){
		List<diagnosticissues> diagnosticIssuesList=new ArrayList<diagnosticissues>();
		diagnosticissues diagnosticissuesBean=null;
		try{
			 c=ConnectionHelper.getConnection();
			 s=c.createStatement();
			 String selectQuery="select d.documentNo, d.date, d.appointmnetId, d.testName, d.amount, d.extra1, d.extra2, d.extra3, d.extra4, d.extra5,s.name,a.doctorId,a.patientid,doc.doctorName,p.patientName  from diagnosticissues d,subhead s,appointments a,doctordetails doc,patientsentry p where d.date >='"+fromDate+"'and d.date<='"+toDate+"' and d.testName=s.sub_head_id and a.appointmnetId=d.appointmnetId and a.doctorid=doc.doctorid and a.patientid=p.patientid order by d.appointmnetId;";
			 ResultSet resultSet=s.executeQuery(selectQuery);
			 while(resultSet.next()){
				 diagnosticissuesBean=new diagnosticissues();
				 diagnosticissuesBean.setDocumentNo(resultSet.getString("documentNo"));
				 diagnosticissuesBean.setAppointmnetId(resultSet.getString("appointmnetId"));
				 diagnosticissuesBean.setAmount(resultSet.getString("amount")); 
				 diagnosticissuesBean.setExtra3(resultSet.getString("extra3"));
				 diagnosticissuesBean.setExtra2(resultSet.getString("extra2"));
				 diagnosticissuesBean.setTestName(resultSet.getString("name"));
				 diagnosticissuesBean.setDoctorName( resultSet.getString("doctorName"));
				 diagnosticissuesBean.setPatientName(resultSet.getString("patientName"));
				 diagnosticIssuesList.add(diagnosticissuesBean);
			 }
			 s.close();
			 resultSet.close();
			 c.close();
		   }catch(Exception e){
			e.printStackTrace();
		   }
		return diagnosticIssuesList;
	}
	public List<diagnosticissues> getReceiptDoctorTestWise(String fromDate,String toDate,String sortBy){
		
		String sortingOption="";
		if(sortBy.equals("Doctor wise")){
			sortingOption=" order by a.doctorid";
		}else if(sortBy.equals("Test wise")){
		    sortingOption=" order by d.testname";	
		}else{
			sortingOption=" order by d.documentNo";
		}
		
		String selectQuery="select d.documentNo, d.date, d.appointmnetId, d.testName, d.amount, d.extra1, d.extra2, d.extra3, d.extra4, d.extra5,s.name,a.doctorId,a.patientid,doc.doctorName,p.patientName  from diagnosticissues d,subhead s,appointments a,doctordetails doc,patientsentry p where d.date >='"+fromDate+"'and d.date<='"+toDate+"' and d.testName=s.sub_head_id and a.appointmnetId=d.appointmnetId and a.doctorid=doc.doctorid and a.patientid=p.patientid"+sortingOption;
		System.out.println("select query::::::"+selectQuery);
		List<diagnosticissues> diagnosticIssuesList=new ArrayList<diagnosticissues>();
		diagnosticissues diagnosticissuesBean=null;
		try{
			 c=ConnectionHelper.getConnection();
			 s=c.createStatement();
			 
			 ResultSet resultSet=s.executeQuery(selectQuery);
			 while(resultSet.next()){
				 diagnosticissuesBean=new diagnosticissues();
				 diagnosticissuesBean.setDocumentNo(resultSet.getString("documentNo"));
				 diagnosticissuesBean.setAppointmnetId(resultSet.getString("appointmnetId"));
				 diagnosticissuesBean.setAmount(resultSet.getString("amount")); 
				 diagnosticissuesBean.setExtra3(resultSet.getString("extra3"));
				 diagnosticissuesBean.setExtra2(resultSet.getString("extra2"));
				 diagnosticissuesBean.setTestName(resultSet.getString("name"));
				 diagnosticissuesBean.setDoctorName( resultSet.getString("doctorName"));
				 diagnosticissuesBean.setPatientName(resultSet.getString("patientName"));
				 diagnosticissuesBean.setPatientId(resultSet.getString("patientid"));
				 diagnosticissuesBean.setDate(resultSet.getString("date"));
				 diagnosticissuesBean.setExtra4(resultSet.getString("extra4"));
				 diagnosticissuesBean.setExtra5(resultSet.getString("extra5"));
				 diagnosticIssuesList.add(diagnosticissuesBean);
			 }
			 s.close();
			 resultSet.close();
			 c.close();
		   }catch(Exception e){
			e.printStackTrace();
		   }
		return diagnosticIssuesList;
	}
	
 public List<diagnosticissues> getDayWiseTests(String fromDate,String toDate){
		
		String selectQuery="select d.testname,count(*)as numberoftests,sum(d.extra2)sansthanamount ,sum(d.extra3)as patientamount,s.name from diagnosticissues d,subhead s,appointments a,doctordetails doc,patientsentry p  where d.date>='"+fromDate+"' and d.date<='"+toDate+"' and d.testname=s.sub_head_id and a.appointmnetId=d.appointmnetId and a.doctorid=doc.doctorid and a.patientid=p.patientid group by d.testname having count(*)>=1;";
		
		
		List<diagnosticissues> diagnosticIssuesList=new ArrayList<diagnosticissues>();
		diagnosticissues diagnosticissuesBean=null;
		try{
			 c=ConnectionHelper.getConnection();
			 s=c.createStatement();
			 ResultSet resultSet=s.executeQuery(selectQuery);
			 while(resultSet.next()){
				 diagnosticissuesBean=new diagnosticissues();
				 diagnosticissuesBean.setExtra3(resultSet.getString("patientamount"));
				 diagnosticissuesBean.setExtra2(resultSet.getString("sansthanamount"));
				 diagnosticissuesBean.setTestName(resultSet.getString("name"));
				 diagnosticissuesBean.setNumberOfTests(resultSet.getString("numberoftests"));
				 diagnosticIssuesList.add(diagnosticissuesBean);
			 }
			 s.close();
			 resultSet.close();
			 c.close();
		   }catch(Exception e){
			e.printStackTrace();
		   }
		return diagnosticIssuesList;
	}
 	
}
