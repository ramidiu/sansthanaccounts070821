package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import dbase.sqlcon.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.indent;
import beans.indentapprovals;
public class indentapprovalsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String approve_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setapprove_id(String approve_id)
{
this.approve_id = approve_id;
}
public String getapprove_id(){
return(this.approve_id);
}
public List getindentapprovals()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indentapprovals(rs.getString("approve_id"),rs.getString("admin_id"),rs.getString("description"),rs.getString("admin_status"),rs.getString("approved_date"),rs.getString("indentinvoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMindentapprovals(String approve_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  approve_id = '"+approve_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indentapprovals(rs.getString("approve_id"),rs.getString("admin_id"),rs.getString("description"),rs.getString("admin_status"),rs.getString("approved_date"),rs.getString("indentinvoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<beans.indent> getIndentDetails(String indentinvoice_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
/*String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id = '"+indentinvoice_id+"' group by admin_id";*/
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id = '"+indentinvoice_id+"' and admin_status !='Postpone' group by admin_id order by approved_date";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indentapprovals(rs.getString("approve_id"),rs.getString("admin_id"),rs.getString("description"),rs.getString("admin_status"),rs.getString("approved_date"),rs.getString("indentinvoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<beans.indent> getIndentDetailsApprovedByBoardMembersAndGsOrChairman(String indentinvoice_id,String mseId)
{
	//System.out.println("id and mseid...."+indentinvoice_id+",,,,,,,"+mseId);
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id in ('"+indentinvoice_id+"','"+mseId+"') and admin_status !='Postpone' group by admin_id order by approved_date desc";
System.out.println("list..gs....."+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indentapprovals(rs.getString("approve_id"),rs.getString("admin_id"),rs.getString("description"),rs.getString("admin_status"),rs.getString("approved_date"),rs.getString("indentinvoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<beans.indent> getIndentDetailsWithApprove(String indentinvoice_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id = '"+indentinvoice_id+"' and admin_status='approved' group by admin_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indentapprovals(rs.getString("approve_id"),rs.getString("admin_id"),rs.getString("description"),rs.getString("admin_status"),rs.getString("approved_date"),rs.getString("indentinvoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<beans.indent> getIndentDetails(String indentinvoice_id,String admin_status)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id = '"+indentinvoice_id+"' and admin_status='"+admin_status+"' and extra3 = '' group by admin_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indentapprovals(rs.getString("approve_id"),rs.getString("admin_id"),rs.getString("description"),rs.getString("admin_status"),rs.getString("approved_date"),rs.getString("indentinvoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<beans.indent> getIndentApprovedByAdmin(String indentinvoice_id,String admin_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id = '"+indentinvoice_id+"' and admin_id='"+admin_id+"'"; 
//System.out.println("!@#$56==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indentapprovals(rs.getString("approve_id"),rs.getString("admin_id"),rs.getString("description"),rs.getString("admin_status"),rs.getString("approved_date"),rs.getString("indentinvoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List<beans.indent> checkPendingIndents(String admin_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT * FROM indent where indentinvoice_id not in(select indentinvoice_id from indentapprovals where admin_id='"+admin_id+"') group by indentinvoice_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new indent(rs.getString("indent_id"),rs.getString("date"),rs.getString("description"),rs.getString("status"),rs.getString("indentinvoice_id"),rs.getString("requirement"),rs.getString("require_date"),rs.getString("head_account_id"),rs.getString("product_id"),rs.getString("tablet_id"),rs.getString("quantity"),rs.getString("vendor_id"),rs.getString("emp_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getApprovedMemNames(String indentinvoice_id)
{
	String boardMems ="";
	mainClasses.superadminListing SADL=new mainClasses.superadminListing();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id = '"+indentinvoice_id+"' and admin_status='approved' group by admin_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(boardMems.equals("")){
		boardMems=SADL.getSuperadminname(rs.getString("admin_id"));;
	}else{
		boardMems=boardMems+" , "+SADL.getSuperadminname(rs.getString("admin_id"));
	}
}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return boardMems;
}
public boolean getBillWithApprove(String indentinvoice_id)
{
boolean list = false;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id = '"+indentinvoice_id+"' "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list = true;

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public String getApprovedMemberId(String indentinvoice_id)
{
	String boardMems ="";
	mainClasses.superadminListing SADL=new mainClasses.superadminListing();
	try {
		// Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  indentinvoice_id = '"+indentinvoice_id+"' and admin_status='approved'"; 
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
			if(boardMems.equals("")){
				boardMems=rs.getString("admin_id");;
			}else{
				boardMems=boardMems+","+rs.getString("admin_id");
			}
		}
		//System.out.println("from database"+boardMems);
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	return boardMems;
}
public List<indentapprovals> getIndentApprovalsList(String expinv_id)
{
	ResultSet rs = null;
	List<indentapprovals> list = null;
	try {
		// Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT iap.approve_id,iap.admin_id,iap.description,iap.admin_status,iap.approved_date,iap.indentinvoice_id,iap.extra1,iap.extra2,iap.extra3 FROM indentapprovals iap where indentinvoice_id = ( select ind.indentinvoice_id from indent ind where ind.extra4 = (select gs.extra9 from godwanstock gs where gs.extra21 = '"+expinv_id+"' group by gs.extra21) group by ind.extra4) and iap.admin_status='approved' and iap.extra1 != 'billapprove'"; 
		rs = s.executeQuery(selectquery);
		list = new ArrayList<indentapprovals>();
		while(rs.next()){
			indentapprovals ip = new indentapprovals();
			ip.setadmin_id(rs.getString("iap.admin_id"));
			ip.setapproved_date(rs.getString("iap.approved_date"));
			ip.setindentinvoice_id(rs.getString("iap.indentinvoice_id"));
			ip.setextra3(rs.getString("iap.extra3"));
			list.add(ip);
		}
		//System.out.println("from database"+boardMems);
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
	finally	{
		try {
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return list;
}

public List<beans.indentapprovals> getDcApprovedByAdmin(String dcNo,String admin_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT approve_id,admin_id,description,admin_status,approved_date,indentinvoice_id,extra1,extra2,extra3 FROM indentapprovals where  extra2 = '"+dcNo+"' and admin_id='"+admin_id+"'"; 
//System.out.println("getDcApprovedByAdmin==>"+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new indentapprovals(rs.getString("approve_id"),rs.getString("admin_id"),rs.getString("description"),rs.getString("admin_status"),rs.getString("approved_date"),rs.getString("indentinvoice_id"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List<indentapprovals> getBillApprovalsBsdOnUniqueNo(String indentInvoiceId)	{
	ArrayList<indentapprovals> list = new ArrayList<indentapprovals>();
	ResultSet rs = null;
	try	{
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="select i.approve_id,i.admin_id,i.admin_status,i.approved_date,i.indentinvoice_id,i.extra1,s.admin_name,s.extra4 from indentapprovals i,superadmin s where indentinvoice_id = '"+indentInvoiceId+"' and (i.admin_id = s.admin_id);"; 
		rs = s.executeQuery(selectquery);
		while(rs.next())	{
			indentapprovals ip = new indentapprovals();
			ip.setapprove_id(rs.getString("i.approve_id"));
			ip.setadmin_id(rs.getString("i.admin_id"));
			ip.setadmin_status(rs.getString("i.admin_status"));
			ip.setapproved_date(rs.getString("i.approved_date"));
			ip.setindentinvoice_id(rs.getString("i.indentinvoice_id"));
			ip.setextra1(rs.getString("i.extra1"));
			ip.setextra2(rs.getString("s.admin_name"));
			ip.setextra3(rs.getString("s.extra4"));
			
			list.add(ip);
		}
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	catch(Exception e)	{
		e.printStackTrace();
	}
	finally	{
		try {
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
return list;
}
}