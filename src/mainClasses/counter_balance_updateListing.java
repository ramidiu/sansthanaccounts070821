package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import dbase.sqlcon.SainivasConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.counter_balance_update;
public class counter_balance_updateListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String c_b_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setc_b_id(String c_b_id)
{
this.c_b_id = c_b_id;
}
public String getc_b_id(){
return(this.c_b_id);
}
public List getcounter_balance_update()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT c_b_id,booking_id,counter_code,shift_code,date,login_id,booking_amount,opening_balance,counter_balance,closing_balance,booked_date,counter_ex1,counter_ex2,counter_ex3,counter_ex4,counter_ex5,counter_ex6,counter_ex7 FROM counter_balance_update "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new counter_balance_update(rs.getString("c_b_id"),rs.getString("booking_id"),rs.getString("counter_code"),rs.getString("shift_code"),rs.getString("date"),rs.getString("login_id"),rs.getString("booking_amount"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("booked_date"),rs.getString("counter_ex1"),rs.getString("counter_ex2"),rs.getString("counter_ex3"),rs.getString("counter_ex4"),rs.getString("counter_ex5"),rs.getString("counter_ex6"),rs.getString("counter_ex7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMcounter_balance_update(String c_b_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT c_b_id,booking_id,counter_code,shift_code,date,login_id,booking_amount,opening_balance,counter_balance,closing_balance,booked_date,counter_ex1,counter_ex2,counter_ex3,counter_ex4,counter_ex5,counter_ex6,counter_ex7 FROM counter_balance_update where  c_b_id = '"+c_b_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new counter_balance_update(rs.getString("c_b_id"),rs.getString("booking_id"),rs.getString("counter_code"),rs.getString("shift_code"),rs.getString("date"),rs.getString("login_id"),rs.getString("booking_amount"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("booked_date"),rs.getString("counter_ex1"),rs.getString("counter_ex2"),rs.getString("counter_ex3"),rs.getString("counter_ex4"),rs.getString("counter_ex5"),rs.getString("counter_ex6"),rs.getString("counter_ex7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getSainivasCounter_Booking(String login_id,String todate,String roomtype)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT b.extra_bed_charges as c_b_id, c.booking_id, counter_code, shift_code, date, login_id, booking_amount, opening_balance, counter_balance, closing_balance, booked_date,b.total_amount as counter_ex1,counter as counter_ex2,downs as counter_ex3, shift as counter_ex4,service_charges as  counter_ex5, extra_bed_charges as counter_ex6, br.roomtype_id as counter_ex7 FROM counter_balance_update c,bookingrooms b,booking_roomsdetails br where b.booking_id=c.booking_id and br.booking_id=b.booking_id and br.roomtype_id='"+roomtype+"' and b.paymentstauts='success' and   login_id='"+login_id+"' and date like '"+todate+"%' group by c.booking_id"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	list.add(new counter_balance_update(rs.getString("c_b_id"),rs.getString("booking_id"),rs.getString("counter_code"),rs.getString("shift_code"),rs.getString("date"),rs.getString("login_id"),rs.getString("booking_amount"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("booked_date"),rs.getString("counter_ex1"),rs.getString("counter_ex2"),rs.getString("counter_ex3"),rs.getString("counter_ex4"),rs.getString("counter_ex5"),rs.getString("counter_ex6"),rs.getString("counter_ex7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getLoginOnBookingID(String booking_id)
{
String list = "";
try {
 // Load the database driver
//c = ConnectionHelper.getConnection();
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT login_id FROM counter_balance_update where  booking_id = '"+booking_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=rs.getString("login_id");

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public double getInvoiceTotAmt(String invoiceId)
{
	double list = 0.00;
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT booking_amount FROM counter_balance_update where  booking_id = '"+invoiceId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list=Double.parseDouble(rs.getString("booking_amount"));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



public List getdonationMoney(String fromdate,String todate,String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT c_b_id,booking_id,counter_code,shift_code,date,login_id,booking_amount,opening_balance,counter_balance,closing_balance,booked_date,counter_ex1,counter_ex2,counter_ex3,counter_ex4,counter_ex5,counter_ex6,counter_ex7 FROM counter_balance_update where  date between '"+fromdate+"' and '"+todate+"' and login_id='"+login_id+"'"; 
//System.out.println("\n getdonationMoney =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new counter_balance_update(rs.getString("c_b_id"),rs.getString("booking_id"),rs.getString("counter_code"),rs.getString("shift_code"),rs.getString("date"),rs.getString("login_id"),rs.getString("booking_amount"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("booked_date"),rs.getString("counter_ex1"),rs.getString("counter_ex2"),rs.getString("counter_ex3"),rs.getString("counter_ex4"),rs.getString("counter_ex5"),rs.getString("counter_ex6"),rs.getString("counter_ex7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getdonationMoney1(String fromdate,String todate,String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
//c = ConnectionHelper.getConnection();
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT c_b_id,booking_id,counter_code,shift_code,date,login_id,booking_amount,opening_balance,counter_balance,closing_balance,booked_date,counter_ex1,counter_ex2,counter_ex3,counter_ex4,counter_ex5,counter_ex6,counter_ex7 FROM counter_balance_update where  date between '"+fromdate+"' and '"+todate+"' and login_id='"+login_id+"'"; 
//System.out.println("getdonationMoney =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new counter_balance_update(rs.getString("c_b_id"),rs.getString("booking_id"),rs.getString("counter_code"),rs.getString("shift_code"),rs.getString("date"),rs.getString("login_id"),rs.getString("booking_amount"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("booked_date"),rs.getString("counter_ex1"),rs.getString("counter_ex2"),rs.getString("counter_ex3"),rs.getString("counter_ex4"),rs.getString("counter_ex5"),rs.getString("counter_ex6"),rs.getString("counter_ex7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getdonationMoneySainivas(String fromdate,String todate,String login_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
//c = ConnectionHelper.getConnection();
c = SainivasConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT c_b_id,booking_id,counter_code,shift_code,date,login_id,booking_amount,opening_balance,counter_balance,closing_balance,booked_date,counter_ex1,counter_ex2,counter_ex3,counter_ex4,counter_ex5,counter_ex6,counter_ex7 FROM counter_balance_update where  date between '"+fromdate+"' and '"+todate+"' and login_id='"+login_id+"'"; 
//System.out.println("\n getdonationMoney =="+selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new counter_balance_update(rs.getString("c_b_id"),rs.getString("booking_id"),rs.getString("counter_code"),rs.getString("shift_code"),rs.getString("date"),rs.getString("login_id"),rs.getString("booking_amount"),rs.getString("opening_balance"),rs.getString("counter_balance"),rs.getString("closing_balance"),rs.getString("booked_date"),rs.getString("counter_ex1"),rs.getString("counter_ex2"),rs.getString("counter_ex3"),rs.getString("counter_ex4"),rs.getString("counter_ex5"),rs.getString("counter_ex6"),rs.getString("counter_ex7")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public String getBookingIDAmount(String booking_id)
{
String list = "00";
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT booking_amount FROM counter_balance_update where  booking_id = '"+booking_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
	if(rs.getString("booking_amount")!=null && !rs.getString("booking_amount").equals("")){
 list=rs.getString("booking_amount");
	}
}
	s.close();
	rs.close();
	c.close();
}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	
}
}