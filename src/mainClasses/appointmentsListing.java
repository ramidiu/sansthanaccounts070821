package mainClasses;

import dbase.sqlcon.ConnectionHelper;
import dbase.sqlcon.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.appointments;
public class appointmentsListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String appointmnetId;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;

 public void setappointmnetId(String appointmnetId)
{
this.appointmnetId = appointmnetId;
}
public String getappointmnetId(){
return(this.appointmnetId);
}
public List getappointments()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT appointmnetId,patientId,doctorId,purpose,noOfPersons,date,extra1,extra2,extra3,extra4,extra5 FROM appointments order by appointmnetId DESC"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getappointmentsBasedOnDoctor(String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(appointmnetId) as appointmnetId,patientId,doctorId,purpose,noOfPersons,date,extra1,extra2,extra3,extra4,extra5 FROM appointments where date>='"+fromdate+"' and date<='"+todate+"' group by doctorId"; 
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getDignosticsDoctorId(String fromdate,String todate)
{
	ArrayList list = new ArrayList();
	try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT count(a.appointmnetId) as appointmnetId,a.doctorId,d.date FROM appointments a,diagnosticissues d where a.appointmnetId=d.appointmnetId and d.date>='"+fromdate+"' and d.date<='"+todate+"'   group by a.doctorId"; 
		/*System.out.println(selectquery);*/
		ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
		appointments app=new appointments();
		app.setappointmnetId(rs.getString("appointmnetId"));
		app.setdoctorId(rs.getString("a.doctorId"));
		app.setdate(rs.getString("d.date"));
		list.add(app);
	}
		s.close();
		rs.close();
		c.close();
	}
	catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception in appointmentsListing.getDignosticsDoctorId();"+e);
	}
	return list;
}
public List getappointmentsBasedOnDoctors(String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT count(a.appointmnetId) as appointmnetId,a.patientId,a.doctorId,a.purpose,a.noOfPersons,a.date,a.extra1,a.extra2,a.extra3,a.extra4,a.extra5,d.date,d.appointmnetId,d.testName,d.amount,d.extra2 FROM appointments a,diagnosticissues d where a.appointmnetId=d.appointmnetId and d.date>='"+fromdate+"' and d.date<='"+todate+"' group by doctorId"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPatientsBasedOnDoctor(String fromdate,String todate,String doctorID)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT appointmnetId,patientId,doctorId,purpose,noOfPersons,date,extra1,extra2,extra3,extra4,extra5 FROM appointments where date>='"+fromdate+"' and date<='"+todate+"' and doctorId='"+doctorID+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPatientsBasedOnDoctors(String fromdate,String todate,String doctorID)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT a.appointmnetId,a.patientId,a.doctorId,a.purpose,a.noOfPersons,a.date,a.extra1,a.extra2,a.extra3,a.extra4,a.extra5,d.date,d.appointmnetId,d.testName,d.amount,d.extra2 FROM appointments a,diagnosticissues d where a.appointmnetId=d.appointmnetId and d.date>='"+fromdate+"' and d.date<='"+todate+"' and doctorId='"+doctorID+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getappointmentsMedicineNotIssued(String appointmnetId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
/*String selectquery="SELECT appointmnetId,p.patientName as extra2, s.patientId,doctorId,purpose,noOfPersons,date,s.extra1,s.extra3,s.extra4,s.extra5 FROM appointments s,patientsentry p  where (s.extra1='' and appointmnetId like '%"+appointmnetId+"%') and p.patientId=s.patientId  ";*/
String selectquery="SELECT appointmnetId,p.patientName as extra2, s.patientId,doctorId,purpose,noOfPersons,date,s.extra1,s.extra3,s.extra4,s.extra5 FROM appointments s,patientsentry p  where (s.extra1='' and (appointmnetId like '%"+appointmnetId+"%' || p.patientName like '%"+appointmnetId+"%')) and p.patientId=s.patientId  ";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getappointmentsMedicineNotIssuedNew(String appointmnetId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
/*String selectquery="SELECT appointmnetId,p.patientName as extra2, s.patientId,doctorId,purpose,noOfPersons,date,s.extra1,s.extra3,s.extra4,s.extra5 FROM appointments s,patientsentry p  where (s.extra1='' and appointmnetId like '%"+appointmnetId+"%') and p.patientId=s.patientId  ";*/
String selectquery="SELECT appointmnetId,p.patientName as extra2, s.patientId,doctorId,purpose,noOfPersons,date,s.extra1,s.extra3,s.extra4,s.extra5 FROM appointments s,patientsentry p  where (s.extra1='' and (appointmnetId like '%"+appointmnetId+"%' || p.patientName like '%"+appointmnetId+"%')) and s.extra2='' and p.patientId=s.patientId  ";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}

public List getappointmentsTestissuse(String appointmnetId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT appointmnetId,p.patientName as extra2, s.patientId,doctorId,purpose,noOfPersons,date,s.extra1,s.extra2,s.extra3,s.extra4,s.extra5 FROM appointments s,patientsentry p  where appointmnetId like '%"+appointmnetId+"%' and p.patientId=s.patientId  ";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getappointmentsMedicineNotIssued(String appointmnetId,String issuestatus)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT appointmnetId,p.patientName as extra2, s.patientId,CONCAT_WS(' ', d.doctorId, d.doctorName) AS doctorId,purpose,noOfPersons,date,p.phone as extra1,p.address as extra3,p.extra1 as extra4,p.extra3 as  extra5 FROM appointments s,patientsentry p,doctordetails d   where ( appointmnetId like '"+appointmnetId+"%') and p.patientId=s.patientId   and d.doctorId=s.doctorId";
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMappointments(String appointmnetId)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT appointmnetId,patientId,doctorId,purpose,noOfPersons,date,extra1,extra2,extra3,extra4,extra5 FROM appointments where  appointmnetId = '"+appointmnetId+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMappointmentsBetweenDates(String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT appointmnetId,patientId,doctorId,purpose,noOfPersons,date,extra1,extra2,extra3,extra4,extra5 FROM appointments where  date >='"+fromdate+"' and date<='"+todate+"' order by date"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new appointments(rs.getString("appointmnetId"),rs.getString("patientId"),rs.getString("doctorId"),rs.getString("purpose"),rs.getString("noOfPersons"),rs.getString("date"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
}