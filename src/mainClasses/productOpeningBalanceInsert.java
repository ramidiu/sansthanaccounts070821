package mainClasses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ProductOpeningBalance;
import beans.ProductOpeningBalanceService;
@WebServlet("/productOpeningBalanceInsert")
public class productOpeningBalanceInsert extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int totalCount=Integer.parseInt(request.getParameter("totalCount"));
		   String category=request.getParameter("category");
		   String subcategory=request.getParameter("subcategory");
		   String finacialyearDate=request.getParameter("finacialyear");
		   ProductOpeningBalanceService productOpeningBalanceService=new ProductOpeningBalanceService();
		   boolean result=productOpeningBalanceService.validateProductOpeningBalanceRecords(category, subcategory, finacialyearDate);
		   if(result==false) {
		   List<ProductOpeningBalance> productOpeningBalanceList=new ArrayList<ProductOpeningBalance>();
		   for(int i=0;i<totalCount;i++){
		    	String productId=request.getParameter("productId"+i);
		         String productName=request.getParameter("productName"+i);
		     	String headOfAccount=request.getParameter("headOfAccount"+i);
		     	String majorHeadId=request.getParameter("majorHeadId"+i);
		     	String minorHeadId=request.getParameter("minorHeadId"+i);
		     	String subHeadId=request.getParameter("subHeadId"+i);
		    	String finacialyear=request.getParameter("finacialyear"+i);
		    	double quantity=Double.parseDouble(request.getParameter("quantity"+i));
		       ProductOpeningBalance productOpeningBalance=new ProductOpeningBalance();	
		       productOpeningBalance.setProductId(productId);
		       productOpeningBalance.setProductName(productName);
		       productOpeningBalance.setHeadOfAccount(headOfAccount);
		       productOpeningBalance.setMajorHeadId(majorHeadId);
		       productOpeningBalance.setMinorHeadId(minorHeadId);
		       productOpeningBalance.setSubHeadId(subHeadId);
		       productOpeningBalance.setQuantity(quantity);
		       productOpeningBalance.setFinacilaYearDate(finacialyear);
		       productOpeningBalance.setCategory(category);
		       productOpeningBalance.setSubCategory(subcategory);
		       productOpeningBalanceList.add(productOpeningBalance);
		   } 
		   
		   
		   productOpeningBalanceService.saveProductOpeningBalance(productOpeningBalanceList);
		   
		   response.sendRedirect("admin/adminPannel.jsp?page=productOpeningBalanceList");
	}else {
		 response.sendRedirect("admin/adminPannel.jsp?page=productopeningbalancealert");
	}
	}
	 
}
