package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.stockrequest;
public class stockrequestListing 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String req_id;
ArrayList list = new ArrayList();
Connection c = null;
Statement s = null;
 public void setreq_id(String req_id)
{
this.req_id = req_id;
}
public String getreq_id(){
return(this.req_id);
}
public List getstockrequest()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT req_id,req_date,narration,product_id,quantity,emp_id,head_account_id,reqinv_id,status,req_type,extra1,extra2,extra3,extra4,extra5 FROM stockrequest "; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new stockrequest(rs.getString("req_id"),rs.getString("req_date"),rs.getString("narration"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("reqinv_id"),rs.getString("status"),rs.getString("req_type"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getMstockrequest(String req_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT req_id,req_date,narration,product_id,quantity,emp_id,head_account_id,reqinv_id,status,req_type,extra1,extra2,extra3,extra4,extra5 FROM stockrequest where  req_id = '"+req_id+"'"; 
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new stockrequest(rs.getString("req_id"),rs.getString("req_date"),rs.getString("narration"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("reqinv_id"),rs.getString("status"),rs.getString("req_type"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getRequestDetailsBasedOnID(String reqinv_id)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT s.req_id,s.req_date,s.narration,s.product_id,s.quantity,s.emp_id,s.head_account_id,s.reqinv_id,status,s.req_type,p.balanceQuantityGodwan  as extra1,p.productName as extra2,p.purchaseAmmount as extra3,p.units as extra4,s.extra5 FROM stockrequest s,products p where p.productId=s.product_id and p.head_account_id=s.head_account_id and  s.reqinv_id = '"+reqinv_id+"' group by req_id";
/*System.out.println(selectquery);*/
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new stockrequest(rs.getString("req_id"),rs.getString("req_date"),rs.getString("narration"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("reqinv_id"),rs.getString("status"),rs.getString("req_type"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getstockrequestBasedOnHOA(String hoid,String fromdate,String todate,String groupby)
{
ArrayList list = new ArrayList();
try {
	String date="";
	String hoa="";
	String type="";
	String groupbyquery="";
	if(!hoid.equals("0")){
		hoa=" where head_account_id='"+hoid+"'";
		if(fromdate!=null && todate!=null){
			date=" and req_date>='"+fromdate+"' and req_date<='"+todate+"'";
		}
	}else{
		if(fromdate!=null && todate!=null){
			date=" where req_date>='"+fromdate+"' and req_date<='"+todate+"'";
		}
	}
	if(hoid.equals("0")){
		if(fromdate!=null && todate!=null){
			type=" and req_type!='shopstockrequest' and status='pending'";
		}else{
			type=" where req_type!='shopstockrequest' and status='pending'";
		}
	}
	if(groupby.equals("KOT")){
			groupbyquery=" group by reqinv_id";
			if(fromdate!=null && todate!=null){
				type=" and req_type!='shopstockrequest' ";
			}else{
				type=" where  req_type!='shopstockrequest'";
			}
		
			}
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT req_id,req_date,narration,product_id,quantity,emp_id,head_account_id,reqinv_id,status,req_type,extra1,extra2,extra3,extra4,extra5 FROM stockrequest "+hoa+date+type+groupbyquery+" order by req_date desc"; 
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new stockrequest(rs.getString("req_id"),rs.getString("req_date"),rs.getString("narration"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("reqinv_id"),rs.getString("status"),rs.getString("req_type"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getPendingKots()
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT req_id,req_date,narration,product_id,quantity,emp_id,head_account_id,reqinv_id,status,req_type,extra1,extra2,extra3,extra4,extra5 FROM stockrequest where (head_account_id='4' ||  head_account_id='1') and status='pending' group by reqinv_id "; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new stockrequest(rs.getString("req_id"),rs.getString("req_date"),rs.getString("narration"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("reqinv_id"),rs.getString("status"),rs.getString("req_type"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
public List getProducedKots(String hoid,String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT req_id,req_date,narration,product_id,quantity,emp_id,head_account_id,reqinv_id,status,req_type,extra1,extra2,extra3,extra4,extra5 FROM stockrequest  where  reqinv_id in( select sstid from produceditems where produced_date>='"+fromdate+"' and produced_date<='"+todate+"' and head_account_id='"+hoid+"'  group by sstid) group by reqinv_id"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new stockrequest(rs.getString("req_id"),rs.getString("req_date"),rs.getString("narration"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("reqinv_id"),rs.getString("status"),rs.getString("req_type"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}


public List getProducedKotbetweenDates(String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT req_id,req_date,narration,product_id,sum(quantity) as quantity,emp_id,head_account_id,reqinv_id,status,req_type,extra1,extra2,extra3,extra4,extra5 FROM stockrequest  where req_date between '"+fromdate+"' and '"+todate+"' and status='issued' group by product_id"; 
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new stockrequest(rs.getString("req_id"),rs.getString("req_date"),rs.getString("narration"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("reqinv_id"),rs.getString("status"),rs.getString("req_type"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}
//this method is written by madhav to get the Status of stock approval by kitchen or not when request stock in stock request table
public String getStatus(String proId )
{
String list ="";
try {
//Load the database driver
	String selectquery="";
c = ConnectionHelper.getConnection();
s=c.createStatement();
	selectquery="SELECT status FROM stockrequest where  product_id = '"+proId+"'"; 
//System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
list=rs.getString("status");
}
}
catch(Exception e){
e.printStackTrace();
System.out.println("Exception is ;"+e);
}
return list;
}



public List getStockUsedBasedOnDate(String fromdate,String todate)
{
ArrayList list = new ArrayList();
try {
 // Load the database driver
c = ConnectionHelper.getConnection();
s=c.createStatement();
String selectquery="SELECT req_id,req_date,narration,product_id,quantity as quantity,emp_id,head_account_id,reqinv_id,status,req_type,extra1,extra2,extra3,extra4,extra5 FROM stockrequest  where req_date between '"+fromdate+"' and '"+todate+"' and status='issued' order by req_date"; 
System.out.println(selectquery);
ResultSet rs = s.executeQuery(selectquery);
while(rs.next()){
 list.add(new stockrequest(rs.getString("req_id"),rs.getString("req_date"),rs.getString("narration"),rs.getString("product_id"),rs.getString("quantity"),rs.getString("emp_id"),rs.getString("head_account_id"),rs.getString("reqinv_id"),rs.getString("status"),rs.getString("req_type"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5")));

}
s.close();
rs.close();
c.close();
}
catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}
return list;
}



}