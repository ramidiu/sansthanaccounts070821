package mainClasses;

import dbase.sqlcon.ConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.bankbalance;

public class bankbalanceListing 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String uniq_id;
	
	ArrayList list = new ArrayList();
	Connection c = null;
	Statement s = null;
	
	public String getUniq_id() {
		return uniq_id;
	}
	public void setUniq_id(String uniq_id) {
		this.uniq_id = uniq_id;
	}
	
	public List getbankbalance()
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type in ('Bank','Counter Cash','Petty Cash')"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getbankbalanceFinYear(String finYear)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type in ('Bank','Counter Cash','Petty Cash') and extra1='"+finYear+"'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	public List getbankbalanceFinYear2(String fromDate,String toDate,String hoa)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where createdDate>='"+fromDate+"' and createdDate<='"+toDate+"' and extra2='"+hoa+"'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getbankbalanceFinYearAndDept(String finYear,String dept)
	{
	ArrayList list = new ArrayList();
	String selectquery="";
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	if(dept!=null&&!dept.equals("")){
	 selectquery="SELECT b.uniq_id,b.bank_id,b.type,b.bank_bal,b.bank_flag,b.createdDate,b.createdBy,b.extra1,b.extra2,b.extra3,b.extra4,b.extra5,b.extra6,b.extra7,b.extra8,b.extra9,b.extra10 FROM bankbalance b,bankdetails bd where b.bank_id=bd.bank_id and type in ('Bank','Counter Cash','Petty Cash') and b.extra1='"+finYear+"' and bd.headAccountId='"+dept+"'"; 
	}
	else{
	 selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type in ('Bank','Counter Cash','Petty Cash') and extra1='"+finYear+"'";	
	}
//	System.out.println("------------>"+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getAssetsLiabilities(String type)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"'"; 
	//System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getAssetsLiabilitiesOfFinYr(String type,String finYr)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	String selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"' and extra1 = '"+finYr+"'"; 
//	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	public List getAssetsLiabilitiesOfFinYrAndHOA(String type,String finYr,String HOA)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	//System.out.println("222222222222222222222222222222222222222222222222222222222222222222222222222222222222222");
	String selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"' and extra1 = '"+finYr+"' and extra2 = '"+HOA+"'"; 
//	System.out.println(selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	
	public List getVendorsOfFinYrAndHOA(String type ,String finYr,String HOA,String status)
	{
	ArrayList list = new ArrayList();
	try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
	//System.out.println("222222222222222222222222222222222222222222222222222222222222222222222222222222222222222");
	String selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where extra1 = '"+finYr+"' and extra2 = '"+HOA+"' and type = '"+type+"' and extra6!='' and extra7='"+status+"'"; 
//	System.out.println("getVendorsOfFinYrAndHOA...."+selectquery);
	ResultSet rs = s.executeQuery(selectquery);
	while(rs.next()){
	 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	return list;
	}
	
	
	
	
	
	
	public List getAssetsLiabilitiesOfFinYrAndHOA1(String type,String finYr,String HOA)
	{
		
		ArrayList list = new ArrayList();
		String selectquery="";
		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		//System.out.println("222222222222222222222222222222222222222222222222222222222222222222222222222222222222222");
		String subQuery = "";
		if(type != null && type.equals("Liabilites")){
			subQuery = " and extra3= '' and extra4 = '' and extra5 = '' ";
		}
		
		if(HOA!=null&&!HOA.equals("")){
		selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"' and extra1 = '"+finYr+"' and extra2 = '"+HOA+"' "+subQuery; 
		}
		else{
		//	System.out.println("This is if Hoa is empty::: or null ");
			selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"' and extra1 = '"+finYr+"' "+subQuery;
		}
//		System.out.println("------------>"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	public List getCreditLiabilitiesOfFinYrAndHOA2(String type,String finYr,String HOA )
	{
		ArrayList list = new ArrayList();
		String selectquery="";
		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		//System.out.println("222222222222222222222222222222222222222222222222222222222222222222222222222222222222222");
		if(HOA!=null&&!HOA.equals("")){
		selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"' and extra1 = '"+finYr+"' and extra2 = '"+HOA+"' and extra3 != '' and extra4 != '' and extra5 != ''"; 
		}
		else{
			selectquery="SELECT uniq_id,bank_id,type,bank_bal,bank_flag,createdDate,createdBy,extra1,extra2,extra3,extra4,extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"' and extra1 = '"+finYr+"' and extra3 != '' and extra4 != '' and extra5 != ''";
		}
//		System.out.println("------------>"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list.add(new bankbalance(rs.getString("uniq_id"),rs.getString("bank_id"),rs.getString("type"),rs.getString("bank_bal"),rs.getString("bank_flag"),rs.getString("createdDate"),rs.getString("createdBy"),rs.getString("extra1"),rs.getString("extra2"),rs.getString("extra3"),rs.getString("extra4"),rs.getString("extra5"),rs.getString("extra6"),rs.getString("extra7"),rs.getString("extra8"),rs.getString("extra9"),rs.getString("extra10")));
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	public double getBankOpeningBal(String bank_id,String type,String finYr)
	{
		String newType = "";
		if(type.equals("bank"))
		{
			newType = "Bank";
		}
		else if(type.equals("cashpaid"))
		{
			newType = "Petty Cash";
		}
		
		double list =0.0;
		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT uniq_id, bank_id, type, bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where  bank_id = '"+bank_id+"' and type = '"+newType+"' and extra1 = '"+finYr+"'"; 
//		System.out.println("bank query........."+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list=Double.parseDouble(rs.getString("bank_bal"));
//	System.out.println("list >>>>> "+list);
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	//This method is created for Receipts & Payments to hide some heads instead of getBankOpeningBal(-) method
	public double getBankOpeningBal1(String bank_id,String type,String finYr,String extraField,String... ids)
	{
		String newType = "";
		String query3="";
		if(type.equals("bank"))
		{
			newType = "Bank";
		}//if ends
		else if(type.equals("cashpaid"))
		{
			newType = "Petty Cash";
		}//else if ends
		if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
			for(String id:ids){
				query3+="and "+extraField+"!='"+id+"'";
			}//for loop ends
		}//if ends
		double list =0.0;
		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT uniq_id, bank_id, type, bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where  bank_id = '"+bank_id+"' and type = '"+newType+"' and extra1 = '"+finYr+"'"+query3; 
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list=Double.parseDouble(rs.getString("bank_bal"));
	
		}//while loop ends
		s.close();
		rs.close();
		c.close();
		}//try block ends
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}//catch block ends
		return list;
	}//method ends
	
	public double getVendorPendingOpeningBal(String vendor_id,String type,String finYr,String extra1,String extra2)
	{
		double list =0.0;
		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT uniq_id, bank_id, type, bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where extra6 = '"+vendor_id+"' and type = '"+type+"' and extra1 = '"+finYr+"'"; 
		//System.out.println("vendoropening-->"+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list=Double.parseDouble(rs.getString("bank_bal"));
	
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	public double getMajorOrMinorOrSubheadOpeningBal(String HOA,String MajorHead,String MinorHead,String SubHead,String headName,String type,String finYr,String extra1,String extra2)
	{
		double list =0.0;
		String query = "";
		String query2 = "";
		if(!extra1.equals(""))
		{
			query2 = " and extra7 = '"+extra1+"'";
		}
		if(headName.equals("subhead"))
		{
			query = " and extra2 = '"+HOA+"' and extra3 = '"+MajorHead+"' and extra4 = '"+MinorHead+"' and extra5 ='"+SubHead+"'";
		}
		else if(headName.equals("minorhead"))
		{
			query = " and extra2 = '"+HOA+"' and extra3 = '"+MajorHead+"' and extra4 = '"+MinorHead+"'";
		}
		else if(headName.equals("majorhead"))
		{
			query = " and extra2 = '"+HOA+"' and extra3 = '"+MajorHead+"'";
		}
		
		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT uniq_id, bank_id, type, sum(bank_bal) as bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"' and extra1 = '"+finYr+"'"+query+query2; 
//		System.out.println("getMajorOrMinorOrSubheadOpeningBal ====> "+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 if(rs.getString("bank_bal") != null)
			list=Double.parseDouble(rs.getString("bank_bal"));
	
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	//This method is created for Income & Expenditure to hide some heads instead of getMajorOrMinorOrSubheadOpeningBal(-) method
	public double getMajorOrMinorOrSubheadOpeningBal1(String HOA,String MajorHead,String MinorHead,String SubHead,String headName,String type,String finYr,String extra1,String extra2,String extraField,String... ids)
	{
		double list =0.0;
		String query = "";
		String query2 = "";
		String query3="";
		if(!extra1.equals(""))
		{
			query2 = " and extra7 = '"+extra1+"'";
		}//if ends
		if(headName.equals("subhead"))
		{
			query = " and extra2 = '"+HOA+"' and extra3 = '"+MajorHead+"' and extra4 = '"+MinorHead+"' and extra5 ='"+SubHead+"'";
		}//if ends
		else if(headName.equals("minorhead"))
		{
			query = " and extra2 = '"+HOA+"' and extra3 = '"+MajorHead+"' and extra4 = '"+MinorHead+"'";
		}//else if ends
		else if(headName.equals("majorhead"))
		{
			query = " and extra2 = '"+HOA+"' and extra3 = '"+MajorHead+"'";
		}//else if ends
		if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
			for(String id:ids){
				query3+="and "+extraField+"!='"+id+"'";
			}//for loop ends
		}//if ends

		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT uniq_id, bank_id, type, sum(bank_bal) as bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where type = '"+type+"' and extra1 = '"+finYr+"'"+query+query2+query3; 
//		System.out.println("getMajorOrMinorOrSubheadOpeningBal1 ====> "+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 if(rs.getString("bank_bal") != null)
			list=Double.parseDouble(rs.getString("bank_bal"));
	
		}//while loop ends
		s.close();
		rs.close();
		c.close();
		}//try block ends
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}//catch block ends
		return list;
	}//method ends
	
	
	
	public List<bankbalance> getMajorOrMinorOrSubheadOpeningBal11(String HOA,String MajorHead,String finYr,String... ids)
	{
		double list =0.0;
		String query = "";
		String query3="";
		List<bankbalance> result = new ArrayList<bankbalance>();
		
			query = " and extra2 = '"+HOA+"' and extra3 = '"+MajorHead+"'";
		
			for(String id:ids){
				query3+="and extra5 != '"+id+"'";
			}

		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT uniq_id, bank_id, type, sum(bank_bal) as bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where (type = 'Assets' or type = 'Liabilites') and extra1 = '"+finYr+"'"+query+query3+" group by extra5"; 
//		System.out.println("getMajorOrMinorOrSubheadOpeningBal11 ====> "+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 if(rs.getString("bank_bal") != null)
			list=Double.parseDouble(rs.getString("bank_bal"));
	
			if( rs.getString("type") != null && rs.getString("extra7") != null && rs.getString("extra3") != null && 
					rs.getString("extra4") != null && rs.getString("extra5") != null && rs.getString("bank_bal") != null ){
				
				bankbalance bankbalance = new bankbalance();
				bankbalance.setType(rs.getString("type"));
				bankbalance.setExtra7(rs.getString("extra7"));
				bankbalance.setExtra3(rs.getString("extra3"));
				bankbalance.setExtra4(rs.getString("extra4"));
				bankbalance.setExtra5(rs.getString("extra5"));
				bankbalance.setBank_bal(rs.getString("bank_bal"));
				
				result.add(bankbalance);
			}
			
		}//while loop ends
		s.close();
		rs.close();
		c.close();
		}//try block ends
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}//catch block ends
		return result;
	}//method ends
	
	public double getOpeningBalOfCounterCash(String HOAID,String finYr)
	{
		double list =0.0;
		try {
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT uniq_id, bank_id, type, bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where  type = 'Counter Cash' and extra2 = '"+HOAID+"' and extra1 = '"+finYr+"'"; 
//		System.out.println("getOpeningBalOfCounterCash ====> "+selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list=Double.parseDouble(rs.getString("bank_bal"));
	
		}
		s.close();
		rs.close();
		c.close();
		}
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
		return list;
	}
	
	//This method is created for Receipts & Payments to hide some heads instead of getOpeningBalOfCounterCash(-) method
	public double getOpeningBalOfCounterCash1(String HOAID,String finYr,String extraField,String... ids)
	{
		double list =0.0;
		String query3="";
		try {
			if(!extraField.equals(null) && !extraField.equals("") && !ids.equals(null) && !ids.equals("")){
				for(String id:ids){
					query3+="and "+extraField+"!='"+id+"'";
				}//for loop ends
			}//if ends
		 // Load the database driver
		c = ConnectionHelper.getConnection();
		s=c.createStatement();
		String selectquery="SELECT uniq_id, bank_id, type, bank_bal, bank_flag, createdDate, createdBy, extra1, extra2, extra3, extra4, extra5,extra6,extra7,extra8,extra9,extra10 FROM bankbalance where  type = 'Counter Cash' and extra2 = '"+HOAID+"' and extra1 = '"+finYr+"'"+query3; 
		//System.out.println(selectquery);
		ResultSet rs = s.executeQuery(selectquery);
		while(rs.next()){
		 list=Double.parseDouble(rs.getString("bank_bal"));
	
		}//while loop ends
		s.close();
		rs.close();
		c.close();
		}//try block ends
		catch(Exception e){
		this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}//catch block ends
		return list;
	}//method ends

public List<bankbalance> getPettyCashAmmount() {
	List<bankbalance> bankbalanceList =  new ArrayList<bankbalance>();
	bankbalance Bankbalance = null;
	try{
	c = ConnectionHelper.getConnection();
	s=c.createStatement();
   String selectQuery = "Select bd.bank_id,bd.bank_name,sum(bb.bank_bal) as bank_bal FROM bankdetails bd,bankbalance bb where bb.type='Petty Cash' and bb.bank_id=bd.bank_id group by bank_id";	
   ResultSet rs = s.executeQuery(selectQuery);
	while(rs.next()) {
		Bankbalance = new bankbalance();
		Bankbalance.setBank_id(rs.getString("bank_id"));
		Bankbalance.setBank_flag(rs.getString("bank_name"));
		Bankbalance.setBank_bal(rs.getString("bank_bal"));
		bankbalanceList.add(Bankbalance);
	}
	s.close();
	rs.close();
	c.close();
	}
	catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}
	

	return bankbalanceList;
}


public List<bankbalance> getBanksBsdOnFinYearHoa(String creditOrDebit,String finYear,String hoa,String type)	{
	String query = "SELECT b.uniq_id,b.bank_id,b.bank_bal,b.extra1,b.extra8 FROM bankbalance b,bankdetails bd where b.type = '"+type+"' and b.extra1 = '"+finYear+"' and b.bank_id = bd.bank_id and bd.headAccountId = "+hoa+" and bd.extra2 = '"+creditOrDebit+"'";
	ResultSet rs = null;
	List<bankbalance> bankbalanceList = null;
	try	{
		c = ConnectionHelper.getConnection();
		s = c.createStatement();
		if (s != null)	{
			rs = s.executeQuery(query);
//			System.out.println("query----"+query);
			bankbalanceList =  new ArrayList<bankbalance>();
			while(rs.next())	{
				bankbalance Bankbalance = new bankbalance();
				Bankbalance.setUniq_id(rs.getString("b.uniq_id"));
				Bankbalance.setBank_id(rs.getString("b.bank_id"));
				Bankbalance.setBank_bal(rs.getString("bank_bal"));
				Bankbalance.setExtra1(rs.getString("b.extra1"));
				Bankbalance.setExtra8(rs.getString("b.extra8"));
				bankbalanceList.add(Bankbalance);
			}
		}
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	catch(Exception e)	{
		e.printStackTrace();
	}
	finally	{
		if (s != null)	{
			try	{
				s.close();
			}
			catch(SQLException se)	{
				se.printStackTrace();
			}
		}
		if (c != null)	{
			try	{
				c.close();
			}
			catch(SQLException se)	{
				se.printStackTrace();
			}
		}
		if (rs != null)	{
			try	{
				rs.close();
			}
			catch(SQLException se)	{
				se.printStackTrace();
			}
		}
	}
return bankbalanceList;
}
}





