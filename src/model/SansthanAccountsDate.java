package model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class SansthanAccountsDate {
	
	public static List<String>  getBetweenDates(String fromDate,String toDate,String currentDateFormat,String expectedDateFormat,TimeZone zone) throws ParseException{
		List<String> allDates=new ArrayList<String>();
		DateFormat df=new SimpleDateFormat(currentDateFormat);
		DateFormat expDateFormat=new SimpleDateFormat(expectedDateFormat);
		
		df.setTimeZone(zone);
		expDateFormat.setTimeZone(zone);
		
		Calendar cal = Calendar.getInstance(zone);
		cal.setTime(df.parse(fromDate));
		while (!cal.getTime().after(df.parse(toDate))) {
		    allDates.add(expDateFormat.format(cal.getTime()));
		    cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return allDates;
	}
	
	public static List<String>  getAddedBetweenDates(String fromDate,String toDate,String currentDateFormat,String expectedDateFormat,TimeZone zone,int amount) throws ParseException{
		List<String> allDates=new ArrayList<String>();
		DateFormat df=new SimpleDateFormat(currentDateFormat);
		DateFormat expDateFormat=new SimpleDateFormat(expectedDateFormat);
		
		df.setTimeZone(zone);
		expDateFormat.setTimeZone(zone);
		
		Calendar cal = Calendar.getInstance(zone);
		cal.setTime(df.parse(fromDate));
		cal.add(Calendar.DAY_OF_MONTH, amount);
		while (!cal.getTime().after(df.parse(toDate))) {
		    allDates.add(expDateFormat.format(cal.getTime()));
		    cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return allDates;
	}
	
	public static String  getAddedDate(String date,String currentDateFormat,String expectedDateFormat,TimeZone zone,int amount) throws ParseException{
		DateFormat df=new SimpleDateFormat(currentDateFormat);
		DateFormat expDateFormat=new SimpleDateFormat(expectedDateFormat);
		
		df.setTimeZone(zone);
		expDateFormat.setTimeZone(zone);
		
		Calendar cal = Calendar.getInstance(zone);
		cal.setTime(df.parse(date));
		cal.add(Calendar.DAY_OF_MONTH, amount);
		return expDateFormat.format(cal.getTime());
	}
	
	/*public String[] getThisFinancialYear(String reqDateFormat,TimeZone zone){
		
	}
	public String[] getThisFinancialYear(String reqDateFormat,TimeZone zone,int addDates){
		
	}*/
	
}
