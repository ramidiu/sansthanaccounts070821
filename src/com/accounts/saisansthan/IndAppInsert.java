package com.accounts.saisansthan;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mainClasses.indentListing;
import mainClasses.no_genaratorListing;
import mainClasses.productsListing;
import mainClasses.vendorsListing;
import MailBox.SendMailBean;
import beans.indent;
import beans.indentService;
import beans.indentapprovals;
import beans.indentapprovalsService;
import beans.no_genaratorService;
import beans.purchaseorder;
import beans.purchaseorderService;
@WebServlet("/IndAppInsert")
public class IndAppInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = null;
		if(request.getSession() != null)	{
			session = request.getSession(false);
			String role = (String)session.getAttribute("role");
			String admin_id=session.getAttribute("superadminId").toString();
			String description=request.getParameter("feedback");
			String admin_status=request.getParameter("status");
			Calendar c1 = Calendar.getInstance(); 
			DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			String extra2="",extra1 = "",indentid="",vendorid="",qty="",trackingNo="",poInv_id="",HOA="";
			String approved_date=(dateFormat.format(c1.getTime())).toString();
			String indentinvoice_id=request.getParameter("indentInvoiceId");
			String[] count=request.getParameterValues("count");

			indentapprovalsService iapservice = new indentapprovalsService();
			indentService idservice = new indentService();
			purchaseorderService poservice = new purchaseorderService();
			indentListing indlistservice = new indentListing();
			no_genaratorListing nogenlservice = new no_genaratorListing();
			productsListing prodservice = new productsListing();
			vendorsListing vendorservice = new vendorsListing();
			no_genaratorService nogenservice = new no_genaratorService();
			SendMailBean mailservice = new SendMailBean();
			List<indent> indentsList = null;
			List<indent> indentsList2 = null;

			indentapprovals iap = new indentapprovals();
			iap.setadmin_id(admin_id);
			iap.setdescription(description);
			iap.setadmin_status(admin_status);
			iap.setapproved_date(approved_date);
			iap.setindentinvoice_id(indentinvoice_id);
			iap.setextra1(extra1);
			iap.setextra2(extra2);
			iap.setextra3(role);
			iapservice.insertIndApp(iap);

			if(count != null){
				for(int i=0;i<count.length;i++){
					indentid = request.getParameter("indentId"+count[i]);
					vendorid = request.getParameter("vendorId"+count[i]);
					qty = request.getParameter("quantity"+count[i]);
					
					idservice.updateIndent(indentid,"SuperAdminApproved", vendorid.split(" ")[0], qty);
				}
			}
			
			indentsList = indlistservice.getMindentsGroupByVendor(indentinvoice_id);
//			String required_date="";
			String price="",data="";
			if(indentsList.size() > 0)	{
				for (indent id : indentsList)	{
					indentsList2 = indlistservice.getMindentProductsDetails(indentinvoice_id,id.getvendor_id());
					try	{
						poInv_id = nogenlservice.getid("POInvoice");
					}
					catch(SQLException se)	{
						se.printStackTrace();
					}
					HOA=id.gethead_account_id();
					if(HOA!=null && !HOA.equals("")){
						try	{
							if(HOA.equals("3")){
								trackingNo=nogenlservice.getid("poojastoreTracking");
								nogenservice.updatInvoiceNumber(trackingNo,"poojastoreTracking");	
							}
							else if(HOA.equals("4")){
									trackingNo=nogenlservice.getid("sansthanTracking");
									nogenservice.updatInvoiceNumber(trackingNo,"sansthanTracking");	
							}
							else if(HOA.equals("1")){
									trackingNo=nogenlservice.getid("charityTracking");
									nogenservice.updatInvoiceNumber(trackingNo,"charityTracking");	
							}
							else if(HOA.equals("5")){
									trackingNo=nogenlservice.getid("sainivasTracking");
									nogenservice.updatInvoiceNumber(trackingNo,"sainivasTracking");	
							}
						}
						catch(SQLException se)	{
							se.printStackTrace();
						}
					
					if(count != null){
						for(int j=0;j<count.length;j++){
							indentid=request.getParameter("indentId"+count[j]);
							idservice.updateTrackingNo(indentid, trackingNo);		
						}
					}
				}
					String vmail="";
					if(indentsList2.size() > 0)	{
					int count1 = 0;
					for (indent id1 : indentsList2)	{
						count1 ++;
//						if (id1.getrequire_date() != null) required_date = id1.getrequire_date();
						if(id1.getproduct_id() != null && !id1.getproduct_id().equals(""))	{
							price = prodservice.getProductPrice(id1.getproduct_id(),id1.gethead_account_id());
						}
						data=data+"<tr><td>"+count1+"</td><td>"+id1.getproduct_id()+"</td><td>"+prodservice.getProductsNameByCat1(id1.getproduct_id(),id1.gethead_account_id())+"</td><td>"+id1.getquantity()+"</td></tr>";
						purchaseorder po = new purchaseorder();
						po.setcreated_date(approved_date);
						po.setstatus("POCreated");
						po.setindentinvoice_id(id1.getindentinvoice_id());
						po.setrequiremrnt(id1.getrequirement());
						po.sethead_account_id(id1.gethead_account_id());
						po.setproduct_id(id1.getproduct_id());
						po.settablet_id(po.gettablet_id());
						po.setpurchase_price(price);
						po.setquantity(id1.getquantity());
						po.setvendor_id(id1.getvendor_id());
						po.setemp_id(id1.getemp_id());
						po.setpoinv_id(poInv_id);
						po.setextra1(trackingNo);
						po.setrequired_date(null);
//						System.out.println("requores dfar-----"+required_date);
						poservice.insertPurchaseOrder(po);

						if(count1 == 1)	{
							vmail = vendorservice.getVendorEmail(id1.getvendor_id());
							if (vmail != null && !vmail.equals(""))	{
								String html="<div style='width:750px; height:500px; margin:0px auto; font-family:Verdana, Geneva, sans-serif; font-size:14px;'><table width='100%' cellpadding='0' cellspacing='0'><tr><td style='text-align:center; font-size:18px; color:#7f3519; font-weight:bold;'>SHRI SHIRDI SAI BABA SANSTHAN TRUST</td></tr><tr><td style='text-align:center'>Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr><tr><td>Dear "+vendorservice.getMvendorsAgenciesName(vmail)+",</td></tr><tr><td>Purchase Order Ref No : "+poInv_id+". </td></tr><tr><td>Purchase order Priority : "+id1.getrequirement()+"</td> </tr></tr><tr><td><p style='text-align:justify;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; We are pleased to confirm our purchase order of the following items : </p></td></tr><tr><td><table border='1' width='100%' cellpadding='0' cellspacing='0'><tr><td>S.NO</td><td>PRODUCT ID</td><td>PRODUCT NAME</td><td>REQUIRED QTY</td></tr>"+data+"</table></td></tr><tr><td height='20'></td></tr><tr><td>Thanking you,</td></tr><tr><td>warm regards</td></tr><tr><td style='text-transform:capitalize;'>Shri Shirdi Sai Baba Sansthan Trust.</td></tr></table></div>";
								System.out.println("p order html--"+html);
								mailservice.send("info@saisansthan.in",vmail, "", "", "We are pleased to confirm our purchase order from shri shirdi sai baba sansthan trust", html, "localhost");
							}
						} //if
				} //inner for
			} //if
			nogenservice.updatInvoiceNumber(poInv_id,"POInvoice");	
			trackingNo="";
		} //outer for
	} //if
			
/*			System.out.println("admin_id------"+admin_id);
			System.out.println("description------"+description);
			System.out.println("admin_status-------"+admin_status);
			System.out.println("approved_date------"+approved_date);
			System.out.println("indentinvoice_id------"+indentinvoice_id);
*/			
			response.sendRedirect(request.getContextPath()+"/superadmin/adminPannel.jsp?page=indentReportList");
		}
		else	{
			response.sendRedirect("superadmin/adminPannel.jsp?page=indentDetailedReport");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
