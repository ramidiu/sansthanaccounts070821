package com.accounts.saisansthan;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.map.MultiValueMap;

import dbase.sqlcon.ConnectionHelper;
import beans.banktransactions;
import beans.customerpurchases;
import beans.productexpenses;

public class JVListing {
	
	private Connection con = null;
	private Statement st = null;
	private ResultSet rs = null;

	public MultiValueMap getJvsBsdOnDatesOrBsdOnJvNo(String fromdate,String todate,String jvNo)	{
		String selectQuery = "";
		MultiValueMap peMap = null;
		if (( (fromdate == null || fromdate.trim().equals("")) && (todate == null || todate.trim().equals("")) ) && (jvNo != null && !jvNo.trim().equals("")))	{
			selectQuery = "select exp_id,vendorId,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,head_account_id,extra1,extra5 from productexpenses where extra5= '"+jvNo+"'";
		}
		else if (( (fromdate != null && !fromdate.trim().equals("")) &&  (todate != null && !todate.trim().equals("")) ) && (jvNo == null || jvNo.trim().equals("")))	{
			selectQuery = "select exp_id,vendorId,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,head_account_id,extra1,extra5 from productexpenses where extra5 like 'JVN%' and entry_date between '"+fromdate+"' and '"+todate+"' order by extra5";
		}
//		System.out.println("getJvsBsdOnDatesOrBsdOnJvNo---"+selectQuery);
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery(selectQuery);
				peMap = new MultiValueMap();
				while (rs.next())	{
					productexpenses pe = new productexpenses();
					pe.setexp_id(rs.getString("exp_id"));
					pe.setvendorId(rs.getString("vendorId"));
					pe.setentry_date(rs.getString("entry_date"));
					pe.setmajor_head_id(rs.getString("major_head_id"));
					pe.setminorhead_id(rs.getString("minorhead_id"));
					pe.setsub_head_id(rs.getString("sub_head_id"));
					pe.setamount(rs.getString("amount"));
					pe.setexpinv_id(rs.getString("expinv_id"));
					pe.setemp_id(rs.getString("emp_id"));
					pe.sethead_account_id(rs.getString("head_account_id"));
					pe.setextra1(rs.getString("extra1"));
					pe.setextra5(rs.getString("extra5"));
					
					peMap.put(rs.getString("extra5"),pe);
					
				}
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	 return peMap;
	}
	
	
	public MultiValueMap getJvsBsdOnDatesOrVoucherNo(String fromdate,String todate,String jvNumber)	{
		String selectQuery = "";
		MultiValueMap cpMap = null;
		if ( ( (fromdate == null || fromdate.trim().equals("")) &&  (todate == null || todate.trim().equals("") ) ) && (jvNumber != null && !jvNumber.trim().equals("")) )	{
			selectQuery = "select purchaseid,date,productid,totalammount,vocharnumber,cash_type,extra1,emp_id,extra6,extra7,extra9 from customerpurchases where vocharnumber = '"+jvNumber+"'";
		}
		else if ( ((fromdate != null && !fromdate.trim().equals("")) &&  (todate != null && !todate.trim().equals("")) ) && (jvNumber == null || jvNumber.trim().equals("")))	{
			selectQuery = "select purchaseid,date,productid,totalammount,vocharnumber,cash_type,extra1,emp_id,extra6,extra7,extra9 from customerpurchases where vocharnumber like 'JVN%' and date between '"+fromdate+"' and '"+todate+"' order by vocharnumber";
		}
//		System.out.println("getJvsBsdOnDatesOrVoucherNo---"+selectQuery);
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery(selectQuery);
				cpMap = new MultiValueMap();
				while (rs.next())	{
					customerpurchases cp = new customerpurchases();
					cp.setpurchaseId(rs.getString("purchaseid"));
					cp.setdate(rs.getString("date"));
					cp.setproductId(rs.getString("productid")); // subhead
					cp.settotalAmmount(rs.getString("totalammount"));
					cp.setvocharNumber(rs.getString("vocharnumber"));
					cp.setcash_type(rs.getString("cash_type"));
					cp.setextra1(rs.getString("extra1")); // head of account
					cp.setemp_id(rs.getString("emp_id"));
					cp.setextra6(rs.getString("extra6")); // major head
					cp.setextra7(rs.getString("extra7")); // minor head
					cp.setextra9(rs.getString("extra9")); // bank
					
					cpMap.put(rs.getString("vocharnumber"),cp);
				}
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	return cpMap;	
	}
	
	public MultiValueMap getProductExpBsdOnDateOrEpiv(String fromdate,String todate,String epivNo)	{
		String selectQuery = "";
		MultiValueMap expMap = null;
		if ( (fromdate == null || fromdate.trim().equals("")) &&  (todate == null || todate.trim().equals("")) && (epivNo != null && !epivNo.trim().equals("")) )	{
			selectQuery = "select exp_id,vendorId,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,head_account_id,extra1,extra5,extra2 from productexpenses where expinv_id = '"+epivNo+"'";
		}
		else if ( ((fromdate != null && !fromdate.trim().equals("")) &&  (todate != null && !todate.trim().equals("")) ) && (epivNo == null || epivNo.trim().equals("")))	{
			selectQuery = "select exp_id,vendorId,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,head_account_id,extra1,extra5,extra2 from productexpenses where expinv_id like 'EPIV%' and entry_date between '"+fromdate+"' and '"+todate+"' order by expinv_id";
		}
		else if ((fromdate == null || fromdate.trim().equals("")) &&  (todate == null || todate.trim().equals("")) && (epivNo == null || epivNo.trim().equals("")))	{
			selectQuery = "select exp_id,vendorId,entry_date,major_head_id,minorhead_id,sub_head_id,amount,expinv_id,emp_id,head_account_id,extra1,extra5,extra2 from productexpenses where expinv_id like 'EPIV%'" ;
		}
//		System.out.println("getProductExpBsdOnDateOrEpiv---"+selectQuery);
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery(selectQuery);
				expMap = new MultiValueMap();
				while (rs.next())	{
					productexpenses pe = new productexpenses();
					pe.setexp_id(rs.getString("exp_id"));
					pe.setvendorId(rs.getString("vendorId"));
					pe.setentry_date(rs.getString("entry_date"));
					pe.setmajor_head_id(rs.getString("major_head_id"));
					pe.setminorhead_id(rs.getString("minorhead_id"));
					pe.setsub_head_id(rs.getString("sub_head_id"));
					pe.setamount(rs.getString("amount"));
					pe.setexpinv_id(rs.getString("expinv_id"));
					pe.setemp_id(rs.getString("emp_id"));
					pe.sethead_account_id(rs.getString("head_account_id"));
					pe.setextra1(rs.getString("extra1"));
					pe.setextra5(rs.getString("extra5"));
					pe.setextra2(rs.getString("extra2"));
					
					expMap.put(rs.getString("expinv_id"),pe);
				}
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	return expMap;
	}
	
	public Map<String,banktransactions> getBankTransactionsMap(String fromdate,String todate,String columnValue,String columnName)	{
		String selectQuery = "";
		Map<String,banktransactions> bktMap = null;
		if((fromdate == null || fromdate.trim().equals("")) &&  (todate == null || todate.trim().equals("")) && (columnValue != null && !columnValue.trim().equals("") && columnName != null && !columnName.trim().equals("") ))	{
			selectQuery = "select banktrn_id, bank_id, narration, date, amount, type, createdBy, sub_head_id, minor_head_id, major_head_id, head_account_id,extra3,extra5 from banktransactions where "+columnName.trim()+" like '%"+columnValue+"%' order by banktrn_id";
		}
		else if (((fromdate != null && !fromdate.trim().equals("")) &&  (todate != null && !todate.trim().equals("")) ) && ( columnValue != null && !columnValue.trim().equals("") && columnName != null && !columnName.trim().equals("") ))	{
			selectQuery = "select banktrn_id, bank_id, narration, date, amount, type, createdBy, sub_head_id, minor_head_id, major_head_id, head_account_id,extra3,extra5 from banktransactions where "+columnName.trim()+" like '%"+columnValue+"%' and date between '"+fromdate+"' and '"+todate+"' order by date";
		}
//		System.out.println("getBankTransactionsMap----"+selectQuery);
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery(selectQuery);
				bktMap = new TreeMap<String, banktransactions>();
				while (rs.next())	{
					banktransactions bkt = new banktransactions();
					bkt.setbanktrn_id(rs.getString("banktrn_id"));
					bkt.setbank_id(rs.getString("bank_id"));
					bkt.setnarration(rs.getString("narration"));
					bkt.setdate(rs.getString("date"));
					bkt.setamount(rs.getString("amount"));
					bkt.settype(rs.getString("type"));
					bkt.setcreatedBy(rs.getString("createdBy"));
					bkt.setSub_head_id(rs.getString("sub_head_id"));
					bkt.setMinor_head_id(rs.getString("minor_head_id"));
					bkt.setMajor_head_id(rs.getString("major_head_id"));
					bkt.setHead_account_id(rs.getString("head_account_id"));
					bkt.setextra3(rs.getString("extra3"));
					bkt.setextra5(rs.getString("extra5"));
					
					bktMap.put(rs.getString("banktrn_id"),bkt);
				}
			}

		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return bktMap;
	}
}
