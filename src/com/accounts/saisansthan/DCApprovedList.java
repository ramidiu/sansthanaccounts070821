package com.accounts.saisansthan;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.mail.Session;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.godwanstock;
import mainClasses.godwanstockListing;

@WebServlet("/DCApprovedList")
public class DCApprovedList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session.getAttribute("adminId") != null)	{
			String dc = request.getParameter("dcNo");
			PrintWriter pw = response.getWriter();
			StringBuilder builder = new StringBuilder();
			godwanstockListing gdl = new godwanstockListing();
//			SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");	
			List<godwanstock> gdlst = gdl.getDcEntriesBetweenDates("","",dc.trim(),"approved");
			if (gdlst.size() > 0)	{
					for (int i = 0 ; i < gdlst.size() ; i++)	{
						godwanstock gd = gdlst.get(i);
							builder.append(gd.getExtra1()).append("#").append(gd.getDate()).append("#").append(gd.getExtra9()).append("#").append(gd.getExtra25()).append("#").append(gd.getDepartment()).append("#").append(gd.getMajorhead_id()).append("#").append(gd.getExtra26()).append("#").append(gd.getMinorhead_id()).append("#").append(gd.getExtra27()).append("#").append(gd.getExtra23()).append("#").append(gd.getExtra28()).append("#").append(gd.getProductId()).append("#").append(gd.getExtra29()).append("#").append(gd.getQuantity()).append("#").append(gd.getPurchaseRate()).append("#").append(gd.getExtra24()).append("#").append(gd.getExtra16()).append("#").append(gd.getExtra30()).append("#").append(gd.getExtra21()).append(",");
					}
				pw.print(builder.toString());
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
