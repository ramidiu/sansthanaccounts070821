package com.accounts.saisansthan;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.godwanstock;
import mainClasses.godwanstockListing;

@WebServlet("/DCList")
public class DCList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session.getAttribute("empId") != null)	{
//		String	fromdate = request.getParameter("fromDate");
//		String	todate = request.getParameter("toDate");
		RequestDispatcher rd = null;
		godwanstockListing gl = new godwanstockListing();
		List<godwanstock> list = gl.getDcEntriesBetweenDates("","","","");
		Map<String,List<godwanstock>> lists = new TreeMap<String,List<godwanstock>>();	
		Map<String,String> dcids = new TreeMap<String,String>();
		Map<String,String> vendorids = new TreeMap<String,String>();
		Map<String,Double> amounts = new TreeMap<String,Double>();
		for (int i = 0 ; i < list.size() ; i++)	{
			godwanstock gd = list.get(i);
			dcids.put(gd.getExtra25(),gd.getExtra1());
			vendorids.put(gd.getVendorId(),gd.getExtra10()+","+gd.getExtra25()+","+gd.getVendorId());
		}
		Set<String> dcs = dcids.keySet();
		Iterator<String> itr = dcs.iterator();
		while (itr.hasNext())	{
			double amount = 0;
			List<godwanstock> gdlist = new ArrayList<godwanstock>();
			String dcNo = itr.next();
			for (int i = 0 ; i < list.size() ; i++)	{
				godwanstock gd = list.get(i);
				if (gd.getExtra25().equals(dcNo))	{
					gdlist.add(gd);
					amount += Double.parseDouble(gd.getExtra24());
				}
			}
			amounts.put(dcNo,amount);
			lists.put(dcNo,gdlist);
		}

		rd = request.getRequestDispatcher("directChallanList.jsp?id=123");
		request.setAttribute("dclist",lists);
		request.setAttribute("ids",dcs);
		request.setAttribute("vendors",vendorids);
		request.setAttribute("amts",amounts);
		rd.forward(request, response);
		}
		else	{
			response.sendRedirect("index.jsp");
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
