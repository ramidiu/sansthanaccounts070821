package com.accounts.saisansthan;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import dbase.sqlcon.ConnectionHelper;
import beans.bankdetails;
import beans.headofaccounts;
import beans.majorhead;
import beans.minorhead;
import beans.subhead;
import beans.vendors;


public class MapOfHeads {
	
	private Connection con = null;
	private Statement st = null;
	private ResultSet rs = null;
	
	public Map<String,majorhead> getMajorHeadsMap()	{
		Map<String,majorhead> majorHeadMap = null;
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery("select * from majorhead");
				majorHeadMap = new HashMap<String, majorhead>();
				while(rs.next())	{
					majorhead mj = new majorhead();
					mj.setmajor_head_id(rs.getString("major_head_id"));
					mj.setname(rs.getString("name"));
					mj.sethead_account_id(rs.getString("head_account_id"));
					mj.setextra1(rs.getString("extra1"));
					mj.setextra2(rs.getString("extra2"));
					
					majorHeadMap.put(rs.getString("major_head_id"), mj);
				}
			}
			
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return majorHeadMap;
	}
	
	
	public Map<String,minorhead> getMinorHeadsMap()	{
		Map<String,minorhead> minorHeadMap = null;
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery("select * from minorhead");
				minorHeadMap = new HashMap<String, minorhead>();
				while(rs.next())	{
					minorhead mn = new minorhead();
					mn.setminorhead_id(rs.getString("minorhead_id"));
					mn.setname(rs.getString("name"));
					mn.setmajor_head_id(rs.getString("major_head_id"));
					mn.sethead_account_id(rs.getString("head_account_id"));
					
					minorHeadMap.put(rs.getString("minorhead_id"), mn);
				}
			}
			
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return minorHeadMap;
	}
	
	
	public Map<String,subhead> getSubHeadsMap()	{
		Map<String,subhead> subHeadMap = null;
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery("select * from subhead");
				subHeadMap = new HashMap<String, subhead>();
				while(rs.next())	{
					subhead sh = new subhead();
					sh.setsub_head_id(rs.getString("sub_head_id"));
					sh.setname(rs.getString("name"));
					sh.setmajor_head_id(rs.getString("major_head_id"));
					sh.setextra1(rs.getString("extra1"));
					sh.sethead_account_id(rs.getString("head_account_id"));
					sh.setextra2(rs.getString("extra2"));
					
					subHeadMap.put(rs.getString("sub_head_id"), sh);
				}
			}
			
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return subHeadMap;
	}
	
	public Map<String,headofaccounts> getHoasMap()	{
		Map<String,headofaccounts> hmap = null;
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery("select * from headofaccounts");
				hmap = new HashMap<String,headofaccounts>();
				while(rs.next())	{
					headofaccounts ha = new headofaccounts();
					ha.sethead_account_id(rs.getString("head_account_id"));
					ha.setname(rs.getString("name"));
					hmap.put(rs.getString("head_account_id"), ha);
				}
			}
			
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return hmap;
	}
	
	public Map<String,bankdetails> getBanksMap()	{
		Map<String,bankdetails> banksMap = null;
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery("select bank_id,bank_name,extra5,headAccountId,extra2 from bankdetails");
				banksMap = new HashMap<String,bankdetails>();
				while(rs.next())	{
					bankdetails bk = new bankdetails();
					bk.setbank_id(rs.getString("bank_id"));
					bk.setbank_name(rs.getString("bank_name"));
					bk.setextra5(rs.getString("extra5"));
					bk.setheadAccountId(rs.getString("headAccountId"));
					bk.setextra2(rs.getString("extra2"));
					banksMap.put(rs.getString("bank_id"), bk);
				}
			}
			
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return banksMap;
	}
	
	public Map<String,vendors> getVendorsMap()	{
		Map<String,vendors> vendorsMap = null;
		try	{
			con = ConnectionHelper.getConnection();
			if (con != null)	{
				st = con.createStatement();
				rs = st.executeQuery("select vendorid,agencyName,phone from vendors");
				vendorsMap = new HashMap<String,vendors>();
				while(rs.next())	{
					vendors vendor = new vendors();
					vendor.setvendorId(rs.getString("vendorid"));
					vendor.setagencyName(rs.getString("agencyName"));
					vendor.setphone(rs.getString("phone"));
					vendorsMap.put(rs.getString("vendorid"), vendor);
				}
			}
			
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (st != null) st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return vendorsMap;
	}
}
