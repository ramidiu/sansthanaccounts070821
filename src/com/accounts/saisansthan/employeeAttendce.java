package com.accounts.saisansthan;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.accounts.saisansthan.attendancecalculation;

import beans.employee_attendence;
import beans.stafftimings;

public class employeeAttendce 
{
	com.accounts.saisansthan.attendancecalculation attedencecal=new attendancecalculation();
	
	
	
	DecimalFormat df = new DecimalFormat("##.##");
	public String[] calculateClOTs(String employeId,String date1 ) throws ParseException
	{
		String l[]=new String[25];
		float LA=0,LP=0,CL=0,AB=0,OT=0,OD=0,temp_CL=0;
		float OP_bal=0,AT_days=0,er_CL=0,To_Cl=0,Usd_CL=0,GTo_CL=0,cal_LA=0,cal_LP=0,cal_Cl=0,fr_cal_Cl=0,cal_AB=0;
		
	  	SimpleDateFormat dtf=new SimpleDateFormat("yyyy/MM/dd");
	    SimpleDateFormat monthformat=new SimpleDateFormat("MM");
	    SimpleDateFormat yearformat=new SimpleDateFormat("yyyy");
	    
	    
	    String year=yearformat.format(dtf.parse(date1));
	    String month=monthformat.format(dtf.parse(date1));
	    
	    Map<String, String> attencdencemap= attedencecal.getAttendanceEmployee(employeId, month, year);
	    Iterator iterator = attencdencemap.keySet().iterator();
	    while(iterator.hasNext()){
	   	  Object key   = iterator.next();
	   	  Object value = attencdencemap.get(key);
	   	  if(key.toString().equals("LP")){
	   		  LP=Float.parseFloat(value.toString());
	   		  
	   	  } else  if(key.toString().equals("LA")){
	   		  LA=Float.parseFloat(value.toString());
	   		  
	   	  } else  if(key.toString().equals("CL")){
	   		  CL=Float.parseFloat(value.toString());
	   		  
	   	  }  else  if(key.toString().equals("AB")){
	   		  AB=Float.parseFloat(value.toString());
	   		  
	   	  }  else  if(key.toString().equals("OD")){
	   		  OD=Float.parseFloat(value.toString());
	   		  
	   	  }
	   	  else  if(key.toString().equals("OT")){
	   		  OT=Float.parseFloat(value.toString());
	   		  
	   	  }
	   	  else  if(key.toString().equals("OP_bal")){
	   		  OP_bal=Float.parseFloat(value.toString());
	   		  
	   	  } else  if(key.toString().equals("carry_cl")){
	   		  er_CL=Float.parseFloat(value.toString());
	   		  
	   	  }
	   	  
	   	  
	   	  
	   	  else  if(key.toString().equals("AB")){
	   		  AB=Float.parseFloat(value.toString());
	   		  
	   	  }
	   	  else  if(key.toString().equals("cal_AB")){
	   		  cal_AB=Float.parseFloat(value.toString());
	   		  
	   	  }
	   	  else  if(key.toString().equals("cal_LP")){
	   		  cal_LP=Float.parseFloat(value.toString());
	   		  
	   	  }
	   	  else  if(key.toString().equals("cal_LA")){
	   		  cal_LA=Float.parseFloat(value.toString());
	   		  
	   	  }
	   	  else  if(key.toString().equals("Usd_CL")){
	   		  Usd_CL=Float.parseFloat(value.toString());
	   		  
	   	  }
	   	  
	   	}
	    To_Cl=er_CL+OP_bal; 

/*	    System.out.println(employeId+"==="+CL);*/
			
			l[0]=new Float(OP_bal).toString();            //opening cl
			l[1]=new Float(er_CL).toString();				//earn cl
			l[2]=(df.format(To_Cl-(cal_LA+cal_LP+CL+cal_AB)));  //total cl
			
			
			l[3]=new Float(LA).toString(); //LA
			l[4]=new Float(LP).toString(); //LP
			l[5]=new Float(CL).toString(); //CL
			l[6]=new Float(AB).toString(); //AB
			
			l[7]=new Float( cal_LA).toString(); // cal_LA
			l[8]=new Float( cal_LP).toString(); // cal_LA
			
			l[9]=new Float( CL).toString(); // cal_CL
			l[10]=new Float( cal_AB).toString(); // cal_LA
			l[11]=new Float( cal_LA+cal_LP+CL+cal_AB+cal_AB).toString(); // cal_LA
			
			l[12]=new Float( To_Cl-(cal_LA+cal_LP+cal_AB)).toString(); // cal_LA
			l[13]=new Float(OD).toString(); //od
			l[14]=new Float(OT).toString();  //ot
			
			
		
	return l; 
	}
}


