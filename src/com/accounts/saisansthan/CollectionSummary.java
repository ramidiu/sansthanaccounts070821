package com.accounts.saisansthan;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.collections.map.MultiValueMap;

import beans.customerpurchases;
import dbase.sqlcon.ConnectionHelper;

public class CollectionSummary {

	private Connection con = null;
	private Statement st = null;
	private ResultSet rs = null;
	
	public MultiValueMap getcollectionDetails(String fromdate,String todate,String dept,String reporttype)	{
		if (!reporttype.trim().equals("") && !todate.trim().equals("") && !fromdate.trim().equals("") && !dept.trim().equals(""))	{
			 MultiValueMap detailsMap = null;
			 String selectQuery = ""; 
			 
			 if (reporttype.equals("year"))	{
				 selectQuery = "SELECT sum(totalAmmount) as amount,extra1,date_format(date,'%M%Y') as date,cash_type FROM customerpurchases where date between '"+fromdate+"' and '"+todate+"' and extra1 = '"+dept+"' and cash_type != 'offerKind' and  cash_type != 'journalvoucher' and cash_type != 'online pending' and cash_type != 'othercash' group by date_format(date,'%M%Y'),cash_type order by date_format(date,'%M%Y')";
			 }
			 else if (reporttype.equals("month"))	{
				 selectQuery = "SELECT sum(totalAmmount) as amount,extra1,date(date) as date,cash_type FROM customerpurchases where date between '"+fromdate+"' and '"+todate+"' and extra1 = '"+dept+"' and cash_type != 'offerKind' and  cash_type != 'journalvoucher' and cash_type != 'online pending' and cash_type != 'othercash' group by date(date),cash_type order by date(date)";
				 
			 }
			 else	{
				 selectQuery = "SELECT sum(totalAmmount) as amount,extra1,emp_id, date,cash_type FROM customerpurchases where date between '"+fromdate+"' and '"+todate+"' and extra1 = '"+dept+"' and cash_type != 'offerKind' and  cash_type != 'journalvoucher' and cash_type != 'online pending' and cash_type != 'othercash' group by date,cash_type order by date";
			 }
//			 System.out.println("selectQuery---"+selectQuery);
			 try	{
				con = ConnectionHelper.getConnection();
				st = con.createStatement();
				rs = st.executeQuery(selectQuery);
				detailsMap = new MultiValueMap();
				while(rs.next())	{
					customerpurchases cp = new customerpurchases();
					cp.settotalAmmount(rs.getString("amount"));
					cp.setextra1(rs.getString("extra1"));
					cp.setdate(rs.getString("date"));
					cp.setcash_type(rs.getString("cash_type"));
					
					detailsMap.put(rs.getString("date") , cp);
				}
			 }
			 catch(SQLException se)	{
				 se.printStackTrace();
			 }
			 catch(Exception e)	{
				 e.printStackTrace();
			 }
			 finally	{
				 try {
					if(con != null) con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				 try {
					 if(st != null) st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				 try {
					 if(rs != null) rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			 }
			 
			 return detailsMap;
		 }
		 else	return null;
	}
}
