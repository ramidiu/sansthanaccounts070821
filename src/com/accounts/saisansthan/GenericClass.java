package com.accounts.saisansthan;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dbase.sqlcon.ConnectionHelper;
import beans.banktransactions;

public class GenericClass {
	Statement st = null;
	ResultSet rs = null;
	PreparedStatement ps =null;
	Connection con = null;
	public List<banktransactions> getBankTransactionsBsdOnJvnumbers(StringBuilder[] numbers)	{
		List<banktransactions> bktLst = null;
		StringBuilder query =  new StringBuilder("select b.banktrn_id,b.bank_id,b.date,b.narration,b.amount,b.createdBy,b.extra3,b.sub_head_id,b.minor_head_id,b.major_head_id,b.head_account_id,mh.name,mn.name,s.name from banktransactions b left outer join majorhead mh on (mh.major_head_id = b.major_head_id) left outer join minorhead mn on (mn.minorhead_id = b.minor_head_id) left outer join subhead s on (s.sub_head_id = b.sub_head_id) where ");
		if (numbers != null && numbers.length > 0)	{
			for (int i = 0 ; i < numbers.length ; i++)	{
				if (numbers[i] != null && !numbers[i].toString().trim().equals(""))	{
					query.append("b.extra3 = '").append(numbers[i]).append("'");
					if (i >= 0 && i != numbers.length - 1) query.append(" or ");
				}
			}
//			System.out.println("quert---"+query);
			try	{
				con = ConnectionHelper.getConnection();
				st = con.createStatement();
				rs = st.executeQuery(query.toString());
				bktLst = new ArrayList<banktransactions>();
				while (rs.next())	{
					banktransactions bkt = new banktransactions();
					bkt.setbanktrn_id(rs.getString("b.banktrn_id"));
					bkt.setbank_id(rs.getString("b.bank_id"));
					bkt.setdate(rs.getString("b.date"));
					bkt.setnarration(rs.getString("b.narration"));
					bkt.setamount(rs.getString("b.amount"));
					bkt.setcreatedBy(rs.getString("b.createdBy"));
					bkt.setextra3(rs.getString("b.extra3"));
					bkt.setSub_head_id(rs.getString("b.sub_head_id"));
					bkt.setMinor_head_id(rs.getString("b.minor_head_id"));
					bkt.setMajor_head_id(rs.getString("b.major_head_id"));
					bkt.setHead_account_id(rs.getString("b.head_account_id"));
					bkt.setExtra10(rs.getString("mh.name"));
					bkt.setExtra11(rs.getString("mn.name"));
					bkt.setExtra12(rs.getString("s.name"));
					
					bktLst.add(bkt);
				}
			}
			catch(SQLException se)	{
				se.printStackTrace();
			}
			catch(Exception e)	{
				e.printStackTrace();
			}
			finally	{
				try {
					if (con != null) con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				try {
					if(st != null) st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				try {
					if(rs != null) rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return bktLst;
	}
}
