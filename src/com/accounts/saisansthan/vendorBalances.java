package com.accounts.saisansthan;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import beans.bankbalance;
import mainClasses.bankbalanceListing;
import mainClasses.godwanstockListing;
import mainClasses.godwanstock_scListing;
import mainClasses.paymentsListing;
import mainClasses.productexpenses_scListing;

public class vendorBalances {
	HashMap<String, Double> pendingBal=new HashMap<String, Double>();
mainClasses.godwanstockListing GDSTCKL=new godwanstockListing();
mainClasses.godwanstockListing GDSTCKL1=new godwanstockListing();
mainClasses.godwanstock_scListing GDSTSCKL=new godwanstock_scListing();
mainClasses.paymentsListing PAYMNTL=new paymentsListing();
mainClasses.productexpenses_scListing PRDES = new productexpenses_scListing();
beans.godwanstock GDSTCK=new beans.godwanstock();
beans.payments PAY=new beans.payments();
List VendrList=null;
double vendoramount=0.0;
double payamount=0.0;
List PaymntList=null;

	public HashMap<String, Double> getVendorBalances(String HOA,String fromdate,String todate){
		VendrList=GDSTCKL.getMSEbyVendorByDate(HOA,todate);
		PaymntList=PAYMNTL.getpaymentsByDateByHOA(HOA,todate);
		if(VendrList.size()>0){
			for(int i=0;i<VendrList.size();i++){
				GDSTCK=(beans.godwanstock)VendrList.get(i);
				if(PaymntList.size()>0){
					for(int j=0;j<PaymntList.size();j++){
						PAY=(beans.payments)PaymntList.get(j);
						if(PAY.getvendorId().equals(GDSTCK.getvendorId()) ){
							vendoramount=Double.parseDouble(GDSTCK.getMRNo());
							payamount=Double.parseDouble(PAY.getTBECResolutionNO());
							pendingBal.put(GDSTCK.getvendorId(), vendoramount-payamount);
							break;
							}
						
					}
				}
				}
				for(int i=0;i<VendrList.size();i++){
					GDSTCK=(beans.godwanstock)VendrList.get(i);
					if(!pendingBal.containsKey(GDSTCK.getvendorId())){
						vendoramount=Double.parseDouble(GDSTCK.getMRNo());
						pendingBal.put(GDSTCK.getvendorId(), vendoramount);
					}
					
				}
			}
		
		return pendingBal;
		
	}
	
	public HashMap<String, Double> getVendorBalancesForFinYr(String HOA,String fromdate,String todate,String finYr){
		/*VendrList=GDSTCKL.getMSEbyVendorByDate(HOA,todate);
		PaymntList=PAYMNTL.getpaymentsByDateByHOA(HOA,todate);*/
		VendrList=GDSTCKL.getMSEbyVendorBtwDates(HOA,fromdate,todate);
		PaymntList=PAYMNTL.getpaymentsBtwDatesByHOA(HOA,fromdate,todate);
		
		bankbalanceListing BBL = new bankbalanceListing();
		if(VendrList.size()>0)
		{
			for(int i=0;i<VendrList.size();i++)
			{
				GDSTCK=(beans.godwanstock)VendrList.get(i);
				if(PaymntList.size()>0)
				{
					for(int j=0;j<PaymntList.size();j++)
					{
						PAY=(beans.payments)PaymntList.get(j);
						if(PAY.getvendorId().equals(GDSTCK.getvendorId()))
						{
							vendoramount=Double.parseDouble(GDSTCK.getMRNo());
							payamount=Double.parseDouble(PAY.getTBECResolutionNO());
							double vendorAmountOpeningBal = BBL.getVendorPendingOpeningBal(GDSTCK.getvendorId(), "Liabilites", finYr, "", "");
							pendingBal.put(GDSTCK.getvendorId(), vendorAmountOpeningBal+vendoramount-payamount);
							break;
						}
						
					}
				}
			}
			
			for(int i=0;i<VendrList.size();i++)
			{
				GDSTCK=(beans.godwanstock)VendrList.get(i);
				if(!pendingBal.containsKey(GDSTCK.getvendorId()))
				{
					vendoramount=Double.parseDouble(GDSTCK.getMRNo());
					double vendorAmountOpeningBal = BBL.getVendorPendingOpeningBal(GDSTCK.getvendorId(), "Liabilites", finYr, "", "");
					pendingBal.put(GDSTCK.getvendorId(), vendorAmountOpeningBal+vendoramount);
				}
				
			}
		}
		List finYrVendorsList = BBL.getAssetsLiabilitiesOfFinYrAndHOA("Liabilites",finYr,HOA);
		double vendorPendingBalFinYr = 0.0;
		bankbalance BB = new bankbalance();
		for(int i=0;i<finYrVendorsList.size();i++)
		{
			BB=(beans.bankbalance)finYrVendorsList.get(i);
			if(!pendingBal.containsKey(BB.getExtra6()))
			{
				vendorPendingBalFinYr=Double.parseDouble(BB.getBank_bal());
				pendingBal.put(BB.getExtra6(), vendorPendingBalFinYr);
			}	
		}
		return pendingBal;	
	}
	public HashMap<String, Double> getVendorBalancesForFinYr1(String HOA,String fromdate,String todate,String finYr, String cumltv){
		/*VendrList=GDSTCKL.getMSEbyVendorByDate(HOA,todate);
		PaymntList=PAYMNTL.getpaymentsByDateByHOA(HOA,todate);*/
		/*
		String lastFinlastYera[] = fromdate.split("-");
		String lastFinYearfirst = (Integer.parseInt(lastFinlastYera[0]) - 1) + "-04-01" ;*/
		String lastFinfirstYear[]  = todate.split("-");
		String lastFinYearlase  = (Integer.parseInt(lastFinfirstYear[0]) - 1) + "-03-31" ;
		String startDate = "2015-04-01";
		//System.out.println(fromdate);
		if(fromdate.contains("2015-04-01 00:00:00")){
			VendrList=GDSTCKL.getMSEbyVendorBtwDates1(HOA,"2015-03-31 00:00:00",todate);
		}
		else{
		VendrList=GDSTCKL.getMSEbyVendorBtwDates1(HOA,fromdate,todate);
		
		}
		
		PaymntList=PAYMNTL.getpaymentsBtwDatesByHOA1(HOA,fromdate,todate);
		
		
		bankbalanceListing BBL = new bankbalanceListing();
		if(VendrList.size()>0)
		{
			for(int i=0;i<VendrList.size();i++)
			{
				GDSTCK=(beans.godwanstock)VendrList.get(i);
				
				if(PaymntList.size()>0)
				{
					for(int j=0;j<PaymntList.size();j++)
					{
						PAY=(beans.payments)PaymntList.get(j);
						if(PAY.getvendorId().trim().equals(GDSTCK.getvendorId().trim()))
						{
							
							vendoramount=Double.parseDouble(GDSTCK.getExtra16());							
							payamount=Double.parseDouble(PAY.getamount());
							double vendorAmountOpeningBal = 0.0;
							double amount = vendoramount - payamount;
							if(cumltv.equals("cumulative")){
								//vendorAmountOpeningBal = BBL.getVendorPendingOpeningBal(GDSTCK.getvendorId(), "Liabilites", finYr, "", "");
								
							String openginpending = GDSTCKL1.OpeningPendingBalanceByDept(GDSTCK.getvendorId(), startDate, lastFinYearlase , HOA);
							
							if(fromdate.equals("2016-04-01 00:00:00") && (GDSTCK.getvendorId().equals("1223") || GDSTCK.getvendorId().equals("1225"))){
								openginpending = "0.00";
							}
							if(openginpending != null && !openginpending.trim().equals("")){						
									vendorAmountOpeningBal = Double.parseDouble(openginpending);							
								}
							
							}
							
								pendingBal.put(GDSTCK.getvendorId().trim(), (vendorAmountOpeningBal+vendoramount)-payamount);						
							break;
						}			
					}
				}
			}
			for(int i=0;i<VendrList.size();i++)
			{
				GDSTCK=(beans.godwanstock)VendrList.get(i);
				if(!pendingBal.containsKey(GDSTCK.getvendorId().trim()))
				{
					vendoramount=Double.parseDouble(GDSTCK.getExtra16());
					double vendorAmountOpeningBal = 0.0;
					if(cumltv.equals("cumulative")){					
					//	vendorAmountOpeningBal = BBL.getVendorPendingOpeningBal(GDSTCK.getvendorId(), "Liabilites", finYr, "", "");
						String openginpending = GDSTCKL1.OpeningPendingBalanceByDept(GDSTCK.getvendorId(), startDate, lastFinYearlase , HOA);							
						if(openginpending != null && !openginpending.equals("")){			
								vendorAmountOpeningBal = Double.parseDouble(openginpending);
						}
					}
					pendingBal.put(GDSTCK.getvendorId(), vendorAmountOpeningBal+vendoramount);
				}	
			}
		}
		/*if(cumltv.equals("cumulative")){
		List finYrVendorsList = BBL.getAssetsLiabilitiesOfFinYrAndHOA("Liabilites",finYr,HOA);
		double vendorPendingBalFinYr = 0.0;
		bankbalance BB = new bankbalance();
		for(int i=0;i<finYrVendorsList.size();i++)
		{
			BB=(beans.bankbalance)finYrVendorsList.get(i);
			if(!pendingBal.containsKey(BB.getExtra6()))
			{
				vendorPendingBalFinYr=Double.parseDouble(BB.getBank_bal());
				pendingBal.put(BB.getExtra6(), vendorPendingBalFinYr);
			}		
		}
	}*/
		return pendingBal;
}
	public HashMap<String, Double> getVendorBalancesBasedOnDates(String HOA,String fromdate,String todate){
		VendrList=GDSTSCKL.getMSEbyVendorBasedOnDates(HOA,fromdate,todate);
		PaymntList=PRDES.getpaymentsByDateByHOA(HOA, fromdate, todate);
		if(VendrList.size()>0){
			for(int i=0;i<VendrList.size();i++){
				GDSTCK=(beans.godwanstock)VendrList.get(i);
				if(PaymntList.size()>0){
					for(int j=0;j<PaymntList.size();j++){
						PAY=(beans.payments)PaymntList.get(j);
						if(PAY.getvendorId().equals(GDSTCK.getvendorId()) ){
							vendoramount=Double.parseDouble(GDSTCK.getMRNo());
							payamount=Double.parseDouble(PAY.getTBECResolutionNO());
							pendingBal.put(GDSTCK.getvendorId(), vendoramount-payamount);
							break;
						}			
					}
				}
			}
				for(int i=0;i<VendrList.size();i++){
					GDSTCK=(beans.godwanstock)VendrList.get(i);
					if(!pendingBal.containsKey(GDSTCK.getvendorId())){
						vendoramount=Double.parseDouble(GDSTCK.getMRNo());
						pendingBal.put(GDSTCK.getvendorId(), vendoramount);
					}		
				}
			}
		return pendingBal;	
	}
}
