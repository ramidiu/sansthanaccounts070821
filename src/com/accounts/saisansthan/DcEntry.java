package com.accounts.saisansthan;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mainClasses.no_genaratorListing;
import mainClasses.productsListing;
import beans.godwanstock;
import beans.godwanstockService;
import beans.invoice;
import beans.invoiceService;
import beans.no_genaratorService;
import beans.productsService;
@WebServlet("/DcEntry")
public class DcEntry extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		
		if (session.getAttribute("empId") != null)	{
			no_genaratorService no_genaratorService = new no_genaratorService();
//			productsListing prod_list =new productsListing();
			no_genaratorListing nogen_l=new no_genaratorListing();
//			productsService prodservice = new productsService();
			invoiceService invService = new invoiceService();
			godwanstockService gservice = new godwanstockService();
			
			Calendar c1 = Calendar.getInstance(); 
			c1.setTimeZone(TimeZone.getTimeZone("IST"));
			DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			int count = Integer.parseInt(request.getParameter("rowcount"));
			String dcBill = request.getParameter("dcBill");
			String productid = "",vendorid="",quantity="",rate="",total="",gtotal="",billtype="",mjid="",mnid="",sid="",expId="",narration="",department="",trackingnumber="",empId="",appDate="",goadst="",shopst="",billamount="",mseId="",dcnumber="";
//			double godstock=0;
//			double shopstock=0;
			billtype = request.getParameter("billtype");
			gtotal = request.getParameter("gtotal");
			narration = request.getParameter("narr");
			empId = session.getAttribute("empId").toString();
			expId = no_genaratorService.getIdAndUpdateBasedOnTableName("expenseinvoice");
			appDate = (dateFormat.format(c1.getTime())).toString();
			try	{
				
				if (billtype.equals("newdc"))	{
					department = request.getParameter("hoa");
					vendorid = request.getParameter("vendor");
					dcnumber = nogen_l.getid("godownstock"); 
				}
				else	{
					department = request.getParameter("vho");
					vendorid = request.getParameter("oldvd");
					dcnumber = request.getParameter("dcNumber"); 
				}
				
					if(department.equals("3")){
						trackingnumber=nogen_l.getid("poojastoreTracking");
						mseId=nogen_l.getid("invoice");
						no_genaratorService.updatInvoiceNumber(trackingnumber,"poojastoreTracking");	
					}
					else if(department.equals("4")){
						trackingnumber=nogen_l.getid("sansthanTracking");
						mseId=nogen_l.getid("invoice");
						no_genaratorService.updatInvoiceNumber(trackingnumber,"sansthanTracking");	
					}
					else if(department.equals("1")){
						trackingnumber=nogen_l.getid("charityTracking");
						mseId=nogen_l.getid("invoice");
						no_genaratorService.updatInvoiceNumber(trackingnumber,"charityTracking");	
					}
					else if(department.equals("5")){
						trackingnumber=nogen_l.getid("sainivasTracking");
						mseId=nogen_l.getid("sainivas_mse_invId");
						no_genaratorService.updatInvoiceNumber(trackingnumber,"sainivasTracking");	
					}
					else if(department.equals("10")){
						trackingnumber=nogen_l.getid("ewfTracking");
						mseId=nogen_l.getid("invoice");
						no_genaratorService.updatInvoiceNumber(trackingnumber,"ewfTracking");	
					}
			
			}
			catch(SQLException se)	{
				se.printStackTrace();
			}
			catch(Exception e)	{
				e.printStackTrace();
			}

			for (int i=0 ; i<count ; i++)	{
				
				productid = request.getParameter("product"+i);
				 if (!productid.trim().equals(""))	{
					 godwanstock gds = new godwanstock();
					 quantity = request.getParameter("qty"+i);
					 rate = request.getParameter("rate"+i);
					 total = request.getParameter("total"+i);
					 mjid = request.getParameter("mjid"+i);
					 mnid = request.getParameter("mnid"+i);
					 sid = request.getParameter("sid"+i);

					 gds.setProductId(productid.split(" ")[0]);
					 gds.setQuantity(quantity);
					 gds.setpurchaseRate(rate);
					 gds.setExtra24(total);
					 gds.setExtra16(gtotal);
					 gds.setMajorhead_id(mjid);
					 gds.setMinorhead_id(mnid); 
					 gds.setExtra23(sid); // subhead
					 gds.setExtra1(mseId);
					 gds.setExtra9(trackingnumber);
					 gds.setExtra21(expId);
					 gds.setExtra17("0");
					 gds.setExtra18("0");
					 gds.setExtra19("0");
					 gds.setExtra20("0");
					 gds.setVat("0");
					 if(billtype.equals("newdc"))	{
						 gds.setVendorId(vendorid.split(" ")[0]);
					 }
					 else	{
						 gds.setVendorId(vendorid);
					 }
					 gds.setDescription(narration);
					 gds.setBillNo(dcBill);
					 gds.setDate(appDate);
					 gds.setDepartment(department);
					 gds.setCheckedby("");
					 gds.setCheckedtime("");
					 gds.setApproval_status("dcnotapproved");
					 gds.setExtra26(billtype);
					 gds.setExtra25(dcnumber);
					 gds.setEmp_id(empId);
					 
					 /*godstock = Double.parseDouble(prod_list.getProductsStockGodwan(productid));
					 godstock=godstock+Double.parseDouble(quantity); 
					 goadst=String.valueOf(godstock);
					 if(department.equals("3")){
					 shopstock=Double.parseDouble(prod_list.getProductsStockStore(productid));
					 shopstock=shopstock+Double.parseDouble(quantity); 
					 shopst=String.valueOf(shopstock);
					 }*/
					 
					 if(session.getAttribute("Role")!=null && session.getAttribute("Role").toString().equals("PO")){
						 gds.setProfcharges("unchecked");
						 gds.setPo_approved_status("Approved");
						 gds.setPo_approved_by(empId);
						 gds.setPo_approved_date(appDate);
						 
					 }
					 else	{
						 gds.setProfcharges("UncheckedByPO");
					 }
					 gds.setExtra7("dcbill");
					 gservice.insertGodownStock(gds);
//					 prodservice.updateStock(productid,shopst,goadst,department);
				 } // if
			} // for loop
			
			invoice inv = new invoice();
			inv.setheadAccountId(department);
			inv.setinv_id(mseId);
			inv.setextra2(trackingnumber);
			 if(billtype.equals("newdc"))	{
				 inv.setvendorid(vendorid.split(" ")[0]);
			 }
			 else	{
				 inv.setvendorid(vendorid);
			 }
			inv.setamount(gtotal);
			inv.setpaid("00");
			inv.setpendingamount(gtotal);
			inv.settype("");
			inv.setemp_id(empId);
			inv.setextra1("");
			inv.setcreatedBy(appDate);
			invService.insertInvoice(inv);
			
			response.sendRedirect("directChallanEntry.jsp?bid="+mseId);
		}
	}

}
