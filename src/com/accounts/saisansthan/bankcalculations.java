package com.accounts.saisansthan;

import java.text.ParseException;
import java.util.List;
import java.util.TimeZone;

import beans.bankdetails;
import beans.banktransactions;

import mainClasses.banktransactionsListing;
import mainClasses.customerpurchasesListing;
import model.SansthanAccountsDate;

/**
 * @author Pruthvi
 * 
 *         tags
 */
public class bankcalculations {
	customerpurchasesListing CP_L=new customerpurchasesListing();
	mainClasses.banktransactionsListing BAK_L=new banktransactionsListing();
	beans.banktransactions BKT=new banktransactions();
double reconsopnbankBal=0.0;
double paidtovendors=0.0;
double bankopeningbalance=0.0;
double BTotBal=0.0;
	public double getBankReconciliationOpeningBalance(String bank_id,String todate){
		List BANKL=BAK_L.getBankTransactionBetweenDates(bank_id,"",todate,"",true,"");
		if(BANKL.size()>0){
			for(int i=0;i<BANKL.size();i++){
			BKT=(beans.banktransactions)BANKL.get(i);	
			if(BKT.gettype().equals("withdraw") || BKT.gettype().equals("pettycash") || BKT.gettype().equals("cashpaid") || BKT.gettype().equals("cheque")){
				reconsopnbankBal=reconsopnbankBal+Double.parseDouble(BKT.getamount());
			}
			else if(BKT.gettype().equals("deposit")){
				reconsopnbankBal=reconsopnbankBal-Double.parseDouble(BKT.getamount());
				}
	}
}
		return reconsopnbankBal;
}
	public double getBankOpeningBalance(String bank_id,String fromdate,String todate,String reportType){

		paidtovendors=Double.parseDouble(BAK_L.getOpeningBalAmountPaidToVendors(bank_id,todate,reportType));
		bankopeningbalance=Double.parseDouble(BAK_L.getOpeningBalance(bank_id,todate,reportType))-paidtovendors;
		return bankopeningbalance;
}
	public double getBankOpeningBalance(String bank_id,String fromdate,String todate,String reportType,String dummy){

		paidtovendors = Double.parseDouble(BAK_L.getOpeningBalAmountPaidToVendors(bank_id,fromdate,todate,reportType));
//		System.out.println("paidtovendors >>>>>> "+paidtovendors);
		bankopeningbalance = Double.parseDouble(BAK_L.getOpeningBalance(bank_id,fromdate,todate,reportType))-paidtovendors;
//		System.out.println("bankopeningbalance >>>>>> "+bankopeningbalance);
		return bankopeningbalance;
		
}
	public double getCreditBankOpeningBalance(String bank_id,String fromdate,String todate,String dummy){

		double opening=Double.parseDouble(BAK_L.getCreditBanksTotalAmt(bank_id,fromdate,todate,"opening",dummy));
		double deposit=Double.parseDouble(BAK_L.getCreditBanksTotalAmt(bank_id,fromdate,todate,"deposit",dummy));
		double withdraw=Double.parseDouble(BAK_L.getCreditBanksTotalAmt(bank_id,fromdate,todate,"withdraw",dummy));
		double result = opening+withdraw-deposit;
		return result;
}
	
	public double getBankReconciliationClosingBalance(String bank_id,String NextDay){
		double bankTotBal=0.0;
		double pettyCashTotBal=0.0;
		if(BAK_L.getBalanceByDeposit(bank_id,NextDay,"deposit")!=null && !BAK_L.getBalanceByDeposit(bank_id,NextDay,"deposit").equals("")){
			bankTotBal=Double.parseDouble(BAK_L.getBalanceByDeposit(bank_id,NextDay,"deposit"));
		}else{
			bankTotBal=0;
		}
		if(BAK_L.getBalanceByNOTDeposit(bank_id,NextDay,"cheque","transfer","pettycash","withdraw")!=null && !BAK_L.getBalanceByNOTDeposit(bank_id,NextDay,"cheque","transfer","pettycash","withdraw").equals("")){
			pettyCashTotBal=Double.parseDouble(BAK_L.getBalanceByNOTDeposit(bank_id,NextDay,"cheque","transfer","pettycash","withdraw"));
		}else{
			pettyCashTotBal=0.00;
		}
		BTotBal=bankTotBal-pettyCashTotBal;
		return BTotBal;
		
	}
	public double getProCashOpeningBalance(String fromdate,String Hoid,String reporttype,String reporttype1){
		
			String addedDate1="";
			double totalcountercash =0.0;
			double totalcashpaid =0.0;
			double grandtotal = 0.0;
			double proopeningbalance=0.0;
			try {
				addedDate1 = SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  if(CP_L.getTotalAmountBeforeFromdate(Hoid,"2015-04-01", fromdate) != null && !CP_L.getTotalAmountBeforeFromdate(Hoid,"2015-04-01", fromdate).equals("")){
			  totalcountercash = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate(Hoid,"2015-04-01", fromdate));
		  }
		  if(BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,"2015-04-02",addedDate1+" 00:00:00")!= null && !BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,"2015-04-02", addedDate1+" 00:00:00").equals("")){
			 totalcashpaid = Double.parseDouble(BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,"2015-04-02", addedDate1+" 00:00:00"));
		  }
		  grandtotal = totalcountercash-totalcashpaid;
		  proopeningbalance = grandtotal;
		
		return proopeningbalance;
	}
	public double getProCashOpeningBalance(String finStartDate,String fromdate,String Hoid,String reporttype,String reporttype1){
		
		String addedDate1="";
		double totalcountercash =0.0;
		double totalcashpaid =0.0;
		double grandtotal = 0.0;
		double proopeningbalance=0.0;
		try {
			addedDate1 = SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  if(CP_L.getTotalAmountBeforeFromdate(Hoid,finStartDate, fromdate) != null && !CP_L.getTotalAmountBeforeFromdate(Hoid,finStartDate, fromdate).equals("")){
		  if(Hoid.equals("4") || Hoid.equals("1")){
		  totalcountercash = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate(Hoid,finStartDate, fromdate));}
	  }
	  if(BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartDate,addedDate1+" 00:00:00")!= null && !BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartDate, addedDate1+" 00:00:00").equals("")){
		 totalcashpaid = Double.parseDouble(BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartDate, addedDate1+" 00:00:00"));
	  }
	  grandtotal = totalcountercash-totalcashpaid;
	  proopeningbalance = grandtotal;
	
	return proopeningbalance;
}
	public double getProCashClosingBalance(String todate,String Hoid,String reporttype,String reporttype1){
		String addedDate1="";
		double totalcountercash =0.0;
		double totalcashpaid =0.0;
		double grandtotal = 0.0;
		double proclosingbalance=0.0;
		try {
			addedDate1 = SansthanAccountsDate.getAddedDate(todate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  if(CP_L.getTotalAmountBeforeFromdate(Hoid,"2015-04-01", todate) != null && !CP_L.getTotalAmountBeforeFromdate(Hoid,"2015-04-01", todate).equals("")){		  
		  totalcountercash = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate(Hoid,"2015-04-01", todate));
	  }
	  if(BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,"2015-04-02",addedDate1+" 23:59:59")!= null && !BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,"2015-04-02", addedDate1+" 23:59:59").equals("")){
		 totalcashpaid = Double.parseDouble(BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,"2015-04-02", addedDate1+" 23:59:59"));
	  }
	  grandtotal = totalcountercash-totalcashpaid;
	  proclosingbalance = grandtotal;
	
	return proclosingbalance;
}
	public double getProCashClosingBalance(String finStartDate,String todate,String Hoid,String reporttype,String reporttype1){
		String addedDate1="";
		double totalcountercash =0.0;
		double totalcashpaid =0.0;
		double grandtotal = 0.0;
		double proclosingbalance=0.0;
		try {
			addedDate1 = SansthanAccountsDate.getAddedDate(todate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  if(CP_L.getTotalAmountBeforeFromdate(Hoid,finStartDate, todate) != null && !CP_L.getTotalAmountBeforeFromdate(Hoid,finStartDate, todate).equals("")){
		  if(Hoid.equals("4") || Hoid.equals("1")){
		  totalcountercash = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate(Hoid,finStartDate, todate));}
	  }
	  if(BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartDate,addedDate1+" 23:59:59")!= null && !BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartDate, addedDate1+" 23:59:59").equals("")){
		 totalcashpaid = Double.parseDouble(BAK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartDate, addedDate1+" 23:59:59"));
	  }
	  grandtotal = totalcountercash-totalcashpaid;
	  proclosingbalance = grandtotal;
	
	return proclosingbalance;
}
	
	
	}
