package com.accounts.saisansthan;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import mainClasses.booking_roomsdetailsListing;
import mainClasses.bookingroomsListing;
import mainClasses.roomtypeListing;
import beans.advance_amount;
import beans.advance_amountService;
import beans.roomtype;

public class InsertAdvanceAmount 
{
	advance_amount advamt=new advance_amount();
	advance_amountService service=new advance_amountService();
	roomtype RMTY=new roomtype();
	
	public boolean insertAdvanceAmount()
	{
		boolean flag=false;
		
		Date today=new Date();
		SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		format1.setTimeZone(TimeZone.getTimeZone("IST"));
		
		String now=format1.format(today);
		advamt.setDatetime(now);
		System.out.println(now);
		
		bookingroomsListing BK_CL=new bookingroomsListing();
		roomtypeListing RM_TY=new roomtypeListing();
		booking_roomsdetailsListing BKR_CL=new booking_roomsdetailsListing();
		List roomtypes=RM_TY.getroomtype();
		if(roomtypes.size()!=0)
		{
			for(int i=0;i<roomtypes.size();i++)
			{
				RMTY=(roomtype)roomtypes.get(i);
				String adv=BKR_CL.getAdvanceAmount(RMTY.getroomtype_id(),now);
				
				if(RMTY.getroomtype_name().equals("Master Suite"))
				{
					advamt.setMaster_suite(adv);
				}
				else if(RMTY.getroomtype_name().equals("Executive Suite"))
				{
					advamt.setExecutive_suite(adv);
				}
				else if(RMTY.getroomtype_name().equals("Deluxe Suite"))
				{
					advamt.setDeluxe_suite(adv);
				}
				else if(RMTY.getroomtype_name().equals("Suite"))
				{
					advamt.setSuite(adv);
				}
				else if(RMTY.getroomtype_name().equals("Deluxe Room (A/C)"))
				{
					advamt.setDeluxe_room_ac(adv);
				}
				else if(RMTY.getroomtype_name().equals("Special Room ( NON A/C )"))
				{
					advamt.setSpecial_room_nonac(adv);
				}
				else if(RMTY.getroomtype_name().equals("Deluxe Room (NON A/C)"))
				{
					advamt.setDeluxe_room_nonac(adv);
				}
				else if(RMTY.getroomtype_name().equals("Deluxe Room (A/c) - 2 Beds"))
				{
					advamt.setDeluxe_room_ac_2beds(adv);
				}
				else if(RMTY.getroomtype_name().equals("Banquet Hall - Vth Floor"))
				{
					advamt.setBanquet_hall_5th_floor(adv);
				}
				else if(RMTY.getroomtype_name().equals("Banquet Hall - Ivth Floor"))
				{
					advamt.setBanquet_hall_4th_floor(adv);
				}

			}
			service.insert(advamt);
		}
		
		
		return flag;
	}
}
