package com.accounts.saisansthan;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class attendancecalculation {
	float OP_bal=0,AT_days=0,temp_CL=0,LA=0,LP=0,CL=0,AB=0,Usd_CL=0,OT=0,OD=0,To_Cl=0,carry_cl=0,cal_LA=0,cal_LP=0,cal_Cl=0,fr_cal_Cl=0,cal_AB=0;
		String openingbalance;
	String monthyear;
	List<beans.employee_attendence> empattendce;
	mainClasses.openingcl_balListing op_cl_bal=new mainClasses.openingcl_balListing();
	mainClasses.employee_attendenceListing EMA_CL = new mainClasses.employee_attendenceListing();
	beans.employee_attendence EMAF=new beans.employee_attendence();
	public attendancecalculation() {
		this.openingbalance ="";
		empattendce=null;
		monthyear="";
		OP_bal=0;
		temp_CL=0;
		AT_days=0;
		LA=0;LP=0;CL=0;AB=0;OT=0;OD=0;carry_cl=0;
	}


	public Map<String, String> getAttendanceEmployee(String empid,String month,String year){
		monthyear="";
		OP_bal=0;
		temp_CL=0;
		AT_days=0;
		LA=0;LP=0;CL=0;AB=0;OT=0;OD=0;carry_cl=0;
		HashMap<String, String> attendce = new HashMap();
		monthyear=year+"-"+month;
		openingbalance =op_cl_bal.getopeningcl_bal(empid,""+(Integer.parseInt(month)-1),year);
		if(!openingbalance.equals("")){
			OP_bal=(float)Double.parseDouble(openingbalance);
		}
		empattendce=EMA_CL.getEmployeeAttendceMonth(empid,monthyear);
		if(empattendce!=null){
		for(int t=0; t < empattendce.size(); t++ ){
			AT_days++;
			EMAF=(beans.employee_attendence)empattendce.get(t);
			if(!EMAF.getlate_hours().equals("") && !EMAF.getlate_hours().equals(null)){
				
				temp_CL=Float.parseFloat(EMAF.getlate_hours());
			}else{
				temp_CL=0;
			}
			if(EMAF.getlate_hourpermision().equals("Yes")){LP=LP+1;} 
			if(EMAF.getextra5().equals("LA")){LA=LA+1;} 
			if(EMAF.getextra5().equals("CL")){CL=CL+temp_CL;}
			if(EMAF.getextra5().equals("Absent")){AB=AB+1;AT_days--;}
			if(EMAF.getextra5().equals("OD")){OD=OD+1;}
			if(!EMAF.getextar7().equals("")){OT=OT+Float.parseFloat(EMAF.getextar7());}
			}}
		carry_cl=AT_days/5;
		carry_cl=(float)(Math.ceil(carry_cl*2)/2);
		if(carry_cl>=6){carry_cl=6;}
		cal_LP=(int)(LP/3); 
		cal_LA=(int)(LA/3);
		To_Cl=carry_cl+OP_bal; 
		Usd_CL=cal_LA+cal_LP;
		 if(CL>(int)CL){
			for(int a=(int)CL;a>0;a--){
				float temp=0;
				if((int)Usd_CL>=(int)To_Cl){
					temp=(float)1.5;
				}else{
					temp=1;
				}
				cal_Cl=cal_Cl+temp;
				Usd_CL=Usd_CL+temp;
			}
			if((int)Usd_CL>=(int)To_Cl){
				fr_cal_Cl=(float)0.75;
				
			}else{
				fr_cal_Cl=(float)0.5;
			}
			cal_Cl=cal_Cl+fr_cal_Cl;
			Usd_CL=Usd_CL+fr_cal_Cl;
			
		}else{
			for(int a=(int)CL;a>0;a--){
				float temp=0;
				if((int)Usd_CL>=(int)To_Cl){
					temp=(float)1.5;
				}else{
					temp=1;
				}
				cal_Cl=cal_Cl+temp;
				Usd_CL=Usd_CL+temp;
			}
	
		}
			for(int a=(int)AB;a>0;a--){ 
				float temp=(float)1.5;
				cal_AB=cal_AB+temp;
				Usd_CL=Usd_CL+temp;
				}
		
		attendce.put("empid", ""+empid);
		attendce.put("OP_bal", ""+OP_bal);
		attendce.put("LP", ""+LP);
		attendce.put("CL", ""+CL);
		attendce.put("AB", ""+AB);
		attendce.put("OD", ""+OD);
		attendce.put("OT", ""+OT);
		attendce.put("carry_cl", ""+carry_cl);
		attendce.put("AB", ""+AB);
		attendce.put("cal_AB", ""+cal_AB);
		attendce.put("cal_LP", ""+cal_LP);
		attendce.put("cal_LA", ""+cal_LA);
		attendce.put("Usd_CL", ""+Usd_CL);
		
		return attendce;
	}

}
