package dbase.sqlcon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class saiSansthanConnectionHelper {
	private String url;
	private String user= "root";
    private String pass= "mysql";
	//private String user= "sansthanaccounts";
   // private String pass= "password36";
	private static saiSansthanConnectionHelper instance;
	
	private saiSansthanConnectionHelper() {
		try {
			Class.forName("com.mysql.jdbc.Driver");

			//String str = URLDecoder.decode(getClass().getClassLoader().getResource("flex/testdrive").toString(), "UTF-8");

			url = "jdbc:mysql://192.168.1.99:3306/saisansthan";

			//url = "jdbc:mysql://localhost:3306/sansthanaccounts";

			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Connection getConnection() throws SQLException {
		if (instance == null) {
			instance = new saiSansthanConnectionHelper();
		}
		return DriverManager.getConnection(instance.url, instance.user, instance.pass);
	}

}
