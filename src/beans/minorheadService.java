package beans;

import java.sql.Connection;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class minorheadService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String minorhead_id="";
private String name="";
private String description="";
private String major_head_id="";
private String head_account_id="";
private String extra1="";
private String extra2="";
private String extra3="";

private Statement st = null;
private Connection c=null;
public minorheadService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setminorhead_id(String minorhead_id)
{
this.minorhead_id = minorhead_id;
}

public String getminorhead_id()
{
return (this.minorhead_id);
}


public void setname(String name)
{
this.name = name;
}

public String getname()
{
return (this.name);
}


public void setdescription(String description)
{
this.description = description;
}

public String getdescription()
{
return (this.description);
}


public void setmajor_head_id(String major_head_id)
{
this.major_head_id = major_head_id;
}

public String getmajor_head_id()
{
return (this.major_head_id);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}

public boolean insert()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
/*rs=st.executeQuery("select * from no_genarator where table_name='minorhead'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 minorhead_id=PriSt+ticketm;*/
String query="insert into minorhead values('"+minorhead_id+"','"+name+"','"+description+"','"+major_head_id+"','"+head_account_id+"','"+extra1+"','"+extra2+"','"+extra3+"')";
st.executeUpdate(query);
/*st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='minorhead'");*/
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean update()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"name = '"+name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",name = '"+name+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!major_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}


if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}
String query="update minorhead "+updatecc+" where minorhead_id='"+minorhead_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
public boolean update1(String majorhead,String report)
{
	//System.out.println("major...."+majorhead);
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="update minorhead set extra1='"+report+"' where major_head_id='"+majorhead+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
public boolean delete()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from minorhead where minorhead_id='"+minorhead_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}