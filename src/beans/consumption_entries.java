package beans;
public class consumption_entries 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String consumption_id="";
private String entry_date="";
private String consumption_category="";
private String con_sub_category="";
private String head_account_id="";
private String narration="";
private String items_qty="";
private String devotees_qty="";
private String damage_qty="";
private String leftover_qty="";
private String emp_id="";
private String consumption_invoice_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
public consumption_entries() {} 
public consumption_entries(String consumption_id,String entry_date,String consumption_category,String con_sub_category,String head_account_id,String narration,String items_qty,String devotees_qty,String damage_qty,String leftover_qty,String emp_id,String consumption_invoice_id,String extra1,String extra2,String extra3,String extra4,String extra5,String extra6,String extra7)
 {
this.consumption_id=consumption_id;
this.entry_date=entry_date;
this.consumption_category=consumption_category;
this.con_sub_category=con_sub_category;
this.head_account_id=head_account_id;
this.narration=narration;
this.items_qty=items_qty;
this.devotees_qty=devotees_qty;
this.damage_qty=damage_qty;
this.leftover_qty=leftover_qty;
this.emp_id=emp_id;
this.consumption_invoice_id=consumption_invoice_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.extra6=extra6;
this.extra7=extra7;

} 
public String getconsumption_id() {
return consumption_id;
}
public void setconsumption_id(String consumption_id) {
this.consumption_id = consumption_id;
}
public String getentry_date() {
return entry_date;
}
public void setentry_date(String entry_date) {
this.entry_date = entry_date;
}
public String getconsumption_category() {
return consumption_category;
}
public void setconsumption_category(String consumption_category) {
this.consumption_category = consumption_category;
}
public String getcon_sub_category() {
return con_sub_category;
}
public void setcon_sub_category(String con_sub_category) {
this.con_sub_category = con_sub_category;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getitems_qty() {
return items_qty;
}
public void setitems_qty(String items_qty) {
this.items_qty = items_qty;
}
public String getdevotees_qty() {
return devotees_qty;
}
public void setdevotees_qty(String devotees_qty) {
this.devotees_qty = devotees_qty;
}
public String getdamage_qty() {
return damage_qty;
}
public void setdamage_qty(String damage_qty) {
this.damage_qty = damage_qty;
}
public String getleftover_qty() {
return leftover_qty;
}
public void setleftover_qty(String leftover_qty) {
this.leftover_qty = leftover_qty;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String getconsumption_invoice_id() {
return consumption_invoice_id;
}
public void setconsumption_invoice_id(String consumption_invoice_id) {
this.consumption_invoice_id = consumption_invoice_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}

}