package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class journal_entriesService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String jv_id="";
private String created_date="";
private String sub_head_id="";
private String narration="";
private String debit_amount="";
private String credit_amount="";
private String jv_inv_no="";
private String voucher_ref_no="";
private String remarks="";
private String emp_id="";
private String status="";
private String head_account_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public journal_entriesService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setjv_id(String jv_id)
{
this.jv_id = jv_id;
}

public String getjv_id()
{
return (this.jv_id);
}


public void setcreated_date(String created_date)
{
this.created_date = created_date;
}

public String getcreated_date()
{
return (this.created_date);
}


public void setsub_head_id(String sub_head_id)
{
this.sub_head_id = sub_head_id;
}

public String getsub_head_id()
{
return (this.sub_head_id);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setdebit_amount(String debit_amount)
{
this.debit_amount = debit_amount;
}

public String getdebit_amount()
{
return (this.debit_amount);
}


public void setcredit_amount(String credit_amount)
{
this.credit_amount = credit_amount;
}

public String getcredit_amount()
{
return (this.credit_amount);
}


public void setjv_inv_no(String jv_inv_no)
{
this.jv_inv_no = jv_inv_no;
}

public String getjv_inv_no()
{
return (this.jv_inv_no);
}


public void setvoucher_ref_no(String voucher_ref_no)
{
this.voucher_ref_no = voucher_ref_no;
}

public String getvoucher_ref_no()
{
return (this.voucher_ref_no);
}


public void setremarks(String remarks)
{
this.remarks = remarks;
}

public String getremarks()
{
return (this.remarks);
}


public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}


public void setstatus(String status)
{
this.status = status;
}

public String getstatus()
{
return (this.status);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='journal_entries'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 jv_id=PriSt+ticketm;
String query="insert into journal_entries values('"+jv_id+"','"+created_date+"','"+sub_head_id+"','"+narration+"','"+debit_amount+"','"+credit_amount+"','"+jv_inv_no+"','"+voucher_ref_no+"','"+remarks+"','"+emp_id+"','"+status+"','"+head_account_id+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='journal_entries'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean update()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!created_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"created_date = '"+created_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",created_date = '"+created_date+"'";}
}

if(!sub_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!debit_amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"debit_amount = '"+debit_amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",debit_amount = '"+debit_amount+"'";}
}

if(!credit_amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"credit_amount = '"+credit_amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",credit_amount = '"+credit_amount+"'";}
}

if(!jv_inv_no.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"jv_inv_no = '"+jv_inv_no+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",jv_inv_no = '"+jv_inv_no+"'";}
}

if(!voucher_ref_no.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"voucher_ref_no = '"+voucher_ref_no+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",voucher_ref_no = '"+voucher_ref_no+"'";}
}

if(!remarks.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"remarks = '"+remarks+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",remarks = '"+remarks+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"status = '"+status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",status = '"+status+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update journal_entries "+updatecc+" where jv_id='"+jv_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from journal_entries where jv_id='"+jv_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}