package beans;
public class employepermissions 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String permission_id="";
private String emp_id="";
private String head_account_id="";
private String emp_permissions="";
private String description="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
public employepermissions() {} 
public employepermissions(String permission_id,String emp_id,String head_account_id,String emp_permissions,String description,String extra1,String extra2,String extra3,String extra4)
 {
this.permission_id=permission_id;
this.emp_id=emp_id;
this.head_account_id=head_account_id;
this.emp_permissions=emp_permissions;
this.description=description;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;

} 
public String getpermission_id() {
return permission_id;
}
public void setpermission_id(String permission_id) {
this.permission_id = permission_id;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getemp_permissions() {
return emp_permissions;
}
public void setemp_permissions(String emp_permissions) {
this.emp_permissions = emp_permissions;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}

}