package beans;
public class superadmin 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String admin_id="";
private String admin_name="";
private String user_name="";
private String password="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String image="";
private String start_date="";
private String end_date="";
private String last_login="";
private String ip_address="";
private String secret_ans="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
public superadmin() {} 
public superadmin(String admin_id,String admin_name,String user_name,String password,String extra1,String extra2,String extra3,String extra4,String extra5,String image,String start_date,String end_date,String last_login,String ip_address,String secret_ans,String extra6,String extra7,String extra8,String extra9)
 {
this.admin_id=admin_id;
this.admin_name=admin_name;
this.user_name=user_name;
this.password=password;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.image=image;
this.start_date=start_date;
this.end_date=end_date;
this.last_login=last_login;
this.ip_address=ip_address;
this.secret_ans=secret_ans;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;

} 
public superadmin(String admin_id,String admin_name,String user_name,String password,String extra1,String extra2,String extra3,String extra4,String extra5)
{
this.admin_id=admin_id;
this.admin_name=admin_name;
this.user_name=user_name;
this.password=password;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getadmin_id() {
return admin_id;
}
public void setadmin_id(String admin_id) {
this.admin_id = admin_id;
}
public String getadmin_name() {
return admin_name;
}
public void setadmin_name(String admin_name) {
this.admin_name = admin_name;
}
public String getuser_name() {
return user_name;
}
public void setuser_name(String user_name) {
this.user_name = user_name;
}
public String getpassword() {
return password;
}
public void setpassword(String password) {
this.password = password;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getimage() {
return image;
}
public void setimage(String image) {
this.image = image;
}
public String getstart_date() {
return start_date;
}
public void setstart_date(String start_date) {
this.start_date = start_date;
}
public String getend_date() {
return end_date;
}
public void setend_date(String end_date) {
this.end_date = end_date;
}
public String getlast_login() {
return last_login;
}
public void setlast_login(String last_login) {
this.last_login = last_login;
}
public String getip_address() {
return ip_address;
}
public void setip_address(String ip_address) {
this.ip_address = ip_address;
}
public String getsecret_ans() {
return secret_ans;
}
public void setsecret_ans(String secret_ans) {
this.secret_ans = secret_ans;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}
public String getextra8() {
return extra8;
}
public void setextra8(String extra8) {
this.extra8 = extra8;
}
public String getextra9() {
return extra9;
}
public void setextra9(String extra9) {
this.extra9 = extra9;
}

}