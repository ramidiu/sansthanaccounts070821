package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class locationsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String location_id="";
private String location_name="";
private String narration="";
private String emp_id="";
private String date="";
private String head_account_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public locationsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setlocation_id(String location_id)
{
this.location_id = location_id;
}

public String getlocation_id()
{
return (this.location_id);
}


public void setlocation_name(String location_name)
{
this.location_name = location_name;
}

public String getlocation_name()
{
return (this.location_name);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}


public void setdate(String date)
{
this.date = date;
}

public String getdate()
{
return (this.date);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}

public boolean insert()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='locations'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 location_id=PriSt+ticketm;
String query="insert into locations values('"+location_id+"','"+location_name+"','"+narration+"','"+emp_id+"','"+date+"','"+head_account_id+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"')";
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='locations'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean update()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!location_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"location_name = '"+location_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",location_name = '"+location_name+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date = '"+date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date = '"+date+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}
String query="update locations "+updatecc+" where location_id='"+location_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from locations where location_id='"+location_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}