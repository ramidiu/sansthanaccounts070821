package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import dbase.sqlcon.ConnectionHelper;
public class employeeDetailsService 
{
	private String employee_details_id;
	private String employee_id;
	private String unique_registration_no;
	private String basic_pay_salary;
	private String ewf_total_loan_amount;
	private String ewf_due_amount;
	private String personal_total_loan_amount;
	private String personal_due_amount;
	private String created_date;
	private String extra1;
	private String extra2;
	private String extra3;
	private String extra4;
	private String extra5;
	private String created_by;
	private String extra6;
	private String extra7;
	private String extra8;
	private String extra9;
	private String extra10;
	private String extra11;
	private String extra12;
	private String extra13;
	private String extra14;
	private String extra15;

	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	
	public employeeDetailsService()
	{
		/*try {
			 	// Load the database driver
				c = ConnectionHelper.getConnection();
				st=c.createStatement();
			}catch(Exception e)
			{
				//this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}*/
	}
	
	public boolean insert()
	{boolean flag = false;
		try
		{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			rs=st.executeQuery("select * from no_genarator where table_name='employee_details'");
			rs.next();
			int ticket=rs.getInt("table_id");
			String PriSt =rs.getString("id_prefix");
			int ticketm=ticket+1;
			employee_details_id=PriSt+ticketm;
			
			//get current date 
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date today = Calendar.getInstance().getTime();  
			String reportDate = df.format(today);
			
			created_date=reportDate;
			
		 
			String query="insert into employee_details values('"+employee_details_id+"','"+employee_id+"','"+unique_registration_no+"','"+basic_pay_salary+"','"+ewf_total_loan_amount+"','"+ewf_due_amount+"','"+personal_total_loan_amount+"','"+personal_due_amount+"','"+created_date+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+created_by+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"')";
			int i = st.executeUpdate(query);
			System.out.println(query);
			st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='employee_details'");
			if(i>0){flag=true;}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}finally{
			try {
				if(rs != null){rs.close();}
				if(st != null){st.close();}
				if(c != null){c.close();}
				} catch (SQLException e) {
				e.printStackTrace();
				}
		}
		return flag;	
	}

	public boolean delete(String employee_details_id)
	{
		
		return false;	
	}
	
	public boolean update()
	{
		boolean flag2=false;
		try{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();			
			String s = "set ";
            boolean flag = false;
            if(!(employee_details_id==null) && !employee_details_id.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("employee_details_id = '").append(employee_details_id).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",employee_details_id = '").append(employee_details_id).append("'").toString();
                }
            }
            
           
            if( !(employee_id==null)&& !employee_id.equals("") )
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("employee_id = '").append(employee_id).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",employee_id = '").append(employee_id).append("'").toString();
                }
            }
            if(!(unique_registration_no==null)&&!unique_registration_no.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("unique_registration_no = '").append(unique_registration_no).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",unique_registration_no = '").append(unique_registration_no).append("'").toString();    
                }
            }
            
            
            if(!(basic_pay_salary==null)&&!basic_pay_salary.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("basic_pay_salary = '").append(basic_pay_salary).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",basic_pay_salary = '").append(basic_pay_salary).append("'").toString();    
                }
            }
            if(!(ewf_total_loan_amount==null)&&!ewf_total_loan_amount.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("ewf_total_loan_amount = '").append(ewf_total_loan_amount).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",ewf_total_loan_amount = '").append(ewf_total_loan_amount).append("'").toString();    
                }
            }
            if(!(ewf_due_amount==null)&&!ewf_due_amount.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("ewf_due_amount = '").append(ewf_due_amount).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",ewf_due_amount = '").append(ewf_due_amount).append("'").toString();    
                }
            }
            
            if(!(personal_total_loan_amount==null)&&!personal_total_loan_amount.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("personal_total_loan_amount = '").append(personal_total_loan_amount).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",personal_total_loan_amount = '").append(personal_total_loan_amount).append("'").toString();    
                }
            }
            if(!(personal_due_amount==null)&&!personal_due_amount.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("personal_due_amount = '").append(personal_due_amount).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",personal_due_amount = '").append(personal_due_amount).append("'").toString();    
                }
            }
            if(!(created_date==null)&&!created_date.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("created_date = '").append(created_date).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",created_date = '").append(created_date).append("'").toString();    
                }
            }
            /*if(!(extra1==null)&&!extra1.equals(""))
            {*/
            	if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra1 = '").append(extra1).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra1 = '").append(extra1).append("'").toString();    
                }
            /*}*/
            /*if(!(extra2==null)&&!extra2.equals(""))
            {*/
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra2 = '").append(extra2).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra2 = '").append(extra2).append("'").toString();    
                }
            /*}*/
            /*if(!(extra3==null)&&!extra3.equals(""))
            {*/
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra3 = '").append(extra3).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra3 = '").append(extra3).append("'").toString();    
                }
            /*}*/
            if(!(extra4==null)&&!extra4.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra4 = '").append(extra4).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra4 = '").append(extra4).append("'").toString();    
                }
            }
            if(!(extra5==null)&&!extra5.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra5 = '").append(extra5).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra5 = '").append(extra5).append("'").toString();    
                }
            }
            if(!(created_by==null)&&!created_by.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("created_by = '").append(created_by).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",created_by = '").append(created_by).append("'").toString();    
                }
            }
            if(!(extra6==null)&&!extra6.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra6 = '").append(extra6).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra6 = '").append(extra6).append("'").toString();    
                }
            }
            if(!(extra7==null)&&!extra7.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra7 = '").append(extra7).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra7 = '").append(extra7).append("'").toString();    
                }
            }
            if(!(extra8==null)&&!extra8.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra8 = '").append(extra8).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra8 = '").append(extra8).append("'").toString();    
                }
            }
            if(!(extra9==null)&&!extra9.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra9 = '").append(extra9).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra9 = '").append(extra9).append("'").toString();    
                }
            }
            if(!(extra10==null)&&!extra10.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra10 = '").append(extra10).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra10 = '").append(extra10).append("'").toString();    
                }
            }
           
			
			 String s1 = (new StringBuilder()).append("update employee_details ").append(s).append(" where employee_id='").append(employee_id).append("'").toString();
			 System.out.println("================"+(new StringBuilder()).append("query:").append(s1).toString());
			
			
			 int ack=st.executeUpdate(s1);
	         if(ack>0)
	         {
	        	 flag2=true;
	         }
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}finally{
			try {
				if(rs != null){rs.close();}
				if(st != null){st.close();}
				if(c != null){c.close();}
				} catch (SQLException e) {
				e.printStackTrace();
				}
		}
		return flag2;
	}
	
	public boolean updateEwfAndPersonalLoad(String ewf_amount,String personal_amount,String employeeId)
	{
		boolean flag=false;
		try{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			String q="update employee_details set ewf_due_amount=(ewf_due_amount-"+ewf_amount+")"+", personal_due_amount=(personal_due_amount-"+personal_amount+") where employee_id='"+employeeId+"'";
			System.out.println("q======>"+q);
			int i=st.executeUpdate(q);
			if(i>0)
			{flag=true;}
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally{
			try {
				if(rs != null){rs.close();}
				if(st != null){st.close();}
				if(c != null){c.close();}
				} catch (SQLException e) {
				e.printStackTrace();
				}
		}
		return flag;
		
	}
	
	public boolean updateEwfAndPersonalInstallments(String extra7,String extra9,String employeeId)
	{
		boolean flag2=false;
		try{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			String s = "set ";
            boolean flag = false;
            if(!(extra7==null) && !extra7.equals(""))
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra7 = '").append(extra7).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra7 = '").append(extra7).append("'").toString();
                }
            }
            
           
            if( !(extra9==null)&& !extra9.equals("") )
            {
                if(!flag)
                {
                    s = (new StringBuilder()).append(s).append("extra9 = '").append(extra9).append("'").toString();
                    flag = true;
                } else
                {
                    s = (new StringBuilder()).append(s).append(",extra9 = '").append(extra9).append("'").toString();
                }
            }
			//System.out.println("q======>"+q);
            String s1 = (new StringBuilder()).append("update employee_details ").append(s).append(" where employee_id='").append(employeeId).append("'").toString();
			 System.out.println("================"+(new StringBuilder()).append("query:").append(s1).toString());
			
			
			 int ack=st.executeUpdate(s1);
	         if(ack>0)
	         {
	        	 flag2=true;
	         }			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}finally{
			try {
				if(rs != null){rs.close();}
				if(st != null){st.close();}
				if(c != null){c.close();}
				} catch (SQLException e) {
				e.printStackTrace();
				}
		}
		return flag2;		
	}
	

	public employeeDetailsService(String employee_details_id, String employee_id,
			String unique_registration_no, String basic_pay_salary,
			String ewf_total_loan_amount, String ewf_due_amount,
			String personal_total_loan_amount, String personal_due_amount,
			String created_date, String extra1, String extra2, String extra3,
			String extra4, String extra5,String created_by, String extra6,
			String extra7, String extra8,String extra9, String extra10, String extra11,
			String extra12, String extra13, String extra14, String extra15) {
		
		try {
		 	// Load the database driver
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		}catch(Exception e)
		{
			//this.seterror(e.toString());
		System.out.println("Exception is ;"+e);
		}
		
	
		this.employee_details_id = employee_details_id;
		this.employee_id = employee_id;
		this.unique_registration_no = unique_registration_no;
		this.basic_pay_salary = basic_pay_salary;
		this.ewf_total_loan_amount = ewf_total_loan_amount;
		this.ewf_due_amount = ewf_due_amount;
		this.personal_total_loan_amount = personal_total_loan_amount;
		this.personal_due_amount = personal_due_amount;
		this.created_date = created_date;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
		this.created_by=created_by;
		this.extra6 = extra6;
		this.extra7 = extra7;
		this.extra8 = extra8;
		this.extra9 = extra9;
		this.extra10 = extra10;
		this.extra11 = extra11;
		this.extra12 = extra12;
		this.extra13 = extra13;
		this.extra14 = extra14;
		this.extra15 = extra15;	
		
	}
	public String getEmployee_details_id() {
		return employee_details_id;
	}
	public void setEmployee_details_id(String employee_details_id) {
		this.employee_details_id = employee_details_id;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public String getUnique_registration_no() {
		return unique_registration_no;
	}
	public void setUnique_registration_no(String unique_registration_no) {
		this.unique_registration_no = unique_registration_no;
	}
	public String getBasic_pay_salary() {
		return basic_pay_salary;
	}
	public void setBasic_pay_salary(String basic_pay_salary) {
		this.basic_pay_salary = basic_pay_salary;
	}
	public String getEwf_total_loan_amount() {
		return ewf_total_loan_amount;
	}
	public void setEwf_total_loan_amount(String ewf_total_loan_amount) {
		this.ewf_total_loan_amount = ewf_total_loan_amount;
	}
	public String getEwf_due_amount() {
		return ewf_due_amount;
	}
	public void setEwf_due_amount(String ewf_due_amount) {
		this.ewf_due_amount = ewf_due_amount;
	}
	public String getPersonal_total_loan_amount() {
		return personal_total_loan_amount;
	}
	public void setPersonal_total_loan_amount(String personal_total_loan_amount) {
		this.personal_total_loan_amount = personal_total_loan_amount;
	}
	public String getPersonal_due_amount() {
		return personal_due_amount;
	}
	public void setPersonal_due_amount(String personal_due_amount) {
		this.personal_due_amount = personal_due_amount;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getExtra6() {
		return extra6;
	}

	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}

	public String getExtra7() {
		return extra7;
	}

	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}

	public String getExtra8() {
		return extra8;
	}

	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}

	public String getExtra9() {
		return extra9;
	}

	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}

	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}

	public String getExtra11() {
		return extra11;
	}

	public void setExtra11(String extra11) {
		this.extra11 = extra11;
	}

	public String getExtra12() {
		return extra12;
	}

	public void setExtra12(String extra12) {
		this.extra12 = extra12;
	}

	public String getExtra13() {
		return extra13;
	}

	public void setExtra13(String extra13) {
		this.extra13 = extra13;
	}

	public String getExtra14() {
		return extra14;
	}

	public void setExtra14(String extra14) {
		this.extra14 = extra14;
	}

	public String getExtra15() {
		return extra15;
	}

	public void setExtra15(String extra15) {
		this.extra15 = extra15;
	}
	
		
	
}
