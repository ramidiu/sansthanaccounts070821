package beans;

public class diagnosticissues {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	private String documentNo="";
	private String date="";
	private String appointmnetId="";
	private String testName="";
	private String amount="";
	private String extra1=""; //employee id
	private String extra2=""; //percentage discount for free 100%
	private String extra3=""; //net amount after discount
	private String extra4=""; //paid or free
	private String extra5="";
	private String patientName;
	private String doctorName;
	private String patientId;
	private String numberOfTests;
	public diagnosticissues() {} 
	public diagnosticissues(String documentNo,String date,String appointmnetId,String testName,String amount,String extra1,String extra2,String extra3,String extra4,String extra5)
	 {
	this.documentNo=documentNo;
	this.date=date;
	this.appointmnetId=appointmnetId;
	this.testName=testName;
	this.amount=amount;
	this.extra1=extra1;
	this.extra2=extra2;
	this.extra3=extra3;
	this.extra4=extra4;
	this.extra5=extra5;

	}
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAppointmnetId() {
		return appointmnetId;
	}
	public void setAppointmnetId(String appointmnetId) {
		this.appointmnetId = appointmnetId;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getNumberOfTests() {
		return numberOfTests;
	}
	public void setNumberOfTests(String numberOfTests) {
		this.numberOfTests = numberOfTests;
	} 
    
	
}
