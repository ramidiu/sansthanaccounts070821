package beans;
public class cash_report 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String cash_report_id="";
private String balance_code_flag="";
private String date="";
private String shift_code="";
private String counter_code="";
private String thousands="";
private String fivehundreds="";
private String hundreds="";
private String fifyts="";
private String twntys="";
private String tens="";
private String fives="";
private String twos="";
private String ones="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";
public cash_report() {} 
public cash_report(String cash_report_id,String balance_code_flag,String date,String shift_code,String counter_code,String thousands,String fivehundreds,String hundreds,String fifyts,String twntys,String tens,String fives,String twos,String ones,String extra1,String extra2,String extra3,String extra4,String extra5,String extra6,String extra7,String extra8,String extra9,String extra10)
 {
this.cash_report_id=cash_report_id;
this.balance_code_flag=balance_code_flag;
this.date=date;
this.shift_code=shift_code;
this.counter_code=counter_code;
this.thousands=thousands;
this.fivehundreds=fivehundreds;
this.hundreds=hundreds;
this.fifyts=fifyts;
this.twntys=twntys;
this.tens=tens;
this.fives=fives;
this.twos=twos;
this.ones=ones;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;
this.extra10=extra10;

} 
public String getcash_report_id() {
return cash_report_id;
}
public void setcash_report_id(String cash_report_id) {
this.cash_report_id = cash_report_id;
}
public String getbalance_code_flag() {
return balance_code_flag;
}
public void setbalance_code_flag(String balance_code_flag) {
this.balance_code_flag = balance_code_flag;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String getshift_code() {
return shift_code;
}
public void setshift_code(String shift_code) {
this.shift_code = shift_code;
}
public String getcounter_code() {
return counter_code;
}
public void setcounter_code(String counter_code) {
this.counter_code = counter_code;
}
public String getthousands() {
return thousands;
}
public void setthousands(String thousands) {
this.thousands = thousands;
}
public String getfivehundreds() {
return fivehundreds;
}
public void setfivehundreds(String fivehundreds) {
this.fivehundreds = fivehundreds;
}
public String gethundreds() {
return hundreds;
}
public void sethundreds(String hundreds) {
this.hundreds = hundreds;
}
public String getfifyts() {
return fifyts;
}
public void setfifyts(String fifyts) {
this.fifyts = fifyts;
}
public String gettwntys() {
return twntys;
}
public void settwntys(String twntys) {
this.twntys = twntys;
}
public String gettens() {
return tens;
}
public void settens(String tens) {
this.tens = tens;
}
public String getfives() {
return fives;
}
public void setfives(String fives) {
this.fives = fives;
}
public String gettwos() {
return twos;
}
public void settwos(String twos) {
this.twos = twos;
}
public String getones() {
return ones;
}
public void setones(String ones) {
this.ones = ones;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}
public String getextra8() {
return extra8;
}
public void setextra8(String extra8) {
this.extra8 = extra8;
}
public String getextra9() {
return extra9;
}
public void setextra9(String extra9) {
this.extra9 = extra9;
}
public String getextra10() {
return extra10;
}
public void setextra10(String extra10) {
this.extra10 = extra10;
}

}