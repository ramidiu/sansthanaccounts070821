package beans;

import java.sql.Connection;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class productexpenses_scService {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	private String exp_id="";
	private String vendorId="";
	private String narration="";
	private String entry_date="";
	private String major_head_id="";
	private String minorhead_id="";
	private String sub_head_id="";
	private String amount="";
	private String expinv_id="";
	private String emp_id="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	private String head_account_id="";
	private String enter_by="";
	private String enter_date="";
	private String extra6="";
	private String extra7="";
	private String extra8="";
	private String extra9="";
	private String extra10="";
	
	public productexpenses_scService(String exp_id,String vendorId,String narration,String entry_date,String major_head_id,String minorhead_id,String sub_head_id,String amount,String expinv_id,String emp_id,String extra1,String extra2,String extra3,String extra4,String extra5,String head_account_id,String enter_by,String enter_date,String extra6,String extra7,String extra8,String extra9,String extra10)
	{
	this.exp_id=exp_id;
	this.vendorId=vendorId;
	this.narration=narration;
	this.entry_date=entry_date;
	this.major_head_id=major_head_id;
	this.minorhead_id=minorhead_id;
	this.sub_head_id=sub_head_id;
	this.amount=amount;
	this.expinv_id=expinv_id;
	this.emp_id=emp_id;
	this.extra1=extra1;
	this.extra2=extra2;
	this.extra3=extra3;
	this.extra4=extra4;
	this.extra5=extra5;
	this.head_account_id=head_account_id;
	this.enter_by=enter_by;
	this.enter_date=enter_date;
	this.extra6=extra6;
	this.extra7=extra7;
	this.extra8=extra8;
	this.extra9=extra9;
	this.extra10=extra10;
	}
	public String getExp_id() {
		return exp_id;
	}
	public void setExp_id(String exp_id) {
		this.exp_id = exp_id;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getEntry_date() {
		return entry_date;
	}
	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}
	public String getMajor_head_id() {
		return major_head_id;
	}
	public void setMajor_head_id(String major_head_id) {
		this.major_head_id = major_head_id;
	}
	public String getMinorhead_id() {
		return minorhead_id;
	}
	public void setMinorhead_id(String minorhead_id) {
		this.minorhead_id = minorhead_id;
	}
	public String getSub_head_id() {
		return sub_head_id;
	}
	public void setSub_head_id(String sub_head_id) {
		this.sub_head_id = sub_head_id;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getExpinv_id() {
		return expinv_id;
	}
	public void setExpinv_id(String expinv_id) {
		this.expinv_id = expinv_id;
	}
	public String getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getHead_account_id() {
		return head_account_id;
	}
	public void setHead_account_id(String head_account_id) {
		this.head_account_id = head_account_id;
	}
	public String getEnter_by() {
		return enter_by;
	}
	public void setEnter_by(String enter_by) {
		this.enter_by = enter_by;
	}
	public String getEnter_date() {
		return enter_date;
	}
	public void setEnter_date(String enter_date) {
		this.enter_date = enter_date;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}

	private Statement st = null;
	private Connection c=null;
	public productexpenses_scService()
	{
	/*try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	}catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}*/
	 }
	
	
	public boolean insert()
	{
		try
		{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		String query="insert into productexpenses_sc values('"+exp_id+"','"+vendorId+"','"+narration+"',now(),'"+major_head_id+"','"+minorhead_id+"','"+sub_head_id+"','"+amount+"','"+expinv_id+"','"+emp_id+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+head_account_id+"','"+enter_by+"','"+enter_date+"','"+extra6+"','"+extra7+"','"+entry_date+"','"+extra9+"','"+extra10+"')";
		System.out.println(query);
		st.executeUpdate(query);
		st.close();
		c.close();
		return true;
		}catch(Exception e){
		this.seterror(e.toString());
		return false;
		}
	}
	public boolean update()
	{
	
	try
	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
	String updatecc="set ";
	 int jk=0; 
	if(!vendorId.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",vendorId = '"+vendorId+"'";}
	}

	if(!narration.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"narration = '"+narration+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",narration = '"+narration+"'";}
	}

	if(!entry_date.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"entry_date = '"+entry_date+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",entry_date = '"+entry_date+"'";}
	}

	if(!major_head_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
	}

	if(!minorhead_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
	}

	if(!sub_head_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
	}

	if(!amount.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"amount = '"+amount+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",amount = '"+amount+"'";}
	}

	if(!expinv_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"expinv_id = '"+expinv_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",expinv_id = '"+expinv_id+"'";}
	}

	if(!emp_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",emp_id = '"+emp_id+"'";}
	}

	if(!extra1.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra1 = '"+extra1+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra1 = '"+extra1+"'";}
	}

	if(!extra2.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra2 = '"+extra2+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra2 = '"+extra2+"'";}
	}

	if(!extra3.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra3 = '"+extra3+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra3 = '"+extra3+"'";}
	}

	if(!extra4.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra4 = '"+extra4+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra4 = '"+extra4+"'";}
	}

	if(!extra5.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra5 = '"+extra5+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra5 = '"+extra5+"'";}
	}

	if(!head_account_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
	}
	
	if(!enter_by.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"enter_by = '"+enter_by+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",enter_by = '"+enter_by+"'";}
	}
	if(!enter_date.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"enter_date = '"+enter_date+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",enter_date = '"+enter_date+"'";}
	}
	if(!extra6.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra6 = '"+extra6+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra6 = '"+extra6+"'";}
	}
	if(!extra7.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra7 = '"+extra7+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra7 = '"+extra7+"'";}
	}
	if(!extra8.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra8 = '"+extra8+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra8 = '"+extra8+"'";}
	}
	if(!extra9.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra9 = '"+extra9+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra9 = '"+extra9+"'";}
	}
	if(!extra10.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra10 = '"+extra10+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra10 = '"+extra10+"'";}
	}
	String query="update productexpenses_sc "+updatecc+" where exp_id='"+exp_id+"'";
	System.out.println("update111     productexpenses_sc    =======>"+query);
	st.executeUpdate(query);
	st.close();
	c.close();
	return true;
	}catch(Exception e){
	this.seterror(e.toString());
	return false;
	}

	}
	public boolean update1()
	{
	try
	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
	String updatecc="set ";
	 int jk=0; 
	if(!vendorId.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",vendorId = '"+vendorId+"'";}
	}

	if(!narration.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"narration = '"+narration+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",narration = '"+narration+"'";}
	}

	if(!entry_date.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"entry_date = '"+entry_date+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",entry_date = '"+entry_date+"'";}
	}

	if(!major_head_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
	}

	if(!minorhead_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
	}

	if(!sub_head_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
	}

	if(!amount.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"amount = '"+amount+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",amount = '"+amount+"'";}
	}

	/*if(!expinv_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"expinv_id = '"+expinv_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",expinv_id = '"+expinv_id+"'";}
	}*/

	if(!emp_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",emp_id = '"+emp_id+"'";}
	}

	if(!extra1.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra1 = '"+extra1+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra1 = '"+extra1+"'";}
	}

	if(!extra2.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra2 = '"+extra2+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra2 = '"+extra2+"'";}
	}

	if(!extra3.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra3 = '"+extra3+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra3 = '"+extra3+"'";}
	}

	if(!extra4.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra4 = '"+extra4+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra4 = '"+extra4+"'";}
	}

	if(!extra5.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra5 = '"+extra5+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra5 = '"+extra5+"'";}
	}

	if(!head_account_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
	}
	String query="update productexpenses_sc "+updatecc+",extra9=now() where expinv_id='"+expinv_id+"'";
	System.out.println("status===>"+query);
	st.executeUpdate(query);
	st.close();
	c.close();
	return true;
	}catch(Exception e){
	this.seterror(e.toString());
	return false;
	}

	}
	public boolean updateBasedOnExpIdAndDate(String expId,String Date)
	{
	try
	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
	String updatecc="set ";
	 int jk=0; 
	if(!vendorId.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",vendorId = '"+vendorId+"'";}
	}

	if(!narration.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"narration = '"+narration+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",narration = '"+narration+"'";}
	}

	if(!entry_date.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"entry_date = '"+entry_date+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",entry_date = '"+entry_date+"'";}
	}

	if(!major_head_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
	}

	if(!minorhead_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
	}

	if(!sub_head_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
	}

	if(!amount.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"amount = '"+amount+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",amount = '"+amount+"'";}
	}

	if(!expinv_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"expinv_id = '"+expinv_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",expinv_id = '"+expinv_id+"'";}
	}

	if(!emp_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",emp_id = '"+emp_id+"'";}
	}

	if(!extra1.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra1 = '"+extra1+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra1 = '"+extra1+"'";}
	}

	if(!extra2.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra2 = '"+extra2+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra2 = '"+extra2+"'";}
	}

	if(!extra3.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra3 = '"+extra3+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra3 = '"+extra3+"'";}
	}

	if(!extra4.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra4 = '"+extra4+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra4 = '"+extra4+"'";}
	}

	if(!extra5.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra5 = '"+extra5+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra5 = '"+extra5+"'";}
	}

	if(!head_account_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
	}
	
	if(!enter_by.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"enter_by = '"+enter_by+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",enter_by = '"+enter_by+"'";}
	}
	if(!enter_date.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"enter_date = '"+enter_date+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",enter_date = '"+enter_date+"'";}
	}
	if(!extra6.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra6 = '"+extra6+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra6 = '"+extra6+"'";}
	}
	if(!extra7.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra7 = '"+extra7+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra7 = '"+extra7+"'";}
	}
	if(!extra8.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra8 = '"+extra8+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra8 = '"+extra8+"'";}
	}
	if(!extra9.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra9 = '"+extra9+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra9 = '"+extra9+"'";}
	}
	if(!extra10.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra10 = '"+extra10+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra10 = '"+extra10+"'";}
	}
	String query="update productexpenses_sc "+updatecc+" where exp_id='"+exp_id+"' and entry_date = '"+Date+"'";
	System.out.println("updatemultipleexpenses   productexpenses_sc  ======>"+query);
	st.executeUpdate(query);
	st.close();
	c.close();
	return true;
	}catch(Exception e){
	this.seterror(e.toString());
	return false;
	}

	}
	public boolean updateStatusBasedOnDate(String Inv_id,String Date,String status)
	{
	try
	{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	String query="update productexpenses_sc set approval_status='"+status+"',extra9=now() where expinv_id='"+Inv_id+"' and entry_date='"+Date+"'";
	System.out.println("psc==>"+query);
	st.executeUpdate(query);
	st.close();
	c.close();
	return true;
	}catch(Exception e){
	this.seterror(e.toString());
	return false;
	}

	}
	public boolean delete()
	{
	try
	{c = ConnectionHelper.getConnection();
	st=c.createStatement();
	String query="delete from productexpenses_sc where exp_id='"+exp_id+"'";
	st.executeUpdate(query);
	st.close();
	c.close();
	return true;
	}catch(Exception e){
	this.seterror(e.toString());
	return false;
	}
	}
}
