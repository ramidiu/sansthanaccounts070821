package beans;
public class majorhead 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String major_head_id="";
private String name="";
private String description="";
private String head_account_id="";
private String extra1="";
private String extra2="";
private String extra3="";
public majorhead() {} 
public majorhead(String major_head_id,String name,String description,String head_account_id,String extra1,String extra2,String extra3)
 {
this.major_head_id=major_head_id;
this.name=name;
this.description=description;
this.head_account_id=head_account_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;

} 
public String getmajor_head_id() {
return major_head_id;
}
public void setmajor_head_id(String major_head_id) {
this.major_head_id = major_head_id;
}
public String getname() {
return name;
}
public void setname(String name) {
this.name = name;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
@Override
public String toString() {
	return "majorhead [error=" + error + ", major_head_id=" + major_head_id
			+ ", name=" + name + ", description=" + description
			+ ", head_account_id=" + head_account_id + ", extra1=" + extra1
			+ ", extra2=" + extra2 + ", extra3=" + extra3 + "]";
}

}