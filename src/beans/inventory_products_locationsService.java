package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class inventory_products_locationsService 
{
	private String error="";
	public void seterror(String error)
	{	
		this.error=error;
	}
	public String geterror()
	{
		return this.error;
	}
	
	private String inventory_id="";
	private String inventory_name="";
	private String product_id="";
	private String quantity="";
	private String location_id="";
	private String created_by="";
	private String created_date="";
	private String updated_by="";
	private String updated_date="";
	private String narration="";
	private String status="";
	private String head_account_id="";
	private String image="";
	private String document="";
	private String extra1=""; //depreciation (%)
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String rate="";
	private String amount="";
	private String warranty;
	private String damaged_amount="";
	

	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	public inventory_products_locationsService()
	{
		/*try {
			// Load the database driver
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		}catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}*/
	}

	
	
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getWarranty() {
		return warranty;
	}
	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}
	public String getDamaged_amount() {
		return damaged_amount;
	}
	public void setDamaged_amount(String damaged_amount) {
		this.damaged_amount = damaged_amount;
	}
	public void setinventory_id(String inventory_id)
	{
		this.inventory_id = inventory_id;
	}

	public String getinventory_id()
	{
		return (this.inventory_id);
	}


	public void setinventory_name(String inventory_name)
	{
		this.inventory_name = inventory_name;
	}

	public String getinventory_name()
	{
		return (this.inventory_name);
	}


	public void setproduct_id(String product_id)
	{
		this.product_id = product_id;
	}

	public String getproduct_id()
	{
		return (this.product_id);
	}


	public void setquantity(String quantity)
	{
		this.quantity = quantity;
	}

	public String getquantity()
	{
		return (this.quantity);
	}


	public void setlocation_id(String location_id)
	{
		this.location_id = location_id;
	}

	public String getlocation_id()
	{
		return (this.location_id);
	}


	public void setcreated_by(String created_by)
	{
		this.created_by = created_by;
	}

	public String getcreated_by()
	{
		return (this.created_by);
	}


	public void setcreated_date(String created_date)
	{
		this.created_date = created_date;
	}

	public String getcreated_date()
	{
		return (this.created_date);
	}


	public void setupdated_by(String updated_by)
	{
		this.updated_by = updated_by;
	}

	public String getupdated_by()
	{
		return (this.updated_by);
	}


	public void setupdated_date(String updated_date)
	{
		this.updated_date = updated_date;
	}

	public String getupdated_date()
	{
		return (this.updated_date);
	}


	public void setnarration(String narration)
	{
		this.narration = narration;
	}

	public String getnarration()
	{
		return (this.narration);
	}


	public void setstatus(String status)
	{
		this.status = status;
	}

	public String getstatus()
	{
		return (this.status);
	}


	public void sethead_account_id(String head_account_id)
	{
		this.head_account_id = head_account_id;
	}

	public String gethead_account_id()
	{
		return (this.head_account_id);
	}


	public void setimage(String image)
	{
		this.image = image;
	}

	public String getimage()
	{
		return (this.image);
	}


	public void setdocument(String document)
	{
		this.document = document;
	}

	public String getdocument()
	{
		return (this.document);
	}


	public void setextra1(String extra1)
	{
		this.extra1 = extra1;
	}

	public String getextra1()
	{
		return (this.extra1);
	}


	public void setextra2(String extra2)
	{
		this.extra2 = extra2;
	}

	public String getextra2()
	{
		return (this.extra2);
	}


	public void setextra3(String extra3)
	{
		this.extra3 = extra3;
	}

	public String getextra3()
	{
		return (this.extra3);
	}


	public void setextra4(String extra4)
	{
		this.extra4 = extra4;
	}

	public String getextra4()
	{
		return (this.extra4);
	}

	//insert into database 
public boolean insert()
{
	try
	{   c = ConnectionHelper.getConnection();
		st=c.createStatement();
		rs=st.executeQuery("select * from no_genarator where table_name='inventory_products_locations'");
		rs.next();
		int ticket=rs.getInt("table_id");
		String PriSt =rs.getString("id_prefix");
		rs.close();
		int ticketm=ticket+1;
		inventory_id=PriSt+ticketm;
		String query="insert into inventory_products_locations values('"+inventory_id+"','"+inventory_name+"','"+product_id+"','"+quantity+"','"+location_id+"','"+created_by+"','"+created_date+"','"+updated_by+"','"+updated_date+"','"+narration+"','"+status+"','"+head_account_id+"','"+image+"','"+document+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+rate+"','"+amount+"','"+warranty+"','"+damaged_amount+"')";
		st.executeUpdate(query);
		st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='inventory_products_locations'");
		st.close();
		c.close();
		return true;
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		return false;
	}
}

public boolean update()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	String updatecc="set ";
	int jk=0; 
	if(!inventory_name.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"inventory_name = '"+inventory_name+"'"; 
			jk=1; 
		}
		else
		{
			updatecc=updatecc+",inventory_name = '"+inventory_name+"'";}
		}

	if(!product_id.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"product_id = '"+product_id+"'"; 
			jk=1; 
		}
		else
		{
			updatecc=updatecc+",product_id = '"+product_id+"'";}
		}

	if(!quantity.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"quantity = '"+quantity+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",quantity = '"+quantity+"'";}
		}

	if(!location_id.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"location_id = '"+location_id+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",location_id = '"+location_id+"'";}
	}

	if(!created_by.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"created_by = '"+created_by+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",created_by = '"+created_by+"'";}
	}

	if(!created_date.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"created_date = '"+created_date+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",created_date = '"+created_date+"'";}
	}

	if(!updated_by.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"updated_by = '"+updated_by+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",updated_by = '"+updated_by+"'";}
	}

	if(!updated_date.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"updated_date = '"+updated_date+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",updated_date = '"+updated_date+"'";}
	}

	if(!narration.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"narration = '"+narration+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",narration = '"+narration+"'";}
	}

	if(!status.equals(""))
	{ 
		if(jk==0)
		{
			updatecc=updatecc+"status = '"+status+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",status = '"+status+"'";}
	}

	if(!head_account_id.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
	}

	if(!image.equals(""))
	{ 
		if(jk==0)
		{
			updatecc=updatecc+"image = '"+image+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",image = '"+image+"'";}
	}

	if(!document.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"document = '"+document+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",document = '"+document+"'";}
	}

	if(!extra1.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"extra1 = '"+extra1+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",extra1 = '"+extra1+"'";}
	}

	if(!extra2.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"extra2 = '"+extra2+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",extra2 = '"+extra2+"'";}
	}

	if(!extra3.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"extra3 = '"+extra3+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",extra3 = '"+extra3+"'";}
	}

	if(!extra4.equals("")){ 
		if(jk==0)
		{
			updatecc=updatecc+"extra4 = '"+extra4+"'"; 
			jk=1; 
		}else
		{
			updatecc=updatecc+",extra4 = '"+extra4+"'";}
	}
	String query="update inventory_products_locations "+updatecc+" where inventory_id='"+inventory_id+"'";
	st.executeUpdate(query);
	st.close();
	c.close();
	return true;
	}catch(Exception e){
		this.seterror(e.toString());
		return false;
	}

}


public boolean delete()
	{
		try
		{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			String query="delete from inventory_products_locations where inventory_id='"+inventory_id+"'";
			st.executeUpdate(query);
			st.close();
			c.close();
			return true;
		}catch(Exception e){
			this.seterror(e.toString());
			return false;
		}

	}
}