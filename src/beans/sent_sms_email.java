package beans;

public class sent_sms_email 
{
	private String error="";
	 public void seterror(String error)
	 {
	 this.error=error;
	 }
	 public String geterror()
	 {
	 return this.error;
	 }
	 
	 private String uniqId="";
	 private String regId="";
	 private String name="";
	 private String purpose="";
	 private String daysremainder="";
	 private String email="";
	 private String phone="";
	 private String createdDate="";
	 private String extra1="";
	 private String extra2="";
	 private String extra3="";
	 private String extra4="";
	 private String extra5="";
	 
	 public sent_sms_email() {}
	 
	public sent_sms_email(String uniqId, String regId, String name,
			String purpose, String daysremainder, String email, String phone,
			String createdDate, String extra1, String extra2, String extra3,
			String extra4, String extra5) {
		super();
		this.uniqId = uniqId;
		this.regId = regId;
		this.name = name;
		this.purpose = purpose;
		this.daysremainder = daysremainder;
		this.email = email;
		this.phone = phone;
		this.createdDate = createdDate;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
	}
	public String getUniqId() {
		return uniqId;
	}
	public void setUniqId(String uniqId) {
		this.uniqId = uniqId;
	}
	public String getRegId() {
		return regId;
	}
	public void setId(String regId) {
		this.regId = regId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getDaysremainder() {
		return daysremainder;
	}
	public void setDaysremainder(String daysremainder) {
		this.daysremainder = daysremainder;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}  
}
