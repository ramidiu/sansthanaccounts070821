package beans;
public class locations 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String location_id="";
private String location_name="";
private String narration="";
private String emp_id="";
private String date="";
private String head_account_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
public locations() {} 
public locations(String location_id,String location_name,String narration,String emp_id,String date,String head_account_id,String extra1,String extra2,String extra3,String extra4)
 {
this.location_id=location_id;
this.location_name=location_name;
this.narration=narration;
this.emp_id=emp_id;
this.date=date;
this.head_account_id=head_account_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;

} 
public String getlocation_id() {
return location_id;
}
public void setlocation_id(String location_id) {
this.location_id = location_id;
}
public String getlocation_name() {
return location_name;
}
public void setlocation_name(String location_name) {
this.location_name = location_name;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}

}