package beans;
public class headsgroup 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String group_id="";
private String group_name="";
private String narration="";
private String created_date="";
private String head_account_id="";
private String emp_id="";
private String extra1="";
private String extra2="";
private String extra3="";
public headsgroup() {} 
public headsgroup(String group_id,String group_name,String narration,String created_date,String head_account_id,String emp_id,String extra1,String extra2,String extra3)
 {
this.group_id=group_id;
this.group_name=group_name;
this.narration=narration;
this.created_date=created_date;
this.head_account_id=head_account_id;
this.emp_id=emp_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;

} 
public String getgroup_id() {
return group_id;
}
public void setgroup_id(String group_id) {
this.group_id = group_id;
}
public String getgroup_name() {
return group_name;
}
public void setgroup_name(String group_name) {
this.group_name = group_name;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getcreated_date() {
return created_date;
}
public void setcreated_date(String created_date) {
this.created_date = created_date;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}

}