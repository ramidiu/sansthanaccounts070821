package beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class productexpensesService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String exp_id="";
private String vendorId="";
private String narration="";
private String entry_date="";
private String major_head_id="";
private String minorhead_id="";
private String sub_head_id="";
private String amount="";
private String expinv_id="";
private String emp_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String head_account_id="";
private String enter_by="";
public String getEnter_by() {
	return enter_by;
}
public void setEnter_by(String enter_by) {
	this.enter_by = enter_by;
}
public String getEnter_date() {
	return enter_date;
}
public void setEnter_date(String enter_date) {
	this.enter_date = enter_date;
}
public String getExtra6() {
	return extra6;
}
public void setExtra6(String extra6) {
	this.extra6 = extra6;
}
public String getExtra7() {
	return extra7;
}
public void setExtra7(String extra7) {
	this.extra7 = extra7;
}
public String getExtra8() {
	return extra8;
}
public void setExtra8(String extra8) {
	this.extra8 = extra8;
}
public String getExtra9() {
	return extra9;
}
public void setExtra9(String extra9) {
	this.extra9 = extra9;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
private String enter_date="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";

private String banktrn_id="";
private String extra11 = "";
private String extra12 = "";
private String extra13 = "";
private String extra14 = "";
private String extra15 = "";
private String extra16 = "";
private String extra17 = "";
private String extra18 = "";
private String extra19 = "";
private String extra20 = "";
private String extra21 = "";
private String extra22 = "";
private String extra23 = "";
private String extra24 = "";
private String extra25 = "";

public String getBanktrn_id() {
	return banktrn_id;
}
public void setBanktrn_id(String banktrn_id) {
	this.banktrn_id = banktrn_id;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}
public String getExtra14() {
	return extra14;
}
public void setExtra14(String extra14) {
	this.extra14 = extra14;
}
public String getExtra15() {
	return extra15;
}
public void setExtra15(String extra15) {
	this.extra15 = extra15;
}
public String getExtra16() {
	return extra16;
}
public void setExtra16(String extra16) {
	this.extra16 = extra16;
}
public String getExtra17() {
	return extra17;
}
public void setExtra17(String extra17) {
	this.extra17 = extra17;
}
public String getExtra18() {
	return extra18;
}
public void setExtra18(String extra18) {
	this.extra18 = extra18;
}
public String getExtra19() {
	return extra19;
}
public void setExtra19(String extra19) {
	this.extra19 = extra19;
}
public String getExtra20() {
	return extra20;
}
public void setExtra20(String extra20) {
	this.extra20 = extra20;
}
public String getExtra21() {
	return extra21;
}
public void setExtra21(String extra21) {
	this.extra21 = extra21;
}
public String getExtra22() {
	return extra22;
}
public void setExtra22(String extra22) {
	this.extra22 = extra22;
}
public String getExtra23() {
	return extra23;
}
public void setExtra23(String extra23) {
	this.extra23 = extra23;
}
public String getExtra24() {
	return extra24;
}
public void setExtra24(String extra24) {
	this.extra24 = extra24;
}
public String getExtra25() {
	return extra25;
}
public void setExtra25(String extra25) {
	this.extra25 = extra25;
}
private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public productexpensesService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setexp_id(String exp_id)
{
this.exp_id = exp_id;
}

public String getexp_id()
{
return (this.exp_id);
}


public void setvendorId(String vendorId)
{
this.vendorId = vendorId;
}

public String getvendorId()
{
return (this.vendorId);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setentry_date(String entry_date)
{
this.entry_date = entry_date;
}

public String getentry_date()
{
return (this.entry_date);
}


public void setmajor_head_id(String major_head_id)
{
this.major_head_id = major_head_id;
}

public String getmajor_head_id()
{
return (this.major_head_id);
}


public void setminorhead_id(String minorhead_id)
{
this.minorhead_id = minorhead_id;
}

public String getminorhead_id()
{
return (this.minorhead_id);
}


public void setsub_head_id(String sub_head_id)
{
this.sub_head_id = sub_head_id;
}

public String getsub_head_id()
{
return (this.sub_head_id);
}


public void setamount(String amount)
{
this.amount = amount;
}

public String getamount()
{
return (this.amount);
}


public void setexpinv_id(String expinv_id)
{
this.expinv_id = expinv_id;
}

public String getexpinv_id()
{
return (this.expinv_id);
}


public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}

public boolean insert()
{int ticketm=0;
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='productexpenses'");
rs.next();
int ticket=rs.getInt("table_id");
//System.out.println("table id====>"+ticket);
String PriSt =rs.getString("id_prefix");
rs.close();
 ticketm=ticket+1;
// System.out.println("incrementeed table id====>"+ticketm);
 exp_id=PriSt+ticketm;
String query="insert into productexpenses values('"+exp_id+"','"+vendorId+"','"+narration+"','"+entry_date+"','"+major_head_id+"','"+minorhead_id+"','"+sub_head_id+"','"+amount+"','"+expinv_id+"','"+emp_id+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+head_account_id+"','"+enter_by+"','"+enter_date+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+banktrn_id+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"','"+extra16+"','"+extra17+"','"+extra18+"','"+extra19+"','"+extra20+"','"+extra21+"','"+extra22+"','"+extra23+"','"+extra24+"','"+extra25+"')";
  //                                                   exp_id,      vendorId,      narration,       entry_date,     major_head_id,      minorhead_id,      sub_head_id,      amount,      expinv_id,      emp_id,      extra1,      extra2,      extra3,     extra4,       extra5,      head_account_id,     enter_by,       enter_date,      extra6,     extra7,      extra8,      extra9,      extra10,        banktrn_id,    extra11,     extra12,       extra13,      extra14,      extra15,      extra16,      extra17,       extra18,      extra19,     extra20            
System.out.println(" query =============  >"+query);  
String query1="insert into productexpenses_sc values('"+exp_id+"','"+vendorId+"','"+narration+"','"+entry_date+"','"+major_head_id+"','"+minorhead_id+"','"+sub_head_id+"','"+amount+"','"+expinv_id+"','"+emp_id+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+head_account_id+"','"+enter_by+"','"+enter_date+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+banktrn_id+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"','"+extra16+"','"+extra17+"','"+extra18+"','"+extra19+"','"+extra20+"','"+extra21+"','"+extra22+"','"+extra23+"','"+extra24+"','"+extra25+"')";
//System.out.println(" productexpenses_sc=============  >"+query);

int result=st.executeUpdate(query);
//System.out.println("result====>"+result);
int ss = st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='productexpenses'");
//System.out.println("id updated----"+ss);
int res=st.executeUpdate(query1);
//System.out.println("res====>(product expenses_sc)"+res);
//st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='productexpenses'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
e.printStackTrace();
return false;
}

}public boolean update()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!vendorId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendorId = '"+vendorId+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!entry_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"entry_date = '"+entry_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",entry_date = '"+entry_date+"'";}
}

if(!major_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
}

if(!minorhead_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
}

if(!sub_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
}

if(!amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"amount = '"+amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",amount = '"+amount+"'";}
}

if(!expinv_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"expinv_id = '"+expinv_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",expinv_id = '"+expinv_id+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}
if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}
String query="update productexpenses "+updatecc+" where exp_id='"+exp_id+"'";

System.out.println("===========================================================================================");
System.out.println("update productexpenses   from  account officer ===========>  "+query);

System.out.println("update... productexpenses   from gs ====>"+query);

st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
public boolean update1()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!vendorId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendorId = '"+vendorId+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!entry_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"entry_date = '"+entry_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",entry_date = '"+entry_date+"'";}
}

if(!major_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
}

if(!minorhead_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
}

if(!sub_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
}

if(!amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"amount = '"+amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",amount = '"+amount+"'";}
}

/*if(!expinv_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"expinv_id = '"+expinv_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",expinv_id = '"+expinv_id+"'";}
}*/

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}


if(!banktrn_id.equals("")){ 
	if(jk==0){
		updatecc=updatecc+"banktrn_id = '"+banktrn_id+"'"; 
		jk=1; 
	}else{
		updatecc=updatecc+",banktrn_id = '"+banktrn_id+"'";}
}
if(!extra9.equals("")){ 
	if(jk==0){
		updatecc=updatecc+"extra9 = '"+extra9+"'"; 
		jk=1; 
	}else{
		updatecc=updatecc+",extra9 = '"+extra9+"'";}
}
if(!extra10.equals("")){ 
	if(jk==0){
		updatecc=updatecc+"extra10 = '"+extra10+"'"; 
		jk=1; 
	}else{
		updatecc=updatecc+",extra10 = '"+extra10+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}
String query="update productexpenses "+updatecc+" where extra1='Approved' and expinv_id='"+expinv_id+"'";
System.out.println("  update in productexpancex services status==============>"+query);
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}

public boolean updateStatus(String expInv_id,String status)
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="update productexpenses set extra1='"+status+"' where expinv_id='"+expInv_id+"' and extra4='BoardApprove'";
String query1="update productexpenses_sc set extra1='"+status+"' where expinv_id='"+expInv_id+"' and extra4='BoardApprove'";
System.out.println("updatestatus productexpenses11111111 =====>"+query);
st.executeUpdate(query);
st.executeUpdate(query1);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}

public boolean updatePayementStausAndBank(String expinv_id)
{
	try
	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
		String query="update productexpenses set extra1='Approved',extra3='' where expinv_id='"+expinv_id+"'";
		//System.out.println(query);
		st.executeUpdate(query);
		st.close();
		c.close();
		return true;
	}catch(Exception e){
		this.seterror(e.toString());
		return false;
	}

}

public boolean updateBasedOnlatestEntry(String expinvId,String Date)
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!vendorId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendorId = '"+vendorId+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!entry_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"entry_date = '"+entry_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",entry_date = '"+entry_date+"'";}
}

if(!major_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
}

if(!minorhead_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
}

if(!sub_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
}

if(!amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"amount = '"+amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",amount = '"+amount+"'";}
}

/*if(!expinv_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"expinv_id = '"+expinv_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",expinv_id = '"+expinv_id+"'";}
}*/

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}
String query="update productexpenses "+updatecc+" where extra1='Approved' and expinv_id='"+expinv_id+"'";
//System.out.println("status===>"+query1);
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}
             
}
public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from productexpenses where exp_id='"+exp_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}
}

	public boolean updateBrsDateBasedOnBktId(String brsDate, String banktrn_id){
		String UPDATE_QUERY = "UPDATE productexpenses SET extra10=? WHERE banktrn_id=?";
		PreparedStatement ps = null;
		boolean result = false;
		
		try {
			c = ConnectionHelper.getConnection();
			ps = c.prepareStatement(UPDATE_QUERY);
			ps.setString(1, brsDate);
			ps.setString(2, banktrn_id);
			
			result = ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				ps.close();
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		return result;
	}
	
	
	
	
	public boolean updateMajorMinorSubHeadProductHeads(){
		try
		{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		String updatecc="set ";
		 int jk=0; 
		
		   if(!major_head_id.equals("")){ 
			if(jk==0)
			{
			updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
			jk=1; 
			}else
			{
			updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
			}
		
			if(!minorhead_id.equals("")){ 
			if(jk==0)
			{
			updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
			jk=1; 
			}else
			{
			updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
			}

			if(!sub_head_id.equals("")){ 
			if(jk==0)
			{
			updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
			jk=1; 
			}else
			{
			updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
			}
			if(!extra2.equals("")){ 
				if(jk==0)
				{
				updatecc=updatecc+"extra2 = '"+extra2+"'"; 
				jk=1; 
				}else
				{
				updatecc=updatecc+",extra2 = '"+extra2+"'";}
				}

			if(!expinv_id.equals("")){ 
				if(jk==0)
				{
				updatecc=updatecc+"expinv_id = '"+expinv_id+"'"; 
				jk=1; 
				}else
				{
				updatecc=updatecc+",expinv_id = '"+expinv_id+"'";}
				}
			
			if(!exp_id.equals("")){ 
				if(jk==0)
				{
				updatecc=updatecc+"exp_id = '"+exp_id+"'"; 
				jk=1; 
				}else
				{
				updatecc=updatecc+",exp_id = '"+exp_id+"'";}
				}
			
			

			String query="update productexpenses "+updatecc+" where expinv_id='"+expinv_id+"'and exp_id='"+exp_id+"'";

			st.executeUpdate(query);
			st.close();
			c.close();
			return true;
			}catch(Exception e){
			this.seterror(e.toString());
			return false;
			}
	       }
	
	
	public boolean updateMajorMinorSubHeadsProductExpenses(String majorHead,String minorHead,String subHead,String expid,double amount,String entryDate){
		boolean result=false;
		String query= "UPDATE productexpenses SET major_head_id=?,minorhead_id=?,sub_head_id=?,amount=?,entry_date=?  WHERE exp_id=?";
		PreparedStatement ps = null;
		
		try {
			c = ConnectionHelper.getConnection();
			ps = c.prepareStatement(query);
			ps.setString(1, majorHead);
			ps.setString(2, minorHead);
			ps.setString(3,subHead);
			ps.setDouble(4,amount);
			ps.setString(5,entryDate);
			ps.setString(6,expid);
			result = ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				ps.close();
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		return result;
	}
}