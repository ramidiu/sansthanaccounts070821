package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import dbase.sqlcon.ConnectionHelper;

public class diagnosticissuesService {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	private String documentNo="";
	private String date="";
	private String appointmnetId="";
	private String testName="";
	private String amount=""; //actual amount
	private String extra1=""; //employee id
	private String extra2=""; //percentage
	private String extra3=""; //deducted amount
	private String extra4=""; //free or paid
	private String extra5="";
	
	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	public diagnosticissuesService()
	{
	/*try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	}catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}*/
	 }
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAppointmnetId() {
		return appointmnetId;
	}
	public void setAppointmnetId(String appointmnetId) {
		this.appointmnetId = appointmnetId;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	} 
	public boolean insert()
	{boolean flag = false;
	try
	{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	rs=st.executeQuery("select * from no_genarator where table_name='diagnosticissues'");
	rs.next();
	int ticket=rs.getInt("table_id");
	String PriSt =rs.getString("id_prefix");
	int ticketm=ticket+1;
	 documentNo=PriSt+ticketm;
	String query="insert into diagnosticissues values('"+documentNo+"','"+date+"','"+appointmnetId+"','"+testName+"','"+amount+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
	//System.out.println(query);
	st.executeUpdate(query);
	st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='diagnosticissues'");
	flag = true;
	}catch(Exception e){
	this.seterror(e.toString());
	e.printStackTrace();
	}finally{
		try {
			if(rs != null){rs.close();}
			if(st != null){st.close();}
			if(c != null){c.close();}
			} catch (SQLException e) {
			e.printStackTrace();
			}
	}
	return flag;

	}
	public boolean update()
	{boolean flag = false;
	try
	{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	String updatecc="set ";
	 int jk=0; 
	if(!date.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"date = '"+date+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",date = '"+date+"'";}
	}

	if(!appointmnetId.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"appointmnetId = '"+appointmnetId+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",appointmnetId = '"+appointmnetId+"'";}
	}

	if(!testName.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"testName = '"+testName+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",testName = '"+testName+"'";}
	}

	if(!amount.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"amount = '"+amount+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",amount = '"+amount+"'";}
	}

	if(!extra1.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra1 = '"+extra1+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra1 = '"+extra1+"'";}
	}

	if(!extra2.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra2 = '"+extra2+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra2 = '"+extra2+"'";}
	}

	if(!extra3.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra3 = '"+extra3+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra3 = '"+extra3+"'";}
	}

	if(!extra4.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra4 = '"+extra4+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra4 = '"+extra4+"'";}
	}

	if(!extra5.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra5 = '"+extra5+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra5 = '"+extra5+"'";}
	}
	String query="update diagnosticissues "+updatecc+" where documentNo='"+documentNo+"'";
	int i = st.executeUpdate(query);
	if(i>0){flag=true;}
	}catch(Exception e){
	this.seterror(e.toString());
	}finally{
		try {
			if(rs != null){rs.close();}
			if(st != null){st.close();}
			if(c != null){c.close();}
			} catch (SQLException e) {
			e.printStackTrace();
			}
	}
	return flag;

	}
	public boolean delete()
	{boolean flag = false;
	try
	{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	String query="delete from diagnosticissues where documentNo='"+documentNo+"'";
	int i = st.executeUpdate(query);
	if(i>0){flag=true;}
	}catch(Exception e){
	this.seterror(e.toString());
	}finally{
		try {
			if(rs != null){rs.close();}
			if(st != null){st.close();}
			if(c != null){c.close();}
			} catch (SQLException e) {
			e.printStackTrace();
			}
	}
	return flag;

	}

}
