package beans;
public class gatepass_entries 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String gp_id="";
private String entry_date="";
private String productId="";
private String qty="";
private String head_account_id="";
private String gatepass_number="";
private String entered_emp_id="";
private String status="";
private String narration="";
private String remarks="";
private String gatepass_category="";
private String updated_status="";
private String updated_date="";
private String updated_emp_id="";
private String return_qty="";
private String gatepass_takenby="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public gatepass_entries() {} 
public gatepass_entries(String gp_id,String entry_date,String productId,String qty,String head_account_id,String gatepass_number,String entered_emp_id,String status,String narration,String remarks,String gatepass_category,String updated_status,String updated_date,String updated_emp_id,String return_qty,String gatepass_takenby,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.gp_id=gp_id;
this.entry_date=entry_date;
this.productId=productId;
this.qty=qty;
this.head_account_id=head_account_id;
this.gatepass_number=gatepass_number;
this.entered_emp_id=entered_emp_id;
this.status=status;
this.narration=narration;
this.remarks=remarks;
this.gatepass_category=gatepass_category;
this.updated_status=updated_status;
this.updated_date=updated_date;
this.updated_emp_id=updated_emp_id;
this.return_qty=return_qty;
this.gatepass_takenby=gatepass_takenby;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getgp_id() {
return gp_id;
}
public void setgp_id(String gp_id) {
this.gp_id = gp_id;
}
public String getentry_date() {
return entry_date;
}
public void setentry_date(String entry_date) {
this.entry_date = entry_date;
}
public String getproductId() {
return productId;
}
public void setproductId(String productId) {
this.productId = productId;
}
public String getqty() {
return qty;
}
public void setqty(String qty) {
this.qty = qty;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getgatepass_number() {
return gatepass_number;
}
public void setgatepass_number(String gatepass_number) {
this.gatepass_number = gatepass_number;
}
public String getentered_emp_id() {
return entered_emp_id;
}
public void setentered_emp_id(String entered_emp_id) {
this.entered_emp_id = entered_emp_id;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getremarks() {
return remarks;
}
public void setremarks(String remarks) {
this.remarks = remarks;
}
public String getgatepass_category() {
return gatepass_category;
}
public void setgatepass_category(String gatepass_category) {
this.gatepass_category = gatepass_category;
}
public String getupdated_status() {
return updated_status;
}
public void setupdated_status(String updated_status) {
this.updated_status = updated_status;
}
public String getupdated_date() {
return updated_date;
}
public void setupdated_date(String updated_date) {
this.updated_date = updated_date;
}
public String getupdated_emp_id() {
return updated_emp_id;
}
public void setupdated_emp_id(String updated_emp_id) {
this.updated_emp_id = updated_emp_id;
}
public String getreturn_qty() {
return return_qty;
}
public void setreturn_qty(String return_qty) {
this.return_qty = return_qty;
}
public String getgatepass_takenby() {
return gatepass_takenby;
}
public void setgatepass_takenby(String gatepass_takenby) {
this.gatepass_takenby = gatepass_takenby;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}