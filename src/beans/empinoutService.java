package beans;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class empinoutService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String emplo_id="";
private String emp_timings="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";

private Statement st = null;
private Connection c=null;
private ResultSet rs=null;
public empinoutService()
{
/*try {
 // Load the database driver

}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setemplo_id(String emplo_id)
{
this.emplo_id = emplo_id;
}

public String getemplo_id()
{
return (this.emplo_id);
}


public void setemp_timings(String emp_timings)
{
this.emp_timings = emp_timings;
}

public String getemp_timings()
{
return (this.emp_timings);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert() throws SQLException
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="insert into empinout values('"+emplo_id+"','"+emp_timings+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
//System.out.println("#####insertempout"+query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();

String updatecc="set ";
 int jk=0; 
if(!emp_timings.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_timings = '"+emp_timings+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_timings = '"+emp_timings+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update empinout "+updatecc+" where emplo_id='"+emplo_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from empinout where emplo_id='"+emplo_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}