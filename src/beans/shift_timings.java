package beans;
public class shift_timings 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String shtime_id="";
private String shift_name="";
private String shift_starttitming="";
private String shift_endtiming="";
private String shift_code="";
private String extra2="";
private String extra3="";
public shift_timings() {} 
public shift_timings(String shtime_id,String shift_name,String shift_starttitming,String shift_endtiming,String shift_code,String extra2,String extra3)
 {
this.shtime_id=shtime_id;
this.shift_name=shift_name;
this.shift_starttitming=shift_starttitming;
this.shift_endtiming=shift_endtiming;
this.shift_code=shift_code;
this.extra2=extra2;
this.extra3=extra3;

} 
public String getshtime_id() {
return shtime_id;
}
public void setshtime_id(String shtime_id) {
this.shtime_id = shtime_id;
}
public String getshift_name() {
return shift_name;
}
public void setshift_name(String shift_name) {
this.shift_name = shift_name;
}
public String getshift_starttitming() {
return shift_starttitming;
}
public void setshift_starttitming(String shift_starttitming) {
this.shift_starttitming = shift_starttitming;
}
public String getshift_endtiming() {
return shift_endtiming;
}
public void setshift_endtiming(String shift_endtiming) {
this.shift_endtiming = shift_endtiming;
}
public String getshift_code() {
return shift_code;
}
public void setshift_code(String shift_code) {
this.shift_code = shift_code;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}

}