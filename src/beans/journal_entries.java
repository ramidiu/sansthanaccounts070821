package beans;
public class journal_entries 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String jv_id="";
private String created_date="";
private String sub_head_id="";
private String narration="";
private String debit_amount="";
private String credit_amount="";
private String jv_inv_no="";
private String voucher_ref_no="";
private String remarks="";
private String emp_id="";
private String status="";
private String head_account_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public journal_entries() {} 
public journal_entries(String jv_id,String created_date,String sub_head_id,String narration,String debit_amount,String credit_amount,String jv_inv_no,String voucher_ref_no,String remarks,String emp_id,String status,String head_account_id,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.jv_id=jv_id;
this.created_date=created_date;
this.sub_head_id=sub_head_id;
this.narration=narration;
this.debit_amount=debit_amount;
this.credit_amount=credit_amount;
this.jv_inv_no=jv_inv_no;
this.voucher_ref_no=voucher_ref_no;
this.remarks=remarks;
this.emp_id=emp_id;
this.status=status;
this.head_account_id=head_account_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getjv_id() {
return jv_id;
}
public void setjv_id(String jv_id) {
this.jv_id = jv_id;
}
public String getcreated_date() {
return created_date;
}
public void setcreated_date(String created_date) {
this.created_date = created_date;
}
public String getsub_head_id() {
return sub_head_id;
}
public void setsub_head_id(String sub_head_id) {
this.sub_head_id = sub_head_id;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getdebit_amount() {
return debit_amount;
}
public void setdebit_amount(String debit_amount) {
this.debit_amount = debit_amount;
}
public String getcredit_amount() {
return credit_amount;
}
public void setcredit_amount(String credit_amount) {
this.credit_amount = credit_amount;
}
public String getjv_inv_no() {
return jv_inv_no;
}
public void setjv_inv_no(String jv_inv_no) {
this.jv_inv_no = jv_inv_no;
}
public String getvoucher_ref_no() {
return voucher_ref_no;
}
public void setvoucher_ref_no(String voucher_ref_no) {
this.voucher_ref_no = voucher_ref_no;
}
public String getremarks() {
return remarks;
}
public void setremarks(String remarks) {
this.remarks = remarks;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}