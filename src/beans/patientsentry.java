package beans;
public class patientsentry 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String patientId="";
private String patientName="";
private String address="";
private String phone="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public patientsentry() {} 
public patientsentry(String patientId,String patientName,String address,String phone,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.patientId=patientId;
this.patientName=patientName;
this.address=address;
this.phone=phone;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getpatientId() {
return patientId;
}
public void setpatientId(String patientId) {
this.patientId = patientId;
}
public String getpatientName() {
return patientName;
}
public void setpatientName(String patientName) {
this.patientName = patientName;
}
public String getaddress() {
return address;
}
public void setaddress(String address) {
this.address = address;
}
public String getphone() {
return phone;
}
public void setphone(String phone) {
this.phone = phone;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}