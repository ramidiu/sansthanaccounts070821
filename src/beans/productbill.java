package beans;
public class productbill 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String productbill_id="";
private String total="";
private String discount="";
private String afterdiscount="";
private String login="";
private String tables="";
private String servicetax="";
private String servicecharges="";
private String createddate="";
private String status="";
private String remarks="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
private String extra8="";
public productbill() {} 
public productbill(String productbill_id,String total,String discount,String afterdiscount,String login,String tables,String servicetax,String servicecharges,String createddate,String status,String remarks,String extra1,String extra2,String extra3,String extra4,String extra5,String extra6,String extra7,String extra8)
 {
this.productbill_id=productbill_id;
this.total=total;
this.discount=discount;
this.afterdiscount=afterdiscount;
this.login=login;
this.tables=tables;
this.servicetax=servicetax;
this.servicecharges=servicecharges;
this.createddate=createddate;
this.status=status;
this.remarks=remarks;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;

} 
public String getproductbill_id() {
return productbill_id;
}
public void setproductbill_id(String productbill_id) {
this.productbill_id = productbill_id;
}
public String gettotal() {
return total;
}
public void settotal(String total) {
this.total = total;
}
public String getdiscount() {
return discount;
}
public void setdiscount(String discount) {
this.discount = discount;
}
public String getafterdiscount() {
return afterdiscount;
}
public void setafterdiscount(String afterdiscount) {
this.afterdiscount = afterdiscount;
}
public String getlogin() {
return login;
}
public void setlogin(String login) {
this.login = login;
}
public String gettables() {
return tables;
}
public void settables(String tables) {
this.tables = tables;
}
public String getservicetax() {
return servicetax;
}
public void setservicetax(String servicetax) {
this.servicetax = servicetax;
}
public String getservicecharges() {
return servicecharges;
}
public void setservicecharges(String servicecharges) {
this.servicecharges = servicecharges;
}
public String getcreateddate() {
return createddate;
}
public void setcreateddate(String createddate) {
this.createddate = createddate;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String getremarks() {
return remarks;
}
public void setremarks(String remarks) {
this.remarks = remarks;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}
public String getextra8() {
return extra8;
}
public void setextra8(String extra8) {
this.extra8 = extra8;
}

}