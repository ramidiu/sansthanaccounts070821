package beans;

public class stafftimings_details {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	private String staff="";
	 public void setstaff(String staff)
	{
	this.staff=staff;
	}
	public String getstaff()
	{
	return this.staff;
	}
	private String staff_id="";
	private String government_reservation="";
	private String caste_regligion="";
	private String dependents="";
	private String educational_qual="";
	private String technical_qual="";
	private String identification_marks="";
	private String previous_experience="";
	private String height="";
	private String physical_disorder="";
	private String present_pay_scale="";
	private String basic_pay="";
	private String reference="";
	private String graduvite_amount="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	private String extra6="";
	private String extra7="";
	private String extra8="";
	private String extra9="";
	public stafftimings_details(String staff_id,String government_reservation,String caste_regligion,String dependents,
			String educational_qual,String technical_qual,String identification_marks,String previous_experience,String height,
			String physical_disorder,String present_pay_scale,String basic_pay,String reference,String graduvite_amount,String extra1,
			String extra2,String extra3,String extra4,String extra5,String extra6,String extra7,String extra8,String extra9)
	{
		super();
		this.staff_id=staff_id;
		this.government_reservation =government_reservation;
		this.caste_regligion=caste_regligion;
		this.dependents=dependents;
		this.educational_qual=educational_qual;
		this.technical_qual=technical_qual;
		this.identification_marks=identification_marks;
		this.previous_experience=previous_experience;
		this.height=height;
		this.physical_disorder=physical_disorder;
		this.present_pay_scale=present_pay_scale;
		this.basic_pay=basic_pay;
		this.reference=reference;
		this.graduvite_amount=graduvite_amount;
		this.extra1=extra1;
		this.extra2=extra2;
		this.extra3=extra3;
		this.extra4=extra4;
		this.extra5=extra5;
		this.extra6=extra6;
		this.extra7=extra7;
		this.extra8=extra8;
		this.extra9=extra9;
		
	}
	public stafftimings_details()
	{
		
	}
	public stafftimings_details(String staff)
	{
		this.staff=staff;
	}
	public String getStaff() {
		return staff;
	}
	public void setStaff(String staff) {
		this.staff = staff;
	}
	public String getStaff_id() {
		return staff_id;
	}
	public void setStaff_id(String staff_id) {
		this.staff_id = staff_id;
	}
	public String getGovernment_reservation() {
		return government_reservation;
	}
	public void setGovernment_reservation(String government_reservation) {
		this.government_reservation = government_reservation;
	}
	public String getCaste_regligion() {
		return caste_regligion;
	}
	public void setCaste_regligion(String caste_regligion) {
		this.caste_regligion = caste_regligion;
	}
	public String getDependents() {
		return dependents;
	}
	public void setDependents(String dependents) {
		this.dependents = dependents;
	}
	public String getEducational_qual() {
		return educational_qual;
	}
	public void setEducational_qual(String educational_qual) {
		this.educational_qual = educational_qual;
	}
	public String getTechnical_qual() {
		return technical_qual;
	}
	public void setTechnical_qual(String technical_qual) {
		this.technical_qual = technical_qual;
	}
	public String getIdentification_marks() {
		return identification_marks;
	}
	public void setIdentification_marks(String identification_marks) {
		this.identification_marks = identification_marks;
	}
	public String getPrevious_experience() {
		return previous_experience;
	}
	public void setPrevious_experience(String previous_experience) {
		this.previous_experience = previous_experience;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getPhysical_disorder() {
		return physical_disorder;
	}
	public void setPhysical_disorder(String physical_disorder) {
		this.physical_disorder = physical_disorder;
	}
	public String getPresent_pay_scale() {
		return present_pay_scale;
	}
	public void setPresent_pay_scale(String present_pay_scale) {
		this.present_pay_scale = present_pay_scale;
	}
	public String getBasic_pay() {
		return basic_pay;
	}
	public void setBasic_pay(String basic_pay) {
		this.basic_pay = basic_pay;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getGraduvite_amount() {
		return graduvite_amount;
	}
	public void setGraduvite_amount(String graduvite_amount) {
		this.graduvite_amount = graduvite_amount;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	

}
