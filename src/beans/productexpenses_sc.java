package beans;

public class productexpenses_sc {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	private String exp_id="";
	private String vendorId="";
	private String narration="";
	private String entry_date="";
	private String major_head_id="";
	private String minorhead_id="";
	private String sub_head_id="";
	private String amount="";
	private String expinv_id="";
	private String emp_id="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	private String head_account_id="";
	private String enter_by="";
	private String enter_date="";
	private String extra6="";
	private String extra7="";
	private String extra8="";
	private String extra9="";
	private String extra10="";
	private String extra11="";
	private String extra12="";
	private String extra13="";
	private String extra14="";
	private String extra15="";
	private String extra16="";
	private String extra17="";
	private String extra18="";
	private String extra19="";
	private String extra20="";
	private String extra21="";
	private String extra22="";
	private String extra23="";
	private String extra24="";
	private String extra25="";
	
	public String getExtra11() {
		return extra11;
	}
	public void setExtra11(String extra11) {
		this.extra11 = extra11;
	}
	public String getExtra12() {
		return extra12;
	}
	public void setExtra12(String extra12) {
		this.extra12 = extra12;
	}
	public String getExtra13() {
		return extra13;
	}
	public void setExtra13(String extra13) {
		this.extra13 = extra13;
	}
	public String getExtra14() {
		return extra14;
	}
	public void setExtra14(String extra14) {
		this.extra14 = extra14;
	}
	public String getExtra15() {
		return extra15;
	}
	public void setExtra15(String extra15) {
		this.extra15 = extra15;
	}
	public String getExtra16() {
		return extra16;
	}
	public void setExtra16(String extra16) {
		this.extra16 = extra16;
	}
	public String getExtra17() {
		return extra17;
	}
	public void setExtra17(String extra17) {
		this.extra17 = extra17;
	}
	public String getExtra18() {
		return extra18;
	}
	public void setExtra18(String extra18) {
		this.extra18 = extra18;
	}
	public String getExtra19() {
		return extra19;
	}
	public void setExtra19(String extra19) {
		this.extra19 = extra19;
	}
	public String getExtra20() {
		return extra20;
	}
	public void setExtra20(String extra20) {
		this.extra20 = extra20;
	}
	public String getExtra21() {
		return extra21;
	}
	public void setExtra21(String extra21) {
		this.extra21 = extra21;
	}
	public String getExtra22() {
		return extra22;
	}
	public void setExtra22(String extra22) {
		this.extra22 = extra22;
	}
	public String getExtra23() {
		return extra23;
	}
	public void setExtra23(String extra23) {
		this.extra23 = extra23;
	}
	public String getExtra24() {
		return extra24;
	}
	public void setExtra24(String extra24) {
		this.extra24 = extra24;
	}
	public String getExtra25() {
		return extra25;
	}
	public void setExtra25(String extra25) {
		this.extra25 = extra25;
	}
	public productexpenses_sc() {}
	public productexpenses_sc(String exp_id,String vendorId,String narration,String entry_date,String major_head_id,String minorhead_id,String sub_head_id,String amount,String expinv_id,String emp_id,String extra1,String extra2,String extra3,String extra4,String extra5,String head_account_id,String enter_by,String enter_date,String extra6,String extra7,String extra8,String extra9,String extra10)
	{
	this.exp_id=exp_id;
	this.vendorId=vendorId;
	this.narration=narration;
	this.entry_date=entry_date;
	this.major_head_id=major_head_id;
	this.minorhead_id=minorhead_id;
	this.sub_head_id=sub_head_id;
	this.amount=amount;
	this.expinv_id=expinv_id;
	this.emp_id=emp_id;
	this.extra1=extra1;
	this.extra2=extra2;
	this.extra3=extra3;
	this.extra4=extra4;
	this.extra5=extra5;
	this.head_account_id=head_account_id;
	this.enter_by=enter_by;
	this.enter_date=enter_date;
	this.extra6=extra6;
	this.extra7=extra7;
	this.extra8=extra8;
	this.extra9=extra9;
	this.extra10=extra10;
	}
	public String getExp_id() {
		return exp_id;
	}
	public void setExp_id(String exp_id) {
		this.exp_id = exp_id;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getEntry_date() {
		return entry_date;
	}
	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}
	public String getMajor_head_id() {
		return major_head_id;
	}
	public void setMajor_head_id(String major_head_id) {
		this.major_head_id = major_head_id;
	}
	public String getMinorhead_id() {
		return minorhead_id;
	}
	public void setMinorhead_id(String minorhead_id) {
		this.minorhead_id = minorhead_id;
	}
	public String getSub_head_id() {
		return sub_head_id;
	}
	public void setSub_head_id(String sub_head_id) {
		this.sub_head_id = sub_head_id;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getExpinv_id() {
		return expinv_id;
	}
	public void setExpinv_id(String expinv_id) {
		this.expinv_id = expinv_id;
	}
	public String getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getHead_account_id() {
		return head_account_id;
	}
	public void setHead_account_id(String head_account_id) {
		this.head_account_id = head_account_id;
	}
	public String getEnter_by() {
		return enter_by;
	}
	public void setEnter_by(String enter_by) {
		this.enter_by = enter_by;
	}
	public String getEnter_date() {
		return enter_date;
	}
	public void setEnter_date(String enter_date) {
		this.enter_date = enter_date;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}
	
}
