package beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import dbase.sqlcon.ConnectionHelper;

public class banktransactionsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String banktrn_id="";
private String bank_id="";
private String name="";
private String narration="";
private String date="";
private String amount="";
private String type="";
private String createdBy="";
private String editedBy="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String brs_date="";
private String extra6="";
private String extra7="";
private String extra8="";
private String sub_head_id="";
private String minor_head_id="";
private String major_head_id="";
private String head_account_id="";
private String extra9="";
private String extra10="";
private String extra11="";
private String extra12="";
private String extra13="";

public String getExtra6() {
	return extra6;
}
public void setExtra6(String extra6) {
	this.extra6 = extra6;
}
public String getExtra7() {
	return extra7;
}
public void setExtra7(String extra7) {
	this.extra7 = extra7;
}
public String getExtra8() {
	return extra8;
}
public void setExtra8(String extra8) {
	this.extra8 = extra8;
}
public String getSub_head_id() {
	return sub_head_id;
}
public void setSub_head_id(String sub_head_id) {
	this.sub_head_id = sub_head_id;
}
public String getMinor_head_id() {
	return minor_head_id;
}
public void setMinor_head_id(String minor_head_id) {
	this.minor_head_id = minor_head_id;
}
public String getMajor_head_id() {
	return major_head_id;
}
public void setMajor_head_id(String major_head_id) {
	this.major_head_id = major_head_id;
}
public String getHead_account_id() {
	return head_account_id;
}
public void setHead_account_id(String head_account_id) {
	this.head_account_id = head_account_id;
}
public String getExtra9() {
	return extra9;
}
public void setExtra9(String extra9) {
	this.extra9 = extra9;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}
private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public banktransactionsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setbanktrn_id(String banktrn_id)
{
this.banktrn_id = banktrn_id;
}

public String getbanktrn_id()
{
return (this.banktrn_id);
}

public void setbrs_date(String brs_date)
{
this.brs_date = brs_date;
}

public String getbrs_date()
{
return (this.brs_date);
}
public void setbank_id(String bank_id)
{
this.bank_id = bank_id;
}

public String getbank_id()
{
return (this.bank_id);
}


public void setname(String name)
{
this.name = name;
}

public String getname()
{
return (this.name);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setdate(String date)
{
this.date = date;
}

public String getdate()
{
return (this.date);
}


public void setamount(String amount)
{
this.amount = amount;
}

public String getamount()
{
return (this.amount);
}


public void settype(String type)
{
this.type = type;
}

public String gettype()
{
return (this.type);
}


public void setcreatedBy(String createdBy)
{
this.createdBy = createdBy;
}

public String getcreatedBy()
{
return (this.createdBy);
}


public void seteditedBy(String editedBy)
{
this.editedBy = editedBy;
}

public String geteditedBy()
{
return (this.editedBy);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
String date1=dateFormat2.format(dateFormat.parse(date));
String findate="2019-04-01";
System.out.println("peint date..."+date1.compareTo(findate));
if(date1.compareTo(findate)>=0){
	rs=st.executeQuery("select * from no_genarator where table_name='banktransactions'");
}
else{
	rs=st.executeQuery("select * from no_genarator where table_name='banktransactions_prev'");
}
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 banktrn_id=PriSt+ticketm;
/*String query="insert into banktransactions values('"+banktrn_id+"','"+bank_id+"','"+name+"','"+narration+"','"+date+"','"+amount+"','"+type+"','"+createdBy+"','"+editedBy+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+brs_date+"','"+extra6+"','"+extra7+"','"+extra8+"')";*/

 String query="insert into banktransactions values('"+banktrn_id+"','"+bank_id+"','"+name+"','"+narration+"','"+date+"','"+amount+"','"+type+"','"+createdBy+"','"+editedBy+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+brs_date+"','"+extra6+"','"+date+"','"+extra8+"','"+sub_head_id+"','"+minor_head_id+"','"+major_head_id+"','"+head_account_id+"','"+extra9+"','"+extra10+"','"+extra11+"','"+extra12+"','"+extra13+"')";
System.out.println("query================================= "+query);
 //String query2="insert into banktransactions2 values('"+banktrn_id+"','"+bank_id+"','"+name+"','"+narration+"','"+date+"','"+amount+"','"+type+"','"+createdBy+"','"+editedBy+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+brs_date+"','"+extra6+"','"+date+"','"+extra8+"','"+sub_head_id+"','"+minor_head_id+"','"+major_head_id+"','"+head_account_id+"','"+extra9+"','"+extra10+"','"+extra11+"','"+extra12+"','"+extra13+"')";
//System.out.println(query);
//System.out.println(query2);
st.executeUpdate(query);
//st.executeUpdate(query2);
if(date1.compareTo(findate)>=0){
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='banktransactions'");
}
else{
	st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='banktransactions_prev'");
}
flag = true;
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}

public int saveBankTransaction(banktransactions bkt)	{
	PreparedStatement ps = null;
	ResultSet rs = null;
	String insertQuery = "insert into banktransactions values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	int result = 0;
	try	{
		c = ConnectionHelper.getConnection();
		st = c.createStatement();
		ps = c.prepareStatement(insertQuery);
		rs = st.executeQuery("select * from no_genarator where table_name='banktransactions'");
		if (rs.next())	{
			int ticket=rs.getInt("table_id");
			String PriSt =rs.getString("id_prefix");
			int ticketm=ticket+1;
			bkt.setbanktrn_id(PriSt+ticketm);
		}
		rs.close();
		// set query parameters
		ps.setString(1,bkt.getbanktrn_id());
		ps.setString(2,bkt.getbank_id());
		ps.setString(3,bkt.getname());
		ps.setString(4,bkt.getnarration());
		ps.setString(5,bkt.getdate());
		ps.setString(6,bkt.getamount());
		ps.setString(7,bkt.gettype());
		ps.setString(8,bkt.getcreatedBy());
		ps.setString(9,bkt.geteditedBy());
		ps.setString(10,bkt.getextra1());
		ps.setString(11,bkt.getextra2());
		ps.setString(12,bkt.getextra3());
		ps.setString(13,bkt.getextra4());
		ps.setString(14,bkt.getextra5());
		ps.setString(15,bkt.getbrs_date());
		ps.setString(16,bkt.getExtra6());
		ps.setString(17,bkt.getExtra7());
		ps.setString(18,bkt.getExtra8());
		ps.setString(19,bkt.getSub_head_id());
		ps.setString(20,bkt.getMinor_head_id());
		ps.setString(21,bkt.getMajor_head_id());
		ps.setString(22,bkt.getHead_account_id());
		ps.setString(23,bkt.getExtra9());
		ps.setString(24,bkt.getExtra10());
		ps.setString(25,bkt.getExtra11());
		ps.setString(26,bkt.getExtra12());
		ps.setString(27,bkt.getExtra13());
		
		result = ps.executeUpdate();
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	catch(Exception se)	{
		se.printStackTrace();
	}
	finally	{
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
return result;
}




public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!bank_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"bank_id = '"+bank_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",bank_id = '"+bank_id+"'";}
}

if(!name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"name = '"+name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",name = '"+name+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date = '"+date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date = '"+date+"'";}
}

if(!amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"amount = '"+amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",amount = '"+amount+"'";}
}

if(!type.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"type = '"+type+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",type = '"+type+"'";}
}

if(!createdBy.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"createdBy = '"+createdBy+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",createdBy = '"+createdBy+"'";}
}

if(!editedBy.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"editedBy = '"+editedBy+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",editedBy = '"+editedBy+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
if(!brs_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"brs_date = '"+brs_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",brs_date = '"+brs_date+"'";}
}
String query="update banktransactions "+updatecc+" where banktrn_id='"+banktrn_id+"'";
System.out.println(query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
public boolean updateByCondition(String depositType,String HOA,String major_head_id,String minor_head_id,String sub_head_id)
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="update banktransactions set extra8='"+depositType+"',head_account_id='"+HOA+"',major_head_id='"+major_head_id+"',minor_head_id='"+minor_head_id+"',sub_head_id='"+sub_head_id+"' where banktrn_id='"+banktrn_id+"'";
//System.out.println("===>"+query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from banktransactions where banktrn_id='"+banktrn_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}

	public boolean insertBankTransactions(){
		boolean flag = false;
		try{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			
			String query="insert into banktransactions values('"+banktrn_id+"','"+bank_id+"','"+name+"','"+narration+"','"+date+"','"+amount+"','"+type+"','"+createdBy+"','"+editedBy+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+brs_date+"','"+extra6+"','"+date+"','"+extra8+"','"+sub_head_id+"','"+minor_head_id+"','"+major_head_id+"','"+head_account_id+"','"+extra9+"','"+extra10+"','"+extra11+"','"+extra12+"','"+extra13+"')";
			int i = st.executeUpdate(query);
			System.out.println("banktransaction--------->>"+query);
			System.out.println("bkt inserted------>"+i);
			flag = true;
		}catch(Exception e){
			this.seterror(e.toString());
		}finally{
			try {
				if(rs != null){
					rs.close();
				}
				if(st != null){
					st.close();
				}
				if(c != null){
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
	
	public int updateExtra3AndBrsDateBasdOnBkt(String banktrnId,String jvNumber,String brsDate)	{
		PreparedStatement ps = null;
		String updateQuery = "";
		if (brsDate != null && !brsDate.equals(""))	{
			updateQuery = "update banktransactions set extra3 = ?,brs_date = ? where banktrn_id = ?";
		}
		else	{
			updateQuery = "update banktransactions set extra3 = ? where banktrn_id = ?";
		}
		int result = 0;
		try	{
			c = ConnectionHelper.getConnection();
			ps = c.prepareStatement(updateQuery);
			ps.setString(1,banktrnId);
			ps.setString(2,jvNumber);
			if (brsDate != null && !brsDate.equals(""))	{
				ps.setString(3,brsDate);
			}
			System.out.println("pstsmt------"+ps.toString());
			result = ps.executeUpdate();
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception se)	{
			se.printStackTrace();
		}
		finally	{
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return result;
	}
	
	public int updateJvAndBrsDate(String jvdate,String jvnumber)	{
		Calendar cal=Calendar.getInstance();
		DateFormat df= new SimpleDateFormat("HH:mm:ss");
		String subdate=df.format(cal.getTime());
		int result=0;
		try	{
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query="update banktransactions set date='"+jvdate+" "+subdate+"',brs_date= '"+jvdate+"' where extra3='"+jvnumber+"'";
			System.out.println("update jv brs date ============>"+query);
			result=st.executeUpdate(query);
			c.close();
			st.close();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
	return result;
	}
	
	public int deleteBankTxn(String jvnumber)	{
		int result=0;
		try	{
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query="delete from banktransactions where extra3='"+jvnumber+"'";
			System.out.println("deleteBankTxn jv ============>"+query);
			result=st.executeUpdate(query);
		//	System.out.println("delete jv in cp(deletejv method)==========>"+result);
			c.close();
			st.close();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
	return result;
	}
	
	public int updateMajorMinorSubHeadsBKT(String majorHead,String minorHead,String subHead,String bankTransactionId,String amount,String entryDate)	{
		int result=0;
		try	{
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query="update banktransactions set major_head_id='"+majorHead+"',minor_head_id='"+minorHead+"',sub_head_id='"+subHead+"',amount='"+amount+"',date='"+entryDate+"' where banktrn_id='"+bankTransactionId+"'";
			result=st.executeUpdate(query);
			c.close();
			st.close();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
	return result;
	}
}