package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class bankdetailsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String bank_id="";
private String bank_name="";
private String bank_branch="";
private String account_holder_name="";
private String account_number="";
private String ifsc_code="";
private String total_amount="";
private String headAccountId="";
private String createdBy="";
private String editedBy="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public bankdetailsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setbank_id(String bank_id)
{
this.bank_id = bank_id;
}

public String getbank_id()
{
return (this.bank_id);
}


public void setbank_name(String bank_name)
{
this.bank_name = bank_name;
}

public String getbank_name()
{
return (this.bank_name);
}


public void setbank_branch(String bank_branch)
{
this.bank_branch = bank_branch;
}

public String getbank_branch()
{
return (this.bank_branch);
}


public void setaccount_holder_name(String account_holder_name)
{
this.account_holder_name = account_holder_name;
}

public String getaccount_holder_name()
{
return (this.account_holder_name);
}


public void setaccount_number(String account_number)
{
this.account_number = account_number;
}

public String getaccount_number()
{
return (this.account_number);
}


public void setifsc_code(String ifsc_code)
{
this.ifsc_code = ifsc_code;
}

public String getifsc_code()
{
return (this.ifsc_code);
}


public void settotal_amount(String total_amount)
{
this.total_amount = total_amount;
}

public String gettotal_amount()
{
return (this.total_amount);
}


public void setheadAccountId(String headAccountId)
{
this.headAccountId = headAccountId;
}

public String getheadAccountId()
{
return (this.headAccountId);
}


public void setcreatedBy(String createdBy)
{
this.createdBy = createdBy;
}

public String getcreatedBy()
{
return (this.createdBy);
}


public void seteditedBy(String editedBy)
{
this.editedBy = editedBy;
}

public String geteditedBy()
{
return (this.editedBy);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='bankdetails'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
bank_id=PriSt+ticketm;
String query="insert into bankdetails values('"+bank_id+"','"+bank_name+"','"+bank_branch+"','"+account_holder_name+"','"+account_number+"','"+ifsc_code+"','"+total_amount+"','"+headAccountId+"','"+createdBy+"','"+editedBy+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
st.executeUpdate(query);
int i = st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='bankdetails'");
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}

public boolean updateAsActiveOrDeactive(String bankId,String activeOrDeactive)
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="update bankdetails set extra5='"+activeOrDeactive+"' where bank_id='"+bankId+"'";
//System.out.println(query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}

public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!bank_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"bank_name = '"+bank_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",bank_name = '"+bank_name+"'";}
}

if(!bank_branch.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"bank_branch = '"+bank_branch+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",bank_branch = '"+bank_branch+"'";}
}

if(!account_holder_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"account_holder_name = '"+account_holder_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",account_holder_name = '"+account_holder_name+"'";}
}

if(!account_number.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"account_number = '"+account_number+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",account_number = '"+account_number+"'";}
}

if(!ifsc_code.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"ifsc_code = '"+ifsc_code+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",ifsc_code = '"+ifsc_code+"'";}
}

if(!total_amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"total_amount = '"+total_amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",total_amount = '"+total_amount+"'";}
}

if(!headAccountId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"headAccountId = '"+headAccountId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",headAccountId = '"+headAccountId+"'";}
}

if(!createdBy.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"createdBy = '"+createdBy+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",createdBy = '"+createdBy+"'";}
}

if(!editedBy.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"editedBy = '"+editedBy+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",editedBy = '"+editedBy+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update bankdetails "+updatecc+" where bank_id='"+bank_id+"'";
System.out.println("udate query bank details table====> "+query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from bankdetails where bank_id='"+bank_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}
}