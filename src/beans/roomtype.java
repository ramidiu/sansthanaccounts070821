package beans;
public class roomtype 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String roomtype_id="";
private String roomtype_name="";
private String adults="";
private String childerns="";
private String default_price="";
private String allow_extrabed="";
private String extrabed_charge="";
private String noof_rooms="";
private String image="";
private String image_name="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";

private String charges="";

private String priority="";

private String rty="";

private String newr="";

private String newextra1="";

public roomtype() {} 
public roomtype(String roomtype_id,String roomtype_name,String adults,String childerns,String default_price,String allow_extrabed,String extrabed_charge,String noof_rooms,String image,String image_name,String extra1,String extra2,String extra3,String extra4,String extra5,String extra6,String extra7,String extra8,String extra9,String extra10)
 {
this.roomtype_id=roomtype_id;
this.roomtype_name=roomtype_name;
this.adults=adults;
this.childerns=childerns;
this.default_price=default_price;
this.allow_extrabed=allow_extrabed;
this.extrabed_charge=extrabed_charge;
this.noof_rooms=noof_rooms;
this.image=image;
this.image_name=image_name;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;
this.extra10=extra10;

} 

public roomtype(String roomtype_id,String roomtype_name,String adults,String childerns,String default_price,String allow_extrabed,String extrabed_charge,String noof_rooms,String image,String image_name,String extra1,String extra2,String extra3,String extra4,String extra5,String extra6,String extra7,String extra8,String extra9,String extra10,String charges,String priority,String rty,String newr,String newextra1)
{
this.roomtype_id=roomtype_id;
this.roomtype_name=roomtype_name;
this.adults=adults;
this.childerns=childerns;
this.default_price=default_price;
this.allow_extrabed=allow_extrabed;
this.extrabed_charge=extrabed_charge;
this.noof_rooms=noof_rooms;
this.image=image;
this.image_name=image_name;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;
this.extra10=extra10;
this.charges=charges;
this.priority=priority;
this.rty=rty;
this.newr=newr;
this.newextra1=newextra1;

} 
public String getroomtype_id() {
return roomtype_id;
}
public void setroomtype_id(String roomtype_id) {
this.roomtype_id = roomtype_id;
}
public String getroomtype_name() {
return roomtype_name;
}
public void setroomtype_name(String roomtype_name) {
this.roomtype_name = roomtype_name;
}
public String getadults() {
return adults;
}
public void setadults(String adults) {
this.adults = adults;
}
public String getchilderns() {
return childerns;
}
public void setchilderns(String childerns) {
this.childerns = childerns;
}
public String getdefault_price() {
return default_price;
}
public void setdefault_price(String default_price) {
this.default_price = default_price;
}
public String getallow_extrabed() {
return allow_extrabed;
}
public void setallow_extrabed(String allow_extrabed) {
this.allow_extrabed = allow_extrabed;
}
public String getextrabed_charge() {
return extrabed_charge;
}
public void setextrabed_charge(String extrabed_charge) {
this.extrabed_charge = extrabed_charge;
}
public String getnoof_rooms() {
return noof_rooms;
}
public void setnoof_rooms(String noof_rooms) {
this.noof_rooms = noof_rooms;
}
public String getimage() {
return image;
}
public void setimage(String image) {
this.image = image;
}
public String getimage_name() {
return image_name;
}
public void setimage_name(String image_name) {
this.image_name = image_name;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}
public String getextra8() {
return extra8;
}
public void setextra8(String extra8) {
this.extra8 = extra8;
}
public String getextra9() {
return extra9;
}
public void setextra9(String extra9) {
this.extra9 = extra9;
}


public String getextra10() {
	return extra10;
	}
	public void setextra10(String extra10) {
	this.extra10 = extra10;
	}
public String getcharges() {
		return charges;
		}
		public void setcharges(String charges) {
		this.charges = charges;
		}
public String getpriority() {
			return priority;
			}
public void setpriority(String priority) {
			this.priority = priority;
			}
public String getrty() {
	return rty;
	}
public void setrty(String rty) {
	this.rty = rty;
	}
public String getnewr() {
	return newr;
	}
public void setnewr(String newr) {
	this.newr = newr;
	}	
public String getnewextra1() {
	return newextra1;
	}
public void setnewextra1(String newextra1) {
	this.newextra1 = newextra1;
	}

}