package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class advanceamountService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String advanceamount_id="";
private String datetime="";
private String master_suite="";
private String executive_suite="";
private String deluxe_suite="";
private String suite="";
private String deluxe_room_ac="";
private String special_room_nonac="";
private String deluxe_room_nonac="";
private String deluxe_room_ac_2beds="";
private String banquet_hall_5th_floor="";
private String banquet_hall_4th_floor="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String net_date="";
private String gross_master_suite="";
private String gross_executive_suite="";
private String gross_deluxe_suite="";
private String gross_suite="";
private String gross_deluxe_room_ac="";
private String gross_special_room_nonac="";
private String gross_deluxe_room_nonac="";
private String gross_deluxe_room_ac_2beds="";
private String gross_banquet_hall_5th_floor="";
private String gross_banquet_hall_4th_floor="";
private String gross_date="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public advanceamountService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setadvanceamount_id(String advanceamount_id)
{
this.advanceamount_id = advanceamount_id;
}

public String getadvanceamount_id()
{
return (this.advanceamount_id);
}


public void setdatetime(String datetime)
{
this.datetime = datetime;
}

public String getdatetime()
{
return (this.datetime);
}


public void setmaster_suite(String master_suite)
{
this.master_suite = master_suite;
}

public String getmaster_suite()
{
return (this.master_suite);
}


public void setexecutive_suite(String executive_suite)
{
this.executive_suite = executive_suite;
}

public String getexecutive_suite()
{
return (this.executive_suite);
}


public void setdeluxe_suite(String deluxe_suite)
{
this.deluxe_suite = deluxe_suite;
}

public String getdeluxe_suite()
{
return (this.deluxe_suite);
}


public void setsuite(String suite)
{
this.suite = suite;
}

public String getsuite()
{
return (this.suite);
}


public void setdeluxe_room_ac(String deluxe_room_ac)
{
this.deluxe_room_ac = deluxe_room_ac;
}

public String getdeluxe_room_ac()
{
return (this.deluxe_room_ac);
}


public void setspecial_room_nonac(String special_room_nonac)
{
this.special_room_nonac = special_room_nonac;
}

public String getspecial_room_nonac()
{
return (this.special_room_nonac);
}


public void setdeluxe_room_nonac(String deluxe_room_nonac)
{
this.deluxe_room_nonac = deluxe_room_nonac;
}

public String getdeluxe_room_nonac()
{
return (this.deluxe_room_nonac);
}


public void setdeluxe_room_ac_2beds(String deluxe_room_ac_2beds)
{
this.deluxe_room_ac_2beds = deluxe_room_ac_2beds;
}

public String getdeluxe_room_ac_2beds()
{
return (this.deluxe_room_ac_2beds);
}


public void setbanquet_hall_5th_floor(String banquet_hall_5th_floor)
{
this.banquet_hall_5th_floor = banquet_hall_5th_floor;
}

public String getbanquet_hall_5th_floor()
{
return (this.banquet_hall_5th_floor);
}


public void setbanquet_hall_4th_floor(String banquet_hall_4th_floor)
{
this.banquet_hall_4th_floor = banquet_hall_4th_floor;
}

public String getbanquet_hall_4th_floor()
{
return (this.banquet_hall_4th_floor);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void setnet_date(String net_date)
{
this.net_date = net_date;
}

public String getnet_date()
{
return (this.net_date);
}


public void setgross_master_suite(String gross_master_suite)
{
this.gross_master_suite = gross_master_suite;
}

public String getgross_master_suite()
{
return (this.gross_master_suite);
}


public void setgross_executive_suite(String gross_executive_suite)
{
this.gross_executive_suite = gross_executive_suite;
}

public String getgross_executive_suite()
{
return (this.gross_executive_suite);
}


public void setgross_deluxe_suite(String gross_deluxe_suite)
{
this.gross_deluxe_suite = gross_deluxe_suite;
}

public String getgross_deluxe_suite()
{
return (this.gross_deluxe_suite);
}


public void setgross_suite(String gross_suite)
{
this.gross_suite = gross_suite;
}

public String getgross_suite()
{
return (this.gross_suite);
}


public void setgross_deluxe_room_ac(String gross_deluxe_room_ac)
{
this.gross_deluxe_room_ac = gross_deluxe_room_ac;
}

public String getgross_deluxe_room_ac()
{
return (this.gross_deluxe_room_ac);
}


public void setgross_special_room_nonac(String gross_special_room_nonac)
{
this.gross_special_room_nonac = gross_special_room_nonac;
}

public String getgross_special_room_nonac()
{
return (this.gross_special_room_nonac);
}


public void setgross_deluxe_room_nonac(String gross_deluxe_room_nonac)
{
this.gross_deluxe_room_nonac = gross_deluxe_room_nonac;
}

public String getgross_deluxe_room_nonac()
{
return (this.gross_deluxe_room_nonac);
}


public void setgross_deluxe_room_ac_2beds(String gross_deluxe_room_ac_2beds)
{
this.gross_deluxe_room_ac_2beds = gross_deluxe_room_ac_2beds;
}

public String getgross_deluxe_room_ac_2beds()
{
return (this.gross_deluxe_room_ac_2beds);
}


public void setgross_banquet_hall_5th_floor(String gross_banquet_hall_5th_floor)
{
this.gross_banquet_hall_5th_floor = gross_banquet_hall_5th_floor;
}

public String getgross_banquet_hall_5th_floor()
{
return (this.gross_banquet_hall_5th_floor);
}


public void setgross_banquet_hall_4th_floor(String gross_banquet_hall_4th_floor)
{
this.gross_banquet_hall_4th_floor = gross_banquet_hall_4th_floor;
}

public String getgross_banquet_hall_4th_floor()
{
return (this.gross_banquet_hall_4th_floor);
}


public void setgross_date(String gross_date)
{
this.gross_date = gross_date;
}

public String getgross_date()
{
return (this.gross_date);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='advanceamount'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 advanceamount_id=PriSt+ticketm;
String query="insert into advanceamount values('"+advanceamount_id+"','"+datetime+"','"+master_suite+"','"+executive_suite+"','"+deluxe_suite+"','"+suite+"','"+deluxe_room_ac+"','"+special_room_nonac+"','"+deluxe_room_nonac+"','"+deluxe_room_ac_2beds+"','"+banquet_hall_5th_floor+"','"+banquet_hall_4th_floor+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+net_date+"','"+gross_master_suite+"','"+gross_executive_suite+"','"+gross_deluxe_suite+"','"+gross_suite+"','"+gross_deluxe_room_ac+"','"+gross_special_room_nonac+"','"+gross_deluxe_room_nonac+"','"+gross_deluxe_room_ac_2beds+"','"+gross_banquet_hall_5th_floor+"','"+gross_banquet_hall_4th_floor+"','"+gross_date+"')";
st.executeUpdate(query);
int i = st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='advanceamount'");
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!datetime.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"datetime = '"+datetime+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",datetime = '"+datetime+"'";}
}

if(!master_suite.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"master_suite = '"+master_suite+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",master_suite = '"+master_suite+"'";}
}

if(!executive_suite.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"executive_suite = '"+executive_suite+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",executive_suite = '"+executive_suite+"'";}
}

if(!deluxe_suite.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"deluxe_suite = '"+deluxe_suite+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",deluxe_suite = '"+deluxe_suite+"'";}
}

if(!suite.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"suite = '"+suite+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",suite = '"+suite+"'";}
}

if(!deluxe_room_ac.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"deluxe_room_ac = '"+deluxe_room_ac+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",deluxe_room_ac = '"+deluxe_room_ac+"'";}
}

if(!special_room_nonac.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"special_room_nonac = '"+special_room_nonac+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",special_room_nonac = '"+special_room_nonac+"'";}
}

if(!deluxe_room_nonac.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"deluxe_room_nonac = '"+deluxe_room_nonac+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",deluxe_room_nonac = '"+deluxe_room_nonac+"'";}
}

if(!deluxe_room_ac_2beds.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"deluxe_room_ac_2beds = '"+deluxe_room_ac_2beds+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",deluxe_room_ac_2beds = '"+deluxe_room_ac_2beds+"'";}
}

if(!banquet_hall_5th_floor.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"banquet_hall_5th_floor = '"+banquet_hall_5th_floor+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",banquet_hall_5th_floor = '"+banquet_hall_5th_floor+"'";}
}

if(!banquet_hall_4th_floor.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"banquet_hall_4th_floor = '"+banquet_hall_4th_floor+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",banquet_hall_4th_floor = '"+banquet_hall_4th_floor+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!net_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"net_date = '"+net_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",net_date = '"+net_date+"'";}
}

if(!gross_master_suite.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_master_suite = '"+gross_master_suite+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_master_suite = '"+gross_master_suite+"'";}
}

if(!gross_executive_suite.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_executive_suite = '"+gross_executive_suite+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_executive_suite = '"+gross_executive_suite+"'";}
}

if(!gross_deluxe_suite.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_deluxe_suite = '"+gross_deluxe_suite+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_deluxe_suite = '"+gross_deluxe_suite+"'";}
}

if(!gross_suite.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_suite = '"+gross_suite+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_suite = '"+gross_suite+"'";}
}

if(!gross_deluxe_room_ac.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_deluxe_room_ac = '"+gross_deluxe_room_ac+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_deluxe_room_ac = '"+gross_deluxe_room_ac+"'";}
}

if(!gross_special_room_nonac.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_special_room_nonac = '"+gross_special_room_nonac+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_special_room_nonac = '"+gross_special_room_nonac+"'";}
}

if(!gross_deluxe_room_nonac.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_deluxe_room_nonac = '"+gross_deluxe_room_nonac+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_deluxe_room_nonac = '"+gross_deluxe_room_nonac+"'";}
}

if(!gross_deluxe_room_ac_2beds.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_deluxe_room_ac_2beds = '"+gross_deluxe_room_ac_2beds+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_deluxe_room_ac_2beds = '"+gross_deluxe_room_ac_2beds+"'";}
}

if(!gross_banquet_hall_5th_floor.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_banquet_hall_5th_floor = '"+gross_banquet_hall_5th_floor+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_banquet_hall_5th_floor = '"+gross_banquet_hall_5th_floor+"'";}
}

if(!gross_banquet_hall_4th_floor.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_banquet_hall_4th_floor = '"+gross_banquet_hall_4th_floor+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_banquet_hall_4th_floor = '"+gross_banquet_hall_4th_floor+"'";}
}

if(!gross_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gross_date = '"+gross_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gross_date = '"+gross_date+"'";}
}
String query="update advanceamount "+updatecc+" where advanceamount_id='"+advanceamount_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from advanceamount where advanceamount_id='"+advanceamount_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}
}