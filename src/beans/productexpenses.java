package beans;
public class productexpenses 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String exp_id="";
private String vendorId="";
private String narration="";
private String entry_date="";
private String major_head_id="";
private String minorhead_id="";
private String sub_head_id="";
private String amount="";
private String expinv_id="";
private String emp_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String head_account_id="";
private String enter_by="";
private String enter_date="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";

private String banktrn_id="";
private String extra11 = "";
private String extra12 = "";
private String extra13 = "";
private String extra14 = "";
private String extra15 = "";
private String extra16 = "";
private String extra17 = "";
private String extra18 = "";
private String extra19 = "";
private String extra20 = "";
private String extra21 = "";
private String extra22 = "";
private String extra23 = "";
private String extra24 = "";
private String extra25 = "";

public String getBanktrn_id() {
	return banktrn_id;
}
public void setBanktrn_id(String banktrn_id) {
	this.banktrn_id = banktrn_id;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}
public String getExtra14() {
	return extra14;
}
public void setExtra14(String extra14) {
	this.extra14 = extra14;
}
public String getExtra15() {
	return extra15;
}
public void setExtra15(String extra15) {
	this.extra15 = extra15;
}
public String getExtra16() {
	return extra16;
}
public void setExtra16(String extra16) {
	this.extra16 = extra16;
}
public String getExtra17() {
	return extra17;
}
public void setExtra17(String extra17) {
	this.extra17 = extra17;
}
public String getExtra18() {
	return extra18;
}
public void setExtra18(String extra18) {
	this.extra18 = extra18;
}
public String getExtra19() {
	return extra19;
}
public void setExtra19(String extra19) {
	this.extra19 = extra19;
}
public String getExtra20() {
	return extra20;
}
public void setExtra20(String extra20) {
	this.extra20 = extra20;
}
public String getExtra21() {
	return extra21;
}
public void setExtra21(String extra21) {
	this.extra21 = extra21;
}
public String getExtra22() {
	return extra22;
}
public void setExtra22(String extra22) {
	this.extra22 = extra22;
}
public String getExtra23() {
	return extra23;
}
public void setExtra23(String extra23) {
	this.extra23 = extra23;
}
public String getExtra24() {
	return extra24;
}
public void setExtra24(String extra24) {
	this.extra24 = extra24;
}
public String getExtra25() {
	return extra25;
}
public void setExtra25(String extra25) {
	this.extra25 = extra25;
}

public productexpenses() {} 
public productexpenses(String exp_id,String vendorId,String narration,String entry_date,String major_head_id,String minorhead_id,String sub_head_id,String amount,String expinv_id,String emp_id,String extra1,String extra2,String extra3,String extra4,String extra5,String head_account_id)
 {
this.exp_id=exp_id;
this.vendorId=vendorId;
this.narration=narration;
this.entry_date=entry_date;
this.major_head_id=major_head_id;
this.minorhead_id=minorhead_id;
this.sub_head_id=sub_head_id;
this.amount=amount;
this.expinv_id=expinv_id;
this.emp_id=emp_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.head_account_id=head_account_id;

}
public productexpenses(String exp_id,String vendorId,String narration,String entry_date,String major_head_id,String minorhead_id,String sub_head_id,String amount,String expinv_id,String emp_id,String extra1,String extra2,String extra3,String extra4,String extra5,String head_account_id,String enter_by,String enter_date,String extra6,String extra7,String extra8,String extra9,String extra10)
{
this.exp_id=exp_id;
this.vendorId=vendorId;
this.narration=narration;
this.entry_date=entry_date;
this.major_head_id=major_head_id;
this.minorhead_id=minorhead_id;
this.sub_head_id=sub_head_id;
this.amount=amount;
this.expinv_id=expinv_id;
this.emp_id=emp_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.head_account_id=head_account_id;
this.enter_by=enter_by;
this.enter_date=enter_date;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;
this.extra10=extra10;
}

public productexpenses(String exp_id,String vendorId,String narration,String entry_date,String major_head_id,String minorhead_id,String sub_head_id,String amount,String expinv_id,String emp_id,String extra1,String extra2,String extra3,String extra4,String extra5,String head_account_id,String enter_by,String enter_date,String extra6,String extra7,String extra8,String extra9,String extra10,String extra11)
{
this.exp_id=exp_id;
this.vendorId=vendorId;
this.narration=narration;
this.entry_date=entry_date;
this.major_head_id=major_head_id;
this.minorhead_id=minorhead_id;
this.sub_head_id=sub_head_id;
this.amount=amount;
this.expinv_id=expinv_id;
this.emp_id=emp_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.head_account_id=head_account_id;
this.enter_by=enter_by;
this.enter_date=enter_date;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;
this.extra10=extra10;
this.extra11=extra11;
}

public productexpenses(String exp_id,String vendorId,String narration,String entry_date,String major_head_id,String minorhead_id,String sub_head_id,String amount,String expinv_id,String emp_id,String extra1,String extra2,String extra3,String extra4,String extra5,String head_account_id,String enter_by)
{
this.exp_id=exp_id;
this.vendorId=vendorId;
this.narration=narration;
this.entry_date=entry_date;
this.major_head_id=major_head_id;
this.minorhead_id=minorhead_id;
this.sub_head_id=sub_head_id;
this.amount=amount;
this.expinv_id=expinv_id;
this.emp_id=emp_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.head_account_id=head_account_id;
this.enter_by=enter_by;

}
public String getexp_id() {
return exp_id;
}
public void setexp_id(String exp_id) {
this.exp_id = exp_id;
}
public String getvendorId() {
return vendorId;
}
public void setvendorId(String vendorId) {
this.vendorId = vendorId;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getentry_date() {
return entry_date;
}
public void setentry_date(String entry_date) {
this.entry_date = entry_date;
}
public String getmajor_head_id() {
return major_head_id;
}
public void setmajor_head_id(String major_head_id) {
this.major_head_id = major_head_id;
}
public String getminorhead_id() {
return minorhead_id;
}
public void setminorhead_id(String minorhead_id) {
this.minorhead_id = minorhead_id;
}
public String getsub_head_id() {
return sub_head_id;
}
public void setsub_head_id(String sub_head_id) {
this.sub_head_id = sub_head_id;
}
public String getamount() {
return amount;
}
public void setamount(String amount) {
this.amount = amount;
}
public String getexpinv_id() {
return expinv_id;
}
public void setexpinv_id(String expinv_id) {
this.expinv_id = expinv_id;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getEnter_by() {
	return enter_by;
}
public void setEnter_by(String enter_by) {
	this.enter_by = enter_by;
}
public String getEnter_date() {
	return enter_date;
}
public void setEnter_date(String enter_date) {
	this.enter_date = enter_date;
}
public String getExtra6() {
	return extra6;
}
public void setExtra6(String extra6) {
	this.extra6 = extra6;
}
public String getExtra7() {
	return extra7;
}
public void setExtra7(String extra7) {
	this.extra7 = extra7;
}
public String getExtra8() {
	return extra8;
}
public void setExtra8(String extra8) {
	this.extra8 = extra8;
}
public String getExtra9() {
	return extra9;
}
public void setExtra9(String extra9) {
	this.extra9 = extra9;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
public productexpenses(String exp_id, String vendorId, String narration,
		String entry_date, String major_head_id, String minorhead_id,
		String sub_head_id, String amount, String expinv_id, String emp_id,
		String extra1, String extra2, String extra3, String extra4,
		String extra5, String head_account_id, String enter_by,
		String enter_date, String extra6, String extra7, String extra8,
		String extra9, String extra10, String banktrn_id, String extra11,
		String extra12, String extra13, String extra14, String extra15,
		String extra16, String extra17, String extra18, String extra19,
		String extra20, String extra21, String extra22, String extra23, 
		String extra24, String extra25) {
	super();
	this.exp_id = exp_id;
	this.vendorId = vendorId;
	this.narration = narration;
	this.entry_date = entry_date;
	this.major_head_id = major_head_id;
	this.minorhead_id = minorhead_id;
	this.sub_head_id = sub_head_id;
	this.amount = amount;
	this.expinv_id = expinv_id;
	this.emp_id = emp_id;
	this.extra1 = extra1;
	this.extra2 = extra2;
	this.extra3 = extra3;
	this.extra4 = extra4;
	this.extra5 = extra5;
	this.head_account_id = head_account_id;
	this.enter_by = enter_by;
	this.enter_date = enter_date;
	this.extra6 = extra6;
	this.extra7 = extra7;
	this.extra8 = extra8;
	this.extra9 = extra9;
	this.extra10 = extra10;
	this.banktrn_id = banktrn_id;
	this.extra11 = extra11;
	this.extra12 = extra12;
	this.extra13 = extra13;
	this.extra14 = extra14;
	this.extra15 = extra15;
	this.extra16 = extra16;
	this.extra17 = extra17;
	this.extra18 = extra18;
	this.extra19 = extra19;
	this.extra20 = extra20;
	this.extra21 = extra21;
	this.extra22 = extra22;
	this.extra23 = extra23;
	this.extra24 = extra24;
	this.extra25 = extra25;
}
@Override
public String toString() {
	return "productexpenses [error=" + error + ", exp_id=" + exp_id
			+ ", vendorId=" + vendorId + ", narration=" + narration
			+ ", entry_date=" + entry_date + ", major_head_id=" + major_head_id
			+ ", minorhead_id=" + minorhead_id + ", sub_head_id=" + sub_head_id
			+ ", amount=" + amount + ", expinv_id=" + expinv_id + ", emp_id="
			+ emp_id + ", extra1=" + extra1 + ", extra2=" + extra2
			+ ", extra3=" + extra3 + ", extra4=" + extra4 + ", extra5="
			+ extra5 + ", head_account_id=" + head_account_id + ", enter_by="
			+ enter_by + ", enter_date=" + enter_date + ", extra6=" + extra6
			+ ", extra7=" + extra7 + ", extra8=" + extra8 + ", extra9="
			+ extra9 + ", extra10=" + extra10 + ", banktrn_id=" + banktrn_id
			+ ", extra11=" + extra11 + ", extra12=" + extra12 + ", extra13="
			+ extra13 + ", extra14=" + extra14 + ", extra15=" + extra15
			+ ", extra16=" + extra16 + ", extra17=" + extra17 + ", extra18="
			+ extra18 + ", extra19=" + extra19 + ", extra20=" + extra20
			+ ", extra21=" + extra21 + ", extra22=" + extra22 + ", extra23="
			+ extra23 + ", extra24=" + extra24 + ", extra25=" + extra25 + "]";
}

}