package beans;
public class cashier_report 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String cashier_report_id="";
private String shift_code="";
private String counter_code="";
private String login_id="";
private String opening_balance="";
private String counter_balance="";
private String closing_balance="";
private String total_amount="";
private String logoin_time="";
private String logout_time="";
private String cash1="";
private String cash2="";
private String cash3="";
private String cash4="";
private String cash5="";
private String cash6="";
private String cash7="";
private String cash8="";
private String cash9="";
private String cash10="";
private String due_banktrnsid="";
private String edit_by="";
private String update_time="";
private String extra11="";
private String extra12="";
public String getDue_banktrnsid() {
	return due_banktrnsid;
}
public void setDue_banktrnsid(String due_banktrnsid) {
	this.due_banktrnsid = due_banktrnsid;
}
public String getEdit_by() {
	return edit_by;
}
public void setEdit_by(String edit_by) {
	this.edit_by = edit_by;
}
public String getUpdate_time() {
	return update_time;
}
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
public cashier_report(String cashier_report_id, String shift_code,
		String counter_code, String login_id, String opening_balance,
		String counter_balance, String closing_balance, String total_amount,
		String logoin_time, String logout_time, String cash1, String cash2,
		String cash3, String cash4, String cash5, String cash6, String cash7,
		String cash8, String cash9, String cash10, String due_banktrnsid,
		String edit_by, String update_time, String extra11, String extra12) {
	this.cashier_report_id = cashier_report_id;
	this.shift_code = shift_code;
	this.counter_code = counter_code;
	this.login_id = login_id;
	this.opening_balance = opening_balance;
	this.counter_balance = counter_balance;
	this.closing_balance = closing_balance;
	this.total_amount = total_amount;
	this.logoin_time = logoin_time;
	this.logout_time = logout_time;
	this.cash1 = cash1;
	this.cash2 = cash2;
	this.cash3 = cash3;
	this.cash4 = cash4;
	this.cash5 = cash5;
	this.cash6 = cash6;
	this.cash7 = cash7;
	this.cash8 = cash8;
	this.cash9 = cash9;
	this.cash10 = cash10;
	this.due_banktrnsid = due_banktrnsid;
	this.edit_by = edit_by;
	this.update_time = update_time;
	this.extra11 = extra11;
	this.extra12 = extra12;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public cashier_report() {} 
public cashier_report(String cashier_report_id,String shift_code,String counter_code,String login_id,String opening_balance,String counter_balance,String closing_balance,String total_amount,String logoin_time,String logout_time,String cash1,String cash2,String cash3,String cash4,String cash5,String cash6,String cash7,String cash8,String cash9,String cash10)
 {
this.cashier_report_id=cashier_report_id;
this.shift_code=shift_code;
this.counter_code=counter_code;
this.login_id=login_id;
this.opening_balance=opening_balance;
this.counter_balance=counter_balance;
this.closing_balance=closing_balance;
this.total_amount=total_amount;
this.logoin_time=logoin_time;
this.logout_time=logout_time;
this.cash1=cash1;
this.cash2=cash2;
this.cash3=cash3;
this.cash4=cash4;
this.cash5=cash5;
this.cash6=cash6;
this.cash7=cash7;
this.cash8=cash8;
this.cash9=cash9;
this.cash10=cash10;

} 
public String getcashier_report_id() {
return cashier_report_id;
}
public void setcashier_report_id(String cashier_report_id) {
this.cashier_report_id = cashier_report_id;
}
public String getshift_code() {
return shift_code;
}
public void setshift_code(String shift_code) {
this.shift_code = shift_code;
}
public String getcounter_code() {
return counter_code;
}
public void setcounter_code(String counter_code) {
this.counter_code = counter_code;
}
public String getlogin_id() {
return login_id;
}
public void setlogin_id(String login_id) {
this.login_id = login_id;
}
public String getopening_balance() {
return opening_balance;
}
public void setopening_balance(String opening_balance) {
this.opening_balance = opening_balance;
}
public String getcounter_balance() {
return counter_balance;
}
public void setcounter_balance(String counter_balance) {
this.counter_balance = counter_balance;
}
public String getclosing_balance() {
return closing_balance;
}
public void setclosing_balance(String closing_balance) {
this.closing_balance = closing_balance;
}
public String gettotal_amount() {
return total_amount;
}
public void settotal_amount(String total_amount) {
this.total_amount = total_amount;
}
public String getlogoin_time() {
return logoin_time;
}
public void setlogoin_time(String logoin_time) {
this.logoin_time = logoin_time;
}
public String getlogout_time() {
return logout_time;
}
public void setlogout_time(String logout_time) {
this.logout_time = logout_time;
}
public String getcash1() {
return cash1;
}
public void setcash1(String cash1) {
this.cash1 = cash1;
}
public String getcash2() {
return cash2;
}
public void setcash2(String cash2) {
this.cash2 = cash2;
}
public String getcash3() {
return cash3;
}
public void setcash3(String cash3) {
this.cash3 = cash3;
}
public String getcash4() {
return cash4;
}
public void setcash4(String cash4) {
this.cash4 = cash4;
}
public String getcash5() {
return cash5;
}
public void setcash5(String cash5) {
this.cash5 = cash5;
}
public String getcash6() {
return cash6;
}
public void setcash6(String cash6) {
this.cash6 = cash6;
}
public String getcash7() {
return cash7;
}
public void setcash7(String cash7) {
this.cash7 = cash7;
}
public String getcash8() {
return cash8;
}
public void setcash8(String cash8) {
this.cash8 = cash8;
}
public String getcash9() {
return cash9;
}
public void setcash9(String cash9) {
this.cash9 = cash9;
}
public String getcash10() {
return cash10;
}
public void setcash10(String cash10) {
this.cash10 = cash10;
}

}