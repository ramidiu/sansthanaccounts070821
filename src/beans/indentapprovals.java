package beans;
public class indentapprovals 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String approve_id="";
private String admin_id="";
private String description="";
private String admin_status="";
private String approved_date="";
private String indentinvoice_id="";
private String extra1="";
private String extra2="";
private String extra3="";
public indentapprovals() {} 
public indentapprovals(String approve_id,String admin_id,String description,String admin_status,String approved_date,String indentinvoice_id,String extra1,String extra2,String extra3)
 {
this.approve_id=approve_id;
this.admin_id=admin_id;
this.description=description;
this.admin_status=admin_status;
this.approved_date=approved_date;
this.indentinvoice_id=indentinvoice_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;

} 
public String getapprove_id() {
return approve_id;
}
public void setapprove_id(String approve_id) {
this.approve_id = approve_id;
}
public String getadmin_id() {
return admin_id;
}
public void setadmin_id(String admin_id) {
this.admin_id = admin_id;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String getadmin_status() {
return admin_status;
}
public void setadmin_status(String admin_status) {
this.admin_status = admin_status;
}
public String getapproved_date() {
return approved_date;
}
public void setapproved_date(String approved_date) {
this.approved_date = approved_date;
}
public String getindentinvoice_id() {
return indentinvoice_id;
}
public void setindentinvoice_id(String indentinvoice_id) {
this.indentinvoice_id = indentinvoice_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}

}