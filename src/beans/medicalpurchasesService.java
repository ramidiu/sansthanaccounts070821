package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class medicalpurchasesService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String purchaseId="";
private String tabletId="";
private String purchaseDate="";
private String vendorId="";
private String billNumber="";
private String date="";
private String amount="";
private String narration="";
private String quantity="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String purchaseby="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public medicalpurchasesService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setpurchaseId(String purchaseId)
{
this.purchaseId = purchaseId;
}

public String getpurchaseId()
{
return (this.purchaseId);
}


public void settabletId(String tabletId)
{
this.tabletId = tabletId;
}

public String gettabletId()
{
return (this.tabletId);
}


public void setpurchaseDate(String purchaseDate)
{
this.purchaseDate = purchaseDate;
}

public String getpurchaseDate()
{
return (this.purchaseDate);
}


public void setvendorId(String vendorId)
{
this.vendorId = vendorId;
}

public String getvendorId()
{
return (this.vendorId);
}


public void setbillNumber(String billNumber)
{
this.billNumber = billNumber;
}

public String getbillNumber()
{
return (this.billNumber);
}


public void setdate(String date)
{
this.date = date;
}

public String getdate()
{
return (this.date);
}


public void setamount(String amount)
{
this.amount = amount;
}

public String getamount()
{
return (this.amount);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setquantity(String quantity)
{
this.quantity = quantity;
}

public String getquantity()
{
return (this.quantity);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void setpurchaseby(String purchaseby)
{
this.purchaseby = purchaseby;
}

public String getpurchaseby()
{
return (this.purchaseby);
}

public boolean insert()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='medicalpurchases'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 purchaseId=PriSt+ticketm;
String query="insert into medicalpurchases values('"+purchaseId+"','"+tabletId+"',"+purchaseDate+",'"+vendorId+"','"+billNumber+"','"+date+"','"+amount+"','"+narration+"','"+quantity+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+purchaseby+"')";
/*System.out.println(query);*/
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='medicalpurchases'");

return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
public boolean updateinvoiceRep()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='invoice'");
rs.next();
int ticket=rs.getInt("table_id");
rs.close();
int ticketm=ticket+1;
 st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='invoice'");
 //System.out.println(ticketm);
 st.close();
 c.close();
 }catch(Exception e){
 this.seterror(e.toString());
 return false;
 }
return true;
 }
public boolean update()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!tabletId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"tabletId = '"+tabletId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",tabletId = '"+tabletId+"'";}
}

if(!purchaseDate.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"purchaseDate = '"+purchaseDate+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",purchaseDate = '"+purchaseDate+"'";}
}

if(!vendorId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendorId = '"+vendorId+"'";}
}

if(!billNumber.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"billNumber = '"+billNumber+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",billNumber = '"+billNumber+"'";}
}

if(!date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date = '"+date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date = '"+date+"'";}
}

if(!amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"amount = '"+amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",amount = '"+amount+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!quantity.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"quantity = '"+quantity+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",quantity = '"+quantity+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!purchaseby.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"purchaseby = '"+purchaseby+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",purchaseby = '"+purchaseby+"'";}
}
String query="update medicalpurchases "+updatecc+" where purchaseId='"+purchaseId+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from medicalpurchases where purchaseId='"+purchaseId+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}