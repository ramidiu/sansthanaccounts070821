package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class sssst_registrationsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String sssst_id="";
private String title="";
private String first_name="";
private String middle_name="";
private String last_name="";
private String spouce_name="";
private String gender="";
private String dob="";
private String address_1="";
private String address_2="";
private String country="";
private String state="";
private String city="";
private String pincode="";
private String mobile="";
private String idproof_1="";
private String idproof1_num="";
private String idproof_2="";
private String idproof2_num="";
private String image="";
private String pancard_no="";
private String email_id="";
private String updates_receive_via="";
private String gothram="";
private String remarks="";
private String refer_by="";
private String register_date="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String father_name="";
private String mother_name="";
private String mothertongue="";
private String bloodgroup="";
private String education_qualification="";
private String previous_experience="";
private String date_of_appointed="";
private String dependents="";
private String designation="";
private String identification_marks="";
private String height="";
private String weight="";
private String basic_pay="";
private String present_pay_scale="";
private String occupation="";
private String permanent_address="";
private String present_address="";
private String service_experinece_in_sansthan_trust="";
private String technical_qualification="";
private String caste_religion="";
private String physical_disorder="";
private String govt_reservation="";
private String enclosures="";
private String doa="";
private String reservation_cat="";
private String percent_of_disorder="";
private String ec_resolution_num="";
private String ec_resolution_date="";
private String tb_resolution_num="";
private String tb_resolution_date="";
private String employee_identification_num="";
private String extra5="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";
private String extra11="";
private String extra12="";
private String extra13="";
private String extra14="";
private String extra15="";


public String getEc_resolution_num() {
	return ec_resolution_num;
}
public void setEc_resolution_num(String ec_resolution_num) {
	this.ec_resolution_num = ec_resolution_num;
}
public String getEc_resolution_date() {
	return ec_resolution_date;
}
public void setEc_resolution_date(String ec_resolution_date) {
	this.ec_resolution_date = ec_resolution_date;
}
public String getTb_resolution_num() {
	return tb_resolution_num;
}
public void setTb_resolution_num(String tb_resolution_num) {
	this.tb_resolution_num = tb_resolution_num;
}
public String getTb_resolution_date() {
	return tb_resolution_date;
}
public void setTb_resolution_date(String tb_resolution_date) {
	this.tb_resolution_date = tb_resolution_date;
}
public String getEmployee_identification_num() {
	return employee_identification_num;
}
public void setEmployee_identification_num(String employee_identification_num) {
	this.employee_identification_num = employee_identification_num;
}
public String getReservation_cat() {
	return reservation_cat;
}
public void setReservation_cat(String reservation_cat) {
	this.reservation_cat = reservation_cat;
}
public String getPercent_of_disorder() {
	return percent_of_disorder;
}
public void setPercent_of_disorder(String percent_of_disorder) {
	this.percent_of_disorder = percent_of_disorder;
}
public String getPermanent_address() {
	return permanent_address;
}
public void setPermanent_address(String permanent_address) {
	this.permanent_address = permanent_address;
}
public String getPresent_address() {
	return present_address;
}
public void setPresent_address(String present_address) {
	this.present_address = present_address;
}
public String getService_experinece_in_sansthan_trust() {
	return service_experinece_in_sansthan_trust;
}
public void setService_experinece_in_sansthan_trust(
		String service_experinece_in_sansthan_trust) {
	this.service_experinece_in_sansthan_trust = service_experinece_in_sansthan_trust;
}
public String getTechnical_qualification() {
	return technical_qualification;
}
public void setTechnical_qualification(String technical_qualification) {
	this.technical_qualification = technical_qualification;
}
public String getCaste_religion() {
	return caste_religion;
}
public void setCaste_religion(String caste_religion) {
	this.caste_religion = caste_religion;
}
public String getPhysical_disorder() {
	return physical_disorder;
}
public void setPhysical_disorder(String physical_disorder) {
	this.physical_disorder = physical_disorder;
}
public String getGovt_reservation() {
	return govt_reservation;
}
public void setGovt_reservation(String govt_reservation) {
	this.govt_reservation = govt_reservation;
}
public String getEnclosures() {
	return enclosures;
}
public void setEnclosures(String enclosures) {
	this.enclosures = enclosures;
}
public String getDoa() {
	return doa;
}
public void setDoa(String doa) {
	this.doa = doa;
}
private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public sssst_registrationsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setsssst_id(String sssst_id)
{
this.sssst_id = sssst_id;
}

public String getsssst_id()
{
return (this.sssst_id);
}


public void settitle(String title)
{
this.title = title;
}

public String gettitle()
{
return (this.title);
}


public void setfirst_name(String first_name)
{
this.first_name = first_name;
}

public String getfirst_name()
{
return (this.first_name);
}


public void setmiddle_name(String middle_name)
{
this.middle_name = middle_name;
}

public String getmiddle_name()
{
return (this.middle_name);
}


public void setlast_name(String last_name)
{
this.last_name = last_name;
}

public String getlast_name()
{
return (this.last_name);
}


public void setspouce_name(String spouce_name)
{
this.spouce_name = spouce_name;
}

public String getspouce_name()
{
return (this.spouce_name);
}


public void setgender(String gender)
{
this.gender = gender;
}

public String getgender()
{
return (this.gender);
}


public void setdob(String dob)
{
this.dob = dob;
}

public String getdob()
{
return (this.dob);
}


public void setaddress_1(String address_1)
{
this.address_1 = address_1;
}

public String getaddress_1()
{
return (this.address_1);
}


public void setaddress_2(String address_2)
{
this.address_2 = address_2;
}

public String getaddress_2()
{
return (this.address_2);
}


public void setcountry(String country)
{
this.country = country;
}

public String getcountry()
{
return (this.country);
}


public void setstate(String state)
{
this.state = state;
}

public String getstate()
{
return (this.state);
}


public void setcity(String city)
{
this.city = city;
}

public String getcity()
{
return (this.city);
}


public void setpincode(String pincode)
{
this.pincode = pincode;
}

public String getpincode()
{
return (this.pincode);
}


public void setmobile(String mobile)
{
this.mobile = mobile;
}

public String getmobile()
{
return (this.mobile);
}


public void setidproof_1(String idproof_1)
{
this.idproof_1 = idproof_1;
}

public String getidproof_1()
{
return (this.idproof_1);
}


public void setidproof1_num(String idproof1_num)
{
this.idproof1_num = idproof1_num;
}

public String getidproof1_num()
{
return (this.idproof1_num);
}


public void setidproof_2(String idproof_2)
{
this.idproof_2 = idproof_2;
}

public String getidproof_2()
{
return (this.idproof_2);
}


public void setidproof2_num(String idproof2_num)
{
this.idproof2_num = idproof2_num;
}

public String getidproof2_num()
{
return (this.idproof2_num);
}


public void setimage(String image)
{
this.image = image;
}

public String getimage()
{
return (this.image);
}


public void setpancard_no(String pancard_no)
{
this.pancard_no = pancard_no;
}

public String getpancard_no()
{
return (this.pancard_no);
}


public void setemail_id(String email_id)
{
this.email_id = email_id;
}

public String getemail_id()
{
return (this.email_id);
}


public void setupdates_receive_via(String updates_receive_via)
{
this.updates_receive_via = updates_receive_via;
}

public String getupdates_receive_via()
{
return (this.updates_receive_via);
}


public void setgothram(String gothram)
{
this.gothram = gothram;
}

public String getgothram()
{
return (this.gothram);
}


public void setremarks(String remarks)
{
this.remarks = remarks;
}

public String getremarks()
{
return (this.remarks);
}


public void setrefer_by(String refer_by)
{
this.refer_by = refer_by;
}

public String getrefer_by()
{
return (this.refer_by);
}


public void setregister_date(String register_date)
{
this.register_date = register_date;
}

public String getregister_date()
{
return (this.register_date);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}
public String getFather_name() {
	return father_name;
}
public void setFather_name(String father_name) {
	this.father_name = father_name;
}
public String getMother_name() {
	return mother_name;
}
public void setMother_name(String mother_name) {
	this.mother_name = mother_name;
}
public String getMothertongue() {
	return mothertongue;
}
public void setMothertongue(String mothertongue) {
	this.mothertongue = mothertongue;
}
public String getBloodgroup() {
	return bloodgroup;
}
public void setBloodgroup(String bloodgroup) {
	this.bloodgroup = bloodgroup;
}
public String getEducation_qualification() {
	return education_qualification;
}
public void setEducation_qualification(String education_qualification) {
	this.education_qualification = education_qualification;
}
public String getPrevious_experience() {
	return previous_experience;
}
public void setPrevious_experience(String previous_experience) {
	this.previous_experience = previous_experience;
}
public String getDate_of_appointed() {
	return date_of_appointed;
}
public void setDate_of_appointed(String date_of_appointed) {
	this.date_of_appointed = date_of_appointed;
}
public String getDependents() {
	return dependents;
}
public void setDependents(String dependents) {
	this.dependents = dependents;
}
public String getDesignation() {
	return designation;
}
public void setDesignation(String designation) {
	this.designation = designation;
}
public String getIdentification_marks() {
	return identification_marks;
}
public void setIdentification_marks(String identification_marks) {
	this.identification_marks = identification_marks;
}
public String getHeight() {
	return height;
}
public void setHeight(String height) {
	this.height = height;
}
public String getWeight() {
	return weight;
}
public void setWeight(String weight) {
	this.weight = weight;
}
public String getBasic_pay() {
	return basic_pay;
}
public void setBasic_pay(String basic_pay) {
	this.basic_pay = basic_pay;
}
public String getPresent_pay_scale() {
	return present_pay_scale;
}
public void setPresent_pay_scale(String present_pay_scale) {
	this.present_pay_scale = present_pay_scale;
}
public String getOccupation() {
	return occupation;
}
public void setOccupation(String occupation) {
	this.occupation = occupation;
}
public String getExtra5() {
	return extra5;
}
public void setExtra5(String extra5) {
	this.extra5 = extra5;
}
public String getExtra6() {
	return extra6;
}
public void setExtra6(String extra6) {
	this.extra6 = extra6;
}
public String getExtra7() {
	return extra7;
}
public void setExtra7(String extra7) {
	this.extra7 = extra7;
}
public String getExtra8() {
	return extra8;
}
public void setExtra8(String extra8) {
	this.extra8 = extra8;
}
public String getExtra9() {
	return extra9;
}
public void setExtra9(String extra9) {
	this.extra9 = extra9;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}
public String getExtra14() {
	return extra14;
}
public void setExtra14(String extra14) {
	this.extra14 = extra14;
}
public String getExtra15() {
	return extra15;
}
public void setExtra15(String extra15) {
	this.extra15 = extra15;
}

public boolean insert()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='sssst_registrations'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 sssst_id=PriSt+ticketm;
String query="insert into sssst_registrations values('"+sssst_id+"','"+title+"','"+first_name+"','"+middle_name+"','"+last_name+"','"+spouce_name+"','"+gender+"','"+dob+"','"+address_1+"','"+address_2+"','"+country+"','"+state+"','"+city+"','"+pincode+"','"+mobile+"','"+idproof_1+"','"+idproof1_num+"','"+idproof_2+"','"+idproof2_num+"','"+image+"','"+pancard_no+"','"+email_id+"','"+updates_receive_via+"','"+gothram+"','"+remarks+"','"+refer_by+"','"+register_date+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+father_name+"','"+mother_name+"','"+mothertongue+"','"+bloodgroup+"','"+education_qualification+"','"+previous_experience+"','"+date_of_appointed+"','"+dependents+"','"+designation+"','"+identification_marks+"','"+height+"','"+weight+"','"+basic_pay+"','"+present_pay_scale+"','"+occupation+"','"+permanent_address+"','"+present_address+"','"+service_experinece_in_sansthan_trust+"','"+technical_qualification+"','"+caste_religion+"','"+physical_disorder+"','"+govt_reservation+"','"+enclosures+"','"+doa+"','"+reservation_cat+"','"+percent_of_disorder+"','"+ec_resolution_num+"','"+ec_resolution_date+"','"+tb_resolution_num+"','"+tb_resolution_date+"','"+employee_identification_num+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"')";
st.executeUpdate(query);
System.out.println(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='sssst_registrations'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}

public String insertWithId()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='sssst_registrations'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 sssst_id=PriSt+ticketm;
String query="insert into sssst_registrations values('"+sssst_id+"','"+title+"','"+first_name+"','"+middle_name+"','"+last_name+"','"+spouce_name+"','"+gender+"','"+dob+"','"+address_1+"','"+address_2+"','"+country+"','"+state+"','"+city+"','"+pincode+"','"+mobile+"','"+idproof_1+"','"+idproof1_num+"','"+idproof_2+"','"+idproof2_num+"','"+image+"','"+pancard_no+"','"+email_id+"','"+updates_receive_via+"','"+gothram+"','"+remarks+"','"+refer_by+"','"+register_date+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+father_name+"','"+mother_name+"','"+mothertongue+"','"+bloodgroup+"','"+education_qualification+"','"+previous_experience+"','"+date_of_appointed+"','"+dependents+"','"+designation+"','"+identification_marks+"','"+height+"','"+weight+"','"+basic_pay+"','"+present_pay_scale+"','"+occupation+"','"+permanent_address+"','"+present_address+"','"+service_experinece_in_sansthan_trust+"','"+technical_qualification+"','"+caste_religion+"','"+physical_disorder+"','"+govt_reservation+"','"+enclosures+"','"+doa+"','"+reservation_cat+"','"+percent_of_disorder+"','"+ec_resolution_num+"','"+ec_resolution_date+"','"+tb_resolution_num+"','"+tb_resolution_date+"','"+employee_identification_num+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"')";
st.executeUpdate(query);
System.out.println(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='sssst_registrations'");
st.close();
c.close();

}catch(Exception e){
this.seterror(e.toString());
}
return sssst_id;
}

public boolean update()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!title.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"title = '"+title+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",title = '"+title+"'";}
}
if(!first_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"first_name = '"+first_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",first_name = '"+first_name+"'";}
}
if(!middle_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"middle_name = '"+middle_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",middle_name = '"+middle_name+"'";}
}
if(!last_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"last_name = '"+last_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",last_name = '"+last_name+"'";}
}
if(!spouce_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"spouce_name = '"+spouce_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",spouce_name = '"+spouce_name+"'";}
}
if(!gender.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gender = '"+gender+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gender = '"+gender+"'";}
}
if(!dob.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"dob = '"+dob+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",dob = '"+dob+"'";}
}
if(!address_1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"address_1 = '"+address_1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",address_1 = '"+address_1+"'";}
}
if(!address_2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"address_2 = '"+address_2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",address_2 = '"+address_2+"'";}
}
if(!country.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"country = '"+country+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",country = '"+country+"'";}
}
if(!state.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"state = '"+state+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",state = '"+state+"'";}
}
if(!city.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"city = '"+city+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",city = '"+city+"'";}
}
if(!pincode.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"pincode = '"+pincode+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",pincode = '"+pincode+"'";}
}
if(!mobile.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"mobile = '"+mobile+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",mobile = '"+mobile+"'";}
}
if(!idproof_1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"idproof_1 = '"+idproof_1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",idproof_1 = '"+idproof_1+"'";}
}

if(!idproof1_num.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"idproof1_num = '"+idproof1_num+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",idproof1_num = '"+idproof1_num+"'";}
}

if(!idproof_2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"idproof_2 = '"+idproof_2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",idproof_2 = '"+idproof_2+"'";}
}
if(!idproof2_num.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"idproof2_num = '"+idproof2_num+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",idproof2_num = '"+idproof2_num+"'";}
}

if(!image.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"image = '"+image+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",image = '"+image+"'";}
}
/*if(!pancard_no.equals("")){ */
if(jk==0)
{
updatecc=updatecc+"pancard_no = '"+pancard_no+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",pancard_no = '"+pancard_no+"'";}
/*}*/

if(!email_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"email_id = '"+email_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",email_id = '"+email_id+"'";}
}

if(!updates_receive_via.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"updates_receive_via = '"+updates_receive_via+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",updates_receive_via = '"+updates_receive_via+"'";}
}

if(!gothram.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gothram = '"+gothram+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gothram = '"+gothram+"'";}
}
if(!remarks.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"remarks = '"+remarks+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",remarks = '"+remarks+"'";}
}

if(!refer_by.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"refer_by = '"+refer_by+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",refer_by = '"+refer_by+"'";}
}

if(!register_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"register_date = '"+register_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",register_date = '"+register_date+"'";}
}
if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}
if(!father_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"father_name = '"+father_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",father_name = '"+father_name+"'";}
}

if(!mother_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"mother_name = '"+mother_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",mother_name = '"+mother_name+"'";}
}

if(!mothertongue.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"mothertongue = '"+mothertongue+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",mothertongue = '"+mothertongue+"'";}
}

if(!bloodgroup.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"bloodgroup = '"+bloodgroup+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",bloodgroup = '"+bloodgroup+"'";}
}

if(!education_qualification.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"education_qualification = '"+education_qualification+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",education_qualification = '"+education_qualification+"'";}
}

if(!previous_experience.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"previous_experience = '"+previous_experience+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",previous_experience = '"+previous_experience+"'";}
}

if(!date_of_appointed.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date_of_appointed = '"+date_of_appointed+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date_of_appointed = '"+date_of_appointed+"'";}
}

if(!dependents.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"dependents = '"+dependents+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",dependents = '"+dependents+"'";}
}

if(!designation.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"designation = '"+designation+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",designation = '"+designation+"'";}
}

if(!identification_marks.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"identification_marks = '"+identification_marks+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",identification_marks = '"+identification_marks+"'";}
}

if(!height.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"height = '"+height+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",height = '"+height+"'";}
}

if(!weight.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"weight = '"+weight+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",weight = '"+weight+"'";}
}

if(!basic_pay.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"basic_pay = '"+basic_pay+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",basic_pay = '"+basic_pay+"'";}
}

if(!present_pay_scale.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"present_pay_scale = '"+present_pay_scale+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",present_pay_scale = '"+present_pay_scale+"'";}
}

if(!occupation.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"occupation = '"+occupation+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",occupation = '"+occupation+"'";}
}

if(!permanent_address.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"permanent_address = '"+permanent_address+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",permanent_address = '"+permanent_address+"'";}
}

if(!present_address.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"present_address = '"+present_address+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",present_address = '"+present_address+"'";}
}

if(!service_experinece_in_sansthan_trust.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"service_experinece_in_sansthan_trust = '"+service_experinece_in_sansthan_trust+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",service_experinece_in_sansthan_trust = '"+service_experinece_in_sansthan_trust+"'";}
}

if(!technical_qualification.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"technical_qualification = '"+technical_qualification+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",technical_qualification = '"+technical_qualification+"'";}
}

if(!caste_religion.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"caste_religion = '"+caste_religion+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",caste_religion = '"+caste_religion+"'";}
}

if(!physical_disorder.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"physical_disorder = '"+physical_disorder+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",physical_disorder = '"+physical_disorder+"'";}
}

if(!govt_reservation.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"govt_reservation = '"+govt_reservation+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",govt_reservation = '"+govt_reservation+"'";}
}

if(!enclosures.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"enclosures = '"+enclosures+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",enclosures = '"+enclosures+"'";}
}

if(!doa.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"doa = '"+doa+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",doa = '"+doa+"'";}
}

if(!reservation_cat.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"reservation_cat = '"+reservation_cat+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",reservation_cat = '"+reservation_cat+"'";}
}
if(!percent_of_disorder.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"percent_of_disorder = '"+percent_of_disorder+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",percent_of_disorder = '"+percent_of_disorder+"'";}
}
if(!ec_resolution_num.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"ec_resolution_num = '"+ec_resolution_num+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",ec_resolution_num = '"+ec_resolution_num+"'";}
}
if(!ec_resolution_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"ec_resolution_date = '"+ec_resolution_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",ec_resolution_date = '"+ec_resolution_date+"'";}
}
if(!tb_resolution_num.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"tb_resolution_num = '"+tb_resolution_num+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",tb_resolution_num = '"+tb_resolution_num+"'";}
}
if(!tb_resolution_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"tb_resolution_date = '"+tb_resolution_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",tb_resolution_date = '"+tb_resolution_date+"'";}
}
if(!employee_identification_num.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"employee_identification_num = '"+employee_identification_num+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",employee_identification_num = '"+employee_identification_num+"'";}
}
if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
if(!extra6.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra6 = '"+extra6+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra6 = '"+extra6+"'";}
}
if(!extra7.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra7 = '"+extra7+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra7 = '"+extra7+"'";}
}
if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}
if(!extra9.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra9 = '"+extra9+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra9 = '"+extra9+"'";}
}
if(!extra10.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra10 = '"+extra10+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra10 = '"+extra10+"'";}
}
if(!extra11.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra11 = '"+extra11+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra11 = '"+extra11+"'";}
}
if(!extra12.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra12 = '"+extra12+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra12 = '"+extra12+"'";}
}
if(!extra13.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra13 = '"+extra13+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra13 = '"+extra13+"'";}
}
if(!extra14.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra14 = '"+extra14+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra14 = '"+extra14+"'";}
}
if(!extra15.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra15 = '"+extra15+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra15 = '"+extra15+"'";}
}

String query="update sssst_registrations "+updatecc+" where sssst_id='"+sssst_id+"'";
System.out.println(query);
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from sssst_registrations where sssst_id='"+sssst_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}