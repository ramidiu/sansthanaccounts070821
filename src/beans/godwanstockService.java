package beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mainClasses.no_genaratorListing;
import dbase.sqlcon.ConnectionHelper;

public class godwanstockService {
private String error="";
public void seterror(String error) {
	this.error=error;
}
public String geterror() {
	return this.error;
}
private String gsId="";
private String vendorId="";
private String productId="";
private String description="";
private String quantity="";
private String purchaseRate="";
private String date="";
private String MRNo="";
private String billNo="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String vat="";
private String vat1="";
private String profcharges="";
private String emp_id="";
private String department="";
private String checkedby="";
private String checkedtime="";
private String approval_status="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";
private String po_approved_status="";
private String po_approved_by="";
private String po_approved_date="";
private String bill_status="";
private String bill_update_by="";
private String bill_update_time="";
private String extra11="";
private String extra12="";
private String extra13="";
private String extra14="";
private String extra15="";
private String majorhead_id="";
private String minorhead_id="";
private String payment_status="";
private String actualentry_date="";
private String extra16="";
private String extra17="";
private String extra18="";
private String extra19="";
private String extra20="";
private String expensesEntryDate = "";
public String getExpensesEntryDate() {
	return expensesEntryDate;
}
public void setExpensesEntryDate(String expensesEntryDate) {
	this.expensesEntryDate = expensesEntryDate;
}
private String checKPrintDate = "";
private String banktrn_id = "";
private String brsDate = "";
public String getChecKPrintDate() {
	return checKPrintDate;
}
public void setChecKPrintDate(String checKPrintDate) {
	this.checKPrintDate = checKPrintDate;
}
public String getBanktrn_id() {
	return banktrn_id;
}
public void setBanktrn_id(String banktrn_id) {
	this.banktrn_id = banktrn_id;
}
public String getBrsDate() {
	return brsDate;
}
public void setBrsDate(String brsDate) {
	this.brsDate = brsDate;
}
private String extra21 = "";
private String extra22 = "";
private String extra23 = "";
private String extra24 = "";
private String extra25 = "";
private String extra26 = "";
private String extra27 = "";
private String extra28 = "";
private String extra29 = "";
private String extra30 = "";
private String extra31 = "";
private String extra32 = "";
private String extra33 = "";
private String extra34 = "";
private String extra35 = "";

public String getCheckedby() {
	return checkedby;
}
public void setCheckedby(String checkedby) {
	this.checkedby = checkedby;
}
public String getCheckedtime() {
	return checkedtime;
}
public void setCheckedtime(String checkedtime) {
	this.checkedtime = checkedtime;
}
public String getApproval_status() {
	return approval_status;
}
public void setApproval_status(String approval_status) {
	this.approval_status = approval_status;
}
public String getExtra6() {
	return extra6;
}
public void setExtra6(String extra6) {
	this.extra6 = extra6;
}
public String getExtra7() {
	return extra7;
}
public void setExtra7(String extra7) {
	this.extra7 = extra7;
}
public String getExtra8() {
	return extra8;
}
public void setExtra8(String extra8) {
	this.extra8 = extra8;
}
public String getExtra9() {
	return extra9;
}
public void setExtra9(String extra9) {
	this.extra9 = extra9;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public godwanstockService() {
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setgsId(String gsId) {
	this.gsId = gsId;
}

public String getgsId() {
	return (this.gsId);
}


public void setvendorId(String vendorId) {
	this.vendorId = vendorId;
}

public String getvendorId()
{
return (this.vendorId);
}


public void setproductId(String productId)
{
this.productId = productId;
}

public String getproductId()
{
return (this.productId);
}


public void setdescription(String description)
{
this.description = description;
}

public String getdescription()
{
return (this.description);
}


public void setquantity(String quantity)
{
this.quantity = quantity;
}

public String getquantity()
{
return (this.quantity);
}


public void setpurchaseRate(String purchaseRate)
{
this.purchaseRate = purchaseRate;
}

public String getpurchaseRate()
{
return (this.purchaseRate);
}


public void setdate(String date)
{
this.date = date;
}

public String getdate()
{
return (this.date);
}


public void setMRNo(String MRNo)
{
this.MRNo = MRNo;
}

public String getMRNo()
{
return (this.MRNo);
}


public void setbillNo(String billNo)
{
this.billNo = billNo;
}

public String getbillNo()
{
return (this.billNo);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void setvat(String vat)
{
this.vat = vat;
}

public String getvat()
{
return (this.vat);
}


public void setvat1(String vat1)
{
this.vat1 = vat1;
}

public String getvat1()
{
return (this.vat1);
}


public void setprofcharges(String profcharges)
{
this.profcharges = profcharges;
}

public String getprofcharges()
{
return (this.profcharges);
}


public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}

public void setdepartment(String department)
{
this.department = department;
}

public String getdepartment()
{
return (this.department);
}

public void setpo_approved_status(String po_approved_status)
{
this.po_approved_status = po_approved_status;
}

public String getpo_approved_status()
{
return (this.po_approved_status);
}


public void setpo_approved_by(String po_approved_by)
{
this.po_approved_by = po_approved_by;
}

public String getpo_approved_by()
{
return (this.po_approved_by);
}


public void setpo_approved_date(String po_approved_date)
{
this.po_approved_date = po_approved_date;
}

public String getpo_approved_date()
{
return (this.po_approved_date);
}


public void setbill_status(String bill_status)
{
this.bill_status = bill_status;
}

public String getbill_status()
{
return (this.bill_status);
}


public void setbill_update_by(String bill_update_by)
{
this.bill_update_by = bill_update_by;
}

public String getbill_update_by()
{
return (this.bill_update_by);
}


public void setbill_update_time(String bill_update_time)
{
this.bill_update_time = bill_update_time;
}

public String getbill_update_time()
{
return (this.bill_update_time);
}


public void setextra11(String extra11)
{
this.extra11 = extra11;
}

public String getextra11()
{
return (this.extra11);
}


public void setextra12(String extra12)
{
this.extra12 = extra12;
}

public String getextra12()
{
return (this.extra12);
}


public void setextra13(String extra13)
{
this.extra13 = extra13;
}

public String getextra13()
{
return (this.extra13);
}


public void setextra14(String extra14)
{
this.extra14 = extra14;
}

public String getextra14()
{
return (this.extra14);
}


public void setextra15(String extra15)
{
this.extra15 = extra15;
}

public String getextra15()
{
return (this.extra15);
}


public String getMajorhead_id() {
	return majorhead_id;
}
public void setMajorhead_id(String majorhead_id) {
	this.majorhead_id = majorhead_id;
}
public String getMinorhead_id() {
	return minorhead_id;
}
public void setMinorhead_id(String minorhead_id) {
	this.minorhead_id = minorhead_id;
}
public String getPayment_status() {
	return payment_status;
}
public void setPayment_status(String payment_status) {
	this.payment_status = payment_status;
}
public String getActualentry_date() {
	return actualentry_date;
}
public void setActualentry_date(String actualentry_date) {
	this.actualentry_date = actualentry_date;
}
public String getExtra16() {
	return extra16;
}
public void setExtra16(String extra16) {
	this.extra16 = extra16;
}
public String getExtra17() {
	return extra17;
}
public void setExtra17(String extra17) {
	this.extra17 = extra17;
}
public String getExtra18() {
	return extra18;
}
public void setExtra18(String extra18) {
	this.extra18 = extra18;
}
public String getExtra19() {
	return extra19;
}
public void setExtra19(String extra19) {
	this.extra19 = extra19;
}
public String getExtra20() {
	return extra20;
}
public void setExtra20(String extra20) {
	this.extra20 = extra20;
}


public String getError() {
	return error;
}
public void setError(String error) {
	this.error = error;
}
public String getGsId() {
	return gsId;
}
public void setGsId(String gsId) {
	this.gsId = gsId;
}
public String getVendorId() {
	return vendorId;
}
public void setVendorId(String vendorId) {
	this.vendorId = vendorId;
}
public String getProductId() {
	return productId;
}
public void setProductId(String productId) {
	this.productId = productId;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}
public String getPurchaseRate() {
	return purchaseRate;
}
public void setPurchaseRate(String purchaseRate) {
	this.purchaseRate = purchaseRate;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getBillNo() {
	return billNo;
}
public void setBillNo(String billNo) {
	this.billNo = billNo;
}
public String getExtra1() {
	return extra1;
}
public void setExtra1(String extra1) {
	this.extra1 = extra1;
}
public String getExtra2() {
	return extra2;
}
public void setExtra2(String extra2) {
	this.extra2 = extra2;
}
public String getExtra3() {
	return extra3;
}
public void setExtra3(String extra3) {
	this.extra3 = extra3;
}
public String getExtra4() {
	return extra4;
}
public void setExtra4(String extra4) {
	this.extra4 = extra4;
}
public String getExtra5() {
	return extra5;
}
public void setExtra5(String extra5) {
	this.extra5 = extra5;
}
public String getVat() {
	return vat;
}
public void setVat(String vat) {
	this.vat = vat;
}
public String getVat1() {
	return vat1;
}
public void setVat1(String vat1) {
	this.vat1 = vat1;
}
public String getProfcharges() {
	return profcharges;
}
public void setProfcharges(String profcharges) {
	this.profcharges = profcharges;
}
public String getEmp_id() {
	return emp_id;
}
public void setEmp_id(String emp_id) {
	this.emp_id = emp_id;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department = department;
}
public String getPo_approved_status() {
	return po_approved_status;
}
public void setPo_approved_status(String po_approved_status) {
	this.po_approved_status = po_approved_status;
}
public String getPo_approved_by() {
	return po_approved_by;
}
public void setPo_approved_by(String po_approved_by) {
	this.po_approved_by = po_approved_by;
}
public String getPo_approved_date() {
	return po_approved_date;
}
public void setPo_approved_date(String po_approved_date) {
	this.po_approved_date = po_approved_date;
}
public String getBill_status() {
	return bill_status;
}
public void setBill_status(String bill_status) {
	this.bill_status = bill_status;
}
public String getBill_update_by() {
	return bill_update_by;
}
public void setBill_update_by(String bill_update_by) {
	this.bill_update_by = bill_update_by;
}
public String getBill_update_time() {
	return bill_update_time;
}
public void setBill_update_time(String bill_update_time) {
	this.bill_update_time = bill_update_time;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}
public String getExtra14() {
	return extra14;
}
public void setExtra14(String extra14) {
	this.extra14 = extra14;
}
public String getExtra15() {
	return extra15;
}
public void setExtra15(String extra15) {
	this.extra15 = extra15;
}
public String getExtra21() {
	return extra21;
}
public void setExtra21(String extra21) {
	this.extra21 = extra21;
}
public String getExtra22() {
	return extra22;
}
public void setExtra22(String extra22) {
	this.extra22 = extra22;
}
public String getExtra23() {
	return extra23;
}
public void setExtra23(String extra23) {
	this.extra23 = extra23;
}
public String getExtra24() {
	return extra24;
}
public void setExtra24(String extra24) {
	this.extra24 = extra24;
}
public String getExtra25() {
	return extra25;
}
public void setExtra25(String extra25) {
	this.extra25 = extra25;
}
public String getExtra26() {
	return extra26;
}
public void setExtra26(String extra26) {
	this.extra26 = extra26;
}
public String getExtra27() {
	return extra27;
}
public void setExtra27(String extra27) {
	this.extra27 = extra27;
}
public String getExtra28() {
	return extra28;
}
public void setExtra28(String extra28) {
	this.extra28 = extra28;
}
public String getExtra29() {
	return extra29;
}
public void setExtra29(String extra29) {
	this.extra29 = extra29;
}
public String getExtra30() {
	return extra30;
}
public void setExtra30(String extra30) {
	this.extra30 = extra30;
}
public String getExtra31() {
	return extra31;
}
public void setExtra31(String extra31) {
	this.extra31 = extra31;
}
public String getExtra32() {
	return extra32;
}
public void setExtra32(String extra32) {
	this.extra32 = extra32;
}
public String getExtra33() {
	return extra33;
}
public void setExtra33(String extra33) {
	this.extra33 = extra33;
}
public String getExtra34() {
	return extra34;
}
public void setExtra34(String extra34) {
	this.extra34 = extra34;
}
public String getExtra35() {
	return extra35;
}
public void setExtra35(String extra35) {
	this.extra35 = extra35;
}
public ResultSet getRs() {
	return rs;
}
public void setRs(ResultSet rs) {
	this.rs = rs;
}
public Statement getSt() {
	return st;
}
public void setSt(Statement st) {
	this.st = st;
}
public Connection getC() {
	return c;
}
public void setC(Connection c) {
	this.c = c;
}
public boolean insert()
{	 int ticketm=0;
	boolean flag = false;
	no_genaratorListing nl= new no_genaratorListing();
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='godwanstock'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
 ticketm=ticket+1;
 gsId=PriSt+ticketm;
/*String gid=nl.getId("godwanstock");*/
System.out.println("BEFORE INSERT INT GODWAN STOCK");
String query="insert into godwanstock values('"+gsId+"','"+vendorId+"','"+productId+"','"+description+"','"+quantity+"','"+purchaseRate+"','"+date+"','"+MRNo+"','"+billNo+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+vat+"','"+vat1+"','"+profcharges+"','"+emp_id+"','"+department+"','"+checkedby+"','"+checkedtime+"','"+approval_status+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+po_approved_status+"','"+po_approved_by+"','"+po_approved_date+"','"+bill_status+"','"+bill_update_by+"','"+bill_update_time+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"','"+majorhead_id+"','"+minorhead_id+"','"+payment_status+"','"+actualentry_date+"','"+extra16+"','"+extra17+"','"+extra18+"','"+extra19+"','"+extra20+"','"+expensesEntryDate+"','"+checKPrintDate+"','"+banktrn_id+"','"+brsDate+"','"+extra21+"','"+extra22+"','"+extra23+"','"+extra24+"','"+extra25+"','"+extra26+"','"+extra27+"','"+extra28+"','"+extra29+"','"+extra30+"','"+extra31+"','"+extra32+"','"+extra33+"','"+extra34+"','"+extra35+"')";
			//		db column names(old)							gsId,      vendorId,       productId,      description,      quantity,    purchaseRate,      date,      MRNo,      billNo,         extra1,     extra2,       extra3,        extra4,       extra5,   vat,   vat1,         profcharges,       emp_id,   department,    checkedby,         checkedtime,       approval_status,    extra6,       extra7,       extra8,     extra9,      extra10,      po_approved_status,      po_approved_by,        po_approved_date,      bill_status,     bill_update_by,     bill_update_time,      extra11,      extra12,     extra13,         extra14,    extra15,     majorhead_id,       minorhead_id,       payment_status,      actualentry_date,      extra16,      extra17,    extra18,      extra19,      extra20,     extra21, extra22, extra23, extra24, extra25, extra26, extra27, extra28, extra29, extra30, extra31, extra32, extra33, extra34, extra35
System.out.println("query===> "+query);
String query1="insert into godwanstock_sc values('"+gsId+"','"+vendorId+"','"+productId+"','"+description+"','"+quantity+"','"+purchaseRate+"','"+date+"','"+MRNo+"','"+billNo+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+vat+"','"+vat1+"','"+profcharges+"','"+emp_id+"','"+department+"','"+checkedby+"','"+checkedtime+"','"+approval_status+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+po_approved_status+"','"+po_approved_by+"','"+po_approved_date+"','"+bill_status+"','"+bill_update_by+"','"+bill_update_time+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"','"+majorhead_id+"','"+minorhead_id+"','"+payment_status+"','"+actualentry_date+"','"+extra16+"','"+extra17+"','"+extra18+"','"+extra19+"','"+extra20+"')";
System.out.println(" after mse enter godwaninsert===>"+query);
int result=st.executeUpdate(query);
System.out.println("AFTER INSER IN GODWAN STOCK" );
System.out.println("result================>"+result);
st.executeUpdate(query1);
String updatequery="update no_genarator set table_id="+ticketm+" where table_name='godwanstock'";
System.out.println(updatequery);
st.executeUpdate(updatequery);
flag = true;
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{
	System.out.println("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
	boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!vendorId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendorId = '"+vendorId+"'";}
}

if(!productId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"productId = '"+productId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",productId = '"+productId+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!quantity.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"quantity = '"+quantity+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",quantity = '"+quantity+"'";}
}

if(!purchaseRate.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"purchaseRate = '"+purchaseRate+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",purchaseRate = '"+purchaseRate+"'";}
}

if(!date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date = '"+date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date = '"+date+"'";}
}

if(!MRNo.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"MRNo = '"+MRNo+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",MRNo = '"+MRNo+"'";}
}

if(!billNo.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"billNo = '"+billNo+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",billNo = '"+billNo+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!vat.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat = '"+vat+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat = '"+vat+"'";}
}

if(!vat1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat1 = '"+vat1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat1 = '"+vat1+"'";}
}

if(!profcharges.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"profcharges = '"+profcharges+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",profcharges = '"+profcharges+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!department.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"department = '"+department+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",department = '"+department+"'";}
}
if(!checkedby.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"checkedby = '"+checkedby+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",checkedby = '"+checkedby+"'";}
}
if(!checkedtime.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"checkedtime = '"+checkedtime+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",checkedtime = '"+checkedtime+"'";}
}
if(!approval_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"approval_status = '"+approval_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",approval_status = '"+approval_status+"'";}
}



if(!extra10.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra10 = '"+extra10+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra10 = '"+extra10+"'";}
}

if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}

if(!extra11.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra11 = '"+extra11+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra11 = '"+extra11+"'";}
}

if(!extra12.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra12 = '"+extra12+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra12 = '"+extra12+"'";}
}

if(!extra13.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra13 = '"+extra13+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra13 = '"+extra13+"'";}
}

if(!extra14.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra14 = '"+extra14+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra14 = '"+extra14+"'";}
}
if(!po_approved_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_status = '"+po_approved_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_status = '"+po_approved_status+"'";}
}
if(!po_approved_by.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_by = '"+po_approved_by+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_by = '"+po_approved_by+"'";}
}
if(!po_approved_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_date = '"+po_approved_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_date = '"+po_approved_date+"'";}
}
if(!extra13.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra13 = '"+extra13+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra13 = '"+extra13+"'";}
}
if(!majorhead_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"majorhead_id = '"+majorhead_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",majorhead_id = '"+majorhead_id+"'";}
}
if(!minorhead_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
}
if(!payment_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"payment_status = '"+payment_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",payment_status = '"+payment_status+"'";}
}
if(!actualentry_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"actualentry_date = '"+actualentry_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",actualentry_date = '"+actualentry_date+"'";}
}
if(!extra16.equals("")){ 
if(jk==0)
{
//	System.out.println("extra16 111111..."+extra16);
updatecc=updatecc+"extra16 = '"+extra16+"'"; 
jk=1; 
}else
{
//	System.out.println("extra16 222222222222..."+extra16);
updatecc=updatecc+",extra16 = '"+extra16+"'";}
}
if(!extra17.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra17 = '"+extra17+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra17 = '"+extra17+"'";}
}
if(!extra18.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra18 = '"+extra18+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra18 = '"+extra18+"'";}
}
if(!extra19.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra19 = '"+extra19+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra19 = '"+extra19+"'";}
}
if(!extra20.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra20 = '"+extra20+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra20 = '"+extra20+"'";}
}
String query="update godwanstock "+updatecc+" where gsId='"+gsId+"'";
System.out.println("update.. godwanstock  from gs ======================  =======>   "+query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
public boolean update1(String mseId)
{
	System.out.println("222222222222222222222222222222222222222222222222222222222222222222222222222222");
	boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!vendorId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendorId = '"+vendorId+"'";}
}

if(!productId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"productId = '"+productId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",productId = '"+productId+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!quantity.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"quantity = '"+quantity+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",quantity = '"+quantity+"'";}
}

if(!purchaseRate.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"purchaseRate = '"+purchaseRate+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",purchaseRate = '"+purchaseRate+"'";}
}

if(!date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date = '"+date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date = '"+date+"'";}
}

if(!MRNo.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"MRNo = '"+MRNo+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",MRNo = '"+MRNo+"'";}
}

if(!billNo.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"billNo = '"+billNo+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",billNo = '"+billNo+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!vat.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat = '"+vat+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat = '"+vat+"'";}
}

if(!vat1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat1 = '"+vat1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat1 = '"+vat1+"'";}
}

if(!profcharges.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"profcharges = '"+profcharges+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",profcharges = '"+profcharges+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!department.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"department = '"+department+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",department = '"+department+"'";}
}
if(!checkedby.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"checkedby = '"+checkedby+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",checkedby = '"+checkedby+"'";}
}
if(!checkedtime.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"checkedtime = '"+checkedtime+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",checkedtime = '"+checkedtime+"'";}
}
if(!approval_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"approval_status = '"+approval_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",approval_status = '"+approval_status+"'";}
}



if(!extra10.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra10 = '"+extra10+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra10 = '"+extra10+"'";}
}

if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}

if(!extra11.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra11 = '"+extra11+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra11 = '"+extra11+"'";}
}

if(!extra12.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra12 = '"+extra12+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra12 = '"+extra12+"'";}
}

if(!extra13.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra13 = '"+extra13+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra13 = '"+extra13+"'";}
}

if(!extra14.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra14 = '"+extra14+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra14 = '"+extra14+"'";}
}
if(!po_approved_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_status = '"+po_approved_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_status = '"+po_approved_status+"'";}
}
if(!po_approved_by.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_by = '"+po_approved_by+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_by = '"+po_approved_by+"'";}
}
if(!po_approved_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_date = '"+po_approved_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_date = '"+po_approved_date+"'";}
}
if(!extra13.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra13 = '"+extra13+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra13 = '"+extra13+"'";}
}
if(!majorhead_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"majorhead_id = '"+majorhead_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",majorhead_id = '"+majorhead_id+"'";}
}
if(!minorhead_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
}
if(!payment_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"payment_status = '"+payment_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",payment_status = '"+payment_status+"'";}
}
if(!actualentry_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"actualentry_date = '"+actualentry_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",actualentry_date = '"+actualentry_date+"'";}
}
if(!extra16.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra16 = '"+extra16+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra16 = '"+extra16+"'";}
}
if(!extra17.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra17 = '"+extra17+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra17 = '"+extra17+"'";}
}
if(!extra18.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra18 = '"+extra18+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra18 = '"+extra18+"'";}
}
if(!extra19.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra19 = '"+extra19+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra19 = '"+extra19+"'";}
}
if(!extra20.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra20 = '"+extra20+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra20 = '"+extra20+"'";}
}


if(!checKPrintDate.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"checKPrintDate = '"+checKPrintDate+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",checKPrintDate = '"+checKPrintDate+"'";}
}
if(!banktrn_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"banktrn_id = '"+banktrn_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",banktrn_id = '"+banktrn_id+"'";}
}
if(!brsDate.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"brsDate = '"+brsDate+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",brsDate = '"+brsDate+"'";}
}
String query="update godwanstock "+updatecc+" where extra1='"+mseId+"'";
System.out.println(" update godwanstockservice================================"+query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
public boolean updateBasedUniqueKey()
{
	System.out.println("3333333333333333333333333333333333333333333333333333333333333333333333333333");
	boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!vendorId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendorId = '"+vendorId+"'";}
}

if(!productId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"productId = '"+productId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",productId = '"+productId+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!quantity.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"quantity = '"+quantity+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",quantity = '"+quantity+"'";}
}

if(!purchaseRate.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"purchaseRate = '"+purchaseRate+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",purchaseRate = '"+purchaseRate+"'";}
}

if(!date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date = '"+date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date = '"+date+"'";}
}

if(!MRNo.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"MRNo = '"+MRNo+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",MRNo = '"+MRNo+"'";}
}

if(!billNo.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"billNo = '"+billNo+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",billNo = '"+billNo+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}
if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!vat.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat = '"+vat+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat = '"+vat+"'";}
}

if(!vat1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat1 = '"+vat1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat1 = '"+vat1+"'";}
}

if(!profcharges.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"profcharges = '"+profcharges+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",profcharges = '"+profcharges+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!department.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"department = '"+department+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",department = '"+department+"'";}
}
if(!checkedby.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"checkedby = '"+checkedby+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",checkedby = '"+checkedby+"'";}
}
if(!checkedtime.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"checkedtime = '"+checkedtime+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",checkedtime = '"+checkedtime+"'";}
}
if(!approval_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"approval_status = '"+approval_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",approval_status = '"+approval_status+"'";}
}



if(!extra10.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra10 = '"+extra10+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra10 = '"+extra10+"'";}
}

if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}

if(!extra11.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra11 = '"+extra11+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra11 = '"+extra11+"'";}
}

if(!extra12.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra12 = '"+extra12+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra12 = '"+extra12+"'";}
}

if(!extra13.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra13 = '"+extra13+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra13 = '"+extra13+"'";}
}

if(!extra14.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra14 = '"+extra14+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra14 = '"+extra14+"'";}
}
if(!po_approved_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_status = '"+po_approved_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_status = '"+po_approved_status+"'";}
}
if(!po_approved_by.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_by = '"+po_approved_by+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_by = '"+po_approved_by+"'";}
}
if(!po_approved_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"po_approved_date = '"+po_approved_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",po_approved_date = '"+po_approved_date+"'";}
}
if(!extra13.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra13 = '"+extra13+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra13 = '"+extra13+"'";}
}

if(!bill_update_by.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"bill_update_by = '"+bill_update_by+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",bill_update_by = '"+bill_update_by+"'";}
}

if(!bill_update_time.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"bill_update_time = '"+bill_update_time+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",bill_update_time = '"+bill_update_time+"'";}
}
String query="update godwanstock "+updatecc+" where extra9='"+extra9+"'";
System.out.println(query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
public boolean updateStatus(String Inv_id,String status)
{	
	boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="update godwanstock set approval_status='"+status+"' where extra1='"+Inv_id+"'";
System.out.println("update8585 ==============>"+query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from godwanstock where gsId='"+gsId+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}

	public boolean updateBRSDateBasedOnBktId(String brsDate, String banktrn_id){
		String UPDATE_QUERY = "UPDATE godwanstock SET brsDate=? where banktrn_id=?";
		boolean flag = false;
		PreparedStatement ps = null;
		
		try {
			c = ConnectionHelper.getConnection();
			ps = c.prepareStatement(UPDATE_QUERY);
			ps.setString(1, brsDate);
			ps.setString(2, banktrn_id);
			flag = ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				ps.close();
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return flag;
	}
	
	public boolean updateExpensesEntryDateBasedOnExpvId(String expensesEntryDate, String expinv_id){
		String UPDATE_QUERY = "UPDATE godwanstock SET expensesEntryDate=? where extra21=?";
		boolean flag = false;
		PreparedStatement ps = null;
		
		try {
			c = ConnectionHelper.getConnection();
			ps = c.prepareStatement(UPDATE_QUERY);
			ps.setString(1, expensesEntryDate);
			ps.setString(2, expinv_id);
			flag = ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				ps.close();
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return flag;
	}
	
	public int insertGodownStock(godwanstock gs)	{
		PreparedStatement ps = null;
		String query = "insert into godwanstock values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		int result = 0;
		int ticketm=0;
		no_genaratorListing nl= new no_genaratorListing();
		try	{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			rs=st.executeQuery("select * from no_genarator where table_name='godwanstock'");
			rs.next();
			int ticket=rs.getInt("table_id");
			String PriSt =rs.getString("id_prefix");
			ticketm=ticket+1;
			gs.setGsId(PriSt+ticketm);
			
			ps = c.prepareStatement(query);
			ps.setString(1,gs.getGsId());
			ps.setString(2,gs.getVendorId());
			ps.setString(3,gs.getProductId());
			ps.setString(4,gs.getDescription());
			ps.setString(5,gs.getQuantity());
			ps.setString(6,gs.getPurchaseRate());
			ps.setString(7,gs.getDate());
			ps.setString(8,gs.getMRNo());
			ps.setString(9, gs.getBillNo());
			ps.setString(10, gs.getExtra1());
			ps.setString(11, gs.getExtra2());
			ps.setString(12, gs.getExtra3());
			ps.setString(13, gs.getExtra4());
			ps.setString(14, gs.getExtra5());
			ps.setString(15, gs.getVat());
			ps.setString(16, gs.getVat1());
			ps.setString(17, gs.getProfcharges());
			ps.setString(18, gs.getEmp_id());
			ps.setString(19, gs.getDepartment());
			ps.setString(20, gs.getCheckedby());
			ps.setString(21, gs.getCheckedtime());
			ps.setString(22, gs.getApproval_status());
			ps.setString(23, gs.getExtra6());
			ps.setString(24, gs.getExtra7());
			ps.setString(25, gs.getExtra8());
			ps.setString(26, gs.getExtra9());
			ps.setString(27, gs.getExtra10());
			ps.setString(28, gs.getPo_approved_status());
			ps.setString(29, gs.getPo_approved_by());
			ps.setString(30, gs.getPo_approved_date());
			ps.setString(31, gs.getBill_status());
			ps.setString(32, gs.getBill_update_by());
			ps.setString(33, gs.getBill_update_time());
			ps.setString(34, gs.getExtra11());
			ps.setString(35, gs.getExtra12());
			ps.setString(36, gs.getExtra13());
			ps.setString(37, gs.getExtra14());
			ps.setString(38, gs.getExtra15());
			ps.setString(39, gs.getMajorhead_id());
			ps.setString(40, gs.getMinorhead_id());
			ps.setString(41, gs.getPayment_status());
			ps.setString(42, gs.getActualentry_date());
			ps.setString(43, gs.getExtra16());
			ps.setString(44, gs.getExtra17());
			ps.setString(45, gs.getExtra18());
			ps.setString(46, gs.getExtra19());
			ps.setString(47, gs.getExtra20());
			ps.setString(48, gs.getExpensesEntryDate());
			ps.setString(49, gs.getChecKPrintDate());
			ps.setString(50, gs.getBanktrn_id());
			ps.setString(51, gs.getBrsDate());
			ps.setString(52, gs.getExtra21());
			ps.setString(53, gs.getExtra22());
			ps.setString(54, gs.getExtra23());
			ps.setString(55, gs.getExtra24());
			ps.setString(56, gs.getExtra25());
			ps.setString(57, gs.getExtra26());
			ps.setString(58, gs.getExtra27());
			ps.setString(59, gs.getExtra28());
			ps.setString(60, gs.getExtra29());
			ps.setString(61, gs.getExtra30());
			ps.setString(62, gs.getExtra31());
			ps.setString(63, gs.getExtra32());
			ps.setString(64, gs.getExtra33());
			ps.setString(65, gs.getExtra34());
			ps.setString(66, gs.getExtra35());
			
			result = ps.executeUpdate();
			
			String updatequery="update no_genarator set table_id="+ticketm+" where table_name='godwanstock'";
			st.executeUpdate(updatequery);
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		finally	{
			try {
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				st.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	return result;	
	}
	
	
	public int updateAppStatusBasedOnDcno(String dcNo){
		String UPDATE_QUERY = "UPDATE godwanstock SET approval_status='Approved' where approval_status='Not-Approve' and extra25=?";
		int result = 0;
		PreparedStatement ps = null;
		
		try {
			c = ConnectionHelper.getConnection();
			ps = c.prepareStatement(UPDATE_QUERY);
			ps.setString(1,dcNo);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				ps.close();
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	
	public boolean updateMajorMinorSubHeadProduct(){
		boolean flag=false;
		try
		{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
		String updatecc="set ";
		 int jk=0; 
		
		if(!productId.equals("")){ 
			if(jk==0)
			{
			updatecc=updatecc+"productId = '"+productId+"'"; 
			jk=1; 
			}else
			{
			updatecc=updatecc+",productId = '"+productId+"'";}
			}
		if(!majorhead_id.equals("")){ 
			if(jk==0)
			{
			updatecc=updatecc+"majorhead_id = '"+majorhead_id+"'"; 
			jk=1; 
			}else
			{
			updatecc=updatecc+",majorhead_id = '"+majorhead_id+"'";}
			}
			if(!minorhead_id.equals("")){ 
			if(jk==0)
			{
			updatecc=updatecc+"minorhead_id = '"+minorhead_id+"'"; 
			jk=1; 
			}else
			{
			updatecc=updatecc+",minorhead_id = '"+minorhead_id+"'";}
			}
			if(!extra21.equals("")){ 
				if(jk==0)
				{
				updatecc=updatecc+"extra21 = '"+extra21+"'"; 
				jk=1; 
				}else
				{
				updatecc=updatecc+",extra21 = '"+extra21+"'";}
				}
			if(!gsId.equals("")){ 
				if(jk==0)
				{
				updatecc=updatecc+"gsId = '"+gsId+"'"; 
				jk=1; 
				}else
				{
				updatecc=updatecc+",gsId = '"+gsId+"'";}
				}
			
			String query="update godwanstock "+updatecc+" where extra21='"+extra21+"' and gsid='"+gsId+"'";
			
			
			int i = st.executeUpdate(query);
			if(i>0){flag=true;}
			}catch(Exception e){
			this.seterror(e.toString());
			}finally{
				try {
					if(rs != null){rs.close();}
					if(st != null){st.close();}
					if(c != null){c.close();}
					} catch (SQLException e) {
					e.printStackTrace();
					}
			}
			return flag;
	}
	
	
	
	
}