package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class cash_reportService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String cash_report_id="";
private String balance_code_flag="";
private String date="";
private String shift_code="";
private String counter_code="";
private String thousands="";
private String fivehundreds="";
private String hundreds="";
private String fifyts="";
private String twntys="";
private String tens="";
private String fives="";
private String twos="";
private String ones="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public cash_reportService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setcash_report_id(String cash_report_id)
{
this.cash_report_id = cash_report_id;
}

public String getcash_report_id()
{
return (this.cash_report_id);
}


public void setbalance_code_flag(String balance_code_flag)
{
this.balance_code_flag = balance_code_flag;
}

public String getbalance_code_flag()
{
return (this.balance_code_flag);
}


public void setdate(String date)
{
this.date = date;
}

public String getdate()
{
return (this.date);
}


public void setshift_code(String shift_code)
{
this.shift_code = shift_code;
}

public String getshift_code()
{
return (this.shift_code);
}


public void setcounter_code(String counter_code)
{
this.counter_code = counter_code;
}

public String getcounter_code()
{
return (this.counter_code);
}


public void setthousands(String thousands)
{
this.thousands = thousands;
}

public String getthousands()
{
return (this.thousands);
}


public void setfivehundreds(String fivehundreds)
{
this.fivehundreds = fivehundreds;
}

public String getfivehundreds()
{
return (this.fivehundreds);
}


public void sethundreds(String hundreds)
{
this.hundreds = hundreds;
}

public String gethundreds()
{
return (this.hundreds);
}


public void setfifyts(String fifyts)
{
this.fifyts = fifyts;
}

public String getfifyts()
{
return (this.fifyts);
}


public void settwntys(String twntys)
{
this.twntys = twntys;
}

public String gettwntys()
{
return (this.twntys);
}


public void settens(String tens)
{
this.tens = tens;
}

public String gettens()
{
return (this.tens);
}


public void setfives(String fives)
{
this.fives = fives;
}

public String getfives()
{
return (this.fives);
}


public void settwos(String twos)
{
this.twos = twos;
}

public String gettwos()
{
return (this.twos);
}


public void setones(String ones)
{
this.ones = ones;
}

public String getones()
{
return (this.ones);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void setextra6(String extra6)
{
this.extra6 = extra6;
}

public String getextra6()
{
return (this.extra6);
}


public void setextra7(String extra7)
{
this.extra7 = extra7;
}

public String getextra7()
{
return (this.extra7);
}


public void setextra8(String extra8)
{
this.extra8 = extra8;
}

public String getextra8()
{
return (this.extra8);
}


public void setextra9(String extra9)
{
this.extra9 = extra9;
}

public String getextra9()
{
return (this.extra9);
}


public void setextra10(String extra10)
{
this.extra10 = extra10;
}

public String getextra10()
{
return (this.extra10);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='cash_report'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 cash_report_id=PriSt+ticketm;
String query="insert into cash_report values('"+cash_report_id+"','"+balance_code_flag+"',now(),'"+shift_code+"','"+counter_code+"','"+thousands+"','"+fivehundreds+"','"+hundreds+"','"+fifyts+"','"+twntys+"','"+tens+"','"+fives+"','"+twos+"','"+ones+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"')";
st.executeUpdate(query);
int i = st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='cash_report'");
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!balance_code_flag.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"balance_code_flag = '"+balance_code_flag+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",balance_code_flag = '"+balance_code_flag+"'";}
}

if(!date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date = '"+date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date = '"+date+"'";}
}

if(!shift_code.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"shift_code = '"+shift_code+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",shift_code = '"+shift_code+"'";}
}

if(!counter_code.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"counter_code = '"+counter_code+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",counter_code = '"+counter_code+"'";}
}

if(!thousands.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"thousands = '"+thousands+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",thousands = '"+thousands+"'";}
}

if(!fivehundreds.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"fivehundreds = '"+fivehundreds+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",fivehundreds = '"+fivehundreds+"'";}
}

if(!hundreds.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"hundreds = '"+hundreds+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",hundreds = '"+hundreds+"'";}
}

if(!fifyts.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"fifyts = '"+fifyts+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",fifyts = '"+fifyts+"'";}
}

if(!twntys.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"twntys = '"+twntys+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",twntys = '"+twntys+"'";}
}

if(!tens.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"tens = '"+tens+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",tens = '"+tens+"'";}
}

if(!fives.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"fives = '"+fives+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",fives = '"+fives+"'";}
}

if(!twos.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"twos = '"+twos+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",twos = '"+twos+"'";}
}

if(!ones.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"ones = '"+ones+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",ones = '"+ones+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!extra6.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra6 = '"+extra6+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra6 = '"+extra6+"'";}
}

if(!extra7.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra7 = '"+extra7+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra7 = '"+extra7+"'";}
}

if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}

if(!extra9.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra9 = '"+extra9+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra9 = '"+extra9+"'";}
}

if(!extra10.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra10 = '"+extra10+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra10 = '"+extra10+"'";}
}
String query="update cash_report "+updatecc+" where cash_report_id='"+cash_report_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from cash_report where cash_report_id='"+cash_report_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;
}
}