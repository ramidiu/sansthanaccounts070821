package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class headofaccountsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String head_account_id="";
private String name="";
private String phone="";
private String address="";
private String description="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public headofaccountsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setname(String name)
{
this.name = name;
}

public String getname()
{
return (this.name);
}


public void setphone(String phone)
{
this.phone = phone;
}

public String getphone()
{
return (this.phone);
}


public void setaddress(String address)
{
this.address = address;
}

public String getaddress()
{
return (this.address);
}


public void setdescription(String description)
{
this.description = description;
}

public String getdescription()
{
return (this.description);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='headofaccounts'");
rs.next();
int ticket=rs.getInt("table_id");
//String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 head_account_id=""+ticketm;
String query="insert into headofaccounts values('"+head_account_id+"','"+name+"','"+phone+"','"+address+"','"+description+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='headofaccounts'");
flag = true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"name = '"+name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",name = '"+name+"'";}
}

if(!phone.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"phone = '"+phone+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",phone = '"+phone+"'";}
}

if(!address.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"address = '"+address+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",address = '"+address+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update headofaccounts "+updatecc+" where head_account_id='"+head_account_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from headofaccounts where head_account_id='"+head_account_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}