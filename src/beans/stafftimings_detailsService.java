package beans;

import java.sql.Connection;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class stafftimings_detailsService {
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String staff_id="";
	private String government_reservation="";
	private String caste_regligion="";
	private String dependents="";
	private String educational_qual="";
	private String technical_qual="";
	private String identification_marks="";
	private String previous_experience="";
	private String height="";
	private String physical_disorder="";
	private String present_pay_scale="";
	private String basic_pay="";
	private String reference="";
	private String graduvite_amount="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	private String extra6="";
	private String extra7="";
	private String extra8="";
	private String extra9="";
	
	private Statement st = null;
	private Connection c=null;
	
	
	public stafftimings_detailsService()
	{
		/*try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			}catch(Exception e){
			this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
			}*/
		
	}
	public String getStaff_id() {
		return staff_id;
	}
	public void setStaff_id(String staff_id) {
		this.staff_id = staff_id;
	}
	public String getGovernment_reservation() {
		return government_reservation;
	}
	public void setGovernment_reservation(String government_reservation) {
		this.government_reservation = government_reservation;
	}
	public String getCaste_regligion() {
		return caste_regligion;
	}
	public void setCaste_regligion(String caste_regligion) {
		this.caste_regligion = caste_regligion;
	}
	public String getDependents() {
		return dependents;
	}
	public void setDependents(String dependents) {
		this.dependents = dependents;
	}
	public String getEducational_qual() {
		return educational_qual;
	}
	public void setEducational_qual(String educational_qual) {
		this.educational_qual = educational_qual;
	}
	public String getTechnical_qual() {
		return technical_qual;
	}
	public void setTechnical_qual(String technical_qual) {
		this.technical_qual = technical_qual;
	}
	public String getIdentification_marks() {
		return identification_marks;
	}
	public void setIdentification_marks(String identification_marks) {
		this.identification_marks = identification_marks;
	}
	public String getPrevious_experience() {
		return previous_experience;
	}
	public void setPrevious_experience(String previous_experience) {
		this.previous_experience = previous_experience;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getPhysical_disorder() {
		return physical_disorder;
	}
	public void setPhysical_disorder(String physical_disorder) {
		this.physical_disorder = physical_disorder;
	}
	public String getPresent_pay_scale() {
		return present_pay_scale;
	}
	public void setPresent_pay_scale(String present_pay_scale) {
		this.present_pay_scale = present_pay_scale;
	}
	public String getBasic_pay() {
		return basic_pay;
	}
	public void setBasic_pay(String basic_pay) {
		this.basic_pay = basic_pay;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getGraduvite_amount() {
		return graduvite_amount;
	}
	public void setGraduvite_amount(String graduvite_amount) {
		this.graduvite_amount = graduvite_amount;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public boolean insert()
	{
	try
	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
	String query="insert into stafftimings_details values('"+staff_id+"','"+government_reservation+"','"+caste_regligion+"','"+dependents+"','"+educational_qual+"','"+technical_qual+"','"+identification_marks+"','"+previous_experience+"','"+height+"','"+physical_disorder+"','"+present_pay_scale+"','"+basic_pay+"','"+reference+"','"+graduvite_amount+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"')";
	st.executeUpdate(query);
	//System.out.println("insertnewstaffdetails"+query);
	st.close();
	c.close();
	}catch(Exception e){
		e.printStackTrace();
	this.seterror(e.toString());
	return false;
	}
	return true;

	}public boolean update()
	{
	try
	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
	String updatecc="set ";
	 int jk=0; 
	 if(!staff_id.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"staff_id = '"+staff_id+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",staff_id = '"+staff_id+"'";}
	}

	if(!government_reservation.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"government_reservation = '"+government_reservation+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",government_reservation = '"+government_reservation+"'";}
	}

	if(!caste_regligion.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"caste_regligion = '"+caste_regligion+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",caste_regligion = '"+caste_regligion+"'";}
	}

	if(!dependents.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"dependents = '"+dependents+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",dependents = '"+dependents+"'";}
	}

	if(!educational_qual.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"educational_qual = '"+educational_qual+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",educational_qual = '"+educational_qual+"'";}
	}

	if(!technical_qual.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"technical_qual = '"+technical_qual+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",technical_qual = '"+technical_qual+"'";}
	}

	if(!identification_marks.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"identification_marks = '"+identification_marks+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",identification_marks = '"+identification_marks+"'";}
	}

	if(!previous_experience.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"previous_experience = '"+previous_experience+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",previous_experience = '"+previous_experience+"'";}
	}

	if(!height.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"height = '"+height+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",height = '"+height+"'";}
	}

	if(!physical_disorder.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"physical_disorder = '"+physical_disorder+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",physical_disorder = '"+physical_disorder+"'";}
	}

	if(!present_pay_scale.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"present_pay_scale = '"+present_pay_scale+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",present_pay_scale = '"+present_pay_scale+"'";}
	}

	if(!basic_pay.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"basic_pay = '"+basic_pay+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",basic_pay = '"+basic_pay+"'";}
	}

	if(!reference.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"reference = '"+reference+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",reference = '"+reference+"'";}
	}

	if(!graduvite_amount.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"graduvite_amount = '"+graduvite_amount+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",graduvite_amount = '"+graduvite_amount+"'";}
	}
	if(!extra1.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra1 = '"+extra1+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra1 = '"+extra1+"'";}
	}
	if(!extra2.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra2 = '"+extra2+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra2 = '"+extra2+"'";}
	}
	if(!extra3.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra3 = '"+extra3+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra3 = '"+extra3+"'";}
	}
	if(!extra4.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra4 = '"+extra4+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra4 = '"+extra4+"'";}
	}
	if(!extra5.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra5 = '"+extra5+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra5 = '"+extra5+"'";}
	}
	if(!extra6.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra6 = '"+extra6+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra6 = '"+extra6+"'";}
	}
	if(!extra7.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra7 = '"+extra7+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra7 = '"+extra7+"'";}
	}
	if(!extra8.equals("")){ 
	if(jk==0)
	{
	updatecc=updatecc+"extra8 = '"+extra8+"'"; 
	jk=1; 
	}else
	{
	updatecc=updatecc+",extra8 = '"+extra8+"'";}
	}
	if(!extra9.equals("")){ 
		if(jk==0)
		{
		updatecc=updatecc+"extra9 = '"+extra9+"'"; 
		jk=1; 
		}else
		{
		updatecc=updatecc+",extra9 = '"+extra9+"'";}
		}

	String query="update stafftimings_details "+updatecc+" where staff_id='"+staff_id+"'";
	//System.out.print("updateeestaffdetails=====>"+query);
	st.executeUpdate(query);
	
	st.close();
	c.close();
	return true;
	}catch(Exception e){
	this.seterror(e.toString());
	return false;
	}

	}
	public boolean delete()
	{
	try
	{c = ConnectionHelper.getConnection();
	st=c.createStatement();
	String query="delete from stafftimings_details where staff_id='"+staff_id+"'";
	st.executeUpdate(query);
	st.close();
	c.close();
	return true;
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		e.printStackTrace();
		return false;
	}

	}

}
