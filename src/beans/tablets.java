package beans;
public class tablets 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String tabletId="";
private String tabletName="";
private String quantity="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public tablets() {} 
public tablets(String tabletId,String tabletName,String quantity,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.tabletId=tabletId;
this.tabletName=tabletName;
this.quantity=quantity;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String gettabletId() {
return tabletId;
}
public void settabletId(String tabletId) {
this.tabletId = tabletId;
}
public String gettabletName() {
return tabletName;
}
public void settabletName(String tabletName) {
this.tabletName = tabletName;
}
public String getquantity() {
return quantity;
}
public void setquantity(String quantity) {
this.quantity = quantity;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}