package beans;

import java.sql.Connection;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class openingcl_balService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String emp_id="";
private String mon_yr="";
private String value="";
private String actual_opening_cl="";

private Statement st = null;
private Connection c=null;
public openingcl_balService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}


public void setmon_yr(String mon_yr)
{
this.mon_yr = mon_yr;
}

public String getmon_yr()
{
return (this.mon_yr);
}


public void setvalue(String value)
{
this.value = value;
}

public String getvalue()
{
return (this.value);
}


public String getActual_opening_cl() {
	return actual_opening_cl;
}
public void setActual_opening_cl(String actualOpeningCl) {
	actual_opening_cl = actualOpeningCl;
}
public boolean insert()
{
	try
	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
		String query="insert into openingcl_bal values('"+emp_id+"','"+mon_yr+"','"+value+"','"+actual_opening_cl+"')";
		st.executeUpdate(query);
		st.close();
		c.close();
		return true;
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		e.printStackTrace();
		return false;
	}
}

public boolean update()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
	String query="update openingcl_bal set value='"+value+"' where emp_id='"+emp_id+"' and mon_yr='"+mon_yr+"' ";
	st.executeUpdate(query);
	st.close();
	c.close();
	return true;
	}
	catch(Exception e)
	{
		this.seterror(e.toString());
		return false;
	}
}
public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from openingcl_bal where emp_id='"+emp_id+"' and mon_yr='"+mon_yr+"' ";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}