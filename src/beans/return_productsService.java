package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class return_productsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String return_id="";
private String product_id="";
private String return_qty="";
private String return_date="";
private String mse_inv_id="";
private String vendor_billno="";
private String return_inv_id="";
private String entered_by="";
private String approved_by="";
private String approved_date="";
private String narration="";
private String vendor_id="";
private String head_account_id="";
private String status="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public return_productsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setreturn_id(String return_id)
{
this.return_id = return_id;
}

public String getreturn_id()
{
return (this.return_id);
}


public void setproduct_id(String product_id)
{
this.product_id = product_id;
}

public String getproduct_id()
{
return (this.product_id);
}


public void setreturn_qty(String return_qty)
{
this.return_qty = return_qty;
}

public String getreturn_qty()
{
return (this.return_qty);
}


public void setreturn_date(String return_date)
{
this.return_date = return_date;
}

public String getreturn_date()
{
return (this.return_date);
}


public void setmse_inv_id(String mse_inv_id)
{
this.mse_inv_id = mse_inv_id;
}

public String getmse_inv_id()
{
return (this.mse_inv_id);
}


public void setvendor_billno(String vendor_billno)
{
this.vendor_billno = vendor_billno;
}

public String getvendor_billno()
{
return (this.vendor_billno);
}


public void setreturn_inv_id(String return_inv_id)
{
this.return_inv_id = return_inv_id;
}

public String getreturn_inv_id()
{
return (this.return_inv_id);
}


public void setentered_by(String entered_by)
{
this.entered_by = entered_by;
}

public String getentered_by()
{
return (this.entered_by);
}


public void setapproved_by(String approved_by)
{
this.approved_by = approved_by;
}

public String getapproved_by()
{
return (this.approved_by);
}


public void setapproved_date(String approved_date)
{
this.approved_date = approved_date;
}

public String getapproved_date()
{
return (this.approved_date);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setvendor_id(String vendor_id)
{
this.vendor_id = vendor_id;
}

public String getvendor_id()
{
return (this.vendor_id);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setstatus(String status)
{
this.status = status;
}

public String getstatus()
{
return (this.status);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='return_products'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 return_id=PriSt+ticketm;
String query="insert into return_products values('"+return_id+"','"+product_id+"','"+return_qty+"','"+return_date+"','"+mse_inv_id+"','"+vendor_billno+"','"+return_inv_id+"','"+entered_by+"','"+approved_by+"','"+approved_date+"','"+narration+"','"+vendor_id+"','"+head_account_id+"','"+status+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='return_products'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean update()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!product_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"product_id = '"+product_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",product_id = '"+product_id+"'";}
}

if(!return_qty.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"return_qty = '"+return_qty+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",return_qty = '"+return_qty+"'";}
}

if(!return_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"return_date = '"+return_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",return_date = '"+return_date+"'";}
}

if(!mse_inv_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"mse_inv_id = '"+mse_inv_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",mse_inv_id = '"+mse_inv_id+"'";}
}

if(!vendor_billno.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendor_billno = '"+vendor_billno+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendor_billno = '"+vendor_billno+"'";}
}

if(!return_inv_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"return_inv_id = '"+return_inv_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",return_inv_id = '"+return_inv_id+"'";}
}

if(!entered_by.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"entered_by = '"+entered_by+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",entered_by = '"+entered_by+"'";}
}

if(!approved_by.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"approved_by = '"+approved_by+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",approved_by = '"+approved_by+"'";}
}

if(!approved_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"approved_date = '"+approved_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",approved_date = '"+approved_date+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!vendor_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendor_id = '"+vendor_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendor_id = '"+vendor_id+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

if(!status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"status = '"+status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",status = '"+status+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update return_products "+updatecc+" where return_id='"+return_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from return_products where return_id='"+return_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}