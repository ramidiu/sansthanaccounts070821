package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import dbase.sqlcon.ConnectionHelper;
import dbase.sqlcon.SainivasConnectionHelper;

public class cashier_reportService {
	private String error = "";

	public void seterror(String error) {
		this.error = error;
	}

	public String geterror() {
		return this.error;
	}

	private String cashier_report_id = "";
	private String shift_code = "";
	private String counter_code = "";
	private String login_id = "";
	private String opening_balance = "";
	private String counter_balance = "";
	private String closing_balance = "";
	private String total_amount = "";
	private String logoin_time = "";
	private String logout_time = "";
	private String cash1 = "";
	private String cash2 = "";
	private String cash3 = "";
	private String cash4 = "";
	private String cash5 = "";
	private String cash6 = "";
	private String cash7 = "";
	private String cash8 = "";
	private String cash9 = "";
	private String cash10 = "";
	private String due_banktrnsid = "";
	private String edit_by = "";
	private String update_time = "";
	private String extra11 = "";
	private String extra12 = "";

	private ResultSet rs = null;
	private Statement st = null;
	private Connection c = null;

	public cashier_reportService() {
		/*
		 * try { // Load the database driver c =
		 * ConnectionHelper.getConnection(); st=c.createStatement();
		 * }catch(Exception e){ this.seterror(e.toString());
		 * System.out.println("Exception is ;"+e); }
		 */
	}

	public void setcashier_report_id(String cashier_report_id) {
		this.cashier_report_id = cashier_report_id;
	}

	public String getcashier_report_id() {
		return (this.cashier_report_id);
	}

	public void setshift_code(String shift_code) {
		this.shift_code = shift_code;
	}

	public String getshift_code() {
		return (this.shift_code);
	}

	public void setcounter_code(String counter_code) {
		this.counter_code = counter_code;
	}

	public String getcounter_code() {
		return (this.counter_code);
	}

	public void setlogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getlogin_id() {
		return (this.login_id);
	}

	public void setopening_balance(String opening_balance) {
		this.opening_balance = opening_balance;
	}

	public String getopening_balance() {
		return (this.opening_balance);
	}

	public void setcounter_balance(String counter_balance) {
		this.counter_balance = counter_balance;
	}

	public String getcounter_balance() {
		return (this.counter_balance);
	}

	public void setclosing_balance(String closing_balance) {
		this.closing_balance = closing_balance;
	}

	public String getclosing_balance() {
		return (this.closing_balance);
	}

	public void settotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}

	public String gettotal_amount() {
		return (this.total_amount);
	}

	public void setlogoin_time(String logoin_time) {
		this.logoin_time = logoin_time;
	}

	public String getlogoin_time() {
		return (this.logoin_time);
	}

	public void setlogout_time(String logout_time) {
		this.logout_time = logout_time;
	}

	public String getlogout_time() {
		return (this.logout_time);
	}

	public void setcash1(String cash1) {
		this.cash1 = cash1;
	}

	public String getcash1() {
		return (this.cash1);
	}

	public void setcash2(String cash2) {
		this.cash2 = cash2;
	}

	public String getcash2() {
		return (this.cash2);
	}

	public void setcash3(String cash3) {
		this.cash3 = cash3;
	}

	public String getcash3() {
		return (this.cash3);
	}

	public void setcash4(String cash4) {
		this.cash4 = cash4;
	}

	public String getcash4() {
		return (this.cash4);
	}

	public void setcash5(String cash5) {
		this.cash5 = cash5;
	}

	public String getcash5() {
		return (this.cash5);
	}

	public void setcash6(String cash6) {
		this.cash6 = cash6;
	}

	public String getcash6() {
		return (this.cash6);
	}

	public void setcash7(String cash7) {
		this.cash7 = cash7;
	}

	public String getcash7() {
		return (this.cash7);
	}

	public void setcash8(String cash8) {
		this.cash8 = cash8;
	}

	public String getcash8() {
		return (this.cash8);
	}

	public void setcash9(String cash9) {
		this.cash9 = cash9;
	}

	public String getcash9() {
		return (this.cash9);
	}

	public void setcash10(String cash10) {
		this.cash10 = cash10;
	}

	public String getcash10() {
		return (this.cash10);
	}

	public String getDue_banktrnsid() {
		return due_banktrnsid;
	}

	public void setDue_banktrnsid(String due_banktrnsid) {
		this.due_banktrnsid = due_banktrnsid;
	}

	public String getEdit_by() {
		return edit_by;
	}

	public void setEdit_by(String edit_by) {
		this.edit_by = edit_by;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public String getExtra11() {
		return extra11;
	}

	public void setExtra11(String extra11) {
		this.extra11 = extra11;
	}

	public String getExtra12() {
		return extra12;
	}

	public void setExtra12(String extra12) {
		this.extra12 = extra12;
	}

	public boolean insert() {
		boolean flag = false;
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			rs = st.executeQuery("select * from no_genarator where table_name='cashier_report'");
			rs.next();
			int ticket = rs.getInt("table_id");
			String PriSt = rs.getString("id_prefix");
			int ticketm = ticket + 1;
			cashier_report_id = PriSt + ticketm;
			String query = "insert into cashier_report values('"
					+ cashier_report_id + "','" + shift_code + "','"
					+ counter_code + "','" + login_id + "','" + opening_balance
					+ "','" + counter_balance + "','" + closing_balance + "','"
					+ total_amount + "','" + logoin_time + "','" + logout_time
					+ "','" + cash1 + "','" + cash2 + "','" + cash3 + "','"
					+ cash4 + "','" + cash5 + "','" + cash6 + "','" + cash7
					+ "','" + cash8 + "','" + cash9 + "','" + cash10 + "','"
					+ due_banktrnsid + "','" + edit_by + "','" + update_time
					+ "','" + extra11 + "','" + extra12 + "')";
			st.executeUpdate(query);
			int i = st.executeUpdate("update no_genarator set table_id="
					+ ticketm + " where table_name='cashier_report'");
			if (i > 0) {
				flag = true;
			}
		} catch (Exception e) {
			this.seterror(e.toString());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;

	}

	public boolean update() {
		boolean flag = false;
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String updatecc = "set ";
			int jk = 0;
			if (!shift_code.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "shift_code = '" + shift_code + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",shift_code = '" + shift_code + "'";
				}
			}

			if (!counter_code.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "counter_code = '" + counter_code
							+ "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",counter_code = '" + counter_code
							+ "'";
				}
			}

			if (!login_id.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "login_id = '" + login_id + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",login_id = '" + login_id + "'";
				}
			}

			if (!opening_balance.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "opening_balance = '"
							+ opening_balance + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",opening_balance = '"
							+ opening_balance + "'";
				}
			}

			if (!counter_balance.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "counter_balance = '"
							+ counter_balance + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",counter_balance = '"
							+ counter_balance + "'";
				}
			}

			if (!closing_balance.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "closing_balance = '"
							+ closing_balance + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",closing_balance = '"
							+ closing_balance + "'";
				}
			}

			if (!total_amount.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "total_amount = '" + total_amount
							+ "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",total_amount = '" + total_amount
							+ "'";
				}
			}

			if (!logoin_time.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "logoin_time = '" + logoin_time + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",logoin_time = '" + logoin_time
							+ "'";
				}
			}

			if (!logout_time.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "logout_time = '" + logout_time + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",logout_time = '" + logout_time
							+ "'";
				}
			}

			if (!cash1.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash1 = '" + cash1 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash1 = '" + cash1 + "'";
				}
			}

			if (!cash2.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash2 = '" + cash2 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash2 = '" + cash2 + "'";
				}
			}

			if (!cash3.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash3 = '" + cash3 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash3 = '" + cash3 + "'";
				}
			}

			if (!cash4.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash4 = '" + cash4 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash4 = '" + cash4 + "'";
				}
			}

			if (!cash5.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash5 = '" + cash5 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash5 = '" + cash5 + "'";
				}
			}

			if (!cash6.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash6 = '" + cash6 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash6 = '" + cash6 + "'";
				}
			}

			if (!cash7.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash7 = '" + cash7 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash7 = '" + cash7 + "'";
				}
			}

			if (!cash8.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash8 = '" + cash8 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash8 = '" + cash8 + "'";
				}
			}

			if (!cash9.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash9 = '" + cash9 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash9 = '" + cash9 + "'";
				}
			}

			if (!cash10.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash10 = '" + cash10 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash10 = '" + cash10 + "'";
				}
			}

			if (!due_banktrnsid.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "due_banktrnsid = '" + due_banktrnsid
							+ "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",due_banktrnsid = '"
							+ due_banktrnsid + "'";
				}
			}

			if (!edit_by.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "edit_by = '" + edit_by + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",edit_by = '" + edit_by + "'";
				}
			}
			if (!update_time.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "update_time = '" + update_time + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",update_time = '" + update_time
							+ "'";
				}
			}

			String query = "update cashier_report " + updatecc
					+ " where cashier_report_id='" + cashier_report_id + "'";
			System.out.println();
			System.out.println("cashier report table update7777777777777777777777777777777777777777777777777777777777777"+query);
			int i = st.executeUpdate(query);
			System.out.println("request coming to cash111111------"+Calendar.getInstance().getTime());
			if (i > 0) {
				flag = true;
			}
		} catch (Exception e) {
			this.seterror(e.toString());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;

	}

	public boolean SainivasUpdate() {
		boolean flag = false;
		try {
			c = SainivasConnectionHelper.getConnection();
			st = c.createStatement();
			String updatecc = "set ";
			int jk = 0;
			if (!shift_code.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "shift_code = '" + shift_code + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",shift_code = '" + shift_code + "'";
				}
			}

			if (!counter_code.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "counter_code = '" + counter_code
							+ "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",counter_code = '" + counter_code
							+ "'";
				}
			}

			if (!login_id.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "login_id = '" + login_id + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",login_id = '" + login_id + "'";
				}
			}

			if (!opening_balance.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "opening_balance = '"
							+ opening_balance + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",opening_balance = '"
							+ opening_balance + "'";
				}
			}

			if (!counter_balance.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "counter_balance = '"
							+ counter_balance + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",counter_balance = '"
							+ counter_balance + "'";
				}
			}

			if (!closing_balance.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "closing_balance = '"
							+ closing_balance + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",closing_balance = '"
							+ closing_balance + "'";
				}
			}

			if (!total_amount.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "total_amount = '" + total_amount
							+ "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",total_amount = '" + total_amount
							+ "'";
				}
			}

			if (!logoin_time.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "logoin_time = '" + logoin_time + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",logoin_time = '" + logoin_time
							+ "'";
				}
			}

			if (!logout_time.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "logout_time = '" + logout_time + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",logout_time = '" + logout_time
							+ "'";
				}
			}

			if (!cash1.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash1 = '" + cash1 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash1 = '" + cash1 + "'";
				}
			}

			if (!cash2.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash2 = '" + cash2 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash2 = '" + cash2 + "'";
				}
			}

			if (!cash3.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash3 = '" + cash3 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash3 = '" + cash3 + "'";
				}
			}

			if (!cash4.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash4 = '" + cash4 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash4 = '" + cash4 + "'";
				}
			}

			if (!cash5.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash5 = '" + cash5 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash5 = '" + cash5 + "'";
				}
			}

			if (!cash6.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash6 = '" + cash6 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash6 = '" + cash6 + "'";
				}
			}

			if (!cash7.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash7 = '" + cash7 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash7 = '" + cash7 + "'";
				}
			}

			if (!cash8.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash8 = '" + cash8 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash8 = '" + cash8 + "'";
				}
			}

			if (!cash9.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash9 = '" + cash9 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash9 = '" + cash9 + "'";
				}
			}

			if (!cash10.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "cash10 = '" + cash10 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",cash10 = '" + cash10 + "'";
				}
			}
			String query = "update cashier_report " + updatecc
					+ " where cashier_report_id='" + cashier_report_id + "'";
			System.out.println(query);
			int i = st.executeUpdate(query);
			if (i > 0) {
				flag = true;
			}
		} catch (Exception e) {
			this.seterror(e.toString());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;

	}

	public boolean delete() {
		boolean flag = false;
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query = "delete from cashier_report where cashier_report_id='"
					+ cashier_report_id + "'";
			int i = st.executeUpdate(query);
			if (i > 0) {
				flag = true;
			}
		} catch (Exception e) {
			this.seterror(e.toString());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (st != null) {
					st.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;

	}
}