package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class ewf_entriesService 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String ewf_id="";
	private String sub_head_id="";
	private String entry_date="";
	private String narration="";
	private String enter_by="";
	private String minor_head_id="";
	private String major_head_id="";
	private String head_account_id="";
	private String amount="";
	private String credit_or_debit="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	private String extra6="";
	private String extra7="";
	private String extra8="";
	private String extra9="";
	private String extra10="";
	
	Connection c=null;
	Statement st=null;
	ResultSet rs=null;
	public ewf_entriesService()
	{
		/*try 
		{
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		}catch(Exception e)
		{
			e.printStackTrace();
		}*/
	}
	
	
	
	public String getEwf_id() {
		return (this.ewf_id);
	}
	public void setEwf_id(String ewf_id) {
		this.ewf_id = ewf_id;
	}
	public String getSub_head_id() {
		return (this.sub_head_id);
	}
	public void setSub_head_id(String sub_head_id) {
		this.sub_head_id = sub_head_id;
	}
	public String getEntry_date() {
		return (this.entry_date);
	}
	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}
	public String getNarration() {
		return (this.narration);
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getEnter_by() {
		return (this.enter_by);
	}
	public void setEnter_by(String enter_by) {
		this.enter_by = enter_by;
	}
	public String getMinor_head_id() {
		return (this.minor_head_id);
	}
	public void setMinor_head_id(String minor_head_id) {
		this.minor_head_id = minor_head_id;
	}
	public String getMajor_head_id() {
		return (this.major_head_id);
	}
	public void setMajor_head_id(String major_head_id) {
		this.major_head_id = major_head_id;
	}
	public String getHead_account_id() {
		return (this.head_account_id);
	}
	public void setHead_account_id(String head_account_id) {
		this.head_account_id = head_account_id;
	}
	public String getAmount() {
		return (this.amount);
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCredit_or_debit() {
		return (this.credit_or_debit);
	}
	public void setCredit_or_debit(String credit_or_debit) {
		this.credit_or_debit = credit_or_debit;
	}
	public String getExtra1() {
		return (this.extra1);
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return (this.extra2);
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return (this.extra3);
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return (this.extra4);
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return (this.extra5);
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return (this.extra6);
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return (this.extra7);
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return (this.extra8);
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return (this.extra9);
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return (this.extra10);
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}

	public boolean insert()
	{boolean flag = false;
	try
	{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	rs=st.executeQuery("select * from no_genarator where table_name='ewf_entries'");
	rs.next();
	int ticket=rs.getInt("table_id");
	String PriSt =rs.getString("id_prefix");
	rs.close();
	int ticketm=ticket+1;
	 ewf_id=PriSt+ticketm;
	String query="insert into ewf_entries values('"+ewf_id+"','"+sub_head_id+"','"+entry_date+"','"+narration+"','"+enter_by+"','"+minor_head_id+"','"+major_head_id+"','"+head_account_id+"','"+amount+"','"+credit_or_debit+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"')";
	st.executeUpdate(query);
	st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='ewf_entries'");
	flag = true;
	}catch(Exception e){
	this.seterror(e.toString());
	}finally{
		try {
			if(rs != null){rs.close();}
			if(st != null){st.close();}
			if(c != null){c.close();}
			} catch (SQLException e) {
			e.printStackTrace();
			}
	}
	return flag;
	}
}
