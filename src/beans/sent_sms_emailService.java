package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class sent_sms_emailService 
{
	private String uniqId="";
	 private String regId="";
	 private String name="";
	 private String purpose="";
	 private String daysremainder="";
	 private String email="";
	 private String phone="";
	 private String createdDate="";
	 private String extra1="";
	 private String extra2="";
	 private String extra3="";
	 private String extra4="";
	 private String extra5="";
	 
	 private ResultSet rs = null;
	 private Statement st = null;
	 private Connection c=null;
	 
	 public sent_sms_emailService()
	 {
	 /*try {
	  // Load the database driver
	 c = ConnectionHelper.getConnection();
	 st=c.createStatement();
	 }catch(Exception e){
	 System.out.println("Exception is ;"+e);
	 }*/
	  }

	public String getUniqId() {
		return uniqId;
	}

	public void setUniqId(String uniqId) {
		this.uniqId = uniqId;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getDaysremainder() {
		return daysremainder;
	}

	public void setDaysremainder(String daysremainder) {
		this.daysremainder = daysremainder;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getExtra1() {
		return extra1;
	}

	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}

	public String getExtra2() {
		return extra2;
	}

	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}

	public String getExtra3() {
		return extra3;
	}

	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}

	public String getExtra4() {
		return extra4;
	}

	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}

	public String getExtra5() {
		return extra5;
	}

	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	 
	public boolean insert()
	{
	try
	{c = ConnectionHelper.getConnection();
	st=c.createStatement();
	rs=st.executeQuery("select * from no_genarator where table_name='sent_sms_email'");
	rs.next();
	int ticket=rs.getInt("table_id");
	String PriSt =rs.getString("id_prefix");
	rs.close();
	int ticketm=ticket+1;
	uniqId=PriSt+ticketm;
	String query="insert into sent_sms_email values('"+uniqId+"','"+regId+"','"+name+"','"+purpose+"','"+daysremainder+"','"+email+"','"+phone+"','"+createdDate+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
	System.out.println(query);
	st.executeUpdate(query);
	st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='sent_sms_email'");
	st.close();
	c.close();
	return true;
	}catch(Exception e){
	return false;
	}

	}
}
