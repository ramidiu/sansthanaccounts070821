package beans;
public class invoice 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String inv_id="";
private String headAccountId="";
private String vendorid="";
private String amount="";
private String paid="";
private String pendingamount="";
private String type="";
private String createdBy="";
private String editedBy="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String emp_id="";
public invoice() {} 
public invoice(String inv_id,String headAccountId,String vendorid,String amount,String paid,String pendingamount,String type,String createdBy,String editedBy,String extra1,String extra2,String extra3,String extra4,String extra5,String emp_id)
 {
this.inv_id=inv_id;
this.headAccountId=headAccountId;
this.vendorid=vendorid;
this.amount=amount;
this.paid=paid;
this.pendingamount=pendingamount;
this.type=type;
this.createdBy=createdBy;
this.editedBy=editedBy;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.emp_id=emp_id;

} 
public String getinv_id() {
return inv_id;
}
public void setinv_id(String inv_id) {
this.inv_id = inv_id;
}
public String getheadAccountId() {
return headAccountId;
}
public void setheadAccountId(String headAccountId) {
this.headAccountId = headAccountId;
}
public String getvendorid() {
return vendorid;
}
public void setvendorid(String vendorid) {
this.vendorid = vendorid;
}
public String getamount() {
return amount;
}
public void setamount(String amount) {
this.amount = amount;
}
public String getpaid() {
return paid;
}
public void setpaid(String paid) {
this.paid = paid;
}
public String getpendingamount() {
return pendingamount;
}
public void setpendingamount(String pendingamount) {
this.pendingamount = pendingamount;
}
public String gettype() {
return type;
}
public void settype(String type) {
this.type = type;
}
public String getcreatedBy() {
return createdBy;
}
public void setcreatedBy(String createdBy) {
this.createdBy = createdBy;
}
public String geteditedBy() {
return editedBy;
}
public void seteditedBy(String editedBy) {
this.editedBy = editedBy;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}

}