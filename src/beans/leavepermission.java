package beans;
public class leavepermission 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String leavper_id="";
private String leave_date="";
private String leave_type="";
private String leave_askeddate="";
private String leave_status="";
private String employee_id="";
private String extra1="";
private String extra2="";
public leavepermission() {} 
public leavepermission(String leavper_id,String leave_date,String leave_type,String leave_askeddate,String leave_status,String employee_id,String extra1,String extra2)
 {
this.leavper_id=leavper_id;
this.leave_date=leave_date;
this.leave_type=leave_type;
this.leave_askeddate=leave_askeddate;
this.leave_status=leave_status;
this.employee_id=employee_id;
this.extra1=extra1;
this.extra2=extra2;

} 
public String getleavper_id() {
return leavper_id;
}
public void setleavper_id(String leavper_id) {
this.leavper_id = leavper_id;
}
public String getleave_date() {
return leave_date;
}
public void setleave_date(String leave_date) {
this.leave_date = leave_date;
}
public String getleave_type() {
return leave_type;
}
public void setleave_type(String leave_type) {
this.leave_type = leave_type;
}
public String getleave_askeddate() {
return leave_askeddate;
}
public void setleave_askeddate(String leave_askeddate) {
this.leave_askeddate = leave_askeddate;
}
public String getleave_status() {
return leave_status;
}
public void setleave_status(String leave_status) {
this.leave_status = leave_status;
}
public String getemployee_id() {
return employee_id;
}
public void setemployee_id(String employee_id) {
this.employee_id = employee_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}

}