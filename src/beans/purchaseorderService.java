package beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class purchaseorderService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String po_id="";
private String created_date="";
private String description="";
private String status="";
private String indentinvoice_id="";
private String requiremrnt="";
private String required_date="";
private String head_account_id="";
private String product_id="";
private String tablet_id="";
private String purchase_price="";
private String quantity="";
private String vendor_id="";
private String emp_id="";
private String poinv_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public purchaseorderService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setpo_id(String po_id)
{
this.po_id = po_id;
}

public String getpo_id()
{
return (this.po_id);
}


public void setcreated_date(String created_date)
{
this.created_date = created_date;
}

public String getcreated_date()
{
return (this.created_date);
}


public void setdescription(String description)
{
this.description = description;
}

public String getdescription()
{
return (this.description);
}


public void setstatus(String status)
{
this.status = status;
}

public String getstatus()
{
return (this.status);
}


public void setindentinvoice_id(String indentinvoice_id)
{
this.indentinvoice_id = indentinvoice_id;
}

public String getindentinvoice_id()
{
return (this.indentinvoice_id);
}


public void setrequiremrnt(String requiremrnt)
{
this.requiremrnt = requiremrnt;
}

public String getrequiremrnt()
{
return (this.requiremrnt);
}


public void setrequired_date(String required_date)
{
this.required_date = required_date;
}

public String getrequired_date()
{
return (this.required_date);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setproduct_id(String product_id)
{
this.product_id = product_id;
}

public String getproduct_id()
{
return (this.product_id);
}


public void settablet_id(String tablet_id)
{
this.tablet_id = tablet_id;
}

public String gettablet_id()
{
return (this.tablet_id);
}


public void setpurchase_price(String purchase_price)
{
this.purchase_price = purchase_price;
}

public String getpurchase_price()
{
return (this.purchase_price);
}


public void setquantity(String quantity)
{
this.quantity = quantity;
}

public String getquantity()
{
return (this.quantity);
}


public void setvendor_id(String vendor_id)
{
this.vendor_id = vendor_id;
}

public String getvendor_id()
{
return (this.vendor_id);
}


public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}


public void setpoinv_id(String poinv_id)
{
this.poinv_id = poinv_id;
}

public String getpoinv_id()
{
return (this.poinv_id);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='purchaseorder'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
po_id=PriSt+ticketm;
String query="insert into purchaseorder values('"+po_id+"','"+created_date+"','"+description+"','"+status+"','"+indentinvoice_id+"','"+requiremrnt+"',now(),'"+head_account_id+"','"+product_id+"','"+tablet_id+"','"+purchase_price+"','"+quantity+"','"+vendor_id+"','"+emp_id+"','"+poinv_id+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
System.out.println("pos order====================>"+query);
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='purchaseorder'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}

public int insertPurchaseOrder(purchaseorder po)	{
	PreparedStatement ps = null;
	String insertQuery = "insert into purchaseorder values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	String updateQuery = "update no_genarator set table_id = ? where table_name='purchaseorder'";
	int result = 0;
	try	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
		rs=st.executeQuery("select * from no_genarator where table_name='purchaseorder'");
		rs.next();
		int ticket=rs.getInt("table_id");
		String PriSt =rs.getString("id_prefix");
		rs.close();
		int ticketm=ticket+1;
		po_id=PriSt+ticketm;
		
		ps = c.prepareStatement(insertQuery);
		ps.setString(1,po_id);
		ps.setString(2,po.getcreated_date());
		ps.setString(3,po.getdescription());
		ps.setString(4,po.getstatus() );
		ps.setString(5,po.getindentinvoice_id() );
		ps.setString(6,po.getrequiremrnt() );
		ps.setString(7,po.getrequired_date());
		ps.setString(8,po.gethead_account_id());
		ps.setString(9,po.getproduct_id());
		ps.setString(10,po.gettablet_id());
		ps.setString(11,po.getpurchase_price());
		ps.setString(12,po.getquantity());
		ps.setString(13,po.getvendor_id());
		ps.setString(14,po.getemp_id());
		ps.setString(15,po.getpoinv_id());
		ps.setString(16,po.getextra1());
		ps.setString(17,po.getextra2());
		ps.setString(18,po.getextra3());
		ps.setString(19,po.getextra4());
		ps.setString(20,po.getextra5());
		
		result = ps.executeUpdate();
		ps.close();
		
		ps = c.prepareStatement(updateQuery);
		ps.setInt(1,ticketm);
		ps.executeUpdate();
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	finally	{
		try {
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	return result;
}


public boolean update()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();

String updatecc="set ";
 int jk=0; 
if(!created_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"created_date = '"+created_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",created_date = '"+created_date+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"status = '"+status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",status = '"+status+"'";}
}

if(!indentinvoice_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"indentinvoice_id = '"+indentinvoice_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",indentinvoice_id = '"+indentinvoice_id+"'";}
}

if(!requiremrnt.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"requiremrnt = '"+requiremrnt+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",requiremrnt = '"+requiremrnt+"'";}
}

if(!required_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"required_date = '"+required_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",required_date = '"+required_date+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

if(!product_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"product_id = '"+product_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",product_id = '"+product_id+"'";}
}

if(!tablet_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"tablet_id = '"+tablet_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",tablet_id = '"+tablet_id+"'";}
}

if(!purchase_price.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"purchase_price = '"+purchase_price+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",purchase_price = '"+purchase_price+"'";}
}

if(!quantity.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"quantity = '"+quantity+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",quantity = '"+quantity+"'";}
}

if(!vendor_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendor_id = '"+vendor_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendor_id = '"+vendor_id+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!poinv_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"poinv_id = '"+poinv_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",poinv_id = '"+poinv_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update purchaseorder "+updatecc+" where po_id='"+po_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from purchaseorder where po_id='"+po_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}