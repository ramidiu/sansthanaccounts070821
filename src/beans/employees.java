package beans;
public class employees 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String emp_id="";
private String firstname="";
private String email="";
private String user_name="";
private String password="";
private String lastname="";
private String join_date="";
private String emp_sal="";
private String emp_address="";
private String emp_phone="";
private String headAccountId="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";
public employees() {} 
public employees(String emp_id,String firstname,String email,String user_name,String password,String lastname,String join_date,String emp_sal,String emp_address,String emp_phone,String headAccountId,String extra1,String extra2,String extra3,String extra4,String extra5,String extra6,String extra7,String extra8,String extra9,String extra10)
 {
this.emp_id=emp_id;
this.firstname=firstname;
this.email=email;
this.user_name=user_name;
this.password=password;
this.lastname=lastname;
this.join_date=join_date;
this.emp_sal=emp_sal;
this.emp_address=emp_address;
this.emp_phone=emp_phone;
this.headAccountId=headAccountId;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;
this.extra10=extra10;

} 
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String getfirstname() {
return firstname;
}
public void setfirstname(String firstname) {
this.firstname = firstname;
}
public String getemail() {
return email;
}
public void setemail(String email) {
this.email = email;
}
public String getuser_name() {
return user_name;
}
public void setuser_name(String user_name) {
this.user_name = user_name;
}
public String getpassword() {
return password;
}
public void setpassword(String password) {
this.password = password;
}
public String getlastname() {
return lastname;
}
public void setlastname(String lastname) {
this.lastname = lastname;
}
public String getjoin_date() {
return join_date;
}
public void setjoin_date(String join_date) {
this.join_date = join_date;
}
public String getemp_sal() {
return emp_sal;
}
public void setemp_sal(String emp_sal) {
this.emp_sal = emp_sal;
}
public String getemp_address() {
return emp_address;
}
public void setemp_address(String emp_address) {
this.emp_address = emp_address;
}
public String getemp_phone() {
return emp_phone;
}
public void setemp_phone(String emp_phone) {
this.emp_phone = emp_phone;
}
public String getheadAccountId() {
return headAccountId;
}
public void setheadAccountId(String headAccountId) {
this.headAccountId = headAccountId;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}
public String getextra8() {
return extra8;
}
public void setextra8(String extra8) {
this.extra8 = extra8;
}
public String getextra9() {
return extra9;
}
public void setextra9(String extra9) {
this.extra9 = extra9;
}
public String getextra10() {
return extra10;
}
public void setextra10(String extra10) {
this.extra10 = extra10;
}

}