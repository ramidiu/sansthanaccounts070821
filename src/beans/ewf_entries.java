package beans;

public class ewf_entries 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String ewf_id="";
	private String sub_head_id="";
	private String entry_date="";
	private String narration="";
	private String enter_by="";
	private String minor_head_id="";
	private String major_head_id="";
	private String head_account_id="";
	private String amount="";
	private String credit_or_debit="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	private String extra6="";
	private String extra7="";
	private String extra8="";
	private String extra9="";
	private String extra10="";
	
	public ewf_entries() {}
	
	public ewf_entries(String ewf_id, String sub_head_id, String entry_date,
			String narration, String enter_by, String minor_head_id,
			String major_head_id, String head_account_id, String amount,
			String credit_or_debit, String extra1, String extra2,
			String extra3, String extra4, String extra5, String extra6,
			String extra7, String extra8, String extra9, String extra10) {
		super();
		this.ewf_id = ewf_id;
		this.sub_head_id = sub_head_id;
		this.entry_date = entry_date;
		this.narration = narration;
		this.enter_by = enter_by;
		this.minor_head_id = minor_head_id;
		this.major_head_id = major_head_id;
		this.head_account_id = head_account_id;
		this.amount = amount;
		this.credit_or_debit = credit_or_debit;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
		this.extra6 = extra6;
		this.extra7 = extra7;
		this.extra8 = extra8;
		this.extra9 = extra9;
		this.extra10 = extra10;
	}
	public String getEwf_id() {
		return ewf_id;
	}
	public void setEwf_id(String ewf_id) {
		this.ewf_id = ewf_id;
	}
	public String getSub_head_id() {
		return sub_head_id;
	}
	public void setSub_head_id(String sub_head_id) {
		this.sub_head_id = sub_head_id;
	}
	public String getEntry_date() {
		return entry_date;
	}
	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getEnter_by() {
		return enter_by;
	}
	public void setEnter_by(String enter_by) {
		this.enter_by = enter_by;
	}
	public String getMinor_head_id() {
		return minor_head_id;
	}
	public void setMinor_head_id(String minor_head_id) {
		this.minor_head_id = minor_head_id;
	}
	public String getMajor_head_id() {
		return major_head_id;
	}
	public void setMajor_head_id(String major_head_id) {
		this.major_head_id = major_head_id;
	}
	public String getHead_account_id() {
		return head_account_id;
	}
	public void setHead_account_id(String head_account_id) {
		this.head_account_id = head_account_id;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCredit_or_debit() {
		return credit_or_debit;
	}
	public void setCredit_or_debit(String credit_or_debit) {
		this.credit_or_debit = credit_or_debit;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	} 
}
