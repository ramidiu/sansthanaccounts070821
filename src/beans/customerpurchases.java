package beans;
public class customerpurchases
{
    private String error;
    private String purchaseId;
    private String date;
    private String productId;
    private String quantity;
    private String rate;
    private String totalAmmount;
    private String billingId;
    private String vocharNumber;
    private String cash_type;
    private String extra1;
    private String extra2;
    private String extra3;
    private String extra4;
    private String extra5;
    private String customername;
    private String phoneno;
    private String emp_id;
    private String gothram;
    private String image;
    private String extra6;
    private String extra7;
    private String extra8;
    private String extra9;
    private String extra10;
    private String offerkind_refid;
    private String tendered_cash;
    private String cheque_no;
    private String name_on_cheque;
    private String cheque_narration;
    private String extra11;
    private String extra12;
    private String extra13;
    private String extra14;
    private String extra15;
    private String othercash_deposit_status;
    private String othercash_totalamount;
    private String extra16;
    private String extra17;
    private String extra18;
    private String extra19;
    private String extra20;
    private String sub_head_id;
    private String sub_head_name;
    private String depositeddate;
    private String jvtype;
    private String extra21;
    private String extra22;
    private String extra23;
    private String extra24;
    private String extra25;
    private String extra26;
    private String extra27;
    private String extra28;
    private String extra29;
    private String extra30;
    private String majorHeadId;
    private String minorHeadId;
    private String majorHeadName;
    private String minorHeadName;
    
    public void seterror(final String error) {
        this.error = error;
    }
    
    public String geterror() {
        return this.error;
    }
    
    public String getDepositeddate() {
        return this.depositeddate;
    }
    
    public void setDepositeddate(final String depositeddate) {
        this.depositeddate = depositeddate;
    }
    
    public String getJvtype() {
        return this.jvtype;
    }
    
    public void setJvtype(final String jvtype) {
        this.jvtype = jvtype;
    }
    
    public String getExtra21() {
        return this.extra21;
    }
    
    public void setExtra21(final String extra21) {
        this.extra21 = extra21;
    }
    
    public String getExtra22() {
        return this.extra22;
    }
    
    public void setExtra22(final String extra22) {
        this.extra22 = extra22;
    }
    
    public String getExtra23() {
        return this.extra23;
    }
    
    public void setExtra23(final String extra23) {
        this.extra23 = extra23;
    }
    
    public String getExtra24() {
        return this.extra24;
    }
    
    public void setExtra24(final String extra24) {
        this.extra24 = extra24;
    }
    
    public String getExtra25() {
        return this.extra25;
    }
    
    public void setExtra25(final String extra25) {
        this.extra25 = extra25;
    }
    
    public String getExtra26() {
        return this.extra26;
    }
    
    public void setExtra26(final String extra26) {
        this.extra26 = extra26;
    }
    
    public String getExtra27() {
        return this.extra27;
    }
    
    public void setExtra27(final String extra27) {
        this.extra27 = extra27;
    }
    
    public String getExtra28() {
        return this.extra28;
    }
    
    public void setExtra28(final String extra28) {
        this.extra28 = extra28;
    }
    
    public String getExtra29() {
        return this.extra29;
    }
    
    public void setExtra29(final String extra29) {
        this.extra29 = extra29;
    }
    
    public String getExtra30() {
        return this.extra30;
    }
    
    public void setExtra30(final String extra30) {
        this.extra30 = extra30;
    }
    
    public customerpurchases() {
        this.error = "";
        this.purchaseId = "";
        this.date = "";
        this.productId = "";
        this.quantity = "";
        this.rate = "";
        this.totalAmmount = "";
        this.billingId = "";
        this.vocharNumber = "";
        this.cash_type = "";
        this.extra1 = "";
        this.extra2 = "";
        this.extra3 = "";
        this.extra4 = "";
        this.extra5 = "";
        this.customername = "";
        this.phoneno = "";
        this.emp_id = "";
        this.gothram = "";
        this.image = "";
        this.extra6 = "";
        this.extra7 = "";
        this.extra8 = "";
        this.extra9 = "";
        this.extra10 = "";
        this.offerkind_refid = "";
        this.tendered_cash = "";
        this.cheque_no = "";
        this.name_on_cheque = "";
        this.cheque_narration = "";
        this.extra11 = "";
        this.extra12 = "";
        this.extra13 = "";
        this.extra14 = "";
        this.extra15 = "";
        this.othercash_deposit_status = "";
        this.othercash_totalamount = "";
        this.extra16 = "";
        this.extra17 = "";
        this.extra18 = "";
        this.extra19 = "";
        this.extra20 = "";
        this.sub_head_id = "";
        this.sub_head_name = "";
        this.depositeddate = "";
        this.jvtype = "";
        this.extra21 = "";
        this.extra22 = "";
        this.extra23 = "";
        this.extra24 = "";
        this.extra25 = "";
        this.extra26 = "";
        this.extra27 = "";
        this.extra28 = "";
        this.extra29 = "";
        this.extra30 = "";
        this.majorHeadId = "";
        this.minorHeadId = "";
        this.majorHeadName = "";
        this.minorHeadName = "";
    }
    
    public customerpurchases(final String productId, final String totalAmmount, final String sub_head_id, final String sub_head_name) {
        this.error = "";
        this.purchaseId = "";
        this.date = "";
        this.productId = "";
        this.quantity = "";
        this.rate = "";
        this.totalAmmount = "";
        this.billingId = "";
        this.vocharNumber = "";
        this.cash_type = "";
        this.extra1 = "";
        this.extra2 = "";
        this.extra3 = "";
        this.extra4 = "";
        this.extra5 = "";
        this.customername = "";
        this.phoneno = "";
        this.emp_id = "";
        this.gothram = "";
        this.image = "";
        this.extra6 = "";
        this.extra7 = "";
        this.extra8 = "";
        this.extra9 = "";
        this.extra10 = "";
        this.offerkind_refid = "";
        this.tendered_cash = "";
        this.cheque_no = "";
        this.name_on_cheque = "";
        this.cheque_narration = "";
        this.extra11 = "";
        this.extra12 = "";
        this.extra13 = "";
        this.extra14 = "";
        this.extra15 = "";
        this.othercash_deposit_status = "";
        this.othercash_totalamount = "";
        this.extra16 = "";
        this.extra17 = "";
        this.extra18 = "";
        this.extra19 = "";
        this.extra20 = "";
        this.sub_head_id = "";
        this.sub_head_name = "";
        this.depositeddate = "";
        this.jvtype = "";
        this.extra21 = "";
        this.extra22 = "";
        this.extra23 = "";
        this.extra24 = "";
        this.extra25 = "";
        this.extra26 = "";
        this.extra27 = "";
        this.extra28 = "";
        this.extra29 = "";
        this.extra30 = "";
        this.majorHeadId = "";
        this.minorHeadId = "";
        this.majorHeadName = "";
        this.minorHeadName = "";
        this.productId = productId;
        this.totalAmmount = totalAmmount;
        this.sub_head_id = sub_head_id;
        this.sub_head_name = sub_head_name;
    }
    
    public customerpurchases(final String purchaseId, final String date, final String productId, final String quantity, final String rate, final String totalAmmount, final String billingId, final String vocharNumber, final String cash_type, final String extra1, final String extra2, final String extra3, final String extra4, final String extra5, final String customername, final String phoneno, final String emp_id, final String gothram, final String image, final String extra6, final String extra7, final String extra8, final String extra9, final String extra10, final String offerkind_refid, final String tendered_cash, final String cheque_no, final String name_on_cheque, final String cheque_narration, final String extra11, final String extra12, final String extra13, final String extra14, final String extra15, final String othercash_deposit_status, final String othercash_totalamount, final String extra16, final String extra17, final String extra18, final String extra19, final String extra20) {
        this.error = "";
        this.purchaseId = "";
        this.date = "";
        this.productId = "";
        this.quantity = "";
        this.rate = "";
        this.totalAmmount = "";
        this.billingId = "";
        this.vocharNumber = "";
        this.cash_type = "";
        this.extra1 = "";
        this.extra2 = "";
        this.extra3 = "";
        this.extra4 = "";
        this.extra5 = "";
        this.customername = "";
        this.phoneno = "";
        this.emp_id = "";
        this.gothram = "";
        this.image = "";
        this.extra6 = "";
        this.extra7 = "";
        this.extra8 = "";
        this.extra9 = "";
        this.extra10 = "";
        this.offerkind_refid = "";
        this.tendered_cash = "";
        this.cheque_no = "";
        this.name_on_cheque = "";
        this.cheque_narration = "";
        this.extra11 = "";
        this.extra12 = "";
        this.extra13 = "";
        this.extra14 = "";
        this.extra15 = "";
        this.othercash_deposit_status = "";
        this.othercash_totalamount = "";
        this.extra16 = "";
        this.extra17 = "";
        this.extra18 = "";
        this.extra19 = "";
        this.extra20 = "";
        this.sub_head_id = "";
        this.sub_head_name = "";
        this.depositeddate = "";
        this.jvtype = "";
        this.extra21 = "";
        this.extra22 = "";
        this.extra23 = "";
        this.extra24 = "";
        this.extra25 = "";
        this.extra26 = "";
        this.extra27 = "";
        this.extra28 = "";
        this.extra29 = "";
        this.extra30 = "";
        this.majorHeadId = "";
        this.minorHeadId = "";
        this.majorHeadName = "";
        this.minorHeadName = "";
        this.purchaseId = purchaseId;
        this.date = date;
        this.productId = productId;
        this.quantity = quantity;
        this.rate = rate;
        this.totalAmmount = totalAmmount;
        this.billingId = billingId;
        this.vocharNumber = vocharNumber;
        this.cash_type = cash_type;
        this.extra1 = extra1;
        this.extra2 = extra2;
        this.extra3 = extra3;
        this.extra4 = extra4;
        this.extra5 = extra5;
        this.customername = customername;
        this.phoneno = phoneno;
        this.emp_id = emp_id;
        this.gothram = gothram;
        this.image = image;
        this.extra6 = extra6;
        this.extra7 = extra7;
        this.extra8 = extra8;
        this.extra9 = extra9;
        this.extra10 = extra10;
        this.offerkind_refid = offerkind_refid;
        this.tendered_cash = tendered_cash;
        this.cheque_no = cheque_no;
        this.name_on_cheque = name_on_cheque;
        this.cheque_narration = cheque_narration;
        this.extra11 = extra11;
        this.extra12 = extra12;
        this.extra13 = extra13;
        this.extra14 = extra14;
        this.extra15 = extra15;
        this.othercash_deposit_status = othercash_deposit_status;
        this.othercash_totalamount = othercash_totalamount;
        this.extra16 = extra16;
        this.extra17 = extra17;
        this.extra18 = extra18;
        this.extra19 = extra19;
        this.extra20 = extra20;
    }
    
    public customerpurchases(final String purchaseId, final String date, final String productId, final String quantity, final String rate, final String totalAmmount, final String billingId, final String vocharNumber, final String cash_type, final String extra1, final String extra2, final String extra3, final String extra4, final String extra5, final String customername, final String phoneno, final String emp_id, final String gothram, final String image, final String extra6, final String extra7, final String extra8, final String extra9, final String extra10, final String offerkind_refid, final String tendered_cash, final String cheque_no, final String name_on_cheque, final String cheque_narration, final String extra11, final String extra12, final String extra13, final String extra14, final String extra15, final String othercash_deposit_status, final String othercash_totalamount, final String extra16, final String extra17, final String extra18, final String extra19, final String extra20, final String jvtype) {
        this.error = "";
        this.purchaseId = "";
        this.date = "";
        this.productId = "";
        this.quantity = "";
        this.rate = "";
        this.totalAmmount = "";
        this.billingId = "";
        this.vocharNumber = "";
        this.cash_type = "";
        this.extra1 = "";
        this.extra2 = "";
        this.extra3 = "";
        this.extra4 = "";
        this.extra5 = "";
        this.customername = "";
        this.phoneno = "";
        this.emp_id = "";
        this.gothram = "";
        this.image = "";
        this.extra6 = "";
        this.extra7 = "";
        this.extra8 = "";
        this.extra9 = "";
        this.extra10 = "";
        this.offerkind_refid = "";
        this.tendered_cash = "";
        this.cheque_no = "";
        this.name_on_cheque = "";
        this.cheque_narration = "";
        this.extra11 = "";
        this.extra12 = "";
        this.extra13 = "";
        this.extra14 = "";
        this.extra15 = "";
        this.othercash_deposit_status = "";
        this.othercash_totalamount = "";
        this.extra16 = "";
        this.extra17 = "";
        this.extra18 = "";
        this.extra19 = "";
        this.extra20 = "";
        this.sub_head_id = "";
        this.sub_head_name = "";
        this.depositeddate = "";
        this.jvtype = "";
        this.extra21 = "";
        this.extra22 = "";
        this.extra23 = "";
        this.extra24 = "";
        this.extra25 = "";
        this.extra26 = "";
        this.extra27 = "";
        this.extra28 = "";
        this.extra29 = "";
        this.extra30 = "";
        this.majorHeadId = "";
        this.minorHeadId = "";
        this.majorHeadName = "";
        this.minorHeadName = "";
        this.purchaseId = purchaseId;
        this.date = date;
        this.productId = productId;
        this.quantity = quantity;
        this.rate = rate;
        this.totalAmmount = totalAmmount;
        this.billingId = billingId;
        this.vocharNumber = vocharNumber;
        this.cash_type = cash_type;
        this.extra1 = extra1;
        this.extra2 = extra2;
        this.extra3 = extra3;
        this.extra4 = extra4;
        this.extra5 = extra5;
        this.customername = customername;
        this.phoneno = phoneno;
        this.emp_id = emp_id;
        this.gothram = gothram;
        this.image = image;
        this.extra6 = extra6;
        this.extra7 = extra7;
        this.extra8 = extra8;
        this.extra9 = extra9;
        this.extra10 = extra10;
        this.offerkind_refid = offerkind_refid;
        this.tendered_cash = tendered_cash;
        this.cheque_no = cheque_no;
        this.name_on_cheque = name_on_cheque;
        this.cheque_narration = cheque_narration;
        this.extra11 = extra11;
        this.extra12 = extra12;
        this.extra13 = extra13;
        this.extra14 = extra14;
        this.extra15 = extra15;
        this.othercash_deposit_status = othercash_deposit_status;
        this.othercash_totalamount = othercash_totalamount;
        this.extra16 = extra16;
        this.extra17 = extra17;
        this.extra18 = extra18;
        this.extra19 = extra19;
        this.extra20 = extra20;
        this.jvtype = jvtype;
    }
    
    public customerpurchases(final String purchaseId, final String date, final String productId, final String quantity, final String rate, final String totalAmmount, final String billingId, final String vocharNumber, final String cash_type, final String extra1, final String extra2, final String extra3, final String extra4, final String extra5, final String customername, final String phoneno, final String emp_id, final String gothram, final String image, final String extra6, final String extra7, final String extra8, final String extra9, final String extra10, final String offerkind_refid, final String tendered_cash, final String cheque_no, final String name_on_cheque, final String cheque_narration, final String extra11, final String extra12, final String extra13, final String extra14, final String extra15, final String othercash_deposit_status, final String othercash_totalamount, final String extra16, final String extra17, final String extra18, final String extra19, final String extra20, final String majorHeadId, final String minorHeadId, final String minorHeadName, final String majorHeadName, final String extra29, final String extra30) {
        this.error = "";
        this.purchaseId = "";
        this.date = "";
        this.productId = "";
        this.quantity = "";
        this.rate = "";
        this.totalAmmount = "";
        this.billingId = "";
        this.vocharNumber = "";
        this.cash_type = "";
        this.extra1 = "";
        this.extra2 = "";
        this.extra3 = "";
        this.extra4 = "";
        this.extra5 = "";
        this.customername = "";
        this.phoneno = "";
        this.emp_id = "";
        this.gothram = "";
        this.image = "";
        this.extra6 = "";
        this.extra7 = "";
        this.extra8 = "";
        this.extra9 = "";
        this.extra10 = "";
        this.offerkind_refid = "";
        this.tendered_cash = "";
        this.cheque_no = "";
        this.name_on_cheque = "";
        this.cheque_narration = "";
        this.extra11 = "";
        this.extra12 = "";
        this.extra13 = "";
        this.extra14 = "";
        this.extra15 = "";
        this.othercash_deposit_status = "";
        this.othercash_totalamount = "";
        this.extra16 = "";
        this.extra17 = "";
        this.extra18 = "";
        this.extra19 = "";
        this.extra20 = "";
        this.sub_head_id = "";
        this.sub_head_name = "";
        this.depositeddate = "";
        this.jvtype = "";
        this.extra21 = "";
        this.extra22 = "";
        this.extra23 = "";
        this.extra24 = "";
        this.extra25 = "";
        this.extra26 = "";
        this.extra27 = "";
        this.extra28 = "";
        this.extra29 = "";
        this.extra30 = "";
        this.majorHeadId = "";
        this.minorHeadId = "";
        this.majorHeadName = "";
        this.minorHeadName = "";
        this.purchaseId = purchaseId;
        this.date = date;
        this.productId = productId;
        this.quantity = quantity;
        this.rate = rate;
        this.totalAmmount = totalAmmount;
        this.billingId = billingId;
        this.vocharNumber = vocharNumber;
        this.cash_type = cash_type;
        this.extra1 = extra1;
        this.extra2 = extra2;
        this.extra3 = extra3;
        this.extra4 = extra4;
        this.extra5 = extra5;
        this.customername = customername;
        this.phoneno = phoneno;
        this.emp_id = emp_id;
        this.gothram = gothram;
        this.image = image;
        this.extra6 = extra6;
        this.extra7 = extra7;
        this.extra8 = extra8;
        this.extra9 = extra9;
        this.extra10 = extra10;
        this.offerkind_refid = offerkind_refid;
        this.tendered_cash = tendered_cash;
        this.cheque_no = cheque_no;
        this.name_on_cheque = name_on_cheque;
        this.cheque_narration = cheque_narration;
        this.extra11 = extra11;
        this.extra12 = extra12;
        this.extra13 = extra13;
        this.extra14 = extra14;
        this.extra15 = extra15;
        this.othercash_deposit_status = othercash_deposit_status;
        this.othercash_totalamount = othercash_totalamount;
        this.extra16 = extra16;
        this.extra17 = extra17;
        this.extra18 = extra18;
        this.extra19 = extra19;
        this.extra20 = extra20;
        this.majorHeadId = majorHeadId;
        this.minorHeadId = minorHeadId;
        this.majorHeadName = majorHeadName;
        this.minorHeadName = minorHeadName;
        this.extra29 = extra29;
        this.extra30 = extra30;
    }
    
    public customerpurchases(final String purchaseId, final String date, final String productId, final String quantity, final String rate, final String totalAmmount, final String billingId, final String vocharNumber, final String cash_type, final String extra1, final String extra2, final String extra3, final String extra4, final String extra5, final String customername, final String phoneno, final String emp_id, final String gothram, final String image, final String extra6, final String extra7, final String extra8, final String extra9, final String extra10, final String offerkind_refid, final String tendered_cash, final String cheque_no, final String name_on_cheque, final String cheque_narration, final String extra11, final String extra12, final String extra13, final String extra14, final String extra15, final String othercash_deposit_status, final String othercash_totalamount, final String extra16, final String extra17, final String extra18, final String extra19, final String extra20, final String extra29, final String extra30) {
        this.error = "";
        this.purchaseId = "";
        this.date = "";
        this.productId = "";
        this.quantity = "";
        this.rate = "";
        this.totalAmmount = "";
        this.billingId = "";
        this.vocharNumber = "";
        this.cash_type = "";
        this.extra1 = "";
        this.extra2 = "";
        this.extra3 = "";
        this.extra4 = "";
        this.extra5 = "";
        this.customername = "";
        this.phoneno = "";
        this.emp_id = "";
        this.gothram = "";
        this.image = "";
        this.extra6 = "";
        this.extra7 = "";
        this.extra8 = "";
        this.extra9 = "";
        this.extra10 = "";
        this.offerkind_refid = "";
        this.tendered_cash = "";
        this.cheque_no = "";
        this.name_on_cheque = "";
        this.cheque_narration = "";
        this.extra11 = "";
        this.extra12 = "";
        this.extra13 = "";
        this.extra14 = "";
        this.extra15 = "";
        this.othercash_deposit_status = "";
        this.othercash_totalamount = "";
        this.extra16 = "";
        this.extra17 = "";
        this.extra18 = "";
        this.extra19 = "";
        this.extra20 = "";
        this.sub_head_id = "";
        this.sub_head_name = "";
        this.depositeddate = "";
        this.jvtype = "";
        this.extra21 = "";
        this.extra22 = "";
        this.extra23 = "";
        this.extra24 = "";
        this.extra25 = "";
        this.extra26 = "";
        this.extra27 = "";
        this.extra28 = "";
        this.extra29 = "";
        this.extra30 = "";
        this.majorHeadId = "";
        this.minorHeadId = "";
        this.majorHeadName = "";
        this.minorHeadName = "";
        this.purchaseId = purchaseId;
        this.date = date;
        this.productId = productId;
        this.quantity = quantity;
        this.rate = rate;
        this.totalAmmount = totalAmmount;
        this.billingId = billingId;
        this.vocharNumber = vocharNumber;
        this.cash_type = cash_type;
        this.extra1 = extra1;
        this.extra2 = extra2;
        this.extra3 = extra3;
        this.extra4 = extra4;
        this.extra5 = extra5;
        this.customername = customername;
        this.phoneno = phoneno;
        this.emp_id = emp_id;
        this.gothram = gothram;
        this.image = image;
        this.extra6 = extra6;
        this.extra7 = extra7;
        this.extra8 = extra8;
        this.extra9 = extra9;
        this.extra10 = extra10;
        this.offerkind_refid = offerkind_refid;
        this.tendered_cash = tendered_cash;
        this.cheque_no = cheque_no;
        this.name_on_cheque = name_on_cheque;
        this.cheque_narration = cheque_narration;
        this.extra11 = extra11;
        this.extra12 = extra12;
        this.extra13 = extra13;
        this.extra14 = extra14;
        this.extra15 = extra15;
        this.othercash_deposit_status = othercash_deposit_status;
        this.othercash_totalamount = othercash_totalamount;
        this.extra16 = extra16;
        this.extra17 = extra17;
        this.extra18 = extra18;
        this.extra19 = extra19;
        this.extra20 = extra20;
        this.extra29 = extra29;
        this.extra30 = extra30;
    }
    
    public String getpurchaseId() {
        return this.purchaseId;
    }
    
    public void setpurchaseId(final String purchaseId) {
        this.purchaseId = purchaseId;
    }
    
    public String getdate() {
        return this.date;
    }
    
    public void setdate(final String date) {
        this.date = date;
    }
    
    public String getproductId() {
        return this.productId;
    }
    
    public void setproductId(final String productId) {
        this.productId = productId;
    }
    
    public String getquantity() {
        return this.quantity;
    }
    
    public void setquantity(final String quantity) {
        this.quantity = quantity;
    }
    
    public String getrate() {
        return this.rate;
    }
    
    public void setrate(final String rate) {
        this.rate = rate;
    }
    
    public String gettotalAmmount() {
        return this.totalAmmount;
    }
    
    public void settotalAmmount(final String totalAmmount) {
        this.totalAmmount = totalAmmount;
    }
    
    public String getbillingId() {
        return this.billingId;
    }
    
    public void setbillingId(final String billingId) {
        this.billingId = billingId;
    }
    
    public String getvocharNumber() {
        return this.vocharNumber;
    }
    
    public void setvocharNumber(final String vocharNumber) {
        this.vocharNumber = vocharNumber;
    }
    
    public String getcash_type() {
        return this.cash_type;
    }
    
    public void setcash_type(final String cash_type) {
        this.cash_type = cash_type;
    }
    
    public String getextra1() {
        return this.extra1;
    }
    
    public void setextra1(final String extra1) {
        this.extra1 = extra1;
    }
    
    public String getextra2() {
        return this.extra2;
    }
    
    public void setextra2(final String extra2) {
        this.extra2 = extra2;
    }
    
    public String getextra3() {
        return this.extra3;
    }
    
    public void setextra3(final String extra3) {
        this.extra3 = extra3;
    }
    
    public String getextra4() {
        return this.extra4;
    }
    
    public void setextra4(final String extra4) {
        this.extra4 = extra4;
    }
    
    public String getextra5() {
        return this.extra5;
    }
    
    public void setextra5(final String extra5) {
        this.extra5 = extra5;
    }
    
    public String getcustomername() {
        return this.customername;
    }
    
    public void setcustomername(final String customername) {
        this.customername = customername;
    }
    
    public String getphoneno() {
        return this.phoneno;
    }
    
    public void setphoneno(final String phoneno) {
        this.phoneno = phoneno;
    }
    
    public String getemp_id() {
        return this.emp_id;
    }
    
    public void setemp_id(final String emp_id) {
        this.emp_id = emp_id;
    }
    
    public String getgothram() {
        return this.gothram;
    }
    
    public void setgothram(final String gothram) {
        this.gothram = gothram;
    }
    
    public String getimage() {
        return this.image;
    }
    
    public void setimage(final String image) {
        this.image = image;
    }
    
    public String getextra6() {
        return this.extra6;
    }
    
    public void setextra6(final String extra6) {
        this.extra6 = extra6;
    }
    
    public String getextra7() {
        return this.extra7;
    }
    
    public void setextra7(final String extra7) {
        this.extra7 = extra7;
    }
    
    public String getextra8() {
        return this.extra8;
    }
    
    public void setextra8(final String extra8) {
        this.extra8 = extra8;
    }
    
    public String getextra9() {
        return this.extra9;
    }
    
    public void setextra9(final String extra9) {
        this.extra9 = extra9;
    }
    
    public String getextra10() {
        return this.extra10;
    }
    
    public void setextra10(final String extra10) {
        this.extra10 = extra10;
    }
    
    public String getofferkind_refid() {
        return this.offerkind_refid;
    }
    
    public void setofferkind_refid(final String offerkind_refid) {
        this.offerkind_refid = offerkind_refid;
    }
    
    public String gettendered_cash() {
        return this.tendered_cash;
    }
    
    public void settendered_cash(final String tendered_cash) {
        this.tendered_cash = tendered_cash;
    }
    
    public String getcheque_no() {
        return this.cheque_no;
    }
    
    public void setcheque_no(final String cheque_no) {
        this.cheque_no = cheque_no;
    }
    
    public String getname_on_cheque() {
        return this.name_on_cheque;
    }
    
    public void setname_on_cheque(final String name_on_cheque) {
        this.name_on_cheque = name_on_cheque;
    }
    
    public String getcheque_narration() {
        return this.cheque_narration;
    }
    
    public void setcheque_narration(final String cheque_narration) {
        this.cheque_narration = cheque_narration;
    }
    
    public String getextra11() {
        return this.extra11;
    }
    
    public void setextra11(final String extra11) {
        this.extra11 = extra11;
    }
    
    public String getextra12() {
        return this.extra12;
    }
    
    public void setextra12(final String extra12) {
        this.extra12 = extra12;
    }
    
    public String getextra13() {
        return this.extra13;
    }
    
    public void setextra13(final String extra13) {
        this.extra13 = extra13;
    }
    
    public String getextra14() {
        return this.extra14;
    }
    
    public void setextra14(final String extra14) {
        this.extra14 = extra14;
    }
    
    public String getextra15() {
        return this.extra15;
    }
    
    public void setextra15(final String extra15) {
        this.extra15 = extra15;
    }
    
    public String getOthercash_deposit_status() {
        return this.othercash_deposit_status;
    }
    
    public void setOthercash_deposit_status(final String othercash_deposit_status) {
        this.othercash_deposit_status = othercash_deposit_status;
    }
    
    public String getOthercash_totalamount() {
        return this.othercash_totalamount;
    }
    
    public void setOthercash_totalamount(final String othercash_totalamount) {
        this.othercash_totalamount = othercash_totalamount;
    }
    
    public String getExtra16() {
        return this.extra16;
    }
    
    public void setExtra16(final String extra16) {
        this.extra16 = extra16;
    }
    
    public String getExtra17() {
        return this.extra17;
    }
    
    public void setExtra17(final String extra17) {
        this.extra17 = extra17;
    }
    
    public String getExtra18() {
        return this.extra18;
    }
    
    public void setExtra18(final String extra18) {
        this.extra18 = extra18;
    }
    
    public String getExtra19() {
        return this.extra19;
    }
    
    public void setExtra19(final String extra19) {
        this.extra19 = extra19;
    }
    
    public String getExtra20() {
        return this.extra20;
    }
    
    public void setExtra20(final String extra20) {
        this.extra20 = extra20;
    }
    
    public String getSub_head_id() {
        return this.sub_head_id;
    }
    
    public void setSub_head_id(final String sub_head_id) {
        this.sub_head_id = sub_head_id;
    }
    
    public String getSub_head_name() {
        return this.sub_head_name;
    }
    
    public void setSub_head_name(final String sub_head_name) {
        this.sub_head_name = sub_head_name;
    }
    
    public String getMajorHeadId() {
        return this.majorHeadId;
    }
    
    public void setMajorHeadId(final String majorHeadId) {
        this.majorHeadId = majorHeadId;
    }
    
    public String getMinorHeadId() {
        return this.minorHeadId;
    }
    
    public void setMinorHeadId(final String minorHeadId) {
        this.minorHeadId = minorHeadId;
    }
    
    public String getMajorHeadName() {
        return this.majorHeadName;
    }
    
    public void setMajorHeadName(final String majorHeadName) {
        this.majorHeadName = majorHeadName;
    }
    
    public String getMinorHeadName() {
        return this.minorHeadName;
    }
    
    public void setMinorHeadName(final String minorHeadName) {
        this.minorHeadName = minorHeadName;
    }
}

