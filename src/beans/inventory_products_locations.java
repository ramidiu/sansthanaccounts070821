package beans;
public class inventory_products_locations 
{
	private String error="";
	public void seterror(String error)
	{
		this.error=error;
	}
	public String geterror()
	{
		return this.error;
	}
	
	private String inventory_id="";
	private String inventory_name="";
	private String product_id="";
	private String quantity="";
	private String location_id="";
	private String created_by="";
	private String created_date="";
	private String updated_by="";
	private String updated_date="";
	private String narration="";
	private String status="";
	private String head_account_id="";
	private String image="";
	private String document="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String rate;
	private String amount;
	private String warranty;
	private String damaged_amount;
	
	
	public inventory_products_locations() {} 
	public inventory_products_locations(String inventory_id,String inventory_name,String product_id,String quantity,String location_id,String created_by,String created_date,String updated_by,String updated_date,String narration,String status,String head_account_id,String image,String document,String extra1,String extra2,String extra3,String extra4)
	{
		this.inventory_id=inventory_id;
		this.inventory_name=inventory_name;
		this.product_id=product_id;
		this.quantity=quantity;
		this.location_id=location_id;
		this.created_by=created_by;
		this.created_date=created_date;
		this.updated_by=updated_by;
		this.updated_date=updated_date;
		this.narration=narration;
		this.status=status;
		this.head_account_id=head_account_id;
		this.image=image;
		this.document=document;
		this.extra1=extra1;
		this.extra2=extra2;
		this.extra3=extra3;
		this.extra4=extra4;
	} 
	
	
	
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getWarranty() {
		return warranty;
	}
	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}
	public String getDamaged_amount() {
		return damaged_amount;
	}
	public void setDamaged_amount(String damaged_amount) {
		this.damaged_amount = damaged_amount;
	}
	public String getinventory_id() 
	{
		return inventory_id;
	}
	public void setinventory_id(String inventory_id) {
		this.inventory_id = inventory_id;
	}
	public String getinventory_name() {
		return inventory_name;
	}
	public void setinventory_name(String inventory_name) {
		this.inventory_name = inventory_name;
	}
	public String getproduct_id() {
		return product_id;
	}
	public void setproduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getquantity() {
		return quantity;
	}
	public void setquantity(String quantity) {
		this.quantity = quantity;
	}
	public String getlocation_id() {
		return location_id;
	}
	public void setlocation_id(String location_id) {
		this.location_id = location_id;
	}
	public String getcreated_by() {
		return created_by;
	}
	public void setcreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getcreated_date() {
		return created_date;
	}
	public void setcreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getupdated_by() {
		return updated_by;
	}
	public void setupdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	public String getupdated_date() {
		return updated_date;
	}
	public void setupdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
	public String getnarration() {
		return narration;
	}
	public void setnarration(String narration) {
		this.narration = narration;
	}
	public String getstatus() {
		return status;
	}
	public void setstatus(String status) {
		this.status = status;
	}
	public String gethead_account_id() {
		return head_account_id;
	}
	public void sethead_account_id(String head_account_id) {
		this.head_account_id = head_account_id;
	}
	public String getimage() {
		return image;
	}
	public void setimage(String image) {
		this.image = image;
	}
	public String getdocument() {
		return document;
	}
	public void setdocument(String document) {
		this.document = document;
	}
	public String getextra1() {
		return extra1;
	}
	public void setextra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getextra2() {
		return extra2;
	}
	public void setextra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getextra3() {
		return extra3;
	}
	public void setextra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getextra4() {
		return extra4;
	}
	public void setextra4(String extra4) {
		this.extra4 = extra4;
	}
}