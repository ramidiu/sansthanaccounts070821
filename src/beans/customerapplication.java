package beans;
public class customerapplication 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String cust_id="";
private String billingId="";
private String title="";
private String first_name="";
private String middle_name="";
private String last_name="";
private String email_id="";
private String phone="";
private String gender="";
private String dateof_birth="";
private String address1="";
private String address2="";
private String pincode="";
private String city="";
private String gothram="";
private String from_date="";
private String to_date="";
private String image="";
private String idproof1="";
private String idproof2="";
private String pancard_no="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
public customerapplication() {} 
public customerapplication(String cust_id,String billingId,String title,String first_name,String middle_name,String last_name,String email_id,String phone,String gender,String dateof_birth,String address1,String address2,String pincode,String city,String gothram,String from_date,String to_date,String image,String idproof1,String idproof2,String pancard_no,String extra1,String extra2,String extra3,String extra4)
 {
this.cust_id=cust_id;
this.billingId=billingId;
this.title=title;
this.first_name=first_name;
this.middle_name=middle_name;
this.last_name=last_name;
this.email_id=email_id;
this.phone=phone;
this.gender=gender;
this.dateof_birth=dateof_birth;
this.address1=address1;
this.address2=address2;
this.pincode=pincode;
this.city=city;
this.gothram=gothram;
this.from_date=from_date;
this.to_date=to_date;
this.image=image;
this.idproof1=idproof1;
this.idproof2=idproof2;
this.pancard_no=pancard_no;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;

} 
public String getcust_id() {
return cust_id;
}
public void setcust_id(String cust_id) {
this.cust_id = cust_id;
}
public String getbillingId() {
return billingId;
}
public void setbillingId(String billingId) {
this.billingId = billingId;
}
public String gettitle() {
return title;
}
public void settitle(String title) {
this.title = title;
}
public String getfirst_name() {
return first_name;
}
public void setfirst_name(String first_name) {
this.first_name = first_name;
}
public String getmiddle_name() {
return middle_name;
}
public void setmiddle_name(String middle_name) {
this.middle_name = middle_name;
}
public String getlast_name() {
return last_name;
}
public void setlast_name(String last_name) {
this.last_name = last_name;
}
public String getemail_id() {
return email_id;
}
public void setemail_id(String email_id) {
this.email_id = email_id;
}
public String getphone() {
return phone;
}
public void setphone(String phone) {
this.phone = phone;
}
public String getgender() {
return gender;
}
public void setgender(String gender) {
this.gender = gender;
}
public String getdateof_birth() {
return dateof_birth;
}
public void setdateof_birth(String dateof_birth) {
this.dateof_birth = dateof_birth;
}
public String getaddress1() {
return address1;
}
public void setaddress1(String address1) {
this.address1 = address1;
}
public String getaddress2() {
return address2;
}
public void setaddress2(String address2) {
this.address2 = address2;
}
public String getpincode() {
return pincode;
}
public void setpincode(String pincode) {
this.pincode = pincode;
}
public String getcity() {
return city;
}
public void setcity(String city) {
this.city = city;
}
public String getgothram() {
return gothram;
}
public void setgothram(String gothram) {
this.gothram = gothram;
}
public String getfrom_date() {
return from_date;
}
public void setfrom_date(String from_date) {
this.from_date = from_date;
}
public String getto_date() {
return to_date;
}
public void setto_date(String to_date) {
this.to_date = to_date;
}
public String getimage() {
return image;
}
public void setimage(String image) {
this.image = image;
}
public String getidproof1() {
return idproof1;
}
public void setidproof1(String idproof1) {
this.idproof1 = idproof1;
}
public String getidproof2() {
return idproof2;
}
public void setidproof2(String idproof2) {
this.idproof2 = idproof2;
}
public String getpancard_no() {
return pancard_no;
}
public void setpancard_no(String pancard_no) {
this.pancard_no = pancard_no;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}

}