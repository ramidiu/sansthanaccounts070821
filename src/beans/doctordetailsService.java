package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class doctordetailsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String doctorId="";
private String doctorName="";
private String doctorQualification="";
private String phoneNo="";
private String address="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public doctordetailsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setdoctorId(String doctorId)
{
this.doctorId = doctorId;
}

public String getdoctorId()
{
return (this.doctorId);
}


public void setdoctorName(String doctorName)
{
this.doctorName = doctorName;
}

public String getdoctorName()
{
return (this.doctorName);
}


public void setdoctorQualification(String doctorQualification)
{
this.doctorQualification = doctorQualification;
}

public String getdoctorQualification()
{
return (this.doctorQualification);
}


public void setphoneNo(String phoneNo)
{
this.phoneNo = phoneNo;
}

public String getphoneNo()
{
return (this.phoneNo);
}


public void setaddress(String address)
{
this.address = address;
}

public String getaddress()
{
return (this.address);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='doctordetails'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 doctorId=PriSt+ticketm;
String query="insert into doctordetails values('"+doctorId+"','"+doctorName+"','"+doctorQualification+"','"+phoneNo+"','"+address+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
/*System.out.println(query);*/
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='doctordetails'");
flag = true;
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!doctorName.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"doctorName = '"+doctorName+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",doctorName = '"+doctorName+"'";}
}

if(!doctorQualification.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"doctorQualification = '"+doctorQualification+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",doctorQualification = '"+doctorQualification+"'";}
}

if(!phoneNo.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"phoneNo = '"+phoneNo+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",phoneNo = '"+phoneNo+"'";}
}

if(!address.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"address = '"+address+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",address = '"+address+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update doctordetails "+updatecc+" where doctorId='"+doctorId+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from doctordetails where doctorId='"+doctorId+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}