package beans;
public class staff_designation 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String stdes_id="";
private String designation_name="";
private String extra1="";
private String extra2="";
public staff_designation() {} 
public staff_designation(String stdes_id,String designation_name,String extra1,String extra2)
 {
this.stdes_id=stdes_id;
this.designation_name=designation_name;
this.extra1=extra1;
this.extra2=extra2;

} 
public String getstdes_id() {
return stdes_id;
}
public void setstdes_id(String stdes_id) {
this.stdes_id = stdes_id;
}
public String getdesignation_name() {
return designation_name;
}
public void setdesignation_name(String designation_name) {
this.designation_name = designation_name;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}

}