package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import dbase.sqlcon.ConnectionHelper;

public class customerapplicationService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String cust_id="";
private String billingId="";
private String title="";
private String first_name="";
private String middle_name="";
private String last_name="";
private String email_id="";
private String phone="";
private String gender="";
private String dateof_birth="";
private String address1="";
private String address2="";
private String pincode="";
private String city="";
private String gothram="";
private String from_date="";
private String to_date="";
private String image="";
private String idproof1="";
private String idproof2="";
private String pancard_no="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public customerapplicationService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setcust_id(String cust_id)
{
this.cust_id = cust_id;
}

public String getcust_id()
{
return (this.cust_id);
}


public void setbillingId(String billingId)
{
this.billingId = billingId;
}

public String getbillingId()
{
return (this.billingId);
}


public void settitle(String title)
{
this.title = title;
}

public String gettitle()
{
return (this.title);
}


public void setfirst_name(String first_name)
{
this.first_name = first_name;
}

public String getfirst_name()
{
return (this.first_name);
}


public void setmiddle_name(String middle_name)
{
this.middle_name = middle_name;
}

public String getmiddle_name()
{
return (this.middle_name);
}


public void setlast_name(String last_name)
{
this.last_name = last_name;
}

public String getlast_name()
{
return (this.last_name);
}


public void setemail_id(String email_id)
{
this.email_id = email_id;
}

public String getemail_id()
{
return (this.email_id);
}


public void setphone(String phone)
{
this.phone = phone;
}

public String getphone()
{
return (this.phone);
}


public void setgender(String gender)
{
this.gender = gender;
}

public String getgender()
{
return (this.gender);
}


public void setdateof_birth(String dateof_birth)
{
this.dateof_birth = dateof_birth;
}

public String getdateof_birth()
{
return (this.dateof_birth);
}


public void setaddress1(String address1)
{
this.address1 = address1;
}

public String getaddress1()
{
return (this.address1);
}


public void setaddress2(String address2)
{
this.address2 = address2;
}

public String getaddress2()
{
return (this.address2);
}


public void setpincode(String pincode)
{
this.pincode = pincode;
}

public String getpincode()
{
return (this.pincode);
}


public void setcity(String city)
{
this.city = city;
}

public String getcity()
{
return (this.city);
}


public void setgothram(String gothram)
{
this.gothram = gothram;
}

public String getgothram()
{
return (this.gothram);
}


public void setfrom_date(String from_date)
{
this.from_date = from_date;
}

public String getfrom_date()
{
return (this.from_date);
}


public void setto_date(String to_date)
{
this.to_date = to_date;
}

public String getto_date()
{
return (this.to_date);
}


public void setimage(String image)
{
this.image = image;
}

public String getimage()
{
return (this.image);
}


public void setidproof1(String idproof1)
{
this.idproof1 = idproof1;
}

public String getidproof1()
{
return (this.idproof1);
}


public void setidproof2(String idproof2)
{
this.idproof2 = idproof2;
}

public String getidproof2()
{
return (this.idproof2);
}


public void setpancard_no(String pancard_no)
{
this.pancard_no = pancard_no;
}

public String getpancard_no()
{
return (this.pancard_no);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}

public boolean insert()
{boolean flag = false;
try
{
System.out.println("customerAPSERVICEmethod start--"+Calendar.getInstance().getTime());
c = ConnectionHelper.getConnection();
System.out.println("after getting connection-----"+Calendar.getInstance().getTime());
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='customerapplication'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 cust_id=PriSt+ticketm;
String query="insert into customerapplication values('"+cust_id+"','"+billingId+"','"+title+"','"+first_name+"','"+middle_name+"','"+last_name+"','"+email_id+"','"+phone+"','"+gender+"','"+dateof_birth+"','"+address1+"','"+address2+"','"+pincode+"','"+city+"','"+gothram+"','"+from_date+"','"+to_date+"','"+image+"','"+idproof1+"','"+idproof2+"','"+pancard_no+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"')";
System.out.println(query);
System.out.println("beforExecuting---------"+Calendar.getInstance().getTime());
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='customerapplication'");
System.out.println("request coming to customerapplicationservice------"+Calendar.getInstance().getTime());
flag = true;
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
/*if(!billingId.equals("")){
if(jk==0)
{
updatecc=updatecc+"billingId = '"+billingId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",billingId = '"+billingId+"'";}
}*/

if(!title.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"title = '"+title+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",title = '"+title+"'";}
}

if(!first_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"first_name = '"+first_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",first_name = '"+first_name+"'";}
}

if(!middle_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"middle_name = '"+middle_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",middle_name = '"+middle_name+"'";}
}

if(!last_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"last_name = '"+last_name+"'";
jk=1; 
}else
{
updatecc=updatecc+",last_name = '"+last_name+"'";}
}

/*if(!email_id.equals("")){ */
if(jk==0)
{
updatecc=updatecc+"email_id = '"+email_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",email_id = '"+email_id+"'";}
/*}*/

if(!phone.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"phone = '"+phone+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",phone = '"+phone+"'";}
}

if(!gender.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gender = '"+gender+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gender = '"+gender+"'";}
}

if(!dateof_birth.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"dateof_birth = '"+dateof_birth+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",dateof_birth = '"+dateof_birth+"'";}
}

if(!address1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"address1 = '"+address1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",address1 = '"+address1+"'";}
}

if(!address2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"address2 = '"+address2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",address2 = '"+address2+"'";}
}

if(!pincode.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"pincode = '"+pincode+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",pincode = '"+pincode+"'";}
}

if(!city.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"city = '"+city+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",city = '"+city+"'";}
}

if(!gothram.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gothram = '"+gothram+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gothram = '"+gothram+"'";}
}

if(!from_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"from_date = '"+from_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",from_date = '"+from_date+"'";}
}

if(!to_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"to_date = '"+to_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",to_date = '"+to_date+"'";}
}

if(!image.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"image = '"+image+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",image = '"+image+"'";}
}

if(!idproof1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"idproof1 = '"+idproof1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",idproof1 = '"+idproof1+"'";}
}

if(!idproof2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"idproof2 = '"+idproof2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",idproof2 = '"+idproof2+"'";}
}

if(!pancard_no.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"pancard_no = '"+pancard_no+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",pancard_no = '"+pancard_no+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}
String query="update customerapplication "+updatecc+" where billingId='"+billingId+"'";
System.out.println(query);
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from customerapplication where cust_id='"+cust_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}