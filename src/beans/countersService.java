package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class countersService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String counter_id="";
private String counter_code="";
private String counter_name="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public countersService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setcounter_id(String counter_id)
{
this.counter_id = counter_id;
}

public String getcounter_id()
{
return (this.counter_id);
}


public void setcounter_code(String counter_code)
{
this.counter_code = counter_code;
}

public String getcounter_code()
{
return (this.counter_code);
}


public void setcounter_name(String counter_name)
{
this.counter_name = counter_name;
}

public String getcounter_name()
{
return (this.counter_name);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='counters'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 counter_id=PriSt+ticketm;
String query="insert into counters values('"+counter_id+"','"+counter_code+"','"+counter_name+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"')";
st.executeUpdate(query);
int i = st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='counters'");
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!counter_code.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"counter_code = '"+counter_code+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",counter_code = '"+counter_code+"'";}
}

if(!counter_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"counter_name = '"+counter_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",counter_name = '"+counter_name+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}
String query="update counters "+updatecc+" where counter_id='"+counter_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from counters where counter_id='"+counter_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}