package beans;

public class employeeDetails 
{
	private String employee_details_id;
	private String employee_id;
	private String unique_registration_no;
	private String basic_pay_salary;
	private String ewf_total_loan_amount;
	private String ewf_due_amount;
	private String personal_total_loan_amount;
	private String personal_due_amount;
	private String created_date;
	private String extra1;//eligible for deduction
	private String extra2;//eligible for hra
	private String extra3;
	private String extra4; //bank details
	private String extra5;
	private String created_by;
	private String extra6;
	private String extra7;
	private String extra8;
	private String extra9;
	private String extra10;
	private String extra11;
	private String extra12;
	private String extra13;
	private String extra14;
	private String extra15;
	
	public employeeDetails()
	{
		
	}
	public employeeDetails(String employee_details_id, String employee_id,
			String unique_registration_no, String basic_pay_salary,
			String ewf_total_loan_amount, String ewf_due_amount,
			String personal_total_loan_amount, String personal_due_amount,
			String created_date, String extra1, String extra2, String extra3,
			String extra4, String extra5,String created_by,String extra6,
			String extra7, String extra8, String extra9, String extra10, String extra11,
			String extra12, String extra13, String extra14, String extra15) {
		super();
		this.employee_details_id = employee_details_id;
		this.employee_id = employee_id;
		this.unique_registration_no = unique_registration_no;
		this.basic_pay_salary = basic_pay_salary;
		this.ewf_total_loan_amount = ewf_total_loan_amount;
		this.ewf_due_amount = ewf_due_amount;
		this.personal_total_loan_amount = personal_total_loan_amount;
		this.personal_due_amount = personal_due_amount;
		this.created_date = created_date;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
		this.created_by = created_by;
	}
	
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getEmployee_details_id() {
		return employee_details_id;
	}
	public void setEmployee_details_id(String employee_details_id) {
		this.employee_details_id = employee_details_id;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public String getUnique_registration_no() {
		return unique_registration_no;
	}
	public void setUnique_registration_no(String unique_registration_no) {
		this.unique_registration_no = unique_registration_no;
	}
	public String getBasic_pay_salary() {
		return basic_pay_salary;
	}
	public void setBasic_pay_salary(String basic_pay_salary) {
		this.basic_pay_salary = basic_pay_salary;
	}
	public String getEwf_total_loan_amount() {
		return ewf_total_loan_amount;
	}
	public void setEwf_total_loan_amount(String ewf_total_loan_amount) {
		this.ewf_total_loan_amount = ewf_total_loan_amount;
	}
	public String getEwf_due_amount() {
		return ewf_due_amount;
	}
	public void setEwf_due_amount(String ewf_due_amount) {
		this.ewf_due_amount = ewf_due_amount;
	}
	public String getPersonal_total_loan_amount() {
		return personal_total_loan_amount;
	}
	public void setPersonal_total_loan_amount(String personal_total_loan_amount) {
		this.personal_total_loan_amount = personal_total_loan_amount;
	}
	public String getPersonal_due_amount() {
		return personal_due_amount;
	}
	public void setPersonal_due_amount(String personal_due_amount) {
		this.personal_due_amount = personal_due_amount;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}
	public String getExtra11() {
		return extra11;
	}
	public void setExtra11(String extra11) {
		this.extra11 = extra11;
	}
	public String getExtra12() {
		return extra12;
	}
	public void setExtra12(String extra12) {
		this.extra12 = extra12;
	}
	public String getExtra13() {
		return extra13;
	}
	public void setExtra13(String extra13) {
		this.extra13 = extra13;
	}
	public String getExtra14() {
		return extra14;
	}
	public void setExtra14(String extra14) {
		this.extra14 = extra14;
	}
	public String getExtra15() {
		return extra15;
	}
	public void setExtra15(String extra15) {
		this.extra15 = extra15;
	}
	
	
}
