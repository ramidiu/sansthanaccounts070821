package beans;
public class appointments 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String appointmnetId="";
private String patientId="";
private String doctorId="";
private String purpose="";
private String noOfPersons="";
private String date="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public appointments() {} 
public appointments(String appointmnetId,String patientId,String doctorId,String purpose,String noOfPersons,String date,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.appointmnetId=appointmnetId;
this.patientId=patientId;
this.doctorId=doctorId;
this.purpose=purpose;
this.noOfPersons=noOfPersons;
this.date=date;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getappointmnetId() {
return appointmnetId;
}
public void setappointmnetId(String appointmnetId) {
this.appointmnetId = appointmnetId;
}
public String getpatientId() {
return patientId;
}
public void setpatientId(String patientId) {
this.patientId = patientId;
}
public String getdoctorId() {
return doctorId;
}
public void setdoctorId(String doctorId) {
this.doctorId = doctorId;
}
public String getpurpose() {
return purpose;
}
public void setpurpose(String purpose) {
this.purpose = purpose;
}
public String getnoOfPersons() {
return noOfPersons;
}
public void setnoOfPersons(String noOfPersons) {
this.noOfPersons = noOfPersons;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}