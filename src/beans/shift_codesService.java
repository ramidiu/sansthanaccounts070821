package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class shift_codesService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String shift_code_id="";
private String shift_code="";
private String from_time="";
private String to_time="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public shift_codesService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setshift_code_id(String shift_code_id)
{
this.shift_code_id = shift_code_id;
}

public String getshift_code_id()
{
return (this.shift_code_id);
}


public void setshift_code(String shift_code)
{
this.shift_code = shift_code;
}

public String getshift_code()
{
return (this.shift_code);
}


public void setfrom_time(String from_time)
{
this.from_time = from_time;
}

public String getfrom_time()
{
return (this.from_time);
}


public void setto_time(String to_time)
{
this.to_time = to_time;
}

public String getto_time()
{
return (this.to_time);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='shift_codes'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 shift_code_id=PriSt+ticketm;
String query="insert into shift_codes values('"+shift_code_id+"','"+shift_code+"','"+from_time+"','"+to_time+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='shift_codes'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean update()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!shift_code.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"shift_code = '"+shift_code+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",shift_code = '"+shift_code+"'";}
}

if(!from_time.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"from_time = '"+from_time+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",from_time = '"+from_time+"'";}
}

if(!to_time.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"to_time = '"+to_time+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",to_time = '"+to_time+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update shift_codes "+updatecc+" where shift_code_id='"+shift_code_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from shift_codes where shift_code_id='"+shift_code_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}