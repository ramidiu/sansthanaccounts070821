package beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.Calendar;
import dbase.sqlcon.ConnectionHelper;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

public class customerpurchasesService
{
    private String error;
    private String purchaseId;
    private String date;
    private String productId;
    private String quantity;
    private String rate;
    private String totalAmmount;
    private String billingId;
    private String vocharNumber;
    private String cash_type;
    private String extra1;
    private String extra2;
    private String extra3;
    private String extra4;
    private String extra5;
    private String customername;
    private String phoneno;
    private String emp_id;
    private String gothram;
    private String image;
    private String extra6;
    private String extra7;
    private String extra8;
    private String extra9;
    private String extra10;
    private String offerkind_refid;
    private String tendered_cash;
    private String cheque_no;
    private String name_on_cheque;
    private String cheque_narration;
    private String extra11;
    private String extra12;
    private String extra13;
    private String extra14;
    private String extra15;
    private String othercash_deposit_status;
    private String othercash_totalamount;
    private String extra16;
    private String extra17;
    private String extra18;
    private String extra19;
    private String extra20;
    private String depositeddate;
    private String jvtype;
    private String extra21;
    private String extra22;
    private String extra23;
    private String extra24;
    private String extra25;
    private String extra26;
    private String extra27;
    private String extra28;
    private String extra29;
    private String extra30;
    private ResultSet rs;
    private Statement st;
    private Connection c;
    
    public void seterror(final String error) {
        this.error = error;
    }
    
    public String geterror() {
        return this.error;
    }
    
    public customerpurchasesService() {
        this.error = "";
        this.purchaseId = "";
        this.date = "";
        this.productId = "";
        this.quantity = "";
        this.rate = "";
        this.totalAmmount = "";
        this.billingId = "";
        this.vocharNumber = "";
        this.cash_type = "";
        this.extra1 = "";
        this.extra2 = "";
        this.extra3 = "";
        this.extra4 = "";
        this.extra5 = "";
        this.customername = "";
        this.phoneno = "";
        this.emp_id = "";
        this.gothram = "";
        this.image = "";
        this.extra6 = "";
        this.extra7 = "";
        this.extra8 = "";
        this.extra9 = "";
        this.extra10 = "";
        this.offerkind_refid = "";
        this.tendered_cash = "";
        this.cheque_no = "";
        this.name_on_cheque = "";
        this.cheque_narration = "";
        this.extra11 = "";
        this.extra12 = "";
        this.extra13 = "";
        this.extra14 = "";
        this.extra15 = "";
        this.othercash_deposit_status = "";
        this.othercash_totalamount = "";
        this.extra16 = "";
        this.extra17 = "";
        this.extra18 = "";
        this.extra19 = "";
        this.extra20 = "";
        this.depositeddate = "";
        this.jvtype = "";
        this.extra21 = "";
        this.extra22 = "";
        this.extra23 = "";
        this.extra24 = "";
        this.extra25 = "";
        this.extra26 = "";
        this.extra27 = "";
        this.extra28 = "";
        this.extra29 = "";
        this.extra30 = "";
        this.rs = null;
        this.st = null;
        this.c = null;
    }
    
    public String getDepositeddate() {
        return this.depositeddate;
    }
    
    public void setDepositeddate(final String depositeddate) {
        this.depositeddate = depositeddate;
    }
    
    public String getJvtype() {
        return this.jvtype;
    }
    
    public void setJvtype(final String jvtype) {
        this.jvtype = jvtype;
    }
    
    public String getExtra21() {
        return this.extra21;
    }
    
    public void setExtra21(final String extra21) {
        this.extra21 = extra21;
    }
    
    public String getExtra22() {
        return this.extra22;
    }
    
    public void setExtra22(final String extra22) {
        this.extra22 = extra22;
    }
    
    public String getExtra23() {
        return this.extra23;
    }
    
    public void setExtra23(final String extra23) {
        this.extra23 = extra23;
    }
    
    public String getExtra24() {
        return this.extra24;
    }
    
    public void setExtra24(final String extra24) {
        this.extra24 = extra24;
    }
    
    public String getExtra25() {
        return this.extra25;
    }
    
    public void setExtra25(final String extra25) {
        this.extra25 = extra25;
    }
    
    public String getExtra26() {
        return this.extra26;
    }
    
    public void setExtra26(final String extra26) {
        this.extra26 = extra26;
    }
    
    public String getExtra27() {
        return this.extra27;
    }
    
    public void setExtra27(final String extra27) {
        this.extra27 = extra27;
    }
    
    public String getExtra28() {
        return this.extra28;
    }
    
    public void setExtra28(final String extra28) {
        this.extra28 = extra28;
    }
    
    public String getExtra29() {
        return this.extra29;
    }
    
    public void setExtra29(final String extra29) {
        this.extra29 = extra29;
    }
    
    public String getExtra30() {
        return this.extra30;
    }
    
    public void setExtra30(final String extra30) {
        this.extra30 = extra30;
    }
    
    public void setpurchaseId(final String purchaseId) {
        this.purchaseId = purchaseId;
    }
    
    public String getpurchaseId() {
        return this.purchaseId;
    }
    
    public void setdate(final String date) {
        this.date = date;
    }
    
    public String getdate() {
        return this.date;
    }
    
    public void setproductId(final String productId) {
        this.productId = productId;
    }
    
    public String getproductId() {
        return this.productId;
    }
    
    public void setquantity(final String quantity) {
        this.quantity = quantity;
    }
    
    public String getquantity() {
        return this.quantity;
    }
    
    public void setrate(final String rate) {
        this.rate = rate;
    }
    
    public String getrate() {
        return this.rate;
    }
    
    public void settotalAmmount(final String totalAmmount) {
        this.totalAmmount = totalAmmount;
    }
    
    public String gettotalAmmount() {
        return this.totalAmmount;
    }
    
    public void setbillingId(final String billingId) {
        this.billingId = billingId;
    }
    
    public String getbillingId() {
        return this.billingId;
    }
    
    public void setvocharNumber(final String vocharNumber) {
        this.vocharNumber = vocharNumber;
    }
    
    public String getvocharNumber() {
        return this.vocharNumber;
    }
    
    public void setcash_type(final String cash_type) {
        this.cash_type = cash_type;
    }
    
    public String getcash_type() {
        return this.cash_type;
    }
    
    public void setextra1(final String extra1) {
        this.extra1 = extra1;
    }
    
    public String getextra1() {
        return this.extra1;
    }
    
    public void setextra2(final String extra2) {
        this.extra2 = extra2;
    }
    
    public String getextra2() {
        return this.extra2;
    }
    
    public void setextra3(final String extra3) {
        this.extra3 = extra3;
    }
    
    public String getextra3() {
        return this.extra3;
    }
    
    public void setextra4(final String extra4) {
        this.extra4 = extra4;
    }
    
    public String getextra4() {
        return this.extra4;
    }
    
    public void setextra5(final String extra5) {
        this.extra5 = extra5;
    }
    
    public String getextra5() {
        return this.extra5;
    }
    
    public void setcustomername(final String customername) {
        this.customername = customername;
    }
    
    public String getcustomername() {
        return this.customername;
    }
    
    public void setphoneno(final String phoneno) {
        this.phoneno = phoneno;
    }
    
    public String getphoneno() {
        return this.phoneno;
    }
    
    public void setemp_id(final String emp_id) {
        this.emp_id = emp_id;
    }
    
    public String getemp_id() {
        return this.emp_id;
    }
    
    public void setgothram(final String gothram) {
        this.gothram = gothram;
    }
    
    public String getgothram() {
        return this.gothram;
    }
    
    public void setimage(final String image) {
        this.image = image;
    }
    
    public String getimage() {
        return this.image;
    }
    
    public void setextra6(final String extra6) {
        this.extra6 = extra6;
    }
    
    public String getextra6() {
        return this.extra6;
    }
    
    public void setextra7(final String extra7) {
        this.extra7 = extra7;
    }
    
    public String getextra7() {
        return this.extra7;
    }
    
    public void setextra8(final String extra8) {
        this.extra8 = extra8;
    }
    
    public String getextra8() {
        return this.extra8;
    }
    
    public void setextra9(final String extra9) {
        this.extra9 = extra9;
    }
    
    public String getextra9() {
        return this.extra9;
    }
    
    public void setextra10(final String extra10) {
        this.extra10 = extra10;
    }
    
    public String getextra10() {
        return this.extra10;
    }
    
    public void setofferkind_refid(final String offerkind_refid) {
        this.offerkind_refid = offerkind_refid;
    }
    
    public String getofferkind_refid() {
        return this.offerkind_refid;
    }
    
    public void settendered_cash(final String tendered_cash) {
        this.tendered_cash = tendered_cash;
    }
    
    public String gettendered_cash() {
        return this.tendered_cash;
    }
    
    public void setcheque_no(final String cheque_no) {
        this.cheque_no = cheque_no;
    }
    
    public String getcheque_no() {
        return this.cheque_no;
    }
    
    public void setname_on_cheque(final String name_on_cheque) {
        this.name_on_cheque = name_on_cheque;
    }
    
    public String getname_on_cheque() {
        return this.name_on_cheque;
    }
    
    public void setcheque_narration(final String cheque_narration) {
        this.cheque_narration = cheque_narration;
    }
    
    public String getcheque_narration() {
        return this.cheque_narration;
    }
    
    public void setextra11(final String extra11) {
        this.extra11 = extra11;
    }
    
    public String getextra11() {
        return this.extra11;
    }
    
    public void setextra12(final String extra12) {
        this.extra12 = extra12;
    }
    
    public String getextra12() {
        return this.extra12;
    }
    
    public void setextra13(final String extra13) {
        this.extra13 = extra13;
    }
    
    public String getextra13() {
        return this.extra13;
    }
    
    public void setextra14(final String extra14) {
        this.extra14 = extra14;
    }
    
    public String getextra14() {
        return this.extra14;
    }
    
    public void setextra15(final String extra15) {
        this.extra15 = extra15;
    }
    
    public String getextra15() {
        return this.extra15;
    }
    
    public String getOthercash_deposit_status() {
        return this.othercash_deposit_status;
    }
    
    public void setOthercash_deposit_status(final String othercash_deposit_status) {
        this.othercash_deposit_status = othercash_deposit_status;
    }
    
    public String getOthercash_totalamount() {
        return this.othercash_totalamount;
    }
    
    public void setOthercash_totalamount(final String othercash_totalamount) {
        this.othercash_totalamount = othercash_totalamount;
    }
    
    public String getExtra16() {
        return this.extra16;
    }
    
    public void setExtra16(final String extra16) {
        this.extra16 = extra16;
    }
    
    public String getExtra17() {
        return this.extra17;
    }
    
    public void setExtra17(final String extra17) {
        this.extra17 = extra17;
    }
    
    public String getExtra18() {
        return this.extra18;
    }
    
    public void setExtra18(final String extra18) {
        this.extra18 = extra18;
    }
    
    public String getExtra19() {
        return this.extra19;
    }
    
    public void setExtra19(final String extra19) {
        this.extra19 = extra19;
    }
    
    public String getExtra20() {
        return this.extra20;
    }
    
    public void setExtra20(final String extra20) {
        this.extra20 = extra20;
    }
    
    public void addToOptin(final String phoneno) throws IOException {
        final URL url = new URL("https://waoptin.aclwhatsapp.com/api/v1/addoptinpost");
        final HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        String jsonInputString = "{\"enterpriseId\": \"shrisbuat\", \"msisdn\": \"phoneno\", \"token\": \"602800f0-e3aa-4078-b055-fdde62d621d9\"}";
        jsonInputString = jsonInputString.replace("phoneno", phoneno);
        final OutputStream os = connection.getOutputStream();
        final byte[] input = jsonInputString.getBytes("utf-8");
        os.write(input, 0, input.length);
        final int code = connection.getResponseCode();
        String jsonInputString2 = "{\"messages\": [{\"sender\": \"9052966566\",\"to\": \"pNo\",\"messageId\": \"xxxxx\",\"transactionId\": \"xxxxx\",\"channel\": \"wa\",\"type\": \"template\",\"template\": {\"body\": [],\"templateId\": \"welcome_test\",\"langCode\": \"en\"}}    ],    \"responseType\": \"json\"}";
        jsonInputString2 = jsonInputString2.replace("pNo", phoneno);
    }
    
    public void WtspNotification(final String phoneno) throws IOException {
        final URL url = new URL("https://push.aclwhatsapp.com/pull-platform-receiver/wa/messages/");
        final HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("user", "shrisbpd");
        connection.setRequestProperty("pass", "S{hvR9$Z6st");
        String jsonInputString = "{\"messages\": [{\"sender\": \"9052966566\",\"to\": \"pNo\",\"messageId\": \"xxxkxx1\",\"transactionId\": \"xxxkxx1\",\"channel\": \"wa\",\"type\": \"template\",\"template\": {\"body\": [],\"templateId\": \"receipt_for_seva_booking\r\n\",\"langCode\": \"en\"}}    ],    \"responseType\": \"json\"}";
        jsonInputString = jsonInputString.replace("pNo", phoneno);
        final OutputStream os = connection.getOutputStream();
        final byte[] input = jsonInputString.getBytes("utf-8");
        os.write(input, 0, input.length);
        final int code = connection.getResponseCode();
    }
    
    public boolean insert() {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            (this.rs = this.st.executeQuery("select * from no_genarator where table_name='customerpurchases'")).next();
            final int ticket = this.rs.getInt("table_id");
            final String PriSt = this.rs.getString("id_prefix");
            final int ticketm = ticket + 1;
            this.purchaseId = String.valueOf(String.valueOf(PriSt)) + ticketm;
            final String query = "insert into customerpurchases values('" + this.purchaseId + "','" + this.date + "','" + this.productId + "','" + this.quantity + "','" + this.rate + "','" + this.totalAmmount + "','" + this.billingId + "','" + this.vocharNumber + "','" + this.cash_type + "','" + this.extra1 + "','" + this.extra2 + "','" + this.extra3 + "','" + this.extra4 + "','" + this.extra5 + "','" + this.customername + "','" + this.phoneno + "','" + this.emp_id + "','" + this.gothram + "','" + this.image + "','" + this.extra6 + "','" + this.extra7 + "','" + this.extra8 + "','" + this.extra9 + "','" + this.extra10 + "','" + this.offerkind_refid + "','" + this.tendered_cash + "','" + this.cheque_no + "','" + this.name_on_cheque + "','" + this.cheque_narration + "','" + this.extra11 + "','" + this.extra12 + "','" + this.extra13 + "','" + this.extra14 + "','" + this.extra15 + "','" + this.othercash_deposit_status + "','" + this.othercash_totalamount + "','" + this.extra16 + "','" + this.extra17 + "','" + this.extra18 + "','" + this.extra19 + "','" + this.extra20 + "','" + this.depositeddate + "','" + this.jvtype + "','" + this.extra21 + "','" + this.extra22 + "','" + this.extra23 + "','" + this.extra24 + "','" + this.extra25 + "','" + this.extra26 + "','" + this.extra27 + "','" + this.extra28 + "','" + this.extra29 + "','" + this.extra30 + "')";
            System.out.println("customerpurchasesservice insert query==> " + query);
            this.st.executeUpdate(query);
            System.out.println("request coming to CustomerPurchasesServiec------" + Calendar.getInstance().getTime());
            this.st.executeUpdate("update no_genarator set table_id=" + ticketm + " where table_name='customerpurchases'");
            flag = true;
        }
        catch (Exception e) {
            this.seterror(e.toString());
            e.printStackTrace();
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public boolean update() {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            String updatecc = "set ";
            int jk = 0;
            if (!this.date.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "date = '" + this.date + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",date = '" + this.date + "'";
                }
            }
            if (!this.productId.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "productId = '" + this.productId + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",productId = '" + this.productId + "'";
                }
            }
            if (!this.quantity.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "quantity = '" + this.quantity + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",quantity = '" + this.quantity + "'";
                }
            }
            if (!this.rate.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "rate = '" + this.rate + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",rate = '" + this.rate + "'";
                }
            }
            if (!this.totalAmmount.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "totalAmmount = '" + this.totalAmmount + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",totalAmmount = '" + this.totalAmmount + "'";
                }
            }
            if (!this.vocharNumber.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "vocharNumber = '" + this.vocharNumber + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",vocharNumber = '" + this.vocharNumber + "'";
                }
            }
            if (!this.cash_type.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "cash_type = '" + this.cash_type + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",cash_type = '" + this.cash_type + "'";
                }
            }
            if (!this.extra1.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra1 = '" + this.extra1 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra1 = '" + this.extra1 + "'";
                }
            }
            if (!this.extra2.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra2 = '" + this.extra2 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra2 = '" + this.extra2 + "'";
                }
            }
            if (!this.extra3.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra3 = '" + this.extra3 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra3 = '" + this.extra3 + "'";
                }
            }
            if (!this.extra4.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra4 = '" + this.extra4 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra4 = '" + this.extra4 + "'";
                }
            }
            if (!this.extra5.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra5 = '" + this.extra5 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra5 = '" + this.extra5 + "'";
                }
            }
            if (!this.customername.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "customername = '" + this.customername + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",customername = '" + this.customername + "'";
                }
            }
            if (jk == 0) {
                updatecc = String.valueOf(String.valueOf(updatecc)) + "phoneno = '" + this.phoneno + "'";
                jk = 1;
            }
            else {
                updatecc = String.valueOf(String.valueOf(updatecc)) + ",phoneno = '" + this.phoneno + "'";
            }
            if (!this.emp_id.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "emp_id = '" + this.emp_id + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",emp_id = '" + this.emp_id + "'";
                }
            }
            if (!this.gothram.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "gothram = '" + this.gothram + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",gothram = '" + this.gothram + "'";
                }
            }
            if (!this.image.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "image = '" + this.image + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",image = '" + this.image + "'";
                }
            }
            if (!this.extra6.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra6 = '" + this.extra6 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra6 = '" + this.extra6 + "'";
                }
            }
            if (!this.extra7.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra7 = '" + this.extra7 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra7 = '" + this.extra7 + "'";
                }
            }
            if (jk == 0) {
                updatecc = String.valueOf(String.valueOf(updatecc)) + "extra8 = '" + this.extra8 + "'";
                jk = 1;
            }
            else {
                updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra8 = '" + this.extra8 + "'";
            }
            if (!this.extra9.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra9 = '" + this.extra9 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra9 = '" + this.extra9 + "'";
                }
            }
            if (!this.extra10.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra10 = '" + this.extra10 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra10 = '" + this.extra10 + "'";
                }
            }
            if (!this.offerkind_refid.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "offerkind_refid = '" + this.offerkind_refid + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",offerkind_refid = '" + this.offerkind_refid + "'";
                }
            }
            if (!this.tendered_cash.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "tendered_cash = '" + this.tendered_cash + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",tendered_cash = '" + this.tendered_cash + "'";
                }
            }
            if (!this.cheque_no.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "cheque_no = '" + this.cheque_no + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",cheque_no = '" + this.cheque_no + "'";
                }
            }
            if (!this.name_on_cheque.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "name_on_cheque = '" + this.name_on_cheque + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",name_on_cheque = '" + this.name_on_cheque + "'";
                }
            }
            if (!this.cheque_narration.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "cheque_narration = '" + this.cheque_narration + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",cheque_narration = '" + this.cheque_narration + "'";
                }
            }
            if (!this.extra11.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra11 = '" + this.extra11 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra11 = '" + this.extra11 + "'";
                }
            }
            if (!this.extra12.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra12 = '" + this.extra12 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra12 = '" + this.extra12 + "'";
                }
            }
            if (!this.extra13.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra13 = '" + this.extra13 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra13 = '" + this.extra13 + "'";
                }
            }
            if (!this.extra14.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra14 = '" + this.extra14 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra14 = '" + this.extra14 + "'";
                }
            }
            if (!this.extra15.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra15 = '" + this.extra15 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra15 = '" + this.extra15 + "'";
                }
            }
            if (!this.othercash_deposit_status.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "othercash_deposit_status = '" + this.othercash_deposit_status + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",othercash_deposit_status = '" + this.othercash_deposit_status + "'";
                }
            }
            if (!this.othercash_totalamount.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "othercash_totalamount = '" + this.othercash_totalamount + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",othercash_totalamount = '" + this.othercash_totalamount + "'";
                }
            }
            if (!this.extra16.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra16 = '" + this.extra16 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra16 = '" + this.extra16 + "'";
                }
            }
            if (!this.extra17.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra17 = '" + this.extra17 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra17 = '" + this.extra17 + "'";
                }
            }
            if (!this.extra18.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra18 = '" + this.extra18 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra18 = '" + this.extra18 + "'";
                }
            }
            if (!this.extra19.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra19 = '" + this.extra19 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra19 = '" + this.extra19 + "'";
                }
            }
            if (!this.extra20.equals("")) {
                if (jk == 0) {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + "extra20 = '" + this.extra20 + "'";
                    jk = 1;
                }
                else {
                    updatecc = String.valueOf(String.valueOf(updatecc)) + ",extra20 = '" + this.extra20 + "'";
                }
            }
            final String query = "update customerpurchases " + updatecc + " where billingId='" + this.billingId + "'";
            System.out.println("customerpurchasesservice update query."+query);
            final int i = this.st.executeUpdate(query);
            if (i > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public boolean delete() {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "delete from customerpurchases where purchaseId='" + this.purchaseId + "'";
            final int i = this.st.executeUpdate(query);
            if (i > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public boolean delete1(final String vochar) {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "delete from customerpurchases where vocharNumber='" + vochar + "'";
            System.out.println("customerpurchasesservice delete1 query.."+query);
            final int i = this.st.executeUpdate(query);
            if (i > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public boolean updateSellAmount(final String pid, final String rate) {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "update customerpurchases set rate='" + rate + "' where purchaseId='" + pid + "'";
            System.out.println("customerpurchasesservice updatesellamount query.."+query);
            final int i = this.st.executeUpdate(query);
            if (i > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public boolean updateVatRateRate(final String billingId, final String counteramount) {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "update customerpurchases set extra14='" + counteramount + "' where billingId='" + billingId + "' ";
            System.out.println("customerpurchasesservice updateVatRateRate query.."+query);
            final int i = this.st.executeUpdate(query);
            if (i > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public boolean updateDevoteeImg(final String billId, final String img) {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "update customerpurchases set image='" + img + "' where billingId='" + billId + "'";
            System.out.println("customerpurcasesservice updateDevoteeImg query.."+query);
            final int i = this.st.executeUpdate(query);
            if (i > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public boolean updateTotalAmt(final String billId, final String totamt) {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "update customerpurchases set extra14='" + totamt + "' where billingId='" + billId + "'";
            System.out.println("customerpurcasesservice updateTotalAmt query.."+query);
            final int i = this.st.executeUpdate(query);
            if (i > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public boolean updateBasedOnId(final String billId, final String totamt, final String status) {
        boolean flag = false;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "update customerpurchases set othercash_deposit_status='" + status + "',othercash_totalamount='" + totamt + "',extra16=now() where billingId='" + billId + "'";
            System.out.println("customerpurcasesservice updateBasedOnId query.."+query);
            System.out.println(query);
            final int i = this.st.executeUpdate(query);
            if (i > 0) {
                flag = true;
            }
        }
        catch (Exception e) {
            this.seterror(e.toString());
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return flag;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return flag;
    }
    
    public ArrayList<customerpurchases> getCustomerDetails(final String fromdate, final String todate, final String paymenttype, final String hoa) {
        final ArrayList<customerpurchases> cplist = new ArrayList<customerpurchases>();
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "select date,sum(totalammount) as amount,depositeddate,extra21 from customerpurchases where date between '" + fromdate + " 00:00:00' and '" + todate + " 23:59:59' and cash_type='" + paymenttype + "' and extra1='" + hoa + "' group by date(date)";
            System.out.println("customerpurchasesservice getCustomerDetails query." + query);
            this.rs = this.st.executeQuery(query);
            while (this.rs.next()) {
                final customerpurchases cp = new customerpurchases();
                cp.setdate(this.rs.getString("date"));
                cp.settotalAmmount(this.rs.getString("amount"));
                cp.setDepositeddate(this.rs.getString("depositedDate"));
                cp.setvocharNumber(this.rs.getString("extra21"));
                cplist.add(cp);
            }
        }
        catch (SQLException se) {
            se.printStackTrace();
        }
        catch (Exception cnf) {
            cnf.printStackTrace();
        }
        finally {
            try {
                this.c.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            try {
                this.st.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            try {
                this.rs.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            this.c.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.st.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.rs.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.c.close();
        }
        catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            this.st.close();
        }
        catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            this.rs.close();
        }
        catch (Exception e2) {
            e2.printStackTrace();
        }
        return cplist;
    }
    
    public int updateDepositedDate(final String date, String depositeddate, final String paymenttype, String jvnum, final String hoa) {
        int result = 0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            if (depositeddate.equals("")) {
                depositeddate = "";
            }
            if (jvnum.equals("")) {
                jvnum = "";
            }
            final String query = "update customerpurchases set depositedDate='" + depositeddate + "',extra21='" + jvnum + "' where date like'" + date + "%' and cash_type='" + paymenttype + "' and extra1='" + hoa + "'";
            System.out.println("customerpurchasesservice updateDepositedDate query in " + query);
            result = this.st.executeUpdate(query);
        }
        catch (SQLException se) {
            se.printStackTrace();
        }
        catch (Exception cnf) {
            cnf.printStackTrace();
        }
        finally {
            try {
                this.c.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            try {
                this.st.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            this.c.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.st.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.c.close();
        }
        catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            this.st.close();
        }
        catch (Exception e2) {
            e2.printStackTrace();
        }
        return result;
    }
    
    public int insertJvInCustomerPurchases(final String amount, final String date, final String hoa, String jvnum, final String empid) {
        final Calendar cal = Calendar.getInstance();
        final DateFormat df = new SimpleDateFormat("HH:mm:ss");
        final String subdate = df.format(cal.getTime());
        int result1 = 0;
        int result2 = 0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            (this.rs = this.st.executeQuery("select * from no_genarator where table_name='customerpurchases'")).next();
            final int ticket = this.rs.getInt("table_id");
            final String PriSt = this.rs.getString("id_prefix");
            final int ticketm = ticket + 1;
            this.purchaseId = String.valueOf(String.valueOf(PriSt)) + ticketm;
            if (jvnum == "") {
                jvnum = "";
            }
            final String query = "insert into customerpurchases values ('" + this.purchaseId + "','" + date + " " + subdate + "','25765','1','" + amount + "','" + amount + "','" + this.billingId + "','" + jvnum + "','journalvoucher','" + hoa + "','RECEIVABLE FROM SANSTHAN','" + this.extra3 + "','" + this.extra4 + "','" + this.extra5 + "','" + this.customername + "','" + this.phoneno + "','" + empid + "','" + this.gothram + "','" + this.image + "','102','689','" + this.extra8 + "','" + this.extra9 + "','" + this.extra10 + "','" + this.offerkind_refid + "','" + this.tendered_cash + "','" + this.cheque_no + "','" + this.name_on_cheque + "','" + this.cheque_narration + "','" + this.extra11 + "','" + this.extra12 + "','" + this.extra13 + "','" + this.extra14 + "','" + this.extra15 + "','" + this.othercash_deposit_status + "','" + this.othercash_totalamount + "','" + this.extra16 + "','" + this.extra17 + "','" + this.extra18 + "','" + this.extra19 + "','" + this.extra20 + "','" + this.depositeddate + "','with out bank','" + this.extra21 + "','" + this.extra22 + "','" + this.extra23 + "','" + this.extra24 + "','" + this.extra25 + "','" + this.extra26 + "','" + this.extra27 + "','" + this.extra28 + "','" + this.extra29 + "','" + this.extra30 + "')";
            System.out.println("customerpurchasesservice insertJvInCustomerPurchases query in " + query);
            result1 = this.st.executeUpdate(query);
            System.out.println("insert int cp==>" + query);
            result2 = this.st.executeUpdate("update no_genarator set table_id=" + ticketm + " where table_name='customerpurchases'");
            System.out.println("updating no generator table========>" + result2);
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return result1;
        }
        finally {
            try {
                if (this.rs != null) {
                    this.rs.close();
                }
                if (this.st != null) {
                    this.st.close();
                }
                if (this.c != null) {
                    this.c.close();
                }
            }
            catch (SQLException e3) {
                e3.printStackTrace();
            }
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e3) {
            e3.printStackTrace();
        }
        try {
            if (this.rs != null) {
                this.rs.close();
            }
            if (this.st != null) {
                this.st.close();
            }
            if (this.c != null) {
                this.c.close();
            }
        }
        catch (SQLException e4) {
            e4.printStackTrace();
        }
        return result1;
    }
    
    public int updateJvDate(final String jvdate, final String jvnumber) {
        final Calendar cal = Calendar.getInstance();
        final DateFormat df = new SimpleDateFormat("HH:mm:ss");
        final String subdate = df.format(cal.getTime());
        int result = 0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "update customerpurchases set date='" + jvdate + " " + subdate + "' where vocharnumber='" + jvnumber + "'";
            System.out.println("customerpurchasesservice updateJvDate query" + query);
            result = this.st.executeUpdate(query);
            this.c.close();
            this.st.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public int deleteJv(final String jvnumber) {
        int result = 0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "delete from  customerpurchases where vocharnumber='" + jvnumber + "'";
            System.out.println("delete jv ============>" + query);
            result = this.st.executeUpdate(query);
            this.c.close();
            this.st.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public int updateMajorMinorSubHeadCUP(final String majorHeadId, final String minorHeadId, final String subHeadId, final String purchaseId, final String amount, final String entryDate) {
        System.out.println("entryDate:::::::" + entryDate);
        final Calendar cal = Calendar.getInstance();
        int result = 0;
        try {
            this.c = ConnectionHelper.getConnection();
            this.st = this.c.createStatement();
            final String query = "update customerpurchases set extra6='" + majorHeadId + "',extra7='" + minorHeadId + "',productId='" + subHeadId + "',rate='" + amount + "',totalammount='" + amount + "',date='" + entryDate + "' where purchaseId='" + purchaseId + "'";
            System.out.println("customerpurchasesservice updateMajorMinorSubHeadCUP query.."+query);
            result = this.st.executeUpdate(query);
            this.c.close();
            this.st.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

