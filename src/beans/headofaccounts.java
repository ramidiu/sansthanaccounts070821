package beans;
public class headofaccounts 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String head_account_id="";
private String name="";
private String phone="";
private String address="";
private String description="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public headofaccounts() {} 
public headofaccounts(String head_account_id,String name,String phone,String address,String description,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.head_account_id=head_account_id;
this.name=name;
this.phone=phone;
this.address=address;
this.description=description;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getname() {
return name;
}
public void setname(String name) {
this.name = name;
}
public String getphone() {
return phone;
}
public void setphone(String phone) {
this.phone = phone;
}
public String getaddress() {
return address;
}
public void setaddress(String address) {
this.address = address;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}