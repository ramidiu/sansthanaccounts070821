package beans;
public class doctordetails 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String doctorId="";
private String doctorName="";
private String doctorQualification="";
private String phoneNo="";
private String address="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public doctordetails() {} 
public doctordetails(String doctorId,String doctorName,String doctorQualification,String phoneNo,String address,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.doctorId=doctorId;
this.doctorName=doctorName;
this.doctorQualification=doctorQualification;
this.phoneNo=phoneNo;
this.address=address;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getdoctorId() {
return doctorId;
}
public void setdoctorId(String doctorId) {
this.doctorId = doctorId;
}
public String getdoctorName() {
return doctorName;
}
public void setdoctorName(String doctorName) {
this.doctorName = doctorName;
}
public String getdoctorQualification() {
return doctorQualification;
}
public void setdoctorQualification(String doctorQualification) {
this.doctorQualification = doctorQualification;
}
public String getphoneNo() {
return phoneNo;
}
public void setphoneNo(String phoneNo) {
this.phoneNo = phoneNo;
}
public String getaddress() {
return address;
}
public void setaddress(String address) {
this.address = address;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}