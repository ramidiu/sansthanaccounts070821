package beans;
public class medicalpurchases 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String purchaseId="";
private String tabletId="";
private String purchaseDate="";
private String vendorId="";
private String billNumber="";
private String date="";
private String amount="";
private String narration="";
private String quantity="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String purchaseby="";
public medicalpurchases() {} 
public medicalpurchases(String purchaseId,String tabletId,String purchaseDate,String vendorId,String billNumber,String date,String amount,String narration,String quantity,String extra1,String extra2,String extra3,String extra4,String extra5,String purchaseby)
 {
this.purchaseId=purchaseId;
this.tabletId=tabletId;
this.purchaseDate=purchaseDate;
this.vendorId=vendorId;
this.billNumber=billNumber;
this.date=date;
this.amount=amount;
this.narration=narration;
this.quantity=quantity;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.purchaseby=purchaseby;

} 
public String getpurchaseId() {
return purchaseId;
}
public void setpurchaseId(String purchaseId) {
this.purchaseId = purchaseId;
}
public String gettabletId() {
return tabletId;
}
public void settabletId(String tabletId) {
this.tabletId = tabletId;
}
public String getpurchaseDate() {
return purchaseDate;
}
public void setpurchaseDate(String purchaseDate) {
this.purchaseDate = purchaseDate;
}
public String getvendorId() {
return vendorId;
}
public void setvendorId(String vendorId) {
this.vendorId = vendorId;
}
public String getbillNumber() {
return billNumber;
}
public void setbillNumber(String billNumber) {
this.billNumber = billNumber;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String getamount() {
return amount;
}
public void setamount(String amount) {
this.amount = amount;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getquantity() {
return quantity;
}
public void setquantity(String quantity) {
this.quantity = quantity;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getpurchaseby() {
return purchaseby;
}
public void setpurchaseby(String purchaseby) {
this.purchaseby = purchaseby;
}

}