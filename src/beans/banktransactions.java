package beans;
public class banktransactions 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String banktrn_id="";
private String bank_id="";
private String name="";
private String narration="";
private String date="";
private String amount="";
private String type="";
private String createdBy="";
private String editedBy="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String brs_date="";
private String extra6="";
private String extra7="";
private String extra8="";
private String sub_head_id="";
private String minor_head_id="";
private String major_head_id="";
private String head_account_id="";
private String extra9="";
private String extra10="";
private String extra11="";
private String extra12="";
private String extra13="";

public String getSub_head_id() {
	return sub_head_id;
}
public void setSub_head_id(String sub_head_id) {
	this.sub_head_id = sub_head_id;
}
public String getMinor_head_id() {
	return minor_head_id;
}
public void setMinor_head_id(String minor_head_id) {
	this.minor_head_id = minor_head_id;
}
public String getMajor_head_id() {
	return major_head_id;
}
public void setMajor_head_id(String major_head_id) {
	this.major_head_id = major_head_id;
}
public String getHead_account_id() {
	return head_account_id;
}
public void setHead_account_id(String head_account_id) {
	this.head_account_id = head_account_id;
}
public String getExtra9() {
	return extra9;
}
public void setExtra9(String extra9) {
	this.extra9 = extra9;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}



public String getExtra6() {
	return extra6;
}
public void setExtra6(String extra6) {
	this.extra6 = extra6;
}
public String getExtra7() {
	return extra7;
}
public void setExtra7(String extra7) {
	this.extra7 = extra7;
}
public String getExtra8() {
	return extra8;
}
public void setExtra8(String extra8) {
	this.extra8 = extra8;
}

public banktransactions() {} 
public banktransactions(String banktrn_id,String bank_id,String name,String narration,String date,String amount,String type,String createdBy,String editedBy,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.banktrn_id=banktrn_id;
this.bank_id=bank_id;
this.name=name;
this.narration=narration;
this.date=date;
this.amount=amount;
this.type=type;
this.createdBy=createdBy;
this.editedBy=editedBy;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getbanktrn_id() {
return banktrn_id;
}
public void setbanktrn_id(String banktrn_id) {
this.banktrn_id = banktrn_id;
}
public String getbank_id() {
return bank_id;
}
public void setbank_id(String bank_id) {
this.bank_id = bank_id;
}
public String getname() {
return name;
}
public void setname(String name) {
this.name = name;
}
public void setbrs_date(String brs_date)
{
this.brs_date = brs_date;
}

public String getbrs_date()
{
return (this.brs_date);
}



public banktransactions(String banktrn_id, String bank_id, String name,
		String narration, String date, String amount, String type,
		String createdBy, String editedBy, String extra1, String extra2,
		String extra3, String extra4, String extra5, String brs_date,
		String extra6, String extra7, String extra8, String sub_head_id,
		String minor_head_id, String major_head_id, String head_account_id,
		String extra9, String extra10, String extra11, String extra12,
		String extra13) {
	super();
	this.banktrn_id = banktrn_id;
	this.bank_id = bank_id;
	this.name = name;
	this.narration = narration;
	this.date = date;
	this.amount = amount;
	this.type = type;
	this.createdBy = createdBy;
	this.editedBy = editedBy;
	this.extra1 = extra1;
	this.extra2 = extra2;
	this.extra3 = extra3;
	this.extra4 = extra4;
	this.extra5 = extra5;
	this.brs_date = brs_date;
	this.extra6 = extra6;
	this.extra7 = extra7;
	this.extra8 = extra8;
	this.sub_head_id = sub_head_id;
	this.minor_head_id = minor_head_id;
	this.major_head_id = major_head_id;
	this.head_account_id = head_account_id;
	this.extra9 = extra9;
	this.extra10 = extra10;
	this.extra11 = extra11;
	this.extra12 = extra12;
	this.extra13 = extra13;
}
public banktransactions(String banktrn_id, String bank_id, String name,
		String narration, String date, String amount, String type,
		String createdBy, String editedBy, String extra1, String extra2,
		String extra3, String extra4, String extra5, String brs_date,
		String extra6, String extra7, String extra8) {
	super();
	this.banktrn_id = banktrn_id;
	this.bank_id = bank_id;
	this.name = name;
	this.narration = narration;
	this.date = date;
	this.amount = amount;
	this.type = type;
	this.createdBy = createdBy;
	this.editedBy = editedBy;
	this.extra1 = extra1;
	this.extra2 = extra2;
	this.extra3 = extra3;
	this.extra4 = extra4;
	this.extra5 = extra5;
	this.brs_date = brs_date;
	this.extra6 = extra6;
	this.extra7 = extra7;
	this.extra8 = extra8;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String getamount() {
return amount;
}
public void setamount(String amount) {
this.amount = amount;
}
public String gettype() {
return type;
}
public void settype(String type) {
this.type = type;
}
@Override
public String toString() {
	return "banktransactions [error=" + error + ", banktrn_id=" + banktrn_id
			+ ", bank_id=" + bank_id + ", name=" + name + ", narration="
			+ narration + ", date=" + date + ", amount=" + amount + ", type="
			+ type + ", createdBy=" + createdBy + ", editedBy=" + editedBy
			+ ", extra1=" + extra1 + ", extra2=" + extra2 + ", extra3="
			+ extra3 + ", extra4=" + extra4 + ", extra5=" + extra5
			+ ", brs_date=" + brs_date + ", extra6=" + extra6 + ", extra7="
			+ extra7 + ", extra8=" + extra8 + ", sub_head_id=" + sub_head_id
			+ ", minor_head_id=" + minor_head_id + ", major_head_id="
			+ major_head_id + ", head_account_id=" + head_account_id
			+ ", extra9=" + extra9 + ", extra10=" + extra10 + ", extra11="
			+ extra11 + ", extra12=" + extra12 + ", extra13=" + extra13 + "]";
}
public String getcreatedBy() {
return createdBy;
}
public void setcreatedBy(String createdBy) {
this.createdBy = createdBy;
}
public String geteditedBy() {
return editedBy;
}
public void seteditedBy(String editedBy) {
this.editedBy = editedBy;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}