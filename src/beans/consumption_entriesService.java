package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class consumption_entriesService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String consumption_id="";
private String entry_date="";
private String consumption_category="";
private String con_sub_category="";
private String head_account_id="";
private String narration="";
private String items_qty="";
private String devotees_qty="";
private String damage_qty="";
private String leftover_qty="";
private String emp_id="";
private String consumption_invoice_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public consumption_entriesService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setconsumption_id(String consumption_id)
{
this.consumption_id = consumption_id;
}

public String getconsumption_id()
{
return (this.consumption_id);
}


public void setentry_date(String entry_date)
{
this.entry_date = entry_date;
}

public String getentry_date()
{
return (this.entry_date);
}


public void setconsumption_category(String consumption_category)
{
this.consumption_category = consumption_category;
}

public String getconsumption_category()
{
return (this.consumption_category);
}


public void setcon_sub_category(String con_sub_category)
{
this.con_sub_category = con_sub_category;
}

public String getcon_sub_category()
{
return (this.con_sub_category);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setitems_qty(String items_qty)
{
this.items_qty = items_qty;
}

public String getitems_qty()
{
return (this.items_qty);
}


public void setdevotees_qty(String devotees_qty)
{
this.devotees_qty = devotees_qty;
}

public String getdevotees_qty()
{
return (this.devotees_qty);
}


public void setdamage_qty(String damage_qty)
{
this.damage_qty = damage_qty;
}

public String getdamage_qty()
{
return (this.damage_qty);
}


public void setleftover_qty(String leftover_qty)
{
this.leftover_qty = leftover_qty;
}

public String getleftover_qty()
{
return (this.leftover_qty);
}


public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}


public void setconsumption_invoice_id(String consumption_invoice_id)
{
this.consumption_invoice_id = consumption_invoice_id;
}

public String getconsumption_invoice_id()
{
return (this.consumption_invoice_id);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void setextra6(String extra6)
{
this.extra6 = extra6;
}

public String getextra6()
{
return (this.extra6);
}


public void setextra7(String extra7)
{
this.extra7 = extra7;
}

public String getextra7()
{
return (this.extra7);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='consumption_entries'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 consumption_id=PriSt+ticketm;
String query="insert into consumption_entries values('"+consumption_id+"','"+entry_date+"','"+consumption_category+"','"+con_sub_category+"','"+head_account_id+"','"+narration+"','"+items_qty+"','"+devotees_qty+"','"+damage_qty+"','"+leftover_qty+"','"+emp_id+"','"+consumption_invoice_id+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"')";
st.executeUpdate(query);
int i = st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='consumption_entries'");
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!entry_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"entry_date = '"+entry_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",entry_date = '"+entry_date+"'";}
}

if(!consumption_category.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"consumption_category = '"+consumption_category+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",consumption_category = '"+consumption_category+"'";}
}

if(!con_sub_category.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"con_sub_category = '"+con_sub_category+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",con_sub_category = '"+con_sub_category+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!items_qty.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"items_qty = '"+items_qty+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",items_qty = '"+items_qty+"'";}
}

if(!devotees_qty.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"devotees_qty = '"+devotees_qty+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",devotees_qty = '"+devotees_qty+"'";}
}

if(!damage_qty.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"damage_qty = '"+damage_qty+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",damage_qty = '"+damage_qty+"'";}
}

if(!leftover_qty.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"leftover_qty = '"+leftover_qty+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",leftover_qty = '"+leftover_qty+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!consumption_invoice_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"consumption_invoice_id = '"+consumption_invoice_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",consumption_invoice_id = '"+consumption_invoice_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!extra6.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra6 = '"+extra6+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra6 = '"+extra6+"'";}
}

if(!extra7.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra7 = '"+extra7+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra7 = '"+extra7+"'";}
}
String query="update consumption_entries "+updatecc+" where consumption_id='"+consumption_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from consumption_entries where consumption_id='"+consumption_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}