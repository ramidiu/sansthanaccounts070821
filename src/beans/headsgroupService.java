package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class headsgroupService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String group_id="";
private String group_name="";
private String narration="";
private String created_date="";
private String head_account_id="";
private String emp_id="";
private String extra1="";
private String extra2="";
private String extra3="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public headsgroupService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setgroup_id(String group_id)
{
this.group_id = group_id;
}

public String getgroup_id()
{
return (this.group_id);
}


public void setgroup_name(String group_name)
{
this.group_name = group_name;
}

public String getgroup_name()
{
return (this.group_name);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setcreated_date(String created_date)
{
this.created_date = created_date;
}

public String getcreated_date()
{
return (this.created_date);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='headsgroup'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
group_id=PriSt+ticketm;
String query="insert into headsgroup values('"+group_id+"','"+group_name+"','"+narration+"','"+created_date+"','"+head_account_id+"','"+emp_id+"','"+extra1+"','"+extra2+"','"+extra3+"')";
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='headsgroup'");
flag = true;
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!group_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"group_name = '"+group_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",group_name = '"+group_name+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!created_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"created_date = '"+created_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",created_date = '"+created_date+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

if(!emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_id = '"+emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_id = '"+emp_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}
String query="update headsgroup "+updatecc+" where group_id='"+group_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from headsgroup where group_id='"+group_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}