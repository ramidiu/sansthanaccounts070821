package beans;

public class daily_bill_status 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String bill_id="";
	private String createdBy="";
	private String createdDate="";
	private String fileName="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	public String getBill_id() {
		return bill_id;
	}
	public void setBill_id(String bill_id) {
		this.bill_id = bill_id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	
	public daily_bill_status() {} 
	
	public daily_bill_status(String bill_id, String createdBy,
			String createdDate, String fileName, String extra1, String extra2,
			String extra3, String extra4, String extra5) {
		super();
		this.bill_id = bill_id;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.fileName = fileName;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
	}
}
