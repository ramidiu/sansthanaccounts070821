package beans;

public class volunteerseva 
{
	private String volunteerSevaID="";
	private String sevaName="";
	private String sssstId="";
	private String datee="";
	private String createdDate="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	
	public volunteerseva() {} 
	public volunteerseva(String volunteerSevaID,String sevaName,String sssstId,String datee,String createdDate,String extra1,String extra2,String extra3,String extra4,String extra5)
	 {
	this.volunteerSevaID=volunteerSevaID;
	this.sevaName=sevaName;
	this.sssstId=sssstId;
	this.datee=datee;
	this.createdDate=createdDate;
	this.extra1=extra1;
	this.extra2=extra2;
	this.extra3=extra3;
	this.extra4=extra4;
	this.extra5=extra5;

	}
	
	public String getVolunteerSevaID() {
		return volunteerSevaID;
	}
	public void setVolunteerSevaID(String volunteerSevaID) {
		this.volunteerSevaID = volunteerSevaID;
	}
	public String getSevaName() {
		return sevaName;
	}
	public void setSevaName(String sevaName) {
		this.sevaName = sevaName;
	}
	public String getSssstId() {
		return sssstId;
	}
	public void setSssstId(String sssstId) {
		this.sssstId = sssstId;
	}
	public String getDatee() {
		return datee;
	}
	public void setDatee(String datee) {
		this.datee = datee;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	
}
