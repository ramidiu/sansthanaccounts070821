package beans;
public class stockrequest 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String req_id="";
private String req_date="";
private String narration="";
private String product_id="";
private String quantity="";
private String emp_id="";
private String head_account_id="";
private String reqinv_id="";
private String status="";
private String req_type="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public stockrequest() {} 
public stockrequest(String req_id,String req_date,String narration,String product_id,String quantity,String emp_id,String head_account_id,String reqinv_id,String status,String req_type,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.req_id=req_id;
this.req_date=req_date;
this.narration=narration;
this.product_id=product_id;
this.quantity=quantity;
this.emp_id=emp_id;
this.head_account_id=head_account_id;
this.reqinv_id=reqinv_id;
this.status=status;
this.req_type=req_type;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getreq_id() {
return req_id;
}
public void setreq_id(String req_id) {
this.req_id = req_id;
}
public String getreq_date() {
return req_date;
}
public void setreq_date(String req_date) {
this.req_date = req_date;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getproduct_id() {
return product_id;
}
public void setproduct_id(String product_id) {
this.product_id = product_id;
}
public String getquantity() {
return quantity;
}
public void setquantity(String quantity) {
this.quantity = quantity;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getreqinv_id() {
return reqinv_id;
}
public void setreqinv_id(String reqinv_id) {
this.reqinv_id = reqinv_id;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String getreq_type() {
return req_type;
}
public void setreq_type(String req_type) {
this.req_type = req_type;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}