package beans;

import java.io.Serializable;

public class ProductOpeningBalance implements Serializable{

	private static final long serialVersionUID = 1L;
	private String productOpeningBalanceId;
	private String productId;
	private String productName;
	private String headOfAccount;
	private String majorHeadId;
	private String minorHeadId;
	private String subHeadId;
	private String finacilaYearDate;
	private double quantity;
	private String createdDate;
	private String category;
	private String subCategory;
	public String getProductOpeningBalanceId() {
		return productOpeningBalanceId;
	}
	public void setProductOpeningBalanceId(String productOpeningBalanceId) {
		this.productOpeningBalanceId = productOpeningBalanceId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getHeadOfAccount() {
		return headOfAccount;
	}
	public void setHeadOfAccount(String headOfAccount) {
		this.headOfAccount = headOfAccount;
	}
	public String getMajorHeadId() {
		return majorHeadId;
	}
	public void setMajorHeadId(String majorHeadId) {
		this.majorHeadId = majorHeadId;
	}
	public String getMinorHeadId() {
		return minorHeadId;
	}
	public void setMinorHeadId(String minorHeadId) {
		this.minorHeadId = minorHeadId;
	}
	public String getSubHeadId() {
		return subHeadId;
	}
	public void setSubHeadId(String subHeadId) {
		this.subHeadId = subHeadId;
	}
	public String getFinacilaYearDate() {
		return finacilaYearDate;
	}
	public void setFinacilaYearDate(String finacilaYearDate) {
		this.finacilaYearDate = finacilaYearDate;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	@Override
	public String toString() {
		return "ProductOpeningBalance [productOpeningBalanceId=" + productOpeningBalanceId + ", productId=" + productId
				+ ", productName=" + productName + ", headOfAccount=" + headOfAccount + ", majorHeadId=" + majorHeadId
				+ ", minorHeadId=" + minorHeadId + ", subHeadId=" + subHeadId + ", finacilaYearDate=" + finacilaYearDate
				+ ", quantity=" + quantity + ", createdDate=" + createdDate + ", category=" + category
				+ ", subCategory=" + subCategory + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((finacilaYearDate == null) ? 0 : finacilaYearDate.hashCode());
		result = prime * result + ((headOfAccount == null) ? 0 : headOfAccount.hashCode());
		result = prime * result + ((majorHeadId == null) ? 0 : majorHeadId.hashCode());
		result = prime * result + ((minorHeadId == null) ? 0 : minorHeadId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((productName == null) ? 0 : productName.hashCode());
		result = prime * result + ((productOpeningBalanceId == null) ? 0 : productOpeningBalanceId.hashCode());
		long temp;
		temp = Double.doubleToLongBits(quantity);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((subCategory == null) ? 0 : subCategory.hashCode());
		result = prime * result + ((subHeadId == null) ? 0 : subHeadId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductOpeningBalance other = (ProductOpeningBalance) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (finacilaYearDate == null) {
			if (other.finacilaYearDate != null)
				return false;
		} else if (!finacilaYearDate.equals(other.finacilaYearDate))
			return false;
		if (headOfAccount == null) {
			if (other.headOfAccount != null)
				return false;
		} else if (!headOfAccount.equals(other.headOfAccount))
			return false;
		if (majorHeadId == null) {
			if (other.majorHeadId != null)
				return false;
		} else if (!majorHeadId.equals(other.majorHeadId))
			return false;
		if (minorHeadId == null) {
			if (other.minorHeadId != null)
				return false;
		} else if (!minorHeadId.equals(other.minorHeadId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (productName == null) {
			if (other.productName != null)
				return false;
		} else if (!productName.equals(other.productName))
			return false;
		if (productOpeningBalanceId == null) {
			if (other.productOpeningBalanceId != null)
				return false;
		} else if (!productOpeningBalanceId.equals(other.productOpeningBalanceId))
			return false;
		if (Double.doubleToLongBits(quantity) != Double.doubleToLongBits(other.quantity))
			return false;
		if (subCategory == null) {
			if (other.subCategory != null)
				return false;
		} else if (!subCategory.equals(other.subCategory))
			return false;
		if (subHeadId == null) {
			if (other.subHeadId != null)
				return false;
		} else if (!subHeadId.equals(other.subHeadId))
			return false;
		return true;
	}
	
}
