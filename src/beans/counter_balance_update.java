package beans;
public class counter_balance_update 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String c_b_id="";
private String booking_id="";
private String counter_code="";
private String shift_code="";
private String date="";
private String login_id="";
private String booking_amount="";
private String opening_balance="";
private String counter_balance="";
private String closing_balance="";
private String booked_date="";
private String counter_ex1="";
private String counter_ex2="";
private String counter_ex3="";
private String counter_ex4="";
private String counter_ex5="";
private String counter_ex6="";
private String counter_ex7="";
public counter_balance_update() {} 
public counter_balance_update(String c_b_id,String booking_id,String counter_code,String shift_code,String date,String login_id,String booking_amount,String opening_balance,String counter_balance,String closing_balance,String booked_date,String counter_ex1,String counter_ex2,String counter_ex3,String counter_ex4,String counter_ex5,String counter_ex6,String counter_ex7)
 {
this.c_b_id=c_b_id;
this.booking_id=booking_id;
this.counter_code=counter_code;
this.shift_code=shift_code;
this.date=date;
this.login_id=login_id;
this.booking_amount=booking_amount;
this.opening_balance=opening_balance;
this.counter_balance=counter_balance;
this.closing_balance=closing_balance;
this.booked_date=booked_date;
this.counter_ex1=counter_ex1;
this.counter_ex2=counter_ex2;
this.counter_ex3=counter_ex3;
this.counter_ex4=counter_ex4;
this.counter_ex5=counter_ex5;
this.counter_ex6=counter_ex6;
this.counter_ex7=counter_ex7;

} 
public String getc_b_id() {
return c_b_id;
}
public void setc_b_id(String c_b_id) {
this.c_b_id = c_b_id;
}
public String getbooking_id() {
return booking_id;
}
public void setbooking_id(String booking_id) {
this.booking_id = booking_id;
}
public String getcounter_code() {
return counter_code;
}
public void setcounter_code(String counter_code) {
this.counter_code = counter_code;
}
public String getshift_code() {
return shift_code;
}
public void setshift_code(String shift_code) {
this.shift_code = shift_code;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String getlogin_id() {
return login_id;
}
public void setlogin_id(String login_id) {
this.login_id = login_id;
}
public String getbooking_amount() {
return booking_amount;
}
public void setbooking_amount(String booking_amount) {
this.booking_amount = booking_amount;
}
public String getopening_balance() {
return opening_balance;
}
public void setopening_balance(String opening_balance) {
this.opening_balance = opening_balance;
}
public String getcounter_balance() {
return counter_balance;
}
public void setcounter_balance(String counter_balance) {
this.counter_balance = counter_balance;
}
public String getclosing_balance() {
return closing_balance;
}
public void setclosing_balance(String closing_balance) {
this.closing_balance = closing_balance;
}
public String getbooked_date() {
return booked_date;
}
public void setbooked_date(String booked_date) {
this.booked_date = booked_date;
}
public String getcounter_ex1() {
return counter_ex1;
}
public void setcounter_ex1(String counter_ex1) {
this.counter_ex1 = counter_ex1;
}
public String getcounter_ex2() {
return counter_ex2;
}
public void setcounter_ex2(String counter_ex2) {
this.counter_ex2 = counter_ex2;
}
public String getcounter_ex3() {
return counter_ex3;
}
public void setcounter_ex3(String counter_ex3) {
this.counter_ex3 = counter_ex3;
}
public String getcounter_ex4() {
return counter_ex4;
}
public void setcounter_ex4(String counter_ex4) {
this.counter_ex4 = counter_ex4;
}
public String getcounter_ex5() {
return counter_ex5;
}
public void setcounter_ex5(String counter_ex5) {
this.counter_ex5 = counter_ex5;
}
public String getcounter_ex6() {
return counter_ex6;
}
public void setcounter_ex6(String counter_ex6) {
this.counter_ex6 = counter_ex6;
}
public String getcounter_ex7() {
return counter_ex7;
}
public void setcounter_ex7(String counter_ex7) {
this.counter_ex7 = counter_ex7;
}

}