package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class employeeSalaryService 
{
	private String employee_salary_id;
	private String employee_id;
	private String month;
	private String ewf_loan_amount;
	private String ewf_interest_amount;
	private String personal_loan_amount;
	private String personal_interest_amount;
	private String basic_pay;
	private String hra;
	private String otpay;
	private String medical_insurance;
	private String salary_pay;
	private String extra1;
	private String extra2;
	private String extra3;
	private String extra4;
	private String extra5;
	private String extra6;
	private String extra7;
	private String extra8;
	private String extra9;
	private String extra10;
	private String other_deductions;
	private String other_allowances;
	private String extra11;
	private String extra12;
	private String extra13;
	private String extra14;
	private String extra15;
	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	
	public employeeSalaryService()
	{
		/*try {
			 	// Load the database driver
				c = ConnectionHelper.getConnection();
				st=c.createStatement();
			}catch(Exception e)
			{
				//this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}*/
	}
	
	
	
	public boolean insert()
	{boolean flag = false;
		
		try
		{
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			rs=st.executeQuery("select * from no_genarator where table_name='employee_salary'");
			rs.next();
			int ticket=rs.getInt("table_id");
			String PriSt =rs.getString("id_prefix");
			int ticketm=ticket+1;
			employee_salary_id=PriSt+ticketm;
			 
			String query="insert into employee_salary values('"+employee_salary_id+"','"+employee_id+"','"+month+"','"+ewf_loan_amount+"','"+ewf_interest_amount+"','"+personal_loan_amount+"','"+personal_interest_amount+"','"+basic_pay+"','"+hra+"','"+otpay+"','"+medical_insurance+"','"+salary_pay+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+other_deductions+"','"+other_allowances+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"')";
			System.out.println("Insertion"+query);
			st.executeUpdate(query);
			st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='employee_salary'");
			flag = true;
		}
		catch(Exception e)
		{
			//this.seterror(e.toString())
		}finally{
			try {
				if(rs != null){rs.close();}
				if(st != null){st.close();}
				if(c != null){c.close();}
				} catch (SQLException e) {
				e.printStackTrace();
				}
		}
		return flag;
	}
	

	public employeeSalaryService(String employee_salary_id, String employee_id,
			String month, String ewf_loan_amount, String ewf_interest_amount,
			String personal_loan_amount, String personal_interest_amount,
			String basic_pay, String hra, String otpay,
			String medical_insurance, String salary_pay, String extra1,
			String extra2, String extra3, String extra4, String extra5,
			String extra6, String extra7, String extra8, String extra9,
			String extra10) {
		
		
		
		
		try {
		 	// Load the database driver
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		}catch(Exception e)
		{
			e.printStackTrace();
			//this.seterror(e.toString());
			System.out.println("Exception is ;"+e);
		}
		
		
		
		
		
		this.employee_salary_id = employee_salary_id;
		this.employee_id = employee_id;
		this.month = month;
		this.ewf_loan_amount = ewf_loan_amount;
		this.ewf_interest_amount = ewf_interest_amount;
		this.personal_loan_amount = personal_loan_amount;
		this.personal_interest_amount = personal_interest_amount;
		this.basic_pay = basic_pay;
		this.hra = hra;
		this.otpay = otpay;
		this.medical_insurance = medical_insurance;
		this.salary_pay = salary_pay;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
		this.extra6 = extra6;
		this.extra7 = extra7;
		this.extra8 = extra8;
		this.extra9 = extra9;
		this.extra10 = extra10;
	}
	
	
	public String getEmployee_salary_id() {
		return employee_salary_id;
	}
	public void setEmployee_salary_id(String employee_salary_id) {
		this.employee_salary_id = employee_salary_id;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getEwf_loan_amount() {
		return ewf_loan_amount;
	}
	public void setEwf_loan_amount(String ewf_loan_amount) {
		this.ewf_loan_amount = ewf_loan_amount;
	}
	public String getEwf_interest_amount() {
		return ewf_interest_amount;
	}
	public void setEwf_interest_amount(String ewf_interest_amount) {
		this.ewf_interest_amount = ewf_interest_amount;
	}
	public String getPersonal_loan_amount() {
		return personal_loan_amount;
	}
	public void setPersonal_loan_amount(String personal_loan_amount) {
		this.personal_loan_amount = personal_loan_amount;
	}
	public String getPersonal_interest_amount() {
		return personal_interest_amount;
	}
	public void setPersonal_interest_amount(String personal_interest_amount) {
		this.personal_interest_amount = personal_interest_amount;
	}
	public String getBasic_pay() {
		return basic_pay;
	}
	public void setBasic_pay(String basic_pay) {
		this.basic_pay = basic_pay;
	}
	public String getHra() {
		return hra;
	}
	public void setHra(String hra) {
		this.hra = hra;
	}
	public String getOtpay() {
		return otpay;
	}
	public void setOtpay(String otpay) {
		this.otpay = otpay;
	}
	public String getMedical_insurance() {
		return medical_insurance;
	}
	public void setMedical_insurance(String medical_insurance) {
		this.medical_insurance = medical_insurance;
	}
	public String getSalary_pay() {
		return salary_pay;
	}
	public void setSalary_pay(String salary_pay) {
		this.salary_pay = salary_pay;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}



	public String getOther_deductions() {
		return other_deductions;
	}



	public void setOther_deductions(String other_deductions) {
		this.other_deductions = other_deductions;
	}



	public String getOther_allowances() {
		return other_allowances;
	}



	public void setOther_allowances(String other_allowances) {
		this.other_allowances = other_allowances;
	}



	public String getExtra11() {
		return extra11;
	}



	public void setExtra11(String extra11) {
		this.extra11 = extra11;
	}



	public String getExtra12() {
		return extra12;
	}



	public void setExtra12(String extra12) {
		this.extra12 = extra12;
	}



	public String getExtra13() {
		return extra13;
	}



	public void setExtra13(String extra13) {
		this.extra13 = extra13;
	}



	public String getExtra14() {
		return extra14;
	}



	public void setExtra14(String extra14) {
		this.extra14 = extra14;
	}



	public String getExtra15() {
		return extra15;
	}



	public void setExtra15(String extra15) {
		this.extra15 = extra15;
	}
	
	

}
