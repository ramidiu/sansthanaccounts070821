package beans;

public class godwanstock_sc {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}

private String gsId="";
private String vendorId="";
private String productId="";
private String description="";
private String quantity="";
private String purchaseRate="";
private String date="";
private String MRNo="";
private String billNo="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String vat="";
private String vat1="";
private String profcharges="";
private String emp_id="";
private String department="";
private String checkedby="";
private String checkedtime="";
private String approval_status="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";
private String po_approved_status="";
private String po_approved_by="";
private String po_approved_date="";
private String bill_status="";
private String bill_update_by="";
private String bill_update_time="";
private String extra11="";
private String extra12="";
private String extra13="";
private String extra14="";
private String extra15="";
private String majorhead_id="";
private String minorhead_id="";
private String payment_status="";
private String actualentry_date="";
private String extra16="";
private String extra17="";
private String extra18="";
private String extra19="";
private String extra20="";

public godwanstock_sc() {}
public godwanstock_sc(String gsId, String vendorId, String productId,
		String description, String quantity, String purchaseRate, String date,
		String mRNo, String billNo, String extra1, String extra2,
		String extra3, String extra4, String extra5, String vat, String vat1,
		String profcharges, String emp_id, String department, String checkedby,
		String checkedtime, String approval_status, String extra6,
		String extra7, String extra8, String extra9, String extra10,
		String po_approved_status,String po_approved_by,String po_approved_date,String bill_status,String bill_update_by,
		String bill_update_time,String extra11, String extra12, String extra13, String extra14,
		String extra15,String majorhead_id,String minorhead_id,String payment_status,String actualentry_date,
		String extra16,String extra17,String extra18,String extra19,String extra20) {
	super();
	this.gsId = gsId;
	this.vendorId = vendorId;
	this.productId = productId;
	this.description = description;
	this.quantity = quantity;
	this.purchaseRate = purchaseRate;
	this.date = date;
	MRNo = mRNo;
	this.billNo = billNo;
	this.extra1 = extra1;
	this.extra2 = extra2;
	this.extra3 = extra3;
	this.extra4 = extra4;
	this.extra5 = extra5;
	this.vat = vat;
	this.vat1 = vat1;
	this.profcharges = profcharges;
	this.emp_id = emp_id;
	this.department = department;
	this.checkedby = checkedby;
	this.checkedtime = checkedtime;
	this.approval_status = approval_status;
	this.extra6 = extra6;
	this.extra7 = extra7;
	this.extra8 = extra8;
	this.extra9 = extra9;
	this.extra10 = extra10;
	this.po_approved_status = po_approved_status;
	this.po_approved_by = po_approved_by;
	this.po_approved_date = po_approved_date;
	this.bill_status = bill_status;
	this.bill_update_by = bill_update_by;
	this.bill_update_time = bill_update_time;
	this.extra11 = extra11;
	this.extra12 = extra12;
	this.extra13 = extra13;
	this.extra11 = extra11;
	this.extra12 = extra12;
	this.extra13 = extra13;
	this.extra14 = extra14;
	this.extra15 = extra15;
	this.majorhead_id = majorhead_id;
	this.minorhead_id = minorhead_id;
	this.payment_status = payment_status;
	this.actualentry_date = actualentry_date;
	this.extra16 = extra16;
	this.extra17 = extra17;
	this.extra18 = extra18;
	this.extra19 = extra19;
	this.extra20 = extra20;
	
}
public String getGsId() {
	return gsId;
}
public void setGsId(String gsId) {
	this.gsId = gsId;
}
public String getVendorId() {
	return vendorId;
}
public void setVendorId(String vendorId) {
	this.vendorId = vendorId;
}
public String getProductId() {
	return productId;
}
public void setProductId(String productId) {
	this.productId = productId;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}
public String getPurchaseRate() {
	return purchaseRate;
}
public void setPurchaseRate(String purchaseRate) {
	this.purchaseRate = purchaseRate;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getMRNo() {
	return MRNo;
}
public void setMRNo(String mRNo) {
	MRNo = mRNo;
}
public String getBillNo() {
	return billNo;
}
public void setBillNo(String billNo) {
	this.billNo = billNo;
}
public String getExtra1() {
	return extra1;
}
public void setExtra1(String extra1) {
	this.extra1 = extra1;
}
public String getExtra2() {
	return extra2;
}
public void setExtra2(String extra2) {
	this.extra2 = extra2;
}
public String getExtra3() {
	return extra3;
}
public void setExtra3(String extra3) {
	this.extra3 = extra3;
}
public String getExtra4() {
	return extra4;
}
public void setExtra4(String extra4) {
	this.extra4 = extra4;
}
public String getExtra5() {
	return extra5;
}
public void setExtra5(String extra5) {
	this.extra5 = extra5;
}
public String getVat() {
	return vat;
}
public void setVat(String vat) {
	this.vat = vat;
}
public String getVat1() {
	return vat1;
}
public void setVat1(String vat1) {
	this.vat1 = vat1;
}
public String getProfcharges() {
	return profcharges;
}
public void setProfcharges(String profcharges) {
	this.profcharges = profcharges;
}
public String getEmp_id() {
	return emp_id;
}
public void setEmp_id(String emp_id) {
	this.emp_id = emp_id;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department = department;
}
public String getCheckedby() {
	return checkedby;
}
public void setCheckedby(String checkedby) {
	this.checkedby = checkedby;
}
public String getCheckedtime() {
	return checkedtime;
}
public void setCheckedtime(String checkedtime) {
	this.checkedtime = checkedtime;
}
public String getApproval_status() {
	return approval_status;
}
public void setApproval_status(String approval_status) {
	this.approval_status = approval_status;
}
public String getExtra6() {
	return extra6;
}
public void setExtra6(String extra6) {
	this.extra6 = extra6;
}
public String getExtra7() {
	return extra7;
}
public void setExtra7(String extra7) {
	this.extra7 = extra7;
}
public String getExtra8() {
	return extra8;
}
public void setExtra8(String extra8) {
	this.extra8 = extra8;
}
public String getExtra9() {
	return extra9;
}
public void setExtra9(String extra9) {
	this.extra9 = extra9;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
public String getPo_approved_status() {
	return po_approved_status;
}
public void setPo_approved_status(String po_approved_status) {
	this.po_approved_status = po_approved_status;
}
public String getPo_approved_by() {
	return po_approved_by;
}
public void setPo_approved_by(String po_approved_by) {
	this.po_approved_by = po_approved_by;
}
public String getPo_approved_date() {
	return po_approved_date;
}
public void setPo_approved_date(String po_approved_date) {
	this.po_approved_date = po_approved_date;
}
public String getBill_status() {
	return bill_status;
}
public void setBill_status(String bill_status) {
	this.bill_status = bill_status;
}
public String getBill_update_by() {
	return bill_update_by;
}
public void setBill_update_by(String bill_update_by) {
	this.bill_update_by = bill_update_by;
}
public String getBill_update_time() {
	return bill_update_time;
}
public void setBill_update_time(String bill_update_time) {
	this.bill_update_time = bill_update_time;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}
public String getExtra14() {
	return extra14;
}
public void setExtra14(String extra14) {
	this.extra14 = extra14;
}
public String getExtra15() {
	return extra15;
}
public void setExtra15(String extra15) {
	this.extra15 = extra15;
}
public String getMajorhead_id() {
	return majorhead_id;
}
public void setMajorhead_id(String majorhead_id) {
	this.majorhead_id = majorhead_id;
}
public String getMinorhead_id() {
	return minorhead_id;
}
public void setMinorhead_id(String minorhead_id) {
	this.minorhead_id = minorhead_id;
}
public String getPayment_status() {
	return payment_status;
}
public void setPayment_status(String payment_status) {
	this.payment_status = payment_status;
}
public String getActualentry_date() {
	return actualentry_date;
}
public void setActualentry_date(String actualentry_date) {
	this.actualentry_date = actualentry_date;
}
public String getExtra16() {
	return extra16;
}
public void setExtra16(String extra16) {
	this.extra16 = extra16;
}
public String getExtra17() {
	return extra17;
}
public void setExtra17(String extra17) {
	this.extra17 = extra17;
}
public String getExtra18() {
	return extra18;
}
public void setExtra18(String extra18) {
	this.extra18 = extra18;
}
public String getExtra19() {
	return extra19;
}
public void setExtra19(String extra19) {
	this.extra19 = extra19;
}
public String getExtra20() {
	return extra20;
}
public void setExtra20(String extra20) {
	this.extra20 = extra20;
}



}
