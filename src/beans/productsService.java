package beans;

import java.sql.Connection;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class productsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String productId="";
private String productName="";
private String description="";
private String purchaseAmmount="";
private String sellingAmmount="";
private String head_account_id="";
private String major_head_id="";
private String sub_head_id="";
private String reorderLevel="";
private String balanceQuantityStore="";
private String balanceQuantityGodwan="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String vat="";
private String vat1="";
private String profcharges="";
private String units="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";

private Statement st = null;
private Connection c=null;
public productsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setproductId(String productId)
{
this.productId = productId;
}

public String getproductId()
{
return (this.productId);
}


public void setproductName(String productName)
{
this.productName = productName;
}

public String getproductName()
{
return (this.productName);
}


public void setdescription(String description)
{
this.description = description;
}

public String getdescription()
{
return (this.description);
}


public void setpurchaseAmmount(String purchaseAmmount)
{
this.purchaseAmmount = purchaseAmmount;
}

public String getpurchaseAmmount()
{
return (this.purchaseAmmount);
}


public void setsellingAmmount(String sellingAmmount)
{
this.sellingAmmount = sellingAmmount;
}

public String getsellingAmmount()
{
return (this.sellingAmmount);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setmajor_head_id(String major_head_id)
{
this.major_head_id = major_head_id;
}

public String getmajor_head_id()
{
return (this.major_head_id);
}


public void setsub_head_id(String sub_head_id)
{
this.sub_head_id = sub_head_id;
}

public String getsub_head_id()
{
return (this.sub_head_id);
}


public void setreorderLevel(String reorderLevel)
{
this.reorderLevel = reorderLevel;
}

public String getreorderLevel()
{
return (this.reorderLevel);
}


public void setbalanceQuantityStore(String balanceQuantityStore)
{
this.balanceQuantityStore = balanceQuantityStore;
}

public String getbalanceQuantityStore()
{
return (this.balanceQuantityStore);
}


public void setbalanceQuantityGodwan(String balanceQuantityGodwan)
{
this.balanceQuantityGodwan = balanceQuantityGodwan;
}

public String getbalanceQuantityGodwan()
{
return (this.balanceQuantityGodwan);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void setvat(String vat)
{
this.vat = vat;
}

public String getvat()
{
return (this.vat);
}


public void setvat1(String vat1)
{
this.vat1 = vat1;
}

public String getvat1()
{
return (this.vat1);
}


public void setprofcharges(String profcharges)
{
this.profcharges = profcharges;
}

public String getprofcharges()
{
return (this.profcharges);
}


public void setunits(String units)
{
this.units = units;
}

public String getunits()
{
return (this.units);
}


public void setextra6(String extra6)
{
this.extra6 = extra6;
}

public String getextra6()
{
return (this.extra6);
}


public void setextra7(String extra7)
{
this.extra7 = extra7;
}

public String getextra7()
{
return (this.extra7);
}


public void setextra8(String extra8)
{
this.extra8 = extra8;
}

public String getextra8()
{
return (this.extra8);
}


public void setextra9(String extra9)
{
this.extra9 = extra9;
}

public String getextra9()
{
return (this.extra9);
}


public void setextra10(String extra10)
{
this.extra10 = extra10;
}

public String getextra10()
{
return (this.extra10);
}

public boolean insert()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
/*rs=st.executeQuery("select * from no_genarator where table_name='products'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 productId=PriSt+ticketm;*/
String query="insert into products values('"+productId+"','"+productName+"','"+description+"','"+purchaseAmmount+"','"+sellingAmmount+"','"+head_account_id+"','"+major_head_id+"','"+sub_head_id+"','"+reorderLevel+"','"+balanceQuantityStore+"','"+balanceQuantityGodwan+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+vat+"','"+vat1+"','"+profcharges+"','"+units+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"')";
st.executeUpdate(query);
System.out.println(query);
//st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='products'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}

public boolean update()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!productName.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"productName = '"+productName+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",productName = '"+productName+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!purchaseAmmount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"purchaseAmmount = '"+purchaseAmmount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",purchaseAmmount = '"+purchaseAmmount+"'";}
}

if(!sellingAmmount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"sellingAmmount = '"+sellingAmmount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",sellingAmmount = '"+sellingAmmount+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

if(!major_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
}

if(!sub_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
}

if(!reorderLevel.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"reorderLevel = '"+reorderLevel+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",reorderLevel = '"+reorderLevel+"'";}
}

if(!balanceQuantityStore.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"balanceQuantityStore = '"+balanceQuantityStore+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",balanceQuantityStore = '"+balanceQuantityStore+"'";}
}

if(!balanceQuantityGodwan.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"balanceQuantityGodwan = '"+balanceQuantityGodwan+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",balanceQuantityGodwan = '"+balanceQuantityGodwan+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!vat.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat = '"+vat+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat = '"+vat+"'";}
}

if(!vat1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat1 = '"+vat1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat1 = '"+vat1+"'";}
}

if(!profcharges.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"profcharges = '"+profcharges+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",profcharges = '"+profcharges+"'";}
}

if(!units.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"units = '"+units+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",units = '"+units+"'";}
}

if(!extra6.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra6 = '"+extra6+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra6 = '"+extra6+"'";}
}

if(!extra7.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra7 = '"+extra7+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra7 = '"+extra7+"'";}
}

if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}

if(!extra9.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra9 = '"+extra9+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra9 = '"+extra9+"'";}
}

if(!extra10.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra10 = '"+extra10+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra10 = '"+extra10+"'";}
}
String query="update products "+updatecc+" where productId='"+productId+"'";
System.out.println(query);
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}
}
public boolean updateWithHOA()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!productName.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"productName = '"+productName+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",productName = '"+productName+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!purchaseAmmount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"purchaseAmmount = '"+purchaseAmmount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",purchaseAmmount = '"+purchaseAmmount+"'";}
}

if(!sellingAmmount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"sellingAmmount = '"+sellingAmmount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",sellingAmmount = '"+sellingAmmount+"'";}
}



if(!major_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"major_head_id = '"+major_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",major_head_id = '"+major_head_id+"'";}
}

if(!sub_head_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"sub_head_id = '"+sub_head_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",sub_head_id = '"+sub_head_id+"'";}
}

if(!reorderLevel.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"reorderLevel = '"+reorderLevel+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",reorderLevel = '"+reorderLevel+"'";}
}

if(!balanceQuantityStore.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"balanceQuantityStore = '"+balanceQuantityStore+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",balanceQuantityStore = '"+balanceQuantityStore+"'";}
}

if(!balanceQuantityGodwan.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"balanceQuantityGodwan = '"+balanceQuantityGodwan+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",balanceQuantityGodwan = '"+balanceQuantityGodwan+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!vat.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat = '"+vat+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat = '"+vat+"'";}
}

if(!vat1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vat1 = '"+vat1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vat1 = '"+vat1+"'";}
}

if(!profcharges.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"profcharges = '"+profcharges+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",profcharges = '"+profcharges+"'";}
}

if(!units.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"units = '"+units+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",units = '"+units+"'";}
}

if(!extra6.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra6 = '"+extra6+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra6 = '"+extra6+"'";}
}

if(!extra7.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra7 = '"+extra7+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra7 = '"+extra7+"'";}
}

if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}

if(!extra9.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra9 = '"+extra9+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra9 = '"+extra9+"'";}
}

if(!extra10.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra10 = '"+extra10+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra10 = '"+extra10+"'";}
}

String query="update products "+updatecc+" where productId='"+productId+"' and head_account_id='"+head_account_id+"'";
System.out.println("update products updateWithHOA================================>"+query);
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}
}
public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from products where productId='"+productId+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
public boolean updateVarriationStock(String prid,String storestock,String godstck,String hoa)
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String query="update  products set balanceQuantityStore='"+storestock+"',balanceQuantityGodwan='"+godstck+"' where productId='"+prid+"' and head_account_id='"+hoa+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}

public boolean updateStock(String prid,String storestock,String godstck,String hoa)
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	String subqry = "set ";
	int jk=0;
	if(!storestock.trim().equals("")){ 
		if(jk==0)
		{
		subqry=subqry+"balanceQuantityStore = '"+storestock+"'"; 
		jk=1; 
		}else
		{
			subqry=subqry+",balanceQuantityStore = '"+storestock+"'";
		}
	}
	
	if(!godstck.trim().equals("")){ 
		if(jk==0)
		{
		subqry=subqry+"balanceQuantityGodwan = '"+godstck+"'"; 
		jk=1; 
		}else
		{
			subqry=subqry+",balanceQuantityGodwan = '"+godstck+"'";
		}
	}

String query="update products "+subqry+" where productId='"+prid+"' and head_account_id='"+hoa+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}

// below update method is used to update the purchacheAmount in Product table
public int updatePriceBasedOnId(String pId,String price)
{
	int i=0;
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String query="update  products set purchaseAmmount='"+price+"' where productId='"+pId+"'";
System.out.println("updatePriceBasedOnId=====>"+query);
i =st.executeUpdate(query);
st.close();
c.close();

}catch(Exception e){
	e.printStackTrace();
}
return i;

}

}