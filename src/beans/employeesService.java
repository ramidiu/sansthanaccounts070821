package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class employeesService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String emp_id="";
private String firstname="";
private String email="";
private String user_name="";
private String password="";
private String lastname="";
private String join_date="";
private String emp_sal="";
private String emp_address="";
private String emp_phone="";
private String headAccountId="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public employeesService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setemp_id(String emp_id)
{
this.emp_id = emp_id;
}

public String getemp_id()
{
return (this.emp_id);
}


public void setfirstname(String firstname)
{
this.firstname = firstname;
}

public String getfirstname()
{
return (this.firstname);
}


public void setemail(String email)
{
this.email = email;
}

public String getemail()
{
return (this.email);
}


public void setuser_name(String user_name)
{
this.user_name = user_name;
}

public String getuser_name()
{
return (this.user_name);
}


public void setpassword(String password)
{
this.password = password;
}

public String getpassword()
{
return (this.password);
}


public void setlastname(String lastname)
{
this.lastname = lastname;
}

public String getlastname()
{
return (this.lastname);
}


public void setjoin_date(String join_date)
{
this.join_date = join_date;
}

public String getjoin_date()
{
return (this.join_date);
}


public void setemp_sal(String emp_sal)
{
this.emp_sal = emp_sal;
}

public String getemp_sal()
{
return (this.emp_sal);
}


public void setemp_address(String emp_address)
{
this.emp_address = emp_address;
}

public String getemp_address()
{
return (this.emp_address);
}


public void setemp_phone(String emp_phone)
{
this.emp_phone = emp_phone;
}

public String getemp_phone()
{
return (this.emp_phone);
}


public void setheadAccountId(String headAccountId)
{
this.headAccountId = headAccountId;
}

public String getheadAccountId()
{
return (this.headAccountId);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void setextra6(String extra6)
{
this.extra6 = extra6;
}

public String getextra6()
{
return (this.extra6);
}


public void setextra7(String extra7)
{
this.extra7 = extra7;
}

public String getextra7()
{
return (this.extra7);
}


public void setextra8(String extra8)
{
this.extra8 = extra8;
}

public String getextra8()
{
return (this.extra8);
}


public void setextra9(String extra9)
{
this.extra9 = extra9;
}

public String getextra9()
{
return (this.extra9);
}


public void setextra10(String extra10)
{
this.extra10 = extra10;
}

public String getextra10()
{
return (this.extra10);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='employees'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 emp_id=PriSt+ticketm;
String query="insert into employees values('"+emp_id+"','"+firstname+"','"+email+"','"+user_name+"','"+password+"','"+lastname+"','"+join_date+"','"+emp_sal+"','"+emp_address+"','"+emp_phone+"','"+headAccountId+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"')";
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='employees'");
flag = true;
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!firstname.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"firstname = '"+firstname+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",firstname = '"+firstname+"'";}
}

if(!email.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"email = '"+email+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",email = '"+email+"'";}
}

if(!user_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"user_name = '"+user_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",user_name = '"+user_name+"'";}
}

if(!password.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"password = '"+password+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",password = '"+password+"'";}
}

if(!lastname.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"lastname = '"+lastname+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",lastname = '"+lastname+"'";}
}

if(!join_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"join_date = '"+join_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",join_date = '"+join_date+"'";}
}

if(!emp_sal.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_sal = '"+emp_sal+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_sal = '"+emp_sal+"'";}
}

if(!emp_address.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_address = '"+emp_address+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_address = '"+emp_address+"'";}
}

if(!emp_phone.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"emp_phone = '"+emp_phone+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",emp_phone = '"+emp_phone+"'";}
}

if(!headAccountId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"headAccountId = '"+headAccountId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",headAccountId = '"+headAccountId+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!extra6.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra6 = '"+extra6+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra6 = '"+extra6+"'";}
}

if(!extra7.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra7 = '"+extra7+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra7 = '"+extra7+"'";}
}

if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}

if(!extra9.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra9 = '"+extra9+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra9 = '"+extra9+"'";}
}

if(!extra10.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra10 = '"+extra10+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra10 = '"+extra10+"'";}
}
String query="update employees "+updatecc+" where emp_id='"+emp_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from employees where emp_id='"+emp_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}