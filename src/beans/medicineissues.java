package beans;

import java.io.Serializable;

public class medicineissues implements Serializable 
 {

	private static final long serialVersionUID = 1L;
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String documentNo="";
private String date="";
private String appointmnetId="";
private String tabletName="";
private String quantity="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private Double consumption;
public medicineissues() {} 
public medicineissues(String documentNo,String date,String appointmnetId,String tabletName,String quantity,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.documentNo=documentNo;
this.date=date;
this.appointmnetId=appointmnetId;
this.tabletName=tabletName;
this.quantity=quantity;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getdocumentNo() {
return documentNo;
}
public void setdocumentNo(String documentNo) {
this.documentNo = documentNo;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String getappointmnetId() {
return appointmnetId;
}
public void setappointmnetId(String appointmnetId) {
this.appointmnetId = appointmnetId;
}
public String gettabletName() {
return tabletName;
}
public void settabletName(String tabletName) {
this.tabletName = tabletName;
}
public String getquantity() {
return quantity;
}
public void setquantity(String quantity) {
this.quantity = quantity;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public Double getConsumption() {
	return consumption;
}
public void setConsumption(Double consumption) {
	this.consumption = consumption;
}
@Override
public String toString() {
	return "medicineissues [error=" + error + ", documentNo=" + documentNo + ", date=" + date + ", appointmnetId="
			+ appointmnetId + ", tabletName=" + tabletName + ", quantity=" + quantity + ", extra1=" + extra1
			+ ", extra2=" + extra2 + ", extra3=" + extra3 + ", extra4=" + extra4 + ", extra5=" + extra5
			+ ", consumption=" + consumption + "]";
}


}