package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class indentService {
	private String error = "";

	public void seterror(String error) {
		this.error = error;
	}

	public String geterror() {
		return this.error;
	}

	private String indent_id = "";
	private String date = "";
	private String description = "";
	private String status = "";
	private String indentinvoice_id = "";
	private String requirement = "";
	private String require_date = "";
	private String head_account_id = "";
	private String product_id = "";
	private String tablet_id = "";
	private String quantity = "";
	private String vendor_id = "";
	private String emp_id = "";
	private String extra1 = "";
	private String extra2 = "";
	private String extra3 = "";
	private String extra4 = "";
	private String extra5 = "";

	private ResultSet rs = null;
	private Statement st = null;
	private Connection c = null;

	public indentService() {
		/*
		 * try { // Load the database driver c =
		 * ConnectionHelper.getConnection(); st=c.createStatement();
		 * }catch(Exception e){ this.seterror(e.toString());
		 * System.out.println("Exception is ;"+e); }
		 */
	}

	public void setindent_id(String indent_id) {
		this.indent_id = indent_id;
	}

	public String getindent_id() {
		return (this.indent_id);
	}

	public void setdate(String date) {
		this.date = date;
	}

	public String getdate() {
		return (this.date);
	}

	public void setdescription(String description) {
		this.description = description;
	}

	public String getdescription() {
		return (this.description);
	}

	public void setstatus(String status) {
		this.status = status;
	}

	public String getstatus() {
		return (this.status);
	}

	public void setindentinvoice_id(String indentinvoice_id) {
		this.indentinvoice_id = indentinvoice_id;
	}

	public String getindentinvoice_id() {
		return (this.indentinvoice_id);
	}

	public void setrequirement(String requirement) {
		this.requirement = requirement;
	}

	public String getrequirement() {
		return (this.requirement);
	}

	public void setrequire_date(String require_date) {
		this.require_date = require_date;
	}

	public String getrequire_date() {
		return (this.require_date);
	}

	public void sethead_account_id(String head_account_id) {
		this.head_account_id = head_account_id;
	}

	public String gethead_account_id() {
		return (this.head_account_id);
	}

	public void setproduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getproduct_id() {
		return (this.product_id);
	}

	public void settablet_id(String tablet_id) {
		this.tablet_id = tablet_id;
	}

	public String gettablet_id() {
		return (this.tablet_id);
	}

	public void setquantity(String quantity) {
		this.quantity = quantity;
	}

	public String getquantity() {
		return (this.quantity);
	}

	public void setvendor_id(String vendor_id) {
		this.vendor_id = vendor_id;
	}

	public String getvendor_id() {
		return (this.vendor_id);
	}

	public void setemp_id(String emp_id) {
		this.emp_id = emp_id;
	}

	public String getemp_id() {
		return (this.emp_id);
	}

	public void setextra1(String extra1) {
		this.extra1 = extra1;
	}

	public String getextra1() {
		return (this.extra1);
	}

	public void setextra2(String extra2) {
		this.extra2 = extra2;
	}

	public String getextra2() {
		return (this.extra2);
	}

	public void setextra3(String extra3) {
		this.extra3 = extra3;
	}

	public String getextra3() {
		return (this.extra3);
	}

	public void setextra4(String extra4) {
		this.extra4 = extra4;
	}

	public String getextra4() {
		return (this.extra4);
	}

	public void setextra5(String extra5) {
		this.extra5 = extra5;
	}

	public String getextra5() {
		return (this.extra5);
	}

	public boolean insert() {
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			int result=0,result1=0;
			ResultSet rs1 = st.executeQuery("select * from no_genarator where table_name='indent'");
			rs1.next();
			System.out.println("===========> "+rs1.getInt("table_id"));
			
			int ticket = rs1.getInt("table_id");
			String PriSt = rs1.getString("id_prefix");
			
			System.out.println("ticketticket =====> "+ticket);
			rs1.close();
			int ticketm = ticket + 1;
			indent_id = PriSt + ticketm;
			
			System.out.println("ticketmticketm =====> "+ticketm);
			
			System.out.println("indent_idindent_id =====> "+indent_id);
			
			String query = "insert into indent values('" + indent_id + "','"
					+ date + "','" + description + "','" + status + "','"
					+ indentinvoice_id + "','" + requirement + "','"
					+ require_date + "','" + head_account_id + "','"
					+ product_id + "','" + tablet_id + "','" + quantity + "','"
					+ vendor_id + "','" + emp_id + "','" + extra1 + "','"
					+ extra2 + "','" + extra3 + "','" + extra4 + "','" + extra5
					+ "')";
			System.out.println("indent raised values are insert" + query);
			result=st.executeUpdate(query);
			System.out.println("resultresultresultresult =====> "+result);
			result1=st.executeUpdate("update no_genarator set table_id='" + ticketm +"' where table_name='indent'");
			System.out.println("sresult fot inserting in to indents table=========>"+result);
			System.out.println();
			System.out.println("updating no generator table with id=========>"+result1);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			this.seterror(e.toString());
			return false;
		}
		finally	{
			try	{
				st.close();
			}
			catch(Exception e)	{
				e.printStackTrace();
			}
			try	{
				c.close();
			}
			catch(Exception e)	{
				e.printStackTrace();
			}
			
		}
		
	}

	public boolean update() {
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			/* System.out.println("update"); */
			String updatecc = "set ";
			int jk = 0;
			if (!date.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "date = '" + date + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",date = '" + date + "'";
				}
			}

			if (!description.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "description = '" + description + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",description = '" + description
							+ "'";
				}
			}

			if (!status.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "status = '" + status + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",status = '" + status + "'";
				}
			}

			if (!indentinvoice_id.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "indentinvoice_id = '"
							+ indentinvoice_id + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",indentinvoice_id = '"
							+ indentinvoice_id + "'";
				}
			}

			if (!requirement.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "requirement = '" + requirement + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",requirement = '" + requirement
							+ "'";
				}
			}

			if (!require_date.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "require_date = '" + require_date
							+ "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",require_date = '" + require_date
							+ "'";
				}
			}

			if (!head_account_id.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "head_account_id = '"
							+ head_account_id + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",head_account_id = '"
							+ head_account_id + "'";
				}
			}

			if (!product_id.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "product_id = '" + product_id + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",product_id = '" + product_id + "'";
				}
			}

			if (!tablet_id.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "tablet_id = '" + tablet_id + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",tablet_id = '" + tablet_id + "'";
				}
			}

			if (!quantity.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "quantity = '" + quantity + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",quantity = '" + quantity + "'";
				}
			}

			if (!vendor_id.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "vendor_id = '" + vendor_id + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",vendor_id = '" + vendor_id + "'";
				}
			}

			if (!emp_id.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "emp_id = '" + emp_id + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",emp_id = '" + emp_id + "'";
				}
			}

			if (!extra1.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "extra1 = '" + extra1 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",extra1 = '" + extra1 + "'";
				}
			}

			if (!extra2.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "extra2 = '" + extra2 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",extra2 = '" + extra2 + "'";
				}
			}

			if (!extra3.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "extra3 = '" + extra3 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",extra3 = '" + extra3 + "'";
				}
			}

			if (!extra4.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "extra4 = '" + extra4 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",extra4 = '" + extra4 + "'";
				}
			}

			if (!extra5.equals("")) {
				if (jk == 0) {
					updatecc = updatecc + "extra5 = '" + extra5 + "'";
					jk = 1;
				} else {
					updatecc = updatecc + ",extra5 = '" + extra5 + "'";
				}
			}
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query = "update indent " + updatecc + " where indent_id='"
					+ indent_id + "'";
			System.out.println("indentService update query======>" + query);
			st.executeUpdate(query);
			st.close();
			c.close();
			return true;
		} catch (Exception e) {
			this.seterror(e.toString());
			return false;
		}

	}

	public boolean updateIndent(String indentid,String status,String vendorid,String qty)	{
		String query = "";
		if(indentid != null && !indentid.equals(""))	{
			try	{
				c = ConnectionHelper.getConnection();
				st = c.createStatement();
				query = "update indent set status='"+status+"', vendor_id='"+vendorid+"' , quantity='"+qty+"' where indent_id='"+indentid+"'";
//				System.out.println("updateIndent----"+query);
				st.execute(query);
				return true;
			}
			catch(SQLException se)	{
				se.printStackTrace();
			}
			catch(Exception e)	{
				e.printStackTrace();
			}
			finally	{
				try {
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		 return false;
	}
	public boolean updateStatus(String indentinv_id, String status) {
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query = "update indent set status='" + status
					+ "' where indentinvoice_id='" + indentinv_id + "'";
			System.out.println("indent update status===>" + query);
			st.executeUpdate(query);
			st.close();
			c.close();
			return true;
		} catch (Exception e) {
			this.seterror(e.toString());
			return false;
		}
	}

	public boolean updateTrackingNo(String indentid, String trackingNO) {
		int count;
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query = "update indent set extra4='" + trackingNO + "' where indent_id='" + indentid + "'";
//			System.out.println("updateTrackingNo===>" + query);
			count = st.executeUpdate(query);
//			System.out.println("updated----"+count);
			st.close();
			c.close();
			if(count == 1) return true;
			else return false;
		} catch (Exception e) {
			this.seterror(e.toString());
			return false;
		}
	}
	public boolean delete() {
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query = "delete from indent where indent_id='" + indent_id
					+ "'";
			st.executeUpdate(query);
			st.close();
			c.close();
			return true;
		} catch (Exception e) {
			this.seterror(e.toString());
			return false;
		}

	}

	public boolean deleteProduct(String id) {
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query = "delete from indent where indent_id='" + id + "'";
			st.executeUpdate(query);
			st.close();
			c.close();
			return true;
		} catch (Exception e) {
			this.seterror(e.toString());
			return false;
		}

	}

	public boolean deleteIndentInvoice(String indentinvoice_id) {
		try {
			c = ConnectionHelper.getConnection();
			st = c.createStatement();
			String query = "delete from indent where indentinvoice_id='"
					+ indentinvoice_id + "'";
			/* System.out.println(query); */
			st.executeUpdate(query);
			st.close();
			c.close();
			return true;
		} catch (Exception e) {
			this.seterror(e.toString());
			return false;
		}

	}
}