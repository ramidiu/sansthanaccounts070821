package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;


public class daily_bill_statusService 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String bill_id="";
	private String createdBy="";
	private String createdDate="";
	private String fileName="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	public String getBill_id() {
		return bill_id;
	}
	public void setBill_id(String bill_id) {
		this.bill_id = bill_id;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	
	private Statement st = null;
	private Connection c=null;
	private ResultSet rs=null;
	public daily_bill_statusService()
	{
	/*try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	}catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}*/
	 }
	
	public boolean insert()
	{boolean flag = false;
	try
	{
	/*rs=st.executeQuery("select * from no_genarator where table_name='daily_bill_status'");
	rs.next();
	int ticket=rs.getInt("table_id");
	String PriSt =rs.getString("id_prefix");
	rs.close();
	int ticketm=ticket+1;
	bill_id=PriSt+ticketm;*/
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	String query="insert into daily_bill_status values('"+bill_id+"','"+createdBy+"','"+createdDate+"','"+fileName+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";
	System.out.println(query);
	int i = st.executeUpdate(query);
	/*st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='daily_bill_status'");*/
	if(i>0){flag=true;}
	}catch(Exception e){
	this.seterror(e.toString());
	}finally{
		try {
			if(rs != null){rs.close();}
			if(st != null){st.close();}
			if(c != null){c.close();}
			} catch (SQLException e) {
			e.printStackTrace();
			}
	}
	return flag;
	}
	
	public boolean delete()
	{boolean flag = false;
	try
	{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();	
	String query="delete from daily_bill_status where bill_id='"+bill_id+"'";
	int i = st.executeUpdate(query);
	if(i>0){flag=true;}
	}catch(Exception e){
	this.seterror(e.toString());
	}finally{
		try {
			if(rs != null){rs.close();}
			if(st != null){st.close();}
			if(c != null){c.close();}
			} catch (SQLException e) {
			e.printStackTrace();
			}
	}
	return flag;

	}
	
	
}
