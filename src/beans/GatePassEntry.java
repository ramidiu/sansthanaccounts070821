package beans;

import java.io.Serializable;

public class GatePassEntry  implements Serializable{

	private static final long serialVersionUID = 1L;

	private String gatePassEntryId;
	private String staffId;
	private String staffName;
	private String department;
	private String description;
	private String inTime;
	private String outtime;
	private String createdDate;
	private String updatedDate;
	private double clnumber;
	private String clType;
	private String gatePassType;
	public String getGatePassEntryId() {
		return gatePassEntryId;
	}
	public void setGatePassEntryId(String gatePassEntryId) {
		this.gatePassEntryId = gatePassEntryId;
	}
	public String getStaffId() {
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getInTime() {
		return inTime;
	}
	public void setInTime(String inTime) {
		this.inTime = inTime;
	}
	public String getOuttime() {
		return outtime;
	}
	public void setOuttime(String outtime) {
		this.outtime = outtime;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public double getClnumber() {
		return clnumber;
	}
	public void setClnumber(double clnumber) {
		this.clnumber = clnumber;
	}
	public String getClType() {
		return clType;
	}
	public void setClType(String clType) {
		this.clType = clType;
	}
	public String getGatePassType() {
		return gatePassType;
	}
	public void setGatePassType(String gatePassType) {
		this.gatePassType = gatePassType;
	}
	@Override
	public String toString() {
		return "GatePassEntry [gatePassEntryId=" + gatePassEntryId + ", staffId=" + staffId + ", staffName=" + staffName
				+ ", department=" + department + ", description=" + description + ", inTime=" + inTime + ", outtime="
				+ outtime + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + ", clnumber=" + clnumber
				+ ", clType=" + clType + ", gatePassType=" + gatePassType + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clType == null) ? 0 : clType.hashCode());
		long temp;
		temp = Double.doubleToLongBits(clnumber);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((gatePassEntryId == null) ? 0 : gatePassEntryId.hashCode());
		result = prime * result + ((gatePassType == null) ? 0 : gatePassType.hashCode());
		result = prime * result + ((inTime == null) ? 0 : inTime.hashCode());
		result = prime * result + ((outtime == null) ? 0 : outtime.hashCode());
		result = prime * result + ((staffId == null) ? 0 : staffId.hashCode());
		result = prime * result + ((staffName == null) ? 0 : staffName.hashCode());
		result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GatePassEntry other = (GatePassEntry) obj;
		if (clType == null) {
			if (other.clType != null)
				return false;
		} else if (!clType.equals(other.clType))
			return false;
		if (Double.doubleToLongBits(clnumber) != Double.doubleToLongBits(other.clnumber))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (gatePassEntryId == null) {
			if (other.gatePassEntryId != null)
				return false;
		} else if (!gatePassEntryId.equals(other.gatePassEntryId))
			return false;
		if (gatePassType == null) {
			if (other.gatePassType != null)
				return false;
		} else if (!gatePassType.equals(other.gatePassType))
			return false;
		if (inTime == null) {
			if (other.inTime != null)
				return false;
		} else if (!inTime.equals(other.inTime))
			return false;
		if (outtime == null) {
			if (other.outtime != null)
				return false;
		} else if (!outtime.equals(other.outtime))
			return false;
		if (staffId == null) {
			if (other.staffId != null)
				return false;
		} else if (!staffId.equals(other.staffId))
			return false;
		if (staffName == null) {
			if (other.staffName != null)
				return false;
		} else if (!staffName.equals(other.staffName))
			return false;
		if (updatedDate == null) {
			if (other.updatedDate != null)
				return false;
		} else if (!updatedDate.equals(other.updatedDate))
			return false;
		return true;
	}
	
	
}
