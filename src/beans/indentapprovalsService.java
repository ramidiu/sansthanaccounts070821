package beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class indentapprovalsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String approve_id="";
private String admin_id="";
private String description="";
private String admin_status="";
private String approved_date="";
private String indentinvoice_id="";
private String extra1="";
private String extra2="";
private String extra3="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public indentapprovalsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setapprove_id(String approve_id)
{
this.approve_id = approve_id;
}

public String getapprove_id()
{
return (this.approve_id);
}


public void setadmin_id(String admin_id)
{
this.admin_id = admin_id;
}

public String getadmin_id()
{
return (this.admin_id);
}


public void setdescription(String description)
{
this.description = description;
}

public String getdescription()
{
return (this.description);
}


public void setadmin_status(String admin_status)
{
this.admin_status = admin_status;
}

public String getadmin_status()
{
return (this.admin_status);
}


public void setapproved_date(String approved_date)
{
this.approved_date = approved_date;
}

public String getapproved_date()
{
return (this.approved_date);
}


public void setindentinvoice_id(String indentinvoice_id)
{
this.indentinvoice_id = indentinvoice_id;
}

public String getindentinvoice_id()
{
return (this.indentinvoice_id);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}

public boolean insert()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='indentapprovals'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 approve_id=PriSt+ticketm;
String query="insert into indentapprovals values('"+approve_id+"','"+admin_id+"','"+description+"','"+admin_status+"','"+approved_date+"','"+indentinvoice_id+"','"+extra1+"','"+extra2+"','"+extra3+"')";
System.out.println("inserting indentapprovals 111111111111 ==========> "+query);
int i=st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='indentapprovals'");

st.close();
c.close();
return true;
}catch(Exception e){
	e.printStackTrace();
return false;
}
}

public int insertIndApp(indentapprovals app)	{
	String insert_query = "insert into indentapprovals values (?,?,?,?,?,?,?,?,?)";
	String update_query = "update no_genarator set table_id = ? where table_name='indentapprovals'";
	PreparedStatement ps = null;
	String approve_id = "";
	int result=0;
	try	{
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
		ps = c.prepareStatement(insert_query);
		rs=st.executeQuery("select * from no_genarator where table_name='indentapprovals'");
		rs.next();
		int ticket=rs.getInt("table_id");
		String PriSt =rs.getString("id_prefix");
		rs.close();
		int ticketm=ticket+1;
		approve_id=PriSt+ticketm;
		
		ps.setString(1,approve_id);
		ps.setString(2,app.getadmin_id());
		ps.setString(3,app.getdescription());
		ps.setString(4,app.getadmin_status());
		ps.setString(5,app.getapproved_date());
		ps.setString(6,app.getindentinvoice_id());
		ps.setString(7,app.getextra1());
		ps.setString(8,app.getextra2());
		ps.setString(9,app.getextra3());
		
		result = ps.executeUpdate();
		System.out.println("insert indent app----"+result);
		ps.close();
		
		ps = c.prepareStatement(update_query);
		ps.setInt(1,ticketm);
		int result2 = ps.executeUpdate();
		System.out.println("update indent app-----"+result2);
	}
	catch(SQLException se)	{
		se.printStackTrace();
	}
	catch(Exception e)	{
		e.printStackTrace();
	}
	finally	{
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return result;
}

public boolean update()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!admin_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"admin_id = '"+admin_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",admin_id = '"+admin_id+"'";}
}

if(!description.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"description = '"+description+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",description = '"+description+"'";}
}

if(!admin_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"admin_status = '"+admin_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",admin_status = '"+admin_status+"'";}
}

if(!approved_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"approved_date = '"+approved_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",approved_date = '"+approved_date+"'";}
}

if(!indentinvoice_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"indentinvoice_id = '"+indentinvoice_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",indentinvoice_id = '"+indentinvoice_id+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}
String query="update indentapprovals "+updatecc+" where approve_id='"+approve_id+"'";
System.out.println("updatequery   indentapprovals1111111111111111111111111111     ==>"+query);
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from indentapprovals where approve_id='"+approve_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}