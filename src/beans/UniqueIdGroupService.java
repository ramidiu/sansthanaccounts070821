package beans;

import java.sql.Connection;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class UniqueIdGroupService {
	
	private String error="";
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	private String uniqueid;
	private String donor;
	private String trusties;
	private String nad;
	private String lta;
	private String rds;
	private String timeshare;
	private String employee;
	private String general;
	private String createdDate;
	private String updatedDate;
	private String extra1;
	private String extra2;
	private String extra3;
	private String extra4;
	private String extra5;
	private String extra6;
	private String extra7;
	private String extra8;
	private String extra9;
	private String extra10;
	private String extra11;
	private String extra12;
	private String extra13;
	private String extra14;
	private String extra15;

	private Statement st = null;
	private Connection c=null;
	public boolean insert(UniqueIdGroup uig){                                      
		boolean flag=false;
		try {
			 // Load the database driver
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			String query="insert into unique_id_group values('"+uig.getUniqueid()+"','"+uig.getDonor()+"','"+uig.getTrusties()+"','"+uig.getNad()+"','"+uig.getLta()+"','"+uig.getRds()+"','"+uig.getTimeshare()+"','"+uig.getEmployee()+"','"+uig.getGeneral()+"','"+uig.getCreatedDate()+"','"+uig.getUpdatedDate()+"','"+uig.getExtra1()+"','"+uig.getExtra2()+"','"+uig.getExtra3()+"','"+uig.getExtra4()+"','"+uig.getExtra5()+"','"+uig.getExtra6()+"','"+uig.getExtra7()+"','"+uig.getExtra8()+"','"+uig.getExtra9()+"','"+uig.getExtra10()+"','"+uig.getExtra11()+"','"+uig.getExtra12()+"','"+uig.getExtra13()+"','"+uig.getExtra14()+"','"+uig.getExtra15()+"')";
			int i=st.executeUpdate(query);
			System.out.println(query);
			if(i>0){
				flag=true;
			}
		}catch(Exception e){
			System.out.println("Exception In UniqueIdGroupService.insert(UniqueIdGroup uig) method:=>"+e);
		}finally{
			try{
			if(c!=null){c.close();c=null;}
			if(st!=null){st.close();st=null;}
			}catch(Exception e){
				System.out.println("Exception In UniqueIdGroupService.insert(UniqueIdGroup uig).finally method:=>"+e);
			}
		}
		return flag;
	}
	
	public boolean update(UniqueIdGroup uig){
		boolean flag=false;
		String addQuery=" set uniqueid=uniqueid";
		if(uig.getDonor()!=null)
		{
			addQuery=addQuery+",donor='"+uig.getDonor()+"'";
		}
		if(uig.getTrusties()!=null)
		{
			addQuery=addQuery+",trusties='"+uig.getTrusties()+"'";
		}
		if(uig.getNad()!=null)
		{
			addQuery=addQuery+",nad='"+uig.getNad()+"'";
		}
		if(uig.getLta()!=null)
		{
			addQuery=addQuery+",lta='"+uig.getLta()+"'";
		}
		if(uig.getRds()!=null)
		{
			addQuery=addQuery+",rds='"+uig.getRds()+"'";
		}
		if(uig.getTimeshare()!=null)
		{
			addQuery=addQuery+",timeshare='"+uig.getTimeshare()+"'";
		}
		if(uig.getEmployee()!=null)
		{
			addQuery=addQuery+",employee='"+uig.getEmployee()+"'";
		}
		if(uig.getGeneral()!=null)
		{
			addQuery=addQuery+",general='"+uig.getGeneral()+"'";
		}
		if(uig.getExtra1()!=null)
		{
			addQuery=addQuery+",extra1='"+uig.getExtra1()+"'";
		}
		if(uig.getExtra2()!=null)
		{
			addQuery=addQuery+",extra2='"+uig.getExtra2()+"'";
		}
		if(uig.getExtra3()!=null)
		{
			addQuery=addQuery+",extra3='"+uig.getExtra3()+"'";
		}
		if(uig.getExtra4()!=null)
		{
			addQuery=addQuery+",extra4='"+uig.getExtra4()+"'";
		}
		if(uig.getExtra5()!=null)
		{
			addQuery=addQuery+",extra5='"+uig.getExtra5()+"'";
		}
		if(uig.getExtra6()!=null)
		{
			addQuery=addQuery+",extra6='"+uig.getExtra6()+"'";
		}
		if(uig.getExtra7()!=null)
		{
			addQuery=addQuery+",extra7='"+uig.getExtra7()+"'";
		}
		if(uig.getExtra8()!=null)
		{
			addQuery=addQuery+",extra8='"+uig.getExtra8()+"'";
		}
		if(uig.getExtra9()!=null)
		{
			addQuery=addQuery+",extra9='"+uig.getExtra9()+"'";
		}
		if(uig.getExtra10()!=null)
		{
			addQuery=addQuery+",extra10='"+uig.getExtra10()+"'";
		}
		if(uig.getExtra11()!=null)
		{
			addQuery=addQuery+",extra11='"+uig.getExtra11()+"'";
		}
		if(uig.getExtra12()!=null)
		{
			addQuery=addQuery+",extra12='"+uig.getExtra12()+"'";
		}
		if(uig.getExtra13()!=null)
		{
			addQuery=addQuery+",extra13='"+uig.getExtra13()+"'";
		}
		if(uig.getExtra14()!=null)
		{
			addQuery=addQuery+",extra14='"+uig.getExtra14()+"'";
		}
		if(uig.getExtra15()!=null)
		{
			addQuery=addQuery+",extra15='"+uig.getExtra15()+"'";
		}
		
		try {
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			String query="update unique_id_group "+addQuery+" where uniqueid='"+uig.getUniqueid()+"'";
			//System.out.println("from db==="+query);
			int i=st.executeUpdate(query);
			if(i>0){flag=true;}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			try {
				if(c!=null){c.close();c=null;}
				if(st!=null) { st.close();st=null;  }
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	return flag;
}
	
}
