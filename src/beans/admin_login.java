package beans;
public class admin_login 
 {
private String id;
private String user_name;
private String pass_word;
private String menu;
private String last_login;
private String emp_name;
private String email_id;
private String emp_role;
private String created_date;
public admin_login() {} 
public admin_login(String id,String user_name,String pass_word,String menu,String last_login,String emp_name,String email_id,String emp_role,String created_date)
 {
this.id=id;
this.user_name=user_name;
this.pass_word=pass_word;
this.menu=menu;
this.last_login=last_login;
this.emp_name=emp_name;
this.email_id=email_id;
this.emp_role=emp_role;
this.created_date=created_date;

} 
public String getid() {
return id;
}
public void setid(String id) {
this.id = id;
}
public String getuser_name() {
return user_name;
}
public void setuser_name(String user_name) {
this.user_name = user_name;
}
public String getpass_word() {
return pass_word;
}
public void setpass_word(String pass_word) {
this.pass_word = pass_word;
}
public String getmenu() {
return menu;
}
public void setmenu(String menu) {
this.menu = menu;
}
public String getlast_login() {
return last_login;
}
public void setlast_login(String last_login) {
this.last_login = last_login;
}
public String getemp_name() {
return emp_name;
}
public void setemp_name(String emp_name) {
this.emp_name = emp_name;
}
public String getemail_id() {
return email_id;
}
public void setemail_id(String email_id) {
this.email_id = email_id;
}
public String getemp_role() {
return emp_role;
}
public void setemp_role(String emp_role) {
this.emp_role = emp_role;
}
public String getcreated_date() {
return created_date;
}
public void setcreated_date(String created_date) {
this.created_date = created_date;
}

}