package beans;

public class UniqueIdGroup {

	private String uniqueid;
	private String donor;
	private String trusties;
	private String nad;
	private String lta;
	private String rds;
	private String timeshare;
	private String employee;
	private String general;
	private String createdDate;
	private String updatedDate;
	private String extra1; //VOLUNTEER
	private String extra2;
	private String extra3;
	private String extra4;
	private String extra5;
	private String extra6;
	private String extra7;
	private String extra8;
	private String extra9;
	private String extra10;
	private String extra11;
	private String extra12;
	private String extra13;
	private String extra14;
	private String extra15;
	
	public UniqueIdGroup(){}
	
	public UniqueIdGroup(String uniqueid,String donor,String trusties,String nad,String lta,String rds,String timeshare,String employee,
			String general,String createdDate,String updatedDate,String extra1,String extra2,String extra3,String extra4,String extra5,
			String extra6,String extra7,String extra8,String extra9,String extra10,String extra11,String extra12,String extra13,String extra14,String extra15 ){
		this.uniqueid = uniqueid;
		this.donor =donor;
		this.trusties=trusties;
		this.nad=nad;
		this.lta=lta;
		this.rds=rds;
		this.timeshare=timeshare;
		this.employee=employee;
		this.general=general;
		this.createdDate=createdDate;
		this.updatedDate=updatedDate;
		this.extra1=extra1;
		this.extra2=extra2;
		this.extra3=extra3;
		this.extra4=extra4;
		this.extra5=extra5;
		this.extra6=extra6;
		this.extra7=extra7;
		this.extra8=extra8;
		this.extra9=extra9;
		this.extra10=extra10;
		this.extra11=extra11;
		this.extra12=extra12;
		this.extra13=extra13;
		this.extra14=extra14;
		this.extra15=extra15;
	}
	public String getUniqueid() {
		return uniqueid;
	}
	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}
	public String getDonor() {
		return donor;
	}
	public void setDonor(String donor) {
		this.donor = donor;
	}
	public String getTrusties() {
		return trusties;
	}
	public void setTrusties(String trusties) {
		this.trusties = trusties;
	}
	public String getNad() {
		return nad;
	}
	public void setNad(String nad) {
		this.nad = nad;
	}
	public String getLta() {
		return lta;
	}
	public void setLta(String lta) {
		this.lta = lta;
	}
	public String getRds() {
		return rds;
	}
	public void setRds(String rds) {
		this.rds = rds;
	}
	public String getTimeshare() {
		return timeshare;
	}
	public void setTimeshare(String timeshare) {
		this.timeshare = timeshare;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public String getGeneral() {
		return general;
	}
	public void setGeneral(String general) {
		this.general = general;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}
	public String getExtra11() {
		return extra11;
	}
	public void setExtra11(String extra11) {
		this.extra11 = extra11;
	}
	public String getExtra12() {
		return extra12;
	}
	public void setExtra12(String extra12) {
		this.extra12 = extra12;
	}
	public String getExtra13() {
		return extra13;
	}
	public void setExtra13(String extra13) {
		this.extra13 = extra13;
	}
	public String getExtra14() {
		return extra14;
	}
	public void setExtra14(String extra14) {
		this.extra14 = extra14;
	}
	public String getExtra15() {
		return extra15;
	}
	public void setExtra15(String extra15) {
		this.extra15 = extra15;
	}
}
