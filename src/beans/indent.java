package beans;
public class indent 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String indent_id="";
private String date="";
private String description="";
private String status="";
private String indentinvoice_id="";
private String requirement="";
private String require_date="";
private String head_account_id="";
private String product_id="";
private String tablet_id="";
private String quantity="";
private String vendor_id="";
private String emp_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public indent() {} 
public indent(String indent_id,String date,String description,String status,String indentinvoice_id,String requirement,String require_date,String head_account_id,String product_id,String tablet_id,String quantity,String vendor_id,String emp_id,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.indent_id=indent_id;
this.date=date;
this.description=description;
this.status=status;
this.indentinvoice_id=indentinvoice_id;
this.requirement=requirement;
this.require_date=require_date;
this.head_account_id=head_account_id;
this.product_id=product_id;
this.tablet_id=tablet_id;
this.quantity=quantity;
this.vendor_id=vendor_id;
this.emp_id=emp_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getindent_id() {
return indent_id;
}
public void setindent_id(String indent_id) {
this.indent_id = indent_id;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String getindentinvoice_id() {
return indentinvoice_id;
}
public void setindentinvoice_id(String indentinvoice_id) {
this.indentinvoice_id = indentinvoice_id;
}
public String getrequirement() {
return requirement;
}
public void setrequirement(String requirement) {
this.requirement = requirement;
}
public String getrequire_date() {
return require_date;
}
public void setrequire_date(String require_date) {
this.require_date = require_date;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getproduct_id() {
return product_id;
}
public void setproduct_id(String product_id) {
this.product_id = product_id;
}
public String gettablet_id() {
return tablet_id;
}
public void settablet_id(String tablet_id) {
this.tablet_id = tablet_id;
}
public String getquantity() {
return quantity;
}
public void setquantity(String quantity) {
this.quantity = quantity;
}
public String getvendor_id() {
return vendor_id;
}
public void setvendor_id(String vendor_id) {
this.vendor_id = vendor_id;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}