package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dbase.sqlcon.ConnectionHelper;

public class ProductOpeningBalanceService {
	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	
	public boolean validateProductOpeningBalanceRecords(String category,String subCategory,String fincialyeardate) {
		boolean flag=false;
		try {
			 c = ConnectionHelper.getConnection();
			 st=c.createStatement();
			 String query="select * from product_openingbalance where category='"+category+"' and subcategory='"+subCategory+"' and finicialyear_date='"+fincialyeardate+"';";
			 System.out.println("query:::"+query);
			 rs=st.executeQuery(query);
			 while(rs.next()) {
				 flag=true;
			 }
		}catch(Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	public void saveProductOpeningBalance(List<ProductOpeningBalance> productOpeningBalanceList) {
		String productOpeningBalanceId="";
		Calendar calendar=Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		String createdDate=dateFormat.format(calendar.getTime());
		try
		{
		 c = ConnectionHelper.getConnection();
		 for(int i=0;i<productOpeningBalanceList.size();i++) {
			 ProductOpeningBalance productOpeningBalance=productOpeningBalanceList.get(i);
		  st=c.createStatement();
		  rs=st.executeQuery("select * from no_genarator where table_name='product_openingbalance'");
		 rs.next();
		int ticket=rs.getInt("table_id");
		String PriSt =rs.getString("id_prefix");
		int ticketm=ticket+1;
		productOpeningBalanceId=PriSt+ticketm;
		String query="insert into product_openingbalance values('"+productOpeningBalanceId+"','"+productOpeningBalance.getProductId()+"','"+productOpeningBalance.getProductName()+"','"+productOpeningBalance.getHeadOfAccount()+"','"+productOpeningBalance.getMajorHeadId()+"','"+productOpeningBalance.getMinorHeadId()+"','"+productOpeningBalance.getSubHeadId()+"','"+productOpeningBalance.getQuantity()+"','"+productOpeningBalance.getFinacilaYearDate()+"','"+createdDate+"','','"+productOpeningBalance.getCategory()+"','"+productOpeningBalance.getSubCategory()+"')";
		st.executeUpdate(query);
		st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='product_openingbalance'");
	}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				if(rs != null){rs.close();}
				if(st != null){st.close();}
				if(c != null){c.close();}
				} catch (SQLException e) {
				e.printStackTrace();
				}
		}
	}
	
	public List<ProductOpeningBalance> getProductOpeningBalncesBasedOnCategorySubCategoryAndFinacialYear(String category,String subCategory,String finacialYear){
		List<ProductOpeningBalance> productOpeningBalancesList=new ArrayList<ProductOpeningBalance>();
		try {
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			String query="select * from product_openingbalance  where category='"+category+"' and subcategory='"+subCategory+"' and finicialyear_date='"+finacialYear+"';";
			rs=st.executeQuery(query);
			while(rs.next()) {
				ProductOpeningBalance productOpeningBalance=new ProductOpeningBalance();
				productOpeningBalance.setProductOpeningBalanceId(rs.getString("product_openingbalance_id"));
				productOpeningBalance.setProductId(rs.getString("productid"));
				productOpeningBalance.setProductName(rs.getString("product_name"));
				productOpeningBalance.setHeadOfAccount(rs.getString("head_account_id"));
				productOpeningBalance.setFinacilaYearDate(rs.getString("finicialyear_date"));
				productOpeningBalance.setQuantity(rs.getDouble("quantity")); 
				productOpeningBalancesList.add(productOpeningBalance);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(rs != null){rs.close();}
				if(st != null){st.close();}
				if(c != null){c.close();}
				} catch (SQLException e) {
				e.printStackTrace();
				}
		}
	  return productOpeningBalancesList;
	}
	public ProductOpeningBalance getProductOpeningBalanceDetails(String productOpeningBalanceId) {
		ProductOpeningBalance productOpeningBalance=null;
		try {
		c = ConnectionHelper.getConnection();
		st=c.createStatement();
		String query="select * from product_openingbalance  where product_openingbalance_id='"+productOpeningBalanceId+"'";
		rs=st.executeQuery(query);
		while(rs.next()) {
			productOpeningBalance=new ProductOpeningBalance();
			productOpeningBalance.setProductOpeningBalanceId(rs.getString("product_openingbalance_id"));
			productOpeningBalance.setProductId(rs.getString("productid"));
			productOpeningBalance.setProductName(rs.getString("product_name"));
			productOpeningBalance.setHeadOfAccount(rs.getString("head_account_id"));
			productOpeningBalance.setFinacilaYearDate(rs.getString("finicialyear_date"));
			productOpeningBalance.setQuantity(rs.getDouble("quantity")); 
		}
	}catch(Exception e) {
		e.printStackTrace();
	}
	finally{
		try {
			if(rs != null){rs.close();}
			if(st != null){st.close();}
			if(c != null){c.close();}
			} catch (SQLException e) {
			e.printStackTrace();
			}
	}
		return productOpeningBalance;
	}
	
	public void updateProductOpeningBalance(String productOpeningBalanceId,double quantity) {
		try {
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
			String query="update product_openingbalance set quantity="+quantity+"where product_openingbalance_id='"+productOpeningBalanceId+"'";
			st.executeUpdate(query);
		}catch(Exception e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(rs != null){rs.close();}
				if(st != null){st.close();}
				if(c != null){c.close();}
				} catch (SQLException e) {
				e.printStackTrace();
				}
		}
	}
	
	public Map<String,ProductOpeningBalance> getProductOpeningBalancesMap(String category,String subCategory,String finiacialyeardate){
		Map<String,ProductOpeningBalance> productOpeningBalanceMap=new HashMap<String,ProductOpeningBalance>();
		try {
			c=ConnectionHelper.getConnection();
			st=c.createStatement();
			String query="select * from product_openingbalance where category='"+category+"' and subcategory='"+subCategory+"' and finicialyear_date='"+finiacialyeardate+"'";
			rs=st.executeQuery(query);
			while(rs.next()) {
				ProductOpeningBalance productOpeningBalance=new ProductOpeningBalance();
				productOpeningBalance.setProductOpeningBalanceId(rs.getString("product_openingbalance_id"));
				productOpeningBalance.setProductId(rs.getString("productid"));
				productOpeningBalance.setProductName(rs.getString("product_name"));
				productOpeningBalance.setHeadOfAccount(rs.getString("head_account_id"));
				productOpeningBalance.setMajorHeadId(rs.getString("major_head_id"));
				productOpeningBalance.setMinorHeadId(rs.getString("minor_head_id"));
				productOpeningBalance.setSubHeadId(rs.getString("sub_head_id"));
				productOpeningBalance.setQuantity(rs.getDouble("quantity"));
				productOpeningBalance.setFinacilaYearDate(rs.getString("finicialyear_date"));
				productOpeningBalance.setCreatedDate(rs.getString("created_date"));
				productOpeningBalance.setCategory(rs.getString("category"));
				productOpeningBalance.setSubCategory(rs.getString("subcategory"));
				productOpeningBalanceMap.put(rs.getString("productid"),productOpeningBalance);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return productOpeningBalanceMap;
	}
	
	
}
