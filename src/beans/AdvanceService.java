package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class AdvanceService {
		
	String AdvId="";
	String vendorId="";
	String narration="";
	String date="";
	String amount="";
	String majorHeadId="";
	String minorHeadId="";
	String subHeadId="";
	String MSEId="";
	String bankId="";
	String uniqueId="";
	String invoiceId="";
	String extra1="";
	String extra2="";
	String extra3="";
	String extra4="";
	String extra5="";
	String extra6="";
	String extra7="";
	String extra8="";
	String extra9="";
	String extra10="";
	String extra11="";
	String extra12="";
	String extra13="";
	String extra14="";
	String extra15="";
	public String getAdvId() {
		return AdvId;
	}
	public void setAdvId(String advId) {
		AdvId = advId;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMajorHeadId() {
		return majorHeadId;
	}
	public void setMajorHeadId(String majorHeadId) {
		this.majorHeadId = majorHeadId;
	}
	public String getMinorHeadId() {
		return minorHeadId;
	}
	public void setMinorHeadId(String minorHeadId) {
		this.minorHeadId = minorHeadId;
	}
	public String getSubHeadId() {
		return subHeadId;
	}
	public void setSubHeadId(String subHeadId) {
		this.subHeadId = subHeadId;
	}
	public String getMSEId() {
		return MSEId;
	}
	public void setMSEId(String mSEId) {
		MSEId = mSEId;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}
	public String getExtra11() {
		return extra11;
	}
	public void setExtra11(String extra11) {
		this.extra11 = extra11;
	}
	public String getExtra12() {
		return extra12;
	}
	public void setExtra12(String extra12) {
		this.extra12 = extra12;
	}
	public String getExtra13() {
		return extra13;
	}
	public void setExtra13(String extra13) {
		this.extra13 = extra13;
	}
	public String getExtra14() {
		return extra14;
	}
	public void setExtra14(String extra14) {
		this.extra14 = extra14;
	}
	public String getExtra15() {
		return extra15;
	}
	public void setExtra15(String extra15) {
		this.extra15 = extra15;
	}
	@Override
	public String toString() {
		return "Advance [AdvId=" + AdvId + ", vendorId=" + vendorId
				+ ", narration=" + narration + ", date=" + date + ", amount="
				+ amount + ", majorHeadId=" + majorHeadId + ", minorHeadId="
				+ minorHeadId + ", subHeadId=" + subHeadId + ", MSEId=" + MSEId
				+ ", bankId=" + bankId + ", uniqueId=" + uniqueId
				+ ", invoiceId=" + invoiceId + ", extra1=" + extra1
				+ ", extra2=" + extra2 + ", extra3=" + extra3 + ", extra4="
				+ extra4 + ", extra5=" + extra5 + ", extra6=" + extra6
				+ ", extra7=" + extra7 + ", extra8=" + extra8 + ", extra9="
				+ extra9 + ", extra10=" + extra10 + ", extra11=" + extra11
				+ ", extra12=" + extra12 + ", extra13=" + extra13
				+ ", extra14=" + extra14 + ", extra15=" + extra15 + "]";
	}
	
	public AdvanceService(String invoiceId) {
		
		this.invoiceId = invoiceId;
	}
	
	
	
	
	
	public AdvanceService(String advId, String vendorId, String narration,
			String date, String amount, String majorHeadId, String minorHeadId,
			String subHeadId, String mSEId, String bankId, String uniqueId,
			String invoiceId, String extra1, String extra2, String extra3,
			String extra4, String extra5, String extra6, String extra7,
			String extra8, String extra9, String extra10, String extra11,
			String extra12, String extra13, String extra14, String extra15) {
		super();
		AdvId = advId;
		this.vendorId = vendorId;
		this.narration = narration;
		this.date = date;
		this.amount = amount;
		this.majorHeadId = majorHeadId;
		this.minorHeadId = minorHeadId;
		this.subHeadId = subHeadId;
		MSEId = mSEId;
		this.bankId = bankId;
		this.uniqueId = uniqueId;
		this.invoiceId = invoiceId;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
		this.extra6 = extra6;
		this.extra7 = extra7;
		this.extra8 = extra8;
		this.extra9 = extra9;
		this.extra10 = extra10;
		this.extra11 = extra11;
		this.extra12 = extra12;
		this.extra13 = extra13;
		this.extra14 = extra14;
		this.extra15 = extra15;
	}





	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	public AdvanceService()
	{
		
	}
	private String advanceId="";
	public boolean insert(){
		try
		{
			
			c = ConnectionHelper.getConnection();
			st=c.createStatement();
		rs=st.executeQuery("select * from no_genarator where table_name='advance'");
		rs.next();
		int ticket=rs.getInt("table_id");
		String PriSt =rs.getString("id_prefix");
		rs.close();
		int ticketm=ticket+1;
		advanceId=PriSt+ticketm;
		String query="insert into advance values('"+advanceId+"','"+vendorId+"','"+narration+"',now(),'"+amount+"','"+majorHeadId+"','"+minorHeadId+"','"+subHeadId+"','"+MSEId+"','"+bankId+"','"+uniqueId+"','"+invoiceId+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+ extra10+"','"+ extra11+"','"+ extra12+"','"+ extra13+"','"+ extra14+"','"+ extra15+"')";
		System.out.println(" advance service......................"+query);
		st.executeUpdate(query);
		st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='advance'");
		st.close();
		c.close();
		return true;
		}catch(Exception e){
		
		return false;
		}
	}
	
	public boolean update(String invoiceId,String amount){
		
		try {
			c =ConnectionHelper.getConnection();
			st=c.createStatement();
			st.executeUpdate("update advance set extra2=extra2+'"+amount+"' where invoiceId='"+invoiceId+"'");
			st.close();
			c.close();
			
			
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		
	}
	
}
