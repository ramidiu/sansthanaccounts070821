package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class gatepass_entriesService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String gp_id="";
private String entry_date="";
private String productId="";
private String qty="";
private String head_account_id="";
private String gatepass_number="";
private String entered_emp_id="";
private String status="";
private String narration="";
private String remarks="";
private String gatepass_category="";
private String updated_status="";
private String updated_date="";
private String updated_emp_id="";
private String return_qty="";
private String gatepass_takenby="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public gatepass_entriesService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setgp_id(String gp_id)
{
this.gp_id = gp_id;
}

public String getgp_id()
{
return (this.gp_id);
}


public void setentry_date(String entry_date)
{
this.entry_date = entry_date;
}

public String getentry_date()
{
return (this.entry_date);
}


public void setproductId(String productId)
{
this.productId = productId;
}

public String getproductId()
{
return (this.productId);
}


public void setqty(String qty)
{
this.qty = qty;
}

public String getqty()
{
return (this.qty);
}


public void sethead_account_id(String head_account_id)
{
this.head_account_id = head_account_id;
}

public String gethead_account_id()
{
return (this.head_account_id);
}


public void setgatepass_number(String gatepass_number)
{
this.gatepass_number = gatepass_number;
}

public String getgatepass_number()
{
return (this.gatepass_number);
}


public void setentered_emp_id(String entered_emp_id)
{
this.entered_emp_id = entered_emp_id;
}

public String getentered_emp_id()
{
return (this.entered_emp_id);
}


public void setstatus(String status)
{
this.status = status;
}

public String getstatus()
{
return (this.status);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setremarks(String remarks)
{
this.remarks = remarks;
}

public String getremarks()
{
return (this.remarks);
}


public void setgatepass_category(String gatepass_category)
{
this.gatepass_category = gatepass_category;
}

public String getgatepass_category()
{
return (this.gatepass_category);
}


public void setupdated_status(String updated_status)
{
this.updated_status = updated_status;
}

public String getupdated_status()
{
return (this.updated_status);
}


public void setupdated_date(String updated_date)
{
this.updated_date = updated_date;
}

public String getupdated_date()
{
return (this.updated_date);
}


public void setupdated_emp_id(String updated_emp_id)
{
this.updated_emp_id = updated_emp_id;
}

public String getupdated_emp_id()
{
return (this.updated_emp_id);
}


public void setreturn_qty(String return_qty)
{
this.return_qty = return_qty;
}

public String getreturn_qty()
{
return (this.return_qty);
}


public void setgatepass_takenby(String gatepass_takenby)
{
this.gatepass_takenby = gatepass_takenby;
}

public String getgatepass_takenby()
{
return (this.gatepass_takenby);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}

public boolean insert()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='gatepass_entries'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
int ticketm=ticket+1;
 gp_id=PriSt+ticketm;
String query="insert into gatepass_entries values('"+gp_id+"','"+entry_date+"','"+productId+"','"+qty+"','"+head_account_id+"','"+gatepass_number+"','"+entered_emp_id+"','"+status+"','"+narration+"','"+remarks+"','"+gatepass_category+"','"+updated_status+"','"+updated_date+"','"+updated_emp_id+"','"+return_qty+"','"+gatepass_takenby+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"')";

st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='gatepass_entries'");
flag = true;
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean update()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!entry_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"entry_date = '"+entry_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",entry_date = '"+entry_date+"'";}
}

if(!productId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"productId = '"+productId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",productId = '"+productId+"'";}
}

if(!qty.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"qty = '"+qty+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",qty = '"+qty+"'";}
}

if(!head_account_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"head_account_id = '"+head_account_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",head_account_id = '"+head_account_id+"'";}
}

if(!gatepass_number.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gatepass_number = '"+gatepass_number+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gatepass_number = '"+gatepass_number+"'";}
}

if(!entered_emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"entered_emp_id = '"+entered_emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",entered_emp_id = '"+entered_emp_id+"'";}
}

if(!status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"status = '"+status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",status = '"+status+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!remarks.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"remarks = '"+remarks+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",remarks = '"+remarks+"'";}
}

if(!gatepass_category.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gatepass_category = '"+gatepass_category+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gatepass_category = '"+gatepass_category+"'";}
}

if(!updated_status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"updated_status = '"+updated_status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",updated_status = '"+updated_status+"'";}
}

if(!updated_date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"updated_date = '"+updated_date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",updated_date = '"+updated_date+"'";}
}

if(!updated_emp_id.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"updated_emp_id = '"+updated_emp_id+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",updated_emp_id = '"+updated_emp_id+"'";}
}

if(!return_qty.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"return_qty = '"+return_qty+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",return_qty = '"+return_qty+"'";}
}

if(!gatepass_takenby.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"gatepass_takenby = '"+gatepass_takenby+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",gatepass_takenby = '"+gatepass_takenby+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}
String query="update gatepass_entries "+updatecc+" where gp_id='"+gp_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}public boolean delete()
{boolean flag = false;
try
{
c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from gatepass_entries where gp_id='"+gp_id+"'";
int i = st.executeUpdate(query);
if(i>0){flag=true;}
}catch(Exception e){
this.seterror(e.toString());
}finally{
	try {
		if(rs != null){rs.close();}
		if(st != null){st.close();}
		if(c != null){c.close();}
		} catch (SQLException e) {
		e.printStackTrace();
		}
}
return flag;

}
}