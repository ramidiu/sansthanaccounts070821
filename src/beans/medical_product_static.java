package beans;

public class medical_product_static {

private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String productId="";
private String productName="";
private String description="";
private String purchaseAmmount="";
private String sellingAmmount="";
private String head_account_id="";
private String major_head_id="";
private String sub_head_id="";
private String reorderLevel="";
private String balanceQuantityStore="";
private String balanceQuantityGodwan="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String vat="";
private String vat1="";
private String profcharges="";
private String units="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";
public medical_product_static() {} 
public medical_product_static(String productId,String productName,String description,String purchaseAmmount,String sellingAmmount,String head_account_id,String major_head_id,String sub_head_id,String reorderLevel,String balanceQuantityStore,String balanceQuantityGodwan,String extra1,String extra2,String extra3,String extra4,String extra5,String vat,String vat1,String profcharges,String units,String extra6,String extra7,String extra8,String extra9,String extra10)
 {
this.productId=productId;
this.productName=productName;
this.description=description;
this.purchaseAmmount=purchaseAmmount;
this.sellingAmmount=sellingAmmount;
this.head_account_id=head_account_id;
this.major_head_id=major_head_id;
this.sub_head_id=sub_head_id;
this.reorderLevel=reorderLevel;
this.balanceQuantityStore=balanceQuantityStore;
this.balanceQuantityGodwan=balanceQuantityGodwan;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.vat=vat;
this.vat1=vat1;
this.profcharges=profcharges;
this.units=units;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;
this.extra10=extra10;

} 
public medical_product_static(String productId,String productName,String balanceQuantityGodwan){
	this.productId=productId;
	this.productName=productName;
	this.balanceQuantityGodwan=balanceQuantityGodwan;
}
public String getproductId() {
return productId;
}
public void setproductId(String productId) {
this.productId = productId;
}
public String getproductName() {
return productName;
}
public void setproductName(String productName) {
this.productName = productName;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String getpurchaseAmmount() {
return purchaseAmmount;
}
public void setpurchaseAmmount(String purchaseAmmount) {
this.purchaseAmmount = purchaseAmmount;
}
public String getsellingAmmount() {
return sellingAmmount;
}
public void setsellingAmmount(String sellingAmmount) {
this.sellingAmmount = sellingAmmount;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getmajor_head_id() {
return major_head_id;
}
public void setmajor_head_id(String major_head_id) {
this.major_head_id = major_head_id;
}
public String getsub_head_id() {
return sub_head_id;
}
public void setsub_head_id(String sub_head_id) {
this.sub_head_id = sub_head_id;
}
public String getreorderLevel() {
return reorderLevel;
}
public void setreorderLevel(String reorderLevel) {
this.reorderLevel = reorderLevel;
}
public String getbalanceQuantityStore() {
return balanceQuantityStore;
}
public void setbalanceQuantityStore(String balanceQuantityStore) {
this.balanceQuantityStore = balanceQuantityStore;
}
public String getbalanceQuantityGodwan() {
return balanceQuantityGodwan;
}
public void setbalanceQuantityGodwan(String balanceQuantityGodwan) {
this.balanceQuantityGodwan = balanceQuantityGodwan;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getvat() {
return vat;
}
public void setvat(String vat) {
this.vat = vat;
}
public String getvat1() {
return vat1;
}
public void setvat1(String vat1) {
this.vat1 = vat1;
}
public String getprofcharges() {
return profcharges;
}
public void setprofcharges(String profcharges) {
this.profcharges = profcharges;
}
public String getunits() {
return units;
}
public void setunits(String units) {
this.units = units;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}
public String getextra8() {
return extra8;
}
public void setextra8(String extra8) {
this.extra8 = extra8;
}
public String getextra9() {
return extra9;
}
public void setextra9(String extra9) {
this.extra9 = extra9;
}
public String getextra10() {
return extra10;
}
public void setextra10(String extra10) {
this.extra10 = extra10;
}


}
