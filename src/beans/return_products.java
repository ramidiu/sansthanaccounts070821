package beans;
public class return_products 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String return_id="";
private String product_id="";
private String return_qty="";
private String return_date="";
private String mse_inv_id="";
private String vendor_billno="";
private String return_inv_id="";
private String entered_by="";
private String approved_by="";
private String approved_date="";
private String narration="";
private String vendor_id="";
private String head_account_id="";
private String status="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public return_products() {} 
public return_products(String return_id,String product_id,String return_qty,String return_date,String mse_inv_id,String vendor_billno,String return_inv_id,String entered_by,String approved_by,String approved_date,String narration,String vendor_id,String head_account_id,String status,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.return_id=return_id;
this.product_id=product_id;
this.return_qty=return_qty;
this.return_date=return_date;
this.mse_inv_id=mse_inv_id;
this.vendor_billno=vendor_billno;
this.return_inv_id=return_inv_id;
this.entered_by=entered_by;
this.approved_by=approved_by;
this.approved_date=approved_date;
this.narration=narration;
this.vendor_id=vendor_id;
this.head_account_id=head_account_id;
this.status=status;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getreturn_id() {
return return_id;
}
public void setreturn_id(String return_id) {
this.return_id = return_id;
}
public String getproduct_id() {
return product_id;
}
public void setproduct_id(String product_id) {
this.product_id = product_id;
}
public String getreturn_qty() {
return return_qty;
}
public void setreturn_qty(String return_qty) {
this.return_qty = return_qty;
}
public String getreturn_date() {
return return_date;
}
public void setreturn_date(String return_date) {
this.return_date = return_date;
}
public String getmse_inv_id() {
return mse_inv_id;
}
public void setmse_inv_id(String mse_inv_id) {
this.mse_inv_id = mse_inv_id;
}
public String getvendor_billno() {
return vendor_billno;
}
public void setvendor_billno(String vendor_billno) {
this.vendor_billno = vendor_billno;
}
public String getreturn_inv_id() {
return return_inv_id;
}
public void setreturn_inv_id(String return_inv_id) {
this.return_inv_id = return_inv_id;
}
public String getentered_by() {
return entered_by;
}
public void setentered_by(String entered_by) {
this.entered_by = entered_by;
}
public String getapproved_by() {
return approved_by;
}
public void setapproved_by(String approved_by) {
this.approved_by = approved_by;
}
public String getapproved_date() {
return approved_date;
}
public void setapproved_date(String approved_date) {
this.approved_date = approved_date;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getvendor_id() {
return vendor_id;
}
public void setvendor_id(String vendor_id) {
this.vendor_id = vendor_id;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}