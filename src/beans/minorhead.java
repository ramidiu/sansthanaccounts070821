package beans;
public class minorhead 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String minorhead_id="";
private String name="";
private String description="";
private String major_head_id="";
private String head_account_id="";
private String extra1="";
private String extra2="";
private String extra3="";
public minorhead() {} 
public minorhead(String minorhead_id,String name,String description,String major_head_id,String head_account_id,String extra1,String extra2,String extra3)
 {
this.minorhead_id=minorhead_id;
this.name=name;
this.description=description;
this.major_head_id=major_head_id;
this.head_account_id=head_account_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;

} 
public String getminorhead_id() {
return minorhead_id;
}
public void setminorhead_id(String minorhead_id) {
this.minorhead_id = minorhead_id;
}
public String getname() {
return name;
}
public void setname(String name) {
this.name = name;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String getmajor_head_id() {
return major_head_id;
}
public void setmajor_head_id(String major_head_id) {
this.major_head_id = major_head_id;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
@Override
public String toString() {
	return "minorhead [error=" + error + ", minorhead_id=" + minorhead_id
			+ ", name=" + name + ", description=" + description
			+ ", major_head_id=" + major_head_id + ", head_account_id="
			+ head_account_id + ", extra1=" + extra1 + ", extra2=" + extra2
			+ ", extra3=" + extra3 + "]";
}

}