package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class bankbalanceService 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String uniq_id="";
	private String bank_id="";
	private String type="";
	private String bank_bal="";
	private String bank_flag="";
	private String createdDate="";
	private String createdBy="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	private String extra6="";
	private String extra7="";
	private String extra8="";
	private String extra9="";
	private String extra10="";
	
	private ResultSet rs = null;
	private Statement st = null;
	private Connection c=null;
	public bankbalanceService()
	{
	/*try {
	 // Load the database driver
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	}catch(Exception e){
	this.seterror(e.toString());
	System.out.println("Exception is ;"+e);
	}*/
	 }
	public String getUniq_id() {
		return uniq_id;
	}
	public void setUniq_id(String uniq_id) {
		this.uniq_id = uniq_id;
	}
	public String getBank_id() {
		return bank_id;
	}
	public void setBank_id(String bank_id) {
		this.bank_id = bank_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBank_bal() {
		return bank_bal;
	}
	public void setBank_bal(String bank_bal) {
		this.bank_bal = bank_bal;
	}
	public String getBank_flag() {
		return bank_flag;
	}
	public void setBank_flag(String bank_flag) {
		this.bank_flag = bank_flag;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}
	public boolean insert()
	{boolean flag = false;
	try
	{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
	rs=st.executeQuery("select * from no_genarator where table_name='bankbalance'");
	rs.next();
	int ticket=rs.getInt("table_id");
	String PriSt =rs.getString("id_prefix");
	int ticketm=ticket+1;
	uniq_id=PriSt+ticketm;
	String query="insert into bankbalance values('"+uniq_id+"','"+bank_id+"','"+type+"','"+bank_bal+"','"+bank_flag+"','"+createdDate+"','"+createdBy+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"')";
	System.out.println(query);
	st.executeUpdate(query);
	int i = st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='bankbalance'");
	if(i>0){flag=true;}
	}catch(Exception e){
	this.seterror(e.toString());
	}finally{
		try {
			if(rs != null){rs.close();}
			if(st != null){st.close();}
			if(c != null){c.close();}
			} catch (SQLException e) {
			e.printStackTrace();
			}
	}
	return flag;
	}
}
