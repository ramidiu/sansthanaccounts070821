package beans;
public class produceditems 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String pro_id="";
private String sstid="";
private String productId="";
private String produced_date="";
private String description="";
private String trans_id="";
private String status="";
private String quantity="";
private String emp_id="";
private String sale_price="";
private String head_account_id="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
public produceditems() {} 
public produceditems(String pro_id,String sstid,String productId,String produced_date,String description,String trans_id,String status,String quantity,String emp_id,String sale_price,String head_account_id,String extra1,String extra2,String extra3,String extra4,String extra5,String extra6,String extra7)
 {
this.pro_id=pro_id;
this.sstid=sstid;
this.productId=productId;
this.produced_date=produced_date;
this.description=description;
this.trans_id=trans_id;
this.status=status;
this.quantity=quantity;
this.emp_id=emp_id;
this.sale_price=sale_price;
this.head_account_id=head_account_id;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.extra6=extra6;
this.extra7=extra7;

} 
public String getpro_id() {
return pro_id;
}
public void setpro_id(String pro_id) {
this.pro_id = pro_id;
}
public String getsstid() {
return sstid;
}
public void setsstid(String sstid) {
this.sstid = sstid;
}
public String getproductId() {
return productId;
}
public void setproductId(String productId) {
this.productId = productId;
}
public String getproduced_date() {
return produced_date;
}
public void setproduced_date(String produced_date) {
this.produced_date = produced_date;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String gettrans_id() {
return trans_id;
}
public void settrans_id(String trans_id) {
this.trans_id = trans_id;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String getquantity() {
return quantity;
}
public void setquantity(String quantity) {
this.quantity = quantity;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}
public String getsale_price() {
return sale_price;
}
public void setsale_price(String sale_price) {
this.sale_price = sale_price;
}
public String gethead_account_id() {
return head_account_id;
}
public void sethead_account_id(String head_account_id) {
this.head_account_id = head_account_id;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}

}