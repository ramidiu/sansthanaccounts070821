package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class shift_timingsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String shtime_id="";
private String shift_name="";
private String shift_starttitming="";
private String shift_endtiming="";
private String shift_code="";
private String extra2="";
private String extra3="";


private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public shift_timingsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setshtime_id(String shtime_id)
{
this.shtime_id = shtime_id;
}

public String getshtime_id()
{
return (this.shtime_id);
}


public void setshift_name(String shift_name)
{
this.shift_name = shift_name;
}

public String getshift_name()
{
return (this.shift_name);
}


public void setshift_starttitming(String shift_starttitming)
{
this.shift_starttitming = shift_starttitming;
}

public String getshift_starttitming()
{
return (this.shift_starttitming);
}


public void setshift_endtiming(String shift_endtiming)
{
this.shift_endtiming = shift_endtiming;
}

public String getshift_endtiming()
{
return (this.shift_endtiming);
}


public void setshift_code(String shift_code)
{
this.shift_code = shift_code;
}

public String getshift_code()
{
return (this.shift_code);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}

public boolean insert()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='shift_timings'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 shtime_id=PriSt+ticketm;
String query="insert into shift_timings values('"+shtime_id+"','"+shift_name+"','"+shift_starttitming+"','"+shift_endtiming+"','"+shift_code+"','"+extra2+"','"+extra3+"')";
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='shift_timings'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean update()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!shift_name.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"shift_name = '"+shift_name+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",shift_name = '"+shift_name+"'";}
}

if(!shift_starttitming.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"shift_starttitming = '"+shift_starttitming+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",shift_starttitming = '"+shift_starttitming+"'";}
}

if(!shift_endtiming.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"shift_endtiming = '"+shift_endtiming+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",shift_endtiming = '"+shift_endtiming+"'";}
}

if(!shift_code.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"shift_code = '"+shift_code+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",shift_code = '"+shift_code+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}
String query="update shift_timings "+updatecc+" where shtime_id='"+shtime_id+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from shift_timings where shtime_id='"+shtime_id+"'";
//System.out.println(query);
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}