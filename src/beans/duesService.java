package beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import dbase.sqlcon.ConnectionHelper;

public class duesService {

	String DueId="";
	String date="";
	String finYear="";
	String headAccountId="";
	String advanceInvId="";
	String majorHeadId="";
	String minorHeadId="";
	String subHeadId="";
	String invoiceId="";
	String amount="";
	String extra1="";
	String extra2="";
	String extra3="";
	String extra4="";
	String extra5="";
	String extra6="";
	String extra7="";
	String extra8="";
	String extra9="";
	String extra10="";
	String extra11="";
	String extra12="";
	String extra13="";
	String extra14="";
	String extra15="";
	String extra16="";
	String extra17="";
	String extra18="";
	String extra19="";
	String extra20="";
	public String getDueId() {
		return DueId;
	}
	public void setDueId(String dueId) {
		DueId = dueId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFinYear() {
		return finYear;
	}
	public void setFinYear(String finYear) {
		this.finYear = finYear;
	}
	public String getHeadAccountId() {
		return headAccountId;
	}
	public void setHeadAccountId(String headAccountId) {
		this.headAccountId = headAccountId;
	}
	public String getAdvanceInvId() {
		return advanceInvId;
	}
	public void setAdvanceInvId(String advanceInvId) {
		this.advanceInvId = advanceInvId;
	}
	public String getMajorHeadId() {
		return majorHeadId;
	}
	public void setMajorHeadId(String majorHeadId) {
		this.majorHeadId = majorHeadId;
	}
	public String getMinorHeadId() {
		return minorHeadId;
	}
	public void setMinorHeadId(String minorHeadId) {
		this.minorHeadId = minorHeadId;
	}
	public String getSubHeadId() {
		return subHeadId;
	}
	public void setSubHeadId(String subHeadId) {
		this.subHeadId = subHeadId;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	}
	public String getExtra11() {
		return extra11;
	}
	public void setExtra11(String extra11) {
		this.extra11 = extra11;
	}
	public String getExtra12() {
		return extra12;
	}
	public void setExtra12(String extra12) {
		this.extra12 = extra12;
	}
	public String getExtra13() {
		return extra13;
	}
	public void setExtra13(String extra13) {
		this.extra13 = extra13;
	}
	public String getExtra14() {
		return extra14;
	}
	public void setExtra14(String extra14) {
		this.extra14 = extra14;
	}
	public String getExtra15() {
		return extra15;
	}
	public void setExtra15(String extra15) {
		this.extra15 = extra15;
	}
	public String getExtra16() {
		return extra16;
	}
	public void setExtra16(String extra16) {
		this.extra16 = extra16;
	}
	public String getExtra17() {
		return extra17;
	}
	public void setExtra17(String extra17) {
		this.extra17 = extra17;
	}
	public String getExtra18() {
		return extra18;
	}
	public void setExtra18(String extra18) {
		this.extra18 = extra18;
	}
	public String getExtra19() {
		return extra19;
	}
	public void setExtra19(String extra19) {
		this.extra19 = extra19;
	}
	public String getExtra20() {
		return extra20;
	}
	public void setExtra20(String extra20) {
		this.extra20 = extra20;
	}
	@Override
	public String toString() {
		return "duesService [DueId=" + DueId + ", date=" + date + ", finYear="
				+ finYear + ", headAccountId=" + headAccountId
				+ ", advanceInvId=" + advanceInvId + ", majorHeadId="
				+ majorHeadId + ", minorHeadId=" + minorHeadId + ", subHeadId="
				+ subHeadId + ", invoiceId=" + invoiceId + ", amount=" + amount
				+ ", extra1=" + extra1 + ", extra2=" + extra2 + ", extra3="
				+ extra3 + ", extra4=" + extra4 + ", extra5=" + extra5
				+ ", extra6=" + extra6 + ", extra7=" + extra7 + ", extra8="
				+ extra8 + ", extra9=" + extra9 + ", extra10=" + extra10
				+ ", extra11=" + extra11 + ", extra12=" + extra12
				+ ", extra13=" + extra13 + ", extra14=" + extra14
				+ ", extra15=" + extra15 + ", extra16=" + extra16
				+ ", extra17=" + extra17 + ", extra18=" + extra18
				+ ", extra19=" + extra19 + ", extra20=" + extra20 + "]";
	}
	
	
	
	public duesService() {
		super();
	}
	public duesService(String dueId, String date, String finYear,
			String headAccountId, String advanceInvId, String majorHeadId,
			String minorHeadId, String subHeadId, String invoiceId,
			String amount, String extra1, String extra2, String extra3,
			String extra4, String extra5, String extra6, String extra7,
			String extra8, String extra9, String extra10, String extra11,
			String extra12, String extra13, String extra14, String extra15,
			String extra16, String extra17, String extra18, String extra19,
			String extra20) {
		super();
		DueId = dueId;
		this.date = date;
		this.finYear = finYear;
		this.headAccountId = headAccountId;
		this.advanceInvId = advanceInvId;
		this.majorHeadId = majorHeadId;
		this.minorHeadId = minorHeadId;
		this.subHeadId = subHeadId;
		this.invoiceId = invoiceId;
		this.amount = amount;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
		this.extra6 = extra6;
		this.extra7 = extra7;
		this.extra8 = extra8;
		this.extra9 = extra9;
		this.extra10 = extra10;
		this.extra11 = extra11;
		this.extra12 = extra12;
		this.extra13 = extra13;
		this.extra14 = extra14;
		this.extra15 = extra15;
		this.extra16 = extra16;
		this.extra17 = extra17;
		this.extra18 = extra18;
		this.extra19 = extra19;
		this.extra20 = extra20;
	}
	Connection con=null;
	Statement st=null;
	ResultSet rs=null;
	
	public boolean insert(){
		try
		{
			
		String due_id="";
			con = ConnectionHelper.getConnection();
			st=con.createStatement();
		rs=st.executeQuery("select * from no_genarator where table_name='dues'");
		rs.next();
		int ticket=rs.getInt("table_id");
		String PriSt =rs.getString("id_prefix");
		rs.close();
		int ticketm=ticket+1;
		due_id=PriSt+ticketm;
		String query="insert into dues values('"+due_id+"','"+date+"','"+finYear+"','"+headAccountId+"','"+advanceInvId+"','"+majorHeadId+"','"+minorHeadId+"','"+subHeadId+"','"+invoiceId+"','"+amount+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+extra10+"','"+extra11+"','"+extra12+"','"+extra13+"','"+extra14+"','"+extra15+"','"+extra16+"','"+extra17+"','"+extra18+"','"+extra19+"','"+extra20+"')";
		
		System.out.println(query);

		st.executeUpdate(query);

		st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='dues'");
		st.close();
		con.close();
		return true;
		}catch(Exception e){
		
		return false;
	}
	
	}
}
