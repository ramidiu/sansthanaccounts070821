package beans;
public class bankdetails 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String bank_id="";
private String bank_name="";
private String bank_branch="";
private String account_holder_name="";
private String account_number="";
private String ifsc_code="";
private String total_amount="";
private String headAccountId="";
private String createdBy="";
private String editedBy="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String majorHeadId = "";
private String minorHeadId = "";
private String subHeadId = "";
public bankdetails() {} 
public bankdetails(String bank_id,String bank_name,String bank_branch,String account_holder_name,String account_number,String ifsc_code,String total_amount,String headAccountId,String createdBy,String editedBy,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.bank_id=bank_id;
this.bank_name=bank_name;
this.bank_branch=bank_branch;
this.account_holder_name=account_holder_name;
this.account_number=account_number;
this.ifsc_code=ifsc_code;
this.total_amount=total_amount;
this.headAccountId=headAccountId;
this.createdBy=createdBy;
this.editedBy=editedBy;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getmajorHeadId() {
return majorHeadId;
}
public void setmajorHeadId(String majorHeadId) {
this.majorHeadId = majorHeadId;
}
public String getminorHeadId() {
return minorHeadId;
}
public void setminorHeadId(String minorHeadId) {
this.minorHeadId = minorHeadId;
}
public String getsubHeadId() {
return subHeadId;
}
public void setsubHeadId(String subHeadId) {
this.subHeadId = subHeadId;
}

public String getbank_id() {
return bank_id;
}
public void setbank_id(String bank_id) {
this.bank_id = bank_id;
}
public String getbank_name() {
return bank_name;
}
public void setbank_name(String bank_name) {
this.bank_name = bank_name;
}
public String getbank_branch() {
return bank_branch;
}
public void setbank_branch(String bank_branch) {
this.bank_branch = bank_branch;
}
public String getaccount_holder_name() {
return account_holder_name;
}
public void setaccount_holder_name(String account_holder_name) {
this.account_holder_name = account_holder_name;
}
public String getaccount_number() {
return account_number;
}
public void setaccount_number(String account_number) {
this.account_number = account_number;
}
public String getifsc_code() {
return ifsc_code;
}
public void setifsc_code(String ifsc_code) {
this.ifsc_code = ifsc_code;
}
public String gettotal_amount() {
return total_amount;
}
public void settotal_amount(String total_amount) {
this.total_amount = total_amount;
}
public String getheadAccountId() {
return headAccountId;
}
public void setheadAccountId(String headAccountId) {
this.headAccountId = headAccountId;
}
public String getcreatedBy() {
return createdBy;
}
public void setcreatedBy(String createdBy) {
this.createdBy = createdBy;
}
public String geteditedBy() {
return editedBy;
}
public void seteditedBy(String editedBy) {
this.editedBy = editedBy;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}