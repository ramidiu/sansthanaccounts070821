package beans;
public class shopstock 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String sstId="";
private String productId="";
private String description="";
private String quantity="";
private String forwardedDate="";
private String stockType="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String emp_id="";
public shopstock() {} 
public shopstock(String sstId,String productId,String description,String quantity,String forwardedDate,String stockType,String extra1,String extra2,String extra3,String extra4,String extra5,String emp_id)
 {
this.sstId=sstId;
this.productId=productId;
this.description=description;
this.quantity=quantity;
this.forwardedDate=forwardedDate;
this.stockType=stockType;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.emp_id=emp_id;

} 
public String getsstId() {
return sstId;
}
public void setsstId(String sstId) {
this.sstId = sstId;
}
public String getproductId() {
return productId;
}
public void setproductId(String productId) {
this.productId = productId;
}
public String getdescription() {
return description;
}
public void setdescription(String description) {
this.description = description;
}
public String getquantity() {
return quantity;
}
public void setquantity(String quantity) {
this.quantity = quantity;
}
public String getforwardedDate() {
return forwardedDate;
}
public void setforwardedDate(String forwardedDate) {
this.forwardedDate = forwardedDate;
}
public String getstockType() {
return stockType;
}
public void setstockType(String stockType) {
this.stockType = stockType;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getemp_id() {
return emp_id;
}
public void setemp_id(String emp_id) {
this.emp_id = emp_id;
}

}