package beans;

public class bankbalance 
{
	private String error="";
	 public void seterror(String error)
	{
	this.error=error;
	}
	public String geterror()
	{
	return this.error;
	}
	
	private String uniq_id="";
	private String bank_id="";
	private String type="";
	private String bank_bal="";
	private String bank_flag="";
	private String createdDate="";
	private String createdBy="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	private String extra6="";
	private String extra7="";
	private String extra8="";
	private String extra9="";
	private String extra10="";
	
	public bankbalance() {}
	public bankbalance(String uniq_id, String bank_id, String type,
			String bank_bal, String bank_flag, String createdDate,
			String createdBy, String extra1, String extra2, String extra3,
			String extra4, String extra5, String extra6, String extra7, String extra8,
			String extra9, String extra10) {
		super();
		this.uniq_id = uniq_id;
		this.bank_id = bank_id;
		this.type = type;
		this.bank_bal = bank_bal;
		this.bank_flag = bank_flag;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
		this.extra6 = extra6;
		this.extra7 = extra7;
		this.extra8 = extra8;
		this.extra9 = extra9;
		this.extra10 = extra10;
	}
	public String getUniq_id() {
		return uniq_id;
	}
	public void setUniq_id(String uniq_id) {
		this.uniq_id = uniq_id;
	}
	public String getBank_id() {
		return bank_id;
	}
	public void setBank_id(String bank_id) {
		this.bank_id = bank_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBank_bal() {
		return bank_bal;
	}
	public void setBank_bal(String bank_bal) {
		this.bank_bal = bank_bal;
	}
	public String getBank_flag() {
		return bank_flag;
	}
	public void setBank_flag(String bank_flag) {
		this.bank_flag = bank_flag;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	public String getExtra6() {
		return extra6;
	}
	public void setExtra6(String extra6) {
		this.extra6 = extra6;
	}
	public String getExtra7() {
		return extra7;
	}
	public void setExtra7(String extra7) {
		this.extra7 = extra7;
	}
	public String getExtra8() {
		return extra8;
	}
	public void setExtra8(String extra8) {
		this.extra8 = extra8;
	}
	public String getExtra9() {
		return extra9;
	}
	public void setExtra9(String extra9) {
		this.extra9 = extra9;
	}
	public String getExtra10() {
		return extra10;
	}
	public void setExtra10(String extra10) {
		this.extra10 = extra10;
	} 
}
