package beans;
public class cancellation_details 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String cancel_id="";
private String booking_id="";
private String refund_amount_total="";
private String refund_excludetax="";
private String refund_includetax="";
private String r_vat="";
private String r_mvat="";
private String r_luxarytax="";
private String refund_date="";
private String refund_ref_num="";
private String refund_trans_num="";
private String refund_status="";
private String refund_tracking_num="";
private String gained_amount_total="";
private String gained_amount_excludetax="";
private String gained_amount_tax="";
private String g_vat="";
private String g_mvat="";
private String g_luxary_tax="";
private String g_service_charge="";
private String g_pg_charges="";
private String grand_total_paid="";
private String entry_date="";
private String closed_date="";
private String c_x="";
private String c_y="";
private String c_z="";
public cancellation_details() {} 
public cancellation_details(String cancel_id,String booking_id,String refund_amount_total,String refund_excludetax,String refund_includetax,String r_vat,String r_mvat,String r_luxarytax,String refund_date,String refund_ref_num,String refund_trans_num,String refund_status,String refund_tracking_num,String gained_amount_total,String gained_amount_excludetax,String gained_amount_tax,String g_vat,String g_mvat,String g_luxary_tax,String g_service_charge,String g_pg_charges,String grand_total_paid,String entry_date,String closed_date,String c_x,String c_y,String c_z)
 {
this.cancel_id=cancel_id;
this.booking_id=booking_id;
this.refund_amount_total=refund_amount_total;
this.refund_excludetax=refund_excludetax;
this.refund_includetax=refund_includetax;
this.r_vat=r_vat;
this.r_mvat=r_mvat;
this.r_luxarytax=r_luxarytax;
this.refund_date=refund_date;
this.refund_ref_num=refund_ref_num;
this.refund_trans_num=refund_trans_num;
this.refund_status=refund_status;
this.refund_tracking_num=refund_tracking_num;
this.gained_amount_total=gained_amount_total;
this.gained_amount_excludetax=gained_amount_excludetax;
this.gained_amount_tax=gained_amount_tax;
this.g_vat=g_vat;
this.g_mvat=g_mvat;
this.g_luxary_tax=g_luxary_tax;
this.g_service_charge=g_service_charge;
this.g_pg_charges=g_pg_charges;
this.grand_total_paid=grand_total_paid;
this.entry_date=entry_date;
this.closed_date=closed_date;
this.c_x=c_x;
this.c_y=c_y;
this.c_z=c_z;

} 
public String getcancel_id() {
return cancel_id;
}
public void setcancel_id(String cancel_id) {
this.cancel_id = cancel_id;
}
public String getbooking_id() {
return booking_id;
}
public void setbooking_id(String booking_id) {
this.booking_id = booking_id;
}
public String getrefund_amount_total() {
return refund_amount_total;
}
public void setrefund_amount_total(String refund_amount_total) {
this.refund_amount_total = refund_amount_total;
}
public String getrefund_excludetax() {
return refund_excludetax;
}
public void setrefund_excludetax(String refund_excludetax) {
this.refund_excludetax = refund_excludetax;
}
public String getrefund_includetax() {
return refund_includetax;
}
public void setrefund_includetax(String refund_includetax) {
this.refund_includetax = refund_includetax;
}
public String getr_vat() {
return r_vat;
}
public void setr_vat(String r_vat) {
this.r_vat = r_vat;
}
public String getr_mvat() {
return r_mvat;
}
public void setr_mvat(String r_mvat) {
this.r_mvat = r_mvat;
}
public String getr_luxarytax() {
return r_luxarytax;
}
public void setr_luxarytax(String r_luxarytax) {
this.r_luxarytax = r_luxarytax;
}
public String getrefund_date() {
return refund_date;
}
public void setrefund_date(String refund_date) {
this.refund_date = refund_date;
}
public String getrefund_ref_num() {
return refund_ref_num;
}
public void setrefund_ref_num(String refund_ref_num) {
this.refund_ref_num = refund_ref_num;
}
public String getrefund_trans_num() {
return refund_trans_num;
}
public void setrefund_trans_num(String refund_trans_num) {
this.refund_trans_num = refund_trans_num;
}
public String getrefund_status() {
return refund_status;
}
public void setrefund_status(String refund_status) {
this.refund_status = refund_status;
}
public String getrefund_tracking_num() {
return refund_tracking_num;
}
public void setrefund_tracking_num(String refund_tracking_num) {
this.refund_tracking_num = refund_tracking_num;
}
public String getgained_amount_total() {
return gained_amount_total;
}
public void setgained_amount_total(String gained_amount_total) {
this.gained_amount_total = gained_amount_total;
}
public String getgained_amount_excludetax() {
return gained_amount_excludetax;
}
public void setgained_amount_excludetax(String gained_amount_excludetax) {
this.gained_amount_excludetax = gained_amount_excludetax;
}
public String getgained_amount_tax() {
return gained_amount_tax;
}
public void setgained_amount_tax(String gained_amount_tax) {
this.gained_amount_tax = gained_amount_tax;
}
public String getg_vat() {
return g_vat;
}
public void setg_vat(String g_vat) {
this.g_vat = g_vat;
}
public String getg_mvat() {
return g_mvat;
}
public void setg_mvat(String g_mvat) {
this.g_mvat = g_mvat;
}
public String getg_luxary_tax() {
return g_luxary_tax;
}
public void setg_luxary_tax(String g_luxary_tax) {
this.g_luxary_tax = g_luxary_tax;
}
public String getg_service_charge() {
return g_service_charge;
}
public void setg_service_charge(String g_service_charge) {
this.g_service_charge = g_service_charge;
}
public String getg_pg_charges() {
return g_pg_charges;
}
public void setg_pg_charges(String g_pg_charges) {
this.g_pg_charges = g_pg_charges;
}
public String getgrand_total_paid() {
return grand_total_paid;
}
public void setgrand_total_paid(String grand_total_paid) {
this.grand_total_paid = grand_total_paid;
}
public String getentry_date() {
return entry_date;
}
public void setentry_date(String entry_date) {
this.entry_date = entry_date;
}
public String getclosed_date() {
return closed_date;
}
public void setclosed_date(String closed_date) {
this.closed_date = closed_date;
}
public String getc_x() {
return c_x;
}
public void setc_x(String c_x) {
this.c_x = c_x;
}
public String getc_y() {
return c_y;
}
public void setc_y(String c_y) {
this.c_y = c_y;
}
public String getc_z() {
return c_z;
}
public void setc_z(String c_z) {
this.c_z = c_z;
}

}