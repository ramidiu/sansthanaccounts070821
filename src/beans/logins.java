package beans;
public class logins 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String login_id="";
private String name="";
private String email_id="";
private String mobile_number="";
private String username="";
private String password="";
private String role="";
private String address="";
private String branch="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String extra6="";
private String extra7="";
private String extra8="";
private String gender="";
private String qualification="";
private String doj="";
private String salary="";
public logins() {} 
public logins(String login_id,String name,String email_id,String mobile_number,String username,String password,String role,String address,String branch,String extra1,String extra2,String extra3,String extra4,String extra5,String extra6,String extra7,String extra8,String gender,String qualification,String doj,String salary)
 {
this.login_id=login_id;
this.name=name;
this.email_id=email_id;
this.mobile_number=mobile_number;
this.username=username;
this.password=password;
this.role=role;
this.address=address;
this.branch=branch;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.gender=gender;
this.qualification=qualification;
this.doj=doj;
this.salary=salary;

} 
public logins(String login_id,String name,String email_id,String mobile_number,String salary)
{
this.login_id=login_id;
this.name=name;
this.email_id=email_id;
this.mobile_number=mobile_number;

this.salary=salary;
} 
public String getlogin_id() {
return login_id;
}
public void setlogin_id(String login_id) {
this.login_id = login_id;
}
public String getname() {
return name;
}
public void setname(String name) {
this.name = name;
}
public String getemail_id() {
return email_id;
}
public void setemail_id(String email_id) {
this.email_id = email_id;
}
public String getmobile_number() {
return mobile_number;
}
public void setmobile_number(String mobile_number) {
this.mobile_number = mobile_number;
}
public String getusername() {
return username;
}
public void setusername(String username) {
this.username = username;
}
public String getpassword() {
return password;
}
public void setpassword(String password) {
this.password = password;
}
public String getrole() {
return role;
}
public void setrole(String role) {
this.role = role;
}
public String getaddress() {
return address;
}
public void setaddress(String address) {
this.address = address;
}
public String getbranch() {
return branch;
}
public void setbranch(String branch) {
this.branch = branch;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}
public String getextra8() {
return extra8;
}
public void setextra8(String extra8) {
this.extra8 = extra8;
}
public String getgender() {
return gender;
}
public void setgender(String gender) {
this.gender = gender;
}
public String getqualification() {
return qualification;
}
public void setqualification(String qualification) {
this.qualification = qualification;
}
public String getdoj() {
return doj;
}
public void setdoj(String doj) {
this.doj = doj;
}
public String getsalary() {
return salary;
}
public void setsalary(String salary) {
this.salary = salary;
}

}