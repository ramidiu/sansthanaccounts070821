package beans;
public class shift_codes 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String shift_code_id="";
private String shift_code="";
private String from_time="";
private String to_time="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
public shift_codes() {} 
public shift_codes(String shift_code_id,String shift_code,String from_time,String to_time,String extra1,String extra2,String extra3,String extra4,String extra5)
 {
this.shift_code_id=shift_code_id;
this.shift_code=shift_code;
this.from_time=from_time;
this.to_time=to_time;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getshift_code_id() {
return shift_code_id;
}
public void setshift_code_id(String shift_code_id) {
this.shift_code_id = shift_code_id;
}
public String getshift_code() {
return shift_code;
}
public void setshift_code(String shift_code) {
this.shift_code = shift_code;
}
public String getfrom_time() {
return from_time;
}
public void setfrom_time(String from_time) {
this.from_time = from_time;
}
public String getto_time() {
return to_time;
}
public void setto_time(String to_time) {
this.to_time = to_time;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}

}