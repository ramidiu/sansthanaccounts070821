package beans;

public class physiotheraphyissues 
{
	private String physiotheraphyId="";
	private String datee="";
	private String appointmnetId="";
	private String machineName="";
	private String testName="";
	private String extra1=""; 
	private String extra2="";
	private String extra3=""; 
	private String extra4=""; 
	private String extra5="";
	
	public physiotheraphyissues() {}

	public physiotheraphyissues(String physiotheraphyId, String datee,
			String appointmnetId, String machineName, String testName,
			String extra1, String extra2, String extra3, String extra4,
			String extra5) 
	{
		this.physiotheraphyId = physiotheraphyId;
		this.datee = datee;
		this.appointmnetId = appointmnetId;
		this.machineName = machineName;
		this.testName = testName;
		this.extra1 = extra1;
		this.extra2 = extra2;
		this.extra3 = extra3;
		this.extra4 = extra4;
		this.extra5 = extra5;
	}

	public String getPhysiotheraphyId() {
		return physiotheraphyId;
	}

	public void setPhysiotheraphyId(String physiotheraphyId) {
		this.physiotheraphyId = physiotheraphyId;
	}

	public String getDatee() {
		return datee;
	}

	public void setDatee(String datee) {
		this.datee = datee;
	}

	public String getAppointmnetId() {
		return appointmnetId;
	}

	public void setAppointmnetId(String appointmnetId) {
		this.appointmnetId = appointmnetId;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getExtra1() {
		return extra1;
	}

	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}

	public String getExtra2() {
		return extra2;
	}

	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}

	public String getExtra3() {
		return extra3;
	}

	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}

	public String getExtra4() {
		return extra4;
	}

	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}

	public String getExtra5() {
		return extra5;
	}

	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	} 

}
