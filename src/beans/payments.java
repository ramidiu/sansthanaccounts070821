package beans;
public class payments 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String paymentId="";
private String date="";
private String bankId="";
private String narration="";
private String partyName="";
private String vendorId="";
private String reference="";
private String chequeNo="";
private String amount="";
private String remarks="";
private String paymentType="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String TBECResolutionNO="";
private String TBECDate="";
private String status="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";
private String service_tax_amount="";
private String service_cheque_name="";
private String service_cheque_no="";
public payments( String paymentId, String date, String bankId,
		String narration, String partyName, String vendorId, String reference,
		String chequeNo, String amount, String remarks, String paymentType,
		String extra1, String extra2, String extra3, String extra4,
		String extra5, String tBECResolutionNO, String tBECDate, String status,
		String extra6, String extra7, String extra8, String extra9,
		String service_tax_amount, String service_cheque_name,
		String service_cheque_no, String other_amount,
		String other_cheque_name, String other_cheque_no, String extra10,
		String extra11, String extra12, String extra13, String extra14) {
	super();
	this.paymentId = paymentId;
	this.date = date;
	this.bankId = bankId;
	this.narration = narration;
	this.partyName = partyName;
	this.vendorId = vendorId;
	this.reference = reference;
	this.chequeNo = chequeNo;
	this.amount = amount;
	this.remarks = remarks;
	this.paymentType = paymentType;
	this.extra1 = extra1;
	this.extra2 = extra2;
	this.extra3 = extra3;
	this.extra4 = extra4;
	this.extra5 = extra5;
	TBECResolutionNO = tBECResolutionNO;
	TBECDate = tBECDate;
	this.status = status;
	this.extra6 = extra6;
	this.extra7 = extra7;
	this.extra8 = extra8;
	this.extra9 = extra9;
	this.service_tax_amount = service_tax_amount;
	this.service_cheque_name = service_cheque_name;
	this.service_cheque_no = service_cheque_no;
	this.other_amount = other_amount;
	this.other_cheque_name = other_cheque_name;
	this.other_cheque_no = other_cheque_no;
	this.extra10 = extra10;
	this.extra11 = extra11;
	this.extra12 = extra12;
	this.extra13 = extra13;
	this.extra14 = extra14;
}
private String other_amount="";
private String other_cheque_name="";
private String other_cheque_no="";
private String extra10="";
private String extra11="";
private String extra12="";
private String extra13="";
private String extra14="";
public payments() {} 
public String getService_tax_amount() {
	return service_tax_amount;
}
public void setService_tax_amount(String service_tax_amount) {
	this.service_tax_amount = service_tax_amount;
}
public String getService_cheque_name() {
	return service_cheque_name;
}
public void setService_cheque_name(String service_cheque_name) {
	this.service_cheque_name = service_cheque_name;
}
public String getService_cheque_no() {
	return service_cheque_no;
}
public void setService_cheque_no(String service_cheque_no) {
	this.service_cheque_no = service_cheque_no;
}
public String getOther_amount() {
	return other_amount;
}
public void setOther_amount(String other_amount) {
	this.other_amount = other_amount;
}
public String getOther_cheque_name() {
	return other_cheque_name;
}
public void setOther_cheque_name(String other_cheque_name) {
	this.other_cheque_name = other_cheque_name;
}
public String getOther_cheque_no() {
	return other_cheque_no;
}
public void setOther_cheque_no(String other_cheque_no) {
	this.other_cheque_no = other_cheque_no;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}
public String getExtra14() {
	return extra14;
}
public void setExtra14(String extra14) {
	this.extra14 = extra14;
}
public payments(String paymentId,String date,String bankId,String narration,String partyName,String vendorId,String reference,String chequeNo,String amount,String remarks,String paymentType,String extra1,String extra2,String extra3,String extra4,String extra5,String TBECResolutionNO,String TBECDate,String status,String extra6,String extra7,String extra8,String extra9)
 {
this.paymentId=paymentId;
this.date=date;
this.bankId=bankId;
this.narration=narration;
this.partyName=partyName;
this.vendorId=vendorId;
this.reference=reference;
this.chequeNo=chequeNo;
this.amount=amount;
this.remarks=remarks;
this.paymentType=paymentType;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.TBECResolutionNO=TBECResolutionNO;
this.TBECDate=TBECDate;
this.status=status;
this.extra6=extra6;
this.extra7=extra7;
this.extra8=extra8;
this.extra9=extra9;

} 
public payments(String paymentId,String date,String bankId,String narration,String partyName,String vendorId,String reference,String chequeNo,String amount,String remarks,String paymentType,String extra1,String extra2,String extra3,String extra4,String extra5)
{
this.paymentId=paymentId;
this.date=date;
this.bankId=bankId;
this.narration=narration;
this.partyName=partyName;
this.vendorId=vendorId;
this.reference=reference;
this.chequeNo=chequeNo;
this.amount=amount;
this.remarks=remarks;
this.paymentType=paymentType;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;

} 
public String getpaymentId() {
return paymentId;
}
public void setpaymentId(String paymentId) {
this.paymentId = paymentId;
}
public String getdate() {
return date;
}
public void setdate(String date) {
this.date = date;
}
public String getbankId() {
return bankId;
}
public void setbankId(String bankId) {
this.bankId = bankId;
}
public String getnarration() {
return narration;
}
public void setnarration(String narration) {
this.narration = narration;
}
public String getpartyName() {
return partyName;
}
public void setpartyName(String partyName) {
this.partyName = partyName;
}
public String getvendorId() {
return vendorId;
}
public void setvendorId(String vendorId) {
this.vendorId = vendorId;
}
public String getreference() {
return reference;
}
public void setreference(String reference) {
this.reference = reference;
}
public String getchequeNo() {
return chequeNo;
}
public void setchequeNo(String chequeNo) {
this.chequeNo = chequeNo;
}
public String getamount() {
return amount;
}
public void setamount(String amount) {
this.amount = amount;
}
public String getremarks() {
return remarks;
}
public void setremarks(String remarks) {
this.remarks = remarks;
}
public String getpaymentType() {
return paymentType;
}
public void setpaymentType(String paymentType) {
this.paymentType = paymentType;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getTBECResolutionNO() {
return TBECResolutionNO;
}
public void setTBECResolutionNO(String TBECResolutionNO) {
this.TBECResolutionNO = TBECResolutionNO;
}
public String getTBECDate() {
return TBECDate;
}
public void setTBECDate(String TBECDate) {
this.TBECDate = TBECDate;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String getextra6() {
return extra6;
}
public void setextra6(String extra6) {
this.extra6 = extra6;
}
public String getextra7() {
return extra7;
}
public void setextra7(String extra7) {
this.extra7 = extra7;
}
public String getextra8() {
return extra8;
}
public void setextra8(String extra8) {
this.extra8 = extra8;
}
public String getextra9() {
return extra9;
}
public void setextra9(String extra9) {
this.extra9 = extra9;
}


}