package beans;

public class stafftimings 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String staff="";
 public void setstaff(String staff)
{
this.staff=staff;
}
public String getstaff()
{
return this.staff;
}
private String staff_id="";
public stafftimings(String staff_id,
		String staff_name, String designation, String status, String starttime,
		String start_period, String endtime, String end_period,
		String staff_type, String staff_year, String extra1, String extra2,
		String extra3, String extra4, String blood_group, String mobile_no,
		String staff_nid, String emergency_no, String address, String dOB,
		String extra5, String extra6, String next_week_shift, String extra7,
		String extra8, String extra9, String extra10) {
	super();
	this.staff_id = staff_id;
	this.staff_name = staff_name;
	this.designation = designation;
	this.status = status;
	this.starttime = starttime;
	this.start_period = start_period;
	this.endtime = endtime;
	this.end_period = end_period;
	this.staff_type = staff_type;
	this.staff_year = staff_year;
	this.extra1 = extra1;
	this.extra2 = extra2;
	this.extra3 = extra3;
	this.extra4 = extra4;
	this.blood_group = blood_group;
	this.mobile_no = mobile_no;
	this.staff_nid = staff_nid;
	this.emergency_no = emergency_no;
	this.address = address;
	DOB = dOB;
	this.extra5 = extra5;
	this.extra6 = extra6;
	this.next_week_shift = next_week_shift;
	this.extra7 = extra7;
	this.extra8 = extra8;
	this.extra9 = extra9;
	this.extra10 = extra10;
}
private String staff_name="";
private String designation="";
private String status="";
private String starttime="";
private String start_period="";
private String endtime="";
private String end_period=""; //RE-SIGNED DATE
private String staff_type="";
private String staff_year="";
private String extra1=""; //PHOTO UPLOAD
private String extra2=""; //DATE OF APPOINTMENT IN TRUST
private String extra3=""; //FATHER'S NAME
private String extra4=""; //MOTHER'S NAME
private String  blood_group=""; //blood_group
private String mobile_no=""; //mobile_no 
private String staff_nid="";
private String emergency_no=""; //PERMANENT ADDRESS
private String address=""; //PRESENT ADDRESS
private String DOB=""; //DATE OF BIRTH
private String  extra5=""; //SPOUSE NAME
private String extra6=""; //MOTHER TONGUE
private String next_week_shift="";
private String extra7="";
private String extra8="";
private String extra9="";
private String extra10="";
public stafftimings() {} 
public stafftimings(String staff ) {this.staff=staff;} 
public stafftimings(String staff_id,String staff_name,String designation,String status,String starttime,String start_period,String endtime,String end_period,String staff_type,String staff_year,String extra1,String extra2,String extra3,String extra4)
 {
this.staff_id=staff_id;
this.staff_name=staff_name;
this.designation=designation;
this.status=status;
this.starttime=starttime;
this.start_period=start_period;
this.endtime=endtime;
this.end_period=end_period;
this.staff_type=staff_type;
this.staff_year=staff_year;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;

} 
public String getstaff_id() {
return staff_id;
}
public void setstaff_id(String staff_id) {
this.staff_id = staff_id;
}
public String getstaff_name() {
return staff_name;
}
public void setstaff_name(String staff_name) {
this.staff_name = staff_name;
}
public String getdesignation() {
return designation;
}
public void setdesignation(String designation) {
this.designation = designation;
}
public String getstatus() {
return status;
}
public void setstatus(String status) {
this.status = status;
}
public String getstarttime() {
return starttime;
}
public void setstarttime(String starttime) {
this.starttime = starttime;
}
public String getstart_period() {
return start_period;
}
public void setstart_period(String start_period) {
this.start_period = start_period;
}
public String getendtime() {
return endtime;
}
public void setendtime(String endtime) {
this.endtime = endtime;
}
public String getend_period() {
return end_period;
}
public void setend_period(String end_period) {
this.end_period = end_period;
}
public String getstaff_type() {
return staff_type;
}
public void setstaff_type(String staff_type) {
this.staff_type = staff_type;
}
public String getstaff_year() {
return staff_year;
}
public void setstaff_year(String staff_year) {
this.staff_year = staff_year;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getBlood_group() {
	return blood_group;
}
public void setBlood_group(String blood_group) {
	this.blood_group = blood_group;
}
public String getStaff_nid() {
	return staff_nid;
}
public void setStaff_nid(String staff_nid) {
	this.staff_nid = staff_nid;
}
public String getMobile_no() {
	return mobile_no;
}
public void setMobile_no(String mobile_no) {
	this.mobile_no = mobile_no;
}
public String getEmergency_no() {
	return emergency_no;
}
public void setEmergency_no(String emergency_no) {
	this.emergency_no = emergency_no;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getDOB() {
	return DOB;
}
public void setDOB(String dOB) {
	DOB = dOB;
}
public String getExtra6() {
	return extra6;
}
public void setExtra6(String extra6) {
	this.extra6 = extra6;
}
public String getExtra5() {
	return extra5;
}
public void setExtra5(String extra5) {
	this.extra5 = extra5;
}
public String getNext_week_shift() {
	return next_week_shift;
}
public void setNext_week_shift(String next_week_shift) {
	this.next_week_shift = next_week_shift;
}
public String getExtra7() {
	return extra7;
}
public void setExtra7(String extra7) {
	this.extra7 = extra7;
}
public String getExtra8() {
	return extra8;
}
public void setExtra8(String extra8) {
	this.extra8 = extra8;
}
public String getExtra9() {
	return extra9;
}
public void setExtra9(String extra9) {
	this.extra9 = extra9;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
}