package beans;
public class advanceamount 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String advanceamount_id="";
private String datetime="";
private String master_suite="";
private String executive_suite="";
private String deluxe_suite="";
private String suite="";
private String deluxe_room_ac="";
private String special_room_nonac="";
private String deluxe_room_nonac="";
private String deluxe_room_ac_2beds="";
private String banquet_hall_5th_floor="";
private String banquet_hall_4th_floor="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String net_date="";
private String gross_master_suite="";
private String gross_executive_suite="";
private String gross_deluxe_suite="";
private String gross_suite="";
private String gross_deluxe_room_ac="";
private String gross_special_room_nonac="";
private String gross_deluxe_room_nonac="";
private String gross_deluxe_room_ac_2beds="";
private String gross_banquet_hall_5th_floor="";
private String gross_banquet_hall_4th_floor="";
private String gross_date="";
public advanceamount() {} 
public advanceamount(String advanceamount_id,String datetime,String master_suite,String executive_suite,String deluxe_suite,String suite,String deluxe_room_ac,String special_room_nonac,String deluxe_room_nonac,String deluxe_room_ac_2beds,String banquet_hall_5th_floor,String banquet_hall_4th_floor,String extra1,String extra2,String extra3,String extra4,String extra5,String net_date,String gross_master_suite,String gross_executive_suite,String gross_deluxe_suite,String gross_suite,String gross_deluxe_room_ac,String gross_special_room_nonac,String gross_deluxe_room_nonac,String gross_deluxe_room_ac_2beds,String gross_banquet_hall_5th_floor,String gross_banquet_hall_4th_floor,String gross_date)
 {
this.advanceamount_id=advanceamount_id;
this.datetime=datetime;
this.master_suite=master_suite;
this.executive_suite=executive_suite;
this.deluxe_suite=deluxe_suite;
this.suite=suite;
this.deluxe_room_ac=deluxe_room_ac;
this.special_room_nonac=special_room_nonac;
this.deluxe_room_nonac=deluxe_room_nonac;
this.deluxe_room_ac_2beds=deluxe_room_ac_2beds;
this.banquet_hall_5th_floor=banquet_hall_5th_floor;
this.banquet_hall_4th_floor=banquet_hall_4th_floor;
this.extra1=extra1;
this.extra2=extra2;
this.extra3=extra3;
this.extra4=extra4;
this.extra5=extra5;
this.net_date=net_date;
this.gross_master_suite=gross_master_suite;
this.gross_executive_suite=gross_executive_suite;
this.gross_deluxe_suite=gross_deluxe_suite;
this.gross_suite=gross_suite;
this.gross_deluxe_room_ac=gross_deluxe_room_ac;
this.gross_special_room_nonac=gross_special_room_nonac;
this.gross_deluxe_room_nonac=gross_deluxe_room_nonac;
this.gross_deluxe_room_ac_2beds=gross_deluxe_room_ac_2beds;
this.gross_banquet_hall_5th_floor=gross_banquet_hall_5th_floor;
this.gross_banquet_hall_4th_floor=gross_banquet_hall_4th_floor;
this.gross_date=gross_date;

} 
public String getadvanceamount_id() {
return advanceamount_id;
}
public void setadvanceamount_id(String advanceamount_id) {
this.advanceamount_id = advanceamount_id;
}
public String getdatetime() {
return datetime;
}
public void setdatetime(String datetime) {
this.datetime = datetime;
}
public String getmaster_suite() {
return master_suite;
}
public void setmaster_suite(String master_suite) {
this.master_suite = master_suite;
}
public String getexecutive_suite() {
return executive_suite;
}
public void setexecutive_suite(String executive_suite) {
this.executive_suite = executive_suite;
}
public String getdeluxe_suite() {
return deluxe_suite;
}
public void setdeluxe_suite(String deluxe_suite) {
this.deluxe_suite = deluxe_suite;
}
public String getsuite() {
return suite;
}
public void setsuite(String suite) {
this.suite = suite;
}
public String getdeluxe_room_ac() {
return deluxe_room_ac;
}
public void setdeluxe_room_ac(String deluxe_room_ac) {
this.deluxe_room_ac = deluxe_room_ac;
}
public String getspecial_room_nonac() {
return special_room_nonac;
}
public void setspecial_room_nonac(String special_room_nonac) {
this.special_room_nonac = special_room_nonac;
}
public String getdeluxe_room_nonac() {
return deluxe_room_nonac;
}
public void setdeluxe_room_nonac(String deluxe_room_nonac) {
this.deluxe_room_nonac = deluxe_room_nonac;
}
public String getdeluxe_room_ac_2beds() {
return deluxe_room_ac_2beds;
}
public void setdeluxe_room_ac_2beds(String deluxe_room_ac_2beds) {
this.deluxe_room_ac_2beds = deluxe_room_ac_2beds;
}
public String getbanquet_hall_5th_floor() {
return banquet_hall_5th_floor;
}
public void setbanquet_hall_5th_floor(String banquet_hall_5th_floor) {
this.banquet_hall_5th_floor = banquet_hall_5th_floor;
}
public String getbanquet_hall_4th_floor() {
return banquet_hall_4th_floor;
}
public void setbanquet_hall_4th_floor(String banquet_hall_4th_floor) {
this.banquet_hall_4th_floor = banquet_hall_4th_floor;
}
public String getextra1() {
return extra1;
}
public void setextra1(String extra1) {
this.extra1 = extra1;
}
public String getextra2() {
return extra2;
}
public void setextra2(String extra2) {
this.extra2 = extra2;
}
public String getextra3() {
return extra3;
}
public void setextra3(String extra3) {
this.extra3 = extra3;
}
public String getextra4() {
return extra4;
}
public void setextra4(String extra4) {
this.extra4 = extra4;
}
public String getextra5() {
return extra5;
}
public void setextra5(String extra5) {
this.extra5 = extra5;
}
public String getnet_date() {
return net_date;
}
public void setnet_date(String net_date) {
this.net_date = net_date;
}
public String getgross_master_suite() {
return gross_master_suite;
}
public void setgross_master_suite(String gross_master_suite) {
this.gross_master_suite = gross_master_suite;
}
public String getgross_executive_suite() {
return gross_executive_suite;
}
public void setgross_executive_suite(String gross_executive_suite) {
this.gross_executive_suite = gross_executive_suite;
}
public String getgross_deluxe_suite() {
return gross_deluxe_suite;
}
public void setgross_deluxe_suite(String gross_deluxe_suite) {
this.gross_deluxe_suite = gross_deluxe_suite;
}
public String getgross_suite() {
return gross_suite;
}
public void setgross_suite(String gross_suite) {
this.gross_suite = gross_suite;
}
public String getgross_deluxe_room_ac() {
return gross_deluxe_room_ac;
}
public void setgross_deluxe_room_ac(String gross_deluxe_room_ac) {
this.gross_deluxe_room_ac = gross_deluxe_room_ac;
}
public String getgross_special_room_nonac() {
return gross_special_room_nonac;
}
public void setgross_special_room_nonac(String gross_special_room_nonac) {
this.gross_special_room_nonac = gross_special_room_nonac;
}
public String getgross_deluxe_room_nonac() {
return gross_deluxe_room_nonac;
}
public void setgross_deluxe_room_nonac(String gross_deluxe_room_nonac) {
this.gross_deluxe_room_nonac = gross_deluxe_room_nonac;
}
public String getgross_deluxe_room_ac_2beds() {
return gross_deluxe_room_ac_2beds;
}
public void setgross_deluxe_room_ac_2beds(String gross_deluxe_room_ac_2beds) {
this.gross_deluxe_room_ac_2beds = gross_deluxe_room_ac_2beds;
}
public String getgross_banquet_hall_5th_floor() {
return gross_banquet_hall_5th_floor;
}
public void setgross_banquet_hall_5th_floor(String gross_banquet_hall_5th_floor) {
this.gross_banquet_hall_5th_floor = gross_banquet_hall_5th_floor;
}
public String getgross_banquet_hall_4th_floor() {
return gross_banquet_hall_4th_floor;
}
public void setgross_banquet_hall_4th_floor(String gross_banquet_hall_4th_floor) {
this.gross_banquet_hall_4th_floor = gross_banquet_hall_4th_floor;
}
public String getgross_date() {
return gross_date;
}
public void setgross_date(String gross_date) {
this.gross_date = gross_date;
}

}