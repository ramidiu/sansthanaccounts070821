package beans;

public class advance_amount 
{
	private String advanceamount_id="";
	private String datetime="";
	private String master_suite="";
	private String executive_suite="";
	private String deluxe_suite="";
	private String suite="";
	private String deluxe_room_ac="";
	private String special_room_nonac="";
	private String deluxe_room_nonac="";
	private String deluxe_room_ac_2beds="";
	private String banquet_hall_5th_floor="";
	private String banquet_hall_4th_floor="";
	private String extra1="";
	private String extra2="";
	private String extra3="";
	private String extra4="";
	private String extra5="";
	public String getAdvanceamount_id() {
		return advanceamount_id;
	}
	public void setAdvanceamount_id(String advanceamountId) {
		advanceamount_id = advanceamountId;
	}
	
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getMaster_suite() {
		return master_suite;
	}
	public void setMaster_suite(String masterSuite) {
		master_suite = masterSuite;
	}
	public String getExecutive_suite() {
		return executive_suite;
	}
	public void setExecutive_suite(String executiveSuite) {
		executive_suite = executiveSuite;
	}
	public String getDeluxe_suite() {
		return deluxe_suite;
	}
	public void setDeluxe_suite(String deluxeSuite) {
		deluxe_suite = deluxeSuite;
	}
	public String getSuite() {
		return suite;
	}
	public void setSuite(String suite) {
		this.suite = suite;
	}
	public String getDeluxe_room_ac() {
		return deluxe_room_ac;
	}
	public void setDeluxe_room_ac(String deluxeRoomAc) {
		deluxe_room_ac = deluxeRoomAc;
	}
	public String getSpecial_room_nonac() {
		return special_room_nonac;
	}
	public void setSpecial_room_nonac(String specialRoomNonac) {
		special_room_nonac = specialRoomNonac;
	}
	public String getDeluxe_room_nonac() {
		return deluxe_room_nonac;
	}
	public void setDeluxe_room_nonac(String deluxeRoomNonac) {
		deluxe_room_nonac = deluxeRoomNonac;
	}
	public String getDeluxe_room_ac_2beds() {
		return deluxe_room_ac_2beds;
	}
	public void setDeluxe_room_ac_2beds(String deluxeRoomAc_2beds) {
		deluxe_room_ac_2beds = deluxeRoomAc_2beds;
	}
	public String getBanquet_hall_5th_floor() {
		return banquet_hall_5th_floor;
	}
	public void setBanquet_hall_5th_floor(String banquetHall_5thFloor) {
		banquet_hall_5th_floor = banquetHall_5thFloor;
	}
	public String getBanquet_hall_4th_floor() {
		return banquet_hall_4th_floor;
	}
	public void setBanquet_hall_4th_floor(String banquetHall_4thFloor) {
		banquet_hall_4th_floor = banquetHall_4thFloor;
	}
	public String getExtra1() {
		return extra1;
	}
	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}
	public String getExtra2() {
		return extra2;
	}
	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}
	public String getExtra3() {
		return extra3;
	}
	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}
	public String getExtra4() {
		return extra4;
	}
	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}
	public String getExtra5() {
		return extra5;
	}
	public void setExtra5(String extra5) {
		this.extra5 = extra5;
	}
	
}
