package beans;
import java.sql.*;
import dbase.sqlcon.ConnectionHelper;

public class paymentsService 
 {
private String error="";
 public void seterror(String error)
{
this.error=error;
}
public String geterror()
{
return this.error;
}
private String paymentId="";
private String date="";
private String bankId="";
private String narration="";
private String partyName="";
private String vendorId="";
private String reference="";
private String chequeNo="";
private String amount="";
private String remarks="";
private String paymentType="";
private String extra1="";
private String extra2="";
private String extra3="";
private String extra4="";
private String extra5="";
private String TBECResolutionNO="";
private String TBECDate="";
private String status="";
private String extra6="";
private String extra7="";
private String extra8="";
private String extra9="";

private String service_tax_amount="";
private String service_cheque_name="";
private String service_cheque_no="";
private String other_amount="";
private String other_cheque_name="";
private String other_cheque_no="";
private String extra10="";
private String extra11="";
private String extra12="";
private String extra13="";
private String extra14="";
public String getService_tax_amount() {
	return service_tax_amount;
}
public void setService_tax_amount(String service_tax_amount) {
	this.service_tax_amount = service_tax_amount;
}
public String getService_cheque_name() {
	return service_cheque_name;
}
public void setService_cheque_name(String service_cheque_name) {
	this.service_cheque_name = service_cheque_name;
}
public String getService_cheque_no() {
	return service_cheque_no;
}
public void setService_cheque_no(String service_cheque_no) {
	this.service_cheque_no = service_cheque_no;
}
public String getOther_amount() {
	return other_amount;
}
public void setOther_amount(String other_amount) {
	this.other_amount = other_amount;
}
public String getOther_cheque_name() {
	return other_cheque_name;
}
public void setOther_cheque_name(String other_cheque_name) {
	this.other_cheque_name = other_cheque_name;
}
public String getOther_cheque_no() {
	return other_cheque_no;
}
public void setOther_cheque_no(String other_cheque_no) {
	this.other_cheque_no = other_cheque_no;
}
public String getExtra10() {
	return extra10;
}
public void setExtra10(String extra10) {
	this.extra10 = extra10;
}
public String getExtra11() {
	return extra11;
}
public void setExtra11(String extra11) {
	this.extra11 = extra11;
}
public String getExtra12() {
	return extra12;
}
public void setExtra12(String extra12) {
	this.extra12 = extra12;
}
public String getExtra13() {
	return extra13;
}
public void setExtra13(String extra13) {
	this.extra13 = extra13;
}
public String getExtra14() {
	return extra14;
}
public void setExtra14(String extra14) {
	this.extra14 = extra14;
}
private ResultSet rs = null;
private Statement st = null;
private Connection c=null;
public paymentsService()
{
/*try {
 // Load the database driver
c = ConnectionHelper.getConnection();
st=c.createStatement();
}catch(Exception e){
this.seterror(e.toString());
System.out.println("Exception is ;"+e);
}*/
 }

public void setpaymentId(String paymentId)
{
this.paymentId = paymentId;
}

public String getpaymentId()
{
return (this.paymentId);
}


public void setdate(String date)
{
this.date = date;
}

public String getdate()
{
return (this.date);
}


public void setbankId(String bankId)
{
this.bankId = bankId;
}

public String getbankId()
{
return (this.bankId);
}


public void setnarration(String narration)
{
this.narration = narration;
}

public String getnarration()
{
return (this.narration);
}


public void setpartyName(String partyName)
{
this.partyName = partyName;
}

public String getpartyName()
{
return (this.partyName);
}


public void setvendorId(String vendorId)
{
this.vendorId = vendorId;
}

public String getvendorId()
{
return (this.vendorId);
}


public void setreference(String reference)
{
this.reference = reference;
}

public String getreference()
{
return (this.reference);
}


public void setchequeNo(String chequeNo)
{
this.chequeNo = chequeNo;
}

public String getchequeNo()
{
return (this.chequeNo);
}


public void setamount(String amount)
{
this.amount = amount;
}

public String getamount()
{
return (this.amount);
}


public void setremarks(String remarks)
{
this.remarks = remarks;
}

public String getremarks()
{
return (this.remarks);
}


public void setpaymentType(String paymentType)
{
this.paymentType = paymentType;
}

public String getpaymentType()
{
return (this.paymentType);
}


public void setextra1(String extra1)
{
this.extra1 = extra1;
}

public String getextra1()
{
return (this.extra1);
}


public void setextra2(String extra2)
{
this.extra2 = extra2;
}

public String getextra2()
{
return (this.extra2);
}


public void setextra3(String extra3)
{
this.extra3 = extra3;
}

public String getextra3()
{
return (this.extra3);
}


public void setextra4(String extra4)
{
this.extra4 = extra4;
}

public String getextra4()
{
return (this.extra4);
}


public void setextra5(String extra5)
{
this.extra5 = extra5;
}

public String getextra5()
{
return (this.extra5);
}


public void setTBECResolutionNO(String TBECResolutionNO)
{
this.TBECResolutionNO = TBECResolutionNO;
}

public String getTBECResolutionNO()
{
return (this.TBECResolutionNO);
}


public void setTBECDate(String TBECDate)
{
this.TBECDate = TBECDate;
}

public String getTBECDate()
{
return (this.TBECDate);
}


public void setstatus(String status)
{
this.status = status;
}

public String getstatus()
{
return (this.status);
}


public void setextra6(String extra6)
{
this.extra6 = extra6;
}

public String getextra6()
{
return (this.extra6);
}


public void setextra7(String extra7)
{
this.extra7 = extra7;
}

public String getextra7()
{
return (this.extra7);
}


public void setextra8(String extra8)
{
this.extra8 = extra8;
}

public String getextra8()
{
return (this.extra8);
}


public void setextra9(String extra9)
{
this.extra9 = extra9;
}

public String getextra9()
{
return (this.extra9);
}

public boolean insert()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
rs=st.executeQuery("select * from no_genarator where table_name='payments'");
rs.next();
int ticket=rs.getInt("table_id");
String PriSt =rs.getString("id_prefix");
rs.close();
int ticketm=ticket+1;
 paymentId=PriSt+ticketm;
String query="insert into payments values('"+paymentId+"','"+date+"','"+bankId+"','"+narration+"','"+partyName+"','"+vendorId+"','"+reference+"','"+chequeNo+"','"+amount+"','"+remarks+"','"+paymentType+"','"+extra1+"','"+extra2+"','"+extra3+"','"+extra4+"','"+extra5+"','"+TBECResolutionNO+"','"+TBECDate+"','"+status+"','"+extra6+"','"+extra7+"','"+extra8+"','"+extra9+"','"+service_tax_amount+"','"+ service_cheque_name+"','"+ service_cheque_no+"','"+ other_amount+"','"+ other_cheque_name+"','"+ other_cheque_no+"','"+ extra10+"','"+ extra11+"','"+ extra12+"','"+ extra13+"','"+ extra14+"')";
System.out.println(" insert payment service============================================ "+query);
st.executeUpdate(query);
st.executeUpdate("update no_genarator set table_id="+ticketm+" where table_name='payments'");
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean update()
{
try
{
	c = ConnectionHelper.getConnection();
	st=c.createStatement();
String updatecc="set ";
 int jk=0; 
if(!date.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"date = '"+date+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",date = '"+date+"'";}
}

if(!bankId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"bankId = '"+bankId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",bankId = '"+bankId+"'";}
}

if(!narration.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"narration = '"+narration+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",narration = '"+narration+"'";}
}

if(!partyName.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"partyName = '"+partyName+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",partyName = '"+partyName+"'";}
}

if(!vendorId.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"vendorId = '"+vendorId+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",vendorId = '"+vendorId+"'";}
}

if(!reference.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"reference = '"+reference+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",reference = '"+reference+"'";}
}

if(!chequeNo.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"chequeNo = '"+chequeNo+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",chequeNo = '"+chequeNo+"'";}
}

if(!amount.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"amount = '"+amount+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",amount = '"+amount+"'";}
}

if(!remarks.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"remarks = '"+remarks+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",remarks = '"+remarks+"'";}
}

if(!paymentType.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"paymentType = '"+paymentType+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",paymentType = '"+paymentType+"'";}
}

if(!extra1.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra1 = '"+extra1+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra1 = '"+extra1+"'";}
}

if(!extra2.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra2 = '"+extra2+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra2 = '"+extra2+"'";}
}

if(!extra3.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra3 = '"+extra3+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra3 = '"+extra3+"'";}
}

if(!extra4.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra4 = '"+extra4+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra4 = '"+extra4+"'";}
}

if(!extra5.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra5 = '"+extra5+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra5 = '"+extra5+"'";}
}

if(!TBECResolutionNO.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"TBECResolutionNO = '"+TBECResolutionNO+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",TBECResolutionNO = '"+TBECResolutionNO+"'";}
}

if(!TBECDate.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"TBECDate = '"+TBECDate+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",TBECDate = '"+TBECDate+"'";}
}

if(!status.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"status = '"+status+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",status = '"+status+"'";}
}

if(!extra6.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra6 = '"+extra6+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra6 = '"+extra6+"'";}
}

if(!extra7.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra7 = '"+extra7+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra7 = '"+extra7+"'";}
}

if(!extra8.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra8 = '"+extra8+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra8 = '"+extra8+"'";}
}

if(!extra9.equals("")){ 
if(jk==0)
{
updatecc=updatecc+"extra9 = '"+extra9+"'"; 
jk=1; 
}else
{
updatecc=updatecc+",extra9 = '"+extra9+"'";}
}
String query="update payments "+updatecc+" where paymentId='"+paymentId+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}public boolean delete()
{
try
{c = ConnectionHelper.getConnection();
st=c.createStatement();
String query="delete from payments where paymentId='"+paymentId+"'";
st.executeUpdate(query);
st.close();
c.close();
return true;
}catch(Exception e){
this.seterror(e.toString());
return false;
}

}
}