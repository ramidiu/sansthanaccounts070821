// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   HtmlMail.java

package MailBox;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.StringTokenizer;

public class HtmlMail
{

    public HtmlMail()
    {
    }

    public void setrespText(String s)
    {
        respText = s;
    }

    public String getrespText()
    {
        return respText;
    }

    public HashMap httpcall(String s, String s1, String s2, String s3, String s4)
    {
        String s5 = (new StringBuilder()).append("http://www.kreativwebsolutions.co.uk/").append(s4).toString();
        String s6 = "2.3";
        String s7 = "Mozilla/4.0";
        respText = "vivek";
        HashMap hashmap = null;
        String s8 = (new StringBuilder()).append("To=").append(s3).append("&From=").append(s1).append("&Message=").append(s2).append("&Subject=").append(s).toString();
        try
        {
            URL url = new URL(s5);
            HttpURLConnection httpurlconnection = (HttpURLConnection)url.openConnection();
            httpurlconnection.setDoInput(true);
            httpurlconnection.setDoOutput(true);
            httpurlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpurlconnection.setRequestProperty("User-Agent", s7);
            httpurlconnection.setRequestProperty("Content-Length", String.valueOf(s8.length()));
            httpurlconnection.setRequestMethod("POST");
            DataOutputStream dataoutputstream = new DataOutputStream(httpurlconnection.getOutputStream());
            dataoutputstream.writeBytes(s8);
            dataoutputstream.flush();
            dataoutputstream.close();
            DataInputStream datainputstream = new DataInputStream(httpurlconnection.getInputStream());
            int i = httpurlconnection.getResponseCode();
            if(i != -1)
            {
                BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()));
                for(String s9 = null; (s9 = bufferedreader.readLine()) != null;)
                    respText = (new StringBuilder()).append(respText).append(s9).toString();

                setrespText(respText);
                hashmap = deformatNVP(respText);
            }
            return hashmap;
        }
        catch(IOException ioexception)
        {
            return null;
        }
    }

    public HashMap deformatNVP(String s)
    {
        HashMap hashmap = new HashMap();
        StringTokenizer stringtokenizer = new StringTokenizer(s, "&");
        do
        {
            if(!stringtokenizer.hasMoreTokens())
                break;
            StringTokenizer stringtokenizer1 = new StringTokenizer(stringtokenizer.nextToken(), "=");
            if(stringtokenizer1.countTokens() == 2)
            {
                String s1 = URLDecoder.decode(stringtokenizer1.nextToken());
                String s2 = URLDecoder.decode(stringtokenizer1.nextToken());
                hashmap.put(s1.toUpperCase(), s2);
            }
        } while(true);
        return hashmap;
    }

    private String respText;
}
