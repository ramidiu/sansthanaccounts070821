package helperClasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import helperClasses.GetDaysInAMonth;

public class SplitDatesInToMonths {
	
	GetDaysInAMonth getDays = new GetDaysInAMonth();
	
	public SplitDatesInToMonths() {		
	}
	
	public List<String> getListOfDates(String fromdate,String todate)
	{
		List<String> list=new ArrayList<String>();
		
		String[] frmDate = fromdate.split("-");
		String[] toDate = todate.split("-");
		
		String fdate = frmDate[2];
		String fmonth = frmDate[1];
		String fyear = frmDate[0];
		String tdate = toDate[2];
		String tmonth = toDate[1];
		String tyear = toDate[0];
		int fmonthInInt = Integer.parseInt(frmDate[1]);
		int fyearInInt = Integer.parseInt(frmDate[0]);
		int tmonthInInt = Integer.parseInt(toDate[1]);
		int tyearInInt = Integer.parseInt(toDate[0]);		
		
		
		if(fmonth.equals(tmonth) && fyear.equals(tyear))
		{
			list.add(fyear+"-"+fmonth+"-"+fdate);
			list.add(tyear+"-"+tmonth+"-"+tdate);
		}
		
		else if(!fmonth.equals(tmonth) && fyear.equals(tyear))
		{
			list.add(fyear+"-"+fmonth+"-"+fdate);
			list.add(fyear+"-"+fmonth+"-"+getDays.getDaysInMonth(fmonth, fyear));
			
			for(int j = fmonthInInt+1; j <= tmonthInInt; j++)
			{
				String month = "";
				if(j < 10)
				{
					month = "0"+j;
				}
				else
				{
					month = ""+j;
				}
				
				if(j == tmonthInInt)
				{			
					list.add(fyear+"-"+tmonth+"-"+"01");
					list.add(fyear+"-"+tmonth+"-"+tdate);
					
					break;
				}
				
				list.add(fyear+"-"+month+"-"+"01");
				list.add(fyear+"-"+month+"-"+getDays.getDaysInMonth(month, fyear));
			}
		}
		
		else if(!fyear.equals(tyear))
		{
			list.add(fyear+"-"+fmonth+"-"+fdate);
			list.add(fyear+"-"+fmonth+"-"+getDays.getDaysInMonth(fmonth, fyear));
			
			year:
				for(int j = fyearInInt; j <= tyearInInt; j++)
				{
					if(j == tyearInInt)
					{
						for(int k = 1; k <= tmonthInInt; k++)
						{
							String month = "";
							if(k < 10)
							{
								month = "0"+k;
							}
							else
							{
								month = ""+k;
							}
							
							if(k == tmonthInInt)
							{
								list.add(tyear+"-"+tmonth+"-"+"01");
								list.add(tyear+"-"+tmonth+"-"+tdate);
								
								break year;
							}
							
							list.add(tyear+"-"+month+"-"+"01");
							list.add(tyear+"-"+month+"-"+getDays.getDaysInMonth(month, tyear));
						}
					}
					
					else if(j == fyearInInt)
					{
						for(int l = fmonthInInt+1; l <= 12; l++)
						{
							String month = "";
							if(l < 10)
							{
								month = "0"+l;
							}
							else
							{
								month = ""+l;
							}
							
							list.add(fyear+"-"+month+"-"+"01");
							list.add(fyear+"-"+month+"-"+getDays.getDaysInMonth(month, fyear));
						}
					}
					
					else
					{
						for(int m = 1; m <= 12; m++)
						{
							String month = "";
							if(m < 10)
							{
								month = "0"+m;
							}
							else
							{
								month = ""+m;
							}
							
							list.add(j+"-"+month+"-"+"01");
							list.add(j+"-"+month+"-"+getDays.getDaysInMonth(month, ""+j));
						}
					}
				}							
		}
		
		return list;
	}
}
