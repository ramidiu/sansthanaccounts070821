package helperClasses;

import java.util.ArrayList;
import java.util.List;

public enum DevoteeGroups {

	DONOR,TRUSTEES,NAD,LTA,RDS,TIME_SHARE,EMPLOYEE,GENERAL,VOLUNTEER;
	public static List<String> getAllDevotees(){
		List<String> list=new ArrayList<String>();
		list.add(DONOR.toString());
		list.add(TRUSTEES.toString());
		list.add(NAD.toString());
		list.add(LTA.toString());
		list.add(RDS.toString());
		list.add(TIME_SHARE.toString());
		/*list.add(EMPLOYEE.toString());*/
		list.add(GENERAL.toString());
		/*list.add(VOLUNTEER.toString());*/
		
		return list;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
