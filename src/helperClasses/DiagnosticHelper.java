package helperClasses;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mainClasses.diagnosticissuesListing;

import beans.diagnosticissues;
import beans.doctordetails;
import beans.patientsentry;

public class DiagnosticHelper 
{
	private String error="Exception in helperClasses.DiagnosticHelper-";

	public String getError() {
		return error;
	}
	
	public List<diagnosticissues> getDiagnosticDetails(String fromDate,String toDate,String submitType,String paidFree,String doctorId)
	{
		List<diagnosticissues> list=new ArrayList<diagnosticissues>();
		try{
			
			String fromTo[]=getFromAndToDateBasedOnSubmitType(fromDate,toDate,submitType);
			diagnosticissuesListing   listing=new diagnosticissuesListing();
			list=listing.getPaidFreeDiagnosticBetweenDates(fromTo[0], fromTo[1],paidFree,doctorId);
		}
		catch(Exception e)
		{
			error=error+"getDiagnosticDetails-"+e.toString();
		}
		return list;
	}
	public List<diagnosticissues> getDiagnosticDetails(String fromDate,String toDate,String submitType,String paidFree,String doctorId,String patientId)
	{
		List<diagnosticissues> list=new ArrayList<diagnosticissues>();
		try{
			
			String fromTo[]=getFromAndToDateBasedOnSubmitType(fromDate,toDate,submitType);
			diagnosticissuesListing   listing=new diagnosticissuesListing();
			list=listing.getPaidFreeDiagnosticBasedOnPatient(fromTo[0], fromTo[1],paidFree,doctorId,patientId);
		}
		catch(Exception e)
		{
			error=error+"getDiagnosticDetails-"+e.toString();
		}
		return list;
	}
	public List<doctordetails> getDoctorId(String fromDate,String toDate,String submitType,String paidFree)
	{
		List<doctordetails> list=new ArrayList<doctordetails>();
		try{
			
			String fromTo[]=getFromAndToDateBasedOnSubmitType(fromDate,toDate,submitType);
			diagnosticissuesListing   listing=new diagnosticissuesListing();
			list=listing.getDoctorIdBetweenDates(fromTo[0], fromTo[1],paidFree);
		}
		catch(Exception e)
		{
			error=error+"getDiagnosticDetails-"+e.toString();
		}
		return list;
		
	}
	public List<patientsentry> getpatientIds(String fromDate,String toDate,String submitType,String paidFree,String doctorId)
	{
		List<patientsentry> list=new ArrayList<patientsentry>();
		try{
			
			String fromTo[]=getFromAndToDateBasedOnSubmitType(fromDate,toDate,submitType);
			diagnosticissuesListing   listing=new diagnosticissuesListing();
			list=listing.getPatientIdBasedOnDoctorId(fromTo[0], fromTo[1],paidFree,doctorId);
		}
		catch(Exception e)
		{
			error=error+"getDiagnosticDetails-"+e.toString();
		}
		return list;
		
	}
	/** 
	 reusable method ,this method should be used when we today date ,this week date ,this month date and this fiscal year date 
	 @author Aaditya
	*/
	public String[] getFromAndToDateBasedOnSubmitType(String fromDate,String toDate,String submitType)
	{
		String fromTo[]=new String[2];
		String from="";
		String to="";
		try{
			
			DateFormat mySqlOnlyDateFormat=new SimpleDateFormat("yyyy-MM-dd");
			String suffix1=" 00:00:00";
			String suffix2=" 23:59:59";
			
			if(fromDate!=null)
			{
				from=fromDate;
			}
			if(toDate!=null)
			{
				to=toDate;
			}
			/*if("Submit".equals(submitType))*/
			if("Search".equals(submitType))
			{
				from=fromDate+suffix1;
				to=toDate+suffix2;
				
			}else if("Today".equals(submitType)){
				Date now =new Date();
				String date=mySqlOnlyDateFormat.format(now);
				from=date+suffix1;
				to=date+suffix2;
			}else if("This Week".equals(submitType)){
				Calendar c1=Calendar.getInstance();
				c1.set(c1.DAY_OF_WEEK, c1.SUNDAY);
				from=mySqlOnlyDateFormat.format(c1.getTime())+suffix1; 
				
				Calendar c2=Calendar.getInstance();
				c2.set(c2.DAY_OF_WEEK, c2.SATURDAY);
				to=mySqlOnlyDateFormat.format(c2.getTime())+suffix2; 
			}else if("This Month".equals(submitType)){
				Calendar c1 = Calendar.getInstance();
				c1.set(c1.DAY_OF_MONTH, c1.getActualMinimum(Calendar.DAY_OF_MONTH));
				from=mySqlOnlyDateFormat.format(c1.getTime())+suffix1; 
				
				Calendar c2 = Calendar.getInstance();
				c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));
				to=mySqlOnlyDateFormat.format(c2.getTime())+suffix2; 
			
				
			}else if("This Financial Year".equals(submitType)){
				Calendar c1=Calendar.getInstance();
				c1.set(c1.MONTH, c1.APRIL);
				c1.set(c1.DAY_OF_MONTH, c1.getActualMinimum(Calendar.DAY_OF_MONTH));
				from=mySqlOnlyDateFormat.format(c1.getTime())+suffix1;
				
				Calendar c2=Calendar.getInstance();
				c2.add(c2.YEAR, 1);
				c2.set(c2.MONTH, c2.MARCH);
				c2.set(c2.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));
				to=mySqlOnlyDateFormat.format(c2.getTime())+suffix2;
			}
		}
		catch(Exception e)
		{
			error=error+"getFromAndToDateBasedOnSubmitType-"+e.toString();
		}
		
		fromTo[0]=from;
		fromTo[1]=to;
		return fromTo;
	}
	

}
