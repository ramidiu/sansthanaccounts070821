package helperClasses;

import java.util.List;

public class GetDaysInAMonth {
	
	public String getDaysInMonth(String month,String year)
	{
		String days= "";
		if(month.equals("01") || month.equals("03") || month.equals("05") || month.equals("07") || month.equals("08") || month.equals("10") || month.equals("12"))
		{
			days = "31";
		}
		
		else if(month.equals("04") || month.equals("06") || month.equals("09") || month.equals("11"))
		{
			days = "30";
		}
		
		else if(month.equals("02"))
		{
			int yr = Integer.parseInt(year);
			
			if((yr % 400 == 0) || ((yr % 4 == 0) && (yr % 100 != 0)))
			{
				days = "29";
			}
			
			else
			{
				days = "28";
			}
		}
		
		return days;	
	}

}
