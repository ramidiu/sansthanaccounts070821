package helperClasses;

import java.util.ArrayList;
import java.util.List;

public class SaiMonths 
{
	public static final String JAN="January";
	public static final String FEB="February";
	public static final String MAR="March";
	public static final String APR="April";
	public static final String MAY="May";
	public static final String JUN="June";
	public static final String JUL="July";
	public static final String AUG="August";
	public static final String SEP="September";
	public static final String OCT="October";
	public static final String NOV="November";
	public static final String DEC="December";
	
	public static final String[] getMonths(){
		String months[]=new String[12];
		months[0]=JAN;
		months[1]=FEB;
		months[2]=MAR;
		months[3]=APR;
		months[4]=MAY;
		months[5]=JUN;
		months[6]=JUL;
		months[7]=AUG;
		months[8]=SEP;
		months[9]=OCT;
		months[10]=NOV;
		months[11]=DEC;
		return months;
	}
	
	public static List<String> getAllMonth()
	{
		List<String> list=new ArrayList<String>();
		list.add(JAN);
		list.add(FEB);
		list.add(MAR);
		list.add(JAN);
		list.add(APR);
		list.add(MAY);
		list.add(JUN);
		list.add(JUL);
		list.add(AUG);
		list.add(SEP);
		list.add(OCT);
		list.add(NOV);
		list.add(DEC);
		return list;
	}
	public static String getMonthValue(String month)
	{
		String monthValue="0";
		if(month!=null){
			if(month.trim().equalsIgnoreCase(JAN)){
				monthValue="1";
			}else if(month.trim().equalsIgnoreCase(FEB)){
				monthValue="2";
			}else if(month.trim().equalsIgnoreCase(MAR)){
				monthValue="3";
			}else if(month.trim().equalsIgnoreCase(APR)){
				monthValue="4";
			}else if(month.trim().equalsIgnoreCase(MAY)){
				monthValue="5";
			}else if(month.trim().equalsIgnoreCase(JUN)){
				monthValue="6";
			}else if(month.trim().equalsIgnoreCase(JUL)){
				monthValue="7";
			}else if(month.trim().equalsIgnoreCase(AUG)){
				monthValue="8";
			}else if(month.trim().equalsIgnoreCase(SEP)){
				monthValue="9";
			}else if(month.trim().equalsIgnoreCase(OCT)){
				monthValue="10";
			}else if(month.trim().equalsIgnoreCase(NOV)){
				monthValue="11";
			}else if(month.trim().equalsIgnoreCase(DEC)){
				monthValue="12";
			}
		}
		return monthValue;
		
	}
	
	public static String getMonthName(int monthValue)
	{
		String monthValue1="";
		
			if(monthValue == 0){
				monthValue1=JAN;
			}else if(monthValue == 1){
				monthValue1=FEB;
			}else if(monthValue == 2){
				monthValue1=MAR;
			}else if(monthValue ==3){
				monthValue1=APR;
			}else if(monthValue == 4){
				monthValue1=MAY;
			}else if(monthValue == 5){
				monthValue1=JUN;
			}else if(monthValue == 6){
				monthValue1=JUL;
			}else if(monthValue == 7){
				monthValue1=AUG;
			}else if(monthValue == 8){
				monthValue1=SEP;
			}else if(monthValue == 9){
				monthValue1=OCT;
			}else if(monthValue == 10){
				monthValue1=NOV;
			}else if(monthValue ==11){
				monthValue1=DEC;
			}
		
		return monthValue1;
		
	}

}
