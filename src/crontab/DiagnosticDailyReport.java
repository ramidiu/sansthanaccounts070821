package crontab;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import beans.diagnosticissues;

public class DiagnosticDailyReport {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		processDailyMail();
	}
	public static String processDailyMail(){
		
		MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  dateFormat_new= new SimpleDateFormat("dd MMM yyyy");
		mainClasses.doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
		mainClasses.appointmentsListing APPL=new mainClasses.appointmentsListing();
		mainClasses.diagnosticissuesListing MED_L=new mainClasses.diagnosticissuesListing();
		//beans.medicineissues MEDI=new beans.medicineissues();
		TimeZone tz = TimeZone.getTimeZone("IST");
		DecimalFormat df = new DecimalFormat("#.##");
		DecimalFormat unitsformat = new DecimalFormat("#.##");
		dateFormat2.setTimeZone(tz.getTimeZone("IST"));
		dateFormat_new.setTimeZone(tz.getTimeZone("IST"));
		Calendar c1 = Calendar.getInstance(); 
		String currentDate=(dateFormat2.format(c1.getTime())).toString();
		String presentdate=(dateFormat_new.format(c1.getTime())).toString();
		String fromDate=currentDate+" 00:00:01";
		String toDate=currentDate+" 23:59:59";
		double totalprice,doctrwiseprice=0.0;
		/*String fromDate="2016-03-25 00:00:01";
		String toDate="2016-03-25 23:59:59";*/
		int totPatients=0;
		doctrwiseprice=totalprice=0.0;
		beans.appointments APP=new beans.appointments();
		List Medicineissue=null;
		List<diagnosticissues> diagnosticIssueList=null;
		List DOCT_List=APPL.getDignosticsDoctorId(fromDate,toDate);
		String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=3> SSSST- DAILY DIAGNOSTIC REPORT </th></tr>";
		String s2="";
		String html="";
		int totalMedIssued=0;
		if(DOCT_List.size()>0){
			s2=s2+"<tr style=background:#8b421b;><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>DOCTOR NAME</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;> TOTAL PATIENTS VISITED </th></tr>";
		for(int i=0; i < DOCT_List.size(); i++ ){
			APP=(beans.appointments)DOCT_List.get(i);
			if(APP.getappointmnetId()!=null && !APP.getappointmnetId().equals("")){
				totPatients=totPatients+Integer.parseInt(APP.getappointmnetId());
			}
			s2=s2+"<tr style='background:#81f7f3;font-size:12px'><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+(i+1)+"</td><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+DOCT.getDoctorName(APP.getdoctorId())+"</td><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+APP.getappointmnetId()+"</td></tr>";
			//getDailyDaignosticIssueBasedOnDoctor
			diagnosticIssueList=MED_L.getDailyDaignosticIssueNameBasedOnDoctor(fromDate,toDate,APP.getdoctorId());
			
			if(diagnosticIssueList.size()>0){
				doctrwiseprice=0.0;
				s2=s2+"<tr><td colspan='3'><table align='center' border=1 width=100%> <tr style='background:#81f7f3;font-size:12px'><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Test Name</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>No Of Tests</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Unit Price</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Total Price</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Discount</th>";
			for(int k=0; k < diagnosticIssueList.size(); k++ ){
				diagnosticissues DIS=diagnosticIssueList.get(k);
				if(DIS.getExtra3()!=null && !DIS.getExtra3().equals("")){
					doctrwiseprice=doctrwiseprice+Double.parseDouble(DIS.getExtra3());
				}
				s2=s2+"<tr><td style=padding:5px;>"+DIS.getExtra5()+"</td><td style=padding:5px;>"+DIS.getDocumentNo() +"</td><td style=padding:5px;>"+DIS.getAmount() +"</td><td style=padding:5px;>&#8377; "+df.format(Double.parseDouble(DIS.getExtra3())) +" /-</td><td style=padding:5px;>"+DIS.getExtra2() +" %</td></tr>";
			}
			totalprice=doctrwiseprice+totalprice;
			s2=s2+"<tr><td style=padding:5px; colspan=3>Total Price</td><td style=padding:5px; colspan=1>&#8377; "+df.format(doctrwiseprice) +" /-</td></tr>";
			s2=s2+"</table></td></tr>";} else{
				s2=s2+"<tr><td style='color:#8b421b;font-size:14px;font-weight:bold;border-top:1px;text-align: center;' colspan=3>No,tests issued</td></tr>";
				
			}
		}
		totPatients=Integer.parseInt(MED_L.getTotalAppointment(fromDate, toDate));
		totalMedIssued=Integer.parseInt(MED_L.getTotalTest(fromDate, toDate));
		totalprice=Double.parseDouble(MED_L.getTotalAmount(fromDate, toDate));
		/*Medicineissue=MED_L.getDailymedicineissues(fromDate,toDate);
		if(Medicineissue.size()>0){
			for(int s=0;s<Medicineissue.size();s++){
				MEDI=(beans.medicineissues)Medicineissue.get(s);
				totalMedIssued=totalMedIssued+Integer.parseInt(MEDI.getquantity());
			}
		}*/
		s2=s2+"<tr><td style=padding:5px; colspan=2>TOTAL NO OF PATIENTS VISITED</td><td style=padding:5px;>"+totPatients+" PATIENTS </td></tr>";
		s2=s2+"<tr><td style=padding:5px; colspan=2>TOTAL QTY OF TEST ISSUED</td><td style=padding:5px;>"+totalMedIssued+" Units</td></tr>";
		s2=s2+"<tr><td style=padding:5px; colspan=2>TOTAL COST OF TEST ISSUED</td><td style=padding:5px;>&#8377;  "+df.format(totalprice)+" /-</td></tr>";
		}else{
			s2=s2+"<tr><td style=padding:5px;color:red;font-size:20px; align='center' colspan=3>Sorry,there is no data entered for today</td></tr>";
		}
		
		html=s1+s2+"</table>";
		//System.out.println(html);
		
		/*sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST- DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST- DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST-  DAILY DIAGNOSTIC REPORT", html,"localhost");*/
		return html;
	}
}
