package crontab;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import mainClasses.productexpensesListing;
import beans.productexpenses_sc;
import beans.productexpenses_scService;

public class SundryCreditorsCronJob {
	public static void main(String[] args) throws ParseException, SQLException{
		
		productexpenses_sc PES = new productexpenses_sc();
		productexpenses_scService PESS = new productexpenses_scService();
		productexpensesListing PEList = new productexpensesListing();
		
		DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd");
		
		 Calendar c = Calendar.getInstance();
		 c.add(Calendar.MONTH, -1);
		 c.set(Calendar.DATE, 1);
		 
		 String firstDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();
		 c.set(Calendar.DATE,c.getActualMaximum(Calendar.DAY_OF_MONTH));
		 String lastDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();
		 
		 String fromdate=firstDateOfPreviousMonth+" 00:00:00";
		 String todate=lastDateOfPreviousMonth+" 23:59:59";
		 
		 List PE_list = PEList.getPaymentsBetweenDates(fromdate,todate);
		 
		 if(PE_list.size()>0){
			 for(int i=0;i<PE_list.size();i++){
				 PESS=(beans.productexpenses_scService)PE_list.get(i);
				 PESS.insert();
			 }
		 }
		
	}

}
