package crontab;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class MSEReport {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		process();
	}
	public static int process(){
		try{
			MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
			sms.CallSmscApi SMS=new sms.CallSmscApi();
			DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dateFormat_new= new SimpleDateFormat("dd MMM yyyy");
			mainClasses.godwanstockListing GODL=new mainClasses.godwanstockListing();
			mainClasses.headofaccountsListing HODA=new mainClasses.headofaccountsListing();
			beans.headofaccounts HOD=new beans.headofaccounts();
			beans.godwanstock GODS=new beans.godwanstock();
			TimeZone tz = TimeZone.getTimeZone("IST");
			DecimalFormat df = new DecimalFormat("#.##");
			dateFormat2.setTimeZone(tz.getTimeZone("IST"));
			dateFormat_new.setTimeZone(tz.getTimeZone("IST"));
			Calendar c1 = Calendar.getInstance(); 
			String currentDate=(dateFormat2.format(c1.getTime())).toString();
			String presentdate=(dateFormat_new.format(c1.getTime())).toString();
			String fromDate=currentDate+" 00:00:01";
			String toDate=currentDate+" 23:59:59";
			//String fromDate="2015-08-02 00:00:01";
			//String toDate="2015-08-02 23:59:59";
			String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=8> SSSST- DAILY MASTER STOCK ENTRY REPORT </th></tr>";
			String s2="";
			String s3="";
			String html="";
			List HODET=HODA.getheadofaccounts();
			mainClasses.productsListing PROL=new mainClasses.productsListing();
			mainClasses.subheadListing SUBL=new mainClasses.subheadListing();
			mainClasses.vendorsListing VENL=new mainClasses.vendorsListing();
			List STOCDET=null;
			String proName="";
			double totAmt=0.00;
			/*if(HODET.size()>0){ 
				for(int i=0;i<HODET.size();i++){
				HOD=(beans.headofaccounts)HODET.get(i);*/
				STOCDET=GODL.getMasterStockEntryReport("", fromDate, toDate);
				s2=s2+"<tr style=background:#BDBDBD;><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>MSE NO</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;> VENDOR NAME</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>PRODUCT NAME</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>QTY</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>AMOUNT</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>NARRATION</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>DEPARTMENT</th></tr>";
				if(STOCDET.size()>0){
					for(int s=0;s<STOCDET.size();s++){
						GODS=(beans.godwanstock)STOCDET.get(s);
						if(PROL.getProductsNameByCat1(GODS.getproductId(), "")!=null && !PROL.getProductsNameByCat1(GODS.getproductId(), "").equals("")){
							/*proName=PROL.getProductsNameByCat1(GODS.getproductId(), "");*///commented by madhav
							proName=PROL.getProductsNameByCat2(GODS.getproductId(),GODS.getdepartment());
							System.out.println("11...proName===="+proName);
						}else{
							/*proName=SUBL.getMsubheadname(GODS.getproductId());*///commted by madhav
							proName=SUBL.getMsubheadnameWithDepartment(GODS.getproductId() ,GODS.getdepartment());
							System.out.println("22...proName===="+proName);
						}
						totAmt=totAmt+Double.parseDouble(GODS.getpurchaseRate());
						s2=s2+"<tr><td style=padding:5px;>"+(s+1)+"</td><td style=padding:5px;>"+GODS.getextra1()+"</td><td style=padding:5px;>"+VENL.getMvendorsAgenciesName(GODS.getvendorId())+"</td><td style=padding:5px;text-align:right;>"+proName+"</td><td style=padding:5px;text-align:right;>"+GODS.getquantity()+"</td><td style=padding:5px;text-align:right;>"+df.format(Double.parseDouble(GODS.getpurchaseRate()))+"</td><td style=padding:5px;text-align:right;>"+GODS.getdescription()+"</td><td style=padding:5px;text-align:right;>"+HODA.getHeadofAccountName(GODS.getdepartment())+"</td></tr>";
					}
				}else{
					s2=s2+"<tr><td style=padding:5px;color:red;font-size:15px; colspan=8 align='center'> Sorry,there is no master stock entry entered today!</td></tr>";
				}
			/*	}
			}*/
			s3=s3+"<tr><th colspan=5 style=padding:5px;text-align:right;color:#8b421b;>GRAND TOTAL AMOUNT</th><th colspan=1>"+df.format(totAmt)+"</th><th colspan=2></th></tr>";
			html=s1+s2+s3+"</table>";
			
			String message="Sai Sansthan Total Amount generated at MSE entries is Rs. "+df.format(totAmt);
			 /*String result1=SMS.SendSMS("8125745281",message.replace(" ", "%20"));
			    String result2=SMS.SendSMS("9985508281",message.replace(" ", "%20"));
			    String result3=SMS.SendSMS("9347237255",message.replace(" ", "%20"));
			    String result4=SMS.SendSMS("9290743663",message.replace(" ", "%20"));
			    String result5=SMS.SendSMS("9880463555",message.replace(" ", "%20"));
			    String result6=SMS.SendSMS("8106668634",message.replace(" ", "%20"));
			    String result7=SMS.SendSMS("9949664297",message.replace(" ", "%20"));
			    String result8=SMS.SendSMS("9989121240",message.replace(" ", "%20"));
			    String result9=SMS.SendSMS("9885225434",message.replace(" ", "%20"));
			    String result10=SMS.SendSMS("9010080426",message.replace(" ", "%20"));
			    String result11=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
			    String result12=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
			    String result13=SMS.SendSMS("8790317244",message.replace(" ", "%20"));
			    String result14=SMS.SendSMS("9703133294",message.replace(" ", "%20"));
			    String result15=SMS.SendSMS("9391072421",message.replace(" ", "%20"));
			    String result16=SMS.SendSMS("9603585958",message.replace(" ", "%20"));
			    String result17=SMS.SendSMS("9392483933",message.replace(" ", "%20"));
			    String result18=SMS.SendSMS("9440865333",message.replace(" ", "%20"));
			    String result19=SMS.SendSMS("9866919444",message.replace(" ", "%20"));
			    String result20=SMS.SendSMS("9700000303",message.replace(" ", "%20"));*/
			//System.out.println(html);
			/*sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"localhost");*/
			
			//newly added on 16/06/2016 by pradeep by keeping disable above sendmail lines
			sendMail.send1("reports@saisansthan.in", "ramidiu@yahoo.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.55.61");
			
			//sendMail.send("reports@saisansthan.in", "madhav@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
		    //sendMail.send("reports@saisansthan.in", "karthik@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "venkataiahb4849@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send1("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.55.61");
			//sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send1("reports@saisansthan.in", "nagesh4444@yahoo.com","","","SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.55.61");
			sendMail.send1("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.55.61");
			sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			//sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "SSSST- DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
		}catch(Exception e){
			System.out.println(e);
		}
		return 1;
		}
}
