package crontab;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import beans.banktransactions2;
import beans.banktransactionsService2;
import mainClasses.banktransactionsListing;

public class insertUnclearedintoBankTransaction2 {
	public static void main(String args[]) throws ParseException
	{
		banktransactions2 BKT = new banktransactions2();
		banktransactionsService2 BKTS = new banktransactionsService2();
		banktransactionsListing banktrans = new banktransactionsListing();
		
		DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd");
		
		 Calendar c = Calendar.getInstance();
		 c.add(Calendar.MONTH, -1);
		 c.set(Calendar.DATE, 1);
		 
		 String firstDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();
		 c.set(Calendar.DATE,     c.getActualMaximum(Calendar.DAY_OF_MONTH));
		 String lastDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();
		 
		 String fromdate=firstDateOfPreviousMonth+" 00:00:00";
		 String todate=lastDateOfPreviousMonth+" 23:59:59";
		 
		 List list1 = banktrans.getBankTransactionBetweenDates(fromdate, todate, "sub");
		 
		 List list2 = banktrans.getBankTransactionBetweenDates(fromdate, todate, "add");
		 
		 if(list1.size()>0){
			 for(int i=0;i<list1.size();i++){
				 BKTS=(beans.banktransactionsService2)list1.get(i);
				 BKTS.insert();
			 }
		 }
		 
		 if(list2.size()>0){
			 for(int i=0;i<list2.size();i++){
				 BKTS=(beans.banktransactionsService2)list2.get(i);
				 BKTS.insert();
			 }
		 }
	}
}
