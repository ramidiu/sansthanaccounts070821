package crontab;

import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class CountersClose {
	 public static void main(String[] args){
		 mainClasses.cashier_reportListing CSH=new mainClasses.cashier_reportListing();
		 beans.cashier_reportService CR_SER=new beans.cashier_reportService();
		 beans.cashier_report CHR_RB=new beans.cashier_report();
		
		 Calendar IndiaCal = Calendar.getInstance();
		 java.util.Date date99 = new java.util.Date();
		 TimeZone tz = TimeZone.getTimeZone("IST");
		 DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd");
		 dateFormat.setTimeZone(tz.getTimeZone("IST"));
		 DateFormat dateFormat_time = new SimpleDateFormat("HH:mm");
		 dateFormat_time.setTimeZone(tz.getTimeZone("IST"));
		 Calendar c1 = Calendar.getInstance(); 
		 String datenow=(dateFormat.format(IndiaCal.getTime())).toString();
		 String timenow=(dateFormat_time.format(IndiaCal.getTime())).toString();

		 List OPEN_COUN=CSH.getOpenedCountersList();
		 if(OPEN_COUN.size()>0){
			 for(int i=0;i<OPEN_COUN.size();i++){
				 CHR_RB=(beans.cashier_report)OPEN_COUN.get(i);
				 
				 CR_SER.setcashier_report_id(CHR_RB.getcashier_report_id());
				 CR_SER.setclosing_balance(CHR_RB.getcounter_balance());
				 CR_SER.setlogout_time(timenow);
				 CR_SER.setcash2(datenow);
				 CR_SER.setcash4(CHR_RB.getlogin_id());
				 CR_SER.setcash5("no");
				 CR_SER.setcash6("Not-deposited");
				 
				 CR_SER.update();
			 }
		 }
	 }
}
