package crontab;

import java.text.ParseException;
import java.util.List;
import java.util.TimeZone;

import beans.sssst_registrations;
import beans.sent_sms_emailService;
import mainClasses.sssst_registrationsListing;
import beans.customerapplication;
import mainClasses.customerapplicationListing;
import beans.customerpurchases;
import mainClasses.customerpurchasesListing;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.lang.Thread;

import sms.CallSmsApi1;

public class AnniversaryWishesToDevotees 
{
	public static void main(String args[]) throws ParseException
	{
		DateFormat  dateFormat5= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		TimeZone tz = TimeZone.getTimeZone("IST");
		dateFormat5.setTimeZone(tz);
		CallSmsApi1 SMS = new CallSmsApi1();
//		sms.CallSmscApi SMS = new sms.CallSmscApi();
		String mailBody="";
		String subject = "Anniversary Greetings";
		String emailTemplate = "<div style='width:650px;margin:0px auto; background:#ff6600;padding:20px 0px;'><div style='width:580px;margin:0px auto;border:3px solid #fab001;padding:10px 10px;border-radius:15px;'><h3 style='text-align:center;color:#fff;font-family:Open Sans, sans-serif ;'>Shri Shirdi Saibaba Sansthan Trust</h3><h4 style='padding-left:18px;color:#fff;font-family:Open Sans, sans-serif ;'>Dear Saibandhu,</h4><p style='letter-spacing:0.5px;text-align:center;color:#fff;font-family:Open Sans, sans-serif;font-size:14px;'>Greetings and blessings from Shri Shirdi Saibaba Sansthan Trust, Dilsukhnagar, Hyderabad.</p><p style='letter-spacing:0.5px;line-height:20px;text-align:center;padding:0px 25px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'> A wedding anniversary is the celebration of love, trust, partnership, tolerance and tenacity.  On this gracious occasion, Shri Shirdi Saibaba Sansthan Trust, Dilsukhnagar, Hyderabad wishes you both a Many More Happy Returns of this memorable day.  Shri Shirdi Saibaba blessings shown on you y(ears) of togetherness, happiness and joy and remain in best of health and wealth, prosperity and success in the years ahead.</p><p style='letter-spacing:0.5px;line-height:20px;text-align:center;padding:0px 25px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>On this memorable day, the Sansthan Trust is cordially invites you both to have the blessings of ShriSaibaba, takeThirtha Prasad and Asheervachanam at Sansthan, Dilsukhnagar, Hyderabad.   Shri Shirdi Saibaba will keep your love alive as always.  Happy Anniversay to a great pair of people who belong together.</p><h5 style='text-align:center;font-size:17px;line-height:36px;color:#fff;font-family:cursive, sans-serif ;'> Vivaha Varshikha subhasamayana &#45; Dampatya Sukha Jeevana </br> Manovancha phalasiddhirasthu</h5><p style='padding:0px 0px 0px 30px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;font-weight:bold;'>- With the blessings of Shri Shirdi Saibaba </p><p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Shri Shirdi Saibaba Sansthan Trust,</p> <p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Dilsukhnagar, Hyderabad. </p><p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>www.saisansthan.in</p> <p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>91-40-24066566</p></div></div> <div style='margin:0px auto;padding-left:20px;'><ul style='list-style:none;'><li style='float:left;color:#ff6600;padding:10px 10px 10px 10px;font-family:Open Sans, sans-serif;letter-spacing:0.5px;font-weight:bold;'>Follow us :</li><li style='padding:10px 10px 10px 10px;'><a href='http://www.facebook.com/dilsuknagarsaibabatemple' target='_blank'><img src='http://www.accounts.saisansthan.in/images/fb.png'/></a></li></ul></div>";
		/*FileInputStream fisTargetFile = null;
		System.out.println("22222");
		try {
			//fisTargetFile=new FileInputStream(new File("E:\\workspace\\Saisansthan_accounts\\WebContent\\templates\\anniv.jsp"));
			fisTargetFile=new FileInputStream(new File("/var/www/accounts/data/www/accounts.saisansthan.in/ROOT/templates/anniv.jsp"));
			emailTemplate=IOUtils.toString(fisTargetFile, "UTF-8");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		model.sendmailservice sendMail1=new model.sendmailservice();
		customerapplicationListing CAPL=new customerapplicationListing();
		customerapplication CAP = new customerapplication();
		customerpurchasesListing SALE_L = new customerpurchasesListing();
		customerpurchases CUSPUR = new customerpurchases();
		
		sssst_registrationsListing SSREGL=new sssst_registrationsListing();
		sssst_registrations SSSST = new sssst_registrations();
		
		List SSSSTLIST = null;
		
		
		DateFormat dateFormat4= new SimpleDateFormat("MM-dd");
		Calendar c = Calendar.getInstance(); 
		
		String currentDate=(dateFormat4.format(c.getTime())).toString();
		
		SSSSTLIST= SSREGL.getSSSSTListForDOBOrAnniv("doa",currentDate,"","withPhone","");
		
		List CAPLIST= CAPL.getListForDOBOrAnnivOrNADLTAReminder("middle_name",currentDate,"","withPhone","");
		String temp = "Greetings n blessings on the occasion of ur Anniversary. Ur pair is cordially invited to Sansthan to receive blessings of Shri Saibaba SSSST, DSNR.";
		if(CAPLIST.size() > 0)
		{
			for(int i=0; i < CAPLIST.size(); i++)
			{
				String mobilenum = "";
				String email = "";
				String name = "";
				
				CAP=(customerapplication)CAPLIST.get(i);
				List CUSPURLIST=SALE_L.getBillDetails(CAP.getbillingId());
				CUSPUR=(customerpurchases)CUSPURLIST.get(0);
				mobilenum = CUSPUR.getphoneno().trim();
				email = CUSPUR.getextra8().trim();
				name = CUSPUR.getcustomername().trim().replace("&", "");
				
				if(CUSPUR.getproductId().equals("20092") || CUSPUR.getproductId().equals("20063"))
				{
					name = CAP.getlast_name().trim().replace("&", "");
				}
				
				
				if(!mobilenum.equals(""))
				{
					String template = "Shri/Smt "+name+", "+temp;
					SMS.sendSms(mobilenum,template.replace(" ", "%20"));
					
					Calendar c1 = Calendar.getInstance(); 
					String date=(dateFormat5.format(c1.getTime())).toString();
					
					sent_sms_emailService SNE_SER=new sent_sms_emailService();
					
					SNE_SER.setRegId(CUSPUR.getbillingId());
					SNE_SER.setName(name);
					SNE_SER.setPurpose("Anniversary");
					SNE_SER.setDaysremainder("");
					SNE_SER.setEmail("");
					SNE_SER.setPhone(mobilenum);
					SNE_SER.setCreatedDate(date);
					
					
					SNE_SER.insert();
					
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(!email.equals(""))
				{
					mailBody="<h4 style='color:#ff6600;'>Shri/Smt "+name+",</h4>"+emailTemplate; 
					try {
						/*sendMail1.sendmail("reports@saisansthan.in", email,subject,mailBody);*/
						sendMail1.sendmailsansthan("info@saisansthan.in", email,subject ,mailBody);
						
						Calendar c1 = Calendar.getInstance(); 
						String date=(dateFormat5.format(c1.getTime())).toString();
						
						sent_sms_emailService SNE_SER=new sent_sms_emailService();
						
						SNE_SER.setRegId(CUSPUR.getbillingId());
						SNE_SER.setName(name);
						SNE_SER.setPurpose("Anniversary");
						SNE_SER.setDaysremainder("");
						SNE_SER.setEmail(email);
						SNE_SER.setPhone("");
						SNE_SER.setCreatedDate(date);
						
						SNE_SER.insert();
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						Thread.sleep(8000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}	
		
		if(SSSSTLIST.size() > 0)
		{
			for(int i=0; i < SSSSTLIST.size(); i++)
			{
				String mobilenum = "";
				String email = "";
				String name = "";
				String spouseName = "";
				
				SSSST=(sssst_registrations)SSSSTLIST.get(i);
				mobilenum = SSSST.getmobile().trim();
				email = SSSST.getemail_id().trim();
				name = SSSST.getfirst_name().trim().replace("&", "")+" "+SSSST.getmiddle_name().trim().replace("&", "");
				spouseName = SSSST.getspouce_name().trim();
				if(!spouseName.equals(""))
				{
					name += " and "+spouseName;
				}
				
				if(!mobilenum.equals(""))
				{
					String template = "Shri/Smt "+name+", "+temp;
					SMS.sendSms(mobilenum,template.replace(" ", "%20"));
					
					Calendar c1 = Calendar.getInstance(); 
					String date=(dateFormat5.format(c1.getTime())).toString();
					
					sent_sms_emailService SNE_SER=new sent_sms_emailService();
					
					SNE_SER.setRegId(SSSST.getsssst_id());
					SNE_SER.setName(name);
					SNE_SER.setPurpose("Anniversary");
					SNE_SER.setDaysremainder("");
					SNE_SER.setEmail("");
					SNE_SER.setPhone(mobilenum);
					SNE_SER.setCreatedDate(date);
					
					SNE_SER.insert();
					
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(!email.equals(""))
				{
					mailBody="<h4 style='color:#ff6600;'>Shri/Smt "+name+",</h4>"+emailTemplate; 
					try {
						/*sendMail1.sendmail("reports@saisansthan.in", email,subject,mailBody);*/
						sendMail1.sendmailsansthan("info@saisansthan.in", email,subject ,mailBody);
						
						Calendar c1 = Calendar.getInstance(); 
						String date=(dateFormat5.format(c1.getTime())).toString();
						
						sent_sms_emailService SNE_SER=new sent_sms_emailService();
						
						SNE_SER.setRegId(SSSST.getsssst_id());
						SNE_SER.setName(name);
						SNE_SER.setPurpose("Anniversary");
						SNE_SER.setDaysremainder("");
						SNE_SER.setEmail(email);
						SNE_SER.setPhone("");
						SNE_SER.setCreatedDate(date);
						
						SNE_SER.insert();
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						Thread.sleep(8000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}	
	}
}
