package crontab;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import mainClasses.godwanstockListing;

import beans.godwanstock_sc;
import beans.godwanstock_scService;

public class SundryCreditorsGodwanStockCJ {
	public static void main(String[] args) throws ParseException, SQLException{
		godwanstock_sc GODS = new godwanstock_sc();
		godwanstock_scService GODSS = new godwanstock_scService();
		godwanstockListing GODList = new godwanstockListing();
		
		DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd");
		
		 Calendar c = Calendar.getInstance();
		 c.add(Calendar.MONTH, -1);
		 c.set(Calendar.DATE, 1);
		 
		 String firstDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();
		 c.set(Calendar.DATE,c.getActualMaximum(Calendar.DAY_OF_MONTH));
		 String lastDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();
		 
		 String fromdate=firstDateOfPreviousMonth+" 00:00:00";
		 String todate=lastDateOfPreviousMonth+" 23:59:59";
		 
		 List GOD_list = GODList.getgodwanstockListBetweenDates(fromdate,todate);
		 
		 if(GOD_list.size()>0){
			 for(int i=0;i<GOD_list.size();i++){
				 GODSS=(beans.godwanstock_scService)GOD_list.get(i);
				 GODSS.insert();
			 }
		 }
	}

}
