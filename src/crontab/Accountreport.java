package crontab;
import java.text.DateFormat;


import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import mainClasses.godwanstockListing;

import beans.godwanstock;
import beans.indent;
import beans.productexpenses;
public class Accountreport {
	public static void main(String args[]) throws ParseException
	{
		process();
	}
	public static int process(){
		try{
		//System.out.println("Enter");
			List APP_Det=null;
		mainClasses.indentListing IND_L=new mainClasses.indentListing();
		mainClasses.indentapprovalsListing APPR_L=new mainClasses.indentapprovalsListing();
		mainClasses.superadminListing SUP_L=new mainClasses.superadminListing();
		beans.indent IND=new beans.indent();
		beans.godwanstock GOD=new beans.godwanstock();
		beans.productexpenses PRDEXP=new beans.productexpenses();
		beans.indentapprovals IAP=new beans.indentapprovals();
		mainClasses.productexpensesListing PRDEXP_L=new mainClasses.productexpensesListing();
		mainClasses.godwanstockListing GDS_L=new godwanstockListing();
		MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
		sms.CallSmscApi SMS=new sms.CallSmscApi();
		DecimalFormat df = new DecimalFormat("0.00");
			beans.headofaccounts HOD=new beans.headofaccounts();
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateFormat1= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  DBFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat time=new SimpleDateFormat("HH:mm");
		TimeZone tz = TimeZone.getTimeZone("IST");
		dateFormat2.setTimeZone(tz.getTimeZone("IST"));
		Calendar c1 = Calendar.getInstance(); 
		String currentDate=(dateFormat2.format(c1.getTime())).toString();
		String fromDate=currentDate+" 00:00:01";
		String toDate=currentDate+" 23:59:59";
		List indentopening=IND_L.getIndentsBasedOnDeptAndStatusAndDate("","pending",fromDate,"Before");
		List indentcurrent=IND_L.getIndentsBasedOnDeptAndStatusAndDate("","pending",fromDate,"Uptonow");
		List billopening=GDS_L.getgodwanstock("pending","",fromDate,"Before");
		List billcurrent=GDS_L.getgodwanstock("pending","",fromDate,"Uptonow");
		List payopening=PRDEXP_L.getExpensesListGroupByInvoice(fromDate,"Before","");
		List paycurrent=PRDEXP_L.getExpensesListGroupByInvoice(fromDate,"Uptonow","");
		List paycompleted=PRDEXP_L.getExpensesListGroupByInvoice(fromDate,"Today","PaymentDone");
		
		String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=7> SHRI SHIRDI SAIBABA SANSTHAN TRUST </th></tr><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=7>Account Report</th></tr><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=7>Opening Balance</th></tr>";
		String s2="<tr><td colspan=5 style=font-size:14px;height:30px;line-height:30px;>No Of Indents Pending</td><td colspan=2>"+indentopening.size()+"</td></tr><tr><td colspan=5 style=font-size:14px;height:30px;line-height:30px;>No Of Bills Pending</td><td colspan=2>"+billopening.size()+"</td></tr><tr><td colspan=5 style=font-size:14px;height:30px;line-height:30px;>No Of  payments Pending</td><td colspan=2>"+payopening.size()+"</td></tr><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=7>Performance</th></tr><tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>Tracking No</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; colspan=2>Indent</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; colspan=2>Bills</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; colspan=2>Payment</th></tr><tr><td>Unique No</td><td>Indent Date</td><td>Aprroved By</td><td>Bill Date</td><td>Aprroved By</td><td>Payment Date</td><td>Aprroved By</td></tr>";
		String s3="";
		String s4="</table>";
		String html="";
		String indentapprove="";
		String billapprove="";
		String payapprove="";
		if(indentcurrent.size()>0){
			for(int i=0;i<indentcurrent.size();i++){
				IND=(indent)indentcurrent.get(i);
				APP_Det=APPR_L.getIndentDetails(IND.getindentinvoice_id());
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);indentapprove=indentapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+IND.getindentinvoice_id()+"</th><td>"+dateFormat1.format(DBFormat.parse(IND.getdate()))+" </td><td>"+indentapprove+"</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>";
				indentapprove="";
			}
			
		}
		if(billcurrent.size()>0){
			for(int i=0;i<billcurrent.size();i++){
				GOD=(godwanstock)billcurrent.get(i);
				APP_Det=APPR_L.getIndentDetails(IND_L.getindentnoBasedonUniqueId(GOD.getExtra9()));
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);indentapprove=indentapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				APP_Det=APPR_L.getIndentDetails(GOD.getextra1());
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);billapprove=billapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				if(GOD.getExtra6().equals("BillApproveNotRequire")){
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+GOD.getExtra9()+"</th><td>No Indent & Purchase Order</td><td>No Indent & Purchase Order</td><td>"+dateFormat2.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>Bill Approve NotRequire</td><td>-</td><td>-</td></tr>";
				} 
				else if(GOD.getExtra6().equals("BillApproveRequire") && GOD.getextra2().equals("")){
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+GOD.getExtra9()+"</th><td>No Indent & Purchase Order </td><td>No PO</td><td>"+dateFormat1.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>Bill Approve NotRequire</td><td>-</td><td>-</td></tr>";
				} else {
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+GOD.getExtra9()+"</th><td>"+dateFormat1.format(DBFormat.parse(IND_L.getEntryDateUniqueId(PRDEXP.getExtra6())))+"</td><td>"+indentapprove+"</td><td>"+dateFormat2.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>"+billapprove+"</td><td>-</td><td>-</td></tr>";
				}
				
				indentapprove="";
				billapprove="";
			}
			
		}

/*		System.out.println(billcurrent.size());*/
		if(paycurrent.size()>0){
			for(int i=0;i<paycurrent.size();i++){
				PRDEXP=(productexpenses)paycurrent.get(i);
				APP_Det=APPR_L.getIndentDetails(IND_L.getindentnoBasedonUniqueId(PRDEXP.getExtra6()));
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);indentapprove=indentapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				APP_Det=APPR_L.getIndentDetails(GDS_L.getGodwannoBasedUniqueno(PRDEXP.getExtra6()));
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);billapprove=billapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				APP_Det=APPR_L.getIndentDetails(PRDEXP.getexpinv_id());
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);payapprove=payapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				if(indentapprove.equals("")){
				
				if(billapprove.equals("")){
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+PRDEXP.getExtra6()+"</th><td>No Indent & Purchase Order</td><td>No Indent & Purchase Order</td><td>"+dateFormat1.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>Bill Approve NotRequire</td><td>"+dateFormat1.format(DBFormat.parse(PRDEXP.getentry_date()))+"</td><td>"+payapprove+"</td></tr>";
				} else {
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+PRDEXP.getExtra6()+"</th><td>No Indent & Purchase Order</td><td>No Indent & Purchase Order</td><td>"+dateFormat1.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>"+billapprove+"</td><td>"+dateFormat1.format(DBFormat.parse(PRDEXP.getentry_date()))+"</td><td>"+payapprove+"</td></tr>";
				}}else {
				
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+PRDEXP.getExtra6()+"</th><td>"+dateFormat1.format(DBFormat.parse(IND_L.getEntryDateUniqueId(PRDEXP.getExtra6())))+"</td><td>"+indentapprove+"</td><td>"+dateFormat1.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>"+billapprove+"</td><td>"+dateFormat1.format(DBFormat.parse(PRDEXP.getentry_date()))+"</td><td>"+payapprove+"</td></tr>";
				}
				indentapprove="";
				billapprove="";
				payapprove="";
			}
			
		}
		if(paycompleted.size()>0){
			for(int i=0;i<paycompleted.size();i++){
				PRDEXP=(productexpenses)paycompleted.get(i);
				APP_Det=APPR_L.getIndentDetails(IND_L.getindentnoBasedonUniqueId(PRDEXP.getExtra6()));
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);indentapprove=indentapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				APP_Det=APPR_L.getIndentDetails(GDS_L.getGodwannoBasedUniqueno(PRDEXP.getExtra6()));
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);billapprove=billapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				APP_Det=APPR_L.getIndentDetails(PRDEXP.getexpinv_id());
				if(APP_Det.size()>0){
					for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);payapprove=payapprove+SUP_L.getSuperadminname(IAP.getadmin_id())+"</br><br>("+dateFormat2.format(DBFormat.parse(IAP.getapproved_date()))+" "+time.format(DBFormat.parse(IAP.getapproved_date()))+ ")</br> ";}}
				if(indentapprove.equals("")){
				
				if(billapprove.equals("")){
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+PRDEXP.getExtra6()+"</th><td>No Indent & Purchase Order</td><td>No Indent & Purchase Order</td><td>"+dateFormat1.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>Bill Approve NotRequire</td><td>"+dateFormat1.format(DBFormat.parse(PRDEXP.getentry_date()))+"</td><td>"+payapprove+"</td></tr>";
				} else {
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+PRDEXP.getExtra6()+"</th><td>No Indent & Purchase Order</td><td>No Indent & Purchase Order</td><td>"+dateFormat1.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>"+billapprove+"</td><td>"+dateFormat1.format(DBFormat.parse(PRDEXP.getentry_date()))+"</td><td>"+payapprove+"</td></tr>";
				}}else {
				
					s3=s3+"<tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+PRDEXP.getExtra6()+"</th><td>"+dateFormat1.format(DBFormat.parse(IND_L.getEntryDateUniqueId(PRDEXP.getExtra6())))+"</td><td>"+indentapprove+"</td><td>"+dateFormat1.format(DBFormat.parse(GOD.getCheckedtime()))+"</td><td>"+billapprove+"</td><td>"+dateFormat1.format(DBFormat.parse(PRDEXP.getentry_date()))+"</td><td>"+payapprove+"</td></tr>";
				}
				indentapprove="";
				billapprove="";
				payapprove="";
			}
			
		}
		html=s1+s2+s3+s4;
/*		System.out.println(html);*/
		/*sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"localhost");*/
		
		//newly added on 16/06/2016 by pradeep by keeping disable above sendmail lines
		sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "nagesh4444@yahoo.com","","","SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sagar@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST  Bills Report", html,"68.169.54.16");
		}catch(Exception e){
			System.out.println(e);
		}
		return 1;
	}
}

	

