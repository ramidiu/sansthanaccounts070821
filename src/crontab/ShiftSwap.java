package crontab;

import java.util.List;

import mainClasses.shift_timingsListing;
import mainClasses.stafftimingsListing;
import beans.stafftimings;
import beans.stafftimingsService;

public class ShiftSwap {
		
	public static void main(String[] args) {
		String[] sTime;
		shift_timingsListing stl = new shift_timingsListing();
		stafftimingsService sts = new stafftimingsService();
		stafftimingsListing sttl = new stafftimingsListing();
		List sList = sttl.getAllStaffTimings();
		if(sList.size()>0){
			for(int i=0;i<sList.size();i++){
				stafftimings SList = (stafftimings)sList.get(i);
				if(SList.getNext_week_shift() != null && !SList.getNext_week_shift().equals("")){
					sTime = stl.getTimings(SList.getNext_week_shift()).split("To");
					sts.setstarttime(sTime[0]);
					sts.setendtime(sTime[1]);
				}else{
					sts.setstarttime(SList.getstarttime());
					sts.setendtime(SList.getendtime());
				}
				sts.setstart_period(SList.getNext_week_shift());
				sts.setNext_week_shift(SList.getstart_period());
				sts.setstaff_id(SList.getstaff_id());
				boolean flag = sts.update();
			}
			
		}

	}

}
