package crontab;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import beans.consumption_entries;

import mainClasses.consumption_entriesListing;
import mainClasses.produceditemsListing;
import mainClasses.stockrequestListing;

public class KotRequestAndProducedItems {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		processDailyMail();
	}
	public static int processDailyMail(){
		beans.produceditems PRDUCD=new beans.produceditems();
		beans.stockrequest STCKREQ=new beans.stockrequest();
		stockrequestListing STCKREQL=new stockrequestListing();
		consumption_entries CNSUMP=new consumption_entries();
		produceditemsListing PRDUCDL=new produceditemsListing();
		consumption_entriesListing CONSL=new consumption_entriesListing();
		
		double totalamount = 0.00;
		double totalquantity = 0.00;
		double grandamount = 0.00;
		List KOTREQLIST=null;
		List PRODUCDKOTREQLIST=null;
		List CNSMPLIST=null;
		List KOTDETLREQLIST=null;
		List PRDCDLIST=null;
		MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
		sms.CallSmscApi SMS=new sms.CallSmscApi();
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  dateFormat_new= new SimpleDateFormat("dd MMM yyyy");
		TimeZone tz = TimeZone.getTimeZone("IST");
		DecimalFormat df = new DecimalFormat("#.##");
		dateFormat2.setTimeZone(tz.getTimeZone("IST"));
		dateFormat_new.setTimeZone(tz.getTimeZone("IST"));
		Calendar c1 = Calendar.getInstance();
		String currentDate=(dateFormat2.format(c1.getTime())).toString();
		String presentdate=(dateFormat_new.format(c1.getTime())).toString();
		//String currentDate="2016-01-16";
		String fromDate=currentDate+" 00:00:01";
		String toDate=currentDate+" 23:59:59";
		String html="";
		String message="";
		KOTREQLIST=STCKREQL.getstockrequestBasedOnHOA("0",fromDate,toDate,"KOT");
		PRODUCDKOTREQLIST=STCKREQL.getProducedKots("0",fromDate,toDate);
		String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=7> SSSST-KOT Request And Produced Products Report </th></tr>";
		String s2="";
		String s4="</table></td></tr></table>";					
		s2="<tr><td colspan=5><table border=1 width=960 cellpadding=0 cellspacing=0  style=margin: auto;><tr><tr><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px width=50px>S.NO.</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px width=130px>Purpose</td><td align=center style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px width=500px>Stock Received</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px; width=130px>End Product</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px width=130px>Distribution</td></tr>";
		if(PRODUCDKOTREQLIST.size()>0){
			for(int i=0;i<PRODUCDKOTREQLIST.size();i++){
			
			STCKREQ=(beans.stockrequest)PRODUCDKOTREQLIST.get(i);
			KOTDETLREQLIST=STCKREQL.getRequestDetailsBasedOnID(STCKREQ.getreqinv_id());
			PRDCDLIST=PRDUCDL.getproduceditems(STCKREQ.getreqinv_id());
			CNSMPLIST=CONSL.getConsumption_entriesBasedKOT(STCKREQ.getreqinv_id());
			s2=s2+"<tr ><td width=50px>"+(i+1)+"</td><td width=130px> KOT NO:"+STCKREQ.getreqinv_id()+"<br>"+STCKREQ.getnarration()+"</td><td><table border=1 cellpadding=0 cellspacing=0 align=center width=500px><tr><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px >Item</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px>Quantity</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px>Units</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px>Price</td></tr>";
			for(int j=0;j<KOTDETLREQLIST.size();j++){
				STCKREQ=(beans.stockrequest)KOTDETLREQLIST.get(j);
			s2=s2+"<tr><td style=font-weight: bold; >"+STCKREQ.getproduct_id()+" "+STCKREQ.getextra2()+"</td><td style=font-weight: bold;>"+STCKREQ.getquantity()+"</td><td style=font-weight: bold;>"+STCKREQ.getextra4()+"</td><td style=font-weight: bold;>"+STCKREQ.getextra3()+"</td></tr>";
			
			}
			s2=s2+"</table></td><td width=130px>";
			for(int j=0;j<PRDCDLIST.size();j++){
						PRDUCD=(beans.produceditems)PRDCDLIST.get(j);
						s2=s2+""+PRDUCD.getproductId()+" :"+PRDUCD.getquantity()+" <br/>";} 
			s2=s2+"</td><td width=130px>";
			for(int k=0;k<CNSMPLIST.size();k++){
							CNSUMP=(beans.consumption_entries)CNSMPLIST.get(k);
							s2=s2+"<span style=font-size: 10px;>"+CNSUMP.getconsumption_category()+"("+CNSUMP.getcon_sub_category()+")</span>  : <span style=font-size: 11px;>"+CNSUMP.getdevotees_qty()+"</span> <span style=font-size: 8px;> Membrs</span>  <br/> ";
							}
			s2=s2+"</td></tr>";
			}}
		if(KOTREQLIST.size()>0){
			for(int i=0;i<KOTREQLIST.size();i++){
				double totalprice = 0.00;
				double quantity = 0.00;
				double gamount = 0;
			STCKREQ=(beans.stockrequest)KOTREQLIST.get(i);
			KOTDETLREQLIST=STCKREQL.getRequestDetailsBasedOnID(STCKREQ.getreqinv_id());
			PRDCDLIST=PRDUCDL.getproduceditems(STCKREQ.getreqinv_id());
			CNSMPLIST=CONSL.getConsumption_entriesBasedKOT(STCKREQ.getreqinv_id());
			s2=s2+"<tr ><td width=50px>"+(i+1)+"</td><td width=130px>"+STCKREQ.getnarration()+"</td><td><table border=1 cellpadding=0 cellspacing=0 align=center width=500px><tr><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px >Item</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px>Quantity</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px>Units</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px>Price</td><td style=background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px>Amount</td></tr>";
			for(int j=0;j<KOTDETLREQLIST.size();j++){
				STCKREQ=(beans.stockrequest)KOTDETLREQLIST.get(j);
				quantity = quantity + ((Double.parseDouble(STCKREQ.getquantity())));
				totalprice = totalprice + ((Double.parseDouble(STCKREQ.getextra3())));
				double amount = Double.parseDouble(STCKREQ.getquantity()) * Double.parseDouble(STCKREQ.getextra3());
				gamount += amount;
			s2=s2+"<tr><td style=font-weight: bold; >"+STCKREQ.getproduct_id()+" "+STCKREQ.getextra2()+"</td><td style=font-weight: bold;>"+STCKREQ.getquantity()+"</td><td style=font-weight: bold;>"+STCKREQ.getextra4()+"</td><td style=font-weight: bold;>"+STCKREQ.getextra3()+"</td><td style=font-weight: bold;>"+amount+"</td></tr>";
			
			}
			s2=s2+"<tr><td style=padding:5px; colspan=1>Total</td><td style=padding:5px; colspan=2> "+df.format(quantity) +" </td><td></td><td style=padding:5px; colspan=1>&#8377; "+gamount+" /-</td></tr>";
			s2=s2+"</table></td><td width=130px>";
			for(int j=0;j<PRDCDLIST.size();j++){
						PRDUCD=(beans.produceditems)PRDCDLIST.get(j);
						s2=s2+""+PRDUCD.getproductId()+" :"+PRDUCD.getquantity()+" <br/>";} 
			s2=s2+"</td><td width=130px>";
			for(int k=0;k<CNSMPLIST.size();k++){
							CNSUMP=(beans.consumption_entries)CNSMPLIST.get(k);
							s2=s2+"<span style=font-size: 10px;>"+CNSUMP.getconsumption_category()+"("+CNSUMP.getcon_sub_category()+")</span>  : <span style=font-size: 11px;>"+CNSUMP.getdevotees_qty()+"</span> <span style=font-size: 8px;> Membrs</span>  <br/> ";
							}
			totalquantity = totalquantity + quantity;
			totalamount = totalamount + totalprice;
			grandamount += gamount;
			
			} message="Sai Sansthan Amount generated at KOT Request And Produced Products is Rs. "+df.format(grandamount)+" and Total Quantity of products is "+df.format(totalquantity);
			s2=s2+"<tr><td style=padding:5px; colspan=3 align=center>TOTAL QUANTITY</td><td style=padding:5px; colspan=2 align=center>"+df.format(totalquantity)+" </td></tr>";
			/*s2=s2+"<tr><td style=padding:5px; colspan=3 align=center>TOTAL AMOUNT</td><td style=padding:5px; colspan=2 align=center>"+df.format(totalamount)+" /-</td></tr>";*/
			s2=s2+"<tr><td style=padding:5px; colspan=3 align=center>GRAND AMOUNT</td><td style=padding:5px; colspan=2 align=center>"+df.format(grandamount)+" /-</td></tr>";
			s2=s2+"</td></tr>";	
		} else{message="Sai Sansthan No,KOT Request Found";
				s2=s2+"<tr><td colspan=5>No,KOT Request Found.</td>";
			}
		html=s1+s2+"</table></td></tr></table>";
		System.out.println("1111111");
		System.out.println("html====>"+html);
		
		 String result1=SMS.SendSMS("8125745281",message.replace(" ", "%20"));
		    String result2=SMS.SendSMS("9985508281",message.replace(" ", "%20"));
		    String result3=SMS.SendSMS("9347237255",message.replace(" ", "%20"));
		    String result4=SMS.SendSMS("9290743663",message.replace(" ", "%20"));
		    String result5=SMS.SendSMS("9880463555",message.replace(" ", "%20"));
		    String result6=SMS.SendSMS("8106668634",message.replace(" ", "%20"));
		    String result7=SMS.SendSMS("9949664297",message.replace(" ", "%20"));
		    String result8=SMS.SendSMS("9989121240",message.replace(" ", "%20"));
		    String result9=SMS.SendSMS("9885225434",message.replace(" ", "%20"));
		    String result10=SMS.SendSMS("9010080426",message.replace(" ", "%20"));
		    String result11=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
		    String result12=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
		    String result13=SMS.SendSMS("8790317244",message.replace(" ", "%20"));
		    String result14=SMS.SendSMS("9703133294",message.replace(" ", "%20"));
		    String result15=SMS.SendSMS("9391072421",message.replace(" ", "%20"));
		    String result16=SMS.SendSMS("9603585958",message.replace(" ", "%20"));
		    String result17=SMS.SendSMS("9392483933",message.replace(" ", "%20"));
		    String result18=SMS.SendSMS("9440865333",message.replace(" ", "%20"));
		    String result19=SMS.SendSMS("9866919444",message.replace(" ", "%20"));
		    String result20=SMS.SendSMS("9700000303",message.replace(" ", "%20"));
		/*sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST- KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"localhost");*/
		
		//newly added on 16/06/2016 by pradeep by keeping disable above sendmail lines
		    
		   sendMail.send1("reports@saisansthan.in", "ramidiu@yahoo.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.55.61");
		    
		 //sendMail.send("reports@saisansthan.in", "sagar@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		 sendMail.send("reports@saisansthan.in", "venkataiahb4849@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "karthik@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send1("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.55.61");
	//	sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
	//	sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send1("reports@saisansthan.in", "nagesh4444@yahoo.com","","","SSSST-  KOT Request And Produced Products Report", html,"68.169.55.61");
		sendMail.send1("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.55.61");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
	//	sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rajuyadav9600@gmail.com ", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "ravikumar66@gmail.com", "","", "SSSST-  KOT Request And Produced Products Report", html,"68.169.54.16");
		
		
		return 1;
	}
}
