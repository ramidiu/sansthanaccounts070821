package crontab;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import mainClasses.superadminListing;

public class TotalPendingBills {
	public static void main(String args[]) throws ParseException
	{
		process();
	}
	public static int process(){
		try{
			mainClasses.headofaccountsListing HODA=new mainClasses.headofaccountsListing();
			mainClasses.bankdetailsListing BNKL=new  mainClasses.bankdetailsListing();
			MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
			beans.headofaccounts HOD=new beans.headofaccounts();
			beans.bankdetails BNK=new beans.bankdetails();
			sms.CallSmscApi SMS=new sms.CallSmscApi();
			DecimalFormat DF=new DecimalFormat("0.00");
			DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateFormat CDF= new SimpleDateFormat("dd-MM-yyyy/HH:mm");
			List HODET=HODA.getheadofaccounts();
			List BKDET=null;
			double totAmt=0.00;
			mainClasses.godwanstockListing GODL=new mainClasses.godwanstockListing();
			mainClasses.productexpensesListing PROEXPL=new mainClasses.productexpensesListing();
			mainClasses.vendorsListing VENDL=new mainClasses.vendorsListing();
			List GODSDET=null;List PEXPDET=null;
			
			String boardMemApprved="";
			String boardMemApprvedId="";
			String drawingAuthMem="";
			mainClasses.indentapprovalsListing APRVL=new mainClasses.indentapprovalsListing();
			beans.productexpenses PDA=new beans.productexpenses();
			beans.godwanstock GOD=new beans.godwanstock();
			String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
			String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
			String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=13> SHRI SHIRDI SAIBABA SANSTHAN TRUST</th></tr>";
			String s2="",s3="",t1="",t2="",t3="";
			String payStat="";
			String html="";
			if(HODET.size()>0){ 
				for(int i=0;i<HODET.size();i++){
					HOD=(beans.headofaccounts)HODET.get(i);
					s2=s2+"<tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=13> STATEMENT SHOWING THE PENDING BILLS AS ON "+todate+" FOR "+HOD.getname()+"</th></tr><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=13> BANK DETAILS </th></tr>";
					BKDET=BNKL.getBanksBasedOnHOA(HOD.gethead_account_id());
					if(BKDET.size()>0){
						for(int b=0;b<BKDET.size();b++){
							BNK=(beans.bankdetails)BKDET.get(b);
							double pettyCash=0.00;
							if(BNK.getextra1()!=null && !BNK.getextra1().equals("")){
								pettyCash=Double.parseDouble(BNK.getextra1());
							}
							//s3=s3+"<tr><th style=font-size:14px;height:30px;line-height:30px; align=right colspan=12> "+BNK.getbank_name() +"</th><th style=font-size:14px;height:30px;line-height:30px;> "+DF.format(Double.parseDouble(BNK.gettotal_amount())) +"</th></tr><tr><th style=font-size:14px;height:30px;line-height:30px; align=right colspan=12> "+BNK.getbank_name() +"--PETTY CASH</th><th style=font-size:14px;height:30px;line-height:30px; >"+DF.format(pettyCash)+"</th></tr>";
						}
					}
					s2=s2+s3;
					s3="";
					superadminListing listing=new superadminListing();
					List<String> listOfSuperAdmin=listing.getSuperAdminIdAndName(HOD.gethead_account_id());
					t1=t1+"<tr style=background:#81F7F3;font-size:12px;><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>MSE-ENTRY DATE</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>UNIQUE-NO/MSE-NO</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>TOTAL AMOUNT</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+listOfSuperAdmin.get(1)+"</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+listOfSuperAdmin.get(3)+"</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>"+listOfSuperAdmin.get(5)+"</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>REMARKS</th></tr>";
					List PNDBills=GODL.getTotalPendingBills(HOD.gethead_account_id(),"", "");
					int sno=0;
					double amt=0.00;
					if(PNDBills.size()>0){
						for(int p=0;p<PNDBills.size();p++){
							GODSDET=GODL.getStockDetailsBasedOnInvoice(PNDBills.get(p).toString());
							if(GODSDET.size()>0){
								GOD=(beans.godwanstock)GODSDET.get(0);
								PEXPDET=PROEXPL.getproductexpensesBasedInvoice(GOD.getextra1());
								if(PEXPDET.size()>0){
									PDA=(beans.productexpenses)PEXPDET.get(0);
								}
							}
							
							boardMemApprved=APRVL.getApprovedMemNames(GOD.getextra1());
							boardMemApprvedId=APRVL.getApprovedMemberId(GOD.getextra1());
							String boardMemApprvedIdArray[]=boardMemApprvedId.split(",");
							
							drawingAuthMem=APRVL.getApprovedMemNames(PDA.getexpinv_id());
							
							if(!boardMemApprved.equals("")){
								boardMemApprved="("+boardMemApprved+")";
							}if(!drawingAuthMem.equals("")){ 
								drawingAuthMem="Drawing authority approved ("+drawingAuthMem+")";
							}if(!PDA.getextra1().equals("Payment Done") && PDA.getextra1()!=null && !PDA.getextra1().equals(" ")){
								payStat="Payments Need to issue";
							}
							if(GOD.getpurchaseRate()!=null && !GOD.getpurchaseRate().equals("")){
								amt=Double.parseDouble(GOD.getpurchaseRate());
							}else{
								amt=0.00;
							}
							if(!PDA.getextra1().equals("Payment Done")){
								sno++;
								totAmt=totAmt+Double.parseDouble(GOD.getpurchaseRate());
								
								String approvedOrnot1,approvedOrnot2,approvedOrnot3="";
								String colorOfApprovedNotApproved="red";
								
								if(boardMemApprvedIdArray.length>2)
								{
									System.out.println("array="+boardMemApprvedIdArray[0]+"list="+listOfSuperAdmin.get(0));
									System.out.println("array="+boardMemApprvedIdArray[1]+"list="+listOfSuperAdmin.get(2));
									System.out.println("array="+boardMemApprvedIdArray[2]+"list="+listOfSuperAdmin.get(4));
									
									if(boardMemApprvedIdArray[0].equals(listOfSuperAdmin.get(0)))
									{
										colorOfApprovedNotApproved="green";
										approvedOrnot1="Approved";
									}else{
										approvedOrnot1="Not Approved";
									}
									if(boardMemApprvedIdArray[1].equals(listOfSuperAdmin.get(2)))
									{
										colorOfApprovedNotApproved="green";
										approvedOrnot2="Approved";
									}else{
										approvedOrnot2="Not Approved";
									}
									
									if(boardMemApprvedIdArray[2].equals(listOfSuperAdmin.get(4)))
									{
										colorOfApprovedNotApproved="green";
										approvedOrnot3="Approved";
									}else{
										approvedOrnot3="Not Approved";
									}
								}else{
									colorOfApprovedNotApproved="green";
									approvedOrnot1=approvedOrnot2=approvedOrnot3="Approved";//"BillApproveNotRequire";
								}
								
								t2=t2+"<tr style=background:#E6E6E6;font-size:12px;><td style=padding:5px;>"+sno+"</td><td style=padding:5px;>"+CDF.format(dateFormat.parse(GOD.getdate()))+"</td><td style=padding:5px;>"+PDA.getExtra6()+"/"+PNDBills.get(p)+"</td><td style=padding:5px;text-align:right;>"+DF.format(amt)+"</td><td style=padding:5px;color:"+colorOfApprovedNotApproved+";> "+approvedOrnot1+"</td><td style=padding:5px;color:"+colorOfApprovedNotApproved+";> "+approvedOrnot2+"</td><td style=padding:5px;color:"+colorOfApprovedNotApproved+";>"+approvedOrnot3+"</td><td style=padding:5px;>"+GOD.getdescription()+"</td></tr>";//<td style=padding:5px; 3333>"+GOD.getApproval_status()+boardMemApprved+"</td>
							}
						}
						//System.out.println("table data###"+t2);
					}
					sno=0;
					t3=t3+"<tr style=background:#E6E6E6;><td style=padding:5px; colspan=3></td><td style=padding:5px; >"+DF.format(totAmt)+"</td><td  colspan=5></td></tr>";
					s2=s2+t1+t2+t3;
					t2="";t3="";t1="";
					totAmt=0.00;
					
					//sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SHRI SHIRDI SAIBABA SANSTHAN TRUST  TOTAL PENDING BILLS STATEMENT OF "+HOD.getname(), html,"localhost");
					
				}
			}
			html=s1+s2+"</table>";
			//System.out.println(html);
			/*sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
			sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");
			sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"localhost");*/
			
			//newly added on 16/06/2016 by pradeep by keeping disable above sendmail lines
			sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sagar@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "nagesh4444@yahoo.com","","","SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "SHRI SHIRDI SAIBABA SANSTHAN TRUST STATEMENT OF TOTAL PENDING BILLS", html,"68.169.54.16");
		
		}catch(Exception e){
			System.out.println(e);
		}
		return 1;
	}
}

