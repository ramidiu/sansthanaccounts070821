package crontab;
import java.text.DateFormat;



import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;


public class DailyReport {
	public static void main(String args[]) throws ParseException
	{
		process();
	}
	public static int process(){
		String html="";
		try{
		//System.out.println("Enter");
		mainClasses.headofaccountsListing HODA=new mainClasses.headofaccountsListing();
		MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
		sms.CallSmscApi SMS=new sms.CallSmscApi();
		DecimalFormat df = new DecimalFormat("#.##");
		double bankDepositTot=0.00;
		double creaditSaleTot=0.00;
		double cashSaleAmt=0.00;
		double totalAmount=0.00;
		double totalPaidAmount=0.00;
		double poojaStoreTotAmt=0.00;
		double sansthanTotAmt=0.00;
		double charityTotAmt=0.00;
		double sainivasTotAmt=0.00;
		String Vocherno[]=null;
		String vochertype="";
		String chequeno="";
		mainClasses.customerpurchasesListing SALE_L = new mainClasses.customerpurchasesListing();
		mainClasses.banktransactionsListing BKTRAL=new mainClasses.banktransactionsListing();
		mainClasses.employeesListing EMPL=new mainClasses.employeesListing();
		beans.customerpurchases SALE=new beans.customerpurchases();
		mainClasses.productsListing PROL=new mainClasses.productsListing();
		mainClasses.subheadListing SUBL=new mainClasses.subheadListing();
		List SAL_list=null;
		Double totDeposit=0.00;
		Double totalSaleAmt=0.00;
		beans.headofaccounts HOD=new beans.headofaccounts();
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  dateFormat_new= new SimpleDateFormat("dd MMM yyyy");
		DateFormat  mySqlDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat  reqDateFormat= new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		TimeZone tz = TimeZone.getTimeZone("IST");
		dateFormat2.setTimeZone(tz.getTimeZone("IST"));
		dateFormat_new.setTimeZone(tz.getTimeZone("IST"));
		Calendar c1 = Calendar.getInstance(); 
		String currentDate=(dateFormat2.format(c1.getTime())).toString();
		String presentdate=(dateFormat_new.format(c1.getTime())).toString();
		String fromDate=currentDate+" 00:00:01";
		String toDate=currentDate+" 23:59:59";
		/*String fromDate="2015-12-18 00:00:01";
		String toDate="2015-12-19 23:59:59";*/
	/*System.out.println(currentDate);*/
		List HODET=HODA.getheadofaccounts();
		String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=9> SSSST-RECEIPTS REPORT </th></tr>";
		String s2="";
		String s3="";
		
		String proName="";
		if(HODET.size()>0){ 
				for(int i=0;i<HODET.size();i++){
				HOD=(beans.headofaccounts)HODET.get(i);
				SAL_list=SALE_L.getTotalSaleReportForMaill(HOD.gethead_account_id(),fromDate,toDate);	
				if(!HOD.getname().equals("SAI NIVAS")){
					s2=s2+"<tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=9>"+HOD.getname()+"</th></tr><tr><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; colspan=6>NAME</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>QTY</th><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>AMOUNT</th></tr>";
				}
				
				
				if(SAL_list.size()>0){
					for(int s=0; s < SAL_list.size(); s++ ){
						SALE=(beans.customerpurchases)SAL_list.get(s); //	'CSP5629962', '2015-12-18 09:39:40', '20091', 63, '1500', 76095, 'RCT586469', '', 'cash', '1', '', '', '', 'RS', 'N.SANJEEV KUMAR & UMARANI', '', 'EMP1000027', 'INUKULA', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
						if(PROL.getProductsNameByCat1(SALE.getproductId(), "")!=null && !PROL.getProductsNameByCat1(SALE.getproductId(), "").equals("")){
							proName=PROL.getProductsNameByCat1(SALE.getproductId(), "");
						}else{
							proName=SUBL.getMsubheadname(SALE.getproductId());
						}
					if(SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate)!=null && !SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate).equals("")){
							totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate));
					}
					if(BKTRAL.getEmployeeDepositAmount("",fromDate)!=null && !BKTRAL.getEmployeeDepositAmount("",fromDate).equals("")){			
						 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount("",fromDate)); 
					}
					totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());			
					s2=s2+"<tr><td style=padding:5px;>"+(s+1)+"</td><td style=padding:5px; colspan=6>"+proName+"</td><td style=padding:5px;>"+SALE.getquantity()+"</td><td style=padding:5px;text-align:right;>"+df.format(Double.parseDouble(SALE.gettotalAmmount()))+"</td></tr>";
			
				}
					//s2=s2+"<tr><th colspan=1 style=padding:5px;text-align:right;color:#8b421b;>Total Cash Sales Amount</th><th colspan=3>"+df.format(cashSaleAmt)+"</th><th colspan=2>"+df.format(bankDepositTot)+"</th>" +"<th></th></tr>";
					//s2=s2+"<tr><th colspan=1 style=padding:5px;text-align:right;color:#8b421b;>Total Credit Sales Amount</th><th colspan=3>"+df.format(creaditSaleTot)+"</th><th colspan=2></th></tr>";
					s2=s2+"<tr><th colspan=8 style=padding:5px;text-align:right;color:#8b421b;>Total Amount</th><th colspan=1 style=text-align:right;>"+df.format(totalAmount)+"</th></tr>";
					if(HOD.getname().equals("POOJA STORES")){ 
						poojaStoreTotAmt=totalAmount;}else if(HOD.getname().equals("SANSTHAN")){
							sansthanTotAmt=totalAmount;
						}else if(HOD.getname().equals("CHARITY")){
							charityTotAmt=totalAmount;
						}else if(HOD.getname().equals("SAI NIVAS")){
							sainivasTotAmt=totalAmount;
						}
					cashSaleAmt=0.00;
					bankDepositTot=0.00;
					totalAmount=0.00;
					creaditSaleTot=0.00;
							}
			}}
		s3=s3+"<tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=9>Grand Total</th></tr>";
		s3=s3+"<tr><th colspan=8 style=padding:5px;text-align:right;color:#8b421b; >POOJA STORE </th><th >"+df.format(poojaStoreTotAmt) +"</th></tr>";
		s3=s3+"<tr><th colspan=8 style=padding:5px;text-align:right;color:#8b421b;>SANSTHAN </th><th>"+df.format(sansthanTotAmt)+"</th></tr>";
		s3=s3+"<tr><th colspan=8 style=padding:5px;text-align:right;color:#8b421b;>CHARITY </th><th>"+df.format(charityTotAmt)+"</th></tr>";
		/*s3=s3+"<tr><th colspan=8 style=padding:5px;text-align:right;color:#8b421b;>SAINIVAS</th><th>"+df.format(sainivasTotAmt)+"</th></tr>";*/
		s3=s3+"<tr><th colspan=8 style=padding:5px;text-align:right;color:#8b421b;>Grand Total</th><th>"+df.format(poojaStoreTotAmt+sansthanTotAmt+charityTotAmt)+"</th></tr>";
		
		
		String t2="<tr><td colspan='9' height='30'></td></tr><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=9> SSSST- PAYMENTS REPORT </th></tr>";
		String t3="",t4="";
		mainClasses.paymentsListing PAYL=new mainClasses.paymentsListing();
		mainClasses.productexpensesListing PROEL=new mainClasses.productexpensesListing();
		beans.payments PAYM=new beans.payments();
		beans.productexpenses PROE=new beans.productexpenses();
		mainClasses.vendorsListing VENDL=new mainClasses.vendorsListing();
		mainClasses.bankdetailsListing BNKL=new mainClasses.bankdetailsListing();
		mainClasses.subheadListing SUBHL=new mainClasses.subheadListing();
		mainClasses.majorheadListing MJHL=new mainClasses.majorheadListing();
		List PAYDL=null;
		List PRDEXL=null;
		double poojaStorePaymentTotAmt=0.00;
		double sansthanPaymentTotAmt=0.00;
		double charityPaymentTotAmt=0.00;
		double sainivasPaymentTotAmt=0.00;
		if(HODET.size()>0){ 
			for(int i=0;i<HODET.size();i++){
				HOD=(beans.headofaccounts)HODET.get(i);
				if(!HOD.getname().equals("SAI NIVAS")){
				t3=t3+"<tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=9>"+HOD.getname()+"</th></tr><tr style=background:#81F7F3;><th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th>" +
							"<th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; >PaymentInvoice No</th>" +
							"<th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; >Company</th>" +
							"<th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; >Vocher No</th>" +
							"<th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; >Date/Time</th>" +
							"<th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; >Naration</th>" +
							"<th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b; >VENDOR NAME</th>" +
							"<th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>Total Amount</th>" +
							"<th style=color:#8b421b;font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>Payment Mode(Type)</th>" +
						"</tr>";
				}
				PAYDL=PAYL.getDailyPaymentsReport(HOD.gethead_account_id(),fromDate,toDate);
				if(PAYDL.size()>0){
					for(int p=0;p<PAYDL.size();p++){
						PAYM=(beans.payments)PAYDL.get(p);
						//
						Vocherno=PAYM.getreference().split(",");
						if( Vocherno.length>1){
							if(vochertype.equals("tds") && Vocherno.length>1){
								Vocherno[0]=Vocherno[1];
								chequeno=PAYM.getextra9();
							} else if(vochertype.equals("servicetax")){
									if( Vocherno.length>2 && Vocherno[2]!=null){
								Vocherno[0]=Vocherno[2];
								} else {
									Vocherno[0]=Vocherno[1];
								}
									chequeno=PAYM.getService_cheque_no();
							}
							
						} else{
							Vocherno[0]=PAYM.getreference();
							chequeno=PAYM.getchequeNo();
						}
						//
						
						totalPaidAmount=totalPaidAmount+Double.parseDouble(PAYM.getamount());
						if(HOD.getname().equals("POOJA STORES")){ 
								poojaStorePaymentTotAmt=totalPaidAmount;/*poojaStorePaymentTotAmt+Double.parseDouble(PAYM.getamount())*/
							}else if(HOD.getname().equals("SANSTHAN")){
								sansthanPaymentTotAmt=totalPaidAmount;//sansthanPaymentTotAmt+Double.parseDouble(PAYM.getamount());
							}else if(HOD.getname().equals("CHARITY")){
								charityPaymentTotAmt=totalPaidAmount;//charityPaymentTotAmt+Double.parseDouble(PAYM.getamount());
							}else if(HOD.getname().equals("SAI NIVAS")){
								sainivasPaymentTotAmt=totalPaidAmount;//sainivasPaymentTotAmt+Double.parseDouble(PAYM.getamount());
							}
						t3=t3+"<tr style=background:#E6E6E6;><td style=padding:5px;>"+(p+1)+"</td>" +
									"<td style=padding:5px; >"+PAYM.getpaymentId()+"</td>" +
									"<td style=padding:5px; >"+BNKL.getBankName(PAYM.getbankId())+"</td>" +
									"<td style=padding:5px; >"+Vocherno[0]+"</td>" +
									"<td style=padding:5px; >"+reqDateFormat.format(mySqlDateFormat.parse(PAYM.getdate()))+"</td>" +
									"<td style=padding:5px; >"+PAYM.getnarration()+"</td>" +
									"<td style=padding:5px; >"+VENDL.getMvendorsAgenciesName(PAYM.getvendorId())+"</td>" +
									"<td style=padding:5px;text-align:right;>"+df.format(Double.parseDouble(PAYM.getamount()))+"</td>" +"";
									if(PAYM.getpaymentType()!=null && PAYM.getpaymentType().equals("cheque")){ 
										t3=t3+"<td style=padding:5px; >"+PAYM.getpaymentType()+"("+PAYM.getextra2()+")</td>" +"";
									} else{ 
										t3=t3+"<td style=padding:5px; >"+PAYM.getpaymentType()+"</td>" +"";
									} 
							t3=t3+"</tr>";
					}
					totalPaidAmount=0.00;
				}else{
					t3=t3+"<tr><td colspan='9' align='center' style='color:red;font-size:20px'>Sorry,there are no payments issued today for this department</td></tr>";
				}
			}
		}
		/*t4=t4+"<tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=9>PAYMENTS GRAND TOTAL</th></tr>";
		t4=t4+"<tr><th colspan=7 style=padding:5px;text-align:right;color:#8b421b;>POOJA STORE 	</th><th colspan=2>"+df.format(poojaStorePaymentTotAmt) +"</th></tr>";
		t4=t4+"<tr><th colspan=7 style=padding:5px;text-align:right;color:#8b421b;>SANSTHAN </th><th colspan=2>"+df.format(sansthanPaymentTotAmt)+"</th></tr>";
		t4=t4+"<tr><th colspan=7 style=padding:5px;text-align:right;color:#8b421b;>CHARITY </th><th colspan=2>"+df.format(charityPaymentTotAmt)+"</th></tr>";
		t4=t4+"<tr><th colspan=7 style=padding:5px;text-align:right;color:#8b421b;>SAINIVAS</th><th colspan=2>"+df.format(sainivasPaymentTotAmt)+"</th></tr>";
		t4=t4+"<tr><th colspan=7 style=padding:5px;text-align:right;color:#8b421b;>GRAND TOTAL AMOUNT</th><th colspan=2>"+df.format(poojaStorePaymentTotAmt+sansthanPaymentTotAmt+charityPaymentTotAmt+sainivasPaymentTotAmt)+"</th></tr>";*/
		html=s1+s2+s3+t2+t3+t4+"</table>";
		html=html+"<TABLE width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;>" +
				"<TR>" +
				"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>SANSTHAN</th>" +
				"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>CHARITY</th>" +
				"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>POOJA STORES</th>" +
				/*"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>SAI NIVAS</th>" +*/
				"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;> GRAND TOTAL</th>"+
				"</TR>" +
				"" +
				"<TR>" +
				"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+df.format(sansthanPaymentTotAmt)+"</td>" +
				"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+df.format(charityPaymentTotAmt)+"</td>" +
				"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+df.format(poojaStorePaymentTotAmt) +"</td>" +
/*				"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+df.format(sainivasPaymentTotAmt)+"</td>" +*/
				"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+df.format(poojaStorePaymentTotAmt+sansthanPaymentTotAmt+charityPaymentTotAmt)+"</td>"+
				"</TR><TABLE>";				

		System.out.println(html);
		String message="Sai Sansthan Amount generated at SANSTHAN is Rs. "+df.format(sansthanTotAmt)+" and CHARITY is Rs. "+df.format(charityTotAmt);
		 String result1=SMS.SendSMS("8125745281",message.replace(" ", "%20"));
		    String result2=SMS.SendSMS("9985508281",message.replace(" ", "%20"));
		    String result3=SMS.SendSMS("9347237255",message.replace(" ", "%20"));
		    String result4=SMS.SendSMS("9290743663",message.replace(" ", "%20"));
		    String result5=SMS.SendSMS("9880463555",message.replace(" ", "%20"));
		    String result6=SMS.SendSMS("8106668634",message.replace(" ", "%20"));
		    String result7=SMS.SendSMS("9949664297",message.replace(" ", "%20"));
		    String result8=SMS.SendSMS("9989121240",message.replace(" ", "%20"));
		    String result9=SMS.SendSMS("9885225434",message.replace(" ", "%20"));
		    String result10=SMS.SendSMS("9010080426",message.replace(" ", "%20"));
		    String result11=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
		    String result12=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
		    String result13=SMS.SendSMS("8790317244",message.replace(" ", "%20"));
		    String result14=SMS.SendSMS("9703133294",message.replace(" ", "%20"));
		    String result15=SMS.SendSMS("9391072421",message.replace(" ", "%20"));
		    String result16=SMS.SendSMS("9603585958",message.replace(" ", "%20"));
		    String result17=SMS.SendSMS("9392483933",message.replace(" ", "%20"));
		    String result18=SMS.SendSMS("9440865333",message.replace(" ", "%20"));
		    String result19=SMS.SendSMS("9866919444",message.replace(" ", "%20"));
		    String result20=SMS.SendSMS("9700000303",message.replace(" ", "%20"));
		
		
		/*SMS.SendSMS("9700000303",message);
		SMS.SendSMS("9866919444",message);*/
		//SMS.SendSMS("9533756241",message);
	    //SMS.SendSMS("9014787969",message);
		/*sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST-  DAILY RECEIPTS AND PAYMENTS REPORT", html,"localhost");*/
		
		//newly added on 16/06/2016 by pradeep by keeping disable above sendmail lines
		
		
		    
		
		   sendMail.send1("reports@saisansthan.in", "ramidiu@yahoo.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.55.61");
		    
		    
		sendMail.send("reports@saisansthan.in", "venkataiahb4849@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "karthik@kreativwebsolutions.co.uk", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send1("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.55.61");
		//sendMail.send("reports@saisansthan.in", "sagar@kreativwebsolutions.co.uk", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send1("reports@saisansthan.in", "nagesh4444@yahoo.com","","","SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.55.61");
		sendMail.send1("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.55.61");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
	//	sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rajuyadav9600@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "ravikumar66@gmail.com", "","", "SSSST- DAILY RECEIPTS AND PAYMENTS REPORT", html,"68.169.54.16");
		
		}catch(Exception e){
			System.out.println(e);
		}
		return 1;
	}
}

	

