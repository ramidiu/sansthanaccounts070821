package crontab;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class MedicalDailyReport {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		processDailyMail();
	}
	public static int processDailyMail(){
		
		MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
		sms.CallSmscApi SMS=new sms.CallSmscApi();
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  dateFormat_new= new SimpleDateFormat("dd MMM yyyy");
		mainClasses.doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
		mainClasses.appointmentsListing APPL=new mainClasses.appointmentsListing();
		mainClasses.medicineissuesListing MED_L=new mainClasses.medicineissuesListing();
		beans.medicineissues MEDI=new beans.medicineissues();
		TimeZone tz = TimeZone.getTimeZone("IST");
		DecimalFormat df = new DecimalFormat("#.##");
		DecimalFormat unitsformat = new DecimalFormat("#.##");
		dateFormat2.setTimeZone(tz.getTimeZone("IST"));
		dateFormat_new.setTimeZone(tz.getTimeZone("IST"));
		Calendar c1 = Calendar.getInstance(); 
		String currentDate=(dateFormat2.format(c1.getTime())).toString();
		String presentdate=(dateFormat_new.format(c1.getTime())).toString();
		String fromDate=currentDate+" 00:00:01";
		String toDate=currentDate+" 23:59:59";
		double totalprice,doctrwiseprice=0.0;
		String message="";
		/*	String fromDate="2015-05-02 00:00:01";
		String toDate="2015-05-02 23:59:59";*/
		int totPatients=0;
		doctrwiseprice=totalprice=0.0;
		beans.appointments APP=new beans.appointments();
		List Medicineissue=null;
		List MedicineissueList=null;
		List DOCT_List=APPL.getappointmentsBasedOnDoctor(fromDate,toDate);
		String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=3> SSSST- DAILY MEDICAL REPORT </th></tr>";
		String s2="";
		String html="";
		int totalMedIssued=0;
		if(DOCT_List.size()>0){
			s2=s2+"<tr style=background:#8b421b;><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>DOCTOR NAME</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;> TOTAL PATIENTS VISITED </th></tr>";
		for(int i=0; i < DOCT_List.size(); i++ ){
			APP=(beans.appointments)DOCT_List.get(i);
			totPatients=totPatients+Integer.parseInt(APP.getappointmnetId());
		
			s2=s2+"<tr style='background:#81f7f3;font-size:12px'><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+(i+1)+"</td><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+DOCT.getDoctorName(APP.getdoctorId())+"</td><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+APP.getappointmnetId()+"</td></tr>";
			MedicineissueList=MED_L.getDailymedicineissuesBasedonDotor(fromDate,toDate,APP.getdoctorId());
			if(MedicineissueList.size()>0){
				doctrwiseprice=0.0;
				s2=s2+"<tr><td colspan='3'><table align='center' border=1 width=100%> <tr style='background:#81f7f3;font-size:12px'><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Medicine Name</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Quantity</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Unit Price</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Total Price</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Medical Hall Quantity</th>";
			for(int k=0; k < MedicineissueList.size(); k++ ){
				MEDI=(beans.medicineissues)MedicineissueList.get(k);
				doctrwiseprice=doctrwiseprice+Double.parseDouble(MEDI.getextra3())*Double.parseDouble(MEDI.getquantity());
				s2=s2+"<tr><td style=padding:5px;>"+MEDI.getextra2()+"</td><td style=padding:5px;>"+MEDI.getquantity() +"</td><td style=padding:5px;>"+MEDI.getextra3() +"</td><td style=padding:5px;>&#8377; "+df.format(Double.parseDouble(MEDI.getextra3())*Double.parseDouble(MEDI.getquantity())) +" /-</td><td style=padding:5px;>"+unitsformat.format(Double.parseDouble(MEDI.getextra4())) +" Units</td></tr>";
			}
			totalprice=doctrwiseprice+totalprice;
			s2=s2+"<tr><td style=padding:5px; colspan=3>Medicine Price</td><td style=padding:5px; colspan=1>&#8377; "+df.format(doctrwiseprice) +" /-</td></tr>";
			s2=s2+"</table></td></tr>";} else{
				s2=s2+"<tr><td style='color:#8b421b;font-size:14px;font-weight:bold;border-top:1px;text-align: center;' colspan=3>No,medicine issued</td></tr>";
				
			}
		
		}
		Medicineissue=MED_L.getDailymedicineissues(fromDate,toDate);
		if(Medicineissue.size()>0){
			for(int s=0;s<Medicineissue.size();s++){
				MEDI=(beans.medicineissues)Medicineissue.get(s);
				totalMedIssued=totalMedIssued+Integer.parseInt(MEDI.getquantity());
			}
		}
		message="Sai Sansthan Total Cost of Medicines Issued is Rs. "+df.format(totalprice)+" and Total Quantity of Medicines issued is "+totalMedIssued+" and Total No of Patients visited is"+totPatients;
		s2=s2+"<tr><td style=padding:5px; colspan=2>TOTAL NO OF PATIENTS VISITED</td><td style=padding:5px;>"+totPatients+" PATIENTS </td></tr>";
		s2=s2+"<tr><td style=padding:5px; colspan=2>TOTAL QTY OF MEDICINES ISSUED</td><td style=padding:5px;>"+totalMedIssued+" Units</td></tr>";
		s2=s2+"<tr><td style=padding:5px; colspan=2>TOTAL COST OF MEDICINES ISSUED</td><td style=padding:5px;>&#8377;  "+df.format(totalprice)+" /-</td></tr>";
		}else{
			s2=s2+"<tr><td style=padding:5px;color:red;font-size:20px; align='center' colspan=3>Sorry,there is no data entered for today</td></tr>";
		}
		
		html=s1+s2+"</table>"+"<br>";
		
		String diagnosticReport = DiagnosticDailyReport.processDailyMail();
		html = html +diagnosticReport+ "<br>";
		
		 String result1=SMS.SendSMS("8125745281",message.replace(" ", "%20"));
		   String result2=SMS.SendSMS("9985508281",message.replace(" ", "%20"));
		    String result3=SMS.SendSMS("9347237255",message.replace(" ", "%20"));
		    String result4=SMS.SendSMS("9290743663",message.replace(" ", "%20"));
		    String result5=SMS.SendSMS("9880463555",message.replace(" ", "%20"));
		    String result6=SMS.SendSMS("8106668634",message.replace(" ", "%20"));
		    String result7=SMS.SendSMS("9949664297",message.replace(" ", "%20"));
		    String result8=SMS.SendSMS("9989121240",message.replace(" ", "%20"));
		    String result9=SMS.SendSMS("9885225434",message.replace(" ", "%20"));
		    String result10=SMS.SendSMS("9010080426",message.replace(" ", "%20"));
		    String result11=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
		    String result12=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
		    String result13=SMS.SendSMS("8790317244",message.replace(" ", "%20"));
		    String result14=SMS.SendSMS("9703133294",message.replace(" ", "%20"));
		    String result15=SMS.SendSMS("9391072421",message.replace(" ", "%20"));
		    String result16=SMS.SendSMS("9603585958",message.replace(" ", "%20"));
		    String result17=SMS.SendSMS("9392483933",message.replace(" ", "%20"));
		    String result18=SMS.SendSMS("9440865333",message.replace(" ", "%20"));
		    String result19=SMS.SendSMS("9866919444",message.replace(" ", "%20"));
		    String result20=SMS.SendSMS("9700000303",message.replace(" ", "%20"));
		
		
	/*	System.out.println(html);*/
		/*sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST-  DAILY MEDICAL & DIAGONOSTIC REPORT", html,"localhost");*/
		
		//newly added on 16/06/2016 by pradeep by keeping disable above sendmail lines
		    
		   sendMail.send1("reports@saisansthan.in", "ramidiu@yahoo.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.55.61"); 
		 
		    
		// sendMail.send("reports@saisansthan.in", "sagar@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "venkataiahb4849@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "karthik@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send1("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.55.61");
		//sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send1("reports@saisansthan.in", "nagesh4444@yahoo.com","","","SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.55.61");
		sendMail.send1("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.55.61");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		//sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "ravikumar66@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rajuyadav9600@gmail.com", "","", "SSSST- DAILY MEDICAL & DIAGONOSTIC REPORT", html,"68.169.54.16");
		return 1;
	}
}
