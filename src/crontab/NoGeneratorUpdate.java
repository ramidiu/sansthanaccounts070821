package crontab;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import dbase.sqlcon.ConnectionHelper;
import beans.no_genarator;
import mainClasses.no_genaratorListing;

public class NoGeneratorUpdate {
	
	public static void main(String[] args) {
		
		// modified by gowri shankar on 20-mar-2019
      // new code
		Connection con = null;
		Statement st = null;
		String query1 = "SELECT table_name,table_id FROM no_genarator";
		no_genaratorListing genaratorListing = new no_genaratorListing();
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("IST"));
		int year = cal.get(Calendar.YEAR);
		System.out.println("year =====> "+year);
		String id = String.valueOf(year).substring(3)+""+String.valueOf(year+1).substring(3);
		System.out.println("id =====> "+id);
		List<no_genarator> noLst = genaratorListing.getNoGeneratorList1(query1);
		int tables[] = null;
		try	{
			con = ConnectionHelper.getConnection(); 
			con.setAutoCommit(false);
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query1);
			while (rs.next())	{
				System.out.println("Data-->"+rs.getString("table_name")+" "+rs.getString("table_id"));
			}
			
			for (int i = 0 ; i < noLst.size() ; i++)	{
				no_genarator noGen = noLst.get(i);
				String tableId = ""+id;	
				int length = String.valueOf(noGen.gettable_id()).length();
				for(int j = 0 ; j < length-2; j++){
					tableId = tableId + "0";
				}
				st.addBatch("update no_genarator set table_id = '"+tableId+"' where table_name = '"+noGen.gettable_name()+"'");
			}
			tables = st.executeBatch();
			System.out.println("tables.length--"+tables.length);
			System.out.println("tables--"+Arrays.toString(tables));
			if (tables.length > 0)	{
				for (int i = 0 ; i < tables.length ; i++)	{
					if (tables[i] == 0)	{
						con.rollback();
					}
				}
				con.commit();
			}
		}
		catch(SQLException se)	{
			se.printStackTrace();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		finally	{
			try {
				if (con != null)
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(st != null)
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
	
	// Old Code
		
/*		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("IST"));
		
		int year = cal.get(Calendar.YEAR);
		
		System.out.println("year =====> "+year);
		
		
		String id = String.valueOf(year).substring(3)+""+String.valueOf(year+1).substring(3);
		
		System.out.println("id =====> "+id);
		
		
		String tables[] = {"patientsentry","appointments","medicalpurchases","medicineissues","payments","invoice","banktransactions",
				"godwanstock","shopstock","customerpurchases","shopsaleinvoice","indent","indentinvoicenumber","indentapprovals",
				"POInvoice","purchaseorder","cashier_report","counter_balance_update","cash_report","customerapplication","produceditems",
				"stockTransferInvoice","productexpenses","expenseinvoice","stockRequest","stockRequestInvoice","offerKindsInvoice","poojaSaaman",
				"paymentinvoice","billimages","vouchernumber","charityvoucher","poojastorevoucher","sansthandevvoucher","sansthannondevvoucher",
				"sainivasvoucher","journal_entries","journal_voucher_no","poojastoreTracking","sansthanTracking","charityTracking","sainivasTracking",
				"return_products","returnProductInv","sainivas_indent_invNum","sainivas_mse_invId","locations","inventory_products_locations","KOT",
				"consumption_entries","consumption_entries_invoice","gatepass_entries","gatepass_entries_inv","employee_details","employee_salary","pro_sansthan",
				"pro_charity","pro_sainivas","pro_poojastores","charityvoucherp","poojastorevoucherp","sansthandevvoucherp","sansthannondevvoucherp",
				"sainivasvoucherp","ewf_entries","ewfTracking","ewfvoucher","ewfvoucherp","sent_sms_email","bankbalance",
				"daily_bill_status","advance","dues"};
		
		
		
		System.out.println("tables =====> "+tables.length);
		String subQuery = "select * from no_genarator where ";
		
		for(int i = 0 ; i < tables.length; i++){
			
			subQuery = subQuery + " table_name = '"+tables[i]+"' or";
			
		}
		
		subQuery = subQuery.substring(0, subQuery.length() - 2);
		
		System.out.println("subQuery ====> "+subQuery);
		
		no_genaratorListing genaratorListing = new no_genaratorListing();
		
		List<no_genarator> no_genarators = genaratorListing.getNoGeneratorList(subQuery);
		
		if(no_genarators != null && no_genarators.size() > 0){
			
			for(int i = 0 ; i < no_genarators.size(); i++){
				
				no_genarator genarator = no_genarators.get(i);
			String tableId = ""+id;	
				int length = String.valueOf(genarator.gettable_id()).length();
				
				for(int j = 0 ; j < length-2; j++){
					tableId = tableId + "0";
				}
				
				String query = "update no_genarator set table_id = '"+tableId+"' where table_name = '"+genarator.gettable_name()+"'"; 
				System.out.println("query ====> "+query);
//				genaratorListing.updateNoGenerator(query);
				
			}
			
			
		}*/
	
	} // main
} //class
