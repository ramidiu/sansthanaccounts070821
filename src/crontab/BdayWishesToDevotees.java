package crontab;

import java.text.ParseException;
import java.util.List;
import java.io.IOException;
import java.text.DateFormat;
import beans.sent_sms_emailService;
import java.util.Calendar;
import beans.sssst_registrations;
import mainClasses.sssst_registrationsListing;
import beans.customerpurchases;
import mainClasses.customerpurchasesListing;
import beans.customerapplication;
import mainClasses.WtspNotification;
import mainClasses.customerapplicationListing;
import model.sendmailservice;
import sms.CallSmscApi;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

public class BdayWishesToDevotees
{
    public static void main(final String[] args) throws ParseException, IOException {
    
    	final DateFormat dateFormat5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final TimeZone tz = TimeZone.getTimeZone("IST");
        dateFormat5.setTimeZone(tz);
        final CallSmscApi SMS = new CallSmscApi();
        String mailBody = "";
        final String subject = "Birthday Greetings";
        final String emailTemplate = "<div style='width:650px;margin:0px auto; background:#ff6600;padding:20px 0px;'><div style='width:580px;margin:0px auto;border:5px solid #fab001;padding:10px 10px;border-radius:15px;'><h3 style='text-align:center;color:#fff;font-family:Open Sans, sans-serif ;'>Shri Shirdi Saibaba Sansthan Trust</h3><h4 style='padding-left:18px;color:#fff;font-family:Open Sans, sans-serif ;'>Dear Saibandhu,</h4><p style='letter-spacing:0.5px;text-align:center;color:#fff;font-family:Open Sans, sans-serif;font-size:14px;'>Greetings and blessings from Shri Shirdi Saibaba Sansthan Trust, Dilsukhnagar,Hyderabad.</p><p style='letter-spacing:0.5px;line-height:20px;text-align:center;padding:0px 25px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Birthday gives us an opportunity to celebrate the gift of life granted to us by the Lord Almighty. On this most memorable day of your life, Shri Shirdi Saibaba Sansthan Trust, Dilsukhnagar, Hyderabad taken an immense pleasure to warmly wish you <span style='font-family:cursive;'>A VERY HAPPY BIRTHDAY AND MANY MANY HAPPY RETURNS OF THIS WONDERFUL DAY</span>.Shri Shirdi Saibaba blessings shown on you y(ears) of joy, happiness for your carrier with good health, wealth, prosperity and success in all your endeavours.</p><p style='letter-spacing:0.5px;line-height:20px;text-align:center;padding:0px 25px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>On this wonderful day, the Sansthan Trust is cordially invites you and your family to have the blessings of ShriSaibaba, take thirtha Prasad and Asheervachanam at Sansthan, Dilsukhnagar, Hyderabad.   We wish you to take Babas hand and get excited for he's got a lot of great things planned ahead for you in the years to come. </p><h5 style='text-align:center;font-size:17px;line-height:36px;color:#fff;font-family:cursive, sans-serif ;'>Shathamanambhavathi shatayuH puruShaHshatendriyaH</br>aayuShyevendriye &#45; pratitiShThati</h5><p style='padding:0px 0px 0px 30px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;font-weight:bold;'>- With the blessings of Shri Shirdi Saibaba </p><p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Shri Shirdi Saibaba Sansthan Trust,</p> <p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Dilsukhnagar, Hyderabad. </p><p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>www.saisansthan.in</p> <p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>91-40-24066566</p></div></div> <div style='margin:0px auto;padding-left:20px;'><ul style='list-style:none;'><li style='float:left;color:#ff6600;padding:10px 10px 10px 10px;font-family:Open Sans, sans-serif;letter-spacing:0.5px;font-weight:bold;'>Follow us :</li><li style='padding:10px 10px 10px 10px;'><a href='http://www.facebook.com/dilsuknagarsaibabatemple' target='_blank'><img src='http://www.accounts.saisansthan.in/images/fb.png'/></a></li></ul></div>";
        final sendmailservice sendMail1 = new sendmailservice();
        final customerapplicationListing CAPL = new customerapplicationListing();
        customerapplication CAP = new customerapplication();
        final customerpurchasesListing SALE_L = new customerpurchasesListing();
         WtspNotification wtsp=new WtspNotification();
        customerpurchases CUSPUR = new customerpurchases();
        final sssst_registrationsListing SSREGL = new sssst_registrationsListing();
        sssst_registrations SSSST = new sssst_registrations();
        List SSSSTLIST = null;
        final DateFormat dateFormat6 = new SimpleDateFormat("MM-dd");
        final Calendar c = Calendar.getInstance();
        final String currentDate = dateFormat6.format(c.getTime()).toString();
        SSSSTLIST = SSREGL.getSSSSTListForDOBOrAnniv("dob", "____-" + currentDate, "like", "withPhone", "");
        final List CAPLIST = CAPL.getListForDOBOrAnnivOrNADLTAReminder("dateof_birth", "____-" + currentDate, "like", "withPhone", "");
        System.out.println("customer purchase list size.."+CAPLIST.size()+"currentdate.. "+currentDate);
        final String temp = "The Sansthan Trust warmly wish U A very Happy Birthday n Many More Happy Returns of the Day , With Shri Saibaba blessings SSSST, DSNR.";
        if (CAPLIST.size() > 0) {
            for (int i = 0; i < CAPLIST.size(); ++i) {
                String mobilenum = "";
                String email = "";
                String name = "";
                CAP = (customerapplication)CAPLIST.get(i);
                final List CUSPURLIST = SALE_L.getBillDetails(CAP.getbillingId());
                CUSPUR = (customerpurchases) CUSPURLIST.get(0);
                mobilenum = CUSPUR.getphoneno().trim();
                email = CUSPUR.getextra8().trim();
                name = CUSPUR.getcustomername().trim().replace("&", "");
                if (CUSPUR.getproductId().equals("20092") || CUSPUR.getproductId().equals("20063")) {
                    name = CAP.getlast_name().trim().replace("&", "");
                }
                if (!mobilenum.equals("")) {
                	if(!name.equals("")) {
                	wtsp.birthdayWishes(name, "birthday", mobilenum);
                	}
                    //added for whatsup birthday notifications 27-10-2021 
                	final String template = "Shri/Smt " + name + ", " + temp;
                    SMS.SendSMS(mobilenum, template.replace(" ", "%20"));
                    final Calendar c2 = Calendar.getInstance();
                    final String date = dateFormat5.format(c2.getTime()).toString();
                    final sent_sms_emailService SNE_SER = new sent_sms_emailService();
                    SNE_SER.setRegId(CUSPUR.getbillingId());
                    SNE_SER.setName(name);
                    SNE_SER.setPurpose("Birthday");
                    SNE_SER.setDaysremainder("");
                    SNE_SER.setEmail("");
                    SNE_SER.setPhone(mobilenum);
                    SNE_SER.setCreatedDate(date);
                    SNE_SER.insert();
                    try {
                        Thread.sleep(2000L);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!email.equals("")) {
                    mailBody = "<h4 style='color:#ff6600;'>Hello " + name + ",</h4>" + emailTemplate;
                    try {
                        sendMail1.sendmailsansthan("info@saisansthan.in", email, subject, mailBody);
                        final Calendar c3 = Calendar.getInstance();
                        final String date2 = dateFormat5.format(c3.getTime()).toString();
                        final sent_sms_emailService SNE_SER2 = new sent_sms_emailService();
                        SNE_SER2.setRegId(CUSPUR.getbillingId());
                        SNE_SER2.setName(name);
                        SNE_SER2.setPurpose("Birthday");
                        SNE_SER2.setDaysremainder("");
                        SNE_SER2.setEmail(email);
                        SNE_SER2.setPhone("");
                        SNE_SER2.setCreatedDate(date2);
                        SNE_SER2.insert();
                    }
                    catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        Thread.sleep(8000L);
                    }
                    catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
            }
        }
        if (SSSSTLIST.size() > 0) {
            for (int i = 0; i < SSSSTLIST.size(); ++i) {
                String mobilenum = "";
                String email = "";
                String name = "";
                SSSST = (sssst_registrations)SSSSTLIST.get(i);
                mobilenum = SSSST.getmobile().trim();
                email = SSSST.getemail_id().trim();
                name = String.valueOf(SSSST.getfirst_name().trim().replace("&", "")) + " " + SSSST.getmiddle_name().trim().replace("&", "");
                if (!mobilenum.equals("")) {
                	if(!name.equals("")) {
                    	wtsp.birthdayWishes(name, "birthday", mobilenum);
                    }
                	final String template2 = "Shri/Smt " + name + ", " + temp;
                    SMS.SendSMS(mobilenum, template2.replace(" ", "%20"));
                    final Calendar c3 = Calendar.getInstance();
                    final String date2 = dateFormat5.format(c3.getTime()).toString();
                    final sent_sms_emailService SNE_SER2 = new sent_sms_emailService();
                    SNE_SER2.setRegId(SSSST.getsssst_id());
                    SNE_SER2.setName(name);
                    SNE_SER2.setPurpose("Birthday");
                    SNE_SER2.setDaysremainder("");
                    SNE_SER2.setEmail("");
                    SNE_SER2.setPhone(mobilenum);
                    SNE_SER2.setCreatedDate(date2);
                    SNE_SER2.insert();
                    try {
                        Thread.sleep(2000L);
                    }
                    catch (InterruptedException e4) {
                        e4.printStackTrace();
                    }
                }
                if (!email.equals("")) {
                    mailBody = "<h4 style='color:#ff6600;'>Hello " + name + ",</h4>" + emailTemplate;
                    try {
                        sendMail1.sendmailsansthan("info@saisansthan.in", email, subject, mailBody);
                        final Calendar c4 = Calendar.getInstance();
                        final String date3 = dateFormat5.format(c4.getTime()).toString();
                        final sent_sms_emailService SNE_SER3 = new sent_sms_emailService();
                        SNE_SER3.setRegId(SSSST.getsssst_id());
                        SNE_SER3.setName(name);
                        SNE_SER3.setPurpose("Birthday");
                        SNE_SER3.setDaysremainder("");
                        SNE_SER3.setEmail(email);
                        SNE_SER3.setPhone("");
                        SNE_SER3.setCreatedDate(date3);
                        SNE_SER3.insert();
                    }
                    catch (Exception e5) {
                        e5.printStackTrace();
                    }
                    try {
                        Thread.sleep(8000L);
                    }
                    catch (InterruptedException e6) {
                        e6.printStackTrace();
                    }
                }
            }
        }
    }
}