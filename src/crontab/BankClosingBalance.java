package crontab;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import beans.bankdetails;

import mainClasses.bankdetailsListing;
import mainClasses.cashier_reportListing;
import mainClasses.customerpurchasesListing;
import mainClasses.headofaccountsListing;

public class BankClosingBalance {

	static String mailhtml="";
	static String rowhtml="";
	public static void main(String args[]) throws ParseException
	{
		process();
	}
	public static int process(){
		try{
			
			DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
			TimeZone tz = TimeZone.getTimeZone("IST");
			dateFormat2.setTimeZone(tz.getTimeZone("IST"));
			Calendar c1 = Calendar.getInstance(); 
			String currentDate=(dateFormat2.format(c1.getTime())).toString();
			mailhtml="";
			rowhtml="";
			
			cashier_reportListing CSHR_L=new  cashier_reportListing();
			customerpurchasesListing CUSTP_L=new customerpurchasesListing();
			mainClasses.headofaccountsListing HODA=new mainClasses.headofaccountsListing();
			
			MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
			beans.headofaccounts HOA=new beans.headofaccounts();
			DecimalFormat NumbToDecml=new DecimalFormat("0.0#");
			String sansthanHOA[]={"NON-DEVELOPMENT","DEVELOPMENT","1","3","5"};
			/*String BankNames[]={"Andhra Bank","Andhra Bank Shirdi","State Bank Of Travencore","PNB"};*/
			String BankNames[]={"Andhra Bank","Andhra Bank Shirdi","State Bank Of Travencore"};
			double NONDEVSNSTHNCSH,NONDEVSNSTHNCSHGateway,DEVSNSTHNCSH,SINVASCSH,PJSTRCSH,CHRTYCSH,TNONDEVSNSTHNCSH,TNONDEVSNSTHNCSHGateway,TDEVSNSTHNCSH,TSINVASCSH,TPJSTRCSH,TCHRTYCSH,BNKTOTAL,SaiCounter,PJRCounter,CharityCounter,SansthanCounter,HundiCollectionNDEV,HundiCollectionDEV,THundiCollectionNDEV,THundiCollectionDEV;
			NONDEVSNSTHNCSH=DEVSNSTHNCSH=SINVASCSH=PJSTRCSH=NONDEVSNSTHNCSHGateway=CHRTYCSH=TNONDEVSNSTHNCSH=TNONDEVSNSTHNCSHGateway=TDEVSNSTHNCSH=TSINVASCSH=TPJSTRCSH=TCHRTYCSH=BNKTOTAL=SaiCounter=PJRCounter=CharityCounter=SansthanCounter=HundiCollectionNDEV=HundiCollectionDEV=THundiCollectionNDEV=THundiCollectionDEV=0.0;
			ArrayList HOA_ARRY=new ArrayList();
			beans.bankdetails BNK=new bankdetails();
			headofaccountsListing HOAL=new headofaccountsListing();
			bankdetailsListing BNKDTL=new bankdetailsListing();
			List HOALIST=HOAL.getActiveHOA();
			List BankList=null;
			mailhtml="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=8> SSSST-BANK CLOSING BALANCE </th></tr><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=8> BANK CLOSING BALANCE </th></tr><tr style=background:#F76E75;><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>BANK NAME</th>";
		
					for(int j=0;j<sansthanHOA.length;j++)
					{
						if(j<2){
						mailhtml=mailhtml+"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>"+sansthanHOA[j]+"</th>";
						} else{
							mailhtml=mailhtml+"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>"+HOAL.getHeadofAccountName(sansthanHOA[j]) +"</th>";				
						}
					}
				
			mailhtml=mailhtml+"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>TOTAL</th></tr>";
			for(int j=0;j<BankNames.length;j++)
			{
				NONDEVSNSTHNCSH=BNKDTL.getBanksAmountBasedOnHOA("4",BankNames[j],"NON-DEVELOPMENT");
				NONDEVSNSTHNCSHGateway=BNKDTL.getBanksAmountBasedOnHOA("4",BankNames[j],"NON-DEVELOPMENTGATEWAY");
				if(BankNames[j].equalsIgnoreCase("Andhra Bank")){
					double temp=0.0;
					temp=NONDEVSNSTHNCSH;
					NONDEVSNSTHNCSH=NONDEVSNSTHNCSHGateway;
					NONDEVSNSTHNCSHGateway=temp;
				}
				DEVSNSTHNCSH=BNKDTL.getBanksAmountBasedOnHOA("4",BankNames[j],"DEVELOPMENT");
				CHRTYCSH=BNKDTL.getBanksAmountBasedOnHOA("1",BankNames[j],"");
				SINVASCSH=BNKDTL.getBanksAmountBasedOnHOA("5",BankNames[j],"");
				PJSTRCSH=BNKDTL.getBanksAmountBasedOnHOA("3",BankNames[j],"");
				
						TNONDEVSNSTHNCSH=TNONDEVSNSTHNCSH+NONDEVSNSTHNCSH;
						TNONDEVSNSTHNCSHGateway=TNONDEVSNSTHNCSHGateway+NONDEVSNSTHNCSHGateway;
						TDEVSNSTHNCSH=TDEVSNSTHNCSH+DEVSNSTHNCSH;
						TSINVASCSH=TSINVASCSH+SINVASCSH;
						TPJSTRCSH=TPJSTRCSH+PJSTRCSH;
						TCHRTYCSH=TCHRTYCSH+CHRTYCSH;
						
						BNKTOTAL=NONDEVSNSTHNCSH+NONDEVSNSTHNCSHGateway+DEVSNSTHNCSH+CHRTYCSH+SINVASCSH+PJSTRCSH;
						
				rowhtml=rowhtml+"<tr ><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+(j+1)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+BankNames[j]+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+((NumbToDecml.format(NONDEVSNSTHNCSH + NONDEVSNSTHNCSHGateway)))+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(DEVSNSTHNCSH)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(CHRTYCSH)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(PJSTRCSH)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(SINVASCSH)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(BNKTOTAL)+"</td>";
				/*System.out.println("non===>"+NumbToDecml.format(NONDEVSNSTHNCSH));
			System.out.println("non===>"+NumbToDecml.format(NONDEVSNSTHNCSHGateway));
			System.out.println("non===>"+((NumbToDecml.format(NONDEVSNSTHNCSH + NONDEVSNSTHNCSHGateway))));*/
			}
			SaiCounter=CSHR_L.getSainivasCounterBalance(currentDate);
					PJRCounter=CUSTP_L.getDepartmentDayWiseIncome("3", currentDate);
							CharityCounter=CUSTP_L.getDepartmentDayWiseIncome("1", currentDate);
									SansthanCounter=CUSTP_L.getDepartmentDayWiseIncome("4", currentDate);
									HundiCollectionNDEV=CUSTP_L.getHundiCollectionNDDayWiseIncome("4","20017",currentDate);
									HundiCollectionDEV=CUSTP_L.getHundiCollectionNDDayWiseIncome("4","20016",currentDate);
									TCHRTYCSH=TCHRTYCSH+CharityCounter;
									TPJSTRCSH=TPJSTRCSH+PJRCounter;
									TSINVASCSH=TSINVASCSH+SaiCounter;
									TNONDEVSNSTHNCSH=TNONDEVSNSTHNCSH+SansthanCounter+HundiCollectionNDEV;
									TDEVSNSTHNCSH=TDEVSNSTHNCSH+HundiCollectionDEV;
									BNKTOTAL=SansthanCounter+SaiCounter+PJRCounter+CharityCounter;
									
			rowhtml=rowhtml+"<tr ><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>4</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Counter Cash</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(SansthanCounter + 0)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>0</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(CharityCounter)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(PJRCounter)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(SaiCounter)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(BNKTOTAL)+"</td>";
			rowhtml=rowhtml+"<tr ><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>5</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Hundi Collection</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(HundiCollectionNDEV)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(HundiCollectionDEV)+"</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>0.0</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>0.0</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>0.0</td><td style='background:#81f7f3;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(HundiCollectionNDEV + HundiCollectionDEV)+"</td>";
			rowhtml=rowhtml+"<tr><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'></td><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>TOTAL</td><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>&#8377; "+NumbToDecml.format(TNONDEVSNSTHNCSH + TNONDEVSNSTHNCSHGateway)+" /-</td><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>&#8377; "+NumbToDecml.format(TDEVSNSTHNCSH)+" /-</td><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>&#8377; "+NumbToDecml.format(TCHRTYCSH)+" /-</td><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>&#8377; "+NumbToDecml.format(TPJSTRCSH)+" /-</td><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>&#8377; "+NumbToDecml.format(TSINVASCSH)+" /-</td><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>&#8377; "+NumbToDecml.format(TNONDEVSNSTHNCSH+TNONDEVSNSTHNCSHGateway+TDEVSNSTHNCSH+TSINVASCSH+TPJSTRCSH+TCHRTYCSH)+"/-</td></tr></table>";
			
			
			mailhtml=mailhtml+rowhtml;
			//integrating  all major head pending balance....
			mailhtml=mailhtml+"<br>";
			//System.out.println(mailhtml);
			String pendingBillAndTotalAmountForSansthan[]=TotalPendingBillsForSansthan.returnToBankClosingBalance();
			mailhtml=mailhtml+pendingBillAndTotalAmountForSansthan[1]+"<br>";
			String pendingBillsAndTotalAmountForCharity[]=TotalPendingBillsForCharity.returnToBankClosingBalance();
			mailhtml=mailhtml+pendingBillsAndTotalAmountForCharity[1]+"<br>";
			String pendingBillsAndTotalAmountForSaiNivas[]=TotalPendingBillsForSaiNivas.returnToBankClosingBalance();
			mailhtml=mailhtml+pendingBillsAndTotalAmountForSaiNivas[1]+"<br>";
			
			double sansthanTotal=0.0;
			double sainivasTotal=0.0;
			double charityTotal=0.0;
			if(pendingBillAndTotalAmountForSansthan[0]!=null && !pendingBillAndTotalAmountForSansthan[0].trim().equals("")){
				sansthanTotal=Double.parseDouble(pendingBillAndTotalAmountForSansthan[0]);
			}
			if(pendingBillsAndTotalAmountForCharity[0]!=null && !pendingBillsAndTotalAmountForCharity[0].trim().equals("")){
				charityTotal=Double.parseDouble(pendingBillsAndTotalAmountForCharity[0]);
			}
			if(pendingBillsAndTotalAmountForSaiNivas[0]!=null && !pendingBillsAndTotalAmountForSaiNivas[0].trim().equals("")){
				sainivasTotal=Double.parseDouble(pendingBillsAndTotalAmountForSaiNivas[0]);
			}
			double grandTotalPendingBill=sansthanTotal+sainivasTotal+charityTotal;
			
			mailhtml=mailhtml+"<TABLE width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;>" +
										"<TR><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>TOTAL</th>" +
										"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>SANSTHAN</th>" +
										"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>CHARITY</th>" +
										"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>POOJA STORES</th>" +
										"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>SAI NIVAS</th>" +
										"<th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;text-transform: uppercase;>GRAND TOTAL</th></TR>" +
										"" +
										"<TR><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>TOTAL CLOSING BALANCE</td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(TNONDEVSNSTHNCSH+TNONDEVSNSTHNCSHGateway+TDEVSNSTHNCSH)+"</td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(TCHRTYCSH)+"</td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(TPJSTRCSH)+" </td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(TSINVASCSH)+"</td>	" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+NumbToDecml.format(TNONDEVSNSTHNCSH+TNONDEVSNSTHNCSHGateway+TDEVSNSTHNCSH+TCHRTYCSH+TPJSTRCSH+TSINVASCSH)+"</td>	</TR>" +
										"" +
										"<TR><td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>TOTAL PENDING BILLS</td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+sansthanTotal+"</td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+charityTotal+"</td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>0</td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+sainivasTotal+"</td>" +
										"<td style='background:#D3F332;font-size:12px;color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>"+grandTotalPendingBill+"</td>" +
										"</TR><TABLE>";				
			
			System.out.println("mailhtml===>"+mailhtml);
			sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST - TRUST BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "nagesh4444@yahoo.com","","","SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			//new added (8 july2015) trustees mail ids who get the mail daily basis ...
			sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sagar@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");
			sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", mailhtml,"68.169.54.16");

			
		}catch (Exception e) {
			e.printStackTrace();
	//System.out.println(e.toString());
		}
	return 1;

	}
}
