package crontab;

import java.util.List;
import java.io.IOException;
import java.text.DateFormat;
import beans.sent_sms_emailService;
import java.text.ParseException;
import beans.sssst_registrations;
import mainClasses.sssst_registrationsListing;
import beans.customerpurchases;
import mainClasses.customerpurchasesListing;
import beans.customerapplication;
import mainClasses.WtspNotification;
import mainClasses.customerapplicationListing;
import model.sendmailservice;
import java.util.ArrayList;
import java.util.Calendar;
import sms.CallSmscApi;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

public class NADLTARemainder
{
    public static void main(final String[] args) throws ParseException, IOException {
    	WtspNotification wtsp=new WtspNotification();
        final DateFormat dateFormat5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final TimeZone tz = TimeZone.getTimeZone("IST");
        dateFormat5.setTimeZone(tz);
        final CallSmscApi SMS = new CallSmscApi();
        String mailBody = "";
        final String subject = "NITYA ANNADANAM INVITATION";
        final String emailTemplate = "<div style='width:650px;margin:0px auto; background:#ff6600;padding:20px 0px;'><div style='width:580px;margin:0px auto;border:3px solid #fab001;padding:10px 10px;border-radius:15px;'><h3 style='text-align:center;color:#fff;font-family:Open Sans, sans-serif ;'>Shri Shirdi Saibaba Sansthan Trust</h3><h4 style='padding-left:18px;color:#fff;font-family:Open Sans, sans-serif ;'>Dear Saibandhu,</h4><p style='letter-spacing:0.5px;text-align:center;color:#fff;font-family:Open Sans, sans-serif;font-size:14px;'>Greetings and blessings from Shri Shirdi Saibaba Sansthan Trust, Dilsukhnagar, Hyderabad.</p><p style='letter-spacing:0.5px;line-height:20px;text-align:center;padding:0px 25px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'> The Sansthan Trust is very thankful and grateful for your generous contribution toward the Nitya Annadana Padhakam the sacred tradition of food offering, being organized by the Sansthan Trust on every day to serve the hunger.  </p><p style='letter-spacing:0.5px;line-height:20px;text-align:center;padding:0px 25px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>On this occasion, the Sansthan Trust performs Archana on the name of donor.  The Sansthan Trust cordially invites you, your family members and friends (maximum of 5 members) to participate in Archana which will be performed before 11.00 am. Attend Shri Saibaba Akanda Harathi and                Sri Sai Prasadam (Annadanam) and to receive the blessings of Shri Saibaba at Sansthan, Dilsukhnagar, Hyderabad.</p><h5 style='text-align:center;font-size:17px;color:#fff;font-family:cursive, sans-serif ;'> AnnidanalaKanna &#45; AnnadanamMinna</h5><h5 style='text-align:center;font-size:17px;color:#fff;font-family:cursive, sans-serif;margin-top:-20px;'>Annadata Sukhibhava</h5><p style='padding:0px 0px 0px 30px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;font-weight:bold;'>- With the blessings of Shri Shirdi Saibaba </p><p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Shri Shirdi Saibaba Sansthan Trust,</p> <p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Dilsukhnagar, Hyderabad. </p><p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>www.saisansthan.in</p> <p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>91-40-24066566</p></div></div> <div style='margin:0px auto;padding-left:20px;'><ul style='list-style:none;'><li style='float:left;color:#ff6600;padding:10px 10px 10px 10px;font-family:Open Sans, sans-serif;letter-spacing:0.5px;font-weight:bold;'>Follow us :</li><li style='padding:10px 10px 10px 10px;'><a href='http://www.facebook.com/dilsuknagarsaibabatemple' target='_blank'><img src='http://www.accounts.saisansthan.in/images/fb.png'/></a></li></ul></div>";
        final SimpleDateFormat dateFormat6 = new SimpleDateFormat("MM-dd");
        final SimpleDateFormat dateFormat7 = new SimpleDateFormat("dd-MMM");
        final DateFormat dateFormat8 = new SimpleDateFormat("MM-dd");
        final Calendar c1 = Calendar.getInstance();
        final Calendar c2 = Calendar.getInstance();
        c1.add(5, 1);
        c2.add(5, 7);
        final String str1 = dateFormat8.format(c1.getTime()).toString();
        final String str2 = dateFormat8.format(c2.getTime()).toString();
        final ArrayList<String> ar = new ArrayList<String>();
        ar.add(str1);
        ar.add(str2);
        final sendmailservice sendMail1 = new sendmailservice();
        final customerapplicationListing CAPL = new customerapplicationListing();
        customerapplication CAP = new customerapplication();
        final customerpurchasesListing SALE_L = new customerpurchasesListing();
        customerpurchases CUSPUR = new customerpurchases();
        final sssst_registrationsListing SSREGL = new sssst_registrationsListing();
        final sssst_registrations SSSST = new sssst_registrations();
        final String nadTemp = ".u r cordially invited to participate with 5 members before 11.00 AM for Archana n Annadanam at Sansthan SSSST, DSNR.";
        final String ltaTemp = ".u r cordially invited to participate in Archana before 11.00 AM n receive blessings of Shri Saibaba. SSSST, DSNR.";
        for (int j = 0; j < ar.size(); ++j) {
            final String date = ar.get(j);
            final List CAPLIST = CAPL.getListForDOBOrAnnivOrNADLTAReminder("extra1", date, "", "withPhone", "NAD");
            if (CAPLIST.size() > 0) {
                for (int i = 0; i < CAPLIST.size(); ++i) {
                    String mobilenum = "";
                    String email = "";
                    String name = "";
                    String daysRemainder = "";
                    String nadOrLtaId = "";
                    String perfDate = "";
                    String details = "";
                    if (j == 0) {
                        daysRemainder = "1 day";
                    }
                    else if (j == 1) {
                        daysRemainder = "7 days";
                    }
                    CAP = (customerapplication) CAPLIST.get(i);
                    final List CUSPURLIST = SALE_L.getBillDetails(CAP.getbillingId());
                    CUSPUR = (customerpurchases)CUSPURLIST.get(0);
                    mobilenum = CUSPUR.getphoneno().trim();
                    email = CUSPUR.getextra8().trim();
                    name = CAP.getlast_name().trim().replace("&", "");
                    nadOrLtaId = CAP.getphone().trim();
                    perfDate = CAP.getextra1().trim();
                    if (!nadOrLtaId.equals("")) {
                        details = String.valueOf(details) + " (YOUR REF NO: " + nadOrLtaId + ")";
                    }
                    if (!perfDate.equals("") && !perfDate.equals("-")) {
                        String dt = "";
                        try {
                            dt = dateFormat7.format(dateFormat6.parse(perfDate));
                        }
                        catch (ParseException parseEx) {
                            parseEx.printStackTrace();
                        }
                        perfDate = dt;
                    }
                    if (!mobilenum.equals("")) {
                        if(!name.equals("") && !CAP.getbillingId().equals("") && !perfDate.equals("")) {
                        	wtsp.getNdaLtaWishes(name, CAP.getbillingId(), perfDate, mobilenum);
                        	System.out.println("calling from whatsup notification..in nad");
                        }
                    	
                    	final String template = "Shri/Smt, NAD on " + name + ", " + details + ". Greetings n blessings ur NAD is on " + perfDate + nadTemp;
                        SMS.SendSMS(mobilenum, template.replace(" ", "%20"));
                        final Calendar c3 = Calendar.getInstance();
                        final String date2 = dateFormat5.format(c3.getTime()).toString();
                        final sent_sms_emailService SNE_SER = new sent_sms_emailService();
                        SNE_SER.setRegId(nadOrLtaId);
                        SNE_SER.setName(name);
                        SNE_SER.setPurpose("NAD");
                        SNE_SER.setDaysremainder(daysRemainder);
                        SNE_SER.setEmail("");
                        SNE_SER.setPhone(mobilenum);
                        SNE_SER.setCreatedDate(date2);
                        SNE_SER.insert();
                        try {
                            Thread.sleep(2000L);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (!email.equals("")) {
                        mailBody = "<h4 style='color:#ff6600;'>Hello, NAD on " + name + " ," + details + ".Please attend on " + perfDate + ".</h4>" + emailTemplate;
                        try {
                            sendMail1.sendmailsansthan("info@saisansthan.in", email, subject, mailBody);
                            final Calendar c4 = Calendar.getInstance();
                            final String date3 = dateFormat5.format(c4.getTime()).toString();
                            final sent_sms_emailService SNE_SER2 = new sent_sms_emailService();
                            SNE_SER2.setRegId(nadOrLtaId);
                            SNE_SER2.setName(name);
                            SNE_SER2.setPurpose("NAD");
                            SNE_SER2.setDaysremainder(daysRemainder);
                            SNE_SER2.setEmail(email);
                            SNE_SER2.setPhone("");
                            SNE_SER2.setCreatedDate(date3);
                            SNE_SER2.insert();
                        }
                        catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        try {
                            Thread.sleep(8000L);
                        }
                        catch (InterruptedException e3) {
                            e3.printStackTrace();
                        }
                    }
                }
            }
        }
        final List CAPLIST2 = CAPL.getListForDOBOrAnnivOrNADLTAReminder("extra1", (String)ar.get(0), "", "withPhone", "LTA");
        if (CAPLIST2.size() > 0) {
            for (int k = 0; k < CAPLIST2.size(); ++k) {
                String mobilenum2 = "";
                String email2 = "";
                String name2 = "";
                final String daysRemainder2 = "1 day";
                String nadOrLtaId2 = "";
                String perfDate2 = "";
                String details2 = "";
                CAP = (customerapplication)CAPLIST2.get(k);
                final List CUSPURLIST2 = SALE_L.getBillDetails(CAP.getbillingId());
                CUSPUR = (customerpurchases)CUSPURLIST2.get(0);
                mobilenum2 = CUSPUR.getphoneno().trim();
                email2 = CUSPUR.getextra8().trim();
                name2 = CAP.getlast_name().trim().replace("&", "");
                nadOrLtaId2 = CAP.getphone().trim();
                perfDate2 = CAP.getextra1().trim();
                if (!nadOrLtaId2.equals("")) {
                    details2 = String.valueOf(details2) + " (YOUR REF NO: " + nadOrLtaId2 + ")";
                }
                if (!perfDate2.equals("") && !perfDate2.equals("-")) {
                    String dt2 = "";
                    try {
                        dt2 = dateFormat7.format(dateFormat6.parse(perfDate2));
                    }
                    catch (ParseException parseEx2) {
                        parseEx2.printStackTrace();
                    }
                    perfDate2 = dt2;
                }
                if (!mobilenum2.equals("")) {
                	if(!name2.equals("") && !perfDate2.equals("") && !CAP.getbillingId().equals("")) {
                		wtsp.getNdaLtaWishes(name2, CAP.getbillingId(), perfDate2, mobilenum2);
                		System.out.println("calling from whatsup notification..in lta");
                	}
                    final String template2 = "Shri/Smt, LTA on " + name2 + ", " + details2 + ". Greetings n blessings ur LTA is on " + perfDate2 + ltaTemp;
                    SMS.SendSMS(mobilenum2, template2.replace(" ", "%20"));
                    final Calendar c5 = Calendar.getInstance();
                    final String date4 = dateFormat5.format(c5.getTime()).toString();
                    final sent_sms_emailService SNE_SER3 = new sent_sms_emailService();
                    SNE_SER3.setRegId(nadOrLtaId2);
                    SNE_SER3.setName(name2);
                    SNE_SER3.setPurpose("LTA");
                    SNE_SER3.setDaysremainder(daysRemainder2);
                    SNE_SER3.setEmail("");
                    SNE_SER3.setPhone(mobilenum2);
                    SNE_SER3.setCreatedDate(date4);
                    SNE_SER3.insert();
                    try {
                        Thread.sleep(2000L);
                    }
                    catch (InterruptedException e4) {
                        e4.printStackTrace();
                    }
                }
            }
        }
    }
}

