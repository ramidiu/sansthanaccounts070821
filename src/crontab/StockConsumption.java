package crontab;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class StockConsumption {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		process();
	}
	public static int process(){
		MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat  dateFormat_new= new SimpleDateFormat("dd MMM yyyy");
		mainClasses.headofaccountsListing HODA=new mainClasses.headofaccountsListing();
		beans.headofaccounts HOD=new beans.headofaccounts();
		TimeZone tz = TimeZone.getTimeZone("IST");
		DecimalFormat df = new DecimalFormat("#.##");
		dateFormat2.setTimeZone(tz.getTimeZone("IST"));
		dateFormat_new.setTimeZone(tz.getTimeZone("IST"));
		Calendar c1 = Calendar.getInstance(); 
		String currentDate=(dateFormat2.format(c1.getTime())).toString();
		String presentdate=(dateFormat_new.format(c1.getTime())).toString();
		String fromDate=currentDate;
		String toDate=currentDate;
		
		
		mainClasses.shopstockListing SHL=new mainClasses.shopstockListing();
		mainClasses.productsListing PROL=new mainClasses.productsListing();
		mainClasses.employeesListing EMPL=new mainClasses.employeesListing();
		beans.shopstock SHPST=new beans.shopstock();
		List HODET=HODA.getheadofaccounts();
		List STCDET=null;
		String html="";
		String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=5> SSSST- DAILY STOCK CONSUMPTION REPORT </th></tr>";
		String s2="";		
		if(HODET.size()>0){ 
				for(int i=0;i<HODET.size();i++){
						HOD=(beans.headofaccounts)HODET.get(i);
						s2=s2+"<tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=7>"+HOD.getname()+"</th></tr><tr style=background:#BDBDBD;><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>PRODUCT NAME</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>ISSUED QTY</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>NARRATION</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>ENTERED BY</th></tr>";
						STCDET=SHL.getStockDailyIssuses(fromDate, toDate, HOD.gethead_account_id());
						if(STCDET.size()>0){
							for(int s=0;s<STCDET.size();s++){
								SHPST=(beans.shopstock)STCDET.get(s);
								s2=s2+"<tr><td style=padding:5px;>"+(s+1)+"</td><td style=padding:5px;>"+PROL.getProductsNameByCat1(SHPST.getproductId(),"")+"</td><td style=padding:5px;>"+SHPST.getquantity()+""+PROL.getProductsNameByCatUnits(SHPST.getproductId(),"")+"</td><td style=padding:5px;text-align:right;>"+SHPST.getdescription()+"</td><td style=padding:5px;text-align:right;>"+EMPL.getMemployeesName(SHPST.getemp_id())+"</td></tr>";
							}
						}else{
							s2=s2+"<tr><td style=padding:5px;color:red;font-size:20px; colspan=5>Sorry,there is no stock issued for this department today!</td></tr>";
						}
						
					}
				}
		html=s1+s2+"</table>";
/*		System.out.println(html);*/
		/*sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY MASTER STOCK ENTRY REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST-  DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SSSST-  DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST-  DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST-  DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST-  DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST-  DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"localhost");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"localhost");*/
		
		//newly added on 16/06/2016 by pradeep by keeping disable above sendmail lines
		sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "SSSST -  DAILY MASTER STOCK ENTRY REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "nagesh4444@yahoo.com","","","SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "saisansthan_dsnr@yahoo.co.in", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sagar@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "SSSST - BANK CLOSING BALANCE & PENDING BILLS", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "SSSST- DAILY STOCK CONSUMPTION REPORT", html,"68.169.54.16");
		return 1;
	}
}
