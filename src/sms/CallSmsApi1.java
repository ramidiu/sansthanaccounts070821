package sms;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

// OkHttp Client
public class CallSmsApi1{
	
		/*public static void main(String[] args) {
			OkHttpClient client = new OkHttpClient();
			Request request = new Request.Builder().url("http://websms.bulksmspremium.com/rest/services/sendSMS/sendGroupSms?AUTH_KEY=28cdefd7b3917aaeca03666dca8e6&message=helloumesh&senderId=SAITST&routeId=1&mobileNos=9700000303&smsContentType=english").get().addHeader("Cache-Control", "no-cache").build();
			try	{
				Response response = client.newCall(request).execute();
				System.out.println("sms response body-----"+response.body().string());
				System.out.println("sms response message-----"+response.message());
				System.out.println("sms response headers-----"+response.headers());
			}
			catch(IOException ie)	{
				ie.printStackTrace();
			}
			catch(Exception e)	{
				e.printStackTrace();
			}
		}*/
		
		public void sendSms(String mobileNo,String message)	{
			if ((mobileNo != null && !mobileNo.trim().equals("")) && (message != null && !message.trim().equals("")))	{
				OkHttpClient client = new OkHttpClient();
				Request request = new Request.Builder().url("http://websms.bulksmspremium.com/rest/services/sendSMS/sendGroupSms?AUTH_KEY=28cdefd7b3917aaeca03666dca8e6&message="+message+"&senderId=SAITST&routeId=1&mobileNos="+mobileNo+"&smsContentType=english").get().addHeader("Cache-Control", "no-cache").build();
				try	{
					Response response = client.newCall(request).execute();
					System.out.println("mobile---"+mobileNo);
					System.out.println("sms response body-----"+response.body().string());
					System.out.println("sms response message-----"+response.message());
					System.out.println("sms response headers-----"+response.headers());
					boolean ismessageSent = response.isSuccessful();
					System.out.println("ismessageSent------"+ismessageSent);
				}
				catch(IOException ie)	{
					System.out.println("*** sms not sent ***");
					ie.printStackTrace();
				}
				catch(Exception e)	{
					System.out.println("*** sms not sent ***");
					e.printStackTrace();
				}
			}
		}
}
