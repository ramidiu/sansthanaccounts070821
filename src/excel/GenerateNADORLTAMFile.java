package excel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import beans.customerapplication;
import beans.customerpurchases;

import mainClasses.customerapplicationListing;
import mainClasses.customerpurchasesListing;
import mainClasses.productsListing;

public class GenerateNADORLTAMFile {
	public String NADORLTAMExcelFile(String subheadId,String hoid)
	{
		String fileName="NADORLTAM";
        if(subheadId.equals("20092"))
        {
        	fileName = "NAD";
        }else{
        	fileName = "LTAM";
        }
		final String FILE_PATH = "D:/Workspace_new/saiSansthanAccounts/WebContent/employeedocuments/"+fileName+"Devotees.xlsx";
		//final String FILE_PATH = "/var/www/kreativ/data/www/10.kreativwebsolutions.com/ROOT/administrator/menuxlsxFiles/MenuList.xlsx";
		
		// Using XSSF for xlsx format, for xls use HSSF
        Workbook workbook = new XSSFWorkbook();
        
        Sheet studentsSheet = workbook.createSheet(fileName);

        customerpurchases SALE = new customerpurchases();
		customerapplication CAP = new customerapplication();
		customerpurchasesListing SALE_L = new customerpurchasesListing();
		customerapplicationListing CUSAL=new customerapplicationListing();
		productsListing PRDL=new productsListing();
		List CUS_DETAIL=null;
		List SAL_list=SALE_L.getDevoteesList(subheadId);
		if(SAL_list.size()>0){
			int rowIndex0 = 0;
			Row row0 = studentsSheet.createRow(rowIndex0++);
			row0.createCell(4).setCellValue("LIST OF DEVOTEES  FOR"+PRDL.getProductsNameByCats(subheadId,hoid));
	        int rowIndex = 1;
	        Row row1 = studentsSheet.createRow(rowIndex++);
	        row1.createCell(0).setCellValue("S.NO");
	        row1.createCell(1).setCellValue("APP REF.NO");
	        row1.createCell(2).setCellValue("RECEIPT.NO");
	        row1.createCell(3).setCellValue("NAME");
	        row1.createCell(4).setCellValue("ADDRESS1");
	        row1.createCell(5).setCellValue("ADDRESS2");
	        row1.createCell(6).setCellValue("ADDRESS3");
	        row1.createCell(7).setCellValue("CITY");
	        row1.createCell(8).setCellValue("PINCODE");
	        row1.createCell(9).setCellValue("SEVA PERFORM IN THE NAME OF");
	        row1.createCell(10).setCellValue("GOTHRAM");
	        for(int i=0; i < SAL_list.size(); i++ )
			{ 
				SALE=(beans.customerpurchases)SAL_list.get(i);
			
				CUS_DETAIL=CUSAL.getMcustomerDetailsBasedOnBillingId(SALE.getbillingId());
				if(CUS_DETAIL.size()>0){
				CAP=(customerapplication)CUS_DETAIL.get(0);}
				String address2 = CAP.getextra2();
				String add2[]={""};
				if(!address2.equals("")){
				add2 = address2.split(",");
				}
				String fAdd2="";
				String fAdd3="";
				String cites = CAP.getextra3();
				String city[]={""};
				if(!cites.equals("")){
					city = cites.split(",");
				}
				String fCity="";
				String fPincode="";
				String PerformName="";
				
				Row row = studentsSheet.createRow(rowIndex++);
	            int cellIndex = 0;
	
	            row.createCell(cellIndex++).setCellValue(i+1);
	            row.createCell(cellIndex++).setCellValue(CAP.getphone());
	            row.createCell(cellIndex++).setCellValue(SALE.getbillingId());
	            row.createCell(cellIndex++).setCellValue(SALE.getcustomername());
	            row.createCell(cellIndex++).setCellValue(SALE.getextra6());
	            if(add2.length > 0)
	            {
	            	for(int k=0;k<add2.length;k++)
	            	{
	            		fAdd2 = fAdd2 + add2[k];
	            		if(add2.length>1 && k<add2.length-1)
	            		{
	            			fAdd2 = fAdd2 +",";
	            		}
	            	}
	            }
	            row.createCell(cellIndex++).setCellValue(fAdd2);
	            if(CUS_DETAIL.size()>0){
	            	fAdd3 = CAP.getaddress2();
	            	}
	            row.createCell(cellIndex++).setCellValue(fAdd3);
	            if(city.length > 0)
	            	{
	            		for(int k=0;k<city.length;k++)
	            		{
	            			fCity = fCity + city[k];
	            			if(city.length>1 && k<city.length-1)
	            			{ 
	            				fCity = fCity + ",";
	            			}
	            		}
	            	}
	            row.createCell(cellIndex++).setCellValue(fCity);
	            if(CUS_DETAIL.size()>0){
	            	fPincode = CAP.getpincode();
	            	}
	            row.createCell(cellIndex++).setCellValue(fPincode);
	            if(CUS_DETAIL.size()>0)
	            {
	            	PerformName = CAP.getlast_name();
	            }
	            row.createCell(cellIndex++).setCellValue(PerformName);
	            row.createCell(cellIndex++).setCellValue(SALE.getgothram());
	            
		}
	}
		try {
            FileOutputStream fos = new FileOutputStream(FILE_PATH);
            workbook.write(fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return FILE_PATH;
	}
}
