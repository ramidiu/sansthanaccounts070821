<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="beans.customerapplication"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Group List</title>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />

 <script src="js/jquery-1.4.2.js"></script>
<script src="js/nicEdit-latest.js" type="text/javascript"></script>

<Script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</Script>
<script type="text/javascript">bkLib.onDomLoaded(function() {
    nicEditors.editors.push(new nicEditor().panelInstance(document.getElementById('long_desc')));
    $("div.nicEdit-main").keydown(function(event) {
        if (event.ctrlKey==true && (event.which == '118' || event.which == '86')) {
		//alert('key presse');
		event.preventDefault();
		modalWindowRTF.windowId = "myModal";
		modalWindowRTF.width = 550;
		modalWindowRTF.height = 260;
		modalWindowRTF.open();
	 }
    });
 });
function validate(){
	var nicE = new nicEditors.findEditor('long_desc');
	var question = nicE.getContent();
	$("#long_desc").val(question); // Value What we entered should be passed to another page.
	}
</script>
<script language = "JavaScript">

//----for check atleast one check box-----
function check1(){
	var inputs = document.getElementsByTagName("input");
	var msg=document.groupForm.message.value;
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one");
	return false;} 
	
	else{
		return true;
	}
}

function verify()
{
	if(document.getElementById('NadOrLta').value=="")
	{
	alert("Please Select Seva");
	$("#NadOrLta").focus();
	return false;
	}
	
	if(document.getElementById('remainderDays').value=="")
	{
	alert("Please Select No Of Days");
	$("#remainderDays").focus();
	return false;
	}
}
</script>

 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

</head>
<body>
<%if(session.getAttribute("empId")!=null){ 
	 String NadOrLta = request.getParameter("NadOrLta");
	 String remainderDays = request.getParameter("remainderDays"); 
%>
<div><%@ include file="title-bar.jsp"%></div>
<form name="reg" id="reg" method="post" action="NadLtaRemainder.jsp" onsubmit="return verify();">
<table></table>
<table style="margin-left:80px;margin-top: 100px;"><tr>
<td><select name="NadOrLta" id="NadOrLta">
<%
	if(NadOrLta == null)
	{%>
		<option value="">-- SELECT --</option>
	<%}
%>
<option value="NAD" <%if(NadOrLta != null && NadOrLta.equals("NAD")) {%> selected="selected"<%} %>>NAD</option>
<option value="LTA" <%if(NadOrLta != null && NadOrLta.equals("LTA")) {%> selected="selected"<%} %>>LTA</option>
<option value="NAD AND LTA" <%if(NadOrLta != null && NadOrLta.equals("NAD AND LTA")) {%> selected="selected"<%} %>>NAD AND LTA</option>
</select></td>
<td><select name="remainderDays" id="remainderDays"  style="margin-left:20px;">
<%
	if(remainderDays == null)
	{%>
		<option value="">-- SELECT --</option>
	<%}
%>
<option value="1 day" <%if(remainderDays != null && remainderDays.equals("1 day")) {%> selected="selected"<%} %>>1 Day Remainder</option>
<option value="7 days" <%if(remainderDays != null && remainderDays.equals("7 days")) {%> selected="selected"<%} %>>7 Days Remainder</option>
<option value="15 days" <%if(remainderDays != null && remainderDays.equals("15 days")) {%> selected="selected"<%} %>>15 Days Remainder</option>
</select></td>
<td><input type="submit" class="click" value="SUBMIT" style="margin-left:20px;"></td>
</tr></table>
</form>
<% if(NadOrLta != null && remainderDays != null){%>
<%
	customerapplicationListing CAPL=new customerapplicationListing();
	customerapplication CAP = new customerapplication();
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	customerpurchases CUSPUR = new customerpurchases();
%>
<div class="vendor-page">
<div class="vendor-list">
<div class="icons">
	<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
	<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>

<div class="clear"></div>
<div class="list-details">
	 <div class="printable">
 <form name="groupForm" method="POST" action="sendMsgNEmailsToNadLtam.jsp" onSubmit="return check1();">	
	<input type="hidden" name="nadLtaReaminder" value="nadLtaReaminder" />	
	 <table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
			<tr><td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;"><%=NadOrLta %> <%=remainderDays%> Remainder List</td></tr>
			
			<tr>
			<jsp:useBean id="REG"  class="beans.sssst_registrations"></jsp:useBean>
			<td colspan="8">
				<table width="100%" cellpadding="0" cellspacing="0"  class="print_table">
					 
            <tr class="print_table" style="background-color: #e3eaf3;color: #7490ac">
            	<td align="left"   width="11%" style="font-weight: bold;"><input type="checkbox" name="SelectAll" id="selecctall"/>Check All</td>
				<td align="left"   width="5%" style="font-weight: bold;">S.NO.</td>
				<td align="left"   width="7%" style="font-weight: bold;">NAD/LTAM-ID</td>
				<td align="left" width="17%" style="font-weight: bold;">CUSTOMER NAME</td>
				<td align="left" width="17%" style="font-weight: bold;">SEVA PERFORMED ON THE NAME OF</td>
				<td align="left" width="12%" style="font-weight: bold;">PERFORMED DATE(Month-Date)</td>
				<td align="left"   width="13%" style="font-weight: bold;">PHONE NO</td>
				<td align="left"   width="18%" style="font-weight: bold;">EMAIL-ID</td>				
			</tr>
<%
DateFormat df= new SimpleDateFormat("dd-MM-yyyy");
DateFormat df2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat df3= new SimpleDateFormat("dd-MMM");
DateFormat dbdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	DateFormat dateFormat4= new SimpleDateFormat("MM-dd");
	Calendar c = Calendar.getInstance(); 
	
	String currentDate=(dateFormat4.format(c.getTime())).toString();
	
	if(remainderDays.equals("1 day"))
	{
		c.add(Calendar.DATE, 1);
	}
	
	else if(remainderDays.equals("7 days"))
	{
		c.add(Calendar.DATE, 7);
	}
	
	else if(remainderDays.equals("15 days"))
	{
		c.add(Calendar.DATE, 15);
	}
	
	String date=(dateFormat4.format(c.getTime())).toString();

	mainClasses.customerapplicationListing CAL = new mainClasses.customerapplicationListing();	
	
	List CAPLIST = CAPL.getListForDOBOrAnnivOrNADLTAReminder("extra1",date,"","withPhone",NadOrLta);
	
	int k = 0;
	if(CAPLIST.size() > 0)
	{
		for(int i=0; i < CAPLIST.size(); i++ )
		{
			CAP=(customerapplication)CAPLIST.get(i);
			List CUSPURLIST=SALE_L.getBillDetails(CAP.getbillingId());
			CUSPUR=(customerpurchases)CUSPURLIST.get(0);
			k+=1;
				%>
					<tr style="position: relative;">
					<td align="left" width="11%"><input type="checkbox" name="select1" class="checkbox1" value = "<%=CAP.getbillingId()%>"></td>
				 	<td align="left" width="5%"><%=k%></td>			 		
					<td align="left" width="7%"><%=CAP.getphone() %></td>		
					<td align="left" width="17%"><%=CAP.getfirst_name() %></td>		 	
				 	<td align="left" width="17%"><%=CAP.getlast_name() %></td>
				 	<td align="left" width="12%"><%=CAP.getextra1() %></td>
				 	<td align="left" width="13%"><%=CUSPUR.getphoneno() %></td>
				 	<td align="left" width="18%"><%=CUSPUR.getextra8() %></td>
				 </tr>	
				<%}	}%>
<table width="100%" border="1" cellspacing="0" cellpadding="0" id="tblExport">
	<tr>
<td colspan="3" align="right" >Template</td>
<td colspan="3" align=left>
	<textarea name="message" id="long_desc" rows="4" cols="40" placeHolder="Write your message here!"   ></textarea>
	<br><span>Note: Enter 160 characters message only!</span>
	<br><input type="text" name="subject" value="" placeHolder="Write E-Mail Subject here!"/>
</td>
<td colspan="4" align="left">
<input type="submit" class="click" name="submit" value="SEND SMS"><br/> 
<!-- <input type="submit" name="submit" class="click" value="SEND MAIL'S"><br/> 
<input type="submit" name="submit" class="click" value="SEND SMS&MAIL"></td> -->
</tr>
	</table>
	</table>
			</td>
			</tr>
	</table>
	</form>
	 </div>
</div>

</div>
</div>

<%}}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>