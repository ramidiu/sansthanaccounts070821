<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SHOP SALES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( ".printable" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<jsp:useBean id="DOC" class="beans.doctordetails"/>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-list">
<!-- <div class="arrow-down"><img src="images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div> -->
<div class="icons">
<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>
<div class="clear"></div>
<div class="list-details">
<%
mainClasses.doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
List DOCT_List=DOCT.getdoctorDetailsGroupBySpecialist();
List DOC_Det=null;
int sno=0;
if(DOCT_List.size()>0){%>
<div class="printable">
      <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
<tr>
  <td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</td>
</tr>
<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<tr><td colspan="7" align="center" style="font-weight: bold;">Doctor's Timings Report</td></tr>
<tr><td colspan="7"><table  width="100%" cellpadding="0" cellspacing="0" id="tblExport" class="yourID">
<tr>
<td class="bg" width="3%">S.NO.</td>
<td class="bg" width="15%">DOCTOR NAME</td>
<td class="bg" width="12%"> QUALIFICATION</td>
<td class="bg" width="10%">PHONE NO</td>
<td class="bg" width="20%">ADDRESS</td>
<td class="bg" width="20%">ATTENDING DAYS</td>
<td class="bg" width="20%">TIMINGS</td>
<!-- <td class="bg" width="20%">Edit</td>
<td class="bg" width="10%">Delete</td> -->
</tr>
</table></td></tr>
<%
for(int i=0; i < DOCT_List.size(); i++ ){
	DOC=(beans.doctordetails)DOCT_List.get(i); %>
<tr><td colspan="7" style="color: blue;font-weight: bold;"><%=DOC.getextra3() %></td></tr>
<%DOC_Det=DOCT.getDoctorsBasedOnSpecialist(DOC.getextra3()); 
for(int d=0;d<DOC_Det.size();d++){
	DOC=(beans.doctordetails)DOC_Det.get(d);
	sno=sno+1;
%>
<tr>
<td width="3%" align="center" ><%=sno%></td>
<td width="15%" align=""><span><%=DOC.getdoctorName()%></span></td>
<td width="12%" align=""><%=DOC.getdoctorQualification()%></td>
<td width="10%" align=""><%=DOC.getphoneNo()%></td>
<td width="20%" align=""><%=DOC.getaddress()%></td>
<td width="20%" align=""><%=DOC.getextra4()%></td>
<td width="20%" align=""><%=DOC.getextra5()%></td>
<%-- <td><a href="doctordetails_Form.jsp?id=<%=DOC.getdoctorId() %>">Edit</a></td>
<td><a href="doctordetails_Delete.jsp?Id=<%=DOC.getdoctorId()%>">Delete</a></td> --%>
</tr>
<%}} %>
</table>
</div>
<%}else{%>
<div align="center"><h1>No Tablets Added Yet</h1></div>
<%}%>


</div>
</div>

</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>