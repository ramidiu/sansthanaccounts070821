<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
/*.bod-nn{
	border-right:none !important;}*/
	}
/*	.bod-nn{
	border-right:1px solid #000 !important;
	font-weight: bold;}*/
</style>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRO OFFICE DAILY SALES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">
		<div class="vendor-list">
		
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<div class="sj">
					 <form action="proOffice-salesReport.jsp" method="post">  
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form name="searchForm" id="searchForm" action="proOffice-salesReport.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" class="click" name="search" value="Search"></input>
						<select name="headID" id="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="5" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("5")){ %>selected="selected" <%} %>>SAI NIVAS</option>
						<option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %>selected="selected" <%} %>>POOJA STORES</option>
						</select></li>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<a id="print"><span><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Columns</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	banktransactionsListing BKTRAL=new banktransactionsListing();
	productsListing PRDL=new productsListing();
	employeesListing EMPL=new employeesListing();
	
	List BANK_DEP=null;
	
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:00";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	String hoaID=session.getAttribute("headAccountId").toString();
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoaID=request.getParameter("headID");
	}
	List SAL_list=SALE_L.getcustomerpurchasesListBasedOnHOAandDates(hoaID,fromDate,toDate);
	List SAL_DETAIL=null;
	double totalAmount=0.00; 
	double totalUSDAmount=0.00; 
	double totalPoundAmount=0.00;
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double bankDepositTot=0.00;
	double creaditSaleTot=0.00;
	double chequeSaleTot=0.00;
	double onlineSaleTot=0.00;
	double cashSaleAmt=0.00;
	double cardSaleTot=0.00;
	double gPaySaleTot=0.00;
	double phnPeSaleAmt=0.00;
	if(SAL_list.size()>0){%>
		<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td>
						</tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;">Daily Sales Detail Report</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
						<tr>
						<td colspan="7">
						<table width="100%" cellpadding="0" cellspacing="0" class="print_table">
						<tr>
							<td class="bg" width="4%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="13%" style="font-weight: bold;">INVOICE DATE</td>
							<td class="bg" width="10%" style="font-weight: bold;">ACCOUNT/V.NO</td>
							<td class="bg" width="5%" style="font-weight: bold;">CODE</td>
							<td class="bg" width="21%" style="font-weight: bold;">SEVA NAME</td>
							<td class="bg" width="20%" style="font-weight: bold;">DEVOTEE NAME</td>
							<td class="bg" width="7%" style="font-weight: bold;">GOTHRAM</td>
							<td class="bg" width="10%" style="font-weight: bold;">AMOUNT</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">ENTERED BY</td>
							
						</tr>
						<%
						Double totalSaleAmt=0.00;
						Double totDeposit=0.00;
						if(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate)!=null && !SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate).equals("")){
							 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate));
						}
						if(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate)!=null && !BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate).equals("")){
							 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate));}
						Double openingBal=totalSaleAmt-totDeposit;
						%>
						<tr>
						<td colspan="4"></td>
						<td colspan="2">OPENING BALANCE</td>
						<td align="right" style="font-weight: bold;">&#8377; <%=df.format(openingBal)%></td>
						<td align="right" style="font-weight: bold;">&#8377;0.00</td>
						
						</tr>
						<%for(int i=0; i < SAL_list.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_list.get(i);
							if(!presentDate.equals("")){
								prevDate=presentDate;	
							}
							presentDate=""+chngDateFormat.format(dbDateFormat.parse(SALE.getdate()));
							if(SALE.getcash_type().equals("cash")){
								cashSaleAmt=cashSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}else if(SALE.getcash_type().equals("cheque")){
								chequeSaleTot=chequeSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}else if(SALE.getcash_type().equals("online success")){
								onlineSaleTot=onlineSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}else if(SALE.getcash_type().equals("card")){
								cardSaleTot=cardSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}else if(SALE.getcash_type().equals("googlepay")){
								gPaySaleTot=gPaySaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}else if(SALE.getcash_type().equals("phonepe")){
								phnPeSaleAmt=phnPeSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}
							%>
						    <tr style="position: relative;">
							<td style="font-weight: bold;"><%=i+1%></td>
							<td style="font-weight: bold;"><%=chngDateFormat.format(dbDateFormat.parse(SALE.getdate()))%></td>
							<td style="font-weight: bold;"><%=SALE.getbillingId()%></td>
							<td style="font-weight: bold;"><%=SALE.getproductId()%></td>
							
							<td style="font-weight: bold;"><span>
								<%=PRDL.getProductsNameByCats(SALE.getproductId(),session.getAttribute("headAccountId").toString())%></span></td>
							<td style="font-weight: bold;"><%=SALE.getcustomername()%></td>
							<td style="font-weight: bold;"><%=SALE.getgothram()%></td>
							<%if(SALE.getextra5().equals("USD")){
								totalUSDAmount=totalUSDAmount+Double.parseDouble(SALE.gettotalAmmount());
							}else if(SALE.getextra5().equals("POUND")){
								totalPoundAmount=totalPoundAmount+Double.parseDouble(SALE.gettotalAmmount());
							} else{
								totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());
							}
							%>
							<td style="font-weight: bold;" align="right"><%if(SALE.getextra5().equals("USD")){ %>$<%}else if(SALE.getextra5().equals("POUND")){ %>&pound;<%}else{ %>&#8377;<%} %> <%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td>
							<td align="right"> <%if(EMPL.getMemployeesName(SALE.getemp_id())!=null && !EMPL.getMemployeesName(SALE.getemp_id()).equals("")){ %> <%=EMPL.getMemployeesName(SALE.getemp_id()) %> <%}else{ %> ONLINE <%} %> </td>
							<%--  <%if(!prevDate.equals(presentDate)){ 
								String sjDate=chngDF.format(dbDateFormat.parse(SALE.getdate()));
								String depositAmt=BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),sjDate);%>
								
								 <td style="font-weight: bold;" align="right">&#8377;<%if(depositAmt!=null){
									//bankDepositTot=bankDepositTot+Double.parseDouble(depositAmt);
									%> <%=depositAmt%><%}else{ %>00<%} %></td>
							<%}else{ %>
							<td></td>
							<%} %> --%>
						</tr>
					   <%} %>
					   <%//List BankDeposits=BKTRAL.getDepositsList(session.getAttribute("empId").toString(), fromDate,toDate);
					   String amtDepositedCat="pro-counter";
					   if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
							if(request.getParameter("headID").equals("1")){
								amtDepositedCat="Charity-cash";
							}else if(request.getParameter("headID").equals("4")){
								amtDepositedCat="pro-counter";
							}
						}
					   List BankDeposits=BKTRAL.getDepositsListBasedOnHead(amtDepositedCat, fromDate,toDate);
					   if(BankDeposits.size()>0){
					   		for(int j=0;j<BankDeposits.size();j++){
					   		BNK=(banktransactions)BankDeposits.get(j);
					   		bankDepositTot=bankDepositTot+Double.parseDouble(BNK.getamount());
					   %>
					   <tr style="color: fuchsia;">
					   		<td style="font-weight: bold;"><%=j+1%></td>
					   		<td style="font-weight: bold;"><%=chngDateFormat.format(dbDateFormat.parse(BNK.getdate()))%></td>
					   		<td style="font-weight: bold;"><%=BNK.getbanktrn_id()%></td>
					   		<td style="font-weight: bold;" colspan="3"><%=BNK.getnarration() %></td>
					   		<td style="font-weight: bold;" colspan="2" align="right"><%=df.format(Double.parseDouble(BNK.getamount()))%></td>
					   		<td style="font-weight: bold;" align="right"></td>
					   </tr>
					   
					   <%}} %>
					   <tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total Cash sales amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(cashSaleAmt)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total Cheque sales amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(chequeSaleTot)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total Online sales amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(onlineSaleTot)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total Card sales amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(cardSaleTot)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total BharathPe sales amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(gPaySaleTot)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total PhonePe sales amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(phnPeSaleAmt)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						
						
						
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total sales amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<%if(totalUSDAmount>0){ %>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total sales amount in (USD-$) | </td>
						<td style="color: orange;font-weight: bold;" align="right"> $ <%=df.format(totalUSDAmount)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<%} %>
						<%if(totalPoundAmount>0){ %>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total sales amount in (Pounds &pound;) | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &pound; <%=df.format(totalPoundAmount)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr><%} %>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Total Bank Deposit Amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(bankDepositTot) %></td>
						<td style="font-weight: bold;"></td>
						</tr>
						<%-- <tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total credit sales| </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(creaditSaleTot) %></td>
						<td style="font-weight: bold;"></td>
						</tr> --%>
						<tr style="border: 1px solid #000;">
						<td colspan="7" align="right" style="font-weight: bold;">Closing Balance | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount-bankDepositTot) %></td>
						<td style="font-weight: bold;"></td>
						</tr>
					</table>
					</td>
					</tr>
					</table>
				</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no sales list found for today!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>