<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>

<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function idValidate(){
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	var id=$('#major_head_id').val().trim();
$.ajax({
	type:'post',
	url: 'IDValidate.jsp', 
	data: {
		Id : id,
		type : 'majorhead'
	},
	success: function(response) { 
		var msg=response.trim();
		$('#majorheadErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#major_head_id').focus();
			return false;
		}
	}});
}

</script>
<script type="text/javascript">

function validate(){
	$('#headIdErr').hide();
	$('#majorheadErr').hide();
	$('#headNameErr').hide();

	if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	}
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	if($('#name').val().trim()==""){
		$('#headNameErr').show();
		$('#name').focus();
		return false;
	}
}
function productsearch(){

	  var data1=$('#productname').val();
	  var headID=$('#headAccountId').val();
	  $.post('searchProducts.jsp',{q:data1,hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">

<div class="vendor-box">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Product Ledger Report</td></tr>
</table>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
String productid="";String hid="";
if(request.getParameter("headAccountId")!=null){
	hid=request.getParameter("headAccountId");
}
if(request.getParameter("productname")!=null){
	productid=request.getParameter("productname");
}
%>

<form name="tablets_Update" method="POST" action="productReport.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account<span style="color: red;">*</span></td>
<td>Product Name<span style="color: red;">*</span></td>
<td></td>
</tr>
<tr>
<td>
<select name="headAccountId" id="headAccountId">
<%if(!hid.equals("")){
	%>
	<option value="<%=hid%>" selected="selected"><%=HOA_L.getHeadofAccountName(hid) %></option>
	<%
}
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select>
</td>
<td><input type="text" name="productname" id="productname" value="<%=productid %> " onkeyup="productsearch()" /></td>
<td align="right"><input type="submit" value="Search" class="click" style="border:none;"/></td>
</tr>

<tr> 
<td colspan="3"></td>

</tr>
</table>
</form>

</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
</ul>
</div>
<!-- <div class="icons">
<span><img src="images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div> -->
<div class="clear"></div>
<div class="list-details">
<%productsListing PRD_L=new productsListing();
if(request.getParameter("productname")!=null){
productid=request.getParameter("productname");
String temp[]=productid.split(" ");
if(temp.length>1){
if(PRD_L.getvalidProduct(temp[0])){%>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Month</td>
<td class="bg" width="10%">Opening Balance</td>
<td class="bg" width="23%">Inwards </td>
<td class="bg" width="23%">OutWards</td>
<td class="bg" width="20%">Closing Balance</td>

</tr>
<%
godwanstockListing GDSTK_L=new godwanstockListing();
shopstockListing SHPSTK_L=new shopstockListing();
DateFormatSymbols dfs = new DateFormatSymbols();
String[] months = dfs.getMonths();
String month = "";
int num=0;
String dos="2014-04-01";
String inwards="00";
int opengbal=0;
int closinbal=0;
int inwardsstock=0;
int closeingstock=0;
String outwards="";
SimpleDateFormat dateformat  = new SimpleDateFormat("yyyy-MM"); // Just the year, with 2 digits
String formattedDate = dateformat .format(Calendar.getInstance().getTime());
DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM");
Date d=(Date)dateFormat.parse(dos);
Date curent=(Date)dateFormat.parse(formattedDate);
Calendar c1 = Calendar.getInstance(); 
c1.setTime(d);
dos = (dateFormat.format(d)).toString();
 while(!(d.after(curent)))
{ num=c1.get(c1.MONTH);
if (num >= 0 && num <= 11 ) {
    month = months[num];
}
%>
<tr>
<td></td>
<td><span><a href="productDayReport.jsp?prdid=<%=temp[0]%>&dt=<%=dos%>"><%=month%></a></span></td>
<td><%=opengbal %></td>
<%
	 inwards=GDSTK_L.getgodwanstockbymonth(dos,temp[0]);
	 outwards=SHPSTK_L.getgShopstockbymonth(dos,temp[0]);
	 if(inwards!=null){
		 opengbal=opengbal+Integer.parseInt(inwards);
/* 		 System.out.println(opengbal); */
		 } else{
			 inwards="0";
	 }
	 if(outwards!=null){
		 opengbal=opengbal-Integer.parseInt(outwards);
		 } else{
			 outwards="0";
	 }
	
	    c1.setTime(d);
	    
			c1.add(c1.MONTH, +1);
			closinbal=opengbal;
%>


<td><%=inwards %>pcs</td>
<td><%=outwards %>pcs</td>
<td><%=closinbal %></td>
</tr>
<%
dos = (dateFormat.format(c1.getTime())).toString();
	d=(Date)dateFormat.parse(dos);
} %>
<tr><td colspan="5" align="right"><h>Total Stock :</h></td><td><%=closinbal %></td></tr>
</table>
<%} }else{%>
<span>No Reports</span>
<%} }%>
</div>
</div>
</div>
</div>
<!-- main content -->
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
