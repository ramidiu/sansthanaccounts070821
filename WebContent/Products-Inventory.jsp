<%@page import="beans.inventory_products_locations"%>
<%@page import="mainClasses.inventory_products_locationsListing"%>
<%@page import="beans.locations"%>
<%@page import="mainClasses.locationsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.poojasaamanListing"%>
<%@page import="mainClasses.headsgroupListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>

<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<head>
<title>LOCATIONS CREATE</title>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="js/jquery.blockUI.js"></script>

<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script type="text/javascript">
$(function() {
	$("#warranty" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		
		});
$("#extra2" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		
		});
	});
	
</script>
<script>
function isNumberKey(evt)
{ // Numbers only
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
<script>
	function calculate()
	{
		
		var qnt=$("#qty").val();
		var rate=$("#rate").val();
		//var depreciation_rate=$("#depreciation_rate").val();
		var amt=qnt*rate;
		$("#amount_of_product").val(amt);
	}
</script>
<script>
function enableAmount()
{
	var status=document.getElementById("status").value;
	if(status=="Damaged"){
		$("#dmgAmt").show();
	}else{
		$("#dmgAmt").hide();
	}
	/* if(!(status=='Working-Condition'))
	{
		$("#dmgAmt").hide;
		document.getElementById("status_id2").innerHTML="<input type='text' name='damaged_amount' id='damaged_amount' value=''>";
	}
	else
	{
		document.getElementById("status_id1").innerHTML="";
		document.getElementById("status_id2").innerHTML="";
	} */
}
	//$("#status").change(function(){alert("2222");document.getElementById("status_id").innerHTML="<input type='text' name='sss'>";});
</script>
<script>
function formBlock(){
	$.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } }); 
}
function combochange(denom,desti,jsppage) {
	var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value; 
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	$.post(jsppage,{ id: com_id } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");

				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
				sda1.add(y,null);
				}
				catch(e)
				{
				sda1.add(y);
				}
			}
			}
		});
	} 
function productsearch(){
	  var hoid=$('#HOA').val();
	  var data1=$('#product').val();
	  var arr = [];
	  $.post('searchProducts.jsp',{q:data1,hoa : hoid},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);			
			}
		}); 
	});
} 
</script>
</head>
<body onload="combochange('HOA','location','getLocation.jsp')" >
<%
if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="LOC" class="beans.inventory_products_locations"/>
<div class="vendor-page">
<div class="vendor-box">

<div class="vender-details">
	<%headofaccountsListing HOA_L=new headofaccountsListing();
		List HA_Lists=HOA_L.getheadofaccounts();
		majorheadListing MajorHead_list=new majorheadListing();
		subheadListing SUBL=new subheadListing();
		productsListing PROL=new productsListing();
	%>
	<table width="70%" border="0" cellspacing="0" cellpadding="0">
		<tr>
						<td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST POOJA STORES</td>
					</tr>
					<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="4" align="center" style="font-weight: bold;">Prodcut's Locations Create</td></tr>
	</table>
	<hr width="96%">

	<form name="tablets_Update" method="post" action="inventory_products_locations_Insert.jsp" onsubmit="formBlock();">
		<table>
			<tr><td colspan="4"></td></tr>
			<tr>
				<td>Head of account </td>
				<td><select name="HOA" id="HOA" onChange="combochange('HOA','location','getLocation.jsp')">
					<%if(session.getAttribute("headAccountId").toString().equals("5")){ %>
						<option value="5" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("5")){ %> selected="selected" <%} %>>SHRI SAI NIVAS</option>
					<%}else{ %>
						<option value="3" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("3")){ %> selected="selected" <%} %>>POOJA STORES</option>
						<option value="4" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("4")){ %> selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("1")){ %> selected="selected" <%} %>>CHARITY</option>
					<%} %>
				</select>
				</td>
			</tr>
			<tr>	
				<td>Location</td>
				<td>
				<select name="location" id="location" required>
						<option value="">Select Location</option>
				</select>
				</td>
			</tr>
			<tr>
				<td>PRODUCT Name*</td>
				<td><input type="text" name="product" id="product" placeholder="Find a product here" required="required" onkeyup="productsearch();">
				</td>
			</tr>
			<tr>
				<td>Qty*</td>	
				<td><input type="text" name="qty" id="qty" value="1" placeholder="Enter Product Qty" OnKeypress="javascript:return isNumberKey(event);" onChange="calculate();"></td>
			</tr>
			<tr>
				<td>Rate*</td>	
				<td><input type="text" name="rate" id="rate" value="" placeholder="Enter Rate of Single product"OnKeypress="javascript:return isNumberKey(event);" onChange="calculate();" autocomplete="off"></td>
			</tr>
			
			<tr>
				<td>Amount*</td>	
				<td><input type="text" name="amount" id="amount_of_product" readonly></td>
			</tr>
			<tr>
				<td>Depreciation(% yearly)*</td>	
				<td><input type="text" name="depreciation" id="depreciation_rate" value="" OnKeypress="javascript:return isNumberKey(event);" autocomplete="off"></td>
			</tr>
			<tr>
				<td>Depreciation(Upto How Many year)*</td>	
				<td><input type="text" name="extra4" id="extra4" value="" OnKeypress="javascript:return isNumberKey(event);" autocomplete="off"></td>
			</tr>
			<tr>
				<td>Purchase Date*</td>	
				<td><input type="text" name="extra2" id="extra2" value="" placeholder="purchase date" autocomplete="off"></td>
			</tr>
			<tr>
				<td>Warranty Upto*</td>	
				<td><input type="text" name="warranty" id="warranty" value="" placeholder="warranty upto" autocomplete="off"></td>
			</tr>
			<tr>
				<td>Status*</td>	
				<td>
					<select name="status" id="status" onchange="enableAmount()">
						<option>Working-Condition</option>
						<option>Damaged</option>
						<option>Need To Repair</option>
						<option>Not in Use</option>
						<option>Partially Damaged</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><p id="status_id1"></p></td>	
				<td><span id="status_id2"></span></td>
			</tr>
			
		 <tr id="dmgAmt" style="display: none;" ><td>Damaged Amount*</td>	
				
				<td><input type="text" name="damaged_amount" id="damaged_amount" value=""  onkeyup="javascript:this.value=this.value.toUpperCase();"></td>
			</tr>	
			<tr>
				<td>Remark*</td>
				<td><textarea rows="4" cols="50" name="narration" value="" placeholder="Write your narration" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea></td>
				<td colspan="3"></td>
			</tr>
	
			<tr>
				<td colspan="4" align="right"><input type="submit" value="Create" class="click" style="border:none;"/></td>
			</tr>
		</table>
	</form>
</div>


<!-- DOWN PART OF PAGE -->
<div style="clear:both;"></div>
</div>
<div class="vendor-list">
<div class="arrow-down"><img src="images/Arrow-down.png"/><b>PRODUCTS LOCATIONS LIST</b></div>

<div class="clear"></div>
<div class="list-details">
<%
locationsListing LOCL=new locationsListing();
inventory_products_locationsListing INVPL=new inventory_products_locationsListing();
List GROPList=INVPL.getProductLocationSBasedOnHoa(session.getAttribute("headAccountId").toString());
if(GROPList.size()>0){%>


<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" >S.No.</td>
<td class="bg" >HEAD ACCOUNT</td>
<td class="bg" >PRODUCT-ID/NAME</td>
<td class="bg" >LOCATION-ID/LOCATION-NAME </td>
<td class="bg" > </td>
<td class="bg" >NARRATION</td>

<!-- <td class="bg" width="20%">Edit</td>
<td class="bg" width="10%">Delete</td> -->
</tr>
<%for(int i=0; i < GROPList.size(); i++ ){
	LOC=(inventory_products_locations)GROPList.get(i); %>
<tr>
<td><%=i+1 %></td>
<td><%=HOA_L.getHeadofAccountName(LOC.gethead_account_id()) %></td>
<td><span><%=LOC.getproduct_id() %> / <%=PROL.getProductsNameByCat1(LOC.getproduct_id(), "") %> </span></td>
<td><%=LOC.getlocation_id() %> / <%=LOCL.getLocationsName(LOC.getlocation_id()) %></td>
<td><span><%=LOC.getquantity() %></span></td>
<td><%=LOC.getnarration()%></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no products locations found</h1></div>
<%}%>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>