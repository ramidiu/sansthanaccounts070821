<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Opening Balance Product Form</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<style>
.list-details table td.bg{
padding:15px 15px 15px 15px;
}
</style>
</head>
<body>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
		<div class="vendor-box">
				<table width="95%" cellpadding="0" cellspacing="0" >
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Opening Balance Product Form</td></tr>
				</table>
					<div class="vender-details">
							<form>
								<table width="50%" border="0" cellspacing="0" cellpadding="0" style="margin:auto;">
								<tr>
								<td>Product Name</td>
								<td>Opening Balance</td>
								<td></td>
								</tr>
								<tr>
									<td><input type="text" name="product name"/></td>
									<td><input type="text" name="opening balance"/></td>
									<td><input type="submit" name="submit" value="submit"/></td>
								</tr>
								</table>
							</form>
					</div>
		</div>
		<div class="list-details">
		<table width="95%" cellpadding="0" cellspacing="0" style="margin:auto;" >
				<tr>
					<td class="bg">S.No.</td>
					<td class="bg">Product Name</td>
					<td class="bg">Balance in Gowdan</td>
				</tr>
				<tr>
					<td>1</td>
					<td>Rice</td>
					<td>2500</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Moond Dhal</td>
					<td>1500</td>
				</tr>
		</table>
		</div>
</div>

</body>
</html>