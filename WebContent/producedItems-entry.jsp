<%@page import="beans.shopstock"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock Transfer</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	if($('#product0').val().trim()==""){
		$('#product0').addClass('borderred');
		$('#product0').focus();
		return false;
	}
	if($('#quantity0').val().trim()==""){
		$('#quantity0').addClass('borderred');
		$('#quantity0').focus();
		return false;
	}
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var hoid=$('#headAccountId').val();
	  var arr = [];
	  $.post('searchProducts.jsp',{q:data1,hoa : hoid},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[4],
			        vat:d[5],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				event.preventDefault();
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				event.preventDefault();
				$(this).val(ui.item.label);
				$('#purchaseRate'+j).val(ui.item.amount);
				$('#vat'+j).val(ui.item.vat);
				
			}
		}); 
			});
} 
function calculate(i){
	var price=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;

	u=document.getElementById("addcount").value;

	if(document.getElementById("product"+i).value!=""){

		if(document.getElementById("purchaseRate"+i).value!="")
		{
		
			for(var j=0;j<=u;j++){
			
				if(document.getElementById("purchaseRate"+j).value!=""){

				grossprice=Number(parseFloat(document.getElementById("purchaseRate"+j).value)+Number(parseFloat(document.getElementById("purchaseRate"+j).value)*(Number(parseFloat(document.getElementById("vat"+j).value)/100))));

			document.getElementById("grossAmt"+j).value=grossprice;
		
			
				}
				}

		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;

}
function addOption(){
	$("#purpose").empty();
	var hoaid=$("#headAccountId").val();
	if(hoaid=="3"){
		$("#purpose").append('<option value=godown>Shop sales</option>');
	}else if(hoaid=="4" || hoaid=="1"){
		$("#purpose").append('');
		$("#purpose").append('<option value=santhanstores>For annadanam</option>');
		$("#purpose").append('<option value=santhanstores>For staff launch</option>');
		$("#purpose").append('<option value=santhanstores>Other</option>');
	}
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="STC" class="beans.shopstock"/>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<form name="tablets_Update" method="post" action="produceditemsInsert.jsp" onsubmit="return validate();">
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Produced Items entry</div>
<div class="vender-details">
  

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<%mainClasses.employeesListing EMP_l=new mainClasses.employeesListing();
String EMP_HOA=EMP_l.getMemployeesHOA(session.getAttribute("empId").toString());
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();  %>
<td width="35%"><div class="warning" id="dateError" style="display: none;">Please select date.</div>Date*</td>
<td>Head Of account</td>
<td>Transfer Id</td>
<td>Products Entry type</td>
</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<tr>
<td><input type="text" name="date" id="date" value="<%=currentDate%>" /></td>
<td><select name="headAccountId" id="headAccountId" style="width:200px;" onchange="addOption();">
<%if(EMP_HOA.equals("4")){ %>
<option value="1">CHARITY</option>
<option value="4">SANSTHAN</option><%}else{ %>
<option value="<%=EMP_HOA%>"><%=HOA_CL.getHeadofAccountName(EMP_HOA)%></option>
<%} %>
</select></td>
<td>
<select name="transfer_id" id="transfer_id">
<%shopstockListing ST_TRL=new shopstockListing();
List TRAL=ST_TRL.getStockTransferIDS(EMP_HOA); 
if(TRAL.size()>0){
	for(int i=0;i<TRAL.size();i++){
		STC=(shopstock)TRAL.get(i);
%>
<option value="<%=STC.getextra3()%>"><%=STC.getextra3()%></option>
<%}} %>
</select>
</td>
<td>
<select name="purpose" id="purpose">
<%if(EMP_HOA.equals("3")){ %>
<option value="shop sales">Shop sales</option>
<%}else if(EMP_HOA.equals("4")){  %>
	<option value="produced">Produced</option>
<!-- 	<option value="sale">Sale</option>
	<option value="return">Return</option>
	<option value="wasteage">Wasteage</option> -->
<%} %>
</select>
</td>
</tr>
<tr>
<td>Department*</td>
<td>Description</td>
</tr>
<tr>
<td>
<select name="department" id="department">
<%if(EMP_HOA.equals("3")){ %>
<option value="shop">Shop</option>
<option value="godown">Godown</option>
<%}else if(EMP_HOA.equals("4")){  %>
	<option value="Salescounter">Sales Counter</option>
<%} %>
</select></td>
<td><textarea  name="narration" id="narration" style="height: 25px;"></textarea></td>
<td colspan="2"></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>


<div class="vendor-list">

<div class="yourID"><div class="sno1 bgcolr">S.No.</div>
<div class="ven-nme1 bgcolr">Product Name</div>
<div class="ven-nme1 bgcolr">Description</div>
<!-- <div class="ven-nme1 bgcolr">Sell Price</div>
<div class="ven-amt1 bgcolr">Vat</div> -->
<div class="ven-amt1 bgcolr">Qty</div></div>
<div class="clear"></div>
<!-- <div class="ven-nme1 bgcolr">Sell Price(VAT)</div> -->
<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 20; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

<div class="sno1"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1">
<select name="product<%=i%>" id="product<%=i%>">
<option value="">-Product-</option>
<option value="PULIHORA">PULIHORA</option>
<option value="LADDU">LADDU</option>
<option value="KESARI PRASADAM">KESARI PRASADAM</option>
<option value="BELLAM PONGALI">BELLAM PONGALI</option>
<option value="TEA">TEA</option>
<option value="BREAKFAST">BREAKFAST</option>
<option value="CHEKARA PONGALI">CHEKARA PONGALI</option>
<option value="ANNADANAM">ANNADANAM</option>
<option value="SEJ HARATHI">SEJ HARATHI</option>
<option value="SAI PRASADAM">SAI PRASADAM</option>
<option value="THEERTHAM">THEERTHAM</option>

</select>
</div>
<div class="ven-nme1"><input type="text" name="description<%=i%>" id="description<%=i%>"  value=""></input></div>
<%-- <div class="ven-nme1">&#8377;<input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" value="" readonly="readonly"></input></div>
<div class="ven-amt1"><input type="text" name="vat<%=i%>" id="vat<%=i%>"  readonly="readonly" value="" style="width:50px;"/></div> --%>
<div class="ven-amt1"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  onblur="calculate('<%=i %>')" value="" style="width:50px;"></input></div>
<%-- <div class="ven-nme1">&#8377;<input type="text" name="grossAmt<%=i%>" id="grossAmt<%=i%>" readonly="readonly" style="width:80px;"></input></div> --%>
<div class="clear"></div>
</div>
<%} %>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/><input type="reset" name="button" value="RESET"  class="click"/>
<input type="submit" value="Submit" class="click" style="border:none; float:right; margin:0 500px 0 0"/>
</div>

</div>
</div>
</form>
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>