<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.diagnosticissuesListing"%>
<%@page import="java.util.List"%>
<%@page import="beans.diagnosticissues"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>

</head>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( ".printable" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>

<body>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-list">	
				<div class="search-list">
				
				<%
				   SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
				   
				   Calendar calendar=Calendar.getInstance();
				   String toDayDate=dateFormat.format(calendar.getTime());
				   
				   
				%> <form action="reportfordiagnostic.jsp">
			   
					<ul>
					   
						<li><label>From Date:&nbsp;</label><input type="text" name="fromDate" id="fromDate" class="DatePicker" value="<%=toDayDate%>"></li>
						<li><label>To Date:&nbsp;</label><input type="text" name="toDate" id="toDate" class="DatePicker" value="<%=toDayDate%>"></li> 
					     <li><input type="submit" class="click" ></li>
					  
					</ul>
					</form>
				</div>
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span>
					
				</div>
				<div class="clear"></div>
				<div class="list-details" style="border-bottom:none !important;">
				<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tbody><tr>
						<%
						   String reportFromDate="";
						   String reportToDate="";
						   if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){
							   reportFromDate=request.getParameter("fromDate");
						   }
						   if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
							   reportToDate=request.getParameter("toDate");
						   }
						   
						   if((reportFromDate!=null && !reportFromDate.equals("")) && (reportToDate!=null && !reportToDate.equals(""))){
						%>
						
  <td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</td>
</tr>
<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<tr><td colspan="9" align="center" style="font-weight: bold;">Report From <%=reportFromDate%> To <%=reportToDate%></td></tr>
<%}%>
						<tr><td colspan="9"><table width="100%" cellpadding="0" cellspacing="0" id="tblExport" class="yourID">
<tbody><tr>
</tr>
</tbody>
</table>
						<div class="printable">
			<%
			   String fromDate="";
			   String toDate="";
			   if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){
				    fromDate=request.getParameter("fromDate");
				    fromDate=fromDate+" 00:00:00";
			   }
			    if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
			    	toDate=request.getParameter("toDate");
			    	toDate=toDate+" 23:59:59";
			    }
			   
			    			
			 %>			
				 <%
				    if((fromDate!=null && !fromDate.equals("") && (toDate!=null && !toDate.equals("")))){
			        diagnosticissuesListing diagonsticIssuesListing=new diagnosticissuesListing();
			        List<diagnosticissues>   diagonsticList=diagonsticIssuesListing.getDiagnosticTestIssuesBasedOnDates(fromDate, toDate);
			         if(diagonsticList.size()>0 &&  diagonsticList!=null){
			        	 double trustAmount=0.0;
			        	 double patientPaidAmount=0.0;
			        %>				
		       <table width="95%" cellpadding="1" cellspacing="1" class="print_table" style="margin: 30px auto;" border="1">
			   <tbody><tr>
				     <td class="bg" width="13%" style="font-weight: bold;">Document Number</td>
				     <td class="bg" width="10%" style="font-weight: bold;">Appointment id</td>
				     <td class="bg" width="10%" style="font-weight: bold;">Patient Name</td>
				     <td class="bg" width="10%" style="font-weight: bold;">Doctor Name</td>
				     <td class="bg" width="10%" style="font-weight: bold;">Test Name</td>
				     <td class="bg" width="10%" style="font-weight: bold;">Discount Price</td>
				     <td class="bg" width="18%" style="font-weight: bold;">Patient paid amount</td>
			      </tr>
			          <% for(int i=0;i<diagonsticList.size();i++){ 
			        	 diagnosticissues diagnostic=diagonsticList.get(i);
			        	 
			        	 double amount=Double.parseDouble(diagnostic.getAmount());
			        	 double percentage=Double.parseDouble(diagnostic.getExtra2());
			        	 double discountAmount=((amount)-(amount*percentage/100));
			        	 trustAmount=trustAmount+discountAmount;
			        	 patientPaidAmount=patientPaidAmount+Double.parseDouble(diagnostic.getExtra3());
			        %> 
			       <tr>
			         <td width="13%"><%=diagnostic.getDocumentNo()%></td>
			         <td width="10%"><%=diagnostic.getAppointmnetId()%></td>
			         <td width="10%"><%=diagnostic.getPatientName()%></td>
			         <td width="10%"><%=diagnostic.getDoctorName()%></td>
			         <td width="20%"><%=diagnostic.getTestName()%></td>
			         <td width="10%"><%=discountAmount%></td>
			         <td width="10%"><%=diagnostic.getExtra3()%></td>
			      </tr> 
			      <%} %>
			<tr>
				<td colspan="5" height="28" bgcolor="#996600" style="font-weight:bold; color:#fff"><strong>Total(Rupees)</strong></td>
				<td style="font-weight: bold;color: #934C1E;">Rs.<%=trustAmount%></td>
				<td style="font-weight: bold;color: #934C1E;">Rs.<%=patientPaidAmount%></td>
			</tr>
		       </tbody></table> 
		
				<%
			        }else{
			        	%>
			        	 <center><span style="color:red;font-family: verdana;font-size: 250%;"><strong>No Records Are available</strong></span></center>
			        	<% 
			        }
				    }
			        %>     
			</div>
			</div>
</body>
</html>