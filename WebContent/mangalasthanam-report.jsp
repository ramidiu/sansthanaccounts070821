<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Managala Snanam Report</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
</head>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                    $( "a" ).css("text-decoration","none");
                    $( ".printable" ).print();
                   

                    // Cancel click event.
                    return( false );
                });
			});
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
});
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<body>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-list">
		<div class="arrow-down">
				<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form name="searchForm" id="searchForm" action="nityaarchanaReport.jsp" method="post">
				<div class="search-list">
					<ul>
						<li><input type="text" name="productId" id="productId" value="" onkeyup="productsearch()" placeholder="Find a seva here" autocomplete="off"></li>
						
						
						<li><input type="text" name="fromDate" id="fromDate" class="DatePicker " value="2019-08-13" readonly=""></li>
						 
					<li><input type="submit" class="click" name="search" value="Search"></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span>
					
				</div>
				<div class="clear"></div>
				<div class="list-details" style="border-bottom:none !important;">
				<table width="100%" cellpadding="10" cellspacing="10">
						 <tbody><tr><td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;"> (Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;">Mangala Snanam Report</td></tr>
					
				</tbody></table>
				</div>
				
				
			</div>
			</div>
</body>
</html>