<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="DCT" class="beans.doctordetailsService">
<%
String doctorId=request.getParameter("doctorId");
String doctorName=request.getParameter("doctorName");
String doctorQualification=request.getParameter("doctorQualification");
String phoneNo=request.getParameter("phoneNo");
String address=request.getParameter("address");
String extra1=request.getParameter("extra1");
String specialistIn=request.getParameter("specialist");
String consultingDays="";
String toDay="";
if(request.getParameter("consToDay")!=null && !request.getParameter("consToDay").equals("")){
	toDay=" TO "+request.getParameter("consToDay");
}
consultingDays=request.getParameter("consFromDay")+toDay;
 
String Timings=request.getParameter("consFromTime")+" "+request.getParameter("con1")+" TO "+request.getParameter("consToTime")+" "+request.getParameter("con1");
%>
<jsp:setProperty name="DCT" property="doctorId" value="<%=doctorId%>"/>
<jsp:setProperty name="DCT" property="doctorName" value="<%=doctorName%>"/>
<jsp:setProperty name="DCT" property="doctorQualification" value="<%=doctorQualification%>"/>
<jsp:setProperty name="DCT" property="phoneNo" value="<%=phoneNo%>"/>
<jsp:setProperty name="DCT" property="address" value="<%=address%>"/>
<jsp:setProperty name="DCT" property="extra1" value="<%=extra1%>"/>
<jsp:setProperty name="DCT" property="extra3" value="<%=specialistIn%>"/>
<jsp:setProperty name="DCT" property="extra4" value="<%=consultingDays%>"/>
<jsp:setProperty name="DCT" property="extra5" value="<%=Timings%>"/>
<%=DCT.update()%><%=DCT.geterror()%>
</jsp:useBean>
<%
response.sendRedirect("doctordetails_Form.jsp");
%>