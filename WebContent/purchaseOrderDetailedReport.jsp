<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.indentapprovals"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.util.List" %>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<jsp:useBean id="PO" class="beans.purchaseorder"/>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
<jsp:useBean id="VND" class="beans.vendors"/>

<html>
<body>
<div><%@ include file="title-bar.jsp"%></div>
	
	<%String INVId=request.getParameter("poinvid");
	String Producttype="";
	mainClasses.purchaseorderListing PUR_L = new mainClasses.purchaseorderListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	mainClasses.employeesListing EMP_L = new employeesListing();
	String stkEntered = "Not Entered or Not yet Received",stkApproved="";
	productsListing PRD_L = new productsListing();
	vendorsListing VND_L = new mainClasses.vendorsListing();
	employeesListing empl = new employeesListing();
	SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat CDF=new SimpleDateFormat("dd MMMM yyyy HH:mm");
	List PUR_List=PUR_L.getMpurchaseorderBasedONPoINVID(INVId);
	godwanstockListing GSL_LST =new godwanstockListing();
	List<godwanstock> glist = GSL_LST.getBillsBasedOnPurchaseOrderId(INVId); 
	if (glist.size() > 0)	{
		stkEntered = empl.getMemployeesName(glist.get(0).getemp_id());
		String status = glist.get(0).getprofcharges();
		if (status.equals("unchecked"))	{
			stkApproved = "Stock Not Approved";
		}
		else	{
			stkApproved = empl.getMemployeesName(glist.get(0).getCheckedby())+" Time:["+glist.get(0).getCheckedtime()+"]";
		}
	}
	if(PUR_List.size()>0){
		PO=(beans.purchaseorder)PUR_List.get(0);
		List VNDT_LIST=VND_L.getMvendors(PO.getvendor_id());
		VND=(vendors)VNDT_LIST.get(0);
		Producttype=PO.gethead_account_id();
		%>
<div class="vendor-page">
<div class="vendor-list">
<div class="name-title"><%=PO.getvendor_id()%> <span>(<%=HOA_CL.getHeadofAccountName(PO.gethead_account_id())%>)</span></div>
<div style="clear:both;"></div>
<div class="details-list">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="1"><span>Vendor Name :</span></td><td><%=VND.gettitle()%> <%=VND.getfirstName()%> <%=VND.getlastName()%></td>
</tr>
<tr>
<td colspan="1"><span>Vendor Address :</span></td><td><%=VND.getaddress()%>,<%=VND.getcity()%>,<%=VND.getpincode()%></td>
</tr>
<tr>
<td colspan="1"><span>Vendor Phone No :</span></td><td><%=VND.getphone()%></td>
</tr>
<tr>
<td colspan="1"><span>Vendor Email Id :</span></td><td><%=VND.getemail()%></td>
</tr>
<tr>
 <%-- <td colspan="2"><span>Indent Raised By:</span><%=EMP_L.getMemployeesName(INDT.getemp_id())%></td> --%>
<td colspan="1"></td>
</tr>
<tr>
<td colspan="1"><span>PO Raised Date :</span></td><td><%=CDF.format(SDF.parse(PO.getcreated_date()))%></td>
</tr>
</table>
</div>
<span style="font-weight: bold;">Stock Entered By:</span>
<span><%=stkEntered %></span><br>
<span style="font-weight: bold;">Stock Approved By:</span>
<span><%=stkApproved %></span>
</div>
<%mainClasses.indentapprovalsListing APPR_L=new mainClasses.indentapprovalsListing();
List APP_Det=APPR_L.getIndentDetails(INVId);
%>
<%-- <div class="vendor-list"> 
<div class="trans">Purchase order details</div>
<div class="clear"></div>
 <div class="arrow-down"><img src="images/Arrow-down.png"></div>
<form method="post" action="indentStatusUpdate.jsp">
<div class="search-list">
<ul>
<li>
<input type="hidden" name="indentid" value="<%=INVId%>"/>
<select name="reportStatus">
<option value="">Report Status</option>
<%if(!PO.getstatus().equals("indentSubmitToVendor")){ %>
<option value="indentSubmitToVendor">Indent sent to vendor</option><%} %>
<option value="itemsReceived">Indent Items Received</option>
</select></li>
<li><input type="submit" name="submit" value="Update"></li>
</ul>
</div>
</form> --%>
<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%"><input type="checkbox"></td>
<td class="bg" width="20%">Vendor Name</td>
<td class="bg" width="8%">Product Name</td>
<td class="bg" width="8%">Product price</td>
<td class="bg" width="20%">Quantity Raised</td>
<td class="bg" width="20%">Received Quantity</td>
<td class="bg" width="20%">Balance Quantity</td>
</tr>
<%/* banktransactionsListing BAK_L=new mainClasses.banktransactionsListing();  */
employeesListing EMP=new employeesListing();
PUR_List=PUR_L.getMpurchaseorderBasedONPoINVID(INVId); 
if(PUR_List.size()>0){
	for(int i=0;i<PUR_List.size();i++){
		PO=(beans.purchaseorder)PUR_List.get(i);
%>
<tr>
<td><input type="checkbox"></td>
<td><%=PO.getvendor_id()%> [<%=VND_L.getMvendorsAgenciesName(PO.getvendor_id()) %>]</td>
<td><%=PO.getproduct_id()%> <%if(!PRD_L.getProductsNameByCat(PO.getproduct_id(),Producttype).equals("")) {%><%=PRD_L.getProductsNameByCat(PO.getproduct_id(),Producttype)%><%} else { %><%=PRD_L.getProductsNameByCat1(PO.getproduct_id(),Producttype)%><%} %></td>
 <td><%=PO.getextra1() %></td>
<td><%=PO.getquantity() %></td>
<td><%if(GSL_LST.getTotalReceivedQuantity(PO.getproduct_id(), INVId)!=null && !GSL_LST.getTotalReceivedQuantity(PO.getproduct_id(), INVId).equals("")){%><%=GSL_LST.getTotalReceivedQuantity(PO.getproduct_id(), INVId) %><%}else{ %>0<%} %></td>
<%-- <%if(Producttype.equals("1")){ %>
<td></td>
<%} else{ if(GSL_LST.getPurchStockStauts(PO.getpoinv_id())){%>
<td><%=(Integer.parseInt(PO.getquantity())-Integer.parseInt(GSL_LST.getPurchStockByInv(PO.getpoinv_id()))) %></td>
<%} else{ %><td>Not Received</td>
<%}}%> --%>
<%int recvQty=0;
if(GSL_LST.getTotalReceivedQuantity(PO.getproduct_id(), INVId)!=null && !GSL_LST.getTotalReceivedQuantity(PO.getproduct_id(), INVId).equals("")){
	recvQty=Integer.parseInt(GSL_LST.getTotalReceivedQuantity(PO.getproduct_id(), INVId));
}
%>
<td><%=Integer.parseInt(PO.getquantity())-recvQty %></td>
</tr><%}} %>
</table>
</div>
</div>
</div>
<%}%>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>	
