<%@page import="mainClasses.productsListing"%>
<%@page import="beans.stockrequest"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="beans.employees"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock Transfer</title>
<jsp:useBean id="EMPLY" class="beans.employees"></jsp:useBean>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	 var quantity ='';
	var addcount=Number($('#addcount').val());
	var i=0;
	
	if($('#product0').val().trim()==""){
		$('#product0').addClass('borderred');
		$('#product0').focus();
		return false;
	}
	var quantvalid="valid";
	 for (i =0 ; i <addcount ; i++)
	{
		 quantity = $('#quantity'+i).val();
	quan=quantity;
	if(quan != "" && isNaN(quan))
		{
		alert("Enter the valid Number");
		quantvalid="notvalid";
		return false;
		}
	
	}
	 if(quantvalid=="notvalid"){
	return false;
	}  
	/*  if($('#quantity0').val().trim()==""){
		$('#quantity0').addClass('borderred');
		$('#quantity0').focus();
		return false;
	}  */
	
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var hoid=$('#headAccountId').val();
	  var role=$('#role').val();
	  var arr = [];
	  $.post('searchProducts.jsp',{q:data1,hoa : hoid,role:role},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var qntystore=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[4],
			        vat:d[5],
			        qntystore :d[6],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#purchaseRate'+j).val(ui.item.amount);
				$('#vat'+j).val(ui.item.vat);
				$(this).val(ui.item.label);
				$('#purchaseRate'+j).val(Math.round(ui.item.amount));
				$('#vat'+j).val(ui.item.vat);
				$('#storequantity'+j).val(ui.item.qntystore);
				$('#godQty'+j).text("BalQty:"+ui.item.qntystore);				
				 if(ui.item.qntystore>0){
				$(this).val(ui.item.label);
				$('#purchaseRate'+j).val(Math.round(ui.item.amount));
				$('#vat'+j).val(ui.item.vat);
				} else{
				alert("There is no stock for this product.");
				$('#product'+j).val('');
				
				}
			}
		}); 
			});
} 
function calculate(i){
	var price=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;
	var ishopqty=0;
	var iqty=0;
	ishopqty=document.getElementById("storequantity"+i).value;
	iqty=document.getElementById("quantity"+i).value;
	if(Number(iqty)>Number(ishopqty)){
	document.getElementById("quantity"+i).value=Number(ishopqty).toFixed(0);
	}
	u=document.getElementById("addcount").value;
	if(document.getElementById("product"+i).value!=""){
		if(document.getElementById("purchaseRate"+i).value!="")
		{
		
			for(var j=0;j<=u;j++){
			
				if(document.getElementById("purchaseRate"+j).value!=""){

				grossprice=Number(parseFloat(document.getElementById("purchaseRate"+j).value)+Number(parseFloat(document.getElementById("purchaseRate"+j).value)*(Number(parseFloat(document.getElementById("vat"+j).value)/100))));

			document.getElementById("grossAmt"+j).value=grossprice;
		
			
				}
				}

		}
		}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;
}
function addOption(){
	$("#purpose").empty();
	var hoaid=$("#headAccountId").val();
	if(hoaid=="3"){
		$("#purpose").append('<option value=godown>Shop sales</option>');
	}else if(hoaid=="4" || hoaid=="1" ){
		$("#purpose").append('');
		$("#purpose").append('<option value=SAIANNADANAM>SAIANNADANAM</option>');
		$("#purpose").append('<option value=SAI PRASAD>SAI PRASAD</option>');
		$("#purpose").append('<option value=MAINTENANCE>MAINTENANCE</option>');
		$("#purpose").append('<option value=TEA,BREAKFAST>TEA,BREAKFAST</option>');
		$("#purpose").append('<option value=STATIONARY&ELECTRICAL>STATIONARY&ELECTRICAL</option>');
		$("#purpose").append('<option value=SAIPOOJA ITEMS>SAIPOOJA ITEMS</option>');
		$("#purpose").append('<option value=OTHER>OTHER PURPOSE</option>');
	}else if(hoaid=="5"){
		$("#purpose").append('<option value=Sainivas Store>Sainivas Store</option>');
	}
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="REQ" class="beans.stockrequest"></jsp:useBean>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<form name="tablets_Update" method="get" action="shopStockTransferInsert.jsp" onsubmit="return validate();">
<div class="vendor-page">

<div class="vendor-box">
<!-- <div class="vendor-title">Stock Transfer</div> -->
<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="3" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;font-size: 10px;"> (Regd.No.646/92)</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;">STOCK ISSUE FORM</td></tr>
<tr>

<%mainClasses.employeesListing EMP_l=new mainClasses.employeesListing();
String role="";
List EMPL=EMP_l.getMemployees(session.getAttribute("empId").toString());
if(EMPL.size()>0){
	EMPLY=(employees)EMPL.get(0);
}
String EMP_HOA=EMPLY.getheadAccountId();
List RQDET=null;
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
stockrequestListing SRQL=new stockrequestListing();
List HOA_List=HOA_CL.getheadofaccounts();  %>
<td width="35%"><div class="warning" id="dateError" style="display: none;">Please select date.</div>Date*</td>
<td>Head Of account</td>
<td>Purpose</td>
</tr>

 <%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
if(EMP_HOA.equals("5")){
	if(EMPLY.getextra1().equals("Sainivas StoreKeeper")){
	role="OnlyStoreKeeper";
	} else if(EMPLY.getextra1().equals("SainivasRestaurant")){
		role="OnlyRestaurant";
	}
}
%>

<tr>
<td>

<%--  this is for to get HeadAccountName 
<input type="hidden" name="headActName" id="headActName" value="<%=REQ.gethead_account_id()%>"/>  --%>

<input type="hidden" name="role" id="role" value="<%=role %>" />
<input type="hidden" name="req_id" id="req_id" value="<%=request.getParameter("reqId") %>" />
<input type="text" name="date" value="<%=currentDate%>"  readonly/></td>
<%RQDET=SRQL.getRequestDetailsBasedOnID(request.getParameter("reqId"));
if(RQDET.size()>0){
for(int i=0; i < RQDET.size(); i++ ){
REQ=(stockrequest)RQDET.get(i);
}
}%>

<td><select name="headAccountId" id="headAccountId" style="width:200px;" onchange="addOption();">
<%System.out.println("reqId...."+request.getParameter("reqId"));
if(request.getParameter("reqId")!=null && !request.getParameter("reqId").equals("")){ %>
	<option value="<%=REQ.gethead_account_id()%>">  <%=HOA_CL.getHeadofAccountName(REQ.gethead_account_id()) %>  </option><%} %>
<%if(REQ.gethead_account_id().equals("1")){ %>
<option value="4">SANSTHAN</option><%}else if(EMP_HOA.equals("4")){ %>
<option value="1">CHARITY</option> <%}%><%-- else{ %>
<option value="<%=EMP_HOA%>"><%=HOA_CL.getHeadofAccountName(EMP_HOA)%></option>
<%} %> --%>
</select></td>
<td>
<select name="purpose" id="purpose">
<%if(request.getParameter("reqId")!=null && !request.getParameter("reqId").equals("")){ %>
	<option value="<%=REQ.getreq_type()%>"> <%=REQ.getreq_type()%> </option><%} %>
<%if(EMP_HOA.equals("3")){ %>
<option value="shop sales">SHOP SALES</option>
<%}else if(EMP_HOA.equals("4") || EMP_HOA.equals("1")){  %>
		<option value="TEA,COFFE">TEA,COFFE</option>
		<option value="BREAKFAST">BREAKFAST</option>
		<option value="MILK">MILK</option>
		<option value="SAIANNADANAM">SAI ANNADANAM</option>
		<option value="PRASADAM DISTRIBUTION">PRASADAM DISTRIBUTION</option>
		<option value="PRASADAM SALE">SAI PRASADAM SALE</option>
		<option value="SAIPOOJA ITEMS">SAIPOOJA ITEMS</option>
		<option value="OTHER">OTHER PURPOSE</option>
<%}else if(EMP_HOA.equals("5")){ 
if(role.equals("OnlyStoreKeeper")){%>
	<option value="SainivasRestaurant">Sainivas Store</option>
<% }  else if(role.equals("OnlyRestaurant")){%> 
<option value="SainivasRes Store">Sainivas Restaurant Store</option>
<%}	} %>
</select>
</td>
</tr>
<tr>
<td>Department*</td>
<td>Description</td>
</tr>
<tr>
<td>
<select name="department" id="department">
<%if(EMP_HOA.equals("3")){ %>
<option value="shop">Shop</option>
<!-- <option value="godown">Godown</option> -->
<%}else if(EMP_HOA.equals("4")){  %>
	<option value="Salescounter">Sales Counter</option>
<%} else if(EMP_HOA.equals("5")){ if(role.equals("OnlyStoreKeeper")){%>
<option value="SainivasStore">Sainivas Store</option>
<% }  else if(role.equals("OnlyRestaurant")){%> 
<option value="Restaurant">Restaurant</option>
<%}}%>
</select></td>
<td><textarea  name="narration" id="narration" style="height: 25px;"></textarea></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>
<!-- 
   <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();
    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script> -->

<div class="vendor-list">

<div class="yourID"><div class="sno1 bgcolr">S.No.</div>
<div class="ven-nme1 bgcolr">Product Name</div>
<!-- <div class="ven-nme1 bgcolr">Description</div> -->
<!-- <div class="ven-nme1 bgcolr">Sell Price</div>
<div class="ven-amt1 bgcolr">Vat</div> -->
<div class="ven-amt1 bgcolr">Qty</div></div>
<div class="clear"></div>
<!-- <div class="ven-nme1 bgcolr">Sell Price(VAT)</div> -->
<input type="hidden" name="addcount" id="addcount" value="<%=RQDET.size()%>"/>
<%

productsListing PROL=new productsListing();
if(request.getParameter("reqId")!=null && !request.getParameter("reqId").equals("")){
   RQDET=SRQL.getRequestDetailsBasedOnID(request.getParameter("reqId"));
	if(RQDET.size()>0){
	for(int i=0; i < RQDET.size(); i++ ){
	REQ=(stockrequest)RQDET.get(i);
	%>
	<div <%if(i>RQDET.size()){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">	
		<div class="sno1"><%=i+1%></div>
		<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
		<div class="ven-nme1"><input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>')" class="" value="<%=REQ.getproduct_id()%> <%=PROL.getProductsNameByCat1(REQ.getproduct_id(),"")%>"/></div>
		<div class="ven-amt1">
		<input type="hidden" name="storequantity<%=i%>" id="storequantity<%=i%>" value="<%=REQ.getextra1() %>" />
		<input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  onkeyup="calculate('<%=i %>')" value="<%=REQ.getquantity() %>" style="width:50px;"></input><span id="godQty<%=i%>" style="color: red;"></span></div>
		<div class="clear"></div>
	</div>
<%}}}else{ %>
<%for(int i=0; i < 100; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

<div class="sno1"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1"><input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>')" class="" value=""/></div>
<%-- <div class="ven-nme1"><input type="text" name="description<%=i%>" id="description<%=i%>"  value=""></input></div> --%>
<%-- <div class="ven-nme1">&#8377;<input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" value="" readonly="readonly"></input></div>
<div class="ven-amt1"><input type="text" name="vat<%=i%>" id="vat<%=i%>"  readonly="readonly" value="" style="width:50px;"/></div> --%>
<div class="ven-amt1">
<input type="hidden" name="storequantity<%=i%>" id="storequantity<%=i%>" value="1" />
<input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  onkeyup="calculate('<%=i %>')" value="" style="width:50px;"></input><span id="godQty<%=i%>" style="color: red;"></span></div>
<%-- <div class="ven-nme1">&#8377;<input type="text" name="grossAmt<%=i%>" id="grossAmt<%=i%>" readonly="readonly" style="width:80px;"></input></div> --%>
<div class="clear"></div>
</div>
<%}} %>
<div><input type="text" name="recvName" value="" placeholder="Enter receiver name * " required></input><input type="text" name="recvDesgn" value="" placeholder="Enter receiver designation *" required></input></div>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/><input type="reset" name="button" value="RESET"  class="click"/>
<input type="submit" value="Transfer" class="click" style="border:none; float:right; margin:0 500px 0 0"/>
</div>

</div>
</div>
</form>
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>