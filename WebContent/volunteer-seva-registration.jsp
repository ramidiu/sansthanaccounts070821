<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@page import="beans.UniqueIdGroup"%>
<%@page import="mainClasses.uniqueIdGroupsListing"%>
<%@page import="java.util.List"%>
<!--  <script src="../js/jquery-1.8.2.js"></script> -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <script src="ui/jquery.ui.core.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="ui/jquery.ui.datepicker.js"></script>
<!-- <link href="../js/date.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/> -->
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery-1.4.2.js"></script>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
  
<Script >
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</Script>

<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
});
</script>

<script language = "JavaScript">

//---For check all checkboxes----

function checkAll() {
var select_All = document.getElementById("SelectAll");
var inputs = document.getElementsByTagName("input");
for (var i = 0; i < inputs.length; i++)
{
if (inputs[i].type == "checkbox" && inputs[i].name == "select1") {
if(select_All.checked==true)
inputs[i].checked = true;
else
inputs[i].checked = false;
}
}
}
</script>

<script type="text/javascript">
function check1(){
	 /*  var obj1 = document.getElementById('description');
	 if(obj1.value =='') 
     {      
       alert("Please Enter description!");
       document.getElementById('description').focus();
       return false;       
     }
      var date1=document.getElementById('date');
      
     if(date1.value =='') 
     {      
       alert("Please Enter Date!");
       document.getElementById('date').focus();
       return false;       
     } */
     
     var inputs1 = document.getElementsByTagName("input");
 	var c=0;
     for (var i = 0; i < inputs1.length; i++)
 	{
 	if (inputs1[i].type == "checkbox" && inputs1[i].name == "select1"){
 		if(inputs1[i].checked == true)
 			c++;
 			}}
 	if(c<1){
 	alert("please check atleast one member");
 	return false;
 	}
     
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "preferred_seva"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one seva");
	return false;
	} else{
	document.getElementById("volunteer-seva").action="volunteer_seva_insert.jsp";
	document.getElementById("volunteer-seva").submit();
	}
}

</script>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>

</head>
<body>
<%if(session.getAttribute("empId")!=null){ 
%>
<div><%@ include file="title-bar.jsp"%></div>
<form name="volunteer-seva" id="volunteer-seva" method="POST">
<!-- <table style="margin : 0px auto">
<tr>
	 <td>Description</td>
	 <td>Select Date: </td> 
</tr>
<tr>
	 <td><textarea name="description" id="description" value="" placeHolder="Enter Description" /></textarea></td> 
	 <td><input type="text" style="width: 105px;margin-left: 10px;" name="date" id="date" value="" placeHolder="Select Date" readonly="readonly"></td> 
</tr>
</table> -->
<jsp:useBean id="UI" class="beans.UniqueIdGroup"/>
<jsp:useBean id="SSSST" class="beans.sssst_registrations"/>


<table width="980" border="0" cellspacing="0" cellpadding="0" style="margin : 0px auto;" class="print_table">

<tr style="background-color: #e3eaf3;color: #7490ac;">   
         <th>S.No</th>
         	<th ><input type="checkbox" name="SelectAll" id="selecctall"/>Check All </th>
         	<th >Volunteer Name </th>
         	<th>SSSST ID </th>
          </tr>

<%
	mainClasses.uniqueIdGroupsListing UIGL = new mainClasses.uniqueIdGroupsListing();
	mainClasses.sssst_registrationsListing SSSSTL = new mainClasses.sssst_registrationsListing();

	List UIG_List=UIGL.getUniqueIdGroupsOfVolunteer();
	
	for(int i=0; i < UIG_List.size(); i++ )
	{
		UI=(beans.UniqueIdGroup)UIG_List.get(i);
		List SSSST_List=SSSSTL.getMsssst_registrations(UI.getUniqueid().toString());
		for(int G=0; G < SSSST_List.size(); G++ ){
		SSSST=(beans.sssst_registrations)SSSST_List.get(G);%>
		
		<tr>
<td align="center" height=30><%= i+1 %> </td>
<td align="center"><input type="checkbox" name="select1" class="checkbox1" value = "<%=SSSST.getsssst_id()%>" ></td>
		<td align="center"><%=SSSST.getfirst_name()+" "+SSSST.getmiddle_name()+" "+SSSST.getlast_name()%></td>
		<td align="center"><%=SSSST.getsssst_id() %></td>
		
		<% }}
%>
<tr>
<td colspan="8" style="border:1px solid #000; padding:10px; margin-top:10px;
margin-bottom:10px;">

	<label>
      <input type="checkbox" name="preferred_seva" value="Q.MAINTENANCE"/>Q.MAINTENANCE &nbsp;&nbsp;&nbsp;
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="PRASADAM PACKING"/>PRASADAM PACKING &nbsp;&nbsp;&nbsp;
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="ANNADANAM (DAILY)"/>ANNADANAM (DAILY) &nbsp;&nbsp;&nbsp;
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="ANNADANAM (Thursday)"/>ANNADANAM (Thursday) &nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="HUNDI COLLECTION"/>HUNDI COLLECTION &nbsp;&nbsp;&nbsp;
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="MEDICAL SERVICES"/>MEDICAL SERVICES &nbsp;&nbsp;&nbsp;
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="STOTRA PARAYANAM"/>STOTRA PARAYANAM &nbsp;&nbsp;&nbsp;
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="PALLIQUEN PROCESSION"/>PALLIQUEN PROCESSION &nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="IMPORTANT EVENTS"/>IMPORTANT EVENTS 
     </label>
     <label>
      <input type="checkbox" name="preferred_seva" value="FESTIVALS"/>FESTIVALS &nbsp;&nbsp;&nbsp;
     </label>
</td>
</tr>     
	<tr>
	<td colspan="7" align="center"><input type="submit" name="create" value="CREATE" class="click" style="margin-top: 10px;" onclick="return check1();" /></td>
	</tr>
     
</table>
<%} %>
</form>

<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>