<%-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="beans.customerpurchases"%>
<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.List"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>digitalwall</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
<style>
        @import url('https://fonts.googleapis.com/css?family=Muli:300,400,400i,700,900');

        body {
            padding: 0 0;
            margin: 0 0;
            background-color: #fff;
            font-size: 100%;
            color: #505458;
            height: 100%;
            width: 100%;
            overflow-x: hidden;
            font-family: 'Muli', sans-serif;
            height: 100vh;
            overflow-x: hidden;
            overflow-y: hidden;
        }

        * {
            box-sizing: border-box;
        }

        a,
        a:hover {
            text-decoration: none;
        }

        ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p {
            margin: 5px 0;
            line-height: 1.3;
        }

        .text-case {
            text-transform: uppercase
        }

        .main-wrapper {
            padding: 0 20px;
        }

        .marquee {
            background-image: url('https://signage-digitalwall.sgp1.digitaloceanspaces.com/5c2716dc5141e41b1f9678c9/cc8fa5cd-c78a-4288-aedc-d5a49181cbfd.jpg');
            background-size: 100% 100%;
            height: 100vh;
            width: 100%;
            background-repeat: no-repeat;
        }

        .content-block {
            box-shadow: 0 7px 30px 0 rgba(0, 0, 0, 0.55);
        }

        .header {
            margin-top: 50px;
            text-align: center;
            padding: 15px 15px;
            background-color: rgba(31, 25, 25, 0.88);
            border-radius: 5px 5px 0 0;
        }

        .header .title {
            font-size: 17px;
            color: #ffffff;
            text-shadow: 0 0 3px rgba(0, 0, 0, 0.36);
        }

        .row-strip {
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 15px 15px;
            background-color: rgba(238, 241, 243, 0.88);
            border: 1px solid rgb(204, 204, 204);
            border-top: 0;
        }

        .main-wrapper .row-strip:nth-of-type(odd) {
            background-color: rgba(255, 255, 255, 0.88);
        }

        .row-strip p {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 90%;
        }

        .row-strip p,
        .row-strip .number {
            font-size: 20px;
            font-weight: 600;
            color: rgb(36, 39, 57);
        }

        .row-strip .number {
            font-weight: 800;
        }
    </style>
</head>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<jsp:useBean id="CAP" class="beans.customerapplication" />
	<jsp:useBean id="VEN" class="beans.vendors" />
	<jsp:useBean id="SALE" class="beans.customerpurchases" />
	<jsp:useBean id="SA" class="beans.customerpurchases" />
	<jsp:useBean id="BNK" class="beans.banktransactions" />
	<marquee loop="infinite" behavior="scroll" direction="up" scrollamount="5" class="marquee">
	
	<div class="list-details ">
	<div class="container">
	
 	
 	<div class="marque-in">
		<table width="100%" cellpadding="1" cellspacing="1"
			class="print_table">
			<tr>
				 <!-- <td class="bg" width="3%" style="font-weight: bold;">S.NO.</td> -->
				<!--<td class="bg" width="7%" style="font-weight: bold;">APP REF.NO</td>
				<td class="bg" width="7%" style="font-weight: bold;">CREATED
					DATE</td>
				<td class="bg" width="7%" style="font-weight: bold;">RECEIPT.NO</td> -->
				<!-- <td class="bg" width="20%" style="font-weight: bold;">SEVA NAME</td> -->
				<td class="bg" width="15%" style="font-weight: bold;display:none">SEVA
					PERFORM IN THE NAME OF</td>
				<!-- <td class="bg" width="10%" style="font-weight: bold;">GOTHRAM</td>
				<td class="bg" width="8%" style="font-weight: bold;">PERFORMED
					DATE(Month-date)</td>
				<td class="bg" width="17%" style="font-weight: bold;">NARRATION</td> -->
				<td class="bg" width="10%" style="font-weight: bold;display:none;">AMOUNT</td>
			</tr>
		</table>
		<table width="100%" cellpadding="0" cellspacing="0"
			class="print_table">
			<tbody>
				<tr>
					<td colspan="9" style="padding: 0px; margin: 0px;" class="yourID">
					</td>
				</tr>
				<!-- <tr>
                      <td class="bg" colspan="9" align="center" style="font-size: 16px;color: red;font-weight: bold;">NITYA ANNADANA PADHAKAM FOR THE DAY OF 01-Jan-2019 to 02-Jan-2019</td>
                     </tr> -->
			</tbody>
		</table>

		<%
		        Calendar  calendar=Calendar.getInstance();
				String fromdate=new SimpleDateFormat("MM-dd").format(calendar.getTime());
				String toDate=new SimpleDateFormat("MM-dd").format(calendar.getTime())+" 23:59:59";
				String fromDateForWeekOrMonth = "";
				String toDateForWeekOrMonth = "";
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dateFormat3= new SimpleDateFormat("dd-MMM-yyyy");
				fromDateForWeekOrMonth = dateFormat2.format(calendar.getTime());
				toDateForWeekOrMonth = dateFormat2.format(calendar.getTime());
				String fromDate2=(dateFormat2.format(calendar.getTime()));
				String currentDate=(dateFormat2.format(calendar.getTime()));
				String toDate2=currentDate+" 23:59:59";
				customerpurchasesListing SALE_L = new customerpurchasesListing();
				customerapplicationListing CAPP_L = new customerapplicationListing();
				banktransactionsListing BKTRAL=new banktransactionsListing();
				productsListing PRDL=new productsListing();
				employeesListing EMPL=new employeesListing();
				
				List BANK_DEP=null;
				double grandtotal =0.00;
				
		
				String productCode[]=new String[3];
				if(request.getParameter("productId")!=null && !request.getParameter("productId").equals(""))
				{
					productCode=request.getParameter("productId").split(" ");
				}
				String hoaID="4";
				customerapplicationListing CUSAL=new customerapplicationListing();
				List SAL_list=null;
				List CUS_DETAIL=null;
				DecimalFormat df = new DecimalFormat("0.00");
				
	    String poojaIds[];
	if(request.getParameter("productId")!=null && !request.getParameter("productId").equals("")){
		productCode=request.getParameter("productId").split(" ");
		poojaIds=new String[]{productCode[0]};
	}else{
		poojaIds=new String[]{"20063","20092","20091"};
	}
	
	if(poojaIds.length>0){
		for(int s=0;s<poojaIds.length;s++){
			
			if(poojaIds[s].equals("20092") || poojaIds[s].equals("20063")){
				if(poojaIds[s].equals("20092"))
				{
					SAL_list=CUSAL.getMcustomerDetailsBasedOnBillingId("RCT237482");
					List newList = CUSAL.GetDetailsCustomerPerformedDate(fromdate,toDate,poojaIds[s]);
					SAL_list.addAll(newList);
					
				}	
				else{
					SAL_list=CUSAL.GetDetailsCustomerPerformedDate(fromdate,toDate,poojaIds[s]);
				}
				
				
				
				
				if(SAL_list.size()>0){%>
		<table width="100%" cellpadding="0" cellspacing="0"
			class="print_table" style="margin-bottom:50px;">
			<tr>
				<%if(fromDateForWeekOrMonth != "" && toDateForWeekOrMonth != "") { %>
				<td class="bg" colspan="9" align="center"
					><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%>
					 DONARS FOR THE TODAY <%=dateFormat3.format(dateFormat2.parse(fromDateForWeekOrMonth))%>
					to <%=dateFormat3.format(dateFormat2.parse(toDateForWeekOrMonth))%></td>
				<%}else{ %>
				<td class="bg" colspan="9" align="center"
					><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%>
					DONARS FOR THE TODAY  <%=dateFormat3.format(dateFormat2.parse(request.getParameter("fromDate")))%>
					to <%=dateFormat3.format(dateFormat2.parse(request.getParameter("toDate")))%></td>
				<%} %>
				<!-- <td class="bg" colspan="9" align="center" style="font-size: 16px;color: red;font-weight: bold;">NITYA ANNADANA PADHAKAM FOR THE DAY OF 01-Jan-2019 to 02-Jan-2019</td> -->
			</tr>
			<%
				
					double totalamount=0.00;
					for(int i=0; i < SAL_list.size(); i++ )
						{ 
						 CAP=(customerapplication)SAL_list.get(i); 
						 CUS_DETAIL=SALE_L.getBillDetails(CAP.getbillingId());
							if(CUS_DETAIL.size()>0)
							{
								SALE=(customerpurchases)CUS_DETAIL.get(0);}%>
			<tr>
				<td colspan="9">
					<table class="table " width="100%" cellpadding="1" cellspacing="1"
						class="print_table">
						<tr style="position: relative;">
							<td width="3%"><%=i+1%></td>
							<td width="7%"><%=CAP.getphone()%></td>
							<%if(CAP.getbillingId().startsWith("RCT2")) {%>
							<td width="7%"><%=SALE.getdate()%></td>
							<%}else{ %>
							<td width="7%"><%=dateFormat3.format(dateFormat2.parse(SALE.getdate()))%></td>
							<%} %>
							<td width="7%"><%=CAP.getbillingId()%></td>
							<td  width="20%"><%=CAP.getimage()%>-<%=PRDL.getProductsNameByCats(CAP.getimage(),session.getAttribute("headAccountId").toString())%></td>
							<td width="15%"><%=CAP.getlast_name() %></td> 
							
							<td width="10%"><%=CAP.getgothram()%></td>
							<td width="8%"><%=CAP.getextra1()%></td>
							<td width="17%"><%=SALE.getextra2()%></td>
							<td width="10%" style="font-weight: 800 !important;text-align: right;font-size: 22px;">Rs.<%=SALE.gettotalAmmount()%></td>
						</tr>
					</table>
				</td>
			</tr>
			<%totalamount = totalamount + (Double.parseDouble(SALE.gettotalAmmount())); %>
			<%} %>
			<tr style="text-align: center; font-weight: bold;">
				<td colspan="7"><b>Total Amount</b></td>
				<td>Rs.<%=totalamount %></td>
			</tr>
			<%grandtotal = grandtotal + totalamount; %>
		</table>


		<%		
					
				
			}
				
		}
			
			else if(poojaIds[s].equals("20091"))
			{
				List ADDTOSAL_list2 = null;
				List SAL_list2=SALE_L.GetDetailsCustomerBasedOnDates(fromDate2,toDate2,poojaIds[s]);
				ADDTOSAL_list2=SALE_L.getOnlineBookingsBasedOnDates(fromDate2,toDate2,poojaIds[s]); //added by srinivas
				if(ADDTOSAL_list2 != null && ADDTOSAL_list2.size() > 0)
				{
					SAL_list2.addAll(ADDTOSAL_list2);
				}
				//System.out.println("array"+poojaIds[s]);
				//System.out.println("11111");
			
				if(SAL_list2.size()>0){%>

		<table width="100%" cellpadding="0" cellspacing="0" id="tblExport"
			class="print_table" style="margin-bottom:50px;">

			<tr>
				<%if(fromDateForWeekOrMonth != "" && toDateForWeekOrMonth != "") { %>
				<td class="bg" colspan="9" align="center"
					style="font-size: 16px; color: red; font-weight: bold;"><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%>
					DONARS FOR THE TODAY  <%=dateFormat3.format(dateFormat2.parse(fromDateForWeekOrMonth))%>
					to <%=dateFormat3.format(dateFormat2.parse(toDateForWeekOrMonth))%></td>
				<%}else{ %>
				<td class="bg" colspan="9" align="center"
					style="font-size: 16px; color: red; font-weight: bold;"><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%>
					DONARS FOR THE TODAY  <%=dateFormat3.format(dateFormat2.parse(request.getParameter("fromDate")))%>
					to <%=dateFormat3.format(dateFormat2.parse(request.getParameter("toDate")))%></td>
				<%} %>
			</tr>
			<%double totalamount=0.00;
								for(int i=0; i < SAL_list2.size(); i++ )
									{ 
									SALE=(customerpurchases)SAL_list2.get(i); 
									 List CUS_DETAIL2=SALE_L.getBillDetails(SALE.getbillingId());
										if(CUS_DETAIL2.size()>0)
										{
											SALE=(customerpurchases)CUS_DETAIL2.get(0);}%>

			<tr>
				<td colspan="9">
					<table class="table " width="100%" cellpadding="1" cellspacing="1"
						class="print_table">
						<tr style="position: relative;">
							<td width="3%"><%=i+1%></td>
							<td width="7%">&nbsp;</td>
							<td width="7%"><%=dateFormat3.format(dateFormat2.parse(SALE.getdate()))%></td>
							<td width="7%"><%=SALE.getbillingId()%></td>
							<td width="15%"><%=SALE.getcustomername()%></td>
							<td  width="20%"><%=CAP.getimage()%>-<%=PRDL.getProductsNameByCats(CAP.getimage(),session.getAttribute("headAccountId").toString())%></td>
							<%
												if(SALE.getcash_type().equals("online success"))
												{
													String perfName = "";
													
													List capp = CAPP_L.getMcustomerDetailsBasedOnBillingId(SALE.getbillingId());
													if(capp.size() > 0)
													{
														customerapplication cappObj = (customerapplication)capp.get(0);
														perfName = cappObj.getlast_name();
													}
													%>
							<td width="45%"><%=perfName%></td>
							<% 
												}
												else{
											%>
							<td width="15%"><%=SALE.getextra2() %></td>
							<%} %>
							<td width="10%"><%=SALE.getgothram()%></td>
							<%
												if(!SALE.getextra3().equals("") && !SALE.getextra3().equals("null"))
												{
											%>
							<td width="8%"><%=dateFormat3.format(dateFormat2.parse(SALE.getextra3()))%></td>
							<%}else{ %>
							<td  width="8%"  ><%=dateFormat3.format(dateFormat2.parse(SALE.getextra3()))%></td>
							<td width="14%">&nbsp;</td>
							<%} %>
							<td width="17%"><%=SALE.getextra2()%></td>
							<td width="10%"  style="font-weight: 800 !important;text-align: right;font-size: 22px;">Rs.<%=SALE.gettotalAmmount()%></td>
						</tr>
					</table>
				</td>
			</tr>
			<%totalamount = totalamount + (Double.parseDouble(SALE.gettotalAmmount())); %>
			<%} %>
			<tr style="text-align: center; font-weight: bold;">
				<td colspan="7"><b>Total Amount</b></td>
				<td>Rs.<%=totalamount %></td>
			</tr>
			<%grandtotal = grandtotal + totalamount; %>
		</table>

		<%}
			}
	} %>
		<table width="100%" cellpadding="0" cellspacing="0">

			<tr style="text-align: center; font-weight: bold;">
				<td colspan="7"><b>Grand Total Amount</b></td>
				<td>Rs.<%=grandtotal %></td>
			</tr>
		</table>
		<%}else{%>
		<div align="center">
			<div align="center"
				style="padding-top: 50px; font: bold; font-size: x-large;">
				<span>There were no list found for today!</span>
			</div>
		</div>
		<%}%>
		</div>
		</div>
	</div>
		</marquee>
	
	

	</body>
	<%} %> 
</html> --%>

<!DOCTYPE html>
<html>
<%@page import="beans.customerpurchases"%>
<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.List"%>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Saisansthan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Muli:300,400,400i,700,900');

        body {
            padding: 0 0;
            margin: 0 0;
            background-color: #fff;
            font-size: 100%;
            color: #505458;
            height: 100%;
            width: 100%;
            overflow-x: hidden;
            font-family: 'Muli', sans-serif;
            height: 100vh;
            overflow-x: hidden;
            overflow-y: hidden;
        }

        * {
            box-sizing: border-box;
        }

        a,
        a:hover {
            text-decoration: none;
        }

        ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p {
            margin: 5px 0;
            line-height: 1.3;
        }

        .text-case {
            text-transform: uppercase
        }

        .main-wrapper {
            padding: 0 20px;
        }

        .marquee {
            background-image: url('https://signage-digitalwall.sgp1.digitaloceanspaces.com/5c2716dc5141e41b1f9678c9/cc8fa5cd-c78a-4288-aedc-d5a49181cbfd.jpg');
            background-size: 100% 100%;
            height: 100vh;
            width: 100%;
            background-repeat: no-repeat;
        }

        .content-block {
            box-shadow: 0 7px 30px 0 rgba(0, 0, 0, 0.55);
        }

        .header {
            margin-top: 50px;
            text-align: center;
            padding: 15px 15px;
            background-color: rgba(31, 25, 25, 0.88);
            border-radius: 5px 5px 0 0;
        }

        .header .title {
            font-size: 17px;
            color: #ffffff;
            text-shadow: 0 0 3px rgba(0, 0, 0, 0.36);
        }

        .row-strip {
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 15px 15px;
            background-color: rgba(238, 241, 243, 0.88);
            border: 1px solid rgb(204, 204, 204);
            border-top: 0;
        }

        .main-wrapper .row-strip:nth-of-type(odd) {
            background-color: rgba(255, 255, 255, 0.88);
        }

        .row-strip p {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 90%;
        }

        .row-strip p,
        .row-strip .number {
            font-size: 20px;
            font-weight: 600;
            color: rgb(36, 39, 57);
        }

        .row-strip .number {
            font-weight: 800;
        }
    </style>
</head>

<body>
<jsp:useBean id="CAP" class="beans.customerapplication" />
	<jsp:useBean id="VEN" class="beans.vendors" />
	<jsp:useBean id="SALE" class="beans.customerpurchases" />
	<jsp:useBean id="SA" class="beans.customerpurchases" />
	<jsp:useBean id="BNK" class="beans.banktransactions" />
	  <marquee loop="infinite" behavior="scroll" direction="up" scrollamount="5" class="marquee">
        <div class="main-wrapper">
            <!-- table heading -->
            <div class="content-block">
               
	<%
		        Calendar  calendar=Calendar.getInstance();
				String fromdate=new SimpleDateFormat("MM-dd").format(calendar.getTime());
				String toDate=new SimpleDateFormat("MM-dd").format(calendar.getTime())+" 23:59:59";
				String fromDateForWeekOrMonth = "";
				String toDateForWeekOrMonth = "";
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dateFormat3= new SimpleDateFormat("dd-MMM-yyyy");
				fromDateForWeekOrMonth = dateFormat2.format(calendar.getTime());
				toDateForWeekOrMonth = dateFormat2.format(calendar.getTime());
				String fromDate2=(dateFormat2.format(calendar.getTime()));
				String currentDate=(dateFormat2.format(calendar.getTime()));
				String toDate2=currentDate+" 23:59:59";
				customerpurchasesListing SALE_L = new customerpurchasesListing();
				customerapplicationListing CAPP_L = new customerapplicationListing();
				banktransactionsListing BKTRAL=new banktransactionsListing();
				productsListing PRDL=new productsListing();
				employeesListing EMPL=new employeesListing();
				
				List BANK_DEP=null;
			
				
		
				String productCode[]=new String[3];
				if(request.getParameter("productId")!=null && !request.getParameter("productId").equals(""))
				{
					productCode=request.getParameter("productId").split(" ");
				}
				String hoaID="4";
				customerapplicationListing CUSAL=new customerapplicationListing();
				List SAL_list=null;
				List CUS_DETAIL=null;
				DecimalFormat df = new DecimalFormat("0.00");
				
	    String poojaIds[];
	if(request.getParameter("productId")!=null && !request.getParameter("productId").equals("")){
		productCode=request.getParameter("productId").split(" ");
		poojaIds=new String[]{productCode[0]};
	}else{
		poojaIds=new String[]{"20063","20092","20091"};
	}
	
	if(poojaIds.length>0){
		for(int s=0;s<poojaIds.length;s++){
			
			if(poojaIds[s].equals("20092") || poojaIds[s].equals("20063")){
				if(poojaIds[s].equals("20092"))
				{
					SAL_list=CUSAL.getMcustomerDetailsBasedOnBillingId("RCT237482");
					List newList = CUSAL.GetDetailsCustomerPerformedDate(fromdate,toDate,poojaIds[s]);
					SAL_list.addAll(newList);
					
				}	
				else{
					SAL_list=CUSAL.GetDetailsCustomerPerformedDate(fromdate,toDate,poojaIds[s]);
				}
				
				
				if(SAL_list.size()>0){
					
					if(fromDateForWeekOrMonth != "" && toDateForWeekOrMonth != "") {
				%>
 
                
                 <div class="header">
                    <h1 class="title text-case"><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%>
					 DONARS  FOR THE TODAY</h1>
				</div>
					 <%
					 
					 for(int i=0; i < SAL_list.size(); i++ )
						{ 
						 CAP=(customerapplication)SAL_list.get(i); 
						 CUS_DETAIL=SALE_L.getBillDetails(CAP.getbillingId());
							if(CUS_DETAIL.size()>0)
							{
								SALE=(customerpurchases)CUS_DETAIL.get(0);
							}
					%>
                
                <!-- single row -->
                 <div class="row-strip">
                    <p><%=SALE.getcustomername()%> </p>
                     <h3 class="number">Rs.<%=SALE.gettotalAmmount()%></h3>
                </div> 
                
                <%
						}//for end
					}//h1tag
        }//list close
				
				
				
			}//if
			
			else if(poojaIds[s].equals("20091"))
			{
				List ADDTOSAL_list2 = null;
				List SAL_list2=SALE_L.GetDetailsCustomerBasedOnDates(fromDate2,toDate2,poojaIds[s]);
				ADDTOSAL_list2=SALE_L.getOnlineBookingsBasedOnDates(fromDate2,toDate2,poojaIds[s]); //added by srinivas
				if(ADDTOSAL_list2 != null && ADDTOSAL_list2.size() > 0)
				{
					SAL_list2.addAll(ADDTOSAL_list2);
				}
				//System.out.println("array"+poojaIds[s]);
				//System.out.println("11111");
			
			    if(SAL_list2.size()>0){
			    	if(fromDateForWeekOrMonth != "" && toDateForWeekOrMonth != "") { 
			
                %>
                
                
                
                 
					<div class="header">
                    <h1 class="title text-case"><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%>
					 DONARS  FOR THE TODAY</h1>
				</div>
				
                <%
                  for(int i=0; i < SAL_list2.size(); i++ )
									{ 
									SALE=(customerpurchases)SAL_list2.get(i); 
									 List CUS_DETAIL2=SALE_L.getBillDetails(SALE.getbillingId());
										if(CUS_DETAIL2.size()>0)
										{
											SALE=(customerpurchases)CUS_DETAIL2.get(0);}%>
											
                 <div class="row-strip">
                     <p><%=SALE.getextra2() %></p>
                    <h3 class="number">Rs.<%=SALE.gettotalAmmount()%></h3>
                </div>
              <%   
              
											
										}%>
                
                <%
			    	}//h1 tag end
			    }//list end
			}//else if end
                %>
               
										
											
              <!--  <div class="row-strip">
                    <p>Koteswara Rao</p>
                    <h3 class="number">Rs.2,300</h3>
                </div>
                <div class="row-strip">
                    <p>Poturaju</p>
                    <h3 class="number">Rs.2,300</h3>
                </div>
                <div class="row-strip">
                    <p>DR.RAKESH REDDY & SMT.SRI ANUJA</p>
                    <h3 class="number">Rs.34,300</h3>
                </div>
            </div>

            <div class="content-block">
                <div class="header">
                    <h1 class="title text-case">NITYA ANNADANA PADHAKAM DONORS FOR THE TODAY
                    </h1>
                </div>
                <div class="row-strip">
                    <p>ANNAPURNA</p>
                    <h3 class="number">Rs.12,300</h3>
                </div>
                <div class="row-strip">
                    <p>BURUGU AMBADAS</p>
                    <h3 class="number">Rs.2,489</h3>
                </div>
            </div>

            <div class="content-block">
                <div class="header">
                    <h1 class="title text-case">LIFE TIME ARCHANA DONORS FOR THE TODAY</h1>
                </div>
                <div class="row-strip">
                    <p>ANNAPURNA Rao</p>
                    <h3 class="number">Rs.2,300</h3>
                </div>
                <div class="row-strip">
                    <p>ANANDA Rao</p>
                    <h3 class="number">Rs.2,322</h3>
                </div>
            </div>
            <div class="content-block">
                <div class="header">
                    <h1 class="title text-case">AYYAPPA IRUMUDI DONORS FOR THE TODAY</h1>
                </div>
                <div class="row-strip">
                    <p>ANNAPURNA Rao</p>
                    <h3 class="number">Rs.2,300</h3>
                </div>
                <div class="row-strip">
                    <p>Durga Rao</p>
                    <h3 class="number">Rs.2,787</h3>
                </div>
                <div class="row-strip">
                    <p>ANNAPURNA Rao</p>
                    <h3 class="number">Rs.2,300</h3>
                </div>
                <div class="row-strip">
                    <p>ANANDA Rao</p>
                    <h3 class="number">Rs.2,322</h3>
                </div>
            </div>
            <div class="content-block">

                <div class="header">
                    <h1 class="title text-case">MANGALSNANAM (WITH SAMAN) DONORS FOR THE TODAY</h1>
                </div>
                <div class="row-strip">
                    <p>ANNAPURNA Rao</p>
                    <h3 class="number">Rs.23,300</h3>
                </div>
                <div class="row-strip">
                    <p>Siva koteswara Rao</p>
                    <h3 class="number">Rs.2,300</h3>
                </div>
                <div class="row-strip">
                    <p>ANNAPURNA Rao</p>
                    <h3 class="number">Rs.23,300</h3>
                </div>
                <div class="row-strip">
                    <p>Siva koteswara Rao</p>
                    <h3 class="number">Rs.2,300</h3>
                </div>-->
                
            

      
        <%
				
			
			
			
			
			
			
			
		}//for
		
	}//if%>
	</div> 
	</div>
	
    </marquee>
</body>

</html>