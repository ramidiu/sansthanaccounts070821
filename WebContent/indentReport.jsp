<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Indent Raise</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#indentDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
});
	</script>
	
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	

<script type="text/javascript">

/* numbers only */
  $(document).ready(function () {
    $('input.numberinput').bind('keypress', function (e) {
        return (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) ? false : true;
    });
});

function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
//var d = new Date();
//if(d.getDate() >= 5 && d.getDate() <=20 )	{
	var count = 1;
	for (var i = 0 ; i < count ; i++)	{
		var vendor = $('#vendorId'+i).val();
		var product = $('#product'+i).val();
		var quantity = $('#quantity'+i).val();
		if ($.trim(vendor) === "")	{
			$('#vendorId'+i).focus();
			return false;
		}
		if ($.trim(product) === "")	{
			$('#product'+i).focus();
			return false;
		}
		if ($.trim(quantity) === "")	{
			$('#quantity'+i).focus();
			return false;
		}
//		count = $('#addcount').val();
	}
	$('#form1').attr("action","indent_Insert.jsp");
	$('#form1').attr("method","post");
	$('#form1').submit();
	event.preventDefault();
	$.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } }); 
//}
//else 	{
//		alert("Indent can't be raised");
//		return false;
//}
}
function vendorsearch(){
	 var data1=$('#vendorId'+j).val();
	  var headID=$('#headAccountId').val();
	  if($('#headAccountId').val().trim()==""){
			$('#billError').show();
			$('#headAccountId').focus();
			return false;
		}
	 
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $( '#vendorId'+j).autocomplete({source: availableTags}); 
			});
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function vendorsearch(j){
	
	 var data1=$('#vendorId'+j).val();
	  var headID=$('#headAccountId').val();
	  if($('#headAccountId').val().trim()==""){
			$('#billError').show();
			$('#headAccountId').focus();
			return false;
		}
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		
		availableTags=doctorNames;
		
		 $( '#vendorId'+j).autocomplete({source: availableTags}); 
	
			});
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var headID=$('#headAccountId').val();
	  if($('#headAccountId').val().trim()==""){
			$('#billError').show();
			$('#headAccountId').focus();
			return false;
		}
	  var arr = [];
	  $.post('searchProd.jsp',{q:data1,hId:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var amount=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#instock'+j).val(ui.item.amount);
				
			}
		}); 
			});
} 
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;

	u=document.getElementById("addcount").value;

	if(document.getElementById("product"+i).value!=""){

		if(document.getElementById("purchaseRate"+i).value!="")
		{
		
			for(var j=0;j<=u;j++){
			
				if(document.getElementById("purchaseRate"+j).value!=""){

				grossprice=Number(parseFloat(document.getElementById("purchaseRate"+j).value)*Number(parseFloat(document.getElementById("quantity"+j).value)));

			document.getElementById("grossAmt"+j).value=grossprice;
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice));
			
				}
				}
			document.getElementById("total").value=Totalprice;
		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;

}
$(document).ready(function(){
	$("#spcDate").hide();
	$("#department").change(function(){
	    var req=$("#department").val();
	    if(req==='specific Date'){
	    	$("#spcDate").show();
	    }else{
	    	$("#spcDate").hide();
	    }
	  });
	});
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="REQ" class="beans.stockrequest"/>
<jsp:useBean id="REQS" class="beans.stockrequestService"/>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<form name="tablets_Update" id="form1">
<div class="vendor-page">

<div class="vendor-box">

<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Create New Indent</td></tr>
<tr>
<td>Indent Invoice No</td>
<td><div class="warning" id="billError" style="display: none;">Please select head of account.</div>Head of account*</td>
<!-- <td ><div class="warning" id="vendorError" style="display: none;">Please select vendor.</div>Stock Requirement*</td> -->
<td >Date</td>
<td>Description</td>
</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
mainClasses.no_genaratorListing NG_l=new mainClasses.no_genaratorListing();
String indentinvoice_id="";

if(session.getAttribute("headAccountId").toString().equals("5")){
	indentinvoice_id=NG_l.getidWithoutUpdatingNoGen("sainivas_indent_invNum");
}else{
	indentinvoice_id=NG_l.getidWithoutUpdatingNoGen("indentinvoicenumber");
}
%>
<tr>
<td><input name="invoiceId" value="<%=indentinvoice_id%>" readonly="readonly"></input></td>
<td>
<%employeesListing EMP_l=new employeesListing();
String EMP_HOA=EMP_l.getMemployeesHOA(session.getAttribute("empId").toString());

mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getActiveHOA();
%>
<select name="headAccountId" id="headAccountId" style="width:200px;">
<%if(session.getAttribute("headAccountId").toString().equals("5")){ %>
<option value="5"  selected="selected" >SHRI SAI NIVAS</option>
<%}else if(session.getAttribute("Role").toString().equals("medicalhall")){ %>
<option value="1"  selected="selected">CHARITY</option>
<%}else{ %>
<%if(HOA_List.size()>0){
	for(int i=0;i<HOA_List.size();i++){
	HOA=(headofaccounts)HOA_List.get(i);%>	
	<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname()%></option>
<%
}
	}
} %>
</select>
</td>
<td><input type="text" name="indentDate" id="indentDate" value="<%=currentDate %>" readonly="readonly"></input></td>
<td><textarea  name="narration" id="narration" style="height: 25px;"></textarea></td>
</tr>
<tr>
<td></td>
<td></td>

</tr>
<tr>
<td>
<select name="requirement" id="department">
<option value="Standard">Standard</option>
<option value="Urgent">Urgent</option>
<option value="specific Date">Specific Date</option>
<option value="EmergencyApproval">Emergency Approval</option>
</select></td>
<td id="spcDate"><input type="text" name="date" id="date" placeholder="Select order specific date" value="" /></td>

</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>

<%String req_id[]=request.getParameterValues("req_id");

%>

<div class="vendor-list" style="width:100%">
<div class="sno bgcolr">S.No.</div>
<div class="ven-nme bgcolr">Vendor Name</div>
<div class="ven-nme bgcolr">Product Name</div>
<div class="ven-nme bgcolr">Required Qty</div>
<div class="ven-amt bgcolr">In Stock Qty</div>
<%
stockrequestListing REQL=new stockrequestListing();
productsListing PROL=new productsListing();
List REQDET=null;

if(req_id!=null){
	System.out.println("length::::::;"+req_id.length);
%>
	<input type="hidden" name="addcount" id="addcount" value="5"/>
	<%for(int j=0; j <req_id.length; j++ )
	{System.out.println("req_id[j]::::::;"+req_id[j]);
		REQDET=REQL.getMstockrequest(req_id[j]);
	    REQ=(beans.stockrequest)REQDET.get(0);
	    REQS.updateStatus(REQ.getreq_id(),"indentRaised");
	%>
		<div <%if(j>4){ %>style="display: none;"<%} %> id="addcolumn<%=j%>">
		<div class="sno"><%=j+1%></div>
		<input type="hidden" name="count" id="check<%=j%>"  value="<%=j%>"/> 
		<div class="ven-nme"><input type="text" name="vendorId<%=j%>" id="vendorId<%=j%>" placeholder="Enter vendor name" value="" onkeyup="vendorsearch('<%=j%>')" autocomplete="off"/></div>
		<div class="ven-nme"><input type="text" name="product<%=j%>" id="product<%=j%>" placeholder="Enter product name" onkeyup="productsearch('<%=j%>')" class="" value="<%=REQ.getproduct_id()%> <%=PROL.getProductsNameByCat(REQ.getproduct_id(), REQ.gethead_account_id())%>" autocomplete="off"/></div>
		<div class="ven-nme"><input type="text" name="quantity<%=j%>" id="quantity<%=j%>" placeholder="Enter quantity" onblur="calculate('<%=j %>')"  onKeyPress="return numbersonly(this, event,true);" value="<%=REQ.getquantity() %>" style="width:100px;"></input></div>
		<div class="ven-amt"><input type="text" name="instock<%=j%>" id="instock<%=j%>"  value="<%=PROL.getProductsStockByCat(REQ.getproduct_id(), REQ.gethead_account_id()) %>" style="width:100px;" readonly="readonly"></input></div>
		</div>
	<%} %>
<%}else{
	req_id=new String[0];
 }%>
	<input type="hidden" name="addcount" id="addcount" value="5"/>
	<%for(int i=req_id.length; i < 200; i++ ){%>
		<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

		<div class="sno"><%=i+1%></div>
		<input type="hidden" name="count" id="check<%=i%>"  value="<%=i%>"/> 
		<div class="ven-nme"><input type="text" name="vendorId<%=i%>" id="vendorId<%=i%>" placeholder="Enter vendor name" value="" onkeyup="vendorsearch('<%=i%>')" autocomplete="off" <%if(i==0){ %>  <%} %> /></div>
		<div class="ven-nme"><input type="text" name="product<%=i%>" id="product<%=i%>" placeholder="Enter product name" onkeyup="productsearch('<%=i%>')" class="" value="" autocomplete="off" <%if(i==0){ %>  <%} %>/></div>
		<div class="ven-nme"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>" placeholder="Enter quantity" onblur="calculate('<%=i %>')"  onKeyPress="return numbersonly(this, event,true);" value="" style="width:100px;" <%if(i==0){ %>  <%} %>></input></div>
		<div class="ven-amt"><input type="text" name="instock<%=i%>" id="instock<%=i%>"  value="" style="width:100px;" readonly="readonly"></input></div>
        
        <div class="clear"></div>
		</div>
<%} %>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/></div>
<!-- <div class="tot-ven">Total<input type="text" name="total" id="total" value="0.00" readonly="readonly" style="width:50px;"/></div> -->
<input type="button" value="Create" class="click" style="border:none; float:right; margin:0 145px 0 0" onclick="validate();"/>
</div>
</div>
</form>
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>