<%@ page import="java.util.List" errorPage="" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>	
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Head Of Account</div>
<div class="click floatright"><a href="./newHeadOfAccount.jsp" target="_blank" onclick="openMyModal('newHeadOfAccount.jsp'); return false;">New Head Of Account</a></div>
</div>

<div class="vendor-list">
<div class="search-list">
<ul>
<li><input type="search" placeholder="Search"/></li>
</ul>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="clear"></div>
<div class="list-details" id="tblExport">
<%mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();
if(HOA_List.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%"><input type="checkbox"></td>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Name</td>
<td class="bg" width="23%">Phone</td>
<td class="bg" width="20%">Address</td>
</tr>
<%for(int i=0; i < HOA_List.size(); i++ ){
	HOA=(beans.headofaccounts)HOA_List.get(i);%>
<tr>
<td><input type="checkbox"></td>
<td><%=HOA.gethead_account_id()%></td>
<td><ul>
<li><span><a href="adminPannel.jsp?page=headOfAccountDetails&id=<%=HOA.gethead_account_id()%>"><%=HOA.getname()%></a></span><br /></li>
</ul></td>
<td><%=HOA.getphone()%></td>
<td><%=HOA.getaddress()%></td>
</tr>
<%}%>
</table>
<%}else{%>
<div><h2>No Head Of Accounts Created</h2></div>
<%}%>


</div>
</div>




</div>
