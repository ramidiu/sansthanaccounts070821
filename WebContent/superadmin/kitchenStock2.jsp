<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sai sansthan accounts-Admin Pannel</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<script>
$(function() {
	
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
function submission(){
	
	var fromDate=$("#fromDate").val();
	var toDate=$("#toDate").val();
	if(fromDate==""){
		alert('Please select From Date');
		return false;
	}
	if(toDate==""){
		alert('Please select To Date');
		return false;
	}
	$("#fulltable").show();
	return true;
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>

<%if(session.getAttribute("superadminId")!=null){ %>
<jsp:useBean id="SHP" class="beans.products" />
<%

String inwards="00";
String outwards="00";

productsListing PRD_l=new productsListing ();
List PRODL=PRD_l.getPresentShopStock1();
godwanstockListing GDSTK_L=new godwanstockListing();
shopstockListing SHPSTK_L=new shopstockListing();
%>


<div><%@ include file="title-bar.jsp"%></div>
<div class="list-details" >
	<form name="kitchenStock2.jsp" method="post">
	<div class="search-list">
		
					<ul style="margin:80px 0px 220px 400px;">
						<li>
							<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  placeholder="From Date" readonly="readonly" />
						</li>
						<li>
							<input type="text" name="toDate" id="toDate"  class="DatePicker" value="" placeholder="To Date" readonly required/>
						</li>
						<li>
							<input type="submit" class="click" name="search" value="Search" onclick="return submission();"/>
						</li>
					</ul>
			
	</div></form>
<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
					<span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
		<%String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate"); %>		
	<div class="printable" id="fulltable">
	<table width="95%"  cellspacing="0" cellpadding="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
			<%if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("")){ %>
			<tbody> 
				<tr>
					<td colspan="3" align="center" style="font-weight: bold;color:red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
				</tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">KITCHEN STOCK REPORT</td></tr>
				 <tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=fromDate %> TO <%=toDate%> </td></tr> 
			</tbody>
			
		</table><br>
			<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border='1'>
				<%if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("")){ %>
				<tr>
					<!-- <td class="bg" width="5%" style="font-weight: bold;">SNO</td> -->
					<td class="bg" width="5%" style="font-weight: bold;">CODE</td>
					<td class="bg" width="20%" style="font-weight: bold;">PRODUCT NAME</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">OPENING BALANCE</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">RECEIVED QUANTITY</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">ISSUED QUANTITY</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">CLOSING BALANCE</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">UNITS</td>
				</tr>
				<%}
				
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date myDate = dateFormat.parse(fromDate);
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(myDate);
				cal1.add(Calendar.DAY_OF_YEAR, -1);
				Date previousDate = cal1.getTime();
				String previousDate1="";
				TimeZone tz = TimeZone.getTimeZone("IST");
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				previousDate1=dateFormat2.format(previousDate);
				//System.out.println("previous date..."+previousDate1);
				
				
				String closingBal="0";
				String openingbal="0";
				double StartingBal=0.0;
				double EndingBal=0.0;
				//String[] totalQuantity=new String[2050];;
				if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("") && toDate!=null && !toDate.equals("")){
				for(int i=0; i < PRODL.size(); i++ ){
					SHP=(beans.products)PRODL.get(i);
					//totalQuantity=new String[i]; getgodwanstockbyENDDate
					 inwards=GDSTK_L.getgodwanstockbyDate(fromDate,toDate,SHP.getproductId()); 
					 outwards=SHPSTK_L.getgShopstockbyDate(fromDate,toDate,SHP.getproductId());
					 openingbal=GDSTK_L.getgodwanstockbyDate("2014-04-01",previousDate1,SHP.getproductId()); 
					 closingBal=SHPSTK_L.getgShopstockbyENDDate("2014-04-01",previousDate1,SHP.getproductId());
					 if(openingbal!=null && !openingbal.equals("") && !openingbal.trim().equals("null")){
						 StartingBal=Double.parseDouble(openingbal);
					 }
					 else{
						 StartingBal=0.0;
					 }
					 if(closingBal!=null && !closingBal.equals("") && !closingBal.trim().equals("null")){
						 EndingBal=Double.parseDouble(closingBal);
					 }else{
						 EndingBal=0.0;
					 }
					 StartingBal=StartingBal-EndingBal;
					 %>
				<tr>
				    <%-- <td><%=i+1%></td> --%>
					<td><%=SHP.getproductId()%></td>
					<td><%=SHP.getproductName()%></td>
					
					<td><%=Math.round(StartingBal) %></td>
					<%if(inwards!=null && !inwards.equals("null") && !inwards.equals("")){ %>
					<td><%=inwards%></td>
					<%}else{ 
					inwards="0";
					%>
					<td>0</td>
					<%}if(outwards!=null && !outwards.equals("null") && !outwards.equals("")){ %>
					<td><%=outwards%></td>
					<%}else{
					outwards="0";	%>
					<td>0</td>
					<%} 
					
					 EndingBal=StartingBal+Double.parseDouble(inwards)-Double.parseDouble(outwards);
					%>
					<td><%=Math.round(EndingBal) %></td>
					<td><%=SHP.getunits()%></td>
					
				</tr>
				<%}}}else{ %>
				
				<%} %>
				
		</table>
	</div>
</div>
<%}else{
	 response.sendRedirect("index.jsp");
} %>
</body>
</html>