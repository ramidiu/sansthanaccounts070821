<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<%indentListing INDL=new indentListing();
purchaseorderListing POL=new purchaseorderListing();
godwanstockListing GOSL=new godwanstockListing();
List INDC=INDL.getindentGroupByIndentinvoice_id();
List POCOUL=POL.getpurchaseordersBasedOnVendor();
List MSEL=GOSL.getgodwanstockGroupByInvoice();
List recvdms=GOSL.getClosedPOSize();
%>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Total Stock Performance Report</div>
<div style="clear:both;"></div>

<div class="unpaid-box">
<div class="unpaid"><bold>TOTAL INDENTS RAISED</bold>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOTAL PURCHASE ORDERS </div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount">&#8377;<%=INDC.size()%></li>
 <li><%=INDC.size()%> INDENTS RAISED BY PURCHASE ORDER</li>
</ul>
</div>
<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount">&#8377;<%=POCOUL.size() %></li>
 <li><%=POCOUL.size() %> PO'S RAISED FROM THE APPROVED INDENTS</li>
</ul>
</div>
</div>
</div>
</div>
<div class="paid-box">
<div class="unpaid">TOTAL MASTER STOCK ENTRIES</div>
<div class="bills-cash">
<div class="bill-paid">
 <ul>
<li class="bill-amount">&#8377;<%=MSEL.size() %></li>
 <li><%=MSEL.size() %> BILLS RECEIVED FROM THE VENDOR</li>
</ul>
</div>
</div>
</div>

</div>
<div class="vendor-box">
<div class="unpaid-box">
<div class="unpaid">TOTAL PENDING PURCHASE ORDER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TOTAL CLOSED PURCHASE ORDERS</div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount">&#8377;<%=POCOUL.size()-recvdms.size()%></li>
 <li><%=POCOUL.size()-recvdms.size()%> PURCHASE ORDERS PENDING</li>
</ul>
</div>
<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount">&#8377;<%=recvdms.size() %></li>
 <li><%=recvdms.size() %> PURCHASE ORDERS CLOSED FROM <%=POCOUL.size() %> PO'S</li>
</ul>
</div>
</div>
</div>
</div>

</div>

<%-- <div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%mainClasses.bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
List BNK_List=BNK_CL.getbankdetails();
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
if(BNK_List.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%"><input type="checkbox"></td>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Bank Name</td>
<td class="bg" width="23%">Bank Branch</td>
<td class="bg" width="20%">Account Holder Name</td>
<td class="bg" width="10%">Total Amount</td>
</tr>
<%for(int i=0; i < BNK_List.size(); i++ ){
	BNK=(beans.bankdetails)BNK_List.get(i); %>
<tr>
<td><input type="checkbox"></td>
<td><%=BNK.getbank_id()%></td>
<td><ul>
<li><span><a href="adminPannel.jsp?page=bankDetails&id=<%=BNK.getbank_id()%>"><%=BNK.getbank_name()%>  </a></span><br />
<span><%=HOA_CL.getHeadofAccountName(BNK.getheadAccountId()) %></span></li>
</ul></td>
<td><%=BNK.getbank_branch()%></td>
<td><%=BNK.getaccount_holder_name()%></td>
<td><%=BNK.gettotal_amount()%></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>No Banks Added Yet</h1></div>
<%}%>


</div>
</div> --%>




</div>
