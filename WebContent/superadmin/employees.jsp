<%@page import="java.util.List" %>
<jsp:useBean id="EMP" class="beans.employees"/>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<div class="vendor-page">
<%mainClasses.employeesListing EMP_CL = new mainClasses.employeesListing();
List EMP_List=EMP_CL.getemployees();
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); %>
<div class="vendor-box">
<div class="vendor-title">Employees</div>
<div class="click floatright"><a href="./newEmployee.jsp" target="_blank" onclick="openMyModal('newEmployee.jsp'); return false;">New Employee</a></div>
<div style="clear:both;"></div>

<div class="unpaid-box">
<div class="unpaid">Unpaid</div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OPEN BILL</li>
</ul>
</div>

<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OVERDUE</li>
</ul>
</div>
</div>
</div>
</div>
<div class="paid-box">
<div class="unpaid">Paid</div>
<div class="bills-cash">
<div class="bill-paid">
 <ul>
<li class="bill-amount"><%=EMP_List.size()%></li>
 <li>Employees</li>
</ul>
</div>
</div>
</div>
</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details" id="tblExport">
<%
if(EMP_List.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%"><input type="checkbox"></td>
<td class="bg" width="23%">Employee Name</td>
<td class="bg" width="23%">EMAIL</td>
<td class="bg" width="20%">Salary</td>
<td class="bg" width="10%">Joining Date</td>
</tr>
<%for(int i=0; i < EMP_List.size(); i++ ){
	EMP=(beans.employees)EMP_List.get(i); %>
<tr>
<td><input type="checkbox"></td>
<td><ul>
<li><span><a href="adminPannel.jsp?page=employeeDetails&id=<%=EMP.getemp_id()%>"><%=EMP.getfirstname()%> <%=EMP.getlastname()%></a></span><br />
<span><%=HOA_CL.getHeadofAccountName(EMP.getheadAccountId()) %></span></li>
<%if(!EMP.getemail().equals("")){%>
<li><img src="../images/w.png" align="center" style="vertical-align:central"/></li>
<%}%>
</ul></td>
<td><%=EMP.getemail()%></td>
<td><%=EMP.getemp_sal()%></td>
<td><%=EMP.getjoin_date()%></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>No Employees Added Yet</h1></div>
<%}%>


</div>
</div>




</div>
