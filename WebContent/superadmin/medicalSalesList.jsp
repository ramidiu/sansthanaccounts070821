<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.patientsentry"%>
<%@page import="mainClasses.patientsentryListing"%>
<%@page import="mainClasses.appointmentsListing"%>
<%@page import="beans.medicineissues"%>
<%@page import="mainClasses.medicineissuesListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SHOP SALES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	$("#"+billId).slideToggle();
	/* if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	} */
	
}
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( ".printable" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
 <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("superadminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="MEDI" class="beans.medicineissues" />
		<jsp:useBean id="APP" class="beans.appointments" />
		<jsp:useBean id="PAT" class="beans.patientsentry" />
		<div class="vendor-page">
				<div align="center" style="padding-top: 50px;font: bold;">Medicine Issue Report</div>
		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<input type="hidden" name="page" value="medicalSalesList"></input>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
medicineissuesListing MED_L=new medicineissuesListing();
employeesListing EMP_L=new employeesListing();
productsListing PRD_L=new productsListing();
appointmentsListing APPL=new appointmentsListing();
List Medicineissue=null;
patientsentryListing PATL=new patientsentryListing();
doctordetailsListing DOCL=new doctordetailsListing();
Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate")+" 00:00:01";
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate")+" 23:59:59";
	}
	//List SAL_list=MED_L.getmedicineissues(fromDate,toDate);
	List SAL_list=APPL.getMappointmentsBetweenDates(fromDate,toDate);
	List SAL_DETAIL=null;
	double totalAmount=0.00; 
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	List APP_Det=null;
	List PAT_Det=null;
	if(SAL_list.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%; margin:0 auto;
    background:#d0e3fb; 
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
  <td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</td>
</tr>
<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<tr><td colspan="9" align="center" style="font-weight: bold;">Doctor's Wise Visited Patients Report</td></tr>
						<tr><td colspan="9" style="padding:0px;margin:0px;"><table width="100%" cellpadding="0" cellspacing="0">
<tr>
							<td class="bg" width="3%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="12%" style="font-weight: bold;">APPOINTMENT-ID </td>
							<td class="bg" width="12%" style="font-weight: bold;">DATE</td>
							<td class="bg" width="20%" style="font-weight: bold;">PATIENT-ID/NAME</td>
							<td class="bg" width="3%" style="font-weight: bold;">AGE</td>
							<td class="bg" width="5%" style="font-weight: bold;">GENDER</td>
							<td class="bg" width="20%" style="font-weight: bold;">ADDRESS</td>
							<td class="bg" width="10%" style="font-weight: bold;">MOBILE NO</td>
							<td class="bg" width="20%" style="font-weight: bold;">CONSULTED DOCTOR</td>										
						</tr>
</table></td></tr>
						<%for(int i=0;i<SAL_list.size();i++){
							//MEDI=(medicineissues)SAL_list.get(i);
							APP=(beans.appointments)SAL_list.get(i);
							PAT_Det=PATL.getMpatientsentry(APP.getpatientId());
							if(PAT_Det.size()>0){
								PAT=(patientsentry)PAT_Det.get(0);
							}					
							%>
					<tr style="position: relative;">
						<td width="3%"><%=i+1 %></td>
						<td width="12%"><a href="javascript:void(0);" onclick="showDetails('<%=APP.getappointmnetId()%>')"><%=APP.getappointmnetId()%></a></td>
						<td width="12%"><%=chngDateFormat.format(dbDateFormat.parse(APP.getdate()))%></td>
						<td width="20%"><%=PAT.getpatientId() %>-<%=PAT.getpatientName()%></td>	
						<td width="3%"><%=PAT.getextra3() %></td>	
						<td width="5%"><%=PAT.getextra1() %></td>	
						<td width="20%"><%=PAT.getaddress() %></td>	
						<td width="10%"><%=PAT.getphone() %></td>
						<td><%=APP.getdoctorId() %>-<%=DOCL.getDoctorName(APP.getdoctorId()) %></td>																			
					</tr>
			
						<tr>
							<td colspan="9"   >
							<div style="display: none;" id="<%=APP.getappointmnetId()%>">
							<table width="100%" style="border: 1px solid #000;">
							<tr>
							<td width="5%"></td>
							<td width="5%">CODE</td>
							<td width="20%">TABLET NAME</td>
							<td width="20%">ISSUE QTY</td>
<!-- 							<td width="10%">PRICE</td>
							<td width="20%">TOTAL AMOUNT</td> -->
							<td width="20%">ISSUED BY</td>
						</tr>
					<% Medicineissue=MED_L.getMmedicineissuesBasedOnAppoitment(APP.getappointmnetId());
					if(Medicineissue.size()>0){
					for(int j=0;j<Medicineissue.size();j++){
						MEDI=(medicineissues)Medicineissue.get(j);
					%>
								<tr style="position: relative;">
									<td ></td>
									<td><%=MEDI.gettabletName() %></td>
									<td><%=PRD_L.getProductsNameByCats(MEDI.gettabletName(),"") %></td>
									<td><%=MEDI.getquantity() %></td>
									<td ><%=EMP_L.getMemployeesName(MEDI.getextra1())%></td>
								</tr>
					<%} }else{ %>
					<tr><td colspan="5" style="color: red;text-align:center;">Tablet's,not issued !</td></tr>
					<%} %>
						</table>
						</div>
						</td>
						</tr>
		<%} %>
				
					</table>
					</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;"><span>There are no tablets issued list found!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>