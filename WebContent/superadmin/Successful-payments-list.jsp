<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.payments"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.banktransactionsListing,java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
         $(document).ready(function () {
            $("#btnExport").click(function () {
                $( "a" ).css("text-decoration","none");
            	alert("hi");
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
</head>
<body>
	<%
	String Uniqueno[]=null;
	String vochertype="";
	double amount=0.0;
	double totalamount=0.0;
	String chequeno="";
	DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat  dateFormat2= new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
    String Vocherno[]=null;
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(new SimpleDateFormat("yyyy-MM-dd").format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	banktransactionsListing BNKT_L=new banktransactionsListing();
	productexpensesListing PROE_L=new productexpensesListing();
	paymentsListing PAYL=new paymentsListing();
	vendorsListing VENl=new vendorsListing();
	bankdetailsListing BNKL=new bankdetailsListing();
	mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
	List HA_Lists=HOA_L.getheadofaccounts();
	String hoid="";
	if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
		hoid=request.getParameter("hoid");
	}
	 if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
			if(request.getParameter("fromDate")!=null){
				fromDate=request.getParameter("fromDate")+" 00:00:01";
			}
			if(request.getParameter("toDate")!=null){
				toDate=request.getParameter("toDate")+" 23:59:59";
			}
		}
	DecimalFormat DF=new DecimalFormat("0.00");
	String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
		 fromdate=request.getParameter("fromDate");
	}
	if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
		todate=request.getParameter("toDate");
	}
	List Pay_Det=PAYL.getDailyPaymentsReport(hoid,fromDate,toDate);
	%>
<div class="vendor-page">
<div class="vendor-list">
<form action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
			<li>
<select name="hoid" id="hoid" onchange="formSubmit();">
<option value="">ALL DEPARTMENTS</option>
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></li>
						<li>
						<input type="hidden" name="page" value="Successful-payments-list"></input>
						<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span id="btnExport"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<div class="list-details" id="tblExport">

<table width="100%" cellpadding="0" cellspacing="0"   >
<tr><td colspan="8" height="10"></td> </tr>
<tr><td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 20px;">Successful Payments  </td></tr>

</table>
	<table width="100%" cellpadding="0" cellspacing="0">
   <!--  <tr><td colspan="10"><table width="100%" cellpadding="0" cellspacing="0"  class="yourID" > -->
<tr>
		<td class="bg" width="3%">S.No.</td>
		<td class="bg" width="10%">Unique No</td>
		<td class="bg" width="10%">PaymentInvoice No</td>
		<td class="bg" width="10%">Company</td>
		<td class="bg" width="8%">Vocher No</td>
		<td class="bg" width="7%">Date/Time</td>
		<td class="bg" width="10%">Naration</td>
		<td class="bg" width="10%">Vendor Name</td>
		<td class="bg" width="10%">Total Amount</td>
		<td class="bg" width="8%">Payment Type</td>

	</tr>
<!-- </table></td></tr> -->
	<%
	if(Pay_Det.size()>0){
		for(int i=0;i<Pay_Det.size();i++){
			PAY=(payments)Pay_Det.get(i);
			Vocherno=PAY.getreference().split(",");
			Uniqueno=PAY.getextra6().split(",");
			if( Vocherno.length>1){
				if(vochertype.equals("tds") && Vocherno.length>1){
					Vocherno[0]=Vocherno[1];
					chequeno=PAY.getextra9();
				} else if(vochertype.equals("servicetax")){
						if( Vocherno.length>2 && Vocherno[2]!=null){
					Vocherno[0]=Vocherno[2];
					} else {
						Vocherno[0]=Vocherno[1];
					}
						chequeno=PAY.getService_cheque_no();
				}
				
			} else{
				Vocherno[0]=PAY.getreference();
				chequeno=PAY.getchequeNo();
			}
			%>
	<tr>
		<td width="3%"><%=i+1 %></td>
		<td width="10%">
		<%if(Uniqueno.length>0){for(int j=0;j<Uniqueno.length;j++){
			%><span style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=Uniqueno[j]%>')"><%=Uniqueno[j]%></span><br/><%
		}} else{ %><%=PAY.getreference() %><%} %></td>
		<td width="10%"><%=PAY.getpaymentId()%></td>
		<td width="10%"><%=BNKL.getBankName(PAY.getbankId()) %></td>
		<td  width="8%"><%=Vocherno[0] %></td>
		<td  width="7%"><%=dateFormat2.format(dateFormat.parse(PAY.getdate())) %></td>
		<td  width="10%"><%=PAY.getnarration()%></td>
		<td  width="10%"><%=VENl.getMvendorsAgenciesName(PAY.getvendorId()) %></td>
		<td  width="10%"><%=DF.format(Math.round(Double.parseDouble(PAY.getamount())))%></td>
			<%if(PAY.getpaymentType()!=null && PAY.getpaymentType().equals("cheque")){ %>
				<td width="8%" align="center"><%=PAY.getextra2() %> Type</td>
		<%} else{ %>
		<td width="8%" align="center"><%=PAY.getpaymentType() %> Type</td>
		<%} %>
	</tr>
	<%totalamount=totalamount+Double.parseDouble(PAY.getamount());
	}	}else{ %>
	<tr><td colspan="10" style="color: red;font-size: 20px;" align="center">Sorry,there are no successful payments found!</td></tr>
	<%} %>
	<tr >
	<td  width="100%"  colspan="10">
		<div style="border-bottom:1px dashed #000; background:none;"></div>
	</td>
</tr>
		<tr>
	<td colspan="8" style="font-weight: bold;font-size: 15px;color: #E18D25;text-align: center;" >Total </td>
	<td colspan="2" style="font-weight: bold;font-size: 15px;color: #E18D25;text-align: left;  ">&#8377; <%=totalamount %>  /-</td>
	</tr>
	</table>
</div>
</div>
</div>
</body>
</html>