<%@page import="beans.indentapprovals"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script src="../js/jquery.blockUI.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	$( document ).ready(function() {
		$( "#hoid" ).change(function() {
			$.blockUI({ css: { 
			    border: 'none', 
			    padding: '15px', 
			    backgroundColor: '#000', 
			    '-webkit-border-radius': '10px', 
			    '-moz-border-radius': '10px', 
			    opacity: .5, 
			    color: '#fff' 
			}});
			setTimeout(function() {
				$("#filtration-form").submit();
			}, 500);
			});
		$( "#status" ).change(function() {
			$.blockUI({ css: { 
			    border: 'none', 
			    padding: '15px', 
			    backgroundColor: '#000', 
			    '-webkit-border-radius': '10px', 
			    '-moz-border-radius': '10px', 
			    opacity: .5, 
			    color: '#fff' 
			}});
			setTimeout(function() {
				$("#filtration-form").submit();
			}, 500);
			});
	});
</script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<jsp:useBean id="GDS" class="beans.godwanstock"/>
<jsp:useBean id="PRDE" class="beans.productexpenses"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<form id="filtration-form"  action="adminPannel.jsp" method="post">
<div class="search-list">
<ul>
<li><input type="hidden" name="page" value="paymentsApprovalsList"></li>
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts(); %>
<li><select name="hoid" id="hoid"  >
<option value=""  selected="selected" > ALL DEPARTMENTS </option>
<%if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("") && request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%} %>> <%=HOA.getname() %> </option>
<%}} %>
</select></li>
<li><select name="status" id="status" onchange="filter();">
<option value="">Sort by Approval Status</option>
<option value="Approved" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("Approved")){ %>  selected="selected" <%} %>>Approved List</option>\
<option value="Not-Approve"  <%if(request.getParameter("status")!=null && request.getParameter("status").equals("Not-Approve")){ %>  selected="selected" <%} %>>Not-Approved List</option>
<%if( session.getAttribute("role").equals("GS") || session.getAttribute("role").equals("CM")){ %>
<option value="Partially-Approved"  <%if(request.getParameter("status")!=null && request.getParameter("status").equals("Partially-Approved")){ %>  selected="selected" <%} %>>Partially Approved List</option>
<%} %>
<option value="Postpone"  <%if(request.getParameter("status")!=null && request.getParameter("status").equals("Postpone")){ %>  selected="selected" <%} %>>Postpone List</option>
<option value="DisApprove"  <%if(request.getParameter("status")!=null && request.getParameter("status").equals("DisApprove")){ %>  selected="selected" <%} %>>DisApprove List</option>
</select></li>
</ul>
</div>
</form>
<%String status="Not-Approve";
if(request.getParameter("status")!=null && !request.getParameter("status").equals("") ){
	status=request.getParameter("status");
} %>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="list-details">
<%mainClasses.indentListing IND_L = new indentListing();
indentapprovalsListing APPR_L=new indentapprovalsListing();
godwanstockListing GSK_L=new godwanstockListing();
productexpensesListing PRDEXP_L=new productexpensesListing();
superadminListing SAD_l=new superadminListing();
subheadListing SUBHL=new subheadListing();
List IND_List=IND_L.getindentGroupByIndentinvoice_id();
vendorsListing VENL=new vendorsListing();
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
List BAPR_L=GSK_L.getgodwanstockbillApprovePending(status,"");
List PRDEXP=null;
DecimalFormat DF=new DecimalFormat("#,##,###.00");
double totAmt=0.00;
double otherCharges=0.00;
String hoaid="";
if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")) {
	hoaid=request.getParameter("hoid");
}
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
superadminListing SUP_CL = new superadminListing();
List pendingIndents=IND_L.getPendingIndents();%>
<%if( session.getAttribute("role").equals("GS") || session.getAttribute("role").equals("CM")){
	String superadminId = "";
	if(session.getAttribute("role").equals("GS")){
		superadminId = SUP_CL.getAdminIdBasedOnRole("CM");
	}else if(session.getAttribute("role").equals("CM")){
		superadminId = SUP_CL.getAdminIdBasedOnRole("GS");
	}
	System.out.println("status---"+status);
	/* PRDEXP=PRDEXP_L.getExpensesListGroupByInvoiceAndHOA(status,hoaid,"1",session.getAttribute("superadminId").toString()); */
	PRDEXP=PRDEXP_L.getExpensesListGroupByInvoiceAndHOA(status,hoaid,"1",session.getAttribute("superadminId").toString(),superadminId);
	
	%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%; margin:0 2%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%=HOA_CL.getHeadofAccountName(hoaid) %></td></tr>
<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<tr><td colspan="9" align="center" style="font-weight: bold;">PAYMENTS <%=status %> LIST</td></tr>
<%if(PRDEXP.size()>0){ %>
<tr>
<td class="yourID" colspan="9">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%">S.NO.</td>
<td class="bg" width="6%">INVOICE NO / Unique No</td>
<td class="bg" width="10%">DEPARTMENT</td>
<td class="bg" width="15%">VENDOR NAME</td>
<td class="bg" width="10%">DATE</td>
<td class="bg" width="10%">AMOUNT</td>
<td class="bg" width="8%">STATUS</td>
<td class="bg" width="18%">NARRATION</td>
<td class="bg" width="20%">APPROVED BY</td>
</tr>
</table>
</td>
</tr>
<%List APP_Det=null;
String approvedBy="",indentapprBy = "";
for(int i=0; i < PRDEXP.size(); i++ ){
	PRDE=(productexpenses)PRDEXP.get(i);
	
		GDS=(godwanstock)GSK_L.getgodwanstockByinvoiceId(PRDE.getextra5()).get(0);
	System.out.println(PRDEXP.size()+" GDS.getbanktrnid;;;;;;;;"+GDS.getgsId());
		
	/* APP_Det=APPR_L.getIndentDetails(PRDE.getexpinv_id()); */
	APP_Det=APPR_L.getIndentDetailsApprovedByBoardMembersAndGsOrChairman(PRDE.getexpinv_id(),PRDE.getextra5());
	List<indentapprovals> iaplist = APPR_L.getIndentApprovalsList(PRDE.getexpinv_id());
	System.out.println("iaplist.size()===="+iaplist.size());
	if(iaplist.size() > 0)	{
		System.out.println("iaplist.size()=="+iaplist.size());
		int i1 = 0;
		for (indentapprovals iap : iaplist)	{
			i1++;
			if (i == 1)	{
				indentapprBy = SAD_l.getSuperadminname(iap.getadmin_id())+"--TIME ["+CDF.format(SDF.parse(iap.getapproved_date()))+" "+time.format(SDF.parse(iap.getapproved_date()))+"] "+iap.getextra3();
			}
			else	{
				indentapprBy=indentapprBy+"<br></br>"+SAD_l.getSuperadminname(iap.getadmin_id())+" --TIME ["+CDF.format(SDF.parse(iap.getapproved_date()))+" "+time.format(SDF.parse(iap.getapproved_date()))+"] "+iap.getextra3();
			}
		}
	}
	System.out.println("APP_Det.size()===="+APP_Det.size());
	if(APP_Det.size()>0){
		System.out.println("APP_Det.size()=="+APP_Det.size());
		for(int j=0;j<APP_Det.size();j++){
			IAP=(beans.indentapprovals)APP_Det.get(j);
			if(j==0){
				/* approvedBy=SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME ["+CDF.format(SDF.parse(IAP.getapproved_date()))+"]"; */
				approvedBy=SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"]";
			}else{
				approvedBy=approvedBy+"<br></br>"+SAD_l.getSuperadminname(IAP.getadmin_id())+" --TIME ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"]";
			}
		}
	} %>
<tr>
<td width="3%"><%=i+1%></td>
<%-- 	<%if(PRDE.getExtra6()!=null && !PRDE.getExtra6().equals("")){ %>
<td>
<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>')"><span><%=PRDE.getExtra6() %></span></div></td>
<%} else { %>
<td></td>
<%} %> --%>
<%if(status.equals("Not-Approve") || status.equals("Postpone")){ %>
<td width="6%"><a href="adminPannel.jsp?page=payments-detailsReport&invid=<%=PRDE.getexpinv_id()%>&approvedBy=<%=approvedBy%>&status=<%=status%>&indentapprBy=<%=indentapprBy%>&extra7=<%=GDS.getExtra7()%>&idd=<%=PRDE.gethead_account_id()%>"><b><%=PRDE.getexpinv_id()%></b></a>/<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>&MSENo=<%=PRDE.getextra5()%>&BillNo=<%=GDS.getbillNo()%>&Approve=<%=approvedBy%>&Narration=<%=PRDE.getnarration()%>')"><span><%=PRDE.getExtra6() %></span></div></td>
<%}else { %>
<td width="10%"><a ><b><%=PRDE.getexpinv_id()%></b></a>/<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>')"><span><%=PRDE.getExtra6() %></span></div></td>
<%} %>

<td width="15%"><%=HOA_CL.getHeadofAccountName(PRDE.gethead_account_id())%></td>
<td width="10%"> <%if(VENL.getMvendorsAgenciesName(PRDE.getvendorId())!=null && !VENL.getMvendorsAgenciesName(PRDE.getvendorId()).equals("")){ %> <%=VENL.getMvendorsAgenciesName(PRDE.getvendorId())%> <%}else{ %> <%=SUBHL.getMsubheadnameWithDepartment(PRDE.getsub_head_id(),PRDE.gethead_account_id()) %> <%} %> </td>
<td><%=df2.format(dateFormat.parse(PRDE.getentry_date()))%></td>
<td align="right" width="10%">
<%
System.out.println("PRDE.gethead_account_id()=="+PRDE.gethead_account_id());
if(PRDE.getExtra7()!=null && !PRDE.getExtra7().equals("")){
	otherCharges=Double.parseDouble(PRDE.getExtra7());
}
totAmt=totAmt+Double.parseDouble(PRDEXP_L.getPaymentTOT(PRDE.getexpinv_id())); %>
<%=DF.format(Double.parseDouble(PRDEXP_L.getPaymentTOT(PRDE.getexpinv_id()))+otherCharges)%></td>
<td align="center" width="8%"><%=PRDE.getextra1()%></td>
<td width="18%"><%=PRDE.getnarration() %></td>
<td width="20%"><%=approvedBy %></td>
</tr>

<%
	approvedBy="";
	}
%>
<tr style="border-top: 1px solid #000;"><td colspan="5" align="center" class="bg">TOTAL AMOUNT</td><td class="bg" align="right" style="font-size: 20px;"><%=DF.format(totAmt+otherCharges) %></td><td colspan="3" class="bg"></td></tr>
</table>
<%totAmt=0.00;}else{%>
<div align="center"><h1>Payments invoices not found! </h1></div>
<%} } else if(session.getAttribute("role").equals("FS")){
	
	PRDEXP=PRDEXP_L.getExpensesListGroupByInvoiceAndHOA(status,hoaid,"2",session.getAttribute("superadminId").toString(),"");
	System.out.println(PRDEXP.size()+"sizepex");
if(PRDEXP.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">




<tr>
<td class="bg" width="3%">S.NO.</td>
<td class="bg" width="3%">INVOICE NO/Unique No</td>
<td class="bg" width="10%">DEPARTMENT</td>
<td class="bg" width="15%">VENDOR NAME</td>
<td class="bg" width="10%">DATE</td>
<td class="bg" width="10%">AMOUNT</td>
<td class="bg" width="8%">STATUS</td>
<td class="bg" width="6%">NARRATION</td>
<td class="bg" width="15%">APPROVED BY(SUB COMMITTEE)</td>
<td class="bg" width="15%">APPROVED BY(AUTHORITY)</td>
<%List APP_Det=null;
String approvedBy="";
String approvedBy1="";
for(int i=0; i < PRDEXP.size(); i++ ){
	PRDE=(productexpenses)PRDEXP.get(i);
	GDS=(godwanstock)GSK_L.getgodwanstockByinvoiceId(PRDE.getextra5()).get(0);
	/* APP_Det=APPR_L.getIndentDetails(PRDE.getexpinv_id()); */
	APP_Det=APPR_L.getIndentDetailsApprovedByBoardMembersAndGsOrChairman(PRDE.getexpinv_id(),PRDE.getextra5());
	if(APP_Det.size()>0){
		for(int j=0;j<APP_Det.size();j++){
			IAP=(beans.indentapprovals)APP_Det.get(j);
			if(j==0){
				approvedBy1=SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME  ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"]";;
				System.out.println("id..."+PRDE.getexpinv_id()+".......11111111111......."+approvedBy1);
			}else{
				approvedBy=approvedBy+SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME  ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"],";
				 System.out.println("id..."+PRDE.getexpinv_id()+".......22222222222......."+approvedBy); 
				
			}
		}
		if(approvedBy.length()>0){
		approvedBy=approvedBy.substring(0,approvedBy.length()-1);
		}
	} %>
<tr>
<td width="3%"><%=i+1%></td>
	<%-- <%if(PRDE.getExtra6()!=null && !PRDE.getExtra6().equals("")){ %>
<td>
<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>')"><span><%=PRDE.getExtra6() %></span></div></td>
<%} else { %>
<td></td>
<%} %> --%>
<%if(status.equals("Not-Approve") || status.equals("Postpone")){ %>

<td width="3%"><a href="adminPannel.jsp?page=payments-detailsReport&invid=<%=PRDE.getexpinv_id()%>&approvedBy=<%=approvedBy%>&approvedBy1=<%=approvedBy1%>&status=<%=status%>&idd=<%=PRDE.gethead_account_id()%>"><b><%=PRDE.getexpinv_id()%></b></a>/<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>&MSENo=<%=PRDE.getextra5()%>&BillNo=<%=GDS.getbillNo()%>&Approve=<%=approvedBy%>&approvedBy1=<%=approvedBy1%>&Narration=<%=PRDE.getnarration()%>')"><span><%=PRDE.getExtra6() %></span></div></td>
<%} else{ %>
<td width="10%"><a ><b><%=PRDE.getexpinv_id()%></b></a>/<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>')"><span><%=PRDE.getExtra6() %></span></div></td>
<%} %>
<td width="10%"><%=HOA_CL.getHeadofAccountName(PRDE.gethead_account_id())%></td>
<td width="15%"> <%if(VENL.getMvendorsAgenciesName(PRDE.getvendorId())!=null && !VENL.getMvendorsAgenciesName(PRDE.getvendorId()).equals("")){ %> <%=VENL.getMvendorsAgenciesName(PRDE.getvendorId())%> <%}else{ %> <%=SUBHL.getMsubheadnameWithDepartment(PRDE.getsub_head_id(),PRDE.gethead_account_id()) %> <%} %> </td>
<td width="10%"><%=df2.format(dateFormat.parse(PRDE.getentry_date()))%></td>
<td align="right" width="10%">
<%
if(PRDE.getExtra7()!=null && !PRDE.getExtra7().equals("")){
	otherCharges=Double.parseDouble(PRDE.getExtra7());
}
totAmt=totAmt+Double.parseDouble(PRDEXP_L.getPaymentTOT(PRDE.getexpinv_id())); %>
<%=DF.format(Double.parseDouble(PRDEXP_L.getPaymentTOT(PRDE.getexpinv_id()))+otherCharges)%></td>
<td align="center" width="8%"><%=PRDE.getextra1()%></td>
<td width="6%"><%=PRDE.getnarration() %></td>
<td width="15%" style="color:red"> <%=approvedBy %> </td>
<td width="15"><%=approvedBy1 %></td>
</tr>
<%
approvedBy="";
} %>
<tr style="border-top: 1px solid #000;"><td colspan="6" align="center" class="bg">TOTAL AMOUNT</td><td class="bg" style="font-size: 20px;" align="right"><%=DF.format(totAmt+otherCharges) %></td><td colspan="2" class="bg"></td><td colspan="1" class="bg"></td></tr>
</table>
<%totAmt=0.00;}else{%>
<div align="center"><h1><%=status%> Payments invoices not found! </h1></div>
<%}}	%>

</div>
</div>
</div>
