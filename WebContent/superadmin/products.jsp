<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#ProductIdErr').hide();
	$('#ProductNameErr').hide();
	$('#MajorIdErr').hide();
	$('#MinorIdErr').hide();

	if($('#major_head_id').val().trim()==""){
		$('#MajorIdErr').show();
		$('#major_head_id').focus();
		return false;
	}
	if($('#minor_head_id').val().trim()==""){
		$('#MinorIdErr').show();
		$('#minor_head_id').focus();
		return false;
	}
	if($('#Product_id').val().trim()==""){
		$('#ProductIdErr').show();
		$('#Product_id').focus();
		return false;
	}
	if($('#Product_name').val().trim()==""){
		$('#ProductNameErr').show();
		$('#Product_name').focus();
		return false;
	}
	if($('#buyprice').val().trim()==""){
		$("#buyprice").attr('class', 'borderred');
		$('#buyprice').focus();
		return false;
	}
	if($('#vatbuy').val().trim()==""){	
		$("#vatbuy").attr('class', 'borderred');
		$('#vatbuy').focus();
		return false;
	}
}
function calculate(i){
	var vat=0;
	var bypr=0;	
	var bygr=0;	
	vat=document.getElementById("vat").options[document.getElementById("vat").selectedIndex].value;
	if(i=='1'){
if(vat!=""){
	document.getElementById("vatbuy").value=vat;
	if(document.getElementById("buyprice").value!=""){
		bygr=Number(parseFloat(document.getElementById("buyprice").value)+Number(parseFloat(document.getElementById("buyprice").value)*(parseFloat(vat)/100)));
		document.getElementById("buygross").value=bygr;
	}
	else if(document.getElementById("buygross").value!=""){
		bypr=Number(parseFloat(document.getElementById("buygross").value)-Number(parseFloat(document.getElementById("buygross").value)*(parseFloat(vat)/100)));
		document.getElementById("buyprice").value=bypr;
	}
}
	}
	else if(i=='2'){
		if(document.getElementById("vatsell").value!=""){
						if(document.getElementById("sellprice").value!=""){
				bygr=Number(parseFloat(document.getElementById("sellprice").value)+Number(parseFloat(document.getElementById("sellprice").value)*(parseFloat(document.getElementById("vatsell").value)/100)));
				document.getElementById("sellgross").value=bygr;
			}
			else if(document.getElementById("sellgross").value!=""){
				bypr=Number(parseFloat(document.getElementById("sellprice").value)-Number(parseFloat(document.getElementById("sellprice").value)*(parseFloat(document.getElementById("vatsell").value)/100)));
				document.getElementById("sellgross").value=bypr;
			}
		}
	}
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
$(document).ready(function(){
  $("#major_head_id").keyup(function(){
	  var data1=$("#major_head_id").val();
	  $.post('searchMajor.jsp',{q:data1},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#major_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#minor_head_id").keyup(function(){
	  var data1=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  $.post('searchMinior.jsp',{q:data1,q1:major},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#minor_head_id" ).autocomplete({source: availableTags}); 
			});
  });
});
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("superadminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"><%if(request.getParameter("id")!=null) {%>Edit major head<%}else{%>Create a new major head <%} %></div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
if(request.getParameter("id")!=null) {
List HD_details=MajorHead_list.getMmajorhead(request.getParameter("id"));
MH=(beans.majorhead)HD_details.get(0);
%>
<form name="tablets_Update" method="post" action="products_Insert.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="major_head_id" id="major_head_id" value="<%=MH.getmajor_head_id()%>"/>
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td width="35%">
<div class="warning" id="majorheadErr" style="display: none;">Please Provide  "Major Head Number".</div>
Major Head No*</td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Major Head Name".</div>
Major Head Name*</td>
</tr>
<tr>
<td><select>
<option value="<%=MH.gethead_account_id()%>"><%=HOA_L.getHeadofAccountName(MH.gethead_account_id()) %></option>
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><input type="text" name="major_head_id" id="major_head_id" value="" /></td>
<td><input type="text" name="name" id="name" value="<%=MH.getname()%>" /></td>
</tr>


<tr>
<td colspan="3">
<textarea name="description" id="description" placeholder="Enter description"><%=MH.getdescription()%></textarea></td>
</tr>
<tr> 
<td colspan="2"></td>
<td align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<% } else{%>
<form name="tablets_Update" method="post" action="products_Insert.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account*</td>
<td width="35%">
<div class="warning" id="MajorIdErr" style="display: none;">Please Provide  "Major Head Number".</div>
Major Head No*</td>
<td >
<div class="warning" id="MinorIdErr" style="display: none;">Please Provide  "Minor Head Name".</div>
Minor Head Name*</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><input type="text" name="major_head_id" id="major_head_id" value="" /></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value="" /></td>
</tr>
<tr>
<td>
<div class="warning" id="ProductIdErr" style="display: none;">Please Provide  "Product Number".</div>
Product No*</td>
<td >
<div class="warning" id="ProductNameErr" style="display: none;">Please Provide  "Product Name".</div>
Product Name*</td>
<td>Vat*</td>
</tr>
<tr>
<td><input type="text" name="Product_id" id="Product_id" value="" /></td>
<td><input type="text" name="Product_name" id="Product_name" value="" /></td>
<td><select name="vat" id="vat" onclick="calculate('1')">
<option value="">-VAT-</option>
<option value="0">0</option>
<option value="2">2</option>
<option value="5">5</option>
<option value="14.5">14.5</option>
</select>
</tr>
<tr>
<td colspan="2">
<textarea name="description" id="description" placeholder="Enter description"></textarea></td>
</tr>
<tr>
<td>Purchase</td>
<td>VAT</td>
<td>Gross</td>
</tr>
<tr>

<td><input type="text" name="buyprice" id="buyprice" value="" onblur="calculate('1')" placeholder="Buying Price"/></td>
<td><input type="text" name="vatbuy" id="vatbuy" value="" readonly="readonly"/></td>
<td><input type="text" name="buygross" id="buygross" value="" onblur="calculate('1')" /></td></tr>
<tr>

<td><input type="text" name="sellprice" id="sellprice" value="" placeholder="Selling Price" onblur="calculate('2')"  /></td>
<td><input type="text" name="vatsell" id="vatsell" value="" onblur="calculate('2')" /></td>
<td><input type="text" name="sellgross" id="sellgross" value="" readonly="readonly" /></td></tr>

<tr> 
<td colspan="2"></td>
<td align="right"><input type="submit" value="Create" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<%} %>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%
List<majorhead> MJH_list=MajorHead_list.getmajorhead();
if(MJH_list.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Major Head Name</td>
<td class="bg" width="23%">Description </td>
<td class="bg" width="23%">Head of account</td>
<td class="bg" width="20%">Edit</td>
<td class="bg" width="10%">Delete</td>
</tr>
<%for(int i=0; i < MJH_list.size(); i++ ){
	MH=(beans.majorhead)MJH_list.get(i); %>
<tr>
<td><%=MH.getmajor_head_id()%></td>
<td><span><%=MH.getname()%></span></td>
<td><%=MH.getdescription()%></td>
<td><%=HOA_L.getHeadofAccountName(MH.gethead_account_id())%></td>
<td><a href="adminPannel.jsp?page=majorHead&id=<%=MH.getmajor_head_id()%>">Edit</a></td>
<td><a href="majorhead_Delete.jsp?Id=<%=MH.getmajor_head_id()%>">Delete</a></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no major heads</h1></div>
<%}%>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
