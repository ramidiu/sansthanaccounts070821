<%@page import="beans.PasswordDecryptor"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="beans.PasswordEncryptor"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
.myButton {
	-moz-box-shadow:inset 0px 1px 0px 0px #a6827e;
	-webkit-box-shadow:inset 0px 1px 0px 0px #a6827e;
	box-shadow:inset 0px 1px 0px 0px #a6827e;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #7d5d3b), color-stop(1, #634b30));
	background:-moz-linear-gradient(top, #7d5d3b 5%, #634b30 100%);
	background:-webkit-linear-gradient(top, #7d5d3b 5%, #634b30 100%);
	background:-o-linear-gradient(top, #7d5d3b 5%, #634b30 100%);
	background:-ms-linear-gradient(top, #7d5d3b 5%, #634b30 100%);
	background:linear-gradient(to bottom, #7d5d3b 5%, #634b30 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#7d5d3b', endColorstr='#634b30',GradientType=0);
	background-color:#7d5d3b;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	border:1px solid #54381e;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:13px;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #4d3534;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #634b30), color-stop(1, #7d5d3b));
	background:-moz-linear-gradient(top, #634b30 5%, #7d5d3b 100%);
	background:-webkit-linear-gradient(top, #634b30 5%, #7d5d3b 100%);
	background:-o-linear-gradient(top, #634b30 5%, #7d5d3b 100%);
	background:-ms-linear-gradient(top, #634b30 5%, #7d5d3b 100%);
	background:linear-gradient(to bottom, #634b30 5%, #7d5d3b 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#634b30', endColorstr='#7d5d3b',GradientType=0);
	background-color:#634b30;
}
.myButton:active {
	position:relative;
	top:1px;
}
.tb10 {
	background-repeat:repeat-x;
	border:1px solid #d1c7ac;
	width: 230px;
	color:#333333;
	padding:3px;
	margin-right:4px;
	margin-bottom:8px;
	font-family:tahoma, arial, sans-serif;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  <script>
	function validateCaptcha()	{
		  var $captcha = $( '#recaptcha' ),
	      response = grecaptcha.getResponse();
	  
	  if (response.length === 0) {
	    if( !$captcha.hasClass( "error" ) ){
	      alert("Complete The Capthcha");
	    }
	    return false;
	  } else {
	    $captcha.removeClass( "error" );
	  }
	}
</script>
<title>Encrypt|Decrypt</title>
</head>
<body>
<div style="height: 20px;"></div>
<form action="PswdMethods.jsp" method="post" onsubmit="return validateCaptcha();">
<table align="center">
 <tr>
 	<td style="font-weight: bold;font-family:sans-serif;">USERNAME : </td>
 	<td><input type="text" name="username" value="" autocomplete="off" class="tb10"/></td>
 </tr>
 <tr>
 	<td style="font-weight: bold;font-family:sans-serif;">PASSWORD : </td>
 	<td><input type="password" name="password" value="" autocomplete="off" class="tb10"/></td>
 </tr>
  <tr>
  	<td style="font-weight: bold;font-family:sans-serif;">SECRET KEY : </td>
  	<td><input type="password" name="key" value="" required="required" autocomplete="off" class="tb10"/></td>
  </tr>
  <tr></tr>
  <tr></tr>
  <tr>
  	<td align="left"><input type="submit" name="encrypt"  value="ENCRYPT" class="myButton"/></td>
  	<td align="right"><input type="submit" name="decrypt"  value="DECRYPT" class="myButton"/></td>
 </tr>
 </table>
 <div style="height: 20px;" align="center">
 <div class="g-recaptcha" data-sitekey="6LcnxKoUAAAAAPUaT5n9gymmvEMZyYWyIxbhMrPM" id="recaptcha">
</div>
 </div>
 </form>
 
 <%
 PasswordEncryptor PSD=new PasswordEncryptor();
 PasswordDecryptor PSDE=new PasswordDecryptor();
 superadminListing SUPL=new superadminListing();
 System.out.println("password==="+request.getParameter("password"));
 if(request.getParameter("password")!=null && !request.getParameter("password").equals("") && request.getParameter("encrypt") != null && request.getParameter("encrypt").equals("ENCRYPT") && request.getParameter("key").equals("123sj") ){
	 out.print("<br><br><br><br><h3 style='text-align:center;font-family:sans-serif;'>Encrypted password::&nbsp;&nbsp;"+PSD.encrypt("Bar12345Bar12345", "ThisIsASecretKet", request.getParameter("password"))+"</h3>");
 }else if(request.getParameter("username")!=null && !request.getParameter("username").equals("") && request.getParameter("decrypt").equals("DECRYPT") && request.getParameter("key").equals("123sj")){
	 String pswd=SUPL.getPassword(request.getParameter("username"));
	 System.out.println("pswd------"+pswd);
	 if (pswd.trim().equals(""))	{
		 out.print("<br><br><br><br><h3 style='text-align:center;font-family:sans-serif;'>The UserName is Wrong Or User is InActive</h3>");
	 }
	 else out.print("<br><br><br><br><h3 style='text-align:center;font-family:sans-serif;'>Decrypted password::&nbsp;&nbsp;"+PSDE.decrypt("Bar12345Bar12345", "ThisIsASecretKet",pswd)+"</h3>");
 }else if(request.getParameter("decrypt")!=null ||request.getParameter("encrypt")!=null  ){
	 out.print("<br><br><br><br><h3 style='text-align:center;font-family:sans-serif;'>Secret key you entered wrong !...</h3>");
 }
 %>
</body>
</html>