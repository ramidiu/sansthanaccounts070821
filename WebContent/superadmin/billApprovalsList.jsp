<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.billimages"%>
<%@page import="mainClasses.billimagesListing"%>
<%@page import="javax.mail.Session"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List"%>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript"
	src="../js/modal-window.js"></script>
<script src="../js/jquery.blockUI.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source) {
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();

	};
	function filter() {
		/*  $.blockUI({ css: { 
		    border: 'none', 
		    padding: '15px', 
		    backgroundColor: '#000', 
		    '-webkit-border-radius': '10px', 
		    '-moz-border-radius': '10px', 
		    opacity: .5, 
		    color: '#fff' 
		}});
		setTimeout(function() {
			$("#filtration-form").submit();
		}, 500); */
		$("#filtration-form").submit();
		//location.reload('godwanForm.jsp?poid='+poid); 
		//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
	}
</script>
<script type="text/javascript" lang="javascript">
	var openMyModalSj = function(source) {
		modalWindow.windowId = "myModal";
		modalWindow.width = 450;
		modalWindow.height = 200;
		modalWindow.content = "<iframe width='450' height='200' frameborder='0' scrolling='yes' allowtransparency='false' src='pendingIndentsPopup.jsp'></iframe>";
		modalWindow.open();

	};
</script>
<script type="text/javascript">
	// When the document is ready, initialize the link so
	// that when it is clicked, the printable area of the
	// page will print.
	$(function() {
		// Hook up the print link.
		$("#print").attr("href", "javascript:void( 0 )").click(function() {
			// Print the DIV.
			$("a").css("text-decoration", "none");
			$("#printable").print();

			// Cancel click event.
			return (false);
		});
	});
	$(document).ready(function() {
		$("#btnExport").click(function() {
			$("#tblExport").btechco_excelexport({
				containerid : "tblExport",
				datatype : $datatype.Table
			});
		});
	});
</script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="INDT" class="beans.indent" />
<jsp:useBean id="IAP" class="beans.indentapprovals" />
<jsp:useBean id="GDS" class="beans.godwanstock" />
<jsp:useBean id="IMG" class="beans.billimages" />
<jsp:useBean id="PRDE" class="beans.productexpenses"></jsp:useBean>
<%
	if (session.getAttribute("role").equals("GS") || session.getAttribute("role").equals("CM")
			|| session.getAttribute("role").equals("FS")) {
		if (session.getAttribute("popupboardaprv") != null && !session.getAttribute("popupboardaprv").equals("")
				&& session.getAttribute("expbilaprov") != null
				&& !session.getAttribute("expbilaprov").equals("")) {
%>
<script>
	window.onload = openMyModalSj();
</script>
<%
	session.setAttribute("popupboardaprv", null);
		}
	} else {
		if (session.getAttribute("popupPage") != null && !session.getAttribute("popupPage").equals("")) {
%>
<script>
	window.onload = openMyModalSj();
</script>
<%
	session.setAttribute("popupPage", null);
		}
	}
%>
<div class="vendor-page">
	<div class="vendor-list">
		<!-- <div class="arrow-down"><img src="../images/Arrow-down.png"></div> -->
		<form id="filtration-form" action="adminPannel.jsp" method="post">
			<div class="search-list">
				<ul>
					<li><select name="finYear" id="finYear">
							<option value="2021-2022"
								<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2021-2022")) {%>
								selected="selected" <%}%>>2021-22</option>
							<option value="2020-2021"
								<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-2021")) {%>
								selected="selected" <%}%>>2020-21</option>
							<option value="2019-2020"
								<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-2020")) {%>
								selected="selected" <%}%>>2019-20</option>
							<option value="2018-2019"
								<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-2019")) {%>
								selected="selected" <%}%>>2018-19</option>
							<option value="2017-2018"
								<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-2018")) {%>
								selected="selected" <%}%>>2017-18</option>
							<option value="2016-2017"
								<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-2017")) {%>
								selected="selected" <%}%>>2016-17</option>
							<option value="2015-2016"
								<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-2016")) {%>
								
								selected="selected" <%}%>>2015-16</option>
					</select></li>
					<li><input type="hidden" name="page" value="billApprovalsList"></li>
					<li><select name="status">
							<option value="">Sort by Approval Status</option>
							<option value="Approved"
								<%if (request.getParameter("status") != null && request.getParameter("status").equals("Approved")) {%>
								selected="selected" <%}%>>Approved List</option>
							<option value="Partly-Approved"
								<%if (request.getParameter("status") != null && request.getParameter("status").equals("Partly-Approved")) {%>
								selected="selected" <%}%>>Partially Approved List</option>
							<option value="Not-Approve"
								<%if (request.getParameter("status") != null && request.getParameter("status").equals("Not-Approve")) {%>
								selected="selected" <%}%>>Not-Approved List</option>
							<option value="Disapproved"
								<%if (request.getParameter("status") != null && request.getParameter("status").equals("Disapproved")) {%>
								selected="selected" <%}%>>Disapproved List</option>
					</select></li>
					<li><input type="submit" class="click" name="search"
						value="Search" onsubmit="filter();"></input></li>
				</ul>
			</div>
		</form>
		<%
			String status = "Not-Approve";
			if (request.getParameter("status") != null && !request.getParameter("status").equals("")) {
				if (request.getParameter("status").equals("Not-Approve")) {
					status = "Not-Approve";
				} else {
					status = request.getParameter("status");
				}
			}
		%>
		<div class="icons">
			<a id="print"><span><img src="../images/printer.png"
					style="margin: 0 20px 0 0" title="print" /></span></a>
			<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> --%>
			<span><a id="btnExport" href="#Export to excel"><img
					src="../images/excel.png" style="margin: 0 20px 0 0"
					title="export to excel" /></a></span>
		</div>
		<div class="clear"></div>
		<div class="list-details" id="printable">
			<%
			    String  finYear=request.getParameter("finYear");
			    System.out.println("finYear:::"+finYear);
				List APP_Det = null;
				mainClasses.indentListing IND_L = new indentListing();
				indentapprovalsListing APPR_L = new indentapprovalsListing();
				godwanstockListing GSK_L = new godwanstockListing();
				productexpensesListing PRDEXP_L = new productexpensesListing();
				superadminListing SAD_l = new superadminListing();
				List IND_List = IND_L.getindentGroupByIndentinvoice_id();
				vendorsListing VENL = new vendorsListing();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
				SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat CDF = new SimpleDateFormat("dd-MM-yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String superadminId = "";
				if (!session.getAttribute("role").equals("GS") || !session.getAttribute("role").equals("FS")) {
					superadminId = session.getAttribute("superadminId").toString();
				}
				if (status.equals("Disapproved")) {
					status = "billdisapproved";
				}
				List BAPR_L = GSK_L.getgodwanstockbillApprovePendingBasedOnDepartment(status, finYear, session.getAttribute("department").toString(), superadminId);
						
					
				List PRDEXP = null;
				mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
				billimagesListing BILLIMG = new billimagesListing();
				List pendingIndents = IND_L.getPendingIndents();
				List BILL = null;
				double totalAmt = 0.00;

				DecimalFormat DF = new DecimalFormat("0.00");
			%>
			<%
				PRDEXP = PRDEXP_L.getExpensesListGroupByInvoice(status);
				if (BAPR_L.size() > 0) {
			%>
			<style>
.yourID.fixed {
	position: fixed;
	top: 0;
	left: 0px;
	z-index: 1;
	width: 96%;
	margin: 0 2%;
	background: #d0e3fb;
}
</style>
			<script>
				$(document).ready(
						function() {
							var top = $('.yourID').offset().top
									- parseFloat($('.yourID').css('marginTop')
											.replace(/auto/, 100));
							$(window).scroll(function(event) {
								// what the y position of the scroll is
								var y = $(this).scrollTop();

								// whether that's below the form
								if (y >= top) {
									// if so, ad the fixed class
									$('.yourID').addClass('fixed');

								} else {
									// otherwise remove it
									$('.yourID').removeClass('fixed');

								}
							});
						});
			</script>
			<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">


				<tr>
					<td colspan="10" align="center"
						style="font-weight: bold; color: red;" class="bg-new">SHRI
						SHIRIDI SAI BABA SANSTHAN TRUST</td>
				</tr>
				<tr>
					<td colspan="10" align="center"
						style="font-weight: bold; font-size: 12px;" class="bg-new">(Regd.No.646/92)</td>
				</tr>
				<tr>
					<td colspan="10" align="center"
						style="font-weight: bold; font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td>
				</tr>
				<%-- <tr><td colspan="10" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Bill's <%=status %> List</td></tr> --%>
				<tr>
					<td colspan="10" align="center"
						style="font-weight: bold; font-size: 16px;" class="bg-new"><br />Bill's
						<%
						if (status.equals("billdisapproved")) {
					%>Disapproved<%
						} else {
					%><%=status%>
						<%
							}
						%> List</td>
				</tr>



				<tr>
					<td colspan="10" class="yourID"><table width="100%"
							cellpadding="0" cellspacing="0">
							<tr>
								<td class="bg" width="2%">S.No.</td>
								<td class="bg" width="8%">Unique No</td>
								<td class="bg" width="4%">Purchase Voucher No</td>
								<td class="bg" width="10%">Company</td>
								<td class="bg" width="15%">Vendor Name</td>
								<td class="bg" width="10%">Date</td>
								<td class="bg" width="10%">Total Amount</td>
								<!-- <td class="bg" width="8%">Status</td> -->
								<%
									if (status.equals("billdisapproved")) {
								%>
								<td class="bg" width="28%">Disapproved By</td>
								<%
									} else {
								%>
								<td class="bg" width="28%">Approved By</td>
								<%
									}
								%>
								<td class="bg" width="20%">Narration</td>
								<td class="bg" width="3%">Attached Bills</td>
							</tr>
						</table></td>
				</tr>


				<%
					String approvedBy = "";
						String conCheck = "";
						List appSize = null;
						for (int i = 0; i < BAPR_L.size(); i++) {
							GDS = (godwanstock) BAPR_L.get(i);
							/* APP_Det=APPR_L.getIndentDetails(GDS.getextra1()); */
							if (status.equals("billdisapproved")) {
								APP_Det = APPR_L.getIndentDetails(GDS.getextra1(), "billdisapproved");
							} else {
								APP_Det = APPR_L.getIndentDetails(GDS.getextra1(), "approved");
							}
							if (APP_Det.size() > 0) {
								for (int j = 0; j < APP_Det.size(); j++) {
									IAP = (beans.indentapprovals) APP_Det.get(j);
									if (j == 0) {
										approvedBy = SAD_l.getSuperadminname(IAP.getadmin_id()) + "--TIME  ["
												+ CDF.format(SDF.parse(IAP.getapproved_date())) + "]";
									} else {
										approvedBy = approvedBy + "<br></br>" + SAD_l.getSuperadminname(IAP.getadmin_id())
												+ " --TIME [" + CDF.format(SDF.parse(IAP.getapproved_date())) + " "
												+ time.format(SDF.parse(IAP.getapproved_date())) + "]";
									}
								}
							}
							if (status.equals("Not-Approve")) {
								if (APPR_L.getIndentApprovedByAdmin(GDS.getextra1(), superadminId).size() > 0) {
									appSize = APPR_L.getIndentApprovedByAdmin(GDS.getextra1(), superadminId);
								}
							}
							if (appSize == null) {
								totalAmt = totalAmt + Math.round(Double.parseDouble(GSK_L.getBillTOT(GDS.getextra1())));
				%>
				<tr>
					<td width="2%"><%=i + 1%></td>
					<%
						if (GDS.getExtra9() != null && !GDS.getExtra9().equals("")) {
					%>
					<td width="8%">
						<div style="font-weight: bold; color: red; cursor: pointer;"
							onclick="openMyModal('Trackingno_Details.jsp?TId=<%=GDS.getExtra9()%>&Approve=<%=approvedBy%>&Narration=<%=GDS.getdescription()%>&MSENo=<%=GDS.getextra1()%>&BillNo=<%=GDS.getbillNo()%>')">
							<span><%=GDS.getExtra9()%></span>
						</div>
					</td>
					<%
						} else {
					%>
					<td width="8%"></td>
					<%
						}
					%>
					<td width="4%"><a
						href="adminPannel.jsp?page=bill_detailsReport&invid=<%=GDS.getextra1()%>&approvedBy=<%=approvedBy%>&statuss=<%=status%>"><b><%=GDS.getextra1()%></b></a></td>
					<td width="10%"><%=HOA_CL.getHeadofAccountName(GDS.getdepartment())%></td>
					<td width="15%"><%=VENL.getMvendorsAgenciesName(GDS.getvendorId())%></td>
					<td width="10%"><%=df2.format(dateFormat.parse(GDS.getdate()))%></td>
					<td align="right" width="10%"><%=DF.format(Math.round(Double.parseDouble(GSK_L.getBillTOTAL(GDS.getextra1()))))%>
					</td>
					<%-- <td><%=GDS.getApproval_status()%></td> --%>
					<td width="28%"><%=approvedBy%></td>
					<td width="20%"><%=GDS.getdescription() %></td>
					<td width="3%">
						<%if(BILLIMG.getImageName(GDS.getextra1())!=null && !BILLIMG.getImageName(GDS.getextra1()).equals("")){ %>
						<a href="../BillImages/<%=BILLIMG.getImageName(GDS.getextra1())%>"
						target="_blank">DOC</a>
						<%}else{ %>N/A <%} %>
					</td>
				</tr>
				<%} %>
				<%
approvedBy="";
appSize=null;
} %>
				<tr>
					<td colspan="6" align="right" class="bg">TOTAL AMOUNT</td>
					<td class="bg" align="right"><%=DF.format(totalAmt) %></td>
					<td colspan="3" class="bg"></td>
				</tr>
			</table>
			<%}else{%>
			<div align="center">
				<h1>
					Sorry,there are no bills found for
					<%if(status.equals("billdisapproved")){ %>Disapprovals<%}else{ %>approvals<%} %>!..
				</h1>
			</div>
			<%}  %>
		</div>
	</div>
</div>
