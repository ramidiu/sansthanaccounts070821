<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.shopstock"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="beans.indentapprovals"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.util.List" %>
 <link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function deleteProduct(id){
	var retVal = confirm("Are you sure you want to delete this product ?");
	if( retVal == true ){
	$.ajax({
		 type:'post',
		 url: 'productDelete.jsp',
		 data: {
		 indentid : id
		 },
		 success: function(response) {
			$('#refresh').load(window.location+''+' #refresh');
		 }});
	}
}
function vendorsearch(j){
	 var data1=$('#vendorId'+j).val();
	  var headID=$('#headAccountId').val();
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $('#vendorId'+j).autocomplete({source: availableTags}); 
			});
}
function blockPage(){
	$.blockUI({ css: { 
	    border: 'none', 
	    padding: '15px', 
	    backgroundColor: '#000', 
	    '-webkit-border-radius': '10px', 
	    '-moz-border-radius': '10px', 
	    opacity: .5, 
	    color: '#fff' 
	}});
}
$(function(){
	$('#submitBtn').click(function(){
		blockPage();
		document.getElementById("indentAppForm").action= "<%=request.getContextPath()%>/IndAppInsert";
		document.getElementById("indentAppForm").submit();
	});
});
</script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
<jsp:useBean id="VND" class="beans.vendors"/>
<jsp:useBean id="INDAPPROV" class="beans.indentapprovals"/>
	<%
		String INVId=request.getParameter("invid");
	System.out.println(request.getParameter("invid"));	
	String statuss=request.getParameter("statuss");
		String Producttype="";
		mainClasses.indentListing INDT_L = new indentListing();
		mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
		mainClasses.employeesListing EMP_L = new employeesListing();
		productsListing PRD_L = new productsListing();
		vendorsListing VND_L = new mainClasses.vendorsListing();
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List INDT_List=INDT_L.getMindentsByIndentinvoice_id(INVId); 
		
		if(INDT_List.size()>0){
			INDT=(indent)INDT_List.get(0);
			String temp[]=INDT.getvendor_id().split(" ");
			List VNDT_LIST=VND_L.getMvendors(temp[0]);
			VND=(vendors)VNDT_LIST.get(0);
			Producttype=INDT.gethead_account_id();
			%>
<div class="vendor-page">
<div class="vendor-box">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Indent Approvals List</td></tr>
</table>
<div class="name-title"><%=INDT.getvendor_id()%> <span>(<%=HOA_CL.getHeadofAccountName(INDT.gethead_account_id())%>)</span></div>
<div style="clear:both;"></div>
<div class="details-list">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="1"><span>Vendor Name :</span></td>
	<td>
		<% if(VND.getfirstName()!=null){out.println(VND.gettitle());out.println(VND.getfirstName()); out.println(VND.getlastName());}else{out.println("Not Available");}%>
	</td>
</tr>
<tr>
<td colspan="1"><span>Vendor Address :</span></td>
	<td>
		<% if(VND.getaddress()!=null){out.println(VND.getaddress());%>,<%out.println(VND.getcity());%>,<%out.println(VND.getpincode());}else{out.println("Not Available");}%>
	</td>
</tr>
<tr>
<td colspan="1"><span>Vendor Phone No :</span></td>
	<td>	
		<%if(VND.getphone()!=null && !VND.getphone().equals("") ){out.println(VND.getphone());}else{out.println("Not Available");};%>
	</td>
</tr>
<tr>
<td colspan="1"><span>Vendor Email Id :</span></td>
	<td>
		<% if(VND.getemail().equals("") || VND.getemail()==null)
		{
			out.print("Not Available");
		}
		else
		{
			out.print(VND.getemail());
		}%>
	</td>
</tr>
<tr>
<td colspan="1"><span>Indent Raised By:</span></td>
<td>
<%
	if (INDT.getemp_id() != null && !INDT.getemp_id().equals(""))	{
		out.println(EMP_L.getMemployeesName(INDT.getemp_id()));
	}
	else	{
		out.println("No Data");
	}
%>
</td>
<!-- <td colspan="1"><span>Indent Raised By :</span></td><td>GS</td> -->
</tr>
<tr>
<td colspan="1"><span>Indent Raised Date :</span></td>
	<td>
		<%
			if(INDT.getdate()!=null)
			{
				out.println(INDT.getdate());
			}
			else
			{
				out.println("Not Available");
			}
		%>
	</td>
</tr>
</table>
</div>
<div><center><%if(statuss.equals("disapproved")){ %>Disapproved<%}else{ %>Approved<%} %> By:<br><br>
<%
	if(request.getParameter("approvedBy")!=null)
	{
		String approvedBy=request.getParameter("approvedBy");
		out.println(approvedBy);
	}
	else
	{
		out.println("Not-Approved");
	}

%>
</center></div>
</div>
<div class="icons">
<div style="margin-bottom: 20px;float: left;cursor: pointer;"><img src="../images/back-button (1).png" width="100" height="50" onclick="goBack()" title="print"/></div>
</div>

<div class="vendor-list">

<%if(INDT.getextra4()!=null && !INDT.getextra4().equals("")){ %>
<div style="font-weight: bold;color: red;cursor: pointer;" onclick="tracktransaction()">Track Transaction : <span><%=INDT.getextra4() %></span></div><%}  %>
<div class="clear"></div>

<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<div id="refresh">
<form action="indentapprovals_Insert.jsp" method="POST" onsubmit="blockPage();" id="indentAppForm">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="5%"></td>
<td class="bg" width="25%">Vendor Name</td>
<td class="bg" width="15%">Product Name</td>
<td class="bg" width="20%">Quantity Raised</td>
<td class="bg" width="20%">BalQtyAtGodown</td>
<td class="bg" width="20%">Consumption(last 15 days)</td>
<!-- <td class="bg" width="20%">Balance Quantity At Store</td> -->
</tr>

<%/* banktransactionsListing BAK_L=new mainClasses.banktransactionsListing(); */ 
indentapprovalsListing APPL=new indentapprovalsListing();
shopstockListing shopDao = new shopstockListing();
productsListing prodDao = new productsListing();
employeesListing EMP=new employeesListing();
INDT_List=INDT_L.getMindentsByIndentinvoice_id(INVId); 
DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
DecimalFormat decimalFormat= new DecimalFormat("###,###.###");
Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));
String fromdate1 =  dateFormat1.format(cal1.getTime());
double consumption = 0d;
String todate1 = "";
cal1.setTime(dateFormat1.parse(fromdate1));
cal1.add(Calendar.DATE,-14);
todate1 = dateFormat2.format(cal1.getTime());
List<shopstock>	stocklist = shopDao.getShopStkListBsdOnDatesAndIds(todate1+" 23:59:59",fromdate1+" 00:00:00","",INDT.gethead_account_id(),"");
if(INDT_List.size()>0){
	for(int i=0;i<INDT_List.size();i++){
		INDT=(indent)INDT_List.get(i);		
		for (int j = 0 ; j < stocklist.size() ; j++)	{
			shopstock shop = stocklist.get(j);
			if (shop.getproductId().equals(INDT.getproduct_id()))
			consumption += Double.parseDouble(shop.getquantity());
		}
%>
<tr>
<td style="cursor: pointer;"><input type="hidden" name="count" id="check<%=i%>"  value="<%=i%>"/> 
<input type="hidden" name="indentId<%=i%>" id="indentId" value="<%=INDT.getindent_id()%>">
<input type="hidden" name="headAccountId" id="headAccountId" value="<%=INDT.gethead_account_id()%>">
<img src="../images/delete.png" onclick="deleteProduct('<%=INDT.getindent_id()%>');"></td>
<td><input type="text" name="vendorId<%=i%>" id="vendorId<%=i%>"  value="<%=INDT.getvendor_id()%> <%=VND_L.getMvendorsAgenciesName(INDT.getvendor_id()) %>" onkeyup="vendorsearch('<%=i%>')"/></td>
<td><%=PRD_L.getProductsNameByCat1(INDT.getproduct_id(),Producttype) %></td>
<td><input type="text" name="quantity<%=i%>" id="quantity<%=i%>" placeholder="Enter quantity" onblur="calculate('<%=i %>')"  onKeyPress="return numbersonly(this, event,true);" value="<%=INDT.getquantity() %>" style="width:100px;"></input></td>
<td><%=decimalFormat.format(Double.parseDouble(PRD_L.getProductsStockByCat(INDT.getproduct_id(),Producttype)))%></td>
<td><%=decimalFormat.format(consumption)%></td>
</tr>
<%}}

mainClasses.superadminListing listing=new mainClasses.superadminListing ();
boolean emergencyApprovalPerson= listing.getEmergencyApproval(session.getAttribute("superadminId").toString());
/* System.out.println("emergencyApprovalPerson..."+emergencyApprovalPerson); */
if(!session.getAttribute("department").toString().equals("") && !"EmergencyApproval".equals(INDT.getrequirement())){
	List INd_APL=APPL.getIndentApprovedByAdmin(INVId,session.getAttribute("superadminId").toString());
if(INd_APL.size()>0){ %>
<%
	INDAPPROV = (indentapprovals)INd_APL.get(0);
%>
<!-- <tr><td colspan="4" style="color: orange;font-weight: bold;" align="center">Note : This indent report approved by you!</td></tr> -->
<tr><td colspan="4" style="color: orange;font-weight: bold;" align="center">Note : This indent report <%=INDAPPROV.getadmin_status()%> by you!</td></tr>
<%}else{
	
	%>
<tr>
<td colspan="2">
<input type="hidden" name="indentInvoiceId" value="<%=INVId%>">
<select name="status">
<option value="approved">Approve</option>
<option value="disapproved">Dis-Approved</option>
</select></td>
<td><textarea name="feedback" id="feedback"></textarea></td>
<!-- <td><input type="submit" name="submit" value="APPROVE"></td> -->
<td><input type="submit" name="submit" . value="SUBMIT" class="click"></td>
</tr>

<%} 
}

// added by gowri shankar
else if((session.getAttribute("role").equals("FS") || session.getAttribute("role").equals("GS") || session.getAttribute("role").equals("CM")) && !"EmergencyApproval".equals(INDT.getrequirement())){
	List INd_APL=APPL.getIndentApprovedByAdmin(INVId,session.getAttribute("superadminId").toString());
if(INd_APL.size()>0){ 
	INDAPPROV = (indentapprovals)INd_APL.get(0);
%>
<tr><td colspan="4" style="color: orange;font-weight: bold;" align="center">Note : This indent report <%=INDAPPROV.getadmin_status()%> by you!</td></tr>
<%  }else{ 
	List indentlist = INDT_L.getindentGroupByIndentinvoice_idWithDate("",INVId,"","");
	if(indentlist.size() > 0)	{
		indent id = (indent)indentlist.get(0);
		long diff = 0l,hrs = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("IST"));
		Date todate = cal.getTime();
		Date fromdate = format1.parse(id.getdate());
		
		diff = todate.getTime() - fromdate.getTime();
		hrs = diff / (60 * 60 * 1000);	
		System.out.println("hours--"+hrs);
		if (!id.getstatus().equals("3indentApproved") && !id.getstatus().equals("reportPending") && hrs >= 48)	{%>
		<tr>
		<td colspan="2">
		<input type="hidden" name="indentInvoiceId" value="<%=INVId%>">
		<input type="hidden" value="" id="role">
		<select name="status">
		<option value="approved">Approve</option>
		<option value="disapproved">Dis-Approved</option>
		</select></td>
		<td><textarea name="feedback" id="feedback"></textarea></td>
		<td><input type="submit" name="submit" value="SUBMIT" id="submitBtn" class="click"></td>
		</tr>
		<%} %>
	
<%} //if
 } //else 
} // else if
//added by gowri shankar
 

//"VC-III".equals(session.getAttribute("role").toString())
else if(emergencyApprovalPerson){

if(INDT.getrequirement().equals("EmergencyApproval"))
{
	List INd_APL=APPL.getIndentApprovedByAdmin(INVId,session.getAttribute("superadminId").toString());
	if(INd_APL.size()>0){%>
	<%
	INDAPPROV = (indentapprovals)INd_APL.get(0);
	%>
	<!-- <tr><td colspan="4" style="color: orange;font-weight: bold;" align="center"> ====== Note : This indent report approved by you!</td></tr> -->
	<tr><td colspan="4" style="color: orange;font-weight: bold;" align="center"> ====== Note : This indent report <%=INDAPPROV.getadmin_status()%> by you!</td></tr>
	<%}else{
		%>
	<tr>
	<td colspan="2">
	<input type="hidden" name="indentInvoiceId" value="<%=INVId%>">
	<select name="status">
	<option value="approved">Approve</option>
	<option value="disapproved">Dis-Approved</option>
	</select>
	<input type="hidden" name="EmergencyApproval" value="<%=INDT.getrequirement() %>" /></td>
	<td><textarea name="feedback" id="feedback"></textarea></td>
	<!-- <td><input type="submit" name="submit" value="APPROVE"></td> -->
	<td><input type="submit" name="submit" value="SUBMIT" class="click"></td>
	</tr>

	<%}
}
}%>
</table>
</form>
</div>
</div>
</div>




</div>
<%}else{response.sendRedirect("adminPannel.jsp?page=banks");}%>

<!--Below code is Commented by madhav for not allowing indent approvals to gs and fs.... place this code if required before else if(emergencyApprovalPerson)-->

<%-- else if(session.getAttribute("role").equals("FS") || session.getAttribute("role").equals("GS") && !"EmergencyApproval".equals(INDT.getrequirement())){
	List INd_APL=APPL.getIndentApprovedByAdmin(INVId,session.getAttribute("superadminId").toString());
if(INd_APL.size()>0){ 
	INDAPPROV = (indentapprovals)INd_APL.get(0);
%>
<tr><td colspan="4" style="color: orange;font-weight: bold;" align="center">Note : This indent report <%=INDAPPROV.getadmin_status()%> by you!</td></tr>
<%  }else{ 
	
	%>
<tr>
<td colspan="2">
<input type="hidden" name="indentInvoiceId" value="<%=INVId%>">
<select name="status">
<option value="approved">Approve</option>
<option value="disapproved">Dis-Approved</option>
</select></td>
<td><textarea name="feedback" id="feedback"></textarea></td>
<td><input type="submit" name="submit" value="SUBMIT"></td>
</tr>

<%} }  --%>

