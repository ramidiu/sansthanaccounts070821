<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing,java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript" lang="javascript">
	var openMyModalSj = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 450;
		modalWindow.height = 200;
		modalWindow.content = "<iframe width='450' height='200' frameborder='0' scrolling='yes' allowtransparency='false' src='pendingIndentsPopup.jsp'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
	<jsp:useBean id="HOD" class="beans.headofaccounts"></jsp:useBean>
	<jsp:useBean id="SALE" class="beans.customerpurchases" />
<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down">ONLINE SALES REPORT</div>
<div class="icons">
<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>
<div class="clear"></div>
<div class="total-report">
<%headofaccountsListing HODA=new headofaccountsListing();
List HODET=HODA.getheadofaccounts();
%>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="4" class="bg-new" style="color: #F00;">*** SHRI SHIRDI SAI BABA SANSTHAN TRUST ***</td></tr>
<form action="adminPannel.jsp" method="post">
<tr>
<td width="20%">
<%-- <ul>
<%if(HODET.size()>0){ 
	for(int i=0;i<HODET.size();i++){
		HOD=(headofaccounts)HODET.get(i);%>
	<li><input type="checkbox" name="hoid" id="hoid" value="<%=HOD.gethead_account_id()%>"/> <%=HOD.getname() %></li>
<%}} %>
</ul> --%>
</td>
<%
String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
<td width="30%"><input type="hidden" name="page" value="Online-sale-report">From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="30%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" name="search" value="Search" class="click bordernone"/></td>
</tr>
</form>
 <form action="adminPannel.jsp" method="post">  
 <tr>
	<td><input type="hidden" name="page" value="Online-sale-report"></input><input type="submit" name="today" value="Today" class="click bordernone"/></td>
	<td><input type="submit" name="weekly" value="Week Report" class="click bordernone"/></td>
	<td><input type="submit" name="monthly" value="Month Report" class="click bordernone"/></td>
	<td><input type="submit" name="yearly" value="Year Report" class="click bordernone"/></td>
</tr>
</form>
</table>
<div id="tblExport">
<%
mainClasses.customerpurchasesListing SALE_L = new mainClasses.customerpurchasesListing();
mainClasses.banktransactionsListing BKTRAL=new mainClasses.banktransactionsListing();
subheadListing SUBl=new  subheadListing();
employeesListing EMPL=new employeesListing();
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
DateFormat  onlyYear= new SimpleDateFormat("yyyy");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String currentDate=(dateFormat2.format(c1.getTime())).toString();
String fromDate=currentDate+" 00:00:01";
String toDate=currentDate+" 23:59:59";
if(request.getParameter("today")!=null && request.getParameter("today").equals("Today")){
	fromDate=currentDate+" 00:00:01";
	toDate=currentDate+" 23:59:59";
}else if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
	c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
	fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
	toDate=(dateFormat2.format(c1.getTime())).toString();
	toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
	c1.getActualMaximum(Calendar.DAY_OF_MONTH);
	int lastday = c1.getActualMaximum(Calendar.DATE);
	c1.set(Calendar.DATE, lastday);  
	toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
	c1.getActualMinimum(Calendar.DAY_OF_MONTH);
	int firstday = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday); 
	fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
	int lastday_y = c1.get(Calendar.YEAR);
	c1.set(Calendar.DATE, lastday_y);  
	c1.getActualMinimum(Calendar.DAY_OF_YEAR);
	int firstday_y = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday_y); 
	Calendar cld = Calendar.getInstance();
	cld.set(Calendar.DAY_OF_YEAR,1); 
	fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
	cld.add(Calendar.YEAR,+1); 
	toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate")+" 00:00:01";
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate")+" 23:59:59";
	}
}
List SAL_list=null;
Double totalSaleAmt=0.00;
Double totDeposit=0.00;
Double openingBal=0.00;
DecimalFormat df = new DecimalFormat("0.00");
String prevDate="";
String presentDate="";
double bankDepositTot=0.00;
double creaditSaleTot=0.00;
double cashSaleAmt=0.00;
double onlinesaleamount=0.00;
double totalAmount=0.00;
double poojaStoreTotAmt=0.00;
double sansthanTotAmt=0.00;
double charityTotAmt=0.00;
double sainivasTotAmt=0.00;
double servCharg=0.00;
double sansthanTotservCharg=0.00;
double charityTotservCharg=0.00;
double sainivasTotservCharg=0.00;
if(HODET.size()>0){ 
	for(int i=0;i<HODET.size();i++){
		HOD=(headofaccounts)HODET.get(i);
		SAL_list=null;//SALE_L.getTotalOnlineSaleReport(HOD.gethead_account_id(),fromDate,toDate);
		if(!HOD.gethead_account_id().equals("3")){
		
		%>
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="9" class="bg-new" style="color:#F00;cursor: pointer;" onclick="showDetails('<%=HOD.gethead_account_id()%>');"><%=HOD.getname() %></td></tr>
	<%if(SAL_list.size()>0){ %>
	<tr ><td colspan="9">
	<table width="100%" cellpadding="0" cellspacing="0" class="date-wise" style="display: none;" id="<%=HOD.gethead_account_id()%>">
	<tr>
		<td style="font-weight: bold;">S.NO.</td>
		<td style="font-weight: bold;">INVOICE DATE</td>
		<td style="font-weight: bold;">INVOICE.NO.</td>
		<td style="font-weight: bold;">NAME</td>
		<td style="font-weight: bold;">SEVA/DONATION</td>
		<td style="font-weight: bold;">Address</td>
		<td style="font-weight: bold;" width="10%">Date of seva</td>
		<td style="font-weight: bold;" >Gothram</td>
		<td style="font-weight: bold;">Mobile No</td>
		<td style="font-weight: bold;">QTY</td>
		<td style="font-weight: bold;" align="right">AMOUNT</td>
		<td style="font-weight: bold;" align="right">SERVICE CHARGES</td>
		<td style="font-weight: bold;" align="right">TOTAL AMOUNT</td>
	</tr>
	<%if(SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate)!=null && !SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate).equals("")){
		 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate));
	}
	if(BKTRAL.getEmployeeDepositAmount("",fromDate)!=null && !BKTRAL.getEmployeeDepositAmount("",fromDate).equals("")){
		 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount("",fromDate));}
	for(int s=0; s < SAL_list.size(); s++ ){ 
				SALE=(beans.customerpurchases)SAL_list.get(s);
				if(!presentDate.equals("")){
								prevDate=presentDate;	
							}
							presentDate=""+df2.format(dateFormat.parse(SALE.getdate()));
							if(SALE.getcash_type().equals("cash")){
								cashSaleAmt=cashSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							} else if(SALE.getcash_type().equals("online success")){
								onlinesaleamount=onlinesaleamount+Double.parseDouble(SALE.gettotalAmmount());
							} else{
								creaditSaleTot=creaditSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}%>
	<tr style="position: relative;">
			<td ><%=s+1%></td>
			<td><%=df2.format(dateFormat.parse(SALE.getdate()))%></td>
			<td><%=SALE.getbillingId()%></td>
			<td><%=SALE.getcustomername()%></td>
			<td><%=SUBl.getMsubheadname(SALE.getproductId()) %></td>
			<td><%=SALE.getextra6() %></td>
			<td><%if(SALE.getextra11()!=null && SALE.getextra11().equals("")){%><%=SALE.getextra11() %><%}else{ %><%=df2.format(dateFormat.parse(SALE.getdate()))%> <%} %></td>
			<td><%=SALE.getgothram() %></td>
			<td><%=SALE.getphoneno() %></td>
			<td><%=SALE.getquantity()%></td>
			<%totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());
			if(SALE.getextra12()!=null && !SALE.getextra12().trim().equals("")){
			servCharg=servCharg+Double.parseDouble(SALE.getextra12());
			}%>
			<td style="align="right"><%=df.format(Double.parseDouble(SALE.getrate()))%></td>
			<td style="align="right">
			<%
				if(SALE.getextra12()!=null && !SALE.getextra12().equals("")){
				%>
			<%=df.format(Double.parseDouble(SALE.getextra12()))%>
			<%} %>
			</td>
			<td style="align="right"><%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td>
	</tr>
	<%} %>
	</table>
	</td></tr>
	<%-- <tr>
		<td colspan="5"  style="text-align:right">Total Cash Sales Amount</td>
		<td><%=df.format(cashSaleAmt) %></td>
		<td colspan="2"><%=df.format(bankDepositTot) %></td>
			</tr> --%>
			<%if(!HOD.getname().equals("POOJA STORES")){  %>
				<tr>
		<td colspan="7"  style="text-align:right">Total Online Sales Amount</td>
		<td><%=df.format(servCharg)%></td>
		<td><%=df.format(onlinesaleamount) %></td>
			</tr>
			<%} %>
	<%-- <tr>
		<td colspan="5"  style="text-align:right">Total Credit Sales Amount</td>
		<td><%=df.format(creaditSaleTot) %></td>
		<td colspan="2"></td>
	</tr> --%>
	<!-- <tr>
		<td colspan="5"  style="text-align:right">Total Sales Amount</td> -->
		<%if(HOD.getname().equals("POOJA STORES")){ 
				poojaStoreTotAmt=totalAmount;}else if(HOD.getname().equals("SANSTHAN")){
					sansthanTotservCharg=servCharg;
					sansthanTotAmt=totalAmount;
				}else if(HOD.getname().equals("CHARITY")){
					charityTotservCharg=servCharg;
					charityTotAmt=totalAmount;
				}else if(HOD.getname().equals("SAI NIVAS")){
					sainivasTotservCharg=servCharg;
					sainivasTotAmt=totalAmount;
				}%>
		<%-- <td><%=df.format(totalAmount)%></td>
		<td colspan="2"></td>
	</tr> --%>
	<%
	servCharg=0.00;
	cashSaleAmt=0.00;
	bankDepositTot=0.00;
	totalAmount=0.00;
	creaditSaleTot=0.00;
	onlinesaleamount=0.00;
	}else{ %>
	<tr><td colspan="9" style="color: #000;">No sales!</td></tr>
	<%} %>
</table>
<% }}} %>
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="9" class="bg-new">Grand Total</td></tr>
<%-- <tr>
<td colspan="6"  style="text-align:right" width="75%">POOJA STORE</td>
<td colspan="3"><%=df.format(poojaStoreTotAmt) %></td>
</tr> --%>
<tr>
<td colspan="6"  style="text-align:right" width="75%">SANSTHAN</td>
<td><%=df.format(sansthanTotservCharg) %></td>
<td colspan="2"><%=df.format(sansthanTotAmt) %></td>
</tr>
<tr>
<td colspan="6"  style="text-align:right" width="75%">CHARITY</td>
<td><%=df.format(charityTotservCharg) %></td>
<td colspan="2"><%=df.format(charityTotAmt) %></td>
</tr>
<tr>
<td colspan="6"  style="text-align:right" width="75%">SAINIVAS</td>
<td><%=df.format(sainivasTotservCharg) %></td>
<td colspan="2"><%=df.format(sainivasTotAmt) %></td>
</tr>
<tr>
<td colspan="6"  style="text-align:right"><span>Grand Total</span></td>
<td><%=df.format(sansthanTotservCharg+charityTotservCharg+sainivasTotservCharg) %></td>
<td colspan="2"><%=df.format(poojaStoreTotAmt+sansthanTotAmt+charityTotAmt+sainivasTotAmt) %></td>
</tr>
</table>
</div>
</div>
</div>
</div>
</body>
</html>