<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sai sansthan accounts-Admin Pannel</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script language="javascript">
function combochange(denom,desti,jsppage) { 
        var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value; 
        document.getElementById(desti).options.length = 0;
		var sda1 = document.getElementById(desti);
		$.post(jsppage,{ id: com_id } ,function(data)
 		{
		var where_is_mytool=data;
		//alert(where_is_mytool);
		var mytool_array=where_is_mytool.split("\n");
		var ab=mytool_array.length-5;
		//alert(mytool_array.length);
		for(var i=0;i<mytool_array.length;i++)
		{
		if(mytool_array[i] !="")
		{
		//alert (mytool_array[i]);
		var y=document.createElement('option');
		var val_array=mytool_array[i].split(":");
		
					y.text=val_array[1];
					y.value=val_array[0];
					try
					{
					sda1.add(y,null);
					}
					catch(e)
					{
					sda1.add(y);
					}
		}
		}
		});
   } 
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function goBack() {
    window.history.back()
}
</script>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
</head>
<%-- <%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %> --%>
<body>
<%

if(session.getAttribute("superadminId")!=null){
	
	%>
<div><%@ include file="title-bar.jsp"%></div>
<%
if(request.getParameter("page")!=null && !request.getParameter("page").equals("")){
	 String includejsp=request.getParameter("page")+".jsp";
%>
<div><jsp:include page="<%=includejsp%>" flush="true"/></div>
<%}else{%><div><jsp:include page="vendors.jsp" flush="true"/></div>
<%}
}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>