<%@page import="mainClasses.CounterBalanceAndBankDeposits"%>
<%@page import="model.SansthanAccountsDate"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="java.util.ArrayList"%>
<%@page import="beans.minorhead"%>
<%@page import="beans.customerpurchases"%>
<%@page import="beans.productexpenses"%>
<%@page import="beans.bankbalance"%>
<%@page import="mainClasses.ReportesListClass"%>
<%@page import="beans.vendors"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.accounts.saisansthan.vendorBalances"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RECEIPTS AND PAYMENTS REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>


<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;

}
.new-table td{
padding: 2px 0 2px 5px !important;}

@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
</style>

<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 

</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
        function minorhead(id){
        	$('#'+id).toggle();
        }
    </script>
   

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
</head>

<body>
<%

	if(session.getAttribute("superadminId")!=null){ 
		
	
		DecimalFormat df=new DecimalFormat("#,###.00");
		Calendar c1 = Calendar.getInstance(); 
// 		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		
		String fromdate="";
		String onlyfromdate="";
		String todate="";
		
		String cashtype="online pending";
		String cashtype1="othercash";
		String cashtype2="offerKind";
		String cashtype3="journalvoucher";
		
// 		minorheadListing MINH_L=new minorheadListing();
		String onlytodate="";
// 		productexpensesListing PRDEXP_L=new productexpensesListing();

		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		
		c1.set(Calendar.DATE, lastday);  
		todate=(dateFormat2.format(c1.getTime())).toString();
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		
		int firstday = c1.getActualMinimum(Calendar.DATE);
		
		c1.set(Calendar.DATE, firstday); 
		fromdate=(dateFormat2.format(c1.getTime())).toString();
		
		if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
									 fromdate=request.getParameter("fromDate");
								}
								if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
									todate=request.getParameter("toDate");
								}
		headofaccountsListing HOA_L=new headofaccountsListing();
	List headaclist=HOA_L.getheadofaccounts();
	String hoaid="1";
	if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
		hoaid=request.getParameter("head_account_id");
	}
	
	%>
<div class="vendor-page">

		<div>
				<form name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 70px;padding-top: 30px;">
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td colspan="2">Select Financial<br/>Year</td>
<td class="border-nn">&nbsp;&nbsp;From Date</td>
<td class="border-nn">To Date</td>
<td class="border-nn" colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<!-- <td class="border-nn">Report Type</td> -->
</tr>
				<tr>
					
			<td colspan="2">
			<select name="finYear" id="finYear"  Onchange="datepickerchange();">
						<%@ include file="yearDropDown1.jsp" %>
					</select>
			</td> 
		<td class="border-nn">&nbsp;&nbsp;<input type="text"  name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>" /> </td>
				<td class="border-nn"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
				<input type="hidden" name="page" value="trailBalanceReport_newBySitaram"/>
				 </td>
<td colspan="2" class="border-nn"><select name="head_account_id" id="head_account_id" onchange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select>
<input type="hidden" name="cumltv" value="noncumulative" />
</td>
<!-- <td> -->

<!-- 	<select name="cumltv" id="cumltv"> -->
<%-- 	<option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option> --%>
<%-- 	<option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option> --%>
<!-- 	</select> -->
<!-- </td> -->
<td class="border-nn"><input type="submit" value="SEARCH" class="click"/></td>
</tr></tbody></table> </form>
				<%
// 				String finyr[]=null;	
// 				String headgroup="";
// 				String subhid="";
// 				double total=0.0;
				double credit=0.0;
				double debit=0.0;
// 				double creditincome=0.0;
// 				double debitincome=0.0;
// 				double creditexpens=0.0;
// 				double debitexpens=0.0;
// 				double creditliab=0.0;
// 				double debitliab=0.0;
// 				Object key =null;
// 				Object value=null;
// 				Object key1 =null;
// 				Object value1=null;
// 				double value2=0.0;
// 				double amt1=0.0;
// 				double amt2=0.0;
// 				double amt3=0.0;
// 				double amt4=0.0;
				
// 				productexpensesListing PEXP_L=new productexpensesListing();
// 				banktransactionsListing BNK_L = new banktransactionsListing();
// 				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat onlydat2=new SimpleDateFormat("dd-MMM-yyyy");
// 				vendorsListing VNDR=new vendorsListing();
// 				vendors vendor = new vendors(); 
// 				vendorBalances VNDBALL=new vendorBalances();
// 				bankbalanceListing BBL =new bankbalanceListing();
				
// 				subheadListing SUBH_L=new subheadListing();
// 				customerpurchasesListing CP_L=new customerpurchasesListing();
// 				majorheadListing MH_L=new majorheadListing();
				

			
				
// 				List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid, "revenue",headgroup);
// 				List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"expenses",headgroup);
// 				List assetsmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"Assets",headgroup);
// 				List liabilmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"Liabilities",headgroup);
				
				%>
				<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
														
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span> <span>
						
					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	
	
	<div class="total-report">

<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<% if(request.getParameter("fromDate") != null && !request.getParameter("toDate").equals("") && request.getParameter("finYear") != null){
		bankbalanceListing BBAL_L=new bankbalanceListing();
		String finYr = request.getParameter("finYear");
		String finStartYr = request.getParameter("finYear").substring(0, 4);
		String fdate = finStartYr+"-04-01"+" 00:00:00";
		if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("noncumulative")){
			fdate = request.getParameter("fromDate")+" 00:00:00";
		}
		String tdate = request.getParameter("toDate")+" 23:59:59";
	%>				
	<div class="printable" id="tblExport">	
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>

<table width="90%" cellpadding="0" cellspacing="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
<tbody><tr>
						<td colspan="3" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hoaid) %></td>
					</tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Receipts And Payments Report</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=onlydat2.format(onlydat.parse(fdate.substring(0, 10))) %> TO <%=onlydat2.format(onlydat.parse(request.getParameter("toDate"))) %> </td></tr>



</tbody></table>
<div style="clear:both"></div>

<table width="90%" border="1" cellspacing="0" cellpadding="0"  style="margin:0 auto;" class="new-table">
  <tbody><tr>
    <th width="38%" height="29" align="center">Account Name</th>
    <th width="29%" align="center">Debits</th>
    <th width="33%" align="center">Credits</th>
  </tr>
  
  <%
  
  
  
  double debit1 = 0.0;
  double credit1 = 0.0;
  double balance1 = 0.0;
  double bankopeningbal=0.0;
//   double bankopeningbalcredit=0.0;
	double finOpeningBal=0.0;
	double finOpeningBal2=0.0;
	double onlinedeposits = 0.0;
// 	double totalBanksAndCashAmount = 0.0;
	double totalcountercash =0.0;
	String reporttype="";
	String reporttype1="";
	double totalcashpaid =0.0;
	double grandtotal = 0.0;
	double proopeningbal=0.0;
	
	
  DecimalFormat DF=new DecimalFormat("#,###.00"); 
  String typeofbanks[]={"bank","cashpaid"};
  String headgroup="";
  
  
  	if(request.getParameter("head_account_id")!=null){
		hoaid=request.getParameter("head_account_id");
	

		 if(hoaid.equals("4")){
			
			headgroup="1";
			reporttype = "pro-counter";
			reporttype1 = "Development-cash";	
		}else if(hoaid.equals("1")){
			reporttype = "Charity-cash";
			reporttype1 = "Not-Required";	
		}
		else if(hoaid.equals("3")){
			reporttype = "poojastore-counter";
			reporttype1 = "Not-Required";
		}
		else if(hoaid.equals("5")){
			reporttype = "Sainivas";
			reporttype1 = "Not-Required";	
		} 
	}
  
  
  	
 	 ReportesListClass reportesListClass = new ReportesListClass();
	bankdetailsListing BNKL=new bankdetailsListing();
	bankcalculations BNKCAL=new bankcalculations();
	customerpurchasesListing CP_L=new customerpurchasesListing();
	banktransactionsListing BNK_L = new banktransactionsListing();
	
	  
  	Object key = null;
	Object value = null;
	Object key1 = null;
	Object value1 = null;
	double value2 = 0.0;
  	
  vendorBalances VNDBALL = new vendorBalances();
  vendorsListing VNDR = new vendorsListing();
	
 	List<majorhead> majorHeadList = new ArrayList<majorhead>();
  
	List<majorhead> incomemajhdlist = reportesListClass.getMajorHeadesBasedOnNatcherAndHeadOfAccount(hoaid, "revenue");
	List<majorhead> expendmajhdlist = reportesListClass.getMajorHeadesBasedOnNatcherAndHeadOfAccount(hoaid, "expenses");
	List<majorhead> assetsmajhdlist = reportesListClass.getMajorHeadesBasedOnNatcherAndHeadOfAccount(hoaid, "Assets");
	List<majorhead> liabilmajhdlist = reportesListClass.getMajorHeadesBasedOnNatcherAndHeadOfAccount(hoaid, "Liabilities");
	
  
	List<bankbalance> debitAmountOpeningBalList = null;
	List<bankbalance> creditAmountOpeningBalList = null;
	
	
	if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
	{
		debitAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hoaid, "", "", "", finYr,"Assets", "debit", "extra5", "MONTHNAME");
		creditAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hoaid, "", "", "", finYr,"Liabilites", "credit", "extra5", "MONTHNAME");
	} 
	
	
	for(int aa = 1; aa <= 4; aa++){
	
		
  		if(aa == 1){
  			majorHeadList = incomemajhdlist;
  			%>
  			<tr>
  		   		<td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">RECEIPTS</td>
  			</tr>
  			
  		
  		<%
  		List banklist1 = BNKL.getBanksBasedOnHOA(hoaid);
				/* String bank=""; */
				
				  if(banklist1.size()>0)
				  { 
					  int i=0;
				  
					for(i=0;i<banklist1.size();i++)
					{
						BNK=(bankdetails)banklist1.get(i);
						for(int j=0;j<typeofbanks.length;j++)
						{

							bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",fromdate,typeofbanks[j],"");


							finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], request.getParameter("finYear"));
							bankopeningbal += finOpeningBal;
							if(bankopeningbal >0 || bankopeningbal<0)
							{		
				%>
				
				<tr>
				    <td height="28" style="color:#F00; font-weight:bold;">
				    <a href="<%=request.getContextPath()%>/superadmin/adminPannel.jsp?page=bankDetails&id=<%=BNK.getbank_id() %>&reportType=<%=typeofbanks[j]%>&finYear=<%=finYr%>&fromdate=<%=fromdate %>&todate=<%=todate%>" target="_blank">
	  					<strong><%=BNK.getbank_id() %> <%=BNK.getbank_name() %> <%=typeofbanks[j]%></strong>
	  				</a>
				    </td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(bankopeningbal)%></td>
				</tr>
    			<%
    					credit = credit + bankopeningbal;
    			//System.out.println("bankopeningbalcredit..."+bankopeningbalcredit);
				//bankopeningbal=0.0;
							}
						}
							} 
							
				String addedDate1=SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
				
				 finOpeningBal2 = BBAL_L.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));				
				 
				 
				 String custPer = CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate);
				 String banktrns = BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1);
				 
	  			if( custPer != null && !custPer.equals(""))
	  			{
					totalcountercash = Double.parseDouble(custPer);
	  			}
	  			if( banktrns != null && !banktrns.equals(""))
	  			{
					totalcashpaid = Double.parseDouble(banktrns);
	  			}
	  			
	  			
// 	  			if(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1)!= null && !BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1).equals(""))
// 	  			{
// 					onlinedeposits = Double.parseDouble(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1));
// 	  			}
// 	  			grandtotal = finOpeningBal2+totalcountercash-totalcashpaid-onlinedeposits;

				grandtotal = finOpeningBal2 + totalcountercash - totalcashpaid;
	  			proopeningbal = grandtotal;
				  }
				  credit = credit +	proopeningbal ;
				  
				 
				  
				  %>
  			
  			<tr>
				    <td height="28" style="color:#F00; font-weight:bold;">
				    	<a href="<%=request.getContextPath()%>/superadmin/adminPannel.jsp?page=DailycounterBalanceReport_new&finYear=<%=finYr %>&page=totalsalereport&fromDate=<%=fromdate %>&toDate=<%=todate %>&head_account_id=<%=hoaid %>" target="_blank">
	  						<strong>CASH ACCOUNT</strong>
	  					</a>
				    </td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(proopeningbal)%></td>
				</tr>
  			
  			<%
  		}else if(aa == 2){
  			majorHeadList = expendmajhdlist;
  			%>
  			<tr>
  		   		<td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">PAYMENTS</td>
  			</tr>
  			<%
  		}else if(aa == 3){
  			majorHeadList = assetsmajhdlist;
  			%>
  			<tr>
  		   		<td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">ASSETS</td>
  			</tr>
  			<%
  		}else{

  			majorHeadList = liabilmajhdlist;
  			%>
  			 <tr>
  		    	<td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">LIABILITIES</td>
  		 	</tr>
  		 <%
  		}
  		
		if(majorHeadList != null && majorHeadList.size() > 0){
	  		for(int i = 0 ; i < majorHeadList.size(); i++){
	  			
	  			MH = (majorhead) majorHeadList.get(i);
	  			
// 	  			List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "sub_head_id", "MONTHNAME");
	  			
	  			List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "sub_head_id", "MONTHNAME", "not a loan bank");
	  			List<banktransactions> otherAmountListForLoanBank = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "sub_head_id", "MONTHNAME", "loan bank");
	  			
	  			
	  			List<productexpenses> productexpensesList = reportesListClass.getSumOfAmountFromProductexpenses(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "pe.sub_head_id", "MONTHNAME");
				List<customerpurchases> customerpurchases = reportesListClass.getWithBankJVAmountsFromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, cashtype3, "productId", "MONTHNAME");
// 				List<productexpenses> productexpensesJVDebit =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, MH.getmajor_head_id(), "", "", cashtype3, fdate, tdate, "pe.sub_head_id", "MONTHNAME", "debit");

				// jv amount from productexpensec withbank and withloanbank and withvendor 
				List<productexpenses> productexpensesJV =	reportesListClass.getWithBankJVAmountsFromproductexpenses(hoaid, MH.getmajor_head_id(), "", "", cashtype3, fdate, tdate, "pe.sub_head_id", "MONTHNAME", "credit");
				List<customerpurchases> DollarAmt = reportesListClass.getSumOfDollorAmountfromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, cashtype1, "s.sub_head_id", "MONTHNAME");
				List<customerpurchases> cardCashChequeJvAmountList = reportesListClass.getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "s.sub_head_id", "MONTHNAME");
				
	  			
				double otherAmount = 0.0;
				if(otherAmountList.size()>0)
				{
					for(int j = 0 ; j < otherAmountList.size(); j++){
						banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
						if(banktransactions.getamount() != null)
						{
							otherAmount = otherAmount + Double.parseDouble(banktransactions.getamount());
					
						}
					}
				}
				
				double otherAmountLoan = 0.0;
				if(otherAmountListForLoanBank.size()>0)
				{
					for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
						banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
						if(banktransactions.getamount() != null)
						{
							otherAmountLoan = otherAmountLoan + Double.parseDouble(banktransactions.getamount());
					
						}
					}
				}
	  			
				double debitAmountOpeningBal = 0.0;
				double creditAmountOpeningBal = 0.0;
				
				if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
					for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
						bankbalance bankbalance = debitAmountOpeningBalList.get(j);
						if(bankbalance.getExtra3().trim().equals(MH.getmajor_head_id())){
							debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
						}
					}
				}
				
				if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
					for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
						bankbalance bankbalance = creditAmountOpeningBalList.get(j);
						if(bankbalance.getExtra3().trim().equals(MH.getmajor_head_id())){
							creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
						}
					}
				}
				
					double LedgerSum = 0.0;
					double LedgerSumBasedOnJV = 0.0;
				
				
				if(productexpensesList != null && productexpensesList.size() > 0){
					for(int j = 0; j < productexpensesList.size(); j++){
						LedgerSum = LedgerSum + Double.parseDouble(productexpensesList.get(j).getamount());
					}
				}
				
				if(customerpurchases != null && customerpurchases.size() > 0){
					for(int j = 0; j < customerpurchases.size(); j++){
						LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases.get(j).gettotalAmmount());
					}
				}
				
				double productExpJVdebitAmount = 0.0;
// 				if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
// 					for(int j = 0; j < productexpensesJVDebit.size(); j++){
// 						productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
// 					}
// 				}
				
					debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan;
					debit = debit + debit1;
					
					double productExpJVAmount = 0.0;
					
				
				if(productexpensesJV != null && productexpensesJV.size() > 0){
					for(int j = 0; j < productexpensesJV.size(); j++){
						productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpensesJV.get(j).getamount());
					}
				}
				double SumDollarAmt = 0.0;
				
				
				if(DollarAmt != null && DollarAmt.size() > 0){
					for(int j = 0; j < DollarAmt.size(); j++){
						SumDollarAmt = SumDollarAmt + Double.parseDouble(DollarAmt.get(j).gettotalAmmount());
					}
				}
				
				double cardCashChequeJvOnlineAmount = 0.0;
				
				if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
					for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
						cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(cardCashChequeJvAmountList.get(j).gettotalAmmount());
					}
				}
				double jvCreditAmountFromCustomerpurchases = 0.0;
// 				List<customerpurchases> jvCreditAmountFromCustomerpurchasesList = reportesListClass.getJVCreditAmountFromCustomerPurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "s.sub_head_id", "MONTHNAME");
// 				if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
// 					for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
// 						jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
// 					}
// 				}
				
				credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount + jvCreditAmountFromCustomerpurchases;
				
				credit = credit + credit1;
	  			
				if(debit1 > credit1)
				{
					balance1 = debit1 - credit1;
				}
				if(credit1 > debit1)
				{
					balance1 = credit1 - debit1;
				}
				if(credit1 == debit1)
				{
					balance1 = 0.0;
				}
				
				%>
				<tr style=" color: #934C1E; font-weight:bold;">
					<td height="28" align="center">
						<a href="adminPannel.jsp?page=minorheadLedgerReportBySitaram&hoaid=<%=hoaid %>&majrhdid=<%=MH.getmajor_head_id()%>&fromdate=<%=fromdate%>&todate=<%=todate%>&finYear=<%=finYr%>&report=Ledger Report&cumltv=<%=request.getParameter("cumltv")%>" target="_blank"><%=MH.getmajor_head_id() %> <%=MH.getname() %></a>
					</td>
					<td align="center"><%=df.format(debit1) %></td>
					<td align="center"><%=df.format(credit1) %></td>
					
				</tr>
					<%
				
					List<minorhead> minhdlist = reportesListClass.getMinorHeadsByMajorHeadAndHeadOfAccountId(hoaid, MH.getmajor_head_id());
					
					if(minhdlist.size() > 0){ 
						
						
						
						for(int a = 0 ; a < minhdlist.size() ; a++){
							
							
							MNH = (minorhead) minhdlist.get(a);
							
							debit1 = 0.0;
							credit1 = 0.0;
							
							
							double otherAmount1 = 0.0;
							if( otherAmountList != null && otherAmountList.size()>0)
							{
								for(int j = 0 ; j < otherAmountList.size(); j++){
									banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
									if( banktransactions.getMinor_head_id().trim().equals(MNH.getminorhead_id()) && banktransactions.getamount() != null)
									{
										otherAmount1 = otherAmount1 + Double.parseDouble(banktransactions.getamount());
								
									}
								}
							}
							
							double otherAmountLoan1 = 0.0;
							if( otherAmountListForLoanBank != null && otherAmountListForLoanBank.size()>0)
							{
								for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
									banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
									if( banktransactions.getMinor_head_id().trim().equals(MNH.getminorhead_id()) && banktransactions.getamount() != null)
									{
										otherAmountLoan1 = otherAmountLoan1 + Double.parseDouble(banktransactions.getamount());
								
									}
								}
							}
							
							debitAmountOpeningBal = 0.0;
							creditAmountOpeningBal = 0.0;
							
							if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
								for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
									bankbalance bankbalance = debitAmountOpeningBalList.get(j);
									if(bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())){
										debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
									}
								}
							}
							
							if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
								for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
									bankbalance bankbalance = creditAmountOpeningBalList.get(j);
									if(bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())){
										creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
									}
								}
							}
							
							LedgerSum = 0.0;
							LedgerSumBasedOnJV = 0.0;
							productExpJVdebitAmount = 0.0;
						
					
							if(productexpensesList != null && productexpensesList.size() > 0){
								for(int j = 0; j < productexpensesList.size(); j++){
									productexpenses productexpenses = productexpensesList.get(j);
									if(productexpenses.getminorhead_id().trim().equals(MNH.getminorhead_id())){
										LedgerSum = LedgerSum + Double.parseDouble(productexpenses.getamount());
									}
								}
							}
							
// 							if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
// 								for(int j = 0; j < productexpensesJVDebit.size(); j++){
// 									productexpenses productexpenses = productexpensesJVDebit.get(j);
// 									if(productexpenses.getminorhead_id().trim().equals(MNH.getminorhead_id())){
// 										productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
// 									}
// 								}
// 							}
							
							
							if(customerpurchases != null && customerpurchases.size() > 0){
								for(int j = 0; j < customerpurchases.size(); j++){
									
									customerpurchases customerpurchases2 = customerpurchases.get(j);
									if(customerpurchases2.getextra7().trim().equals(MNH.getminorhead_id())){
										LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases2.gettotalAmmount());
									}
								}
							}
							
								debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan1;
								
								productExpJVAmount = 0.0;
									if(productexpensesJV != null && productexpensesJV.size() > 0){
										for(int j = 0; j < productexpensesJV.size(); j++){
											productexpenses productexpenses = productexpensesJV.get(j);
											if(productexpenses.getminorhead_id().trim().equals(MNH.getminorhead_id())){
												productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpenses.getamount());
											}
										}
									}
									
									SumDollarAmt = 0.0;
									if(DollarAmt != null && DollarAmt.size() > 0){
										for(int j = 0; j < DollarAmt.size(); j++){
											customerpurchases customerpurchases2 = DollarAmt.get(j);
											if(customerpurchases2.getextra7().trim().equals(MNH.getminorhead_id())){
												SumDollarAmt = SumDollarAmt + Double.parseDouble(customerpurchases2.gettotalAmmount());
											}
										}
									}
									
									cardCashChequeJvOnlineAmount = 0.0;
									if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
										for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
											customerpurchases customerpurchases2 = cardCashChequeJvAmountList.get(j);
											if(customerpurchases2.getextra7().trim().equals(MNH.getminorhead_id())){
												cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(customerpurchases2.gettotalAmmount());
											}
										}
									}
									
									jvCreditAmountFromCustomerpurchases = 0.0;
// 									if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
// 										for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
// 											customerpurchases customerpurchases2 = jvCreditAmountFromCustomerpurchasesList.get(j);
// 											if(customerpurchases2.getextra7().trim().equals(MNH.getminorhead_id())){
// 												jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
// 											}
// 										}
// 									}
									
									credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount1 +jvCreditAmountFromCustomerpurchases;
							if(debit1 > credit1)
							{
								balance1 = debit1 - credit1;
							}
							if(credit1 > debit1)
							{
								balance1 = credit1 - debit1;
							}
							if(credit1 == debit1)
							{
								balance1 = 0.0;
							}
							
							if(!MNH.getminorhead_id().equals("453")){
							%>
							<tr style="color:#f00;">
								<td height="28" >
									<a href="<%=request.getContextPath()%>/superadmin/adminPannel.jsp?page=subheadLedgerReportBySitaram&hoaid=<%=hoaid %>&majorHeadId=<%=MH.getmajor_head_id() %>&minorHeadId=<%=MNH.getminorhead_id() %>&fromdate=<%=fromdate%>&todate=<%=todate%>&finYear=<%=finYr%>&report=Income and Expenditure&cumltv=<%=request.getParameter("cumltv")%>" style="color:#f00;" target="_blank"><strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong></a>
								</td>
								<td align="right"><%=df.format(debit1) %></td>
								<td align="right"><%=df.format(credit1) %></td>
							</tr>
							
							<%
							}
							List<subhead> subhdlist = reportesListClass.getSubheadsByMinorHeadAndMajorHeadAndHeadOfAccount(hoaid, MH.getmajor_head_id(), MNH.getminorhead_id());
							
							if(subhdlist.size() > 0){ 
								
								for(int b = 0 ; b < subhdlist.size(); b++){
									
									SUBH = (subhead) subhdlist.get(b);
									
									debit1 = 0.0;
									credit1 = 0.0;
									
									double otherAmount2 = 0.0;
										if( otherAmountList != null && otherAmountList.size()>0)
										{
											for(int j = 0 ; j < otherAmountList.size(); j++){
												banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
												if( banktransactions.getSub_head_id().trim().equals(SUBH.getsub_head_id()) && banktransactions.getamount() != null)
												{
													otherAmount2 = otherAmount2 + Double.parseDouble(banktransactions.getamount());
												}
											}
										}
										
										double otherAmountLoan2 = 0.0;
										if( otherAmountListForLoanBank != null && otherAmountListForLoanBank.size()>0)
										{
											for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
												banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
												if( banktransactions.getSub_head_id().trim().equals(SUBH.getsub_head_id()) && banktransactions.getamount() != null)
												{
													otherAmountLoan2 = otherAmountLoan2 + Double.parseDouble(banktransactions.getamount());
												}
											}
										}
									
										
										debitAmountOpeningBal = 0.0;
										creditAmountOpeningBal = 0.0;
										
										if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
											for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
												bankbalance bankbalance = debitAmountOpeningBalList.get(j);
												if(bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id())){
													debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
												}
											}
										}
										if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
											for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
												bankbalance bankbalance = creditAmountOpeningBalList.get(j);
												if(bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id())){
													creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
												}
											}
										}
										
										LedgerSum = 0.0;
										LedgerSumBasedOnJV = 0.0;
										productExpJVdebitAmount = 0.0;
									
								
										if(productexpensesList != null && productexpensesList.size() > 0){
											for(int j = 0; j < productexpensesList.size(); j++){
											
												productexpenses productexpenses = productexpensesList.get(j);
												if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
													LedgerSum = LedgerSum + Double.parseDouble(productexpenses.getamount());
												}
											}
										}
										
// 										if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
// 											for(int j = 0; j < productexpensesJVDebit.size(); j++){
// 												productexpenses productexpenses = productexpensesJVDebit.get(j);
// 												if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
// 													productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
// 												}
// 											}
// 										}
										
										if(customerpurchases != null && customerpurchases.size() > 0){
											for(int j = 0; j < customerpurchases.size(); j++){
												customerpurchases customerpurchases2 = customerpurchases.get(j);
												if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
													LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases2.gettotalAmmount());
												}
											}
										}
											debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan2;
										 	
											productExpJVAmount = 0.0;
											if(productexpensesJV != null && productexpensesJV.size() > 0){
												for(int j = 0; j < productexpensesJV.size(); j++){
													productexpenses productexpenses = productexpensesJV.get(j);
													if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
														productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpenses.getamount());
													}
													
												}
											}
											
											SumDollarAmt = 0.0;
											if(DollarAmt != null && DollarAmt.size() > 0){
												for(int j = 0; j < DollarAmt.size(); j++){
													customerpurchases customerpurchases2 = DollarAmt.get(j);
													if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
														SumDollarAmt = SumDollarAmt + Double.parseDouble(customerpurchases2.gettotalAmmount());
													}
												}
											}
											
											cardCashChequeJvOnlineAmount = 0.0;
											if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
												for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
													customerpurchases customerpurchases2 = cardCashChequeJvAmountList.get(j);
													if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
														cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(customerpurchases2.gettotalAmmount());
													}
												}
											}
											jvCreditAmountFromCustomerpurchases = 0.0;
// 											if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
// 												for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
// 													customerpurchases customerpurchases2 = jvCreditAmountFromCustomerpurchasesList.get(j);
// 													if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
// 														jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
// 													}
// 												}
// 											}
											
											credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount2 + jvCreditAmountFromCustomerpurchases;

											if(debit1>credit1)
						                   {
						                	   balance1=debit1-credit1;
						                   }
						                   if(credit1>debit1)
						                   {
						                	   balance1=credit1-debit1;
						                   }
						                   if(credit1==debit1)
						                   {
						                	   balance1=0.0;
						                   }
						                   if(!MNH.getminorhead_id().equals("453")){ 
						                   %>
						                   <tr>
												<td height="28">
													<a href="adminPannel.jsp?page=dateLedgerReportBySitaram&typeserch=Subhead&subheadId=<%=SUBH.getsub_head_id()%>&minorheadId=<%=MNH.getminorhead_id()%>&majorHeadId=<%=MH.getmajor_head_id() %>&hod=<%=hoaid%>&finYr=<%=finYr%>&fromdate=<%=fromdate%>&todate=<%=todate%>&cumltv=<%=request.getParameter("cumltv")%>" target="_blank">
														<strong><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></strong>
													</a>
												</td>
												<td align="center"><%=df.format(debit1) %></td>
												<td align="center"><%=df.format(credit1) %></td>
											</tr>
						                   <%}
											 	
								}
								
								
							}
							
						}
						
						
					}
	  		}
	  		
			}
	}
  
 %>
	
	
	<tr>
    <td align="center" style=" color: #934C1E; font-weight:bold;" height="28">TOTAL AMOUNT OF PAYMENTS</td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">Rs.<%=df.format(debit) %></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">0.00</td>
</tr>
	
	
	
	
	
	  		<%
	  		
	  		if(request.getParameter("head_account_id")!=null){
				hoaid=request.getParameter("head_account_id");
				 if(hoaid.equals("4")){
					headgroup="1";
					reporttype = "pro-counter";
					reporttype1 = "Development-cash";	
				}else if(hoaid.equals("1")){
					reporttype = "Charity-cash";
					reporttype1 = "Not-Required";	
				}
				else if(hoaid.equals("3")){
					reporttype = "poojastore-counter";
					reporttype1 = "Not-Required";
				}
				else if(hoaid.equals("5")){
					reporttype = "Sainivas";
					reporttype1 = "Not-Required";	
				} 
			}
	  		
	  		
	  		
	  		
	  		List banklist2=BNKL.getBanksBasedOnHOA(hoaid);
					if(banklist2.size()>0)
					{ 	int i=0;
	  					for(i=0;i<banklist2.size();i++)
	  					{
	  						BNK=(bankdetails)banklist2.get(i);
	  						int j=0;
	  						for(j=0;j<typeofbanks.length;j++)
	  						{
							  	bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",todate+" 23:59:59",typeofbanks[j],"");
							  	finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], request.getParameter("finYear"));
							  	bankopeningbal += finOpeningBal;
							  	if(bankopeningbal >0 || bankopeningbal<0)
							  	{
							  
					  				%> 
					   				<tr>
					    				<td height="28" style="color:#F00; font-weight:bold;">
						    				<a href="<%=request.getContextPath()%>/superadmin/adminPannel.jsp?page=bankDetails&id=<%=BNK.getbank_id() %>&reportType=<%=typeofbanks[j]%>&finYear=<%=finYr%>&fromdate=<%=fromdate %>&todate=<%=todate%>" target="_blank">
			  									<strong><%=BNK.getbank_id() %> <%=BNK.getbank_name() %> <%=typeofbanks[j]%></strong>
			  								</a>
						    			</td>
						    			<td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(bankopeningbal) %></td>
						    			<td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
					  				</tr>
					  				<%
					debit = debit + bankopeningbal;
					//bankopeningbal=0.0;
								}
							 }
	  					}
	  					}
	
				

				 double finOpeningBalSansthan = BBAL_L.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));
  
				  String counterBal = CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate);
				  String bankDeposit = BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",fromdate);
				  
				  
				  double cashAccount = 0.0;
				  
				  if(counterBal != null && !counterBal.trim().equals("")){
					  cashAccount = finOpeningBalSansthan + Double.parseDouble(counterBal);
				  }
				  if(bankDeposit != null && !bankDeposit.trim().equals("")){
					  cashAccount = cashAccount - Double.parseDouble(bankDeposit);
				  }
				  
				  CounterBalanceAndBankDeposits balanceAndBankDeposits = new CounterBalanceAndBankDeposits();
				  
				  List<customerpurchases> customerpurchasesList = balanceAndBankDeposits.getcustomerpurchasesAfterFinYearToBeforeFromDate(hoaid, fromdate+" 00:00:00", todate+" 23:59:59", "");
				  List<banktransactions> banktransactionsList = balanceAndBankDeposits.getbanktransactionsAfterFinYearToBeforeFromDate(reporttype, reporttype1, fromdate+" 00:00:00", todate+" 23:59:59", "");
				  
				  
				  if(customerpurchasesList != null && customerpurchasesList.size() > 0 ){
					  customerpurchases customerpurchases = customerpurchasesList.get(0);
					  if(customerpurchases != null && customerpurchases.gettotalAmmount() != null && !customerpurchases.gettotalAmmount().equals("")){
						  cashAccount = cashAccount + Double.parseDouble(customerpurchases.gettotalAmmount());
					  }
				  }
				  
				  if(banktransactionsList != null && banktransactionsList.size() > 0 ){
					  banktransactions banktransactions = banktransactionsList.get(0);
					  if(banktransactions != null && banktransactions.getamount() != null && !banktransactions.getamount().equals("")){
						  cashAccount = cashAccount - Double.parseDouble(banktransactions.getamount());
					  }
				  }
				  
	  				debit = debit + cashAccount;
			
					%>
	  				<tr>
	    				<td height="28" style="color:#F00; font-weight:bold;">
	    				
	    				<a href="<%=request.getContextPath()%>/superadmin/adminPannel.jsp?page=DailycounterBalanceReport_new&finYear=<%=finYr %>&page=totalsalereport&fromDate=<%=fromdate %>&toDate=<%=todate %>&head_account_id=<%=hoaid %>" target="_blank">
	  						<strong>CASH ACCOUNT</strong>
	  					</a>
	    					
	    				
	    				</td>
					    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(cashAccount)%></td>
					    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
	  				</tr> 
	  				
	  				
	  				<%
	  				
	  					if(hoaid.equals("1")){
	  						
	  						
	  						List<customerpurchases> customerpurchasesList1 =	reportesListClass.getJVAmountsFromcustomerpurchases(hoaid, "102", "689", "25765", fromdate+" 00:00:00", todate+" 23:59:59", "journalvoucher", "productId", "year");
	  						
	  						if(customerpurchasesList1 != null && customerpurchasesList1.size() > 0){
	  							
	  							double RFS = Double.parseDouble(customerpurchasesList1.get(0).gettotalAmmount());
	  							%>
	  								<tr>
					    				<td height="28" style="color:#F00; font-weight:bold;">
					  						<strong>RECEIVABLE FROM SANSTHAN</strong>
					    				</td>
									    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(RFS)%></td>
									    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
					  				</tr> 
	  							
	  							<%
	  							debit = debit + RFS;
	  						}
	  						
	  						
	  						
	  					}
	  				
	  				
	  				%>
	  				
	  				
<tr>
   <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debit) %></td>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(credit) %></td>
</tr>
  
</tbody></table>
</div>
<% } %>
  </div>
			</div>
				
			</div>
			</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>