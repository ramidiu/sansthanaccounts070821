<%@page import="beans.indentapprovals"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.TreeMap"%>
<%@page import="beans.productexpenses"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dc Bill PaymentApproval</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
table, tr, td, th
{
    border: 1px solid black;
    border-collapse:collapse;
}
tr.header
{
    cursor:pointer;
}
</style>
<script>
$(function(){
	$('.header').click(function(){
		$(this).find('span').text(function(_, value){return value=='-'?'+':'-'});
		   $(this).nextUntil('tr.header').slideToggle(100, function(){
		   });
	});
	$('.body').hide();
});

function submitForm(dcNo)	{
/* 	var narration = $('#feedback'+dcNo).val();
	if ($.trim(narration) === "")	{
		alert("Please Enter Narration");
		event.preventDefault();
	}
	else	{
		$('#form1').submit();
	} */
	$('#form1').submit();
}
</script>
<body>
<%if(session.getAttribute("superadminId")!=null){
	productexpensesListing prodService =new productexpensesListing();
	List<productexpenses> prodExpLst =null;
	String sid = session.getAttribute("superadminId").toString();
	prodExpLst = prodService.getDcProdExpenses(); 
	indentapprovalsListing iapl = new indentapprovalsListing();
	List<indentapprovals> lst = null;
	Map<String,String> dcnos = new TreeMap<String,String>();
	Map<String,String> lists = new TreeMap<String,String>();
	String dcno = "";%>
	<form id="form1" action="dcPaymentApprovalInsert.jsp" method="post">
	<%if (prodExpLst.size() > 0)	{%>
		<!-- <table border="0" style='width: 90%; margin: 0 auto;' class='table table-bordered'> -->
		<%for (int i = 0; i < prodExpLst.size() ; i++)	{
			productexpenses p = prodExpLst.get(i);
			dcnos.put(p.getExtra11().trim(),p.getExtra13());
//			lists.put(p.getexpinv_id(),null);
		}
	Set<String> dcs = dcnos.keySet();
//	Set<String> epivs = lists.keySet();
	Iterator<String> itr = dcs.iterator();
//	Iterator<String> it1 = epivs.iterator();
		while(itr.hasNext())	{
		String dc = itr.next();
		lst = iapl.getDcApprovedByAdmin(dc,sid);
		if (lst.size() == 0)	{%>
		<table border="0" style='width: 90%;margin: 33px auto 0;position: relative;top: 49px;' class='table table-bordered'>
		<input type="hidden" name="dcNumber" id="dcNumber" value="<%=dc %>">
		<tr class="header"><th colspan="11" style="text-align: center;"><%=dc %>&nbsp;&nbsp;&nbsp;<%=dcnos.get(dc) %><span>+</span></th></tr>
		<tr class="body"><td style="font-size: 11px;font-weight: bold;">S.no</td><td style="font-size: 11px;font-weight: bold;">MseNumber</td><td style="font-size: 11px;font-weight: bold;">Date</td><td style="font-size: 11px;font-weight: bold;">MajorHead</td><td style="font-size: 11px;font-weight: bold;">MinorHead</td><td style="font-size: 11px;font-weight: bold;">SubHead</td><td style="font-size: 11px;font-weight: bold;">Product</td><td style="font-size: 11px;font-weight: bold;">Hoa</td><td style="font-size: 11px;font-weight: bold;">Qty</td><td style="font-size: 11px;font-weight: bold;">Price</td><td style="font-size: 11px;font-weight: bold;">Amount</td></tr>
		<%List<productexpenses> pe = new ArrayList<productexpenses>();
		int count = 0;	
		for (int j = 0; j < prodExpLst.size() ; j++)	{
				productexpenses p = prodExpLst.get(j);
				 if (p.getExtra11().trim().equals(dc))	{ 
					 lists.put(p.getexpinv_id(),null);	 
				count ++;
				%>
				  <tr class="body">
    				<td style="font-size: 11px;font-weight: bold;"><%=count %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getextra5() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getentry_date() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getExtra14() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getExtra15() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getExtra16() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getExtra17() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getExtra18() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getExtra19() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getExtra20() %></td>
    				<td style="font-size: 11px;font-weight: bold;"><%=p.getamount() %></td>
  				</tr>
				 <input type="hidden" name="expId<%=count %>" id="expId<%=count %>" value="<%=p.getexp_id()%>">
				 <%}%>
			<input type="hidden" name="headofaccount" id="headofaccount" value="<%=p.gethead_account_id() %>"> 
			<%}%>
			<input type="hidden" id="count" name ="count" value= "<%=count %>">
			<tr class="body">
			<td colspan="11" align="left" style="font-size: 11px;font-weight: bold;">Enter Narration:: <input type="text" palceholder="Description" id="feedback<%=dc%>" name="feedback"></td>
			</tr>
			<tr class="body">
			<td colspan="11" align="center"><button class="click" onclick="submitForm(<%=dc%>)">Approve</button></td>
			</tr>
			</table>
		<%
		Set<String> epivs = lists.keySet();
		Iterator<String> it1 = epivs.iterator();
		int count1 = 0;
		while(it1.hasNext())	{
			count1 ++;
		%>
		<input type="hidden" name="indentInvoiceId<%=count1 %>" id="indentInvoiceId<%=count1 %>" value="<%=it1.next()%>">	
		<%if(!it1.hasNext())	{ %>
		<input type="hidden" name="InvoiceIdCount" id="InvoiceIdCount" value="<%=count1 %>">
		<%}
		}
	}else	{%>
			 <h2 style="color:red;text-align: center;font-family: monospace;margin: 0 0 0 0;">No delivery Challans for Approval</h2> 
	<%}
} // iterator itr
} // if
else	{%>
<h2 style="color:red;text-align: center;font-family: monospace;margin: 0 0 0 0;">No delivery Challans for Approval</h2>
<%}%>
</form>
<!-- <table border="0" style='width: 90%; margin: 0 auto;' class='table table-bordered'>
  
  <tr class="header">
      <th colspan="2">Header <span>-</span></th>
  </tr>
  <tr>
    <td>data</td>
    <td>data</td>
  </tr>
  <tr>
    <td>data</td>
    <td>data</td>
  </tr>
  
  <tr class="header">
    <th colspan="2">Header <span>-</span></th>
  </tr>
  <tr>
    <td>date</td>
    <td>data</td>
  </tr>
  <tr>
    <td>data</td>
    <td>data</td>
  </tr>
  <tr>
    <td>data</td>
    <td>data</td>
  </tr>
</table> -->	
<%}
else {
response.sendRedirect("index.jsp");
}%>
</body>
</html>