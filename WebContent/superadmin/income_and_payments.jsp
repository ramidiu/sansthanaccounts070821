<%@page import="java.util.List"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.payments"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>

<script>
/* $(function() {
	$( "#fromDate2" ).datepicker({
		
		//minDate: new Date(2016, 3, 1),
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate2" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate2" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate2" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
$(function() {
	$( "#fromDate3" ).datepicker({
		
		//minDate: new Date(2016, 3, 1),
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate3" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate3" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate3" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
}); */

/* function datepickerchange()
{
	var finYear = $("#finYear").val();
	
	if(finYear == "2015-16")
	{
		document.getElementById('li1').style.display = "block";
		document.getElementById('li2').style.display = "block";
		document.getElementById('li3').style.display = "none";
		document.getElementById('li4').style.display = "none";
		document.getElementById('li5').style.display = "none";		
		document.getElementById('li6').style.display = "none";
	}
	
	else if(finYear == "2016-17")
	{
		document.getElementById('li1').style.display = "none";
		document.getElementById('li2').style.display = "none";
		document.getElementById('li3').style.display = "block";		
		document.getElementById('li4').style.display = "block";
		document.getElementById('li5').style.display = "none";		
		document.getElementById('li6').style.display = "none";
	}
	else if(finYear == "2017-18")
	{
		document.getElementById('li1').style.display = "none";
		document.getElementById('li2').style.display = "none";
		document.getElementById('li3').style.display = "none";		
		document.getElementById('li4').style.display = "none";
		document.getElementById('li5').style.display = "block";		
		document.getElementById('li6').style.display = "block";
	}
} */
function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}
$(document).ready(function(){
	datepickerchange();
});
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
        function(){
				// Hook up the print link.
            $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                    function(){
                        // Print the DIV.
                        $( "a" ).css("text-decoration","none");
                        $( ".printable" ).print();
                       

                        // Cancel click event.
                        return( false );
                    });
				});
         $(document).ready(function () {
            $("#btnExport").click(function () {
                $( "a" ).css("text-decoration","none");
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};
	
	
</script>
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>

</head>
<body>
	<%
	DecimalFormat df=new DecimalFormat("#.##");
	String vochertype="";
	double amount,bankopeningbalcredit,incomeamount,paymentamount,counteramount;
	amount=bankopeningbalcredit=incomeamount=paymentamount=counteramount=0.0;
	String chequeno="";
	bankcalculations BNKCAL=new bankcalculations();
	DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat  dateFormat2= new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
    String Vocherno[]=null;
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat DBDate= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(new SimpleDateFormat("yyyy-MM-dd").format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	int day=1;
	String toDate=currentDate+" 23:59:59";
	banktransactionsListing BNKT_L=new banktransactionsListing();
	productexpensesListing PROE_L=new productexpensesListing();
	customerpurchasesListing CUSTP_L=new customerpurchasesListing();
	paymentsListing PAYL=new paymentsListing();
	vendorsListing VENl=new vendorsListing();
	bankdetailsListing BNKL=new bankdetailsListing();
	mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
	List HA_Lists=HOA_L.getheadofaccounts();
	String hoid="";
	String finStartDate = "";
	String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
		hoid=request.getParameter("hoid");
	}
	 /* if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
			if(request.getParameter("fromDate")!=null){
				fromDate=request.getParameter("fromDate")+" 00:00:01";
			}
			if(request.getParameter("toDate")!=null){
				toDate=request.getParameter("toDate")+" 23:59:59";
			}
		} */
		if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
			if(request.getParameter("finYear")!=null && request.getParameter("finYear").equals("2015-16"))
			{
				if(request.getParameter("fromDate")!=null){
					fromdate=request.getParameter("fromDate");
					fromDate=request.getParameter("fromDate")+" 00:00:01";
				}
				if(request.getParameter("toDate")!=null){
					todate=request.getParameter("toDate");
					toDate=request.getParameter("toDate")+" 23:59:59";
				}
				
				finStartDate = "2015-04-01 00:00:01";
			}
			
			else if(request.getParameter("finYear")!=null && request.getParameter("finYear").equals("2016-17"))
			{	
				if(request.getParameter("fromDate")!=null){
					fromdate=request.getParameter("fromDate");
					fromDate=request.getParameter("fromDate")+" 00:00:01";
				}
				if(request.getParameter("toDate")!=null){
					todate=request.getParameter("toDate");
					toDate=request.getParameter("toDate")+" 23:59:59";
				}
				
				finStartDate = "2016-04-01 00:00:01";
			}
			else if(request.getParameter("finYear")!=null && request.getParameter("finYear").equals("2017-18"))
			{	
				if(request.getParameter("fromDate")!=null){
					fromdate=request.getParameter("fromDate");
					fromDate=request.getParameter("fromDate")+" 00:00:01";
				}
				if(request.getParameter("toDate")!=null){
					todate=request.getParameter("toDate");
					toDate=request.getParameter("toDate")+" 23:59:59";
				}
				
				finStartDate = "2017-04-01 00:00:01";
			}
		}	
	DecimalFormat DF=new DecimalFormat("0.00");
	double bankopeningbal=0.0;
	
	/* if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
		 fromdate=request.getParameter("fromDate");
	}
	if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
		todate=request.getParameter("toDate");
	} */
	
	String typeofbanks[]={"bank","cashpaid"};
	List banklist=BNKL.getBanksBasedOnHOA(hoid);
	List Pay_Det=PAYL.getDailyPaymentsReport(hoid,fromDate,toDate);
	%>
<div class="vendor-page">
<div class="vendor-list">
<form action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
			<li>
<select name="hoid" id="hoid" onchange="formSubmit();">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></li>
						<%-- <li><select name="finYear" id="finYear"  Onchange="datepickerchange();">
						<%if(request.getParameter("finYear") == null){ %>
						<option value="">-- Select Fin Year --</option><%} %>
						<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%> selected="selected"<%} %>>2015-16</option>
						<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
						</select></li> --%>
						<li>
						
			
			<select name="finYear" id="finYear"  Onchange="datepickerchange();">
			<!-- <option value="">-- Select Fine Year --</option> -->
					<%@ include file="yearDropDown1.jsp" %>
					</select>
				
						
						
						</li>
						<li> 
						<input type="hidden" name="page" value="income_and_payments"></input></li>
						<%-- <li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li> --%>
						<%-- <li id = "li1" style="display:none;"><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li id = "li2" style="display:none;"><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly/></li>


						<li id = "li3" style="display:none;"><input type="text"  name="fromDate2" id="fromDate2" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li id = "li4" style="display:none;"><input type="text" name="toDate2" id="toDate2"  class="DatePicker" value="<%=todate %>" readonly/></li>
						
						<li id = "li5" style="display:none;"><input type="text"  name="fromDate3" id="fromDate3" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li id = "li6" style="display:none;"><input type="text" name="toDate3" id="toDate3"  class="DatePicker" value="<%=todate %>" readonly/></li> --%>
					<li style="display:block;"><input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/></li>
					<li style="display:block;"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					
					
					</ul>
				</div></form>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="printable">
<div class="list-details" id="tblExport">
<% 
if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("")){%>
<table width="100%" cellpadding="0" cellspacing="0"   >
<tr><td colspan="8" height="10"></td> </tr>
<tr><td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 20px;">Income and Payments  </td></tr>

</table>
	<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
    <td colspan="6"><table width="100%" cellpadding="0" cellspacing="0"  class="yourID" >
<tr>
		<td class="bg" width="20%">Name</td>
		<td class="bg" width="15%">Amount</td></tr>
			<tr>
	<td style="font-weight: bold;font-size: 15px;color: #CA25E1;" colspan="2">Opening Balance </td>
	</tr>
		<%
		if(banklist.size()>0){ 
			for(int i=0;i<banklist.size();i++){
				BNK=(beans.bankdetails)banklist.get(i);
				for(int j=0;j<typeofbanks.length;j++){
										 //bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),"",fromDate,typeofbanks[j]); 
								bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartDate,fromDate,typeofbanks[j],"");
										if(bankopeningbal >0 || bankopeningbal<0){ %>
		<tr>
		<td><%=BNK.getbank_name() %> <%=typeofbanks[j]%></td>
		<td>Rs.<%=df.format(bankopeningbal) %> /-</td>
		</tr>
		<%bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
		bankopeningbal=0.0;
										}}} }
		
		incomeamount=incomeamount+bankopeningbalcredit;
%>
<%-- 	<tr>
	<td style="font-weight: bold;font-size: 15px;color: #E18D25;">Total </td>
	<td style="font-weight: bold;font-size: 15px;color: #E18D25;">&#8377; <%=incomeamount %> /-</td>
	</tr> --%>
			<tr>
	<td style="font-weight: bold;font-size: 15px;color: #CA25E1;" colspan="2">Counter Income </td>
	</tr>
<%
while(DBDate.parse(todate).compareTo(DBDate.parse(fromdate))>=0){
	counteramount=CUSTP_L.getDayWiseIncome(hoid,fromdate);
	%>
	<tr>
	<td><%=fromdate%></td>
	<td>Rs.<%=counteramount %> /-</td>
	</tr>
	<%
	incomeamount=incomeamount+counteramount;
	c1 = Calendar.getInstance(); 
	c1.setTime(DBDate.parse(fromdate)) ;
	c1.add(Calendar.DATE, day);
	fromdate= DBDate.format(c1.getTime());
}
		%>
	
		</table></td>
    
    <td colspan="6"><table width="100%" cellpadding="0" cellspacing="0"  class="yourID" >
<tr>
	<td width="3%" class="bg">S.No</td>
		<td class="bg" width="10%">Date</td>
		<td class="bg" width="15%">Naration</td>
		<td class="bg" width="15%">Vendor Name</td>
		<td class="bg" width="10%">Total Amount</td>
		<td class="bg" width="3%">Type</td>
		</tr>
			<tr>
	<td style="font-weight: bold;font-size: 15px;color: #CA25E1;text-align: center;" colspan="6">Payments</td>
	</tr>
			<%
	if(Pay_Det.size()>0){
		for(int i=0;i<Pay_Det.size();i++){
			PAY=(payments)Pay_Det.get(i);
			Vocherno=PAY.getreference().split(",");
			if( Vocherno.length>1){
				if(vochertype.equals("tds") && Vocherno.length>1){
					Vocherno[0]=Vocherno[1];
					chequeno=PAY.getextra9();
				} else if(vochertype.equals("servicetax")){
						if( Vocherno.length>2 && Vocherno[2]!=null){
					Vocherno[0]=Vocherno[2];
					} else {
						Vocherno[0]=Vocherno[1];
					}
						chequeno=PAY.getService_cheque_no();
				}
				
			} else{
				Vocherno[0]=PAY.getreference();
				chequeno=PAY.getchequeNo();
			}
			%>
	<tr>
		<td width="3%"><%=i+1 %></td>
		<td  width="10%"><%=df2.format(dateFormat.parse(PAY.getdate())) %></td>
		<td  width="15%"><%=PAY.getnarration()%></td>
		<td  width="15%"><%=VENl.getMvendorsAgenciesName(PAY.getvendorId()) %></td>
		<td  width="10%">Rs.<%=DF.format(Math.round(Double.parseDouble(PAY.getamount())))%> /-</td>
			<%if(PAY.getpaymentType()!=null && PAY.getpaymentType().equals("cheque")){ %>
				<td width="3%" align="center"><%=PAY.getextra2() %></td>
		<%} else{ %>
		<td width="3%" align="center"><%=PAY.getpaymentType() %></td>
		<%} %>

	</tr>
	<%paymentamount=paymentamount+Double.parseDouble(PAY.getamount());
	}	}%>
		<tr>
	<td style="font-weight: bold;font-size: 15px;color: #CA25E1;" colspan="6">Closing Balance </td>
	</tr>
		<%bankopeningbalcredit=bankopeningbal=0.0;
		if(banklist.size()>0){ 
			for(int i=0;i<banklist.size();i++){
				BNK=(beans.bankdetails)banklist.get(i);
				for(int j=0;j<typeofbanks.length;j++){
										//bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),"",toDate,typeofbanks[j]);
										bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartDate,fromDate,typeofbanks[j],"");
										if(bankopeningbal >0 || bankopeningbal<0){ %>
		<tr>
		<td colspan="2"></td>
		<td colspan="2"><%=BNK.getbank_name() %> <%=typeofbanks[j]%></td>
		<td colspan="1">Rs.<%=df.format(bankopeningbal) %> /-</td>
		<td></td>
		</tr>
		<%bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
		bankopeningbal=0.0;
										}}} }
		
		paymentamount=paymentamount+bankopeningbalcredit;
%>
	
</table></td>

</tr>
<tr>
<td colspan="2">
-------------------------------------------</td>
<td colspan="6"  style="text-align: center;">
-----------------------------------------------------------------------------------------------------</td>
</tr>
		<tr>
	<td style="font-weight: bold;font-size: 15px;color: #E18D25;">Total </td>
	<td style="font-weight: bold;font-size: 15px;color: #E18D25;">Rs.<%=df.format(incomeamount) %> /-</td>
	
	<td colspan="4" style="font-weight: bold;font-size: 15px;color: #E18D25;text-align: right;" >Total </td>
	<td style="font-weight: bold;font-size: 15px;color: #E18D25;  padding-right: 149px;	text-align: right;">Rs.<%=df.format(paymentamount) %>  /-</td>
	</tr>
	</table><%} %>
</div>
</div>
</div>
</div>
</body>
</html>