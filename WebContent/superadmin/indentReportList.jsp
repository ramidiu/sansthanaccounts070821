<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="beans.indentapprovals"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	$( document ).ready(function() {
		$( "#hoid" ).change(function() {
				$("#search" ).submit();
			});
		$( "#status" ).change(function() {
				$("#search" ).submit();
			});
	});
</script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>

<%
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
mainClasses.indentListing IND_L = new indentListing();
indentapprovalsListing APPR_L=new indentapprovalsListing();
superadminListing SAD_l=new superadminListing();
String hoid="";
if(session.getAttribute("department")!=null && !session.getAttribute("department").toString().equals("")){
	hoid=session.getAttribute("department").toString();
}
if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")) {
	hoid=request.getParameter("hoid");
}
String status="pending";
if(request.getParameter("status")!=null &&!request.getParameter("status").equals("")) {
	status=request.getParameter("status");
	System.out.println("status..."+status);
}
String superadminId="";
if(!session.getAttribute("role").equals("GS") || !session.getAttribute("role").equals("FS")){
	superadminId=session.getAttribute("superadminId").toString();
}
List IND_List=IND_L.getIndentsBasedOnDeptAndStatus1(hoid,status,superadminId);
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
List pendingIndents=IND_L.getPendingIndents();%>
<div class="vendor-page">
<div class="vendor-box">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Indent's Approvals List</td></tr>
</table>
<div style="clear:both;"></div>

<%-- <div class="unpaid-box">
<div class="unpaid">Pending indents</div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount"><%=IND_List.size()- pendingIndents.size()%></li>
 <li>APPROVED INDENTS</li>
</ul>
</div>
<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount"><%=pendingIndents.size()%></li>
 <li> PENDING INDENT</li>
</ul>
</div>
</div>
</div>
</div>
<div class="paid-box">
<div class="unpaid">Total indents</div>
<div class="bills-cash">
<div class="bill-paid">
 <ul>
<li class="bill-amount"><%=IND_List.size() %></li>
 <li>Total indents raised</li>
</ul>
</div>
</div>
</div> --%>
</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<form id="search" action="adminPannel.jsp" method="get">
<div class="search-list">
<ul>
<input type="hidden" name="page" value="indentReportList">
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts(); %>
<li><select name="hoid" id="hoid">
<option value="" selected="selected">ALL</option>
<%
if(session.getAttribute("department")!=null && !session.getAttribute("department").toString().equals("")){%>
<option value="<%=session.getAttribute("department").toString()%>"> <%=HOA_L.getHeadofAccountName(session.getAttribute("department").toString()) %></option>
<%}else{
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("") && request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%}%> > <%=HOA.getname() %> </option>
<%}}} %>
</select></li>
<li><select name="status" id="status">
<!-- <option value="">SORT BY INDENT STATUS</option> -->
<option value="pending" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("pending")){ %> selected="selected" <%} %> >PENDING FOR APPROVAL</option>
<option value="partially" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("partially")){ %> selected="selected" <%} %> >PARTIALLY APPROVED</option>
<option value="closed" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("closed")){ %> selected="selected" <%} %>>INDENTS CLOSED</option>
<%-- <%if("VC".equals(session.getAttribute("role").toString())){ %><%} %> --%>
<option value="EmergencyApproval" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("EmergencyApproval")){ %> selected="selected" <%} %>>EmergencyApproval</option>
<option value="disapproved" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("disapproved")){ %> selected="selected" <%} %>>Disapproved</option>

</select></li>
</ul>
</div></form>
<!-- <div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Column's</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>
</span></div> -->
<div class="clear"></div>
<div class="list-details">
<%
if(IND_List.size()>0){%>
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    /*width:auto; margin:0 2%;*/
    background:#d0e3fb;
}
</style>
<!-- <script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>  -->
<table width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:30px;margin-top:30px;">
<thead class="yourID">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="6%">Indent Invoice / Unique No</td>
<td class="bg" width="10%">Company</td>
<!-- <td class="bg" width="20%">Vendor Name</td> -->
<td class="bg" width="10%">Date/Time</td>
<td class="bg" width="5%">Requirement</td>
<td class="bg" width="10%">Status</td>
<%if(status.equals("disapproved")) {%>
<td class="bg" width="61%">Disapproved By </td><%}else{ %>
<td class="bg" width="61%">Approved By </td><%} %>
</tr>
</thead>
<%  
List APP_Det=null;
List appSize=null;
String approvedBy="";

for(int i=0; i < IND_List.size(); i++ ){
	INDT=(indent)IND_List.get(i);
	 APP_Det=APPR_L.getIndentDetails(INDT.getindentinvoice_id());	
	/* if(status.equals("disapproved"))
	{
		APP_Det=APPR_L.getIndentDetails(INDT.getindentinvoice_id(),"disapproved");
	}
	else{
		APP_Det=APPR_L.getIndentDetails(INDT.getindentinvoice_id(),"approved");
		System.out.println("APP_Det ap:::"+APP_Det.size());
	} */
	if(APP_Det.size()>0){
		for(int j=0;j<APP_Det.size();j++){
			IAP=(indentapprovals)APP_Det.get(j);
			if(j==0)
			{
				approvedBy=SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME  ["+CDF.format(SDF.parse(IAP.getapproved_date()))+"]";
			}else{
				approvedBy=approvedBy+"<br></br>"+SAD_l.getSuperadminname(IAP.getadmin_id())+" --TIME ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"]";
			}
		}
	}
	if(status.equals("pending"))
	{
		appSize=APPR_L.getIndentApprovedByAdmin(INDT.getindentinvoice_id(), superadminId);
	}
if( appSize!=null && appSize.size()>0){
	
}else{%>
<tr>
<td width="3%"><%=i+1%></td>
<td width="6%"><a href="adminPannel.jsp?page=indentDetailedReport&invid=<%=INDT.getindentinvoice_id()%>&approvedBy=<%=approvedBy%>&statuss=<%=status%>"><b><%=INDT.getindentinvoice_id() %></b></a> / <%if( INDT.getextra4()!=null && !INDT.getextra4().equals("") ){ %>
<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=INDT.getextra4()%>')"><span><%=INDT.getextra4() %></span></div><%} %></td>
<td width="10%"><%=HOA_CL.getHeadofAccountName(INDT.gethead_account_id())%></td>
<%-- <td><ul>
<li><span><a href="adminPannel.jsp?page=indentDetailedReport&invid=<%=INDT.getindentinvoice_id()%>"><%=INDT.getvendor_id()%>  </a></span><br /></li>
</ul></td> --%>
<td width="10%"><%=CDF.format(SDF.parse(INDT.getdate()))%> <%=time.format(SDF.parse(INDT.getdate()))%></td>
<td width="5%"><%=INDT.getrequirement()%></td>
<td width="10%"><%%><%if(status.equals("disapproved")){%> disapproved<%}else{%>  <%=INDT.getstatus()%><%} %></td>
<td width="61%"><%=approvedBy%></td>
</tr>
<%}
approvedBy="";
} %>
</table>
<%}else{%>
<div align="center"><h1>No Indents Raised Yet</h1></div>
<%}%>
</div>
</div>
</div>
