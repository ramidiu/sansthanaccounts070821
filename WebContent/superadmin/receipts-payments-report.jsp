<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="model.SansthanAccountsDate"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.accounts.saisansthan.vendorBalances"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.subhead" %>
<%@page import="mainClasses.subheadListing" %>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Receipts And Payments  | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;

}
.new-table td{
padding: 2px 0 2px 5px !important;}


</style>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script>

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 
</script>

<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
        function minorhead(id){
        	$('#'+id).toggle();
        }
    </script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
</head>
<body>
<%if(session.getAttribute("superadminId")!=null){ 
		customerpurchasesListing CP_L=new customerpurchasesListing();
		majorheadListing MH_L=new majorheadListing();
		vendorsListing VNDR=new vendorsListing();
		bankdetailsListing BNKL=new bankdetailsListing();
		bankcalculations BNKCAL=new bankcalculations();
		banktransactionsListing BNK_L = new banktransactionsListing();
		vendorBalances VNDBALL=new vendorBalances();
		Calendar c1 = Calendar.getInstance(); 
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat df=new DecimalFormat("#.##");
		String reporttype="";
		String reporttype1="";
		String fromdate="";
		String onlyfromdate="";
		String todate="";
		String Todate="";
		double bankopeningbal=0.0;
		double bankopeningbalcredit=0.0;
		String onlytodate="";
		String cashtype="online pending";
		String cashtype1="othercash";
		String cashtype2="offerKind";
		String cashtype3="journalvoucher";
		String finStartDate = "";
		
		List minhdlist=null;
		minorheadListing MINH_L=new minorheadListing();
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		todate=(dateFormat2.format(c1.getTime())).toString();
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromdate=(dateFormat2.format(c1.getTime())).toString();
		if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
									 fromdate=request.getParameter("fromDate");
								}
								if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
									todate=request.getParameter("toDate");
								} 
								
		headofaccountsListing HOA_L=new headofaccountsListing();
	List headaclist=HOA_L.getheadofaccounts();
	String hoaid="";
	/* String hoaid="1";
	if(hoaid.equals("1")){
	reporttype = "Charity-cash";
	reporttype1 = "Not-Required";	
	} */
	if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
		hoaid=request.getParameter("head_account_id");
	
	}
	
	%>
	<div>
			<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form  name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 50px;padding-top: 30px;">
				 <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
<!-- <tr>
<td colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td colspan="2">Select Financial Year</td>
<td id = "li5" style="display:none;">From Date</td>&nbsp;
<td id = "li6" style="display:none;">To Date</td>

</tr> -->
				<!-- <tr> -->
<!-- <td colspan="2"> -->
<tr>
<td style="width:14%;">Select Financial Year</td>
<td style="width:20%;">From Date</td>
<td style="width:20%;">To Date</td>
<td style="width:14%;"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td style="width:14%;">Select Cumulative Type</td>
</tr>
<tr>
				<%-- <td style="width:14%;">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();" style="width:165px;">
					<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
					<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
					</select>
				</td> --%>
				
			<td style="width:14%;">
			<select name="finYear" id="finYear"  Onchange="datepickerchange();" style="width:165px;">
				<%@ include file="yearDropDown1.jsp" %>
					</select>
			</td> 
				
				<td style="width:20%;"> <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
				<td style="width:20%;"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
				<input type="hidden" name="page" value="receipts-payments-report_neww"/>
				 </td>
				 <td style="width:14%;">
<!-- <div class="search-list">
<ul>
			<li> -->
<select name="head_account_id" id="head_account_id" onChange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
<!-- <option value="SasthanDev" >Sansthan Development</option> -->
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select> 
</td>
<td style="width:14%;">
<select name="cumltv" id="cumltv">
	<option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option>
	<option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option>
</select>
</td><!-- </li> -->

<td><input type="submit" value="SEARCH" class="click" /></td>
 </tr></table> </form>

<%DecimalFormat DF=new DecimalFormat("#,###.00"); 
String typeofbanks[]={"bank","cashpaid"};
				
				String headgroup="";
				double total=0.0;
				double credit=0.0;
				double debit=0.0;
				double creditreceipt =0.0;
				double debitreceipt =0.0;
				double creditpayment=0.0;
				double debitpayment =0.0;
				subheadListing SUBH_L=new subheadListing();
				productexpensesListing PEXP_L=new productexpensesListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				if(request.getParameter("head_account_id")!=null){
					hoaid=request.getParameter("head_account_id");
					if(hoaid.equals("SasthanDev")){
						headgroup="2";
						hoaid="4";
					} else if(hoaid.equals("4")){
						headgroup="1";
						reporttype = "pro-counter";
						reporttype1 = "Development-cash";	
					}else if(hoaid.equals("1")){
						reporttype = "Charity-cash";
						reporttype1 = "Not-Required";	
					}
					else if(hoaid.equals("3")){
						reporttype = "poojastore-counter";
						reporttype1 = "Not-Required";
					}
					else if(hoaid.equals("5")){
						reporttype = "Sainivas";
						reporttype1 = "Not-Required";	
					} 
					/* else if(hoaid.equals("3")){
						reporttype = "poojastore-counter";
						reporttype1 = "Not-Required";	
					}else if(hoaid.equals("5")){
						reporttype = "Sainivas";
						reporttype1 = "Not-Required";	
					} */
				}
				List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid, "revenue",headgroup);
				List banklist=BNKL.getBanksBasedOnHOA(hoaid);
				List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"expenses",headgroup);
				List assetsmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"Assets",headgroup);
				%>
				<div class="icons">
				<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
					<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
<div class="printable" id="tblExport">	
<% 
if(request.getParameter("fromDate") != null && !request.getParameter("toDate").equals("")){
	onlyfromdate=fromdate;
	fromdate=fromdate+" 00:00:00";
	onlytodate=todate;
	todate=todate+" 23:59:59";
	Todate=todate;
	String finStartYr = request.getParameter("finYear").substring(0, 4);
	String finYr = request.getParameter("finYear");
	if(request.getParameter("cumltv").equals("cumulative"))
	{
		onlyfromdate = finStartYr+"-04-01";
		fromdate = finStartYr+"-04-01"+" 00:00:00";
	}
%>
<table width="95%"  cellspacing="0" cellpadding="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
<tbody> <tr>
						<td colspan="3" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hoaid) %></td>
					</tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">RECEIPTS &amp; PAYMENTS  Report</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=onlyfromdate %> TO <%=onlytodate %> </td></tr>
</tbody></table>

<table width="90%" border="1" cellspacing="0" cellpadding="0" style="margin:0 auto;" class="new-table">
  <tbody><tr>
    <td width="38%" height="29" align="center" style="font-size:15px">Account(Debits)</td>
    <td width="29%" align="center" style="font-size:15px">Debits</td>
    <td width="33%" align="center" style="font-size:15px">Credits</td>
  </tr>
  <tr>
    <td height="28" colspan="3" bgcolor="#99FF00" style="font-weight:bold;">RECEIPTS</td>
   
  </tr>
  <tr>
    <td height="28">&nbsp;</td>
    <td height="28" align="right">0.00</td>
    <td height="28" align="right">0.00</td>
  </tr>
  <%
  bankbalanceListing BBAL_L=new mainClasses.bankbalanceListing();
  if(banklist.size()>0){ 
			for(int i=0;i<banklist.size();i++){
				BNK=(bankdetails)banklist.get(i);
				for(int j=0;j<typeofbanks.length;j++){
				//bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),"",fromdate,typeofbanks[j]);
				bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",fromdate,typeofbanks[j],"");
				double finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], request.getParameter("finYear"));
				bankopeningbal += finOpeningBal;
				if(bankopeningbal >0 || bankopeningbal<0){
								%>
  <tr>
    <td height="28" style="color:#F00; font-weight:bold;"><%=BNK.getbank_name() %> <%=typeofbanks[j]%></td>
    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(bankopeningbal) %></td>

     </tr>
    <%bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
					bankopeningbal=0.0;
						}}} }%>
		<%
		String addedDate1=SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
			 double totalcountercash =0.0;
			  double totalcashpaid =0.0;
			  double grandtotal = 0.0;
			  double finOpeningBal2 = BBAL_L.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));
			  if(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate) != null && !CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate).equals("")){
				  totalcountercash = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate));
			  }
			  if(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1)!= null && !BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1).equals("")){
				  totalcashpaid = Double.parseDouble(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1));
			  }
			  double onlinedeposits = 0.0;
			  if(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1)!= null && !BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1).equals("")){
				  onlinedeposits = Double.parseDouble(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1));
			  }
			  grandtotal = finOpeningBal2+totalcountercash-totalcashpaid-onlinedeposits;
			 
			  double proopeningbal = grandtotal;
			 
			 /* double proopeningbal = 0.0;
			proopeningbal = BNKCAL.getProCashOpeningBalance(fromdate, hoaid, reporttype, reporttype1); */
			//proopeningbal = BNKCAL.getProCashOpeningBalance(finStartDate,fromdate, hoaid, reporttype, reporttype1);
		%>
	<tr>
    <td height="28" style="color:#F00; font-weight:bold;">CASH ACCOUNT</td>
    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(proopeningbal)%></td>
        <%-- <%System.out.println("proopeningbal=>"+proopeningbal); %> --%>
     </tr>	
						
						 <%creditreceipt=creditreceipt+bankopeningbalcredit+proopeningbal;
							if(incomemajhdlist.size()>0){ 
								for(int i=0;i<incomemajhdlist.size();i++){
									MH=(majorhead)incomemajhdlist.get(i); 
								%>
 
  <tr>
    <%
	    double debitAmountOpeningBal = 0.0;
		double creditAmountOpeningBal = 0.0;
		
		if(request.getParameter("cumltv").equals("cumulative"))
		{
			debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "");
			creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", "");
		}
    %>
    <td height="28" align="center"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');" style=" color: #934C1E; font-weight:bold;"><%=MH.getname() %></a></td>
   <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid))) %></td>
    <%
    debitreceipt=debitreceipt+debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));%>
    <%-- <% if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid).equals("00")){ %> --%>
    <%-- <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid)) %></td> --%>
    <%-- <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td> --%>
    <%-- <%System.out.println("bbb=>"+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))); %> --%>
   <%--  <%creditreceipt=creditreceipt+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid)); } else{ %> --%>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,hoaid))) %></td>
    <%-- <%creditreceipt=creditreceipt+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); } %> --%>
    <%creditreceipt=creditreceipt+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,hoaid)); /* }  */%>
    
  </tr>
 <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									double amount =Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) +Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())));
									//System.out.println("dddd=>"+amount);
									double amount2=Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
									//System.out.println("newdd=>"+amount2);
									/* if(!("0.0".equals(""+amount))) */
									double debitAmountOpeningBal2 = 0.0;
								  double creditAmountOpeningBal2 = 0.0;
								  
								if(request.getParameter("cumltv").equals("cumulative"))
								{
								  debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
								  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", "");
								}
									if(!("0.0".equals(""+amount)) ||  !("0.0".equals(""+amount2)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0)
									{
								%>	
  
  <tr>
    <%-- <td height="28" style="color:#F00;"><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Receipts and Payments" style="color:#F00;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></td> --%>
    <td height="28" style="color:#F00;"><%=MNH.getname() %> <%=MNH.getminorhead_id() %></td>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(debitAmountOpeningBal2+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid))) %></td>
    <%-- <% System.out.println("eeee"+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)));%> --%>
    <%debitreceipt=debitreceipt+debitAmountOpeningBal2+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
    /* if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){  */
    	if(!CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ 
    %>
    <%-- <td height="28" align="center" style="color:#F00;"><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) %></td> --%>
    <%-- <% System.out.println("ffff"+CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00"));%> --%>
    <%-- <td height="28" align="center" style="color:#F00;"><%=DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td> --%>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
    <%} else{ %>
    <%-- <%System.out.println("ggg"+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))); %> --%>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) +Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
   <%} %>
   <!-- the below lines are added by srinivas -->
   <%-- <% if(!PEXP_L.getLedgerSum2(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ %> --%>
   
   
    
  </tr>
 <%List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id()); 
 String subhid= MNH.getminorhead_id();
 if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										cashtype ="online pending";
										cashtype1="othercash";
										cashtype2="offerKind";
										cashtype3="journalvoucher";
									onlyfromdate=fromdate;
									fromdate=fromdate+" 00:00:00";
									onlytodate=todate;
									todate=todate+" 23:59:59";
									//double amount1= Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
									double amount1= Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
									double amount3=Double.parseDouble(PEXP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,hoaid));
									double debitAmountOpeningBal3 = 0.0;
									double creditAmountOpeningBal3 = 0.0;
									if(request.getParameter("cumltv").equals("cumulative"))
									{
										debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "");
										creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", "");
									}
									//System.out.println("hhhh=>"+amount1);
									//if(!("0.0".equals(""+amount1))){
									if(!("0.0".equals(""+amount1)) || !("0.0".equals(""+amount3)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){ 
								%>	
  
  
  
  <tr>
  <%if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %>
    <%-- <td height="28" style="padding-left:15px;"><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000;"><%=SUBH.getname() %></a></td> --%>
    <td height="28" style="padding-left:15px;"><%=SUBH.getname() %></td>
    <%}else { %>
    <%-- <td height="28" style="padding-left:15px;"><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000;"><%=SUBH.getname() %></a></td> --%>
    <td height="28" style="padding-left:15px;"><%=SUBH.getname() %></td>
    <%} %>
    <td height="28" align="left"><%=DF.format(debitAmountOpeningBal3+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid))) %></td>
    <%debit=debit+debitAmountOpeningBal3+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
	if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %>
    <%-- <td height="28" align="left"><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid))) %></td> --%>
    <td height="28" align="left"><%=DF.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid))))%></td>
    <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));} else{ %>
    <td height="28" align="left"><%=DF.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))) %></td>
    <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));} %>
  </tr>
  <%}}}}}}}} %>
  
  
  <!-- <tr>
    <td height="28">Total Rupess)</td>
    <td height="28" align="right">0.00</td>
    <td height="28" align="right">0.00</td>
  </tr> -->
  
  <!-- payments -->
  
   <tr>
    <td height="28" colspan="3" bgcolor="#99FF00" style="font-weight:bold;">PAYMENTS</td>
  </tr>
  
  <tr>
    <td height="28">&nbsp;</td>
    <td height="28" align="right">0.00</td>
    <td height="28" align="right">0.00</td>
  </tr>
  
  <% if(expendmajhdlist.size()>0){ 
	double sainivasTransferOfFund = 0.0;
	double debitSainivasTransferOfFund = 0.0;
	
	if(hoaid.equals("5"))
	{
		if(request.getParameter("cumltv").equals("cumulative"))
		{
			debitSainivasTransferOfFund = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,"81","669", "", "minorhead","Assets", finYr, "debit", "");
		}
		sainivasTransferOfFund = debitSainivasTransferOfFund+Double.parseDouble(PEXP_L.getLedgerSum("669","minorhead_id",fromdate,todate,hoaid));
	}
	
	
	  
	  for(int i=0;i<expendmajhdlist.size();i++){
									MH=(majorhead)expendmajhdlist.get(i);
								
									double debitAmountOpeningBal = 0.0;
									double creditAmountOpeningBal = 0.0;
									
									if(request.getParameter("cumltv").equals("cumulative"))
									{
										debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "");
										creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", "");
									}
									
									if(!PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid).equals("00") || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){
								%>
  <tr>
    <td height="28"align="center"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');" style=" color: #934C1E; font-weight:bold;"><%=MH.getname() %></a></td>
    <%if(MH.getmajor_head_id().equals("81")) {%>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(debitAmountOpeningBal-sainivasTransferOfFund+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid))) %></td>
	<%debitpayment=debitpayment-sainivasTransferOfFund+debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));}else{%>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid))) %></td>
	<%debitpayment=debitpayment+debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));}%>
	<%if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype2,hoaid).equals("00")){ %>   
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype2,hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td>
    <%creditpayment=creditpayment+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype2,hoaid)); } else{ %>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td>
    <%creditpayment=creditpayment+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); } %>
    
  </tr>
 
  <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									String amountpayment = PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									double debitAmountOpeningBal2 = 0.0;
									double creditAmountOpeningBal2 = 0.0;
									  
									if(request.getParameter("cumltv").equals("cumulative"))
									{
									  debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
									  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", "");
									}
									
									if((!("00".equals(""+amountpayment)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0) && !MNH.getminorhead_id().equals("669")){
									/* if(!("00".equals(""+amountpayment)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){ */
								%>
  <tr>
    <%-- <td height="28"><span style="padding-left:25px; color:#F00;"><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Receipts and Payments" style="color:#F00;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></span></td> --%>
    <td height="28"><span style="padding-left:25px; color:#F00;"><%=MNH.getname() %> <%=MNH.getminorhead_id() %></span></td>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(debitAmountOpeningBal2+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid))) %></td>
    <%-- <%debitpayment = debitpayment + Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)); --%>
   <% if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ %>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
    <%}else{ %>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
  <%} %>
  </tr>

<% 
List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
String subhid= MNH.getminorhead_id();
if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										cashtype ="online pending";
										cashtype1="othercash";
										cashtype2="offerKind";
										cashtype3="journalvoucher";
									onlyfromdate=fromdate;
									fromdate=fromdate+" 00:00:00";
									onlytodate=todate;
									todate=todate+" 23:59:59";
									String amountpayment2= PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									
									double debitAmountOpeningBal3 = 0.0;
									double creditAmountOpeningBal3 = 0.0;
									if(request.getParameter("cumltv").equals("cumulative"))
									{
										debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "");
										creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", "");
									}	
									
									if(!("00".equals(""+amountpayment2)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){
								%>
   <tr>
   <%if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %>
    <%-- <td height="28"><span style="padding-left:35px;"> <a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000;"><%=SUBH.getname() %></a></span></td> --%>
    <td height="28"><span style="padding-left:35px;"><%=SUBH.getname() %></span></td>
    <%}else { %>
    <%-- <td height="28"><span style="padding-left:35px;"> <a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000;"><%=SUBH.getname() %></a></span></td> --%>
    <td height="28"><span style="padding-left:35px;"><%=SUBH.getname() %></span></td>
    <%} %>
    <td height="28" align="left"><%=DF.format(debitAmountOpeningBal3+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid))) %></td>
    <%debit=debit+debitAmountOpeningBal3+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
									if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %>
    <td height="28" align="left"><%=DF.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,cashtype2,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))) %></td>
    <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,cashtype2,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));} else{ %>
    <td height="28" align="left"><%=DF.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,cashtype2,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))) %></td>
    <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,cashtype2,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));} %>
  </tr>
  <%}}}}}}}}} %>
  
  <!-- Assets -->
  
  <tr>
    <td height="28" colspan="3" bgcolor="#99FF00" style="font-weight:bold;">ASSETS</td>
  </tr>
  
  <tr>
    <td height="28">&nbsp;</td>
    <td height="28" align="right">0.00</td>
    <td height="28" align="right">0.00</td>
  </tr>
  
  <% 
    String pettycashMajorhead = "";
	String pettycashMinorhead = "";
	String pettycashSubhead = "";
	double pettycashAmount = 0.0;
	
	if(hoaid.equals("1"))
	{
		pettycashMajorhead = "54";
		pettycashMinorhead = "425";
		pettycashSubhead = "21450";
	}
	else if(hoaid.equals("4"))
	{
		pettycashMajorhead = "34";
		pettycashMinorhead = "235";
		pettycashSubhead = "21452";
	}
	else if(hoaid.equals("5"))
	{
		pettycashMajorhead = "84";
		pettycashMinorhead = "633";
		pettycashSubhead = "21405";
	}
	
  if(assetsmajhdlist.size()>0){ 
	  for(int i=0;i<assetsmajhdlist.size();i++){
		  MH=(majorhead)assetsmajhdlist.get(i);
									/* if(!MH.getname().trim().equals("LOANS AND ADVANCES")){ */
		double debitAmountOpeningBal = 0.0;
		double creditAmountOpeningBal = 0.0;
		
		if(request.getParameter("cumltv").equals("cumulative"))
		{
			debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "");
			creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", "");
		}
		
		double ledgerSum = Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
		
		if(MH.getmajor_head_id().equals("54") || MH.getmajor_head_id().equals("34") || MH.getmajor_head_id().equals("84"))
		{
			pettycashAmount = Double.parseDouble(PEXP_L.getLedgerSum2(pettycashSubhead,"sub_head_id",pettycashMinorhead,"minorhead_id",fromdate,todate,hoaid));
			ledgerSum -= pettycashAmount;
		}
		
	/* if(!PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid).equals("00") || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){ */
	if(ledgerSum > 0 || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){							
	%>
  <tr>
    <td height="28"align="center"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');" style=" color: #934C1E; font-weight:bold;"><%=MH.getname() %></a></td>
    <%if(MH.getmajor_head_id().equals("54") || MH.getmajor_head_id().equals("34") || MH.getmajor_head_id().equals("84")) {%>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(debitAmountOpeningBal-pettycashAmount+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid))) %></td>
	<%debitpayment=debitpayment+debitAmountOpeningBal-pettycashAmount+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));}else{%>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid))) %></td>
	<%debitpayment=debitpayment+debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));}%>
	<%if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid).equals("00")){ %>   
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td>
    <%creditpayment=creditpayment+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); } else{ %>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=DF.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td>
    <%creditpayment=creditpayment+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); } %>
    
  </tr>
 
  <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									String amountpayment = PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									double amountpaymentInDouble = Double.parseDouble(amountpayment);
									double debitAmountOpeningBal2 = 0.0;
									double creditAmountOpeningBal2 = 0.0;
									
									if(MNH.getminorhead_id().equals("425") || MNH.getminorhead_id().equals("235") || MNH.getminorhead_id().equals("633"))
									{
										amountpaymentInDouble -= pettycashAmount;
									}
									  
									if(request.getParameter("cumltv").equals("cumulative"))
									{
									  debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
									  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", "");
									}
									
									/* if(!("00".equals(""+amountpayment)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){ */
										if(amountpaymentInDouble > 0 || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){
								%>
  <tr>
    <%-- <td height="28"><span style="padding-left:25px; color:#F00;"><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Receipts and Payments" style="color:#F00;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></span></td> --%>
    <td height="28"><span style="padding-left:25px; color:#F00;"><%=MNH.getname() %> <%=MNH.getminorhead_id() %></span></td>
   <%if(MNH.getminorhead_id().equals("425") || MNH.getminorhead_id().equals("235") || MNH.getminorhead_id().equals("633")){ %>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(debitAmountOpeningBal2-pettycashAmount+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid))) %></td><%}else{ %>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(debitAmountOpeningBal2+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid))) %></td><%} %>
    <%-- <%debitpayment = debitpayment + Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)); --%>
   <% if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ %>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
    <%}else{ %>
    <td height="28" align="center" style="color:#F00;"><%=DF.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
  <%} %>
  </tr>

<% 
List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
String subhid= MNH.getminorhead_id();
if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										cashtype ="online pending";
										cashtype1="othercash";
										cashtype2="offerKind";
										cashtype3="journalvoucher";
									onlyfromdate=fromdate;
									fromdate=fromdate+" 00:00:00";
									onlytodate=todate;
									todate=todate+" 23:59:59";
									String amountpayment2= PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									
									double debitAmountOpeningBal3 = 0.0;
									double creditAmountOpeningBal3 = 0.0;
									if(request.getParameter("cumltv").equals("cumulative"))
									{
										debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "");
										creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", "");
									}	
									
									if(SUBH.getsub_head_id().equals("21450") || SUBH.getsub_head_id().equals("21452") || SUBH.getsub_head_id().equals("21405"))
									{
										amountpayment2 = "00";
									}
									if(!("00".equals(""+amountpayment2)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){
								%>
  
  
  
   <tr>
   <%if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %>
    <%-- <td height="28"><span style="padding-left:35px;"> <a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000;"><%=SUBH.getname() %></a></span></td> --%>
    <td height="28"><span style="padding-left:35px;"><%=SUBH.getname() %></span></td>
    <%}else { %>
    <%-- <td height="28"><span style="padding-left:35px;"> <a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000;"><%=SUBH.getname() %></a></span></td> --%>
    <td height="28"><span style="padding-left:35px;"><%=SUBH.getname() %></span></td>
    <%} %>
    <td height="28" align="left"><%=DF.format(debitAmountOpeningBal3+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid))) %></td>
    <%debit=debit+debitAmountOpeningBal3+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
									if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %>
    <td height="28" align="left"><%=DF.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))) %></td>
    <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));} else{ %>
    <td height="28" align="left"><%=DF.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))) %></td>
    <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));} %>
  </tr>
  <%}}}}}}}/* } */}} %>
  
  
  <!-- Assets -->
  
   <tr>
    <td align="center" style=" color: #934C1E; font-weight:bold;" height="28">TOTAL AMOUNT OF PAYMENTS</td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">Rs.<%=DF.format(debitpayment) %></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
  </tr>
  <%
  bankopeningbalcredit=0.0;
  if(banklist.size()>0){ 
  	for(int i=0;i<banklist.size();i++){
  		BNK=(bankdetails)banklist.get(i);
  		for(int j=0;j<typeofbanks.length;j++){
  			//bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),"",todate,typeofbanks[j]);
  			bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",todate,typeofbanks[j],"");
  			double finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], request.getParameter("finYear"));
  			bankopeningbal += finOpeningBal;
  			if(bankopeningbal >0 || bankopeningbal<0){
  %> 
   <tr>
    <td height="28" bgcolor="#CCFF00" style="font-weight:bold;"><%=BNK.getbank_name() %> <%=typeofbanks[j]%></td>
    
    <td height="28" align="center" style="font-weight:bold;"><%=DF.format(bankopeningbal) %></td>
    <td height="28" align="center" style="font-weight:bold;">0.00</td>
  </tr>
  <%
bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
bankopeningbal=0.0;
}}} }
debitpayment=debitpayment+bankopeningbalcredit;

 String addedDate1New=SansthanAccountsDate.getAddedDate(todate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
 double totalcountercashNew =0.0;
  double totalcashpaidNew =0.0;
  double grandtotalNew = 0.0;
  double finOpeningBal2New = BBAL_L.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));
  if(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", todate) != null && !CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", todate).equals("")){
	  totalcountercashNew = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01 00:00:00", todate+" 23:59:59"));
  }
  if(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1New)!= null && !BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1New).equals("")){
	  totalcashpaidNew = Double.parseDouble(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01 00:00:00", addedDate1New+" 23:59:59"));
  }
  double onlinedepositsNew = 0.0;
  if(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1New)!= null && !BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1New).equals("")){
	  onlinedepositsNew = Double.parseDouble(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01 00:00:00", addedDate1New+" 23:59:59"));
  }
  grandtotalNew = finOpeningBal2New+totalcountercashNew-totalcashpaidNew-onlinedepositsNew;
 
  double proclosingbal = grandtotalNew;
 
 
 
/* double proclosingbal = 0.0;
proclosingbal = BNKCAL.getProCashClosingBalance(Todate, hoaid, reporttype, reporttype1); */
//proclosingbal = BNKCAL.getProCashClosingBalance(finStartDate,Todate, hoaid, reporttype, reporttype1);
%>
  <tr>
    <td height="28" bgcolor="#CCFF00" style="font-weight:bold;">CASH ACCOUNT</td>
    
    <td height="28" align="center" style="font-weight:bold;"><%=DF.format(proclosingbal)%></td>
    <td height="28" align="center" style="font-weight:bold;">0.00</td>
  </tr> 
 <%-- <%
System.out.println("credit==>"+credit);
System.out.println("debit==>"+debit);
System.out.println("creditreceipt==>"+creditreceipt);
System.out.println("debitreceipt==>"+debitreceipt);
System.out.println("creditpayment==>"+creditpayment);
System.out.println("debitpayment==>"+debitpayment);
%> --%>
<%-- <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Excess of Incomes over Expenditures</td>
    
    <%if(creditreceipt>debitpayment){	%>
    <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
    <td align="center" style="font-weight:bold; color: #934C1E;"><%=df.format(creditreceipt-debitpayment)%></td>
    <%	debitpayment =creditreceipt;
} else{	%>
<td align="center" style="font-weight:bold; color: #934C1E;"><%=df.format(debitpayment-creditreceipt)%></td>
<td align="center" style="font-weight:bold; color: #934C1E;">-</td>
<%creditreceipt=debitpayment;%>
<%} %>
  </tr> --%>
  
   <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=DF.format(creditreceipt) %></td> 
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=DF.format(creditreceipt) %></td>
    <%-- <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitpayment) %></td> --%>
  </tr>
  </tbody>
</table><%} %>
</div>
</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>