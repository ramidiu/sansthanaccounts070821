<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!--Date picker script  -->

<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
	<%if(session.getAttribute("superadminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page"><br></br>
		<div class="vendor-list">
		
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<div class="sj">
					 <form action="adminPannel.jsp" method="post">  
					    <input type="hidden" name="page" value="offerKinds-dailySalesReport_new"/>
					    <select name="headID">
						<!-- <option value="offer" selected="selected">OFFERED KINDS</option> -->
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %> selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %> selected="selected" <%} %>>CHARITY</option>
						<option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %> selected="selected" <%} %>>POOJA COUNTER</option>
						</select>
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form name="searchForm" id="searchForm" action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" name="page" value="offerKinds-dailySalesReport_new"/>
						<select name="headID" id="headID">
						<!-- <option value="offer" selected="selected">OFFERED KINDS</option> -->
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %> selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %> selected="selected" <%} %>>CHARITY</option>
						<option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %> selected="selected" <%} %>>POOJA COUNTER</option>
						</select></li>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					<!-- <span>
						<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span> -->
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	banktransactionsListing BKTRAL=new banktransactionsListing();
	productsListing PRDL=new productsListing();
	employeesListing EMPL=new employeesListing();
	
	List BANK_DEP=null;
	
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:01";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:01";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:01";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	String hoaID="4";
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoaID=request.getParameter("headID");
	}
	List SAL_list=SALE_L.getOfferKindsPROCollectionsSummaryss(hoaID,fromDate,toDate,"20021");
	subheadListing SUBHL=new subheadListing();
	List SAL_DETAIL=null;
	double totalAmount=0.00;
	double totalAmounts=0.00;
	double quantity=0.00;
	double quantitys=0.00;
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double bankDepositTot=0.00;
	double creaditSaleTot=0.00;
	double cashSaleAmt=0.00;
	if(SAL_list.size()>0){%>
	<div class="printable">
    
             <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;
	margin:0 2%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="10" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;">Offered Kinds Report</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
						<tr>
						<td colspan="10">
						<table width="100%" cellpadding="0" cellspacing="0" >
						<tr><td colspan="10"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
						
							<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
							<td>
							<table  width="100%" cellpadding="0" cellspacing="0">
							<tr>
							<td class="bg" style="font-weight: bold;">INVOICE DATE</td>
							<td class="bg" style="font-weight: bold;">CODE-KIND NAME</td>
							<td class="bg" style="font-weight: bold;">RECEIPT.NO</td>
							<td class="bg" style="font-weight: bold;">DEVOTEE</td>
							<td class="bg" style="font-weight: bold;">NARRATION</td>
								<td class="bg" style="font-weight: bold;">QTY</td>
							<td class="bg" style="font-weight: bold;">RUPEES</td>
						</tr>
						
							<%
							Double totalSaleAmt=0.00;
							Double totDeposit=0.00;
							if(SALE_L.getTotalSalesAmount("","",fromDate)!=null && !SALE_L.getTotalSalesAmount("","",fromDate).equals("")){
								 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount("","",fromDate));
							}
							if(BKTRAL.getEmployeeDepositAmount("",fromDate)!=null && !BKTRAL.getEmployeeDepositAmount("",fromDate).equals("")){
								 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount("",fromDate));}
							Double openingBal=totalSaleAmt-totDeposit;
					
							int openingbal=0;
							int openingbals=0;
							int openbal;
							String productId="";
							String date = request.getParameter("fromDate")+" 00:00:01";
							/*  openingbal=Integer.parseInt(SALE_L.getkindGoldBefore(date,"20021"));  */
							 /* System.out.println(openingbal); */ 
							/* openingbals =openingbals+(openingbal -Integer.parseInt(SALE_L.getKindSilverbyBefore(date,"20027")));   */ 
							/* System.out.println(openingbals);  */
							/*  System.out.println(openingbal-openingbals); */ 
							 /* openbal=openingbal-openingbals;  */
							%>
							 <%-- <tr>
									<td colspan="4" align="center" style="font-weight: bold; color: orange;">OPENING BALANCE</td>
							         <td style="font-weight: bold;color: orange;" align="left" colspan="5"><%=openbal %></td>
									</tr>  --%>
							<%for(int i=0; i < SAL_list.size(); i++ ){ 
								SALE=(beans.customerpurchases)SAL_list.get(i); %>
								
							<tr>
							<td><%=chngDateFormat.format(dbDateFormat.parse(SALE.getdate()))%></td>
							<td><%=SALE.getproductId()%>-<%if(PRDL.getProductsNameByCat1(SALE.getproductId(),"")!=null && !PRDL.getProductsNameByCat1(SALE.getproductId(),"").equals("")){ %>
								<%=PRDL.getProductsNameByCat1(SALE.getproductId(),"")%><%}else{ %>
								<%=SUBHL.getMsubheadnames(SALE.getproductId(),hoaID)%>
								<%} %></td>
							<td><%=SALE.getbillingId()%></td>
							<td><%=SALE.getcustomername()%></td>
							<td><%=SALE.getextra2()%></td>
							<td><%=SALE.getquantity()%></td>
							<td>&#8377;<%=SALE.gettotalAmmount()%></td></tr>
							<%totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());
							 /* System.out.println(SALE.gettotalAmmount());  */
							quantity=quantity+Double.parseDouble(SALE.getquantity());
							/* System.out.println(quantity); */
							%>
							<!-- <tr><td>#</td><td>fff</td><td>fff</td><td>fff</td><td>fff</td>
							</tr> -->
							<%} %>
							
							</table></td>	
													
							<%-- <td style="padding:0px;"><table  width="100%" cellpadding="0" cellspacing="0">
							<tr>
							
							<td class="bg" style="font-weight: bold;">INVOICE DATE</td>
							<td class="bg" style="font-weight: bold;">CODE-KIND NAME</td>
							<td class="bg" style="font-weight: bold;">RECEIPT.NO</td>
							<td class="bg" style="font-weight: bold;">DEVOTEE</td>
							<td class="bg" style="font-weight: bold;">NARRATION</td>
							<td class="bg" style="font-weight: bold;">QTY</td>
							<td class="bg" style="font-weight: bold;">&#8377;RUPEES</td>
							<!-- <td class="bg" style="font-weight: bold;" align="left">ENTERED BY</td> -->
							
						</tr>
						<%
						Double totalSaleAmtt=0.00;
						Double totDepositt=0.00;
						if(SALE_L.getTotalSalesAmount("","",fromDate)!=null && !SALE_L.getTotalSalesAmount("","",fromDate).equals("")){
							 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount("","",fromDate));
						}
						if(BKTRAL.getEmployeeDepositAmount("",fromDate)!=null && !BKTRAL.getEmployeeDepositAmount("",fromDate).equals("")){
							 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount("",fromDate));}
						Double openingBall=totalSaleAmt-totDeposit;
						List SAL_List=SALE_L.getOfferKindsPROCollectionsSummarys(hoaID,fromDate,toDate,"20027");
						for(int i=0; i < SAL_List.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_List.get(i);
							
							%>
							<tr><td><%=chngDateFormat.format(dbDateFormat.parse(SALE.getdate()))%></td>
								<%totalAmounts=totalAmounts+Double.parseDouble(SALE.gettotalAmmount());
								quantitys=quantitys+Double.parseDouble(SALE.getquantity());
							%> 
								<td><%=SALE.getproductId()%>-<%if(PRDL.getProductsNameByCat1(SALE.getproductId(),"")!=null && !PRDL.getProductsNameByCat1(SALE.getproductId(),"").equals("")){ %>
								<%=PRDL.getProductsNameByCat1(SALE.getproductId(),"")%><%}else{ %>
								<%=SUBHL.getMsubheadnames(SALE.getproductId(),hoaID)%>
								<%} %></td>
								<td><%=SALE.getbillingId()%></td>
								<td><%=SALE.getcustomername()%></td>
								<td><%=SALE.getextra2()%></td>
								<td><%=SALE.getquantity()%></td>
								<td>&#8377;<%=SALE.gettotalAmmount()%></td></tr>

								
							<!-- <tr><td>@</td><td>RRR</td><td>RRR</td><td>RRR</td><td>RRR</td></tr></table></td>
							</tr> -->
								<%} %>
						
							</table> --%>
</table></td></tr>
						
						  
						</table>
						
						</td>
						</tr>
						<%
						/*  quantity=openbal+quantity;  */
						%>
                        
                        
                        
                         <tr style="border: 1px solid #000;"> <td width="50%">
                        	<div style="float:right;padding-right:5px;padding-bottom: 5px;">
                            	<ul style="list-style:none;float:left;">
                                    <li style="font-weight: bold; color: orange;"><%=quantity%></li>
                                     <li style="font-weight: bold; color: orange;">&#8377;<%=df.format(totalAmount)%></li>
                                 </ul>
                                
                            </div>
                           </td>
                           <%-- <td width="50%">
                            <div style="float:right;padding-bottom: 5px;">
                            
                            		<ul style="list-style:none;padding-left:15px;margin-left: 47px; float:left">
                                    <li style="font-weight: bold; color: orange;"><%=quantitys%></li>
                                     <li style="font-weight: bold; color: orange; padding-right: 25px;">&#8377;<%=df.format(totalAmounts)%></li>
                                 </ul>
                                 
                            	
                            </div>
                            </td> --%>
                      
                        </tr>
                        
                        
                        
                        
                        
                        
                        
                        
                        
								<!--<tr style="border: 1px solid #000;">
						<td width="50%" align="right" style="font-weight: bold; color: orange;text-align: center;padding-left: 475px;"><%=quantity%></td>
						<td align="left" style="font-weight: bold; color: orange;">&#8377;<%=df.format(totalAmount)%></td>
						<td style="font-weight: bold;" align="right"></td>
						<td  style="font-weight: bold; color: orange;text-align: center;padding-left: 673px;"><%=quantitys%></td>
						<td  style="font-weight: bold; color: orange;">&#8377;<%=df.format(totalAmounts)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>-->
						<%-- <tr style="border: 1px solid #000;">
						<td width="41%" align="center" style="font-weight: bold; color: orange;" colspan="10">Closing Balance</td>
						
						<td style="font-weight: bold;color: orange;" align="left" colspan="5"><%=(quantity-quantitys) %></td>
						</tr> --%>
					</table>
					</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no offered kinds list found for today!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>