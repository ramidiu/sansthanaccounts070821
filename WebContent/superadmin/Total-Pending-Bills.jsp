<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Receipts And Payments Report</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script>
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
            $("#hoid").change(function () {
              $("#search").submit();
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="GOD" class="beans.godwanstock"/>
<jsp:useBean id="PDA" class="beans.productexpenses"/>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<div class="vendor-page">
<div class="vendor-list">
<div style="text-align: center;"></div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="clear"></div>
<div class="total-report">
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing(); %>
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="7" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 15px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<form id="search" action="adminPannel.jsp" method="post" >
<tr>
<td width="20%" colspan="4">
<ul>
<%List HA_Lists=HOA_L.getheadofaccounts();
String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
<li><input name="page" type="hidden"  value="Total-Pending-Bills"></input>
<select name="hoid" id="hoid">
<%if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("") ) { if(request.getParameter("hoid").equals(HOA.gethead_account_id())){%> selected="selected" <%}}else if(HOA.gethead_account_id().equals("3")){ %> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select>
<%-- <input type="radio" name="hoid" value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> checked="checked" <%} %> /> <%=HOA.getname() %></li><%}} %> --%>
</ul></td>
<%-- <td width="30%">From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="30%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" name="search" value="Search" class="click bordernone"/></td> --%>
</tr></form>
<!-- <form action="adminPannel.jsp" method="post">
<tr>
<td><input type="hidden" name="page" value="Total-Pending-Bills"></input><input type="submit" name="today" value="Today" class="click bordernone"/></td>
	<td><input type="submit" name="weekly" value="Week Report" class="click bordernone"/></td>
	<td><input type="submit" name="monthly" value="Month Report" class="click bordernone"/></td>
	<td><input type="submit" name="yearly" value="Year Report" class="click bordernone"/></td>
</tr></form> -->
</table>
<%
String hoid="3";
if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")){
	hoid=request.getParameter("hoid");
}
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat CDF= new SimpleDateFormat("dd-MM-yyyy/HH:mm");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
DateFormat  onlyYear= new SimpleDateFormat("yyyy");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String currentDate=(dateFormat2.format(c1.getTime())).toString();
String fromDate=currentDate+" 00:00:01";
String toDate=currentDate+" 23:59:59";
if(request.getParameter("today")!=null && request.getParameter("today").equals("Today")){
	fromDate=currentDate+" 00:00:01";
	toDate=currentDate+" 23:59:59";
}else if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
	c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
	fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
	toDate=(dateFormat2.format(c1.getTime())).toString();
	toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
	c1.getActualMaximum(Calendar.DAY_OF_MONTH);
	int lastday = c1.getActualMaximum(Calendar.DATE);
	c1.set(Calendar.DATE, lastday);  
	toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
	c1.getActualMinimum(Calendar.DAY_OF_MONTH);
	int firstday = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday); 
	fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
	int lastday_y = c1.get(Calendar.YEAR);
	c1.set(Calendar.DATE, lastday_y);  
	c1.getActualMinimum(Calendar.DAY_OF_YEAR);
	int firstday_y = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday_y); 
	Calendar cld = Calendar.getInstance();
	cld.set(Calendar.DAY_OF_YEAR,1); 
	fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
	cld.add(Calendar.YEAR,+1); 
	toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate")+" 00:00:01";
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate")+" 23:59:59";
	}
}
DecimalFormat DF=new DecimalFormat("0.00");
String boardMemApprved="";
String drawingAuthMem="";
indentapprovalsListing APRVL=new indentapprovalsListing();
%>
<div class="printable">
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise"  id="tblExport"  >
<tr><td colspan="13" class="bg-new" style="font-weight: bold;color: red;">TOTAL BILL'S  PENDING-REPORTS FOR <%=HOA_L.getHeadofAccountName(hoid) %></td></tr>
<%mainClasses.bankdetailsListing BNKL=new  mainClasses.bankdetailsListing();
List BKDET=BNKL.getBanksBasedOnHOA(hoid);
if(BKDET.size()>0){%>
<tr><td colspan="13" class="bg-new">BANK BALANCE DETAILS</td></tr>
<%
	for(int i=0;i<BKDET.size();i++){
		BNK=(beans.bankdetails)BKDET.get(i);
%>
<tr>
<td colspan="12" style="text-align: right;"><%=BNK.getbank_name() %></td>
<td  style="text-align: right;"><%=DF.format(Double.parseDouble(BNK.gettotal_amount())) %></td>
</tr>
<tr>
<td colspan="12" style="text-align: right;"><%=BNK.getbank_name() %>-PETTY CASH</td>
<td style="text-align: right;"> <%if(BNK.getextra1()!=null && !BNK.getextra1().equals("")){ %> <%=DF.format(Double.parseDouble(BNK.getextra1())) %> <%}else{ %> 0.00 <%} %> </td>
</tr><%}} %>
<tr><td colspan="8" class="bg-new">BILL DETAILS</td>
<td colspan="5" class="bg-new">BILL STATUS</td></tr>
<tr  >
<td class="bg-new  fontsmall" >S.NO.</td>
<td class="bg-new fontsmall">UNIQUE NO / MSE-NO</td>
<td class="bg-new fontsmall">PO.NO.</td>
<td class="bg-new fontsmall">MSE-ENTRY DATE</td>
<td class="bg-new fontsmall">NARRATION</td>
<td class="bg-new fontsmall">VENDOR</td>
<td class="bg-new fontsmall">VENDOR BILL.NO</td>
<td class="bg-new fontsmall">TOTAL AMOUNT</td>
<td class="bg-new fontsmall">EPIV NO</td>
<td class="bg-new fontsmall">BILL APPROVAL(Board Members) </td>
<td class="bg-new fontsmall">PAYMENTS APPROVAL</td>
<td colspan="2" class="bg-new fontsmall">PAYMENT(Accounts Dept.)</td>
<!-- <td class="bg-new fontsmall">NARRATION</td> -->
</tr>
<%
double totAmt=0.00;
godwanstockListing GODL=new godwanstockListing();
productexpensesListing PROEXPL=new productexpensesListing();
vendorsListing VENDL=new vendorsListing();
List GODSDET=null;List PEXPDET=null;
int sno=1;
List PNDBills=GODL.getTotalPendingBills(hoid, "", "");
if(PNDBills.size()>0){
	for(int i=0;i<PNDBills.size();i++){
		GODSDET=GODL.getStockDetailsBasedOnInvoice(PNDBills.get(i).toString());
		if(GODSDET.size()>0){
			GOD=(godwanstock)GODSDET.get(0);
			PEXPDET=PROEXPL.getproductexpensesBasedInvoice(GOD.getextra1());
			if(PEXPDET.size()>0){
				PDA=(productexpenses)PEXPDET.get(0);
			}
		}
		boardMemApprved=APRVL.getApprovedMemNames(GOD.getextra1());
		drawingAuthMem=APRVL.getApprovedMemNames(PDA.getexpinv_id());
if(!PDA.getextra1().equals("Payment Done")){
	totAmt=totAmt+Double.parseDouble(GOD.getpurchaseRate());%>
<tr>
<td class="fontsmall"><%=sno++ %></td>
<td class="fontsmall"> <%if(PDA.getExtra6()!=null && !PDA.getExtra6().equals("")){ %> <span style="color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PDA.getExtra6()%>')"> <%=PDA.getExtra6() %> </span> <%}else{ %> N/A <%} %>  / <%=PNDBills.get(i) %></td>
<td class="fontsmall"><%=GOD.getExtra15() %></td>
<td class="fontsmall"><%=CDF.format(dateFormat.parse(GOD.getdate())) %></td>
<td class="fontsmall"><%=GOD.getdescription() %></td>
<td class="fontsmall" style="text-align: left;"><%=VENDL.getMvendorsAgenciesName(GOD.getvendorId()) %> <%if(GOD.getExtra7()!=null && !GOD.getExtra7().equals("")){ %> ( <%=GOD.getExtra7() %> ) <%} %></td>
<td class="fontsmall"><%if(GOD.getbillNo()!=null && !GOD.getbillNo().equals("")){%><%=GOD.getbillNo() %> <%}else{ %> ( N/A )<%} %></td>
<td class="fontsmall" style="text-align: right;"><%=DF.format(Double.parseDouble(GOD.getpurchaseRate())) %></td>
<td class="fontsmall"><%=PDA.getexpinv_id() %></td>
<td class="fontsmall"> <span <%if(GOD.getApproval_status().equals("Approved")){ %> style="color: green;" <%}else{ %> style="color: red;" <%} %> > <%=GOD.getApproval_status() %></span> <br></br> <%if(!boardMemApprved.equals("")){ %> (<%=boardMemApprved%>) <%} %> </td>
<td class="fontsmall"><%if(PDA.getextra4().equals("BoardApprove") && PDA.getextra4()!=null && !PDA.getextra4().equals(" ")){ %> Drawing Authority Approved <br></br><%} %> <%if(!drawingAuthMem.equals("")){ %> (<%=drawingAuthMem %>) <%} %> </td>
<td colspan="2" class="fontsmall"><%if(!PDA.getextra1().equals("Payment Done") && PDA.getextra1()!=null && !PDA.getextra1().equals(" ")){ %> Payment need to issue <%} %></td>
<%-- <td class="fontsmall"><%=PDA.getnarration() %></td> --%>
</tr><%}}} %>
<tr>
<td colspan="7"  style="text-align:right"><span>Grand Total</span></td>
<td style="text-align:right"><%=DF.format(totAmt) %></td>
<td colspan="5"></td>
</tr>
</table>
</div>
</div>
</div>
</div>
</body>
</html>