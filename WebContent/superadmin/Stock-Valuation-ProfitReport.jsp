<%@page import="beans.products"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SALE VAT REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!-- <link href="css/printStyle.css" type="text/css" rel="stylesheet" /> -->
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>

<link rel="stylesheet" href="admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
function productsearch(){

	  var data1=$('#productname').val();
	  var headID="3";
	  $.post('searchProducts.jsp',{q:data1,page:"",hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
function sortByVat(){
	var vat=$("#vat").val();
	$("#formLoad").submit();
	//location.reload('godwanForm.jsp?poid='+poid); 
	//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("superadminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="PUR" class="beans.godwanstock" />
		<jsp:useBean id="SAL" class="beans.customerpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
				
				<%-- <div class="sj">
					  <form action="adminPannel.jsp" method="post">  
					    <input type="hidden" name="page" value="Profit-Report"></input>
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div> --%>
				<%if(request.getParameter("con")!=null && request.getParameter("con").equals("both")){}else{ %>
				<form action="adminPannel.jsp" method="post" id="formLoad">
				<div class="search-list">
					<ul>
						<li >
						<select name="stocktype">
						<option value="">SELECT</option>
						<option value="godown" <%if(request.getParameter("stocktype")!=null && request.getParameter("stocktype").equals("godown")){ %> selected="selected"  <%} %> >GODOWN</option>
						<option value="poojastore" <%if(request.getParameter("stocktype")!=null && request.getParameter("stocktype").equals("poojastore")){ %> selected="selected"  <%} %>>POOJA STORE</option>
						</select>
						<input type="hidden" name="page" value="Stock-Valuation-ProfitReport"></input>
						</li>
				<li><input type="submit" class="click" name="search" value="Search"></input></li>
				</ul>
				</div>
				</form><%} %>
				<div class="icons">
					<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	productsListing PROL=new productsListing();
	List PRODL=null;
	List Sale_List=null;
	DecimalFormat df = new DecimalFormat("0.00");
	String vat[]={"0","5","14.5"}; 
	String stocktype="godown";
	if(request.getParameter("stocktype")!=null && !request.getParameter("stocktype").equals("")){
		stocktype=request.getParameter("stocktype");
	}
	double vatAmt=0.00;
	double vatTotalAmt=0.00;
	double totalAmount=0.00;
	double VatIOAmt=0.00;
	double VatOPAmt=0.00;
	double ProfitAmt=0.00;
	double StoreVatIOAmt=0.00;
	double StoreVatOPAmt=0.00;
	double StoreProfitAmt=0.00;
	double TotProfitAmt=0.00;
	double TotStoreProfitAmt=0.00;
	double totCollAmt=0.00;
	double totAmt=0.00;
	double diffVatTotAmt=0.00;
	double diffVatTotAmtStore=0.00;
	double totValue=0.00;
	double totValueStore=0.00;
	double Qty=0.00;
	double MrpRate=0.00;
	String proName="";
	/* if(Sale_List.size()>0){ */%>
	<div class="printable">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}
</style>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<jsp:useBean id="PRO" class="beans.products"/>
<%if(request.getParameter("con")!=null &&request.getParameter("con").equals("both")){ %>
	<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="16" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST POOJA STORES</td></tr>
						<tr><td colspan="16" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
						<tr><td colspan="16" align="center" style="font-weight: bold;">GODOWN & POOJA STORE STOCK  VALUATION PROFIT REPORT</td></tr>
						<tr>
						<td colspan="16">
						<table width="100%" cellpadding="0" cellspacing="0" border='1'>
						<tr><td colspan="16"><table width="100%" cellpadding="0" cellspacing="0" border='1' class="yourID">
						<tr>
							<td class="bg" width="3%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="20%" style="font-weight: bold;">PRODUCT NAME</td>
							<td class="bg" width="4%" style="font-weight: bold;" align="center">PURCHASE RATE </td>
							<td class="bg" width="4%" style="font-weight: bold;" align="center">SALE RATE </td>
							<td class="bg" width="5%" style="font-weight: bold;" align="center">GODOWN VAT I/O (VAT ON PURCHASES) </td>
							<td class="bg" width="5%" style="font-weight: bold;" align="right">GODOWN VAT O/P (VAT ON SALES)</td>
							<td class="bg" width="8%" style="font-weight: bold;" align="right">GODOWN Difference In Vat Amount </td>
							<td class="bg" width="4%" style="font-weight: bold;" align="right">GODOWN QTY </td>
							<td class="bg" width="5%" style="font-weight: bold;" align="right">STORE VAT I/O (VAT ON PURCHASES) </td>
							<td class="bg" width="5%" style="font-weight: bold;" align="right">STORE VAT O/P (VAT ON SALES)</td>
							<td class="bg" width="8%" style="font-weight: bold;" align="right">STORE Difference In Vat Amount </td>
							<td class="bg" width="4%" style="font-weight: bold;" align="right">STORE QTY </td>
							<td class="bg" width="5%" style="font-weight: bold;" align="right">TOTAL STOCK QTY </td>
							<td class="bg" width="8%" style="font-weight: bold;" align="right">TOTAL VALUE GODOWN </td>
							<td class="bg" width="8%" style="font-weight: bold;" align="right">TOTAL VALUE STORE </td>
							<td class="bg" width="8%" style="font-weight: bold;" align="right">PROFIT</td>
						</tr>
					</table></td></tr>
						<%
						if(vat.length>0){
						for(int v=0;v<vat.length;v++){
							PRODL=PROL.getPoojaStoreStockWithVat("3", vat[v],"");
						if(PRODL.size()>0){%>
						<tr align="center" style="font-size: large;"><td class="bg" colspan="16"  style="font-weight: bold;">"<%=vat[v] %> %" Vat product sales </td></tr>
						<%for(int i=0; i < PRODL.size(); i++ ){ 
							PRO=(products)PRODL.get(i);
							if(PRO.getproductName().length()>15){
								proName=PRO.getproductName().substring(0,15)+"..";
							}else{
								proName=PRO.getproductName();
							}
							%>
						    <tr style="position: relative;">
								<td style="font-weight: bold;" width="3%"><%=i+1%></td>
								<td style="font-weight: bold;"width="20%"><%=PRO.getproductId()%> - <%=proName%></td>
								<%
								//vatAmt=Double.parseDouble(SAL.getrate())*(Double.parseDouble(vat[v])/100);
								//vatTotalAmt=vatTotalAmt+vatAmt;
								
									VatIOAmt=((Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(PRO.getbalanceQuantityGodwan()))*(Double.parseDouble(PRO.getvat())/100));
									VatOPAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(PRO.getbalanceQuantityGodwan()))*(Double.parseDouble(PRO.getvat1())/100);
									ProfitAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(PRO.getbalanceQuantityGodwan()))-(Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(PRO.getbalanceQuantityGodwan()))-VatOPAmt;
								
									StoreVatIOAmt=((Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(PRO.getbalanceQuantityStore()))*(Double.parseDouble(PRO.getvat())/100));
									StoreVatOPAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(PRO.getbalanceQuantityStore()))*(Double.parseDouble(PRO.getvat1())/100);
									StoreProfitAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(PRO.getbalanceQuantityStore()))-(Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(PRO.getbalanceQuantityStore()))-StoreVatOPAmt;
								
								TotProfitAmt=TotProfitAmt+(ProfitAmt);
								TotStoreProfitAmt=TotStoreProfitAmt+StoreProfitAmt;
								diffVatTotAmt=diffVatTotAmt+(VatOPAmt-VatIOAmt);
								diffVatTotAmtStore=diffVatTotAmtStore+(StoreVatOPAmt-StoreVatIOAmt);
								MrpRate=(Double.parseDouble(PRO.getsellingAmmount()))+(Double.parseDouble(PRO.getsellingAmmount()))*(Double.parseDouble(PRO.getvat1())/100);
								totValue=totValue+(Double.parseDouble(PRO.getbalanceQuantityGodwan())*MrpRate);
								totValueStore=totValueStore+(Double.parseDouble(PRO.getbalanceQuantityStore())*MrpRate);
								%>
								<td width="4%" align="center"><%=PRO.getpurchaseAmmount()%></td>
								<td  width="4%" align="center"><%=PRO.getsellingAmmount()%></td>
								<td width="5%" align="center"><%=df.format(VatIOAmt)%></td>							
								<td width="5%" align="right"><%=df.format(VatOPAmt)%></td>
								<td  width="8%" align="right"><%=df.format(VatOPAmt-VatIOAmt)%></td>
								<td  width="4%" align="right"><%=PRO.getbalanceQuantityGodwan() %> </td>
								<td width="5%" align="right"><%=df.format(StoreVatIOAmt)%></td>							
								<td width="5%" align="right"><%=df.format(StoreVatOPAmt)%></td>
								<td  width="8%" align="right"><%=df.format(StoreVatOPAmt-StoreVatIOAmt)%></td>
								<td  width="4%" align="right"><%=PRO.getbalanceQuantityStore() %> </td>
								<td  width="5%" align="right"><%=df.format(Double.parseDouble(PRO.getbalanceQuantityGodwan())+Double.parseDouble(PRO.getbalanceQuantityStore()))%></td>
								<td  width="8%" align="right"><%=df.format(Double.parseDouble(PRO.getbalanceQuantityGodwan())*MrpRate)%></td>
								<td  width="8%" align="right"><%=df.format(Double.parseDouble(PRO.getbalanceQuantityStore())*MrpRate)%></td>
								<td style="font-weight: bold;" width="8%" align="right">&#8377; <%=df.format(ProfitAmt+StoreProfitAmt)%></td>
								<%-- <td style="font-weight: bold;" width="10%" align="center">&#8377; <%=df.format(Double.parseDouble(SAL.gettotalAmmount()))%></td> --%>
							</tr>
						<%}} %>
						<%}}%>
					   </table>
					   </td>
					   </tr>
						<tr style="border: 1px solid #000;">
							<td colspan="6" width="41%" align="right"></td>
							<td style="font-weight: bold;" width="8%" colspan="1"  align="right"><%=df.format(diffVatTotAmt) %></td>
							<td colspan="3"  width="14%" align="right"></td>
							<td style="font-weight: bold;" colspan="1" width="8%"  align="right"><%=df.format(diffVatTotAmtStore) %></td>
							<td colspan="2" width="9%"  align="right"></td>
							<%-- <td align="right" style="font-weight: bold;"> &#8377; <%=df.format(diffVatTotAmt+diffVatTotAmtStore)%> </td> --%>
							<td style="font-weight: bold;" width="8%" colspan="1"  align="right"><%=df.format(totValue) %></td>
							<td style="font-weight: bold;" width="8%" colspan="1"  align="right"><%=df.format(totValueStore) %></td>
							<%-- <td style="font-weight: bold;" align="right">Total  Value |&#8377; <%=df.format(totValue+totValueStore)%></td> --%>
							<td style="font-weight: bold;" width="8%" align="right">&#8377; <%=df.format(TotProfitAmt+TotStoreProfitAmt)%></td>
						</tr>
						<tr style="border: 1px solid #000;">
							<td colspan="6" width="41%" align="right"></td>
							<td style="font-weight: bold;" width="8%" colspan="1"  align="right"></td>
							<td colspan="3"  width="14%" align="right">(GODOWN VAT DIFF AMT+STORE VAT DIFF AMOUNT)</td>
							<td style="font-weight: bold;" colspan="1" width="8%"  align="right"><%=df.format(diffVatTotAmtStore+diffVatTotAmt) %></td>
							<td colspan="2" width="9%"  align="right"></td>
							<%-- <td align="right" style="font-weight: bold;"> &#8377; <%=df.format(diffVatTotAmt+diffVatTotAmtStore)%> </td> --%>
							<td style="font-weight: bold;" width="8%" colspan="1"  align="right">(GODOWN TOTAL VALUE+STORE TOTAL VALUE)</td>
							<td style="font-weight: bold;" width="8%" colspan="1"  align="right"><%=df.format(totValueStore+totValue) %></td>
							<%-- <td style="font-weight: bold;" align="right">Total  Value |&#8377; <%=df.format(totValue+totValueStore)%></td> --%>
							<td style="font-weight: bold;" width="8%" align="right">&#8377; <%=df.format(TotProfitAmt+TotStoreProfitAmt)%></td>
						</tr>
	</table>
<%}else{ %>

					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="11" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST POOJA STORES</td></tr>
						<tr><td colspan="11" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
						<tr><td colspan="11" align="center" style="font-weight: bold;"><%=stocktype.toUpperCase() %> STOCK  VALUATION PROFIT Report</td></tr>
						<tr>
						<td colspan="11">
						<table width="100%" cellpadding="0" cellspacing="0" border='1'>
						<tr><td colspan="11"><table width="100%" cellpadding="0" cellspacing="0" border='1' class="yourID">
						<tr>
							<td class="bg" width="3%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="40%" style="font-weight: bold;" align="center">PRODUCT NAME</td>
							<td class="bg" width="4%" style="font-weight: bold;" align="center">PURCHASE RATE </td>
							<!-- <td class="bg" width="4%" style="font-weight: bold;" align="center">VAT % </td> -->
							<td class="bg" width="4%" style="font-weight: bold;" align="center">SALE RATE </td>
							<td class="bg" width="8%" style="font-weight: bold;" align="center">VAT I/O (VAT ON PURCHASES) </td>
							<!-- <td class="bg" width="4%" style="font-weight: bold;" align="center">VAT % </td> -->
							<td class="bg" width="8%" style="font-weight: bold;" align="center">VAT O/P (VAT ON SALES)</td>
							<td class="bg" width="4%" style="font-weight: bold;" align="center">Difference In Vat Amount </td>
							<td class="bg" width="4%" style="font-weight: bold;" align="center">STOCK QTY </td>
							<td class="bg" width="5%" style="font-weight: bold;" align="center">MRP Rate </td>
							<td class="bg" width="5%" style="font-weight: bold;" align="center">TOTAL VALUE </td>
							<td class="bg" width="8%" style="font-weight: bold;" align="center">PROFIT</td>
						</tr>
					</table></td></tr>
						<%
						if(vat.length>0){
						for(int v=0;v<vat.length;v++){
							PRODL=PROL.getPoojaStoreStockWithVat("3", vat[v],stocktype);
						if(PRODL.size()>0){%>
						<tr align="center" style="font-size: large;"><td class="bg" colspan="13"  style="font-weight: bold;">"<%=vat[v] %> %" Vat product sales </td></tr>
						<%for(int i=0; i < PRODL.size(); i++ ){ 
							PRO=(products)PRODL.get(i);%>
						    <tr style="position: relative;">
								<td style="font-weight: bold;" width="3%"><%=i+1%></td>
								<td style="font-weight: bold;"width="40%" ><%=PRO.getproductId()%> - <%=PRO.getproductName()%></td>
								<%
								//vatAmt=Double.parseDouble(SAL.getrate())*(Double.parseDouble(vat[v])/100);
								//vatTotalAmt=vatTotalAmt+vatAmt;
								if(stocktype.equals("godown")){
									VatIOAmt=((Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(PRO.getbalanceQuantityGodwan()))*(Double.parseDouble(PRO.getvat())/100));
									VatOPAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(PRO.getbalanceQuantityGodwan()))*(Double.parseDouble(PRO.getvat1())/100);
									ProfitAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(PRO.getbalanceQuantityGodwan()))-(Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(PRO.getbalanceQuantityGodwan()))-VatOPAmt;
								}else if(stocktype.equals("poojastore")){
									VatIOAmt=((Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(PRO.getbalanceQuantityStore()))*(Double.parseDouble(PRO.getvat())/100));
									VatOPAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(PRO.getbalanceQuantityStore()))*(Double.parseDouble(PRO.getvat1())/100);
									ProfitAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(PRO.getbalanceQuantityStore()))-(Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(PRO.getbalanceQuantityStore()))-VatOPAmt;
								}
								TotProfitAmt=TotProfitAmt+ProfitAmt;
								diffVatTotAmt=diffVatTotAmt+(VatOPAmt-VatIOAmt);
								if(stocktype.equals("godown")){
									Qty=Double.parseDouble(PRO.getbalanceQuantityGodwan());
								}else if(stocktype.equals("poojastore")){
									Qty=Double.parseDouble(PRO.getbalanceQuantityStore());
								}
								MrpRate=(Double.parseDouble(PRO.getsellingAmmount()))+(Double.parseDouble(PRO.getsellingAmmount()))*(Double.parseDouble(PRO.getvat1())/100);
								totValue=totValue+(Qty*MrpRate);
								%>
								<td width="4%" align="center"><%=PRO.getpurchaseAmmount()%></td>
								<%-- <td width="4%" align="center"><%=PRO.getvat()%> </td> --%>
								<td  width="4%" align="center"><%=PRO.getsellingAmmount()%></td>
								<td width="8%" align="center"><%=df.format(VatIOAmt)%></td>
								
								<%-- <td width="4%" align="center"><%=PRO.getvat1()%> </td> --%>
								<td width="8%" align="center"><%=df.format(VatOPAmt)%></td>
								<td  width="4%" align="center"><%=df.format(VatOPAmt-VatIOAmt)%></td>
								<td  width="4%" align="center"><%=Qty %> </td>
								<td  width="5%" align="center"><%=df.format(0)%></td>
								<td  width="5%" align="center"><%=df.format(Qty*MrpRate)%></td>
								<td style="font-weight: bold;" width="8%" align="center">&#8377; <%=df.format(ProfitAmt)%></td>
								<%-- <td style="font-weight: bold;" width="10%" align="center">&#8377; <%=df.format(Double.parseDouble(SAL.gettotalAmmount()))%></td> --%>
							</tr>
						<%}} %>
						<%}}%>
					   </table>
					   </td>
					   </tr>
						<tr style="border: 1px solid #000;">
							<td colspan="8" width="100%" align="right">Total Vat Differance Amount |</td>
							<td align="right" style="font-weight: bold;"> &#8377; <%=df.format(diffVatTotAmt)%> </td>
							<td style="font-weight: bold;" align="right">Total  Value |&#8377; <%=df.format(totValue)%></td>
							<td style="font-weight: bold;" align="right"> Total  Profit amount | &#8377; <%=df.format(TotProfitAmt)%></td>
						</tr>
					</table>
		<%} %>
	</div>
</div>
</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>