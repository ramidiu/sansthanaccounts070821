<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Approval pending reports</title>
<script>
function popupClose(id){
	if(id=="1"){		
	window.parent.location= 'adminPannel.jsp?page=paymentsApprovalsList';
	} else if(id=="2"){
		window.parent.location= 'adminPannel.jsp?page=indentReportList';	
	} else if(id=="3")
		{
		window.parent.location= 'adminPannel.jsp?page=billApprovalsList';
		}
}
</script>
</head>
<body>
<% if( session.getAttribute("role").equals("GS") || session.getAttribute("role").equals("CM") || session.getAttribute("role").equals("FS") ){
%>
<div style="font-weight: bold;width:420px;" class="unpaid">Payments reports you have to approve.Please <span onclick="return popupClose('1');" style="color: 

blue;cursor: pointer;" >click here</span> to see the payments report list </div> 

<!-- commented by madhav -->
<%-- if( session.getAttribute("role").equals("GS")  || session.getAttribute("role").equals("FS") ){  --%>

<!-- <div style="font-weight: bold;width:420px;" class="unpaid">Payments reports you have to approve.Please <span onclick="return popupClose('1');" style="color: 

blue;cursor: pointer;" >click here</span> to see the payments report list </div> -->
<%-- <%if(session.getAttribute("indaprov")!=null) { %> --%>
<!-- <div style="font-weight: bold;width:420px;" class="unpaid">Indents reports you have to approve.Please <span onclick="return popupClose('2');" style="color: 

blue;cursor: pointer;" >click here</span> to see the indents report list </div> -->
<%-- <%} if(session.getAttribute("bilaprov")!=null) {%> --%>
<!-- <div style="font-weight: bold;width:420px;" class="unpaid">Bills reports you have to approve.Please <span onclick="return popupClose('3');" style="color: 

blue;cursor: pointer;" >click here</span> to see the bills report list </div> -->
<%-- <%}}else if(session.getAttribute("role").equals("CM")){ %> --%>
<!-- <div style="font-weight: bold;width:420px;" class="unpaid">Payments reports you have to approve.Please <span onclick="return popupClose('1');" style="color: 

blue;cursor: pointer;" >click here</span> to see the payments report list </div>	 -->
<%-- <% --%>
<!--  } -->
<%} else { if(session.getAttribute("indaprov")!=null) { %>
<div style="font-weight: bold;width:420px;" class="unpaid">Indents reports you have to approve.Please <span onclick="return popupClose('2');" style="color: blue;cursor: pointer;" >click here</span> to see the indents report list </div>
<%} if(session.getAttribute("bilaprov")!=null) {%>
<div style="font-weight: bold;width:420px;" class="unpaid">Bills reports you have to approve.Please <span onclick="return popupClose('3');" style="color: blue;cursor: pointer;" >click here</span> to see the bills report list </div>
<%} } %>
</body>
</html>
