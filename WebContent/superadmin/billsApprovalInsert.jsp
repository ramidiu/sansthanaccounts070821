<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.godwanstock_scListing"%>
<%@page import="mainClasses.tabletsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<jsp:useBean id="NOG" class="beans.no_genaratorService"/>
<jsp:useBean id="IAP" class="beans.indentapprovalsService"/>
<jsp:useBean id="IND" class="beans.indentService"/>
<jsp:useBean id="GDS" class="beans.godwanstockService"/>
<jsp:useBean id="ID" class="beans.indent"/>
<jsp:useBean id="ID1" class="beans.indent"/>
<jsp:useBean id="PO" class="beans.purchaseorder"/>
<jsp:useBean id="POS" class="beans.purchaseorderService"/>
<jsp:useBean id="GODS" class="beans.godwanstock_scService"/>
<%
godwanstock_scListing GODSList = new godwanstock_scListing();
mainClasses.no_genaratorListing NG_l=new mainClasses.no_genaratorListing();
String poInv_id="";
String indentId[]=request.getParameterValues("indentId");
String quantity="";
String vendorID="";
String approve_id=request.getParameter("approve_id");
String admin_id=session.getAttribute("superadminId").toString();
String description=request.getParameter("feedback");
String admin_status=request.getParameter("status");
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
String role = session.getAttribute("role").toString(); // added by gowri shankar
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String approved_date=(dateFormat.format(c1.getTime())).toString();
String ExpInnvoice_id=request.getParameter("indentInvoiceId");
%>
<jsp:setProperty name="IAP" property="approve_id" value="<%=approve_id%>"/>
<jsp:setProperty name="IAP" property="admin_id" value="<%=admin_id%>"/>
<jsp:setProperty name="IAP" property="description" value="<%=description%>"/>
<jsp:setProperty name="IAP" property="admin_status" value="<%=admin_status%>"/>
<jsp:setProperty name="IAP" property="approved_date" value="<%=approved_date%>"/>
<jsp:setProperty name="IAP" property="indentinvoice_id" value="<%=ExpInnvoice_id%>"/>
<jsp:setProperty name="IAP" property="extra1" value="billapprove"/>
<jsp:setProperty name="IAP" property="extra2" value=""/>
<!-- added by gowri shankar -->
<%if (role.equals("FS") || role.equals("GS") || role.equals("CM")) {%>
<jsp:setProperty name="IAP" property="extra3" value="<%=role%>"/>
<%}	else	{%>
<jsp:setProperty name="IAP" property="extra3" value=""/>
<%}%>
<!-- added by gowri shankar -->
<%=IAP.insert()%><%=IAP.geterror()%>
<%
mainClasses.indentapprovalsListing APPR_L=new mainClasses.indentapprovalsListing();
productsListing PROL=new productsListing();
indentListing INDL=new indentListing();
List IndDetails=null;
List INdProdDet=null;
String price="";
List APP_Det=null;
APP_Det=APPR_L.getIndentDetails(ExpInnvoice_id,"approved");
if(APP_Det.size() == 3 || (role.equals("FS") || role.equals("GS") || role.equals("CM"))){  // changed by gowri shankar
	GDS.updateStatus(ExpInnvoice_id,"Approved");
	
	/* this lines are written by pradeep to update the latest entry in godwanstock_sc table */
			String countInString = GODSList.getCountOfRecordsBasedOnMseId(ExpInnvoice_id);
				int count = Integer.parseInt(countInString);
				if(count == 1)
				{
					GODS.updateStatusInNewGDS(ExpInnvoice_id,"Approved");
				}
				else if(count > 1)
				{
					String entrydate = GODSList.getMaxDateForRecordBasedOnMseId(ExpInnvoice_id);
					
					GODS.updateStatusBasedOnDate(ExpInnvoice_id, entrydate,"Approved"); 
					
				}
	/*END  */
}
/* APP_Det=APPR_L.getIndentDetails(ExpInnvoice_id,"disapproved"); */
APP_Det=APPR_L.getIndentDetails(ExpInnvoice_id,"billdisapproved");
if(APP_Det.size()>0 && APP_Det.size()<=2){
	//IND.updateStatus(ExpInnvoice_id, APP_Det.size()+" members approved");
	System.out.println("status..111."+admin_status);
}else if(APP_Det.size()!=0){
	GDS.updateStatus(ExpInnvoice_id,"Not-Approve");
	System.out.println("status..."+admin_status);
	/* this lines are written by pradeep to update the latest entry in godwanstock_sc table */
	
	String countInString = GODSList.getCountOfRecordsBasedOnMseId(ExpInnvoice_id);
	int count = Integer.parseInt(countInString);
	if(count == 1)
	{
		GODS.updateStatusInNewGDS(ExpInnvoice_id,"Not-Approve");
	}
	else if(count > 1)
	{
		String entrydate = GODSList.getMaxDateForRecordBasedOnMseId(ExpInnvoice_id);
		
		GODS.updateStatusBasedOnDate(ExpInnvoice_id, entrydate,"Not-Approve"); 
		
	}
/*END  */

}
APP_Det=APPR_L.getIndentDetails(ExpInnvoice_id,"postpone");
if(APP_Det.size()>0 && APP_Det.size()<=2){
	//IND.updateStatus(ExpInnvoice_id, APP_Det.size()+" members approved");
}else if(APP_Det.size()!=0){
	GDS.updateStatus(ExpInnvoice_id,"Postpone");
	
	/* this lines are written by pradeep to update the latest entry in godwanstock_sc table */
	
	String countInString = GODSList.getCountOfRecordsBasedOnMseId(ExpInnvoice_id);
	int count = Integer.parseInt(countInString);
	if(count == 1)
	{
		GODS.updateStatusInNewGDS(ExpInnvoice_id,"Postpone");
	}
	else if(count > 1)
	{
		String entrydate = GODSList.getMaxDateForRecordBasedOnMseId(ExpInnvoice_id);
		
		GODS.updateStatusBasedOnDate(ExpInnvoice_id, entrydate,"Postpone"); 
		
	}
/*END  */

}
 response.sendRedirect("adminPannel.jsp?page=billApprovalsList");  
%>