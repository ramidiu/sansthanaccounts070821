<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Total sale report</title>

<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
</head>

<body>
<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<form id="filtration-form"  action="adminPannel.jsp" method="post">
<div class="search-list">
<ul>
<li><input type="hidden" name="page" value="billApprovalsList"></li>
<li><select name="status" onchange="filter();">
<option value="">Sort by Approval Status</option>
<option value="Approved"   selected="selected">Approved List</option>
<option value="Not-Approve"  selected="selected" >Not-Approved List</option>
</select></li>
</ul>
</div>
</form>

<div style="text-align: center;">Bill  List</div>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>
</span></div>
<div class="clear"></div>
<div class="total-report">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="4" class="bg-new">Sri Sai Nivas Accounts</td></tr>
<tr>
<td width="20%">
<ul>
<li><input type="checkbox" /> Master suite</li>
<li><input type="checkbox" /> Master suite</li>
<li><input type="checkbox" /> Master suite</li>
<li><input type="checkbox" /> Master suite</li>
<li><input type="checkbox" /> Master suite</li>
</ul></td>
<td width="30%">From Date <input type="text" /> <img src="../images/calendar-icon.png" /></td>
<td width="30%">To Date <input type="text" /> <img src="../images/calendar-icon.png" /></td>
<td width="15%"><input type="submit" value="Submit" class="click bordernone"/></td>
</tr>
<tr>
<td><input type="submit" value="Today" class="click bordernone"/></td>
<td><input type="submit" value="Current Week" class="click bordernone"/></td>
<td><input type="submit" value="Current Month" class="click bordernone"/></td>
<td><input type="submit" value="Current Year" class="click bordernone"/></td>
</tr>
</table>


<table width="95%" cellpadding="0" cellspacing="0" class="indent-wise">
<tr>
<td colspan="3" class=borderleftcolr>Indent</td>
<td colspan="3" class="borderleftcolr">Purchaseorder</td>
<td colspan="3" class="borderleftcolr">MSE</td>
<td colspan="3" class="borderleftcolr">Bills</td>
<td colspan="3" class="borderleftcolr">Payment</td>
</tr>
<tr>
<td >INO</td>
<td >Entered</td>
<td >QTY</td>
<td >P.No</td>
<td >Entered</td>
<td>Date</td>
<td>MSE No</td>
<td>Entered</td>
<td >QTY</td>
<td>Bill No</td>
<td>Approved</td>
<td>Date</td>
<td>Ident</td>
<td>Approved</td>
<td>Status</td>
</tr>
<tr>
<td rowspan="5">IN1</td>
<td rowspan="5">ABC</td>
<td rowspan="5">100</td>
<td rowspan="4">PO1</td>
<td rowspan="4">ABC</td>
<td rowspan="4">10-11</td>
<td rowspan="3">MSE1</td>
<td rowspan="3">ABC</td>
<td rowspan="3">105</td>
<td  rowspan="2">BillNo1</td>
<td  rowspan="2">pending</td>
<td  rowspan="2">12-11</td>
<td>Id1</td>
<td>yes</td>
<td>yes</td>
</tr>
<tr>
<td>Id1</td>
<td>yes</td>
<td>yes</td>
</tr>
<tr>
<td>BillNo1</td>
<td>pending</td>
<td>12-11</td>
<td>Id1</td>
<td>yes</td>
<td>yes</td>
</tr>
<tr>
<td>MSE1</td>
<td>ABC</td>
<td >105</td>
<td>BillNo1</td>
<td>pending</td>
<td>12-11</td>
<td>Id1</td>
<td>yes</td>
<td>yes</td>
</tr>
<tr>
<td>PO11</td>
<td>ABC</td>
<td>10-11</td>
<td>MSE1</td>
<td>ABC</td>
<td>105</td>
<td>BillNo1</td>
<td>pending</td>
<td>12-11</td>
<td>Id1</td>
<td>yes</td>
<td>yes</td>
</tr>
<tr>
<td colspan="15" class="bordercolr"></td>
</tr>
<tr>
<td>IN1</td>
<td>ABC</td>
<td>100</td>
<td>PO1</td>
<td>ABC</td>
<td>10-11</td>
<td>MSE1</td>
<td>ABC</td>
<td>105</td>
<td>BillNo1</td>
<td>pending</td>
<td>12-11</td>
<td>Id1</td>
<td>yes</td>
<td>yes</td>
</tr>
</table>


</div>
</div>
</div>

</body>
</html>