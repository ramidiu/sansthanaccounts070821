<%@page import="java.math.BigDecimal"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.minorhead" %>
<%@page import="mainClasses.minorheadListing" %>
<%@page import="beans.subhead" %>
<%@page import="mainClasses.subheadListing" %>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Trail Balance REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>


<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;

}
.new-table td{
padding: 2px 0 2px 5px !important;}


</style>

<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
<%@page import="java.util.List"%>
</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>


<body>
<%if(session.getAttribute("superadminId")!=null){ %>
	<!-- main content -->
	<%headofaccountsListing HOA_L=new headofaccountsListing();
	List headaclist=HOA_L.getheadofaccounts();%>
<div class="vendor-page">

		<div>
			<form action="adminPannel.jsp" method="post" id="formLoad" style="padding-left: 70px;padding-top: 30px;">
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td class="border-nn">From Date</td>
<td class="border-nn">To Date</td>
<td class="border-nn"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
</tr>
				<tr>
				<%List HA_Lists=HOA_L.getheadofaccounts();
						//String fromdate="2014-04-01";
						String fromdate="";
						String todate="";
						DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
						Calendar c1 = Calendar.getInstance(); 
						TimeZone tz = TimeZone.getTimeZone("IST");
						c1.getActualMaximum(Calendar.DAY_OF_MONTH);
						int lastday = c1.getActualMaximum(Calendar.DATE);
						c1.set(Calendar.DATE, lastday);  
						todate=(dateFormat2.format(c1.getTime())).toString();
						c1.getActualMinimum(Calendar.DAY_OF_MONTH);
						int firstday = c1.getActualMinimum(Calendar.DATE);
						c1.set(Calendar.DATE, firstday);
						fromdate=(dateFormat2.format(c1.getTime())).toString();
						//todate=new SimpleDateFormat("yyyy-MM-dd").format(c1.getTime()).toString();
						
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}
						String hoaid[]=new String[]{"1"};
						if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
							if(request.getParameter("hoid").equals("ALL")){
								hoaid=new String[]{"1","3","4","5"};
							}else{
							hoaid=new String[]{""+request.getParameter("hoid")};
							}
						}
						%>
				<td class="border-nn"> <input type="text"  name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>" /> </td>
				<td class="border-nn"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
				<input type="hidden" name="page" value="trailBalanceReport_new">
				 </td>
<td colspan="2" class="border-nn"><select name="hoid" required>
<%if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
	<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%>  selected="selected" <%} %>/> <%=HOA.getname() %><%}} %>
	<%-- <option value="ALL" <%if(request.getParameter("hoid")!=null &&request.getParameter("hoid").equals("")){%> selected="selected"<%} %>>ALL</option> --%>
</select></td>
<td class="border-nn"><input type="submit" class="click" name="search" value="Search"></input></td>
</tr></tbody></table> </form>
				<% String finyr[]=null;	
				double credit=0.0;
				double debit=0.0;
				double credittrail=0.0;
				double debittrail=0.0;
				String cashtype="online pending";
				String cashtype1="othercash";
				String cashtype2="offerKind";
				String cashtype3="journalvoucher";
				DecimalFormat bd=new  DecimalFormat("#,###.00");
				productexpensesListing PEXP_L=new productexpensesListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				/*Calendar c1 = Calendar.getInstance(); 
				TimeZone tz = TimeZone.getTimeZone("IST");
				 String hoaid[]=null;
				if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
					hoaid=new String[]{""+request.getParameter("hoid")};
				} else {
				hoaid=new String[]{"1","3","4","5"};
				} */
				String currentDate=(onlydat.format(c1.getTime())).toString();
				String finyrfrm=fromdate+" 00:00:01";
				String finyrto=todate+" 23:59:59";
                 String prevhead="";
                 String preshead="";
				customerpurchasesListing CP_L=new customerpurchasesListing();
				majorheadListing MH_L=new majorheadListing();
				List majhdlist=null;
				minorheadListing MINH_L=new minorheadListing();
				productexpensesListing PRDEXP_L=new productexpensesListing();
				subheadListing SUBH_L=new subheadListing();
				
				%>
				<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
														
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span> <span>
						
					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	
	
	<div class="total-report">

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
					
	<div class="printable" id="tblExport">	


<table width="90%" cellpadding="0" cellspacing="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
<tbody><tr>
						<td colspan="3" align="center" style="font-weight: bold;">SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td>
					</tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Trail Balance Report</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=onlydat.format(dbdat.parse(finyrfrm)) %> TO <%=onlydat.format(dbdat.parse(finyrto)) %> </td></tr>



</tbody></table>
<div style="clear:both"></div>
<table width="90%" border="1" cellspacing="0" cellpadding="0"  style="margin:0 auto;" class="new-table">
  <tbody>
  <tr><th colspan="3" height="28" style="font-weight:bold;">
 <%for(int h=0;h<hoaid.length;h++)
	{
	majhdlist=MH_L.getMajorHeadBasdOnCompany(hoaid[h]);
				if(majhdlist.size()>0)
				{ 
					int j=0;
					for(int i=0;i<majhdlist.size();i++){
					MH=(majorhead)majhdlist.get(i);
						if(h>0){
													if(!MH.gethead_account_id().equals(hoaid[h-1]) && i==0)
													{
														%><span><%=HOA_L.getHeadofAccountName(hoaid[h])%></span><%
													}
									
													} 
													else if(h==0 && j==0)
													{
														j++;
														%><span><%=HOA_L.getHeadofAccountName(hoaid[h])%></span><%
													}%>
													<%}}} %>
  </th></tr>
  <tr>
    <th width="38%" height="29" align="center">Account</th>
    <th width="29%" align="center">Debit</th>
    <th width="33%" align="center">Credit</th>
  </tr>
  
  <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">Debit</td>
  
  </tr>
 							 <%for(int h=0;h<hoaid.length;h++)
								{
								majhdlist=MH_L.getMajorHeadBasdOnCompany(hoaid[h]);
									if(majhdlist.size()>0)
										{ 
											int j=0;
												for(int i=0;i<majhdlist.size();i++){
												MH=(majorhead)majhdlist.get(i);
								%>	
								<%
								//to display only details which have some credit and debit...
								if(!(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]))==0.0 && Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,""))==0.0))
									{ 
								  %>
  
  <tr>
    <%-- <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><a href="adminPannel.jsp?page=minorheadTrailBalanceReport&majrhdid=<%=MH.getmajor_head_id()%>&fromDate=<%=onlydat.format(dbdat.parse(finyrfrm)) %>&toDate=<%=onlydat.format(dbdat.parse(finyrto)) %>"><%=MH.getname() %></a></td> --%>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><%=MH.getname() %></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=bd.format(Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"")))%></td>
    <% debittrail=debittrail+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,""));
		if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]).equals("00")){ %>
	<td align="center" style=" color: #934C1E; font-weight:bold;"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,cashtype,hoaid[h]) ))%></td>
  	<%credittrail=credittrail+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,cashtype,hoaid[h]) ); 
			}else{%>
	<td align="center" style=" color: #934C1E; font-weight:bold;"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,hoaid[h]))) %></td> 
  	<%credittrail=credittrail+Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,hoaid[h])); }%>
  </tr>
 
  <%List minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id()); 
  String majrhdid=MH.getmajor_head_id();
  if(minhdlist.size()>0)
								{ 
								int k=0;
								for(int x=0;x<minhdlist.size();x++)
								{
									
									MNH=(minorhead)minhdlist.get(x);
								%>	
						    								
								<%
								if(!(Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",finyrfrm,finyrto,""))==0.0 && Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))==0.0))
								{ 
								  %>
  
  <tr>
    <%-- <td height="28" style="color:#f00;"><strong><a href="adminPannel.jsp?page=subheadTrailBalance&subhid=<%=MNH.getminorhead_id() %>&fromDate=<%=onlydat.format(dbdat.parse(finyrfrm)) %>&toDate=<%=onlydat.format(dbdat.parse(finyrto)) %>" style="color:#f00;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></strong></td> --%>
    <td height="28" style="color:#f00;"><strong><%=MNH.getname() %><%=MNH.getminorhead_id() %></strong></td>
    <td align="center" style="color:#f00;"><%=PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",finyrfrm,finyrto,"") %></td>
    <%debit=debit+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",finyrfrm,finyrto,""));
		if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)).equals("00")){%>
	<%-- <td align="center" style="color:#f00;"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))))%></td> --%>
 	<td align="center" style="color:#f00;"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))))%></td>
 	<% credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)));
		}else{ %>
	<%-- <td align="center" style="color:#f00;"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))) %></td> --%>
	<td align="center" style="color:#f00;"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))) %></td>
  	<%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)));}%>	
  </tr>
 
 <%List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id()); 
 String subhid=MNH.getminorhead_id();
 if(subhdlist.size()>0)
								{ 
									int y=0;
									for(int z=0;z<subhdlist.size();z++)
									{
										SUBH=(subhead)subhdlist.get(z);
										if(SUBH.getsub_head_id().equals("20201"))
											cashtype="creditsale";
										else if(SUBH.getsub_head_id().equals("20206"))
											cashtype="creditsale";
										
								%>	
						    
								
								<%
								/* if(!(Double.parseDouble(PRDEXP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,""))==0.0 && Double.parseDouble(CP_L.getLedgerSumOfSubhead(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))==0.0)) */
								if(!(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))==0.0 || Double.parseDouble(CP_L.getLedgerSumOfSubhead(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))==0.0))
								{ 
								  %>	
  
  <tr>
  	<%if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %>
	<%-- <td height="28"><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&category=trailbalance&subhead=<%=SUBH.getsub_head_id()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000;"><%=SUBH.getname() %></a></td> --%>
	<td height="28"><%=SUBH.getname() %></td>
    <%} else{ %>
    <%-- <td height="28"> <a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&category=trailbalance&subhead=<%=SUBH.getsub_head_id()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000;"><%=SUBH.getname() %></a></td> --%>
    <td height="28"><%=SUBH.getname() %></td>
     <%} %>
    <td><%=bd.format(Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",finyrfrm,finyrto,""))) %></td>
    <%debit=debit+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",finyrfrm,finyrto,""));
		if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %>
	<%-- <td><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))) %></td> --%>
	<td><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))) %></td>
    <%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
		}else{ %>
	<td><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))) %></td>
 	<%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",finyrfrm,finyrto,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));}%>
  </tr>
  <%}}}}}}}}}} %> 
  
  <%-- <%System.out.println("credit==>"+credit);
  System.out.println("debit==>"+debit);
  System.out.println("credittrail==>"+credittrail);
  System.out.println("debittrail==>"+debittrail);%> --%>
  <tr>
    <td align="center" style=" color: #934C1E; font-weight:bold;" height="28">TOTAL AMOUNT OF EXPENS </td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">Rs.<%=bd.format(debittrail) %></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
  </tr>
  
  <%-- <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Balance Left</td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=bd.format(credittrail-debittrail) %></td>
    <td align="center" style="font-weight:bold; color: #934C1E;">&nbsp;</td>
  </tr> --%>
  
 <%--  <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Excess over Income</td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=bd.format(credittrail-debittrail) %></td>
    <td align="center" style="font-weight:bold; color: #934C1E;">&nbsp;</td>
  </tr> --%>
  
  <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Excess of Income </td>
    <%if(credittrail<debittrail){	%>
    <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=bd.format(debittrail-credittrail)%></td>
    <%	
} else{	%>
<td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=bd.format(credittrail-debittrail)%></td>
<td align="center" style="font-weight:bold; color: #934C1E;">-</td>
<%} %>
  </tr>
  
  <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
    <%-- <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=bd.format(debittrail) %></td> --%>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=bd.format(credittrail) %></td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=bd.format(credittrail) %></td>
  </tr>
  
   <!-- <tr>
    <td height="28">Total(Rupees)</td>
    <td align="center">0.00</td>
    <td align="center">0.00</td>
  </tr> -->
  
  
 <%--    <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">EXPENDITURE</td>
  	</tr>
    
    <% if(expendmajhdlist.size()>0){ 
								for(int i=0;i<expendmajhdlist.size();i++){
									MH=(majorhead)expendmajhdlist.get(i);
									if(!PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"").equals("00")){
									
								%>
     <tr>
    <td height="28" style=" color: #934C1E; font-weight:bold;"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getname() %></a></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"") %></td>
   <%debitexpens=debitexpens+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,""));%>
     <%if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid).equals("00")){ %>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid)) %></td>
    <%creditexpens=creditexpens+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); } else{ %>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid) %></td>
 <%creditexpens=creditexpens+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); } %>
  </tr>
  
  <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									String amountexp = PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,"");
											if(!("00".equals(""+amountexp))){
								%>
    <tr>
    <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Income and Expenditure" style="color:red;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></strong></td>
    <td align="center" style="color:#C30;"><%=PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,"") %></td>
    <%debit=debit+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,""));
                if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ %>
    <td align="center" style="color:#C30;"><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) %></td>
     <%} else{ %>
     <td align="center" style="color:#C30;"><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) %></td>
     <%} %>
  </tr>
  <%
				List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
         if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										 cashtype1="othercash";
										 cashtype2="offerKind";
										cashtype3="journalvoucher";
										String amountexp2= PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,"");
										if(!("00".equals(""+amountexp2))){
								%>	
  
  
  
   <tr>
   <%if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td>
     <%} else{ %>
     <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td>
   <%} %>
    <td><%=PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,"") %></td>
    <%debit=debit+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,""));
				if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <td><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) %></td>
     <%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));} else{ %>
     <td><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) %></td>
  <%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));} %>
  </tr>
  
 <%}}}}}}}}} %>
  
   
  
   <!-- <tr>
    <td height="28">TOTAL GIA FOR STAFF </td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td height="28"></td>
    <td align="center">0.00</td>
    <td align="center">0.00</td>
  </tr> -->
  <%System.out.println("credit==>"+credit);
System.out.println("debit===>"+debit);
System.out.println("creditincome==>"+creditincome);
System.out.println("creditexpens==>"+creditexpens);
System.out.println("debitincome==>"+debitincome);
System.out.println("debitexpens==>"+debitexpens);
 %>
  
   <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Excess of Income over Expenditures</td>
    <%if(creditincome<debitexpens){	%>
    <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
    <td align="center" style="font-weight:bold; color: #934C1E;"><%=(debitexpens-creditincome)%></td>
    <%	creditincome=debitexpens;
} else{	%>
<td align="center" style="font-weight:bold; color: #934C1E;"><%=(creditincome-debitexpens)%></td>
<%debitexpens=creditincome;%>
<%} %>
  </tr>
  
   <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=creditincome %></td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=debitexpens %></td>
  </tr>
  <!--  <tr>
    <td height="28" colspan="3"></td>
  </tr> -->
  
  <!-- <tr>
    <td height="28"><strong>Total</strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr> -->

 --%>
  
</tbody></table>
</div>
  </div>
			</div>
				
			</div>
			</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>