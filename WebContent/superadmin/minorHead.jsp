<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>

<script type="text/javascript">

function validate(){
	$('#headIdErr').hide();
	$('#MajorHeadErr').hide();
	$('#minorheadErr').hide();
	$('#headNameErr').hide();

	if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	}
	if($('#major_head_id').val().trim()==""){
		$('#MajorHeadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	/* if($('#minorheadErr').val().trim()==""){
		$('#sub_head_id').show();
		$('#minorheadErr').focus();
		return false;
	} */
	if($('#name').val().trim()==""){
		$('#headNameErr').show();
		$('#name').focus();
		return false;
	}
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("superadminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="SUB" class="beans.subhead"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"><%if(request.getParameter("id")!=null) {%>Edit minorhead<%}else{%>Create a new minor heads <%} %></div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
subheadListing SUBH_L=new subheadListing();
if(request.getParameter("id")!=null) {
List SUBHD_details=SUBH_L.getMsubhead(request.getParameter("id"));
SUB=(beans.subhead)SUBHD_details.get(0);
%>
<form name="tablets_Update" method="post" action="subhead_Update.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="sub_head_id" id="sub_head_id" value="<%=SUB.getsub_head_id()%>"/>
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td><div class="warning" id="MajorHeadErr" style="display: none;">Please Provide  "major head".</div>Major Head*</td>
<td width="35%">
<div class="warning" id="minorheadErr" style="display: none;">Please Provide  "Minor Head Number".</div>
Minor Head No*</td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Minor Head Name".</div>
Minor Head Name*</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id" onChange="combochange('head_account_id','major_head_id','getMajorHeads.jsp')">
<option value="<%=SUB.gethead_account_id()%>"><%=HOA_L.getHeadofAccountName(SUB.gethead_account_id()) %></option>
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}}%>
</select></td>
<td><select name="major_head_id" id="major_head_id">
<option value="<%=SUB.getmajor_head_id()%>"><%=MajorHead_list.getmajorheadName(SUB.getmajor_head_id()) %></option>

</select></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value="" disabled="disabled"/></td>
<td><input type="text" name="name" id="name" value="<%=SUB.getname()%>" /></td>
</tr>


<tr>
<td colspan="3">
<textarea name="description" id="description" placeholder="Enter description"><%=SUB.getdescription()%></textarea></td>
</tr>
<tr> 
<td colspan="2"></td>
<td align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<% } else{%>
<form name="tablets_Update" method="post" action="subhead_Insert.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account*</td>
<td><div class="warning" id="MajorHeadErr" style="display: none;">Please select  "Major head".</div>Major head*</td>
<td width="35%">
<div class="warning" id="headIdErr" style="display: none;">Please Provide  "Minor Head Number".</div>
Minor Head No*</td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Minor Head Name".</div>
Minor Head Name*</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id" onChange="combochange('head_account_id','major_head_id','getMajorHeads.jsp')">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><select name="major_head_id" id="major_head_id">
<option value="">Select major head</option>
</select></td>
<td><input type="text" name="sub_head_id" id="sub_head_id" value="" /></td>
<td><input type="text" name="name" id="name" value="" /></td>
</tr>


<tr>
<td colspan="3">
<textarea name="description" id="description" placeholder="Enter description"></textarea></td>
</tr>
<tr> 
<td colspan="2"></td>
<td align="right"><input type="submit" value="Create" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<%} %>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%
List<subhead> Sub_list=SUBH_L.getsubhead();
if(Sub_list.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Minor Head Name</td>
<td class="bg" width="23%">Description </td>
<td class="bg" width="23%">Major Head </td>
<td class="bg" width="23%">Head of account</td>
<td class="bg" width="20%">Edit</td>
<td class="bg" width="10%">Delete</td>
</tr>
<%for(int i=0; i < Sub_list.size(); i++ ){
	SUB=(beans.subhead)Sub_list.get(i); %>
<tr>
<td><%=SUB.getsub_head_id()%></td>
<td><span><%=SUB.getname()%></span></td>
<td><%=SUB.getdescription()%></td>
<td><%=MajorHead_list.getmajorheadName(SUB.getmajor_head_id())%></td>
<td><%=HOA_L.getHeadofAccountName(SUB.gethead_account_id())%></td>
<td><a href="adminPannel.jsp?page=minorHead&id=<%=SUB.getsub_head_id()%>">Edit</a></td>
<td><a href="subhead_Delete.jsp?Id=<%=SUB.getsub_head_id()%>">Delete</a></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no minor heads</h1></div>
<%}%>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
