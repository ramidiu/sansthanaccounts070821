<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.godwanstock"%>
<%@page import="beans.indentapprovals"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.util.List"%>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List"%>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css"
	href="../main/jquery.autocomplete.css" />
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript"
	src="../js/modal-window.js"></script>

<script type="text/javascript">
	function numbersonly(myfield, e, dec) {
		var key;
		var keychar;

		if (window.event)
			key = window.event.keyCode;
		else if (e)
			key = e.which;
		else
			return true;
		keychar = String.fromCharCode(key);

		// control keys
		if ((key == null) || (key == 0) || (key == 8) || (key == 9)
				|| (key == 13) || (key == 27))
			return true;

		// numbers
		else if ((("0123456789-").indexOf(keychar) > -1))
			return true;

		// decimal point jump
		else if (dec && (keychar == ".")) {
			myfield.form.elements[dec].focus();
			return false;
		} else
			return false;
	}
</script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />
<script>
	function deleteProduct(id) {
		var retVal = confirm("Are you sure you want to delete this product ?");
		if (retVal == true) {
			$.ajax({
				type : 'post',
				url : 'productDelete.jsp',
				data : {
					indentid : id
				},
				success : function(response) {
					$('#refresh').load(window.location + '' + ' #refresh');
				}
			});
		}
	}
	function vendorsearch(j) {
		var data1 = $('#vendorId' + j).val();
		var headID = $('#headAccountId').val();
		$.post('searchVendor.jsp', {
			q : data1,
			b : headID
		}, function(data) {
			var response = data.trim().split("\n");
			var doctorNames = new Array();
			var doctorIds = new Array();
			for ( var i = 0; i < response.length; i++) {
				var d = response[i].split(",");
				doctorIds.push(d[0]);
				doctorNames.push(d[0] + " " + d[1]);
			}
			var availableTags = data.trim().split("\n");
			var availableIds = data.trim().split("\n");
			availableTags = doctorNames;
			$('#vendorId' + j).autocomplete({
				source : availableTags
			});
		});
	}
	function blockPage() {
		$.blockUI({
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff'
			}
		});
	}
</script>
<jsp:useBean id="INDT" class="beans.indent" />
<jsp:useBean id="BKT" class="beans.banktransactions" />
<jsp:useBean id="VND" class="beans.vendors" />
<jsp:useBean id="GDS" class="beans.godwanstock" />
<jsp:useBean id="INDAPPROV" class="beans.indentapprovals" />
<%
	String INVId = request.getParameter("invid");
	String statuss = request.getParameter("statuss");
	String Producttype = "";
	mainClasses.indentListing INDT_L = new indentListing();
	godwanstockListing GDS_L = new godwanstockListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	mainClasses.employeesListing EMP_L = new employeesListing();
	productsListing PRD_L = new productsListing();
	vendorsListing VND_L = new mainClasses.vendorsListing();
	List INDT_List = INDT_L.getMindentsByIndentinvoice_id(INVId);
	List BILAP = GDS_L.getgodwanstockByinvoiceId1(INVId);
	if (BILAP.size() > 0) {
		for (int i = 0; i < 1; i++) {
			GDS = (godwanstock) BILAP.get(0);
		}
	}
	if (BILAP.size() > 0) {
		GDS = (godwanstock) BILAP.get(0);

		List VNDT_LIST = VND_L.getMvendors(GDS.getvendorId());
		if (VNDT_LIST.size() > 0) {
			VND = (vendors) VNDT_LIST.get(0);
		}
		if (INDT_List.size() > 0) {
			INDT = (indent) INDT_List.get(0);
		}
		Producttype = GDS.getdepartment();
%>
<div class="vendor-page">
	<div class="vendor-box">
		<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
			<tr>
				<td colspan="1" align="center"
					style="font-weight: bold; color: red;" class="bg-new">SHRI
					SHIRIDI SAI BABA SANSTHAN TRUST</td>
			</tr>
			<tr>
				<td colspan="1" align="center"
					style="font-weight: bold; font-size: 12px;" class="bg-new">(Regd.No.646/92)</td>
			</tr>
			<tr>
				<td colspan="1" align="center"
					style="font-weight: bold; font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td>
			</tr>
			<tr>
				<td colspan="1" align="center"
					style="font-weight: bold; font-size: 16px;" class="bg-new"><br />Bill
					<%
					if (statuss.equals("billdisapproved")) {
				%>Disapprovals<%
					} else {
				%>Approvals<%
					}
				%>
					List</td>
			</tr>
		</table>
		<div class="name-title"><%=GDS.getvendorId()%>
			<span>(<%=HOA_CL.getHeadofAccountName(GDS.getdepartment())%>)
			</span>
		</div>
		<div style="clear: both;">
			<div class="details-list">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td colspan="1"><span>Vendor Name :</span></td>
						<td>
							<%
								if (VND.getfirstName() != null) {
										out.println(VND.gettitle());
										out.println(VND.getfirstName());
										out.println(VND.getlastName());
									} else {
										out.println("Not Available");
									}
							%>
						</td>
					</tr>
					<tr>
						<td colspan="1"><span>Vendor Address :</span></td>
						<td>
							<%
								if (VND.getaddress() != null) {
										out.println(VND.getaddress());
							%>,<%
								out.println(VND.getcity());
							%>,<%
								out.println(VND.getpincode());
									} else {
										out.println("Not Available");
									}
							%>
						</td>
					</tr>

					<tr>
						<td colspan="1"><span>Vendor Phone No :</span></td>
						<td>
							<%
								if (VND.getphone() != null && !VND.getphone().equals("")) {
										out.println(VND.getphone());
									} else {
										out.println("Not Available");
									}
									;
							%>
						</td>
					</tr>
				</table>

			</div>
			<%-- <div><center><%if(statuss.equals("billdisapproved")){ %>Disapproved<%}else{ %>Approved<%} %> By:<br><br>
<%
	if(request.getParameter("approvedBy")!=null)
	{
		String approvedBy=request.getParameter("approvedBy");
		out.println(approvedBy);
	}
	else
	{
		out.println("Not-Approved");
	}
%>
</center></div> --%>
			<div class="details-amount">
				<ul>
					<li class="colr1" style="font-size: 15px;">
						<%
							if (statuss.equals("billdisapproved")) {
						%>Disapproved<%
							} else {
						%>Approved<%
							}
						%>
						By:<br> <%
 	if (request.getParameter("approvedBy").equals("")
 				|| request.getParameter("approvedBy") == null) {
 			out.println("Not-Approved");
 		} else {
 			String approvedBy = request.getParameter("approvedBy");
 			out.println(approvedBy);
 		}
 %>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="vendor-list">
		<div class="trans" style="text-align: center;">Transactions</div>
		<%
			if (GDS.getExtra9() != null && !GDS.getExtra9().equals("")) {
		%>
		<div style="font-weight: bold; color: red; cursor: pointer;"
			onclick="openMyModal('Trackingno_Details.jsp?TId=<%=GDS.getExtra9()%>')">
			Track Transaction : <span><%=GDS.getExtra9()%></span>
		</div>
		<%
			}
		%>
		<div class="clear"></div>
		<div class="icons">
			<div style="margin-bottom: 20px; float: left; cursor: pointer;">
				<img src="../images/back-button (1).png" width="100" height="50"
					onclick="goBack()" title="print" />
			</div>
			<span><img src="../images/printer.png"
				style="margin: 0 20px 0 0" title="print" /></span> <span><img
				src="../images/excel.png" style="margin: 0 20px 0 0"
				title="export to excel" /></span>
		</div>

		<div class="clear"></div>
		<div class="list-details" style="margin: 10px 10px 0 0">
			<div id="refresh">
				<form action="billsApprovalInsert.jsp" method="post"
					onsubmit="blockPage();">
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="bg" width="3%"></td>
							<td class="bg" width="20%">Vendor Name</td>
							<td class="bg" width="8%">Product CODE/NAME</td>
							<td class="bg" width="20%">Amount</td>
							<td class="bg" width="20%">Qnty</td>
							<td class="bg" width="20%">Price</td>
							<td class="bg" width="9%">GST(Rs)</td>
							<!-- <td class="bg" width="20%">Balance Quantity</td> -->
						</tr>

						<%
							banktransactionsListing BAK_L = new mainClasses.banktransactionsListing();
								indentapprovalsListing APPL = new indentapprovalsListing();
								subheadListing SUBL = new subheadListing();
								employeesListing EMP = new employeesListing();
								double totAmt = 0.00;
								double otherCharges = 0.00;
								DecimalFormat DF = new DecimalFormat("#.00");
								List INd_APL = APPL.getIndentApprovedByAdmin(INVId, session.getAttribute("superadminId").toString());
								if (BILAP.size() > 0) {
									for (int i = 0; i < BILAP.size(); i++) {
										GDS = (godwanstock) BILAP.get(i);
						%>
						<tr>
							<td style="cursor: pointer;"><input type="hidden"
								name="count" id="check<%=i%>" value="<%=i%>" /> <input
								type="hidden" name="indentId<%=i%>" id="indentId"
								value="<%=GDS.getextra2()%>"></td>
							<td><%=GDS.getvendorId()%> <%=VND_L.getMvendorsAgenciesName(GDS
								.getvendorId())%></td>
							<%-- <td><input type="text" name="vendorId<%=i%>" id="vendorId<%=i%>"  value="<%=EXP.getvendorId()%> <%=VND_L.getMvendorsAgenciesName(EXP.getvendorId()) %>" onkeyup="vendorsearch('<%=i%>')"/></td> --%>
							<td><%=GDS.getproductId()%> - <%
								if (!PRD_L.getProductsNameByCat(GDS.getproductId(),GDS.getdepartment()).equals("")) {
							%><%=PRD_L.getProductsNameByCat(
									GDS.getproductId(), GDS.getdepartment())%>
								<%
									} else {
								%><%=PRD_L.getProductsNameByCat1(
									GDS.getproductId(), GDS.getdepartment())%>
								<%
									}
								%></td>
							<%
								totAmt = totAmt + Double.parseDouble(GDS.getvat1());
											if (GDS.getExtra8() != null
													&& !GDS.getExtra8().equals(""))
												otherCharges = Double.parseDouble(GDS.getExtra8());
							%>
							<td align="right"><%=DF.format(Double.parseDouble(GDS.getvat1()))%></td>
							<td align="center"><%=GDS.getquantity()%></td>
							<td><%=DF.format(Double.parseDouble(GDS
								.getpurchaseRate()))%></td>
							<td><%=DF.format(Double.parseDouble(GDS.getExtra18())
								+ Double.parseDouble(GDS.getExtra19())
								+ Double.parseDouble(GDS.getExtra20()))%></td>
							<%-- <td><input type="text" name="quantity<%=i%>" id="quantity<%=i%>" placeholder="Enter quantity" onblur="calculate('<%=i %>')"  onKeyPress="return numbersonly(this, event,true);" value="<%=EXP.getamount() %>" style="width:100px;"></input></td> --%>
							<%-- <td><%=PRD_L.getProductsStockByCat(INDT.getproduct_id(),Producttype) %></td> --%>
						</tr>
						<%
							}
						%>
						<tr>
							<td class="bg" colspan="3" align="right">TOTAL
								AMOUNT(+OtherCharges)</td>
							<td class="bg" align="right"><%=DF.format(totAmt + otherCharges)%></td>
							<td class="bg" colspan="3"></td>
						</tr>
						<%
							}
						%>
						<%
							String approvOrDisapprov = "";
								if (INd_APL.size() > 0) {
						%>
						<%
							INDAPPROV = (indentapprovals) INd_APL.get(0);
									if (INDAPPROV.getadmin_status().equals("billdisapproved")) {
										approvOrDisapprov = "disapproved";
									} else {
										approvOrDisapprov = "approved";
									}
						%>
						<!-- <tr><td colspan="4" style="color: orange;font-weight: bold;" align="center">Note : This Bill payment approved by you!</td></tr> -->
						<tr>
							<td colspan="4" style="color: orange; font-weight: bold;"
								align="center">Note : This Bill payment <%=approvOrDisapprov%>
								by you!
							</td>
						</tr>
						<%
							} // if
							else {
					if (!session.getAttribute("role").equals("CM") && !session.getAttribute("role").equals("GS") && !session.getAttribute("role").equals("FS")) {
						%>
						<tr>
							<td colspan="2"><input type="hidden" name="indentInvoiceId"
								value="<%=INVId%>"> <select name="status">
									<option value="approved">Approve</option>
									<option value="billdisapproved">Dis-Approve</option>
							</select></td>
							<td><textarea name="feedback" id="feedback"
									placeHolder="Write your remarks"></textarea></td>
							<!-- <td><input type="submit" name="submit" value="APPROVE"></td> -->
							<td><input type="submit" name="submit" value="SUBMIT" class="click"></td>
						</tr>
						<%
	
						DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						DateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						System.out.println("sdf1//"+sdf1+"//"+sdf2);
						long diff = 0l,hrs = 0;
						Calendar cal = Calendar.getInstance();
						cal.setTimeZone(TimeZone.getTimeZone("IST"));
						Date todate = sdf2.parse(sdf1.format(cal.getTime()));
						Date fromdate = sdf2.parse(GDS.getDate());
						System.out.println("fromdate//"+fromdate);
						System.out.println("todate//"+todate);
						diff = todate.getTime() - fromdate.getTime();
						hrs = diff / (60 * 60 * 1000);	
						if(hrs<48){
							System.out.println("current date "+hrs);	
						}else{
							System.out.println("current date todate "+hrs);
						}
						
						System.out.println("bill hours--"+hrs);
						
					} // if
	// added by gowri shankar
	else	{
	DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	long diff = 0l,hrs = 0;
	Calendar cal = Calendar.getInstance();
	cal.setTimeZone(TimeZone.getTimeZone("IST"));
	Date todate = sdf2.parse(sdf1.format(cal.getTime()));
	Date fromdate = sdf2.parse(GDS.getDate());
	System.out.println("fromdate//"+fromdate);
	System.out.println("todate//"+todate);
	diff = todate.getTime() - fromdate.getTime();
	hrs = diff / (60 * 60 * 1000);	
	System.out.println();
	System.out.println("bill hours--"+hrs);
	if (hrs >= 72 )	{%>
			<!-- fs or cm or gs -->				
						<tr>
							<td colspan="2"><input type="hidden" name="indentInvoiceId"
								value="<%=INVId%>"> <select name="status">
									<option value="approved">Approve</option>
									<option value="billdisapproved">Dis-Approve</option>
							</select></td>
							<td><textarea name="feedback" id="feedback"
									placeHolder="Write your remarks"></textarea></td>
							<!-- <td><input type="submit" name="submit" value="APPROVE"></td> -->
							<td><input type="submit" name="submit" value="SUBMIT" class="click"></td>
						</tr>
	<%}
	} // else
	// added by gowri shankar
 } // else%>
					</table>
				</form>
			</div>
		</div>
	</div>
	<%
		} else {
			response.sendRedirect("adminPannel.jsp?page=banks");
		}
	%>