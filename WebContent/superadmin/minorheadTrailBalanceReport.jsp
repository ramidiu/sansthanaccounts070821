<%@page import="java.math.BigDecimal"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Trail Balance REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
    <script type="text/javascript">
    function formsubmit(){
    	if($( "#major_head_id" ).val()!=""){    		
        	document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReport&majrhdid="+$( '#major_head_id' ).val();
        	document.getElementById("departmentsearch").submit();
    		    	} else if($( "#minor_head_id" ).val()!=""){
    		    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReport&subhid="+$( '#minor_head_id' ).val();
    		        	document.getElementById("departmentsearch").submit();
    		    	}  else{    	
    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReport";
    	document.getElementById("departmentsearch").submit();
    	}
    }</script>
<%@page import="java.util.List"%>
</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<%headofaccountsListing HOA_L=new headofaccountsListing();
	List headaclist=HOA_L.getheadofaccounts();
			String majrhdid="";
				if(request.getParameter("majrhdid")!=null){
					majrhdid=request.getParameter("majrhdid");
				}%>
	<div>
			<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
							<form action="adminPannel.jsp" method="post" id="formLoad">
				<div class="search-list">
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li>
						<input type="hidden" name="page" value="minorheadTrailBalanceReport"></input>
							<input type="hidden" name="majrhdid" value="<%=majrhdid%>"></input>
						<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					
					</ul>
				</div></form>
				<%
				productexpensesListing PRDEXP_L=new productexpensesListing();
				String finyr[]=null;
				double credit=0.0;
				DecimalFormat bd=new  DecimalFormat("#,###.00");
				double debit=0.0;
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
						String finyrfrm=fromdate+" 00:00:01";
				String finyrto=todate+" 23:59:59";
				majorheadListing MJRH_L=new majorheadListing();
			minorheadListing MINH_L=new minorheadListing();
			customerpurchasesListing CP_L=new customerpurchasesListing();
				List minhdlist=MINH_L.getMinorHeadBasdOnCompany(majrhdid);
				%>
				<div class="icons">
				<div style="margin-bottom: 20px;float: left;cursor: pointer;"><img src="../images/back-button (1).png" width="100" height="50" onclick="goBack()" title="print"/></div>
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)) %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Trail Balance Report </td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date  <%=onlydat.format(dbdat.parse(finyrfrm)) %> TO <%=onlydat.format(dbdat.parse(finyrto)) %> </td></tr>
						<tr>
						<td colspan="7">
						<table width="100%" cellpadding="0" cellspacing="0" border="1">
						<tr>
							<td class="bg" width="10%" style="font-weight: bold;">Major Head</td>
							<td class="bg" width="40%" style="font-weight: bold;" colspan="2">Minor Head</td>
							<td class="bg" width="25%" style="font-weight: bold;" align="right" colspan="2">DEBIT</td>
							<td class="bg" width="25%" style="font-weight: bold;" align="right" colspan="2">CREDIT</td>
						</tr>
								<% if(minhdlist.size()>0){ 
								for(int i=0;i<minhdlist.size();i++){
									MNH=(minorhead)minhdlist.get(i);
								%>	
						    <tr style="position: relative;">
								<td style="font-weight: bold;"><%
if(i==0) {
									%><span><%=MJRH_L.getmajorheadName(majrhdid)%></span><%
								}%>
</td>
									<td style="font-weight: bold;" colspan="2"><a href="adminPannel.jsp?page=subheadTrailBalance&subhid=<%=MNH.getminorhead_id() %>&fromDate=<%=onlydat.format(dbdat.parse(finyrfrm)) %>&toDate=<%=onlydat.format(dbdat.parse(finyrto)) %>"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></td>
								<td style="font-weight: bold;text-align: right;" colspan="2"><%=PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",finyrfrm,finyrto,"") %></td>
								<%debit=debit+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",finyrfrm,finyrto,""));
								if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)).equals("00")){ %>
								<td style="font-weight: bold;text-align: right;" colspan="2"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))))%>
								</td>
								<% credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)));
								} else{ %>
								<td style="font-weight: bold;text-align: right;" colspan="2"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))) %></td>
								<%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",finyrfrm,finyrto,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)));} %>
							
						</tr>
				<% }} %>
				
					   </table>
						</td>
						</tr>
							<tr style="border: 1px solid #000;">
						<td colspan="3" align="right" style="font-weight: bold;">Total  </td>
					
							<td style="font-weight: bold;text-align: right;" colspan="2" >&#8377;<%=bd.format(debit)%></td>
							
						<td style="font-weight: bold;text-align: right;" colspan="2" > &#8377;<%=bd.format(credit) %> </td>
					
						</tr>
					</table>
			</div>
				
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>