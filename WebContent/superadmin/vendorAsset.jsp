<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.List"%>
    <%@page import="beans.godwanstock"%>
    <%@page import="mainClasses.ReportesListClass"%>
    <%@page import="java.text.DecimalFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>vendor Asset</title>
<style>
 table td.bg {
    background: #e3eaf3;
    font-size: 13px;
    color: #7490ac;
   padding: 16px 0 22px 13px;
    border-right: 1px solid #c0d0e4;
    border-bottom: 1px solid #c0d0e4;
}
 table td {
    font-size: 13px;
    padding: 7px 0 7px 5px;
}
</style> 
</head> 
<body>

<%
DecimalFormat df=new DecimalFormat("#,###.00");
String fromdate=request.getParameter("fromdate")+" 00:00:00";
String todate =request.getParameter("todate")+" 23:59:59";
String hoa=request.getParameter("hoa");
System.out.println(fromdate);
System.out.println(todate);
System.out.println(hoa);
double totamt=0.0d;
String[] time=null;
ReportesListClass reportesListClass = new ReportesListClass();
List<godwanstock> vendorPendingBalances = reportesListClass.getVendorPendingAmountOld(fromdate,todate, "", hoa);
List<godwanstock> openingVendorPendingBalances = reportesListClass.getVendorSuccessOpeningEntrysAmount(fromdate, todate, "",hoa);
%>

<table rules="rows" class="table table-bordered"width="100%" cellpadding="0" cellspacing="0" class="yourID" style="width: 65%;position: relative;left: 176px;top: 100px;">
<tbody>
<tr>
<td class="bg" >DATE</td>
<td class="bg" >VENDOR ID</td>
<td class="bg" >VENDOR NAME</td>
<td class="bg" >PRODUCTID</td>
<td class="bg" >MSE ID</td>
<td class="bg" >EXPENSE ID</td> 
<td class="bg" >AMOUNT</td>
</tr>
<%
for(int i=0;i<vendorPendingBalances.size();i++)	{ 
	godwanstock gs = new godwanstock();
	gs=vendorPendingBalances.get(i);
	totamt+=Double.parseDouble(gs.getExtra16());
	time=gs.getdate().split(" ");
%>
<tr>
<td align="center"><%=time[0]%></td>
<td  align="center"><%=gs.getvendorId() %></td>
<td align="center"><%=gs.getMRNo()%></td>
<td align="center"><%=gs.getproductId()%></td>
<td align="center"><%=gs.getextra1()%></td>
<td align="center"><%=gs.getExtra21()%></td>
<td align="center">&#8377;<%=df.format(Double.parseDouble(gs.getExtra16()))%></td>
</tr>
		
<%}%>
<tr>
<td align="center" class="bg" style="font-weight:bold" colspan="6">TOTAL AMOUNT</td><td align="center" class="bg" style="font-weight:bold"><%=df.format(totamt)%></td><tr>
</tbody>
</table>
<br><br>
<table rules="rows" class="table table-bordered"width="100%" cellpadding="0" cellspacing="0" class="yourID" style="width: 65%;position: relative;left: 176px;top: 100px;">
<tbody>
<tr>
<td class="bg" >DATE</td>
<td class="bg" >VENDOR ID</td>
<td class="bg" >VENDOR NAME</td>
<td class="bg" >PRODUCTID</td>
<td class="bg" >MSE ID</td>
<td class="bg" >EXPENSE ID</td> 
<td class="bg" >AMOUNT</td>
</tr>
<%
totamt=0.0;
for(int i=0;i<openingVendorPendingBalances.size();i++)	{ 
	godwanstock gs = openingVendorPendingBalances.get(i);
	totamt+=Double.parseDouble(gs.getExtra16());
	time=gs.getdate().split(" ");
%>
<tr>
<td align="center"><%=time[0]%></td>
<td  align="center"><%=gs.getvendorId() %></td>
<td align="center"><%=gs.getMRNo()%></td>
<td align="center"><%=gs.getproductId()%></td>
<td align="center"><%=gs.getextra1()%></td>
<td align="center"><%=gs.getExtra21()%></td>
<td align="center">&#8377;<%=df.format(Double.parseDouble(gs.getExtra16()))%></td>
</tr>
		
<%}%>
<tr>
<td align="center" class="bg" style="font-weight:bold" colspan="6">TOTAL AMOUNT</td><td align="center" class="bg" style="font-weight:bold"><%=df.format(totamt)%></td><tr>
</tbody>
</table>
</body>
</html>