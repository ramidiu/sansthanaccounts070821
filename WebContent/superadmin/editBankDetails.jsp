<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.util.List"   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Bank Details</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

function validate(){
	$('#headOfAccountError').hide();
	$('#bankNameError').hide();
	$('#bankBranchError').hide();
	$('#accountHolderNameError').hide();
	$('#accountNumberError').hide();
	
	if($('#headAccountId').val().trim()==""){
		$('#headOfAccountError').show();
		$('#headAccountId').focus();
		return false;
	}
	
	if($('#bank_name').val().trim()==""){
		$('#bankNameError').show();
		$('#bank_name').focus();
		return false;
	}
	
	if($('#bank_branch').val().trim()==""){
		$('#bankBranchError').show();
		$('#bank_branch').focus();
		return false;
	}
	
	if($('#account_holder_name').val().trim()==""){
		$('#accountHolderNameError').show();
		$('#account_holder_name').focus();
		return false;
	}
	
	if($('#account_number').val().trim()==""){
		$('#accountNumberError').show();
		$('#account_number').focus();
		return false;
	}
		
}
</script>


</head>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>	
<body>
<%String bankId=request.getParameter("id");
mainClasses.bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
List BNK_List=BNK_CL.getMbankdetails(bankId); 
if(BNK_List.size()>0){
		BNK=(beans.bankdetails)BNK_List.get(0);%>
<div class="vender-details">
<form method="POST" action="bankdetails_Update.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Bank Information</span></td>
</tr>
<tr>
<td width="50%">
<table width="100%" cellpadding="0" cellspacing="0">
<!-- <tr>
<td colspan="3">
<div class="warning" id="vendorIdError" style="display: none;">"Vendor Id" Required.</div>
Vendor Id*</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="vendorId" id="vendorId"/></td>
</tr> -->

 <tr>
<td colspan="3">
<div class="warning" id="headOfAccountError" style="display: none;">please Select "Head Of Account".</div>
Head Of Account*</td>
</tr>
<tr>
<td colspan="3">
<input type="hidden" name="bank_id" value="<%=BNK.getbank_id()%>"/>
<select name="headAccountId" id="headAccountId" style="width:400px;">
<option value="">--select--</option>
<%mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();
for(int i=0; i < HOA_List.size(); i++ ){
	HOA=(beans.headofaccounts)HOA_List.get(i);%>
	<option value="<%=HOA.gethead_account_id()%>" <%if(BNK.getheadAccountId().equals(HOA.gethead_account_id())) {%>selected="selected" <%}%>><%=HOA.getname()%></option>
	<%}%>
</select>
</td>
</tr> 


<tr>
<td colspan="3">
<div class="warning" id="bankNameError" style="display: none;">"Bank Name" Required</div>
Bank Name*</td>
</tr>
<tr>
<td colspan="3">
<input type="text" style="width:99%;" name="bank_name" id="bank_name" value="<%=BNK.getbank_name()%>" /></td>
</tr>

<tr>
<td colspan="3"><div class="warning" id="bankBranchError" style="display: none;">"Bank Branch" Required</div>Bank Branch*</td>
</tr>
<tr>
<td colspan="3"><input type="text" style="width:99%;" name="bank_branch" id="bank_branch" value="<%=BNK.getbank_branch()%>"></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="accountHolderNameError" style="display: none;">"Account Holder Name" Required.</div>
Account Holder Name*</td>
</tr>
<tr>
<td colspan="3"><input type="text" style="width:99%;" name="account_holder_name" id="account_holder_name" value="<%=BNK.getaccount_holder_name()%>"/></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="accountNumberError" style="display: none;">"Account Number" Required</div>Account Number*</td>
</tr>
<tr>
<td colspan="3"><input type="text" style="width:99%;" onKeyPress="return numbersonly(this, event,true);" name="account_number" id="account_number" value="<%=BNK.getaccount_number()%>"/></td>
</tr>
<tr>
<td colspan="3">IFSC Code</td>
</tr>
<tr>
<td colspan="3"><input type="text" style="width:99%;" name="ifsc_code" id="ifsc_code" value="<%=BNK.getifsc_code()%>"/></td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>
<%}else{response.sendRedirect("adminPannel.jsp?page=banks");}%>
</body>
</html>