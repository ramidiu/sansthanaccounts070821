<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="mainClasses.medicineissuesListing"%>
<%@ page import="beans.medicineissues"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="beans.products"%>
<%@ page import="mainClasses.productsListing"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Medicineissuereport</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript"
	src="js/modal-window.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script>
	$(function() {
		$("#fromDate").datepicker({

			changeMonth : true,
			changeYear : true,
			numberOfMonths : 1,
			showOn : 'both',
			buttonImage: "../images/calendar-icon.png",
			buttonText : 'Select Date',
			buttonImageOnly : true,
			dateFormat : 'yy-mm-dd',
			onSelect : function(selectedDate) {
				$("#toDate").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#toDate").datepicker({

			changeMonth : true,
			changeYear : true,
			numberOfMonths : 1,
			showOn : 'both',
			buttonImage: "../images/calendar-icon.png",
			buttonText : 'Select Date',
			buttonImageOnly : true,
			dateFormat : 'yy-mm-dd',
			onSelect : function(selectedDate) {
				$("#fromDate").datepicker("option", "maxDate", selectedDate);
			}
		});
	});
</script>
<script type="text/javascript">
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
});
</script>

<style>
.form1 {
	/* background-color: #7f7f7f; */
	position: relative;
	top: 50px;
	/* margin: 51px 2% 5px 2%; */
}

.clear {
	clear: both;
}

.table1 table td.bg {
	background: #e3eaf3;
	font-size: 13px;
	color: #7490ac;
	padding: 5px 0 5px 5px;
	border-right: 1px solid #c0d0e4;
	border-bottom: 1px solid #c0d0e4;
}

.table1 td {
	padding: .75rem;
	line-height: 1.5;
	vertical-align: top;
	/* border-top: 1px solid #eceeef;   */
	font-size: 13px;
	/* padding: 7px 0 7px 5px; */
}

.form-list {
	left: 28px;
	top: 30px;
	position: relative;
	clear: both;
}

.icons {
	float: right;
	margin: 0 42px 0 0;
	width: auto;
	height: 30px;
}
</style>
</head>
<body>
	<div class="form-list">
		<div class="icons">
			<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span>
			<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0"
					title="export to excel"></a></span>

		</div>
		<div class="search-list">
          <input type="hidden" name="page" value="medicineissuereport"></input>
			<form action="adminPannel.jsp" method="post">
			 <input type="hidden" name="page" value="medicineissuereport"></input>
				<ul>
					<li><label>From Date:&nbsp;</label> <input type="text"
						name="fromDate" id="fromDate" class="DatePicker " value="">
					</li>
					<li><label>To Date:&nbsp;</label> <input type="text"
						name="toDate" id="toDate" class="DatePicker " value=""></li>
					<li><input type="submit" class="click"></li>
				</ul>
			</form>

		</div>
	</div>
	<!-- </div>  -->



	<div class="clear"></div>

	<div class="table1" style="margin: 15px 2% 5px 2%;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				<%
				 String fromDate="";
			     String toDate="";
			     String sortBy="";
			      if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){
			    	  fromDate=request.getParameter("fromDate");
			    	  fromDate=fromDate+" 00:00:00";
			      }
			      
			      if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
			         toDate=request.getParameter("toDate");
			         toDate=toDate+" 23:59:59";
			      }
				String productId = "";
				Double consumption = 0.0;
				Double openingBalance = 0.0;
				Double closingBalance = 0.0;
				Double totalOpeningBalance=0.0;
				Double totalclosingBalance=0.0;
				Double totalConsumption=0.0;
				medicineissuesListing ml = new medicineissuesListing();
				List<medicineissues> medicineissuesList = ml.getIssuedMedicineDetails(fromDate,toDate);

				productsListing pl = new productsListing();
				Map<String, products> productsMap = pl.getMedicineProdutsMap();
				
				
				%>
				<% if(medicineissuesList.size()>0 &&  medicineissuesList!=null){%>
					<table width="100%" cellpadding="0" cellspacing="0" border="1"
						id="tblExport">
						<tbody>
							<tr>
								<td class="bg" width="20%">Product Name</td>
								<td class="bg" width="20%">Product ID</td>
								<td class="bg" width="20%">Opening Balance</td>
								<td class="bg" width="20%">Consumption</td>
								<td class="bg" width="20%">Closing Balance</td>
							</tr>

							<%
						
								for (int i = 0; i < medicineissuesList.size(); i++) {
									medicineissues mi = medicineissuesList.get(i);
									productId = mi.gettabletName();
									consumption = mi.getConsumption();
									products product=null;
									if(productsMap.containsKey(productId)){
									    product = productsMap.get(productId);
										openingBalance = Double.parseDouble(product.getbalanceQuantityGodwan());
										closingBalance = openingBalance - consumption;
									
									
							%>
							<tr>

								<td><%=product.getproductName()%></td>
								<td><%=product.getproductId()%></td>
								<td><%=openingBalance%></td>
								<td><%=consumption%></td>
								<td><%=closingBalance%></td>


							</tr>

							<%
									}
								}
							%>

							<tr>


							</tr>
							<!-- <tr>
								<td colspan="3" align="left"
									style="background-color: #bf802b; color: #fff; font-weight: bold;"><b>Total</b></td>
								<td></td>
								<td></td>
							</tr> -->


						</tbody>
						<%}else{
							%> <center><span style="color:red;font-family: verdana;font-size: 250%;"><strong>No Records Are available</strong></span></center>
							<%
						}
							%>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>