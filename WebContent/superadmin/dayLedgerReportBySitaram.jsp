<%@page import="beans.customerpurchases"%>
<%@page import="beans.productexpenses"%>
<%@page import="beans.bankbalance"%>
<%@page import="mainClasses.ReportesListClass"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="beans.banktransactions"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
        <script type="text/javascript">
    function formsubmit(){
    	if($( "#major_head_id" ).val()!=""){    		
        	document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReportBySitaram&majrhdid="+$( '#major_head_id' ).val()+"&cumltv="+$('#cumltv').val();
        	document.getElementById("departmentsearch").submit();
    		    	} else if($( "#minor_head_id" ).val()!=""){
    		    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReportBySitaram&subhid="+$( '#minor_head_id' ).val()+"&cumltv="+$('#cumltv').val();
    		        	document.getElementById("departmentsearch").submit();
    		    	}  else{    	
    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReportBySitaram&cumltv="+$('#cumltv').val();
    	document.getElementById("departmentsearch").submit();
    	}
    }</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("superadminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">
<%
DecimalFormat df=new DecimalFormat("#,###.00");	
SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
headofaccountsListing HOA_L=new headofaccountsListing();
mainClasses.subheadListing SUBH_L=new mainClasses.subheadListing();
SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
String subhid="";
if(request.getParameter("subhid")!=null){
	subhid=request.getParameter("subhid");
}
String hod = request.getParameter("hod");
String subhead = request.getParameter("subid").toString();
String minorhead = request.getParameter("minorHeadId").toString();
String majorHeadId = request.getParameter("majorHeadId");

String finYr = request.getParameter("finYr");

String fromYear = finYr.substring(0, 4);
String toYear = finYr.substring(5, 7);

String dt = request.getParameter("dt");
String type=request.getParameter("typeserch");


String subheadName = SUBH_L.getSuheadNameBasedOnHOAandMINHD(subhead,minorhead,hod);

%>
<div class="vendor-list">
				<div class="icons">
				<div style="margin-bottom: 20px;float: left;cursor: pointer;"><img src="../images/back-button (1).png" width="100" height="50" onclick="goBack()" title="print"/></div>
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
<div class="clear"></div>
<div class="list-details">
<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hod) %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Day Wise Ledger Report Of Sub Head</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date  <%=dt+"-01" %> TO <%=dt+"-31" %> </td></tr>
						<tr>
						<td colspan="7">
						<span style="font-weight: bold; font-size:14px; padding-left:80px;"><%=subheadName %>(<%=subhead %>)</span>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="bg" width="10%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="20%" style="font-weight: bold;">Date</td>
							<td class="bg" width="25%" style="font-weight: bold;" align="right">DEBIT</td>
							<td class="bg" width="25%" style="font-weight: bold;" align="right">CREDIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="right">BALANCE</td>
						</tr>
<%

String cashtype="online pending";
String cashtype1="othercash";
String cashtype2="offerKind";
String cashtype3="journalvoucher";


double credit=0.0;
double debit=0.0;
double credit1=0.0;
double debit1=0.0;
double balance=0.0;

		String fromdate = dt+"-01 00:00:00";
		String todate = dt+"-31 23:59:59";
		
		String fdate = fromdate;
		String tdate = todate;

		ReportesListClass reportesListClass = new ReportesListClass();
		
		List<bankbalance> debitAmountOpeningBalList = null;
		List<bankbalance> creditAmountOpeningBalList = null;
		
		if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
		{
			debitAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hod, majorHeadId, minorhead, subhead, finYr,"Assets", "debit", "extra5", "date");
			creditAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hod, majorHeadId, minorhead, subhead, finYr,"Liabilites", "credit", "extra5", "date");
		} 
		
		List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hod, majorHeadId, minorhead, subhead, fromdate, todate, "date", "date", "not a loan bank");
		List<banktransactions> otherAmountListForLoanBank = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hod, MH.getmajor_head_id(), minorhead, subhead, fromdate, todate, "date", "date", "loan bank");
			
		List<productexpenses> productexpensesList = reportesListClass.getSumOfAmountFromProductexpenses(hod, majorHeadId, minorhead, subhead, fdate, tdate, "p.date", "date");
// 		List<productexpenses> productexpensesJVDebit =	reportesListClass.getJVAmountsFromproductexpenses(hod, majorHeadId, minorhead, subhead, cashtype3, fdate, tdate, "pe.entry_date", "date", "debit");
		List<customerpurchases> customerpurchases = reportesListClass.getJVAmountsFromcustomerpurchases(hod, majorHeadId, minorhead, subhead, fdate, tdate, cashtype3, "date", "date");
		
		// jv amount from productexpensec withbank and withloanbank and withvendor 
		List<productexpenses> productexpensesJV =	reportesListClass.getJVAmountsFromproductexpenses(hod, majorHeadId, minorhead, subhead, cashtype3, fdate, tdate, "pe.entry_date", "date", "credit");
		List<customerpurchases> DollarAmt = reportesListClass.getSumOfDollorAmountfromcustomerpurchases(hod, majorHeadId, minorhead, subhead, fdate, tdate, cashtype1, "cp.date", "date");
		List<customerpurchases> cardCashChequeJvAmountList = reportesListClass.getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(hod, majorHeadId, minorhead, subhead, fdate, tdate, "cp.date", "date");
// 		List<customerpurchases> jvCreditAmountFromCustomerpurchasesList = reportesListClass.getJVCreditAmountFromCustomerPurchases(hod, majorHeadId, minorhead, subhead, fdate, tdate, "cp.date", "date");
		
		String day = "";
		
		for(int i = 1; i <= 31; i++){
			
			if(i < 10){
				day = "-0"+i;
			}else{
				day = "-"+i;
			}
			
			double otherAmount2 = 0.0;
			if( otherAmountList != null && otherAmountList.size()>0)
			{
				for(int j = 0 ; j < otherAmountList.size(); j++){
					banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
					if( banktransactions.getExtra13().trim().equalsIgnoreCase(dt+day) && banktransactions.getamount() != null)
					{
						otherAmount2 = otherAmount2 + Double.parseDouble(banktransactions.getamount());
					}
				}
			}
			
			double otherAmountLoan2 = 0.0;
			if( otherAmountListForLoanBank != null && otherAmountListForLoanBank.size()>0)
			{
				for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
					banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
					if( banktransactions.getExtra13().trim().equalsIgnoreCase(dt+day) && banktransactions.getamount() != null)
					{
						otherAmountLoan2 = otherAmountLoan2 + Double.parseDouble(banktransactions.getamount());
					}
				}
			}
			
			
			
			double debitAmountOpeningBal = 0.0;
			double creditAmountOpeningBal = 0.0;
			if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
				for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
					bankbalance bankbalance = debitAmountOpeningBalList.get(j);
					if(bankbalance.getExtra10().trim().equalsIgnoreCase(dt+day)){
						debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
					}
				}
			}
			if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
				for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
					bankbalance bankbalance = creditAmountOpeningBalList.get(j);
					if(bankbalance.getExtra10().trim().equalsIgnoreCase(dt+day)){
						creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
					}
				}
			}
			
			double LedgerSum = 0.0;
			double LedgerSumBasedOnJV = 0.0;
			double productExpJVdebitAmount = 0.0;
		
	
			if(productexpensesList != null && productexpensesList.size() > 0){
				for(int j = 0; j < productexpensesList.size(); j++){
				
					productexpenses productexpenses = productexpensesList.get(j);
					if(productexpenses.getExtra10().trim().equalsIgnoreCase(dt+day)){
						LedgerSum = LedgerSum + Double.parseDouble(productexpenses.getamount());
					}
				}
			}
			
			if(customerpurchases != null && customerpurchases.size() > 0){
				for(int j = 0; j < customerpurchases.size(); j++){
					
					customerpurchases customerpurchases2 = customerpurchases.get(j);
					if(customerpurchases2.getExtra20().trim().equalsIgnoreCase(dt+day)){
						LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases2.gettotalAmmount());
					}
				}
			}
			
// 			if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
// 				for(int j = 0; j < productexpensesJVDebit.size(); j++){
// 					productexpenses productexpenses = productexpensesList.get(j);
// 					if(productexpenses.getExtra10().trim().equalsIgnoreCase(dt+day)){
// 						productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
// 					}
// 				}
// 			}
			
			debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan2;
			debit = debit + debit1; 
			
			
			double productExpJVAmount = 0.0;
		 	
			if(productexpensesJV != null && productexpensesJV.size() > 0){
				for(int j = 0; j < productexpensesJV.size(); j++){
					productexpenses productexpenses = productexpensesJV.get(j);
					if(productexpenses.getExtra10().trim().equalsIgnoreCase(dt+day)){
						productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpenses.getamount());
					}
					
				}
			}
			double SumDollarAmt = 0.0;
			
			if(DollarAmt != null && DollarAmt.size() > 0){
				for(int j = 0; j < DollarAmt.size(); j++){
					customerpurchases customerpurchases2 = DollarAmt.get(j);
					if(customerpurchases2.getExtra20().trim().equalsIgnoreCase(dt+day)){
						SumDollarAmt = SumDollarAmt + Double.parseDouble(customerpurchases2.gettotalAmmount());
					}
				}
			}
			
			double cardCashChequeJvOnlineAmount = 0.0;
			
			
			if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
				for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
					customerpurchases customerpurchases2 = cardCashChequeJvAmountList.get(j);
					if(customerpurchases2.getExtra20().trim().equalsIgnoreCase(dt+day)){
						cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(customerpurchases2.gettotalAmmount());
					}
				}
			}
			
			double jvCreditAmountFromCustomerpurchases = 0.0;
// 			if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
// 				for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
// 					customerpurchases customerpurchases2 = jvCreditAmountFromCustomerpurchasesList.get(j);
// 					if(customerpurchases2.getExtra20().trim().equalsIgnoreCase(dt+day)){
// 						jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
// 					}
// 				}
// 			}
			
			credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount2 + jvCreditAmountFromCustomerpurchases;
			
			credit = credit + credit1;
			
			%>
			<tr>
			<td><%=i %></td>
			<td><span style="font-weight: bold;"><a href="adminPannel.jsp?page=dateLedgerReportBySitaram&typeserch=<%=type%>&fromdate=<%=dt+day %>&todate=<%=dt+day %>&finYr=<%=finYr %>&subheadId=<%=subhead%>&minorheadId=<%=minorhead%>&majorHeadId=<%=majorHeadId %>&hod=<%=hod%>&cumltv=<%=request.getParameter("cumltv")%>"><%=dt+day %></a></span></td>
			<td align="right"> <%=df.format(debit1) %> </td>
			<td align="right"> <%=df.format(credit1) %> </td>
			<%
			 if(debit>credit)
             {
          	   balance=debit-credit;
             }
             if(credit>debit)
             {
          	   balance=credit-debit;
             }
             if(credit==debit)
             {
          	   balance=0.0;
             }
             
             %>
			<td style="font-weight: bold;" colspan="0" width="20%" align="right"><%=df.format(balance)%></td>					                   
			</tr>
			<%
			
			
		}

			

%>



</table>
			</td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td style="font-weight: bold;text-align: right;" colspan="0" width="30%">Total  </td>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="25%">Rs.<%=df.format(debit) %></td>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="25%">Rs.<%=df.format(credit) %> </td>
						<%if(debit>credit){balance=debit-credit;}if(credit>debit){balance=credit-debit;}if(credit==debit){balance=0.0;}%>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="20%">Rs.<%=df.format(balance) %> </td>
						</tr>
				
					</table>
			</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
