<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
function search(){
	$("#search").submit();
}

</script>
</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="PRD" class="beans.products"/>
<body>
<%
if(session.getAttribute("superadminId")!=null){ %>
<div class="vendor-page">
<div align="center" style="padding-top: 50px;font: bold;">Stock Ledger Report</div>
<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<form id="search" action="adminPannel.jsp" method="get">
<div class="search-list">
<ul>
<li>
<input type="hidden" name="page" value="stock-LedgerReport">
<select name="headAccountId" id="headAccountId" onchange="search();">
<option value="">Sort By Head Account</option>
<%
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
Calendar cal = Calendar.getInstance();
// added by gowri shankar
int year = 0;
// month start from 0 to 11
if (cal.get(Calendar.MONTH) >= 3) year = cal.get(Calendar.YEAR);
else year = cal.get(Calendar.YEAR) - 1;
//added by gowri shankar
List HA_Lists=HOA_L.getheadofaccounts();
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></li>
</ul>
</div>
</form>
<div class="clear"></div>
<div class="list-details">
<%
String hoid="";
if(request.getParameter("headAccountId")!=null){
	hoid=request.getParameter("headAccountId");
}
mainClasses.productsListing PRD_LST=new mainClasses.productsListing();
List PRD_L=PRD_LST.getMedicalStore(hoid);
if(PRD_L.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="6%">Product Id</td>
<td class="bg" width="23%">Product Name</td>
<td class="bg" width="23%"></td>
<!-- <td class="bg" width="10%">Delete</td> -->
</tr>
<%for(int i=0; i < PRD_L.size(); i++ ){
	PRD=(beans.products)PRD_L.get(i); %>
<tr>
<td><%=i+1%></td>
<td><a href="adminPannel.jsp?page=product-Monthly-Ledger&productname=<%=PRD.getproductId()%>&year=<%=year%>"><span><%=PRD.getproductId()%></span></a></td>
<td><a href="adminPannel.jsp?page=product-Monthly-Ledger&productname=<%=PRD.getproductId()%>&year=<%=year%>"><span><%=PRD.getproductName()%></span></a></td>
<%-- <td><a href="majorhead_Delete.jsp?Id=<%=PRD.getproductId()%>">Delete</a></td> --%>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no major heads</h1></div>
<%}%>
</div>
</div>

</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>