<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#checkDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#dateError').hide();
	$('#bankError').hide();
	$('#checkNoError').hide();
	$('#nameError').hide();

	if($('#checkDate').val().trim()==""){
		$('#dateError').show();
		$('#checkDate').focus();
		return false;
	}
	if($('#bank').val().trim()==""){
		$('#bankError').show();
		$('#bank').focus();
		return false;
	}
	if($('#checkNo').val().trim()==""){
		$('#checkNoError').show();
		$('#checkNo').focus();
		return false;
	}
	if($('#nameOnCheck').val().trim()==""){
		$('#nameError').show();
		$('#nameOnCheck').focus();
		return false;
	}
	if(($('#amount0').val().trim()=="" || $('#vendorname0').val().trim()=="")){
		$('#vendorname0').addClass('borderred');
		$('#vendorname0').focus();
		return false;
	}
}
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	if(document.getElementById("vendorname"+i).value!=""){
		if(document.getElementById("amount"+i).value!="")
		{
			for(var j=0;j<=i;j++){
			price=document.getElementById("amount"+j).value;
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(price));
			document.getElementById("total").value=Totalprice;}
		}
	}
	else{
		document.getElementById('vendorname'+i).className = 'borderred';
		document.getElementById('vendorname'+i).placeholder  = 'Please enter vendor';
		document.getElementById("vendorname"+i).focus();
	}
	document.getElementById("check"+i).checked = true;
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
$(document).ready(function(){
  $("#vendorName").keyup(function(){
	  var data1=$("#vendorName").val();
	  $.post('searchVendor.jsp',{q:data1},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#vendorName" ).autocomplete({source: availableTags}); 
			});
  });
  $("#bank").keyup(function(){
	  var data1=$("#bank").val();
	  $.post('searchBanks.jsp',{q:data1},function(data)
	  {
		 var response = data.trim().split("\n");
		 var bankNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 bankNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=bankNames;
		 $( "#bank" ).autocomplete({source: availableTags}); 
			});
  });
});
</script>

<script>
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j-5;
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function vendorsearch(j){
		  var data1=$('#vendorname'+j).val();
		  var bank=$('#headAccountId').val();
		//alert("sj"+bank);
		  $.post('searchVendor.jsp',{q:data1,b:bank},function(data)
		{
			var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
 			 $( '#vendorname'+j).autocomplete({source: availableTags}); 
				});
}
function invoicesList(j){
	  var headId=$('#headAccountId').val(); 
	  var data1=$('#vendorname'+j).val();
	  var inv=$('#invoiceNo'+j).val();
	  $.post('searchInvoices.jsp',{q:headId,id:data1,invo:inv},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[1]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $('#invoiceNo'+j).autocomplete({source: availableTags,select: function(event, ui) {
			 amount=ui.item.value.split(" ");
			 $('#amount'+j).val(amount[1]);
         }}); 
			});
}
function demo(value) {
	alert(value);

	}
</script>

<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>


<%doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
int u=0;
if(session.getAttribute("superadminId")!=null){ %>

<!-- main content -->

<jsp:useBean id="DOC" class="beans.doctordetails"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<form name="tablets_Update" method="post" action="payments_Insert.jsp" onsubmit="return validate();">
<div class="vendor-page">
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<div class="vendor-box">
<div class="vendor-title">Payments to vendor's(Print check)</div>
<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- <td width="35%">Document Number</td> -->
<td ><div class="warning" id="dateError" style="display: none;">Please select date.</div>Date*</td>
<td><div class="warning" id="bankError" style="display: none;">Please select a bank.</div>Bank*</td>
<td><div class="warning" id="headOfAccountError" style="display: none;">please Select "Head Of Account".</div>
Head Of Account*</td>
</tr>
<tr>
<!-- <td><input type="text" name="docNumber" id="docNumber" value="" disabled="disabled"/></td> -->
<td><input type="text" name="checkDate" id="checkDate" value="<%=currentDate%>" /></td>
<td><input type="text" name="bank" id="bank" value="" /></td>
<td >
<select name="headAccountId" id="headAccountId" style="width:200px;">
<option value="">--select--</option>
<%mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();
for(int i=0; i < HOA_List.size(); i++ ){
	HOA=(beans.headofaccounts)HOA_List.get(i);%>
	<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname()%></option>
	<%}%>
</select>
</td>
</tr>


<tr>
<td><div class="warning" id="checkNoError" style="display: none;">Please Provide  "Check Number".</div>Check no*</td>
<td><div class="warning" id="nameError" style="display: none;">Please Provide  "name on check".</div>Name on Check *</td>
<td >Description</td>

</tr>
<tr>
<td><input type="text" name="checkNo" id="checkNo" value="" /></td>
<td><input type="text" name="nameOnCheck" id="nameOnCheck" value="" /></td>
<td><textarea  name="Narration" id="Narration" style="height: 25px;"></textarea></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>


<div class="vendor-list">
<div class="sno bgcolr">S.No.</div>
<div class="ven-nme bgcolr">Vendor Name</div>
<div class="ven-nme bgcolr">Invoice Number</div>
<div class="ven-nme bgcolr">Description</div>
<div class="ven-amt bgcolr">Amount</div>


<input type="hidden" name="addcount" id="addcount" value="10"/>
<%for(int i=0; i < 20; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

<div class="sno"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme"><input type="text" name="vendorname<%=i%>" id="vendorname<%=i%>"  onkeyup="vendorsearch('<%=i%>')" class="" value=""/></div>
<div class="ven-nme"><input type="text" name="invoiceNo<%=i%>" id="invoiceNo<%=i%>" onkeyup="invoicesList('<%=i%>')" onclick="formValuesFill();" value=""></input></div>
<div class="ven-nme"><input type="text" name="desc<%=i%>" id="desc<%=i%>" value=""></input></div>
<div class="ven-amt">&#8377;<input type="text" name="amount<%=i%>" id="amount<%=i%>" onblur="calculate('<%=i%>')" value="" style="width:110px;"/></div>
</div>
<%} %>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/></div>
<div class="tot-ven">Total<input type="text" name="total" id="total" value="0.00" style="width:120px;"/></div>

<input type="submit" value="Pay" class="click" style="border:none; float:right; margin:0 10px 0 0"/>

</div>



</div>
</form>

<!-- main content -->

<%}else{
	response.sendRedirect("index.jsp");
} %>

