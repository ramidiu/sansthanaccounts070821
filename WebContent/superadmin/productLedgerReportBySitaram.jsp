<%@page import="beans.customerpurchases"%>
<%@page import="beans.productexpenses"%>
<%@page import="beans.bankbalance"%>
<%@page import="mainClasses.ReportesListClass"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
        <script type="text/javascript">
    function formsubmit(){
    	if($( "#major_head_id" ).val()!=""){    		
        	document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReportBySitaram&majrhdid="+$( '#major_head_id' ).val()+"&cumltv="+$('#cumltv').val();
        	document.getElementById("departmentsearch").submit();
    		    	} else if($( "#minor_head_id" ).val()!=""){
    		    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReportBySitaram&subhid="+$( '#minor_head_id' ).val()+"&cumltv="+$('#cumltv').val();
    		        	document.getElementById("departmentsearch").submit();
    		    	}  else{    	
    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReportBySitaram&cumltv="+$('#cumltv').val();
    	document.getElementById("departmentsearch").submit();
    	}
    }</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
</head>
<body>

<%
if(session.getAttribute("superadminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">
<%
DecimalFormat df = new DecimalFormat("#,###.00");

// SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
headofaccountsListing HOA_L = new headofaccountsListing();
subheadListing SUBH_L=new subheadListing();
SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
SimpleDateFormat onlydat2=new SimpleDateFormat("dd-MMM-yyyy");
// String subhid="";



String category="";
if(request.getParameter("category")!=null){
	category=request.getParameter("category").toString();
	
}
// String hod=request.getParameter("hod");
// if(request.getParameter("subhid")!=null){
// 	subhid=request.getParameter("subhid");
// 	//System.out.println(subhid);
// }
String finyr[]=null;
String finyrfrm="2015-04-01 00:00:01";
String finyrto="2016-03-31 23:59:59";
if(request.getParameter("financialyear")!=null){
	finyr=request.getParameter("financialyear").split("-");
	finyrfrm=finyr[0]+"-04-01 00:00:01";
	finyrto=finyr[1]+"-04-01 00:00:01";
}


// String subhead=request.getParameter("subhead");
// String minorhead = request.getParameter("subhid");
//System.out.println("subhead==>"+subhead);
//System.out.println("minorhead==>"+minorhead);

String cashtype="online pending";
String cashtype1="othercash";
String cashtype2="offerKind";
String cashtype3="journalvoucher";




String subhead = request.getParameter("subhead");
String minorHeadId = request.getParameter("minorHeadId");
String majorHeadId = request.getParameter("majorHeadId");
String hod = request.getParameter("hod");

String fromdate = request.getParameter("fromdate");
String todate = request.getParameter("todate");

String fdate = fromdate+" 00:00:00";
String tdate = todate +" 23:59:59";

String type=request.getParameter("typeserch");
String subheadName = SUBH_L.getSuheadNameBasedOnHOAandMINHD(subhead, minorHeadId, hod);


%>
<div class="vendor-list">
				<div class="icons">
				<div style="margin-bottom: 20px;float: left;cursor: pointer;"><img src="../images/back-button (1).png" width="100" height="50" onclick="goBack()" title="print"/></div>
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
<div class="clear"></div>
<div class="list-details">
<div class="printable">
					<%
					
				
						String dos=request.getParameter("fromdate");
						if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
						{
							dos = request.getParameter("finYear").substring(0, 4)+"-04-01";
						}
					%>
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hod) %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Month Ledger Report Of Sub Head</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date  <%=fromdate %> TO <%=todate %> </td></tr>
						<tr>
						<td colspan="7">
						
						<span style="font-weight: bold; font-size:14px; padding-left:80px;"><%=subheadName %>(<%=subhead %>)</span>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
							<td class="bg" width="10%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="20%" style="font-weight: bold;">Month</td>
							<td class="bg" width="25%" style="font-weight: bold;" align="right">DEBIT</td>
							<td class="bg" width="25%" style="font-weight: bold;" align="right">CREDIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="right" colspan="0">BALANCE</td>
						</tr>
						<%if(!category.equals("")){ %>
						<tr><td colspan="2"><%=SUBH_L.getMsubheadname(subhead) %></td>
						<td colspan="2"></td></tr>
	
						<%}
		
		String finYr = request.getParameter("finYear");
		String fromYear = finYr.substring(0, 4);
		String toYear = finYr.substring(0, 2)+finYr.substring(5, 7);
		
		double credit = 0.0;
		double debit = 0.0;
		double debit1 = 0.0;
		double credit1 = 0.0;
		double balance = 0.0;
		


		ReportesListClass reportesListClass = new ReportesListClass();
		
		List<bankbalance> debitAmountOpeningBalList = null;
		List<bankbalance> creditAmountOpeningBalList = null;
		
		if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
		{
			debitAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hod, majorHeadId, minorHeadId, subhead, finYr,"Assets", "debit", "extra5", "MONTHNAME");
			creditAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hod, majorHeadId, minorHeadId, subhead, finYr,"Liabilites", "credit", "extra5", "MONTHNAME");
		} 
		
		
		List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hod, majorHeadId, minorHeadId, subhead, fromdate, todate, "month(date)", "MONTHNAME", "not a loan bank");
		List<banktransactions> otherAmountListForLoanBank = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hod, majorHeadId, minorHeadId, subhead, fromdate, todate, "month(date)", "MONTHNAME", "loan bank");
		
		
		List<productexpenses> productexpensesList = reportesListClass.getSumOfAmountFromProductexpenses(hod, majorHeadId, minorHeadId, subhead, fdate, tdate, "month(p.date)", "MONTHNAME");
// 		List<productexpenses> productexpensesJVDebit =	reportesListClass.getJVAmountsFromproductexpenses(hod, majorHeadId, minorHeadId, subhead, cashtype3, fdate, tdate, "month(pe.entry_date)", "MONTHNAME", "debit");
		List<customerpurchases> customerpurchases = reportesListClass.getJVAmountsFromcustomerpurchases(hod, majorHeadId, minorHeadId, subhead, fdate, tdate, cashtype3, "month(date)", "MONTHNAME");
		
		// jv amount from productexpensec withbank and withloanbank and withvendor 
		List<productexpenses> productexpensesJV =	reportesListClass.getJVAmountsFromproductexpenses(hod, majorHeadId, minorHeadId, subhead, cashtype3, fdate, tdate, "month(pe.entry_date)", "MONTHNAME", "credit");
		List<customerpurchases> DollarAmt = reportesListClass.getSumOfDollorAmountfromcustomerpurchases(hod, majorHeadId, minorHeadId, subhead, fdate, tdate, cashtype1, "month(cp.date)", "MONTHNAME");
		List<customerpurchases> cardCashChequeJvAmountList = reportesListClass.getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(hod, majorHeadId, minorHeadId, subhead, fdate, tdate, "month(cp.date)", "MONTHNAME");
// 		List<customerpurchases> jvCreditAmountFromCustomerpurchasesList = reportesListClass.getJVCreditAmountFromCustomerPurchases(hod, majorHeadId, minorHeadId, subhead, fdate, tdate, "month(cp.date)", "MONTHNAME");
		
		
		
		String monthNames[] = {"April", "May", "June", "July", "August", "September", "October", "November", "December", "January", "February", "March"};
		String monthNo[] = { fromYear+"-04", fromYear+"-05", fromYear+"-06", fromYear+"-07", fromYear+"-08", fromYear+"-09", fromYear+"-10", fromYear+"-11", fromYear+"-12", toYear+"-01", toYear+"-02", toYear+"-03"};
		
		for(int k = 0; k < monthNames.length; k++){
			
			double otherAmount2 = 0.0;
			if( otherAmountList != null && otherAmountList.size()>0)
			{
				for(int j = 0 ; j < otherAmountList.size(); j++){
					banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
					if( banktransactions != null && banktransactions.getExtra13().trim().equalsIgnoreCase(monthNames[k]) && banktransactions.getamount() != null)
					{
						otherAmount2 = otherAmount2 + Double.parseDouble(banktransactions.getamount());
					}
				}
			}
			
			double otherAmountLoan2 = 0.0;
			if( otherAmountListForLoanBank != null && otherAmountListForLoanBank.size()>0)
			{
				for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
					banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
					if( banktransactions != null && banktransactions.getExtra13().trim().equalsIgnoreCase(monthNames[k]) && banktransactions.getamount() != null)
					{
						otherAmountLoan2 = otherAmountLoan2 + Double.parseDouble(banktransactions.getamount());
					}
				}
			} 
			
			
			
			double debitAmountOpeningBal = 0.0;
			double creditAmountOpeningBal = 0.0;
			if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
				for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
					bankbalance bankbalance = debitAmountOpeningBalList.get(j);
					if( bankbalance != null && bankbalance.getExtra10().trim().equalsIgnoreCase(monthNames[k])){
						debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
					}
				}
			}
			if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
				for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
					bankbalance bankbalance = creditAmountOpeningBalList.get(j);
					if( bankbalance != null && bankbalance.getExtra10().trim().equalsIgnoreCase(monthNames[k])){
						creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
					}
				}
			}
			

			double LedgerSum = 0.0;
			double LedgerSumBasedOnJV = 0.0;
			double productExpJVdebitAmount = 0.0;
		
	
			if(productexpensesList != null && productexpensesList.size() > 0){
				for(int j = 0; j < productexpensesList.size(); j++){
				
					productexpenses productexpenses = productexpensesList.get(j);
					if( productexpenses != null && productexpenses.getExtra10().trim().equalsIgnoreCase(monthNames[k])){
						LedgerSum = LedgerSum + Double.parseDouble(productexpenses.getamount());
					}
				}
			}
			
			if(customerpurchases != null && customerpurchases.size() > 0){
				for(int j = 0; j < customerpurchases.size(); j++){
					
					customerpurchases customerpurchases2 = customerpurchases.get(j);
					if( customerpurchases2 != null && customerpurchases2.getExtra20().trim().equalsIgnoreCase(monthNames[k])){
						LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases2.gettotalAmmount());
					}
				}
			}
			
// 			if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
// 				for(int j = 0; j < productexpensesJVDebit.size(); j++){
// 					productexpenses productexpenses = productexpensesList.get(j);
// 					if( productexpenses != null && productexpenses.getExtra10().trim().equalsIgnoreCase(monthNames[k])){
// 						productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
// 					}
// 				}
// 			}
			
				debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan2;
				debit = debit + debit1; 
				
				
				double productExpJVAmount = 0.0;
			 	
				if(productexpensesJV != null && productexpensesJV.size() > 0){
					for(int j = 0; j < productexpensesJV.size(); j++){
						productexpenses productexpenses = productexpensesJV.get(j);
						if( productexpenses != null && productexpenses.getExtra10().trim().equalsIgnoreCase(monthNames[k])){
							productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpenses.getamount());
						}
						
					}
				}
				double SumDollarAmt = 0.0;
				
				
				if(DollarAmt != null && DollarAmt.size() > 0){
					for(int j = 0; j < DollarAmt.size(); j++){
						customerpurchases customerpurchases2 = DollarAmt.get(j);
						if( customerpurchases2 != null && customerpurchases2.getExtra20().trim().equalsIgnoreCase(monthNames[k])){
							SumDollarAmt = SumDollarAmt + Double.parseDouble(customerpurchases2.gettotalAmmount());
						}
					}
				}
				
				double cardCashChequeJvOnlineAmount = 0.0;
				
				
				if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
					for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
						customerpurchases customerpurchases2 = cardCashChequeJvAmountList.get(j);
						if( customerpurchases2 != null && customerpurchases2.getExtra20().trim().equalsIgnoreCase(monthNames[k])){
							cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(customerpurchases2.gettotalAmmount());
						}
					}
				}
				
				double jvCreditAmountFromCustomerpurchases = 0.0;
// 				if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
// 					for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
// 						customerpurchases customerpurchases2 = jvCreditAmountFromCustomerpurchasesList.get(j);
// 						if( customerpurchases2 != null && customerpurchases2.getExtra20().trim().equalsIgnoreCase(monthNames[k])){
// 							jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
// 						}
// 					}
// 				}
				
				credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount2 + jvCreditAmountFromCustomerpurchases;
				
				credit = credit + credit1;
			%>
			<tr>
			<td><%=(k+1) %></td>
			<td><span style="font-weight: bold;"><a href="adminPannel.jsp?page=dayLedgerReportBySitaram&typeserch=<%=type%>&subid=<%=subhead%>&minorHeadId=<%=minorHeadId%>&majorHeadId=<%=majorHeadId %>&hod=<%=hod%>&finYr=<%=finYr %>&dt=<%=monthNo[k]%>&cumltv=<%=request.getParameter("cumltv")%>"><%=monthNames[k] %></a></span></td>
			<td align="right"> <%=df.format(debit1) %> </td>
			<td align="right"> <%=df.format(credit1) %> </td>
			<%
			 if(debit>credit)
             {
          	   balance=debit-credit;
             }
             if(credit>debit)
             {
          	   balance=credit-debit;
             }
             if(credit==debit)
             {
          	   balance=0.0;
             }
             
             %>
			<td style="font-weight: bold;" colspan="0" width="20%" align="right"><%=df.format(balance)%></td>					                   
			</tr>
			<%
		}
		
// }
		 
		 %>

</table>
			</td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td style="font-weight: bold;text-align: right;" colspan="0" width="30%">Total  </td>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="25%">Rs.<%=df.format(debit) %></td>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="25%">Rs.<%=df.format(credit) %> </td>
						<%if(debit>credit){balance=debit-credit;}if(credit>debit){balance=credit-debit;}if(credit==debit){balance=0.0;}%>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="20%">Rs.<%=df.format(balance) %> </td>
						</tr>
				
					</table>
			</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
