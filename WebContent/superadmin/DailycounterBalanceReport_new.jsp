<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="mainClasses.CounterBalanceAndBankDeposits"%>
<%@page import="java.util.ArrayList"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.util.TimeZone"%>
<%@page import="model.SansthanAccountsDate"%>
<%@page import="beans.banktransactions"%>
<%@page import="java.util.Date"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.cashier_reportListing"%>
<%@page import="mainClasses.banktransactionsListing" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAILY COUNTER BALANCE REPORT</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	
	
	/* $(document).ready(function(){ */
		$(function() {
			$( "#fromDate" ).datepicker({
				minDate: new Date(2016, 03, 1),
				maxDate: new Date(2017, 02, 31),
				changeMonth: true,
				changeYear: true,
				numberOfMonths: 1,
				showOn: 'both',
				buttonImage: "../images/calendar-icon.png",
				buttonText: 'Select Date',
				 buttonImageOnly: true,
				dateFormat: 'yy-mm-dd',
				onSelect: function( selectedDate ) {
					$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
				} 
			});
				$( "#toDate" ).datepicker({
					minDate: new Date(2016, 03, 1),
					maxDate: new Date(2017, 02, 31),
				changeMonth: true,
				changeYear: true,
				numberOfMonths:1,
				showOn: 'both',
				buttonImage: "../images/calendar-icon.png",
				buttonText: 'Select Date',
				 buttonImageOnly: true,
				dateFormat: 'yy-mm-dd',
				onSelect: function( selectedDate ) {
					$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
				} 
			});
		});
		/* }); */

		function datepickerchange()
		{
			var finYear=$("#finYear").val();
			var yr = finYear.split('-');
			var startDate = yr[0]+",04,01";
			var endDate = parseInt(yr[0])+1+",03,31";
			
			$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
			$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
			$('#toDate').datepicker('option', 'minDate', new Date(startDate));
			$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
		}

		$(document).ready(function(){
			datepickerchange();
		});
</script>
<script>
 /*  $(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
	});	
}); */   
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>

<body>
<jsp:useBean id="CUS" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="BTR" class="beans.banktransactions"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">


<div style="text-align: center;"></div>
<div class="icons">
<span> <a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="total-report">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<div class="printable" >
<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td><table width="100%" cellpadding="0" cellspacing="0" class="date-wise "  >
<tr><td colspan="5" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;" class="bg-new">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="5" align="center" style="font-weight: bold;" class="bg-new">Daily Counter Balance Report</td></tr>
<form action="adminPannel.jsp" method="get">
<input type="hidden" name="page" value="DailycounterBalanceReport_new"></input>
<tr>
<ul>
<%
DecimalFormat DF = new DecimalFormat("#.00;-#.00");
customerpurchasesListing c_list =new customerpurchasesListing();
bankbalanceListing BBAL_L=new mainClasses.bankbalanceListing();
headofaccountsListing HODL = new headofaccountsListing();
subheadListing SUBL = new subheadListing();
banktransactionsListing blist = new banktransactionsListing();

SimpleDateFormat cdf = new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");

c1.getActualMaximum(Calendar.DAY_OF_MONTH);
int lastday = c1.getActualMaximum(Calendar.DATE);
c1.set(Calendar.DATE, lastday);  
String todate=(sdf.format(c1.getTime())).toString();
c1.getActualMinimum(Calendar.DAY_OF_MONTH);
int firstday = c1.getActualMinimum(Calendar.DATE);
c1.set(Calendar.DATE, firstday); 
String fromdate=(sdf.format(c1.getTime())).toString();



//String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
//String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
							}
/*  String hoid[]=new String[]{"4","1"};
String reportType[]=new String[]{"pro-counter","Charity-cash"}; */
						%>
<li></li>
</ul>
<%-- <td width="20%">
Select Fin Year : 
	<select name="finYear" id="finYear"  Onchange="datepickerchange();">
		<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
		<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
	</select>
</td>		 --%>


			<td width="20%">Select Fin Year : 
			<select name="finYear" id="finYear"  Onchange="datepickerchange();">
			<%@ include file="yearDropDown1.jsp" %>
					</select>
			</td> 

<%String hoaid="";
String headgroup="";
String reporttype="";
String reporttype1="";
headofaccountsListing HOA_L=new headofaccountsListing();
List headaclist=HOA_L.getheadofaccounts(); 
if(request.getParameter("head_account_id")!=null){
	hoaid=request.getParameter("head_account_id");
	System.out.println("head account111..."+hoaid);
	}

%>

<td width="20%"><input type="hidden" name="page" value="totalsalereport"/>From Date <input type="text" name="fromDate" id="fromDate" class="DatePicker" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="20%">To Date <input type="text" name="toDate" id="toDate" class="DatePicker" readonly="readonly" value="<%=todate%>"/> </td>
<td width="20%"><select name="head_account_id" id="head_account_id">
<option value="counterBal" >Select Department</option>
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td width="15%"><input type="submit" value="Submit" class="click bordernone"/></td>
</tr>
</form>
</table></td></tr></table>

<br/>
<%  

String headAccountName = "";
if(request.getParameter("head_account_id")!=null){
	hoaid=request.getParameter("head_account_id");
	 //System.out.println("hoid value123..."+hoaid);

 if(hoaid.equals("4")){
		//System.out.println("sansthan...");
		headgroup="1";
		reporttype = "pro-counter";
		reporttype1 = "Development-cash";	
		headAccountName = "SANSTHAN";
	}else if(hoaid.equals("1")){
		reporttype = "Charity-cash";
		reporttype1 = "Not-Required";	
		headAccountName = "CHARITY";
	}
	else if(hoaid.equals("3")){
		reporttype = "poojastore-counter";
		reporttype1 = "Not-Required";
		headAccountName = "POOJA STORES";
	}
	else if(hoaid.equals("5")){
		reporttype = "Sainivas";
		reporttype1 = "Not-Required";	
		headAccountName = "SAI NIVAS";
	} 
}
%>
<%if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("")) {

String head_account_name=HOA_L.getHeadofAccountName(hoaid);
%>
<div id="tblExport">

<!-- SANSTHAN -->
<table width="100%" border="1" cellspacing="0" cellpadding="0">
<tr><td colspan="8" style="font-weight:bold;color:#f00;text-align:center;padding:5px 0px 5px 0px;"><%=head_account_name %> COUNTER BALANCE AND DEPOSITED AMOUNT</td></tr>
  <tr>
    <td colspan="3"  align="center" style="font-weight: bold;color: teal;">INCOME</td>
    <td colspan="5" align="center"  style="font-weight: bold;color: teal;">DEPOSIT</td>
  </tr>
  
  <tr>
    <td align="center" style="font-weight: bold;color:#934C1E;">Date</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Head Of Account</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Counter Amount</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Date</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Account Deposit Category</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Deposit Amount</td>
   <!--  <td align="center" style="font-weight: bold;color:#934C1E;">Balance Amount</td> -->
    <td align="center" style="font-weight: bold;color:#934C1E;">Total Bal Amount</td>
  </tr>
  <%
  String addedDate1=SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
  double sansthantotalcountercash =0.0;
  double sansthantotalcashpaid =0.0;
  double sansthangrandtotal = 0.0;
  String finStartYr = request.getParameter("finYear").substring(0, 4);
  
  double finOpeningBalSansthan = BBAL_L.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));
  
  String counterBal = c_list.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate);
  String bankDeposit = blist.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1);
  
  
  
  System.out.println("counterBal ======> "+counterBal);
  System.out.println("bankDeposit ======> "+bankDeposit);
  
  
  
  
  if(counterBal != null && !counterBal.equals("")){
	  sansthantotalcountercash = Double.parseDouble(counterBal);
  }
   if(bankDeposit != null && !bankDeposit.equals("")){
	  sansthantotalcashpaid = Double.parseDouble(bankDeposit);
  } 
//   double sansthanonlinedeposits = 0.0;
//   if(blist.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1)!= null && !blist.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1).equals("")){
// 	  sansthanonlinedeposits = Double.parseDouble(blist.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1));
//   }
  sansthangrandtotal = finOpeningBalSansthan + sansthantotalcountercash - sansthantotalcashpaid;

  double openingbal = 0.0;
  openingbal = sansthangrandtotal;
  
  System.out.println("finOpeningBalSansthan ======> "+finOpeningBalSansthan);
  System.out.println("sansthantotalcountercash ======> "+sansthantotalcountercash);
  System.out.println("sansthantotalcashpaid ======> "+sansthantotalcashpaid);
  
  System.out.println("sansthangrandtotal ======> "+sansthangrandtotal);
  
  
  
  %>
  <tr>
  <td colspan="6" align="center" style="font-weight: bold">OPENING BALANCE</td>
  <td style="font-weight: bold"><%=DF.format(openingbal) %></td>
  </tr>
  <%
  
  List<String> listOfDates= SansthanAccountsDate.getAddedBetweenDates(fromdate, todate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
  
  double totalamt11=0.0;
  double totalamt22=0.0;
  double grandTotal=0.0;
  double totalamt=openingbal;
  double santhanprototal=0.0;
  double santhandeposittotal=0.0;
  
  double counterAmountTotal = 0.0;
  double depositAmountTotal = 0.0;
  double totalPendingAmount = 0.0;
  
  counterAmountTotal = openingbal;
  
  CounterBalanceAndBankDeposits balanceAndBankDeposits = new CounterBalanceAndBankDeposits();
  
  double counterAmount1 = 0.0;
  double depositAmount1 = 0.0;
  
  counterAmount1 = openingbal;
  
  List<customerpurchases> customerpurchasesList = balanceAndBankDeposits.getcustomerpurchasesAfterFinYearToBeforeFromDate(hoaid, fromdate+" 00:00:00", todate+" 23:59:59", "groupBy");
  List<banktransactions> banktransactionsList = balanceAndBankDeposits.getbanktransactionsAfterFinYearToBeforeFromDate(reporttype, reporttype1, fromdate+" 00:00:00", todate+" 23:59:59", "group");
  
  %>
  <tr>
  
		<td style="color:#000;">--</td>
	    <td style="color:#000;">--</td>
	    <td style="color:#000;">--</td>
  
  <%
  for(int i = 0 ; i < 1; i++){
	  String dateNow = listOfDates.get(i);
	  %>
	    <td style="color:#000;background:rgba(228, 220, 220,1);"><%=dateNow %></td>
	    	<%
  if(banktransactionsList != null && banktransactionsList.size() > 0){
	    			for(int j = 0 ; j < banktransactionsList.size(); j++){
	    				banktransactions banktransactions = banktransactionsList.get(j);
	    				if(banktransactions != null && banktransactions.getdate().trim().equals(dateNow)){
	    					%>
	    					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=banktransactions.getextra4() %></td>
	    					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=DF.format(Double.parseDouble( banktransactions.getamount() ))%></td>
	    					<%
	    					depositAmount1 = depositAmount1 + Double.parseDouble(banktransactions.getamount());
	    					banktransactionsList.remove(j);
	    					break;
	    				}else{
	    	    			%>
	   					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=reporttype %></td>
	   					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=0.00 %></td>
	   					<%
	   					break;
	   	    			}
	    			}
	    		}else{
	    			%>
					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=reporttype %></td>
					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=0.00 %></td>
					<%
	    		}
  }
%>
<td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format( counterAmount1 - depositAmount1 ) %></td>
</tr>
<%
 for(int i = 0 ; i < listOfDates.size() ; i++){
	 String dateNow = listOfDates.get(i);
	  %>
	  <tr>
	    <td style="color:#000;"><%=dateNow %></td>
	    <td style="color:#000;"><%=headAccountName %></td>
	    <td style="color:#000;"> 
	    	<%
	    		if(customerpurchasesList != null && customerpurchasesList.size() > 0){
	    			for(int j = 0 ; j < customerpurchasesList.size(); j++){
	    				customerpurchases customerpurchases = customerpurchasesList.get(j);
	    				if(customerpurchases != null && customerpurchases.getdate().trim().equals(dateNow)){
		    				%>
		    					<%=DF.format(Double.parseDouble( customerpurchases.gettotalAmmount() )) %>
		    				<%
		    				counterAmount1 = counterAmount1 + Double.parseDouble(customerpurchases.gettotalAmmount());
		    				customerpurchasesList.remove(j);
		    				break;
	    				}else{
	    	    			%>
	    					<%=0.00 %>
	    					<%
	    					break;
	    	    		}
	    			}
	    		}else{
	    			%>
					<%=0.00 %>
					<%
	    		}
	    	%>
	    </td>
	    <%
	    	if(i != listOfDates.size() - 1 ){
	    		dateNow = listOfDates.get(i+1);
	    %>
	    <td style="color:#000;background:rgba(228, 220, 220,1);"><%=dateNow %></td>
	    	<%
	    		if(banktransactionsList != null && banktransactionsList.size() > 0){
	    			for(int j = 0 ; j < banktransactionsList.size(); j++){
	    				banktransactions banktransactions = banktransactionsList.get(j);
	    				if(banktransactions != null && banktransactions.getdate().trim().equals(dateNow)){
	    					%>
	    					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=banktransactions.getextra4() %></td>
	    					 <td style="color:#000;background:rgba(228, 220, 220,1);">
	    					 	<a href="adminPannel.jsp?page=DailycounterBalanceReportDayWise&dt=<%=dateNow%>&rt=<%=hoaid%>" target="_blank">
	    					 		<%=DF.format(Double.parseDouble( banktransactions.getamount() ))%>
	    					 	</a>
	    					 </td>
	    					<%
	    					depositAmount1 = depositAmount1 + Double.parseDouble(banktransactions.getamount());
	    					banktransactionsList.remove(j);
	    					break;
	    				}else{
	    	    			%>
	   					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=reporttype %></td>
	   					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=0.00 %></td>
	   					<%
	   					break;
	   	    			}
	    			}
	    		}else{
	    			%>
					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=reporttype %></td>
					 <td style="color:#000;background:rgba(228, 220, 220,1);"><%=0.00 %></td>
					<%
	    		}
	    	}else{
	    		%>
				 <td style="color:#000;background:rgba(228, 220, 220,1);">--</td>
				 <td style="color:#000;background:rgba(228, 220, 220,1);">--</td>
				 <td style="color:#000;background:rgba(228, 220, 220,1);">--</td>
				<%
	    	}
	    	%>
	    <td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format( counterAmount1 - depositAmount1 ) %></td>
	</tr>
	    <%
 }
 %>
 	<tr>
		<td colspan="2" align="right" style="font-weight: bold">TOTAL</td>
		<td style="font-weight: bold"><%=DF.format(counterAmount1) %></td>
		<td colspan="2"></td>
		<td style="font-weight: bold"><%=DF.format(depositAmount1) %></td>
		<td style="font-weight: bold"><%=DF.format( counterAmount1 - depositAmount1 ) %></td>
	</tr>

</table>
<br/>
<br/>

<br/>
</div>
<%} %>
</div>
</div>
</div>
</div>

</body>
</html>
