<%@page import="beans.consumption_entries"%>
<%@page import="mainClasses.consumption_entriesListing"%>
<%@page import="beans.produceditems"%>
<%@page import="mainClasses.produceditemsListing"%>
<%@page import="beans.stockrequest"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Stock_Issue_And_ProducedItems</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
function showDetails1(billId){
	$("#"+billId).slideToggle();
	/* if(document.getElementById(billId).style.display=="none"){
		//document.getElementById(billId).style.display='block';
		$("#"+billId).slideToggle();
	}else if(document.getElementById(billId).style.display=="block"){
		//document.getElementById(billId).style.display='none';
		$("#"+billId).slideToggle();
	} */
	
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="STCKREQ" class="beans.stockrequest"></jsp:useBean>
<jsp:useBean id="PRDUCD" class="beans.produceditems"></jsp:useBean>
<jsp:useBean id="CNSUMP" class="beans.consumption_entries"></jsp:useBean>
</head>
<body>
<%
stockrequestListing STCKREQL=new stockrequestListing();
produceditemsListing PRDUCDL=new produceditemsListing();
consumption_entriesListing CONSL=new consumption_entriesListing();
List KOTREQLIST=null;
List KOTDETLREQLIST=null;
List PRDCDLIST=null;
List CNSMPLIST=null;
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateOnlyFormat= new SimpleDateFormat("yyyy-MM-dd");
DateFormat UserDateFormat= new SimpleDateFormat("dd-MM-yyyy");
DateFormat onlyYearFormat= new SimpleDateFormat("yyyy");

dateFormat.setTimeZone(tz.getTimeZone("IST"));
String currentDate=(dateOnlyFormat.format(c1.getTime())).toString();
String fromDate=currentDate+" 00:00:01";
String toDate=currentDate+" 23:59:59";
if(request.getParameter("today")!=null && request.getParameter("today").equals("Today")){
	fromDate=currentDate+" 00:00:01";
	toDate=currentDate+" 23:59:59";
}else if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
	c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
	fromDate=(dateOnlyFormat.format(c1.getTime())).toString()+" 00:00:00";
	c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
	toDate=(dateOnlyFormat.format(c1.getTime())).toString();
	toDate=(dateOnlyFormat.format(c1.getTime())).toString()+" 23:59:59";
}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
	c1.getActualMaximum(Calendar.DAY_OF_MONTH);
	int lastday = c1.getActualMaximum(Calendar.DATE);
	c1.set(Calendar.DATE, lastday);  
	toDate=(dateOnlyFormat.format(c1.getTime())).toString()+" 23:59:59";
	c1.getActualMinimum(Calendar.DAY_OF_MONTH);
	int firstday = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday); 
	fromDate=(dateOnlyFormat.format(c1.getTime())).toString()+" 00:00:00";
}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
	int lastday_y = c1.get(Calendar.YEAR);
	c1.set(Calendar.DATE, lastday_y);  
	c1.getActualMinimum(Calendar.DAY_OF_YEAR);
	int firstday_y = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday_y); 
	Calendar cld = Calendar.getInstance();
	cld.set(Calendar.DAY_OF_YEAR,1); 
	fromDate=(onlyYearFormat.format(cld.getTime())).toString()+"-04-01 00:00:00";
	cld.add(Calendar.YEAR,+1); 
	toDate=(onlyYearFormat.format(cld.getTime())).toString()+"-03-31 23:59:59";
}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate")+" 00:00:01";
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate")+" 23:59:59";
	}
}
String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}
						KOTREQLIST=STCKREQL.getstockrequestBasedOnHOA("0",fromdate,todate,"KOT");
					
						
						
%>
<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down"> KOT Request And Produced Products Report</div>
<div class="icons">
<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>
<div class="clear"></div>
<div class="total-report">
<form action="adminPannel.jsp" method="post">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="4" class="bg-new" style="color: #F00;">*** SHRI SHIRDI SAI BABA SANSTHAN TRUST ***</td></tr>
<tr>
<td width="40%" colspan="2"><input type="hidden" name="page" value="Stock_Issue_And_ProducedItems">From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="40%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" name="search" value="Search" class="click bordernone"/></td>
</tr>
 <tr>
	<td><input type="submit" name="today" value="Today" class="click bordernone"/></td>
	<td><input type="submit" name="weekly" value="Week Report" class="click bordernone"/></td>
	<td><input type="submit" name="monthly" value="Month Report" class="click bordernone"/></td>
	<td><input type="submit" name="yearly" value="Year Report" class="click bordernone"/></td>
</tr>
</table>
</form>
<div id="tblExport">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="5" class="bg-new" style="color:#F00;cursor: pointer;"> KOT Request And Produced Products Report</td></tr>
	<tr ><td colspan="5">
	<table width="95%" cellpadding="0" cellspacing="0" class="date-wise" style="margin: auto;" >
	<tr>
		<td style="font-weight: bold;color:#F00;cursor: pointer;" width="5%">S.NO.</td>
		<td style="font-weight: bold;color:#F00;cursor: pointer;" width="5%">Date</td>
		<td style="font-weight: bold;color:#F00;cursor: pointer;" width="25%">Purpose</td>
		<td style="font-weight: bold;color:#F00;cursor: pointer;" width="40%">Stock Received</td>
		<td style="font-weight: bold;color:#F00;cursor: pointer;" width="25%">End Product</td>
		<td style="font-weight: bold;color:#F00;cursor: pointer;" width="25%">Distribution</td>
	</tr>
	<%
	if(KOTREQLIST.size()>0){
		for(int i=0;i<KOTREQLIST.size();i++){
		STCKREQ=(stockrequest)KOTREQLIST.get(i);
		KOTDETLREQLIST=STCKREQL.getRequestDetailsBasedOnID(STCKREQ.getreqinv_id());
		PRDCDLIST=PRDUCDL.getproduceditems(STCKREQ.getreqinv_id());
		CNSMPLIST=CONSL.getConsumption_entriesBasedKOT(STCKREQ.getreqinv_id());
	%>
	<tr ><td width="10%"><%=i+1%></td>
			<td width="10%"><%=UserDateFormat.format(dateFormat.parse(STCKREQ.getreq_date()))%></td>
		<td width="20%"><%=STCKREQ.getnarration() %></td>
		<td width="35%">
	<table cellpadding="0" cellspacing="0" class="date-wise" >
	<tr>
		<td style="font-weight: bold;color:#F00;cursor: pointer;" >Item</td>
		<td style="font-weight: bold;color:#F00;cursor: pointer;">Quantity</td>
		<td style="font-weight: bold;color:#F00;cursor: pointer;">Units</td>
		<td style="font-weight: bold;color:#F00;cursor: pointer;">Price</td>
	</tr>
	<%		for(int j=0;j<KOTDETLREQLIST.size();j++){
		STCKREQ=(stockrequest)KOTDETLREQLIST.get(j); %>
			<tr>
		<td style="font-weight: bold;" ><%=STCKREQ.getproduct_id() %> <%=STCKREQ.getextra2() %></td>
		<td style="font-weight: bold;"><%=STCKREQ.getquantity() %></td>
		<td style="font-weight: bold;"><%=STCKREQ.getextra4() %></td>
		<td style="font-weight: bold;"><%=STCKREQ.getextra3() %></td>
	</tr>
		<%} %>
	</table></td>
	
	<td width="20%"><%for(int j=0;j<PRDCDLIST.size();j++){
		PRDUCD=(produceditems)PRDCDLIST.get(j); %> <%=PRDUCD.getproductId() %> :<%=PRDUCD.getquantity() %> <br/> <%} %></td>
		<td width="15%"><%for(int k=0;k<CNSMPLIST.size();k++){
			CNSUMP=(consumption_entries)CNSMPLIST.get(k); %> <span style="font-size: 10px;"> <%=CNSUMP.getconsumption_category() %>(<%=CNSUMP.getcon_sub_category() %>)</span>  : <span style="font-size: 11px;"><%=CNSUMP.getdevotees_qty() %></span> <span style="font-size: 8px;"> Membrs</span>  <br/> <%} %></td>
		</tr>
	<%}} else{
	%>
	<tr>
	<td colspan="6">
	No,KOT Request Found. 
	</td>
	</tr>
	<%
	}%>
	</table></td>
	</tr>

	</table></div>
</div>
</div>
</div>
</body>
</html>