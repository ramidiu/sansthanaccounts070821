<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="beans.payments"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="beans.purchaseorder"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.indent"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<jsp:useBean id="INDT" class="beans.indent"></jsp:useBean>
<jsp:useBean id="IAP" class="beans.indentapprovals"></jsp:useBean>
<jsp:useBean id="PUO" class="beans.purchaseorder"></jsp:useBean>
<jsp:useBean id="PDEXP" class="beans.productexpenses"></jsp:useBean>
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<jsp:useBean id="GOD" class="beans.godwanstock"></jsp:useBean>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Total sale report</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<style>
#yourID.fixed {
    position: fixed;
    top: 0;
    left: 28px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}
#yourID1.fixed1 {
    position: fixed;
    top: 0;
    left: 300px;
    z-index: 1;
    width: 150px;
    border-bottom: 5px solid #ffffff; background:#F00;
}
#yourID2.fixed2 {
    position: fixed;
    top: 0;
    left: 320px;
    z-index: 1;
    width: 150px;
    border-bottom: 5px solid #ffffff; background:#F00;
}
#yourID3.fixed3 {
    position: fixed;
    top: 0;
    left: 470px;
    z-index: 1;
    width: 150px;
    border-bottom: 5px solid #ffffff; background:#F00;
}
#yourID4.fixed4 {
    position: fixed;
    top: 0;
    left: 620px;
    z-index: 1;
    width: 150px;
    border-bottom: 5px solid #ffffff; background:#F00;
}
</style>
<script>
$(document).ready(function () {  
  var top = $('#yourID').offset().top - parseFloat($('#yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('#yourID').addClass('fixed');
	        $('#yourID1').addClass('fixed1');
			      $('#yourID2').addClass('fixed2');
				        $('#yourID3').addClass('fixed3');
						      $('#yourID4').addClass('fixed4');
    } else {
      // otherwise remove it
      $('#yourID').removeClass('fixed');
	        $('#yourID1').removeClass('fixed1');
				        $('#yourID2').removeClass('fixed2');
$('#yourID3').removeClass('fixed3');
$('#yourID4').removeClass('fixed4');
    }
  });
});
</script>
</head>

<body>
<div class="vendor-page">
<div class="vendor-list">
<div class="icons">
					<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
					</span>
				</div>
<div class="clear"></div>
<form action="adminPannel.jsp" method="get">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>Status</td>
<td>Head Account</td>
<td>MSE (or) IDR ID</td>
</tr>
<tr>
<td>
<select name="status" id="status" required>
<option value="">-Status-</option>
<option value="1" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("1")){ %> selected="selected" <%} %>>All Pending</option>
<option value="2" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("2")){ %> selected="selected" <%} %>>All Success</option>
</select></td>
<td>
<select name="hoid" id="hoid" required>
<option value="">-Select HOA-</option>
<option value="3" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("3")){ %> selected="selected" <%} %>>Pooja stores</option>
<option value="4" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("4")){ %> selected="selected" <%} %>>Sansthan</option>
<option value="1" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("1")){ %> selected="selected" <%} %>>CHARITY</option>
<option value="5" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("5")){ %> selected="selected" <%} %>>SHRI SAINIVAS</option>
</select></td><td>
<input type="text" name="numbersearch" id="numbersearch" <%if(request.getParameter("numbersearch")!=null && !request.getParameter("numbersearch").equals("")) {%>value="<%=request.getParameter("numbersearch") %>" <%} else { %>value="" <%} %> placeholder="Indent No or MSE No" />
<input type="submit" value="Search" />
</td>
<td>
<input type="hidden" name="page" value="performance-detail-report" />
</td>
</tr>
</table>

</form>
<div class="printable">
<div class="total-report" >
<!-- <table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="4" class="bg-new">Sri Sai Nivas Accounts</td></tr>
<tr>
<td width="20%">
<ul>
<li><input type="checkbox" /> Master suite</li>
<li><input type="checkbox" /> Master suite</li>
<li><input type="checkbox" /> Master suite</li>
<li><input type="checkbox" /> Master suite</li>
<li><input type="checkbox" /> Master suite</li>
</ul></td>
<td width="30%">From Date <input type="text" /> <img src="../images/calendar-icon.png" /></td>
<td width="30%">To Date <input type="text" /> <img src="../images/calendar-icon.png" /></td>
<td width="15%"><input type="submit" value="Submit" class="click bordernone"/></td>
</tr>
<tr>
<td><input type="submit" value="Today" class="click bordernone"/></td>
<td><input type="submit" value="Current Week" class="click bordernone"/></td>
<td><input type="submit" value="Current Month" class="click bordernone"/></td>
<td><input type="submit" value="Current Year" class="click bordernone"/></td>
</tr>
</table> -->
<!--<table width="95%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="15">

<table width="100%" cellpadding="0" cellspacing="0" class="indent-wise">


</table>
</td>
</tr>
</table>-->


<%
String hod="";
String numbersearch="";
String status="";
if(request.getParameter("numbersearch")!=null){
	numbersearch=request.getParameter("numbersearch");
}
if(request.getParameter("hoid")!=null){
	hod=request.getParameter("hoid").toString();
}
if(request.getParameter("status")!=null){
	status=request.getParameter("status").toString();
}
mainClasses.indentListing INDL=new mainClasses.indentListing();
mainClasses.indentapprovalsListing APPR_L=new mainClasses.indentapprovalsListing();
employeesListing EMP_L=new employeesListing();
superadminListing SUP_L=new superadminListing();
mainClasses.godwanstockListing GOSL=new mainClasses.godwanstockListing();
vendorsListing VENL=new vendorsListing();
productexpensesListing PRDE_L=new productexpensesListing();
paymentsListing PAYM_L=new paymentsListing();
SimpleDateFormat DBDate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat userDate=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
DecimalFormat DF=new DecimalFormat("0.00");
List POCOUL=null;
List MSEL=null;
List PAYMETL=null;
List PRDEXPL=null;
List APP_Det=null;
int payrow=0;
String payrows="";
String payrowA[]=null;
int billrow=0;
int masterrow=0;
int porow=0;
int maxno=1;
int invno=0;
mainClasses.purchaseorderListing POL=new mainClasses.purchaseorderListing();
List INDC=null;
if(!hod.equals("") && numbersearch.equals("")){
	INDC=INDL.getindentGroupByIndentinvoice_id(hod,"");
} else if(!hod.equals("") && numbersearch.contains("INV") || numbersearch.contains("MSE") ){
	INDC=INDL.getindentGroupByIndentinvoice_id(hod,INDL.getindentnoBasedonMaster(numbersearch));
} else if(!hod.equals("") && numbersearch.contains("IDR")){
	INDC=INDL.getindentGroupByIndentinvoice_id(hod,numbersearch);
}
%>
<table width="95%" cellpadding="0" cellspacing="0" class="indent-wise" style="background:#f3f8fe;" id="tblExport">

<tr>
<td id="yourID" style="padding:0px; border:none; background:#d0e3fb; color:#000; font-size:13px;" colspan="25">
<table cellpadding="0" cellspacing="0" style="padding:0px; margin:0px; border:none;" width="100%">
<tr>
<td colspan="4" style="width:280px; font-size:13px;">Indent</td>
<td colspan="3"  style="width:210px; font-size:13px;">Purchaseorder</td>
<td colspan="4" style="width:220px; font-size:13px;">MSE</td>
<td colspan="6" style="width:170px; font-size:13px;">BillsApproved/VerifiedBy</td>
<td colspan="3" style="width:160px; font-size:13px;">BillPaymentApprovedBy</td>
<td colspan="5" style="width:150px; font-size:13px;">Method Of Transaction</td>
</tr>
<tr>
<td style="border:none; text-align:left">INO</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">EnterBy</td>
<td style="border:none; text-align:left;">ApprovedBy</td>
<td class="borderleftcolr" style="border:none; text-align:left">P.No</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left; padding:0 0 0 5px;">VENDOR</td>
<td class="borderleftcolr;" style="border:none; text-align:left">MSE No</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">EnterBy</td>
<td style="border:none; text-align:left">ApprovedBy</td>
<td class="borderleftcolr" style="border:none; text-align:left">Bill No</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">AMOUNT</td>
<td style="border:none; text-align:left">ApprovedBy</td>
<!-- <td style="border:none; text-align:left">BillNo</td> -->
<td style="border:none; text-align:left">Accountant</td>
<td style="border:none; text-align:left">Manager</td>
<td class="borderleftcolr" style="border:none; text-align:left">P.O.NO</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">ApprovedBy</td>
<td class="borderleftcolr" style="border:none; text-align:left">VCR.NO</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">PaymentType</td>
<td style="border:none; text-align:left">ChequeMode</td>
<td style="border:none; text-align:left">Amount</td>
</tr>
</table>
</td>
</tr>
<% if(INDC!=null && INDC.size()>0 && !status.equals("")){
for(int i=0;i<INDC.size();i++){
		INDT=(indent)INDC.get(i);
	if(!INDT.getindentinvoice_id().equals("IDR243") && !INDT.getindentinvoice_id().equals("IDR242")){
		if(INDT.getextra4()!=null && !INDT.getextra4().equals("") && !INDT.getextra4().equals("null")){
			POCOUL=POL.getpurchaseordersBasedIndentId(INDT.getindentinvoice_id());
			porow=POL.getpurchaseordersBasedIndentIdSize(INDT.getindentinvoice_id());
			masterrow=GOSL.getgodwanstockGroupByInvoiceIDSizeWithUniqueId(INDT.getextra4());
			billrow=PRDE_L.getproductexpensesBasedUniqueIdSize(INDT.getextra4());
			payrows=PAYM_L.getMpaymentsBasedOnUniqueIdSize(INDT.getextra4());
			payrowA=payrows.split(" ");
			payrow=Integer.parseInt(payrowA[0]);
			payrows=payrowA[1];
		} else {
			POCOUL=POL.getpurchaseordersBasedIndentId(INDT.getindentinvoice_id());
			porow=POL.getpurchaseordersBasedIndentIdSize(INDT.getindentinvoice_id());
			masterrow=GOSL.getgodwanstockGroupByInvoiceIDSize(INDT.getindentinvoice_id());
			billrow=PRDE_L.getproductexpensesBasedIndentSize(INDT.getindentinvoice_id());
			payrows=PAYM_L.getMpaymentsBasedOnExpIDSize(INDT.getindentinvoice_id());
			payrowA=payrows.split(" ");
			payrow=Integer.parseInt(payrowA[0]);
			payrows=payrowA[1];
		}

if(payrows.equals("no") && status.equals("1")) {
	if (porow > maxno) {
		maxno = porow;
	}
	if (masterrow > maxno) {
		maxno = masterrow;
	}
	if (billrow > maxno) {
		maxno = billrow;
	}
	if (payrow > maxno) {
		maxno = payrow;
	}
	if(i==0){ %>

<%} %>
<tr>

<td rowspan="<%=maxno %>" style="width:50px;"><a href="#" target="_blank" onclick="openMyModal('invoiceDetails.jsp?invID=<%=INDT.getindentinvoice_id()%>&report=indent'); return false;"><%=INDT.getindentinvoice_id()%></a></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%=userDate.format(DBDate.parse(INDT.getdate())) %></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%=EMP_L.getMemployeesName(INDT.getemp_id())  %></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%
APP_Det=APPR_L.getIndentDetails(INDT.getindentinvoice_id());
if(APP_Det.size()>0){
	for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);%><%=SUP_L.getSuperadminname(IAP.getadmin_id())%></br><br>(<%=CDF.format(SDF.parse(IAP.getapproved_date())) %> <%=time.format(SDF.parse(IAP.getapproved_date())) %>)</br> <%}} %></td>
<%if(POCOUL.size()>0){
for(int j=0;j<POCOUL.size();j++){
	PUO=(purchaseorder)POCOUL.get(j);
	MSEL=GOSL.getgodwanstockGroupByInvoiceID(PUO.getpoinv_id());
	if(j>0){
%>
<tr>
<%} %>
<td rowspan="<%=maxno %>" class="borderleftcolr" style="width:50px;"><a href="#" target="_blank" onclick="openMyModal('invoiceDetails.jsp?invID=<%=PUO.getpoinv_id()%>&report=purchaseorder'); return false;"><%=PUO.getpoinv_id() %></a></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%=userDate.format(DBDate.parse(PUO.getcreated_date())) %></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%=VENL.getMvendorsAgenciesName(PUO.getvendor_id()) %></td>
<%if(MSEL.size()>0){
for(int k=0;k<MSEL.size();k++){ 
GOD=(godwanstock)MSEL.get(k);
PRDEXPL=PRDE_L.getproductexpensesBasedInvoice(GOD.getextra1());
if(k>0){
%>
<tr>
<%} %>

<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" class="borderleftcolr" style="width:50px;"><a href="#" target="_blank" onclick="openMyModal('invoiceDetails.jsp?invID=<%=GOD.getextra1() %>&report=mse'); return false;"><%=GOD.getextra1() %></a></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=userDate.format(DBDate.parse(GOD.getdate())) %></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=EMP_L.getMemployeesName(GOD.getemp_id())  %></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=EMP_L.getMemployeesName(GOD.getCheckedby())  %></td>
<%if(PRDEXPL.size()>0){
for(int l=0;l<PRDEXPL.size();l++){ 
	PDEXP=(productexpenses)PRDEXPL.get(l);
	PAYMETL=PAYM_L.getMpaymentsBasedOnExpID(PDEXP.getexpinv_id());
	APP_Det=APPR_L.getIndentDetails(GOD.getextra1());
if(l>0){%>
<tr>
<%} %>
<td  rowspan="1" class="borderleftcolr" style="width:50px;"><%=PDEXP.getexpinv_id() %></td>
<td  rowspan="1" style="width:50px;"><%=userDate.format(DBDate.parse(PDEXP.getentry_date())) %></td>
<td  rowspan="1" style="width:50px;"><%=DF.format(Double.parseDouble(PDEXP.getamount())) %></td>
<td  rowspan="1" style="width:50px;"><%if(APP_Det.size()>0){
	for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);%><%=SUP_L.getSuperadminname(IAP.getadmin_id())%><br>(<%=CDF.format(SDF.parse(IAP.getapproved_date())) %> <%=time.format(SDF.parse(IAP.getapproved_date())) %>)</br></br><%}} %></td>
<td  rowspan="1" style="width:50px;"><%=EMP_L.getMemployeesName(PDEXP.getEnter_by())  %></td>
<td  rowspan="1" style="width:50px;"><%=EMP_L.getMemployeesName(PDEXP.getemp_id())  %></td>
<%
List PayApprL=null;
if(PAYMETL.size()>0){
	for(int m=0;m<PAYMETL.size();m++){
		PAY=(payments)PAYMETL.get(m);
		APP_Det=APPR_L.getIndentDetails(PDEXP.getexpinv_id());
		PayApprL=PAYM_L.getSuccesfulPaymentsBasedOnExpID(PDEXP.getexpinv_id());
	%>
<td class="borderleftcolr" style="width:50px;"><%=PAY.getpaymentId() %></td>
<td style="width:50px;"><%=userDate.format(DBDate.parse(PAY.getdate())) %></td>
<td style="width:50px;"><%if(APP_Det.size()>0){
	for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);%><%=SUP_L.getSuperadminname(IAP.getadmin_id())%></br><br>(<%=CDF.format(SDF.parse(IAP.getapproved_date())) %> <%=time.format(SDF.parse(IAP.getapproved_date())) %>)</br><%}} %></td>
<%if(m>0){ %>
<tr>
<% }%>
<%if(PayApprL.size()>0){
	for(int s=0;s<PayApprL.size();s++){
		PAY=(payments)PAYMETL.get(s);
	%>
<td class="borderleftcolr" style="width:50px;"><%=PAY.getreference() %></td>
<td style="width:50px;"><%=userDate.format(DBDate.parse(PAY.getdate())) %></td>
<td style="width:50px;"><%=PAY.getpaymentType() %></td>
<td style="width:50px;"><%=PAY.getextra2() %></td>
<td style="width:50px;"><%=DF.format(Double.parseDouble(PAY.getamount())) %></td>
<% if(s==0){%></tr><% } else {%></tr><% }}} else{%>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td><%} }} else{%>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<%}}} else{%>
<td  rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<%} } }else{%>
<td rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<%
	} } }else{%>

<td rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<%} %>
</tr>
<%
} else if(payrows.equals("yes") && status.equals("2")) {
	if (porow > maxno) {
		maxno = porow;
	}
	if (masterrow > maxno) {
		maxno = masterrow;
	}
	if (billrow > maxno) {
		maxno = billrow;
	}
	if (payrow > maxno) {
		maxno = payrow;
	}
	if(i==0){ %>

<%} %>
<tr>

<td rowspan="<%=maxno %>" style="width:50px;"><a href="#" target="_blank" onclick="openMyModal('invoiceDetails.jsp?invID=<%=INDT.getindentinvoice_id()%>&report=indent'); return false;"><%=INDT.getindentinvoice_id()%></a></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%=userDate.format(DBDate.parse(INDT.getdate())) %></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%=EMP_L.getMemployeesName(INDT.getemp_id())  %></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%
APP_Det=APPR_L.getIndentDetails(INDT.getindentinvoice_id());
if(APP_Det.size()>0){
	for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);%><%=SUP_L.getSuperadminname(IAP.getadmin_id())%></br><br>(<%=CDF.format(SDF.parse(IAP.getapproved_date())) %> <%=time.format(SDF.parse(IAP.getapproved_date())) %>)</br> <%}} %></td>
<%if(POCOUL.size()>0){
for(int j=0;j<POCOUL.size();j++){
	PUO=(purchaseorder)POCOUL.get(j);
	MSEL=GOSL.getgodwanstockGroupByInvoiceID(PUO.getpoinv_id());
	if(j>0){
%>
<tr>
<%} %>
<td rowspan="<%=maxno %>" class="borderleftcolr" style="width:50px;"><a href="#" target="_blank" onclick="openMyModal('invoiceDetails.jsp?invID=<%=PUO.getpoinv_id()%>&report=purchaseorder'); return false;"><%=PUO.getpoinv_id() %></a></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%=userDate.format(DBDate.parse(PUO.getcreated_date())) %></td>
<td rowspan="<%=maxno %>" style="width:50px;"><%=VENL.getMvendorsAgenciesName(PUO.getvendor_id()) %></td>
<%if(MSEL.size()>0){
for(int k=0;k<MSEL.size();k++){ 
GOD=(godwanstock)MSEL.get(k);
PRDEXPL=PRDE_L.getproductexpensesBasedInvoice(GOD.getextra1());
if(k>0){
%>
<tr>
<%} %>

<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" class="borderleftcolr" style="width:50px;"><a href="#" target="_blank" onclick="openMyModal('invoiceDetails.jsp?invID=<%=GOD.getextra1() %>&report=mse'); return false;"><%=GOD.getextra1() %></a></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=userDate.format(DBDate.parse(GOD.getdate())) %></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=EMP_L.getMemployeesName(GOD.getemp_id())  %></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=EMP_L.getMemployeesName(GOD.getCheckedby())  %></td>
<%if(PRDEXPL.size()>0){
for(int l=0;l<PRDEXPL.size();l++){ 
	PDEXP=(productexpenses)PRDEXPL.get(l);
	PAYMETL=PAYM_L.getMpaymentsBasedOnExpID(PDEXP.getexpinv_id());
	APP_Det=APPR_L.getIndentDetails(GOD.getextra1());
if(l>0){%>
<tr>
<%} %>
<td  rowspan="1" class="borderleftcolr" style="width:50px;"><%=PDEXP.getexpinv_id() %></td>
<td  rowspan="1" style="width:50px;"><%=userDate.format(DBDate.parse(PDEXP.getentry_date())) %></td>
<td  rowspan="1" style="width:50px;"><%=DF.format(Double.parseDouble(PDEXP.getamount())) %></td>
<td  rowspan="1" style="width:50px;"><%if(APP_Det.size()>0){
	for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);%><%=SUP_L.getSuperadminname(IAP.getadmin_id())%><br>(<%=CDF.format(SDF.parse(IAP.getapproved_date())) %> <%=time.format(SDF.parse(IAP.getapproved_date())) %>)</br></br><%}} %></td>
<td  rowspan="1" style="width:50px;"><%=EMP_L.getMemployeesName(PDEXP.getEnter_by())  %></td>
<td  rowspan="1" style="width:50px;"><%=EMP_L.getMemployeesName(PDEXP.getemp_id())  %></td>
<%
List PayApprL=null;
if(PAYMETL.size()>0){
	for(int m=0;m<PAYMETL.size();m++){
		PAY=(payments)PAYMETL.get(m);
		APP_Det=APPR_L.getIndentDetails(PDEXP.getexpinv_id());
		PayApprL=PAYM_L.getSuccesfulPaymentsBasedOnExpID(PDEXP.getexpinv_id());
	%>
<td class="borderleftcolr" style="width:50px;"><%=PAY.getpaymentId() %></td>
<td style="width:50px;"><%=userDate.format(DBDate.parse(PAY.getdate())) %></td>
<td style="width:50px;"><%if(APP_Det.size()>0){
	for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);%><%=SUP_L.getSuperadminname(IAP.getadmin_id())%></br><br>(<%=CDF.format(SDF.parse(IAP.getapproved_date())) %> <%=time.format(SDF.parse(IAP.getapproved_date())) %>)</br><%}} %></td>
<%if(m>0){ %>
<tr>
<% }%>
<%if(PayApprL.size()>0){
	for(int s=0;s<PayApprL.size();s++){
		PAY=(payments)PAYMETL.get(s);
	%>
<td class="borderleftcolr" style="width:50px;"><%=PAY.getreference() %></td>
<td style="width:50px;"><%=userDate.format(DBDate.parse(PAY.getdate())) %></td>
<td style="width:50px;"><%=PAY.getpaymentType() %></td>
<td style="width:50px;"><%=PAY.getextra2() %></td>
<td style="width:50px;"><%=DF.format(Double.parseDouble(PAY.getamount())) %></td>
<% if(s==0){%></tr><% } else {%></tr><% }}} else{%>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td><%} }} else{%>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<%}}} else{%>
<td  rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<%} } }else{%>
<td rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<%
	} } }else{%>

<td rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<%} %>
</tr>
<%
} 
maxno=1;
payrow=0;
 billrow=0;
 masterrow=0;
 porow=0;
 payrows="";
 } 
} } else{ %>
<tr>
<td colspan="25"> No Bills</td>
</tr>
<%} %>
<tr>
<td colspan="25" class="bordercolr"></td>
</tr>
</table>
</div>
</div>
</div>
</div>
</body>
</html>