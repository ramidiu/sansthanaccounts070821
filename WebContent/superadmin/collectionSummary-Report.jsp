<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!--Date picker script  -->
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$('#'+billId).toggle();
	/* if(document.getElementById(billId).style.display=="none"){
		//document.getElementById(billId).style.display='block';
		$('#'+billId).toggle();
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	} */
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("superadminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">
<div align="center" style="padding-top: 50px;font: bold;">PRO Collection Summary Report</div>
		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<div class="sj">
					 <form action="collectionSummary-Report.jsp" method="post">  
						 <select name="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="of" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("of")){ %>selected="selected" <%} %>>OFFER KINDS</option>
						</select>
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form name="searchForm" id="searchForm" action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" name="page" value="collectionSummary-Report"></input>
						<select name="headID" id="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="of" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("of")){ %>selected="selected" <%} %>>OFFER KINDS</option>
						</select>
						
						</li>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					<!--  <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span> -->
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	banktransactionsListing BKTRAL=new banktransactionsListing();
	productsListing PRDL=new productsListing();
	employeesListing EMPL=new employeesListing();
	
	List BANK_DEP=null;
	
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:00";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	String hoaID="4";
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoaID=request.getParameter("headID");
	}
	List SAL_list=null;
	if(hoaID.equals("of")){
		SAL_list=SALE_L.getOfferKindsPROCollectionSummary(hoaID,fromDate,toDate);
	}else{
		SAL_list=SALE_L.getcustomerPurchasesPROCollectionSummary(hoaID,fromDate,toDate);
	}
	List SAL_DETAIL=null;
	double totalAmount=0.00; 
	double totalUSDAmount=0.00; 
	double totalPoundAmount=0.00; 
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double bankDepositTot=0.00;
	double creaditSaleTot=0.00;
	double cashSaleAmt=0.00;
	if(SAL_list.size()>0){%>
	 <div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;">Collection Summary Report</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
						<tr>
						<td colspan="7">
						<table width="100%" cellpadding="0" cellspacing="0" border='1'>
						<tr>
							<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="2%" style="font-weight: bold;"><!-- DATE --></td>
							<td class="bg" width="15%" style="font-weight: bold;">CODE</td>
							<td class="bg" width="30%" style="font-weight: bold;">PARTICULARS</td>
							<td class="bg" width="5%" style="font-weight: bold;">QTY</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="right">AMOUNT</td>
							<td class="bg" width="15%" style="font-weight: bold;" align="right">CREDIT</td>
						</tr>
						<%-- <%Double totalSaleAmt=0.00;
						Double totDeposit=0.00;
						if(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate)!=null && !SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate).equals("")){
							 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate));
						}
						if(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate)!=null && !BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate).equals("")){
							 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate));}
						 %>
						<tr>
							<td colspan="3"></td>
							<td colspan="2">OPENING BALANCE</td>
							<td align="right" style="font-weight: bold;">&#8377; <%=df.format(totalSaleAmt-totDeposit)%></td>
							<td align="right" style="font-weight: bold;">&#8377;0.00</td>
						</tr> --%>
						<%for(int i=0; i < SAL_list.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_list.get(i);
							if(!presentDate.equals("")){
								prevDate=presentDate;	
							}
							presentDate=""+chngDateFormat.format(dbDateFormat.parse(SALE.getdate()));
							if(SALE.getcash_type().equals("cash")){
								cashSaleAmt=cashSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}else{
								creaditSaleTot=creaditSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}
							%>
						    <tr style="position: relative;">
								<td style="font-weight: bold;"><%=i+1%></td>
								<td style="font-weight: bold;"><%-- <a href="#" onclick="showDetails('<%=SALE.getbillingId()%>')" ><%=chngDateFormat.format(dbDateFormat.parse(SALE.getdate()))%></a> --%></td>
								<td style="font-weight: bold;"><%=SALE.getproductId()%>
								</td>
								<td style="font-weight: bold;"><span><a href="#" onclick="showDetails('<%=SALE.getbillingId()%>')">
								<%=PRDL.getProductsNameByCat(SALE.getproductId(),hoaID)%></a></span></td>
								<td style="font-weight: bold;"><%=SALE.getquantity()%></td>
								<%
								
								if(SALE.getextra5().equals("USD")){
									totalUSDAmount=totalUSDAmount+Double.parseDouble(SALE.gettotalAmmount());
								}else if(SALE.getextra5().equals("POUND")){
									totalPoundAmount=totalPoundAmount+Double.parseDouble(SALE.gettotalAmmount());
								} else{
									totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());
								}
								%>
								<td style="font-weight: bold;" align="right"><%if(SALE.getextra5().equals("USD")){ %>$<%}else if(SALE.getextra5().equals("POUND")){ %>&pound;<%}else{ %>&#8377;<%} %> <%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td>
								<td></td>
						</tr>
						<%/* SAL_DETAIL=SALE_L.getSalesDetailsBasedOnCode(SALE.getproductId()); */
						SAL_DETAIL=SALE_L.getcustomerPurchasesPROCollectionSummaryBasedOnProduct(hoaID,fromDate,toDate,SALE.getproductId());
						if(SAL_DETAIL.size()>0){ %>
						<tr>
							<td colspan="7"   >
							<div style="display: none;" id="<%=SALE.getbillingId()%>">
							<table width="95%" style="border: 1px solid #000;">
							<tr>
							<td width="5%"></td>
							<td width="20%">DATE</td>
							<td width="20%">PARTICULAR</td>
							<td width="20%">PRODUCT SALE QTY</td>
							<td width="10%">AMOUNT</td>
							<td width="20%">BANK AMOUNT</td>
							<td width="10%">NARRATIOIN</td>
						</tr>
						<%for(int s=0;s<SAL_DETAIL.size();s++){
								SA=(beans.customerpurchases)SAL_DETAIL.get(s);%>
								<tr style="position: relative;">
									<td ></td>
									<td><%=chngDateFormat.format(dbDateFormat.parse(SA.getdate()))%></td>
									<td><%=PRDL.getProductsNameByCat(SA.getproductId(), SA.getextra1())%></td>
									<td><%=SA.getquantity()%></td>
									<td><%=SA.getrate() %></td>
									<td><%=SA.gettotalAmmount()%></td>
									<td ><%=SA.getextra2()%><%-- <%=EMPL.getMemployeesName(SA.getemp_id()) %> --%></td>
								</tr>
								<%} %>
						</table>
						</div>
						</td>
						</tr>
					   <%}} %>
					   <%
					   String amtDepositedCat="pro-counter";
					   if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
							if(request.getParameter("headID").equals("1")){
								amtDepositedCat="Charity-cash";
							}else if(request.getParameter("headID").equals("4")){
								amtDepositedCat="pro-counter";
							}
						}
					   List BankDeposits=BKTRAL.getDepositsListBasedOnHead(amtDepositedCat, fromDate,toDate);
					   if(BankDeposits.size()>0){
					   		for(int j=0;j<BankDeposits.size();j++){
					   		BNK=(banktransactions)BankDeposits.get(j);
					   		bankDepositTot=bankDepositTot+Double.parseDouble(BNK.getamount());
					   %>
					   <tr style="color: fuchsia;">
					   		<td style="font-weight: bold;"><%=j+1%></td>
					   		<td style="font-weight: bold;"><%=chngDateFormat.format(dbDateFormat.parse(BNK.getdate()))%></td>
					   		<td style="font-weight: bold;"><%=BNK.getbanktrn_id()%></td>
					   		<td style="font-weight: bold;"><%=BNK.getnarration() %></td>
					   		<td style="font-weight: bold;" colspan="2" align="right"></td>
					   		<td style="font-weight: bold;" align="right">&#8377; <%=df.format(Double.parseDouble(BNK.getamount()))%></td>
					   </tr>
					   
					   <%}} %>
					   </table>
					   </td>
					   </tr>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total sales amount in (&#x20B9;) | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount)%> </td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<%if(totalUSDAmount>0){ %>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total sales amount in (USD-$) | </td>
						<td style="color: orange;font-weight: bold;" align="right"> $ <%=df.format(totalUSDAmount)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<%} %>
						<%if(totalPoundAmount>0){ %>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total sales amount in (Pounds &pound;) | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &pound; <%=df.format(totalPoundAmount)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr><%} %>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total Bank Deposit amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"></td>
						<td style="font-weight: bold;" align="right"> &#8377; <%=df.format(bankDepositTot)%></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Closing Balance | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount-bankDepositTot) %></td>
						<td style="font-weight: bold;"></td>
						</tr>
					</table>
				</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;"><span>There were no collection summary report founds for today!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>