<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sai sansthan accounts-Admin Pannel</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	


<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>

<%if(session.getAttribute("superadminId")!=null){ %>
<jsp:useBean id="SHP" class="beans.products" />
<%

String inwards="00";
String outwards="00";
//String[] majorHeads={"silver","gold"};

productsListing PRD_l=new productsListing ();
List PRODL=PRD_l.getPresentShopStockAssetsName("30038","30316");
godwanstockListing GDSTK_L=new godwanstockListing();
shopstockListing SHPSTK_L=new shopstockListing();%>

<div><%@ include file="title-bar.jsp"%></div>
<div class="list-details" >
	
	 <div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
					<span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div> 
				
	<div class="printable" id="fulltable">
	<table width="95%"  cellspacing="0" cellpadding="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
		<%-- 	<%if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("")){ %> --%>
			<tbody> 
				<tr>
					<td colspan="3" align="center" style="font-weight: bold;color:red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
				</tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
<!-- 				<tr><td colspan="3" align="center" style="font-weight: bold;">GOLD SILVER REPORT</td></tr> -->
				  <tr><td colspan="3" align="center" style="font-weight: bold;">SILVER REPORT</td></tr>
			</tbody>
			<%-- <%} %> --%>
		</table><br>
			<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border='1'>
				<%-- <%if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("")){ %> --%>
				<tr>
					<!-- <td class="bg" width="5%" style="font-weight: bold;">SNO</td> -->
					<td class="bg" width="3%">S.No.</td>
					<td class="bg" width="10%">Product Id</td>
							<td class="bg" width="23%">Product Name</td>
							<td class="bg" width="23%">Quantity</td>
							<td class="bg" width="10%">Units</td>
				</tr>
				<%
				
				for(int i=0; i < PRODL.size(); i++ ){
					SHP=(beans.products)PRODL.get(i);
					
					 %>
				<tr>
				    
					<td><%=i+1 %></td>
					<td><%=SHP.getproductId()%></td>
					<td><%=SHP.getproductName()%></td>
					<td><%=SHP.getbalanceQuantityGodwan()%></td>
					<td><%=SHP.getunits()%></td>
				</tr>
				<%}} %>
				
			
				
		</table>
	</div>
</div>
  

</table>
</div>
</div>


 

<%-- <%}else{
	 response.sendRedirect("index.jsp");
} %> --%>
</body>
</html>