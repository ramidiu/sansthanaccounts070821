<%@page import="java.util.Iterator"%>
<%@page import="com.accounts.saisansthan.vendorBalances"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Major Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
</head>

<body>
	<%if(session.getAttribute("adminId")!=null){ 
		customerpurchasesListing CP_L=new customerpurchasesListing();
		majorheadListing MH_L=new majorheadListing();
		vendorsListing VNDR=new vendorsListing();
		bankdetailsListing BNKL=new bankdetailsListing();
		bankcalculations BNKCAL=new bankcalculations();
		vendorBalances VNDBALL=new vendorBalances();
		Object key =null;
		Object value=null;
		Iterator VendorPayments=null;
		Calendar c1 = Calendar.getInstance(); 
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat df=new DecimalFormat("#.##");
		String fromdate="";
		String onlyfromdate="";
		String todate="";
		double bankopeningbal=0.0;
		double bankopeningbalcredit=0.0;
		String onlytodate="";
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		todate=(dateFormat2.format(c1.getTime())).toString();
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromdate=(dateFormat2.format(c1.getTime())).toString();
		if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
									 fromdate=request.getParameter("fromDate");
								}
								if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
									todate=request.getParameter("toDate");
								}
		headofaccountsListing HOA_L=new headofaccountsListing();
	List headaclist=HOA_L.getheadofaccounts();
	String hoaid="1";
	if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
		hoaid=request.getParameter("head_account_id");
	}
	
	%>
	<div>
			<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form  name="departmentsearch" id="departmentsearch" method="post">
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>From Date</td>
<td>To Date</td>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
</tr>
				<tr>
				<td> <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
				<td><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
				<input type="hidden" name="page" value="balanceSheet_new"/>
				 </td>
<td colspan="2"><select name="head_account_id" id="head_account_id" onChange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
<option value="SasthanDev" >Sansthan Development</option>
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><input type="submit" value="SEARCH" class="click" /></td>
</tr></table> </form>
				<% String typeofbanks[]={"bank","cashpaid"};	
				String headgroup="";
				double total=0.0;
				double credit=0.0;
				double debit=0.0;
				productexpensesListing PEXP_L=new productexpensesListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				if(request.getParameter("head_account_id")!=null){
					hoaid=request.getParameter("head_account_id");
					if(hoaid.equals("SasthanDev")){
						headgroup="2";
						hoaid="4";
					} else if(hoaid.equals("4")){
						headgroup="1";
					}
				}
				onlyfromdate=fromdate;
				fromdate=fromdate+" 00:00:00";
				onlytodate=todate;
				todate=todate+" 23:59:59";
				List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid, "Assets",headgroup);
				List banklist=BNKL.getBanksBasedOnHOA(hoaid);
				List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid,"Liabilities",headgroup);
				HashMap VendorPaymentPedning=VNDBALL.getVendorBalances(hoaid, fromdate, todate);
				%>
				
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	<div class="printable" id="tblExport">
	
	<div class="total-report">


					<div style="float:left; width:1270px; margin:0 0px 10px 0">
					
					
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise" >
<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hoaid) %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Balance Sheet Report</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date  <%=onlyfromdate %> TO <%=onlytodate %> </td></tr>
<tr>
<td  class="bg-new" style="font-weight: bold;color: teal;">Liabilites</td>
<td  class="bg-new" style="font-weight: bold;color: teal;">Assets</td>
</tr>
	<tr>

	<td >	<table id="bord_ful" cellpadding="0" cellspacing="0"  width="60%" >
			<% if(expendmajhdlist.size()>0){ 
								for(int i=0;i<expendmajhdlist.size();i++){
									MH=(majorhead)expendmajhdlist.get(i);
									if(!PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"").equals("00")){
								%>	
	<tr >
<td style="font-weight: bold;"   align="left">
<a href="adminPannel.jsp?page=minorheadLedgerReport&majrhdid=<%=MH.getmajor_head_id()%>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Balance Sheet"><%=MH.getname() %></a></td>
									<td style="font-weight: bold;" colspan="0" width="25%" align="left"><%=PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"") %></td>
								<%debit=debit+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,""));%>

</tr>
<%}}}
if(VendorPaymentPedning.size()>0){%>
<tr >
<td style="font-weight: bold;"   align="left" colspan="2">
	<span style="color: #97A21D;">SUNDRY CREDITORS</span>
</td>
</tr><%
	VendorPayments=VendorPaymentPedning.keySet().iterator();	
	while(VendorPayments.hasNext()){
		  key   = VendorPayments.next();
	   	 value = VendorPaymentPedning.get(key);
	   	 if(Double.parseDouble(value.toString())>0){
		%>	
<tr >
<td style="font-weight: bold;"   align="left">
<%if(!key.toString().equals("")){ %>
<%=VNDR.getMvendorsAgenciesName(key.toString()) %>
<%} else{%>
Bill with out Vendor
<% 
}%>
</td>
				<td style="font-weight: bold;" colspan="0" width="25%" align="left"><%=df.format(Double.parseDouble(value.toString())) %></td>
			<%debit=debit+Double.parseDouble(value.toString());%>

</tr>
<%}
	}
}
			
			%>
	</table></td>
	
	<!-- Assests -->
		<td>
	<table width="66%" border="0" id="bord_ful">
			<% if(incomemajhdlist.size()>0){ 
								for(int i=0;i<incomemajhdlist.size();i++){
									MH=(majorhead)incomemajhdlist.get(i);
								%>	
	<tr >
<td style="font-weight: bold;"   align="left"><a href="adminPannel.jsp?page=minorheadLedgerReport&majrhdid=<%=MH.getmajor_head_id()%>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Balance Sheet"><%=MH.getname() %></a></td>
								<% if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid).equals("00")){ %>
								<td style="font-weight: bold;" align="left"><%=CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid) %>		</td>
								<%credit=credit+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); } else{ %>
								<td style="font-weight: bold;"  align="left"><%=CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid) %>		</td>
								<%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); } %>

</tr>
<%}} if(banklist.size()>0){ 
								for(int i=0;i<banklist.size();i++){
									BNK=(bankdetails)banklist.get(i);
									for(int j=0;j<typeofbanks.length;j++){
															bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),fromdate,todate,typeofbanks[j]);
															if(bankopeningbal >0 || bankopeningbal<0){
								%>	
	<tr >
<td style="font-weight: bold;"   align="left"><%=BNK.getbank_name() %> <%=typeofbanks[j]%></td>
								<td style="font-weight: bold;" align="left"><%=df.format(bankopeningbal) %>		</td>
								
</tr>
<%
bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
bankopeningbal=0.0;
}}} }
credit=credit+bankopeningbalcredit;
%>
	</table>
	</td>
</tr>
<%
System.out.println("credit==>"+credit);
System.out.println("debit==>"+debit);
%>
<tr>
<td style="font-weight: bold;" colspan="0" width="25%" align="left"><%if(credit>debit){%>
	<span style="color: #97A21D;">Excess of Expenditure over Incomes</span>:  <%=df.format((credit-debit))%>
	<%debit=credit;
} else{	%>
-
<%} %></td>

<td style="font-weight: bold;" colspan="0" width="25%" align="left">
<%
if(credit<debit){	%>
	<span style="color: #97A21D;">Excess of Incomes over Expenditure</span>: <%=df.format((debit-credit))%>
	<%	credit=debit;
} else{	%>
-
<%} %>
</td>

</tr>

<tr>

<td width="50%" colspan="0">
<div style="width:100%"  id="bord_ful">
<table width="100%">
<tr>
								<!-- <td style="border: 0px;">
								<div style="font-weight: bold; width: 70%; display: inline-block;"   align="left" >
							&nbsp;
								</div>
								</td> -->
								<td>
								<div style="width: 28%;display: inline-block;"   align="center" >
					Total&nbsp;:&nbsp;<span style=" font-weight: bold; color: #65230d;">Rs.<%=df.format(credit) %></span>
								</div>
								</td>
								</tr>	
	
	</table>
	</div></td>
	<!--  -->
	<td width="50%" colspan="0">
<div style="width:100%"  id="bord_ful">
<table width="100%">
<tr>
								<!-- <td style="border: 0px;">
								<div style="font-weight: bold; width: 70%; display: inline-block;"   align="left" >
							&nbsp;
								</div>
								</td> -->
								<td>
								<div style="width: 28%;display: inline-block;     "   align="center" >
					Total&nbsp;:&nbsp;<span style=" font-weight: bold; color: #65230d;">Rs.<%=df.format(debit) %></span>
								</div>
								</td>
								</tr>	
	
	</table>
	</div></td>
	</tr>
<%-- <tr>
<td align="right" >Total <span style="text-align:right !important;margin-right:80px;margin-right: 50px;
display: block;">&#8377; <%=df.format(credit) %></span>




</td>
<td style="">Total  <span style="float:right !important;margin-right:23% !important;"> &#8377; <%=df.format(debit) %></span></td>

</tr> --%>
</table></div>
					
					</div>
			</div>
				
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>