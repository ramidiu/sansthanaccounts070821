<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="BNK" class="beans.bankdetailsService">
<%
if(session.getAttribute("superadminId")!=null){
String bank_name=request.getParameter("bank_name");
String bank_branch=request.getParameter("bank_branch");
String account_holder_name=request.getParameter("account_holder_name");
String account_number=request.getParameter("account_number");
String ifsc_code=request.getParameter("ifsc_code");
String total_amount="0";
String headAccountId=request.getParameter("headAccountId");
String createdBy=session.getAttribute("adminId").toString();
%>
<jsp:setProperty name="BNK" property="bank_name" value="<%=bank_name%>"/>
<jsp:setProperty name="BNK" property="bank_branch" value="<%=bank_branch%>"/>
<jsp:setProperty name="BNK" property="account_holder_name" value="<%=account_holder_name%>"/>
<jsp:setProperty name="BNK" property="account_number" value="<%=account_number%>"/>
<jsp:setProperty name="BNK" property="ifsc_code" value="<%=ifsc_code%>"/>
<jsp:setProperty name="BNK" property="total_amount" value="<%=total_amount%>"/>
<jsp:setProperty name="BNK" property="headAccountId" value="<%=headAccountId%>"/>
<jsp:setProperty name="BNK" property="createdBy" value="<%=createdBy%>"/>
<%BNK.insert();%><%=BNK.geterror()%>
<script type="text/javascript">
window.parent.location="adminPannel.jsp?page=banks";
</script>
<%
}else{
	response.sendRedirect("index.jsp");
}
%>
</jsp:useBean>