<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="beans.payments"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="beans.purchaseorder"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.indent"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Total sale report</title>
<jsp:useBean id="INDT" class="beans.indent"></jsp:useBean>
<jsp:useBean id="IAP" class="beans.indentapprovals"></jsp:useBean>
<jsp:useBean id="PUO" class="beans.purchaseorder"></jsp:useBean>
<jsp:useBean id="PDEXP" class="beans.productexpenses"></jsp:useBean>
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<jsp:useBean id="GOD" class="beans.godwanstock"></jsp:useBean>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<style>
#yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%; margin:0 2%;
    background:#d0e3fb;
}

</style>
<script>
$(document).ready(function () {  
  var top = $('#yourID').offset().top - parseFloat($('#yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('#yourID').addClass('fixed');
	        $('#yourID1').addClass('fixed1');
			      $('#yourID2').addClass('fixed2');
				        $('#yourID3').addClass('fixed3');
						      $('#yourID4').addClass('fixed4');
    } else {
      // otherwise remove it
      $('#yourID').removeClass('fixed');
	        $('#yourID1').removeClass('fixed1');
				        $('#yourID2').removeClass('fixed2');
$('#yourID3').removeClass('fixed3');
$('#yourID4').removeClass('fixed4');
    }
  });
});
</script>
</head>

<body>
<div class="vendor-page">
<div class="vendor-list">
<form action="adminPannel.jsp" method="get">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>Head Account</td>
</tr>
<tr>
<td>
<select name="hoid" id="hoid" required>
<option value="">-Select HOA-</option>
<option value="3" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("3")){ %> selected="selected" <%} %>>Pooja stores</option>
<option value="4" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("4")){ %> selected="selected" <%} %>>Sansthan</option>
<option value="1" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("1")){ %> selected="selected" <%} %>>CHARITY</option>
<option value="5" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("5")){ %> selected="selected" <%} %>>SHRI SAINIVAS</option>
</select>
<input type="submit" value="Search" />
<input type="hidden" name="page" id="page" value="performance-detail-reportwithoutIndent" />
</td>
</tr>
</table>

</form>
<%
String hod="";
if(request.getParameter("hoid")!=null){
	hod=request.getParameter("hoid").toString();
}
mainClasses.indentapprovalsListing APPR_L=new mainClasses.indentapprovalsListing();
employeesListing EMP_L=new employeesListing();
superadminListing SUP_L=new superadminListing();
mainClasses.godwanstockListing GOSL=new mainClasses.godwanstockListing();
vendorsListing VENL=new vendorsListing();
productexpensesListing PRDE_L=new productexpensesListing();
paymentsListing PAYM_L=new paymentsListing();
SimpleDateFormat DBDate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat userDate=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
DecimalFormat DF=new DecimalFormat("0.00");
List POCOUL=null;
List MSEL=null;
List PAYMETL=null;
List PRDEXPL=null;
List APP_Det=null;
int payrow=0;
String payrows="";
String payrowA[]=null;
int billrow=0;
int masterrow=0;
int porow=0;
int maxno=1;
int invno=0;
mainClasses.purchaseorderListing POL=new mainClasses.purchaseorderListing();
if(!hod.equals("") ){
	MSEL=GOSL.getgodwanstockGroupByInvoiceIDWithoutIndent(hod);
} 
%>
<div style="text-align: center;">Performance Report With Out Indent</div>
<div class="icons">
					<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
					</span>
				</div>
<div class="clear"></div>
<div id="tblExport">
<div class="printable">
<div class="total-report">
<table width="95%" cellpadding="0" cellspacing="0" class="indent-wise" style="background:#f3f8fe;" id="tblExport">
<tr>
<td id="yourID" style="padding:0px; border:none; background:#d0e3fb; color:#000; font-size:13px;" colspan="20">
<table cellpadding="0" cellspacing="0" style="padding:0px; margin:0px; border:none;" width="100%">
<tr>
<td colspan="1" class="borderleftcolr">Transation</br>No</td>
<td colspan="5" class="borderleftcolr">MSE</td>
<td colspan="5" class="borderleftcolr">Bills</td>
<td colspan="3" class="borderleftcolr">Payment</td>
<td colspan="5" class="borderleftcolr">Method Of Transaction</td>
<td colspan="1" style="width:40px; font-size:13px;">Prints</td>
</tr>
<tr>
<td style="border:none; text-align:left">Unique No</td>
<td class="borderleftcolr;" style="border:none; text-align:left">MSE No</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">EnterBy</td>
<td style="border:none; text-align:left">ApprovedBy</td>
<td style="border:none; text-align:left">ApprovedBy</td>
<td class="borderleftcolr" style="border:none; text-align:left">Bill No</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">AMOUNT</td>

<!-- <td style="border:none; text-align:left">BillNo</td> -->
<td style="border:none; text-align:left">Accountant</td>
<td style="border:none; text-align:left">Manager</td>
<td class="borderleftcolr" style="border:none; text-align:left">P.O.NO</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">ApprovedBy</td>
<td class="borderleftcolr" style="border:none; text-align:left">VCR.NO</td>
<td style="border:none; text-align:left">Date</td>
<td style="border:none; text-align:left">PaymentType</td>
<td style="border:none; text-align:left">ChequeMode</td>
<td style="border:none; text-align:left">Amount</td>
<td class="borderleftcolr" style="border:none; text-align:left">Pass Order/</br>Voucher</td>
</tr></table>


<tr>
<%if(MSEL!=null && MSEL.size()>0){
for(int k=0;k<MSEL.size();k++){ 
GOD=(godwanstock)MSEL.get(k);
PRDEXPL=PRDE_L.getproductexpensesBasedInvoice(GOD.getextra1());
if(GOD.getExtra9() !=null && !GOD.getExtra9().equals("") && !GOD.getExtra9().equals("null")){
	billrow=PRDE_L.getproductexpensesBasedUniqueIdSize(GOD.getExtra9());
	payrows=PAYM_L.getMpaymentsBasedOnUniqueIdSize(GOD.getExtra9());
	payrowA=payrows.split(" ");
	payrow=Integer.parseInt(payrowA[0]);
	payrows=payrowA[1];
} else {
	billrow=PRDE_L.getproductexpensesBasedWithoutIndentSize(GOD.getextra1());
	payrows=PAYM_L.getMpaymentsBasedOnExpIDSize(GOD.getextra1());
	payrowA=payrows.split(" ");
	payrow=Integer.parseInt(payrowA[0]);
	payrows=payrowA[1];
}
APP_Det=APPR_L.getIndentDetails(GOD.getextra1());
%>
	<%if(GOD.getExtra9()!=null && !GOD.getExtra9().equals("")){ %>
	<td rowspan="<%=maxno %>" style="width:50px;">
	<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=GOD.getExtra9()%>')"><span><%=GOD.getExtra9() %></span></div></td>
	<%} else { %>
	<td rowspan="<%=maxno %>" style="width:50px;"><div style="font-weight: bold;color: red;cursor: pointer;">-No Unique-</div></td>
	<%} %>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" class="borderleftcolr" style="width:50px;"><a href="#" target="_blank" onclick="openMyModal('invoiceDetails.jsp?invID=<%=GOD.getextra1() %>&report=mse'); return false;"><%=GOD.getextra1() %></a></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=userDate.format(DBDate.parse(GOD.getdate())) %></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=EMP_L.getMemployeesName(GOD.getemp_id())  %></td>
<td rowspan="<%=PRDE_L.getproductexpensesBasedInvoiceSize(GOD.getextra1()) %>" style="width:50px;"><%=EMP_L.getMemployeesName(GOD.getCheckedby())  %></td>
<td  rowspan="1" style="width:50px;"><%if(APP_Det.size()>0){
	for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);%><%=SUP_L.getSuperadminname(IAP.getadmin_id())%><br>(<%=CDF.format(SDF.parse(IAP.getapproved_date())) %> <%=time.format(SDF.parse(IAP.getapproved_date())) %>)</br></br><%}} %></td>
<%if(PRDEXPL.size()>0){
for(int l=0;l<PRDEXPL.size();l++){ 
	PDEXP=(productexpenses)PRDEXPL.get(l);
	PAYMETL=PAYM_L.getMpaymentsBasedOnExpID(PDEXP.getexpinv_id());

if(l>0){%>
<tr>
<%} %>
<td  rowspan="1" class="borderleftcolr" style="width:50px;"><%=PDEXP.getexpinv_id() %></td>
<td  rowspan="1" style="width:50px;"><%=userDate.format(DBDate.parse(PDEXP.getentry_date())) %></td>
<td  rowspan="1" style="width:50px;"><%=DF.format(Double.parseDouble(PDEXP.getamount())) %></td>

<td  rowspan="1" style="width:50px;"><%=EMP_L.getMemployeesName(PDEXP.getEnter_by())  %></td>
<td  rowspan="1" style="width:50px;"><%=EMP_L.getMemployeesName(PDEXP.getemp_id())  %></td>
<%
List PayApprL=null;
if(PAYMETL.size()>0){
	for(int m=0;m<PAYMETL.size();m++){
		PAY=(payments)PAYMETL.get(m);
		APP_Det=APPR_L.getIndentDetails(PDEXP.getexpinv_id());
		PayApprL=PAYM_L.getSuccesfulPaymentsBasedOnExpID(PDEXP.getexpinv_id());
	%>
<td class="borderleftcolr" style="width:50px;"><%=PAY.getpaymentId() %></td>
<td style="width:50px;"><%=userDate.format(DBDate.parse(PAY.getdate())) %></td>
<td style="width:50px;"><%if(APP_Det.size()>0){
	for(int ap=0;ap<APP_Det.size();ap++){	IAP=(beans.indentapprovals)APP_Det.get(ap);%><%=SUP_L.getSuperadminname(IAP.getadmin_id())%></br><br>(<%=CDF.format(SDF.parse(IAP.getapproved_date())) %> <%=time.format(SDF.parse(IAP.getapproved_date())) %>)</br><%}} %></td>
<%if(m>0){ %>
<tr>
<% }%>
<%if(PayApprL.size()>0){
	for(int s=0;s<PayApprL.size();s++){
		PAY=(payments)PAYMETL.get(s);
	%>
<td class="borderleftcolr" style="width:50px;"><%=PAY.getreference() %></td>
<td style="width:50px;"><%=userDate.format(DBDate.parse(PAY.getdate())) %></td>
<td style="width:50px;"><%=PAY.getpaymentType() %></td>
<td style="width:50px;"><%=PAY.getextra2() %></td>
<td style="width:50px;"><%=DF.format(Double.parseDouble(PAY.getamount())) %></td>
<td  class="borderleftcolr" style="width:50px;"><a href="adminPannel.jsp?page=PassOrder&payId=<%=PAY.getpaymentId()%>" target="blank">PASSORDER PRINT</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="adminPannel.jsp?page=voucher&payId=<%=PAY.getpaymentId()%>" target="blank">VOUCHER PRINT</a></td>
<% if(s==0){%></tr><% } else {%></tr><% }}} else{%>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<%} }} else{%>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<%}}} else{%>
<td  rowspan="1" class="borderleftcolr" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>
<td  rowspan="1" style="width:50px;">-</td>

<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td style="width:50px;">-</td>
<td class="borderleftcolr" style="width:50px;">-</td>
<%}%></tr><% } }else{ %>
<tr>
<td colspan="20"> No Bills</td>
</tr>
<%} %>
<tr>
<td colspan="20" class="bordercolr"></td>
</tr>
</table>
</div>
</div>
</div>
</body>
</html>