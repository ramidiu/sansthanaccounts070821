<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="helperClasses.SaiMonths"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="mainClasses.employeeDetailsListing,beans.employeeDetails,java.util.List,java.util.Iterator,beans.employeeSalary,beans.employeeSalaryService"%>
   <%@ page import="mainClasses.employeeSalarySlipListing,mainClasses.employeeAttendce,mainClasses.stafftimingsListing,beans.stafftimings"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>


<style>th{border: 1px solid black;
    color: white;
    padding: 6px;
    background: rgb(139, 66, 27);}


 
.bord td{border: 1px solid #000; height: 25px;
    padding: 10px 0px 0px 0px;}
.bord input{border: none;}
</style>
<script type="text/javascript">
$(function() {
	$("#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		
		});
	});
function salarypay(id){
	var deductions=0;
	var allwoance=0;
	var totalamount=0;
	var payamount=0;
	deductions=Number($("#deductions"+id).val());
	 allwoance=Number($("#allowances"+id).val());
	 totalamount=Number($("#totalsalary_pay"+id).val());
	 payamount=Number(totalamount)+Number(allwoance)-Number(deductions);
	 $("#salary_pay"+id).val(payamount);
	
}
</script>
<script>
function myFunction() 
{
	 	if(document.frm.month.value=="")
	    {
	       alert("please enter valid  month and year ");
	       document.frm.month.focus();
	       return false;
	    }
	 	
	 	else
		 {
    		document.getElementById("id1").submit();
		 }
}
</script>

<script>
function form2_submit() 
{
	
	var n=document.querySelectorAll('input[type="checkbox"]:checked').length;
		if( n > 0 )
		{ 
		
			return true;
		}
		else
		{ 
			alert( 'please check atleast one list' );
			return false;
		}	
}
</script>

<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>

</head>
<body><center>

<form action="adminPannel.jsp"  name="frm" method="get">

	
	<%
	    DecimalFormat decimalFormat=new DecimalFormat("0.00");
		DateFormat monthFormat=new SimpleDateFormat("MMMM");
		DateFormat yearFormat=new SimpleDateFormat("yyyy");
		  stafftimingsListing stlisting=new stafftimingsListing(); 
		 List stlist=stlisting.getAllDistinctStaffType();   
		 Iterator stitr=stlist.iterator();
	
			Date now=new Date();
			Calendar today=Calendar.getInstance();
			today.add(Calendar.MONTH, -1);
			String thisMonth=monthFormat.format(today.getTime());
			/* String thisYear=yearFormat.format(now); */
			String thisYear=yearFormat.format(today.getTime());
		%>
  		Month: 
  		 <select name="month">
  			<%List<String> listOfAllMonths= SaiMonths.getAllMonth(); 
  				//for(int i=0;i<listOfAllMonths.size();i++){
  			%>
  			 	<%-- <option value="<%=listOfAllMonths.get(i)%>" 
  				<%if(request.getParameter("month")!=null && request.getParameter("month").equalsIgnoreCase(listOfAllMonths.get(i)) ){ %>selected<% }
  					else{
  						if(thisMonth.equalsIgnoreCase(listOfAllMonths.get(i))){
  							%>selected<%
  						}else{
  							%>hidden<%
  						}
  					} %> >
  				<%=listOfAllMonths.get(i)%>
  				</option>  --%>
  				<%//} %>
  			<option value="<%=thisMonth%>"><%=thisMonth%></option>
  		</select>
  		Year: 
  		<select name="year">
  			<option value="2015">2015</option>
  			<option value="<%=thisYear%>"><%=thisYear%></option>
  		</select> 
  	 <!--  <input type="text" id="fromDate" name="month"/>  -->
	
	<input type="hidden" name="page" value="bank_lr"  >
	<input type="submit" id="id1" value="ok" onclick=" return myFunction()">
</form>	
<%
if((request.getParameter("month")!=null))
{
	String month=request.getParameter("month");
	String year=request.getParameter("year");
	String monthAndYear=month+"@"+year;
	//System.out.println("month======>"+request.getParameter("month"));
	String employee_id="";
	String depttype = "";
	
	int checkBoxCLick=0;
	employeeDetailsListing listing=new employeeDetailsListing();
	employeeSalarySlipListing salarylist = new employeeSalarySlipListing();%>
	
	
	<div class="vendor-page">
<div class="vendor-list">
	<div class="icons">
<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>
<div class="clear"></div>
<div class="total-report">
<!--  <form action="employeeSalarySlipSubmit.jsp" name="frm2" onSubmit="return form2_submit()" method="post">  -->
	<%-- <table align="left">
	<tr><td></td>	
	<td><h6><%=department%></h6></td></tr>
	</table>
	<br> --%>
	<% double finaltotal = 0.0; %>
	<div id="tblExport">
	<table class="bord"  cellpadding="1" cellspacing="1" width="100%">
	<tr>
				<td colspan="5" style="text-align:center; border-bottom: none;"><strong style="font-weight: bold; ">SHRI SHIRDI SAIBABA SANSTHAN TRUST ,DILSUKHNAGAR,HYDERABAD</strong></td>
			</tr> 
	<tr>
				<td colspan="5" style="text-align:center;border-top: none;"><strong style="font-weight: bold; ">STATEMENT  OF STAFF SALARIES FOR THE MONTH OF <%=month%>- <%=year%></strong></td>
			</tr> 
			
	<% 
	while(stitr.hasNext())
	{
		stafftimings stimings=(stafftimings)stitr.next();
		depttype = stimings.getstaff_type();
	List salarylists = salarylist.selectAllSalarySlipBasedOnMonth(monthAndYear);
	double grandtotal = 0.0;
	
	/* List list=listing.selectAllEmployeeDetailsBasedUponDepartment(department); */
	if(salarylists!=null){
	Iterator itr=salarylists.iterator();%>
	<%
	int count = 0;
	Iterator itrs=salarylists.iterator();
		while(itrs.hasNext())
		{
			employeeSalary es=(employeeSalary)itrs.next();	
		    employee_id=es.getEmployee_id();
		    String staftype = stlisting.getStafCategoryName(employee_id);
		    if(staftype.equals(depttype))
		    {
		    	count += 1;
		    }
		    
		}
	%>
	
	
			<% if(count > 0)
		    {System.out.println("Count"+count);%>
		    <table class="bord"  cellpadding="1" cellspacing="1" width="100%">
		    <br/>
		    
		    <tr>
				<td colspan=5 style="text-align:left;"><strong style="font-weight: bold; "><%=depttype%></strong></td>
			</tr> 
	<tr><th>s no:</th><th>Emp Id:</th><th>Emp Name:</th><th>Bank Account No.</th><th>Amount</th></tr>
	  <% }%>
	<%
	while(itr.hasNext())
	{		
		employeeSalary es=(employeeSalary)itr.next();	
	    employee_id=es.getEmployee_id();
	    String staftype = stlisting.getStafCategoryName(employee_id);
	    if(staftype.equals(depttype))
	    {
	    	//System.out.println(es.getOther_allowances());
	    //pass the employeeId and month to employeeAttendce.java  to get all CL,OT...etc
			%>
			
			<tr>
				<td align="center"><%=++checkBoxCLick%></td>
				<td align="center"><%=employee_id%></td>
				<!--  getting employee name from stafftiming table of saisansthan schema ..,just to display no need to store in DB -->
				<%
					stafftimings stft=stlisting.getStaffDetails(es.getEmployee_id());
				double total = Double.parseDouble(es.getSalary_pay());
				grandtotal += total;
				%>
				<td align="center"><%=stft.getstaff_name() %></td>
				
				<%employeeDetails ed = listing.selectSingleEmployeeDetailsBasedUponEmployeeId(employee_id); %>
				<td align="center"><%=ed.getExtra5()%></td>	
				<td align="center"><%=decimalFormat.format(Double.parseDouble(es.getSalary_pay()))%></td>	
				<!-- <td><input type="submit" value="ok"></td> -->
			</tr>
<%}%> 

<%}%>
<% finaltotal += grandtotal; %>

			<% if(count > 0)
		    {System.out.println("Count"+count);%>
<tr>
	<td colspan=4 align="center"><strong>Total : </strong></td><td colspan=1 align="center"><strong><%=decimalFormat.format(grandtotal)%></strong></td>
</tr><% }%>
 	<%}//while(itr.hasNext()))%> 
 	
	
	</table><!-- </form>  -->
<%}%>

<tr>
<td>
<table class="bord"  cellpadding="1" cellspacing="1" width="100%" style="margin-top: 10px;">
	<tr>
		<td  style="text-align:center; color:#f00; border:1px solid #000;width: 87%;"><strong>Grand Total : </strong></td>
		<td align="center" style="width:13%;"><strong><%=decimalFormat.format(finaltotal)%></strong></td>
</tr>
</table>
</td>
</tr>
</table>
</table><%  }
%>
</div>
</div>
</div>
</div>



</center></body>
</html>