<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
	<%String headOfAccountId=request.getParameter("id");
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	List HOA_List=HOA_CL.getMheadofaccounts(headOfAccountId);

	if(HOA_List.size()>0){
		HOA=(beans.headofaccounts)HOA_List.get(0);%>
<div class="vendor-page">
<div class="vendor-box">
<div class="name-title"><%=HOA.getname()%> <span></span></div>
<div class="click floatleft"><a href="./editHeadOfAccount.jsp?id=<%=HOA.gethead_account_id()%>" target="_blank" onclick="openMyModal('editHeadOfAccount.jsp?id=<%=HOA.gethead_account_id()%>'); return false;">Edit</a></div>
<div style="clear:both;"></div>
<div class="details-list">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="2"><span>Phone:</span><%=HOA.getphone()%></td>
</tr>
<tr>
<td colspan="2"><span>Address:</span><%=HOA.getaddress()%></td>
</tr>
<tr>
<td colspan="2"><span>Description:</span><%=HOA.getdescription()%></td>
</tr>
</table>

</div>
<div class="details-amount">
 <ul>
<li class="colr">&#8377;0.00<br>
 <span>OPEN</span></li>
  <li class="colr1">&#8377;0.00<br>
 <span>OVERDUE</span></li>
</ul>
</div>
</div>

<div class="vendor-list">
<div class="trans">Transactions</div>
<div class="clear"></div>
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0" id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%"><input type="checkbox"></td>
<td class="bg" width="8%">DATE</td>
<td class="bg" width="20%">TYPE</td>
<td class="bg" width="5%">NO.</td>
<td class="bg" width="15%">PAYEE</td>
<td class="bg" width="15%">CATEGORY</td>
<td class="bg" width="15%">TOTAL</td>
<td class="bg" width="10%">ACTION</td>
</tr>
<tr>
<td><input type="checkbox"></td>
<td>05/06/2014</td>
<td>Expenses</td>
<td></td>
<td>mr harish kumar yadav</td>
<td><select>
<option>Create expenses</option>
</select></td>
<td>5000</td>
<td><div class="print-btn">Print Check</div></td>
</tr>


</table>

</div>
</div>




</div>
<%}else{%>
<script type="text/javascript">
window.location="adminPannel.jsp?page=mastersTree";
</script>
<%}%>

