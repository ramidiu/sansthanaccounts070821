<%@page import="java.util.Calendar"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.indentapprovals"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.billimagesListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.util.List"%>
 <link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
$(function(){
	$('#entryDate').html($('#mseDate').val());
});
function majorheadsearch(j){
	var data1=$('#major_head_id'+j).val();
	 var hoid=$("#head_account_id"+j).val();
	// alert('hoid....'+hoid);
	  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#major_head_id'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
}
</script>

<script>
function minorheadsearch(j){
		  var data1=$('#minor_head_id'+j).val();
		  var major=$('#major_head_id'+j).val();
		  var hoid=$("#head_account_id"+j).val();
		  
		  $.post('searchMinor.jsp',{q:data1,q1:major},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			$('#minor_head_id'+j).autocomplete({
				source: availableTags,
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				}
			}); 		
	  });
}


</script>
<script>
function subheadsearch(j){
	
		  var data1=$("#sub_head_id"+j).val();
		  var minor=$("#minor_head_id"+j).val();
		  var major=$("#major_head_id"+j).val();
		  var hoid=$("#head_account_id"+j).val();
		  //alert('minor.....'+minor);
		  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID:hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			 $( "#sub_head_id"+j ).autocomplete({source: availableTags}); 
				});
}

</script>
<script>
function deleteProduct(id){
	var retVal = confirm("Are you sure you want to delete this product ?");
	if( retVal == true ){
	$.ajax({
		 type:'post',
		 url: 'productDelete.jsp',
		 data: {
		 indentid : id
		 },
		 success: function(response) {
			$('#refresh').load(window.location+''+' #refresh');
		 }});
	}
}
function vendorsearch(j){
	 var data1=$('#vendorId'+j).val();
	  var headID=$('#headAccountId').val();
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $('#vendorId'+j).autocomplete({source: availableTags}); 
			});
}
function validateChecked(){
	var $b = $('.invcheckbox');
if($b.filter(':checked').length==0){
	alert("please Select Atleast One Checkbox");
	return false;
}
$.blockUI({ css: { 
    border: 'none', 
    padding: '15px', 
    backgroundColor: '#000', 
    '-webkit-border-radius': '10px', 
    '-moz-border-radius': '10px', 
    opacity: .5, 
    color: '#fff' 
}});
}
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.invcheckbox').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.invcheckbox').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>
<script>
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	
			if(document.getElementById("price"+i).value!=""   ){
			//grossprice=Number((parseFloat(document.getElementById("price"+i).value)*parseFloat(document.getElementById("quantity"+i).value))+((parseFloat(document.getElementById("price"+i).value)*parseFloat(document.getElementById("quantity"+i).value)))*parseFloat(document.getElementById("vat"+i).value)/100).toFixed(2);
			//grossprice=Number((parseFloat(document.getElementById("price"+i).value)+parseFloat(document.getElementById("CGSTname"+i).value)+parseFloat(document.getElementById("SGSTname"+i).value)+parseFloat(document.getElementById("IGSTname"+i).value))+((parseFloat(document.getElementById("price"+i).value)+parseFloat(document.getElementById("CGSTname"+i).value)+parseFloat(document.getElementById("SGSTname"+i).value)+parseFloat(document.getElementById("IGSTname"+i).value)))*parseFloat(document.getElementById("vat"+i).value)/100).toFixed(2);
			grossprice=Number(((parseFloat(document.getElementById("price"+i).value)+parseFloat(document.getElementById("CGSTname"+i).value)+parseFloat(document.getElementById("SGSTname"+i).value)+parseFloat(document.getElementById("IGSTname"+i).value)).toFixed(2)*parseFloat(document.getElementById("QTY"+i).value))+(((parseFloat(document.getElementById("price"+i).value)+parseFloat(document.getElementById("CGSTname"+i).value)+parseFloat(document.getElementById("SGSTname"+i).value)+parseFloat(document.getElementById("IGSTname"+i).value)).toFixed(2)*parseFloat(document.getElementById("QTY"+i).value)))*parseFloat(document.getElementById("vat"+i).value)/100).toFixed(2);
			/* if(document.getElementById("discountAmt"+v+j).value!=""){
							discountprice=Number(grossprice)*(1-parseFloat(document.getElementById("discountAmt"+v+j).value)/100).toFixed(2);
			document.getElementById("purchaseRate"+v+j).value=(Number(document.getElementById("purchaseRate"+v+j).value)*(1-parseFloat(document.getElementById("discountAmt"+v+j).value)/100)).toFixed(2);
			grossprice=discountprice;
			} */
		document.getElementById("amount"+i).value=grossprice;
			}


}
</script>
<jsp:useBean id="INDA" class="beans.indentapprovals"></jsp:useBean>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
<jsp:useBean id="VND" class="beans.vendors"/>
<jsp:useBean id="PRDE" class="beans.productexpenses"/>
<jsp:useBean id="GDS" class="beans.godwanstock"/>
<jsp:useBean id="GDS1" class="beans.godwanstock"/>
<jsp:useBean id="EXP" class="beans.productexpenses"/>
	<%String INVId=request.getParameter("invid");
	String approvedBy1=request.getParameter("approvedBy1");
	String Producttype="";
	DecimalFormat formatter = new DecimalFormat("0.00");
	mainClasses.indentListing INDT_L = new indentListing();
	productexpensesListing PRDEXP_L=new productexpensesListing();
	mainClasses.bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	mainClasses.employeesListing EMP_L = new employeesListing();
	majorheadListing MJR_L = new majorheadListing();
	minorheadListing MINR_L = new minorheadListing();
	productsListing PRD_L = new productsListing();
	vendorsListing VND_L = new mainClasses.vendorsListing();
	List INDT_List=INDT_L.getMindentsByIndentinvoice_ids(INVId); 
	List BILAP=PRDEXP_L.getExpListBasedOnInvoiceID(INVId,session.getAttribute("role").toString());
	godwanstockListing GSK_L=new godwanstockListing();
	List BNK_List=null;
	if(BILAP.size()>0){
		PRDE=(productexpenses)BILAP.get(0);

		List VNDT_LIST=VND_L.getMvendors(PRDE.getvendorId());
		if(VNDT_LIST.size()>0){
		VND=(vendors)VNDT_LIST.get(0);}
		if(INDT_List.size()>0){
			INDT=(indent)INDT_List.get(0);		
		}
		Producttype=PRDE.gethead_account_id();
		BNK_List=BNK_CL.getMbankdetails(PRDE.getextra3()); 
		System.out.println("nklistsize]]]]]]]]]]]]]]]]]]]]]]]]"+BNK_List.size());
		if(BNK_List.size()>0){
			BNK=(beans.bankdetails)BNK_List.get(0);
			System.out.println("nklistsize]]]]=====]]]]]]]]]]]]]]]]]]]]"+BNK.getbank_id());
		}
		%>
<div class="vendor-page">
<div class="vendor-box"  style="padding:30px 0px 150px 0px;">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Payment Approvals</td></tr>
</table>
<div class="name-title"> <span><%=HOA_CL.getHeadofAccountName(PRDE.gethead_account_id())%></span></div>
<div style="clear:both;"></div>
<div class="details-list col-md-6">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="1"><span>Vendor Name :</span></td>
	<td>
		<% 
		if(!(VND.getfirstName().equals("")) && VND.getfirstName()!=null)
		{
			out.println(VND.gettitle());
			out.println(VND.getfirstName());
			out.println(VND.getlastName());
		}
		else
		{
			out.println("Not Available");
		}%>
	</td>
</tr>

<tr>
<td colspan="1"><span>Indent Raised By:</span></td><td>
	<%
	
	if (request.getParameter("extra7") != null && request.getParameter("extra7").equals("serviceBill"))	{
		out.println("SERVICE ENTRY");
	}
	else	{
		if(EMP_L.getMemployeesName(INDT.getextra2())==null || EMP_L.getMemployeesName(INDT.getextra2()).equals("") )
		{
			out.println(EMP_L.getMemployeesName(INDT.getemp_id()));
		}
		else
		{
			out.println(EMP_L.getMemployeesName(INDT.getextra2()));
			
		}
	}%>
</td>
</tr>
<tr>
<td colspan="1"><span>Indent Raised Date :</span></td>
<td>
	<%if(INDT.getdate()==null ||(INDT.getdate().equals("")))
	{
		out.println("Not Available");
	}
	else
	{
		out.println(INDT.getdate());
	}%>
</td>

</tr>
<tr>
<td colspan="1"><span>Payment Raised Date :</span></td><td><%if(PRDE.getentry_date()!=null){out.println(PRDE.getentry_date());}else{out.println("Not Available");}%></td>
</tr>
<tr>
	<td>
	<span>IndentInvoice Number::</span>
	</td>
	<td>
	<%
	if (request.getParameter("extra7") != null && request.getParameter("extra7").equals("serviceBill"))	{
		out.println("SERVICE ENTRY");
	}
	else	{
		if(INDT.getindentinvoice_id()!=null){
			out.println(INDT.getindentinvoice_id());
		}
		else{
			out.println("Not Available");
			}
	}%>
	</td>
</tr>
<tr>
<td><span>MSE Number::</span></td>
<td><%if(PRDE.getextra5() != null){out.println(PRDE.getextra5());}else{out.println("Not Available");}%></td>
</tr>
<tr>
<td><span>MSE Entry Date::</span></td>
<td><span id="entryDate"></span></td>
</tr>
 <tr>
<td>
<span>Indent Approved BY</span>
<%
	if(request.getParameter("indentapprBy") != null && request.getParameter("indentapprBy").equals(""))
	{
		String indapprovedBy=request.getParameter("indentapprBy");
		out.println(indapprovedBy);
	}
	else
	{
		out.println("Indent Not-Approved");
	}
%>
</td>
</tr> 
</table>
</div>

<div class="details-amount col-md-6">
<div class="name-title"><span><%=BNK.getbank_name()%></span></div>
 <ul>
<li class="colr"> 

<%
String bankid=request.getParameter("idd");
double notApprAmmount=0.0;
productexpensesListing PRO_EXPL=new productexpensesListing();
List EXP_L=PRO_EXPL.getExpensesListGroupByInvoice("Approved");
System.out.println("EXP_L////"+EXP_L.size());
if(EXP_L.size()>0){
	List APP_Det=null;
	for(int i=0; i < EXP_L.size(); i++ ){
		EXP=(productexpenses)EXP_L.get(i);
		if(EXP.gethead_account_id().equals(bankid)){
		System.out.println("amount -----"+EXP.getamount()+" = "+EXP.gethead_account_id());
		notApprAmmount=notApprAmmount+Double.parseDouble(EXP.getamount());
		}
	}
}

System.out.println("notApprAmmount//"+notApprAmmount);


String bankids="";
String cdType="";
System.out.println("banktrnid"+bankid);
if(bankid.equals("1")){
	bankids="BNK1029";
	cdType="debit";
}else if(bankid.equals("3")){
	bankids="BNK1030";
	cdType="debit";
}else{
	bankids="BNK1027";
	cdType="debit";
}

bankcalculations BNKCAL=new bankcalculations();
 bankbalanceListing BBAL_L=new mainClasses.bankbalanceListing();
 
 DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
 TimeZone tz = TimeZone.getTimeZone("IST");
 dateFormat2.setTimeZone(tz);
 
 Calendar c2 = Calendar.getInstance(); 
 String currentDateNew=(dateFormat2.format(c2.getTime())).toString();
 
 if(request.getParameter("todate") != null && !request.getParameter("todate").equals(""))
 {
	 currentDateNew = request.getParameter("todate");
 }
 
 String monthInString=currentDateNew.substring(5, 7);
 int month = Integer.parseInt(monthInString);
 String yearInString=currentDateNew.substring(0, 4);
 int year = Integer.parseInt(yearInString);
 
 String finStartYr2 = "";

 if(month >= 4)
 {
		finStartYr2 = Integer.toString(year);
 }
 else
 {
	 int finStartYrInInt = year - 1;
	 finStartYr2 = Integer.toString(finStartYrInInt);
 }
 
 String finStartAndEndYr = "";
 
 String tempInString = finStartYr2.substring(2, 4);
 int tempInInt = Integer.parseInt(tempInString)+1;
 finStartAndEndYr = finStartYr2+"-"+tempInInt;
 
 double bankopeningbal = 0.0;
	double pettycashOpeningbal = 0.0;
	
	 if(cdType.equals("debit"))
	 {
		 System.out.println("from working bank debit adj");
	 	bankopeningbal=BNKCAL.getBankOpeningBalance(bankids,finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","bank","");
		double finOpeningBalNew = BBAL_L.getBankOpeningBal(bankids,"bank",finStartAndEndYr);
		bankopeningbal += finOpeningBalNew;
		
		pettycashOpeningbal=BNKCAL.getBankOpeningBalance(bankids,finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","cashpaid","");
		double pettycashFinOpeningBal = BBAL_L.getBankOpeningBal(bankids,"cashpaid",finStartAndEndYr);
		pettycashOpeningbal += pettycashFinOpeningBal;
		System.out.println("from working bank debit"+bankopeningbal);
	 }	
	 else if(cdType.equals("credit"))
	 {
		 bankopeningbal=BNKCAL.getCreditBankOpeningBalance(bankids,finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","");
			double finOpeningBalNew = BBAL_L.getBankOpeningBal(bankids,"bank",finStartAndEndYr);
			bankopeningbal += finOpeningBalNew;
			System.out.println("from working bank credit"+bankopeningbal);
	 }
 %>



<%

if(!BNK.gettotal_amount().equals("")){ %>&#8377;<%=formatter.format(Double.parseDouble(BNK.gettotal_amount()))%><%}else{ %>&#8377;<%=formatter.format(bankopeningbal)%> <%} %>
 <%-- <%=NUM.convert(Integer.parseInt(sjf.format(Double.parseDouble(BNK.gettotal_amount())))) %><br> --%>


 <span>CLOSING BALANCE</span></li>
  <li class="colr1">&#8377;<%if(BNK_CL.getPettyCashAmount(PRDE.getextra3())!=null && !BNK_CL.getPettyCashAmount(PRDE.getextra3()).equals("")){%><%=formatter.format(Double.parseDouble(BNK_CL.getPettyCashAmount(PRDE.getextra3())))%><%}else{ %><%=formatter.format(pettycashOpeningbal)%><%} %>
 <span>PETTY CASH AMOUNT</span></li>
<li class="colr1">&#8377;<%=formatter.format(notApprAmmount)%>
<span>APPROVED AMOUNT</span></li>
<li class="colr1" style="font-size:15px;">

Approved By:<br>
<%
	if(request.getParameter("approvedBy").equals("") || request.getParameter("approvedBy")==null)
	{
		out.println("Not-Approved");
	}
	else
	{
		String approvedBy=request.getParameter("approvedBy");
		out.println(approvedBy);
	}
%>
</li>

<%if(session.getAttribute("role").equals("FS")){ %>
<li class="colr1" style="font-size:15px;color:red">
<br>Authorized By:<br>
<%

if(request.getParameter("approvedBy1").equals("") || request.getParameter("approvedBy1")==null)
{
	out.println("Not-Approved");
}
else
{
	out.println(approvedBy1);
}
%>
</li>
<%} %>
</ul>
</div>
</div>

<div class="vendor-list" >
<div class="trans" style="text-align: center;">Transactions</div>
<%if(PRDE.getExtra6()!=null && !PRDE.getExtra6().equals("")){%>
<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>')">Track Transaction : <span><%=PRDE.getExtra6() %></span></div><%}  %>
<div class="clear"></div>
<div class="icons">
<div style="margin-bottom: 20px;float: left;cursor: pointer;"><img src="../images/back-button (1).png" width="100" height="50" onclick="goBack()" title="print"/></div>
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<div id="refresh">
<form action="paymentsApprovalInsert.jsp" method="post" onsubmit="return validateChecked();">
<input type="hidden" name="headofaccount" id="headofaccount" value="<%=PRDE.gethead_account_id() %>"/>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="5%"><input type="checkbox" id="selecctall" >Check All</td>
<td class="bg" width="10%">VENDOR NAME</td>
<td class="bg" width="10%">MAJOR HEAD NAME</td>
<td class="bg" width="10%">MINOR HEAD NAME</td>
<td class="bg" width="10%">PRODUCT/SUB HEAD NAME</td>
<td class="bg" width="5%">QTY</td>
<td class="bg" width="7%">PRICE</td>

<!-- <td class="bg" width="5%">VAT(%)</td> -->
<td class="bg" width="5%">CGST(Rs)</td>
 <td class="bg" width="5%">SGST(Rs)</td>
<td class="bg" width="5%">IGST(Rs)</td> 
<td class="bg" width="8%">TOTAL AMOUNT</td>
<td class="bg" width="20%">NARRATION</td>
<!-- <td class="bg" width="20%">Balance Quantity</td> -->
</tr>
<%
banktransactionsListing BAK_L=new mainClasses.banktransactionsListing(); 
DecimalFormat df=new DecimalFormat("0.00");
indentapprovalsListing APPL=new indentapprovalsListing();
subheadListing SUBL=new subheadListing();
employeesListing EMP=new employeesListing();
billimagesListing BIMG=new billimagesListing();
double otherCharges=0.00;
double totAmt=0.00;
List INd_APL=APPL.getIndentApprovedByAdmin(INVId,session.getAttribute("superadminId").toString());
 if(INd_APL.size() > 0){
	INDA = (indentapprovals)INd_APL.get(0); 
 }
if(BILAP.size()>0){
	for(int i=0;i<BILAP.size();i++){
		PRDE=(productexpenses)BILAP.get(i);	
		GDS=(godwanstock)GSK_L.getgodwanstockByinvoiceId(PRDE.getextra5()).get(0);
		GDS1=(godwanstock)GSK_L.getInvoiceDetails(PRDE.getextra5()).get(i); //this line written by pradeep on 18/06/2016 because to get godwanstock id to update
		if(PRDE.getExtra7()!=null && !PRDE.getExtra7().equals("")){
			otherCharges=Double.parseDouble(PRDE.getExtra7());
		}
		totAmt=totAmt+Double.parseDouble(PRDE.getamount());
%>
<tr>
<td style="cursor: pointer;"><input type="hidden" name="count" id="check<%=i%>"  value="<%=i%>"/> 

<%-- <input type="checkbox" name="expindentId" id="expindentId" class="invcheckbox"  value="<%=PRDE.getexp_id()%>" <%if(session.getAttribute("role")!=null && session.getAttribute("role").equals("FS")){ %>disabled="disabled" checked="checked"<%} %>> --%>
<input type="checkbox" name="expindentId" id="expindentId" class="invcheckbox"  value="<%=PRDE.getexp_id()%>">
<input type="hidden" name="gsId" value="<%=GDS1.getgsId() %>" />
<input type="hidden" id="mseDate" value="<%=GDS.getDate() %>">
<input type="hidden" name="invId" value="<%=PRDE.getextra5()%>" />
<input type="hidden" name="quantity<%=GDS1.getgsId()%>" id="quantity<%=GDS1.getgsId()%>" value="<%=GDS1.getquantity()%>" />
<input type="hidden" name="head_account_id<%=i%>" id="head_account_id<%=i%>" value="<%=PRDE.gethead_account_id()%>" />
</td>
<td> <%if(!VND_L.getMvendorsAgenciesName(PRDE.getvendorId()).equals("")){ %> <%=PRDE.getvendorId()%> <%=VND_L.getMvendorsAgenciesName(PRDE.getvendorId()) %> <%}else{ %> SERVICE BILL<%} %> </td>
<%-- <td><input type="text" name="vendorId<%=i%>" id="vendorId<%=i%>"  value="<%=EXP.getvendorId()%> <%=VND_L.getMvendorsAgenciesName(EXP.getvendorId()) %>" onkeyup="vendorsearch('<%=i%>')"/></td> --%>
<td><input type="text" name="major_head_id<%=i%>" id="major_head_id<%=i%>" onkeyup="majorheadsearch('<%=i%>')" value="<%=PRDE.getmajor_head_id()%> <%=MJR_L.getMajorHeadNameByCat1(PRDE.getmajor_head_id(),PRDE.gethead_account_id()) %>" required placeholder="find a major head here" autocomplete="off"/></td>
<td><input type="text" name="minor_head_id<%=i%>" id="minor_head_id<%=i%>" onkeyup="minorheadsearch('<%=i%>')" value="<%=PRDE.getminorhead_id() %> <%=MINR_L.getMinorHeadNameByCat1(PRDE.getminorhead_id(),PRDE.gethead_account_id()) %>" required  placeholder="find a minor head here" autocomplete="off"/></td>
<%-- <td><%if(!MJR_L.getMajorHeadNameByCat1(PRDE.getmajor_head_id(),PRDE.gethead_account_id()).equals("")){%> <%=MJR_L.getMajorHeadNameByCat1(PRDE.getmajor_head_id(),PRDE.gethead_account_id()) %><%}else{ %> <%=SUBL.getMsubheadnameWithDepartment(PRDE.getsub_head_id(),PRDE.gethead_account_id()) %> <%} %></td>
<td><%if(!MINR_L.getMinorHeadNameByCat1(PRDE.getminorhead_id(),PRDE.gethead_account_id()).equals("")){%> <%=MINR_L.getMinorHeadNameByCat1(PRDE.getminorhead_id(),PRDE.gethead_account_id()) %><%}else{ %> <%=SUBL.getMsubheadnameWithDepartment(PRDE.getsub_head_id(),PRDE.gethead_account_id()) %> <%} %></td> --%>
<%-- <td><input type="text" name="sub_head_id<%=i%>" id="sub_head_id<%=i%>"  onkeyup="subheadsearch('<%=i%>')" value="<%if(!PRD_L.getProductsNameByCat1(PRDE.getextra2(),PRDE.gethead_account_id()).equals("")){%><%=PRDE.getextra2() %> <%=PRD_L.getProductsNameByCat1(PRDE.getextra2(),PRDE.gethead_account_id()) %><%}else{ %><%=PRDE.getsub_head_id() %> <%=SUBL.getMsubheadnameWithDepartment(PRDE.getsub_head_id(),PRDE.gethead_account_id()) %> <%} %>" required placeholder="find a sub head here" autocomplete="off"/></td> --%>
<td><input type="text" name="sub_head_id<%=i%>" id="sub_head_id<%=i%>"  onkeyup="subheadsearch('<%=i%>')" value="<%=PRDE.getsub_head_id() %> <%=SUBL.getMsubheadnameWithDepartment(PRDE.getsub_head_id(),PRDE.gethead_account_id()) %>" required placeholder="find a sub head here" autocomplete="off"/></td>

<td><input type="text" name="QTY<%=GDS1.getgsId()%>" id="QTY<%=GDS1.getgsId()%>"  value="<%=GDS1.getquantity()%>" style="width:35px" readonly/></td>
<td><input type="text" name="price<%=GDS1.getgsId()%>" id="price<%=GDS1.getgsId()%>"  value="<%=df.format( Double.parseDouble(GDS1.getpurchaseRate()))%>" style="width:120px" onkeyup="calculate('<%=GDS1.getgsId()%>')"/></td><%-- 
<td><input type="text" name="vat<%=GDS1.getgsId()%>" id="vat<%=GDS1.getgsId()%>"  value="<%=GDS1.getvat()%>" style="width:35px" readonly/></td> --%>

<input type="hidden" name="vat<%=GDS1.getgsId()%>" id="vat<%=GDS1.getgsId()%>"  value="0" style="width:35px" readonly/>

<td><input type="text" name="CGSTname<%=GDS1.getgsId()%>" id="CGSTname<%=GDS1.getgsId()%>"  value="<%=GDS1.getExtra18()%>" style="width:35px" readonly/></td>
<td><input type="text" name="SGSTname<%=GDS1.getgsId()%>" id="SGSTname<%=GDS1.getgsId()%>"  value="<%=GDS1.getExtra19()%>" style="width:35px" readonly/></td>
<td><input type="text" name="IGSTname<%=GDS1.getgsId()%>" id="IGSTname<%=GDS1.getgsId()%>"  value="<%=GDS1.getExtra20()%>" style="width:35px" readonly/></td>
<td><input type="text" name="amount<%=PRDE.getexp_id()%>" id="amount<%=GDS1.getgsId()%>"  value="<%=df.format(Double.parseDouble(PRDE.getamount()))%>" style="width:100px" readonly/></td>
<%-- <td><input type="text" name="amount<%=PRDE.getexp_id()%>" id="amount<%=PRDE.getexp_id()%>"  value="<%=df.format(Double.parseDouble(PRDE.getamount()))%>" style="width:100px" readonly/></td> --%>
<td> <% 
if(approvedBy1!=null && !approvedBy1.equals("null") &&  !approvedBy1.equals("")){%> 
	<textarea rows="2" cols="50" name="narration"><%=PRDE.getnarration() %></textarea>
	 <%}else{ %>
	<textarea rows="2" cols="50" name="narration"><%=PRDE.getnarration() %></textarea>
	<%} %> 
</td>
</tr>
<%
	}
%>
<tr><td colspan="10" class="bg" align="right">TOTAL AMOUNT(+Other Charges)</td><td class="bg" style="font-size: 25px;"><%=df.format(totAmt+otherCharges) %></td><td class="bg"> <%if(BIMG.getImageName(GDS.getextra1())!=null && !BIMG.getImageName(GDS.getextra1()).equals("")){ %><span style="color: blue;"> <a href="../BillImages/<%=BIMG.getImageName(GDS.getextra1())%>" target="_blank" >  <u>Click Here</u> </a> for Attached Bill Scan Copy </span> <%}else{ %> <span style="color: red;"> No,Scanned bill attached <%} %></span> </td></tr>
<%	
} %>
<%
 /* System.out.println("size"+INd_APL.size());
System.out.println(request.getParameter("status")); */
String status = request.getParameter("status"); 
if(INd_APL.size()>0 && !status.equals("Postpone")){
 %>

<tr><td colspan="6" style="color: orange;font-weight: bold;" align="center">Note : Sorry,Already this Bill payment was approved by you!</td></tr>
<%}else{ %>

<%-- <tr>
<td colspan="2">
<input type="hidden" name="indentInvoiceId" value="<%=INVId%>">
<select name="status">
<option value="approved">Approve</option>
<option value="disapproved">Dis-Approved</option>
</select></td>
<td><textarea name="feedback" id="feedback"></textarea></td>
<td><input type="submit" name="submit" value="APPROVE"></td>
</tr> --%>
<tr>
<td colspan="3"><textarea name="feedback" id="feedback" placeHolder="Enter narration here"></textarea>
<input type="hidden" name="indentInvoiceId" value="<%=INVId%>">
</td>
<td align="center"><input type="submit" name="status" value="APPROVE">
<br>
<br>
<br>
<!-- <input type="submit" name="status" value="DISAPPROVE"> -->
<input type="hidden" name="postponestatus" value="<%=status%>">
<input type="hidden" name="approveId" value="<%=INDA.getapprove_id()%>"> 
<input type="submit" name="status" value="POSTPONE"></td>
</tr>
<%} %>
</table>
</form>
</div>
</div>
</div>




</div>
<%}
	else{response.sendRedirect("adminPannel.jsp?page=banks");}%>

