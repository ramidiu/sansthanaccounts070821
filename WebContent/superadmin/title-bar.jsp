<jsp:useBean id="SUP" class="beans.superadmin"></jsp:useBean>
<div class="vendor-name">

<div class="logo-here"><span>Sai Sansthan
Accounts</span></div>
<div class="menu">
<ul>
<li><a href="adminPannel.jsp?page=totalsalereport">Home</a></li>
<li><a href="">Create</a>
	<ul>
		<li><a href="adminPannel.jsp?page=vendors">Vendors</a></li>
		<li><a href="adminPannel.jsp?page=mastersTree">Head of Accounts</a></li>
		<li><a href="adminPannel.jsp?page=employees">Employees</a></li>
		<!-- <li><a href="adminPannel.jsp?page=employeeSalarySlipForm">Employees Salary Slip</a></li> -->
	</ul>
</li>

<li><a href="">Transactions</a>
<ul>
<li><a href="adminPannel.jsp?page=banks">Banking</a></li>
<li><a href="adminPannel.jsp?page=bank_lr">Bank L-R Report</a></li>

</ul>
</li>
<li><a href="#">Approvals</a>
<ul>
<li><a href="adminPannel.jsp?page=indentReportList">Indents Approvals List</a></li>
<li><a href="adminPannel.jsp?page=billApprovalsList">BillApprovals List</a></li>
<%if( session.getAttribute("role").equals("GS") || session.getAttribute("role").equals("CM") || session.getAttribute("role").equals("FS")){ %>
<!-- <li><a href="adminPannel.jsp?page=paymentsApprovalsList">PaymentsApprovals List</a></li> -->
<li><a href="#">PaymentsApprovals</a>
	<ul class="level-2">
		<li><a href="adminPannel.jsp?page=paymentsApprovalsList">PaymentsApprovals List</a></li>
		<li><a href="adminPannel.jsp?page=dcPaymentApproval">Dc PaymentsApprovals List</a></li>
	</ul>
</li>
<li><a href="adminPannel.jsp?page=Total-Pending-Bills">Total Pending Bills</a></li>
<%} %>
<li><a href="adminPannel.jsp?page=Successful-payments-list">Payment Vouchers List</a></li>
</ul>
</li>
<li><a href="">Accounts</a>
	<ul>
		<li><a href="adminPannel.jsp?page=receipts-payments-report_neww_latest">Receipts and Payments</a></li>
		<li><a href="adminPannel.jsp?page=income_and_payments">Payments and Income</a></li>
		<li><a href="">Income & Expenditure Report</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report">Income & Expenditure Report</a></li>
			<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww">Income & Expenditure Report New</a></li>
			<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww_latest">Income & Expenditure Report New</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=ledgerReportBySitaram">Ledger</a></li>
		<li><a href="adminPannel.jsp?page=comparisionReport">Comparison Report</a></li>
		<li><a href="adminPannel.jsp?page=vendorTaxReport-new">TDS Service & Tax</a></li>
		<li><a href="adminPannel.jsp?page=balanceSheet_neww">Balance Sheet </a></li>
		<li><a href="adminPannel.jsp?page=acquittanceReport">Acquittance Report</a></li>
		<li><a href="adminPannel.jsp?page=employeePaySlip">Employee Pay-Slip</a></li>
		<li><a href="adminPannel.jsp?page=medicineissuereport">MedicineConsumptionReport</a></li>
	</ul>
</li>

<li><a href="">StockReport</a>
	<ul>
		<li><a href="adminPannel.jsp?page=medicalStock">Medical Stock</a></li>
		<li><a href="adminPannel.jsp?page=poojaStore-StockReport">Kitchen Stock</a></li>
		<li><a href="adminPannel.jsp?page=kitchenStockAssetsNew">Assets Report</a></li>
		<li><a href="adminPannel.jsp?page=kitchenStockProvisionsNew">Provisions Report</a></li>
		<li><a href="adminPannel.jsp?page=GoldSilverReportNew">Gold and Silver</a></li>
		<li><a href="adminPannel.jsp?page=ConsumerReport">Consumer Report</a></li>
		<li><a href="adminPannel.jsp?page=Consumption-Report">Consumption Report</a></li>
		<li><a href="adminPannel.jsp?page=Stock_Issue_And_ProducedItems">KOT Issues</a></li>
		<li><a href="adminPannel.jsp?page=prasadamExpenseReport">Prasadam Expenses Report</a></li>
		<li><a href="adminPannel.jsp?page=Master-Stock-Entry-Report">MSE Report</a></li>
		<li><a href="adminPannel.jsp?page=stock-LedgerReport">Stock Ledger </a></li>
	</ul>
</li>
<li><a href="">Pro</a>
	<ul>
		<li><a href="adminPannel.jsp?page=collectionSummary-Report">Collection Summary</a></li>
		<li><a href="adminPannel.jsp?page=cardsalesreport">Card Sales Report</a></li>
		<li><a href="#">Offer Kinds</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReportSA">Offer Kinds DaliyReport</a></li>
				<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport_new">Offer Kinds DaliyReport(kind-gold)</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds_dailySalesSilver">Offer Kinds DaliyReport(kind-silver)</a></li>
			<li><a href="adminPannel.jsp?page=offerKindsProductWise">Offer Kinds-Product Wise(Shawls Report)</a></li>
			<li><a href="GoldSilverPostionReport.jsp">Gold Report</a></li>
			<li><a href="silver_Report.jsp">Silver Report</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=totalsalereport">Total Sales</a></li>
		<li><a href="adminPannel.jsp?page=Online-sale-report">Online Sales</a></li>
		<li><a href="adminPannel.jsp?page=proCollectionDetails-Report">Collection Details</a></li>
	</ul>
</li>
<!-- <li><a href="#">Reports</a>
	<ul>
		<li><a href="#">Collection Summary</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=Total-Collection-Summary-Report">Collection Summary</a></li>
			<li><a href="adminPannel.jsp?page=cardsalesreport">Card Sales Report</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReportSA">Offer Kinds DaliyReport</a></li>
				<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport_new">Offer Kinds DaliyReport(kind-gold)</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds_dailySalesSilver">Offer Kinds DaliyReport(kind-silver)</a></li>
			<li><a href="adminPannel.jsp?page=offerKindsProductWise">Offer Kinds-Product Wise</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=totalsalereport">Total Sale Report</a></li>
		<li><a href="adminPannel.jsp?page=Online-sale-report">Online Sales Report</a></li>
		<li><a href="adminPannel.jsp?page=income_and_payments">Payments & Income  Report</a></li>
	<li><a href="adminPannel.jsp?page=performance-detail-report">Performance Detail Report</a></li>
		<li><a href="">Trail Balance</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=trailBalanceReport">Trail Balance Report Old</a></li>
			<li><a href="adminPannel.jsp?page=trailBalanceReportNeww">Trail Balance Report New</a></li>
			<li><a href="adminPannel.jsp?page=trailBalanceReportNeww">Trail Balance Report New</a></li>
			<li><a href="adminPannel.jsp?page=trailBalanceReport_newBySitaram">Trail Balance Report New</a></li>
			</ul>
		</li>	
		<li><a href="adminPannel.jsp?page=receipts-payments-report">Receipts And Payments Report</a></li>
		<li><a href="adminPannel.jsp?page=receipts-payments-report_neww_latest">Receipts And Payments Report</a></li>
		<li><a href="">Income & Expenditure Report</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report">Income & Expenditure Report</a></li>
			<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww">Income & Expenditure Report New</a></li>
			<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww_latest">Income & Expenditure Report New</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=productReport">Godown Inventory Report</a></li>
		<li><a href="adminPannel.jsp?page=ledgerReport">Ledger Report</a></li>	
		<li><a href="adminPannel.jsp?page=ledgerReportBySitaram">Ledger Report</a></li>	
		<li><a href="adminPannel.jsp?page=comparison-Report-Expenses">Comparison Report Of Expenses</a></li>
		<li><a href="adminPannel.jsp?page=productReport">Product Ledger Report</a></li>
		<li><a href="adminPannel.jsp?page=vendorTaxReport">TDS & Service Tax Report</a></li>
		<li><a href="adminPannel.jsp?page=vendorTaxReport-new">TDS & Service Tax New Report</a></li>
		<li><a href="adminPannel.jsp?page=Consumption-Report">Consumption Report</a></li>
		<li><a href="adminPannel.jsp?page=Stock_Issue_And_ProducedItems">KOT & Produced Products Report</a></li>
		<li><a href="adminPannel.jsp?page=Gatepass-Entry-Report">GatePasses Entries Report</a></li>
	    <li><a href="adminPannel.jsp?page=KitchenReport">Consumption Issue Report</a></li>
		<li><a href="">Stock Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=medicalStock">Medical Stock</a></li>
				<li><a href="adminPannel.jsp?page=poojaStore-StockReport">Kitchen Stock</a></li>
				<li><a href="kitchenStock2.jsp">Total Assets & Provisions Report </a></li>
				<li><a href="adminPannel.jsp?page=kitchenStock2New">Total Assets & Provisions Report </a></li>
			    <li><a href="kitchenStockAssets.jsp">Assets Report</a></li>
			     <li><a href="adminPannel.jsp?page=kitchenStockAssetsNew">Assets Report</a></li>
				<li><a href="kitchenStockProvisions.jsp">Provisions Report</a></li>
				<li><a href="adminPannel.jsp?page=kitchenStockProvisionsNew">Provisions Report</a></li>
				<li><a href="medicalStockReport.jsp">Medical Stock Report</a></li>
				<li><a href="adminPannel.jsp?page=medicalStockReportNew">Medical Stock Report</a></li>
				<li><a href="GoldSilverReport.jsp">Gold Silver Report</a></li>
				<li><a href="adminPannel.jsp?page=GoldSilverReportNew">Gold Silver Report</a></li>
				<li><a href="adminPannel.jsp?page=ConsumerReport">Consumer Report</a></li>
				<li><a href="GoldSilverPostionReport.jsp">Gold Silver Position Report</a></li>
				<li><a href="adminPannel.jsp?page=stock-performance-report">Performance Report</a></li>
	
							</ul>
							<li><a href="#">Performance Reports</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=performance-detail-report">Performance Report</a></li>
			<li><a href="adminPannel.jsp?page=performance-detail-reportwithoutIndent">Performance Report With Out Indent</a></li>
			</ul>
		</li>	
		<li><a href="#">PRO Office Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=collectionSummary-Report">Collection Summary</a></li>
				<li><a href="adminPannel.jsp?page=proCollectionDetails-Report">Collection Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=comparisionReport">Comparison Report</a></li>
				<li><a href="adminPannel.jsp?page=comparison-Report-Month-Wise">Comparison Report Month Wise</a></li>
				<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport">Offer Kinds Report</a></li>
				<li><a href="adminPannel.jsp?page=proOffice-salesReport">Sales Detail Report</a></li>
			</ul>
		</li>
		<li><a href="#">Balance Sheet</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=balanceSheet">Balance Sheet</a></li>
			<li><a href="adminPannel.jsp?page=balanceSheet_new">Balance Sheet OLD</a></li>
			<li><a href="adminPannel.jsp?page=balanceSheet_neww">Balance Sheet NEW</a></li>
			</ul>
			</li>
		<li><a href="#">Medical Reports</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=medicalSalesList">Daily Patients List</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=acquittanceReport">Acquittance Report</a></li>
		<li><a href="adminPannel.jsp?page=bank_lr">Bank LR Report</a></li>
		<li><a href="adminPannel.jsp?page=prasadamExpenseReport">Prasadam Expenses Report</a></li>
		<li><a href='adminPannel.jsp?page=Master-Stock-Entry-Report'>MSE-Report</a></li>
		<li><a href="adminPannel.jsp?page=stock-LedgerReport">Stock Ledger Report</a></li>
		<li><a href="adminPannel.jsp?page=employeePaySlip">Employee Pay Slip</a></li>
	</ul>
</li> -->

<!-- <dl>
<dt><a href="adminPannel.jsp?page=indentReportList">Indents List</a></dt>
</dl> -->
<!-- <li><a href="#">Pooja Store Reports</a>
	<ul>
		<li><a href="adminPannel.jsp?page=Profit-Report">Sales Profit Report</a></li>
		<li><a href="adminPannel.jsp?page=Stock-Valuation-ProfitReport">STOCK VALUATION PROFIT</a></li>
		<li><a href="adminPannel.jsp?page=Stock-Valuation-ProfitReport&con=both">GODOWN & PS STOCK VALUATION PROFIT</a></li>
		<li><a href="#">PS-Sales Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=shopSalesList">shop Sales Report</a></li>
				<li><a href="adminPannel.jsp?page=shopSales-DetailReport">ShopSales Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=PS-DayStock-ValuationReport">STOCK POSITION</a></li>
				
			</ul>
		</li>
	</ul>
</li> -->

</ul>
</div>
<div class="name">
<ul>
<%-- <li><div style="float:left; margin:0 20px 0 0"><%=session.getAttribute("sdname")%></div>  --%>
<li><div style="float:left; margin:0 20px 0 0"><%=session.getAttribute("rolefullname")%></div>
<div class="dropmenu">
<dl>
<!--<dt><div class="title">Account</div></dt>-->
<dt><a href="#" onclick="openMyModal('changePassword.jsp');">Change Password</a></dt>
<dt><a href="logOut.jsp">Log out</a></dt>
</dl>

</div>

</li>
</ul></div>

</div>
<script>
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});</script>
<div style=""><a href="#" class="scrollup"></a></div>