<%@page import="mainClasses.productsListing"%>
<%@page import="beans.invoice"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.invoiceListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.medicalpurchases"%>
<%@page import="mainClasses.medicalpurchasesListing"%>
<%@page import="java.util.List"%>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript"
	src="../js/modal-window.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source) {
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();

	};
</script>
<script type="text/javascript">
	// When the document is ready, initialize the link so
	// that when it is clicked, the printable area of the
	// page will print.
	$(function() {
		// Hook up the print link.
		$("#print").attr("href", "javascript:void( 0 )").click(function() {
			// Print the DIV.
			$("a").css("text-decoration", "none");
			$("#tblExport").print();

			// Cancel click event.
			return (false);
		});
	});
	$(document).ready(function() {
		$("#btnExport").click(function() {
			$('.replaceme').html('Rs.');
			$("#tblExport").btechco_excelexport({
				containerid : "tblExport",
				datatype : $datatype.Table
			});
			$('.replaceme').html('&#8377;');
		});
		$("#hoid").change(function() {
			$("#search").submit();
		});
	});
</script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<jsp:useBean id="VEN" class="beans.vendors" />
<jsp:useBean id="PEXP" class="beans.productexpenses" />
<jsp:useBean id="INV" class="beans.invoice" />
<jsp:useBean id="GOD" class="beans.godwanstock" />
<jsp:useBean id="PMT" class="beans.payments" />
<%
	String vendorId = request.getParameter("id");
	mainClasses.vendorsListing VEN_CL = new mainClasses.vendorsListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	List VEN_List = VEN_CL.getMvendors(vendorId);
	if (VEN_List.size() > 0) {
		VEN = (beans.vendors) VEN_List.get(0);
%>
<div class="vendor-page">
	<div class="vendor-box">
		<div class="name-title"><%=VEN.gettitle()%>
			<%=VEN.getfirstName()%>
			<%=VEN.getlastName()%>
		</div>
		<div class="click floatleft">
			<a href="./editVendor.jsp?id=<%=VEN.getvendorId()%>" target="_blank"
				onclick="openMyModal('editVendor.jsp?id=<%=VEN.getvendorId()%>'); return false;">Edit</a>
		</div>
		<div style="clear: both;"></div>
		<div class="details-list">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td colspan="2"><span>Agency Name:</span><%=VEN.getagencyName()%></td>
				</tr>

				<tr>
					<td width="40%"><span>Email:</span><%=VEN.getemail()%></td>
					<td><span>Address:</span><%=VEN.getaddress()%> <%=VEN.getpincode()%></td>
				</tr>
				<tr>
					<td><span>Phone:</span><%=VEN.getphone()%></td>
					<td><span>City:</span><%=VEN.getcity()%></td>
				</tr>
				<tr>
					<td><span>Gothram:</span><%=VEN.getgothram()%></td>
					<td><span>Nominee:</span><%=VEN.getnomineeName()%></td>
				</tr>

				<tr>
					<td colspan="2"><span>Other Details:</span><%=VEN.getdescription()%></td>
				</tr>

				<tr>
					<td><span>Bank Name:</span><%=VEN.getbankName()%></td>
					<td><span>Bank Branch:</span><%=VEN.getbankBranch()%></td>
				</tr>

				<tr>
					<td><span>IFSC code:</span><%=VEN.getIFSCcode()%></td>
					<td><span>Account Number:</span><%=VEN.getaccountNo()%></td>
				</tr>
				<tr>
					<td colspan="2"><span>Credit Days:</span><%=VEN.getcreditDays()%></td>
				</tr>
			</table>

		</div>
		<%
			mainClasses.medicalpurchasesListing MED_Purchases = new mainClasses.medicalpurchasesListing();
				mainClasses.invoiceListing INV_LST = new mainClasses.invoiceListing();
				mainClasses.paymentsListing PAY_L = new mainClasses.paymentsListing();
				double totVendorAmt = 0.0;
				double totBillAmt = 0.0;
				double paidAmt = 0.0;
				double balanceAmt = 0.0;
				DecimalFormat df = new DecimalFormat("#,###.00");
				/* if(INV_LST.getTotalInvoicesAmount(VEN.getvendorId())!=null && !INV_LST.getTotalInvoicesAmount(VEN.getvendorId()).equals(""))
				 totVendorAmt=Double.parseDouble(INV_LST.getTotalInvoicesAmount(VEN.getvendorId())); *///this lines are commented by pradeep because in this other charges amount is not added so below two lines are added by me
				if (INV_LST.getTotalInvoicesAmount(VEN.getvendorId()) != null
						&& !INV_LST.getTotalInvoicesAmount(VEN.getvendorId())
								.equals("")
						&& INV_LST
								.getTotalOtherChargesAmount(VEN.getvendorId()) != null
						&& !INV_LST.getTotalOtherChargesAmount(
								VEN.getvendorId()).equals(""))
					totVendorAmt = Double.parseDouble(INV_LST
							.getTotalInvoicesAmount(VEN.getvendorId()))
							+ Double.parseDouble(INV_LST
									.getTotalOtherChargesAmount(VEN
											.getvendorId()));
				else
					totVendorAmt = 0.0;
				if (PAY_L.getPaidAmountToVendor(VEN.getvendorId()) != null
						&& !PAY_L.getPaidAmountToVendor(VEN.getvendorId())
								.equals(""))
					paidAmt = Double.parseDouble(PAY_L
							.getPaidAmountToVendor(VEN.getvendorId()));
				else
					paidAmt = 0.0;
		%>
		<div class="details-amount">

			<ul>
				<li class="colr">&#8377; <%=df.format(Math.round(totVendorAmt))%><br>
					<span>TOTAL BILL AMOUNT</span></li>

				<li style="color: green;">&#8377; <%=df.format(Math.round(paidAmt))%><br>
					<span>PAID AMOUNT</span></li>
				<li class="colr1">&#8377; <%=df.format(Math.round(totVendorAmt)
						- Math.round(paidAmt))%><br>
					<span>PENDING BALANCE</span></li>
			</ul>


		</div>
	</div>

	<div class="vendor-list">

		<div class="clear"></div>
		<%-- <div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<div class="search-list">
<form action="adminPannel.jsp" method="post">
<ul>
<li>
<input type="hidden" name="page" value="vendorDetails">
<input type="hidden" name="id" value="<%=request.getParameter("id")%>">
<select>
<option>SORT BY</option>
</select></li>
<li><select name="status">
<option value="Payment Done">COMPLETED BILLS</option>
<option value="pending">PENDING BILLS</option>
</select></li>
<li><input type="submit"  name="submit" value="SEARCH"/></li>
</ul>
</form>
</div> --%>
		<div class="icons">
			<span><img src="../images/back.png" height="50" width="55"
				style="margin: 0 20px 0 0" title="back" onclick="goBack()" /></span> <span><a
				id="print"><img src="../images/printer.png"
					style="margin: 0 20px 0 0" title="print" /></a></span> <span><a
				id="btnExport" href="#Export to excel"><img
					src="../images/excel.png" style="margin: 0 20px 0 0"
					title="export to excel" /></a></span>
		</div>
		<div class="clear"></div>
		<div class="list-details" style="margin: 10px 10px 0 0" id="tblExport">
			<table width="100%" cellpadding="0" cellspacing="0" 
				border="1">
				<tr>
				<tr>
					<td colspan="10" height="10"></td>
				</tr>
				<tr>
					<td colspan="10" align="center"
						style="font-weight: bold; color: red;">SHRI SHIRIDI SAI BABA
						SANSTHAN TRUST</td>
				</tr>
				<tr>
					<td colspan="10" align="center"
						style="font-weight: bold; font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500
						060</td>
				</tr>
				<tr>
					<td colspan="10" align="center"
						style="font-weight: bold; font-size: 14px;"><%=VEN.getagencyName()%>
						PAYMENT TRANSACTIONS</td>
				</tr>
				<tr>
					<td class="bg" width="7%">Invoice No.</td>
					<td class="bg" width="7%">Bill No.</td>
					<td class="bg" width="10%">DATE&TIME</td>
					<td class="bg" width="20%">Vendor</td>
					<td class="bg" width="20%">SUBHEAD</td>
					<td class="bg" width="10%">BILL AMOUNT</td>
					<td class="bg" width="15%">TOTAL INVOICE AMOUNT</td>
					<td class="bg" width="15%">PAYMENT MODE</td>
					<td class="bg" width="10%">CHEQUE NO</td>
					<td class="bg" width="10%">VOUCHER NO</td>
				</tr>
				<jsp:useBean id="GODW" class="beans.godwanstock" />
				<jsp:useBean id="GODW1" class="beans.godwanstock" />
				<jsp:useBean id="GODW2" class="beans.godwanstock" />
				<%
					subheadListing SUBHL = new subheadListing();
						productexpensesListing PROEL = new productexpensesListing();
						paymentsListing PAYL = new paymentsListing();
						godwanstockListing GODSL = new godwanstockListing();
						mainClasses.godwanstockListing GODS_l = new mainClasses.godwanstockListing();
						List PAYDET = null;
						String status = "Payment Done";
						String billNo;
						if (request.getParameter("status") != null
								&& !request.getParameter("status").equals("")) {
							status = request.getParameter("status");
						}
						List EXPD = PROEL.getproductexpensesBasedOnVendorAndStatus(
								request.getParameter("id"), status);
						double tot = 0.00;
						SimpleDateFormat SDF = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						SimpleDateFormat CDF = new SimpleDateFormat("dd-MM-yyyy/HH:mm");
						if (EXPD.size() > 0) {
							for (int i = 0; i < EXPD.size(); i++) {
								billNo = "";
								PEXP = (productexpenses) EXPD.get(i);
								PAYDET = PAYL.getMpaymentsBasedOnExpID(PEXP
										.getexpinv_id());
								if (PAYDET.size() > 0) {
									PMT = (beans.payments) PAYDET.get(0);
								}
								if (PMT.getamount() != null
										&& !PMT.getamount().equals(""))
									tot = tot + Double.parseDouble(PMT.getamount());
								List GODS_DET = GODS_l
										.getStockDetailsBasedOnInvoiceId(PEXP
												.getextra5());
								if (GODS_DET.size() > 0) {
									GODW = (beans.godwanstock) GODS_DET.get(0);
								}
				%>
				<tr>
					<td><%=PEXP.getexpinv_id()%></td>
					<td><%=GODW.getbillNo()%></td>
					<td>
						<%
							if (PMT.getdate() != null && !PMT.getdate().equals("")) {
						%> <%=CDF.format(SDF.parse(PMT.getdate()))%>
						<%
							} else {
						%> N/A <%
							}
						%>
					</td>
					<td><%=VEN.getfirstName()%><%=VEN.getlastName()%></td>
					<td><%=SUBHL.getMsubheadname(PEXP.getsub_head_id())%></td>
					<td align="right">
						<%
							if (GODSL.getBillTOT(PEXP.getextra5()) != null
												&& !GODSL.getBillTOT(PEXP.getextra5()).equals(
														"")) {
						%>
						<span class="replaceme">&#8377;</span> <%=df.format(Double.parseDouble(GODSL
									.getBillTOT(PEXP.getextra5())))%>
						<%
							}
						%>
					</td>
					<td align="right">
						<%
							if (PMT.getamount() != null
												&& !PMT.getamount().equals("")) {
						%> <span
						class="replaceme">&#8377;</span> <%=df.format(Double.parseDouble(PMT
									.getamount()))%>
						<%
							}
						%>
					</td>
					<td align="center">
						<!-- <div class="print-btn">Print Check</div> --> <%=PMT.getpaymentType()%></td>
					<td>
						<!-- <div class="print-btn">Print Check</div> --><%=PMT.getchequeNo()%></td>
					<td>
						<!-- <div class="print-btn">Print Check</div> --><%=PMT.getreference()%></td>
				</tr>
				<%
					}
							totBillAmt = 0.0;
				%>
				<tr>
					<td colspan="5" align="right" class="bg">TOTAL AMOUNT</td>
					<td colspan="1" align="right" class="bg" style="font-weight: bold;"><span
						class="replaceme">&#8377;</span> <%=df.format(tot)%></td>
					<td colspan="3" class="bg"></td>
				</tr>
				<%
					} else {
				%>
				<tr>
					<td colspan="10" align="center"
						style="font-size: 20px; color: red;">There is no transactions
						found for this vendor!..</td>
				</tr>
				<%
					}
				%>

			</table>

			<!-- This pending transactions code is written by pradeep on 17/06/2016 starts -->
			<br />
			<table width="100%" cellpadding="0" cellspacing="0" id="tblExport"
				border="1">
				<tr>
				<tr>
					<td colspan="10" align="center"
						style="font-weight: bold; font-size: 14px;"><%=VEN.getagencyName()%>
						PENDING PAYMENT TRANSACTIONS</td>
				</tr>
				<tr>
					<td class="bg" width="7%">Invoice No.</td>
					<td class="bg" width="7%">Bill No.</td>
					<td class="bg" width="10%">DATE&TIME</td>
					<td class="bg" width="20%">Vendor</td>
					<td class="bg" width="20%">SUBHEAD</td>
					<td class="bg" width="10%">BILL AMOUNT</td>
					<td class="bg" width="15%">TOTAL INVOICE AMOUNT</td>
					<td class="bg" width="35%">PAYMENT STATUS</td>

				</tr>
				<%
					subheadListing SUBHL1 = new subheadListing();
						productexpensesListing PROEL1 = new productexpensesListing();
						paymentsListing PAYL1 = new paymentsListing();
						godwanstockListing GODSL1 = new godwanstockListing();
						invoiceListing INVL = new invoiceListing();
						productsListing PRODL = new productsListing();
						List PAYDET1 = null;
						String status1 = "";
						if (request.getParameter("status") != null
								&& !request.getParameter("status").equals("")) {
							status1 = request.getParameter("status");
						}
						List EXPD1 = PROEL1.getproductexpensesBasedOnVendorAndStatus(
								request.getParameter("id"), status1);
						List INV11 = INVL.getMNOTAdminApprovedinvoiceList(request
								.getParameter("id"));
						double tot1 = 0.00;
						SimpleDateFormat SDF1 = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						SimpleDateFormat CDF1 = new SimpleDateFormat("dd-MM-yyyy/HH:mm");
						if (EXPD1.size() > 0 || INV11.size() > 0) {
							for (int i = 0; i < EXPD1.size(); i++) {
								PEXP = (productexpenses) EXPD1.get(i);
								if (GODSL1.getBillTOT(PEXP.getextra5()) != null
										&& !GODSL1.getBillTOT(PEXP.getextra5()).equals(
												""))
									tot1 = tot1
											+ Double.parseDouble(GODSL1.getBillTOT(PEXP
													.getextra5()));
								List GODS_DET1 = GODS_l
										.getStockDetailsBasedOnInvoiceId(PEXP
												.getextra5());
								if (GODS_DET1.size() > 0) {
									GODW1 = (beans.godwanstock) GODS_DET1.get(0);
								}
				%>
				<tr>
					<td><%=PEXP.getexpinv_id()%></td>
					<td><%=GODW1.getbillNo()%></td>
					<td>
						<%
							if (PEXP.getentry_date() != null
												&& !PEXP.getentry_date().equals("")) {
						%><%=CDF1.format(SDF1.parse(PEXP
									.getentry_date()))%>
						<%
							}
						%>
					</td>
					<td><%=VEN.getfirstName()%><%=VEN.getlastName()%></td>
					<td><%=SUBHL1.getMsubheadname(PEXP.getsub_head_id())%></td>
					<td align="right">
						<%
							if (GODSL1.getBillTOT(PEXP.getextra5()) != null
												&& !GODSL1.getBillTOT(PEXP.getextra5()).equals(
														"")) {
						%>
						&#8377; <%=df.format(Double.parseDouble(GODSL1
									.getBillTOT(PEXP.getextra5())))%>
						<%
							}
						%>
					</td>
					<td align="right">&#8377; <%=df.format(Double.parseDouble(GODSL1
								.getBillTOT(PEXP.getextra5())))%>
					</td>
					<!-- <td align="center"><div class="print-btn">Print Check</div>  PAYMENT PENDING</td> -->
					<td align="center">
						<%
							if (PEXP.getextra1().equals("Not-Approve")
												&& PEXP.getextra4().equals("")) {
						%>Waiting
						For GS Approval<%
							} else if (PEXP.getextra1().equals("Not-Approve")
												&& PEXP.getextra4().equals("BoardApprove")) {
						%>Waiting
						For FS Approval<%
							} else if (PEXP.getextra1().equals("Approved")
												&& PEXP.getextra4().equals("BoardApprove")) {
						%>PAYMENT
						PENDING<%
							} else if (PEXP.getextra1()
												.equals("WaitingMgrApproval")
												&& PEXP.getextra4().equals("")) {
						%>Waiting
						For MGR Approval<%
							}
						%>
					</td>

				</tr>
				<%
					}
							List INV1 = INVL.getMNOTAdminApprovedinvoiceList(request
									.getParameter("id"));
							for (int j = 0; j < INV1.size(); j++) {
								INV = (invoice) INV1.get(j);
								List subheadId = GODSL1.getgodwanstockAllApproved(INV
										.getinv_id());
								if (subheadId.size() > 0)
									GOD = (godwanstock) subheadId.get(0);
								if (INV.getamount() != null
										&& !INV.getamount().equals(""))
									tot1 = tot1 + Double.parseDouble(INV.getamount());
								List GODS_DET2 = GODS_l
										.getStockDetailsBasedOnInvoiceId(PEXP
												.getextra5());
								if (GODS_DET2.size() > 0) {
									GODW2 = (beans.godwanstock) GODS_DET2.get(0);
								}
				%>
				<tr>
					<td><%=INV.getinv_id()%></td>
					<td><%=GODW2.getbillNo()%></td>
					<td>
						<%
							if (INV.getcreatedBy() != null
												&& !INV.getcreatedBy().equals("")) {
						%><%=CDF1.format(SDF1.parse(INV.getcreatedBy()))%>
						<%
							}
						%>
					</td>
					<td><%=VEN.getfirstName()%><%=VEN.getlastName()%></td>
					<td>
						<%
							if (!SUBHL1.getMsubheadname(GOD.getproductId()).equals(
												"")) {
						%><%=SUBHL1.getMsubheadname(GOD.getproductId())%>
						<%
							} else {
						%><%=PRODL.getProductNameByCat(
									GOD.getproductId(), GOD.getdepartment())%>
						<%
							}
						%>
					</td>
					<td align="right">
						<%
							if (INV.getamount() != null
												&& !INV.getamount().equals("")) {
						%><%=df.format(Double.parseDouble(INV
									.getamount()))%>
						<%
							}
						%>
					</td>
					<td align="right">&#8377; <%=df.format(Double.parseDouble(INV.getamount()))%>
					</td>
					<!-- <td align="center"><div class="print-btn">Print Check</div>  PAYMENT PENDING</td> -->
					<td align="center">
						<%
							if (GOD.getprofcharges().equals("UncheckedbyPO")
												&& GOD.getApproval_status().equals("")) {
						%>Waiting
						For PO Approval<%
							} else if (GOD.getprofcharges().equals("checked")
												&& GOD.getApproval_status().equals(
														"Not-Approve")) {
						%>Waiting
						For Board Members Approval<%
							} else if (GOD.getprofcharges().equals("checked")
												&& GOD.getApproval_status().equals("Approved")) {
						%>Waiting
						For Admin Verfication<%
							}
						%>
					</td>

				</tr>
				<%
					}
							totBillAmt = 0.0;
				%>
				<tr>
					<td colspan="5" align="right" class="bg">TOTAL AMOUNT</td>
					<td colspan="1" align="right" class="bg" style="font-weight: bold;"><span
						class="replaceme">&#8377;</span> <%=df.format(tot1)%></td>
					<td colspan="3" class="bg"></td>
				</tr>
				<%
					} else {
				%>
				<tr>
					<td colspan="10" align="center"
						style="font-size: 20px; color: red;">There is no Pending
						transactions found for this vendor!..</td>
				</tr>
				<%
					}
				%>

			</table>

			<!-- This pending transactions code is written by pradeep on 17/06/2016 end -->

		</div>
	</div>
</div>
<%
	} else {
		response.sendRedirect("adminPannel.jsp?page=vendors");
	}
%>

