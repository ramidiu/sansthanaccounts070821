<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Super Admin Login</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<script type="text/javascript">
function validate(){
$('#RequiredPassword').hide();
$('#RequiredUserName').hide();
	if($('#username').val().trim()==""){
		$('#RequiredUserName').show();
		$('#username').focus();
		return false;
	}
	
	if($('#password').val().trim()==""){
		$('#RequiredPassword').show();
		$('#password').focus();
		return false;
	}
	
}
$( document ).ready(function() {
	$( "#target" ).click(function() {
		$( "#signin").hide();
		$( "#forgotpass").show();
		});
	$( ".fPass" ).click(function() {
		$( "#signin").show();
		$( "#forgotpass").hide();
		});
	if (typeof(Storage) != "undefined") {
		$( "#rembrme").show();
		var username=localStorage.getItem("username");
		var password=localStorage.getItem("password");
		if(username!="null" && password!="null" && username!=null && password!=null){
		$( "#username").val(username);
		$( "#password").val(password);
		$('input:checkbox[name=rememberme]').attr('checked',true);
		} else{
			$( "#username").val("");
			$( "#password").val("");
		}
	} else{
		$('input:checkbox[name=rememberme]').attr('checked',false);
		$( "#rembrme").hide();	
	}
	
});
</script>
</head>
<body class="loging_bg">
<div class="main">
<div class="logo"><div class="logo-here" align="center"><span style="color:#FFF;">Sai Sansthan Accounts</span></div></div>
<div class="admin-signin">

<table width="100%" cellpadding="0" cellspacing="0" >
<tr>
<td colspan="2">
<form action="validate.jsp" method="post" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0" id="signin" style="display: block;" >
<tr>
<td colspan="2" align="center"><span class="signin">Sign In</span></td>
</tr>
<tr>
<td align="right" class="correct">USER ID</td>
<td><input type="text" name="username" id="username" value="" placeHolder="Enter your username" />
<div class="plz" id="RequiredUserName" style="display: none;">Please Provide Valid Username.</div></td>
</tr>
<tr>
<td align="right" class="correct">PASSWORD</td>
<td><input type="password" name="password" id="password" value="" placeHolder="Enter your password"/>
<div class="plz" id="RequiredPassword" style="display: none;">Please Enter Password.</div>
<%if(session.getAttribute("error")!=null){%>
<div class="plz" id="RequiredPassword"><%=session.getAttribute("error") %></div>
<%session.invalidate();}%>
</td>
</tr>
<tr>
<td colspan="2" align="center"><span id="rembrme" style="color: #193048;font-weight: bold;" ><input type="checkbox" name="rememberme" id="rememberme" value="rememberme" class="classrem" />Remember me</span> </td>
</tr>
<tr>
<td colspan="2" align="center"><input type="submit" value="Sign In" class="click bordernone" /> <span style="color: red;font-size: 15px;cursor: pointer;" id="target">Forgotten your password?</span> </td>
</tr>
</table>
</form>
</td>
</tr>

<tr>
<td colspan="2" >
<form action="forgot.jsp" method="post">
<table width="100%" cellpadding="0" cellspacing="0" id="forgotpass" style="display: none;" >
<tr>
<td colspan="2" align="center"><span class="signin">Forgot Password</span></td>
</tr>
<tr><td colspan="2">(Enter username to receive an email with password.)</td></tr>
<tr>
<td align="right" class="correct">USER ID</td>
<td><input type="text" name="username" id="username" value="" placeHolder="Enter your username" required/></td>
</tr>
<tr>
<td colspan="2" align="center"><input type="submit" value="Submit" class="click bordernone" /> <a href="#" class="fPass">Go back to Login Page</a> </td>
</tr>
</table>
</form>
</td>
</tr>
</table>

</div>
</div>
</body>
</html>