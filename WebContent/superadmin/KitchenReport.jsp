<%@page import="beans.produceditems"%>
<%@page import="beans.shopstock"%>
<%@page import="beans.stockrequest"%>
<%@page import="mainClasses.produceditemsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.consumption_entries"%>
<%@page import="mainClasses.consumption_entriesListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CONSUMPTION REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("superadminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="StockRequest" class="beans.stockrequest" />
				<jsp:useBean id="ShopStock" class="beans.shopstock" />
				<jsp:useBean id="ProducedItem" class="beans.produceditems" />
		
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
				</div>
				<div class="sj">
					 <form action="adminPannel.jsp" method="post">  
						<input type="hidden" name="page" value="Consumption-Report"/>
						 <%-- <select name="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="of" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("of")){ %>selected="selected" <%} %>>OFFER KINDS</option>
						</select> --%>
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form name="searchForm" id="searchForm" action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" name="page" value="KitchenReport"/>
						<input type="hidden" class="click" name="search" value="Search"></input>
						<%-- <select name="headID" id="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="of" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("of")){ %>selected="selected" <%} %>>OFFER KINDS</option>
						</select> --%>
						
						</li>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					 <span>
					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:00";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	String hoaID="";
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoaID=request.getParameter("headID");
	}
	stockrequestListing StockRequestListing=new stockrequestListing();
	shopstockListing ShopStockListing=new shopstockListing();
	produceditemsListing ProducedItemListing=new produceditemsListing();
	productsListing productListing=new productsListing();
	employeesListing EmployeeListing=new employeesListing();
	/* List StockRequestList=StockRequestListing.getstockrequestBasedOnHOA("0",fromDate,toDate); */
	List StockRequestList=StockRequestListing.getstockrequestBasedOnHOA("0",fromDate,toDate,"KOT");
	List StockTransferList=ShopStockListing.getgShopstockTransfer(fromDate,toDate,"1");
	List ProducedItemList=ProducedItemListing.GetProducedItemBasedOnHeadOfAccount(fromDate, toDate,"");
	
	if(StockRequestList.size()>0 || StockTransferList.size()>0 || ProducedItemList.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:92%; 
    background:#d0e3fb; 
}

</style>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	 <div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="8" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;">CONSUMPTION ISSUE REPORT</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
						<%if(StockRequestList.size()>0){ %>
                                               <tr>
							<td colspan="8"  class="bg"  style="font-weight: bold;color: red;" align="center">Stock Request</td></tr>
						<tr>
							<td class="bg" style="font-weight: bold;">S.NO</td>
							<td class="bg"  style="font-weight: bold;">REQUESTED DATE</td>
							<td class="bg" colspan="2"  style="font-weight: bold;">PRODUCT NAME.</td>
							<td class="bg" style="font-weight: bold;">PRODUCT QTY</td>
									<td class="bg" colspan="3"  style="font-weight: bold;">REQUEST TYPE</td>
						</tr>
					
						<%for(int i=0; i <StockRequestList.size(); i++ ){ 
							StockRequest=(stockrequest)StockRequestList.get(i);
							%>
						    <tr style="position: relative;">
								<td style="font-weight: bold;" ><%=i+1%></td>
								<td style="font-weight: bold;" ><%=chngDateFormat.format(dateFormat2.parse( StockRequest.getreq_date())) %></td>
								<td colspan="2"  style="font-weight: bold;" ><%=productListing.getProductsNameByCat1(StockRequest.getproduct_id(),StockRequest.gethead_account_id())%></td>
								<td style="font-weight: bold;" ><span><%=StockRequest.getquantity() %></span></td>
								<td colspan="3"  style="font-weight: bold;"><%=StockRequest.getreq_type()%></td>
												</tr>
					<%}%>
					   <% } if(StockTransferList.size()>0){ %>
					   
					    <tr>
							<td colspan="8"  class="bg" style="font-weight: bold;color: red;" align="center">Stock Transfer</td></tr>
						<tr>
							<td class="bg" style="font-weight: bold;">S.NO.</td>
							<td class="bg"  style="font-weight: bold;">Date</td>
							<td class="bg"  style="font-weight: bold;">Transfer NO</td>
							<td class="bg" colspan="2"   style="font-weight: bold;">Product Code/Name</td>
							<td class="bg"  style="font-weight: bold;">Quantity</td>
							<td  class="bg" style="font-weight: bold;">Transfer By</td>
						</tr>
						<%for(int i=0; i < StockTransferList.size(); i++ ){ 
							ShopStock=(shopstock)StockTransferList.get(i);
							%>
						    <tr >
								<td style="font-weight: bold;" ><%=i+1%></td>
								<td colspan="2"  style="font-weight: bold;"><%=productListing.getProductsNameByCat1(ShopStock.getproductId(),ShopStock.getextra1())%></td>
								<td   style="font-weight: bold;" ><%=ShopStock.getextra3() %></td>
								<td style="font-weight: bold;" ><%=chngDateFormat.format(dateFormat2.parse( ShopStock.getforwardedDate())) %></td>
								<td style="font-weight: bold;" ><span><%=ShopStock.getquantity() %></span></td>
								<td style="font-weight: bold;" ><%=EmployeeListing.getMemployeesName(ShopStock.getemp_id())%></td>
												</tr>
					<%}%>
				
					   <%} if(ProducedItemList.size()>0){ %>
					 
						<tr>	<td colspan="8"  class="bg"  style="font-weight: bold;color: red;" align="center">Produced Items</td></tr>
			
			<tr>
							<td class="bg" align="center" style="font-weight: bold;">S.NO.</td>
								<td class="bg" align="center" style="font-weight: bold;">Date</td>
							<td class="bg" align="center" style="font-weight: bold;">PRODUCT ID/NAME</td>
							<td class="bg" align="center" style="font-weight: bold;" align="center">QTY</td>
							<td class="bg"  align="left" style="font-weight: bold;">NARRATION</td>
							<td class="bg"  align="left" style="font-weight: bold;">Category</td>
							<td class="bg" colspan="2"   align="left" style="font-weight: bold;">Employee</td>
						</tr>
				
						
						<%for(int i=0; i < ProducedItemList.size(); i++ ){ 
							ProducedItem=(produceditems)ProducedItemList.get(i);
							%>
						    <tr style="position: relative;">
								<td style="font-weight: bold;"  align="center"  ><%=i+1%></td>
								<td style="font-weight: bold;"  align="center" ><%=chngDateFormat.format(dateFormat.parse( ProducedItem.getproduced_date() )) %></td>
								<td style="font-weight: bold;"  align="center" ><%=productListing.getProductsNameByCat1(ProducedItem.getproductId(),ProducedItem.getextra1()) %></td>
								<td style="font-weight: bold;"  align="center" ><%=ProducedItem.getquantity()%></td>
								<td style="font-weight: bold;"  align="left" ><span><%=ProducedItem.getdescription() %></span></td>
								<td style="font-weight: bold;"  align="left" ><span><%=ProducedItem.getstatus() %></span></td>
								<td style="font-weight: bold;" colspan="2"  align="left" ><%=EmployeeListing.getMemployeesName(ProducedItem.getemp_id())%></td>
												</tr>
					<%}%>
					
					   <%} %>
					</table>
				</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>Sorry,There were no consumption  report found!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>