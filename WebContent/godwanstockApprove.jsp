<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GODOWNSTOCK APPROVAL LIST | SHRI SHIRDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function () {
	$( ".tabletexpirydate").datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		 minDate: '0',
		dateFormat: 'yy-mm-dd'
	});	
});
$(document).ready(function() {
    $('#selectall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>
<script>
function showstockaprve(id){
	$('#'+id).toggle();
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>


<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ 
	headofaccountsListing HOAL=new headofaccountsListing();
	%>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="GSTK" class="beans.godwanstock" />
				<jsp:useBean id="GDSTK" class="beans.godwanstock" />
		<jsp:useBean id="SHP" class="beans.shopstock" />
		<jsp:useBean id="PRD" class="beans.products" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
		
<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%=HOAL.getHeadofAccountName(session.getAttribute("headAccountId").toString()) %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Godown Received Stock Approval</td></tr>
				</table>
		
				<div class="clear"></div>
				<div class="list-details">
				
	<%
	SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy");
	godwanstockListing GSK_L=new godwanstockListing();
	productsListing PROL=new productsListing();
	vendorsListing VENL=new vendorsListing();
	List STOC_Apr=null;
	List Godwnl=null;
	if(session.getAttribute("Role")!=null && session.getAttribute("Role").toString().equals("medicalhall")){
		Godwnl=GSK_L.getMedicalUnapprovedList(session.getAttribute("headAccountId").toString());
	}else{
		Godwnl=GSK_L.getgodwanstockUnapprovedList(session.getAttribute("headAccountId").toString());
	}
	 
	  if(Godwnl.size()>0){
		  for(int j=0; j < Godwnl.size(); j++ ){
			  GDSTK=(godwanstock)Godwnl.get(j);			  
      STOC_Apr=GSK_L.getgodwanstockUnapproved(GDSTK.getextra1());
	  if(STOC_Apr.size()>0){ if(j==0){%>
         
	  <table width="100%" cellpadding="0" cellspacing="0" class="yourID">
				<tr>
				
					<td  width="10%" style="font-weight: bold;" align="center">SNo</td>
					<td  colspan="2" width="21%" align="center" style="font-weight: bold;">DATE</td>
					<td  colspan="2" width="21%" align="center" style="font-weight: bold;">Invoice Id</td>
					<td  colspan="2" width="26%" align="center" style="font-weight: bold;">Vendor Name</td>
				</tr>
		</table>
								<%} %>
	  		<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="bg" width="10%" align="center"><%=j+1 %></td>
							<td  colspan="2" width="21%" class="bg" align="center" style="font-weight: bold;"><a onclick="showstockaprve('<%=GDSTK.getextra1()%>')" style="cursor: pointer;" ><%=CDF.format(SDF.parse(GDSTK.getdate()))%></a></td>
							<td colspan="2" align="center"  align="center" width="21%" class="bg" style="font-weight: bold;"><a onclick="showstockaprve('<%=GDSTK.getextra1()%>')" style="cursor: pointer;" ><%=GDSTK.getextra1()%>&nbsp;<%=GDSTK.getExtra25()%></a></td>
							<td colspan="2" align="center" width="26%" class="bg" style="font-weight: bold;"><%=VENL.getMvendorsAgenciesName(GDSTK.getvendorId()) %></td>
						</tr>
           </table>
			<div id="<%=GDSTK.getextra1()%>" style="display: none">
	  				<form action="godwanstockApproveInsert.jsp" method="post" onsubmit="formBlock();">
					<table width="100%" cellpadding="0" cellspacing="0" class="yourID">
						<tr>
							<td width="5%" style="font-weight: bold;"><input type="checkbox" id="selectall"/>Select All</td>
							<td  width="21%" style="font-weight: bold;">S.NO</td>
							<td  width="26%" style="font-weight: bold;">PRODUCT NAME.</td>
							<td  width="17%" style="font-weight: bold;">PRODUCT QTY</td>
							<td  width="10%" style="font-weight: bold;">PRODUCT QTY RECEIVED</td> 
							<%if(session.getAttribute("Role")!=null && session.getAttribute("Role").toString().equals("medicalhall")){ %><td  width="26%" style="font-weight: bold;">EXPIRY DATE</td> <%} %>
						</tr>
						<%for(int i=0; i < STOC_Apr.size(); i++ ){
							GSTK=(godwanstock)STOC_Apr.get(i); %>
						<tr>
							<td  style="font-weight: bold; text-align:center"><input class="checkbox1" type="checkbox" name="god_id" value="<%=GSTK.getgsId()%>"  <%if(i==0){ %> required <%} %> ></input></td>
							<td>
							
							<%=i+1%></td>
							<%-- <td><%=GSTK.getproductId() %>-<%=PROL.getProductsNameByCat(GSTK.getproductId(),"") %> --%>
							<td><%=GSTK.getproductId() %>-<%=PROL.getProductsNameByCat2(GSTK.getproductId(),GSTK.getdepartment()) %>
							<input type="hidden" name="prdid<%=GSTK.getgsId() %>" value="<%=GSTK.getproductId() %>" />
							</td>
							<td><%=GSTK.getquantity() %>
							<input type="hidden" name="poqnty<%=GSTK.getgsId()%>" value="<%=GSTK.getquantity() %>" />
							</td>
							<td><input type="text" name="recvdqnty<%=GSTK.getgsId()%>" value="<%=GSTK.getquantity()%>"/></td>
							<%if(session.getAttribute("Role")!=null && session.getAttribute("Role").toString().equals("medicalhall")){ %> <td> <input type="text" name="expirydate<%=GSTK.getgsId()%>" id="expirydate<%=GSTK.getgsId()%>" class="tabletexpirydate" value=""   required="required" readonly></input> </td><%} %>
						</tr>
						<%} %>
						<%if(session.getAttribute("Role")!=null && session.getAttribute("Role").equals("PO")){%>
							<tr>
							<td><input type="submit" name="submit" value="Raise Indent" class="click"></input></td>
							</tr>
						<%} %>
					</table>
					<div >
					<ul>
								<li style="list-style:none; margin:10px 0 0 0"><input type="submit"  name="search" value="Approve" class="click"></input>
								<input type="hidden" name="headAccountId" value="<%=GSTK.getdepartment() %>"/>
								<input type="hidden" name="invoiceId" value="<%=GDSTK.getextra1() %>"/>
								<input type="hidden" name="bilApprvStatus" value="<%=GDSTK.getExtra6() %>"/>
								</li>
					</ul>
				</div>
					</form>
					</div>
					<%} } } else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>Currently,there are no Stock for your Approval !</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>