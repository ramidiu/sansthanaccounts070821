<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@media print{
   .print_table{
    width: 100%;
    border-collapse: collapse;
	vertical-align: middle;
	page-break-after:avoid;
}
.print_table td{
    border-color: black;
    font-size: 12px !important;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
	
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
	}

</style>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRO OFFICE DAILY POOJAS LIST | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>

$(function() {
	var year = (new Date).getFullYear();	//current year
	$( "#fromDate" ).datepicker({
		minDate: new Date(year, 0, 1),
        maxDate: new Date(year, 11, 31),
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		minDate: new Date(year, 0, 1),
        maxDate: new Date(year, 11, 31),
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});


function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
/* $(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	}); */
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                    $( "a" ).css("text-decoration","none");
                    $( ".printable" ).print();
                   

                    // Cancel click event.
                    return( false );
                });
			});
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
    /* $( "#subheadId" ).change(function() {
		  $( "#searchForm" ).submit();
		}); */
});

function validateForm() 
{
    var subheadId = document.forms["searchForm"]["subheadId"].value;
    var email = document.forms["searchForm"]["email"].value;
    var fromDate = document.forms["searchForm"]["fromDate"].value;
    var toDate = document.forms["searchForm"]["toDate"].value;
    var splDay = document.forms["searchForm"]["splDay"].value;
    
    if (subheadId == null || subheadId == "") 
    {
        alert("Please select Seva Type");
        return false;
    }
    
    if (email == null || email == "") 
    {
        alert("Please select Email Type");
        return false;
    }
    
    if ((fromDate != null && fromDate != "" && toDate != null && toDate != "" && splDay != null && splDay != "") || (fromDate != null && fromDate == "" && toDate != null && toDate == "" && splDay != null && splDay == "")) 
    {
        alert("Please Select Either Performed Date Or Special Day");
        return false;
    }
} 

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="CAP" class="beans.customerapplication"/>
	<%if(session.getAttribute("empId")!=null){ 
		String splDay = request.getParameter("splDay"); 
	%>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">
        <div class="clear"></div>
		<div class="vendor-title" style="padding:30px 30px 50px 20px;">MOBILE E-MAIL LIST</div>
        
        
		<div class="vendor-list">
		
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form name="searchForm" id="searchForm" action="email-list.jsp" method="post" onsubmit="return validateForm()">
				<div class="search-list">
				<%
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dateFormat3= new SimpleDateFormat("dd-MMM-yyyy");
				DateFormat dateFormat4= new SimpleDateFormat("MM-dd");
				DateFormat dateFormat5= new SimpleDateFormat("dd MMM");
				
				String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
				String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
				
				if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals(""))
				{
					 fromdate=request.getParameter("fromDate");
					 todate=request.getParameter("toDate");
				}
				%>
					<ul>
					<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  readonly/></li>
					<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li> 
					
					<li>
				<select name="splDay" id="splDay" style="width:240px;">

		<option value="">-- SELECT --</option>
<option value="Guru poornima" >Guru poornima</option>
<option value="Dasara" >Dasara</option>
<option value="Sri Rama navami" >Sri Rama navami</option>
<option value="Datta jayanthi" >Datta jayanthi</option>
</select>
				</li>
					</ul><ul>	
					<li>
				<select name="email" id="email">
				<%if(request.getParameter("email") == null) {%><option value="">-- SELECT --</option><%} %>
				<option value="WithEmail"<%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithEmail")){ %> selected="selected" <%} %>>With Email</option>
				<option value="WithOutEmail" <%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithOutEmail")){ %> selected="selected" <%} %>>Without Email</option>
				<option value="WithMobile"<%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithMobile")){ %> selected="selected" <%} %>>With Mobile</option>
				<option value="WithOutMobile" <%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithOutMobile")){ %> selected="selected" <%} %>>Without Mobile</option>
				<option value="WithEmail&WithMobile"<%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithEmail&WithMobile")){ %> selected="selected" <%} %>>With Email & With Mobile</option>
				<option value="WithEmail&WithOutMobile" <%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithEmail&WithOutMobile")){ %> selected="selected" <%} %>>With Email & Without Mobile</option>
				<option value="WithOutEmail&WithMobile"<%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithOutEmail&WithMobile")){ %> selected="selected" <%} %>>Without Email & With Mobile</option>
				<option value="WithOutEmail&WithOutMobile" <%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithOutEmail&WithOutMobile")){ %> selected="selected" <%} %>>Without Email & Without Mobile</option>
				</select>
				</li>
					
						<li>
						<select name="subheadId" id="subheadId">
						<%if(request.getParameter("subheadId") == null) {%><option value="">-- SELECT --</option><%} %>
							<option value="20092"<%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20092")){ %> selected="selected" <%} %>>NITYA ANNADANA PADHAKAM</option>
							<option value="20063" <%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20063")){ %> selected="selected" <%} %>>LIFE TIME ARCHANA MEMBERSHIP</option>
							
						</select>
						</li>
						<li><input type="submit" class="click" name="search" value="Search"></input></li>
						
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					 <span>
						<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit Columns</dt>
										<dt><input type="checkbox" class="edit-setting" />Address</dt>
										<dt><input type="checkbox" class="edit-setting" />Email</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
		
				<div class="list-details" style="font-size:12px;">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	customerapplicationListing CA_L = new customerapplicationListing();
	
	//String subhead="20092";
	String subhead="";
	if(request.getParameter("subheadId")!=null && !request.getParameter("subheadId").equals("")){
		subhead=request.getParameter("subheadId");
	}
	
	String emails="";
	if(request.getParameter("email")!=null && !request.getParameter("email").equals("")){
		emails=request.getParameter("email");
	}
	
	String fromDate="";
	String toDate = "";
	
	if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("") && request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
		fromDate=request.getParameter("fromDate");
		fromDate=dateFormat4.format(dateFormat2.parse(fromDate));
		
		toDate=request.getParameter("toDate");
		toDate=dateFormat4.format(dateFormat2.parse(toDate));
	}
	else if(request.getParameter("splDay") != null && !request.getParameter("splDay").equals(""))
	{
		fromDate=request.getParameter("splDay");
	}
	
	List email_list=CA_L.getNADLTAhavingEmail(subhead,emails,fromDate,toDate);
	DecimalFormat df = new DecimalFormat("0.00");
	if(email_list.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:97%; 
    background:#d0e3fb; 
}

</style>

<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<div class="printable">
	<% if(request.getParameter("subheadId") != null && !request.getParameter("subheadId").equals("") && request.getParameter("email") != null && !request.getParameter("email").equals("") && ((request.getParameter("fromDate")!= null && !request.getParameter("fromDate").equals("") && request.getParameter("toDate") != null && !request.getParameter("toDate").equals("")) || (request.getParameter("splDay") != null && !request.getParameter("splDay").equals("")))){ %>
					<table class="print_table" cellspacing="0" cellpadding="0" id="tblExport">
                    <tr>
						<td class="bg" colspan="12" align="center" style="font-weight:bold">LIST OF DEVOTEES</td>
						</tr>
                       
                            
  <tr>
    <td width="4%" class="bg" align="center" style="font-weight:bold"><strong>S.NO</strong></td>
    <td width="5%"class="bg"  align="center" style="font-weight:bold"><strong>APP REF.NO</strong>.</td>
    <td width="10%"class="bg"  align="center" style="font-weight:bold"><strong>PERFORMED DATE(MONTH-DATE)</strong>.</td>
    <td width="7%" class="bg" align="center" style="font-weight:bold"><strong>RECEIPT.NO</strong>.</td>
    <td width="10%"class="bg" align="center" style="font-weight:bold"><strong>NAME</strong></td>
    <td width="21%" class="bg" align="center" style="font-weight:bold"><strong>SEVA PERFORM IN THE NAME OF</strong></td>
    <td width="21%" class="bg" align="center" style="font-weight:bold"><strong>E-MAIL</strong></td>
    <td width="10%" class="bg" align="center" style="font-weight:bold"><strong>MOBILE</strong></td>
    <td width="5%" class="bg" align="center" style="font-weight:bold"><strong>EDIT</strong></td>
    
  </tr>
  
 	<%
 	customerpurchasesListing SALE_L = new customerpurchasesListing();
 	List CUS_DETAIL=null;
	for(int i=0; i < email_list.size(); i++ ){ 
		CAP=(customerapplication)email_list.get(i);				
			CUS_DETAIL=SALE_L.getBillDetails(CAP.getbillingId());
			if(CUS_DETAIL.size()>0)
			{
				SALE=(customerpurchases)CUS_DETAIL.get(0);}%>
                   
  <tr>
    <td  align="center"><%=i+1%></td>
    <td align="center"><%=CAP.getphone()%></td>
    <td align="center"><%=CAP.getextra1()%></td>
    <td align="center" ><%=CAP.getbillingId()%></td>
    <td align="center" style="max-width:140px;"><%=CAP.getfirst_name()%></td>
  	<td align="center"><span><%=CAP.getlast_name()%></span></td>       
    <td align="center" ><%=SALE.getextra8()%></td>
    <td align="center" ><%=SALE.getphoneno()%></td>
    <td align="center"><a href="devoteesAddress_Edit.jsp?bid=<%=CAP.getbillingId()%>">Edit</a></td>   
  </tr>
  

 <%}%>
</table><%} %>



				  </div>
					<%
					}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no list found for your search!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>