<%@page import="sms.CallSmscApi"%>
<%@page import="java.text.ParseException"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@page import="beans.sssst_registrations"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import = "mainClasses.customerpurchasesListing"%>
    <%@page import = "mainClasses.customerapplicationListing"%>
    <%@page import = "beans.customerpurchases"%>
    <%@page import = "beans.customerapplication"%>
    <%@page import="sms.CallSmscApi"%>
    <%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="SALE" class="beans.customerpurchases" />
<jsp:useBean id="CAPP" class="beans.customerapplication" />
<%
sssst_registrations SSSST = new sssst_registrations();
sssst_registrationsListing SSREGL=new sssst_registrationsListing();

//CallSmsApi1 SMS = new CallSmsApi1();
CallSmscApi SMS=new CallSmscApi();

MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
customerpurchasesListing SALE_L = new customerpurchasesListing();
customerapplicationListing CAPL = new customerapplicationListing();
model.sendmailservice sendMail1=new model.sendmailservice();

String billingIds[]=request.getParameterValues("select1");
String template=request.getParameter("message").replace("'", "").replace("&", "").replace("nbsp;", " "); 
String subject=request.getParameter("subject").replace("'", "").replace("&", "");
String reqType=request.getParameter("submit");
String nadlta=request.getParameter("nadlta");
String groupsnadlta=request.getParameter("groupsnadlta");
String specialdays=request.getParameter("specialdays");
String bdayNanniv=request.getParameter("bdayNanniv");
String nadLtaReaminder=request.getParameter("nadLtaReaminder");

SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd");
SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MMM");

String mailBody="";

if(billingIds.length>0)
{
	for(int i=0;i<billingIds.length;i++)
	{
		String mobilenum = "";
		String email = "";
		String name = "";
		String nadOrLtaId = "";
		String perfDate = "";
		String details = "";
		String perfOnName = "";
		
		if(billingIds[i].contains("SSSST"))
		{
			List list=SSREGL.getMsssst_registrations(billingIds[i]);
			SSSST=(sssst_registrations)list.get(0);
			
			mobilenum = SSSST.getmobile().trim();
			email = SSSST.getemail_id().trim();
			name = SSSST.getfirst_name().trim();
		}
		else
		{
			List list=SALE_L.getBillDetails(billingIds[i]);
			SALE=(customerpurchases)list.get(0);
			
			mobilenum = SALE.getphoneno().trim();
			email = SALE.getextra8().trim();
			name = SALE.getcustomername().trim();
			
			List list2 = CAPL.getMcustomerDetailsBasedOnBillingId(billingIds[i]);
			if(list2.size()>0)
			{
				CAPP=(customerapplication)list2.get(0);
				nadOrLtaId = CAPP.getphone().trim();
				perfDate = CAPP.getextra1().trim();
				perfOnName = CAPP.getlast_name().trim();
				
				if(!nadOrLtaId.equals(""))
				{
					details += " (YOUR REF NO: "+nadOrLtaId;
				}
				if(!perfOnName.equals(""))
				{
					details += " ,PERFORMED ON NAME : "+perfOnName;
				}
				if(!perfDate.equals("") && !perfDate.equals("-"))
				{
					String dt = "";
					try{
					dt = dateFormat2.format(dateFormat1.parse(perfDate));
					}
					catch(ParseException parseEx){
						parseEx.printStackTrace();
					}
					details += " ,PERFORMED DATE : "+dt;
				}
					details += ")";
			}
		}
		
		 if("SEND SMS".equals(reqType) || "SEND SMS&MAIL".equals(reqType)){
			if(mobilenum!=null && !mobilenum.equals("")){
				SMS.SendSMS(mobilenum,template.replace(" ", "%20"));
				Thread.sleep(2000);
			}
		} 
		 if("SEND MAIL'S".equals(reqType) || "SEND SMS&MAIL".equals(reqType)){
			if(email != null && !email.equals("")){
				mailBody="<div style='box-shadow: 0px 0px 1px 0px; width:600px; margin:auto; font-family:arial; '><table width='600px' border='0' align='center' cellpadding='0' cellspacing='0' style='margin:0 auto; background:#f00'><tbody><tr><td width='600px'></td></tr></tbody> </table> <table width='600px' cellpadding='0' cellspacing='0' border='0' align='center'><tbody><tr> <td style='font-size:13px;line-height:1.4'><span style='color:#f98228; text-transform:capitalize; font-size:16px; font-weight:bold;' >Shri/Smt "+name+details+",</p><p> "+template+"</p><em style='font-size:12px;'>Kindly note this is system-generated mail.</em><p>With Best Wishes,<br /><strong>Saisanthan</strong></p></div></span>"; 
				/* sendMail1.sendmail("reports@saisansthan.in", email,subject ,mailBody); */
				sendMail1.sendmailsansthan("info@saisansthan.in", email,subject ,mailBody);
				Thread.sleep(8000);
			} 
		}			
	}		
}
	if(nadlta != null && nadlta.equals("nadlta"))
	{
		response.sendRedirect("sendsmsnadlta.jsp");
	}
	else if(groupsnadlta != null && groupsnadlta.equals("groupsnadlta"))
	{
		response.sendRedirect("group-list-nad-lta.jsp");
	}
	else if(specialdays != null && specialdays.equals("specialdays"))
	{
		response.sendRedirect("send-sms-special-days.jsp");
	}
	else if(bdayNanniv != null && bdayNanniv.equals("bdayNanniv"))
	{
		response.sendRedirect("AnniversaryBirthdayWishes.jsp");
	}
	else if(nadLtaReaminder != null && nadLtaReaminder.equals("nadLtaReaminder"))
	{
		response.sendRedirect("NadLtaRemainder.jsp");
	}
	else
	{
		response.sendRedirect("sendsms.jsp");
	}
%>
</body>
</html>