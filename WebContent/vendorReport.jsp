<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>VENDOR REPORTS</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
</head>
<body>
<div><%@ include file="title-bar.jsp"%></div>
<jsp:useBean id="GODW" class="beans.godwanstock"/>
<form><div class="vendor-box">
<div class="vender-details">
<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Master Stock Entry-With Out Purchase Order</td></tr>
				</table>
				<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());  %>
<form action="vendorReport.jsp">
<table width="60%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:50px;margin-left:250px;">
<tr>
<td>From Date<input type="text" name="fromDate" id="fromDate" value="<%=currentDate %>" style="width: 150px;" /></td>
<td>To Date<input type="text" name="toDate" id="toDate" value="<%=currentDate %>" style="width: 150px;" /></td>
<td><input type="submit" name="button" id="" value="submit"/></td>

</tr>
</table>
</form>
</div>
<div style="clear:both;"></div>
</div>
<%mainClasses.godwanstockListing GODS_l=new mainClasses.godwanstockListing();
mainClasses.vendorsListing VEND = new mainClasses.vendorsListing();

String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
	 fromdate=request.getParameter("fromDate")+" 00:00:01";
}
if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
	todate=request.getParameter("toDate")+" 23:23:59";
}
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("") && request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
%>
<table width="100%" cellpadding="10" cellspacing="10" border="1">
<tr>
<th class="bgcolr">S.NO.</th>
<th class="bgcolr">Vendor</th>
<th class="bgcolr">Vendor GSTNO.</th>
<th class="bgcolr">INVOICE NUM</th>
<th class="bgcolr">CGST</th>
<th class="bgcolr">SGST</th>
<th class="bgcolr">IGST</th>
<th class="bgcolr">Amount</th>
<th class="bgcolr">Invoice Date</th>
<!-- <th class="bgcolr">Payment Date</th> -->
</tr>
<%


List GODS_DET=GODS_l.getStockVendorDetailsBasedOnDate(fromdate,todate); 
System.out.println("GODS_DET.size()...."+GODS_DET.size());
if(GODS_DET.size()>0){
	for(int i=0;i<GODS_DET.size();i++){
	GODW=(beans.godwanstock)GODS_DET.get(i);
%>
<tr>
<td style="text-align:center;"><%=i+1%></td>
<td style="text-align:center;"><%=VEND.getMvendorsAgenciesName(GODW.getvendorId())%></td>
<td style="text-align:center;"><%=VEND.getGSTNum(GODW.getvendorId())%></td>
<td style="text-align:center;"><%=GODW.getextra1() %></td>
<td style="text-align:center;"><%=GODW.getExtra18() %></td>
<td style="text-align:center;"><%=GODW.getExtra19() %></td>
<td style="text-align:center;"><%=GODW.getExtra20() %></td>
<td style="text-align:center;"><%=GODW.getpurchaseRate() %></td>
<td style="text-align:center;"><%=GODW.getdate()%></td>
<!-- <td style="text-align:center;">10/11/2017</td> -->
</tr>
<%}} %>
</table>
<%} %>
</form>

</body>
</html>