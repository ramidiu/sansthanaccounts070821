<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>COLLECTION SUMMARY REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$('#'+billId).toggle();
	/* if(document.getElementById(billId).style.display=="none"){
		//document.getElementById(billId).style.display='block';
		$('#'+billId).toggle();
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	} */
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    
    <style>
	
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
/*.bod-nn{
	border-right:none !important;}*/
	}
/*	.bod-nn{
	border-right:1px solid #000 !important;
	font-weight: bold;}*/
</style>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<div class="sj">
					 <form action="collectionSummary-Report.jsp" method="post">  
						 <select name="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="5" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("5")){ %>selected="selected" <%} %>>SAI NIVAS</option>
						<option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %>selected="selected" <%} %>>POOJA STORES</option>
						<option value="of" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("of")){ %>selected="selected" <%} %>>OFFER KINDS</option> 
						</select>
						<%// ...........11111111111%>
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form name="searchForm" id="searchForm" action="collectionSummary-Report.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" class="click" name="search" value="Search"></input>
						<select name="headID" id="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="5" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("5")){ %>selected="selected" <%} %>>SAI NIVAS</option>
						<option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %>selected="selected" <%} %>>POOJA STORES</option>
						<option value="of" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("of")){ %>selected="selected" <%} %>>OFFER KINDS</option> 
						</select>
						
						</li>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					 <span>
						<!-- <ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul> -->

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	banktransactionsListing BKTRAL=new banktransactionsListing();
	productsListing PRDL=new productsListing();
	employeesListing EMPL=new employeesListing();
	
	List BANK_DEP=null;
	
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:00";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}
	else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	String hoaID=session.getAttribute("headAccountId").toString();
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoaID=request.getParameter("headID");
	}
	List SAL_list=null;
	if(hoaID.equals("of")){
		SAL_list=SALE_L.getOfferKindsPROCollectionSummary(hoaID,fromDate,toDate);
	}else{
		SAL_list=SALE_L.getcustomerPurchasesPROCollectionSummary(hoaID,fromDate,toDate);
	}
	List SAL_DETAIL=null;
	double totalAmount=0.00; 
	double totalUSDAmount=0.00; 
	double totalPoundAmount=0.00; 
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double bankDepositTot=0.00;
	double creaditSaleTot=0.00;
	double chequeSaleTot=0.00;
	double onlineSaleTot=0.00;
	double cashSaleAmt=0.00;
	double cardSaleAmt=0.00;

	double gPaySaleAmt=0.00;
	double phnpeSaleAmt=0.00;
	if(SAL_list.size()>0){%>
    <style>
.yourID.fixed {
    /* position: fixed; */
    top: 0;
    left: 25px;
    z-index: 1;
    width:92%; 
    background:#d0e3fb; 
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	 <div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
						<tr><td colspan="6" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td></tr>
						<tr><td colspan="6" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="6" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="6" align="center" style="font-weight: bold;">Collection Summary Report</td></tr>
						<tr><td colspan="6" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
                      
	<tr class="">
    <td colspan="6">
    	<table cellpadding="0" cellspacing="0" width="100%" class="print_table">
        	<tr>
							<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="14%" style="font-weight: bold;">CODE</td>
							<td class="bg" width="29%" style="font-weight: bold;">PARTICULARS</td>
							<td class="bg" width="7%" style="font-weight: bold;">QTY</td>
							<td class="bg" width="19%" style="font-weight: bold;">AMOUNT</td>
							<td class="bg" width="15%" style="font-weight: bold;">CREDIT</td>
                         </tr>
           </td>                 
                            
  </tr>

						<tr>
						
						<td colspan="6">
						<table width="100%" cellpadding="0" cellspacing="0"    class="print_table">
						
						<%-- <%Double totalSaleAmt=0.00;
						Double totDeposit=0.00;
						if(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate)!=null && !SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate).equals("")){
							 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate));
						}
						if(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate)!=null && !BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate).equals("")){
							 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate));}
						 %>
						<tr>
							<td colspan="3"></td>
							<td colspan="2">OPENING BALANCE</td>
							<td align="right" style="font-weight: bold;">&#8377; <%=df.format(totalSaleAmt-totDeposit)%></td>
							<td align="right" style="font-weight: bold;">&#8377;0.00</td>
						</tr> --%>
						<%for(int i=0; i < SAL_list.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_list.get(i);
							if(!presentDate.equals("")){
								prevDate=presentDate;	
							}
							presentDate=""+chngDateFormat.format(dbDateFormat.parse(SALE.getdate()));
							
							%>
						    <tr style="position: relative;">
								<td style="font-weight: bold;" width="5%"><%=i+1%></td> <!-- width="4%" -->
								<%--< td style="font-weight: bold;" width="8%"><%=SALE.getbillingId()%><%=chngDateFormat.format(dbDateFormat.parse(SALE.getdate()))%></td> --%>
								<td style="font-weight: bold;" width="14%"><%=SALE.getproductId()%></td> <!-- width="6%" -->
								<%if(PRDL.getProductsNameByCat(SALE.getproductId(),SALE.getextra1()).equals("")){ %>
									<td style="font-weight: bold;" width="29%"><span><a href="javascript:void(0)" onclick="showDetails('<%=SALE.getbillingId()%>')">
								<%=PRDL.getProductNameByCat(SALE.getproductId(),SALE.getextra1())%></a></span></td> <!-- width="16%" -->
									<%}else{ %>
									<td style="font-weight: bold;" width="29%"><span><a href="javascript:void(0)" onclick="showDetails('<%=SALE.getbillingId()%>')">
								<%=PRDL.getProductsNameByCat(SALE.getproductId(),SALE.getextra1())%></a></span></td> <!-- width="16%" -->
									<%} %>
								<td style="font-weight: bold;" width="7%"><%=SALE.getquantity()%></td> <!-- width="8%" -->
								<td style="font-weight: bold;" width="19%"><%if(SALE.getextra5().equals("USD")){ %>$<%}else if(SALE.getextra5().equals("POUND")){ %>&pound;<%}else{ %>&#8377;<%} %> <%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td><!-- width="20%" -->
								<td width="15%"></td>
						</tr>
						<%/* SAL_DETAIL=SALE_L.getSalesDetailsBasedOnCode(SALE.getproductId()); */
						if(hoaID.equals("of")){
							SAL_DETAIL=SALE_L.getOfferKindsPROCollectionSummaryOnProduct(hoaID,fromDate,toDate,SALE.getproductId());
						}else{
						SAL_DETAIL=SALE_L.getcustomerPurchasesPROCollectionSummaryBasedOnProduct(hoaID,fromDate,toDate,SALE.getproductId());
						}
						if(SAL_DETAIL.size()>0){ %>
						<tr>
							<td colspan="6">
							<div style="display: none;" id="<%=SALE.getbillingId()%>">
							<table width="100%" style="border: 1px solid #000;" border="1">
							<tr>
							<td width="4%">S.NO.</td>
							<td width="8%">DATE</td>
							<td width="8%">PERFORMED DATE</td>
							<td width="8%">CASH TYPE</td>
							<td width="7%">RECEIPT.NO</td>
							
							 <td width="10%">CHEQUE NO</td> 
							<td width="9%">NAME ON CHEQUE</td>
							<td width="8%">LAST 4 DIGITS OF CARD</td>
							<td width="8%">INVOICE NO</td>
							<td width="8%">TRANSACTION MOBILE NUMBER</td>
							<td width="8%">TRANSACTION UTR NUMBER</td>
							<td width="13%">DEVOTEE NAME</td>
							<td width="8%">DEVOTEE ADDRESS</td>
							<td width="8%">PAN NUM</td>
							<!-- <td width="20%">PARTICULAR</td> -->
							<td width="3%">QTY</td>
							<td width="6%">AMOUNT</td>
							<td width="8%">BANK AMOUNT</td>
							<td width="8%">NARRATIOIN</td>
							
						</tr>
						<%for(int s=0;s<SAL_DETAIL.size();s++){
								SA=(beans.customerpurchases)SAL_DETAIL.get(s);
								if(SA.getcash_type().equals("cash")){
									cashSaleAmt=cashSaleAmt+Double.parseDouble(SA.gettotalAmmount());
								} if(SA.getcash_type().equals("cheque")){
									chequeSaleTot=chequeSaleTot+Double.parseDouble(SA.gettotalAmmount());
								}
								if(SA.getcash_type().equals("card")){
									cardSaleAmt=cardSaleAmt+Double.parseDouble(SA.gettotalAmmount());
								}
								if(SA.getcash_type().equals("googlepay")){
									gPaySaleAmt=gPaySaleAmt+Double.parseDouble(SA.gettotalAmmount());
								}
				
								if(SA.getcash_type().equals("phonepe")){
									phnpeSaleAmt=phnpeSaleAmt+Double.parseDouble(SA.gettotalAmmount());
								}
				
								if(SA.getcash_type().equals("online success")){
									onlineSaleTot=onlineSaleTot+Double.parseDouble(SA.gettotalAmmount());
								}
								%>
								<tr style="position: relative;">
									<td width="3%"><%=s+1 %></td>
									<td width="7%"><%=chngDateFormat.format(dbDateFormat.parse(SA.getdate()))%></td>
									<%if(!SA.getextra3().equals("")) {%>
									<td width="7%"><%=chngDateFormat.format(dbDateFormat.parse(SA.getextra3()))%></td>
									<%}else{ %>
									<td width="7%">&nbsp;</td>
									<%} %>
									<td width="8%"><%=SA.getcash_type() %></td>
									<td width="8%"><%=SA.getbillingId() %></td>
									<%if(SA.getcheque_no() !=null && !SA.getname_on_cheque().equals("")){ %>
									<td width="7%"><%=SA.getcheque_no()%></td>
									<%}else{ %>
									<td width="7%"></td>
									<%} %>
									<%if(SA.getname_on_cheque() != null && !SA.getname_on_cheque().equals("")) {%>
									<td width="8%"><%=SA.getname_on_cheque() %></td>
									<%}else{ %>
									<td width="7%"></td>
									<%} %>
									<td width="6%"><%=SA.getextra10() %></td>
									<td width="7%"><%=SA.getextra14() %></td>
									<td width="10%"><%=SA.getExtra29()%><%-- TRANSACTION MOBILE NUMBER --%></td>
									<td width="10%"><%=SA.getExtra30()%><%-- TRANSACTION UTR NUMBER --%></td>
									<td width="12%"><%=SA.getcustomername() %></td>
									<td width="10%"><%=SA.getextra6()%></td>
									<td width="10%"><%=SA.getExtra18()%></td>
									<%-- <%if(PRDL.getProductsNameByCat(SALE.getproductId().trim(), SALE.getextra1()).equals("")){ %>
									<td width="20%"><%=PRDL.getProductNameByCat(SALE.getproductId(), SALE.getextra1())%></td>
									<%}else{ %>
									<td width="20%"><%=PRDL.getProductsNameByCat(SALE.getproductId().trim(), SALE.getextra1())%></td>
									<%} %> --%>
									<%if(SA.getextra5().equals("USD")){
								totalUSDAmount=totalUSDAmount+Double.parseDouble(SA.gettotalAmmount());
								System.out.println("totalAmount...."+totalUSDAmount);
							}else if(SA.getextra5().equals("POUND")){
								totalPoundAmount=totalPoundAmount+Double.parseDouble(SA.gettotalAmmount());
							} else{
								totalAmount=totalAmount+Double.parseDouble(SA.gettotalAmmount());
							}
							%>
									<td width="4%"><%=SA.getquantity()%></td>
									<%-- <td><%=SA.getrate() %></td> --%>
									<td width="10%"><%if(SA.getextra5().equals("USD")){ %>$<%}else if(SA.getextra5().equals("POUND")){ %>&pound;<%}else{ %>&#8377;<%} %> <%=df.format(Double.parseDouble(SA.getrate()))%></td>
									<%-- <td><%=SA.gettotalAmmount()%></td> --%>
									<td width="10%"><%if(SA.getextra5().equals("USD")){ %>$<%}else if(SA.getextra5().equals("POUND")){ %>&pound;<%}else{ %>&#8377;<%} %> <%=df.format(Double.parseDouble(SA.gettotalAmmount()))%></td>
									<td width="10%"><%=SA.getextra2()%><%-- <%=EMPL.getMemployeesName(SA.getemp_id()) %> --%></td>
								
								
								</tr>
								<%} %>
						</table>
						</div>
						</td>
						</tr>
					   <%}} %>
					   <%-- <%
					   String amtDepositedCat="pro-counter";
					   if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
							if(request.getParameter("headID").equals("1")){
								amtDepositedCat="Charity-cash";
							}else if(request.getParameter("headID").equals("4")){
								amtDepositedCat="pro-counter";
							}
						}
					   List BankDeposits=BKTRAL.getDepositsListBasedOnHead(amtDepositedCat, fromDate,toDate);
					   if(BankDeposits.size()>0){
					   		for(int j=0;j<BankDeposits.size();j++){
					   		BNK=(banktransactions)BankDeposits.get(j);
					   		bankDepositTot=bankDepositTot+Double.parseDouble(BNK.getamount());
					   %>
					   <tr style="color: fuchsia;">
					   		<td style="font-weight: bold;" width="5%"><%=j+1%></td>
					   		<td style="font-weight: bold;" width="15%"><%=chngDateFormat.format(dbDateFormat.parse(BNK.getdate()))%></td>
					   		<td style="font-weight: bold;" width="30%"><%=BNK.getbanktrn_id()%></td>
					   		<td  colspan="2" style="font-weight: bold;" width="10%"><%=BNK.getnarration() %></td>
					   		<!-- <td style="font-weight: bold;" width="5%"></td> -->
                            <!-- <td style="font-weight: bold;" width="25%"></td> -->
					   		<td style="font-weight: bold;" align="right" width="15%">&#8377; <%=df.format(Double.parseDouble(BNK.getamount()))%></td>
					   </tr> 
					   
					   <%}} %> --%>
					   </table>
					   </td>
					   </tr>
                       <tr>
                     <table width="100%" cellpadding="0" cellspacing="0">  
                       
					   <tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total Cash sales amount | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &#8377; <%=df.format(cashSaleAmt)%></td>
						<td style="font-weight: bold;" align="right" ></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total Card sales amount | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &#8377; <%=df.format(cardSaleAmt)%></td>
						<td style="font-weight: bold;" align="right" ></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total BharathPe sales amount | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &#8377; <%=df.format(gPaySaleAmt)%></td>
						<td style="font-weight: bold;" align="right" ></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total PhonePe sales amount | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &#8377; <%=df.format(phnpeSaleAmt)%></td>
						<td style="font-weight: bold;" align="right" ></td>
						</tr>
						
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total Cheque sales amount | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &#8377; <%=df.format(chequeSaleTot)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total Online sales amount | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &#8377; <%=df.format(onlineSaleTot)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total sales amount in (&#x20B9;) | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &#8377; <%=df.format(cashSaleAmt+cardSaleAmt+chequeSaleTot+onlineSaleTot+gPaySaleAmt+phnpeSaleAmt)%> </td>
						<td style="font-weight: bold;" align="right" ></td>
						</tr>
						<%if(totalUSDAmount>0){ System.out.println("totalUSDAmount....."+totalUSDAmount);%>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total sales amount in (USD-$) | </td>
						<td style="color:#000;font-weight: bold;" align="right"> $ <%=df.format(totalUSDAmount)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr>
						<%} %>
						<%if(totalPoundAmount>0){ %>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total sales amount in (Pounds &pound;) | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &pound; <%=df.format(totalPoundAmount)%></td>
						<td style="font-weight: bold;" align="right"></td>
						</tr><%} %>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Total Bank Deposit amount | </td>
						
						<td style="color: #000;font-weight: bold; " align="right"></td>
						<td style="font-weight: bold;" align="right"> &#8377; <%=df.format(bankDepositTot)%></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="4" align="right" style="font-weight: bold;">Closing Balance | </td>
						<td style="color: #000;font-weight: bold;" align="right"> &#8377; <%=df.format((gPaySaleAmt+phnpeSaleAmt+cardSaleAmt+chequeSaleTot+onlineSaleTot)-bankDepositTot) %></td>
						<td style="font-weight: bold;"></td>
						</tr>
                        
                        </table>
                        </tr>
					</table>
				</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no collection summary report founds for today!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>