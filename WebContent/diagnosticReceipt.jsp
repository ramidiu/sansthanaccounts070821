<%@page import="java.util.List"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.patientsentry"%>
<%@page import="mainClasses.patientsentryListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.appointments"%>
<%@page import="mainClasses.appointmentsListing"%>
<%@page import="beans.diagnosticissues" %>
<%@page import ="mainClasses.diagnosticissuesListing" %>
<%@page import="mainClasses.subheadListing" %>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<jsp:useBean id="APT" class="beans.appointments"></jsp:useBean>
<jsp:useBean id="PAT" class="beans.patientsentry"></jsp:useBean>
<jsp:useBean id="DIA" class="beans.diagnosticissues"></jsp:useBean>
<jsp:useBean id="SUB" class="beans.subhead"></jsp:useBean>
<title>Medical Receipt</title>
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>

<style>
@media print {
	@page  
{ 
 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin:2.6	cm 0 0 15mm; /* equals to 0.8inches */ 
}
.medicalsheet
{
	width:800px;  height:auto; padding:10px; background:#3F6; border:none !important;
}

.medicalsheet table
{
	border:none !important;
}

.medicalsheet table td
{
	 padding:7px 0px; text-align:center; font-size:15px; color:#000; text-transform:uppercase;  border:none !important;
}

.medicalsheet table td span
{
	font-size:15px; font-weight:bold;
}

.medicalsheet table td ul
{
	margin:0px; padding:10px; float:left;
}

.medicalsheet table td ul li
{
	list-style:none; float:left; margin:0px 0 45px 0; background:#F00; font-size:15px;
}

.medicalsheet table td ul li.br1
{
 width:350px; margin:0 15px 10px 15px; border:none !important;
}

.medicalsheet table td ul li.br2
{
 width:240px !important;margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br3
{
 width:112px; margin:0 0px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br4
{
 width:350px; margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br5
{
 width:25px;margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br6
{
 width:78px; margin:0 0px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br7
{
 width:380px; margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br8
{
 width:200px; margin:0 0px 10px 15px;  border:none !important;
}
.medicalsheet table td ul li.br9
{
 width:150px; margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br10
{
width:150px; margin:0 15px 10px 15px; border:none !important;
}

.medicalsheet table td ul li.br11
{
 width:293px; margin:0 0px 10px 15px border:none !important;
}
.medicalsheet table td.ht
{
	height:940px;
}
.medicalsheet table td.ht1
{
	height:1210px;
}

}
</style>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
</head>

<body onload="print();">
<%
NumberToWordsConverter NTW=new NumberToWordsConverter();
SimpleDateFormat DBdate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat userdate=new SimpleDateFormat("dd MMMM yyyy");
SimpleDateFormat Time=new SimpleDateFormat("HH:MM:ss");
appointmentsListing APT_L=new appointmentsListing();
patientsentryListing PAT_L=new patientsentryListing();
doctordetailsListing DOCT_L=new doctordetailsListing();
diagnosticissuesListing DIA_L=new diagnosticissuesListing();
subheadListing listing=new subheadListing();
List DIAL=DIA_L.getMamountBasedOnAppoitment(request.getParameter("id").toString());
if(DIAL.size()>0){
DIA=(diagnosticissues)DIAL.get(0);
}
List APTL=APT_L.getappointmentsTestissuse(request.getParameter("id").toString());
if(APTL.size()>0){
APT=(appointments)APTL.get(0);
}
List PATL=PAT_L.getMpatientsentry(APT.getpatientId());
if(PATL.size()>0){
PAT=(patientsentry)PATL.get(0);
}

String billingId=request.getParameter("billingId");
System.out.println("printing page billingId:::"+billingId);
String paidFree=request.getParameter("paidFree");
System.out.println("printing page paidFree:::"+paidFree);
%>
<div class="vendor-page">
	<div>
<!-- <div class="icons"><a id="print"><span><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
				<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
				</div> -->
<div class="printable">
<div>
<!--
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="4" align="center">**SHRI SHIRDI SAIBABA SANSTHAN TRUST**</td></tr>
<tr><td colspan="4" align="center" class="smallfont">Regd.No 646/92,Dilsukhnagar,Hyderabad-500060 PH:24066566</td></tr>
</table>
-->

<table width="100%" align="center"  cellpadding="0" cellspacing="0">
  <tr>
  	<td  rowspan="2" style="width:100px;"></td><!-- <img src="images/sai_sai.png" width="100" height="120" /> -->
  	
    <td width="300" rowspan="2" style="width:120px;"><img src="images/saibaba.png" width="50" height="60" /></td>
    <td width="1000"><center><p style="font-size:25px">**SHRI SHIRDI SAIBABA SANSTHAN TRUST**</p>
      <p><strong><b style="font-size:15px">Regd.No 646/92,Dilsukhnagar,Hyderabad-500060 PH:24066566</b></strong></p>
     <!--  <p>PH:24066566</p> --></center>
      <p style=" padding-left:110px; font-size:18px; padding-top:20px;">Free Medical Center</p>
     
      </td>
     
  </tr>
  
  
</table>

</div>


</div> 
</div>
	<table width="100%"  cellpadding="0" cellspacing="0" bordercolor="#000">
	  <tr style="border:1px solid #333;">
	    <td width="51%" height="26" style="padding-left:10px;border-right:1px solid #000;"> App Id :<%=APT.getappointmnetId() %></td>
	    <%if(APT.getdate()!=null && !APT.getdate().equals("")){ %><td width="49%" align="left" style="padding-left:5px;">Reciept No:<%=billingId%></br>Date :<%=userdate.format(DBdate.parse(APT.getdate()))%></td><% }%>
      </tr>
	  <tr style="border:1px solid #333;">
	  	<%doctordetailsListing   doctordetailsListingObj=new doctordetailsListing();
	  	//System.out.print("===="+APT.getdoctorId());
	  	String doctorName=doctordetailsListingObj.getDoctorName(APT.getdoctorId());
	  	%>
	  	 <td width="51%" height="26" style="padding-left:10px;border-right:1px solid #000;" >Patient Name :<%=PAT.getpatientName() %><br>Mobile Number: <%=PAT.getphone()%></br></td>
	  	 <%-- <td width="51%" height="26" style="padding-left:10px;border-right:1px solid #000;" >Patient Name :<%=PAT.getphone()%></td> --%>
	    <td  width="49%" align="left" style="padding-left:5px;">Doctor Name :<%=doctorName%></br>Test type:<%=paidFree%></td>
	    </tr>
	    <tr style="border:1px solid #333;">
       <td  width="51%" height="26" style="padding-left:10px;border-right:1px solid #000;" >
       	 <table>
       	 	<tr>
       	 		<td width="75%" height="26" style="padding-left:1px;border-right:1px solid #000;">Gender :<%=PAT.getextra1()%> </td>
       	 		<td width="24%" align="left" style="padding-left:5px;">Age :<%=PAT.getextra3() %></td></tr>
       	 </table>
       </td>
	    <td width="49%" align="left" style="padding-left:5px;"></td>
	    </tr>
	  <%-- <tr style="border:1px solid #333;">
	    <td width="51%" height="26" style="padding-left:10px;">Gender : <%=PAT.getextra1()%></td>
	     <td width="51%" height="26" style="padding-left:10px;">Age : <%=PAT.getextra3()%></td>
	    <td width="49%" align="left"></td>
      </tr> --%>
	  <tr style="border:1px solid #333;">
	    <td width="51%" height="29" style="padding-left:10px;">Address :<%=PAT.getaddress()%></td>
	    <td width="49%" align="left" style="padding-left:5px;"></td>
      </tr>
     
     <tr style="border:1px solid #333;">
	    <td  height="29" style="padding-left:10px;">Amount :Rs.<span style="display:block;float:right;"></span></td>
	    <td width="49%" align="left" style="padding-left:15px;"><%=DIA.getExtra3() %></td>
      </tr>
     
     
     <tr style="border:1px solid #333; width: 100%">
	    <td width="51%" colspan="2" height="29" style="padding-left:10px;">Amount in Words :<%=NTW.convert(Integer.parseInt(DIA.getExtra3())) %> Rupees Only</td>
	  <!--   <td width="49%" align="left" style="padding-left:5px;"></td> -->
      </tr>
      
      <tr style="border:1px solid #333;">
	    <td>
	    	<ul style="list-style:none; line-height:35px; padding-left:10px;">
	    	<%List list=DIA_L.getMtestissueBasedOnAppoitment(request.getParameter("id").toString());
	    		for(int i=0;i<list.size();i++){
	    			DIA=(diagnosticissues)list.get(i);
	    			%>
	    		<li><%=i+1%>.<%=listing.getSuheadName(DIA.getTestName())%></li>
	    		<!-- <li>2.</li>
	    		<li>3.</li> -->
	    		<%} %>
	    	</ul>
	    </td>
	    <td></td>
	   
      </tr>
     
      <%-- <tr>
    		<td width="51%" align="left" style="padding-left:10px; padding-top:30px;">R<span style="text-transform:lowercase;:">X</span> : <%=APT.getpurpose()%></td>
            <td width="49%" align="left" style="padding-left:5px;"></td>
 	 </tr> --%>

     
  </table>
  
  
	<p>&nbsp;</p>
	<div  style="margin-top:150px;"></div>
	
	<!-- <div style="width: 33%; float:left;">
		<p style="text-align: center;">Manager(accts)</p>
	</div> -->
	<div style="width: 33%; float:left;"><p style="text-align: center;">(Manager Services) </p></div>
	<div style="width: 33%; float:left;"><p style="text-align: center;">Receiver's Signature</p></div>
	
</div>            

</body>
</html>