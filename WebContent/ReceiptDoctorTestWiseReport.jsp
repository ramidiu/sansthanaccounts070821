<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>   
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.diagnosticissuesListing"%>
<%@page import="java.util.List"%>
<%@page import="beans.diagnosticissues"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ReceiptDoctorTestWiseReport</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<style>

</style>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( ".printable" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
</head>
<body>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-list">	
        <div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span>
					
				</div>
				<div class="search-list">
				
			   

			   <%
			       String toDayDate="";
			   	   SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			   	   Calendar calendar=Calendar.getInstance();
			   	   toDayDate=dateFormat.format(calendar.getTime());
			   %>
			      <form action="ReceiptDoctorTestWiseReport.jsp">
					<ul>
					
						<li><label>From Date:&nbsp;</label><input type="text" name="fromDate" id="fromDate" class="DatePicker " value="<%=toDayDate%>"></li>
						<li><label>To Date:&nbsp;</label><input type="text" name="toDate" id="toDate" class="DatePicker " value="<%=toDayDate%>"></li> 
					     <li>SortBy &nbsp;&nbsp;<select name="sortBy" id="sortBy">
					     	<option value="Receipt wise">Receipt wise</option>
					     	<option value="Doctor wise">Doctor wise</option>
					     	<option value="Test wise">Test wise</option>
					     </select>
					     </li>
					     <li><input type="submit" class="click"></li>
					</ul>
					</form>
					
				</div>
				<table width="100%" cellpadding="0" cellspacing="0" >
<tbody><tr>
  
</tr>
<%
 String reportFromDate=""; 
 String reportToDate="";
if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){
	reportFromDate=request.getParameter("fromDate");
}

if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
	reportToDate=request.getParameter("toDate");
}
if((reportFromDate!=null && !reportFromDate.equals("")) && (reportToDate!=null && !reportToDate.equals(""))){
%>
<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</td>
<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<!-- <tr><td colspan="7" align="center" style="font-weight: bold;">Receipt Doctor Test Wise Report</td></tr> -->
<tr><td colspan="7" align="center" style="font-weight: bold;">Report From&nbsp;<%=reportFromDate%>&nbsp;To&nbsp;<%=reportToDate%></td></tr>
<%} %>
</tbody>
</table>
				
				<div class="clear"></div>
				<div class="list-details" >
				<table width="100%" cellpadding="0" cellspacing="0">
						<tbody><tr>
						
						</tr><tr><td colspan="9"><table width="100%" cellpadding="0" cellspacing="0"  class="yourID">
<tbody><tr>
</tr>
</tbody>
</table>
						<div class="printable">
						
				      
			</div>
			

</td></tr></tbody></table>

<%
     String fromDate="";
     String toDate="";
     String sortBy="";
      if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){
    	  fromDate=request.getParameter("fromDate");
    	  fromDate=fromDate+" 00:00:00";
    	  System.out.println("fromDate:::"+fromDate);
      }
      
      if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
         toDate=request.getParameter("toDate");
         toDate=toDate+" 23:59:59";
         System.out.println("toDate:::::"+toDate);
      }
      if(request.getParameter("sortBy")!=null && request.getParameter("sortBy")!=""){
    	  sortBy=request.getParameter("sortBy");
    	  System.out.println("sortby::::::"+sortBy);
      }
      if((fromDate!=null && !fromDate.equals("") && (toDate!=null && !toDate.equals("")))){
      diagnosticissuesListing diagonsticIssuesListing=new diagnosticissuesListing();
      List<diagnosticissues>   diagonsticList=diagonsticIssuesListing.getReceiptDoctorTestWise(fromDate, toDate, sortBy);
      System.out.println("diagonsticList:::::"+diagonsticList.size());
       if(diagonsticList.size()>0 &&  diagonsticList!=null){
     
%>

<table width="100%" cellpadding="0" cellspacing="0" border="1" id="tblExport">
<tbody><tr>
    <td class="bg" width="10%">Created Date</td>
	<td class="bg" width="15%">Receipt Number</td>
	<td class="bg" width="15%">Patient Id</td>
	<td class="bg" width="10%">Patient Name</td>
	<td class="bg" width="10%">AppointmentId</td>
	<td class="bg" width="15%">Doctor Name</td>
	<td class="bg" width="15%">Test Name</td>
	<td class="bg" width="15%">Test Type</td>
	<td class="bg" width="15%">Patient Amount</td>
	<td class="bg" width="20%">SansthanAmount</td>
	<td class="bg" width="15%">Remarks</td>
</tr>
<%

 double patientAmount=0.0;
double sansthanAmount=0.0;

double patientTotalAmount=0.0;
double sansthanTotalAmount=0.0;

 for(int i=0;i<diagonsticList.size();i++){ 
	 diagnosticissues diagnostic=diagonsticList.get(i);
	 patientAmount=Double.parseDouble(diagnostic.getExtra3());
	 patientTotalAmount=patientTotalAmount+patientAmount;
	 sansthanAmount=Double.parseDouble(diagnostic.getExtra2());
	 sansthanTotalAmount=sansthanTotalAmount+sansthanAmount;
	 
%>
<tr>
	
	<td><%=dateFormat.format(dateFormat.parse(diagnostic.getDate()))%></td>
	<td><%=diagnostic.getDocumentNo() %>1</td>
	<td><%=diagnostic.getPatientId() %></td>
	<td><%=diagnostic.getPatientName() %></td>
	<td><%=diagnostic.getAppointmnetId()%></td>
	<td><%=diagnostic.getDoctorName()%></td>
	<td><%=diagnostic.getTestName() %></td>
	<td><%=diagnostic.getExtra4()%></td>
	<td><%=patientAmount%></td>
	<td><%=sansthanAmount%></td>
	<td><%=diagnostic.getExtra5()%></td>
	
</tr>
<tr>

<%
 }
%>
<td colspan="8" align="left" style="    background-color: #bf802b;color: #fff;font-weight: bold;"><b>Total</b></td>
<td><%=patientTotalAmount%></td>
<td><%=sansthanTotalAmount%></td>
<td></td>
</tr>
</tbody>
</table>
<%
       }else{
       	%>
   	 <center><span style="color:red;font-family: verdana;font-size: 250%;"><strong>No Records Are available</strong></span></center>
   	<% 
   }
  }
%>
</div></div></div>
</body>
</html>
