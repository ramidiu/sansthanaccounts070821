<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Indent Raise</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!-- <script src="js/jquery-1.4.2.js"></script>
Date picker script 
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script> -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<script>
$(function(){
	$( "#indentDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	/* $( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	}); */
	
	$.ajax({
		url : "searchProd.jsp",
		type : "POST",
		data : {category : "getCategories"},
		success : function(response)	{
			var data = response.trim().split("\n");
			$('#category').append( $('<option>', {value: "",text: "Select Category" }) );
			if (Number(response) !== 0)	{
				for (var i = 0 ; i < data.length ; i++)	{
					$('#category').append( $('<option>', {value: data[i],text: data[i].toUpperCase() }) );
				}
			}
		}
	});
	
	$('#category').change(function(){
		var category = $(this).val();
		if ($.trim(category) !== "")	{
			$.ajax({
				url : "searchProd.jsp",
				type : "POST",
				data : {category : category , subcategory : ""},
				success : function(response)	{
					var data = response.trim().split("\n");
					$('#subcategory').empty();
					$('#subcategory').append( $('<option>', {value: "",text: "Select Sub Category" }) );	
					if (Number(response) !== 0)	{
						for (var i = 0 ; i < data.length ; i++)	{
							$('#subcategory').append( $('<option>', {value: category+","+data[i],text: data[i].toUpperCase() }) );
						}
					}
				}
			});
		}
		else	{
			$('#subcategory').empty();
		}
	});
	
	$('#subcategory').click(function(){
		var category = $('#category').val();
		if ($.trim(category) === "")	{
			alert("Please Select Category");
			$('#category').focus();
		}
	});
	
	$('#getProducts').click(function(){
		var data = $('#subcategory').val();
		var date = $('#indentDate').val();
		var hoa = $('#headAccountId').val();
		if ($.trim(data) !== "")	{
			$.ajax({
				url : "searchProd.jsp",
				type : "POST",
				data : {category : data.split(",")[0] , subcategory : data.split(",")[1] , date : date , hoa : hoa},
				success : function(response)	{
					var data = response.trim().split("\n");
					if (Number(response) === 0)	{
						alert("No Products Found Under This Category");
					}
					else	{
						$('#tableShow').empty();
						var table = "<table class='table table-bordered' style='width: 100%;margin: 48px auto 0;' id='table'>";
						table += "<tr><td style='font-size: 11px;font-weight: bold;'>Check</td><td style='font-size: 11px;font-weight: bold;'>ProductId</td><td style='font-size: 11px;font-weight: bold;'>ProductName</td><td style='font-size: 11px;font-weight: bold;'>MajorHead</td><td style='font-size: 11px;font-weight: bold;'>MinorHead</td><td style='font-size: 11px;font-weight: bold;'>SubHead</td><td style='font-size: 11px;font-weight: bold;'>Consumption<br>(last 15 days)</td><td style='font-size: 11px;font-weight: bold;'>ClosingStock</td><td style='font-size: 11px;font-weight: bold;'>RequiredQty</td><td style='font-size: 11px;font-weight: bold;'>Remarks</td></tr>";
						for (var i = 0 ; i < data.length ; i++)	{
							var d = data[i].split(",");
							 table += "<tr>";
							 table += "<td style='font-size: 11px;font-weight: bold;'><input type='checkbox' id='check"+(i)+"'></td>";
							 /* table += "<td><input type='text' id='vendorId"+i+"' name='vendorId"+i+"' onkeyup='vendorsearch("+i+")' ></td>"; */
							 table += "<td style='font-size: 11px;font-weight: bold;'>"+d[0]+"</td><input type='hidden' name='product"+i+"' id='product"+i+"' value='"+d[0]+"'>";
							 table += "<td style='font-size: 11px;font-weight: bold;'>"+d[1]+"</td>";
							 table += "<td style='font-size: 11px;font-weight: bold;'>"+d[7]+"</td>";
							 table += "<td style='font-size: 11px;font-weight: bold;'>"+d[8]+"</td>";
							 table += "<td style='font-size: 11px;font-weight: bold;'>"+d[9]+"</td>";
							 table += "<td style='font-size: 11px;font-weight: bold;'>"+d[2]+"</td>";
							 table += "<td style='font-size: 11px;font-weight: bold;'>"+d[3]+"</td>";
							 table += "<td><input type='text' name='quantity"+i+"' id='quantity"+i+"' onkeypress='return numbersonly(this,event,true);' style='width:65%'></td>";
							 table += "<td style='font-size: 11px;font-weight: bold;'><input type='text' name='remarks"+i+"' id='remarks"+i+"'></td>";
							 table += "</tr>";
						}
						table += "<tr><td colspan='10' align='center'><input type='button' value='CREATE' id='submitBtn' class='click' onclick='return checkData();'></td></tr>"
						table += "<input type='hidden' value="+data.length+" id='count' name='count'>"
						table += "</table>";
						$('#tableShow').append(table);
						$('#tableShow').show();
					}
				}
			});
		}
		else	{
			alert("Please Select Sub Category");
			$('#subcategory').focus();
			$('#tableShow').empty();
			$('#tableShow').hide();
		}
	});
	
	$('#submitBtn').click(function(){

	});
});
</script>
<script>
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function vendorsearch(){
	 var data1=$('#vendorId').val();
	  var headID=$('#headAccountId').val();
	  if($('#headAccountId').val().trim()==""){
			$('#billError').show();
			$('#headAccountId').focus();
			return false;
		}
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
		{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $('#vendorId').autocomplete({source: availableTags}); 
	});
}

function checkData()	{
	var d = new Date();
	if(d.getDate() >= 5 && d.getDate() <=20 )	{
		var count = $('#count').val();
		var length = $("input[type='checkbox']:checked").length;
		var vendor = $('#vendorId').val();
		if ($.trim(vendor) === "")	{
			alert("Please Enter Vendor");
			$('#vendorId').focus();
			return false;
		}
		if( length < 1 )	{
			alert("Check Atleast One Product to Continue");
			return false;
		}
		else	{
			for (var i = 0 ; i < count ; i++)	{
				if ($("#check"+i).is(':checked'))	{
					var quantity = $('#quantity'+i).val();
					if ($.trim(quantity) === "" || Number(quantity) <= 0)	{
						alert("Please Enter Quantity Required");
						$('#quantity'+i).focus();
						return false;
					} // if
				}
				else	{
					var quantity = $('#quantity'+i).val("");
					var vendor = $('#vendorId'+i).val("");
				}// if
			} // for
			$('#form1').submit();
		}
	}	
	else	{
		alert("Indent can't be raised");
		return false;
	}
}
</script>
<body>
<%if(session.getAttribute("empId")!=null){%>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<div><%@ include file="title-bar.jsp"%></div>
<form id="form1" action="indent_Insert1.jsp" method="post">
<div class="vendor-page">
<div class="vendor-box">
<div class="vender-details">
<table width="70%" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
<tr><td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<tr><td colspan="7" align="center" style="font-weight: bold;">Create New Indent</td></tr>
<tr>
<td>Indent Invoice No</td>
<td><div class="warning" id="billError" style="display: none;">Please select head of account.</div>Head of account*</td>
<td>Date</td>
<td>Description</td>
</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
mainClasses.no_genaratorListing NG_l=new mainClasses.no_genaratorListing();
String indentinvoice_id="";

if(session.getAttribute("headAccountId").toString().equals("5")){
	indentinvoice_id=NG_l.getidWithoutUpdatingNoGen("sainivas_indent_invNum");
}else{
	indentinvoice_id=NG_l.getidWithoutUpdatingNoGen("indentinvoicenumber");
}
%>
<tr>
<td><input name="invoiceId" value="<%=indentinvoice_id%>" readonly="readonly"></input></td>
<td>
<%employeesListing EMP_l=new employeesListing();
String EMP_HOA=EMP_l.getMemployeesHOA(session.getAttribute("empId").toString());

mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getActiveHOA();
%>
<select name="headAccountId" id="headAccountId" style="width:200px;">
<%if(session.getAttribute("headAccountId").toString().equals("5")){ %>
<option value="5"  selected="selected" >SHRI SAI NIVAS</option>
<%}else if(session.getAttribute("Role").toString().equals("medicalhall")){ %>
<option value="1"  selected="selected">CHARITY</option>
<%}else{ %>
<%if(HOA_List.size()>0){
	for(int i=0;i<HOA_List.size();i++){
	HOA=(headofaccounts)HOA_List.get(i);%>	
	<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname()%></option>
<%
}
	}
}%>
</select>
</td>
<td><input type="text" name="indentDate" id="indentDate" value="<%=currentDate %>" readonly="readonly" style="width: 70%"></input></td>
<td><textarea  name="narration" id="narration"  class="form-control"></textarea></td>
</tr>
<tr>
<td>Requirement Type</td>
<td>Category</td>
<td>Sub-Category</td>
</tr>
<tr>
<td>
<select name="requirement" id="department">
<option value="Standard">Standard</option>
<option value="Urgent">Urgent</option>
<option value="specific Date">Specific Date</option>
<option value="EmergencyApproval">Emergency Approval</option>
</select>
</td>
<td>
<select name="category" id="category">

</select>
</td>
<td>
<select name="subcategory" id="subcategory"></select>
</td>
<td>
<input type="button" id="getProducts" value="GET PRODUCTS" class="click">
</td>
</tr>
<tr>
<td>Vendor</td>
</tr>
<tr>
<td><input type='text' id='vendorId' name='vendorId' onkeyup='vendorsearch()' ></td>
</tr>
</table>
<div id="tableShow" style="display: none;">

</div>
</div>
</div>
</div>
</form>	
<%}
else	{
	response.sendRedirect("index.jsp");
}%>
</body>
</html>