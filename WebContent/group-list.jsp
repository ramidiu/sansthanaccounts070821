<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="beans.customerapplication"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Group List</title>
<style>
.click{
	position: relative;
    top: 25px;
}
</style>
<!-- <link href="../js/date.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/> -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>

<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<!-- <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:92%; 
    background:#d0e3fb; 
}

@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}

	}


</style> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
 <script src="js/jquery-1.4.2.js"></script>
<script src="js/nicEdit-latest.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>

<!-- <script src="../js/jquery-1.8.2.js"></script> -->

<Script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        } 
    });
    
});
</Script>
<script type="text/javascript">bkLib.onDomLoaded(function() {
    nicEditors.editors.push(new nicEditor().panelInstance(document.getElementById('long_desc')));
    $("div.nicEdit-main").keydown(function(event) {
        if (event.ctrlKey==true && (event.which == '118' || event.which == '86')) {
		//alert('key presse');
		event.preventDefault();
		modalWindowRTF.windowId = "myModal";
		modalWindowRTF.width = 550;
		modalWindowRTF.height = 260;
		modalWindowRTF.open();
	 }
    });
 });
function validate(){
	var nicE = new nicEditors.findEditor('long_desc');
	var question = nicE.getContent();
	$("#long_desc").val(question); // Value What we entered should be passed to another page.
	}
</script>
<script language = "JavaScript">

//----for check atleast one check box-----
function check1(){
	var inputs = document.getElementsByTagName("input");
	var msg=document.groupForm.message.value;
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one");
	return false;} 
	
	else{
		return true;
	}
}

function formsumit(){ 
	document.reg.submit();}
</script>

 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

</head>
<body>
<%if(session.getAttribute("empId")!=null){ 
	 String group = request.getParameter("group"); 
%>
<div><%@ include file="title-bar.jsp"%></div>
<form name="reg" id="reg" method="post" action="group-list.jsp">
<table></table>
<table style="margin-left:80px;margin-top: 100px;" width="100%"><tr><td width="10%"><label>Select Group Type </label><br> <select name="group" >
<%
	if(group == null)
	{%>
		<option value="">-- SELECT --</option>
	<%} 
%>
<option value="DONOR" <%if(group != null && group.equals("DONOR")) {%> selected="selected"<%} %>>DONOR</option>
<option value="TRUSTEES" <%if(group != null && group.equals("TRUSTEES")) {%> selected="selected"<%} %>>TRUSTEES</option>
<option value="RDS" <%if(group != null && group.equals("RDS")) {%> selected="selected"<%} %>>RDS</option>
<option value="TIME_SHARE" <%if(group != null && group.equals("TIME_SHARE")) {%> selected="selected"<%} %>>TIME SHARE</option>
<option value="EMPLOYEE" <%if(group != null && group.equals("EMPLOYEE")) {%> selected="selected"<%} %>>EMPLOYEE</option>
<option value="GENERAL" <%if(group != null && group.equals("GENERAL")) {%> selected="selected"<%} %>>GENERAL</option>
<option value="VOLUNTEER" <%if(group != null && group.equals("VOLUNTEER")) {%> selected="selected"<%} %>>VOLUNTEER</option>
<option value="online" <%if(group != null && group.equals("online")) {%> selected="selected"<%} %>>ONLINE</option>
</select></td>
<%
      Calendar calendar=Calendar.getInstance();
      SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
      
      
      
      SimpleDateFormat dateForStringToDate=new SimpleDateFormat("yyyy-MM-dd");
      SimpleDateFormat monthDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
      String toDayDate=dateFormat.format(calendar.getTime());
      
      
%>
<td width="15%"><label>From Date</label><br><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=toDayDate%>"></td>
<td width="18%"><label>To Date</label><br><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=toDayDate%>"></td>
<td width="34%"><input type="submit" class="click" name="search" value="Search" onclick="formsumit()"></td>
</tr></table></form> 
<% if(group != null){%>  
<jsp:useBean id="CAPPOBJ" class="beans.customerapplication" />
<jsp:useBean id="CAPPL" class="mainClasses.customerapplicationListing" />

<div class="vendor-page">
<div class="vendor-list">
<div class="icons">
	<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
	<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>

<div class="clear"></div>


<%

String fromDate="";
String toDate="";
String reportFromDate="";
String reportToDate="";

if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){

	fromDate=request.getParameter("fromDate");
	reportFromDate=fromDate;
	
	fromDate=fromDate+" 00:00:00";
}

if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
	toDate=request.getParameter("toDate");
	reportToDate=toDate;
	toDate=toDate+" 23:59:59";
	
}



%>
<div class="list-details">
	 <div class="printable">
 <form name="groupForm" method="POST" action="sendMessageGroups.jsp" onSubmit="return check1();">	
	 <table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
			<tr><td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold">Report From Date  <%=reportFromDate%> TO <%=reportToDate%></td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;"><%=group %> unique Registrations List</td></tr>
			
			<tr>
			<jsp:useBean id="REG"  class="beans.sssst_registrations"></jsp:useBean>
			<td colspan="8">
				<table width="100%" cellpadding="0" cellspacing="0"  class="print_table">
					 
            <tr class="print_table" style="background-color: #e3eaf3;color: #7490ac">
            	<td align="left"   width="11%" style="font-weight: bold;"><input type="checkbox" name="SelectAll" id="selecctall"/>Check All</td>
				<td align="left"   width="5%" style="font-weight: bold;">S.NO.</td>
				<td align="left"   width="10%" style="font-weight: bold;">UNIQUE ID</td>
				<td align="left"   width="23%" style="font-weight: bold;">NAME</td>
				<td align="left"   width="13%" style="font-weight: bold;">PHONE NO</td>
				<td align="left"   width="23%" style="font-weight: bold;">EMAIL-ID</td>
				<td align="left"   width="15%" style="font-weight :bold;">RegisteredDate</td>
				<td align="left"  width="15%" style="font-weight :bold;">Address</td>
				<td align="left"   width="15%" style="font-weight: bold;">PANCARD</td>
				
				
			</tr>
			
			<jsp:useBean id="UI" class="beans.UniqueIdGroup"/>
	<jsp:useBean id="SSSST" class="beans.sssst_registrations"/> 
	
	<input type="hidden" name="group" value="<%=group%>">
<%
	mainClasses.uniqueIdGroupsListing UIGL = new mainClasses.uniqueIdGroupsListing();
	mainClasses.sssst_registrationsListing SSSSTL = new mainClasses.sssst_registrationsListing();
	
	List UIG_List = null;
	List SSSST_List = null;

	
	if(group.equals("online"))
	{
		SSSST_List=SSSSTL.getOnlinesssstregistrations(fromDate,toDate);
	}
	
	else
	{
		UIG_List=UIGL.getUniqueIdBasedOnGroupDates(group,fromDate,toDate);
	}
	
	int k = 0;
	
	if(SSSST_List == null)
	{
		for(int i=0; i < UIG_List.size(); i++ )
			
		{
			//System.out.println("i...."+i);
			UI=(beans.UniqueIdGroup)UIG_List.get(i);
			SSSST_List=SSSSTL.getMsssst_registrations(UI.getUniqueid().toString());
			for(int G=0; G < SSSST_List.size(); G++ ){
				SSSST=(beans.sssst_registrations)SSSST_List.get(G);
				if(!SSSST.getmobile().trim().equals("") || !SSSST.getemail_id().trim().equals(""))
				{ 
				k+=1;
				String fromatedDate=monthDateFormat.format(dateForStringToDate.parse(SSSST.getregister_date()));
				%>
					<tr style="position: relative;">
					<td align="left" width="11%"><input type="checkbox" name="select1" class="checkbox1" value = "<%=SSSST.getsssst_id()%>"></td>
				 	<td align="left" width="5%"><%=k%></td>				 						 	
				 	<td align="left" width="10%"><%=SSSST.getsssst_id() %></td>
				 	<td align="left" width="23%"><%=SSSST.getfirst_name() %> <%=SSSST.getlast_name() %></td>
				 	<td align="left" width="13%"><%=SSSST.getmobile() %></td>
				 	<td align="left" width="23%"><%=SSSST.getemail_id() %></td>	
				 	<td align="left" width="15%"><%=fromatedDate%></td>	 
				 	<td align="left" width="15%"><%=SSSST.getaddress_1() %></td>					 	
					<td align="left" width="15%" ><%=SSSST.getpancard_no() %></td>
					
				 </tr>	
				<%}}}	%>
<% }
	else
	{
		for(int G=0; G < SSSST_List.size(); G++ ){
			SSSST=(beans.sssst_registrations)SSSST_List.get(G);
			if(!SSSST.getmobile().trim().equals("") || !SSSST.getemail_id().trim().equals(""))
			{ 
			k+=1;
			String fromatedDate=monthDateFormat.format(dateForStringToDate.parse(SSSST.getregister_date()));
			%>
				<tr style="position: relative;">
				<td align="left" width="11%"><input type="checkbox" name="select1" class="checkbox1" value = "<%=SSSST.getsssst_id()%>"></td>
			 	<td align="left" width="5%"><%=k%></td>
			 	<td align="left" width="10%"><%=SSSST.getsssst_id() %></td>
			 	<td align="left" width="23%"><%=SSSST.getfirst_name() %> <%=SSSST.getlast_name() %></td>
			 	<td align="left" width="13%"><%=SSSST.getmobile() %></td>
			 	<td align="left" width="23%"><%=SSSST.getemail_id() %></td>
			 	<td align="left" width="15%"><%=fromatedDate%></td>	
			 	<td align="left" width="15%"><%=SSSST.getaddress_1()%></td>	
			 	<td align="left" width="15%"><%=SSSST.getpancard_no() %></td>
			 			 	
			 </tr>	
			<%}}			
	}
	%>

	</table>
			</td>
			</tr>
	</table>
	<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
<td colspan="3" align="right" >Template</td>
<td colspan="3" align=left>
	<textarea name="message" id="long_desc" rows="4" cols="40" placeHolder="Write your message here!"   ></textarea>
	<br><span>Note: Enter 160 characters message only!</span>
	<br><input type="text" name="subject" value="" placeHolder="Write E-Mail Subject here!"/>
</td>
<td colspan="4" align="left">
<input type="submit" class="click" name="submit" value="SEND SMS"><br/> 
<!-- <input type="submit" name="submit" class="click" value="SEND MAIL'S"><br/> 
<input type="submit" name="submit" class="click" value="SEND SMS&MAIL"></td> -->
</tr>
	</table>
	</form>
	 </div>
</div>

</div>
</div>

<%}}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>

</body>
</html>