<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat,java.text.DecimalFormat"%>
<style>
.print-rep
	{
		width:400px; height:auto; top:0px; position:absolute; left:0px;
	}
	.print-rep table 
	{
	padding:0px 0px 0px 0px; background:#fdf6fd;
		
	}
	
	.print-rep table td
	{
		padding:5px;
	  /*  text-transform:capitalize;*/
		font-size:16px;
	}
	.print-rep table td .border
	{
		
		border-bottom:1px dotted #000;
	}
	
	.print-rep table td .bordertop
	{
		
		border-top:1px dotted #000 ;
		
	}
	 .print-rep .smallfont{
		font-size:18px;
		font-weight:bold;
		font-family: Verdana, Geneva, sans-serif;
		text-transform:uppercase;
	}
@media print
{
	.print-rep
	{
		width:400px; height:auto;
	}
	.print-rep table 
	{
	padding:0px 20px 40px 0px; background:#fdf6fd;
		
	}
	
	.print-rep table td
	{
		padding:5px;
		text-transform:capitalize;
		font-size:16px;
		font-family: Verdana, Geneva, sans-serif;
	}
	.print-rep table td .border
	{
		
		border-bottom:2px dotted #000;
	}
	
	.print-rep table td .bordertop
	{
		
		border-top:1px dotted #000;
		
	}
}

</style>


<%@ page import="java.util.*" %> 
<jsp:useBean id="CSP" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="CAP" class="beans.customerapplication"></jsp:useBean>
<jsp:useBean id="GODW" class="beans.godwanstock"/>
<%

if(request.getParameter("bid")!=null && !request.getParameter("bid").equals("")){
	Calendar c1 = Calendar.getInstance(); 
	c1.setTimeZone(TimeZone.getTimeZone("IST"));
	double items=0;
	int totqnt=0;
	double totamnt=0.0;
	double discamnt=0.0;
	TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat  dateFormat= new SimpleDateFormat("dd/MM/yyyy");
	DateFormat  dbdate= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat  dateFormat2= new SimpleDateFormat("HH:mm:ss");
	DateFormat  dateFormat3= new SimpleDateFormat("MMMM-dd");
	DateFormat  dateFormat4= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  dbdate1= new SimpleDateFormat("MM-dd");
	dbdate.setTimeZone(tz.getTimeZone("IST"));
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	dateFormat2.setTimeZone(tz.getTimeZone("IST"));
	DecimalFormat df = new DecimalFormat("#.##");
	DecimalFormat DF = new DecimalFormat("0.00");
	mainClasses.customerpurchasesListing CUSPL=new mainClasses.customerpurchasesListing();
	customerapplicationListing CAPL=new customerapplicationListing();
	mainClasses.productsListing PROL=new mainClasses.productsListing();
	mainClasses.subheadListing SUBHL=new mainClasses.subheadListing();
	NumberToWordsConverter NTW=new NumberToWordsConverter();
	String billingId=request.getParameter("bid");
	List TICDET=CUSPL.getBillDetails(billingId);
	List TICDET1=CAPL.getMcustomerDetailsBasedOnBillingId(billingId);
	String time="";
	time = c1.get(Calendar.HOUR) + " : " + c1.get(Calendar.MINUTE);
	if(c1.get(Calendar.AM_PM)==0)
        time=time+" AM";
    else
        time=time+" PM";
	if(request.getParameter("recName").equals("sevaticket")){
	if(TICDET.size()>0){ 
		CSP=(beans.customerpurchases)TICDET.get(0);
	}
	if(TICDET1.size()>0){ 
		CAP=(customerapplication)TICDET1.get(0);
	}
	
	%>
<body  onload="window.print();">
<div class="print-rep" onload="window.print();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="4" class="smallfont" style="font-size:14px;">**SHRI SHIRDI SAIBABA SANSTHAN TRUST**</td></tr>
<tr>
  <td colspan="4" align="center" class="smallfont" style="font-size:14px;">Regd.No 646/92</td></tr>
  <tr><td colspan="4" align="center" class="smallfont"> Dilsukhnagar,Hyderabad-500060 PH:24066566</td></tr>
  <tr><td colspan="4" align="center" style="font-size:18px; text-transform:lowercase;font-weight:bold;"> www.saisansthan.in </td></tr>
<tr>
  <td colspan="4" align="center" class="smallfont"> RECEIPT</td></tr>
<tr>
  <td colspan="4" class="smallfont">Receipt No : <%=request.getParameter("bid") %></td></tr>
<tr>
  <td colspan="4" class="smallfont">Cashier : <%=session.getAttribute("empName")%></td></tr>
<tr>
  <td colspan="4" class="smallfont">Date : <%=dateFormat.format(c1.getTime()) %> - <%=time %></td></tr>
  
<%if(CSP.getextra15() != null && !CSP.getextra15().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Unique ID</td>
<td colspan="2" class="smallfont">: <%=CSP.getextra15()%></td>
</tr>
<%} %>
<%if(CSP.getphoneno() != null && !CSP.getphoneno().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Mobile No.</td>
<td colspan="2" class="smallfont">: <%=CSP.getphoneno()%></td>
</tr>
<%} %>
  
<tr><td colspan="4" class="smallfont"><div class="bordertop"></div></td></tr>
<tr>
<td colspan="2" class="smallfont">Received with thanks from Smt/Sri</td>
<td colspan="2" class="smallfont">: <%=CSP.getcustomername() %></td>
</tr>
<%if(!CSP.getgothram().trim().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Gothram</td>
<td colspan="2" class="smallfont">: <%=CSP.getgothram() %></td>
</tr><%}%>
<tr>
<td colspan="2" class="smallfont">Towards</td>

<%if(SUBHL.getMsubheadnames(CSP.getproductId(),CSP.getextra1()).equals("")){ %>
<td colspan="2" class="smallfont">: <%=PROL.getProductNameByCat(CSP.getproductId(),CSP.getextra1())%></td>
<%}else{ %>
<td colspan="2" class="smallfont">: <%=SUBHL.getMsubheadnames(CSP.getproductId(),CSP.getextra1())%></td>
<%}%>
</tr>
<%if(CSP.getcash_type().equals("offerKind") && !CSP.getextra11().equals("") && CSP.getproductId().equals("20026")){ %>
<tr>
<td colspan="2" class="smallfont">From Date</td>
<td colspan="2" class="smallfont">: <%=dateFormat.format(dateFormat4.parse(CSP.getextra11()))%></td>
</tr>
<tr>
<td colspan="2" class="smallfont">Time</td>
<td colspan="2" class="smallfont">: <%=CSP.getextra13()%></td>
</tr>
<%} %>
<%if(CSP.getcheque_no()!= null && !CSP.getcheque_no().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Cheque No</td>
<td colspan="2" class="smallfont">: <%=CSP.getcheque_no()%></td>
</tr>
<%} %>
<%if(CSP.getname_on_cheque()!= null && !CSP.getname_on_cheque().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Name On Cheque</td>
<td colspan="2" class="smallfont">: <%=CSP.getname_on_cheque()%></td>
</tr>
<%} %>
<%if(CAPL.getPerformerName(CSP.getbillingId())!=null && !CAPL.getPerformerName(CSP.getbillingId()).equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Perform in the name of</td>
<td colspan="2" class="smallfont">: <%=CAPL.getPerformerName(CSP.getbillingId())%></td>
</tr>
<%} %>
<%if(CAP.getextra1()!=null && !CAP.getextra1().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Performed Date</td>
<td colspan="2" class="smallfont">: <%=dateFormat3.format(dbdate1.parse(CAP.getextra1()))%></td>
</tr>
<%}else if(CSP.getextra7() != null && !CSP.getextra7().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Performed Date</td>
<td colspan="2" class="smallfont">: <%=CSP.getextra7()%></td>
</tr>
<%} %>
<%-- <%if(CSP.getphoneno() != null && !CSP.getphoneno().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Mobile No.</td>
<td colspan="2" class="smallfont">: <%=CSP.getphoneno()%></td>
</tr>
<%} %> --%>
<%if(CSP.getextra3() != null && !CSP.getextra3().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">From </td>
<td colspan="2" class="smallfont">: <%=dateFormat.format(dbdate.parse(CSP.getextra3()))%></td>
</tr>
<%}
if(CSP.getextra4()!=null && !CSP.getextra4().equals("") && (CSP.getproductId().equals("20066") || CSP.getproductId().equals("21585") || CSP.getproductId().equals("21553")
|| CSP.getproductId().equals("21552") || CSP.getproductId().equals("25302") || CSP.getproductId().equals("25304") || CSP.getproductId().equals("21545") || CSP.getproductId().equals("21550") || CSP.getproductId().equals("21551") || CSP.getproductId().equals("21544") || CSP.getproductId().equals("21555") 
|| CSP.getproductId().equals("21548") || CSP.getproductId().equals("21547"))){%>
<tr>
<td colspan="2" class="smallfont">To </td>
<td colspan="2" class="smallfont">: <%=dateFormat.format(dbdate.parse(CSP.getextra4()))%></td>
</tr>
<%} %>
<%if(CSP.getextra6()!=null && !CSP.getextra6().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Address</td>
<td colspan="2" class="smallfont">: <%=CSP.getextra6()%></td>
</tr>
<%} %>
<%if(CAP.getextra2()!=null && !CAP.getextra2().equals("")){ %>
<tr>
<td colspan="2" class="smallfont"></td>
<td colspan="2" class="smallfont">&nbsp;<%=CAP.getextra2()%></td>
</tr>
<%} %>
<%if(CAP.getaddress2()!=null && !CAP.getaddress2().equals("")){ %>
<tr>
<td colspan="2" class="smallfont"></td>
<td colspan="2" class="smallfont">&nbsp;<%=CAP.getaddress2()%></td>
</tr>
<%} %>
<%if(CAP.getextra3()!=null && !CAP.getextra3().equals("")){ %>
<tr>
<td colspan="2" class="smallfont"></td>
<td colspan="2" class="smallfont">&nbsp;<%=CAP.getextra3()%></td>
</tr>
<%} %>
<%if(CAP.getpincode()!=null && !CAP.getpincode().equals("")){ %>
<tr>
<td colspan="2" class="smallfont"></td>
<td colspan="2" class="smallfont">&nbsp;<%=CAP.getpincode()%></td>
</tr>
<%} %>
<%if(CSP.getextra2()!=null && !CSP.getextra2().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Purpose</td>
<td colspan="2" class="smallfont">: <%=CSP.getextra2()%></td>
</tr>
<%} %>
<%-- <%if(CSP.getextra15() != null && !CSP.getextra15().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">Unique ID</td>
<td colspan="2" class="smallfont">: <%=CSP.getextra15()%></td>
</tr>
<%} %> --%>
<%if(CSP.getofferkind_refid() != null && !CSP.getofferkind_refid().equals("")){ %>
<tr>
<td colspan="2" class="smallfont">OfferKind Ref No</td>
<td colspan="2" class="smallfont">: <%=CSP.getofferkind_refid()%></td>
</tr>
<%} %>
<%if(CSP.getextra5().equals("RS")){ %>
<tr>
<td colspan="2" class="smallfont">Rupees</td>
<td colspan="2" class="smallfont">: <%=DF.format(Double.parseDouble((CSP.gettotalAmmount())))%></td>
</tr>
<%}else if(CSP.getextra5().equals("USD")){ %>
<tr>
<td colspan="2" class="smallfont">Dollars</td>
<td colspan="2" class="smallfont">: $ <%=DF.format(Double.parseDouble((CSP.gettotalAmmount())))%></td>
</tr>
<%}else if(CSP.getextra5().equals("POUND")){ %>
<tr>
<td colspan="2" class="smallfont">Pounds</td>
<td colspan="2" class="smallfont">: &pound; <%=DF.format(Double.parseDouble((CSP.gettotalAmmount())))%></td>
</tr>
<%}else if(CSP.getcash_type().equals("offerKind")){ %>
<tr>
<td colspan="2" class="smallfont">Rupees</td>
<td colspan="2" class="smallfont">: <%=DF.format(Double.parseDouble((CSP.gettotalAmmount())))%></td>
</tr>
<%} %>
<tr>
<td colspan="2" class="smallfont">Amount In Words</td>
<td colspan="2" class="smallfont">: <%=NTW.convert(Integer.parseInt(CSP.gettotalAmmount())) %> Only</td>
</tr>
<tr><td colspan="4" class="smallfont" ><div class="bordertop"></div></td></tr>
<tr><td colspan="4" height="20" class="smallfont"></td></tr>
<tr>
<td colspan="2" align="left" class="smallfont">Signature</td>
<td colspan="2" align="right" class="smallfont">Treasurer</td>
</tr>
<tr><td colspan="4" style="text-align: center;" class="smallfont">**WITH BLESSINGS OF SAIBABA**</td></tr>
</table>
</div>
</body>
<%}else if(request.getParameter("recName").equals("shopsale")){
	
	%>
<body  onload="window.print();">
<div class="print-rep" onload="window.print();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="5" align="center">**SHRI SHIRDI SAIBABA SANSTHAN TRUST**</td></tr>
<tr><td colspan="5" align="center">**POOJA STORES**</td></tr>
<tr><td colspan="5" align="center" class="smallfont">(Managed by shri shirdi saibaba sansthan trust)</td></tr>
<tr><td colspan="5" align="center" class="smallfont">Dilsukhnagar,hyderabad-500060 PH:24066566</td></tr>
<tr><td colspan="5" align="center"> RECEIPT</td></tr>
<tr>
<td colspan="2">Bill Number: <%=request.getParameter("bid") %></td>
<td colspan="3">Cashier : <%=session.getAttribute("empName")%></td></tr>
<tr>
<td colspan="2">Date:  <%=dateFormat.format(c1.getTime()) %></td>
<td colspan="3">Time : <%=time %></td></tr>


<tr><td colspan="5"><div class="border"></div></td></tr>
<tr>
<td>ITEM NAME</td>
<td>QTY</td>
<td>PRICE</td>
 <td>VAT</td> 
<td>AMOUNT</td>
</tr>
<tr><td colspan="5" ><div class="bordertop"></div></td></tr>
<%

double zeroPer=0.00;
double fivePer=0.00;
double fourteenPer=0.00;
if(TICDET.size()>0){ 
	for(int i=0;i<TICDET.size();i++){
	CSP=(beans.customerpurchases)TICDET.get(i);
	if(PROL.getSellingVat(CSP.getproductId()).equals("0")){
		zeroPer=zeroPer+(Double.parseDouble(CSP.getquantity())*Double.parseDouble(PROL.getProductsAmount(CSP.getproductId(), "3")));
	}else if(PROL.getSellingVat(CSP.getproductId()).equals("5")){ 
		fivePer=fivePer+(Double.parseDouble(CSP.getquantity())*Double.parseDouble(PROL.getProductsAmount(CSP.getproductId(), "3")));
	}else if(PROL.getSellingVat(CSP.getproductId()).equals("14.5")){ 
		fourteenPer=fourteenPer+(Double.parseDouble(CSP.getquantity())*Double.parseDouble(PROL.getProductsAmount(CSP.getproductId(), "3")));
	}
	%>
<tr> 
<td><%if(PROL.getProductsNameByCat(CSP.getproductId(), "3")!=null && !PROL.getProductsNameByCat(CSP.getproductId(), "3").equals("")){ %> <%=PROL.getProductsNameByCat(CSP.getproductId(), "3") %><%}else{ %> <%=SUBHL.getMsubheadname(CSP.getproductId()) %> <%} %> </td>
<td align="center"><%=CSP.getquantity()%></td>
<td align="center"><%if(PROL.getProductsAmount(CSP.getproductId(), "3")!=null && !PROL.getProductsAmount(CSP.getproductId(), "3").equals("")){ %> <%=PROL.getProductsAmount(CSP.getproductId(), "3") %> <%}else{%> <%=CSP.getrate()%> <%} %> </td>
<td align="center"> <%=PROL.getSellingVat(CSP.getproductId()) %></td>
<td align="center"><%=CSP.gettotalAmmount() %>
<% 
items=items+Double.parseDouble(CSP.getquantity());
totamnt=totamnt+Double.parseDouble(CSP.gettotalAmmount());%>
</td>
</tr>
<%}} %>
<tr><td colspan="5"><div class="bordertop"></div></td></tr>

<tr><td colspan="1">Total </td>
<td align="center"><%=items %></td>
<td colspan="2"></td>
<td colspan="1" align="center"><%=Math.ceil(totamnt)%> </td>
</tr>
 <tr><td colspan="1">Tot Paid : </td>
<td ><%=CSP.gettendered_cash()%></td>
<td colspan="2">Change Due :</td>
<td colspan="1" align="center"> <%=(int)(Double.parseDouble(CSP.gettendered_cash())-totamnt)%> </td>
</tr>
<tr><td colspan="3"></td>
<td colspan="2" align="right"></td>
</tr>
<tr><td colspan="5"><div class="border"></div></td></tr>
<tr> <td>VAT DESC</td>  <td>SALES</td> <td>VALUE</td> <td colspan="2" align="right">VAT AMT</td>  </tr>
<tr> <td>0.000%</td>  <td align="right"><%=Math.ceil(zeroPer) %></td> <td>@ 0%</td> <td colspan="2" align="right"><%=Math.ceil((zeroPer*0)/100)%></td>  </tr>
<tr> <td>5.000%</td>  <td align="right"><%=Math.ceil(fivePer) %></td> <td>@ 5%</td> <td colspan="2" align="right"> <%=Math.ceil((fivePer*5)/100)%></td>  </tr>
<tr> <td>14.500%</td>  <td align="right"><%=Math.ceil(fourteenPer) %></td> <td>@14.5%</td> <td colspan="2" align="right"><%=Math.ceil((fourteenPer*14.5)/100)%></td>  </tr>
 <tr><td colspan="5" class="smallfont">TIN NUMBER-3642139840</td></tr>
<tr><td colspan="5" height="10"></td></tr>
<tr><td colspan="5" align="center">**THANK YOU! VISIT AGAIN**</td></tr>
<tr><td colspan="5" height="20"></td></tr>
<tr>
<td colspan="3" class="smallfont">With Blessings of Saibaba</td>
<td colspan="1" align="center" class="smallfont">Signature</td>
<td colspan="1" align="right" class="smallfont">Treasurer</td>
</tr>
<tr><td colspan="5"><div class="border"></div></td></tr>
<tr>
<td colspan="3" class="smallfont">Goods once sold can't be taken return</td>

<td colspan="2" align="right" class="smallfont">*No Exchange</td>
</tr>
</table>
</div>
</body>
<%} else if(request.getParameter("recName").equals("purchaseEntry")){
	double othercharges=0.00;
	mainClasses.vendorsListing VEND = new mainClasses.vendorsListing();
	mainClasses.godwanstockListing GODS_l=new mainClasses.godwanstockListing();
	List GODS_DET=GODS_l.getStockDetailsBasedOnInvoiceId(request.getParameter("bid"));
	if(GODS_DET.size()>0){
		GODW=(beans.godwanstock)GODS_DET.get(0);
	}
	if(!GODW.getExtra8().equals("")){
	othercharges=Double.parseDouble(GODW.getExtra8());
	}
	%>
<body  onload="window.print();">
<div class="print-rep" onload="window.print();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="5" align="center">**SHRI SHIRDI SAIBABA SANSTHAN TRUST**</td></tr>
<%if(!GODW.getExtra9().equals("")){ %>
<tr>
<td colspan="3">Unique No: <%=GODW.getExtra9()%></td>
<td colspan="2"> </td></tr>
<%} %>
<tr>
<td colspan="3">MSE Number: <%=request.getParameter("bid") %></td>
<td colspan="2">Date : <%=dateFormat.format(dbdate.parse(GODW.getdate())) %></td></tr>
<tr>
<td colspan="3">Vendor Name: <%=VEND.getMvendorsAgenciesName(GODW.getvendorId())%> </td>
<td colspan="2">Bill No : <%=GODW.getbillNo()%></td></tr>

<tr><td colspan="7"><div class="border"></div></td></tr>
<tr>
<td>ITEM NAME</td>
<td>QTY</td>
<td>PRICE</td>
<!-- <td>VAT</td> -->
<td>CGST</td>
<td>SGST</td>
<td>IGST</td>
<td>AMOUNT</td>
</tr>
<tr><td colspan="7" ><div class="bordertop"></div></td></tr>
<%
String amount="";
double vatAmt=0.00;
double subtotal=0.0;
double zeroPer=0.00;
double fivePer=0.00;
double twelvePer=0.00;
double eighteenPer=0.00;
double twntyeightPer=0.00;
if(GODS_DET.size()>0){ 
	for(int i=0; i <  GODS_DET.size(); i++ ){
		GODW=(beans.godwanstock)GODS_DET.get(i);
		if(GODW.getExtra17().equals("0")){
			zeroPer=zeroPer+(((Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19())+Double.parseDouble(GODW.getExtra20())))*Double.parseDouble(GODW.getquantity()));
		}else if(GODW.getExtra17().equals("5.0") || GODW.getExtra17().equals("5")){ 
			fivePer=fivePer+(((Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19())+Double.parseDouble(GODW.getExtra20())))*Double.parseDouble(GODW.getquantity()));
		}else if(GODW.getExtra17().equals("12")){ 
			twelvePer=twelvePer+(((Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19())+Double.parseDouble(GODW.getExtra20())))*Double.parseDouble(GODW.getquantity()));
		}
		else if(GODW.getExtra17().equals("18")){ 
			eighteenPer=eighteenPer+(((Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19())+Double.parseDouble(GODW.getExtra20())))*Double.parseDouble(GODW.getquantity()));
		}
		else if(GODW.getExtra17().equals("28")){ 
			twntyeightPer=twntyeightPer+(((Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19())+Double.parseDouble(GODW.getExtra20())))*Double.parseDouble(GODW.getquantity()));
		}
	%>
<tr>
<%if(GODW.getExtra7()!=null&&GODW.getExtra7().equals("serviceBill")){ %>
<td><%=GODW.getproductId()%> <%=SUBHL.getMsubheadnameWithDepartment(GODW.getproductId(),GODW.getdepartment())%> @ <%=GODW.getExtra17() %>%</td>
<%}else{ %>
<td><%=GODW.getproductId()%> <%=PROL.getProductsNameByCat2(GODW.getproductId(),GODW.getdepartment())%> @ <%=GODW.getExtra17() %>%</td><%} %>
<td><%=GODW.getquantity()%></td>
<td><%=df.format(Double.parseDouble(GODW.getpurchaseRate()))%></td>
<%-- <td><%=GODW.getvat()%></td> --%>
<%System.out.println("CGST...."+GODW.getExtra18()); %>
<td><%=GODW.getExtra18()%></td>
<td><%=GODW.getExtra19()%></td>
<td><%=GODW.getExtra20()%></td>

<%vatAmt=(Double.parseDouble(GODW.getquantity()))*(Double.parseDouble(GODW.getpurchaseRate()))*Double.parseDouble(GODW.getvat())/100; %>
<td align="right"><%=df.format((Double.parseDouble(GODW.getquantity()))*(Double.parseDouble(GODW.getpurchaseRate())+Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19())+Double.parseDouble(GODW.getExtra20())))%>
<% 
items=items+Double.parseDouble(GODW.getquantity());
subtotal=subtotal+(Double.parseDouble(GODW.getquantity()))*(Double.parseDouble(GODW.getpurchaseRate()));
amount=""+((Double.parseDouble(GODW.getquantity()))*(Double.parseDouble(GODW.getpurchaseRate())+Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19())+Double.parseDouble(GODW.getExtra20())));
totamnt=totamnt+(Double.parseDouble(amount));%>
</td>
</tr>
<%}}%>
<tr><td colspan="7"><div class="bordertop"></div></td></tr>
<tr><td colspan="5" height="20">Received Total Items Qty</td>
<td colspan="2" align="right"><%=items %>(QTY)</td></tr>

<tr><td colspan="7"><div class="bordertop"></div></td></tr>
<tr><td colspan="5">Actual Total Amount(QTY*Price)</td>
<td colspan="2" align="right"><%=df.format(subtotal)%> </td>
</tr>

<tr><td colspan="3"></td>
<td colspan="2" align="right"></td>
</tr>
<tr><td colspan="7"><div class="border"></div></td></tr>
<tr> <td colspan="5">GST@</td>  <td colspan="2" align="right">GST AMT</td>  </tr>
<tr> <td colspan="5">0%</td>   <td colspan="2" align="right"><%=Math.round(zeroPer)%></td>  </tr>
<tr> <td colspan="5">5%</td>  <td colspan="2" align="right"> <%=(fivePer)%></td>  </tr>
<tr> <td colspan="5">12%</td>  <td colspan="2" align="right"><%=Math.round(twelvePer)%></td>  </tr>
 <tr> <td colspan="5">18%</td>  <td colspan="2" align="right"><%=Math.round(eighteenPer)%></td>  </tr>
<tr> <td colspan="5">28%</td>  <td colspan="2" align="right"><%=Math.round(twntyeightPer)%></td>  </tr> 
<tr><td colspan="7"><div class="bordertop"></div></td></tr>
<tr><td colspan="5">Total Amount</td>
<td colspan="2" align="right"><%=Math.round(totamnt)%> </td>
</tr>
<%if(othercharges>0){ %>
<tr><td colspan="5">Other Charges</td>
<td colspan="2" align="right"><%=Math.round(othercharges)%> </td>
</tr>
<%} %>
<tr><td colspan="5">Total</td>
<td colspan="2" align="right"><%=Math.round(totamnt+othercharges)%> </td>
</tr>
<tr><td colspan="5" height="10"></td></tr>
<!-- <tr><td colspan="5" align="center">**THANK YOU! VISIT AGAIN**</td></tr> -->
<tr><td colspan="5" height="20"></td></tr>
<tr>
<td colspan="5"> Entered By</td>
<td colspan="2" align="right" >Receiver Sign</td>
</tr>
</table>
</div>
</body>
<%}%>
<%}%>