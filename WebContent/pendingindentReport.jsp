<%@page import="beans.indent"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Indents Approval Require</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
<script>
function formBlock(){
	$.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } }); 
}
 $(document).ready(function() {
    $('#selectall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>
<!--Date picker script  -->
<script>
function showstockaprve(id){
	$('#'+id).toggle();
}
</script>
<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="INDTT" class="beans.indent" />
				<jsp:useBean id="INDT" class="beans.indent" />
		<jsp:useBean id="SHP" class="beans.shopstock" />
		<jsp:useBean id="PRD" class="beans.products" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down bold">					
Indent Approval	</div>
		
				<div class="clear"></div>
				<div class="list-details">
				<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td  width="10%" style="font-weight: bold;">SNo</td>
							<td  colspan="3" width="21%" style="font-weight: bold;">DATE</td>
							<td  colspan="3" width="21%" style="font-weight: bold;">Invoice Id</td>
															</tr></table>
	<%
	headofaccountsListing HOA_L=new headofaccountsListing();
	SimpleDateFormat SDF=new SimpleDateFormat("yyyy-mm-dd");
	SimpleDateFormat CDF=new SimpleDateFormat("dd-mm-yyyy");
	mainClasses.indentListing IND_L = new mainClasses.indentListing();
	productsListing PROL=new productsListing();
	List IND_List=IND_L.getUnApproveIndents();
    List indentList=null;
	  if(IND_List.size()>0){
		  for(int j=0; j < IND_List.size(); j++ ){
			  INDT=(indent)IND_List.get(j);	%>
	  		<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="bg" width="10%"><%=j+1 %></td>
							<td  colspan="3" width="21%" class="bg" style="font-weight: bold;"><a onclick="showstockaprve('<%=INDT.getindentinvoice_id()%>')" style="cursor: pointer;" ><%=CDF.format(SDF.parse(INDT.getextra3()))%></a></td>
							<td colspan="3" align="left"  width="21%" class="bg" style="font-weight: bold;"><a onclick="showstockaprve('<%=INDT.getindentinvoice_id()%>')" style="cursor: pointer;" ><%=INDT.getindentinvoice_id()%></a></td>
							</tr></table>
						<%indentList=IND_L.getMindentsByIndentinvoice_id(INDT.getindentinvoice_id()); 
						if(indentList.size()>0){%>
	  <div id="<%=INDT.getindentinvoice_id()%>" style="display: none">
	 	  				<form action="IndentApproveInsert.jsp" method="post" onsubmit="formBlock();">
	 	  				 <input type="hidden" name="headAccountId" value="<%=INDT.gethead_account_id()%>">
	 	  				 <span style="color: red;"><%=HOA_L.getHeadofAccountName(INDT.gethead_account_id())%> Department</span>
						<table width="100%" cellpadding="0" cellspacing="0" border="1">
						<tr>
							<td width="5%" style="font-weight: bold;"><input type="checkbox" id="selectall"/>Select All</td>
							<td  width="21%" style="font-weight: bold;">S.NO</td>
							<td  width="26%" style="font-weight: bold;">PRODUCT NAME.</td>
							<td  width="17%" style="font-weight: bold;">PRODUCT QTY In Godwan</td>
							<td  width="26%" style="font-weight: bold;">PRODUCT QTY Raised</td> 
							</tr>
						<%for(int i=0; i < indentList.size(); i++ ){
							INDT=(indent)indentList.get(i); %>
						<tr>
							<td  style="font-weight: bold; text-align:center"><input class="checkbox1" type="checkbox" name="god_id" value="<%=INDT.getindent_id()%>" <%if(i==0){ %> required <%} %> ></input></td>

							<td><%=i+1%></td>
							<td><%=INDT.getproduct_id() %>-<%=PROL.getProductsNameByCats(INDT.getproduct_id(), "") %>							
							</td>
							<td>
							<%=PROL.getProductsStockGodwan(INDT.getproduct_id()) %>
							</td>
							<td><input type="text" name="quntyapr<%=INDT.getindent_id()%>" value="<%=INDT.getquantity() %>" /></td>
											</tr>
						<%} %>
					</table>
						<div >
					<ul>
								<li style="list-style:none; margin:10px 0 0 0"><input type="submit"  name="search" value="Approve" class="click"></input>
								</li>
								
					</ul>
				</div>
					</form>
					</div>
					<%}} }  else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no Stock for Approve !</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>