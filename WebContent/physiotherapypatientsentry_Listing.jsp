<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%-- <%@page import="beans.diagnosticappointments"%>
<%@page import="mainClasses.diagnosticappointmentsListing"%> --%>
<%@page import="beans.patientsentry"%>
<%@page import="mainClasses.patientsentryListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>physiotherapy Appointments</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList"%>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>

<style type="text/css">
      #loadingmsg {
      color: black;
      background: #fff; 
      padding: 10px;
      position: fixed;
      top: 50%;
      left: 50%;
      z-index: 100;
      margin-right: -25%;
      margin-bottom: -25%;
      }
      #loadingover {
      background: black;
      z-index: 99;
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
      filter: alpha(opacity=80);
      -moz-opacity: 0.8;
      -khtml-opacity: 0.8;
      opacity: 0.8;
    }
</style>

<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		    $("#patientName").focus();	
});
	</script>
<!--Date picker script  -->
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<!-- Auto suggestions code-->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
$(document).ready(function(){
  $("#doctorName").keyup(function(){
	  var data1=$("#doctorName").val();
	  $.post('searchDoctor.jsp',{q:data1},function(data)
	 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		//alert(doctorNames);
		 $( "#doctorName" ).autocomplete({source: availableTags}); 
		 
			});
  });
  $("#oldPatient").click(function(){
		$("#patientName").keyup(function(){
			  var data1=$("#patientName").val();
			 
			  if(data1.contains("APP")){
				  $.post('searchOldPatients.jsp',{q:data1,cat:'previous'},function(data)
						  {
					  var arr = [];	
							var response = data.trim().split("\n");
							 var doctorNames=new Array();
							 var doctorIds=new Array();
							 for(var i=0;i<response.length;i++){
								 var d=response[i].split(",");
								doctorNames.push(d[7]+" "+d[0] +" "+ d[1] );
								doctorIds.push(d[8]);
								 arr.push({
								label: d[7]+" "+d[0] +" "+ d[1],
						        phone:d[2],
								        address:d[3],
								        doctorIds:d[8],
								        sortable: true,
								        resizeable: true
								    });
													 
							 }
							var availableTags=data.trim().split("\n");
							var availableIds=data.trim().split("\n");
							availableTags=arr;
							$( "#patientName" ).autocomplete({
								
								source: availableTags,
								focus: function(event, ui) {
									// prevent autocomplete from updating the textbox
									event.preventDefault();
									// manually update the textbox
									$(this).val(ui.item.label);
								},
								select: function(event, ui) {
									// prevent autocomplete from updating the textbox
									event.preventDefault();
									// manually update the textbox and hidden field
									$(this).val(ui.item.label);
								$('#doctorName').val(ui.item.doctorIds);
								 $("#phone").val (ui.item.phone);
								 $("#address").val (ui.item.address);
							
								}	
							}); 
								});
			  } else{
				  $.post('searchOldPatients.jsp',{q:data1},function(data)
			  {
							var response = data.trim().split("\n");
				 var doctorNames=new Array();
				 var doctorIds=new Array();
				 for(var i=0;i<response.length;i++){
					 var d=response[i].split(",");
					 doctorIds.push(d[0]);
					 doctorNames.push(d[0] +" "+ d[1]);
					 $("#phone").val (d[2]);
					 $("#address").val (d[3]);
					 
				 }
				var availableTags=data.trim().split("\n");
				var availableIds=data.trim().split("\n");
				availableTags=doctorNames;
				$( "#patientName" ).autocomplete({source: availableTags}); 
				
					});
			  }
		  });
		});
});
function printrecipt(){
	if(document.getElementById("appointmentid").value != 'null'){
		newwindow=window.open('medicalReceipt-new.jsp?id='+document.getElementById("appointmentid").value,'name','height=600,width=500,menubar=yes,status=yes,scrollbars=yes');
		//newwindow=window.open('print.jsp?rmid=RUM1000017','name','height=600,width=500,menubar=yes,status=yes,scrollbars=yes');
			if (window.focus) {newwindow.focus();}

		}
}
</script>

<!-- Auto suggestions code-->

<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function changeDisplay(val){
	if(val=='new'){
		var newAppointmentId=$('#newAppointmentId').val();
//		alert("new "+newAppointmentId);
	
		$("#patientName").val("");
		$("#doctorName").val("");
		$("#purpose").val("");
		$("#phone").val("");
		$("#age").val("");
		$("#address").val("");
		$("#billnumb").val(newAppointmentId);
		$("#phone").removeAttr("disabled");
		$("#age").removeAttr("disabled");
		$("#genM").removeAttr("disabled");
		$("#genFM").removeAttr("disabled");
		$("#address").removeAttr("disabled");
		document.getElementById("oldAppontmentDetail").style.display="none";
	}else if(val=='old'){
		$('input#phone').attr('disabled', 'disabled');
		$('input#age').attr('disabled', 'disabled');
		$('input#genM').attr('disabled', 'disabled');
		$('input#genFM').attr('disabled', 'disabled');
		$('textarea#address').attr('disabled', 'disabled');
		document.getElementById("oldAppontmentDetail").style.display="block";
	}
}

function validate(){
	$('#saveAndPrintBtn').attr('disabled','disabled');
	$('#saveBtn').attr('disabled','disabled');
	
	$('#PatientError ').hide();
	$('#DoctorError ').hide();
	$('#addressError ').hide();
	$('#PhoneError ').hide();
	var doctorName=$('#doctorName').val();
	/* alert(doctorName.indexOf("DCT"));
	alert(doctorName.indexOf("dct")); */
	if (!(doctorName.indexOf("DCT") >= 0) ){
		/* alert(doctorName); */
		 $('#DoctorError').show();
		 $('#doctorName').focus();  
		 
		 $('#saveAndPrintBtn').removeAttr('disabled');
		 $('#saveBtn').removeAttr('disabled');
		return false;
	}
	if($('#patientName').val().trim()==""){
		$('#patientError').show();
		$('#patientName').focus();
		
		$('#saveAndPrintBtn').removeAttr('disabled');
		$('#saveBtn').removeAttr('disabled');
		return false;
	}
	
		if($('#doctorName').val().trim()==""){
			$('#DoctorError').show();
			$('#doctorName').focus();
			
			$('#saveAndPrintBtn').removeAttr('disabled');
			 $('#saveBtn').removeAttr('disabled');
			return false;
	}	
	if($("input[name='patientType']:checked").val()=='New patient'){
		if($('#phone').val().trim()==""){
			$('#PhoneError').show();
			$('#phone').focus();
			
			$('#saveAndPrintBtn').removeAttr('disabled');
			 $('#saveBtn').removeAttr('disabled');
			return false;
		}
		if($('#address').val().trim()==""){
			$('#addressError').show();
			$('#address').focus();
			
			$('#saveAndPrintBtn').removeAttr('disabled');
			 $('#saveBtn').removeAttr('disabled');
			return false;
		
		}
	}	
	
	 document.getElementById('loadingmsg').style.display = 'block';
	    document.getElementById('loadingover').style.display = 'block';
}
</script>

<script>
$(document).ready(function(){
	$('#saveBtn').click(function(){
		$('#save').val('SAVE');
		$('#savePrint').val('');
	});
	
	$('#saveAndPrintBtn').click(function(){
		$('#save').val('');
		$('#savePrint').val('SAVE&PRINT');
	});
});
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if(session.getAttribute("empId")!=null && !session.getAttribute("empId").equals("")){
	%>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->

<div>
<jsp:useBean id="PAT" class="beans.patientsentry"/>
<jsp:useBean id="APT" class="beans.appointments"/>
<div class="vendor-page">

<div class="vendor-box">
<%-- <div class="vendor-title">Appointment <%if(request.getParameter("id")!=null) {%>Edit<%}%></div> --%>
<div class="vender-details">
<input type="hidden" name="appointmentid" id="appointmentid" value="<%=request.getParameter("appoitId")%>"/>
<%
patientsentryListing PATL = new mainClasses.patientsentryListing();
if(request.getParameter("id")!=null) {
	List PAT_List=PATL.getMpatientsentry(request.getParameter("id").toString());
	for(int i=0; i < PAT_List.size(); i++ ){
	PAT=(beans.patientsentry)PAT_List.get(i);
%>

<form name="tablets_Update" method="post" action="patientsentry_Update.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="patientId" id="patientId" value="<%=PAT.getpatientId()%>" />
		<tr><td colspan="2" align="center" style="font-weight: bold;"> SHRI SHIRDI SAIBABA SANSTHAN TRUST FREE MEDICAL CENTER</td></tr>
		<tr><td colspan="2" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
		<tr><td colspan="2" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
		<tr><td colspan="2" align="center" style="font-weight: bold;">DOCTOR APPOINTMENT'S FORM</td></tr>
<tr>
<td width="35%">
<div class="warning" id="PatientError" style="display: none;">Please Provide  "Patient Name".</div>
Patient Name*</td>
<td >
</td>
</tr>
<tr>
<td><input type="text" name="patientName" id="patientName" value="<%=PAT.getpatientName() %>" onkeyup="javascript:this.value=this.value.toUpperCase();"/></td>
<td></td>
</tr>
<tr>
<td>
<div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone No".</div>
Phone No*</td>
<td>Gender</td>
</tr>
<tr>
<td><input type="text" name="phone" id="phone" onKeyPress="return numbersonly(this, event,true);" value="<%=PAT.getphone() %>" /></td>
<td><input type="radio" name="extra1" value="M" <%if(PAT.getextra1().equals("M")){%>checked="checked" <%} %> />Male<input type="radio" name="extra1" value="F" <%if(PAT.getextra1().equals("F")){%>checked="checked" <%} %>/>Female</td>
</tr>

<tr>



<td colspan="2"><div class="warning" id="addressError" style="display: none;">Please Provide  "Address".</div>
<textarea name="address" id="address" placeholder="Enter Your Address*" onkeyup="javascript:this.value=this.value.toUpperCase();"><%=PAT.getaddress() %></textarea></td>
</tr>
<tr> 
<td></td>
<td align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>

</tr>
</table>

</form>
<%} 
	
} else{%>
<div id='loadingmsg' style='display: none;'>
	<img src="images/logo.jpg" alt="dcs" />
</div>
<div id='loadingover' style='display: none;'></div>
<form name="tablets_Update" method="post" action="physiotherapyapointmentInsert.jsp" id="tablets_Update1" onsubmit="return validate();">
<%
	List list=new ArrayList();
	if(request.getParameter("oldAppointmentId")!=null)
	{
		String oldAppointmentId=request.getParameter("oldAppointmentId");
		//to get Patient details and Appointment details bases upon appontment id
		patientsentryListing listing=new patientsentryListing();
		list=listing.getPatientAndDoctorDetailBasedOnAppoinmentId(oldAppointmentId);
	}
	
	
	
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
%>
<table><tr><td width="70%">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="2" align="center" style="font-weight: bold;"> SHRI SHIRDI SAIBABA SANSTHAN TRUST FREE MEDICAL CENTER</td></tr>
						<tr><td colspan="2" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="2" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="2" align="center" style="font-weight: bold;">DOCTOR APPOINTMENT'S FORM</td></tr>
						
<tr>
<td>Date</td><td width="50%">Appointment-ID</td></tr>
<tr><td><input type="text" name="date" id="date"  value="<%=currentDate%>" class="DatePicker" readonly="readonly"/></td>
<input type="hidden" id="newAppointmentId" value="<%=ng_LST.getidWithoutUpdatingNoGen("physiotherapyappointments")%>">
<td><input type="text" name="billnumb" id="billnumb" value="<%=ng_LST.getidWithoutUpdatingNoGen("physiotherapyappointments")%>"  readonly="readonly"/></td></tr>
<tr>
	<td colspan="1">
		<input type="radio" name="patientType" id="newPatient" value="New patient" <%if(request.getParameter("oldAppointmentId")==null){%>checked="checked"<%} %> onclick="changeDisplay('new')">New Patient</input>
	</td>
	<td><input type="radio" name="patientType" id="oldPatient" <%if(request.getParameter("oldAppointmentId")!=null){%>checked="checked"<%} %> value="Old patient" onclick="changeDisplay('old')">Old Patient</input>
<!-- <td id="oldAppontmentDetail" style="display:none">
	<form action="patientsentry_Listing.jsp"> 
		<input type="text" name="oldAppointmentId" style="margin-left:15px; margin-top:10px;"/>
    	<input class="click" style="border:none;" type="submit" value="getOldDetails"/>
	</form>
</td> -->
</tr>

<tr>
<td width="31%">
<!-- <div class="warning" id="patientError" style="display: none;">Please Provide  "Patient Name".</div> -->
Patient Name*</td>
<td>
  <div class="warning" id="DoctorError" style="display: none;">Please Provide Valid "Doctor Name".</div>
Doctor Name*</td>
</tr>
<tr>
<td><input type="text" name="patientName" id="patientName" autocomplete="off" <%if(request.getParameter("oldAppointmentId")!=null && list.get(0)!=null){%>value="<%=list.get(0)%>"<%}else{%>value=""<%}%> required placeholder="Find a patient here" onkeyup="javascript:this.value=this.value.toUpperCase();"/>
</td>
<td><input type="text" name="doctorName" id="doctorName" <%if(request.getParameter("oldAppointmentId")!=null && list.get(1)!=null){%>value="<%=(String)list.get(7)+" "+(String)list.get(1)%>"<%}else{%>value="" <%}%> autocomplete="off" required placeholder="Find a doctor here" onkeyup="javascript:this.value=this.value.toUpperCase();"/></td>
</tr>
<tr>
<td colspan="2"><textarea name="purpose" id="purpose" rows="2" cols="2" placeholder="Enter Purpose" onkeyup="javascript:this.value=this.value.toUpperCase();"><%if(request.getParameter("oldAppointmentId")!=null && list.get(2)!=null){%><%=list.get(2)%>"<%}%></textarea></td>
</tr>
<tr>
<td width="33%">
<!-- <div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone No".</div> -->
Phone No*</td>
<td width="33%">Age</td>
<td width="33%">Gender</td>
<td>Email</td>
</tr>
<tr>
<td><input type="text" name="phone" id="phone" <%if(request.getParameter("oldAppointmentId")!=null && list.get(3)!=null){%>value="<%=list.get(3)%>"<%}else{%>value="" <%}%> onKeyPress="return numbersonly(this, event,true);"  placeholder='Enter phone number'/></td>
<td><input type="text" name="age" id="age" <%if(request.getParameter("oldAppointmentId")!=null && list.get(4)!=null){%>value="<%=list.get(4)%>"<%}else{%>value="" <%}%>  placeholder="Enter patient age"></input></td>

<%if(request.getParameter("oldAppointmentId")==null || list.get(5)==null || "Male".equals(list.get(5))){%>checked<%}%>

<td><input type="radio" name="gender" id="genM" value="Male" <%if(request.getParameter("oldAppointmentId")==null || list.get(5)==null || "Male".equals(list.get(5))){%>checked<%}%> />Male
	<input type="radio" name="gender" id="genFM" value="Female" <%if(request.getParameter("oldAppointmentId")!=null && "Female".equals(list.get(5))){%>checked<%}%>/>Female</td>
	<td><input type="text" name="extra5" id="extra5" placeholder="example@gmail.com" <%if(request.getParameter("oldAppointmentId")!=null && list.get(8)!=null){%>value="<%=(String)list.get(8)%>"<%}else{%>value="" <%}%>></input></td>
</tr>

<tr>
<td colspan="2"><div class="warning" id="addressError" style="display: none;">Please Provide  "Address".</div>
<textarea name="address" id="address" placeholder="Enter Patient Address*" onkeyup="javascript:this.value=this.value.toUpperCase();"><%if(request.getParameter("oldAppointmentId")!=null && list.get(6)!=null){%><%=list.get(6)%>"<%}%></textarea></td>
</tr>
</div>
<tr> 
<td align="right"><input type="reset" name="reset" value="RESET" class="click" style="border:none;" onclick="changeDisplay('new')"/></td>
<td align="right"><input type="submit" value="SAVE" class="click" style="border:none;" id="saveBtn"/>
<input type="submit" value="SAVE&PRINT" class="click" style="border:none;" id="saveAndPrintBtn"/></td>
<input id="save" name="save" value=""/>
<input id="savePrint" name="savePrint" value=""/>
</form>
</tr>
</table></td>
<td>
<table>
<tr>
<br><br></br></br><br><br></br><br><br><td id="oldAppontmentDetail" <%if(request.getParameter("oldAppointmentId")==null ){ %>style="display:none"<%}else{%>style="display:block"<%}%> width="40%" >
<div style="margin-top:20px;">
	<form action="patientsentry_Listing.jsp"> 
		<input type="text" name="oldAppointmentId" <%if(request.getParameter("oldAppointmentId")!=null ){%>value="<%=request.getParameter("oldAppointmentId")%>"<%}%> style="margin-left:15px; margin-top:10px;"/>
    	<input class="click" style="border:none;" type="submit" value="getOldDetails"/>
	</form>
</td></tr></table> 
</td>
</div>
</tr></table>
</form>
<%

} %>
</div>
<div style="clear:both;"></div>



</div>

<%-- <div class="vendor-list">
<div class="arrow-down"><img src="images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Columns</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%
appointmentsListing APT_L=new appointmentsListing();
doctordetailsListing DOCT_L=new doctordetailsListing();
List APT_List=APT_L.getappointments();
if(APT_List.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Patient Name</td>
<td class="bg" width="23%">Appointed Doctor</td>
<td class="bg" width="23%">purpose</td>
<td class="bg" width="23%">Date/Time</td>
<!-- <td class="bg" width="20%">Edit</td>
<td class="bg" width="10%">Delete</td> -->
</tr>
<%for(int i=0; i < APT_List.size(); i++ ){
	APT=(appointments)APT_List.get(i); %>
<tr ><td><%=i+1%></td>
<td><span><%=PATL.getPatientName(APT.getpatientId())%></span></td>
<td><%=DOCT_L.getDoctorName(APT.getdoctorId())%></td>
<td><%=APT.getpurpose()%></td>
<td><%=APT.getdate()%></td>
<td><a href="patientsentry_Listing.jsp?id=<%=PET.getpatientId() %>"  >Edit</a></td>
<td><a href="patientsentry_Delete.jsp?Id=<%=PET.getpatientId()%>">Delete</a></td>
</tr>
<%} %>

</table>
<%}else{%>
<div align="center"><h1>No Tablets Added Yet</h1></div>
<%}%>


</div>
</div> --%>




</div>

</div>
<!-- main content -->


<%
 
}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
<script>
window.onload=printrecipt();
</script>

</body>
</html>