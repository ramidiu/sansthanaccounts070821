<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SHOP SALES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<jsp:useBean id="GOD" class="beans.godwanstock" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<div class="sj">
					  <form action="Sansthan-Stores-Purchase-Report.jsp" method="post">  
					  <select name="headID" >
						<%-- <option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %>selected="selected" <%} %>>POOJA STORES</option> --%>
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
					</select>
					
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form action="Sansthan-Stores-Purchase-Report.jsp" method="post">
				<div class="search-list">
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><select name="headID" >
						<%-- <option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %>selected="selected" <%} %>>POOJA STORES</option> --%>
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						</select></li>
						<li>
						<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<a id="print"><span><img src="images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat time=new SimpleDateFormat("HH:mm");
	mainClasses.subheadListing SUBHL=new mainClasses.subheadListing();
	productsListing PRDL=new productsListing();
	vendorsListing VENL=new vendorsListing();
	purchaseorderListing POR=new purchaseorderListing();
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
	headofaccountsListing HODAL=new headofaccountsListing();
	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:00";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	godwanstockListing GODSL=new godwanstockListing();
	String hoid="4";
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoid=request.getParameter("headID");
	}
	List stkListDet=GODSL.getSansthanStoreMSE(hoid, fromDate, toDate);
	List SAL_DETAIL=null;
	double totalAmount=0.00; 
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double TotQty=0.00;
	double TotGross=0.00;
	if(stkListDet.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:97%; 
    background:#d0e3fb; 
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%=HODAL.getHeadofAccountName(hoid) %></td>
					</tr>
					<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 11px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="9" align="center" style="font-weight: bold;">STOCK PURCHASED REPORT</td></tr>
					<tr><td colspan="9" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
						
						
						<tbody class="yourID">
<tr>
							<td class="bg" width="3%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="15%" style="font-weight: bold;">BILL.NO/DATE</td>
							<td class="bg" width="10%" style="font-weight: bold;">MSE.NO.</td>
							<td class="bg" width="15%" style="font-weight: bold;">PARTY NAME</td>
							<td class="bg" width="22%" style="font-weight: bold;" >NARRATION</td>
							<td class="bg" width="15%" style="font-weight: bold;" >PRODUCT NAME</td>
							<td class="bg" width="5%" style="font-weight: bold;">QTY</td>
							<td class="bg" width="5%" style="font-weight: bold;">RATE</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">GROSS</td>
							<!-- <td class="bg" width="20%" style="font-weight: bold;" align="right">ENTERED BY</td> -->
							<!-- <td class="bg" width="10%" style="font-weight: bold;" align="right">SENT TO WHOM</td> -->
						</tr>
</tbody>
						
						<%for(int i=0; i < stkListDet.size(); i++ ){ 
							GOD=(beans.godwanstock)stkListDet.get(i);
							
							%>
						   <tr>
								<td width="3%"><%=i+1%></td>
								<td width="15%"> <%if(GOD.getbillNo()!=null &&!GOD.getbillNo().equals("")){%><%=GOD.getbillNo()%> <%}else{ %>N/A <%} %> /<%=df2.format(dateFormat.parse(GOD.getdate()))%>/<%=time.format(dateFormat.parse(GOD.getdate()))%></td>
								<td width="10%"><%=GOD.getextra1()%></td>
								<td width="15%"><span><%if(GOD.getvendorId()!=null && !GOD.getvendorId().equals("")){ %> <%=VENL.getMvendorsAgenciesName(GOD.getvendorId())%><%}else{ %> N/A<%} %></span></td>
								<td width="22%"><%=GOD.getdescription()%></td>
								<td width="15%">
								<%if(GOD.getExtra7().equals("serviceBill")){ %><%=SUBHL.getMsubheadname(GOD.getproductId()) %><%}else{ %>
								<%=PRDL.getProductsNameByCat1(GOD.getproductId(),GOD.getdepartment())%><%} %></td>
								<td style="font-weight: bold;" align="center" width="5%"><%=GOD.getquantity()%></td>
								<%TotQty=TotQty+Double.parseDouble(GOD.getquantity());
								TotGross=TotGross+(Double.parseDouble(GOD.getpurchaseRate())*Double.parseDouble(GOD.getquantity()));%>
								<td style="font-weight: bold;" align="right" width="5%"><%=df.format(Double.parseDouble(GOD.getpurchaseRate()))%></td>
								<td style="font-weight: bold;" align="right" width="10%"><%=df.format(Double.parseDouble(GOD.getpurchaseRate())*Double.parseDouble(GOD.getquantity()))%></td>
												
								<%-- <td align="right"><%if(GOD.getdepartment().equals("3")){ %>GODOWN<%}else if(GOD.getdepartment().equals("4")){ %> KITCHEN STORE  <%} %></td> --%>
							</tr>
						<%} %>
						<tr style="border-top:1px solid #000; " >
						<td colspan="6" class="bg"  style="font-weight: bold;" align="right">TOTAL</td>
						<td colspan="1" align="center" class="bg"  style="font-weight: bold;"><%=TotQty %></td>
						<td colspan="1" class="bg"  style="font-weight: bold;"></td>
						<td colspan="1" align="right" class="bg"  style="font-weight: bold;"><%=df.format(TotGross) %></td>
						<!-- <td colspan="1" class="bg"  style="font-weight: bold;"></td> -->
						</tr>
					  
						
						<%-- <tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total sales amount | </td>
						<td style="font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount)%></td>
						<td style="font-weight: bold;" align="right">&#8377;<%=df.format(bankDepositTot) %></td>
						</tr> --%>
					
					</table>
			</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There is no purchase stock entry found!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>