<%@page import="helperClasses.DevoteeGroups"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EMPLOYEE VOLUNTEER REGISTRATION || SSSST,DSNR</title>
<!--<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />-->
<script src="js/jquery-1.4.2.js"></script>
<!-- <link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" /> -->
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<!-- <link href="../js/date.css" rel="stylesheet" type="text/css" /> -->
<!-- <link href="style.css" rel="stylesheet" type="text/css" /> -->
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/> -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
	.mytabe td{
	text-align:left;}
	.mytable input{
	padding:7px 0px 7px 0px;}
</style>
<script>

/* $(function() {
	$( "#date" ).datepicker({
		yearRange: '1940:2020',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#date2" ).datepicker({
		yearRange: '1940:2020',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#date3" ).datepicker({
		yearRange: '1940:2020',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
}); */

$(function() {
	  $( ".datepicker" ).datepicker({ 
		  yearRange: '1940:2020',
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
		  });
	}); 


function block(){
	 $.blockUI({ css: { 
	        border: 'none', 
	        padding: '15px', 
	        backgroundColor: '#000', 
	        '-webkit-border-radius': '10px', 
	        '-moz-border-radius': '10px', 
	        opacity: .5, 
	        color: '#fff' 
	    } });
}

function formsumit(){ 
	document.reg.submit();}
</script>

<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/> -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
</head>
<body>
<%if(session.getAttribute("empId")!=null){ 
%>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-box">
<% String type = request.getParameter("type");
%>

<% if (type != null) {%>
<form name="reg" id="reg" method="post" action="employee-volunteer-form.jsp">
<table><tr><td>Registration Type:<select name="type" Onchange="formsumit();">
<option value="EMPLOYEE" <%if(request.getParameter("type").equals("EMPLOYEE")) {%> selected="selected"<%} %>>EMPLOYEE</option>
<option value="VOLUNTEER" <%if(request.getParameter("type").equals("VOLUNTEER")) {%> selected="selected"<%} %>>VOLUNTEER</option>
</select></td></tr></table></form>
<%} %>
<div class="vender-details">
<form  method="post" action="employee-volunteer-insert.jsp" onsubmit="block();">
 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
 <tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST </td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<% if(request.getParameter("type") != null) {%>	
<tr><td colspan="5" align="center" style="font-weight: bold;"><%=type%> REGISTRATION</td></tr>
 <%}%>
</table>
<table width="70%" border="0" cellspacing="0" cellpadding="0" class="mytabe">
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();%>
<tr>
	<td>Date</td>
	<!-- <td>Sansthan Unique Id</td> -->
	<td>EMAIL-ID</td>
	<td colspan="1">MOBILE NO <span style="color: red;"></span></td>
</tr>
<tr>
	<td><input type="text" name="curentdate" value="<%=currentDate%>" readonly/></td>
	<%-- <td><input type="text" name="receiptNo"  style="height:30px;" value="<%=ng_LST.getid("sssst_registrations")%>" readonly></td> --%>
	<td><input type="email" name="email-id"  style="margin-left:10px" value="" placeHolder="Enter email-id"></td>
	<td><input type="text" name="mobile"   style="margin-left:10px" value="" placeHolder="Enter mobile number" required></td>
</tr>
<tr>
	<td>Title</td>
	<td>First Name<span style="color: red;"></span></td>
	<td>Middle Name</td>
	<td>Sur Name<span style="color: red;"></span></td>
	<td>Spouse Name</td>
</tr>
<tr>
	<td><select name="title">
	<option value="Mr">Mr.</option>
	<option value="Miss">Miss.</option>
	<option value="Mr&miss">Mr&miss</option>
	<option value="Master">Master</option>
	<option value="Baby">Baby</option>
	</select></td>
	<td><input type="text" name="firstname" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Firstname" required></td>
	<td ><input type="text" name="middlename" style="margin-left:10px" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Middle name"></td>
	<td ><input type="text" name="lastname" style="margin-left:10px" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Sur name"></td>
	<td ><input type="text" name="spousename" style="margin-left:10px" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Spouse name"></td>
</tr>
<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
<tr>
	<td>Father Name</td>
	<td>Mother Name</td>
	<td>Mother Tongue</td>	
</tr>
<tr>
<td><input  type="text" name="fathername" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Father Name"></td>
<td><input  type="text" name="mothername" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Mother Name"></td>
<td><input  type="text" name="mothertongue"  style="margin-left:10px"value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Mother Tongue"></td>
</tr><%}%> 
<tr>
	<td>Gender</td>
	<td>Date of Birth</td>
	<td>Gothram</td>
	<!-- <td>Star</td> -->
	<td>Pancard Number</td>
</tr>
<tr>
	<td><input  type="radio" name="gender" value="male"  checked="checked">Male<input  type="radio" name="gender" value="female">Female</td>
	<td ><input  type="text" style="width: 105px;" name="dob" id="date" class="datepicker" value="" placeHolder="Select Date of birth" readonly></td>
	<td><input  type="text" name="gothram"   value="" placeHolder="Enter Gothram" onkeyup="javascript:this.value=this.value.toUpperCase();"></td>
	<!-- <td><input  type="text" name="star" style="height:30px;" value="" placeHolder="Enter Star" onkeyup="javascript:this.value=this.value.toUpperCase();"></td> -->
	<td><input  type="text" name="pancard" value="" placeHolder="Enter pancard Number"></td>
</tr>
<tr>
	<td>Permanent Address</td>
	<td>Present Address</td>
	<td>Date Of Anniversary</td>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("VOLUNTEER")) {%>
	<td>Service Experience in Sansthan Trust</td><%} %>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
	<td>Technical Qualification</td>
	<td>Caste & Religion</td><%} %>
	
</tr>
<tr>
	<td><textarea name="permanent_address" style="height:30px;margin-left:10px" placeHolder="Enter Permanent address" onblur="javascript:this.value=this.value.toUpperCase();"/></textarea></td>
	<td><textarea name="present_address" style="height:30px;margin-left:10px" placeHolder="Enter Present address" onblur="javascript:this.value=this.value.toUpperCase();"/></textarea></td>
	<!-- <td ><input  type="text" style="width: 105px;height:30px; margin-left:10px" name="doa" id="date2" class="datepicker" value="" placeHolder="Select Date of Anniversary" readonly></td> -->
	<td width="30">
	<select name="doamonth" style="width: 85px;margin-left:20px;">
	<option value="">Month</option>
	<option value="01">JAN</option>
	<option value="02">FEB</option>
	<option value="03">MAR</option>
	<option value="04">APR</option>
	<option value="05">MAY</option>
	<option value="06">JUN</option>
	<option value="07">JUL</option>
	<option value="08">AUG</option>
	<option value="09">SEP</option>
	<option value="10">OCT</option>
	<option value="11">NOV</option>
	<option value="12">DEC</option>
	</select>
	<select name="doadate" style="width: 85px;">
	<option value="">Day</option>
	<%for(int i=01;i<32;i++){ 
	if(i < 10){%>
	
	<option value="0<%=i%>">0<%=i%></option>
	<%}else{ %>
	<option value="<%=i%>"><%=i%></option>
	<%} }%>
	</select></td>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("VOLUNTEER")) {%>
	<td><input type="text" name="service_experinece_in_sansthan_trust" value="" placeHolder="Enter Service Experience in Sansthan Trust"/></td><%} %>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
	<td><textarea name="technical_qualification" style="height:30px;margin-left:10px" placeHolder="Enter Technical Qualification" onblur="javascript:this.value=this.value.toUpperCase();"/></textarea></td>
	<td><input type="text" style="margin-left:10px;" name="caste_religion" value="" placeHolder="Enter Caste & Religion" onblur="javascript:this.value=this.value.toUpperCase();"/></td><%} %>
</tr>
<tr>	
	<td>Identity Proof</td>
	<td>Identity Proof Number</td>
	<% if(request.getParameter("type") != null) {%>
	<td>Education Qualification</td>
	<td>Blood Group</td>
	<%} %>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
	<td>Identification Marks</td>
	<%} %>
</tr>
<tr>
	<td><select  name="idproof1">
		<option value="">ID PROOF</option>
		<option value="UID AADHAAR NO">UID AADHAAR NO</option>
		<option value="VOTE ID NO">VOTE ID NO</option>
		<option value="PAN CARD NO">PAN CARD NO</option>
		<option value="PASSPORT NO">PASSPORT NO</option>
		<option value="RATION CARD NO">RATION CARD NO</option>
		<option value="DRIVING LICENSE NO">DRIVING LICENSE NO</option>
		<option value="OTHERS">OTHERS</option>
	 </select> </td>
	 <td><input type="text" name="idproof_num" value="" placeHolder="Enter Identity proof number"></td>
	 <% if(request.getParameter("type") != null) {%>
	 <td><textarea name="education_qualification" style="height:30px;margin-left:10px" placeHolder="Enter Education Qualification" onblur="javascript:this.value=this.value.toUpperCase();"/></textarea></td>
	 <td><input  type="text" name="bloodgroup" style="margin-left:10px" value="" placeHolder="Enter Blood Group"></td>
	 <%} %>
	 <% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
	 <td><textarea name="identification_marks" style="height:30px;margin-left:10px" placeHolder="Enter Identification Marks" required onblur="javascript:this.value=this.value.toUpperCase();"/></textarea></td>
	<%} %>
	<td></td>
</tr>
<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
<tr>
	<td>Previous Experience</td>
	<td>Dependents</td>
	<td>Height</td>
	<td>Weight</td>
	<td>Enclosures</td>
</tr>
<tr>
<td><textarea name="previous_experience" style="height:30px;margin-left:10px" placeHolder="Enter Previous Experience"/></textarea></td>
<td><textarea name="dependents" style="height:30px;margin-left:10px" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Dependents"/></textarea></td>
<td><input type="text" name="height" style="margin-left:10px;" value="" placeHolder="Enter Height"></td>
<td><input type="text" name="weight" value="" placeHolder="Enter Weight"></td>
<td><textarea name="enclosures" style="height:30px;margin-left:10px" placeHolder="Enter Enclosures"/></textarea></td>
</tr><%} %>


<% if(request.getParameter("type") != null && request.getParameter("type").equals("VOLUNTEER")) {%>
<tr >
	<td>Occupation</td>
</tr>
<tr style="padding:0px">
<td colspan="6">

<label><input  type="radio" name="occupation"  value="Govt Employee" checked="checked">Govt Employee </label>
<label><input  type="radio" name="occupation" value="Private Employee">Private Employee</label>
<label><input  type="radio" name="occupation" value="Retired Person">Retired Person</label>
<label><input  type="radio" name="occupation" value="House-Wife">House-Wife</label>
<label><input  type="radio" name="occupation" value="Student">Student</label>
<label><input  type="radio" name="occupation" value="Other">Other</label>

</td>

</tr>
<tr>
<%}%>

<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
<tr>
	<td>Government Reservation</td>
	<td>If yes,Specify Reservation Category</td> 
	<td>Any Physical Disorder</td>
	<td>If yes,Specify % of Disorder</td>
</tr>	
<tr>	
	<td><input  type="radio" name="govt_reservation" style=" margin-left:10px" value="yes" checked="checked">Yes<input  type="radio" name="govt_reservation" value="no">No</td>
	<td><input type="text" name="reservation_cat" value="" placeHolder="Enter Reservation Category"></td>
	<td><input  type="radio" name="physical_disorder" value="yes" checked="checked">Yes<input  type="radio" name="physical_disorder" value="no">No</td>
	<td><input type="text" name="percent_of_disorder"  value="" placeHolder="Enter Percent Of Disorder"></td>
</tr>
<tr>
	<td>Present Post</td> <!-- for employee designation = present post -->
	<td>Pay Scale Of The Post</td> <!-- for employee present pay scale = pay scale of the post -->
	<td>Date Of Appointment</td>
	<td>Basic Pay</td>
</tr>	
<tr>	
	<td><input  type="text" name="designation" value="" placeHolder="Enter Present Post"></td>
	<td><input type="text" name="present_pay_scale" value="" placeHolder="Enter Pay Scale Of The Post"></td>
	<td ><input  type="text" style="width: 105px;margin-left:10px" name="date_of_appointed" id="date3" class="datepicker" value="" placeHolder="Select Date of Appointment" readonly></td>
	<td><input type="text" name="basic_pay" value="" placeHolder="Enter Basic Pay"></td>
</tr>
<tr>
	<td>EC Resolution No</td> 
	<td>EC Resolution Date</td> 
	<td>TB Resolution No</td>
	<td>TB Resolution Date</td>
	<td>Employee Identification No</td>  
</tr>	
<tr>	
	<td><input  type="text" name="ec_resolution_num" value="" placeHolder="Enter EC Resolution No"></td>
	<td><input  type="text" style="width: 105px; margin-left:10px" name="ec_resolution_date" class="datepicker" value="" placeHolder="Select EC Resolution Date" readonly></td>
	<td><input  type="text" name="tb_resolution_num" value="" placeHolder="Enter TB Resolution No"></td>
	<td><input  type="text" style="width: 105px; margin-left:10px" name="tb_resolution_date" class="datepicker" value="" placeHolder="Select TB Resolution Date" readonly></td>
	<td><input type="text" name="employee_identification_num" value="" placeHolder="Enter Emp Identification No"></td>
</tr>

<%} %>	
<tr><td></td></tr>
<tr><td></td></tr>
<tr>
<td colspan="8" padding:10px; margin-top:10px;
margin-bottom:10px;">
	<%-- <%
	List<String> list=DevoteeGroups.getAllDevotees();
	for(int i=0;i<list.size();i++){
	%>
	 <label>
      <input type="checkbox" name="devoteeGroup" value="<%=list.get(i)%>"/><%=list.get(i)%> &nbsp;&nbsp;&nbsp;
     </label>
	<%} %> --%>
     <%-- <% if(request.getParameter("type") != null) {%>
      <div style="display:none"> 
      <label>
      <input type="checkbox"  style="height:30px;" name="devoteeGroup" <%if(request.getParameter("type").equals("EMPLOYEE")) {%> checked="checked" <%} %> value="EMPLOYEE"/>EMPLOYEE &nbsp;&nbsp;&nbsp;
      </label>
      <label>
      <input type="checkbox" style="height:30px;" name="devoteeGroup" <%if(request.getParameter("type").equals("VOLUNTEER")) {%> checked="checked" <%} %> value="VOLUNTEER"/>VOLUNTEER &nbsp;&nbsp;&nbsp;
      </label>
      </div> 
     <%} %> --%>
     <input type="hidden"  style="height:30px;" name="type" value="<%=type%>"/>
</td>

</tr>
<tr><td></td></tr>
<tr> 
<td colspan="4" align="left"><input type="reset"  value="RESET" class="click" style="border:none; float:right; margin-right:10px;margin-bottom:-8px;"/></td>
<td  align="left" id="save"><input type="submit" value="REGISTER" class="click" style="border:none;"/></td>

</tr>

</table>
<%} %>
</form>
</div>
</div>
</div>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>