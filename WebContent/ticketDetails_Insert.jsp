<%@page import="sms.CallSmsApi1"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="model.sendmailservice"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.WtspNotification"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat,java.util.*"%>
<%@page import="sms.CallSmscApi"%>
<%@page import="beans.UniqueIdGroup" %>
<%@page import="beans.UniqueIdGroupService" %>
<%@page import="helperClasses.DevoteeGroups"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="CSP" class="beans.customerpurchasesService"/>
<jsp:useBean id="PRD" class="beans.productsService"/>
<jsp:useBean id="NOG" class="beans.no_genaratorService"/>
<jsp:useBean id="COU" class="beans.counter_balance_updateService"/>
<jsp:useBean id="CR" class="beans.cashier_report" />
<jsp:useBean id="CSH" class="beans.cashier_reportService" />
<jsp:useBean id="CAP" class="beans.customerapplicationService" />
<jsp:useBean id="SUB" class="beans.subhead"/>
<jsp:useBean id="DREG" class="beans.sssst_registrationsService"></jsp:useBean>

<%
WtspNotification wtsp=new WtspNotification();
mainClasses.no_genaratorListing NG_l=new mainClasses.no_genaratorListing();
mainClasses.counter_balance_updateListing CUBUL=new mainClasses.counter_balance_updateListing();
productsListing PROL=new productsListing();
subheadListing SUBHL=new subheadListing();
customerapplicationListing CAPL=new customerapplicationListing();
String headId = request.getParameter("headId");
DecimalFormat DF = new DecimalFormat("0.00");
NumberToWordsConverter NTW=new NumberToWordsConverter();
String pid[]=request.getParameter("productId").split(" ");
String productId=pid[0];
String tableName = "";

if(headId.equals("4"))
{
	tableName = "pro_sansthan";
	if (productId.equals("20063")) tableName = "pro_sansthan1"; // added by gowri shankar
}

else if(headId.equals("1"))
{
	tableName = "pro_charity";
	if (productId.equals("20092")) tableName = "pro_charity1"; // added by gowri shankar
}

else if(headId.equals("5"))
{
	tableName = "pro_sainivas";
}

else if(headId.equals("3"))
{
	tableName = "pro_poojastores";
}
System.out.println("tablename====>"+tableName);
// String billingId=NG_l.getid(tableName);
	/* String billingId=request.getParameter("receiptNo");
	NOG.updatInvoiceNumber(billingId,tableName); */
	String billingId= NOG.getIdAndUpdateBasedOnTableName(tableName);
	System.out.println("billingId====>"+billingId);
/* String billingId=NG_l.getid("proOfficeSales"); */
String minorHeadId=request.getParameter("type");
String offerkind_refid="";
String offerkind_refidValue1="";
String offerkind_refidValue2="";
String offKind[]=null;
if(productId.equals("20032")){
	offerkind_refidValue1=request.getParameter("offerkind_refid");//added by madhav
	if(request.getParameter("offerkind_refid1")!=null){
		offKind=request.getParameter("offerkind_refid1").split(",");
	}
    offerkind_refidValue2=offKind[0];//added by madhav
    
	if(!offerkind_refidValue1.equals("OFK780")){ //added by madhav
		offerkind_refid =offerkind_refidValue1;
	} else if(offerkind_refidValue2 != null && !offerkind_refidValue2.isEmpty()){
	offerkind_refid =offerkind_refidValue2;
	}
}
String status="sold";//added by madhav
customerpurchasesListing cListing=new customerpurchasesListing();//added by madhav
int i=cListing.updateOfferkindrefidBasedOnBillingId(status,offerkind_refid);//added by madhav
minorheadListing MINHL=new minorheadListing();
//String HOAID=SUBHL.getHeadOFAccountID(productId);
DateFormat mySqlDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
UniqueIdGroupService uniqueIdGroupService=new UniqueIdGroupService();
String uniqueid = request.getParameter("uniqueid");
/* System.out.println("uniqueid===>"+uniqueid);  */
/* String devoteeGroup[]=request.getParameterValues("devoteeGroup"); */

String HOAID=request.getParameter("headId");
String Amount=request.getParameter("Amount");
String ticQty=request.getParameter("ticQty");
String totAmt=request.getParameter("totAmt");
String customerName="";
if(request.getParameter("customerName")!=null){
	customerName=request.getParameter("customerName").replaceAll("'", "@");
}
String mobile=request.getParameter("mobile");
String address="";
if(request.getParameter("address")!=null){
	address=request.getParameter("address").replaceAll("'", "@");
}
String uniqueId="";
if(request.getParameter("uniqueID")!=null && !request.getParameter("uniqueID").equals("")){
	uniqueId=request.getParameter("uniqueID");
}
if(request.getParameter("addressSj")!=null && !request.getParameter("addressSj").equals("")){
	address=request.getParameter("addressSj").replaceAll("'", "@");
}
String purpose="";
if(request.getParameter("purpose")!=null){
	purpose=request.getParameter("purpose").replaceAll("'", "@");
}
String currencyType="";
if(request.getParameter("currencyType")!=null){
	currencyType=request.getParameter("currencyType");
}
String specialDays[]=new String[5];
String days="";
if(request.getParameter("specialDays")!=null){
	specialDays=request.getParameterValues("specialDays");
	if(specialDays.length>0){
		for(int s=0;s<specialDays.length;s++){
			days=days+","+specialDays[s];
		}
	}
}
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat  df1= new SimpleDateFormat("dd/MM/yyyy");
DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat UserDate=new SimpleDateFormat("dd MMMM");
DateFormat  dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat  dateFormat4 = new SimpleDateFormat("dd-MM-yyyy");
DateFormat  dateFormat3= new SimpleDateFormat("MMMM-dd");
DateFormat  dbdate1= new SimpleDateFormat("MM-dd");
dateFormat.setTimeZone(tz);
dateFormat2.setTimeZone(tz);
df1.setTimeZone(tz);
String time="";
time = c1.get(Calendar.HOUR) + " : " + c1.get(Calendar.MINUTE);
if(c1.get(Calendar.AM_PM)==0)
    time=time+" AM";
else
    time=time+" PM";

String date=(dateFormat.format(c1.getTime())).toString();
 boolean x=false;
 String emp_id=session.getAttribute("empId").toString();
 DecimalFormat df2 = new DecimalFormat("###.##");
 Double totalbillAMt=0.00;
 
 String applictn=request.getParameter("applicatn");
 String title="";
 if(request.getParameter("title")!=null){
	 title=request.getParameter("title").replaceAll("'", "@");
	}
 String lastname="";
 if(request.getParameter("lastname")!=null){
	 lastname=request.getParameter("lastname").replaceAll("'", "@");
	}
 String gothram="";
 if(request.getParameter("gothram")!=null){
	 gothram=request.getParameter("gothram").replaceAll("'", "@");
	}
 String emailid=request.getParameter("emailid");
 String pancardNo="";
 if(request.getParameter("pancardNo")!=null){
	 pancardNo=request.getParameter("pancardNo").replaceAll("'", "@");
	}
 String dob=request.getParameter("dob");
 String address2="";
 if(request.getParameter("address2")!=null){
	 address2=request.getParameter("address2").replaceAll("'", "@");
	}
 String address3="";
 if(request.getParameter("address3")!=null){
	 address3=request.getParameter("address3").replaceAll("'", "@");
	}
 String city="";
 if(request.getParameter("city")!=null){
	 city=request.getParameter("city").replaceAll("'", "@");
	}
 String pincode="";
 if(request.getParameter("pincode")!=null){
	 pincode=request.getParameter("pincode").replaceAll("'", "@");
	}
 String AppRefNo="";
 if(request.getParameter("AppRefNo")!=null){
	 AppRefNo=request.getParameter("AppRefNo").replaceAll("'", "@");
	}
 String fromdate="";
 if(request.getParameter("fromdate")!=null && request.getParameter("fromdate")!=""){
	 fromdate=request.getParameter("fromdate")+" 00:00:01";
 }
 String todate="";
 if(request.getParameter("todate")!=null && request.getParameter("todate")!=""){
	 todate=request.getParameter("todate")+" 23:59:59"; 
 }
 if((request.getParameter("todate")==null || request.getParameter("todate").equals("")) && request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals("")){
	 todate=request.getParameter("fromdate")+" 23:59:59";
 }
 String performedDate="";
 if(request.getParameter("performedMonth")!=null && !request.getParameter("performedMonth").equals("")  && request.getParameter("performedDay")!=null && !request.getParameter("performedDay").equals("")){
	 performedDate=request.getParameter("performedMonth")+"-"+request.getParameter("performedDay");
 }
 
 String doa="";
 if(request.getParameter("doamonth") != null && !request.getParameter("doamonth").equals("") && !request.getParameter("doadate").equals("")){
 	doa=request.getParameter("doamonth")+"-"+request.getParameter("doadate");
 }
 
 String paymentType=request.getParameter("paymentType");
 String chequeNo="";
 String nameOnCheque="";
 String otherCashPaymentType="";
 String fourDigitNum = "";
 String authenticationNum = "";
 String trnMobile = "";
 String trxNum = "";
 if(request.getParameter("chequeNo")!=null){
	 chequeNo=request.getParameter("chequeNo");
 }
 if(request.getParameter("nameOnCheque")!=null){
	 nameOnCheque=request.getParameter("nameOnCheque");
 }
 if(paymentType.equals("othercash")  && request.getParameter("otherCashPaymentType")!=null){
	 otherCashPaymentType=request.getParameter("otherCashPaymentType");
 }
 if(paymentType.equals("card"))
 {
	 fourDigitNum=request.getParameter("fourDigits");
	 authenticationNum=request.getParameter("authNum");
 }
 if(paymentType.equals("googlepay"))
 {
	 trnMobile=request.getParameter("trMobile");
	 trxNum=request.getParameter("trnxNum");
 }
 if(paymentType.equals("phonepe"))
 {
	 trnMobile=request.getParameter("trMobile");
	 trxNum=request.getParameter("trnxNum");
 }
%>
<jsp:setProperty name="CSP" property="date" value="<%=date%>"/>
<jsp:setProperty name="CSP" property="productId" value="<%=productId%>"/>
<jsp:setProperty name="CSP" property="quantity" value="<%=ticQty%>"/>
<jsp:setProperty name="CSP" property="rate" value="<%=Amount%>"/>
<jsp:setProperty name="CSP" property="totalAmmount" value="<%=totAmt %>"/>
<jsp:setProperty name="CSP" property="billingId" value="<%=billingId%>"/>
<jsp:setProperty name="CSP" property="vocharNumber" value=""/>
<jsp:setProperty name="CSP" property="cash_type" value="<%=paymentType %>"/>
<jsp:setProperty name="CSP" property="customername" value="<%=customerName%>"/>
<jsp:setProperty name="CSP" property="phoneno" value="<%=mobile%>"/>
<jsp:setProperty name="CSP" property="emp_id" value="<%=emp_id%>"/>
<jsp:setProperty name="CSP" property="extra1" value="<%=HOAID%>"/>
<jsp:setProperty name="CSP" property="extra2" value="<%=purpose%>"/>
<jsp:setProperty name="CSP" property="extra3" value="<%=fromdate%>"/>
<jsp:setProperty name="CSP" property="extra4" value="<%=todate%>"/>
<jsp:setProperty name="CSP" property="extra5" value="<%=currencyType%>"/>
<jsp:setProperty name="CSP" property="gothram" value="<%=gothram%>"/>
<jsp:setProperty name="CSP" property="offerkind_refid" value="<%=offerkind_refid%>"/>
<jsp:setProperty name="CSP" property="extra6" value="<%=address%>"/>
<jsp:setProperty name="CSP" property="extra7" value="<%=days%>"/>
<jsp:setProperty name="CSP" property="extra8" value="<%=emailid%>"/>
<jsp:setProperty name="CSP" property="cheque_no" value="<%=chequeNo%>"/>
<jsp:setProperty name="CSP" property="name_on_cheque" value="<%=nameOnCheque%>"/>
<jsp:setProperty name="CSP" property="extra18" value="<%=pancardNo%>"/>
<jsp:setProperty name="CSP" property="extra19" value="<%=otherCashPaymentType%>"/>
<jsp:setProperty name="CSP" property="extra10" value="<%=fourDigitNum%>"/>
<jsp:setProperty name="CSP" property="extra14" value="<%=authenticationNum%>"/>	
<jsp:setProperty name="CSP" property="extra29" value="<%=trnMobile%>"/>
<jsp:setProperty name="CSP" property="extra30" value="<%=trxNum%>"/>		
<%--  <%x=CSP.insert();%><%=CSP.geterror()%>  --%>
<%
if(applictn!=null && !applictn.equals("") && applictn.equals("application")){ %>
	<jsp:setProperty name="CAP" property="billingId" value="<%=billingId%>"/>
	<jsp:setProperty name="CAP" property="title" value="<%=title%>"/>
	<jsp:setProperty name="CAP" property="first_name" value="<%=customerName%>"/>
	<jsp:setProperty name="CAP" property="middle_name" value="<%=doa%>"/>	<!-- here we r storing date of anniversary in middle name column -->
	<jsp:setProperty name="CAP" property="last_name" value="<%=lastname%>"/>
	<jsp:setProperty name="CAP" property="email_id" value="<%=emailid%>"/>
	<jsp:setProperty name="CAP" property="phone" value="<%=AppRefNo%>"/>
	<jsp:setProperty name="CAP" property="dateof_birth" value="<%=dob%>"/>
	<jsp:setProperty name="CAP" property="address1" value="<%=address%>"/>
	<jsp:setProperty name="CAP" property="pincode" value="<%=pincode%>"/>
	<jsp:setProperty name="CAP" property="gothram" value="<%=gothram%>"/>
	<jsp:setProperty name="CAP" property="pancard_no" value="<%=pancardNo%>"/>
	<jsp:setProperty name="CAP" property="from_date" value="<%=fromdate%>"/>
	<jsp:setProperty name="CAP" property="to_date" value="<%=todate%>"/>
	<jsp:setProperty name="CAP" property="extra1" value="<%=performedDate%>"/>
	<jsp:setProperty name="CAP" property="extra2" value="<%=address2%>"/>
	<jsp:setProperty name="CAP" property="address2" value="<%=address3 %>"/>
	<jsp:setProperty name="CAP" property="extra3" value="<%=city%>"/>
	<%=CAP.insert()%><%=CAP.geterror()%>
<%} %>
<%
/* if(uniqueid == null || uniqueid.equals("")) */
if((uniqueid == null || uniqueid.equals("")) && !mobile.trim().equals(""))
{%>
	<jsp:setProperty property="title" name="DREG" value=""/>
	<%if((dob != null && !dob.equals("null") && !dob.equals("")) || !doa.equals("")) {%>
	<jsp:setProperty property="first_name" name="DREG" value="<%=lastname%>" /><%}else{ %>
	<jsp:setProperty property="first_name" name="DREG" value="<%=customerName %>"/><%} %>
	<jsp:setProperty property="middle_name" name="DREG" value=""/>
	<jsp:setProperty property="last_name" name="DREG" value=""/>
	<jsp:setProperty property="spouce_name" name="DREG" value=""/>
	<jsp:setProperty property="gender" name="DREG" value=""/>
	<jsp:setProperty property="dob" name="DREG" value="<%=dob %>"/>
	<jsp:setProperty property="address_1" name="DREG" value="<%=address %>"/>
	<jsp:setProperty property="address_2" name="DREG" value="<%=address2 %>"/>
	<jsp:setProperty property="country" name="DREG" value="<%=address3 %>"/>
	<jsp:setProperty property="city" name="DREG" value="<%=city %>"/>
	<jsp:setProperty property="pincode" name="DREG" value="<%=pincode %>"/> 
	<jsp:setProperty property="mobile" name="DREG" value="<%=mobile %>"/>
	<jsp:setProperty property="idproof_1" name="DREG" value=""/>
	<jsp:setProperty property="idproof1_num" name="DREG" value=""/>
	<jsp:setProperty property="pancard_no" name="DREG" value="<%=pancardNo %>"/>
	<jsp:setProperty property="email_id" name="DREG" value="<%=emailid %>"/>
	<jsp:setProperty property="updates_receive_via" name="DREG" value=""/>
	<jsp:setProperty property="gothram" name="DREG" value="<%=gothram %>"/>
	<jsp:setProperty property="register_date" name="DREG" value="<%=date %>"/>
	<jsp:setProperty property="extra4" name="DREG" value=""/>
	<jsp:setProperty property="refer_by" name="DREG" value="user"/>
	<jsp:setProperty property="doa" name="DREG" value="<%=doa%>"/>
	<%--  <%=DREG.insert() %><%=DREG.geterror()%>  --%>
	
	<%String sssst_id= DREG.insertWithId(); 
	if(sssst_id !=null && !sssst_id.equals("")){
	UniqueIdGroup uig=new UniqueIdGroup();
	uig.setUniqueid(sssst_id);
	uig.setDonor("");
	uig.setTrusties("");
	uig.setNad("");
	uig.setLta("");
	uig.setRds("");
	uig.setTimeshare("");
	uig.setEmployee("");
	uig.setGeneral("");
	uig.setCreatedDate(mySqlDateFormat.format(new Date()));
	uig.setUpdatedDate(mySqlDateFormat.format(new Date()));
	uig.setExtra1("");
	uig.setExtra2("");
	uig.setExtra3("");
	uig.setExtra4("");
	uig.setExtra5("");
	uig.setExtra6("");
	uig.setExtra7("");
	uig.setExtra8("");
	uig.setExtra9("");
	uig.setExtra10("");
	uig.setExtra11("");
	uig.setExtra12("");
	uig.setExtra13("");
	uig.setExtra14("");
	uig.setExtra15("");
	
	if(productId.equals("20092")){
		uig.setNad(DevoteeGroups.NAD.toString());
	}
	if(productId.equals("20063")){
		uig.setLta(DevoteeGroups.LTA.toString());
	}
	if(!productId.equals("20092") && !productId.equals("20063")  ){
		uig.setGeneral(DevoteeGroups.GENERAL.toString());
	}
	
	uniqueIdGroupService.insert(uig);
}
%>
<jsp:setProperty name="CSP" property="extra15" value="<%=sssst_id%>"/>	
<%}else{%>
	<jsp:setProperty name="CSP" property="extra15" value="<%=uniqueid%>"/>	
<%}
%>

<%x=CSP.insert();%><%=CSP.geterror()%>
<%

System.out.println("phneno....getting.."+request.getParameter("mobile"));
System.out.println("wtsp....getting.."+request.getParameter("wtsp"));
if(request.getParameter("wtsp").equals("ok")){
	CSP.addToOptin(request.getParameter("mobile"));
	//CSP.WtspNotification(request.getParameter("mobile"));
	wtsp.makeDocument(request, response);
	System.out.println("wtsp working with pdf..!");
}


if(!paymentType.equals("") && (paymentType.equals("cash") || paymentType.equals("cheque") || paymentType.equals("card") || paymentType.equals("googlepay") || paymentType.equals("phonepe"))){
if(x==true){
	mainClasses.cashier_reportListing CASH_RE=new mainClasses.cashier_reportListing();
	List cash_list=CASH_RE.getCounter(session.getAttribute("empId").toString(),"");
	for(int l=0;l<cash_list.size();l++)
	{
		CR=(beans.cashier_report)cash_list.get(l);
		if(CR.getcash3().equals(session.getAttribute("Role").toString()))
		{	
			if(CR.getlogout_time().equals(""))
			{
			String count_bal="";
			double counter_bal=0.00;
			totalbillAMt=Double.parseDouble(totAmt);
			counter_bal=(Double.parseDouble(CR.getcounter_balance().toString())+totalbillAMt);
			count_bal=""+Double.valueOf(df2.format(counter_bal));
	%>
	<jsp:setProperty name="COU" property="booking_id" value="<%=billingId%>"/>
	<jsp:setProperty name="COU" property="date" value="<%=date%>"/>
	<jsp:setProperty name="COU" property="login_id" value="<%=emp_id%>"/>
	<jsp:setProperty name="COU" property="booking_amount" value="<%=totAmt%>"/>
	<jsp:setProperty name="COU" property="opening_balance" value="<%=date%>"/>
	<jsp:setProperty name="COU" property="counter_balance" value="<%=count_bal%>"/>
	<jsp:setProperty name="COU" property="closing_balance" value=""/>
	<jsp:setProperty name="COU" property="booked_date" value="<%=date%>"/>
	<jsp:setProperty name="COU" property="counter_ex1" value=""/>
	<jsp:setProperty name="COU" property="counter_ex2" value=""/>
	<jsp:setProperty name="COU" property="counter_ex3" value=""/>
	<jsp:setProperty name="COU" property="counter_ex4" value=""/>
	<jsp:setProperty name="COU" property="counter_ex5" value=""/>
	<jsp:setProperty name="COU" property="counter_ex6" value=""/>
	<jsp:setProperty name="COU" property="counter_ex7" value=""/>
	<%=COU.insert()%><%=COU.geterror()%>
	<jsp:setProperty name="CSH" property="cashier_report_id" value="<%=CR.getcashier_report_id()%>"/>
    <jsp:setProperty name="CSH" property="counter_balance" value="<%=count_bal%>"/>
	<jsp:setProperty name="CSH" property="total_amount" value="<%=count_bal%>"/>
	<%=CSH.update() %><%=CSH.geterror() %>
<%}}
	}
}
}
/* NOG.updatInvoiceNumber(billingId,"proOfficeSales"); */
String certificatehtml="";
//response.sendRedirect("printTicket.jsp?bid="+billingId);
MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
if(request.getParameter("saveonly")!=null && request.getParameter("saveonly").equals("SAVE-ONLY")){
	 String performdate="";
	if(productId.equals("20092") || productId.equals("20063")) {
		if(performedDate !=null && !performedDate.equals("")){
			performdate = dateFormat3.format(dbdate1.parse(performedDate));
		}
	}else if(fromdate !=null && !fromdate.equals("")){
	 performdate = dateFormat2.format(dateFormat.parse(fromdate)); 
	}
//	model.sendmailservice sendMail=new model.sendmailservice();
	if(emailid != null && !emailid.equals("")){
   		
		StringBuilder  mailtemplate = new StringBuilder(); 		
    	mailtemplate.append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'><title>Insert title here</title><link href='css/acct-style.css' type='text/css' rel='stylesheet' /><style type='text/css'>.smallfont{color: maroon;font-weight:bold;font-size: 14px;}</style></head><body><div class='print-rep'><table width='100%' border='1' rules='all' cellspacing='0' cellpadding='0' style='width: 47%;margin: 0 auto;'><tr><td colspan='4' class='smallfont' style='font-size:14px;font-weight: bold;'>**SHRI SHIRDI SAIBABA SANSTHAN TRUST**</td></tr><tr><td colspan='4' align='center' style='font-size:12px;font-weight: bold;'>Regd.No 646/92</td></tr><tr><td colspan='4' align='center' style='font-weight:bold;font-size:12px'> Dilsukhnagar,Hyderabad-500060 PH:24066566</td></tr><tr><td colspan='4' align='center' style='font-size:13px; text-transform:lowercase;font-weight:bold;'> www.saisansthan.in </td></tr><tr><td colspan='4' align='center' class='smallfont'> RECEIPT</td></tr>");
    	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Receipt No :</td><td colspan='2' class='smallfont'>:").append(billingId).append("</td></tr>");
    	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Cashier :</td><td colspan='2' class='smallfont'>:").append(session.getAttribute("empName")).append("</td></tr>");
    	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Date :</td><td colspan='2' class='smallfont'>:").append(df1.format(c1.getTime())).append("-").append(time).append("</td></tr>");
    	if(CSP.getextra15() != null && !CSP.getextra15().trim().equals("")){
        	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Unique ID:</td><td colspan='2' class='smallfont'>").append(CSP.getextra15()).append("</td></tr>");
    	}
    	if(CSP.getphoneno() != null && !CSP.getphoneno().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Mobile No:</td><td colspan='2' class='smallfont'>").append(CSP.getphoneno()).append("</td></tr>");
    	}
    	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Received with thanks from Smt/Sri:</td><td colspan='2' class='smallfont'>").append(CSP.getcustomername()).append("</td></tr>");
    	if(CSP.getgothram() != null && !CSP.getgothram().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Gothram:</td><td colspan='2' class='smallfont'>").append(CSP.getgothram()).append("</td></tr>");
    	}
    	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Towards:</td>");
    	if(SUBHL.getMsubheadnames(CSP.getproductId(),CSP.getextra1()).trim().equals("")){
    		mailtemplate.append("<td colspan='2' class='smallfont'>").append(PROL.getProductNameByCat(CSP.getproductId(),CSP.getextra1())).append("</td></tr>");
    	}
    	else	{
    		mailtemplate.append("<td colspan='2' class='smallfont'>").append(SUBHL.getMsubheadnames(CSP.getproductId(),CSP.getextra1())).append("</td></tr>");
    	}
    	if(CSP.getcash_type().equals("offerKind") && !CSP.getextra11().trim().equals("") && CSP.getproductId().equals("20026")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>From Date:</td><td colspan='2' class='smallfont'>").append(df1.format(dateFormat2.parse(CSP.getextra11()))).append("</td></tr>");
    	}
    	if (CSP.getextra13() != null && !CSP.getextra13().trim().equals(""))	{
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Performed Time:</td><td colspan='2' class='smallfont'>").append(CSP.getextra13()).append("</td></tr>");
    	}
    	if(CSP.getcheque_no()!= null && !CSP.getcheque_no().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Cheque No:</td><td colspan='2' class='smallfont'>").append(CSP.getcheque_no()).append("</td></tr>");
    	}
    	if(CSP.getname_on_cheque()!= null && !CSP.getname_on_cheque().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Name On Cheque:</td><td colspan='2' class='smallfont'>").append(CSP.getname_on_cheque()).append("</td></tr>");
    	}
    	if(CAPL.getPerformerName(CSP.getbillingId())!=null && !CAPL.getPerformerName(CSP.getbillingId()).trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Perform in the name of:</td><td colspan='2' class='smallfont'>").append(CAPL.getPerformerName(CSP.getbillingId())).append("</td></tr>");
    	}
    	if(CAP.getextra1()!=null && !CAP.getextra1().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Performed Date:</td><td colspan='2' class='smallfont'>").append(dateFormat3.format(dbdate1.parse(CAP.getextra1()))).append("</td></tr>");
    	}
    	else if(CSP.getextra7() != null && !CSP.getextra7().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Performed Date:</td><td colspan='2' class='smallfont'>").append(CSP.getextra7()).append("</td></tr>");
    	}
    	if(CSP.getextra3() != null && !CSP.getextra3().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>From: </td><td colspan='2' class='smallfont'>").append(df1.format(dateFormat.parse(CSP.getextra3()))).append("</td></tr>");
    	}
    	if(CSP.getextra4()!=null && !CSP.getextra4().trim().equals("") && (CSP.getproductId().equals("20066") || CSP.getproductId().equals("21585") || CSP.getproductId().equals("21553") || CSP.getproductId().equals("21552") || CSP.getproductId().equals("25302") || CSP.getproductId().equals("25304") || CSP.getproductId().equals("21545") || CSP.getproductId().equals("21550") || CSP.getproductId().equals("21551") || CSP.getproductId().equals("21544") || CSP.getproductId().equals("21555")  || CSP.getproductId().equals("21548") || CSP.getproductId().equals("21547"))){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>To :</td><td colspan='2' class='smallfont'>").append(df1.format(dateFormat.parse(CSP.getextra4()))).append("</td></tr>");
    	}
    	if(CSP.getextra6()!=null && !CSP.getextra6().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Address:</td><td colspan='2' class='smallfont'>").append(CSP.getextra6()).append("</td></tr>");
    	}
    	if(CAP.getextra2()!=null && !CAP.getextra2().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Description:</td><td colspan='2' class='smallfont'>&nbsp;").append(CAP.getextra2()).append("</td></tr>");
    	}
    	if(CAP.getaddress2()!=null && !CAP.getaddress2().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Address2:</td><td colspan='2' class='smallfont'>&nbsp;").append(CAP.getaddress2()).append("</td></tr>");
    	}
    	if(CAP.getextra3()!=null && !CAP.getextra3().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>FromDate:</td><td colspan='2' class='smallfont'>&nbsp;").append(CAP.getextra3()).append("</td></tr>");
    	}
    	if(CAP.getpincode()!=null && !CAP.getpincode().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Pincode:</td><td colspan='2' class='smallfont'>&nbsp;").append(CAP.getpincode()).append("</td></tr>");
    	}
    	if(CSP.getextra2()!=null && !CSP.getextra2().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Purpose:</td><td colspan='2' class='smallfont'>").append(CSP.getextra2()).append("</td></tr>");
    	}
    	if(CSP.getofferkind_refid() != null && !CSP.getofferkind_refid().trim().equals("")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>OfferKind Ref No:</td><td colspan='2' class='smallfont'>").append(CSP.getofferkind_refid()).append("</td></tr>");
    	}
    	if(CSP.getextra5().equals("RS")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Rupees:</td><td colspan='2' class='smallfont'>").append(DF.format(Double.parseDouble((CSP.gettotalAmmount())))).append("</td></tr>");
    	}
    	else if(CSP.getextra5().equals("USD")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Dollars:</td><td colspan='2' class='smallfont'> $").append(DF.format(Double.parseDouble((CSP.gettotalAmmount())))).append("</td></tr>");
    	}
    	else if(CSP.getextra5().equals("POUND")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Pounds:</td><td colspan='2' class='smallfont'> &pound;").append(DF.format(Double.parseDouble((CSP.gettotalAmmount())))).append("</td></tr>");
    	}
    	else if(CSP.getcash_type().equals("offerKind")){
    		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Rupees:</td><td colspan='2' class='smallfont'>").append(DF.format(Double.parseDouble((CSP.gettotalAmmount())))).append("</td></tr>");
    	}
    	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Amount In Words:</td><td colspan='2' class='smallfont'>").append(NTW.convert(Integer.parseInt(CSP.gettotalAmmount()))).append(" only</td></tr>").append("<tr><td colspan='4' style='text-align: center;font-weight: bold;color: green;font-size: 14px;'>**WITH BLESSINGS OF SAIBABA**</td></tr></table></div></body></html>");
    	System.out.println("save mail template--"+mailtemplate.toString());
    	
//    	sendMail.sendmail("reports@saisansthan.in", emailid, "Saisansthan", mailtemplate.toString()); /* saisansthan_dsnr@yahoo.co.in */
    	sendMail.send("reports@saisansthan.in", emailid, "","", "Saisansthan-Dilsukhnagar", mailtemplate.toString(),"68.169.54.16");
    	if(productId.equals("20092")){
			certificatehtml="<div style='padding:15px; border:1px solid #000'><table width='100%' cellpadding='0' cellspacing='0' style='border-top:1px solid #000; border-right:1px solid #000;'><tr><td colspan='3' style='text-align:center; font-size:20px; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase; color:#8b421b;'><span>SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br /><span style='font-size:14px;'>( Regd. No. 646/92 )</span><br /><span style='font-size:16px;'>DILSUKHNAGAR, HYDERABAD - 500 060.</span><br/><span style='font-size:16px;'>phone : 040 24066566.</span><br/></td></tr><tr> <td colspan='3' style='text-align:center;  border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>NITHYA ANNADANA certificate</td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><span style='color:#8b421b;'>Annadanam MembershipNo :</span> "+CSP.getbillingId()+"<span style='float:right'><span style='color:#8b421b;'>Date of Annadanam :</span>"+performdate+"</span></td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><div style='float:left;color:#8b421b;'>Name</div><div style='width:200px; float:left; text-align:right'>:</div> <div style='float:left'>&nbsp;"+CSP.getcustomername()+"</div></td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><div style='float:left;color:#8b421b;'>Address</div> <div style='width:173px; text-align:right; float:left'>:</div> <div style='float:left'>&nbsp;"+address+"</div></td></tr><tr><td width='36%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>In whose name to be performed </td><td width='31%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Gothram</td><td width='33%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Amount Paid In Rs</td></tr><tr><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+lastname+"</td><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+CSP.getgothram()+"</td><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+DF.format(Double.parseDouble(CSP.getrate())*Double.parseDouble(CSP.getquantity()))+"</td></tr><tr><td colspan='2' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'></td><td  height='10' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Receipt No</td></tr><tr><td  colspan='2' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'></td><td height='10' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+CSP.getbillingId()+"/"+dateFormat4.format(mySqlDateFormat.parse(CSP.getdate()))+"</td></tr><tr><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Clerk</td><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Manager</td><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>General Secretary</td></tr></table></div>";
			System.out.println("certificatehtml---"+certificatehtml);
//			sendMail.sendmail("reports@saisansthan.in", emailid, "Your E-Certificate For NITHYA ANNADANAM",certificatehtml);
	    	sendMail.send("reports@saisansthan.in", emailid, "","", "Your E-Certificate For NITHYA ANNADANAM", certificatehtml,"68.169.54.16");

    	}
		else if(productId.equals("20063")){
			certificatehtml="<div style='padding:15px; border:1px solid #000'><table width='100%' cellpadding='0' cellspacing='0' style='border-top:1px solid #000; border-right:1px solid #000;'><tr><td colspan='3' style='text-align:center; font-size:20px; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase; color:#8b421b;'><span>SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br /><span style='font-size:14px;'>( Regd. No. 646/92 )</span><br /><span style='font-size:16px;'>DILSUKHNAGAR, HYDERABAD - 500 060.</span><br/><span style='font-size:16px;'>phone : 040 24066566.</span><br/></td></tr><tr> <td colspan='3' style='text-align:center;  border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>LIFETIME ARCHANA MEMBERSHIP certificate</td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><span style='color:#8b421b;'>Archana Membership No:</span> "+CSP.getbillingId()+"<span style='float:right'><span style='color:#8b421b;'>Date of Archana :</span>"+performdate+"</span></td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><div style='float:left;color:#8b421b;'>Name</div><div style='width:200px; float:left; text-align:right'>:</div> <div style='float:left'>&nbsp;"+CSP.getcustomername()+"</div></td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><div style='float:left;color:#8b421b;'>Address</div> <div style='width:173px; text-align:right; float:left'>:</div> <div style='float:left'>&nbsp;"+address+"</div></td></tr><tr><td width='36%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>In whose name to be performed </td><td width='31%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Gothram</td><td width='33%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Amount Paid In Rs</td></tr><tr><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+lastname+"</td><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+CSP.getgothram()+"</td><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+DF.format(Double.parseDouble(CSP.getrate())*Double.parseDouble(CSP.getquantity()))+"</td></tr><tr><td colspan='2' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'></td><td  height='10' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Receipt No</td></tr><tr><td  colspan='2' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'></td><td height='10' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+CSP.getbillingId()+"/"+dateFormat4.format(mySqlDateFormat.parse(CSP.getdate()))+"</td></tr><tr><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Clerk</td><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Manager</td><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>General Secretary</td></tr></table></div>";
			System.out.println("certificatehtml---"+certificatehtml);
//			sendMail.sendmail("reports@saisansthan.in", emailid, "Your E-Certificate For LIFETIME ARCHANA MEMBERSHIP",certificatehtml);
			sendMail.send("reports@saisansthan.in", emailid, "","", "Your E-Certificate For LIFETIME ARCHANA MEMBERSHIP", certificatehtml,"68.169.54.16");
		}
	}

	CallSmscApi smsService = new CallSmscApi();
	String message="Welcome to Shri Shirdi Saibaba Saisansthan Trust DSNR, Thank you for booking "+SUBHL.getSuheadName(productId)+" on "+performdate+" Worth Rs."+totAmt+" with referenceID "+billingId+"."; 
	smsService.SendSMS(mobile.trim(),message.replace(" ", "%20"));  
	response.sendRedirect("ticket-application.jsp"); 
}
else{
	 String performdatee="";
	if(productId.equals("20092") || productId.equals("20063")) { 
		if(performedDate !=null && !performedDate.equals("")){
			performdatee = dateFormat3.format(dbdate1.parse(performedDate));
		}
	}else if(fromdate !=null && !fromdate.equals("")){
	performdatee = dateFormat2.format(dateFormat.parse(fromdate));
	}
	model.sendmailservice sendMail1=new model.sendmailservice();
	if(emailid != null && !emailid.equals("")){
	
	StringBuilder mailtemplate = new StringBuilder();
   	mailtemplate.append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'><title>Insert title here</title><link href='css/acct-style.css' type='text/css' rel='stylesheet' /><style type='text/css'>.smallfont{color: maroon;font-weight:bold;font-size: 14px;}</style></head><body><div class='print-rep'><table width='100%' border='1' rules='all' cellspacing='0' cellpadding='0' style='width: 47%;margin: 0 auto;'><tr><td colspan='4' class='smallfont' style='font-size:14px;font-weight: bold;'>**SHRI SHIRDI SAIBABA SANSTHAN TRUST**</td></tr><tr><td colspan='4' align='center' style='font-size:12px;font-weight: bold;'>Regd.No 646/92</td></tr><tr><td colspan='4' align='center' style='font-weight:bold;font-size:12px'> Dilsukhnagar,Hyderabad-500060 PH:24066566</td></tr><tr><td colspan='4' align='center' style='font-size:13px; text-transform:lowercase;font-weight:bold;'> www.saisansthan.in </td></tr><tr><td colspan='4' align='center' class='smallfont'> RECEIPT</td></tr>");
	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Receipt No :</td><td colspan='2' class='smallfont'>").append(billingId).append("</td></tr>");
	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Cashier :</td><td colspan='2' class='smallfont'>").append(session.getAttribute("empName")).append("</td></tr>");
	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Date :</td><td colspan='2' class='smallfont'>").append(df1.format(c1.getTime())).append("-").append(time);
	if(CSP.getextra15() != null && !CSP.getextra15().trim().equals("")){
    	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Unique ID:</td><td colspan='2' class='smallfont'>").append(CSP.getextra15()).append("</td></tr>");
	}
	if(CSP.getphoneno() != null && !CSP.getphoneno().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Mobile No:</td><td colspan='2' class='smallfont'>").append(CSP.getphoneno()).append("</td></tr>");
	}
	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Received with thanks from Smt/Sri:</td><td colspan='2' class='smallfont'>").append(CSP.getcustomername()).append("</td></tr>");
	if(CSP.getgothram() != null && !CSP.getgothram().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Gothram:</td><td colspan='2' class='smallfont'>").append(CSP.getgothram()).append("</td></tr>");
	}
	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Towards:</td>");
	if(SUBHL.getMsubheadnames(CSP.getproductId(),CSP.getextra1()).trim().equals("")){
		mailtemplate.append("<td colspan='2' class='smallfont'>:").append(PROL.getProductNameByCat(CSP.getproductId(),CSP.getextra1())).append("</td></tr>");
	}
	else	{
		mailtemplate.append("<td colspan='2' class='smallfont'>:").append(SUBHL.getMsubheadnames(CSP.getproductId(),CSP.getextra1())).append("</td></tr>");
	}
	if(CSP.getcash_type().equals("offerKind") && !CSP.getextra11().equals("") && CSP.getproductId().equals("20026")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>From Date:</td><td colspan='2' class='smallfont'>").append(df1.format(dateFormat2.parse(CSP.getextra11()))).append("</td></tr>");
	}
	if (CSP.getextra13() != null && !CSP.getextra13().trim().equals(""))	{
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Performed Time:</td><td colspan='2' class='smallfont'>").append(CSP.getextra13()).append("</td></tr>");
	}
	if(CSP.getcheque_no()!= null && !CSP.getcheque_no().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Cheque No:</td><td colspan='2' class='smallfont'>").append(CSP.getcheque_no()).append("</td></tr>");
	}
	if(CSP.getname_on_cheque()!= null && !CSP.getname_on_cheque().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Name On Cheque:</td><td colspan='2' class='smallfont'>").append(CSP.getname_on_cheque()).append("</td></tr>");
	}
	if(CAPL.getPerformerName(CSP.getbillingId())!=null && !CAPL.getPerformerName(CSP.getbillingId()).trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Perform in the name of:</td><td colspan='2' class='smallfont'>").append(CAPL.getPerformerName(CSP.getbillingId())).append("</td></tr>");
	}
	if(CAP.getextra1()!=null && !CAP.getextra1().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Performed Date:</td><td colspan='2' class='smallfont'>").append(dateFormat3.format(dbdate1.parse(CAP.getextra1()))).append("</td></tr>");
	}
	else if(CSP.getextra7() != null && !CSP.getextra7().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Performed Date:</td><td colspan='2' class='smallfont'>").append(CSP.getextra7()).append("</td></tr>");
	}
	if(CSP.getextra3() != null && !CSP.getextra3().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>From:</td><td colspan='2' class='smallfont'>").append(df1.format(dateFormat.parse(CSP.getextra3()))).append("</td></tr>");
	}
	if(CSP.getextra4()!=null && !CSP.getextra4().trim().equals("") && (CSP.getproductId().equals("20066") || CSP.getproductId().equals("21585") || CSP.getproductId().equals("21553") || CSP.getproductId().equals("21552") || CSP.getproductId().equals("25302") || CSP.getproductId().equals("25304") || CSP.getproductId().equals("21545") || CSP.getproductId().equals("21550") || CSP.getproductId().equals("21551") || CSP.getproductId().equals("21544") || CSP.getproductId().equals("21555")  || CSP.getproductId().equals("21548") || CSP.getproductId().equals("21547"))){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>To: </td><td colspan='2' class='smallfont'>").append(df1.format(dateFormat.parse(CSP.getextra4()))).append("</td></tr>");
	}
	if(CSP.getextra6()!=null && !CSP.getextra6().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Address:</td><td colspan='2' class='smallfont'>").append(CSP.getextra6()).append("</td></tr>");
	}
	if(CAP.getextra2()!=null && !CAP.getextra2().trim().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Description:</td><td colspan='2' class='smallfont'>&nbsp;").append(CAP.getextra2()).append("</td></tr>");
	}
	if(CAP.getaddress2()!=null && !CAP.getaddress2().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Address2:</td><td colspan='2' class='smallfont'>&nbsp;").append(CAP.getaddress2()).append("</td></tr>");
	}
	if(CAP.getextra3()!=null && !CAP.getextra3().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>FromDate:</td><td colspan='2' class='smallfont'>&nbsp;").append(CAP.getextra3()).append("</td></tr>");
	}
	if(CAP.getpincode()!=null && !CAP.getpincode().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Pincode:</td><td colspan='2' class='smallfont'>&nbsp;").append(CAP.getpincode()).append("</td></tr>");
	}
	if(CSP.getextra2()!=null && !CSP.getextra2().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Purpose:</td><td colspan='2' class='smallfont'>:").append(CSP.getextra2()).append("</td></tr>");
	}
	if(CSP.getofferkind_refid() != null && !CSP.getofferkind_refid().trim().equals("")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>OfferKind Ref No:</td><td colspan='2' class='smallfont'>").append(CSP.getofferkind_refid()).append("</td></tr>");
	}
	if(CSP.getextra5().equals("RS")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Rupees:</td><td colspan='2' class='smallfont'>").append(DF.format(Double.parseDouble((CSP.gettotalAmmount())))).append(" only</td></tr>");
	}
	else if(CSP.getextra5().equals("USD")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Dollars:</td><td colspan='2' class='smallfont'> $").append(DF.format(Double.parseDouble((CSP.gettotalAmmount())))).append(" only</td></tr>");
	}
	else if(CSP.getextra5().equals("POUND")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Pounds:</td><td colspan='2' class='smallfont'> &pound;").append(DF.format(Double.parseDouble((CSP.gettotalAmmount())))).append(" only</td></tr>");
	}
	else if(CSP.getcash_type().equals("offerKind")){
		mailtemplate.append("<tr><td colspan='2' class='smallfont'>Rupees:</td><td colspan='2' class='smallfont'>").append(DF.format(Double.parseDouble((CSP.gettotalAmmount())))).append(" only</td></tr>");
	}
	mailtemplate.append("<tr><td colspan='2' class='smallfont'>Amount In Words:</td><td colspan='2' class='smallfont'>").append(NTW.convert(Integer.parseInt(CSP.gettotalAmmount()))).append(" only</td></tr>").append("<tr><td colspan='4' style='text-align: center;font-weight: bold;color: green;font-size: 14px;'>**WITH BLESSINGS OF SAIBABA**</td></tr></table></div></body></html>");
    System.out.println("print mail template--"+mailtemplate.toString());
    
//	sendMail.sendmail("reports@saisansthan.in", emailid, "Saisansthan", mailtemplate.toString()); /* saisansthan_dsnr@yahoo.co.in */
	sendMail.send("reports@saisansthan.in", emailid, "","", "Saisansthan-Dilsukhnagar", mailtemplate.toString(),"68.169.54.16");
	sendMail.send("reports@saisansthan.in","sankar@kreativwebsolutions.co.uk", "","", "Saisansthan-Dilsukhnagar", mailtemplate.toString(),"68.169.54.16");
	if(productId.equals("20092")){
		certificatehtml="<div style='padding:15px; border:1px solid #000'><table width='100%' cellpadding='0' cellspacing='0' style='border-top:1px solid #000; border-right:1px solid #000;'><tr><td colspan='3' style='text-align:center; font-size:20px; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase; color:#8b421b;'><span>SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br /><span style='font-size:14px;'>( Regd. No. 646/92 )</span><br /><span style='font-size:16px;'>DILSUKHNAGAR, HYDERABAD - 500 060.</span><br/><span style='font-size:16px;'>phone : 040 24066566.</span><br/></td></tr><tr> <td colspan='3' style='text-align:center;  border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>NITHYA ANNADANA certificate</td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><span style='color:#8b421b;'>Annadanam MembershipNo :</span> "+CSP.getbillingId()+"<span style='float:right'><span style='color:#8b421b;'>Date of Annadanam :</span>"+performdatee+"</span></td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><div style='float:left;color:#8b421b;'>Name</div><div style='width:200px; float:left; text-align:right'>:</div> <div style='float:left'>&nbsp;"+CSP.getcustomername()+"</div></td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><div style='float:left;color:#8b421b;'>Address</div> <div style='width:173px; text-align:right; float:left'>:</div> <div style='float:left'>&nbsp;"+address+"</div></td></tr><tr><td width='36%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>In whose name to be performed </td><td width='31%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Gothram</td><td width='33%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Amount Paid In Rs</td></tr><tr><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+lastname+"</td><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+CSP.getgothram()+"</td><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+DF.format(Double.parseDouble(CSP.getrate())*Double.parseDouble(CSP.getquantity()))+"</td></tr><tr><td colspan='2' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'></td><td  height='10' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Receipt No</td></tr><tr><td  colspan='2' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'></td><td height='10' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+CSP.getbillingId()+"/"+dateFormat4.format(mySqlDateFormat.parse(CSP.getdate()))+"</td></tr><tr><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Clerk</td><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Manager</td><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>General Secretary</td></tr></table></div>";
		System.out.println("certificatehtml---"+certificatehtml);
//		sendMail.sendmail("reports@saisansthan.in", emailid, "Your E-Certificate For NITHYA ANNADANAM",certificatehtml);
    	sendMail.send("reports@saisansthan.in", emailid, "","", "Your E-Certificate For NITHYA ANNADANAM", certificatehtml,"68.169.54.16");
    	sendMail.send("reports@saisansthan.in","sankar@kreativwebsolutions.co.uk", "","", "Customer E-Certificate For NITHYA ANNADANAM", certificatehtml,"68.169.54.16");
	}
	else if(productId.equals("20063")){
		certificatehtml="<div style='padding:15px; border:1px solid #000'><table width='100%' cellpadding='0' cellspacing='0' style='border-top:1px solid #000; border-right:1px solid #000;'><tr><td colspan='3' style='text-align:center; font-size:20px; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase; color:#8b421b;'><span>SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br /><span style='font-size:14px;'>( Regd. No. 646/92 )</span><br /><span style='font-size:16px;'>DILSUKHNAGAR, HYDERABAD - 500 060.</span><br/><span style='font-size:16px;'>phone : 040 24066566.</span><br/></td></tr><tr> <td colspan='3' style='text-align:center;  border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>LIFETIME ARCHANA MEMBERSHIP certificate</td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><span style='color:#8b421b;'>Archana Membership No:</span> "+CSP.getbillingId()+"<span style='float:right'><span style='color:#8b421b;'>Date of Archana :</span>"+performdatee+"</span></td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><div style='float:left;color:#8b421b;'>Name</div><div style='width:200px; float:left; text-align:right'>:</div> <div style='float:left'>&nbsp;"+CSP.getcustomername()+"</div></td></tr><tr><td colspan='3' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'><div style='float:left;color:#8b421b;'>Address</div> <div style='width:173px; text-align:right; float:left'>:</div> <div style='float:left'>&nbsp;"+address+"</div></td></tr><tr><td width='36%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>In whose name to be performed </td><td width='31%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Gothram</td><td width='33%' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Amount Paid In Rs</td></tr><tr><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+lastname+"</td><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+CSP.getgothram()+"</td><td style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+DF.format(Double.parseDouble(CSP.getrate())*Double.parseDouble(CSP.getquantity()))+"</td></tr><tr><td colspan='2' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'></td><td  height='10' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Receipt No</td></tr><tr><td  colspan='2' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'></td><td height='10' style='border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;'>"+CSP.getbillingId()+"/"+dateFormat4.format(mySqlDateFormat.parse(CSP.getdate()))+"</td></tr><tr><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Clerk</td><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>Manager</td><td height='100' style='vertical-align:bottom; text-align:center; border-bottom:1px solid #000; border-left:1px solid #000; padding:5px; text-transform:uppercase;color:#8b421b;'>General Secretary</td></tr></table></div>";
		System.out.println("certificatehtml---"+certificatehtml);
//		sendMail.sendmail("reports@saisansthan.in", emailid, "Your E-Certificate For LIFETIME ARCHANA MEMBERSHIP",certificatehtml);
		sendMail.send("reports@saisansthan.in", emailid, "","", "Your E-Certificate For LIFETIME ARCHANA MEMBERSHIP", certificatehtml,"68.169.54.16");
		sendMail.send("reports@saisansthan.in","sankar@kreativwebsolutions.co.uk", "","", "Customer E-Certificate For LIFETIME ARCHANA MEMBERSHIP", certificatehtml,"68.169.54.16");	
		
	}
	
	}
	
	//CallSmsApi1 smsService = new CallSmsApi1();
	CallSmscApi smsService = new CallSmscApi();
	String message1="Welcome to Shri Shirdi Saibaba Saisansthan Trust DSNR, Thank you for booking "+SUBHL.getSuheadName(productId)+" on "+performdatee+" Worth Rs."+totAmt+" with referenceID "+billingId+"."; 
 smsService.SendSMS(mobile.trim(),message1.replace(" ", "%20"));
	
	if(productId.equals("20092")) {
		response.sendRedirect("ticket-application.jsp?bid="+billingId+"&seva=nitya-annadaanam"); 
	}else  if(productId.equals("20063")){
		response.sendRedirect("ticket-application.jsp?bid="+billingId+"&seva=lifetime-archana"); 
	}else{
		response.sendRedirect("ticket-application.jsp?bid="+billingId); 
	}
}
%>
</body>
</html>