<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="org.apache.commons.fileupload.FileUploadException"%>
<%@page import="org.apache.commons.fileupload.FileItemFactory"%>
<%@page import="beans.sssst_registrations" %>
<%@page import="beans.sssst_registrationsService" %>
<%@page import="beans.UniqueIdGroup" %>
<%@page import="beans.UniqueIdGroupService" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
   <%@ page import="java.io.File" %>
   <%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>	
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DEVOTEE REGISTRATION EDIT|| SSSST,DSNR</title>
</head>
<body>
<jsp:useBean id="SST" class="beans.sssst_registrationsService"></jsp:useBean>
<jsp:useBean id="UNG" class="beans.UniqueIdGroupService"></jsp:useBean>
<%
String bid="";
String emailid = "";
String mobile="";
String title= "";
String firstname="";
String middlename="";
String lastname="";
String spousename="";
String gender="";
String extra3="";
String dob="";
String gothram="";
String pancard="";
String updates="";
String address1="";
String address2="";
String address3="";
String city="";
String state="";
String pincode="";
String idproof="";
String idproofnumber="";
String narration="";
String fathername="";
String mothername="";
String mothertongue="";
String bloodgroup="";
String education_qualification="";
String previous_experience="";
String date_of_appointed="";
String dependents="";
String designation="";
String identification_marks="";
String height="";
String weight="";
String basic_pay="";
String present_pay_scale="";
String occupation="";
String permanent_address = "";
String present_address = "";
String service_experinece_in_sansthan_trust = "";
String technical_qualification = "";
String caste_religion = "";
String physical_disorder = "";
String govt_reservation = "";
String enclosures = "";
String doa = "";
String reservation_cat = "";
String percent_of_disorder = "";
String image="";
String filename="";


String devoteeGroup = "";
String devoteeGroup1 = "";
String devoteeGroup2 = "";
String devoteeGroup3 = "";
String devoteeGroup4 = "";
String devoteeGroup5 = "";
/* String devoteeGroup6 = request.getParameter("devoteeGroup6"); */
String devoteeGroup7 = "";
/* String devoteeGroup8 = request.getParameter("devoteeGroup8"); */

boolean isMultipart = ServletFileUpload.isMultipartContent(request);

if (!isMultipart) {

} else {
	FileItemFactory factory = new DiskFileItemFactory();
	ServletFileUpload upload = new ServletFileUpload(factory);
	List items = null;
	try {
	items = upload.parseRequest(request);
	} catch (FileUploadException e) {
	out.print(e);
	}
	Iterator itr = items.iterator();
	while (itr.hasNext()) 
	  {
		FileItem item = (FileItem) itr.next();
		if (item.isFormField())
		{
		     String name = item.getFieldName();
			  String value = item.getString();
			  
			  if(name.equals("receiptNo"))
		       {  
				  bid=value;     
		       }
			  
			  if(name.equals("email-id"))
		       {  
				  emailid=value;     
		       }
			  
			  if(name.equals("mobile"))
		       {  
				  mobile=value;     
		       }
			  
			  if(name.equals("title"))
		       {  
				  title=value;     
		       }
			  
			  if(name.equals("firstname"))
		       {  
				  firstname=value;     
		       }
			  
			  if(name.equals("middlename"))
		       {  
				  middlename=value;     
		       }
			  
			  if(name.equals("lastname"))
		       {  
				  lastname=value;     
		       }
			  
			  if(name.equals("spousename"))
		       {  
				  spousename=value;     
		       }
			  
			  if(name.equals("gender"))
		       {  
				  gender=value;     
		       }
			  
			  if(name.equals("star"))
		       {  
				  extra3=value;     
		       }
			  
			  if(name.equals("dob"))
		       {  
				  dob=value;     
		       }
			  
			  if(name.equals("gothram"))
		       {  
				  gothram=value;     
		       }
			  
			  if(name.equals("pancard"))
		       {  
				  pancard=value;     
		       }
			  
			  if(name.equals("updates"))
		       {  
				  updates=value;     
		       }
			  
			  if(name.equals("address1"))
		       {  
				  address1=value;     
		       }
			  
			  if(name.equals("address2"))
		       {  
				  address2=value;     
		       }
			  
			  if(name.equals("address3"))
		       {  
				  address3=value;     
		       }
			  
			  if(name.equals("city"))
		       {  
				  city=value;     
		       }
			  
			  if(name.equals("state"))
		       {  
				  state=value;     
		       }
			  
			  if(name.equals("pincode"))
		       {  
				  pincode=value;     
		       }
			  
			  if(name.equals("idproof1"))
		       {  
				  idproof=value;     
		       }
			  
			  if(name.equals("idproof_num"))
		       {  
				  idproofnumber=value;     
		       }
			  
			  if(name.equals("narration"))
		       {  
				  narration=value;     
		       }
			  
			  if(name.equals("fathername"))
		       {  
				  fathername=value;     
		       }
			  
			  if(name.equals("mothername"))
		       {  
				  mothername=value;     
		       }
			  
			  if(name.equals("mothertongue"))
		       {  
				  mothertongue=value;     
		       }
			  
			  if(name.equals("bloodgroup"))
		       {  
				  bloodgroup=value;     
		       }
			  
			  if(name.equals("education_qualification"))
		       {  
				  education_qualification=value;     
		       }
			  
			  if(name.equals("previous_experience"))
		       {  
				  previous_experience=value;     
		       }
			  
			  if(name.equals("date_of_appointed"))
		       {  
				  date_of_appointed=value;     
		       }
			  
			  if(name.equals("dependents"))
		       {  
				  dependents=value;     
		       }
			  
			  if(name.equals("designation"))
		       {  
				  designation=value;     
		       }
			  
			  if(name.equals("identification_marks"))
		       {  
				  identification_marks=value;     
		       }
			  
			  if(name.equals("height"))
		       {  
				  height=value;     
		       }
			  
			  if(name.equals("weight"))
		       {  
				  weight=value;     
		       }
			  
			  if(name.equals("basic_pay"))
		       {  
				  basic_pay=value;     
		       }
			  
			  if(name.equals("present_pay_scale"))
		       {  
				  present_pay_scale=value;     
		       }
			  
			  if(name.equals("occupation"))
		       {  
				  occupation=value;     
		       }
			  
			  if(name.equals("doamonth"))
		       {  
				  doa+=value+"-";     
		       }
		       
		       if(name.equals("doadate"))
		       {  
				  doa+=value;     
		       }
		       
		       if(name.equals("doamonth1"))
		       {  
				  doa+=value+"-";     
		       }
		       
		       if(name.equals("doadate1"))
		       {  
				  doa+=value;     
		       }
			  
			  
			  if(name.equals("devoteeGroup"))
		       {  
				  devoteeGroup=value;     
		       }
			  
			  if(name.equals("devoteeGroup1"))
		       {  
				  devoteeGroup1=value;     
		       }
			  
			  if(name.equals("devoteeGroup2"))
		       {  
				  devoteeGroup2=value;     
		       }
			  
			  if(name.equals("devoteeGroup3"))
		       {  
				  devoteeGroup3=value;     
		       }
			  
			  if(name.equals("devoteeGroup4"))
		       {  
				  devoteeGroup4=value;     
		       }
			  
			  if(name.equals("devoteeGroup5"))
		       {  
				  devoteeGroup5=value;     
		       }
			  
			  if(name.equals("devoteeGroup7"))
		       {  
				  devoteeGroup7=value;     
		       }
		} 
		else
	    {
		try 
		{
			
			String strDirectoy3=config.getServletContext().getRealPath("/Devotee_Imgs");
			// String strDirectoy3="E:/workspace/Saisansthan_accounts/WebContent/Devotee_Imgs"; 
			String fname=item.getFieldName();
			String name=item.getName();
			int dotPos = name.lastIndexOf(".")+1;
			String extension =name.substring(dotPos);
			   if(fname.equals("devoteeImage")&&(!name.trim().equals("")))
			   {
				   filename=bid+"."+extension;
				  image=filename;
				 	File savedFile1 = new File(strDirectoy3,filename);
				   item.write(savedFile1);
				 }
		} catch (Exception e) {
			   
			   out.print(e);
			   }
		}
	  }%>
			  
<jsp:setProperty name="SST" property="sssst_id"  value="<%=bid%>"/>
<jsp:setProperty name="SST" property="title"  value="<%=title%>"/>
<jsp:setProperty name="SST" property="first_name" value="<%=firstname%>"/>
<jsp:setProperty name="SST" property="middle_name" value="<%=middlename%>"/>
<jsp:setProperty name="SST" property="last_name" value="<%=lastname%>"/>
<jsp:setProperty name="SST" property="spouce_name" value="<%=spousename%>"/>
<jsp:setProperty name="SST" property="gender" value="<%=gender%>"/>
<jsp:setProperty name="SST" property="dob" value="<%=dob%>"/>
<jsp:setProperty name="SST" property="address_1" value="<%=address1%>"/>
<jsp:setProperty name="SST" property="address_2" value="<%=address2%>"/>
<jsp:setProperty name="SST" property="country" value="<%=address3%>"/>
<jsp:setProperty name="SST" property="city" value="<%=city%>"/>
<jsp:setProperty property="state" name="SST" value="<%=state %>"/>
<jsp:setProperty name="SST" property="pincode" value="<%=pincode%>"/>
<jsp:setProperty name="SST" property="image" value="<%=filename%>"/>
<jsp:setProperty name="SST" property="mobile" value="<%=mobile%>"/>
<jsp:setProperty name="SST" property="idproof_1" value="<%=idproof%>"/>
<jsp:setProperty name="SST" property="idproof1_num" value="<%=idproofnumber%>"/>
<jsp:setProperty name="SST" property="pancard_no" value="<%=pancard%>"/>
<jsp:setProperty name="SST" property="email_id" value="<%=emailid%>"/>
<jsp:setProperty name="SST" property="updates_receive_via" value="<%=updates%>"/>
<jsp:setProperty name="SST" property="gothram" value="<%=gothram%>"/>
<jsp:setProperty property="extra3" name="SST" value="<%=extra3 %>"/>
<jsp:setProperty name="SST" property="remarks" value="<%=narration%>"/>
<jsp:setProperty property="father_name" name="SST" value="<%=fathername %>"/>
<jsp:setProperty property="mother_name" name="SST" value="<%=mothername %>"/>
<jsp:setProperty property="mothertongue" name="SST" value="<%=mothertongue %>"/>
<jsp:setProperty property="bloodgroup" name="SST" value="<%=bloodgroup %>"/>
<jsp:setProperty property="education_qualification" name="SST" value="<%=education_qualification %>"/>
<jsp:setProperty property="previous_experience" name="SST" value="<%=previous_experience %>"/>
<jsp:setProperty property="date_of_appointed" name="SST" value="<%=date_of_appointed %>"/>
<jsp:setProperty property="dependents" name="SST" value="<%=dependents %>"/>
<jsp:setProperty property="designation" name="SST" value="<%=designation %>"/>
<jsp:setProperty property="identification_marks" name="SST" value="<%=identification_marks %>"/>
<jsp:setProperty property="height" name="SST" value="<%=height %>"/>
<jsp:setProperty property="weight" name="SST" value="<%=weight %>"/>
<jsp:setProperty property="basic_pay" name="SST" value="<%=basic_pay %>"/>
<jsp:setProperty property="present_pay_scale" name="SST" value="<%=present_pay_scale %>"/>
<jsp:setProperty property="occupation" name="SST" value="<%=occupation %>"/>
<jsp:setProperty property="permanent_address" name="SST" value="<%=permanent_address %>"/>
<jsp:setProperty property="present_address" name="SST" value="<%=present_address %>"/>
<jsp:setProperty property="service_experinece_in_sansthan_trust" name="SST" value="<%=service_experinece_in_sansthan_trust %>"/>
<jsp:setProperty property="technical_qualification" name="SST" value="<%=technical_qualification %>"/>
<jsp:setProperty property="caste_religion" name="SST" value="<%=caste_religion %>"/>
<jsp:setProperty property="physical_disorder" name="SST" value="<%=physical_disorder %>"/>
<jsp:setProperty property="govt_reservation" name="SST" value="<%=govt_reservation %>"/>
<jsp:setProperty property="enclosures" name="SST" value="<%=enclosures %>"/>
<jsp:setProperty property="doa" name="SST" value="<%=doa %>"/>
<jsp:setProperty property="reservation_cat" name="SST" value="<%=reservation_cat %>"/>
<jsp:setProperty property="percent_of_disorder" name="SST" value="<%=percent_of_disorder %>"/>

<%=SST.update()%>
<%
UniqueIdGroupService uniqueIdGroupService=new UniqueIdGroupService();
UniqueIdGroup uig=new UniqueIdGroup();
uig.setUniqueid(bid);

/* if(devoteeGroup == null){
	uig.setDonor("");
}else{
uig.setDonor(devoteeGroup);
}
if(devoteeGroup1 == null){
	uig.setTrusties("");
}else{
uig.setTrusties(devoteeGroup1);
}
if(devoteeGroup2 == null){
	uig.setNad("");
}else{
uig.setNad(devoteeGroup2);
}
if(devoteeGroup3 == null){
	uig.setLta("");	
}else{
uig.setLta(devoteeGroup3);
}
if(devoteeGroup4 == null){
	uig.setRds("");
}else{
uig.setRds(devoteeGroup4);
}
if(devoteeGroup5 == null){
	uig.setTimeshare("");
}else{
uig.setTimeshare(devoteeGroup5);
}
if(devoteeGroup7 == null){
	uig.setGeneral("");
}else{
uig.setGeneral(devoteeGroup7);
} */

if(devoteeGroup.equals("")){
	uig.setDonor("");
}else{
uig.setDonor(devoteeGroup);
}
if(devoteeGroup1.equals("")){
	uig.setTrusties("");
}else{
uig.setTrusties(devoteeGroup1);
}
if(devoteeGroup2.equals("")){
	uig.setNad("");
}else{
uig.setNad(devoteeGroup2);
}
if(devoteeGroup3.equals("")){
	uig.setLta("");	
}else{
uig.setLta(devoteeGroup3);
}
if(devoteeGroup4.equals("")){
	uig.setRds("");
}else{
uig.setRds(devoteeGroup4);
}
if(devoteeGroup5.equals("")){
	uig.setTimeshare("");
}else{
uig.setTimeshare(devoteeGroup5);
}
if(devoteeGroup7.equals("")){
	uig.setGeneral("");
}else{
uig.setGeneral(devoteeGroup7);
}

uniqueIdGroupService.update(uig);
%>

<%}
	//response.sendRedirect("Devotees-Registrations-List.jsp");
	response.sendRedirect("Devotees-Registrations-Search.jsp"); 
%>
</body>
</html>