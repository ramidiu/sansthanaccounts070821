<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SHOP SALES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
        <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#DoctorError').hide();
	$('#QualificationError').hide();
	$('#PhoneError').hide();
	$('#AddressError').hide();

	if($('#doctorName').val().trim()==""){
		$('#DoctorError').show();
		$('#doctorName').focus();
		return false;
	}
	if($('#doctorQualification').val().trim()==""){
		$('#QualificationError').show();
		$('#doctorQualification').focus();
		return false;
	}
	if($('#phoneNo').val().trim()==""){
		$('#PhoneError').show();
		$('#phoneNo').focus();
		return false;
	}
	if($('#address').val().trim()==""){
		$('#AddressError').show();
		$('#address').focus();
		return false;
	}
}
function conditionCheck(){
	if($("#consFromDay").val()=="Every-Day"){
		$("#consToDay").removeAttr("required","required");
	    $("#consToDay").attr("disabled","disabled");
	}else{
		$("#consToDay").attr("required","required");
		$("#consToDay").removeAttr("disabled");
	}
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="DOC" class="beans.doctordetails"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Doctor Registration <%if(request.getParameter("id")!=null) {%>Edit<%}%></div>
<div class="vender-details">
<%if(request.getParameter("id")!=null) {
List DOCTE=	DOCT.getMdoctordetails(request.getParameter("id").toString());
DOC=(doctordetails)DOCTE.get(0);
%>
<form name="tablets_Update" method="post" action="doctordetails_Update.jsp" onsubmit="return validate();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="doctorId" id="doctorId" value="<%=DOC.getdoctorId()%>"/>
<tr>
<td width="35%">
<!-- <div class="warning" id="DoctorError" style="display: none;">Please Provide  "Doctor Name".</div> -->
Doctor Name*</td>
<td >
<!-- <div class="warning" id="QualificationError" style="display: none;">Please Provide  "Qualification".</div> -->
Qualification*</td>
<td>Specialist In</td>
</tr>
<tr>
<td><input type="text" name="doctorName" id="doctorName" value="<%=DOC.getdoctorName() %>" required placeHolder="Enter doctor name" /></td>
<td><input type="text" name="doctorQualification" id="doctorQualification" value="<%=DOC.getdoctorQualification()%>" required placeHoder="Enter Doctor qualification"/></td>
<td><%-- <input type="text" name="specialist" value="<%=DOC.getextra3() %>" placeholder="Enter doctor specialist in"></input> --%>
<select name="specialist">
<option value="GENERAL PHYSICIAN">GENERAL PHYSICIAN</option>
<option value="GYNAECOLOGY">GYNAECOLOGY</option>
<option value="ORTHOPAEDIC">ORTHOPAEDIC</option>
<option value="CARDIOLOGY">CARDIOLOGY</option>
<option value="GENERAL SURGEON">GENERAL SURGEON</option>
<option value="NEUROLOGY">NEUROLOGY</option>
<option value="PAEDIATRICIAN">PAEDIATRICIAN</option>
<option value="UROLOGIST">UROLOGIST</option>
<option value="E.N.T SPECIALIST">E.N.T SPECIALIST</option>
<option value="DERMOTOLOGY">DERMOTOLOGY</option>
<option value="DENTIST">DENTIST</option>
<option value="PHYSIOTHERAPHY">PHYSIOTHERAPHY</option>
<option value="HOMOEOPATHY">HOMOEOPATHY</option>
<option value="ACCUPRESSURE">ACCUPRESSURE</option>
<option value="OPTHALMOLOGIST">OPTHALMOLOGIST</option>
<option value="GOVT.HOSPITAL,SAROOR NAGAR">GOVT.HOSPITAL,SAROOR NAGAR</option>
</select>
</td>
</tr>
<tr>
<td>
<!-- <div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone No".</div> -->
Phone No*</td>
<td>Gender</td>
<td>Consulting Days</td>
</tr>
<tr>
<td><input type="text" name="phoneNo" id="phoneNo" onKeyPress="return numbersonly(this, event,true);" value="<%=DOC.getphoneNo() %>" required/></td>
<td><input type="radio" name="extra1" value="M" <%if(DOC.getextra1().equals("M")){%>checked="checked" <%} %> />Male<input type="radio" name="extra1" value="F" <%if(DOC.getextra1().equals("F")){%>checked="checked" <%} %>/>Female</td>
<td><select name="consFromDay" id="consFromDay" required onchange="conditionCheck();">
<option value="">Select From day</option>
<option value="Monday">Monday</option>
<option value="Tuesday">Tuesday</option>
<option value="Wednesday">Wednesday</option>
<option value="Thursday">Thursday</option>
<option value="Friday">Friday</option>
<option value="Saturday">Saturday</option>
<option value="Sunday">Sunday</option>
<option value="Every-Day">Every-Day</option>
</select>
TO
<select name="consToDay" id="consToDay" required>
<option value="">Select To day</option>
<option value="Monday">Monday</option>
<option value="Tuesday">Tuesday</option>
<option value="Wednesday">Wednesday</option>
<option value="Thursday">Thursday</option>
<option value="Friday">Friday</option>
<option value="Saturday">Saturday</option>
<option value="Sunday">Sunday</option>
</select></td>
</tr>
<tr><td>Timings</td><td>Address</td></tr>
<tr>
<td><select name="consFromTime" id="consFromTime" required  >
<option value="">From Time</option>
<%for(int f=01;f<13;f++){ %>
<option value="<%=f %>"><%=f %></option><%} %>
</select><select name="con1" >
<option value="AM">AM</option>
<option value="PM">PM</option>
</select>
TO
<select name="consToTime" id="consToTime" required >
<option value="">To Time</option>
<%for(int f=01;f<13;f++){ %>
<option value="<%=f %>"><%=f %></option><%} %>
</select>
</select>
<select name="con2" >
<option value="AM">AM</option>
<option value="PM">PM</option>
</select></td>
<td colspan="2"><!-- <div class="warning" id="AddressError" style="display: none;">Please Provide  "Address".</div> -->
<textarea name="address" id="address" placeholder="Enter Your Address*" required><%=DOC.getaddress() %></textarea></td>
</tr>
<tr> 
<td></td>
<td align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<% } else{%>
<form name="tablets_Update" method="post" action="doctordetails_Insert.jsp" onsubmit="return validate();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%">
<!-- <div class="warning" id="DoctorError" style="display: none;">Please Provide  "Doctor Name".</div> -->
Doctor Name*</td>
<td >
<!-- <div class="warning" id="QualificationError" style="display: none;">Please Provide  "Qualification".</div> -->
Qualification*</td>
<td>Specialist in</td>
</tr>
<tr>
<td><input type="text" name="doctorName" id="doctorName" value=""  required placeHolder="Enter doctor name"/></td>
<td><input type="text" name="doctorQualification" id="doctorQualification" value="" required placeHoder="Enter Doctor qualification"/></td>
<td><!-- <input type="text" name="specialist" value="" placeholder="Enter doctor specialist in"></input> -->
<select name="specialist">
<option value="GENERAL PHYSICIAN">GENERAL PHYSICIAN</option>
<option value="GYNAECOLOGY">GYNAECOLOGY</option>
<option value="ORTHOPAEDIC">ORTHOPAEDIC</option>
<option value="CARDIOLOGY">CARDIOLOGY</option>
<option value="GENERAL SURGEON">GENERAL SURGEON</option>
<option value="NEUROLOGY">NEUROLOGY</option>
<option value="PAEDIATRICIAN">PAEDIATRICIAN</option>
<option value="UROLOGIST">UROLOGIST</option>
<option value="E.N.T SPECIALIST">E.N.T SPECIALIST</option>
<option value="DERMOTOLOGY">DERMOTOLOGY</option>
<option value="DENTIST">DENTIST</option>
<option value="PHYSIOTHERAPHY">PHYSIOTHERAPHY</option>
<option value="HOMOEOPATHY">HOMOEOPATHY</option>
<option value="ACCUPRESSURE">ACCUPRESSURE</option>
<option value="OPTHALMOLOGIST">OPTHALMOLOGIST</option>
<option value="GOVT.HOSPITAL,SAROOR NAGAR">GOVT.HOSPITAL,SAROOR NAGAR</option>
</select>

</td>
</tr>
<tr>
<td>
<!-- <div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone No".</div> -->
Phone No*</td>
<td>Gender</td>
<td>Consulting Days</td>

</tr>
<tr>
<td><input type="text" name="phoneNo" id="phoneNo" onKeyPress="return numbersonly(this, event,true);" value="" required placeHoder="Enter phone number"/></td>
<td><input type="radio" name="extra1" value="M" checked="checked" />Male<input type="radio" name="extra1" value="F" />Female</td>
<td><select name="consFromDay" id="consFromDay" required onchange="conditionCheck();">
<option value="">Select From day</option>
<option value="Monday">Monday</option>
<option value="Tuesday">Tuesday</option>
<option value="Wednesday">Wednesday</option>
<option value="Thursday">Thursday</option>
<option value="Friday">Friday</option>
<option value="Saturday">Saturday</option>
<option value="Sunday">Sunday</option>
<option value="Every-Day">Every-Day</option>
</select>
TO
<select name="consToDay" id="consToDay" required>
<option value="">Select To day</option>
<option value="Monday">Monday</option>
<option value="Tuesday">Tuesday</option>
<option value="Wednesday">Wednesday</option>
<option value="Thursday">Thursday</option>
<option value="Friday">Friday</option>
<option value="Saturday">Saturday</option>
<option value="Sunday">Sunday</option>
</select></td>

</tr>
<tr><td>Timings</td><td>Address</td></tr>
<tr>
<td><select name="consFromTime" id="consFromTime" required  >
<option value="">From Time</option>
<%for(int f=01;f<13;f++){ %>
<option value="<%=f %>"><%=f %></option><%} %>
</select><select name="con1" >
<option value="AM">AM</option>
<option value="PM">PM</option>
</select>
TO
<select name="consToTime" id="consToTime" required >
<option value="">To Time</option>
<%for(int f=01;f<13;f++){ %>
<option value="<%=f %>"><%=f %></option><%} %>
</select>
</select>
<select name="con2" >
<option value="AM">AM</option>
<option value="PM">PM</option>
</select></td>
<td colspan="2"><div class="warning" id="AddressError" style="display: none;">Please Provide  "Address".</div>
<textarea name="address" id="address" placeholder="Enter doctor Address*" required></textarea></td>
</tr>
<tr> 
<td></td>
<td align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<%} %>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<!-- <div class="arrow-down"><img src="images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>-->
<div class="icons">
<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<!-- <span>
<ul>
<li><img src="images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span> -->
</div>
<div class="clear"></div>
<div class="list-details" id="tblExport">
<%
List DOCT_List=DOCT.getdoctordetails();
if(DOCT_List.size()>0){%>
      <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
  <td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</td>
</tr>
<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<tr><td colspan="7" align="center" style="font-weight: bold;">Doctor's List</td></tr>
<tr><td colspan="7"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>

<td class="bg" width="5%" align="center">S.No.</td>
<td class="bg" width="20%">Doctor Name</td>
<td class="bg" width="20%">Doctor Qulification</td>
<td class="bg" width="15%" align="center">Phone</td>
<td class="bg" width="20%">Address</td>
<td class="bg" width="10%" align="center">Edit</td>
<td class="bg" width="10%" align="center">Delete</td>
</tr>
</table></td></tr>
<%for(int i=0; i < DOCT_List.size(); i++ ){
	DOC=(doctordetails)DOCT_List.get(i); %>
<tr>

<td width="5%" align="center"><%=i+1%></td>
<td width="20%"><span><%=DOC.getdoctorName()%></span></td>
<td width="20%"><%=DOC.getdoctorQualification()%></td>
<td width="15%" align="center"><%=DOC.getphoneNo()%></td>
<td width="20%"><%=DOC.getaddress()%></td>

<td width="10%" align="center"><a href="doctordetails_Form.jsp?id=<%=DOC.getdoctorId() %>">Edit</a></td>
<td width="10%" align="center"><a href="doctordetails_Delete.jsp?Id=<%=DOC.getdoctorId()%>">Delete</a></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>No Tablets Added Yet</h1></div>
<%}%>


</div>
</div>




</div>

</div>
<!-- main content -->

<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>