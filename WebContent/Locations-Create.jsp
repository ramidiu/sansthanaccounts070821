<%@page import="beans.locations"%>
<%@page import="mainClasses.locationsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.poojasaamanListing"%>
<%@page import="mainClasses.headsgroupListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>

<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<head>
<title>LOCATIONS CREATE</title>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="js/jquery.blockUI.js"></script>
<script>
function formBlock(){
	$.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } }); 
}
</script>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<%
if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="LOC" class="beans.locations"/>
<div class="vendor-page">
<div class="vendor-box">
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
subheadListing SUBL=new subheadListing();
productsListing PROL=new productsListing();
%>
<form name="tablets_Update" method="post" action="locations_Insert.jsp" onsubmit="formBlock();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
						<td colspan="3" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST POOJA STORES</td>
					</tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Locations Create</td></tr>
<tr>
<td>Head of account</td>
<td>LOCATION Name*</td>
<td>NARRATION*</td>
</tr>
<tr>
<td><select name="HOA" id="HOA">
<%if(session.getAttribute("headAccountId").toString().equals("5")){ %>
<option value="5" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("5")){ %> selected="selected" <%} %>>SHRI SAI NIVAS</option>
<%}else{ %>
<option value="3" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("3")){ %> selected="selected" <%} %>>POOJA STORES</option>
<option value="4" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("4")){ %> selected="selected" <%} %>>SANSTHAN</option>
<option value="1" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("1")){ %> selected="selected" <%} %>>CHARITY</option>
<%} %>
</select></td>
<td><input type="text" name="locationname" id="locationname" placeholder="Enter location name" required="required" onkeyup="javascript:this.value=this.value.toUpperCase();">
</td>
<td><input type="text" name="narration" id="narration" placeholder="Write your narration" onkeyup="javascript:this.value=this.value.toUpperCase();">
</td>
<td align="right"><input type="submit" value="Create" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list">
<div class="arrow-down"><img src="images/Arrow-down.png"/><b>LOCATIONS LIST</b></div>

<div class="clear"></div>
<div class="list-details">
<%
locationsListing LOCL=new locationsListing();
List GROPList=LOCL.getMlocationsBasedOnHead(session.getAttribute("headAccountId").toString());
if(GROPList.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">HEAD ACCOUNT</td>
<td class="bg" width="23%">LOCATION-ID </td>
<td class="bg" width="23%">LOCATION-NAME </td>
<td class="bg" width="23%">NARRATION </td>

<!-- <td class="bg" width="20%">Edit</td>
<td class="bg" width="10%">Delete</td> -->
</tr>
<%for(int i=0; i < GROPList.size(); i++ ){
	LOC=(locations)GROPList.get(i); %>
<tr>
<td><%=i+1 %></td>
<td><%=HOA_L.getHeadofAccountName(LOC.gethead_account_id()) %></td>
<td><%=LOC.getlocation_id() %></td>
<td><span><%=LOC.getlocation_name() %></span></td>
<td><%=LOC.getnarration()%></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no locations found</h1></div>
<%}%>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>