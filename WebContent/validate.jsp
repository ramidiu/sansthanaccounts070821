<%@page import="mainClasses.employepermissionsListing"%>
<jsp:useBean id="EMP" class="beans.employees"/>
<jsp:useBean id="PER" class="beans.employepermissions"/>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.util.List" pageEncoding="ISO-8859-1"%>
<%
String username=request.getParameter("username");
String password=request.getParameter("password");
mainClasses.employeesListing EMP_CL = new mainClasses.employeesListing();
employepermissionsListing PERL=new employepermissionsListing();
List PER_Det=null;
List EMP_List=EMP_CL.getEmployee(username, password);
if(EMP_List.size()>0){ 
	EMP=(beans.employees)EMP_List.get(0);
	session.setAttribute("empId", EMP.getemp_id());
	session.setAttribute("empName", EMP.getfirstname());
	session.setAttribute("headAccountId", EMP.getheadAccountId());
	session.setAttribute("Role",EMP.getextra1());
	PER_Det=PERL.getEmployeDetratmentpermissions(EMP.getemp_id(), EMP.getheadAccountId());
	if(PER_Det.size()>0){
		PER=(beans.employepermissions)PER_Det.get(0);
	}
	session.removeAttribute("error");
	if(PER.getemp_permissions().contains("counter")){
		
		response.sendRedirect("Counter-Open-New.jsp");
	}else{
		response.sendRedirect("home.jsp");
	}
}else{
	session.setAttribute("error", "Invalid UserName or Password.<br/> Please Try Again.");
	response.sendRedirect("index.jsp");
}

%>