<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="crontab.BankClosingBalance"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<html>
<head>
    <title>Print Part of a Page With jQuery</title>
</head>
<body>
<%-- <%
BankClosingBalance BNKCLS=new BankClosingBalance();
%>
<%=BNKCLS.process() %> --%>
<%Calendar cal = Calendar.getInstance();
SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
out.println( sdf.format(cal.getTime()) ); %>
</body>
</html>