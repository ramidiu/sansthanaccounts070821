<%@page import="mainClasses.productsListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.medicalpurchases"%>

<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/resources/demos/style.css"/>
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="js/jquery-1.4.2.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"/>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<jsp:useBean id="MDCL" class="beans.medicalpurchases" />
<script>
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;
	u=document.getElementById("countSj").value;
	if(document.getElementById("product"+i).value!=""){

		if(document.getElementById("purchaseRate"+i).value!="" && document.getElementById("vat"+i).value!="" && document.getElementById("quantity"+i).value!="")
		{
		
			for(var j=0;j<u;j++){
			
				if(document.getElementById("purchaseRate"+j).value!=""){
				grossprice=Number((parseFloat(document.getElementById("purchaseRate"+j).value)*parseFloat(document.getElementById("quantity"+j).value))+((parseFloat(document.getElementById("purchaseRate"+j).value)*parseFloat(document.getElementById("quantity"+j).value)))*parseFloat(document.getElementById("vat"+j).value)/100).toFixed(2);
			document.getElementById("grossAmt"+j).value=grossprice;
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice)).toFixed(2);
	
				}
				}
			document.getElementById("total").value=Totalprice;
		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;

}

</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%

double total=0.0;
if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="GODW" class="beans.godwanstock"/>
<div class="vendor-page">
<form name="tablets_Update" method="post" action="billdetails_update.jsp" onsubmit="return validate();">
<div class="vendor-box">
<div class="vendor-title">Purchase Receipt</div>
<div class="vender-details">
<%mainClasses.vendorsListing VEND = new mainClasses.vendorsListing();
godwanstockListing GODS_l=new godwanstockListing();
productsListing PRD_L=new productsListing();
List GODS_DET=GODS_l.getStockDetailsBasedOnInvoice(request.getParameter("id"));
if(GODS_DET.size()>0){
	GODW=(godwanstock)GODS_DET.get(0);
}
%>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%">
<input type="hidden" name="countSj" id="countSj"  value="<%=GODS_DET.size()%>"/>
<input type="hidden" name="inv_id" id="inv_id"  value="<%=request.getParameter("id").toString()%>"/>  
Vendor Name : </td>
<td ><%=VEND.getMvendorsAgenciesName(GODW.getvendorId())%></td>
</tr>
<tr>
<td>Bill Number : </td>
<td><%=GODW.getbillNo()%></td>
</tr>
<tr>
<td>Description : </td>
<td><div><%=GODW.getdescription()%></div></td>
</tr>
<tr>
<td>Invoice No : </td>
<td><%=GODW.getextra1()%></td>
</tr>

</table>

</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="clear"></div>
<div class="list-details">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>

<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Product Name</td>

<td class="bg" width="23%">Quantity</td>
<td class="bg" width="23%">Price Per Unit</td>
<td class="bg" width="23%">Vat</td>
<td class="bg" width="23%">Price</td>
</tr>
<%String amount="";
int check=0;
for(int i=0; i <  GODS_DET.size(); i++ ){
	GODW=(godwanstock)GODS_DET.get(i);
	if(GODW.getprofcharges().equals("checked")){
		check=check+1;
	}
%>
<tr>

<td><%=i+1%></td>
<%
		String prodName = "";
		prodName = PRD_L.getProductsNameByCat(GODW.getproductId(),GODW.getdepartment());
		if(prodName.equals(""))
		{
			prodName = PRD_L.getProductNameByCat(GODW.getproductId(),GODW.getdepartment());
		}
%>
<td><span><input type="text"  value="<%=GODW.getproductId()%> <%-- <%=PRD_L.getProductsNameByCat(GODW.getproductId(),2)%> --%><%=prodName %>" disabled="disabled" /> </span><input type="hidden" name="product<%=i%>" id="product<%=i%>"  value="<%=GODW.getgsId() %>" /></td>

<td><input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  value="<%=GODW.getquantity()%>" <%if(GODW.getprofcharges().equals("checked")){ %> readonly="readonly" <%} %> onkeyup="calculate('<%=i %>')" /></td>
<td><input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" value="<%=GODW.getpurchaseRate()%>" readonly="readonly"/></td>
<td><input type="text" name="vat<%=i%>" id="vat<%=i%>" value="<%=GODW.getvat()%>" readonly="readonly" /></td>
<td><input type="text" name="grossAmt<%=i%>" id="grossAmt<%=i%>" value="<%=Math.round((Double.parseDouble(GODW.getquantity()))*(Double.parseDouble(GODW.getpurchaseRate())*(1+Double.parseDouble(GODW.getvat())/100)))%>" disabled="disabled" /></td>

</tr>
<%
amount=""+(Double.parseDouble(GODW.getquantity()))*(Double.parseDouble(GODW.getpurchaseRate())*(1+Double.parseDouble(GODW.getvat())/100));
total=total+(Double.parseDouble(amount));
} %>
<tr>
<td colspan="4" class="bg" >Total Bill Amount</td><td class="bg"><input type="text" name="total" id="total" value="<%=total %>" disabled="disabled" style="width:50px;"/></td>
</tr>
</table>
</div>
</div>
<%if(check!=GODS_DET.size()){ %>
<input type="submit" value="Submit" class="click" style="border:none; float:right; margin:0 115px 0 0"/>
<%} %>

</form>
</div>

</div>
<!-- main content -->

<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>