<%@page import="mainClasses.employeesListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">

function validate(){
	$('#oldError').hide();
	$('#newError').hide();
	$('#confirmError').hide();
	$('#oldError1').hide();
	$('#confirmError1').hide();
	if($('#oldpassword').val().trim()==""){
		$('#oldError').show();
		$('#oldpassword').focus();
		return false;
	}
	if($('#oldpassword').val().trim()!=$('#password').val().trim()){
		$('#oldError1').show();
		$('#oldpassword').focus();
		return false;
	}
	
	if($('#newpassword').val().trim()==""){
		$('#newError').show();
		$('#newpassword').focus();
		return false;
	}
	
	if($('#confirmpassword').val().trim()==""){
		$('#confirmError').show();
		$('#confirmpassword').focus();
		return false;
	}
	
	if($('#newpassword').val().trim()!=$('#confirmpassword').val().trim()){
		$('#confirmError1').show();
		$('#confirmpassword').focus();
		return false;
	}
		
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="VEN" class="beans.vendors"/>	
<jsp:useBean id="TBL" class="beans.tablets"/>
<div class="vendor-page">

<div class="vendor-box">
<form method="post" action="changepasswordupdate.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Change Password</span><%if((request.getParameter("status")!=null )&& (request.getParameter("status").equals("sucess")) ){%><div class="warning">(New Password Changed Successfully)</div><%} %></td>
</tr>
<%employeesListing EMPL=new mainClasses.employeesListing();%>
<input type="hidden" name="password" id="password" value="<%=EMPL.getMEmployeePassword(session.getAttribute("empId").toString())%>"/>
<tr>

<td width="2%"></td>
<td valign="top">
<table width="50%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3">
<div class="warning" id="oldError" style="display: none;">Please Provide  "Old Password".</div>
<div class="warning" id="oldError1" style="display: none;">Please Provide Correct "Old Password".</div>
Old Password*</td>
</tr>
<tr>
<td colspan="3"><input type="password" name="oldpassword" id="oldpassword" style="width:98%;"></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="newError" style="display: none;">"New Password" Required.</div>New Password*</td>
</tr>
<tr>
<td colspan="3"><input type="password" name="newpassword" id="newpassword" onKeyPress="return numbersonly(this, event,true);" style="width:98%;"/></td>
</tr>
<tr>
<td colspan="3"><div class="warning" id="confirmError" style="display: none;">"Confirm Password" Required.</div>
<div class="warning" id="confirmError1" style="display: none;">"Confirm Password" is not matching with "New Password".</div>
Confirm Password*</td>
</tr>
<tr>
<td colspan="3"><input type="password" name="confirmpassword" id="confirmpassword" style="width:98%;"/></td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

<tr>
<td ></td>
<td ><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>

<div style="clear:both;"></div>
</div>
</div>

</div>
<!-- main content -->

<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>