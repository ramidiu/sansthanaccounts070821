
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>

<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type='text/javascript'>

$("#doctorName").keyup(function() {
alert("ok");
});
/* $().ready(function() 
{
$("#doctorName").keypress().autocomplete("searchDoctor.jsp",{
		type : "POST",
		//extraParams: {lfor : function() { return $("input:text[@name='doctorId']").val(); EndLocations} },
        delay:200,
        selectOnly:true,
        mustMatch:1,
		max: 2,
		highlight: false,
		multiple: true,
		multipleSeparator: "\n",
		scroll: true,
		//extraTextField: "doctorId", 
		scrollHeight: 200
	});

}); */

</script>


<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#DoctorError').hide();
	$('#QualificationError').hide();
	$('#PhoneError').hide();
	$('#AddressError').hide();

	if($('#doctorName').val().trim()==""){
		$('#DoctorError').show();
		$('#doctorName').focus();
		return false;
	}
	if($('#patientId').val().trim()==""){
		$('#patientError').show();
		$('#patientId').focus();
		return false;
	}
	if($('#phoneNo').val().trim()==""){
		$('#PhoneError').show();
		$('#phoneNo').focus();
		return false;
	}
	if($('#address').val().trim()==""){
		$('#AddressError').show();
		$('#address').focus();
		return false;
	}
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="DOC" class="beans.doctordetails"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Patient Appointment</div>
<div class="vender-details">

<form name="tablets_Update" method="post" action="doctordetails_Insert.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%">
<div class="warning" id="DoctorError" style="display: none;">Please Provide  "Doctor Name".</div>
Doctor Name*</td>
<td>
<div class="warning" id="patientError" style="display: none;">Please Provide  "Patient Name".</div>
Patient Name*</td>
</tr>
<tr>
<td><input type="text" name="doctorName" id="doctorName"  />
<input type="text" name="doctorId" id="doctorId" /></td>
<td><input type="text" name="patientId" id="patientId" value="" /></td>
</tr>
<tr>
<td>
<div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone No".</div>
Phone No*</td>
<td>Gender</td>
</tr>
<tr>
<td><input type="text" name="phoneNo" id="phoneNo" onKeyPress="return numbersonly(this, event,true);" value="" /></td>
<td><input type="radio" name="extra1" value="M" checked="checked" />Male<input type="radio" name="extra1" value="F" />Female</td>
</tr>

<tr>



<td colspan="2"><div class="warning" id="AddressError" style="display: none;">Please Provide  "Address".</div>
<textarea name="address" id="address" placeholder="Enter Your Address*"></textarea></td>
</tr>
<tr> 
<td></td>
<td align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>

</tr>
</table>
</form>

</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%doctordetailsListing DOCT = new mainClasses.doctordetailsListing();
List DOCT_List=DOCT.getdoctordetails();
if(DOCT_List.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>

<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Doctor Name</td>
<td class="bg" width="23%">Doctor Qulification</td>
<td class="bg" width="23%">Phone</td>
<td class="bg" width="23%">Address</td>
<td class="bg" width="20%">Edit</td>
<td class="bg" width="10%">Delete</td>
</tr>
<%for(int i=0; i < DOCT_List.size(); i++ ){
	DOC=(doctordetails)DOCT_List.get(i); %>
<tr>

<td><%=i+1%></td>
<td><span><%=DOC.getdoctorName()%></span></td>
<td><%=DOC.getdoctorQualification()%></td>
<td><%=DOC.getphoneNo()%></td>
<td><%=DOC.getaddress()%></td>

<td><a href="#" onclick="openMyModal('tablets_Edit.jsp?ID=<%=DOC.getdoctorId() %>');"  >Edit</a></td>
<td><a href="doctordetails_Delete.jsp?Id=<%=DOC.getdoctorId()%>">Delete</a></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>No Tablets Added Yet</h1></div>
<%}%>


</div>
</div>




</div>

</div>
<!-- main content -->

<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>