<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="beans.subhead" %>
<%@page import="mainClasses.subheadListing" %>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<style>
#approvedBy{
	    position: relative;
    left: -112px;
}
</style>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
    $("#appointmnetId").focus();	
});
	</script>
	<script type="text/javascript">
	   $(document).ready(function(){
		   $('#approvedByLabel').hide();
		   $('#approvedBy').hide();
	   });
	</script>
<script type="text/javascript"  src="js/modal-window.js"></script>	
<script type="text/javascript" >
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	function addmore(){
		var j=0;
		var i=0;
		j=document.getElementById("addcount").value;
		i=j;
		j=Number(Number(j)+5);
		for(i;i<j;i++){
		document.getElementById("addcolumn"+i).style.display="block";
		}
		document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
	}
</script>

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#NameError').hide();
	if($('#appointmnetId').val().trim()==""){
		$('#NameError').show();
		$('#appointmnetId').focus();
		return false;
	}

	if(($('#price0').val().trim()=="" || $('#testname0').val().trim()=="") || ($('#quantity0').val().trim()=="")){
		$('#testname0').addClass('borderred');
		$('#testname0').focus();
		return false;
	}
}
function checkname(i){

if(document.getElementById("testname"+i).value==""){
	document.getElementById('testname'+i).className = 'borderred';
	document.getElementById('testname'+i).placeholder  = 'Test Name';
	document.getElementById("testname"+i).focus();
}
document.getElementById("check"+i).checked = true;
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script type="text/javascript">
$(document).ready(function(){
  $("#appointmnetId").keyup(function(){
	  var data1=$("#appointmnetId").val();
	  $.post('Searchappointment.jsp',{q:data1},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#appointmnetId" ).autocomplete({source: availableTags}); 
			});
  });
});
function combochange(denom,desti,jsppage) { 
    var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value; 
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	var amt=document.getElementById("Amount");
	$.post(jsppage,{ id: com_id } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	var x=document.createElement('option');
	x.text='Select test name';
	x.value='';
	sda1.add(x);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");
				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
					sda1.add(y,null);
				}
				catch(e)
				{
					sda1.add(y);
				}
	}
	}
	}); 
}
function productsearch(j){
	  var data1=$('#testname'+j).val();
	  var hoid=$('#headAccountId').val();
	  var arr = [];
	  $.post('searchSubhead.jsp',{q:data1,hoa : hoid},function(data)
	{
		  
		var response = data.trim().split("\n");
		var doctorNames=new Array();
		 var doctorIds=new Array();
		 var amount=new Array();
		 var percent=new Array();
		 
		 var amt=document.getElementById("Amount" +j).value;
		 var per=document.getElementById("percentage"+j).value;
		 
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 //alert(response);
			 /* doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]); */
			 arr.push({
				 //label: d[0]+" "+d[1]+" "+d[2],
				 label: d[0]+" "+d[1],
			        amount:d[2],
			         percent:d[4],
			        sortable: true,
			        resizeable: true
			    });
			 
		 }
		
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#testname' +j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				var paidFlag=$('#paidFlag').val();
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				var amt=ui.item.amount;
				$('#actualAmount' + j).val(ui.item.amount);
				if(paidFlag=='true')
				{
					var finalPrice=Math.round(ui.item.amount);
					roundOfAndSetAmount(Math.round(finalPrice),'Amount'+j);
					$('#bufferedAmount' + j).val('0');
				}
				else if(paidFlag=='false')
				{
					$('#Amount' + j).val('0');
					roundOfAndSetAmount(0,'bufferedAmount'+j);
					$('#bufferedAmount' + j).val((amt)-(amt*ui.item.percent/100));
				}
				$('#percentage'+j).val(ui.item.percent); 
				// $("#testname" + j).attr('readonly', true); 
				 
				 
			}
	
			});
	});
	  var addcount=$('#addcount').val();
	  totalAmt(addcount);  
	 
}

function totalAmt(j)
{	
	 var totalamt = Number("0");
	for (k = 0; k <= j; k++) 
	{ 
		var amt=document.getElementById("Amount" + k).value;
		totalamt += parseFloat(amt);
		document.getElementById("totalamt").value=Math.round(totalamt);
	}
	
}
function roundOfAndSetAmount(calculatedAmount,valueSetToId){
			//document.getElementById("totalamt").value=Number(calculatedAmount);
			var totalAmount=$('#totalamt').val();
			var totalCalculatedAmount=Number(totalAmount)+Number(calculatedAmount);
			$('#totalamt').val(Number(totalCalculatedAmount));
			
	/* calculatedAmount=calculatedAmount+10;//client told to add 10 rs for each and round of with 10
	var reminder=calculatedAmount%10;
	if(reminder!=0){
		calculatedAmount=calculatedAmount+(10-reminder);
	} */
	$('#'+valueSetToId).val(calculatedAmount);
}
function getTestPrice(j){
	var subhId=document.getElementById("testname").value;
	var amt=document.getElementById("Amount" + j);
	
   
	var per=document.getElementById("percentage"+j);
	
	/* var qty=document.getElementById("ticQty").value; */
	/* var totAmt=document.getElementById("totAmt").value; */
	$.ajax({
		type:'post',
		url: 'getTestPrice.jsp', 
		data: {
		q : subhId,
		},
		success: function(response) { 
			alert(response);
			var arr=response.trim().split(":");
			//amt=arr[0];
			
			document.getElementById("Amount").value=arr[0];
			document.getElementById("percentage").value=arr[1];
			
		}
		
			
		});
}
function changePaidFlag(flag)
{
	var addcount=$('#addcount').val();
	if(flag=='paidTest')
	{
		$('#paidFlag').val('true');
		for(var i=0;i<addcount;i++)
		{
			$('#approvedBy').hide();
			$('#approvedByLabel').hide();
			var amt=$('#bufferedAmount'+i).val();
			var amount=Number(amt);
			$('#Amount'+i).val(Number(amount));
			$('#bufferedAmount'+i).val('0');
		}
		 totalAmt(addcount);  
	}
	else if(flag=='freeTest')
	{
		$('#paidFlag').val('false');
		for(var i=0;i<addcount;i++)
		{
			var amt=$('#Amount'+i).val();
			var amount=Number(amt);
			$('#Amount'+i).val('0');
			$('#approvedBy').show();
			$('#approvedByLabel').show();
			$('#bufferedAmount'+i).val(amt);
		}
		 totalAmt(addcount);  
	}
	
}
/* function calculate(i){
	var avablqty=0;
	var iqty=0;
	avablqty=document.getElementById("availablequantity"+i).value;
	iqty=document.getElementById("quantity"+i).value;
	if(Number(iqty)>Number(avablqty)){
	document.getElementById("quantity"+i).value=Number(avablqty).toFixed(0);
	}
} */
</script>
<script type="text/javascript">
function printrecipt(appId,billingid,paidFree){
	if(appId!=null && appId!='null')
	{
		newwindow=window.open('diagnosticReceipt.jsp?id='+appId+'&billingId='+billingid+'&paidFree='+paidFree,'name','height=600,width=500,menubar=yes,status=yes,scrollbars=yes');
		if (window.focus) {newwindow.focus();}
	}
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
if(session.getAttribute("empId")!=null){ %>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());%>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="DOC" class="beans.doctordetails"/>
<div class="vendor-page">
<form name="tablets_Update" method="post" action="diagnosticissues_Insert.jsp" onsubmit="return validate();">
<div class="vendor-box">
<div class="vendor-title">DIAGNOSTICS TEST ISSUSES FOR PATIENT </div>
<div class="vender-details">
<table width="50%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>Date</td>
<td width="35%">
<div class="warning" id="NameError" style="display: none;">Please Provide  "Appointment Id".</div>
Appointment Id*</td>

</tr>
<tr>
<td><input type="text" name="date" id="date"  value="<%=currentDate%>" class="DatePicker" readonly="readonly"/></td>
<td><input type="text" name="appointmnetId" id="appointmnetId" value="" autocomplete="off"/></td>

</tr>
<tr>
	<td>
	<input type="radio"  name="paidFree" id="paidTest"  value="paid" onclick="changePaidFlag('paidTest')" checked/> paid
	<input type="radio"  name="paidFree" id="freeTest" value="free" onclick="changePaidFlag('freeTest')"  /> free
	<input type="hidden" id="paidFlag" value="true"/>
	</td>
	<td id="approvedByLabel">Approved By</td>
	<td><input type="text" name="approvedBy" id="approvedBy"/></td>
	<td></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="clear"></div>
<div class="list-details">
	<div class="bg" style="float:left ; width:100px; margin:0 0 10px 0">S.NO.</div>
	<div class="bg" style="float:left;width:250px; margin:0 0 10px 0">TEST NAME</div>
	<div class="bg" style="float:left; width:250px; margin:0 0 10px 0">AMOUNT</div>
	<div class="clear"></div>
	<input type="hidden" name="headAccountId" id="headAccountId" value="1"/>
	<input type="hidden" name="addcount" id="addcount" value="5"/>
	<%for(int i=0; i < 150; i++ ){%>
		<div  <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>" >
			<input type="hidden" name="count1" id="check<%=i%>" value="<%=i%>"/>
			<div style="float:left ; width:100px; margin:0 0 10px 0"><%=i+1%><input type="checkbox" name="count" id="check<%=i%>"  value="<%=i%>" style="display:none;" /> </div>
			<div style="float:left ; width:250px; margin:0 0 10px 0"><span><input type="text" name="testname<%=i%>" id="testname<%=i%>"  onkeyup="productsearch('<%=i %>')" autocomplete='off' onblur="getTestPrice('<%=i %>');"/></span>
			
			<input type="hidden" name="availablequantity<%=i%>" id="availablequantity<%=i%>" value="0" /></div>
			<div style="float:left ; width:200px; margin:0 0 10px 0">
				<input type="text" name="Amount<%=i%>" id="Amount<%=i%>" onKeyPress="return numbersonly(this, event,true);" value="0"  autocomplete="off"/>
				<input type="hidden" name="actualAmount<%=i%>" id="actualAmount<%=i%>"/>
				<input type="hidden" name="bufferedAmount<%=i%>" id="bufferedAmount<%=i%>"/>
				<input type="hidden" name="percentage<%=i%>" id="percentage<%=i%>" value="0" onKeyPress="return numbersonly(this, event,true);" autocomplete="off"/>
			</div>
			<%-- <div><a href="diagnosticIssue_Form.jsp"><img src="images/button-cancel.png" width="20px"></img></a></div> --%>
			<div class="clear"></div>
		</div>
	<%} %>
	
	
	 <div>
	<!-- <ul>
		<li style=""><div style="float:center; width:350px; margin:0 0 10px 0">TOTAL</div></li>
		<li><div style="float:left ; width:200px; margin:0 0 10px 0"><input type="text" name="totalamt" id="totalamt" value="0" readonly /></div></li>
	</ul> -->
	<ul style="padding-left:200px;">
		<li style="float:left;list-style:none;padding:10px 20px 0px 20px;">TOTAL<span style="padding-left:20px;font-weight:bold;">:</span></li>
		<li style="list-style:none;padding:0px 20px 0px 150px;"><input type="text" name="totalamt" id="totalamt"  readonly /></li>
	</ul>
	</div> 
	
	<div style="float:left; margin:0 20px 0 0"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/></div>
	<div style="float:left;"><input type="submit" value="ISSUE" class="click" /></div>
</div>

</div>
</form>
</div>

</div>
<!-- main content -->
<%
String appId=request.getParameter("appointmnetId");
String billingid=request.getParameter("billingId");

String paidFree=request.getParameter("paidFree");
%>
<script>
	window.onload=printrecipt('<%=appId%>','<%=billingid%>','<%=paidFree%>');
</script>


<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>

</body>
</html>