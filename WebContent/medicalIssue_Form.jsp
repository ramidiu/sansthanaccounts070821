<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>

<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
    $("#appointmnetId").focus();	
});
	</script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	function addmore(){
		var j=0;
		var i=0;
		j=document.getElementById("addcount").value;
		i=j;
		j=Number(Number(j)+5);
		for(i;i<j;i++){
		document.getElementById("addcolumn"+i).style.display="block";
		}
		document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
	}
</script>

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#NameError').hide();
	if($('#appointmnetId').val().trim()==""){
		$('#NameError').show();
		$('#appointmnetId').focus();
		return false;
	}

	if(($('#price0').val().trim()=="" || $('#tabletname0').val().trim()=="") || ($('#quantity0').val().trim()=="")){
		$('#tabletname0').addClass('borderred');
		$('#tabletname0').focus();
		return false;
	}
}
function checkname(i){

if(document.getElementById("tabletname"+i).value==""){
	document.getElementById('tabletname'+i).className = 'borderred';
	document.getElementById('tabletname'+i).placeholder  = 'Tablet Name';
	document.getElementById("tabletname"+i).focus();
}
document.getElementById("check"+i).checked = true;
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(document).ready(function(){
  $("#appointmnetId").keyup(function(){
	  var data1=$("#appointmnetId").val();
	  $.post('Searchappointment.jsp',{q:data1},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#appointmnetId" ).autocomplete({source: availableTags}); 
			});
  });
});
function productsearch(j){
	  var data1=$('#tabletname'+j).val();
	  var hoid=$('#headAccountId').val();
	  var arr = [];
	  $.post('searchProducts.jsp',{q:data1,hoa : hoid,page:"medical"},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 var qty=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 qty.push(d[6]);
			 doctorNames.push(d[0] +" "+ d[1]+"  "+d[6]);
			 arr.push({
				 label: d[0]+" "+d[1]+"  "+d[6],
			        amount:d[4],
			        vat:d[5],
			        qty:d[6],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#tabletname'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
		select: function(event, ui) {
			event.preventDefault();
			$(this).val(ui.item.label);
			if(Number(ui.item.qty)>0){
			$('#availablequantity'+j).val(ui.item.qty);
			} else{
			$('#tabletname'+j).val("");
			$('#availablequantity'+j).val("");
			}
		}
		}); 
			});
} 
function calculate(i){
	var avablqty=0;
	var iqty=0;
	avablqty=document.getElementById("availablequantity"+i).value;
	iqty=document.getElementById("quantity"+i).value;
	if(Number(iqty)>Number(avablqty)){
	document.getElementById("quantity"+i).value=Number(avablqty).toFixed(0);
	}
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
if(session.getAttribute("empId")!=null){ %>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());%>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="DOC" class="beans.doctordetails"/>
<div class="vendor-page">
<form name="tablets_Update" method="post" action="medicineissues_Insert.jsp" onsubmit="return validate();">
<div class="vendor-box">
<div class="vendor-title">MEDICINES ISSUSES FOR PATIENT</div>
<div class="vender-details">
<table width="50%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>Date</td>
<td width="35%">
<div class="warning" id="NameError" style="display: none;">Please Provide  "Appointment Id".</div>
Appointment Id*</td>
</tr>
<tr>
<td><input type="text" name="date" id="date"  value="<%=currentDate%>" class="DatePicker" readonly="readonly"/></td>
<td><input type="text" name="appointmnetId" id="appointmnetId" value="" autocomplete="off"/></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="clear"></div>
<div class="list-details">


<div class="bg" style="float:left ; width:100px; margin:0 0 10px 0">S.NO.</div>
<div class="bg" style="float:left;width:250px; margin:0 0 10px 0">MEDICINE NAME</div>
<div class="bg" style="float:left; width:250px; margin:0 0 10px 0">ISSUE QUANTITY</div>
<div class="clear"></div>
<input type="hidden" name="headAccountId" id="headAccountId" value="1"/>


<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 150; i++ ){%>

<div  <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>" >

<div style="float:left ; width:100px; margin:0 0 10px 0"><%=i+1%><input type="checkbox" name="count" id="check<%=i%>"  value="<%=i %>" style="display:none;" /> </div>

<div style="float:left ; width:250px; margin:0 0 10px 0"><span><input type="text" name="tabletname<%=i%>" id="tabletname<%=i%>"  onkeyup="productsearch('<%=i %>')" class="" autocomplete='off'/></span>
<input type="hidden" name="availablequantity<%=i%>" id="availablequantity<%=i%>" value="0" /></div>

<div style="float:left ; width:250px; margin:0 0 10px 0"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>" value="1" onKeyPress="return numbersonly(this, event,true);" onkeyup="calculate('<%=i %>');" onblur="checkname('<%=i %>');" /></div>
<div class="clear"></div>
</div>
<%} %>
 
<div style="float:left; margin:0 20px 0 0"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/></div>
<div style="float:left;"><input type="submit" value="ISSUE" class="click" /></div>



</div>
</div>



</form>
</div>

</div>
<!-- main content -->

<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>