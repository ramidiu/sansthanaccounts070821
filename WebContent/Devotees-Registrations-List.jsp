<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" /> 
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:92%; 
    background:#d0e3fb; 
}

@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}

	}


</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
</head>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-list">
<div class="icons">
	<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="clear"></div>
<div class="list-details" id="tblExport">
	 <table width="100%" cellpadding="0" cellspacing="0">
			<tr><td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;">Devotee's unique Registrations List</td></tr>
         
		
		
			<tr>
			<jsp:useBean id="REG"  class="beans.sssst_registrations"></jsp:useBean>
			<jsp:useBean id="UI" class="beans.UniqueIdGroup"/>
			<td colspan="8">
				<table width="100%" cellpadding="0" cellspacing="0"  class="print_table">
					 
                     	<tr class="print_table">
				<td class="bg" width="3%" style="font-weight: bold;">S.NO.</td>
				<td class="bg" width="7%" style="font-weight: bold;">UNIQUE ID</td>
				<td class="bg" width="10%" style="font-weight: bold;">NAME</td>
				<td class="bg" width="10%" style="font-weight: bold;">PHONE NO</td>
				<td class="bg" width="15%" style="font-weight: bold;">EMAIL-ID</td>
				<td class="bg" width="25%" style="font-weight: bold;">ADDRESS</td>
				<td class="bg" width="13%" style="font-weight: bold;">PANCARD</td>
				<td class="bg" width="17%" style="font-weight: bold;">EDIT</td>
			</tr>
                     
					 <%-- <%sssst_registrationsListing SSRL=new sssst_registrationsListing();
					 List RLDET=SSRL.getsssst_registrations();
					 if(RLDET.size()>0){
						 for(int i=0;i<RLDET.size();i++){
							 REG=(sssst_registrations)RLDET.get(i);
					 %> --%>
					 <%
			mainClasses.uniqueIdGroupsListing UIGL = new mainClasses.uniqueIdGroupsListing();
					 sssst_registrationsListing SSRL=new sssst_registrationsListing();	 
					 List UIG_List=UIGL.getUniqueIdGroupsOfDevotees();
					 
					 for(int i=0; i < UIG_List.size(); i++ )
					 {
						UI=(beans.UniqueIdGroup)UIG_List.get(i);
						List SSSST_List=SSRL.getMsssst_registrations(UI.getUniqueid().toString());
						for(int G=0; G < SSSST_List.size(); G++ ){
						REG=(beans.sssst_registrations)SSSST_List.get(G);%>
					 
					 <tr style="position: relative;">
					 	<td width="3%"><%=i+1 %></td>
					 	<td width="7%"><%=REG.getsssst_id() %></td>
					 	<td width="15%"><%=REG.getfirst_name() %> <%=REG.getlast_name() %></td>
					 	<td width="12%"><%=REG.getmobile() %></td>
					 	<td width="14%"><%=REG.getemail_id() %></td>
					 	<td width="19%"><%=REG.getaddress_1() %> ,<%=REG.getaddress_2() %>,<%=REG.getcity() %></td>
					 	<td width="10%"><%=REG.getpancard_no() %></td>
					 	<td width="20%"><a href="Devotees-Registrations_Edit.jsp?bid=<%=REG.getsssst_id()%>">Edit</a></td>
					 	
					 </tr>	
					 <%}} %>
				</table>
			</td>
			</tr>
	</table>
	 </div>
</div>
</div>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
<%}else{ response.sendRedirect("index.jsp");%>

<%} %>
</body>
</html>