<%@page import="model.sendmailservice"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat,java.util.*"%>
<%@page import="sms.CallSmscApi"%>
<%@page import="beans.UniqueIdGroup" %>
<%@page import="beans.UniqueIdGroupService" %>
<%@page import="helperClasses.DevoteeGroups"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="CSP" class="beans.customerpurchasesService"/>
<jsp:useBean id="CAP" class="beans.customerapplicationService" />
<jsp:useBean id="DREG" class="beans.sssst_registrationsService"></jsp:useBean>

<%
DateFormat mySqlDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

String sssstId = request.getParameter("uniqueid");
String billId = request.getParameter("receiptNo");

String[] pSplit = new String[2];
String prodId = "";
String mobile = "";
String customerName = "";
String gothram = "";
String address = "";
String emailid = "";
String purpose = "";
String fromdate = "";
String todate = "";
String pancardNo = "";
String paymentType = "";
String fourDigitNum = "";
String authenticationNum = "";

if(request.getParameter("productId")!=null){
	pSplit=request.getParameter("productId").split(" ");
	prodId = pSplit[0];
}
if(request.getParameter("customerName")!=null){
	customerName=request.getParameter("customerName").replaceAll("'", "@");
}
if(request.getParameter("mobile")!=null){
	mobile=request.getParameter("mobile");
}
if(request.getParameter("gothram")!=null){
	 gothram=request.getParameter("gothram").replaceAll("'", "@");
}
if(request.getParameter("address")!=null){
	address=request.getParameter("address").replaceAll("'", "@");
}
if(request.getParameter("emailid")!=null){
	emailid=request.getParameter("emailid");
}
if(request.getParameter("purpose")!=null){
	purpose=request.getParameter("purpose").replaceAll("'", "@");
}
if(request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals("")){
	 fromdate=request.getParameter("fromdate")+" 00:00:01";
}
if(request.getParameter("todate")!=null && !request.getParameter("todate").equals("")){
	 todate=request.getParameter("todate")+" 23:59:59"; 
}
if(request.getParameter("pancardNo")!=null){
	 pancardNo=request.getParameter("pancardNo");
}
if(request.getParameter("paymentType")!=null){
	paymentType=request.getParameter("paymentType");
}
if(paymentType.equals("card")){
	 fourDigitNum=request.getParameter("fourDigits");
	 authenticationNum=request.getParameter("authNum");
}
%>

<jsp:setProperty name="CSP" property="billingId" value="<%=billId%>"/>
<jsp:setProperty name="CSP" property="customername" value="<%=customerName%>"/>
<jsp:setProperty name="CSP" property="phoneno" value="<%=mobile%>"/>
<jsp:setProperty name="CSP" property="extra2" value="<%=purpose%>"/>
<jsp:setProperty name="CSP" property="extra3" value="<%=fromdate%>"/>
<jsp:setProperty name="CSP" property="extra4" value="<%=todate%>"/>
<jsp:setProperty name="CSP" property="gothram" value="<%=gothram%>"/>
<jsp:setProperty name="CSP" property="extra6" value="<%=address%>"/>
<jsp:setProperty name="CSP" property="extra8" value="<%=emailid%>"/>
<jsp:setProperty name="CSP" property="extra18" value="<%=pancardNo%>"/>
<jsp:setProperty name="CSP" property="extra10" value="<%=fourDigitNum%>"/>
<jsp:setProperty name="CSP" property="extra14" value="<%=authenticationNum%>"/>
<%boolean flag1 = CSP.update();%>

<%if(!prodId.equals("") && (prodId.equals("20063") || prodId.equals("20092"))){%>
<jsp:setProperty name="CAP" property="billingId" value="<%=billId%>"/>
<jsp:setProperty name="CAP" property="first_name" value="<%=customerName%>"/>
<jsp:setProperty name="CAP" property="email_id" value="<%=emailid%>"/>
<jsp:setProperty name="CAP" property="address1" value="<%=address%>"/>
<jsp:setProperty name="CAP" property="gothram" value="<%=gothram%>"/>
<jsp:setProperty name="CAP" property="pancard_no" value="<%=pancardNo%>"/>
<jsp:setProperty name="CAP" property="from_date" value="<%=fromdate%>"/>
<jsp:setProperty name="CAP" property="to_date" value="<%=todate%>"/>
<%CAP.update();
}%>

<%if(sssstId != null && !sssstId.equals("")){%>
<jsp:setProperty property="sssst_id" name="DREG" value="<%=sssstId %>"/>
<jsp:setProperty property="first_name" name="DREG" value="<%=customerName %>"/>
<jsp:setProperty property="address_1" name="DREG" value="<%=address %>"/>
<jsp:setProperty property="mobile" name="DREG" value="<%=mobile %>"/>
<jsp:setProperty property="pancard_no" name="DREG" value="<%=pancardNo %>"/>
<jsp:setProperty property="email_id" name="DREG" value="<%=emailid %>"/>
<jsp:setProperty property="gothram" name="DREG" value="<%=gothram %>"/>
<%DREG.update();
}%>

<%
if(flag1 == true){
	response.sendRedirect("lastHourEdit.jsp?msg=detailsupdated");
}	
%>

</body>
</html>