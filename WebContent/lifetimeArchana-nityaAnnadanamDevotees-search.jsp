<%@page import="beans.customerpurchases"%>
<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@media print{
   .print_table{
    width: 100%;
    border-collapse: collapse;
	vertical-align: middle;
	page-break-after:avoid;
}
.print_table td{
    border-color: black;
    font-size: 12px !important;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
	
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
	}

</style>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRO OFFICE DAILY POOJAS LIST | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
function productsearch(){
	  var data1=$('#productId').val();
	  var headID=null;
	  $.post('searchSubhead.jsp',{q:data1,hoa:headID,page :'Rec'},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productId" ).autocomplete({source: availableTags}); 
			});
}
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                    $( "a" ).css("text-decoration","none");
                    $( ".printable" ).print();
                   

                    // Cancel click event.
                    return( false );
                });
			});
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
   /*  $( "#subheadId" ).change(function() {
		  $( "#searchForm" ).submit();
		}); */
});

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="CAP" class="beans.customerapplication"/>
	<%if(session.getAttribute("empId")!=null){ 
		
		String searchbyname=request.getParameter("searchbyname");
		String searchbyperfname=request.getParameter("searchbyperfname");
		String searchbygothram=request.getParameter("searchbygothram");
		String searchbyid=request.getParameter("searchbyid");
		
	%>
	<div><%@ include file="title-bar.jsp"%></div>
	    
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">
        <div class="clear"></div>
		
        
        
		<div class="vendor-list">
		
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form name="searchForm" id="searchForm" action="lifetimeArchana-nityaAnnadanamDevotees-search.jsp" method="post">
				<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td></td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold; padding-top: 10px;">Devotees List NAD,LTA</td></tr>
				</table>
				<br/>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr style="margin-bottom:20px;">
				<td width="5%" class="bg">Name</td>
				<td width="10%" class="bg"><input type="text"
					name="searchbyname" id="searchbyname" value="" /></td>
				<td width="5%" class="bg">Performed Name</td>	
				<td width="10%" class="bg"><input type="text"
					name="searchbyperfname" id="searchbyperfname" value="" /></td>
				<td width="5%" class="bg">Gothram</td>	
				<td width="10%" class="bg"><input type="text"
					name="searchbygothram" id="searchbygothram" value="" /></td></tr>
				<tr><td width="5%" class="bg">NAD/LTAM-ID</td>	
				<td width="10%" class="bg"><input type="text"
					name="searchbyid" id="searchbyid" value="" /></td>	
					<td width="15%">
				<select name="subheadId" id="subheadId">
					<option value="20092"<%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20092")){ %> selected="selected" <%} %>>NITYA ANNADANA PADHAKAM</option>
					<option value="20063" <%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20063")){ %> selected="selected" <%} %>>LIFE TIME ARCHANA MEMBERSHIP</option>					
				</select>
				</td>
				<td width="10%" align="center"><input type="submit" name="Submit" value="Search" class="click"/></td>
					</tr>
				<%-- <tr>
				 <td width="18%">
				<select name="subheadId" id="subheadId">
					<option value="20092"<%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20092")){ %> selected="selected" <%} %>>NITYA ANNADANA PADHAKAM</option>
					<option value="20063" <%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20063")){ %> selected="selected" <%} %>>LIFE TIME ARCHANA MEMBERSHIP</option>					
				</select>
				</td> 
				</tr> --%>	
				<!-- <tr>
				<td colspan="6" height="35" align="center" style="padding: 5px;"><input
					name="Submit" type="submit" class="Submit" value="search"
					style="width: 30%; height: 50px; font-size: 14px;" class="click" /></td> 
				</tr> -->
				</table>
				<%-- <div class="search-list">
					<ul>
						<li>
						<select name="subheadId" id="subheadId">
							<option value="20092"<%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20092")){ %> selected="selected" <%} %>>NITYA ANNADANA PADHAKAM</option>
							<option value="20063" <%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20063")){ %> selected="selected" <%} %>>LIFE TIME ARCHANA MEMBERSHIP</option>
							
						</select>
						</li>
					</ul>
				</div> --%>
				</form>
				<div class="vendor-title" style="padding:30px 30px 50px 500px";> DEVOTEES LIST</div>
				<!-- <div class="vendor-title" style="padding:30px 30px 50px 20px;" align="center"> DEVOTEES LIST</div> -->
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					 <span>
						<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit Columns</dt>
										<dt><input type="checkbox" class="edit-setting" />Address</dt>
										<dt><input type="checkbox" class="edit-setting" />Email</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
		
				<div class="list-details" style="font-size:12px;">
					
	<%
	String submit=request.getParameter("Submit");
	if(submit!=null){
		%>
	<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:97%; 
    background:#d0e3fb; 
}

</style>

<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>		
	
		<% 
	SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	productsListing PRDL=new productsListing();

	String subhead="20092";
	if(request.getParameter("subheadId")!=null && !request.getParameter("subheadId").equals("")){
		subhead=request.getParameter("subheadId");
	}
	String hoaID="4";
	customerapplicationListing CUSAL=new customerapplicationListing();
	customerpurchases CP=new customerpurchases();
	CP.setcustomername(searchbyname);
	CP.setgothram(searchbygothram);
	CP.setExtra20(searchbyperfname);  //here using extra20 for performed name
	CP.setExtra19(searchbyid);  //here using extra19 for nad/ltam-id
	//List SAL_list=SALE_L.getDevoteesList(subhead);
	List SAL_list=SALE_L.searchAndGetDevoteesList(CP,subhead);
	List CUS_DETAIL=null;
	DecimalFormat df = new DecimalFormat("0.00");
	if(SAL_list.size()>0){%>

	<div class="printable">
					<table class="print_table" cellspacing="0" cellpadding="0" id="tblExport">
                    <tr>
						<td class="bg" colspan="12" align="center" style="font-weight:bold">LIST OF DEVOTEES  FOR <%=PRDL.getProductsNameByCats(subhead,session.getAttribute("headAccountId").toString())%></td>
						</tr>
                       
                            
  <tr>
    <td width="4%" class="bg" align="center" style="font-weight:bold"><strong>S.NO</strong></td>
    <td width="5%"class="bg"  align="center" style="font-weight:bold"><strong>APP REF.NO</strong>.</td>
    <td width="7%" class="bg" align="center" style="font-weight:bold"><strong>RECEIPT.NO</strong>.</td>
    <td width="10%"class="bg" align="center" style="font-weight:bold"><strong>NAME</strong></td>
    <td width="5%" class="bg" align="center" style="font-weight:bold"><strong>ADDRESS1</strong></td>
    <td width="5%" class="bg" align="center" style="font-weight:bold"><strong>ADDRESS2</strong></td>
    <td width="5%" class="bg" align="center" style="font-weight:bold"><strong>ADDRESS3</strong></td>
    <td width="7%" class="bg" align="center" style="font-weight:bold"><strong>CITY</strong></td>
    <td width="8%" class="bg" align="center" style="font-weight:bold"><strong>PINCODE</strong></td>
    <td width="21%" class="bg" align="center" style="font-weight:bold"><strong>SEVA PERFORM IN THE NAME OF</strong></td>
    <td width="12%" class="bg" align="center" style="font-weight:bold"><strong>GOTHRAM</strong></td>
    <td width="5%" class="bg" align="center" style="font-weight:bold"><strong>EDIT</strong></td>
    
  </tr>
  
 	<%
					for(int i=0; i < SAL_list.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_list.get(i);
						
							CUS_DETAIL=CUSAL.getMcustomerDetailsBasedOnBillingId(SALE.getbillingId());
							if(CUS_DETAIL.size()>0){
							CAP=(customerapplication)CUS_DETAIL.get(0);}
							%>
							<%
							String address2 = CAP.getextra2();
							String add2[]={""};
							if(!address2.equals("")){
							add2 = address2.split(",");
							}
							String cites = CAP.getextra3();
							String city[]={""};
							if(!cites.equals("")){
								city = cites.split(",");
							}
							
							%> 
                   
  <tr>
    <%-- <td  align="center"><input type="checkbox" name="check"/><%=i+1%></td> --%>
    <td  align="center"><%=i+1%></td>
    <td align="center"><%=CAP.getphone()%></td>
    <td align="center" ><%=SALE.getbillingId()%></td>
    <td align="center" style="max-width:140px;"><%=SALE.getcustomername()%></td>
  	<td align="center"><span><%=SALE.getextra6()%></span></td> 
  <%--  <td align="center" ><%if(CUS_DETAIL.size()>0){%><%=CAP.getextra2()%><%}%></td>  --%>
   <td align="center">
   <%if(add2.length > 0){
	   for(int k=0;k<add2.length;k++){%>
   <span><%=add2[k]%><%if(add2.length>1 && k<add2.length-1){ %>,<%} %><br/></span>
    <%}} %>
    </td>
    <td align="center" style="max-width:110px;"><%if(CUS_DETAIL.size()>0){%><%=CAP.getaddress2()%><%}%></td>
   <%--  <td align="center" style="max-width:90px;"><%if(CUS_DETAIL.size()>0){%><%=CAP.getextra3()%><%}%></td> --%>
    
    <td align="center" style="max-width:90px;">
   <%if(city.length > 0){
	   for(int k=0;k<city.length;k++){%>
   <%=city[k]%><%if(city.length>1 && k<city.length-1){ %>,<%} %><br/>
    <%}} %>
    </td>
    
    <td align="center" ><%if(CUS_DETAIL.size()>0){%><%=CAP.getpincode()%><%}%></td>
    <td align="center" style="max-width:210px;"><%if(CUS_DETAIL.size()>0){%><%=CAP.getlast_name()%><%}%></td>
    <td align="center"><%=SALE.getgothram()%></td>
      <td align="center"><a href="devoteesAddress_Edit.jsp?bid=<%=SALE.getbillingId()%>">Edit</a></td>
    
  </tr>
  

 <%}%>
</table>



				  </div>
					<%
					}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no list found for today!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>

	<!-- main content -->
	<%}} else{
	response.sendRedirect("index.jsp");
} %>
<div ><jsp:include page="footer.jsp"></jsp:include></div>
</body>
</html>