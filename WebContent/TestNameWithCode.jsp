<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<div class="list-details">
	    <%subheadListing subHeadListing=new subheadListing();
		List list=subHeadListing.getSubHead2("1","60");
		if(list.size()>0)
		{%>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="bg" width="3%">S.No.</td>
					<td class="bg" width="23%">Test Name</td>
					<td class="bg" width="23%">Test Code</td>
					<td class="bg" width="16%">Original Price</td>
					<td class="bg" width="16%">Price After Discount</td>
					<td class="bg" width="16%">Price Taken From Customer</td>
				</tr>
				<%for(int i=0; i < list.size(); i++ ){
					
					subhead diagnosticTest=(subhead)list.get(i);
					String originalPrice=diagnosticTest.getextra2();
					%>
				<tr>
					<td><%=i+1%></td>
					<td><span><%=diagnosticTest.getname()%></span></td>
					<td><%=diagnosticTest.getsub_head_id()%></td>
					<td><%=originalPrice%></td>
					<%
					int discountAmount=0;
					int remainingAmount=0;
					String dis=diagnosticTest.getextra4();
					if(dis!=null && !dis.trim().equals("") && originalPrice!=null && !originalPrice.trim().equals("")){
						discountAmount=(Integer.parseInt(originalPrice)*Integer.parseInt(dis)/100);
						remainingAmount=Integer.parseInt(originalPrice)-discountAmount;
					}
					%>
					<td><%=remainingAmount%></td>
					<%
					int roundOffAmount=remainingAmount+10;
					int reminder=roundOffAmount%10;
					if(reminder>0)
					{
						roundOffAmount=roundOffAmount+(10-reminder);
					}
					
					%>
					<td><%=roundOffAmount%></td>
				</tr>
				<%} %>
			</table>
		<%}else{%>
			<div align="center">
				<h1>No stock found</h1>
			</div>
		<%}%>
	</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>