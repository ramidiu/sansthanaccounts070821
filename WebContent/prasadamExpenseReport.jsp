<%@page import="mainClasses.produceditemsListing"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!--Date picker script  -->
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>

<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>

<script>
/* $(function() {
	$( "#fromDate2" ).datepicker({
		
	//	 minDate: new Date(2016, 3, 1),
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate2" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate2" ).datepicker({
			
			//minDate: new Date(2016, 3, 1),
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate2" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
$(function() {
	$( "#fromDate3" ).datepicker({
		
		// minDate: new Date(2016, 3, 1),
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate3" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate3" ).datepicker({
			
			//minDate: new Date(2016, 3, 1),
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate3" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

function datepickerchange()
{
	var finYear = $("#finYear").val();
	
	if(finYear == "2015-16")
	{
		document.getElementById('li1').style.display = "block";
		document.getElementById('li2').style.display = "block";
		document.getElementById('li3').style.display = "none";
		document.getElementById('li4').style.display = "none";
		document.getElementById('li5').style.display = "none";		
		document.getElementById('li6').style.display = "none";
	}
	
	else if(finYear == "2016-17")
	{
		document.getElementById('li1').style.display = "none";
		document.getElementById('li2').style.display = "none";
		document.getElementById('li3').style.display = "block";		
		document.getElementById('li4').style.display = "block";
		document.getElementById('li5').style.display = "none";		
		document.getElementById('li6').style.display = "none";
	}
	else if(finYear == "2017-18")
	{
		document.getElementById('li1').style.display = "none";
		document.getElementById('li2').style.display = "none";
		document.getElementById('li3').style.display = "none";		
		document.getElementById('li4').style.display = "none";
		document.getElementById('li5').style.display = "block";		
		document.getElementById('li6').style.display = "block";
	}
} */

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
});
</script>

 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        
        function validateForm() 
        {
            var finYear = document.forms["searchForm"]["finYear"].value;
            var type = document.forms["searchForm"]["type"].value;
            if (finYear == null || finYear == "") 
            {
                alert("Please select financial year");
                return false;
            }
            
            if (type == null || type == "") 
            {
                alert("Please select prasadam");
                return false;
            }
        } 
        
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
       
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<div class="vendor-page">
<%String type = request.getParameter("type"); %>
		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form name="searchForm" id="searchForm" action="prasadamExpenseReport.jsp" method="post" onsubmit="return validateForm()">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" name="page" value="prasadamExpenseReport"></input>
						<%-- <input type="hidden" name="type" value="<%=type%>"></input> --%>
						</li>
						<%
						
						String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("finYear")!=null && request.getParameter("finYear").equals("null"))
						{	
							if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
								 fromdate=request.getParameter("fromDate");
							}
							if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
								todate=request.getParameter("toDate");
							}
						}
						
// 						else if(request.getParameter("finYear")!=null && request.getParameter("finYear").equals("2016-17"))
// 						{	
// 							if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
// 								 fromdate=request.getParameter("fromDate");
// 							}
// 							if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
// 								todate=request.getParameter("toDate");
// 							}
// 						}
// 						else if(request.getParameter("finYear")!=null && request.getParameter("finYear").equals("2017-18"))
// 						{	
// 							if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
// 								 fromdate=request.getParameter("fromDate");
// 							}
// 							if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
// 								todate=request.getParameter("toDate");
// 							}
// 						}
						
						%>
						<%-- <li><select name="finYear" id="finYear"  Onchange="datepickerchange();">
						<%if(request.getParameter("finYear") == null){ %>
						<option value="">-- Select Fin Year --</option><%} %>
						<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%> selected="selected"<%} %>>2015-16</option>
						<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
						</select></li>
						 --%>
						 <li>
						 <%
		SimpleDateFormat SDF1=new SimpleDateFormat("yyyy-MM-dd");
		Calendar calld = Calendar.getInstance();
		calld.add(Calendar.DATE,0);
		String todayDate=SDF1.format(calld.getTime());
		String date[] = todayDate.split("-");
		 String currentyear = date[0];// 2017
		 String currentyear1 = currentyear.replace(currentyear.substring(2),""); // 20 
		 String currentyear2 = currentyear.replace(currentyear.substring(0,2),"");// 17
		 String year2 ="";
		 String year22 = "";	 
		 int currentyearinint = Integer.parseInt(currentyear2);	 
		 if(todayDate.compareTo(currentyear+"-04-01") >=0){
			 year2 =String.valueOf(currentyearinint+1);// 18
			 %>		
			
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
			<!-- <option value="">-- Select Fin Year --</option> -->
					<option value="<%=currentyear1 %><%= currentyear2%>-<%= year2%>" ><%=currentyear1 %><%=currentyear2 %>-<%=currentyear1 %><%=year2 %></option>
						<%
							for(int i=currentyearinint;i>15;i--){
								%>
								<option value="<%=currentyear1 %><%=i-1%>-<%= i%>"><%=currentyear1 %><%=i-1%>-<%=currentyear1 %><%= i%></option>
								<%}
						%>
					</select>
			<%
		 }
		 else{
			 year2 =String.valueOf(currentyearinint-1);
			 %>
			 		<select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<!-- <option value="">-- Select Fin Year --</option> -->
					<option value="<%=currentyear1 %><%= year2%>-<%= currentyear2%>" ><%=currentyear1 %><%=year2 %>-<%=currentyear1 %><%=currentyear2 %></option>
						<%
							for(int i=currentyearinint-1;i>15;i--){
								%>
								<option value="<%=currentyear1 %><%=i-1%>-<%= i%>"><%=currentyear1 %><%=i-1%>-<%=currentyear1 %><%= i%></option>
								<%}
						/* System.out.println(currentyear1); */
						%>
					</select> <% }%>
					 </li> 
						<li><select name="type" id="type">
						<%-- <%if(request.getParameter("type") == null){ %> --%>
						<%-- <option value="">-- Select --</option><%} %> --%>
						<option value="laddu" <%if(request.getParameter("type") != null && request.getParameter("type").equals("laddu")) {%> selected="selected"<%} %>>LADDU</option>
						<option value="pulihora" <%if(request.getParameter("type") != null && request.getParameter("type").equals("pulihora")) {%> selected="selected"<%} %>>PULIHORA</option>
						<option value="doodhpeda" <%if(request.getParameter("type") != null && request.getParameter("type").equals("doodhpeda")) {%> selected="selected"<%} %>>DOODH PEDA</option>
						</select></li>

						<%-- <li id = "li1" style="display:none;"><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li id = "li2" style="display:none;"><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly/></li>


						<li id = "li3" style="display:none;"><input type="text"  name="fromDate2" id="fromDate2" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li id = "li4" style="display:none;"><input type="text" name="toDate2" id="toDate2"  class="DatePicker" value="<%=todate %>" readonly/></li>

						<li id = "li5" style="display:none;"><input type="text"  name="fromDate3" id="fromDate3" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li id = "li6" style="display:none;"><input type="text" name="toDate3" id="toDate3"  class="DatePicker" value="<%=todate %>" readonly/></li> --%>
						
						<li style="display:block;"><input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/></li>
						<li style="display:block;"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>

				</div>
				<div class="clear"></div>
				<div class="list-details">

	 <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>	
    <div class="clear"></div>
  <div class="printable">
  <%
  if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("") && request.getParameter("type") != null && !request.getParameter("type").equals(""))
  {
  DateFormat df2= new SimpleDateFormat("dd-MM-yyyy"); 
  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	DecimalFormat formatter = new DecimalFormat("#,##,###.00");
	
	customerpurchasesListing SALE_L=new customerpurchasesListing();
	shopstockListing shopstock=new shopstockListing();
	produceditemsListing productedItems = new produceditemsListing();
	
	String name = "";
	String productId = "";
	String subheadId = "";
	
	if(type.equals("laddu"))
	{
		name = "Laddu";
		productId = "8595";
		subheadId = "20098";				
	}
	
	else if(type.equals("pulihora"))
	{
		name = "PULIHORA";
		productId = "PULIHORA";
		subheadId = "20100";				
	}
	
	else if(type.equals("doodhpeda"))
	{
		name = "DOODH PEDA";
		productId = "8553";
		subheadId = "20097";				
	}
 
	String openingBalFromDate = "";
	String openingBalFromDateAndTime = "";
	String openingBalToDate = "";
	String openingBalToDateAndTime = "";
	Double totalIssueQuan = 0.0;
	Double totalSalesQuan = 0.0;
	Double openingBal = 0.0;
	

	if(request.getParameter("finYear").equals("2015-16"))
	{
		openingBalFromDate = "2015-04-01";
		openingBalFromDateAndTime = "2015-04-01 00:00:01";
	}
	
	else if(request.getParameter("finYear").equals("2016-17"))
	{
		openingBalFromDate = "2016-04-01";
		openingBalFromDateAndTime = "2016-04-01 00:00:01";
	}
	
	

	openingBalToDate = fromdate;
	openingBalToDateAndTime = todate+" 00:00:00";
	
	

	if(productId .equals("PULIHORA"))
	{
		totalIssueQuan = productedItems.getDayTransferQty(productId,openingBalFromDateAndTime,openingBalToDateAndTime);
	}
	
	else
	{
		totalIssueQuan = shopstock.getStockValuationReport2(productId,openingBalFromDate,openingBalToDate);
	}
	
	totalSalesQuan = Double.parseDouble(SALE_L.getProductSaleQty(subheadId,openingBalFromDateAndTime,openingBalToDateAndTime));
	
	openingBal = totalIssueQuan - totalSalesQuan;
	
	%>
  <table width="100%" cellpadding="0" cellspacing="0" >
	<tr><td colspan="5" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;"><%=name%> Expenses Report</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;">Report From Date <%=df2.format(sdf.parse(fromdate))%>  TO  <%=df2.format(sdf.parse(todate)) %> </td></tr></table>
   <table width="100%" cellpadding="0" cellspacing="0" id="tblExport">			
   <tr>
   <td class="bg" width="10%">S.No</td>								
   <td class="bg" width="20%">Date</td>
   <td class="bg" width="20%">Issue Quantity</td>
   <td class="bg" width="20%">Sales Quantity</td>
   <td class="bg" width="20%">Balance On That Day</td>
   <td class="bg" width="20%">Balance</td>
   </tr>
   <tr>
   <td colspan="5" align="right" style="font-weight: bold;color: orange;">OPENING BALANCE | </td>
   <td colspan="1" align="right"><%=formatter.format(openingBal)%></td>  
   </tr>
			<% 
			
			
			int k = 1;
			Date start;
			Date end;
				
			start = (Date)format.parse(fromdate);
			end = (Date)format.parse(todate);
			
			GregorianCalendar gcal = new GregorianCalendar();

			gcal.setTime(start);
		    while (gcal.getTime().before(end) || gcal.getTime().equals(end)) 
		    {
		       
		        Date d = (Date)gcal.getTime();
		        
		        
		        //Date convertedDate = sdf.parse(d);
		        String datee=sdf.format(d);
		        
		        String start2 = datee + " 00:00:01";
		        String end2 = datee + " 23:59:59";		      		        
		        
		        String salequan = SALE_L.getProductSaleQty(subheadId,start2,end2);		
		        
		        if(salequan == null || salequan.equals(""))
		        {
		        	salequan = "0";
		        }
				
				Double isuuequan = 0.0;
				
				if(productId .equals("PULIHORA"))
				{
		        	isuuequan = productedItems.getDayTransferQty(productId,start2,end2);
				}
				
				else
				{
					isuuequan = shopstock.getDayTransferQty2(productId,datee);
				}
		        %>
		        <tr><td><%=k++ %></td>
		        <td> <%=df2.format(sdf.parse(datee)) %></td>
		        <td><%=isuuequan %></td>
		        <td><%=Double.parseDouble(salequan) %></td>
		        <%double bal = (isuuequan - Double.parseDouble(salequan)); %>
		        <td><%=bal %></td>	
		        <%openingBal += bal; %>			
		        <td align="right"><%=openingBal %></td>
		        </tr>			        
		        
		    <%
		    gcal.add(Calendar.DATE, 1);
		    }
			%>       
<tr>
	<td class="bg" colspan="5" align="right" style="font-weight: bold;color: orange;">CLOSING BALANCE | </td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(openingBal) %></td>
</tr>
			         
					</table>									
			</div>
			</div>
</div>
</div>
	
<%} %>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</div>
</body>