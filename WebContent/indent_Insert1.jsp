<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.superadmin"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="NOG" class="beans.no_genaratorService"/>
<jsp:useBean id="IND" class="beans.indentService"/>
<jsp:useBean id="SUPA" class="beans.superadmin"/>
<%if (session.getAttribute("empId") != null)	{
	String head_account_id=request.getParameter("headAccountId");
	String indentinvoice_id = request.getParameter("invoiceId");
	String desc=request.getParameter("narration");
	String description=desc.replaceAll("'","#");
	String requirement=request.getParameter("requirement");
	Calendar c1 = Calendar.getInstance(); 
	DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat  dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
	String date=request.getParameter("indentDate");
	String status="reportRaised";
	String emp_id=session.getAttribute("empId").toString();
	String role=session.getAttribute("Role").toString();
	String require_date="";
	if(request.getParameter("require_date")!=null && !request.getParameter("require_date").equals("")){
		require_date=request.getParameter("require_date");
	}
	String product_id="";
	String quantity="";
	String vendor_id = request.getParameter("vendorId").split(" ")[0];
	int count=Integer.parseInt(request.getParameter("count"));
	for (int i = 0 ; i < count ; i++ )	{
		product_id = request.getParameter("product"+i);
		quantity=request.getParameter("quantity"+i);
		if (!quantity.trim().equals(""))	{%>
					<%if(role.equals("AsstPO")){ %>
		<jsp:setProperty name="IND" property="status" value="reportPending"/>
		<jsp:setProperty name="IND" property="extra2" value="<%=emp_id%>"/>
		<jsp:setProperty name="IND" property="extra3" value="<%=date%>"/>
		<%} else{ %>
		<jsp:setProperty name="IND" property="emp_id" value="<%=emp_id%>"/>
		<jsp:setProperty name="IND" property="status" value="<%=status%>"/>
		<%} %>
		<jsp:setProperty name="IND" property="description" value="<%=description%>"/>
		<jsp:setProperty name="IND" property="date" value="<%=date%>"/>
		<jsp:setProperty name="IND" property="indentinvoice_id" value="<%=indentinvoice_id%>"/>
		<jsp:setProperty name="IND" property="requirement" value="<%=requirement%>"/>
		<jsp:setProperty name="IND" property="require_date" value="<%=require_date%>"/>
		<jsp:setProperty name="IND" property="head_account_id" value="<%=head_account_id%>"/>
		<jsp:setProperty name="IND" property="product_id" value="<%=product_id%>"/>
		<jsp:setProperty name="IND" property="quantity" value="<%=quantity%>"/>
		<jsp:setProperty name="IND" property="vendor_id" value="<%=vendor_id%>"/>
		<%=IND.insert()%><%=IND.geterror()%>
		<%}
}
 	if(head_account_id.equals("5")){
		NOG.updatInvoiceNumber(indentinvoice_id,"sainivas_indent_invNum");	
	}else{
		NOG.updatInvoiceNumber(indentinvoice_id,"indentinvoicenumber");	
	} 
	superadminListing SUPA_L=new superadminListing();
	headofaccountsListing HOFA_L=new headofaccountsListing();
	String heodofacname= HOFA_L.getHeadofAccountName(head_account_id);
	String html="";
	List suplist=SUPA_L.getMsuperadminWithHOA(head_account_id);
	if(!role.equals("AsstPO")){ 
	MailBox.SendMailBean Mail=new MailBox.SendMailBean();
	if(suplist.size()>0){
		for(int i=0;i<suplist.size();i++)
		{
		SUPA=(superadmin)suplist.get(i);
		html="<div style='width:750px; height:500px; margin:0px auto; font-family:Verdana, Geneva, sans-serif; font-size:14px;'><table width='100%' cellpadding='0' cellspacing='0'><tr><td style='text-align:center; font-size:18px; color:#7f3519; font-weight:bold;'>SHRI SHIRDI SAI BABA SANSTHAN TRUST</td></tr><tr><td style='text-align:center'>Dilsuknagar, Hyderabad</td></tr><tr><td>Dear  "+SUPA.getadmin_name()+",</td></tr><tr><td><p style='text-align:justify;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; One indent is raised by "+heodofacname+" department. </p></td></tr><tr><td style='font-weight:bold; font-size:16px;'>Please <a href='www.true-local.co.uk/superadmin/adminPannel.jsp?page=indentDetailedReport&invid="+IND.getindent_id()+"' style='text-decoration:none; color:#F00;'>Click Here</a> to find the indent details and please check the indent products quantity raised by purchase officer and please approve it. </td></tr><tr><td height='20'></td></tr><tr><td>Thanking you,</td></tr><tr><td>warm regards</td></tr><tr><td style='text-transform:capitalize;'>Sai Sansthan Trust.</td></tr></table></div>";
		//html="<br></br> Dear "+SUPA.getadmin_name()+", <br> One Indent is Raised by  "+heodofacname+" department. Please follow this link for approve the indent <br></br><br></br> <a href='www.true-local.co.uk/superadmin/adminPannel.jsp?page=indentDetailedReport&invid="+IND.getindent_id()+"' >Click Here </a>Indent No "+IND.getindent_id()+".<br></br>Thanks & Regards,<br></br>Sai Sansthan Trust.";
		//out.println(Mail.send("accounts@saisansthan.in",SUPA.getextra3(), "", "", "Indent is awaiting for approval  Raised By "+heodofacname+".Please approve the indent", html, "localhost"));
		}
	}
	}
	response.sendRedirect("indentReport1.jsp");
}
else	{
response.sendRedirect("index.jsp");
}%>
</body>
</html>