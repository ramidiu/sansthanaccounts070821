<%@page import="java.util.TimeZone"%>
<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="java.util.Date"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PRINT RECEIPT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
function getNumber()
	{
		var headId = $("#headId").val();
		var receiptNo = document.getElementById("receiptNo");
		$.post('getNumGenVal.jsp',{headAccountId:headId}, function(response) 
				{       	
					var where_is_mytool=response.trim();
					document.getElementById("receiptNo").value = where_is_mytool;
				});
	}


$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#fromdate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#todate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
});

$(function() {
	  $( ".datepickernew" ).datepicker({ 
		  yearRange: '1940:2020',
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
		  });
	}); 
	</script>
<!--Date picker script  -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function combochange(denom,desti,jsppage) { 
    var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value; 
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	var amt=document.getElementById("Amount");
	$.post(jsppage,{ id: com_id } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	var x=document.createElement('option');
	x.text='Select pooja name';
	x.value='';
	sda1.add(x);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");
				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
					sda1.add(y,null);
				}
				catch(e)
				{
					sda1.add(y);
				}
	}
	}
	}); 
}
function getPoojaPrice(){
	
	var proId=document.getElementById("productId").value;
	var subhId=document.getElementById("type").value;
	var amt=document.getElementById("Amount");
	var qty=document.getElementById("ticQty").value;
	var totAmt=document.getElementById("totAmt").value;
	$.ajax({
		type:'post',
		url: 'getPoojaPrice.jsp', 
		data: {
		q : subhId,
		b : proId
		},
		success: function(response) { 
			var arr=response.trim().split(":");
			var price=arr[0];
			var application=arr[1];
			amt.value='0';
			document.getElementById("appl").style.display='none';			
			if(application!=null && application!="" && application=="application"){
				document.getElementById("applicatn").value=arr[1];
				document.getElementById("appl").style.display='block';
			}
			amt.value=price;
			document.getElementById("totAmt").value=Number(price)*Number(qty);
			
		}});
}
 function productsearch(){
	
	  var data1=$('#productId').val();
	  var headId=$('#headId').val();
	  var arr = [];
	 
	  $.post('searchSubhead.jsp',{q:data1,hoa:headId,page : 'Rec'},function(data)
	{
		var response = data.trim().split("\n");
		
		 var doctorNames=new Array();
		 var amount=new Array();
		 var appl=new Array();
		 var gross=0;
		 
			var amt=document.getElementById("Amount");
			var qty=document.getElementById("ticQty").value;
			var totAmt=document.getElementById("totAmt").value;
		 for(var i=0;i<response.length;i++){			 
			 var d=response[i].split(",");
			 gross=d[2];
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        appl:""+d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#productId').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#Amount').val(ui.item.amount);
				$('#Amounthidden').val(ui.item.amount);//this value will store in hidden field of "Amounthidden"
				var amt=ui.item.amount;
				var application=ui.item.appl;
				var offerkind=ui.item.label.split(' ');
		        var offer=offerkind[0];
				document.getElementById("totAmt").value=Number(amt)*Number(qty);
				document.getElementById("totAmthidden").value=Number(amt)*Number(qty);//this value will store in hidden field of "totAmthidden"
				document.getElementById("appl").style.display='none';
				  if(application.trim()==="application"){
					document.getElementById("applicatn").value=application;
					document.getElementById("appl").style.display='block';
				 } 
				 if(offer.trim()==="20032"){
					document.getElementById("offerkindt").style.display='block';
					document.getElementById("offerkindn").style.display='block';
					$('#offerkind_refid').focus();
					 } else{
							document.getElementById("offerkindt").style.display='none';
							document.getElementById("offerkindn").style.display='none';
							//$('#registration').focus();
							/* $('#mobile').focus(); */
							$('#uniqueID').focus();
					 }
				// $("#offerkindn").focus();
				//$('#mobile').focus();
				//$('#registration').focus();
				 $("#productId").attr('readonly', true);
			}
		}); 
	});
} 

function amountInWords(){
	  var amount=$('#totAmt').val();
		  var arr = [];
	  $.post('numberInWords.jsp',{q:amount},function(data)
	{
		var response = data.trim().split("\n");
		 $('#amountinwords').empty();
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();	 
		 			 var d=response[0].split(",");	
			 $('#amountinwords').append('('+d[0]+')');
		
	
	});
	} 

function phonenosearch(){
	  var mobile=$('#mobile').val();
		  var arr = [];
	  $.post('searchPhoneNo.jsp',{q:mobile},function(data)
	{
		var response = data.trim().split("\n");
		
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();	 
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
		
			 arr.push({
				 label: d[0],
				 customerName:d[1],
				 gothram:d[2],
				 address:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#mobile').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#customerName').val(ui.item.customerName);
				$('#gothram').val(ui.item.gothram);
				$('#address').val(ui.item.address);			
			}
		}); 
	});
}

function GetUniqueRegistrations(){
	  var mobile=$('#uniqueID').val();
		  var arr = [];
	 $.post('searchPhoneNo.jsp',{q:mobile,con : 'uniqueId'},function(data) 
	{
		var response = data.trim().split("\n");
		var sssstId = new Array();
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();
		 var address2=new Array();
		 var address3=new Array();
		 var city=new Array();
		 var pincode=new Array();
		 var pancard=new Array();
		 var email=new Array();
		 var dob=new Array();
		 for(var i=0;i<response.length;i++){
			// var d=response[i].split(",");
			 var d=response[i].split(":");
			 arr.push({
				 label: d[0],
				 sssstId :d[1],
				 phoneno:d[2],
				 customerName:d[3],
				 gothram:d[4],
				 address:d[5],
				 address2:d[6],
				 city:d[7],
				 pincode:d[8],
				 email:d[9],
				 pancard:d[10],
				 dob:d[11],
				 address3:d[12],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#uniqueID').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#uniqueid').val(ui.item.sssstId);
				$('#mobile').val(ui.item.phoneno);
				$('#customerName').val(ui.item.customerName);
				$('#gothram').val(ui.item.gothram);
				$('#address').val(ui.item.address);	
				$('#addressSj').val(ui.item.address);		
				$('#address2').val(ui.item.address2);
				$('#address3').val(ui.item.address3);
				$('#city').val(ui.item.city);
				$('#pincode').val(ui.item.pincode);
				$('#emailid').val(ui.item.email);
				$('#date').val(ui.item.dob);
				$('#pancardNo').val(ui.item.pancard);
				
			}
		}); 
	});
}
function calculateTotal(){
	var amt=document.getElementById("Amount").value;
	var qty=document.getElementById("ticQty").value;
	document.getElementById("totAmt").value=Number(amt)*Number(qty);
	amountInWords();
} 
function validate(){
	$('#poojaError').hide();
	$('#amtError').hide();
	$('#nameErr').hide();
	$('#othercashError').hide();
	$('#currencyError').hide();
	var currencyType=$('#currencyType').val();
	var paymentType=$('#paymentType').val();
	/* alert(currencyType); */
	
	var amt=$('#Amount').val().trim();
	var taille = $('#pancardNo').val().length;
	if(amt>9999)
	{
		if($('#pancardNo').val().trim()=="")
		{
		$('#panError').show();
		$('#pancardNo').addClass('borderred');
		$('#pancardNo').focus();
		return false;
		}
		if (taille !=10) 
		{
			$('#panLenError').show();
			$('#pancardNo').addClass('borderred');
			$('#pancardNo').focus();
			return false;
		}
	}
	
	if (paymentType =="card"){
		if(($('#fourDigits').val().trim()==""))
		{
			$('#fdError').show();
			$('#fourDigits').addClass('borderred');
			$('#fourDigits').focus();
			 return false;
		}
		
		if(($('#authNum').val().trim()==""))
		{
			$('#anError').show();
			$('#authNum').addClass('borderred');
			$('#authNum').focus();
			 return false;
		}
	}
	
	if (currencyType != "RS" && paymentType !="othercash"){
		/*  alert("please select paymentType as othercash"); */
		$('#othercashError').show();
		$('#paymentType').addClass('borderred');
		$('#paymentType').focus();
		 return false;
	}
	if (currencyType == "RS" && paymentType =="othercash"){
		/*  alert("please select paymentType as othercash"); */
		$('#currencyError').show();
		$('#currencyType').addClass('borderred');
		$('#currencyType').focus();
		 return false;
	}
	if(($('#productId').val().trim()=="")){
		$('#poojaError').show();
		$('#productId').addClass('borderred');
		$('#productId').focus();
		return false;
	}
	/* if(($('#customerName').val().trim()=="")){
		$('#nameErr').show();
		$('#customerName').addClass('borderred');
		$('#customerName').focus();
		return false;
	} */
	if(($('#Amount').val().trim()=="") || ($('#Amount').val().trim()=="0")){
		$('#amtError').show();
		$('#Amount').addClass('borderred');
		$('#Amount').focus();
		return false;
	}
		/* if($("#applicatn").val()=="application"){
		var amt=$('#Amount').val().trim();
		var taille = $('#pancardNo').val().length;
		if($('#pancardNo').val().trim()==""){
			$('#panError').show();
			$('#pancardNo').addClass('borderred');
			$('#pancardNo').focus();
			return false;
		}
		if (taille !=10) {
    		$('#panLenError').show();
			$('#pancardNo').addClass('borderred');
			$('#pancardNo').focus();
			return false;
		}
		if(amt>9999){
			if($('#pancardNo').val().trim()==""){
			$('#panError').show();
			$('#pancardNo').addClass('borderred');
			$('#pancardNo').focus();
			return false;
		}}
	} */
		var sevaname=$('#productId').val();
		var amount=$('#totAmt').val();
		var wordAmt=$('#amountinwords').text();
		 var status = confirm('You have entered below details.Please check : \n Seva Name : '+sevaname+' \n Total Amount : '+amount+' '+wordAmt+' \n Click OK to proceed');
		   if(status == false){
		   return false;
		   }
		   else{
			   $.blockUI({ css: { 
			        border: 'none', 
			        padding: '15px', 
			        backgroundColor: '#000', 
			        '-webkit-border-radius': '10px', 
			        '-moz-border-radius': '10px', 
			        opacity: .5, 
			        color: '#fff' 
			    } });
		   return true; 
		   }
		  
}
function nameAddValidation(){
	$("#warMsg").hide();
	$("#save").show();
	var fsname=$("#customerName").val();
	var mobileNo=$("#mobile").val();
	var email=$("#emailid").val();
	if(fsname=="" && email==""){

	}else if($("#applicatn").val()=="application"){
	$.ajax({

		type:'post',
		url: 'customerValidation.jsp', 
		data: {
		name : fsname,
		mobile : mobileNo,
		emailId : email
		},
		success: function(response) { 
			var resMsg=response.trim();
			/* if(resMsg==="customer exists"){ */
				/* $("#warMsg").show();
				$("#save").hide(); */
				/* $('#panError').show();
				$('#pancardNo').addClass('borderred');
				$('#pancardNo').focus();
			}else if(resMsg==="customer does not exists"){ */
				/* $("#warMsg").hide();
				$("#save").show(); */
				/* $('#panError').hide();
			} */
		}});
	}
}
$(document).ready(function () {
    $("#paymentType").change(function () {
        var paymentType=$("#paymentType").val();
        $("#chequeDetails1").hide();
        $("#chequeDetails2").hide();
        $("#chequeDetails3").hide();
        $("#chequeDetails4").hide();
        
        $("#othercashheading").hide();
        $("#otherCashPaymentType").hide();
        
        $("#fd1").hide();
        $("#an1").hide();
        $("#fd2").hide();
        $("#an2").hide();
        
        if(paymentType==="cheque"){
        	$("#chequeDetails1").show();
        	$("#chequeDetails2").show();
        	$("#chequeDetails3").show();
        	$("#chequeDetails4").show();
        }
        
        
        if(paymentType==="othercash"){
        	$("#othercashheading").show();
        	$("#otherCashPaymentType").show();
        }
        
        if(paymentType==="card"){
        	$("#fd1").show();
        	$("#an1").show();
        	$("#fd2").show();
        	$("#an2").show();
        }
        
        
    });
    
    $("#otherCashPaymentType").change(function () {
    	var otherCashPaymentTypeVal=$("#otherCashPaymentTypeVal").val();
        $("#chequeDetails1").hide();
        $("#chequeDetails2").hide();
        $("#chequeDetails3").hide();
        $("#chequeDetails4").hide();
        
        if(otherCashPaymentTypeVal==="othercash-cheque"){
        	$("#chequeDetails1").show();
        	$("#chequeDetails2").show();
        	$("#chequeDetails3").show();
        	$("#chequeDetails4").show();
        }
    });
    
    
    $(".click").click(function () {
    	$('#productId').focus();
    	 $("#productId").attr('readonly', false);
    });
    $("#registration").click(function () {
    		 document.getElementById("uniqueID").disabled = true;
    		 $("#mobile").focus();
    });
    $("#registration1").click(function () {
		document.getElementById("uniqueID").disabled = false;
		/* document.getElementById("uniqueid").disabled = false; */
		$('#uniqueID').focus();
	});
});
</script>
 <script>
function makereadonly(){
	var amounthidden = document.getElementById("Amounthidden").value;
	var totamthidden = document.getElementById("totAmthidden").value;
	var quantityhidden = document.getElementById("ticQtyhidden").value;
 if(document.getElementById("paymentType").value == "othercash"){
	 var amount1 = 1;
	 $('#Amount').attr('readonly',true);
	 document.getElementById("ticQty").value = Number(amount1);
	 document.getElementById("Amount").value=Number(amount1);
	 document.getElementById("totAmt").value=Number(amount1);
 }else if(document.getElementById("paymentType").value == "cash" || document.getElementById("paymentType").value == "cheque"){
	 $('#Amount').removeAttr('readonly');
	 document.getElementById("ticQty").value = Number(quantityhidden);
	 document.getElementById("Amount").value=Number(amounthidden); 
	 document.getElementById("totAmt").value=Number(totamthidden); 
 }
 
}
</script>
<script type="text/javascript">
function submit(){
	document.getElementById("sub").submit();
}
</script>
<script type="text/javascript">
setTimeout(function() {
$('#successMessage').fadeOut('slow');
}, 5000);
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="SUB" class="beans.subhead"></jsp:useBean>
<jsp:useBean id="MNR" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="CSP" class="beans.customerpurchases"/>
<%if(session.getAttribute("empId")!=null){ 
String HOAID=session.getAttribute("headAccountId").toString();

%>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-box">
 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO OFFICE </td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;padding-top:20px">EDIT OFFERKIND</td></tr>
</table>
<br>
<%if(request.getParameter("msg") != null && !request.getParameter("msg").equals("")){
	if(request.getParameter("msg").equals("detailsupdated")){%>
<div id="successMessage" style="text-align: center;padding-bottom:15px;">
<div style="text-align:center;color:green;font-style:italic;font-weight: bold;"> OfferKind Details updated Successfully</div>
</div>
<%
}}
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
Calendar cal = Calendar.getInstance();
cal.setTime(new Date());
cal.add(Calendar.HOUR, -1);
Date oneHourBack = cal.getTime();
System.out.println(" oneHourBack  ===>"+oneHourBack);
SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sd2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
sd1.setTimeZone(TimeZone.getTimeZone("IST"));
sd2.setTimeZone(TimeZone.getTimeZone("IST"));
String currentTime = sd1.format(Calendar.getInstance().getTime());
System.out.println("currentTime=== >"+currentTime);
String minusOneHour = sd2.format(oneHourBack);
System.out.println("minusOneHour====>"+minusOneHour);
SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
customerpurchasesListing cpl = new customerpurchasesListing();
subheadListing shl = new subheadListing();
productsListing pl = new productsListing();
sssst_registrationsListing srl = new sssst_registrationsListing();
String offk="offerKind";
List<customerpurchases> cpList = cpl.getLastOneHourListOfferkind(minusOneHour, currentTime, offk);
//List<customerpurchases> cpList = cpl.getLastOneHourList("2017-01-21 10:53:24", "2017-01-21 11:53:24");
if(cpList.size()>0){
%>
<form action="lastHourOfferKindEdit.jsp" name="sub" id="sub" method="post">
<div style="padding-left:540px"><Select name="rId" id="rId" onChange="submit();">
<option value="">---Select Billing ID---</option>

<%for(int i=0;i<cpList.size();i++){
		customerpurchases CpList = cpList.get(i);%>
<option value="<%=CpList.getpurchaseId()%>" <%if(request.getParameter("rId") != null && request.getParameter("rId").equals(CpList.getpurchaseId())){%>selected<%}%>><%=CpList.getbillingId()%></option>
<%}%>
</Select></div>
</form>
<% 	if(request.getParameter("rId") != null && !request.getParameter("rId").equals("")){
 	String rId = request.getParameter("rId");
 	
	List<customerpurchases> cList = cpl.getLastOneHourCustomer(minusOneHour, currentTime, rId);
 	//List<customerpurchases> cList = cpl.getLastOneHourCustomer("2017-01-21 10:53:24", "2017-01-21 11:53:24", rId);
 	if(cList.size()>0){
 		customerpurchases CList = cList.get(0);
 	%>

<div class="vender-details">
<div style="display: none;color: red;" id="warMsg" >Sorry,this customer unable to donate the amount because of he is already donated amount in this year with this same details.</div>
<form name="tablets_Update" id="tablets_Update" method="post" action="ticketDetails_UpdateOfferKind.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>Date</td>
	<td>Receipt No</td>
	<td>Head of account</td>
	<td colspan="1">
		<!-- <div class="warning" id="poojaError" style="display: none;">Please select a pooja.</div> -->
		SEVA Name <span style="color: red;">*</span></td>
	<%if(CList.getcash_type() != null && !CList.getcash_type().equals("") && CList.getcash_type().equals("offerKind")){%>
	<td><div id="offerkindn">Offer Kind No</div></td>
	<%}%>
	
</tr>
<tr>
	<td><input type="text" name="curentdate" value="<%=sd.format(Calendar.getInstance().getTime())%>" readonly="readonly"/></td>
	<td><input type="text" id="receiptNo" name="receiptNo" value="<%=CList.getbillingId()%>" readonly="readonly"></td>
	<td colspan="1">
	<select name="headId" id="headId"  autofocus="autofocus">
		<option value="4" <%if(CList.getextra1().equals("4")){%>selected<%}else{%>disabled<%}%>>SANSTHAN</option>
		<option value="1" <%if(CList.getextra1().equals("1")){%>selected<%}else{%>disabled<%}%>>CHARITY</option>
		<option value="5" <%if(CList.getextra1().equals("5")){%>selected<%}else{%>disabled<%}%>>SAI NIVAS</option>
		<option value="3" <%if(CList.getextra1().equals("3")){%>selected<%}else{%>disabled<%}%>>POOJA STORES</option>
	</select>
	</td>
<td>
<%
String pName = "";
pName = pl.getProductsNameByCats(CList.getproductId(), CList.getextra1());
if(pName.trim().equals("")){
	pName = pl.getProductsNameByCats(CList.getproductId(), "");
}
%>
<input type="text" name="productId" id="productId" value="<%=CList.getproductId()%> <%=pName%>" readonly>
</td>
<%if(CList.getcash_type() != null && !CList.getcash_type().equals("") && CList.getcash_type().equals("offerKind")){%>
<td><input type="text" name="offerKind" id="offerKind" value="<%=CList.getofferkind_refid()%>" readonly></td>
<%}%>
</tr>
<%
String uId = CList.getextra15();
List sList = srl.getMsssst_registrations(uId);
sssst_registrations SList = new sssst_registrations();
if(sList.size()>0){SList = (sssst_registrations)sList.get(0);}
%>
<tr>
<td>Mobile Number</td>
<td>Unique Id</td>
</tr>
<tr>
	<td><input type="text" name="mobile" id="mobile" value="<%=CList.getphoneno()%>" placeholder="Enter mobile number"></td>
	<td><input type="text" name="uniqueid" id="uniqueid" value="<%=SList.getsssst_id()%>" readonly/>
</tr>
<tr>
<!-- <td> -->
<div class="warning" id="QualificationError" style="display: none;">Please Provide  "Qualification".</div>
<!-- Mobile Number</td> -->
<td><div class="warning" id="nameErr" style="display: none;">Please Provide  "First name".</div>First Name <span style="color: red;">*</span></td>
<td>Gothram</td>
<td>Address</td>
<td>Email Id</td>
</tr>
<tr>
<td><input type="text" name="customerName" id="customerName" value="<%=CList.getcustomername()%>" placeholder="Enter full name"></td>
<td><input type="text" name="gothram" id="gothram" value="<%=CList.getgothram()%>" placeholder="Enter gothram"></td>
<td><input type="text" name="address" id="address" value="<%=CList.getextra6()%>" placeholder="Enter Your Address" /></td>
<td><input type="text" name="emailid" id="emailid" value="<%=CList.getextra8()%>" placeholder="Enter Your Email Id"></td>
</tr>
<tr>
<!-- <td>CurrencyType
<div class="warning" id="currencyError" style="display: none;">Please select USD/POUND.</div>
</td>
<td>Payment Type
<div class="warning" id="othercashError" style="display: none;">Please select a other cash.</div>
</td> -->
<%if(CList.getcash_type() != null && !CList.getcash_type().equals("") && CList.getcash_type().equals("othercash")){%>
<td colspan="1" id="othercashheading">Other Cash Payment Type</td>
<%}%>
<td>Qty</td>
<td>
<div class="warning" id="amtError" style="display: none;"  >Please enter amount.</div>
Amount <span style="color: red;">*</span></td>
<td colspan="1">Gross Amount</td>
</tr>
<tr>
<%-- <td>
	<select name="currencyType" id="currencyType">
		<option value="RS" <%if(CList.getextra5().equals("RS")){%>selected<%}else{%>disabled<%}%>>&#x20B9; - Rupees </option>
		<option value="USD" <%if(CList.getextra5().equals("USD")){%>selected<%}else{%>disabled<%}%>>$ - USD </option>
		<option value="POUND" <%if(CList.getextra5().equals("POUND")){%>selected<%}else{%>disabled<%}%>>&pound; - POUND </option>
	</select>
</td> --%>
<%-- <td>
	<select name="paymentType" id="paymentType" onchange="makereadonly()">
		<option value="cash" <%if(CList.getcash_type().equals("cash")){%>selected<%}else{%>disabled<%}%>>CASH </option>
		<option value="cheque" <%if(CList.getcash_type().equals("cheque")){%>selected<%}else{%>disabled<%}%>>CHEQUE</option>
		<option value="othercash" <%if(CList.getcash_type().equals("othercash")){%>selected<%}else{%>disabled<%}%>>OTHER CASH</option>
		<option value="card" <%if(CList.getcash_type().equals("card")){%>selected<%}else{%>disabled<%}%>>CARD</option>
	</select>
</td> --%>
<%-- <%if(CList.getcash_type() != null && !CList.getcash_type().equals("") && CList.getcash_type().equals("othercash")){%>
<td id="otherCashPaymentType" colspan="1">
	<select name="otherCashPaymentType" id="otherCashPaymentTypeVal">
		<option value="othercash-cash" <%if(CList.getcash_type().equals("othercash") && CList.getExtra19().equals("othercash-cash")){%>selected<%}else{%>disabled<%}%>>CASH </option>
		<option value="othercash-cheque" <%if(CList.getcash_type().equals("othercash") && CList.getExtra19().equals("othercash-cheque-cash")){%>selected<%}else{%>disabled<%}%>>CHEQUE</option>
	</select>
</td>
<%}%> --%>
<td><input type="text" name="ticQty" id="ticQty" value="<%=CList.getquantity()%>" readonly></td>
<td><input type="text" name="Amount" id="Amount" value="<%=CList.getrate()%>" readonly/></td>
<td><input type="text" name="totAmt" id="totAmt" value="<%=CList.gettotalAmmount()%>" readonly>
<div id="amountinwords" style="text-align: center;font-size: 12px;color: #F00;"></div></td>
</tr>
<tr>
	<%-- <td colspan="1" <%if(!CList.getcash_type().equals("cheque") || (CList.getcash_type().equals("othercash") && CList.getExtra19().equals("othercash-cash"))){%>style="display: none;"<%}%> id="chequeDetails1">Cheque No<span style="color: red;">*</span></td>
	<td colspan="1" <%if(!CList.getcash_type().equals("cheque")){%>style="display: none;"<%}%> id="chequeDetails2">Name On Cheque<span style="color: red;">*</span></td> --%>
	<td>Purpose</td>
	<!-- <td >From date</td>
	<td>To date</td>
	<td>Pancard Number</td> -->
</tr>
<%-- <%
String fromdate = "";
String todate = "";
if(CList.getextra3() != null && !CList.getextra3().equals("")){fromdate = sd.format(sd.parse(CList.getextra3()));}
if(CList.getextra3() != null && !CList.getextra4().equals("")){todate = sd.format(sd.parse(CList.getextra4()));}
%> --%>
<tr>
<%-- <td colspan="1" <%if(!CList.getcash_type().equals("cheque") || (CList.getcash_type().equals("othercash") && CList.getExtra19().equals("othercash-cash"))){%>style="display: none;"<%}%> id="chequeDetails3"><input type="text" name="chequeNo" id="chequeNo" value="<%=CList.getcheque_no()%>" readonly/></td>
<td colspan="1" <%if(!CList.getcash_type().equals("cheque")){%>style="display: none;"<%}%> id="chequeDetails4"><input type="text" name="nameOnCheque" id="nameOnCheque" value="<%=CList.getname_on_cheque()%>" readonly></td> --%>
<td><input type="text" name="purpose" id="purpose" value="<%=CList.getextra2()%>" onblur="javascript:this.value=this.value.toUpperCase();" placeholder="Enter purpose"></td>
<%-- <td><input type="text" name="fromdate" id="fromdate" value="<%=fromdate%>" style="float: left;width: 110px;" readonly="readonly"></td>
<td><input type="text" name="todate" id="todate" value="<%=todate%>" style="float: left;width: 110px;"  readonly="readonly"></td> --%>
<%-- <td>
	<div class="warning" id="panLenError" style="display: none;">PAN Number length must be 10 digists.</div>
	<div class="warning" id="panError" style="display: none;">Please enter pan card number.</div><input type="text" name="pancardNo" id="pancardNo"  maxlength="10" value="<%=CList.getExtra18()%>">
	</td> --%>
</tr>
<tr>
<td colspan="5">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="appl" style="display: none;">
	<tr>
	<td>Application Ref No</td>
	<!-- <td>Title</td> -->
	<td>Performed in the Name of</td>
	<td>Performed Date</td>
	<td colspan="2">Special Days</td>
	</tr>
	<tr>
	<td><input type="text" name="AppRefNo" placeHolder="Enter Application reference no"></input></td>
	<td><input type="text" name="lastname" value="" onBlur="javascript:this.value=this.value.toUpperCase();"></td>
	<td width="30">
	<select name="performedMonth" style="width: 85px;">
	<option value="">Month</option>
	<option value="01">JAN</option>
	<option value="02">FEB</option>
	<option value="03">MAR</option>
	<option value="04">APR</option>
	<option value="05">MAY</option>
	<option value="06">JUN</option>
	<option value="07">JUL</option>
	<option value="08">AUG</option>
	<option value="09">SEP</option>
	<option value="10">OCT</option>
	<option value="11">NOV</option>
	<option value="12">DEC</option>
	</select>
	<select name="performedDay" style="width: 85px;">
	<option value="">Day</option>
	<%for(int i=01;i<32;i++){ 
	if(i < 10){%>
	
	<option value="0<%=i%>">0<%=i%></option>
	<%}else{ %>
	<option value="<%=i%>"><%=i%></option>
	<%} }%>
	</select>
	</td>
	
	<td colspan="2" style="border: 1px solid #000;"><input type="checkbox" name="specialDays" value="Guru poornima">Guru poornima
	<input type="checkbox" name="specialDays" value="Dasara">Dasara
	<input type="checkbox" name="specialDays" value="Sri Rama navami">Sri Rama navami<br>
	<input type="checkbox" name="specialDays" value="Datta jayanthi">Datta jayanthi</td>
	
	<!-- <td> <input type="text" name="gothram" value=""> </td> -->
	</tr>
	<tr>
	<td>Address</td>
	<td>Address2</td>
	<td>Address3</td>
	<td>City</td>
	<td>Pin code</td>
	<td></td>
	</tr>
	<tr>
	<td><input type="text" name="addressSj" id="addressSj" value="" onblur="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input type="text" name="address2" id="address2" value="" onblur="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input type="text" name="address3" id="address3" value="" onblur="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input type="text" name="city" id="city" value="" onblur="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input type="text" name="pincode" id="pincode" value=""></td>
	<td></td>
	</tr>
	<tr>
	<td>Date of Birth</td>
	<td>Date Of Anniversary</td>
	<!-- <td>Pancard Number<span style="color: red;">*</span></td> -->
	
	</tr>
	<tr>
	<td><input type="text" name="dob"  class="datepickernew" value="" readonly="readonly" style="width: 80px;">
	 </td>
	<td width="30">
	<select name="doamonth" style="width: 85px;">
	<option value="">Month</option>
	<option value="01">JAN</option>
	<option value="02">FEB</option>
	<option value="03">MAR</option>
	<option value="04">APR</option>
	<option value="05">MAY</option>
	<option value="06">JUN</option>
	<option value="07">JUL</option>
	<option value="08">AUG</option>
	<option value="09">SEP</option>
	<option value="10">OCT</option>
	<option value="11">NOV</option>
	<option value="12">DEC</option>
	</select>
	<select name="doadate" style="width: 85px;">
	<option value="">Day</option>
	<%for(int i=01;i<32;i++){ 
	if(i < 10){%>
	
	<option value="0<%=i%>">0<%=i%></option>
	<%}else{ %>
	<option value="<%=i%>"><%=i%></option>
	<%} }%>
	</select></td> 
	</tr>
	</table>
</td>
</tr>
<%if(CList.getcash_type().equals("card")){%>
<tr>
	<td colspan="1" id="fd1">Last Four Digits On Card</td>
	<td colspan="1" id="an1">Invoice Number</td>
</tr>
<tr>
<td colspan="1" id="fd2"><div class="warning" id="fdError" style="display: none;">Please Enter Card Last Four Digits.</div><input type="text" name="fourDigits" id="fourDigits" onKeyPress="return numbersonly(this, event,true);" value="<%=CList.getextra10()%>" style="float: left;width: 110px;"></td>
<td colspan="1" id="an2"><div class="warning" id="anError" style="display: none;">Please Enter Authentication Number.</div><input type="text" name="authNum" id="authNum" value="<%=CList.getextra14()%>" style="float: left;;width: 150px;"></td>
</tr>
<%}%>
<tr > 
<td colspan="2" align="right"></td>
<td colspan="1" align="right"><input type="submit" name="saveonly" value="SAVE-ONLY" class="click" style="border:none;"/></td>
<td align="right" style="display: block;" id="save"><input type="submit" value="Save & Print" class="click"  style="border:none;"/></td>
<td></td>
</tr>
</table>
</form>
</div>
<%}else{%>
	<div style="color:red;text-align:center;">This receipt was not created in last one hour, so cannot be edited..!</div>
<%}%>
</div>
</div>
<%}}else{%>
	<div style="color:red;text-align:center;">No receipts were created in last one hour..!</div>
<%}}%>
<script>
window.onload=function a(){
	combochange('type','productId','getPoojas.jsp')
	};
	function print()
	{
		
	 if(document.getElementById("bid").value != 'null' && document.getElementById("seva").value == 'null' ){
		  newwindow=window.open('printReceipt.jsp?bid='+document.getElementById("bid").value+'&recName=sevaticket','name','height=600,width=500,menubar=yes,status=yes,scrollbars=yes'); 
				 /*newwindow=window.open('Pro_print.jsp?bid='+document.getElementById("bid").value); */
		  //newSJwindow=window.open('Devotee-Image-Capture.jsp?bid='+document.getElementById("bid").value,'nameSJ','height=600,width=650,menubar=yes,status=yes,scrollbars=yes'); 		
		if (window.focus) {newSJwindow.focus();}
	}else if(document.getElementById("bid").value != 'null' && document.getElementById("seva").value != 'null'){
		
		new2window=window.open('nithya-annadana-certificat.jsp?bid='+document.getElementById("bid").value+'&seva='+document.getElementById("seva").value,'name1','height=600,width=500,menubar=yes,status=yes,scrollbars=yes');
		newwindow=window.open('printReceipt.jsp?bid='+document.getElementById("bid").value+'&recName=sevaticket','name','height=300,width=450,menubar=yes,status=yes,scrollbars=yes'); 
		 /*newwindow=window.open('Pro_print.jsp?bid='+document.getElementById("bid").value);*/ 
		 //newSJwindow=window.open('Devotee-Image-Capture.jsp?bid='+document.getElementById("bid").value,'nameSJ','height=600,width=650,menubar=yes,status=yes,scrollbars=yes'); 		
		if (window.focus) {newSJwindow.focus();}
		} 
	}
	window.onload=print();
</script>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>