<%@page import="mainClasses.no_genaratorListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<title>Delivery Challan Entry</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
.clear {
	clear: both;
}
</style>
</head>
<script type="text/javascript">
var vendIds=" ";
$(function(){
	var dataMap;
	$('input[name="dc"]').change(function(){
		var dc = $(this).val();
		if (dc == "olddc")	{
			$('#vs').hide();
			$('#ss').show();
			$('#sh').show();
			$('#dc').show();
			$('#head').hide();
			$('#vd').empty();
			$('#newDc').hide();
			$('#pp').html("");
			var myOption = document.createElement("option");
			myOption.text = "Select Vendor";
			myOption.value = "";
			$('#vd').append(myOption);
			$.ajax({
				url : "searchProd.jsp",
				type : "POST",
				data : {key : "olddc"},
				success : function(response)	{
					if (Number(response) === 0)	{
						alert("There are no old dc's Exist");
						event.preventDefault();
					}
					else	{
						var data = response.trim().split("\n");
						dataMap = new Map();
						for (var i = 0 ; i < data.length ; i++)	{
							if ($.trim(data[i]) !== "")	{
								var d = data[i].split(",");
								$('#vd').append( $('<option>', {value: d[6],text: d[5] }) );
								dataMap.set(d[6],d[0]+","+d[1]+","+d[3]+","+d[4]+","+d[7]+","+d[8]);
								if (i == 0)	{
									$('#vho').append($('<option>', {value: d[7],text: d[8] }));
								}
							}
						}
					}
				}
			}); 
		}
		else if (dc == "newdc")	{
			$('#ss').hide();
			$('#sh').hide();
			$('#dc').hide();
			$('#vs').show();
			$('#head').show();
			$('#pp').html("New Dc Number:");
			$('#newDc').show();
				$.ajax({
					url : "searchVendor.jsp",
					type :"POST",
					data : {p : "xxx"},
					success : function(response)	{
					var ids = response.split("\n");
					for (var i = 0 ; i < ids.length ; i++)	{
						if ($.trim(ids[i]) !== "")	{
							ids[i] = ids[i].split(":")[0];
							vendIds += ids[i]+" ";
						}
					}
					}
				});
			}
		});
	
	$('#vd').change(function(){
		var vendor = $(this).val();
		if ($.trim(vendor) != "")	{
			$('#dcNumber').val(dataMap.get(vendor).split(",")[3]);
			$('#vho').empty();
			$('#vho').append($('<option>', {value: dataMap.get(vendor).split(",")[4],text: dataMap.get(vendor).split(",")[5] }));
		}
		else	{
			$('#dcNumber').val("");
			$('#vho').empty();
		}
	});

	$('#vendor').keypress(function(){
		var hoa = $('#hoa').val();
		if ($.trim(hoa) == "")	{
			alert("Please Select Head Of Account");
			document.getElementById('vendor').value = "";
			$('#hoa').focus();
		}
	});
	
	$('#btn').click(function(){
		var result = validateForm();
		$('#billtype').val($('input:radio:checked').val());
		if (result)	{
			$('#submitFrm').attr("action","<%=request.getContextPath()%>/DcEntry");
			$('#submitFrm').attr("method","POST");
			$('#submitFrm').submit();
		}
	});
	
	$('#mseNumber').val($('#mse').val());
	$('#newDc').val($('#dcNo').val());
});
</script>
<script>
function findProduct(index,event)	{
	var key = event.which; 
	if (key != 13 && key != 38 && key != 40)	{ // key codes list https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes 
		$('#mj').html("");
    	$('#mn').html("");
    	$('#sb').html("");
    	var data = $('#product'+index).val();
    	var type= $('input:radio:checked').val();
    	var	hoa = "";
    	if (type == "newdc")	{
    	hoa = $('#hoa').val();
    	
    	}
    	else	{
    	hoa = $('#vho').val();
    	}
    	
    	if($.trim(data) != "")	{
    		$.ajax({
    			url : "searchProd.jsp",
    			type : "POST",
    			data : {q:data,hId:hoa,key:"ss"},
    			success : function(response)	{
    				var products = new Array();
    				var prodmap = new Map();
    				var namesmap = new Map();
    				var data = response.trim().split("\n");
    				for (var i=0 ; i < data.length ; i++)	{
    					var d = data[i].split(",");
    					products.push(d[0]+" "+d[1]);
    					prodmap.set(d[0],d[3]+","+d[4]+","+d[5]);
    					namesmap.set(d[0],d[6]+","+d[7]+","+d[8]);
    				}
    				$('#product'+index).autocomplete({
    					source : products,
    					focus : function (event,ui)	{
    						event.preventDefault();
    						$(this).val(ui.item.label);
    					},
    					select : function (event,ui)	{
    						event.preventDefault();
    						$(this).val(ui.item.label);
    						var id = ui.item.value.split(" ")[0];
    						var names = namesmap.get(id);
    						var ids = prodmap.get(id); 
    						$('#mj').html(names.split(",")[0]);
    						$('#mn').html(names.split(",")[1]);
    						$('#sb').html(names.split(",")[2]);
    						$('#mjid'+index).val(ids.split(",")[0]);
    						$('#mnid'+index).val(ids.split(",")[1]);
    						$('#sid'+index).val(ids.split(",")[2]);
    					}

    				});
    			}
    		});
    	}
    	else	{
    		$('#qty'+index).val("0.00");
    		$('#rate'+index).val("0.00");
    		$('#total'+index).val("0.00");
    		calculateGross();
    	}
	}
}
function checkHeadOfAccount(index)	{
	var length = $('input:radio:checked').length;
	if (length == 0)	{
		$('#product'+index).val("");
		alert("please check Radio Button");
	}
	else	{
		var type = $('input:radio:checked').val();
		switch(type)	{
			case "olddc" : {
				var hoa = $('#vho').val();
				if (hoa == "")	{
					alert("Please Select HeadOfAccount");
					$('#vho').focus();
				}
			} break;
			case "newdc" : {
				var hoa = $('#hoa').val();
				if (hoa != "")	{
					var vendor = $('#vendor').val();
					if ($.trim(vendor) != "")	{
						
					}
					else {
						alert("Please Enter Vendor");
						$('#vendor').focus();
					}
				}
				else	{
					alert("Please Select HeadOfAccount");
					$('#hoa').focus();
				}
			} break;
		}
	}
}
function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;

    return true;
}
function validateForm()	{
 
 var type = $('input:radio:checked').val();
 var count = $('#addcount').val();
 var bill = $('#dcBill').val();
 var narration = $('#narr').val();
 if ($.trim(bill) == "") {
	 alert("Please Enter The bill Number");
	 $('#dcBill').focus();
	 return false;
 }
 if (type === "olddc")	{
	 var vendor = $('#vd').val();
	 if($.trim(vendor) === "")	{
		 alert("Please Select The Vendor");
		 $('#vd').focus();
		 return false;
	 }
	 var dc = $('#dcNumber').val();
	 if ($.trim(dc) === "")	{
		 alert("Dc Number is Empty");
		 return false;
	 }
 }
 if ($.trim(narration) === "")	{
	 alert("Please enter Description");
	 $('#narr').focus();
	 return false;
 }
 for (var i = 0 ; i < count ; i++)	{
		if(i == 0)	{
	  			var product = document.getElementById('product0').value;
	  			if(product.trim() != "")	{
	  	  			var qty = document.getElementById('qty0').value;
	  	  			if(qty.trim() == "")	{
	    				alert("Please Enter Qty");
	    				document.getElementById('qty0').focus();
	    				return false;
	    			}
	  	  			var rate = document.getElementById('rate0').value;
	  	  			if(Number(rate) <= 0 )	{
	  	  			alert("Please Enter Rate");
	  	  			document.getElementById('rate0').focus();
	  	  			return false;
	  	  			}
	  			}
	  			else	{
	  				alert("Please Enter Atleast one Product");
	  				document.getElementById('product0').focus();
	  				return false;
	  			}
		} // if
			var product = document.getElementById('product'+i).value;
  			if(product.trim() != "")	{
  	  			var qty = document.getElementById('qty'+i).value;
  	  			if(qty.trim() == "")	{
    					alert("Please Enter Qty");
    					document.getElementById('qty'+i).focus();
    					return false;
    			}
  	  			var rate = document.getElementById('rate'+i).value;
  	  			if(Number(rate) < 0 )	{
  	  			alert("Please Enter Rate");
  	  			document.getElementById('rate'+i).focus();
  	  			return false;
  	  			}
  			}
  return true;
 } // for
} // function

function checkAmount(index)	{
	var qty = $('#qty'+index).val();
	var rate = $('#rate'+index).val();
	if (Number(qty) <= 0)	{
		$('#qty'+index).focus();
		$('#rate'+index).val("")
	}
	var total = Number(qty)*Number(rate);
	$('#total'+index).val(total.toFixed(2));
	calculateGross();
}
function checkProduct(index)	{
	var product = $('#product'+index).val();
	if($.trim(product) == "")	{
		$('#qty'+index).val("");
		$('#product'+index).focus();
	}
}
function calculateGross()	{
	var count = $('#addcount').val();
	var total = 0;
	for (var i = 0 ; i < count ; i++)	{
	var	tot = $('#total'+i).val();
	total = Number(total) + Number(tot);	
	}
	$('#gtotal').val(total.toFixed(2));
}
function addRows()	{
  		var count =  document.getElementById("addcount").value;
  		var i=0,j=0;
  		i = count;
  		j = Number(count) + 5;
   		for(var i ;i < j ; i++)	{
   			//document.getElementById("addcolumn" + i).style.display = "block";
   			$('#addcolumn'+i).show();
  		}
   		document.getElementById("addcount").value = Number(Number(document.getElementById("addcount").value) + 5);
}

function getVendor(event)	{
	var key = event.which;
	if (key != 13 && key != 38 && key != 40)	{
	 var vendor = $('#vendor').val();
	 var hoa = $('#hoa').val();
		if ($.trim(vendor) != "")	{
			$.ajax({
				url : "searchVendor.jsp",
				type :"POST",
				data : {q : vendor,hoa : hoa},
				success : function(response)	{
					var data = response.trim().split("\n");
					var vendors = new Array();
					for(var i = 0;i < data.length; i++)	{
						var d = data[i].split(",");
						vendors.push(d[0]+" "+d[1]);
					}
					$('#vendor').autocomplete({
						source : vendors,
						focus : function (event,ui)	{
							event.preventDefault();
							$(this).val(ui.item.label);
						},
						select : function (event,ui)	{
							event.preventDefault();
							$(this).val(ui.item.label);
							var id = $(this).val();
							 if (vendIds.includes(id.split(" ")[0]))	{
								alert("Dc Already Exist For this Vendor");
								$('#vendor').val("");
							} 
						}
		});
	}
  });
}
}
}
</script>
<body>
	<%
		if(session.getAttribute("empId") != null){
	%>
	<div><%@ include file="title-bar.jsp"%></div>
	<div class="vendor-page">
		<div class="vendor-box">
			<form id="submitFrm">
				<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
					<tr>
						<td colspan="7" align="center" style="font-weight: bold;">
							SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr>
						<td colspan="7" align="center"
							style="font-weight: bold; font-size: 10px;">Regd.No.646/92</td>
					</tr>
					<tr>
						<td colspan="7" align="center"
							style="font-weight: bold; font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500
							060,Ph:24066566,24150184</td>
					</tr>
					<tr>
						<td colspan="7" align="center" style="font-weight: bold;">Delivery
							Challan Entry</td>
					</tr>
				</table>
				<div class="vender-details">

					<table>
						<tr>
							<td>Select One</td>
							<td><input type="radio" name="dc" id="olddc" value="olddc">OLD&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="dc" id="newdc" value="newdc">NEW
							</td>
						</tr>
						<tr>
							<td>MajorHead:</td>
							<td><span id="mj" style="font-size: 11px;"></span></td>
						</tr>
						<tr>
							<td>MinorHead:</td>
							<td><span id="mn" style="font-size: 11px;"></span></td>
						</tr>
						<tr>
							<td>SubHead:</td>
							<td><span id="sb" style="font-size: 11px;"></span></td>
						</tr>
						<tr>
							<td>BillNumber:</td>
							<td><input type="text" id="dcBill" name="dcBill"></td>
							<td>Narration:</td>
							<td><input type="text" id="narr" name="narr"></td>
							<td>MseNumber:</td>
							<td><input type="text" id="mseNumber" name="mseNumber"
								readonly="readonly"></td>
							<td><p id="pp">New Dc Number:</p></td>
							<td><input type="text" id="newDc" name="newDc"
								readonly="readonly"></td>
						</tr>
						<tr style="display: none;" id="ss">
							<td>Select Vendor</td>
							<td><select id="vd" name="oldvd">
									<option value="">Select Vendor</option>
							</select></td>
						</tr>
						<tr style="display: none;" id="dc">
							<td>Old DCNumber:</td>
							<td><input type="text" id="dcNumber" name="dcNumber"
								readonly="readonly"></td>
						</tr>
						<tr style="display: none;" id="sh">
							<td>Select HeadOfAccount</td>
							<td><select id="vho" name="vho">
							</select></td>
						</tr>
						<tr style="display: none;" id="head">
							<td>Select HeadOfAccount</td>
							<td><select id="hoa" name="hoa">
									<option value="">Select Head Of Account</option>
									<option value="1">CHARITY</option>
									<option value="4">SANSTHAN</option>
							</select></td>
						</tr>
						<tr style="display: none;" id="vs">
							<td>Enter Vendor</td>
							<td><input type="text" id="vendor" name="vendor"
								placeholder="Enter Vendor Name Here" style="width: 100%"
								onkeyup="getVendor(event);"></td>
						</tr>
					</table>
				</div>
				<div class="clear"></div>
				<%
					no_genaratorListing nogen_l=new no_genaratorListing();
				String mseInvId="",billId=null,dcNo="";
				if(session.getAttribute("headAccountId").toString().equals("5")){
					mseInvId=nogen_l.getidWithoutUpdatingNoGen("sainivas_mse_invId");
				}else{
					mseInvId=nogen_l.getidWithoutUpdatingNoGen("invoice");
				}
				dcNo=nogen_l.getidWithoutUpdatingNoGen("godownstock");
				if(request.getParameter("bid") != null && !request.getParameter("bid").equals("")) {
					billId = request.getParameter("bid");
				}
				%>
				<div class="table">
					<table class="table table-bordered"
						style="width: 64%; margin: 0 auto;">
						<thead>
							<tr>
								<th>S.no</th>
								<th>Product</th>
								<th>Quantity</th>
								<th>Rate</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<%
								for (int i = 0 ; i < 100 ; i++)	{
							%>
							<tr id="addcolumn<%=i%>" <%if(i > 4){%> style="display: none;"
								<%}%>>
								<td><%=(i+1)%><input type="hidden" name="rowcount"
									id="addcount" value="5" /></td>
								<td><input type="text" name="product<%=i%>"
									id="product<%=i%>" onkeyup="findProduct(<%=i%>,event)"
									onkeypress="checkHeadOfAccount(<%=i%>)"></td>
								<td><input type="text" name="qty<%=i%>" id="qty<%=i%>"
									onkeypress="javascript:return isNumber(event)"
									onkeyup="checkProduct(<%=i%>)" value="0.00"></td>
								<td><input type="text" name="rate<%=i%>" id="rate<%=i%>"
									onkeypress="javascript:return isNumber(event)"
									onkeyup="checkAmount(<%=i%>)" value="0.00"></td>
								<td><input type="text" name="total<%=i%>" id="total<%=i%>"
									readonly="readonly" value="0.00"></td>
								<input type="hidden" name="mjid<%=i%>" id="mjid<%=i%>">
								<input type="hidden" name="mnid<%=i%>" id="mnid<%=i%>">
								<input type="hidden" name="sid<%=i%>" id="sid<%=i%>">
							</tr>
							<%
								}
							%>
							<input type="hidden" name="vendor" id="vendor">
							<input type="hidden" name="billtype" id="billtype">
							<input type="hidden" id="mse" value="<%=mseInvId%>">
							<input type="hidden" id="dcNo" value="<%=dcNo%>">
							<input type="hidden" id="bid" name="bid" value="<%=billId%>">
							<tr>
								<td colspan="4">Total</td>
								<td><input type="text" name="gtotal" id="gtotal"
									readonly="readonly" value="0.00"></td>
							</tr>
							<tr>
								<td colspan="5"><input type="button" value="Submit"
									id="btn" class="click" style="display: block; margin: 0 auto;"></td>
							</tr>
							<tr>
								<td colspan="5"><input type="button" value="ADD MORE ROWS"
									onclick="addRows()" class="click"
									style="display: block; margin: 0 auto;"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</form>
			<script>
window.onload=function print()
	{ 
		if(document.getElementById("bid").value != 'null' && document.getElementById("bid").value != null){
			newwindow1=window.open('billimage_form.jsp?bid='+document.getElementById("bid").value,'BillImageUpload','height=300,width=800,menubar=yes,status=yes,scrollbars=yes');
			newwindow=window.open('printReceipt.jsp?bid='+document.getElementById("bid").value+'&recName=purchaseEntry','name','height=400,width=500,menubar=yes,status=yes,scrollbars=yes');
			if (window.focus) {newwindow.focus();}
	}
	}
</script>
		</div>
	</div>
	<%}
else {
	response.sendRedirect("index.jsp");
}
%>
</body>
</html>