<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="beans.patientsentry"%>
<%@page import="mainClasses.patientsentryListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SHOP SALES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!-- <script src="js/jquery-1.4.2.js"></script> -->
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){

	$('#PatientError ').hide();
	$('#addressError ').hide();
	$('#PhoneError ').hide();
	

	if($('#patientName').val().trim()==""){
		$('#PatientError').show();
		$('#patientName').focus();
		return false;
	}	

	if($('#address').val().trim()==""){
		$('#addressError').show();
		$('#address').focus();
		return false;
	}	

	if($('#phone').val().trim()==""){
		$('#PhoneError').show();
		$('#phone').focus();
		return false;
	}	
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="PAT" class="beans.patientsentry"/>
<jsp:useBean id="PET" class="beans.patientsentry"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Patient Registration <%if(request.getParameter("id")!=null) {%>Edit<%}%></div>
<div class="vender-details">
<%
patientsentryListing PATL = new mainClasses.patientsentryListing();
if(request.getParameter("id")!=null) {
	List PAT_List=PATL.getMpatientsentry(request.getParameter("id").toString());
	for(int i=0; i < PAT_List.size(); i++ ){
	PAT=(beans.patientsentry)PAT_List.get(i);
%>
<form name="tablets_Update" method="post" action="patientsentry_Update.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="patientId" id="patientId" value="<%=PAT.getpatientId()%>" />
<tr>
<td width="35%">
<div class="warning" id="PatientError" style="display: none;">Please Provide  "Patient Name".</div>
Patient Name*</td>
<td >
</td>
</tr>
<tr>
<td><input type="text" name="patientName" id="patientName" value="<%=PAT.getpatientName() %>" /></td>
<td></td>
</tr>
<tr>
<td>
<div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone No".</div>
Phone No*</td>
<td>Gender</td>
</tr>
<tr>
<td><input type="text" name="phone" id="phone" onKeyPress="return numbersonly(this, event,true);" value="<%=PAT.getphone() %>" /></td>
<td><input type="radio" name="extra1" value="Male" <%if(PAT.getextra1().equals("Male")){%>checked="checked" <%} %> />Male<input type="radio" name="extra1" value="Female" <%if(PAT.getextra1().equals("Female")){%>checked="checked" <%} %>/>Female</td>
</tr>

<tr>



<td colspan="2"><div class="warning" id="addressError" style="display: none;">Please Provide  "Address".</div>
<textarea name="address" id="address" placeholder="Enter Your Address*"><%=PAT.getaddress() %></textarea></td>
</tr>
<tr> 
<td></td>
<td align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<%} } else{%>
<form name="tablets_Update" method="post" action="patientsentry_Insert.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%">
<!-- <div class="warning" id="PatientError" style="display: none;">Please Provide  "Patient Name".</div> -->
Patient Name*</td>
<td >
</td>
</tr>
<tr>
<td><input type="text" name="patientName" id="patientName" value="" required placeholder="Enter patient name" onkeyup="javascript:this.value=this.value.toUpperCase();" /></td>
<td></td>
</tr>
<tr>
<td><!-- <div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone No".</div> -->
Phone No*</td>
<td>Age</td>
<td>Gender</td>
</tr>
<tr>
<td><input type="text" name="phone" id="phone" onKeyPress="return numbersonly(this, event,true);" value="" required placeholder="Enter patient phone number" /></td>
<td><input type="text" name="age" id="age" value="" placeHolder="Enter patient age"></input></td>
<td><input type="radio" name="gender" value="Male" checked="checked" />Male<input type="radio" name="gender" value="Female"/>Female</td>
</tr>

<tr>
<td colspan="2"><!-- <div class="warning" id="addressError" style="display: none;">Please Provide  "Address".</div> -->
<textarea name="address" id="address" placeholder="Enter patient Address*" required onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea></td>
</tr>
<tr> 
<td colspan="1" align="right"><input type="reset" value="RESET" class="click" style="border:none;"/></td>
<td align="right"><input type="submit" value="REGISTER" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
<%} %>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="clear"></div>
<form name="searchForm" id="searchForm" action="patientsList.jsp" method="post">
<div class="search-list">
	<ul>
		<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  readonly/></li>
		<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li> 
	<li><input type="submit" class="click" name="search" value="Search"></input></li>
	</ul>
</div>
</form>
<!-- <div class="arrow-down"><img src="images/Arrow-down.png"></div> -->
<!-- <div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div> -->
<div class="icons">
<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<!-- <span>
<ul>
<li><img src="images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl> 
</div>
</li> 
</ul>

</span> --></div>
<%
	if(request.getParameter("fromDate") != null && !request.getParameter("fromDate").equals("") && request.getParameter("toDate") != null && !request.getParameter("toDate").equals(""))
	{
		DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateFormat3= new SimpleDateFormat("dd-MMM-yyyy");
%>
<div class="list-details" id="tblExport">
<%
/* List PAT_L=PATL.getpatientsentry(); */
List PAT_L=PATL.getpatientsentryBetweenDates(request.getParameter("fromDate")+" 00:00:00",request.getParameter("toDate")+" 23:59:59");
if(PAT_L.size()>0){%>
      <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<table width="100%" cellpadding="0" cellspacing="0" >
<tr>
  <td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</td>
</tr>
<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<tr><td colspan="8" align="center" style="font-weight: bold;">Patients from <%=dateFormat3.format(dateFormat2.parse(request.getParameter("fromDate")))%> To <%=dateFormat3.format(dateFormat2.parse(request.getParameter("toDate")))%></td></tr>
<%-- <tr><td colspan = "8" style="text-align:center;font-size:18px;color:#65230d;;padding-bottom:10px;">Patients from <%=dateFormat3.format(dateFormat2.parse(request.getParameter("fromDate")))%> To <%=dateFormat3.format(dateFormat2.parse(request.getParameter("toDate")))%></td></tr> --%>
<tr><td colspan="8" ><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="5%" align="center">S.No.</td>
<td class="bg" width="20%" align="center">Patient Name</td>
<td class="bg" width="10%" align="center">Created Date</td>
<td class="bg" width="20%" align="center">Address</td>
<td class="bg" width="20%" align="center">Phone</td>
<td class="bg" width="15%" align="center">Gender</td>
<td class="bg" width="5%" align="center">Edit</td>
<td class="bg" width="5%" align="center">Delete</td>
</tr>
</table></td></tr>
<%for(int i=0; i < PAT_L.size(); i++ ){
	PET=(patientsentry)PAT_L.get(i); %>
<tr ><td width="5%" align="center"><%=i+1%></td>
<td width="20%" align=""><span><%=PET.getpatientName()%></span></td>
<td width="10%" align=""><span><%=dateFormat3.format(dateFormat.parse(PET.getextra2()))%></span></td>
<td width="20%" align=""><%=PET.getaddress()%></td>
<td width="20%" align="center"><%=PET.getphone()%></td>
<td width="15%" align="center"><%=PET.getextra1()%></td>
<td width="5%" align="center"><a href="patientsentry_Listing.jsp?id=<%=PET.getpatientId() %>">Edit</a></td>
<td width="5%" align="center"><a href="patientsentry_Delete.jsp?Id=<%=PET.getpatientId()%>">Delete</a></td>
</tr>
<%} %>

</table>
<%}else{%>
<div align="center"><h1>No Tablets Added Yet</h1></div>
<%}%>


</div><%}%>
</div>




</div>

</div>
<!-- main content -->


<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>