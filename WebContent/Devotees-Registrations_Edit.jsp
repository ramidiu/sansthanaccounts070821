<%@page import="mainClasses.uniqueIdGroupsListing"%>
<%@page import="helperClasses.DevoteeGroups"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.sssst_registrations" %>
<%@page import="mainClasses.sssst_registrationsListing" %>
<%@page import="beans.UniqueIdGroup" %>
<%@page import="mainClasses.uniqueIdGroupsListing" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DEVOTEE REGISTRATION EDIT|| SSSST,DSNR</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
});
function block(){
	 $.blockUI({ css: { 
	        border: 'none', 
	        padding: '15px', 
	        backgroundColor: '#000', 
	        '-webkit-border-radius': '10px', 
	        '-moz-border-radius': '10px', 
	        opacity: .5, 
	        color: '#fff' 
	    } });
}
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
</head>
<body>
<jsp:useBean id="SST" class="beans.sssst_registrations"></jsp:useBean>
<jsp:useBean id="UNG" class="beans.UniqueIdGroup"></jsp:useBean>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-box">

<div class="vender-details">
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
String sssstId = request.getParameter("bid");
sssst_registrationsListing sssst_l = new sssst_registrationsListing();
List sssst_List = sssst_l.getMsssst_registrations(sssstId); 
if(sssst_List.size() > 0){
SST = (sssst_registrations)sssst_List.get(0);
%>
<form  method="post" action="Devotees-Registrations_Update.jsp" enctype="multipart/form-data">
 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST PRO OFFICE </td>
	<%if(!SST.getimage().trim().equals("")){ %>
	<td style="padding-top:30px;"><a href="<%=request.getContextPath()%>/Devotee_Imgs/<%=SST.getimage() %>" target="_blank">
	<img src="<%=request.getContextPath()%>/Devotee_Imgs/<%=SST.getimage() %>" style="height: 100px;width: 120px;" /></a></td><%} %>
	</tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;">DEVOTEE REGISTRATION</td></tr>
</table>
<table width="70%" border="0" cellspacing="0" cellpadding="0">

<tr>
	<td>Sansthan Unique Id</td>
	<td>EMAIL-ID</td>
	<td colspan="1">MOBILE NO <span style="color: red;">*</span></td>
</tr>
<tr>
	<td><input type="text" name="receiptNo" value="<%=sssstId %>" readonly="readonly"></td>
	<td><input type="email" name="email-id" value="<%=SST.getemail_id() %>" placeHolder="Enter devotee email-id"></td>
	<td><input type="text" name="mobile" value="<%=SST.getmobile() %>" placeHolder="Enter devotee mobile number" required></td>
</tr>
<tr>
	<td>Title</td>
	<td>First Name<span style="color: red;">*</span></td>
	<td>Middle Name</td>
	<td>Last Name<span style="color: red;">*</span></td>
	<td>Spouse Name</td>
	<td>Date Of Anniversary</td>
</tr>
<tr>
	<td><select name="title">
	<option value="<%=SST.gettitle()%>"><%=SST.gettitle()%></option>
	<option value="Mr">Mr.</option>
	<option value="Miss">Miss.</option>
	<option value="Mr&miss">Mr&miss</option>
	</select></td>
	<td><input type="text" name="firstname" value="<%=SST.getfirst_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" required placeHolder="Enter devotee Firstname"></td>
	<td ><input type="text" name="middlename" value="<%=SST.getmiddle_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter devotee Middle name"></td>
	<td ><input type="text" name="lastname" value="<%=SST.getlast_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter devotee Last name"></td>
	<td ><input type="text" name="spousename" value="<%=SST.getspouce_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter devotee Spouse name"></td>
	<%if(SST.getDoa() != null && !SST.getDoa().trim().equals("") && !SST.getDoa().trim().equals("-")){ %>
	<%
	String DOAnniv = SST.getDoa();
	String[] tokens = DOAnniv.split("-");
	%>
	<td>
			<select name="doamonth" style="width: 85px;">
			<option value="">-- SELECT --</option>
					<option value="01" <% if(tokens[0].equals("01")){%>selected<% }%>>JAN</option>
					<option value="02" <% if(tokens[0].equals("02")){%>selected<% }%>>FEB</option>
					<option value="03" <% if(tokens[0].equals("03")){%>selected<% }%>>MAR</option>
					<option value="04" <% if(tokens[0].equals("04")){%>selected<% }%>>APR</option>
					<option value="05" <% if(tokens[0].equals("05")){%>selected<% }%>>MAY</option>
					<option value="06" <% if(tokens[0].equals("06")){%>selected<% }%>>JUN</option>
					<option value="07" <% if(tokens[0].equals("07")){%>selected<% }%>>JUL</option>
					<option value="08" <% if(tokens[0].equals("08")){%>selected<% }%>>AUG</option>
					<option value="09" <% if(tokens[0].equals("09")){%>selected<% }%>>SEP</option>
					<option value="10" <% if(tokens[0].equals("10")){%>selected<% }%>>OCT</option>
					<option value="11" <% if(tokens[0].equals("11")){%>selected<% }%>>NOV</option>
					<option value="12" <% if(tokens[0].equals("12")){%>selected<% }%>>DEC</option>
				</select>
				<select name="doadate" style="width: 85px;">
					<option value="<%=tokens[1]%>"><%=tokens[1]%></option>
						<%for(int i=01;i<32;i++){ 
							if(i < 10){%>
					<option value="0<%=i%>">0<%=i%></option>
							<%}else{ %>
					<option value="<%=i%>"><%=i%></option>
						<%} }%>
				</select>
			</td>
			<%}else{ %>
			<td >
			<select name="doamonth1" style="width: 85px;">
				<option value="">Month</option>
				<option value="01">JAN</option>
				<option value="02">FEB</option>
				<option value="03">MAR</option>
				<option value="04">APR</option>
				<option value="05">MAY</option>
				<option value="06">JUN</option>
				<option value="07">JUL</option>
				<option value="08">AUG</option>
				<option value="09">SEP</option>
				<option value="10">OCT</option>
				<option value="11">NOV</option>
				<option value="12">DEC</option>
			</select>
			<select name="doadate1" style="width: 85px;">
				<option value="">Day</option>
					<%for(int i=01;i<32;i++){ 
						if(i < 10){%>
				<option value="0<%=i%>">0<%=i%></option>
						<%}else{ %>
				<option value="<%=i%>"><%=i%></option>
					<%} }%>
			</select>
			</td>
			<%} %>
</tr>
<tr>
	<td>Gender</td>
	<td>Date of Birth</td>
	<td>Gothram</td>
	<td>Star</td>
	<td>Pancard Number</td>
	<td>Updates receive By</td>
</tr>
<tr>
	<td><input  type="radio" name="gender" value="male" <%if(SST.getgender().equals("male")) {%>checked="checked" <%} %>>Male
	 <input  type="radio" name="gender" value="female" <%if(SST.getgender().equals("female")){ %>checked="checked" <%} %>>Female</td>
	<td ><input  type="text" style="width: 105px;" name="dob" id="date" value="<%=SST.getdob() %>" placeHolder="Select Devotee Date of birth" readonly="readonly"></td>
	<td><input  type="text" name="gothram" value="<%=SST.getgothram() %>" placeHolder="Enter Devotee Gothram" onkeyup="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input  type="text" name="star" value="<%=SST.getextra3() %>" placeHolder="Enter Devotee Star" onkeyup="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input  type="text" name="pancard" value="<%=SST.getpancard_no() %>" placeHolder="Select Devotee pancard Number"></td>
	<td><input  type="radio" name="updates" value="email" <%if(SST.getupdates_receive_via().equals("email")) {%>checked="checked" <%} %>>Email
	<input  type="radio" name="updates" value="post" <%if(SST.getupdates_receive_via().equals("post")){ %>checked="checked" <%} %>>Post
	<input  type="radio" name="updates" value="mobile" <%if(SST.getupdates_receive_via().equals("mobile")){ %>checked="checked" <%} %>>SMS</td>
</tr>
<tr>
	<td>Address</td>
	<td>Address2</td>
	<td>Address3</td>
	<td>City</td>
	<td>State</td>
	<td>PinCode</td>
</tr>
<tr>
	<td><input type="text" name="address1" value="<%=SST.getaddress_1() %>" placeHolder="Enter address" required onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="address2" value="<%=SST.getaddress_2() %>" placeHolder="Enter address2" required onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="address3" value="<%=SST.getcountry() %>" placeHolder="Enter address3" required onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="city" value="<%=SST.getcity() %>" placeHolder="Enter city" required onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="state" value="<%=SST.getstate() %>" required placeHolder="Enter State" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="pincode" value="<%=SST.getpincode() %>" placeHolder="Enter PinCode" required onblur="javascript:this.value=this.value.toUpperCase();"/></td>
</tr>
<tr>
	<td>Identity Proof</td>
	<td>Identity Proof Number</td>
	<td>Narration</td>
</tr>
<tr>
	<td><select  name="idproof1" required>
		<option value="<%=SST.getidproof_1()%>"><%=SST.getidproof_1()%></option>
		<option value="UID AADHAAR NO">UID AADHAAR NO</option>
		<option value="VOTE ID NO">VOTE ID NO</option>
		<option value="PAN CARD NO">PAN CARD NO</option>
		<option value="PASSPORT NO">PASSPORT NO</option>
		<option value="RATION CARD NO">RATION CARD NO</option>
		<option value="DRIVING LICENSE NO">DRIVING LICENSE NO</option>
		<option value="OTHERS">OTHERS</option>
	 </select> </td>
	 <td><input type="text" name="idproof_num" value="<%=SST.getidproof1_num() %>" required placeHolder="Enter Identity proof number"></td>
	 <td><input  type="text" name="narration" value="<%=SST.getremarks() %>" placeHolder="Enter Narration" onblur="javascript:this.value=this.value.toUpperCase();"></td>
</tr>
<tr>
	<td>Photo Upload</td>
</tr>
<tr>
	<td><input name="devoteeImage" id="devoteeImage" type="file"/>
	<%=SST.getimage() %>
</td>
</tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr>
<td colspan="8" style="border:1px solid #000; padding:10px; margin-top:10px;
margin-bottom:10px;">
	<%
	uniqueIdGroupsListing ulist = new uniqueIdGroupsListing();
	List unique_List = ulist.getUniqueIdGroups(sssstId);
	UNG=(UniqueIdGroup)unique_List.get(0);
	%>
	 <label>
      <input type="checkbox" name="devoteeGroup" value="DONOR"<%if(UNG.getDonor().contains("DONOR")) {%> checked="checked"<%} %> />DONOR &nbsp;&nbsp;&nbsp;
     <input type="checkbox" name="devoteeGroup1" value="TRUSTEES"<%if(UNG.getTrusties().contains("TRUSTEES")) {%> checked="checked"<%} %> />TRUSTEES &nbsp;&nbsp;&nbsp;
     <input type="checkbox" name="devoteeGroup2" value="NAD"<%if(UNG.getNad().contains("NAD")) {%> checked="checked"<%} %> />NAD &nbsp;&nbsp;&nbsp;
     <input type="checkbox" name="devoteeGroup3" value="LTA"<%if(UNG.getLta().contains("LTA")) {%> checked="checked"<%} %> />LTA &nbsp;&nbsp;&nbsp;
     <input type="checkbox" name="devoteeGroup4" value="RDS"<%if(UNG.getRds().contains("RDS")) {%> checked="checked"<%} %> />RDS &nbsp;&nbsp;&nbsp;
     <input type="checkbox" name="devoteeGroup5" value="TIME_SHARE"<%if(UNG.getTimeshare().contains("TIME_SHARE")) {%> checked="checked"<%} %> />TIME_SHARE &nbsp;&nbsp;&nbsp;
     <%-- <input type="checkbox" name="devoteeGroup6" value="EMPLOYEE"<%if(UNG.getEmployee().contains("EMPLOYEE")) {%> checked="checked"<%} %> />EMPLOYEE &nbsp;&nbsp;&nbsp; --%>
     <input type="checkbox" name="devoteeGroup7" value="GENERAL"<%if(UNG.getGeneral().contains("GENERAL")) {%> checked="checked"<%} %> />GENERAL &nbsp;&nbsp;&nbsp;
     </label>
</td>

</tr>
<tr><td></td></tr>
<tr> 
<td colspan="4" align="right"></td> 
<td align="right" style="display: block;" id="save"><input type="submit" value="UPDATE" class="click"  style="border:none;"/></td>

</tr>

</table>
</form>
<%} %>
</div>
</div>
</div>
<%}else{
	response.sendRedirect("index.jsp");
	%>

<%} %>
</body>
</html>