<%@page import="java.util.List"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="beans.customerapplicationService"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DEVOTEES EDIT</title>
</head>
<body>
<jsp:useBean id="CUS" class="beans.customerpurchasesService"></jsp:useBean>
<jsp:useBean id="CAP" class="beans.customerapplicationService"></jsp:useBean>
<jsp:useBean id="DREG" class="beans.sssst_registrationsService"></jsp:useBean>
<%
String applictn=request.getParameter("applicatn");
DateFormat  dateFormat2= new SimpleDateFormat("MMMM");
DateFormat  dbdate1= new SimpleDateFormat("MM");
String bid=request.getParameter("bid");
String AppRefNo = request.getParameter("AppRefNo");
String customername=request.getParameter("customerName").replace("'", "");
String gothram=request.getParameter("gothram").replace("'", "");
String email=request.getParameter("emailid");
String address1=request.getParameter("address").replace("'", "");
String mobile=request.getParameter("mobile");
String performedDate="";
if(request.getParameter("performedMonth1")!=null && !request.getParameter("performedMonth1").equals("")  && request.getParameter("performedDay1")!=null && !request.getParameter("performedDay1").equals("")){
	 performedDate=request.getParameter("performedMonth1")+"-"+request.getParameter("performedDay1");
}

String lastname = request.getParameter("lastname");
/* if(request.getParameter("performedMonth")!=null && !request.getParameter("performedMonth").equals("")  && request.getParameter("performedDay")!=null && !request.getParameter("performedDay").equals("")) */
if(request.getParameter("performedMonth")!=null && request.getParameter("performedDay")!=null)
{
 	//System.out.println("date"+dbdate1.format(dateFormat2.parse(request.getParameter("performedMonth")))); 
 	//System.out.println("performedDay==>"+request.getParameter("performedDay"));
 	//System.out.println("performedMonth==>"+request.getParameter("performedMonth"));
 	
	//performedDate = dbdate1.format(dateFormat2.parse(request.getParameter("performedMonth")))+"-"+request.getParameter("performedDay");
	performedDate = request.getParameter("performedMonth")+"-"+request.getParameter("performedDay");
	//System.out.println("performedDate==>"+performedDate);
}
String address2=request.getParameter("address2").replace("'", "");
String address3=request.getParameter("address3").replace("'", "");
String city=request.getParameter("city");
String pincode=request.getParameter("pincode");
String landline=request.getParameter("landline");

String dob=request.getParameter("dob");

String doa="";
if(request.getParameter("doamonth1")!=null && !request.getParameter("doamonth1").equals("")  && request.getParameter("doadate1")!=null && !request.getParameter("doadate1").equals("")){
	doa=request.getParameter("doamonth1")+"-"+request.getParameter("doadate1");
}
if(request.getParameter("doamonth")!=null && request.getParameter("doadate")!=null)
{
	doa = request.getParameter("doamonth")+"-"+request.getParameter("doadate");
}

String specialDays[]=new String[5];
String days="";
if(request.getParameter("specialDays")!=null){
	specialDays=request.getParameterValues("specialDays");
	if(specialDays.length>0){
		for(int s=0;s<specialDays.length;s++){
			days=days+","+specialDays[s];
		}
	}
}
else{
	days = ",";
}
mainClasses.customerapplicationListing CAPL=new mainClasses.customerapplicationListing();
List CAP_DET=CAPL.getMcustomerDetailsBasedOnBillingId(bid);
%>
<jsp:setProperty name="CUS" property="billingId" value="<%=bid%>"/>
<jsp:setProperty name="CUS" property="extra6" value="<%=address1%>"/>
<jsp:setProperty name="CUS" property="phoneno" value="<%=mobile%>"/>
<jsp:setProperty name="CUS" property="customername" value="<%=customername%>"/>
<jsp:setProperty name="CUS" property="gothram" value="<%=gothram%>"/>
<jsp:setProperty name="CUS" property="extra7" value="<%=days%>"/>
<jsp:setProperty name="CUS" property="extra8" value="<%=email%>"/>
<jsp:setProperty name="CUS" property="extra20" value="<%=landline%>"/>		<!-- landline -->
<%=CUS.update()%>
	<jsp:setProperty name="CAP" property="billingId" value="<%=bid%>"/>
	<jsp:setProperty name="CAP" property="first_name" value="<%=customername%>"/>
	<jsp:setProperty name="CAP" property="phone" value="<%=AppRefNo%>"/>
	<jsp:setProperty name="CAP" property="gothram" value="<%=gothram%>"/>
	<jsp:setProperty name="CAP" property="email_id" value="<%=email%>"/>
	<jsp:setProperty name="CAP" property="address1" value="<%=address1%>"/>
	<jsp:setProperty name="CAP" property="extra1" value="<%=performedDate%>"/>	
	<jsp:setProperty name="CAP" property="last_name" value="<%=lastname%>"/>
	<jsp:setProperty name="CAP" property="extra2" value="<%=address2%>"/>
	<jsp:setProperty name="CAP" property="address2" value="<%=address3%>"/>
	<jsp:setProperty name="CAP" property="extra3" value="<%=city%>"/>
	<jsp:setProperty name="CAP" property="pincode" value="<%=pincode%>"/>
	
	<jsp:setProperty name="CAP" property="middle_name" value="<%=doa%>"/>
	<jsp:setProperty name="CAP" property="dateof_birth" value="<%=dob%>"/>
<%if(CAP_DET.size()>0){ %>
	<%=CAP.update() %>
	<%}else{ %>
	<%=CAP.insert()%>
	<%} %>
	
	<%
	mainClasses.customerpurchasesListing CUSL=new mainClasses.customerpurchasesListing();
	List CUS_DET=CUSL.getBillDetails(bid);
	customerpurchases custp = new customerpurchases();
	custp = (customerpurchases)CUS_DET.get(0);
	String uniqueId = custp.getextra15(); 
	if(uniqueId.contains("SSSST"))
	{%>
		<jsp:setProperty property="dob" name="DREG" value="<%=dob%>"/>
		<jsp:setProperty property="doa" name="DREG" value="<%=doa%>"/>
		<jsp:setProperty property="sssst_id" name="DREG" value="<%=uniqueId%>"/>
		<%=DREG.update() %>
	<% }
	%>
	<%response.sendRedirect("lifetimeArchana-nityaAnnadanamDevotees-search.jsp"); %>
</body>
</html>