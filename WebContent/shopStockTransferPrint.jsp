<%@page import="beans.shopstock"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.*"%>

<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SHOP SALES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="SHS" class="beans.shopstock" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<%-- <form action="shopTransferList.jsp" method="post">
				<div class="search-list">
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form> --%>
				<div class="icons">
				<a id="print"><span><img src="images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
								<!-- <a ><span><img src="images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> -->
						 <span><a id="btnExport" href="#Export to excel"><img
						src="images/excel.png" style="margin: 0 20px 0 0"
						title="export to excel" /></span> <span>
					<!-- 	<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul> -->

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%
	shopstockListing  SSK_L=new shopstockListing();
	productsListing PRD_L=new productsListing();
	employeesListing EMP_L=new employeesListing();
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");	
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat dF2= new SimpleDateFormat("dd-MM-yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+"";
	String toDate=currentDate+"";
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate");
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate");
	}
	List SHOPTR_list=SSK_L.getTransferReportBasedonStockTransaferInvoiceId(request.getParameter("stinvID"));
	System.out.println("SHOPTR_list....."+SHOPTR_list.size());
	mainClasses.employeesListing EMP_l=new mainClasses.employeesListing();
	String EMP_HOA=EMP_l.getMemployeesHOA(session.getAttribute("empId").toString());
	System.out.println("EMP_HOA====> "+EMP_HOA);
	headofaccountsListing HOAL=new headofaccountsListing();
	double totalAmount=0.00;
	DecimalFormat df = new DecimalFormat("0.00");
	if(SHOPTR_list.size()>0){%>
	<div class="printable">
	
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
					<tr>
						<td colspan="6" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%=HOAL.getHeadofAccountName(SSK_L.getHeadOfAccount(request.getParameter("stinvID"))) %>  </td>
					</tr>
					<tr><td colspan="6" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="6" align="center" style="font-weight: bold;">STOCK ISSUE</td></tr>
					<tr><td colspan="3" align="left">Voucher No : <%=request.getParameter("stinvID") %></td><td colspan="3" align="right">Date : <%=dF2.format(dateFormat2.parse(SSK_L.getDate(request.getParameter("stinvID")))) %></td></tr>
					<tr>
					<td colspan="6">
					<table width="100%" cellpadding="0" cellspacing="0" border="1">
					<tr>
							<td class="bg" width="5%" style="font-weight: bold;" align="center">S.NO.</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="center">FROM GODOWN</td>
							<td class="bg" width="40%" style="font-weight: bold;" align="right">PRODUCT NAME</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">QUANTITY</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">RATE</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">TOTAL</td>
						</tr>
						<%for(int i=0;i<SHOPTR_list.size();i++){
						SHS=(shopstock)SHOPTR_list.get(i); 
						mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
						String id=SHS.getextra1();
						%>
						<tr>
							<td align="center"><%=i+1 %></td>
							<td align="center"><%if(EMP_HOA.equals("3")){ %>To Shop <%}else if(EMP_HOA.equals("5")) {  %> To Restaurant <%}
					         else if(id.equals("4")){ %>To kitchen(Santhan) <%}
					         else if(id.equals("1")) {  %> To  kitchen(Charity) <%}
					         else {%>
							  TO kitchen <%} %></td>
							<td align="left"><%=SHS.getproductId()%>-<%=PRD_L.getProductsNameByCat1(SHS.getproductId(),"")%></td>
							<td align="center"><%=SHS.getquantity()%></td>
							<td align="right"><%=PRD_L.getSellingRate(SHS.getproductId()) %></td>
							<td align="right"><%=df.format(Double.parseDouble(SHS.getquantity())*Double.parseDouble(PRD_L.getSellingRate(SHS.getproductId()))) %></td>
						</tr>
		<%} %>
		</table>
		</td>
		</tr>
		<tr style="height: 30px;"></tr>
		<tr><td colspan="4" align="left" style="font-weight: bold;">Receiver</td><td colspan="1" align="left" style="font-weight: bold;">Issuer </td> <td></td> </tr>
		<tr><td colspan="4" align="left" style="font-weight: bold;">Signature : </td><td colspan="1" align="left" style="font-weight: bold;">Signature : </td> <td></td></tr>
		<tr><td colspan="4" align="left" style="font-weight: bold;">Name : <%=SHS.getextra4() %></td><td colspan="1" align="left" style="font-weight: bold;">Name :  <%=EMP_L.getMemployeesName(SHS.getemp_id()) %></td><td></td> </tr>
		<tr><td colspan="4" align="left" style="font-weight: bold;">Designation : <%=SHS.getextra5() %></td><td colspan="1" align="left" style="font-weight: bold;">Designation : <%=EMP_L.getMemployeesRole(SHS.getemp_id()) %></td> <td></td></tr>
		<tr><td colspan="6" align="center"></td></tr>
		<tr><td colspan="6" align="center">Certified</td></tr>
		<tr><td colspan="6" align="center"></td></tr>
		<tr><td colspan="6" align="center">(Manager)</td></tr>	
			</table>
		</div>
			<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There are no transfer list found!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>