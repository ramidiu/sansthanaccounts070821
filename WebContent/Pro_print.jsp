<%@page import="java.util.Calendar"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<style>
.main_shawls{width:1500px;float:left;l}
table.shawls-sales{margin:0;padding:0;}
table.shawls-sales{margin-top:415px;width:100%;}
table.shawls-sales td{padding:15px 0; }
table.shawls-sales td span.text-cnt{font-size:24px;color:#000;text-transform:uppercase !important ;line-height:30px;}
table.shawls-sales td span.digit-cnt{font-size:24px;color:#000;line-height:30px;}
table.shawls-sales td span.clmn1-lft-mrg{margin-left:300px !important;}
</style>
<jsp:useBean id="CSP" class="beans.customerpurchases"></jsp:useBean>
<body  onload="window.print();">
<%
String HOAID=session.getAttribute("headAccountId").toString();
if(request.getParameter("bid")!=null && !request.getParameter("bid").equals("")){
	Calendar c1 = Calendar.getInstance(); 
	c1.setTimeZone(TimeZone.getTimeZone("IST"));
mainClasses.subheadListing SUBHL=new mainClasses.subheadListing();
NumberToWordsConverter NTW=new NumberToWordsConverter();
mainClasses.customerpurchasesListing CUSPL=new mainClasses.customerpurchasesListing();
String billingId=request.getParameter("bid");
List TICDET=CUSPL.getBillDetails(billingId);
if(TICDET.size()>0){ 
	CSP=(beans.customerpurchases)TICDET.get(0);
}
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat  dateFormat= new SimpleDateFormat("dd/MM/yyyy");
DateFormat  dbdate= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
%>
<div class="main_shawls" onload="window.print();">
<table class="shawls-sales" cellpadding="0" cellspacing="0" border="0">
<tr><td colspan="2"  align="center"><span class="text-cnt " ><%=SUBHL.getMsubheadnames(CSP.getproductId(),HOAID)%></span></td>  </tr>
<tr><td style="width:995px;"><span class="digit-cnt clmn1-lft-mrg"><%=request.getParameter("bid") %></span></td><td style="width:15%"><span class="digit-cnt "><%=dateFormat.format(c1.getTime()) %></span></td></tr>
<tr><td><span class="text-cnt clmn1-lft-mrg" style="margin-left:525px !important;"><%=CSP.getcustomername() %></span></td><td><%if(!CSP.getgothram().trim().equals("")){ %><%=CSP.getgothram() %><%} %></td></tr>
<tr><td><span class="text-cnt clmn1-lft-mrg" ><%=NTW.convert(Integer.parseInt(CSP.gettotalAmmount())) %></span></td><td><%if(!CSP.getextra3().equals("")){ %><%=dateFormat.format(dbdate.parse(CSP.getextra3()))%><%} %></td></tr>
<tr><td><span class="text-cnt clmn1-lft-mrg"><%=SUBHL.getMsubheadnames(CSP.getproductId(),HOAID)%></span></td><td><%if(!CSP.getextra4().equals("")){%><%=dateFormat.format(dbdate.parse(CSP.getextra4()))%><%} %></td></tr>
<tr><td>&nbsp;&nbsp;</td><td></td></tr>
<tr><td><span class="digit-cnt clmn1-lft-mrg"><%if(CSP.getextra6()!=null && !CSP.getextra6().equals("")){ %><%=CSP.getextra6()%><%} %></span></td><td></td></tr>
<tr><td><span class="digit-cnt clmn1-lft-mrg"><%=CSP.gettotalAmmount()%></span></td><td></td></tr>
<tr><td><span class="digit-cnt clmn1-lft-mrg"></span></td><td></td></tr>
</table>
</div>
<%} %>

</body>
</html>