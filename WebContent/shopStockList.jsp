<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Stock List</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

function productsearch(){

	  var data1=$('#productname').val();
	  var headID="3";
	  $.post('searchProducts.jsp',{q:data1,hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
	</script>
	  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( ".printable" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SHP" class="beans.products" />
		<jsp:useBean id="PRD" class="beans.products" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form action="shopStockList.jsp" method="post">  
				<div class="search-list">
					<ul>
					<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text" name="productname" id="productname" <%if(request.getParameter("productname")!=null){ %> value="<%=request.getParameter("productname") %>" <%} %> onkeyup="productsearch()" placeholder="Find product here" required/></li>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
						<!-- <li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li> -->
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div>
				</form>
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate;
	String toDate=currentDate;
	shopstockListing SHPL=new shopstockListing();
	productsListing PRD_l=new productsListing ();
	DecimalFormat df = new DecimalFormat("0.00");
	if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate");
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate");
		}
	}
	customerpurchasesListing CUSPL=new customerpurchasesListing();
	String proID[]=null;
	double SHP_STOCKL=0.00;
	double SaleQtyTot=0.00;
	if(request.getParameter("productname")!=null && !request.getParameter("productname").equals("")){
		proID=request.getParameter("productname").split(" ");
		SHP_STOCKL=SHPL.getStockValuationReport(proID[0],fromDate,"");
		SaleQtyTot=CUSPL.getProductSaleQty1(proID[0],fromDate+" 00:00:01","");
	}
	double openingBal=SHP_STOCKL-SaleQtyTot;


	 DateFormatSymbols dfs = new DateFormatSymbols();
	 String[] months = dfs.getMonths();
	 int num=0;
	 String dt=fromDate;
	 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	 Calendar c = Calendar.getInstance();
	 c.setTime(sdf.parse(dt));
	 int month = c.get(Calendar.MONTH);
	 int month1=month;
	 int sno=1;
	// if(PRODL.size()>0){
		Date toodate = sdf.parse(toDate);
		Date frmdate=sdf.parse(fromDate);
		double closingBal=openingBal;
		double dayInward=0.00;
		double dayOutward=0.00;
		double totInward=0.00;
		double totOutward=0.00;
		if(request.getParameter("productname")!=null && !request.getParameter("productname").equals("")){
	%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 9px;
    z-index: 1;
    width:100%; 
    background:#d0e3fb; 
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	 <div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
                    <tr>
                    <td colspan="6">
                    <table width="100%" class="yourID">
						<tr><td colspan="6" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST POOJA STORES</td></tr>
						<tr><td colspan="6" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
						<tr><td colspan="2"></td><td  align="center" style="font-weight: bold;">Shop Stock Report</td><td colspan="3" align="center">Date : <%=currentDate %></td></tr>
						<tr align="center"><td colspan="6" class="bg" style="font-weight: bold;color: orange;font-size: 15px;"><%=request.getParameter("productname") %></td></tr>
						<tr><td colspan="4" align="right" class="bg" style="font-weight: bold;color: red;">Opening Balance</td><td class="bg" style="font-weight: bold;color: red;" colspan="2"><%=openingBal %></td></tr>
						
<tr>
							<td class="bg" width="5%" style="font-weight: bold;" align="center">S.NO</td>
							<td class="bg" width="15%" style="font-weight: bold;">DATE</td>
							<td class="bg" width="15%" style="font-weight: bold;">DAY OPENING QTY</td>
							<td class="bg" width="15%" style="font-weight: bold;" align="center">INWARDS</td>
							<td class="bg" width="15%" style="font-weight: bold;" align="center">OUTWARDS</td>
							<td class="bg" width="45%" style="font-weight: bold;" align="center">CLOSING QTY</td>
							
						
							</tr>
                            </table>
                            </td>
</tr>
						<% while(frmdate.compareTo(toodate)<=0){
							if(""+SHPL.getDayTransferQty(proID[0],dt)!=null){
								dayInward=SHPL.getDayTransferQty(proID[0],dt);
							}
							if(""+CUSPL.getDaySaleQty(proID[0],dt)!=null){
								dayOutward=CUSPL.getDaySaleQty(proID[0],dt);
							}
							totInward=totInward+dayInward;
							totOutward=totOutward+dayOutward;
							%>
						<tr>
							<td align="center" width="5%"><%=sno++%></td>
							<td width="15%"><%=dt %></td>
							<td width="15%"><%=closingBal %></td>
							<td align="center" width="15%"> <%=dayInward %> </td>
							<td align="center" width="15%"><span><%=dayOutward %></span></td>
							<%closingBal=closingBal-dayOutward+dayInward; %>
							<td align="center" width="45%"><%=closingBal %></td>
							
						</tr>
						<%
						 c.add(Calendar.DATE, 1);  // number of days to add
				 		 dt = sdf.format(c.getTime());
						 //c.setTime(sdf.parse(dt));
				 		 frmdate = sdf.parse(dt);		
						} %>
						<tr>
						<td class="bg" colspan="3" align="right">TOTAL</td>
						
						<td class="bg" align="center"><%=totInward %></td>
						<td class="bg" align="center"><%=totOutward %></td>
						
						<td class="bg" align="center" style="font: bold;"> <%=df.format(closingBal) %></td>
						<td></td>
						</tr>
					</table>
				</div><%} %>
				
					<%-- <%}else{%>
					<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no products found in shop!</span></div>
					<%}%> --%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
