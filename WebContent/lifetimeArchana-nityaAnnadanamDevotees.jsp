<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@media print{
   .print_table{
    width: 900px;
    border-collapse: collapse;
}
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
	
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
	}

</style>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRO OFFICE DAILY POOJAS LIST | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
function productsearch(){
	  var data1=$('#productId').val();
	  var headID=null;
	  $.post('searchSubhead.jsp',{q:data1,hoa:headID,page :'Rec'},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productId" ).autocomplete({source: availableTags}); 
			});
}
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                    $( "a" ).css("text-decoration","none");
                    $( ".printable" ).print();
                   

                    // Cancel click event.
                    return( false );
                });
			});
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
    $( "#subheadId" ).change(function() {
		  $( "#searchForm" ).submit();
		});
});

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="CAP" class="beans.customerapplication"/>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">
        <div class="clear"></div>
		<div class="vendor-title" style="padding:30px 30px 50px 20px;"> DEVOTEES LIST</div>
        
        
		<div class="vendor-list">
		
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form name="searchForm" id="searchForm" action="lifetimeArchana-nityaAnnadanamDevotees.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<select name="subheadId" id="subheadId">
							<option value="20092"<%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20092")){ %> selected="selected" <%} %>>NITYA ANNADANA PADHAKAM</option>
							<option value="20063" <%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20063")){ %> selected="selected" <%} %>>LIFE TIME ARCHANA MEMBERSHIP</option>
							
						</select>
						</li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					 <span>
						<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit Columns</dt>
										<dt><input type="checkbox" class="edit-setting" />Address</dt>
										<dt><input type="checkbox" class="edit-setting" />Email</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
		
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	productsListing PRDL=new productsListing();

	String subhead="20092";
	if(request.getParameter("subheadId")!=null && !request.getParameter("subheadId").equals("")){
		subhead=request.getParameter("subheadId");
	}
	String hoaID="4";
	customerapplicationListing CUSAL=new customerapplicationListing();
	List SAL_list=SALE_L.getDevoteesList(subhead);
	List CUS_DETAIL=null;
	DecimalFormat df = new DecimalFormat("0.00");
	if(SAL_list.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:97%; 
    background:#d0e3fb; 
}

</style>

<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
						
						<tr>
						<td class="bg" colspan="10" align="center">LIST OF DEVOTEES  FOR <%=PRDL.getProductsNameByCats(subhead,session.getAttribute("headAccountId").toString())%></td>
						</tr>
						<tr>
                        <td colspan="10" style="padding:0px;margin:0px" class="yourID"><table width="100%" class="print_table">
<tr>
							<td class="bg" width="4%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="6%" style="font-weight: bold;">APP REF.NO</td>
							<td class="bg" width="6%" style="font-weight: bold;">RECEIPT.NO</td>
							<td class="bg" width="14%" style="font-weight: bold;">NAME</td>
							<td class="bg" width="10%" style="font-weight: bold;">ADDRESS1</td>
							<td class="bg" width="8%" style="font-weight: bold;">ADDRESS2</td>
							<td class="bg" width="8%" style="font-weight: bold;">ADDRESS3</td>
							<td class="bg" width="8%" style="font-weight: bold;">CITY</td>
							<td class="bg" width="8%" style="font-weight: bold;">PINCODE</td>
							<td class="bg" width="15%" style="font-weight: bold;">SEVA PERFORM IN THE NAME OF</td>
							<td class="bg" width="9%" style="font-weight: bold;">GOTHRAM</td>
							<td class="bg" width="4%" style="font-weight: bold;">EDIT</td>
						</tr>

</table></td></tr>
						<%
					for(int i=0; i < SAL_list.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_list.get(i);
						
							CUS_DETAIL=CUSAL.getMcustomerDetailsBasedOnBillingId(SALE.getbillingId());
							if(CUS_DETAIL.size()>0){
							CAP=(customerapplication)CUS_DETAIL.get(0);}
							%>
						    <tr><td colspan="10" ><table width="100%"  class="print_table">
<tr>
							<td style="font-weight: bold;" width="4%"><input type="checkbox" name="check"/><%=i+1%></td>
							<td style="font-weight: bold;" width="6%"><a href="javascript:void(0)"><%=CAP.getphone()%></a></td>
							<td style="font-weight: bold;" width="6%"><a href="javascript:void(0)"><%=SALE.getbillingId()%></a></td>
							<td style="font-weight: bold;" width="14%"><a href="javascript:void(0)"><%=SALE.getcustomername()%></a></td>
							<td style="font-weight: bold;" width="10%" height="auto"><span><%=SALE.getextra6()%></span></td>
							<td style="font-weight: bold;" width="8%" height="auto"><span><%if(CUS_DETAIL.size()>0){%><%=CAP.getextra2()%><%}%></span></td>
							<td style="font-weight: bold;" width="8%" height="auto"><span><%if(CUS_DETAIL.size()>0){%><%=CAP.getaddress2()%><%}%></span></td>
							<td style="font-weight: bold;" width="8%" height="auto"><span><%if(CUS_DETAIL.size()>0){%><%=CAP.getextra3()%><%}%></span></td>
							<td style="font-weight: bold;" width="8%"><span><%if(CUS_DETAIL.size()>0){%><%=CAP.getpincode()%><%}%></span></td>
							<td style="font-weight: bold;" width="15%"><a href="javascript:void(0)"><%if(CUS_DETAIL.size()>0){%><%=CAP.getlast_name()%><%}%></a></td>
							<td style="font-weight: bold;" width="9%"><a href="javascript:void(0)"><%=SALE.getgothram()%></a></td>
							<td style="font-weight: bold;" width="4%"><a href="devoteesAddress_Edit.jsp?bid=<%=SALE.getbillingId()%>">Edit</a></td>
							
							</tr>
</table></td></tr>
							
					<%}%>
				
					</table>
					</div>
					<%
					}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no list found for today!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>