<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" /> 
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:92%; 
    background:#d0e3fb; 
}

@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}

	}


</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
</head>
<body>
<%if(session.getAttribute("empId")!=null){ 
	String searchuniqueid = request.getParameter("searchuniqueid");
	String searchbyname = request.getParameter("searchbyname");
	String searchbyphone = request.getParameter("searchbyphone");
	String searchbygothram = request.getParameter("searchbygothram");
	
%>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-list">
<form name="searchForm" id="searchForm" action="Devotees-Registrations-Search.jsp" method="post">
				<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td></td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold; padding-top: 10px;">Devotee's unique Registrations Search</td></tr>
				</table>
				<br/>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr style="margin-bottom:20px;">
				<td width="5%" class="bg">Unique-Id (SSSST)</td>
				<td width="10%" class="bg"><input type="text"
					name="searchuniqueid" id="searchuniqueid" value="" /></td>
				<td width="5%" class="bg">Name</td>	
				<td width="10%" class="bg"><input type="text"
					name="searchbyname" id="searchbyname" value="" /></td>
				<td width="5%" class="bg">Mobile No</td>	
				<td width="10%" class="bg"><input type="text"
					name="searchbyphone" id="searchbyphone" value="" /></td>
				<td width="5%" class="bg">Gothram</td>	
				<td width="10%" class="bg"><input type="text"
					name="searchbygothram" id="searchbygothram" value="" /></td>	
				<td width="10%" align="center"><input type="submit" name="Submit" value="Search" class="click"/></td>
					</tr>
</form>
<div class="icons">
	<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
	<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
	<%
	sssst_registrationsListing SSSSL = new sssst_registrationsListing();
	String submit=request.getParameter("Submit");
	if(submit!=null){%>
<div class="clear"></div>
<div class="list-details">
	 <div class="printable">
	 <table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
	 <jsp:useBean id="REG"  class="beans.sssst_registrations"></jsp:useBean>
			<jsp:useBean id="UI" class="beans.UniqueIdGroup"/>
			<%
				REG.setsssst_id(searchuniqueid);
				REG.setfirst_name(searchbyname);
				REG.setmobile(searchbyphone);
				REG.setgothram(searchbygothram);
				
				List SSSST_List = SSSSL.searchAndGetDevoteesRegList(REG);
				if(SSSST_List.size()>0){%>
				
		
		
			<tr>
			
			<td colspan="8">
				<table width="100%" cellpadding="0" cellspacing="0"  class="print_table sansthan" >
					 
            <tr class="print_table"  style="background-color: #e3eaf3;color: #7490ac;height:30px;">
				<td width="3%" style="font-size:13px;padding-top:5px;">S.NO.</td>
				<td width="7%" style="font-size:13px;padding-top:5px;">UNIQUE ID</td>
				<td width="10%" style="font-size:13px;padding-top:5px;">NAME</td>
				<td width="10%" style="font-size:13px;padding-top:5px;">PHONE NO</td>
				<td width="15%" style="font-size:13px;padding-top:5px;">EMAIL-ID</td>
				<td width="25%" style="font-size:13px;padding-top:5px;">ADDRESS</td>
				<td width="13%" style="font-size:13px;padding-top:5px;">GOTHRAM</td>
				<td width="17%" style="font-size:13px;padding-top:5px;">EDIT</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
                     
					 <%
						for(int G=0; G < SSSST_List.size(); G++ ){
						REG=(beans.sssst_registrations)SSSST_List.get(G);%>
					 
					 <tr style="position: relative;margin-bottom: 10px;">
					 	<td width="3%" style="font-size:13px;padding:10px 0px;"><%=G+1 %></td>
					 	<td width="7%" style="font-size:13px;padding:10px 0px;"><%=REG.getsssst_id() %></td>
					 	<td width="15%" style="font-size:13px;padding:10px 0px;"><%=REG.getfirst_name() %> <%=REG.getlast_name() %></td>
					 	<td width="12%" style="font-size:13px;padding:10px 0px;"><%=REG.getmobile() %></td>
					 	<td width="14%" style="font-size:13px;padding:10px 0px;"><%=REG.getemail_id() %></td>
					 	<td width="19%" style="font-size:13px;padding:10px 0px;"><%=REG.getaddress_1() %> ,<%=REG.getaddress_2() %>,<%=REG.getcity() %></td>
					 	<td width="10%" style="font-size:13px;padding:10px 0px;"><%=REG.getgothram() %></td>
					 	<td width="20%" style="font-size:13px;padding:10px 0px;"><a href="Devotees-Registrations_Edit.jsp?bid=<%=REG.getsssst_id()%>">Edit</a></td>
					 	
					 </tr>	
					 <%} %>
				</table>
			</td>
			</tr>
	</table>
	 </div>
	 		<%
					}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no list found for your search!</span></div>
					</div>
					<%}%>
</div>
</div>
</div>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
<%}}else{ response.sendRedirect("index.jsp");%>

<%} %>
</body>
</html>