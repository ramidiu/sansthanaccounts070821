<%@page import="java.util.TimeZone"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="beans.customerpurchases"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PRINT RECEIPT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
function getNumber()
	{
		var headId = $("#headId").val();
		var receiptNo = document.getElementById("receiptNo");
		$.post('getNumGenVal.jsp',{headAccountId:headId}, function(response) 
				{       	
					var where_is_mytool=response.trim();
					document.getElementById("receiptNo").value = where_is_mytool;
				});
	}


$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#fromdate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#todate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
});

$(function() {
	  $( ".datepickernew" ).datepicker({ 
		  yearRange: '1940:2020',
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
		  });
	}); 
	</script>
<!--Date picker script  -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
	
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function combochange(denom,desti,jsppage) { 
    var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value; 
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	var amt=document.getElementById("Amount");
	$.post(jsppage,{ id: com_id } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	var x=document.createElement('option');
	x.text='Select pooja name';
	x.value='';
	sda1.add(x);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");
				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
					sda1.add(y,null);
				}
				catch(e)
				{
					sda1.add(y);
				}
	}
	}
	}); 
}
function getPoojaPrice(){
	
	var proId=document.getElementById("productId").value;
	var subhId=document.getElementById("type").value;
	var amt=document.getElementById("Amount");
	var qty=document.getElementById("ticQty").value;
	var totAmt=document.getElementById("totAmt").value;
	$.ajax({
		type:'post',
		url: 'getPoojaPrice.jsp', 
		data: {
		q : subhId,
		b : proId
		},
		success: function(response) { 
			var arr=response.trim().split(":");
			var price=arr[0];
			var application=arr[1];
			amt.value='0';
			document.getElementById("appl").style.display='none';			
			if(application!=null && application!="" && application=="application"){
				document.getElementById("applicatn").value=arr[1];
				document.getElementById("appl").style.display='block';
			}
			amt.value=price;
			document.getElementById("totAmt").value=Number(price)*Number(qty);
			
		}});
}
 function productsearch(){
	  var data1=$('#productId').val();
	  var headId=$('#headId').val();
	  var arr = [];
	  var page = "Rec"; // added by gowri shankar
	  if($("#check").is(':checked')) page = "Hec"; // added by gowri shankar
	
	$.post('searchSubhead.jsp',{q:data1,hoa:headId,page : page},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var appl=new Array();
		 var gross=0;
		 
			var amt=document.getElementById("Amount");
			var qty=document.getElementById("ticQty").value;
			var totAmt=document.getElementById("totAmt").value;
		 for(var i=0;i<response.length;i++){			 
			 var d=response[i].split(",");
			 
			 gross=d[2];
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        appl:""+d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#productId').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#Amount').val(ui.item.amount);
				$('#Amounthidden').val(ui.item.amount);//this value will store in hidden field of "Amounthidden"
				var amt=ui.item.amount;
				var application=ui.item.appl;
				var offerkind=ui.item.label.split(' ');
		        var offer=offerkind[0];
				document.getElementById("totAmt").value=Number(amt)*Number(qty);
				document.getElementById("totAmthidden").value=Number(amt)*Number(qty);//this value will store in hidden field of "totAmthidden"
				document.getElementById("appl").style.display='none';
				  if(application.trim()==="application"){
					document.getElementById("applicatn").value=application;
					document.getElementById("appl").style.display='block';
				 } 
				  
				  
				 if(offer.trim()==="20032" || offer.trim()==="20033" || offer.trim()==="25771" || offer.trim()==="25794" || offer.trim()==="25795" ){
					document.getElementById("offerkindt").style.display='block';
					document.getElementById("offerkindts").style.display='block';
					document.getElementById("offerkindn").style.display='block';
					document.getElementById("offerkindns").style.display='block';
					$('#offerkind_refid').focus();
					getOfferKind(offer);
					 } else{
							document.getElementById("offerkindt").style.display='none';
							document.getElementById("offerkindts").style.display='none';
							document.getElementById("offerkindn").style.display='none';
							document.getElementById("offerkindns").style.display='none';
							
							//$('#registration').focus();
							/* $('#mobile').focus(); */
							$('#uniqueID').focus();
					 }
				// $("#offerkindn").focus();
				//$('#mobile').focus();
				//$('#registration').focus();
				 $("#productId").attr('readonly', true);
			}
		}); 
	});
} 
 
  function ofksearch(){
	 var ofkId=$('#offerkind_refid').val();
	 //alert("ofkID==="+ofkId);
	 //var headId=$('#headId').val();
	 $.post('offerkindSearch.jsp',{q:ofkId},function(data)
				{
						 var productNames=new Array();
					var response = data.trim().split("\n");
					 for(var i=0;i<response.length;i++){
						 var d=response[i].split(",");
						 productNames.push(d[0]);
					 }
					var availableTags=data.trim().split("\n");
					var availableIds=data.trim().split("\n");
					availableTags=productNames;
					$( "#offerkind_refid" ).autocomplete({source: availableTags,
						select: function(event, ui) {
							//$('#customerName').focus();
							/* $('#uniqueid').focus(); */	//while using Tab the cursor will come on First Name field
							//$('#uniqueID').focus();
							//$('#customerName').val(ui.item.customerName);
						}	
					}); 
				});
	 }
 
function amountInWords(){
	  var amount=$('#totAmt').val();
		  var arr = [];
	  $.post('numberInWords.jsp',{q:amount},function(data)
	{
		var response = data.trim().split("\n");
		 $('#amountinwords').empty();
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();	 
		 			 var d=response[0].split(",");	
			 $('#amountinwords').append('('+d[0]+')');
		
	
	});
	} 

function phonenosearch(){
	  var mobile=$('#mobile').val();
		  var arr = [];
	  $.post('searchPhoneNo.jsp',{q:mobile},function(data)
	{
		var response = data.trim().split("\n");
		
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();	 
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
		
			 arr.push({
				 label: d[0],
				 customerName:d[1],
				 gothram:d[2],
				 address:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#mobile').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#customerName').val(ui.item.customerName);
				$('#gothram').val(ui.item.gothram);
				$('#address').val(ui.item.address);			
			}
		}); 
	});
}

function GetUniqueRegistrations(){
	  var mobile=$('#uniqueID').val();
		  var arr = [];
	 $.post('searchPhoneNo.jsp',{q:mobile,con : 'uniqueId'},function(data) 
	{
		var response = data.trim().split("\n");
		var sssstId = new Array();
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();
		 var address2=new Array();
		 var address3=new Array();
		 var city=new Array();
		 var pincode=new Array();
		 var pancard=new Array();
		 var email=new Array();
		 var dob=new Array();
		 for(var i=0;i<response.length;i++){
			// var d=response[i].split(",");
			 var d=response[i].split(":");
			 arr.push({
				 label: d[0],
				 sssstId :d[1],
				 phoneno:d[2],
				 customerName:d[3],
				 gothram:d[4],
				 address:d[5],
				 address2:d[6],
				 city:d[7],
				 pincode:d[8],
				 email:d[9],
				 pancard:d[10],
				 dob:d[11],
				 address3:d[12],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#uniqueID').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#uniqueid').val(ui.item.sssstId);
				$('#mobile').val(ui.item.phoneno);
				$('#customerName').val(ui.item.customerName);
				$('#gothram').val(ui.item.gothram);
				$('#address').val(ui.item.address);	
				$('#addressSj').val(ui.item.address);		
				$('#address2').val(ui.item.address2);
				$('#address3').val(ui.item.address3);
				$('#city').val(ui.item.city);
				$('#pincode').val(ui.item.pincode);
				$('#emailid').val(ui.item.email);
				$('#date').val(ui.item.dob);
				$('#pancardNo').val(ui.item.pancard);
				
			}
		}); 
	});
}
function calculateTotal(){
	var amt=document.getElementById("Amount").value;
	var qty=document.getElementById("ticQty").value;
	document.getElementById("totAmt").value=Number(amt)*Number(qty);
	amountInWords();
} 
function validate(){
   var str=$('#offerkind_refid').val();
   var str1=$('#offerkind_refid1').val();
   /* var str2=$('#offerkind_refid1').val().split(",");
 alert("offerKindId::: "+str2[0]);
   $('#offerkind_refid1').val(str2[0]); */
   
   var productId =$('#productId').val();
   
   if(productId == "20032 SALE OF SHAWL"){
          if (str=="OFK780" && str1=="") {
	       alert('please enter Offerkind');
          return false;
          } 
   }
      var ofkValue = str.substring(0, 3);
      var emptyValue = str.substring(3, 4); 
	if(ofkValue!="OFK" && ofkValue!="ofk" && ofkValue!=""){
			  alert('please enter OfferKind full billingId which starts with OFK...');
			  return false;
		  }
		  if(str.includes("/")) {
			    alert('please dont enter date,enter only OFK billingId');
			    return false;
			}
		  if(str.includes("-")) {
			    alert('please dont enter date,enter only OFK billingId');
			    return false;
			}
		  if(str.includes(".")) {
			    alert('please dont enter date,enter only OFK billingId');
			    return false;
			}
	
	$('#poojaError').hide();
	$('#amtError').hide();
	$('#nameErr').hide();
	$('#othercashError').hide();
	$('#currencyError').hide();
	var currencyType=$('#currencyType').val();
	var paymentType=$('#paymentType').val();
	/* alert(currencyType); */
	
	var amt=$('#Amount').val().trim();
	var taille = $('#pancardNo').val().trim().length;
	if(amt>9999)
	{
		if($('#pancardNo').val().trim()==="")
		{
		$('#panError').show();
		$('#pancardNo').addClass('borderred');
		$('#pancardNo').focus();
		return false;
		}
		if (taille<10  || taille>12) 
		{
			$('#panLenError').show();
			$('#pancardNo').addClass('borderred');
			$('#pancardNo').focus();
			return false;
		}
	}
	
	if (paymentType =="card"){
		if(($('#fourDigits').val().trim()==""))
		{
			$('#fdError').show();
			$('#fourDigits').addClass('borderred');
			$('#fourDigits').focus();
			 return false;
		}
		
		if(($('#authNum').val().trim()==""))
		{
			$('#anError').show();
			$('#authNum').addClass('borderred');
			$('#authNum').focus();
			 return false;
		}
	}
	if ((paymentType =="googlepay")||(paymentType =="phonepe") ){

		if(($('#trMobile').val().trim()==""))
		{
			$('#gpError').show();
			$('#trMobile').addClass('borderred');
			$('#trMobile').focus();
			 return false;
		}
		if(($('#trnxNum').val().trim()==""))
		{
			$('#peError').show();
			$('#trnxNum').addClass('borderred');
			$('#trnxNum').focus();
			 return false;
		}
		
	}
	
	if (currencyType != "RS" && paymentType !="othercash"){
		/*  alert("please select paymentType as othercash"); */
		$('#othercashError').show();
		$('#paymentType').addClass('borderred');
		$('#paymentType').focus();
		 return false;
	}
	if (currencyType == "RS" && paymentType =="othercash"){
		/*  alert("please select paymentType as othercash"); */
		$('#currencyError').show();
		$('#currencyType').addClass('borderred');
		$('#currencyType').focus();
		 return false;
	}
	if(($('#productId').val().trim()=="")){
		$('#poojaError').show();
		$('#productId').addClass('borderred');
		$('#productId').focus();
		return false;
	}
	/* if(($('#customerName').val().trim()=="")){
		$('#nameErr').show();
		$('#customerName').addClass('borderred');
		$('#customerName').focus();
		return false;
	} */
	if(($('#Amount').val().trim()=="") || ($('#Amount').val().trim()=="0")){
		$('#amtError').show();
		$('#Amount').addClass('borderred');
		$('#Amount').focus();
		return false;
	}
		/* if($("#applicatn").val()=="application"){
		var amt=$('#Amount').val().trim();
		var taille = $('#pancardNo').val().length;
		if($('#pancardNo').val().trim()==""){
			$('#panError').show();
			$('#pancardNo').addClass('borderred');
			$('#pancardNo').focus();
			return false;
		}
		if (taille !=10) {
    		$('#panLenError').show();
			$('#pancardNo').addClass('borderred');
			$('#pancardNo').focus();
			return false;
		}
		if(amt>9999){
			if($('#pancardNo').val().trim()==""){
			$('#panError').show();
			$('#pancardNo').addClass('borderred');
			$('#pancardNo').focus();
			return false;
		}}
	} */
		
	// added by gowri shankar
	if (productId.split(" ")[0] === "20092" || productId.split(" ")[0] === "20063")	{
		var gothram = $('#gothram').val();
		var mobile = $('#mobile').val();
		var emailid = $('#emailid').val();
		var performedName = $('#performedName').val();
		if ($.trim(gothram) === "")	{
			$('#gothram').focus();
			return false;
		}
 		if ($.trim(mobile) === "")	{
			$('#mobile').focus();
			return false;
		}
		if ($.trim(emailid) === "")	{
			$('#emailid').focus();
			return false;
		}
		if ($.trim(performedName) === "")	{
			$('#performedName').focus();
			return false;
		}
	}
	
	
	
	
	var sevaname=$('#productId').val();
		var amount=$('#totAmt').val();
		var wordAmt=$('#amountinwords').text();
		 var status = confirm('You have entered below details.Please check : \n Seva Name : '+sevaname+' \n Total Amount : '+amount+' '+wordAmt+' \n Click OK to proceed');
		   if(status == false){
		   return false;
		   }
		   else{
			   $.blockUI({ css: { 
			        border: 'none', 
			        padding: '15px', 
			        backgroundColor: '#000', 
			        '-webkit-border-radius': '10px', 
			        '-moz-border-radius': '10px', 
			        opacity: .5, 
			        color: '#fff' 
			    } });
		   return true; 
		   }
		  
}
function CheckSelect(){
	var selectedItem=$('#offerkind_refid1').val();
	if(selectedItem!=null && selectedItem!=''){
		$('#ticQty').val('');
		$('#amt').empty();
		//$('#totAmt').val('');
		$('#purpose').val('');
		
		var items=selectedItem.split(",");
		$('#ticQty').val(items[1]);
		$('#amt').append(items[2]);
		//$('#totAmt').val(items[3]);
		$('#purpose').val(items[4]);
		
	}
	
	var element1=$("#offerkind_refid").val();
    var element2 = $("#offerkind_refid1").val();
    var element3=$("#offerkindns").val();
    var element5=$("#offerkindn").val();
     
   if(element1 =="OFK780" && element2 ==""){
		$("#offerkind_refid").show();
		$("#offerkindn").show();
		$("#offerkind_refid1").show();
		$("#offerkindns").show();
		}
   else if(element1 == "OFK780" && element2!=""){	
   	  $("#offerkind_refid").hide();
	  $("#offerkindn").hide();
		}
   
   else if(element2 != "OFK780" && element2==""){
		    $("#offerkind_refid1").hide();
			$("#offerkindns").hide();
			}
   if (element1.length<6){
    var element1=$("#offerkind_refid").val("OFK780");
    //alert("11111"+x);
      }
   
   	if(element1.length>=3){
   		var str =element1.substring(0,3);
   		 if(str!="OFK" && str!="ofk"){
   			alert("please enter proper id which starts with OFK");
   			return false;
   		} 
   	    //alert("aaa=="+str.charAt(0));
   		/*  if(str.charAt(0)!="O" && str.charAt(0)!="o"){
   			alert("id starting with O");
   			return false;
   		}	
   		
   		else if(str.charAt(1)!="F" && str.charAt(1)!="f"){
   			//alert("BBBBB=="+str.charAt(1));
   			alert("second letter must be  starting with F");
   			return false;
   		}
   		else if(str.charAt(2)!="K" && str.charAt(2)!="k"){
   			//alert("BBBBB=="+str.charAt(2));
   			alert("third letter must be id starting with k");
   			return false;
   		}  */ //commented by madhav
   	}
 }	   
   
       
function nameAddValidation(){
	$("#warMsg").hide();
	$("#save").show();
	var fsname=$("#customerName").val();
	var mobileNo=$("#mobile").val();
	var email=$("#emailid").val();
	if(fsname=="" && email==""){

	}else if($("#applicatn").val()=="application"){
	$.ajax({

		type:'post',
		url: 'customerValidation.jsp', 
		data: {
		name : fsname,
		mobile : mobileNo,
		emailId : email
		},
		success: function(response) { 
			var resMsg=response.trim();
			/* if(resMsg==="customer exists"){ */
				/* $("#warMsg").show();
				$("#save").hide(); */
				/* $('#panError').show();
				$('#pancardNo').addClass('borderred');
				$('#pancardNo').focus();
			}else if(resMsg==="customer does not exists"){ */
				/* $("#warMsg").hide();
				$("#save").show(); */
				/* $('#panError').hide();
			} */
		}});
	}
}
$(document).ready(function () {
    $("#paymentType").change(function () {
        var paymentType=$("#paymentType").val();
        $("#chequeDetails1").hide();
        $("#chequeDetails2").hide();
        $("#chequeDetails3").hide();
        $("#chequeDetails4").hide();
        
        $("#othercashheading").hide();
        $("#otherCashPaymentType").hide();
        
        $("#fd1").hide();
        $("#an1").hide();
        $("#fd2").hide();
        $("#an2").hide();
        $("#gpayNum1").hide();
        $("#gpayTr1").hide();
        $("#gpayNum2").hide();
        $("#gpayTr2").hide();
        
        if(paymentType==="cheque"){
        	$("#chequeDetails1").show();
        	$("#chequeDetails2").show();
        	$("#chequeDetails3").show();
        	$("#chequeDetails4").show();
        }
        
        
        if(paymentType==="othercash"){
        	$("#othercashheading").show();
        	$("#otherCashPaymentType").show();
        }
        
        if(paymentType==="card"){
        	$("#fd1").show();
        	$("#an1").show();
        	$("#fd2").show();
        	$("#an2").show();
        }
        if((paymentType==="googlepay") || (paymentType==="phonepe")){
        	 $("#gpayNum1").show();
             $("#gpayTr1").show();
             $("#gpayNum2").show();
             $("#gpayTr2").show();
        }
        
        
        
    });
    
    $("#otherCashPaymentType").change(function () {
    	var otherCashPaymentTypeVal=$("#otherCashPaymentTypeVal").val();
        $("#chequeDetails1").hide();
        $("#chequeDetails2").hide();
        $("#chequeDetails3").hide();
        $("#chequeDetails4").hide();
        
        if(otherCashPaymentTypeVal==="othercash-cheque"){
        	$("#chequeDetails1").show();
        	$("#chequeDetails2").show();
        	$("#chequeDetails3").show();
        	$("#chequeDetails4").show();
        }
    });
    
    
    $(".click").click(function () {
    	$('#productId').focus();
    	 $("#productId").attr('readonly', false);
    });
    $("#registration").click(function () {
    		 document.getElementById("uniqueID").disabled = true;
    		 $("#mobile").focus();
    });
    $("#registration1").click(function () {
		document.getElementById("uniqueID").disabled = false;
		/* document.getElementById("uniqueid").disabled = false; */
		$('#uniqueID').focus();
	});
});
</script>
 <script>
function makereadonly(){
	var amounthidden = document.getElementById("Amounthidden").value;
	var totamthidden = document.getElementById("totAmthidden").value;
	var quantityhidden = document.getElementById("ticQtyhidden").value;
 if(document.getElementById("paymentType").value == "othercash"){
	 var amount1 = 1;
	 $('#Amount').attr('readonly',true);
	 document.getElementById("ticQty").value = Number(amount1);
	 document.getElementById("Amount").value=Number(amount1);
	 document.getElementById("totAmt").value=Number(amount1);
 }else if(document.getElementById("paymentType").value == "cash" || document.getElementById("paymentType").value == "cheque"){
	 $('#Amount').removeAttr('readonly');
	 document.getElementById("ticQty").value = Number(quantityhidden);
	 document.getElementById("Amount").value=Number(amounthidden); 
	 document.getElementById("totAmt").value=Number(totamthidden); 
 }
 
}
</script>
<script type="text/javascript">
function getOfferKind(offer){
	var op='';
	var op1='<option value="">--Select OfferKind ID--</option>';
	var data="pId="+offer;
	
	$.ajax({
		type:'post',
		url: 'GetOfferKindBasedOnProductId.jsp', 
		data: data,
		success: function(response) {
			var resData=response.split("\n");
			var resData2='';
			$("#offerkind_refid1").empty();
			for(var k=0;k<resData.length;k++){
				resData2=resData[k].split(",");
				if(resData2[4]!='' && resData2[4]!=null){
					op=op+'<option value='+resData2[0]+','+resData2[1]+','+resData2[2]+','+resData2[3]+','+resData2[4].replace(/\s/g,'\u00a0')+'>'+resData2[0]+'</option>';
				}
			}
			
			 $("#offerkind_refid1").append(op1+op);
		} 
		
	});
}

</script>
 
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="SUB" class="beans.subhead"></jsp:useBean>
<jsp:useBean id="MNR" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="CSP" class="beans.customerpurchases"/>
<%if(session.getAttribute("empId")!=null){ 
String HOAID=session.getAttribute("headAccountId").toString();

%>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-box">
 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO OFFICE </td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;">RECEIPTS</td></tr>
</table>
<div class="vender-details">
<div style="display: none;color: red;" id="warMsg" >Sorry,this customer unable to donate the amount because of he is already donated amount in this year with this same details.</div>
<form name="tablets_Update" id="tablets_Update" method="post" action="ticketDetails_Insert.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">

<%
 Calendar c1 = Calendar.getInstance();
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz); 
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
String currentDate = dateFormat.format(c1.getTime());
%>
<input type="hidden" name="bid" id="bid" value="<%=request.getParameter("bid")%>"/>
<input type="hidden" name="seva" id="seva" value="<%=request.getParameter("seva")%>"/>
<tr>
	<td>Date</td>
	<!-- <td>Receipt No</td> -->
	<td>Head of account</td>
	<td colspan="1">
		<!-- <div class="warning" id="poojaError" style="display: none;">Please select a pooja.</div> -->
		SEVA Name <span style="color: red;">*</span></td>
	<td><div id="offerkindn" style="display: none">Offer Kind No</div> </td>
	<td><div id="offerkindns" style="display: none">Offer Kind Id</div></td>
	
</tr>
<tr>
	 <td><input type="text" name="curentdate" value="<%=currentDate%>" readonly="readonly"/></td> 
	<%-- <td><input type="text" id="receiptNo" name="receiptNo" value="<%=ng_LST.getid("proOfficeSales")%>" readonly="readonly"></td> --%>
	<%-- <td><input type="text" id="receiptNo" name="receiptNo" value="<%=ng_LST.getidWithoutUpdatingNoGen("pro_sansthan")%>" readonly="readonly"></td> --%>
	<td colspan="1">
	<!-- <select name="headId" id="headId"  autofocus="autofocus"> -->
	<select name="headId" id="headId"  autofocus="autofocus" onchange="getNumber()">
		<option value="4">SANSTHAN</option>
		<option value="1">CHARITY</option>
		<option value="5">SAI NIVAS</option>
		<option value="3">POOJA STORES</option> 
	</select>
	</td>
<td>
<input type="text" name="productId" id="productId" autocomplete="off"  autofocus="autofocus" required placeHolder="Find a seva here" onkeyup="productsearch();">
</td>
<td colspan="1">
<!-- <a href="ticket-application.jsp"><img src="images/button-cancel.png" width="20px"></img></a> -->
<a href="ticket-application.jsp"><input type="button" name="other" value="other" class="click" style="border:none;"/></a>
<input type="checkbox" id="check" >&nbsp;LoanAndEmd <!-- added by gowri shankar -->
<div id="offerkindt" style="display: none">
<input type="text" name="offerkind_refid" id="offerkind_refid" value="OFK780" maxlength="10" autocomplete="off" placeHolder="Enter 'OFK' Actual billing No" onkeyup="CheckSelect();" onblur="javascript:this.value=this.value.toUpperCase();" ><!-- onkeyup="ofksearch();"  -->
</div>
</td>    
<td colspan="1">
<div id="offerkindts" style="display: none">
<%-- <%String ofk="OFK";
String cashtype="offerKind";
String offerkind_refid="sold";
	customerpurchasesListing cListing=new customerpurchasesListing();
    List<customerpurchases> Cpurchases = new ArrayList<customerpurchases>();
  	System.out.println("Pid is :::: "+request.getParameter("pId"));

    String fromDate="2018-03-19 00:00:00";
    Cpurchases=cListing.getBillDetailsOfferKindBesedOnFromDateAndToDate(ofk,cashtype,offerkind_refid, fromDate, currentDate+" 23:59:59");
	%> --%>
<select name="offerkind_refid1" id="offerkind_refid1"  autofocus="autofocus" "  autocomplete="off"  onkeyup="ofksearch();" onchange="CheckSelect();"  > 

	    <option value="">--Select OfferKind ID--</option>
	      <%--  <%customerpurchases c2=new customerpurchases();
	       int k=0;
	       int sum1=0;
	         for( k=0;k<Cpurchases.size();k++){ 
	        	
		     c2=Cpurchases.get(k);
		     sum1=sum1+k;%>
	     <option value="<%=c2.getbillingId()%>"><%=c2.getbillingId() %></option>
		         <%
		       }
	         System.out.println("count=====>"+k);
		   %> --%>
	</select>
	</div>
	</td>
	
</tr>
<tr>
<td>Get Details with Mobile</td>
<td>Mobile Number</td>
<td>WhatsUp Verification</td>
<td>Unique Id</td>
</tr>
<tr>
	<!--<td colspan="2">Have SSSST Registration ID ? </td> 
	 <td><input type="radio" name="registration" id="registration" value="no" checked="checked"/>No<input type="radio" name="registration"  id="registration1" value="yes"/>Yes</td> -->
	<!-- <td><input type="text" name="uniqueID" id="uniqueID" onkeyup="();" value="" placeHolder="Find details with UniqueId" disabled /> -->
	<!-- <td colspan="2">Get the Details with Mobile Number </td> -->
	<td><input type="text" name="uniqueID" id="uniqueID" value="" onkeyup="GetUniqueRegistrations();" onkeypress="return numbersonly(this, event,true);" autocomplete="off"  placeHolder="Find details with MobileNumber" />
	<td><input type="text" name="mobile" id="mobile" value=""  onkeypress="return numbersonly(this, event,true);" onblur="nameAddValidation();" autocomplete="off" placeholder="Enter mobile number"></td>
	<td>
<input type="radio" name="wtsp" id="wtsp" value="ok" />Enable 
<input type="radio" name="wtsp" id="wtsp"  value="notOk" checked="checked"/>Disable
</td>	
	<td><input type="text" name="uniqueid" id="uniqueid" value=""  placeHolder="UniqueId Will Displayed" readonly />
</tr>
<tr>
<!-- <td> -->
<div class="warning" id="QualificationError" style="display: none;">Please Provide  "Qualification".</div>
<!-- Mobile Number</td> -->
<td><div class="warning" id="nameErr" style="display: none;">Please Provide  "First name".</div>First Name <span style="color: red;">*</span></td>
<td>Gothram</td>

<td>Address</td>
<td>Email Id</td>
</tr>
<tr>
<!-- <td><input type="text" name="mobile" id="mobile" value=""  onKeyPress="return numbersonly(this, event,true);" onkeyup="phonenosearch();" onblur="nameAddValidation();" autocomplete="off" placeholder="Enter mobile number"></td> -->
<!-- <td><input type="text" name="mobile" id="mobile" value=""  onKeyPress="return numbersonly(this, event,true);" onblur="nameAddValidation();" autocomplete="off" placeholder="Enter mobile number"></td> -->
<td><input type="text" name="customerName" id="customerName" value="" onblur="javascript:this.value=this.value.toUpperCase();" required  placeholder="Enter full name"></td>
<td><input type="text" name="gothram" id="gothram" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeholder="Enter gothram"></td>
 

<td>
<input type="text" name="address" id="address" placeholder="Enter Your Address" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
<td><input type="text" name="emailid" id="emailid" value="" onblur="javascript:this.value=this.value.toLowerCase();" placeholder="Enter Your Email Id"></td>
</tr>
<tr>
<td>CurrencyType
<div class="warning" id="currencyError" style="display: none;">Please select USD/POUND.</div>
</td>
<td>Payment Type
<div class="warning" id="othercashError" style="display: none;">Please select a other cash.</div>
</td>
<td colspan="1" style="display: none;" id="othercashheading">Other Cash Payment Type</td>
<td>Qty</td>
<td>
<div class="warning" id="amtError" style="display: none;"  >Please enter amount.</div>
Amount <span style="color: red;">*</span></td>
<td colspan="1">Gross Amount</td>
</tr>
<tr>
<td>
	<select name="currencyType" id="currencyType">
		<option value="RS">&#x20B9; - Rupees </option>
		<option value="USD">$ - USD </option>
		<option value="POUND">&pound; - POUND </option>
	</select>
</td>
<td>
	<select name="paymentType" id="paymentType" onchange="makereadonly()">
		<option value="cash">CASH </option>
		<option value="cheque">CHEQUE</option>
		<option value="othercash">OTHER CASH</option>
		<option value="card">CARD</option>
		<option value="googlepay">BHARATH PE</option>
		<option value="phonepe">PHONE PE</option>
	</select>
</td>

<td id="otherCashPaymentType" colspan="1" style="display: none;">
	<select name="otherCashPaymentType" id="otherCashPaymentTypeVal">
		<option value="othercash-cash">CASH </option>
		<option value="othercash-cheque">CHEQUE</option>
	</select>
</td>

<!-- <td><input type="radio" name="extra1" value="M" checked="checked" />Male<input type="radio" name="extra1" value="F" />Female</td> -->
<td><input type="hidden" name="applicatn" id="applicatn" value="">
<input type="hidden" name="ticQtyhidden" id="ticQtyhidden" value="1" onKeyPress="return numbersonly(this, event,true);" onkeyup="calculateTotal();">
<input type="text" name="ticQty" id="ticQty" value="1" onKeyPress="return numbersonly(this, event,true);" onkeyup="calculateTotal();"></td>
<td><input type="hidden" name="Amounthidden" id="Amounthidden" onKeyPress="return numbersonly(this, event,true);" value="0" onkeyup="calculateTotal();" autocomplete="off"/>
<input type="text" name="Amount" id="Amount" onKeyPress="return numbersonly(this, event,true);" value="0" onkeyup="calculateTotal();" autocomplete="off"/>
</td>
<td><input type="hidden" name="totAmthidden" id="totAmthidden" value="0" readonly="readonly">
<input type="text" name="totAmt" id="totAmt" value="0" readonly="readonly">
<div id="amountinwords" style="text-align: center;font-size: 12px;color: #F00;"></div></td>

</tr>
<tr>
	<td colspan="5" style="text-align: center;font-size: 18px;color: #F00;" ><div id="amt"></div></td>

</tr> 
<tr>
	<td colspan="1" style="display: none;" id="chequeDetails1">Cheque No<span style="color: red;">*</span></td>
	<td colspan="1" style="display: none;" id="chequeDetails2">Name On Cheque<span style="color: red;">*</span></td>
	<td>Purpose</td>
	<td >From date</td>
	<td>To date</td>
	<td>Pancard Number/Adhaar card</td>
</tr>
<tr>
<td colspan="1" style="display: none;" id="chequeDetails3"><input type="text" name="chequeNo" id="chequeNo" value=""  ></td>
<td colspan="1" style="display: none;" id="chequeDetails4"><input type="text" name="nameOnCheque" id="nameOnCheque" value=""></td>
<td><input type="text" name="purpose" id="purpose" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeholder="Enter purpose"></td>
<td ><input type="text" name="fromdate" id="fromdate" value="" style="float: left;width: 110px;" readonly="readonly"></td>
<td ><input type="text" name="todate" id="todate" value="" style="float: left;;width: 110px;"  readonly="readonly"></td>
<td>
	<div class="warning" id="panLenError" style="display: none;">PAN Number length must be 10 digists.</div>
	<div class="warning" id="panError" style="display: none;">Please enter pan card or Adhaar card number.</div><input type="text" name="pancardNo" id="pancardNo"  maxlength="12" value=""></td>
</tr>
<tr>
<td colspan="5">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="appl" style="display: none;">
	<tr>
	<td>Application Ref No</td>
	<!-- <td>Title</td> -->
	<td>Performed in the Name of</td>
	<td>Performed Date</td>
	<td colspan="2">Special Days</td>
	</tr>
	<tr>
	<td><input type="text" name="AppRefNo" placeHolder="Enter Application reference no"></input></td>
	<!-- <td><select name="title">
	<option value="Mr">Mr.</option>
	<option value="Miss">Miss.</option>
	<option value="Mr&miss">Mr&miss</option>
	</select></td> -->
	<td><input type="text" name="lastname" id="performedName" value="" onBlur="javascript:this.value=this.value.toUpperCase();"></td>
	<td width="30">
	<select name="performedMonth" style="width: 85px;">
	<option value="">Month</option>
	<option value="01">JAN</option>
	<option value="02">FEB</option>
	<option value="03">MAR</option>
	<option value="04">APR</option>
	<option value="05">MAY</option>
	<option value="06">JUN</option>
	<option value="07">JUL</option>
	<option value="08">AUG</option>
	<option value="09">SEP</option>
	<option value="10">OCT</option>
	<option value="11">NOV</option>
	<option value="12">DEC</option>
	</select>
	<select name="performedDay" style="width: 85px;">
	<option value="">Day</option>
	<%for(int i=01;i<32;i++){ 
	if(i < 10){%>
	
	<option value="0<%=i%>">0<%=i%></option>
	<%}else{ %>
	<option value="<%=i%>"><%=i%></option>
	<%} }%>
	</select>
	</td>
	
	<td colspan="2" style="border: 1px solid #000;"><input type="checkbox" name="specialDays" value="Guru poornima">Guru poornima
	<input type="checkbox" name="specialDays" value="Dasara">Dasara
	<input type="checkbox" name="specialDays" value="Sri Rama navami">Sri Rama navami<br>
	<input type="checkbox" name="specialDays" value="Datta jayanthi">Datta jayanthi</td>
	
	<!-- <td> <input type="text" name="gothram" value=""> </td> -->
	</tr>
	<tr>
	<td>Address</td>
	<td>Address2</td>
	<td>Address3</td>
	<td>City</td>
	<td>Pin code</td>
	<td></td>
	</tr>
	<tr>
	<td><input type="text" name="addressSj" id="addressSj" value="" onblur="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input type="text" name="address2" id="address2" value="" onblur="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input type="text" name="address3" id="address3" value="" onblur="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input type="text" name="city" id="city" value="" onblur="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input type="text" name="pincode" id="pincode" value=""></td>
	<td></td>
	</tr>
	<tr>
	<td>Date of Birth</td>
	<td>Date Of Anniversary</td>
	<!-- <td>Pancard Number<span style="color: red;">*</span></td> -->
	
	</tr>
	<tr>
	<td><input type="text" name="dob"  class="datepickernew" value="" readonly="readonly" style="width: 80px;">
	 </td>
	<td width="30">
	<select name="doamonth" style="width: 85px;">
	<option value="">Month</option>
	<option value="01">JAN</option>
	<option value="02">FEB</option>
	<option value="03">MAR</option>
	<option value="04">APR</option>
	<option value="05">MAY</option>
	<option value="06">JUN</option>
	<option value="07">JUL</option>
	<option value="08">AUG</option>
	<option value="09">SEP</option>
	<option value="10">OCT</option>
	<option value="11">NOV</option>
	<option value="12">DEC</option>
	</select>
	<select name="doadate" style="width: 85px;">
	<option value="">Day</option>
	<%for(int i=01;i<32;i++){ 
	if(i < 10){%>
	
	<option value="0<%=i%>">0<%=i%></option>
	<%}else{ %>
	<option value="<%=i%>"><%=i%></option>
	<%} }%>
	</select></td> 
	<!-- <td>
	<div class="warning" id="panLenError" style="display: none;">PAN Number length must be 10 digists.</div>
	<div class="warning" id="panError" style="display: none;">Please enter pan card number.</div><input type="text" name="pancardNo" id="pancardNo"  maxlength="10" value=""></td> -->
	
	
	<!-- <td><input type="text" name="fromdate" id="fromdate" value=""></td>
	<td><input type="text" name="todate" id="todate" value=""></td> -->
	<!-- <td> <input type="text" name="pancardNo" value=""></td> -->
	</tr>
	</table>
</td>
</tr>

<tr>
	<td colspan="1" style="display: none;" id="fd1">Last Four Digits On Card</td>
	<td colspan="1" style="display: none;" id="an1">Invoice Number</td>
</tr>
<tr>
<td colspan="1" style="display: none;" id="fd2"><div class="warning" id="fdError" style="display: none;">Please Enter Card Last Four Digits.</div><input type="text" name="fourDigits" id="fourDigits" onKeyPress="return numbersonly(this, event,true);" value="" style="float: left;width: 110px;"></td>
<td colspan="1" style="display: none;" id="an2"><div class="warning" id="anError" style="display: none;">Please Enter Authentication Number.</div><input type="text" name="authNum" id="authNum" value="" style="float: left;;width: 150px;"></td>
</tr>
<tr>
	<td colspan="1" style="display: none;" id="gpayNum1">Transaction Mobile Number</td>
	<td colspan="1" style="display: none;" id="gpayTr1">Transaction NUmber</td>
</tr>
<tr>
<td colspan="1" style="display: none;" id="gpayNum2"><div class="warning" id="gpError" style="display: none;">Please Enter MobileNumber.</div><input type="text" name="trMobile" id="trMobile" onKeyPress="return numbersonly(this, event,true);" value="" style="float: left;width: 110px;"></td>
<td colspan="1" style="display: none;" id="gpayTr2"><div class="warning" id="peError" style="display: none;">Please Enter Transaction Number.</div><input type="text" name="trnxNum" id="trnxNum" value="" style="float: left;;width: 150px;"></td>
</tr>

<tr > 
<td colspan="3" align="right"><input type="reset" value="RESET" class="click" style="border:none;"/></td>
<td colspan="1" align="right"><input type="submit" name="saveonly" value="SAVE-ONLY" class="click" style="border:none;"/></td>
<td align="right" style="display: block;" id="save"><input type="submit" value="Save&Print" class="click"  style="border:none;"/></td>

</tr>
</table>
</form>
</div>
</div>
</div>
<%} %>
<script>
window.onload=function a(){
	combochange('type','productId','getPoojas.jsp')
	};
	function print()
	{
	 if(document.getElementById("bid").value != 'null' && document.getElementById("seva").value == 'null' ){
		  newwindow=window.open('printReceipt.jsp?bid='+document.getElementById("bid").value+'&recName=sevaticket','name','height=600,width=500,menubar=yes,status=yes,scrollbars=yes'); 
				 /*newwindow=window.open('Pro_print.jsp?bid='+document.getElementById("bid").value); */
		  //newSJwindow=window.open('Devotee-Image-Capture.jsp?bid='+document.getElementById("bid").value,'nameSJ','height=600,width=650,menubar=yes,status=yes,scrollbars=yes'); 		
		if (window.focus) {newSJwindow.focus();}
	}else if(document.getElementById("bid").value != 'null' && document.getElementById("seva").value != 'null'){
		
		new2window=window.open('nithya-annadana-certificat.jsp?bid='+document.getElementById("bid").value+'&seva='+document.getElementById("seva").value,'name1','height=600,width=500,menubar=yes,status=yes,scrollbars=yes');
		newwindow=window.open('printReceipt.jsp?bid='+document.getElementById("bid").value+'&recName=sevaticket','name','height=300,width=450,menubar=yes,status=yes,scrollbars=yes'); 
		 /*newwindow=window.open('Pro_print.jsp?bid='+document.getElementById("bid").value);*/ 
		 //newSJwindow=window.open('Devotee-Image-Capture.jsp?bid='+document.getElementById("bid").value,'nameSJ','height=600,width=650,menubar=yes,status=yes,scrollbars=yes'); 		
		if (window.focus) {newSJwindow.focus();}
		} 
	}
	window.onload=print();
</script>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>