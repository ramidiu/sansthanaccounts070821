<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.poojasaamanListing"%>
<%@page import="mainClasses.no_genaratorListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pooja stores | Sai sansthan</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});		
});
</script>
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#CustomerError').hide();
	$('#PhoneError').hide();
	$('#VoucherError').hide();
	if(document.getElementById("cash_type").value=="creditsale"){
	if($('#customername').val().trim()==""){
		$('#CustomerError').show();
		$('#customername').focus();
		return false;
	}
	 if($('#phoneno').val().trim()==""){
		$('#PhoneError').show();
		$('#phoneno').focus();
		return false;
	}
	}
	if(($('#cash_type option:selected').val()=="vehicle" || $('#cash_type option:selected').val()=="other" )){
		if($('#vocharNumber').val().trim()==""){
			$('#VoucherError').show();
			$('#vocharNumber').focus();
			return false;
		}
	}
	if(document.getElementById("product0").value==""){
		document.getElementById("product0").className = 'borderred';
		document.getElementById("product0").placeholder  = 'Find a product here';
		document.getElementById("product0").focus();
		return false;
	}
	if(document.getElementById("tenderCash").value=="0.00"){
		document.getElementById("tenderCash").className = 'borderred';
		document.getElementById("tenderCash").focus();
		return false;
	}
	var total=$("#total").val();
	var tenderCash=$("#tenderCash").val();
	var returnCash=$("#returnCash").val();
	 var status = confirm('Please check Following Details : \n\n Total Bill Amount : '+total+' \n Total Tendered Cash : '+tenderCash+' \n Change Return : '+returnCash+' \n\n Click OK to Proceed');
	   if(status == false){
	   return false;
	   }
	   else{
	   return true; 
	   }
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function productsearch(j){
		  var data1=$('#product'+j).val();
	  var arr = [];
	  $.post('searchSubhead.jsp',{q:data1,page:'sale',hoa:'3'},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var gross=0;
		 var qntystore=new Array();
		 var sellrate=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			/* gross=Number(parseFloat(d[4])+Number(d[4])*(Number(parseFloat(d[5])/100))); */
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[4],
			        vat :d[5],
			        qntystore :d[6],
			        sellrate :d[7],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
			$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#purchaseRate'+j).val('0');
				$('#grossAmt'+j).val('0');
				//$('#sellprice'+j).val(Math.ceil(ui.item.sellrate));				
				//$('#vat'+j).val(ui.item.vat);
	 			calculate(j);
	 			
	 			/* if(ui.item.qntystore>0){
				$(this).val(ui.item.label);
				$('#purchaseRate'+j).val(Math.round(ui.item.amount));
				$('#vat'+j).val(ui.item.vat);
				calculate(j);
				} else{
				alert("There is no stock for this product.");
				$('#product'+j).val('');
				
				} */
			}
		});
			});
} 
function calculate(i){
	var Totalprice=0.00;
	var grossprice=0.00;
	var u=0;
	u=document.getElementById("addcount").value;
	if(document.getElementById("product"+i).value!=""){
		if(document.getElementById("grossAmt"+i).value!="")
		{
			for(var j=0;j<u;j++){
				if(document.getElementById("purchaseRate"+j).value!="" && document.getElementById("quantity"+j).value!=""){
					grossprice=Number(parseFloat(document.getElementById("purchaseRate"+j).value)*(Number(parseFloat(document.getElementById("quantity"+j).value))));
					document.getElementById("grossAmt"+j).value=grossprice.toFixed(2);
					Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice)).toFixed(2);
				}
				}
			document.getElementById("total").value=Math.round(Totalprice);
		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;

}
function addFields(){
	document.getElementById("pooja").style.display="none";
	if(document.getElementById("cash_type").value=="creditsale"){
		document.getElementById("custHead").style.display="block";
	}else{
		document.getElementById("custHead").style.display="none";
	}
	if(document.getElementById("cash_type").value=="vehicle"){
		document.getElementById("pooja").style.display="block";
	}
}
function changeReturnAmount(){
	var totAmt=$('#total').val();
	var tenderAmt=$('#tenderCash').val();
	document.getElementById("returnCash").value=Number(tenderAmt)-Number(totAmt);
}
function getPoDetails(){
	var poid=$("#cash_type").val();
	var venId=$("#poojaItem").val();
	//alert(venId);
	$("#cashType").val(poid);
	$("#poojaitem").val(venId);
	$("#poidReload").submit();
	//location.reload('godwanForm.jsp?poid='+poid); 
	//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
	
}
/* function tabCall(cou){	
	$("#product"+cou).keydown(function(e) {
	var keyCode = e.keyCode || e.which; 
	if (keyCode == 9) { 
		 var data1=$('#product'+cou).val().split(" ");
		 var proID=data1[0];
		 if(proID==""){
				document.getElementById("product"+cou).className = 'borderred';
				document.getElementById("product"+cou).placeholder  = 'Find a product here';
				document.getElementById("product"+cou).focus();
				return false;
			}
		 $.post('searchSubhead.jsp',{q:proID,page:'tabPress',hoasj:'3'},function(data)
		{
			 document.getElementById("product"+cou).className = '';
			 document.getElementById("product"+cou).placeholder  = 'Find a product here';
			 var response = data.trim().split("\n");
			 if(response=="Subhead Not Found"){
				 	//alert("There is no product found for your search.Please try another.");
				 	$('#product'+cou).val('');
				 	$('#purchaseRate'+cou).val('');
				 	$('#vat'+cou).val('');
				 	$('#grossAmt'+cou).val('');
				 	document.getElementById('product'+cou).className = 'borderred';
					document.getElementById('product'+cou).placeholder  = 'Product Not found.Try another';
					document.getElementById("product"+cou).focus();
			 }else{
				 for(var i=0;i<response.length;i++){
					 var d=response[i].split(",");
					 $("#product"+cou).val(d[0]+" "+d[1]);
					 $('#purchaseRate'+cou).val(Math.round(d[4]));
					 $('#grossAmt'+cou).val(Math.ceil(d[7]));
					 $('#sellprice'+cou).val(Math.ceil(d[7]));				
					 $('#vat'+cou).val(d[5]);
			 		 calculate(cou); 
				 } 
			 }
		});
	}
});
} */
function deleteProduct(i){
	$("#product"+i).val('');
	$('#quantity'+i).val('1');
	 $('#purchaseRate'+i).val('');
	 $('#grossAmt'+i).val('');
	 $('#sellprice'+i).val('');				
	 $('#vat'+i).val(''); 
	 calculate('0');
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="SAM" class="beans.poojasaaman"></jsp:useBean>
<%

no_genaratorListing ng_LST=new no_genaratorListing();
if(session.getAttribute("empId")!=null){
	double totalAmt=0.00;
	%>
<div><%@ include file="title-bar.jsp"%></div>
<form name="poidReload" id="poidReload" action="shopSale.jsp" method="get">
<input type="hidden" name="cashtype" id="cashType" value=""></input>
<input type="hidden" name="poojaitem" id="poojaitem" value=""></input>
</form>
<form name="tablets_Update" method="post" action="shopSaleInsert.jsp" onsubmit="return validate();">
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Shop</div>
<div class="vender-details">
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());%>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>

<div class="warning"  id="dateError" style="display: none;">Please select date.</div>Date</td>
<td >
<input type="hidden" name="bid" id="bid" value="<%=request.getParameter("bid")%>"/>
Bill no</td>
</tr>
<tr>
<td><input type="text" name="date" id="date"  value="<%=currentDate%>" class="DatePicker" readonly="readonly"/></td>
<td><input type="text" name="billnumb" id="billnumb" value="<%=ng_LST.getid("shopsaleinvoice")%>"  readonly="readonly"/></td>


</tr>
<tr>
<td>Payment Type*</td>
<td></td>
<td></td>
</tr>
<tr>
<td>
<input type="hidden" name="formType"  value="sale-subhead"></input>
<select name="cash_type" id="cash_type" onchange="addFields();">
<option value="cash" <%if(request.getParameter("cashtype")!=null && request.getParameter("cashtype").equals("cash")){ %> selected="selected" <%} %>>CASH</option>
</select></td>
</tr>
<tr>
	<td><div class="warning" id="CustomerError" style="display: none;">Please Provide  "Customer Name".</div>
	Customer Name*</td>
	<td><div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone No".</div>
	Phone*</td>
	</tr>
	<tr>
	<td><input type="text"  name="customername" id="customername" /></td>
	<td><input type="text"  name="phoneno" id="phoneno" /></td>
	</tr>

</table>
</div>
<div style="clear:both;"></div>
</div>



<div class="vendor-list" style="">
<div class="bgcolr"  style=" float:left;width:100%;">

<div class="sno1 bgcolr">S.No.</div>
<div class="ven-nme1 bgcolr" style="width:250px; ">SUB HEAD NAME</div>
<div class="ven-nme1 bgcolr">QTY</div>
<div class="ven-nme1 bgcolr">PRICE</div>
<div class="ven-amt1 bgcolr">VAT(%)</div>
<div class="ven-amt1 bgcolr" style="width:auto;float:left; ">GROSS AMOUNT</div>

</div>

<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 50; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

<div class="sno1 MT13"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1" style="width:250px; cursor: pointer;"> <img src='images/delete.png' height="20px;" width="20px;" onclick="deleteProduct('<%=i%>')" />
<input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a subhead here"  autocomplete="off"/></div>
<div class="ven-nme1">
<input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  onkeyup="calculate('<%=i %>')" value="1"  autocomplete="off" onKeyPress="return numbersonly(this, event,true);" style="width: 50px;"/></div>
<div class="ven-nme1">&#8377;<input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" onkeyup="calculate('<%=i %>')" value="0"  style="width: 150px;"></input></div>
<div class="ven-amt1">
<input type="text" name="vat<%=i%>" id="vat<%=i%>" readonly="readonly" value="0" style="width:50px;"/>
<input type="hidden" name="purchaseRate<%=i%>" id="sellprice<%=i%>" value=""/></div>
<div class="ven-amt1" style="width:210px;">&#8377;<input type="text" name="grossAmt<%=i%>" id="grossAmt<%=i%>" value="0" readonly="readonly"/></div>
<div class="clear"></div>
</div>
<%} %>
<div class="add-ven">
<input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/>
<span style="margin:0 80px 0 0">&nbsp;</span>
<strong class="bold">Total</strong> &nbsp;&nbsp;&nbsp;<input type="text" name="total" id="total" value="<%=totalAmt %>" readonly="readonly" style="width:55px;"/>&nbsp;&nbsp;&nbsp;
<strong class="bold">Tendered Cash</strong>&nbsp;&nbsp;&nbsp;<input type="text" name="tenderCash" id="tenderCash" value="0.00" required style="width:60px;" onkeyup="changeReturnAmount();" onKeyPress="return numbersonly(this, event,true);"/>&nbsp;&nbsp;&nbsp;
<strong class="bold">Change return</strong>&nbsp;&nbsp;&nbsp;<input type="text" name="returnCash" id="returnCash" value="0.00" readonly="readonly" style="width:50px;"/>
<span style="margin:0 30px 0 0">&nbsp;</span>
<input type="reset" value="Reset" class="click" style="border:none;"/>
<input type="submit" name="saveonly" value="Save" class="click" style="border:none; "/>
<input type="submit" name="saveprint" value="Save&Print" class="click" style="border:none; "/>
</div>
<div class="tot-ven"></div>

</div>



</div>
</form>
<%}else{
	response.sendRedirect("index.jsp");
} %>



<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>


<script>
function print()
{ 
if(document.getElementById("bid").value != 'null'){
newwindow=window.open('printReceipt.jsp?bid='+document.getElementById("bid").value+'&recName=shopsale','name','height=600,width=500,menubar=yes,status=yes,scrollbars=yes');
//newwindow=window.open('print.jsp?rmid=RUM1000017','name','height=600,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus();}

}
}
window.onload=print();
window.onload=addFields();
</script>
</body>
</html>