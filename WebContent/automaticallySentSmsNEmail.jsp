<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="beans.sent_sms_email"%>
<%@page import="mainClasses.sent_sms_emailListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
	}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AUTOMATICALLY SENT SMS AND EMAIL LIST | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

function validateForm() 
{
    var type = document.forms["searchForm"]["type"].value;
    
    if (type == null || type == "") 
    {
        alert("Please Select Category");
        return false;
    }
} 
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                    $( "a" ).css("text-decoration","none");
                    $( ".printable" ).print();
                   

                    // Cancel click event.
                    return( false );
                });
			});
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
});
</script>
 <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:96%; 
    background:#d0e3fb; 
}
</style>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="SSE" class="beans.sent_sms_email"/>
<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
	<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-list">
		<div class="arrow-down">
		</div>
		<form name="searchForm" id="searchForm" action="automaticallySentSmsNEmail.jsp" method="post" onsubmit="return validateForm()">
		<div class="search-list">
			<ul>
				<%
				String typeNew = request.getParameter("type"); 
				String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
				String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
				if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals(""))
				{
					 fromdate=request.getParameter("fromDate");
					 todate=request.getParameter("toDate");
				}
				%>
				<li style="float:left;" ><select name="type" style="width:220px;">
				<%
	if(typeNew == null)
	{%>
		<option value="">-- SELECT --</option>
	<%}%>
					<option value="ALL" <%if(typeNew != null && typeNew.equals("ALL")) {%> selected="selected"<%} %>>ALL</option>
					<option value="NAD" <%if(typeNew != null && typeNew.equals("NAD")) {%> selected="selected"<%} %>>NAD</option>
					<option value="LTA" <%if(typeNew != null && typeNew.equals("LTA")) {%> selected="selected"<%} %>>LTA</option>
					<option value="Birthday" <%if(typeNew != null && typeNew.equals("Birthday")) {%> selected="selected"<%} %>>Birthday</option>
					<option value="Anniversary" <%if(typeNew != null && typeNew.equals("Anniversary")) {%> selected="selected"<%} %>>Anniversary</option>
					</select>
				</li>
				<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly/></li>
				<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li> 
			<li><input type="submit" class="click" name="search" value="Search"></input></li>
			</ul>
		</div></form>
		<div class="icons">
			<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
			<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
		</div>
		<div class="clear"></div>
		<%if((request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")) && (request.getParameter("toDate")!=null && !request.getParameter("toDate").equals(""))){ %>
		<div class="list-details">
		<table width="100%" cellpadding="10" cellspacing="10" >
				<tr><td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td></tr>
				<tr><td colspan="9" align="center" style="font-weight: bold;"> (Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
				<tr><td colspan="9" align="center" style="font-weight: bold;">AUTOMATICALLY SENT SMS AND EMAIL LIST</td></tr>
		</table>
		</div>
		
		<div class="list-details ">
		  <div class="printable">
		  <table width="100%"  cellpadding="1" cellspacing="1" class="print_table">
			<tr>
				<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
				<td class="bg" width="10%" style="font-weight: bold;">ID</td>
				<td class="bg" width="15%" style="font-weight: bold;">SENT ON</td>
				<td class="bg" width="20%" style="font-weight: bold;">SENT ON THE NAME OF</td>
				<td class="bg" width="10%" style="font-weight: bold;">PURPOSE</td>
				<td class="bg" width="10%" style="font-weight: bold;">DAYS REMAINDER</td>
				<td class="bg" width="20%" style="font-weight: bold;">EMAIL SENT TO</td>
				<td class="bg" width="10%" style="font-weight: bold; ">SMS SENT TO</td>
			</tr>
			</table>
		<%
		SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat dateFormat2=new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
		SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
		
		sent_sms_emailListing SSEL = new sent_sms_emailListing();
		
		String fromDate=fromdate+" 00:00:01";
		String toDate=todate+" 23:59:59";
		
		String type = request.getParameter("type");
		
		List SSE_LIST=SSEL.getSentSmsEmailBasedOnDates(fromDate, toDate, type);%>
		
		<div id="tblExport">
		<%
		if(SSE_LIST.size()>0){%>
		<table width="100%" cellpadding="0" cellspacing="0" class="print_table">
		
		<tr>
		<td colspan="9" style="padding:0px;margin:0px;" class="yourID" >
		</td></tr>
	<%
	for(int i=0; i < SSE_LIST.size(); i++ )
	{ 
		SSE=(sent_sms_email)SSE_LIST.get(i); %>
		<tr>
	    <td  colspan="9">
	    <table width="100%"  cellpadding="1" cellspacing="1" class="print_table">
			<tr style="position: relative;" >
				<td width="5%"><%=i+1%></td>
				<td width="10%"><%=SSE.getRegId()%></td>
				<td width="15%"><%=dateFormat2.format(dbDateFormat.parse(SSE.getCreatedDate()))%></td>
				<td width="20%"><%=SSE.getName()%></td>
				<td width="10%"><%=SSE.getPurpose()%></td>
				<td width="10%"><%=SSE.getDaysremainder()%></td>
				<td width="20%"><%=SSE.getEmail()%></td>
				<td width="10%"><%=SSE.getPhone()%></td>
			</tr>
		</table>
		</td></tr>
	<% 	
	}
		
	}else{%>
	<div align="center">
		<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>EMPTY LIST!</span></div>
	</div>
	<%}%> 
	</div>
	</div> 
	 <%} %> 
	 </div>
	 </div>
			</div>
			</div>
</div>
</div>
</table></div></div></div></div></div></div>
	<!-- main content -->
<%}else{response.sendRedirect("index.jsp");} %> 
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
		
</body>
</html>