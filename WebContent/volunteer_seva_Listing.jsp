<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@page import="beans.volunteerseva"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.volunteerSevaListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
    <script src="ui/jquery.ui.core.js"></script>
    <script src="ui/jquery.ui.datepicker.js"></script>
<!-- <link href="../js/date.css" rel="stylesheet" type="text/css" /> -->
<!-- <link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/> -->
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script> 
 <script src="js/jquery-1.4.2.js"></script>  
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>  
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script> 
<script src="js/nicEdit-latest.js" type="text/javascript"></script>

<!-- <script type="text/JavaScript">
function VALIDATE()
{   if(document.getElementById("date").value==""){
	alert("Please Select Date");
return false;
} 
}</script> -->

<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<style>
	
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
/*.bod-nn{
	border-right:none !important;}*/
	}
/*	.bod-nn{
	border-right:1px solid #000 !important;
	font-weight: bold;}*/
</style>


<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
});
</script>

<Script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</Script>

<script type="text/javascript">bkLib.onDomLoaded(function() {
    nicEditors.editors.push(new nicEditor().panelInstance(document.getElementById('long_desc')));
    $("div.nicEdit-main").keydown(function(event) {
        if (event.ctrlKey==true && (event.which == '118' || event.which == '86')) {
		//alert('key presse');
		event.preventDefault();
		modalWindowRTF.windowId = "myModal";
		modalWindowRTF.width = 550;
		modalWindowRTF.height = 260;
		modalWindowRTF.open();
	 }
    });
 });
function validate(){
	var nicE = new nicEditors.findEditor('long_desc');
	var question = nicE.getContent();
	$("#long_desc").val(question); // Value What we entered should be passed to another page.
	}
</script>

<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<script language = "JavaScript">

//----for check atleast one check box-----
function check1(){
	var inputs = document.getElementsByTagName("input");
	var msg=document.groupForm.message.value;
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one");
	return false;} 
	
	else{
		return true;
	}
}

</script>

 <!-- <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script> -->
    
        <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>

</head>
<body>

<%if(session.getAttribute("empId")!=null){ 
%>
<div><%@ include file="title-bar.jsp"%></div>
<table width="100%" border="0" style="margin: 0px auto;" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table width="980" border="0" cellspacing="0" cellpadding="0" style="margin : 0px auto;" class="normal_text view">
<tr>
<td colspan=2 height=30> </td>
</tr>
<form name="leavepermission_Update" method="POST" action="volunteer_seva_Listing.jsp">
<tr>
 <!-- <td>Select Date</td>
<td><input type="text" style="width: 105px;" name="date" id="date" value="" placeHolder="Select Date" readonly="readonly"></td> -->
<td><select name="role">
<option value="all">Select All</option>
<option value="Q.MAINTENANCE">Q.MAINTENANCE</option>
<option value="PRASADAM PACKING">PRASADAM PACKING</option>
<option value="ANNADANAM (DAILY)">ANNADANAM (DAILY)</option>
<option value="ANNADANAM (Thursday)">ANNADANAM (Thursday)</option>
<option value="HUNDI COLLECTION">HUNDI COLLECTION</option>
<option value="MEDICAL SERVICES">MEDICAL SERVICES</option>
<option value="STOTRA PARAYANAM">STOTRA PARAYANAM</option>
<option value="PALLIQUEN PROCESSION">PALLIQUEN PROCESSION</option>
<option value="IMPORTANT EVENTS">IMPORTANT EVENTS</option>
<option value="FESTIVALS">FESTIVALS</option>
</select>
 
</td>
<td ><input type="submit" name="Submit" value="SUBMIT" class="click"/></td>
</tr>
</form>
</table>


  	<div class="icons">
<span><a id="print"><img src="./images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="./images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
  	<div class="vendor-page">
<div class="vendor-list">

<div class="clear"></div>
<div class="list-details">
	 <div class="printable">
	  <form name="groupForm" method="POST" action="sendMessageGroups.jsp" onSubmit="return check1();">	
	  <input type="hidden" name="volseva" value="volseva"/>	
	 <table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
			<tr><td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;">Volunteer Seva List Based On Date</td></tr>

	 <tr style="background-color: #e3eaf3;color: #7490ac;">   
         
         <th align="left"   width="11%" style="font-weight: bold;"><input type="checkbox" name="SelectAll" id="selecctall"/>Check All</th>
         	<th align="center"  >S.No</th>
         	<th >Seva Name </th>
         	<th>SSSST ID </th>
         	<th>Volunteer Name</th>
         	<th>Phone Number</th>
        	 <th>Email Id</th>
          </tr>
          <tr><td>&nbsp;</td><td></td></tr>
          <jsp:useBean id="VS" class="beans.volunteerseva"/>
          <jsp:useBean id="SSSS" class="beans.sssst_registrations"/>
          <%
          mainClasses.volunteerSevaListing VSL = new mainClasses.volunteerSevaListing();
          sssst_registrationsListing  SSSST=new sssst_registrationsListing();
	System.out.println("seva role..."+request.getParameter("role"));
/* List SFT_AllList=SFT_CL.getALLByStaffcategory(request.getParameter("id").replace("@"," ").replace("*","&"),request.getParameter("yearss")); */
List VSL_AllList=null;

if(request.getParameter("role")==null || request.getParameter("role").equals("")){
VSL_AllList=VSL.getVolunteerSevasListBasedOnDate("all");
}
else{
VSL_AllList=VSL.getVolunteerSevasListBasedOnDate(request.getParameter("role"));
}
if(VSL_AllList.size()!=0)
{	
	for(int i=0; i < VSL_AllList.size(); i++ )
	{
		VS=(beans.volunteerseva)VSL_AllList.get(i);  
       List SSSST_List=SSSST.getMsssst_registrations(VS.getSssstId());
       if(SSSST_List.size()!=0){
       		for(int j=0;j<SSSST_List.size();j++)
       		{
       			SSSS=(sssst_registrations)SSSST_List.get(j);
       %>
       <tr align="center">
       
       <td align="left" width="11%"><input type="checkbox" name="select1" class="checkbox1" value = "<%=VS.getSssstId()%>"></td>
	<td height=30 ><%= i+1 %> </td>
<td><%=VS.getSevaName() %></td>
<td><%=VS.getSssstId() %></td>
<td><%=SSSS.getfirst_name() %></td>
<td><%=SSSS.getmobile() %></td>
<td><%=SSSS.getemail_id() %></td>
</tr>
<%}}}}%>
</table>
</div>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
<td colspan="3" align="right" >Template</td>
<td colspan="3" align=left>
	<textarea name="message" id="long_desc" rows="4" cols="40" placeHolder="Write your message here!"   ></textarea>
	<br><span>Note: Enter 160 characters message only!</span>
	<br><input type="text" name="subject" value="" placeHolder="Write E-Mail Subject here!"/>
</td>
<td colspan="4" align="left">
<!-- <input type="submit" class="click" name="submit" value="SEND SMS"><br/> 
<input type="submit" name="submit" class="click" value="SEND MAIL'S"><br/>  -->
<input type="submit" name="submit" class="click" value="SEND SMS&MAIL"></td>
</tr>
</table>
</form>

</div>
</div>
</div>

 <%}%>
 </td>
 </tr>
 </table>

<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
          
</body>
</html>