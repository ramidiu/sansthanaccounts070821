<%@page import="java.util.List"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.patientsentry"%>
<%@page import="mainClasses.patientsentryListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.appointments"%>
<%@page import="mainClasses.appointmentsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<jsp:useBean id="APT" class="beans.appointments"></jsp:useBean>
<jsp:useBean id="PAT" class="beans.patientsentry"></jsp:useBean>
<title>Medical Receipt</title>
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>

<style>
@media print {
	@page  
{ 
 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin:2.6	cm 0 0 15mm; /* equals to 0.8inches */ 
}
.medicalsheet
{
	width:800px;  height:auto; padding:10px; background:#3F6; border:none !important;
}

.medicalsheet table
{
	border:none !important;
}

.medicalsheet table td
{
	 padding:7px 0px; text-align:center; font-size:15px; color:#000; text-transform:uppercase;  border:none !important;
}

.medicalsheet table td span
{
	font-size:15px; font-weight:bold;
}

.medicalsheet table td ul
{
	margin:0px; padding:10px; float:left;
}

.medicalsheet table td ul li
{
	list-style:none; float:left; margin:0px 0 45px 0; background:#F00; font-size:15px;
}

.medicalsheet table td ul li.br1
{
 width:350px; margin:0 15px 10px 15px; border:none !important;
}

.medicalsheet table td ul li.br2
{
 width:240px !important;margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br3
{
 width:112px; margin:0 0px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br4
{
 width:350px; margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br5
{
 width:25px;margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br6
{
 width:78px; margin:0 0px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br7
{
 width:380px; margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br8
{
 width:200px; margin:0 0px 10px 15px;  border:none !important;
}
.medicalsheet table td ul li.br9
{
 width:150px; margin:0 15px 10px 15px; border:none !important;
}
.medicalsheet table td ul li.br10
{
width:150px; margin:0 15px 10px 15px; border:none !important;
}

.medicalsheet table td ul li.br11
{
 width:293px; margin:0 0px 10px 15px border:none !important;
}
.medicalsheet table td.ht
{
	height:940px;
}
.medicalsheet table td.ht1
{
	height:1210px;
}

}
</style>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
</head>

<body onload="print();">
<%
SimpleDateFormat DBdate=new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
SimpleDateFormat userdate=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat Time=new SimpleDateFormat("HH:MM:ss");
appointmentsListing APT_L=new appointmentsListing();
patientsentryListing PAT_L=new patientsentryListing();
doctordetailsListing DOCT_L=new doctordetailsListing();
List APTL=APT_L.getappointmentsMedicineNotIssued(request.getParameter("id").toString());
if(APTL.size()>0){
APT=(appointments)APTL.get(0);
}
List PATL=PAT_L.getMpatientsentry(APT.getpatientId());
if(PATL.size()>0){
PAT=(patientsentry)PATL.get(0);
}
%>
<div class="vendor-page">
	<div class="vendor-list">
<!-- <div class="icons"><a id="print"><span><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
				<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
				</div> -->
<div class="printable">
<div class="medicalsheet">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3">
<ul style="">
<!--<li>Patient ID No :</li>-->
<li class="br1"><%=APT.getappointmnetId() %></li>
<!--<li>Date :</li>-->
<%if(!APT.getdate().equals("")){ %>
<li class="br2"> <%=userdate.format(DBdate.parse(APT.getdate()))%> </li>
<%} %>
<!--<li>Time :</li>-->
<%if(!APT.getdate().equals("")){ %>
<li class="br3"> <%=Time.format(DBdate.parse(APT.getdate())) %> </li>
<%} %>
<!--<li>Patient Name :</li>-->
<li class="br4"><%=PAT.getpatientName() %></li>
<!--<li>Age :</li>-->
<li class="br5">24</li>
<!--<li>Sex :</li>-->
<li class="br6"><%=PAT.getextra1() %></li>
<!--<li style="text-align:left; width:100px;">Address<span style="float:right">:</span></li>-->
<li class="br7" style="width:330px;"><%=PAT.getaddress() %></li>
<!--<li>Ref. Doctor :</li>-->
<li class="br8"><%=DOCT_L.getDoctorName(APT.getdoctorId())%></li>
<!--<div style="clear:both"></div>
<li>Height :</li>
<li class="br9">&nbsp;</li>
<li>Weight :</li>
<li class="br10">&nbsp;</li>
<li>B.P. :</li>
<li class="br11">&nbsp;</li>-->
</ul>
</td>
</tr>
</table>

</div>

<!--<div class="medicalsheet" style="border:none; margin:10px 0 0 0">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>

<td width="27%">Date</td>
<td width="45%">Notes</td>
<td width="28%">Remarks</td>
</tr>
<tr>

<td width="27%" class="ht1">&nbsp;</td>
<td width="45%" class="ht1">&nbsp;</td>
<td width="28%" class="ht1">&nbsp;</td>
</tr>
</table></div>-->
</div> 
</div>
</div>              

</body>
</html>