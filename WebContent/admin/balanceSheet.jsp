<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.util.TimeZone"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Receipts And Payments Report</title>

<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
</head>

<body>
<div class="vendor-page">
<div class="vendor-list">


<div style="text-align: center;"></div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="total-report">
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
		majorheadListing MH_L=new majorheadListing();
				List majhdlist=null;%>
				<form name="BalanceSheet" id="Balancesheet"  method="post">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="4" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;" class="bg-new">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;" class="bg-new">BALANCE SHEET REPORT</td></tr>
<tr>
<td width="20%">

<ul>
<%List HA_Lists=HOA_L.getheadofaccounts();
String fromdate="2014-04-01";
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<li><input type="radio" name="hoid" value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> checked="checked" <%} %> /> <%=HOA.getname() %></li><%}} %>
<li><input type="radio" name="hoid" value="" checked="checked">ALL</input></li>
</ul></td>
<td width="30%"><input type="hidden" name="page" value="balanceSheet"/>From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="30%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" value="Submit" class="click bordernone"/></td>
</tr>
<!-- <tr>
<td><input type="submit" value="Today" class="click bordernone"/></td>
<td><input type="submit" value="Current Week" class="click bordernone"/></td>
<td><input type="submit" value="Current Month" class="click bordernone"/></td>
<td><input type="submit" value="Current Year" class="click bordernone"/></td>
</tr> -->
</table>
</form>
<%
customerpurchasesListing CP_L=new customerpurchasesListing();
String finyr[]=null;	
				double credit=0.0;
				double debit=0.0;
				DecimalFormat bd=new  DecimalFormat("#,###.00");
				productexpensesListing PEXP_L=new productexpensesListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				Calendar c1 = Calendar.getInstance(); 
				TimeZone tz = TimeZone.getTimeZone("IST");
				String hoaid[]=null;
				if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
					hoaid=new String[]{""+request.getParameter("hoid")};
				} else {
				/* hoaid=new String[]{"1","3"}; */
				 hoaid=new String[]{"1","3","4","5"}; 
				}
				String currentDate=(onlydat.format(c1.getTime())).toString();
				String finyrfrm=fromdate+" 00:00:01";
				String finyrto=todate+" 23:59:59";
				
				%>
				<div id="tblExport">
<div style="float:left; width:628px; margin:0 0px 10px 0">
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr>
<td colspan="3" class="bg-new">Expenditure</td>
</tr>
<tr>
<td>Particulars</td>
<td colspan="2">Amount</td>
</tr>
<%for(int h=0;h<hoaid.length;h++)
								{
								majhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid[h],"expenses");
											if(majhdlist.size()>0){ 
								for(int i=0;i<majhdlist.size();i++){
									MH=(majorhead)majhdlist.get(i);
									if(!PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"").equals("00")){
									%>

<tr>
<td ><a href="adminPannel.jsp?page=minorheadTrailBalanceReport&majrhdid=<%=MH.getmajor_head_id()%>&fromDate=<%=onlydat.format(dbdat.parse(finyrfrm)) %>&toDate=<%=onlydat.format(dbdat.parse(finyrto)) %>"><%=MH.getname() %></a></td>
<td colspan="2"><%=bd.format(Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"")))%></td>
						
								<%debit=debit+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,""));
								%>
</tr>
<%}}}} %>

<tr><td colspan="1" class="bg-new">Total</td><td colspan="1" class="bg-new"><%=bd.format(debit) %></td></tr>

</table>
</div>


<div style="float:left; width:628px; margin:0 0px 10px 0">
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr>
<td colspan="3" class="bg-new">Income</td>
</tr>
<tr>
<td>Particulars</td>
<td colspan="2">Amount</td>
</tr>
<%for(int h=0;h<hoaid.length;h++)
								{
								majhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid[h],"revenue");
											if(majhdlist.size()>0){ 
								for(int i=0;i<majhdlist.size();i++){
									MH=(majorhead)majhdlist.get(i);
									if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]).equals("00") || !CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]).equals("00") ){
									%>

<tr>
<td ><a href="adminPannel.jsp?page=minorheadTrailBalanceReport&majrhdid=<%=MH.getmajor_head_id()%>&fromDate=<%=onlydat.format(dbdat.parse(finyrfrm)) %>&toDate=<%=onlydat.format(dbdat.parse(finyrto)) %>"><%=MH.getname() %></a></td>

								<%if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]).equals("00")){ %>
								<td colspan="2"><%=bd.format(Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]) ))%>		</td>
								<%credit=credit+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h])); } else{ %>
								<td colspan="2"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]))) %>		</td>
								<%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h])); } %>
</tr>
<%}}}} %>

<tr><td colspan="1" class="bg-new">Total</td>
<td colspan="1" class="bg-new"><%=bd.format(credit) %></td></tr>

</table>
</div>
<!--<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" class="bg-new">Total</td><td colspan="1" ><%=bd.format(debit) %></td><td colspan="1" class="bg-new">Total</td>
<td colspan="1"><%=bd.format(credit) %></td></tr>
<tr></tr></table>-->
</div>
</div>
</div>
</div>
</body>
</html>