<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.util.List"   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Employee</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif; font-size:16px;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../js/jquery-1.8.2.js"></script>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="../css/demos.css" />
<script>
$(function() {
	$( "#join_date" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

function validate(){
	$('#headOfAccountError').hide();
	$('#firstNameError').hide();
	$('#emailError').hide();
	
	if($('#headAccountId').val().trim()==""){
		$('#headOfAccountError').show();
		$('#headAccountId').focus();
		return false;
	}
	
	if($('#firstname').val().trim()==""){
		$('#firstNameError').show();
		$('#firstname').focus();
		return false;
	}
	
	
	if($('#email').val().trim()!=""){
		var email=$("#email").val();
		if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))  
		  {
			$('#emailError').show();
			$('#email').focus();
			return false;
			
		  }
	}
	
			
}
function combochange(denom,desti,jsppage) {
var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value; 
document.getElementById(desti).options.length = 0;
var sda1 = document.getElementById(desti);
$.post(jsppage,{ id: com_id } ,function(data)
	{
var where_is_mytool=data;
//alert(where_is_mytool);
var mytool_array=where_is_mytool.split("\n");
var ab=mytool_array.length-5;
//alert(mytool_array.length);
for(var i=0;i<mytool_array.length;i++)
{
if(mytool_array[i] !="")
{
//alert (mytool_array[i]);
var y=document.createElement('option');
var val_array=mytool_array[i].split(":");

			y.text=val_array[1];
			y.value=val_array[0];
			try
			{
			sda1.add(y,null);
			}
			catch(e)
			{
			sda1.add(y);
			}
}
}
});
} 
</script>


</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>	
<body>
<div class="vender-details">
<form method="POST" action="employees_Insert.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Employee Information</span></td>
</tr>
<tr>
<td width="50%">
<table width="100%" cellpadding="0" cellspacing="0">

<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
 <tr>
<td>
<div class="warning" id="headOfAccountError" style="display: none;">please Select "Head Of Account".</div>
Head Of Account*</td>
<td>Head group*</td>
<td>Role*</td>
</tr>
<tr>
<td >
<select name="headAccountId" id="headAccountId" onChange="combochange('headAccountId','headGroup','getheadGroups.jsp')">
<option value="">--select--</option>
<%mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();
for(int i=0; i < HOA_List.size(); i++ ){
	HOA=(beans.headofaccounts)HOA_List.get(i);%>
	<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname()%></option>
	<%}%>
	<option value="0">All(Purchase Officer)</option>
</select>
</td>
<td><select name="headGroup" id="headGroup">
	<option value="">Select group</option>
</select></td>
<td>
<select name="employeerole" id="employeerole" style="width:350px;">
<option value="">--select--</option>
<option value="Employee">Employee</option>
<option value="Manger">ACCOUNTS MANAGER</option>
<option value="account">ACCOUNTANT</option>
<option value="poojacounter">poojacounter</option>
<option value="procounter">procounter</option>
<option value="PO">Purchase Officer</option>
<option value="AsstPO">Asst Purchase Officer</option>
<option value="medicalhall">Medical Hall</option>
<option value="medicalreception">Medical Reception</option>
<option value="PS.STORE SUPERVISOR">PS.STORE SUPERVISOR</option>
<option value="Sainivas StoreKeeper">Sainivas StoreKeeper</option>
<option value="SainivasRestaurant">Sainivas Restaurant</option>
<option value="Catering Supervisor">Catering Supervisor</option>
<option value="Kitchenstore Supervisor">Kitchenstore Supervisor</option>
<option value="Auditor">Auditor</option>
</select>
</td>
</tr> 
<tr>
<td>
<div class="warning" id="firstNameError" style="display: none;">"First Name" Required</div>First Name*</td>
<td>Last Name</td>
<td><div class="warning" id="emailError" style="display: none;">Please Provide Valid "Email".</div>Email</td>
</tr>
<tr>
<td>
<input type="text" style="width:150px;" name="firstname" id="firstname" /></td>
<td><input type="text"  name="lastname" id="lastname"></td>
<td><input type="text" name="email" id="email"></td>
</tr>

<tr>
<td colspan="2">Address</td>
<td></td>
</tr>
<tr>
<td colspan="2"><textarea placeholder="street" name="emp_address" id="emp_address"></textarea></td>
<td></td>
</tr>
<tr>
<td>Phone Number</td>
<td>Join Date</td>
<td>Employee Salary</td>
</tr>
<tr>

<td><input type="text" name="emp_phone" id="emp_phone" onKeyPress="return numbersonly(this, event,true);" /></td>
<td><input type="text" style="width:100px;" name="join_date" id="join_date" value="<%=currentDate%>"></td>
<td><input type="text" name="emp_sal" id="emp_sal" onKeyPress="return numbersonly(this, event,true);"></td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>
</body>
</html>