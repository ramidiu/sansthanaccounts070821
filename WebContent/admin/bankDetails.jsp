<%@page import="beans.bankdetails"%>
<%@page import="beans.banktransactions"%>
<%@page import="beans.customerpurchases"%>
<%@page import="org.apache.commons.collections.map.MultiValueMap"%>
<%@page import="com.accounts.saisansthan.JVListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="beans.productexpenses"%>
<%@page import="java.util.HashMap"%>
<%@page import="beans.subhead"%>
<%@page import="beans.minorhead"%>
<%@page import="java.util.Map"%>
<%@page import="beans.majorhead"%>
<%@page import="com.accounts.saisansthan.MapOfHeads"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.banktransactionsListing,java.util.Date"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(document).ready(function(){
$(function() {
	$( "#fromDate" ).datepicker({
		minDate: new Date(2020, 03, 1),
		maxDate: new Date(2021, 02, 31),
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		} 
	});
		$( "#toDate" ).datepicker({
			minDate: new Date(2020, 03, 1),
			maxDate: new Date(2021, 02, 31),
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		} 
	});
});
});

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	$('#fromDate').val(startDate.split(",")[2]+"-"+startDate.split(",")[1]+"-"+startDate.split(",")[0]);
	$('#toDate').val(endDate.split(",")[2]+"-"+endDate.split(",")[1]+"-"+endDate.split(",")[0]);
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
});

function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}

function changeDateFormat()	{
	var fromdate = document.getElementById('fromDate').value; 
	var todate = document.getElementById('toDate').value;
	document.getElementById('fromDate').value = fromdate.split("-")[2]+"-"+fromdate.split("-")[1]+"-"+fromdate.split("-")[0];
	document.getElementById('toDate').value = todate.split("-")[2]+"-"+todate.split("-")[1]+"-"+todate.split("-")[0];
	return true;
}

$(document).ready(function(){
$( "#reportType" ).change(function() {
	  $( "#searchForm" ).submit();
	});
});
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                	
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
     <script type="text/javascript">
 function updateJvNumber(bkt_id,jv_number)	{
 	$.ajax({
			url : "searchSubHead1.jsp",
 			type : "GET",
 			data : {bktId : bkt_id},
 			success : function(response)	{
 				console.log("respoonse"+response);
 				if (response.trim() !== "")	{
 			     	var jv = window.prompt("Enter Jv Number",response.trim());
 				}
 				else	{
 			     	var jv = window.prompt("Enter Jv Number","");
 				}
 			     	if (jv !== null && jv.trim() !== "")	{
 			     		$.ajax({
 			     			url : "searchSubHead1.jsp",
 			     			type : "POST",
 			     			data : {bktId : bkt_id , jv : $.trim(jv)},
 			     			success : function(response)	{
 			     				console.log("response--"+response);
 			     				if (response.trim() !== "")	{
 			     					alert("Number Updated Successfully");
 			     				}
 			     				else	{
 			     					alert("Number Not Updated");
 			     				}
 			     			}
 			     		});
 			     	}
 				}
 	});
 }
 </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
<jsp:useBean id="NUM" class="beans.NumberToWordsConverter"/>
<jsp:useBean id="BNKBAL" class="beans.bankbalance"/>
	<%String bankId=request.getParameter("id");
	DecimalFormat formatter = new DecimalFormat("#,##,###.00");
	DecimalFormat sjf = new DecimalFormat("####");
	DecimalFormat df = new DecimalFormat("0.00");
	mainClasses.bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	List BNK_List=BNK_CL.getMbankdetails(bankId); 
	if(BNK_List.size()>0){
		BNK=(beans.bankdetails)BNK_List.get(0);%>
<div class="vendor-page">
<div class="vendor-box">
<div class="name-title"><%=BNK.getbank_name()%> <span>(<%=HOA_CL.getHeadofAccountName(BNK.getheadAccountId())%>)</span></div>
<div class="click floatleft"><a href="./editBankDetails.jsp?id=<%=BNK.getbank_id()%>" target="_blank" onclick="openMyModal('editBankDetails.jsp?id=<%=BNK.getbank_id()%>'); return false;">Edit</a></div>

<%-- <div class="click floatleft" style="margin-left: 20px;"><a href="deleteBank.jsp?id=<%=BNK.getbank_id()%>" >Delete</a></div> --%>
<%-- <div class="click floatleft" style="margin-left: 20px;"><a href="editBankDetails.jsp?id=<%=BNK.getbank_id()%>" target="_blank" onclick="openMyModal('banktransactionsForm.jsp?id=<%=BNK.getbank_id()%>'); return false;">Deposit</a></div> --%>

<div style="clear:both;"></div>
<div class="details-list">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><span>Branch Name:</span></td>
<td><%=BNK.getbank_branch()%></td>
</tr>

<tr>
<td><span>Account Holder Name:</span></td>
<td><%=BNK.getaccount_holder_name()%></td>
</tr>

<tr>
<td><span>Account Number:</span></td>
<td><%=BNK.getaccount_number()%></td>
</tr>

<tr>
<td ><span>IFSC Code:</span></td>
<td><%=BNK.getifsc_code()%></td>
</tr>

<tr>
<td ><span>Total Amount:</span></td>
<td><%=formatter.format(Double.parseDouble(BNK.gettotal_amount()))%></td>
</tr>

</table>

</div>
<div class="details-amount">

 <ul>
 <%
 bankcalculations BNKCAL=new bankcalculations();
 bankbalanceListing BBAL_L=new mainClasses.bankbalanceListing();
 
 DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
 TimeZone tz = TimeZone.getTimeZone("IST");
 dateFormat2.setTimeZone(tz);
 
 Calendar c2 = Calendar.getInstance(); 
 String currentDateNew=(dateFormat2.format(c2.getTime())).toString();
 
 if(request.getParameter("todate") != null && !request.getParameter("todate").equals(""))
 {
	 currentDateNew = request.getParameter("todate");
 }
 
 String monthInString=currentDateNew.substring(5, 7);
 int month = Integer.parseInt(monthInString);
 String yearInString=currentDateNew.substring(0, 4);
 int year = Integer.parseInt(yearInString);
 
 String finStartYr2 = "";

 if(month >= 4)
 {
		finStartYr2 = Integer.toString(year);
 }
 else
 {
	 int finStartYrInInt = year - 1;
	 finStartYr2 = Integer.toString(finStartYrInInt);
 }
 
 String finStartAndEndYr = "";
 
 String tempInString = finStartYr2.substring(2, 4);
 int tempInInt = Integer.parseInt(tempInString)+1;
 finStartAndEndYr = finStartYr2+"-"+tempInInt;
 
 double bankopeningbal = 0.0;
	double pettycashOpeningbal = 0.0;
	
	 if(BNK.getextra2().equals("debit"))
	 {
	 	bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","bank","");
		double finOpeningBalNew = BBAL_L.getBankOpeningBal(BNK.getbank_id(),"bank",finStartAndEndYr);
		bankopeningbal += finOpeningBalNew;
		
		pettycashOpeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","cashpaid","");
		double pettycashFinOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(),"cashpaid",finStartAndEndYr);
		pettycashOpeningbal += pettycashFinOpeningBal;
	 }	
	 else if(BNK.getextra2().equals("credit"))
	 {
		 bankopeningbal=BNKCAL.getCreditBankOpeningBalance(BNK.getbank_id(),finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","");
			double finOpeningBalNew = BBAL_L.getBankOpeningBal(BNK.getbank_id(),"bank",finStartAndEndYr);
			bankopeningbal += finOpeningBalNew;
	 }
 %>
<li class="colr">&#8377;
<%=formatter.format(bankopeningbal)%>
<%-- <%=formatter.format(Double.parseDouble(BNK.gettotal_amount()))%> --%>
<br>
 <%-- <%=NUM.convert(Integer.parseInt(sjf.format(Double.parseDouble(BNK.gettotal_amount())))) %><br> --%>
 <span>CLOSING BALANCE</span></li>
 
  <li class="colr1">&#8377;
  <%=formatter.format(pettycashOpeningbal)%>
  <%-- <%if(BNK_CL.getPettyCashAmount(bankId)!=null && !BNK_CL.getPettyCashAmount(bankId).equals("")){%><%=formatter.format(Double.parseDouble(BNK_CL.getPettyCashAmount(bankId)))%><%}else{ %>00<%} %> --%>
  <br>
 <span>PETTY CASH AMOUNT</span></li>
</ul>


</div>
</div>

<div class="vendor-list">
<div class="clear"></div>
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
			<form id="searchForm" action="adminPannel.jsp" method="post" onsubmit="return changeDateFormat();">
				<input type="hidden" name="page" value="<%=request.getParameter("page")%>">
				<input type="hidden" name="id" value="<%=request.getParameter("id")%>">
				<div class="search-list">
					<ul>
					<li><select name="reportType" id="reportType">
						<option value="bank" selected="selected">Bank</option>
						<option value="cashpaid" <%if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %> selected="selected" <%} %>>Petty Cash</option>
					</select></li>
					<li><select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<option value="2021-22" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2021-22")) {%> selected="selected"<%} %>>2021-22</option>
					<option value="2020-21" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-21")) {%> selected="selected"<%} %>>2020-21</option>
					<option value="2019-20" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-20</option>
					<option value="2018-19" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%> selected="selected"<%} %>>2018-19</option>
					<option value="2017-18" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%> selected="selected"<%} %>>2017-18</option>
					<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
					<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
					</select></li>
						<li><input type="text"  name="fromdate" id="fromDate" class="DatePicker" value=""  readonly="readonly" /></li>
						<li><input type="text" name="todate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
<div class="icons">
<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
<span>
<!-- <ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul> -->

</span></div>
<%if(request.getParameter("finYear") != null){%>
<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<div class="printable" style="margin: 0 0 50px 0;">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border="1" rules="all">

<%
Calendar c1 = Calendar.getInstance(); 
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat dateFormatsj= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
dateFormat.setTimeZone(tz.getTimeZone("IST"));

//Calendar cal = Calendar.getInstance();
c1.set(Calendar.DATE, 1);
Date firstDateOfPreviousMonth = c1.getTime();
	String fromDate=(dateFormat2.format(c1.getTime())).toString();
		   fromDate=fromDate+" 00:00:01";
c1.set(Calendar.DATE, c1.getActualMaximum(Calendar.DATE)); 
Date lastDateOfPreviousMonth = c1.getTime();
	String toDate=(dateFormat2.format(c1.getTime())).toString();
			toDate=toDate+" 23:59:59";
//System.out.println("fromDate::"+fromDate+":::todate"+toDate);
	if(request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals("")){
		fromDate=request.getParameter("fromdate")+" 00:00:01";
	}
	if(request.getParameter("todate")!=null && !request.getParameter("todate").equals("")){
		toDate=request.getParameter("todate")+" 23:59:59";
	}

	MapOfHeads mph = new MapOfHeads();
	JVListing jvListing = new JVListing();
	 
	 Map<String,majorhead> majorHeadMap = mph.getMajorHeadsMap();
	 Map<String,minorhead> minorHeadMap = mph.getMinorHeadsMap();
	 Map<String,subhead> subHeadMap = mph.getSubHeadsMap();
	 Map<String,headofaccounts> hmap = mph.getHoasMap();
	 Map<String,bankdetails> banksMap = mph.getBanksMap();
	
MultiValueMap cpMap = jvListing.getJvsBsdOnDatesOrVoucherNo(fromDate,toDate,"");
MultiValueMap peMap = jvListing.getJvsBsdOnDatesOrBsdOnJvNo(fromDate,toDate,"");
MultiValueMap expMap = jvListing.getProductExpBsdOnDateOrEpiv("","","");
Map<String,banktransactions> bktMap = jvListing.getBankTransactionsMap("","","BKT","extra3");

banktransactionsListing BAK_L=new mainClasses.banktransactionsListing();
bankdetailsListing BNKDETL=new bankdetailsListing();
productexpensesListing PRDEXP_L=new productexpensesListing();
subheadListing SUBL = new subheadListing();
employeesListing EMP=new employeesListing();
//List BANKL=BAK_L.getMbanktransactionsId(bankId);
List BANKL=BAK_L.getBankTransactionBetweenDates(bankId,fromDate,toDate,request.getParameter("reportType"));
paymentsListing PAYL=new paymentsListing();
SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy");
double debitBal=0.00;
double creditBal=0.00;

double openingBal=0.00;
double TotPaidAmtToVendors=0.00;

bankcalculations bankcal = new bankcalculations();
double finOpeningBal = BBAL_L.getBankOpeningBal(bankId, request.getParameter("reportType"), request.getParameter("finYear"));
String finStartYr = request.getParameter("finYear").substring(0, 4);
if(BNK.getextra2().equals("debit"))
	openingBal = bankcal.getBankOpeningBalance(bankId, finStartYr+"-04-01 00:00:01", fromDate, request.getParameter("reportType"),"");
else if(BNK.getextra2().equals("credit"))
	openingBal = bankcal.getCreditBankOpeningBalance(bankId, finStartYr+"-04-01 00:00:01", fromDate,"");


/* if(BAK_L.getOpeningBalAmountPaidToVendors(bankId,fromDate,request.getParameter("reportType"))!=null && !BAK_L.getOpeningBalAmountPaidToVendors(bankId,fromDate,request.getParameter("reportType")).equals("")){
	TotPaidAmtToVendors=Double.parseDouble(BAK_L.getOpeningBalAmountPaidToVendors(bankId,fromDate,request.getParameter("reportType")));
}
if(BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType"))!=null && !BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType")).equals("")){
	openingBal=Double.parseDouble(BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType")))-TotPaidAmtToVendors;
} */
%>
<tr><td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<%-- <tr><td colspan="7" align="center" style="font-weight: bold;"><%if(request.getParameter("reportType")!=null){if(request.getParameter("reportType").equals("cashpaid")){ %> Petty Cash <%}else{ %> Bank <%}}else{ %> Bank <%} %>Payments Reports</td></tr> --%>
<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 10px;"><%=BNKDETL.getBankName(request.getParameter("id")) %> ACCOUNT</td></tr>
<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 10px;">Report From Date <%=CDF.format(dateFormatsj.parse(fromDate))%>  TO  <%=CDF.format(dateFormatsj.parse(toDate)) %> </td></tr>

<tr>
	<td width="2%" style="font-size: 11px;font-weight: bold;" align="center">DATE</td>
	<td width="3%" style="font-size: 11px;font-weight: bold;" align="center">VOUCHER</td>
	<td width="6%" style="font-size: 11px;font-weight: bold;" align="center">HEADS</td>
	<td width="2%" style="font-size: 11px;font-weight: bold;" align="center">NARRATION</td>
	<td width="2%" style="font-size: 11px;font-weight: bold;" align="center">CHEQUE NO</td> 
	<%if("credit".equals(BNK.getextra2())){ %>
	<td width="2%" style="font-size: 11px;font-weight: bold;" align="center">DEBIT</td>
	<td width="2%" style="font-size: 11px;font-weight: bold;" align="center">CREDIT</td>
	<%} else {%>
	<td width="2%" style="font-size: 11px;font-weight: bold;" align="center">DEBIT</td>
	<td width="2%" style="font-size: 11px;font-weight: bold;" align="center">CREDIT</td>
	<%}%>
	<td width="2%" style="font-size: 11px;font-weight: bold;" align="center">BALANCE</td>
</tr>

<tr>
	<td colspan="5" align="right" style="font-weight: bold;color: orange;font-size: 10px">OPENING BALANCE | </td>
	<%if("credit".equals(BNK.getextra2())){ %>
	<td  align="right" style="font-weight: bold;font-size: 9px;" width="2%">0.00</td>
	<td style="font-weight: bold;font-size: 9px;" align="right" width="2%"><%=formatter.format(openingBal+finOpeningBal)%></td>  
	<%} else { %>
	<td style="font-weight: bold;font-size: 9px;" align="right" width="2%"><%=formatter.format(openingBal+finOpeningBal)%></td>
	<td align="right" style="font-weight: bold;font-size: 9px;" width="2%">0.00</td>
	<%} %>
	<td align="right" style="font-weight: bold;font-size: 9px;" width="2%"><%=formatter.format(openingBal+finOpeningBal)%></td> 
</tr>

<%
openingBal += finOpeningBal;
String uniqueID="";
String uniqueValue="";
if(BANKL.size()>0){
	for(int i=0;i<BANKL.size();i++){
	BKT=(beans.banktransactions)BANKL.get(i);	
%>
<tr>
<td width="2%" style="font-size: 9px;font-weight: bold;"><%=CDF.format(dateFormat.parse(BKT.getdate())) %></td>
<%--  <td><a onclick="openMyModal('banktransactionsForm.jsp?txid=<%=BKT.getbanktrn_id() %>&id=<%=request.getParameter("id") %>'); return false;" target="_blank" href=""><%=BKT.getbanktrn_id() %></a></td>//commented --%> 
<td width="3%" style="font-size: 9px;font-weight: bold;"><a onclick="openMyModal('banktransactionsForm.jsp?txid=<%=BKT.getbanktrn_id() %>&id=<%=request.getParameter("id") %>'); return false;" target="_blank" href=""><%=BKT.getbanktrn_id() %></a><%if(BKT.gettype().equals("deposit")){%><br><%if(BKT.getExtra8().equals("")){ %>(deposit)<%}else{ %>(<%=BKT.getExtra8() %>)<%}} %></td>

<td width="6%" style="font-size: 9px;font-weight: bold;">
<%if (BKT.getextra3().contains("EPIV"))	{%>
	<table border="1" rules="all">
	<tr><td style="font-size: 9px;font-weight: bold;">Details</td></tr>
	<%String expids[] = BKT.getextra3().split(",");
	for (int j = 0 ; j < expids.length ; j++)	{%>
	  <tr>
	   <td style="font-size: 9px;font-weight: bold;">
		<%List<productexpenses> pexplst = (List<productexpenses>)expMap.get(expids[j]); %>
		<table border="1" rules="all">
		<tr>
			<td style="font-size: 9px;font-weight: bold;">HeadOfAccount</td>
			<td style="font-size: 9px;font-weight: bold;">MajorHead</td>
			<td style="font-size: 9px;font-weight: bold;">MinorHead</td>
			<td style="font-size: 9px;font-weight: bold;">SubHead</td>
			<td style="font-size: 9px;font-weight: bold;">Amount</td>
		</tr>
		<%
		if (pexplst != null && pexplst.size() > 0)	{
			for (int  k = 0 ; k < pexplst.size() ; k++)	{
				productexpenses pexp = pexplst.get(k);%>
			<tr>
				<td style="font-size: 9px;font-weight: bold;"><%=pexp.gethead_account_id() %> <%if(hmap.containsKey(pexp.gethead_account_id())){%> <%=hmap.get(pexp.gethead_account_id()).getname() %> <%}%> </td>
				<td style="font-size: 9px;font-weight: bold;"><%=pexp.getmajor_head_id()%> <%if(majorHeadMap.containsKey(pexp.getmajor_head_id())){%> <%=majorHeadMap.get(pexp.getmajor_head_id()).getname()%> <%}%> </td>
				<td style="font-size: 9px;font-weight: bold;"><%=pexp.getminorhead_id() %> <%if(minorHeadMap.containsKey(pexp.getminorhead_id())){%> <%=minorHeadMap.get(pexp.getminorhead_id()).getname() %> <%}%> </td>
				<td style="font-size: 9px;font-weight: bold;"><%=pexp.getsub_head_id() %> <%if (subHeadMap.containsKey(pexp.getsub_head_id())) {%> <%=subHeadMap.get(pexp.getsub_head_id()).getname()%> <%}%> </td>
				<td style="font-size: 9px;font-weight: bold;"><%=formatter.format(Double.parseDouble(pexp.getamount())) %></td>
			</tr>
			<%} // inner for loop
		}
		else	{%>
		<tr><td colspan="4" style="font-size: 9px;font-weight: bold;">no data found</td></tr>
		<%}%>
		</table>
		</td>
	  </tr>
	<%} // outer for loop%>
	</table>
 <%}
	else if (BKT.getextra3().contains("JVN"))	{
	
	if (peMap.containsKey(BKT.getextra3()))	{
		
		List<productexpenses> pexplst = (List<productexpenses>)peMap.get(BKT.getextra3());
		
		if (pexplst != null && pexplst.size() > 0)	{%>
		<table border="1" rules="all">
		<tr>
			<td style="font-size: 9px;font-weight: bold;">HeadOfAccount</td>
			<td style="font-size: 9px;font-weight: bold;">MajorHead</td>
			<td style="font-size: 9px;font-weight: bold;">MinorHead</td>
			<td style="font-size: 9px;font-weight: bold;">SubHead</td>
			<td style="font-size: 9px;font-weight: bold;">Amount</td>
		</tr>
		<%
		for (int l = 0 ; l < pexplst.size() ; l++ )	{
			productexpenses pexp = pexplst.get(l);%>
			<tr>
				<td style="font-size: 9px;font-weight: bold;"><%=pexp.gethead_account_id() %> <%if(hmap.containsKey(pexp.gethead_account_id())){%> <%=hmap.get(pexp.gethead_account_id()).getname() %> <%}%> </td>
				<td style="font-size: 9px;font-weight: bold;"><%=pexp.getmajor_head_id()%> <%if(majorHeadMap.containsKey(pexp.getmajor_head_id())){%> <%=majorHeadMap.get(pexp.getmajor_head_id()).getname()%> <%}%> </td>
				<td style="font-size: 9px;font-weight: bold;"><%=pexp.getminorhead_id() %> <%if(minorHeadMap.containsKey(pexp.getminorhead_id())){%> <%=minorHeadMap.get(pexp.getminorhead_id()).getname() %> <%}%> </td>
				<td style="font-size: 9px;font-weight: bold;"><%=pexp.getsub_head_id() %> <%if (subHeadMap.containsKey(pexp.getsub_head_id())) {%> <%=subHeadMap.get(pexp.getsub_head_id()).getname()%> <%}%> </td>
				<td style="font-size: 9px;font-weight: bold;"><%=formatter.format(Double.parseDouble(pexp.getamount())) %></td>
			</tr>
		<%}%>
		</table>	
		<%} // if
		else	{%>
		No Data Found(pe)	
		<%} // else
	} // if
	else if (cpMap.containsKey(BKT.getextra3()))	{
		
		List<customerpurchases> cplist = (List<customerpurchases>)cpMap.get(BKT.getextra3());
		
		if (cplist != null && cplist.size() > 0)	{%>
		<table border="1" rules="all">
			<tr>
				<td style="font-size: 9px;font-weight: bold;">HeadOfAccount</td>
				<td style="font-size: 9px;font-weight: bold;">MajorHead</td>
				<td style="font-size: 9px;font-weight: bold;">MinorHead</td>
				<td style="font-size: 9px;font-weight: bold;">SubHead</td>
				<td style="font-size: 9px;font-weight: bold;">Amount</td>
			</tr>
			<%for (int m = 0 ; m < cplist.size() ; m++)	{
				customerpurchases cp = cplist.get(m);%>
				<tr>
					<td style="font-size: 9px;font-weight: bold;"><%=cp.getextra1() %> <% if (hmap.containsKey(cp.getextra1())) {%> <%=hmap.get(cp.getextra1()).getname() %> <%}%></td><!-- hoa -->
					<td style="font-size: 9px;font-weight: bold;"><%=cp.getextra6() %> <% if (majorHeadMap.containsKey(cp.getextra6())) {%> <%=majorHeadMap.get(cp.getextra6()).getname() %> <%}%></td><!-- majorhead -->
					<td style="font-size: 9px;font-weight: bold;"><%=cp.getextra7() %> <% if (minorHeadMap.containsKey(cp.getextra7())) {%> <%=minorHeadMap.get(cp.getextra7()).getname() %> <%}%></td><!-- minorhead -->
					<td style="font-size: 9px;font-weight: bold;"><%=cp.getproductId() %> <% if (subHeadMap.containsKey(cp.getproductId())) {%> <%=subHeadMap.get(cp.getproductId()).getname() %> <%}%></td><!-- subhead -->
					<td style="font-size: 9px;font-weight: bold;"><%=formatter.format(Double.parseDouble(cp.gettotalAmmount())) %></td>
				</tr>
			<%} //for %>
		</table>	
		<%} // if
		else	{%>
			No Data Found(cp)
		<%} // else
	 } // if %>
 <%} 
   
  else if (BKT.getextra3().contains("BKT")){
  	
	if (bktMap.containsKey(BKT.getextra3()))	{%>
  		
  		<table border="1" rules="all">
  		<tr>
  			<td style="font-size: 9px;font-weight: bold;">BankId</td>
  			<td colspan="2" style="font-size: 9px;font-weight: bold;">Narration</td>
  			<td style="font-size: 9px;font-weight: bold;">Type</td>
  			<td style="font-size: 9px;font-weight: bold;">Amount</td>
  		</tr>
  		<tr>
  			<td style="font-size: 9px;font-weight: bold;"><%=bktMap.get(BKT.getextra3()).getbank_id() %> <%=banksMap.get(bktMap.get(BKT.getextra3()).getbank_id()).getbank_name() %></td>
  			<td colspan="2" style="font-size: 9px;font-weight: bold;"><%=bktMap.get(BKT.getextra3()).getnarration() %></td>
  			<td style="font-size: 9px;font-weight: bold;"><%=bktMap.get(BKT.getextra3()).gettype() %></td>
  			<td style="font-size: 9px;font-weight: bold;"><%=bktMap.get(BKT.getextra3()).getamount() %></td>
  		</tr>
  		</table>
  	<%}%> 
<%}
  else if (!BKT.getSub_head_id().equals("") && !BKT.getMajor_head_id().equals("") && !BKT.getMinor_head_id().equals(""))	{%>
	<table border="1" rules="all">
  		<tr>
  			<td style="font-size: 9px;font-weight: bold;">HeadOfAccount</td>
  			<td style="font-size: 9px;font-weight: bold;">MajorHead</td>
  			<td style="font-size: 9px;font-weight: bold;">MinorHead</td>
  			<td style="font-size: 9px;font-weight: bold;">SubHead</td>
  			<td style="font-size: 9px;font-weight: bold;">Amount</td>
  		</tr>
  		<tr>
  			<td style="font-size: 9px;font-weight: bold;"><%=hmap.get(BKT.getHead_account_id()).getname() %></td>
  			<td style="font-size: 9px;font-weight: bold;"><%=BKT.getMajor_head_id() %> <% if(majorHeadMap.containsKey(BKT.getMajor_head_id())) {%> <%=majorHeadMap.get(BKT.getMajor_head_id()).getname() %> <%}%> </td>
  			<td style="font-size: 9px;font-weight: bold;"><%=BKT.getMinor_head_id() %> <%if(minorHeadMap.containsKey(BKT.getMinor_head_id())) {%> <%=minorHeadMap.get(BKT.getMinor_head_id()).getname() %> <%}%> </td>
  			<td style="font-size: 9px;font-weight: bold;"><%=BKT.getSub_head_id() %> <%if(subHeadMap.containsKey(BKT.getSub_head_id())) {%> <%=subHeadMap.get(BKT.getSub_head_id()).getname() %> <%}%> </td>
  			<td style="font-size: 9px;font-weight: bold;"><%=BKT.getamount() %></td>
  		</tr>
  	</table>
	
<%}
  
  else	{%>
	NO HEADS FOUND
<%}%>

</td>

<td width="2%" style="font-size: 9px;font-weight: bold;"><%=BKT.getnarration() %></td>
<td align="center" width="2%" style="font-size: 9px;font-weight: bold;"><%=PAYL.getChequeNo(BKT.getextra5()) %></td>

<!-- For petty cash transactions -->

<%if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %>
<td align="right" width="2%" style="font-size: 9px;font-weight: bold;"> <%if(BKT.gettype().equals("pettycash")){ creditBal=creditBal+Double.parseDouble(BKT.getamount()); openingBal=openingBal+Double.parseDouble(BKT.getamount()); %> <%=formatter.format(Double.parseDouble(BKT.getamount()))%> <%}%></td>
<td align="right" width="2%" style="font-size: 9px;font-weight: bold;"> <%if(BKT.gettype().equals("cashpaid")){ debitBal=debitBal+Double.parseDouble(BKT.getamount()); openingBal=openingBal-Double.parseDouble(BKT.getamount());%><%=BKT.getamount()%> <%}%> </td>

<!-- For petty cash transactions -->
<%} // if
  else{ %>

<!-- For Bank deposits/cheque transactions -->
<td align="right" width="2%" style="font-size: 9px;font-weight: bold;">
	<%if(BKT.gettype().equals("deposit") )	{
		
		if("credit".equals(BNK.getextra2()) )	{ 
			debitBal=debitBal+Double.parseDouble(BKT.getamount());
			openingBal=openingBal-Double.parseDouble(BKT.getamount());
		} // if 
		else{
				debitBal=debitBal+Double.parseDouble(BKT.getamount());
				openingBal=openingBal+Double.parseDouble(BKT.getamount());
		} // else
		
	if (BKT.getbank_id().equals("BNK1019") && BKT.getSub_head_id().equals("25766") && BKT.getExtra8().equals("other-amount") && (BKT.getextra4().equals("Charity-cash") || BKT.getextra4().equals("pro-counter")))	{%>
		<a href="#" onclick="updateJvNumber('<%=BKT.getbanktrn_id()%>','<%=BKT.getextra3()%>');"><%=formatter.format(Double.parseDouble(BKT.getamount()))%></a>
 <%} 
	else  {%> <%=formatter.format(Double.parseDouble(BKT.getamount()))%> <%}
}%>
</td>
<td align="right" width="2%" style="font-size: 9px;font-weight: bold;"> 
	<%if(BKT.gettype().equals("withdraw") || BKT.gettype().equals("pettycash") || BKT.gettype().equals("cashpaid") || BKT.gettype().equals("cheque") || BKT.gettype().equals("transfer") || BKT.gettype().equals("opening")){
		
		if("credit".equals(BNK.getextra2()) && BKT.gettype().equals("opening") && !BKT.gettype().equals("withdraw") ){ 
				creditBal=creditBal+Double.parseDouble(BKT.getamount());
			   	openingBal=openingBal+Double.parseDouble(BKT.getamount());
		} 
		else if("credit".equals(BNK.getextra2()) && !BKT.gettype().equals("withdraw") ){ 
			   	creditBal=creditBal+Double.parseDouble(BKT.getamount());
				openingBal=openingBal-Double.parseDouble(BKT.getamount());
		}else if("credit".equals(BNK.getextra2()) && BKT.gettype().equals("withdraw") ){ 
				creditBal=creditBal+Double.parseDouble(BKT.getamount());
				openingBal=openingBal+Double.parseDouble(BKT.getamount());
		} else if(!"credit".equals(BNK.getextra2())){
				creditBal=creditBal+Double.parseDouble(BKT.getamount());
				openingBal=openingBal-Double.parseDouble(BKT.getamount());
		}%> 
		
		<%=formatter.format(Double.parseDouble(BKT.getamount()))%> 
	
   <%}%>
</td>
<!-- For Bank deposits/cheque transactions -->

<%} // else %>
 <td align="right" width="2%" <%if(openingBal<0){%>style="color: red;font-size: 9px;font-weight: bold;"<%}%> style="font-size: 9px;font-weight: bold;"><%=formatter.format(openingBal) %></td> 
</tr>
<%}} %>
<%-- <%if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %> --%>
<tr>
	<td class="bg" colspan="5" align="right" style="font-weight: bold;color: orange;font-size:10px;">CLOSING BALANCE | </td>
	<td class="bg"  style="font-weight: bold;font-size: 9px;" align="right" width="2%"><%=formatter.format(debitBal) %></td>
	<td class="bg"  style="font-weight: bold;font-size: 9px;" align="right" width="2%"><%=formatter.format(creditBal) %></td>
	<td class="bg"  style="font-weight: bold;font-size: 9px;" align="right" width="2%"><%-- <%=formatter.format(creditBal-debitBal)%> --%><%=formatter.format(openingBal) %></td> 
</tr>
<%-- <%}else{ %>
<tr>
	<td class="bg" colspan="5" align="right" style="font-weight: bold;color: orange;">CLOSING BALANCE | </td>
	<td  class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(debitBal) %></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(creditBal) %></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(debitBal-creditBal)%><%=openingBal %></td>
	<td></td>
</tr>

<%} %> --%>
</table>
</div>
</div>
</div>
</div><%} %>
<%}else{response.sendRedirect("adminPannel.jsp?page=banks");}%>

