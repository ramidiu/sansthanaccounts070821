

<script> 
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false; 
    });

});
</script> 
<style>

.menu ul li ul li .level-3{

    position: absolute;
    left: 202px;
    top: 0;
} 

.menu ul li ul li .level-2 ul li:hover{
	display:block;
}
/* .menu ul li ul li ul li{
    display: none;
    font-size: 12px;
    float: none;
    padding: 7px;
    position: relative;
    left: 0;
    min-width: 200px;
    margin: 2px;
} 
.menu ul li ul li ul li:hover {
    background: #FFF;
    display:block;
} */
</style>
<div class="vendor-name">
<div class="logo-here"><span>Sai Sansthan
Accounts</span></div>
<div class="menu">
<ul>
<li><a href="">Home</a></li>
<%if(session.getAttribute("adminId").toString().equals("EMP1000032")){ %>
<li><a href="#">Heads creation</a>
<ul>
<!-- <li><a href="adminPannel.jsp?page=mastersTree">Head of account</a></li>
<li><a href="adminPannel.jsp?page=headGroup">Head Groups</a></li>
<li><a href="adminPannel.jsp?page=majorHead">Major Heads</a></li>
<li><a href="adminPannel.jsp?page=minorHead">Minor Heads</a></li> -->
<li><a href="adminPannel.jsp?page=subHead">Sub Heads</a></li>
<li><a href="adminPannel.jsp?page=products">Products</a></li>
</ul>
</li>
<%}else if(session.getAttribute("Role").toString().equals("Manger") || session.getAttribute("Role").toString().equals("AsstManger")){ %>
<!-- <li><a href="#">Create</a>
	<ul>
		<li><a href="adminPannel.jsp?page=vendors">Vendor</a></li>
		<li><a href="adminPannel.jsp?page=employees">Employees</a></li>
		<li><a href="adminPannel.jsp?page=Counters">Add Counters</a></li>
		<li><a href="adminPannel.jsp?page=employeeDetailsForm">Employee Details</a></li> 
		<li><a href="adminPannel.jsp?page=employeeDetailsList">Employee Details Edit</a></li> 
		<li><a href="adminPannel.jsp?page=employeeSalarySlipForm">Generate Employee Salary Slip</a></li>
		<li><a href="adminPannel.jsp?page=poojaSaman_group">Pooja saaman group</a></li>
		
	</ul>
</li> -->
<li><a href="#">Create</a>
	<ul>
		<li><a href="adminPannel.jsp?page=vendors">Vendor</a></li>
		
		<li><a href="adminPannel.jsp?page=employees">Employees</a></li>
		<li><a href="adminPannel.jsp?page=Counters">Add Counters</a></li>
		<li><a href="adminPannel.jsp?page=employeeDetailsForm">Employee Details</a></li> 
		<li><a href="adminPannel.jsp?page=employeeDetailsList">Employee Details Edit</a></li> 
		<li><a href="adminPannel.jsp?page=employeeSalarySlipForm">Generate Employee Salary Slip</a></li>
		<li><a href="adminPannel.jsp?page=poojaSaman_group">Pooja saaman group</a></li>
		<li><a href="adminPannel.jsp?page=advancesDuesForm">Heads creation</a>
		<ul class="level-2">
		<li><a href="adminPannel.jsp?page=mastersTree">Head of account</a></li>
<li><a href="adminPannel.jsp?page=headGroup">Head Groups</a></li>
<li><a href="adminPannel.jsp?page=majorHead">Major Heads</a></li>
<li><a href="adminPannel.jsp?page=minorHead">Minor Heads</a></li>
<li><a href="adminPannel.jsp?page=subHead">Sub Heads</a></li>
<li><a href="adminPannel.jsp?page=products">Products</a></li>
<li><a href="adminPannel.jsp?page=HeadOfAccountWiseProducts">HeadOfAccountWiseProducts</a></li>
<li><a href="adminPannel.jsp?page=subheadsForDiagnostic">Subheads For Diagnostic</a></li>
		</ul>
		</li>
	</ul>
</li>
<!-- <li><a href="#">Heads creation</a>
<ul>
<li><a href="adminPannel.jsp?page=mastersTree">Head of account</a></li>
<li><a href="adminPannel.jsp?page=headGroup">Head Groups</a></li>
<li><a href="adminPannel.jsp?page=majorHead">Major Heads</a></li>
<li><a href="adminPannel.jsp?page=minorHead">Minor Heads</a></li>
<li><a href="adminPannel.jsp?page=subHead">Sub Heads</a></li>
<li><a href="adminPannel.jsp?page=products">Products</a></li>
</ul>
</li> -->

<li><a href="#">Bank Transactions</a>
	<ul>
		<li><a href="#">Banks</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=banks">Banks</a></li>
				<li><a href="adminPannel.jsp?page=countersClosed-Deposits">Amount Deposits in Bank</a></li>
				<li><a href="adminPannel.jsp?page=bank-reconciliation">Bank Reconciliation</a></li>
				<!-- <li><a href="adminPannel.jsp?page=bank_reconciliationReportOld">Bank Reconciliation Report Old</a></li> -->
				<li><a href="adminPannel.jsp?page=bank_reconciliationReportNew2">Bank Reconciliation Report New</a></li>
				<!-- <li><a href="adminPannel.jsp?page=bank_reconciliationReport">Bank Reconciliation Report New</a></li> -->
					<li><a href="adminPannel.jsp?page=contraBankEntryForm">Bank To Bank Transfer</a></li>
					<li><a href="adminPannel.jsp?page=SainivasDeposites">Sainivas Deposits</a></li>
					 <!-- <li><a href="adminPannel.jsp?page=DailycounterBalanceReport">Daily counter Balance Report OLD</a></li>  -->
					<li><a href="adminPannel.jsp?page=DailycounterBalanceReport_new">Daily counter Balance Report</a></li>
					<li><a href="adminPannel.jsp?page=DailycounterBalanceReport_cardonline">Daily counter Online & Card Report</a></li>
					<li><a href="adminPannel.jsp?page=bankBalanceForm">Enter Bank Opening Balance</a></li>
					<li><a href="adminPannel.jsp?page=LiabilitiesAssetsOpeningBal">Assets Liabilities Opening Balance</a></li>
					<li><a href="adminPannel.jsp?page=otherDepositsReport">Other Deposits Report</a></li>
					<li><a href="adminPannel.jsp?page=bankCategoryReport">Bank Category Report</a></li>
					
				
			</ul> 
			
		</li>
		
		<li><a href="#">Expenses&Payments</a>
			<ul class="level-2">
			<!-- <li><a href="adminPannel.jsp?page=billApprovalsList">BillApprovals List</a></li> -->
			<li ><a href="#">BillApprovals</a>
			<ul class="level-3">
			<li><a href="adminPannel.jsp?page=billApprovalsList">BillApprovals List</a></li>
			<li><a href="adminPannel.jsp?page=dcBillApproval">DailyChallan BillApproval</a></li>
			</ul>
			</li>
				<!-- <li><a href="adminPannel.jsp?page=paymentsSendForApprovals">Payments Send for Approvals</a></li> -->
				
				<%if(session.getAttribute("Role").toString().equals("Manger")){ %>
					<li><a href="adminPannel.jsp?page=paymentbillApprovalsListbyManager">ExpensesApprovalRequire List</a></li>
				<%} %>
				<li><a href="adminPannel.jsp?page=multipleBills-ChequeIssue">Payments Issue</a></li>	
				<li><a href="adminPannel.jsp?page=paymentsApprovalsList">GS/FSPaymentsApproval List </a></li>
				<li><a href="adminPannel.jsp?page=Successful-payments-list">Successful Payments List</a></li>
				<li><a href="adminPannel.jsp?page=cardCheckonlineBrsdateupdate">CardCheckOnlineBrsUpdate</a></li>
				<li><a href="adminPannel.jsp?page=gowdonstockentrylist">ExpensesRounding</a></li>
				<!-- <li><a href="adminPannel.jsp?page=test">Successful Payments New</a></li>	 -->			
				<!-- <li><a href="adminPannel.jsp?page=expenses">Expenses Entry</a></li>
				<li><a href="adminPannel.jsp?page=paymentsToVendor">Payments to vendor</a></li> -->
			</ul>
			
			
		</li>
		<li><a href="adminPannel.jsp?page=Journal_Entry_Form">Journal Voucher</a>
		<ul class="level-2">
		<!-- <li><a href="adminPannel.jsp?page=Ewf_Entry_Form">EWF Entry</a></li>
		<li><a href="adminPannel.jsp?page=Ewf_Entry_List">EWF Entry List</a></li> -->
		<li><a href="adminPannel.jsp?page=Journal_Entry_Form">Journal Voucher</a></li>
		<li><a href="adminPannel.jsp?page=Journal_Entry_list">Journal Voucher Entry List</a></li>
		</ul>
		</li>
		<li><a href="adminPannel.jsp?page=advancesDuesForm">Advances</a>
		<ul class="level-2">
		<li><a href="adminPannel.jsp?page=advancesDuesList">Advances List</a></li>
		</ul>
		</li>
		<li><a href="adminPannel.jsp?page=bank_lr">Bank LR Report</a></li>
	</ul>
	
</li>
<li><a href="#">Indenting</a>
	<ul>
		<!-- <li><a href="adminPannel.jsp?page=indentReport">Raise Indent</a></li> -->
		<li><a href="adminPannel.jsp?page=indentReportList">Indents List</a></li>
		<li><a href="adminPannel.jsp?page=purchaseOrdersList">Purchase orders</a></li>
		<li><a href="adminPannel.jsp?page=billEdit">Bill Edit</a></li>
		
	</ul>
</li>
<li><a href="#">Pooja Stores</a>
	<ul>
		<li><a href="adminPannel.jsp?page=shopSalesList">shop Sales Report</a></li>
		<li><a href="adminPannel.jsp?page=shopSales-DetailReport">ShopSales Detail Report</a></li>
		<li><a href="adminPannel.jsp?page=PS-DayStock-ValuationReport">Stock Valuation</a></li>
		<li><a href="adminPannel.jsp?page=ProductWise-sale-report">Product wise sale report</a></li>
			<li><a href="adminPannel.jsp?page=poojaStore-StockReport">Pooja store Stock</a></li>
		<li><a href="adminPannel.jsp?page=productReportNegtiveDetailReport">Pooja store Stock Checked</a></li>
	</ul>
</li> 
<%-- <%if(session.getAttribute("Role").toString().equals("Manger")){ %> --%>

<li><a href="#"> Sansthan </a>
	<ul>
		<li><a href="adminPannel.jsp?page=GoldSilverReportNew">Gold and Silver Report</a></li>
		<li><a href="adminPannel.jsp?page=offerKindsProductWise">Shawls Report</a></li>
		<li><a href="#">Schemes</a></li>
		<!-- <li><a href="adminPannel.jsp?page=ledgerReportBySitaram">Ledger Report</a></li> -->
		<li><a href="GoldSilverPostionReport.jsp">Gold Report</a></li>
		<li><a href="silver_Report.jsp">Silver Report</a></li>
	</ul>
</li> 

<li><a href="#">Pro</a>
	<ul>
			<li><a href="adminPannel.jsp?page=collectionSummary-Report">Collection Summary</a></li>
		<li><a href="adminPannel.jsp?page=proCollectionDetails-Report">Collection Detail Report</a></li>
		<li><a href="adminPannel.jsp?page=comparisionReport">Comparison Report</a></li>
			<li><a href="adminPannel.jsp?page=comparison-Report-Month-Wise">Comparison Report Month Wise</a></li>
			<li><a href="adminPannel.jsp?page=proOffice-salesReport">Sales Detail Report</a></li>
		<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport">Offer Kinds Daliy Report</a></li>
		<li><a href="adminPannel.jsp?page=totalPettyCash">Total Petty Cash Report</a></li>
	<li><a href="adminPannel.jsp?page=totalsalereport">Online Sales</a></li>
	</ul>
</li> 
<li><a href="#"> Accounts </a>
	<ul>
		<!-- <li><a href="adminPannel.jsp?page=indentReport">Raise Indent</a></li> -->
		<li><a href="adminPannel.jsp?page=alldebits_credits">All Credits And Debits</a></li>
		<li><a href="adminPannel.jsp?page=Total-Collection-Summary-Report">Collection Summary</a></li>
		<li><a href="./CollectionSummaryReportNew.jsp">CollectionSummaryNewFormat</a></li>
		<li><a href="adminPannel.jsp?page=cardsalesreport">Card Sales Report</a></li>
		<li><a href="adminPannel.jsp?page=totalsalereport">Online Receipts</a></li>
		<li><a href="adminPannel.jsp?page=cardsalesreport">Card Sales Receipts</a></li>
		<li><a href="adminPannel.jsp?page=ledgerReportBySitaram">Ledger Report</a></li>
		<li><a href="adminPannel.jsp?page=medicineissuereport">MedicineConsumptionReport</a></li>
		<!-- <li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww_latest">Income & Expenditure Report (NEW)</a></li> -->
		<li><a href="#">Income & Expenditure</a>
			<ul class="level-2">
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report">Income & Expenditure Report (Weekly)</a></li>
					<!-- <li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_new">Income & Expenditure Report OLD</a></li>  -->
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww_latest">Income & Expenditure Report (NEW)</a></li>
					 <li><a href="adminPannel.jsp?page=Income_Expenditure_new_day_wise">Income_Expenditure_Report_B/W_Dates</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=receipts-payments-report_neww_latest">Receipts & Payments NEW</a></li> 
		<li><a href="#">Trail Balance</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=trailBalanceReport_newBySitaram">Trail Balance NEW</a></li>
			<li><a href="adminPannel.jsp?page=trailBalanceReport_withoutOpeningBalance">TrailBalance(WithoutOpeningBalance)</a></li>
			</ul>
		</li>
		
		
		<li><a href="#">Balance Sheet</a>
			<ul class="level-2">
					<li><a href="adminPannel.jsp?page=balanceSheet_newwBySitaram">Balance Sheet NEW</a></li>
					<li><a href="adminPannel.jsp?page=openingBal">BalanceSheet(OpeningBal)</a></li>
					<li><a href="adminPannel.jsp?page=balanceSheet_NewWithOutOpeningBalance">BalanceSheet(WithoutOpeningBalance)</a></li>
			</ul>
		</li>
	<li><a href="adminPannel.jsp?page=vendorTaxReport" target="_blank">TDS Reports</a></li> 
		<li><a href="#">GST Report</a>
		<ul class="level-2">
			<li><a href="adminPannel.jsp?page=vendorReport">GST Report(Input)</a></li>
			<li><a href="adminPannel.jsp?page=total_sainivas_amounts">GST Report(Output)</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=Total-Pending-Bills">Total Pending Bills</a></li>
		<li><a href="adminPannel.jsp?page=income_and_payments">Payments and Income Report</a></li>
		<li><a href="adminPannel.jsp?page=comparison-Report-Expenses">Comparison Report Of Expenses</a></li>
		<li><a href="#">Expenses Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=expensesReport">Expenses Report-Vendor</a></li>
				<li><a href="adminPannel.jsp?page=expensesReport-subhead">Expenses Report-Sub head</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=stock-performance-report">Performance Report</a></li>
		<li><a href="adminPannel.jsp?page=paysliplist-monthwise">Employee Pay Slip</a></li>
			<li><a href="adminPannel.jsp?page=acquittanceReport">Acquittance Report</a></li>
			<li><a href="#">Daily Bill Status</a>
		<ul class="level-2">
		<li><a href="adminPannel.jsp?page=uploadBillStatus">Upload Daily Bill Status</a></li>
			<!-- <li><a href="adminPannel.jsp?page=balanceSheet_new">Balance Sheet OLD</a></li> -->
			<li><a href="adminPannel.jsp?page=billStatusList">Daily Bill Status List</a></li>

		</ul>
		</li>
		<!-- <li><a href="#">Bank Deposits</a></li>
		<li><a href="#">Jv's</a></li>
		<li><a href="#">Sale Proceeds</a></li>
		<li><a href="#">Assets and Liability</a></li> -->
	</ul> 
</li>
<li><a href="#"> Stock </a>
	<ul>
		<li><a href="adminPannel.jsp?page=medicalStock">Medical Stock</a></li>
		<li><a href="adminPannel.jsp?page=kitchenStore-StockReport">Charity Stock</a></li>
		<li><a href="adminPannel.jsp?page=productReport1">Product Ledger</a></li>
		<li><a href="adminPannel.jsp?page=kitchenStock2New">Total Assets & Provisions Report</a></li>
		<li><a href="adminPannel.jsp?page=kitchenStockAssertsNew">Assets Report</a></li>
		 <li><a href="adminPannel.jsp?page=kitchenStockProvisions">Provisions Detailed Report</a></li>
		 <li><a href="adminPannel.jsp?page=productopeningcategory">Product Openning Balance</a></li>
		 <li><a href="adminPannel.jsp?page=productOpeningBalanceList">Product Openning Balance List</a></li>
		 <li><a href="adminPannel.jsp?page=kitchenStockProvisions1">New Provisions Detailed Report</a></li> 
		<li><a href="adminPannel.jsp?page=kitchenStockProvisionsNew">Provisions Report</a></li>
		<li><a href="adminPannel.jsp?page=GoldSilverReportNew">Gold Silver Report</a></li>
		<li><a href="adminPannel.jsp?page=ConsumerReport">Consumer Report</a></li>
	<li><a href="GoldSilverPostionReport.jsp">Gold Report</a></li>
		<li><a href="silver_Report.jsp">Silver Report</a></li>
			<li><a href="adminPannel.jsp?page=productReport">Godown Inventory Report</a></li>
			<li><a href="adminPannel.jsp?page=productReport">Product Ledger Report</a></li>
			<li><a href="adminPannel.jsp?page=prasadamExpenseReport">Prasadam Expenses Report</a></li>	
	</ul> 
</li>
<!-- <li><a href="#"> Uncategorized Reports</a>
	<ul>
		
	<li><a href="adminPannel.jsp?page=Profit-Report">Profit Report</a></li>
	<li><a href="adminPannel.jsp?page=advance_amount">Advance Amount</a></li>
	
	</ul> 
</li> -->

<!-- <li><a href="#">Reports</a>
	<ul>
		<li><a href="adminPannel.jsp?page=Total-Collection-Summary-Report">Collection Summary</a></li>
		<li><a href="#">Collection Summary</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=Total-Collection-Summary-Report">Collection Summary</a></li>
			<li><a href="adminPannel.jsp?page=cardsalesreport">Card Sales Report</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport">Offer Kinds DaliyReport</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport_new">Offer Kinds DaliyReport(kind-gold)</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds_dailySalesSilver">Offer Kinds DaliyReport(kind-silver)</a></li>
			<li><a href="adminPannel.jsp?page=offerKindsProductWise">Offer Kinds-Product Wise</a></li>
			</ul>
			</li>
			<li><a href="adminPannel.jsp?page=totalPettyCash">Total PettyCash Report</a></li>
			<li><a href="adminPannel.jsp?page=vendorTaxReport" target="_blank">TDS Reports</a></li> 
			<li><a href="#">GST Report</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=vendorReport">GST Report(Input)</a></li>
			<li><a href="adminPannel.jsp?page=total_sainivas_amounts">GST Report(Output)</a></li>
			</ul>
			</li>
		    <li><a href="adminPannel.jsp?page=Total-Pending-Bills">Total Pending Bills</a></li>
				<li><a href="adminPannel.jsp?page=income_and_payments">Payments & Income  Report</a></li>
						<li><a href="#">Receipts & Payments</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=receipts-payments-report">Receipts & Payments</a></li>
			<li><a href="adminPannel.jsp?page=receipts-payments-report_new">Receipts & Payments OLD</a></li>
			 <li><a href="adminPannel.jsp?page=receipts-payments-report_neww_latest">Receipts & Payments NEW</a></li> 
			</ul>
			</li>
			<li><a href="#">Income & Expenditure Report</a>
			<ul class="level-2">
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report">Income & Expenditure Report (Weekly)</a></li>
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_new">Income & Expenditure Report OLD</a></li> 
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww_latest">Income & Expenditure Report (NEW)</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=productReport">Godown Inventory Report</a></li>
		<li><a href="adminPannel.jsp?page=ledgerReportBySitaram">Ledger Report</a></li>
		<li><a href="adminPannel.jsp?page=comparison-Report-Expenses">Comparison Report Of Expenses</a></li>
		<li><a href="adminPannel.jsp?page=productReport">Product Ledger Report</a></li>
				
		<li><a href="#">Expenses Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=expensesReport">Expenses Report-Vendor</a></li>
				<li><a href="adminPannel.jsp?page=expensesReport-subhead">Expenses Report-Sub head</a></li>
			</ul>
		</li>
		 <li><a href="#">Pooja Store Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=shopSalesList">shop Sales Report</a></li>
				<li><a href="adminPannel.jsp?page=shopSales-DetailReport">ShopSales Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=PS-DayStock-ValuationReport">Stock Valuation</a></li>
				<li><a href="adminPannel.jsp?page=ProductWise-sale-report">Product wise sale report</a></li>
				<li><a href="adminPannel.jsp?page=poojaStore-StockReport">Pooja store Stock</a></li>
				<li><a href="adminPannel.jsp?page=productReportNegtiveDetailReport">Pooja store Stock Checked</a></li>
			</ul>
		</li> 
		<li><a href="adminPannel.jsp?page=trailBalanceReport">Trail Balance</a></li> --><!-- OLD LINK
		
		NEW CREATED STARTS
		<li><a href="#">Trail Balance</a>
		<ul class="level-2">
				<li><a href="adminPannel.jsp?page=trailBalanceReport">Trail Balance OLD</a></li>
				<li><a href="adminPannel.jsp?page=trailBalanceReport_newBySitaram">Trail Balance NEW</a></li>
			</ul>
		</li>
		NEW CREATED ENDS
		
		
		<li><a href="adminPannel.jsp?page=Profit-Report">Profit Report</a></li>
		<li><a href="#">PRO Office Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=collectionSummary-Report">Collection Summary</a></li>
				<li><a href="adminPannel.jsp?page=proCollectionDetails-Report">Collection Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=comparisionReport">Comparison Report</a></li>
				<li><a href="adminPannel.jsp?page=comparison-Report-Month-Wise">Comparison Report Month Wise</a></li>
				<li><a href="adminPannel.jsp?page=proOffice-salesReport">Sales Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport">OfferKind Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport_ALL">Offer Kinds DaliyReport</a></li>
			</ul>
		</li>
		<li><a href="#">Vat Reports</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=Vat-Summary-ReportNew">Vat Summary-Report-NEW</a></li>
			<li><a href="adminPannel.jsp?page=VatSummary-Report">Vat Summary-Report OLD</a></li>
			<li><a href="adminPannel.jsp?page=productPurchase-VatReport">Purchase Vat Report</a></li>
			<li><a href="adminPannel.jsp?page=productSale-VatReport">Sale Vat Report</a></li>
		</ul>
		</li>
		<li><a href="#">Stock Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=medicalStock">Medical Stock</a></li>
				<li><a href="adminPannel.jsp?page=kitchenStore-StockReport">Charity Stock</a></li>
				<li><a href="kitchenStock2.jsp">Total Assets & Provisions Report </a></li>
				<li><a href="adminPannel.jsp?page=kitchenStock2New">Total Assets & Provisions Report</a></li>
				<li><a href="kitchenStockAssets.jsp">Assets Report</a></li>
				<li><a href="adminPannel.jsp?page=kitchenStockAssertsNew">Assets Report </a></li>
				 <li><a href="adminPannel.jsp?page=kitchenStockProvisions">Provisions Detailed Report</a></li> 
				<li><a href="adminPannel.jsp?page=kitchenStockProvisionsNew">Provisions Report</a></li>
				<li><a href="medicalStockReport.jsp">Medical Stock Report</a></li>
			
				<li><a href="GoldSilverReport.jsp">Gold Silver Report</a></li>
				<li><a href="adminPannel.jsp?page=GoldSilverReportNew">Gold Silver Report</a></li>
				<li><a href="adminPannel.jsp?page=ConsumerReport">Consumer Report</a></li>
				<li><a href="GoldSilverPostionReport.jsp">Gold Report</a></li>
				<li><a href="silver_Report.jsp">Silver Report</a></li>
				<li><a href="adminPannel.jsp?page=stock-performance-report">Performance Report</a></li>
	
							</ul>
		</li>
			<li><a href="#">Performance Reports</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=performance-detail-report">Performance Report</a></li>
			<li><a href="adminPannel.jsp?page=performance-detail-reportwithoutIndent">Performance Report With Out Indent</a></li>
			</ul>
		</li>		
		<li><a href="adminPannel.jsp?page=totalsalereport">Online Sales</a></li>
		<li><a href="#">Balance Sheet</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=balanceSheet">Balance Sheet</a></li>
			<li><a href="adminPannel.jsp?page=balanceSheet_new">Balance Sheet OLD</a></li>
			<li><a href="adminPannel.jsp?page=balanceSheet_newwBySitaram">Balance Sheet NEW</a></li>
			</ul>
			</li>
						 <li><a href="adminPannel.jsp?page=only_gross">Gross Amount Between The Dates</a></li>
						<li><a href="adminPannel.jsp?page=advance_amount">Advance Amount</a></li>
						<li><a href="employeePaySlip">Employee Pay Slip</a></li>
						<li><a href="adminPannel.jsp?page=paysliplist-monthwise">Employee Pay Slip</a></li>
						<li><a href="adminPannel.jsp?page=PettyCashReport">PettyCash Report</a></li>
						<li><a href="adminPannel.jsp?page=acquittanceReport">Acquittance Report</a></li>
						<li><a href="adminPannel.jsp?page=bank_lr">Bank LR Report</a></li>
						<li><a href="adminPannel.jsp?page=EWFReport">EWF Report</a></li>			
			<li><a href="adminPannel.jsp?page=prasadamExpenseReport">Prasadam Expenses Report</a></li>		
			<li><a href="#">Daily Bill Status</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=uploadBillStatus">Upload Daily Bill Status</a></li>
			<li><a href="adminPannel.jsp?page=balanceSheet_new">Balance Sheet OLD</a></li>
			<li><a href="adminPannel.jsp?page=billStatusList">Daily Bill Status List</a></li>
			</ul>
			</li>
	</ul>
</li> -->
<%-- <%} %> --%>
<%}else{%>
	
<!-- <li><a href="#">Reports</a>
	<ul>
		<li><a href="adminPannel.jsp?page=Total-Collection-Summary-Report">Collection Summary</a></li>
		<li><a href="#">Collection Summary</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=Total-Collection-Summary-Report">Collection Summary</a></li>
			<li><a href="adminPannel.jsp?page=cardsalesreport">Card Sales Report</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport">Offer Kinds DaliyReport</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport_new">Offer Kinds DaliyReport(kind-gold)</a></li></			<li><a href="adminPannel.jsp?page=offerKinds_dailySalesSilver">Offer Kinds DaliyReport(kind-silver)</a></li>
			<li><a href="adminPannel.jsp?page=offerKindsProductWise">Offer Kinds-Product Wise</a></li>
			</ul>
			</li>
			<li><a href="adminPannel.jsp?page=vendorTaxReport" target="_blank">TDS Reports</a></li> 
			<li><a href="adminPannel.jsp?page=totalPettyCash" target="_blank">Total PettyCash Report</a></li>
			<li><a href="#">GST Report</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=vendorReport">GST Report(Input)</a></li>
			<li><a href="adminPannel.jsp?page=total_sainivas_amounts">GST Report(Output)</a></li>
			</ul>
			</li>
		    <li><a href="adminPannel.jsp?page=Total-Pending-Bills">Total Pending Bills</a></li>
				<li><a href="adminPannel.jsp?page=income_and_payments">Payments & Income  Report</a></li>
						<li><a href="#">Receipts & Payments</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=receipts-payments-report">Receipts & Payments</a></li>
			<li><a href="adminPannel.jsp?page=receipts-payments-report_new">Receipts & Payments OLD</a></li>
			 <li><a href="adminPannel.jsp?page=receipts-payments-report_neww_latest">Receipts & Payments NEW</a></li> 
			</ul>
			</li>
			<li><a href="#">Income & Expenditure Report</a>
			<ul class="level-2">
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report">Income & Expenditure Report (Weekly)</a></li>
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_new">Income & Expenditure Report OLD</a></li> 
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww_latest">Income & Expenditure Report (NEW)</a></li>
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=productReport">Godown Inventory Report</a></li>
		<li><a href="adminPannel.jsp?page=ledgerReport">Ledger Report</a></li>
		<li><a href="adminPannel.jsp?page=comparison-Report-Expenses">Comparison Report Of Expenses</a></li>
		<li><a href="adminPannel.jsp?page=productReport">Product Ledger Report</a></li>
				
		<li><a href="#">Expenses Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=expensesReport">Expenses Report-Vendor</a></li>
				<li><a href="adminPannel.jsp?page=expensesReport-subhead">Expenses Report-Sub head</a></li>
			</ul>
		</li>
		 <li><a href="#">Pooja Store Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=shopSalesList">shop Sales Report</a></li>
				<li><a href="adminPannel.jsp?page=shopSales-DetailReport">ShopSales Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=PS-DayStock-ValuationReport">Stock Valuation</a></li>
				<li><a href="adminPannel.jsp?page=ProductWise-sale-report">Product wise sale report</a></li>
				<li><a href="adminPannel.jsp?page=poojaStore-StockReport">Pooja store Stock</a></li>
				<li><a href="adminPannel.jsp?page=productReportNegtiveDetailReport">Pooja store Stock Checked</a></li>
			</ul>
		</li> 
		<li><a href="adminPannel.jsp?page=trailBalanceReport">Trail Balance</a></li> --><!-- OLD LINK
		
		NEW CREATED STARTS
		<li><a href="#">Trail Balance</a>
		<ul class="level-2">
				<li><a href="adminPannel.jsp?page=trailBalanceReport">Trail Balance OLD</a></li>
				<li><a href="adminPannel.jsp?page=trailBalanceReport_new">Trail Balance NEW</a></li>
			</ul>
		</li>
		NEW CREATED ENDS
		
		
		<li><a href="adminPannel.jsp?page=Profit-Report">Profit Report</a></li>
		<li><a href="#">PRO Office Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=collectionSummary-Report">Collection Summary</a></li>
				<li><a href="adminPannel.jsp?page=proCollectionDetails-Report">Collection Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=comparisionReport">Comparison Report</a></li>
				<li><a href="adminPannel.jsp?page=comparison-Report-Month-Wise">Comparison Report Month Wise</a></li>
				<li><a href="adminPannel.jsp?page=proOffice-salesReport">Sales Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport">OfferKind Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport_ALL">Offer Kinds DaliyReport</a></li>
			</ul>
		</li>
		<li><a href="#">Vat Reports</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=Vat-Summary-ReportNew">Vat Summary-Report-NEW</a></li>
			<li><a href="adminPannel.jsp?page=VatSummary-Report">Vat Summary-Report OLD</a></li>
			<li><a href="adminPannel.jsp?page=productPurchase-VatReport">Purchase Vat Report</a></li>
			<li><a href="adminPannel.jsp?page=productSale-VatReport">Sale Vat Report</a></li>
		</ul>
		</li>
		<li><a href="#">Stock Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=medicalStock">Medical Stock</a></li>
				<li><a href="adminPannel.jsp?page=kitchenStore-StockReport">Charity Stock</a></li>
				<li><a href="kitchenStock2.jsp">Total Assets & Provisions Report </a></li>
				<li><a href="adminPannel.jsp?page=kitchenStock2New">Total Assets & Provisions Report</a></li>
				<li><a href="kitchenStockAssets.jsp">Assets Report</a></li>
				<li><a href="adminPannel.jsp?page=kitchenStockAssertsNew">Assets Report </a></li>
				 <li><a href="adminPannel.jsp?page=kitchenStockProvisions">Provisions Detailed Report</a></li> 
				<li><a href="adminPannel.jsp?page=kitchenStockProvisionsNew">Provisions Report</a></li>
				<li><a href="medicalStockReport.jsp">Medical Stock Report</a></li>
			
				<li><a href="GoldSilverReport.jsp">Gold Silver Report</a></li>
				<li><a href="adminPannel.jsp?page=GoldSilverReportNew">Gold Silver Report</a></li>
				<li><a href="adminPannel.jsp?page=ConsumerReport">Consumer Report</a></li>
				<li><a href="GoldSilverPostionReport.jsp">Gold Report</a></li>
				<li><a href="silver_Report.jsp">Silver Report</a></li>
				<li><a href="adminPannel.jsp?page=stock-performance-report">Performance Report</a></li>
	
							</ul>
		</li>
			<li><a href="#">Performance Reports</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=performance-detail-report">Performance Report</a></li>
			<li><a href="adminPannel.jsp?page=performance-detail-reportwithoutIndent">Performance Report With Out Indent</a></li>
			</ul>
		</li>		
		<li><a href="adminPannel.jsp?page=totalsalereport">Online Sales</a></li>
		<li><a href="#">Balance Sheet</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=balanceSheet">Balance Sheet</a></li>
			<li><a href="adminPannel.jsp?page=balanceSheet_new">Balance Sheet OLD</a></li>
			<li><a href="adminPannel.jsp?page=balanceSheet_neww">Balance Sheet NEW</a></li>
			</ul>
			</li>
						 <li><a href="adminPannel.jsp?page=only_gross">Gross Amount Between The Dates</a></li>
						<li><a href="adminPannel.jsp?page=advance_amount">Advance Amount</a></li>
						<li><a href="employeePaySlip">Employee Pay Slip</a></li>
						<li><a href="adminPannel.jsp?page=paysliplist-monthwise">Employee Pay Slip</a></li>
						<li><a href="adminPannel.jsp?page=PettyCashReport">PettyCash Report</a></li>
						<li><a href="adminPannel.jsp?page=acquittanceReport">Acquittance Report</a></li>
						<li><a href="adminPannel.jsp?page=bank_lr">Bank LR Report</a></li>
						<li><a href="adminPannel.jsp?page=EWFReport">EWF Report</a></li>			
			<li><a href="adminPannel.jsp?page=prasadamExpenseReport">Prasadam Expenses Report</a></li>		
			<li><a href="#">Daily Bill Status</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=uploadBillStatus">Upload Daily Bill Status</a></li>
			<li><a href="adminPannel.jsp?page=balanceSheet_new">Balance Sheet OLD</a></li>
			<li><a href="adminPannel.jsp?page=billStatusList">Daily Bill Status List</a></li>
			</ul>
			</li>
	</ul>
</li>
 -->



<%}


%>

</ul>
</div>
<div class="name">
<ul>
<li><div style="float:left; margin:0 20px 0 0;font-size: 13px;"><%=session.getAttribute("name")%></div>
<div class="dropmenu">
<dl>
<!--<dt><div class="title">Account</div></dt>-->
<dt><a href="#" onclick="openMyModal('changePassword.jsp');">Change Password</a></dt>
<dt><a href="logOut.jsp">Log out</a></dt>
</dl>

</div>

</li>
</ul></div>

</div>