<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	function filter(){
		$("#filtration-form").submit();
		//location.reload('godwanForm.jsp?poid='+poid); 
		//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
	}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IMG" class="beans.billimages"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<jsp:useBean id="GDS" class="beans.godwanstock"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<div class="vendor-page">
<div class="vendor-list" style="">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<form id="filtration-form"  action="adminPannel.jsp" method="post">
<%String status="Approved";
if(request.getParameter("status")!=null && !request.getParameter("status").equals("") ){
	status=request.getParameter("status");
}%>
<div class="search-list">
<ul>
<li><input type="hidden" name="page" value="billApprovalsList"></li>
<li><select name="status" onchange="filter();">
<!-- <option value="">Sort by Approval Status</option> -->
<option value="Approved" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("Approved")){ %>  selected="selected" <%} %>>BILLS APPROVED LIST</option>
<option value="Not-Approve"  <%if(request.getParameter("status")!=null && request.getParameter("status").equals("Not-Approve")){ %>  selected="selected" <%} %>>BILLS NOT-APPROVED LIST</option>
</select></li>
<li>
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing(); %>
<select name="head_account_id" id="head_account_id" onchange="filter();">
	<option value="" <%if(request.getParameter("head_account_id")!=null && request.getParameter("head_account_id").equals("")){ %> selected="selected"  <%} %>>ALL DEPARTMENTS</option>
	<%List HA_Lists=HOA_L.getheadofaccounts();
	if(HA_Lists.size()>0){
		for(int i=0;i<HA_Lists.size();i++){
		HOA=(beans.headofaccounts)HA_Lists.get(i);%>
	<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("") && request.getParameter("head_account_id").equals(HOA.gethead_account_id())){ %>  selected="selected" <%} %>><%=HOA.getname() %></option>
	<%}} %>
</select>
</li>
</ul>
</div>
</form>
<div class="icons">
<!-- <span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span> -->
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
</span></div>
<div class="clear"></div>

<div class="list-details">
<%mainClasses.indentListing IND_L = new indentListing();
mainClasses.billimagesListing BILLIMG=new mainClasses.billimagesListing();
DecimalFormat df=new DecimalFormat("#0.00");
indentapprovalsListing APPR_L=new indentapprovalsListing();
productexpensesListing PRDE_L=new productexpensesListing();
godwanstockListing GSK_L=new godwanstockListing();
superadminListing SAD_l=new superadminListing();
List IND_List=IND_L.getindentGroupByIndentinvoice_id("","");
vendorsListing VENL=new vendorsListing();
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
List BAPR_L=GSK_L.getgodwanstockbillApprovePending1(status,request.getParameter("head_account_id"));
System.out.println("request.getParameter//"+request.getParameter("head_account_id"));
System.out.println("request.getParameter status//"+status);
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
List pendingIndents=IND_L.getPendingIndents();%>
<%
if(BAPR_L.size()>0){%>

<style>
#yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('#yourID').offset().top - parseFloat($('#yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('#yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('#yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<div id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0" class="fixed_head "  style="margin:0 !important;padding:0 !important;background:#FFF;">
<tr>
						<td colspan="10" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
						</tr>
<tr><td colspan="10" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>						
<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500060</td></tr>						
<tr><td colspan="10" align="center" style="font-weight: bold;"> <%if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){ %> <%=HOA_CL.getHeadofAccountName(request.getParameter("head_account_id")) %> <%}else{ %> ALL  <%} %>BILLS APPROVALS LIST </td></tr>						
<tr><td colspan="11"><table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan=""><table width="100%" cellpadding="0" cellspacing="0" class=" " id="yourID" style=""><tr>
<td class="bg" width="3%"  align="center">S.NO.</td>
<td class="bg" width="8%"  align="center">Unique No</td>
<td class="bg" width="9%"  align="center">VENDOR BILL NO</td>
<td class="bg" width="8%"  align="center">COMPANY</td>
<td class="bg" width="13%" align="center">VENDOR</td>
<td class="bg" width="7%" align="center">DATE</td>
<td class="bg" width="8%"  align="center">BILL AMOUNT</td>
<td class="bg" width="6%"  align="center">STATUS</td>
<td class="bg" width="10%"  align="center">NARRATION</td>
<td class="bg" width="20%" align="center">APPROVED BY</td>
<td class="bg" width="7%"  align="center">ATTACHED BILLS</td>
</tr></table></td></tr>
</table></td></tr>



<%List APP_Det=null;
List BILL=null;
String approvedBy="";
double totAmt=0.00;
double otherCharges=0.00;
double roundoff=0.00;
int sno=1;
for(int i=0; i < BAPR_L.size(); i++ ){
	GDS=(godwanstock)BAPR_L.get(i);
	System.out.println("==GDS.getExtra1()=="+GDS.getExtra1());
	APP_Det=APPR_L.getIndentDetails(GDS.getextra1());
	if(APP_Det.size()>0){
		for(int j=0;j<APP_Det.size();j++){
			IAP=(beans.indentapprovals)APP_Det.get(j);
			if(j==0){
				approvedBy=SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME  ["+CDF.format(SDF.parse(IAP.getapproved_date()))+"]";
			}else{
				approvedBy=approvedBy+"<br></br>"+SAD_l.getSuperadminname(IAP.getadmin_id())+" --TIME ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"]";
			}
		}
	}
	 BILL=BILLIMG.getListImage(GDS.getextra1());
	 if(BILL.size()>0){
		 IMG=(beans.billimages)BILL.get(0);
	 }
	
	%>
<%-- <%if(!PRDE_L.getPRoductExpensForInvoiceWithCon(GDS.getextra1()).equals(GDS.getextra1())) { %> --%>
<tr>
<td  width="3%"  align="center"><%=sno%><%sno=sno+1; %></td>
<td  width="8%"  align="center"><div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=GDS.getExtra9()%>')"><span><%=GDS.getExtra9() %></span></div></td>
<%if(!status.equals("Not-Approve")){ %>
<td  width="6%"  align="center"><a href="adminPannel.jsp?page=expenses&invid=<%=GDS.getextra1()%>&expflag=1"><b><%=GDS.getbillNo()%><%if(GDS.getbillNo().trim().equals("")){ %>NIL<%} %></b></a></td>
<%} else { %>
<td width="6%"  align="center"><a><b><%=GDS.getbillNo()%></b></a></td>
<%} %>
<td width="8%" align="center"><%=HOA_CL.getHeadofAccountName(GDS.getdepartment())%></td>
<td width="13%" align="center"><%=VENL.getMvendorsAgenciesName(GDS.getvendorId())%></td>
<td width="7%"  align="center"><%=df2.format(dateFormat.parse(GDS.getdate()))%></td>
<%
if(GDS.getExtra8()!=null && !GDS.getExtra8().equals("")){
	otherCharges=Double.parseDouble(GDS.getExtra8());
}

if(!GDS.getExtra12().equals("")&& !GDS.getExtra11().equals(""))
{
	if(GDS.getExtra12().equals("minus")){
		totAmt=totAmt+Double.parseDouble(GDS.getpurchaseRate())+otherCharges-Double.parseDouble(GDS.getExtra11());
		roundoff=-Double.parseDouble(GDS.getExtra11());
	} else if(GDS.getExtra12().equals("plus")) {
		totAmt=totAmt+Double.parseDouble(GDS.getpurchaseRate())+otherCharges+Double.parseDouble(GDS.getExtra11());
		roundoff=Double.parseDouble(GDS.getExtra11());
	}
} else {
totAmt=totAmt+Double.parseDouble(GDS.getpurchaseRate())+otherCharges; }
%>
<td width="8%"  align="center"><%=df.format(Double.parseDouble(GDS.getpurchaseRate())+otherCharges+roundoff)%></td>
<td width="6%"  align="center"><%=GDS.getApproval_status()%></td>
<td width="10%"  align="center"><%=GDS.getdescription() %></td>
<td width="20%" align="center"><%=approvedBy %></td>
<td width="7%"  align="center"><%if(IMG.getextra1()!=null && !IMG.getextra1().equals("")){ %> <a href="../BillImages/<%=IMG.getextra1()%>" target="_blank">DOC</a><%}else{ %>N/A <%} %> </td>
</tr><%-- <%} %> --%>
<%
approvedBy="";
otherCharges=0.00;
roundoff=0.0;
} %>
<tr><td colspan="11" style="border-bottom: 3px solid #8b421b;"></td></tr>
<tr><td colspan="6" align="right" class="bg" style=""><span style="margin-right:10px;font-size:16px;color:#000;">TOTAL AMOUNT</span> </td>
<td colspan="5" align="left" class="bg" style="font-size:18px;color:#000;"><%=df.format(totAmt) %></td>
<!--<td colspan="3" class="bg"></td>--></tr></table>

</div></td></tr>
</table>
<%}else{%>
<div align="center"><h1>Payments invoices not found! </h1></div>
<%}%>
</div>
</div>
</div>
</div>