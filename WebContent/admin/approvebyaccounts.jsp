<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="mainClasses.invoiceListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.indentListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<jsp:useBean id="IND" class="beans.indent"></jsp:useBean>
<jsp:useBean id="PO" class="beans.purchaseorder"></jsp:useBean>
<jsp:useBean id="GOD" class="beans.godwanstock"></jsp:useBean>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<div class="vendor-page">
<div class="vendor-list">

<div style="text-align: center;">REPORT DETAILS </div>
<%String id=request.getParameter("TId");
indentapprovalsListing INDAP_l=new indentapprovalsListing();
SimpleDateFormat db=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
SimpleDateFormat userdate=new SimpleDateFormat("dd-MM-yyyy");
employeesListing EMPL=new employeesListing();
vendorsListing VENL=new vendorsListing();
productsListing PRODL=new productsListing();
purchaseorderListing POL=new purchaseorderListing();
productexpensesListing PRDEP_L=new productexpensesListing();
godwanstockListing GODL=new godwanstockListing();
mainClasses.superadminListing SAD_l=new mainClasses.superadminListing();
indentListing INDL=new indentListing();
DecimalFormat DF=new DecimalFormat("0.00");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
double totQty=0;
double totAmt=0.00;
String approvedBy="";
List APP_Det=null;
List INDET=INDL.getMindentsByTrackingId(id);
List PODET=POL.getMpurchaseorderBasedTrackingno(id);
List GODDET=GODL.getInvoiceDetailsBasedTrackingNo1(id);
List prductexp=PRDEP_L.getMproductexpensesBasedTrackingNo(id);
%>
<form name="accountsapprove" action="approvebyaccountsinsert.jsp" method="post">
<input type="hidden" name="uniqueno" value="<%=id %>" />
<input type="hidden" name="invid" value="<%=request.getParameter("InvId") %>" />
<div style="text-align: center;">Unique No- <%=id %></div>
<div class="clear"></div>
<div class="total-report">
<%if(INDET.size()>0){ %>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td style="text-align: left;"><span style="font-weight: bold;color: red;"><input type="checkbox" name="Indentdetail" value="" required /> Approve</span></td><td colspan="5" class="bg-new">INDENT DETAIL REPORT</td></tr>
<tr>
<td>S.NO</td>
<td>PRODUCT-ID/NAME</td>
<td>QTY</td>
<td>VENDOR</td>
<td>Raised BY</td>
<td>Raised Date</td>
</tr>
<%
for(int i=0;i<INDET.size();i++){
	IND=(beans.indent)INDET.get(i);
	totQty=totQty+Double.parseDouble(IND.getquantity());
%>
<tr>
<td><%=i+1 %></td>
<td><%=IND.getproduct_id() %>/<%=PRODL.getProductsNameByCat1(IND.getproduct_id(), "")%></td>
<td><%=IND.getquantity() %></td>
<td><%=VENL.getMvendorsAgenciesName(IND.getvendor_id()) %></td>
<td><%=EMPL.getMemployeesName(IND.getemp_id()) %></td>
<td><%=userdate.format(db.parse(IND.getdate())) %></td>
</tr>
<%}%>
<tr>
<td colspan="2"  style="text-align:right"><span>Grand Total</span></td>
<td><%=totQty %></td>
<td colspan="3"  style="text-align:right"></td>
</tr>
</table>
<%totQty=0;
} else {%>
<div style="font-weight: bold;color: red;cursor: pointer;"> NO,INDENT.</div>
<%
}
 if(PODET.size()>0){ %>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td style="text-align: left;"><span style="font-weight: bold;color: red;"><input type="checkbox" name="Indentdetail" value="" required /> Approve</span></td><td colspan="5" class="bg-new">PURCHASE ORDER DETAIL REPORT</td></tr>
<tr>
<td>S.NO</td>
<td>PRODUCT-ID/NAME</td>
<td>QTY</td>
<td>VENDOR</td>
<td>ENTERED BY</td>
<td>ENTERED Date</td>
</tr>
<%
for(int i=0;i<PODET.size();i++){
	PO=(beans.purchaseorder)PODET.get(i);
	totQty=totQty+Double.parseDouble(PO.getquantity());
%>
<tr>
<td><%=i+1 %></td>
<td><%=PO.getproduct_id() %>/<%=PRODL.getProductsNameByCat1(PO.getproduct_id(), "")%></td>
<td><%=PO.getquantity() %></td>
<td><%=VENL.getMvendorsAgenciesName(PO.getvendor_id()) %></td>
<td><%=EMPL.getMemployeesName(PO.getemp_id()) %></td>
<td><%=userdate.format(db.parse(PO.getcreated_date())) %></td>
</tr>
<%}%>
<tr>
<td colspan="2"  style="text-align:right"><span>Grand Total</span></td>
<td><%=totQty %></td>
<td colspan="3"  style="text-align:right"></td>
</tr>
</table> 
<%
totQty=0.0;
totAmt=0.0;
 } 
 else {%>
 <div style="font-weight: bold;color: red;cursor: pointer;"> NO,PURCHASE ORDER</div>
 <%
 }
 if(GODDET.size()>0){  %>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td style="text-align: left;"><span style="font-weight: bold;color: red;"><input type="checkbox" name="Indentdetail" value="" required /> Approve</span></td><td colspan="11" class="bg-new">MASTER STOCK ENTRY DETAIL REPORT</td></tr>
<tr>
<td>S.NO</td>
<td>PRODUCT-ID/NAME</td>
<td>QTY</td>
<td>PURCHASE RATE</td>
<!-- <td>VAT</td> -->
<td>CGST</td>
<td>SGST</td>
<td>IGST</td>
<td>TOTAL</td>
<td>VENDOR</td>
<td>ENTERED BY</td>
<td>VERIFIED BY</td>
</tr>
<%
for(int i=0;i<GODDET.size();i++){
	GOD=(beans.godwanstock)GODDET.get(i);
	totQty=totQty+Double.parseDouble(GOD.getquantity());
	//totAmt=totAmt+(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100)));
	if(GOD.getExtra18()!=null && !GOD.getExtra18().equals("null") && !GOD.getExtra18().equals("") && GOD.getExtra19()!=null && !GOD.getExtra19().equals("null") && !GOD.getExtra19().equals("") && GOD.getExtra20()!=null && !GOD.getExtra20().equals("null") && !GOD.getExtra20().equals("")){
		totAmt=totAmt+(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())+Double.parseDouble(GOD.getExtra18())+Double.parseDouble(GOD.getExtra19())+Double.parseDouble(GOD.getExtra20())));	
		}else{
			totAmt=totAmt+(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())));	
		}
%>
<tr>
<td><%=i+1 %></td>
<td><%=GOD.getproductId() %>/<%if(PRODL.getProductsNameByCat1(GOD.getproductId(),"")!=null && !PRODL.getProductsNameByCat1(GOD.getproductId(),"").equals("")){%><%=PRODL.getProductsNameByCat1(GOD.getproductId(), "")%><%}else{ %><%=PRODL.getProductsNameByCat(GOD.getproductId(), GOD.getdepartment())%></td><%} %>
<td><%=GOD.getquantity() %></td>
<td><%=GOD.getpurchaseRate() %></td>
<%-- <td><%=GOD.getvat() %></td> --%>
<%if(GOD.getExtra18()!=null && !GOD.getExtra18().equals("null") && !GOD.getExtra18().equals("") && GOD.getExtra19()!=null && !GOD.getExtra19().equals("null") && !GOD.getExtra19().equals("") && GOD.getExtra20()!=null && !GOD.getExtra20().equals("null") && !GOD.getExtra20().equals("")){ %>
<td><%=GOD.getExtra18() %></td>
<td><%=GOD.getExtra19() %></td>
<td><%=GOD.getExtra20() %></td>
<td><%=(DF.format(Double.parseDouble(GOD.getquantity())*((Double.parseDouble(GOD.getpurchaseRate())+Double.parseDouble(GOD.getExtra18())+Double.parseDouble(GOD.getExtra19())+Double.parseDouble(GOD.getExtra20())))*(1+Double.parseDouble(GOD.getvat())/100))) %></td>
<%}else{ %>
<td>0</td>
<td>0</td>
<td>0</td>
<td><%=DF.format(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100))) %></td>
<%} %>
<td><%=VENL.getMvendorsAgenciesName(GOD.getvendorId()) %></td>
<td><%=EMPL.getMemployeesName(GOD.getemp_id()) %></td>
<td><%if(EMPL.getMemployeesName(GOD.getCheckedby())!=null&&!EMPL.getMemployeesName(GOD.getCheckedby()).equals("")&&!EMPL.getMemployeesName(GOD.getCheckedby()).equals(EMPL.getMemployeesName(GOD.getemp_id()))){%><%=EMPL.getMemployeesName(GOD.getCheckedby()) %><%}else{ %><%=EMPL.getMemployeesName(GOD.getPo_approved_by()) %><%} %></td>
</tr>
<%}%>
<tr>
<td colspan="2"  style="text-align:right"><span>Grand Total</span></td>
<td><%=totQty %></td>
<td colspan="4"></td>
<td><%=DF.format(totAmt) %></td>
<td colspan="5"  style="text-align:right"></td>
</tr>
</table>
<%
totQty=0.0;
totAmt=0.0;
} else {%>
<div style="font-weight: bold;color: red;cursor: pointer;"> NO,MASTER STOCK ENTRY YET</div>
<%
}if(prductexp.size()>0){  %>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td style="text-align: left;"><span style="font-weight: bold;color: red;"><input type="checkbox" name="Indentdetail" value="" required /> Approve</span></td><td colspan="11" class="bg-new">PRODUCT EXPENESES REPORT</td></tr>
<tr>
<td>S.NO</td>
<td>PRODUCT-ID/NAME</td>
<td>QTY</td>
<td>PURCHASE RATE</td>
<!-- <td>VAT</td> -->
<td>CGST</td>
<td>SGST</td>
<td>IGST</td>
<td>TOTAL</td>
<td>VENDOR</td>
<td>ENTERED BY</td>
<td>VERIFIED BY</td>
</tr>
<%
for(int i=0;i<GODDET.size();i++){
	GOD=(beans.godwanstock)GODDET.get(i);
	totQty=totQty+Double.parseDouble(GOD.getquantity());
	//totAmt=totAmt+(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100)));
	if(GOD.getExtra18()!=null && !GOD.getExtra18().equals("null") && !GOD.getExtra18().equals("") && GOD.getExtra19()!=null && !GOD.getExtra19().equals("null") && !GOD.getExtra19().equals("") && GOD.getExtra20()!=null && !GOD.getExtra20().equals("null") && !GOD.getExtra20().equals("")){
		totAmt=totAmt+(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())+Double.parseDouble(GOD.getExtra18())+Double.parseDouble(GOD.getExtra19())+Double.parseDouble(GOD.getExtra20())));	
		}else{
			totAmt=totAmt+(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())));	
		}
%>
<tr>
<td><%=i+1 %></td>
<td><%=GOD.getproductId() %>/<%=PRODL.getProductsNameByCat1(GOD.getproductId(), "")%></td>
<td><%=GOD.getquantity() %></td>
<td><%=GOD.getpurchaseRate() %></td>
<%-- <td><%=GOD.getvat() %></td> --%>
<%-- <td><%=DF.format(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100))) %></td> --%>
<%if(GOD.getExtra18()!=null && !GOD.getExtra18().equals("null") && !GOD.getExtra18().equals("") && GOD.getExtra19()!=null && !GOD.getExtra19().equals("null") && !GOD.getExtra19().equals("") && GOD.getExtra20()!=null && !GOD.getExtra20().equals("null") && !GOD.getExtra20().equals("")){ %>
<td><%=GOD.getExtra18() %></td>
<td><%=GOD.getExtra19() %></td>
<td><%=GOD.getExtra20() %></td>
<td><%=(DF.format(Double.parseDouble(GOD.getquantity())*((Double.parseDouble(GOD.getpurchaseRate())+Double.parseDouble(GOD.getExtra18())+Double.parseDouble(GOD.getExtra19())+Double.parseDouble(GOD.getExtra20())))*(1+Double.parseDouble(GOD.getvat())/100))) %></td>
<%}else{ %>
<td>0</td>
<td>0</td>
<td>0</td>
<td><%=DF.format(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100))) %></td>
<%} %>
<td><%=VENL.getMvendorsAgenciesName(GOD.getvendorId()) %></td>
<td><%=EMPL.getMemployeesName(GOD.getemp_id()) %></td>
<td><%=EMPL.getMemployeesName(GOD.getCheckedby()) %></td>
</tr>
<%}%>
<tr>
<td colspan="2"  style="text-align:right"><span>Grand Total</span></td>
<td><%=totQty %></td>
<td colspan="4"></td>
<td><%=DF.format(totAmt) %></td>
<td colspan="5"  style="text-align:right"></td>
</tr>
</table>
<%} else {%>
<div style="font-weight: bold;color: red;cursor: pointer;"> NO,PRODUCT EXPENESES YET</div>
<%
}
totQty=0.0;
totAmt=0.0;%>
</div>
<div style="text-align: center;"><input type="submit" name="Approve"  value="Approve" class="click" /></div>
</form>
</div>
</div>
</body>
</html>