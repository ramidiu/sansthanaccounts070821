<%@page import="java.util.Arrays"%>
<%@page import="mainClasses.headsgroupListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>

<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	

</script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<script>
function idValidate(){
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	var id=$('#major_head_id').val().trim();
$.ajax({
	type:'post',
	url: 'IDValidate.jsp', 
	data: {
		Id : id,
		type : 'majorhead'
	},
	success: function(response) { 
		var msg=response.trim();
		$('#majorheadErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#major_head_id').focus();
			return false;
		}
	}});
}

</script>
<script type="text/javascript">

function validate(){
	$('#headIdErr').hide();
	$('#majorheadErr').hide();
	$('#headNameErr').hide();

	if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	}
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	if($('#name').val().trim()==""){
		$('#headNameErr').show();
		$('#name').focus();
		return false;
	}
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"><%if(request.getParameter("id")!=null) {%>Edit major head<%}else{%>Create a new major head <%} %></div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
if(request.getParameter("id")!=null) {
List HD_details=MajorHead_list.getMmajorhead(request.getParameter("id"));
MH=(beans.majorhead)HD_details.get(0);
%>
<form name="tablets_Update" method="post" action="majorhead_Update.jsp" onSubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="major_head_id" id="major_head_id" value="<%=MH.getmajor_head_id()%>"/>
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td>Head type*</td>
<td>Category*</td>
<td width="35%">
<div class="warning" id="majorheadErr" style="display: none;">Please Provide  "Major Head Number".</div>
Major Head No*</td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Major Head Name".</div>
Major Head Name*</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id">
<option value="<%=MH.gethead_account_id()%>"><%=HOA_L.getHeadofAccountName(MH.gethead_account_id()) %></option>
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td>
<select name="headType">
<option value="<%=MH.getextra1()%>"><%=MH.getextra1()%></option>
<option value="Development">Development</option>
<option value="non-Development">Non-Development</option>
<option value="Charity">Charity</option>
<option value="Sansthan">Sansthan</option>
<option value="Poojastore">Poojastore</option>
</select>
</td>
<td>
<select name="catType">
<option value="<%=MH.getextra2()%>"><%=MH.getextra2()%></option>
<option value="revenue">Revenue</option>
<option value="expenses">Expenses</option>
<option value="Assets">Assets</option>
<option value="Liabilities">Liabilities</option>
</select>
</td>
<td><input type="text" name="major_head_id" id="major_head_id" value="<%=MH.getmajor_head_id()%>" readonly /></td>
<td><input type="text" name="name" id="name" value="<%=MH.getname()%>" /></td>
</tr>
 <tr>
<td>IncomeExpenditure</td>&nbsp;
<td>ReceiptsPayments</td>&nbsp;
<!-- <td>BalanceSheet</td>&nbsp;
<td>LedgerReport</td>&nbsp; -->
<td>TrailBalance</td>&nbsp;
</tr> 

<tr>
<td><input type="checkbox" name="report" value="IE" <%if(MH.getextra3().contains("IE")){%>checked<%}%>></td>&nbsp;
<td><input type="checkbox" name="report" value="RP" <%if(MH.getextra3().contains("RP")){%>checked<%}%>></td>&nbsp;
<%-- <td><input type="checkbox" name="report" value="BS" <%if(MH.getextra3().contains("BS")){%>checked<%}%>></td>&nbsp;
<td><input type="checkbox" name="report" value="LR" <%if(MH.getextra3().contains("LR")){%>checked<%}%>></td>&nbsp; --%>
<td><input type="checkbox" name="report" value="TB" <%if(MH.getextra3().contains("TB")){%>checked<%}%>></td>&nbsp;
</tr>

<tr>
<td colspan="4">
<textarea name="description" id="description" placeholder="Enter description"><%=MH.getdescription()%></textarea></td>
</tr>
<tr> 
<td colspan="3"></td>
<td align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<% } else{%>
<form name="tablets_Update" method="post" action="majorhead_Insert.jsp" onSubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head  Account<span style="color: red;">*</span></td>
<td>Head Group<span style="color: red;">*</span></td>
<td>Category<span style="color: red;">*</span></td>
<td width="35%">
<div class="warning" id="majorheadErr" style="display: none;">Please Provide  "Major Head Number".</div>
<div class="warning" id="dupErr" style="display: none;">Duplicate number.Please provide another.</div>
Major Head No<span style="color: red;">*</span></td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Major Head Name".</div>
Major Head Name<span style="color: red;">*</span></td>
</tr>

<tr>
<td><select name="head_account_id" id="head_account_id">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td>
<jsp:useBean id="HG" class="beans.headsgroup"/>
<select name="headType">
<%headsgroupListing HDGL=new headsgroupListing();
List HDGList=HDGL.getheadsgroup();
if(HDGList.size()>0){
	for(int i=0;i<HDGList.size();i++){
	HG=(beans.headsgroup)HDGList.get(i);%>
<option value="<%=HG.getgroup_id()%>"><%=HG.getgroup_name()%></option>
<%}} %>

</select>
</td>
<td>
<select name="catType">
<option value="revenue">Revenue</option>
<option value="expenses">expenses</option>
<option value="Assets">Assets</option>
<option value="Liabilities">Liabilities</option>
</select>
</td>
<td><input type="text" name="major_head_id" id="major_head_id" value="" onBlur="idValidate();"/></td>
<td><input type="text" name="name" id="name" value="" /></td>
</tr>
 <tr>
<td>IncomeExpenditure</td>&nbsp;
<td>ReceiptsPayments</td>&nbsp;
<!-- <td>BalanceSheet</td>&nbsp;
<td>LedgerReport</td>&nbsp; -->
<td>TrailBalance</td>&nbsp;
</tr> 

<tr>
<td><input type="checkbox" name="report" value="IE"></td>&nbsp;
<td><input type="checkbox" name="report" value="RP"></td>&nbsp;
<!-- <td><input type="checkbox" name="report" value="BS"></td>&nbsp;
<td><input type="checkbox" name="report" value="LR"></td>&nbsp; -->
<td><input type="checkbox" name="report" value="TB"></td>&nbsp;
</tr>

 <tr>
<td colspan="4">
<textarea name="description" id="description" placeholder="Enter description" style="width:"50px;"></textarea>
</td></tr>
<!-- <td colspan="4">
<input type="checkbox" name="report" value="IE">IncomeExpenditure
<input type="checkbox" name="report" value="IE">ReceiptsPayments
<input type="checkbox" name="report" value="IE">BalanceSheet
<input type="checkbox" name="report" value="IE">LedgerReport
<input type="checkbox" name="report" value="IE">TrailBalance
</td>
</tr>  -->
<tr> 
<td colspan="3"></td>
<td align="right"><input type="submit" value="Create" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<%} %>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">

<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%
List<majorhead> MJH_list=MajorHead_list.getmajorhead();
if(MJH_list.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<div id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td colspan="6"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Major Head Name</td>
<td class="bg" width="23%">Description </td>
<td class="bg" width="23%">Head of account</td>
<td class="bg" width="20%">Edit</td>
<!-- <td class="bg" width="10%">Delete</td> -->
</tr>
</table></td></tr>
<%for(int i=0; i < MJH_list.size(); i++ ){
	MH=(beans.majorhead)MJH_list.get(i); %>
<tr>
<td width="3%"><%=MH.getmajor_head_id()%></td>
<td width="23%"><span><%=MH.getname()%></span></td>
<td width="23%"><%=MH.getdescription()%></td>
<td width="23%"><%=HOA_L.getHeadofAccountName(MH.gethead_account_id())%></td>
<td width="20%"><a href="adminPannel.jsp?page=majorHead&id=<%=MH.getmajor_head_id()%>">Edit</a></td>
<%-- <td width="10%"><a href="majorhead_Delete.jsp?Id=<%=MH.getmajor_head_id()%>">Delete</a></td> --%>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no major heads</h1></div>
<%}%>
</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>