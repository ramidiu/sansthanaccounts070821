<script>
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
</script>
<div class="vendor-name">
<div class="logo-here"><span>Sai Sansthan
Accounts</span></div>
<div class="menu">
<ul>
<li><a href="">Home</a></li>
<%if(session.getAttribute("adminId").toString().equals("EMP1000032")){ %>
<li><a href="#">Heads creation</a>
<ul>
<!-- <li><a href="adminPannel.jsp?page=mastersTree">Head of account</a></li>
<li><a href="adminPannel.jsp?page=headGroup">Head Groups</a></li>
<li><a href="adminPannel.jsp?page=majorHead">Major Heads</a></li>
<li><a href="adminPannel.jsp?page=minorHead">Minor Heads</a></li> -->
<li><a href="adminPannel.jsp?page=subHead">Sub Heads</a></li>
<li><a href="adminPannel.jsp?page=products">Products</a></li>
</ul>
</li>
<%}else{ %>
<li><a href="#">Create</a>
	<ul>
		<li><a href="adminPannel.jsp?page=vendors">Vendor</a></li>
		<li><a href="adminPannel.jsp?page=employees">Employees</a></li>
		<li><a href="adminPannel.jsp?page=Counters">Add Counters</a></li>
		<li><a href="adminPannel.jsp?page=employeeDetailsForm">Employee Details</a></li> 
		<li><a href="adminPannel.jsp?page=employeeDetailsList">Employee Details Edit</a></li> 
		<li><a href="adminPannel.jsp?page=employeeSalarySlipForm">Generate Employee Salary Slip</a></li>
		<li><a href="adminPannel.jsp?page=poojaSaman_group">Pooja saaman group</a></li>
	</ul>
</li>
<li><a href="#">Heads creation</a>
<ul>
<li><a href="adminPannel.jsp?page=mastersTree">Head of account</a></li>
<li><a href="adminPannel.jsp?page=headGroup">Head Groups</a></li>
<li><a href="adminPannel.jsp?page=majorHead">Major Heads</a></li>
<li><a href="adminPannel.jsp?page=minorHead">Minor Heads</a></li>
<li><a href="adminPannel.jsp?page=subHead">Sub Heads</a></li>
<li><a href="adminPannel.jsp?page=products">Products</a></li>
</ul>
</li>

<li><a href="#">Bank Transactions</a>
	<ul>
		<li><a href="adminPannel.jsp?page=banks">Banks</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=countersClosed-Deposits">Amount Deposits in Bank</a></li>
				<li><a href="adminPannel.jsp?page=bank-reconciliation">Bank Reconciliation</a></li>
				<!-- <li><a href="adminPannel.jsp?page=bank_reconciliationReportOld">Bank Reconciliation Report Old</a></li> -->
				<li><a href="adminPannel.jsp?page=bank_reconciliationReportNew2">Bank Reconciliation Report New</a></li>
				<!-- <li><a href="adminPannel.jsp?page=bank_reconciliationReport">Bank Reconciliation Report New</a></li> -->
					<li><a href="adminPannel.jsp?page=contraBankEntryForm">Bank To Bank Transfer</a></li>
					<li><a href="adminPannel.jsp?page=SainivasDeposites">Sainivas Deposits</a></li>
					 <!-- <li><a href="adminPannel.jsp?page=DailycounterBalanceReport">Daily counter Balance Report OLD</a></li>  -->
					<li><a href="adminPannel.jsp?page=DailycounterBalanceReport_new">Daily counter Balance Report</a></li>
					<li><a href="adminPannel.jsp?page=bankBalanceForm">Enter Bank Opening Balance</a></li>
					<li><a href="adminPannel.jsp?page=LiabilitiesAssetsOpeningBal">Assets Liabilities Opening Balance</a></li>
					<li><a href="adminPannel.jsp?page=otherDepositsReport">Other Deposits Report</a></li>
					<li><a href="adminPannel.jsp?page=bankCategoryReport">Bank Category Report</a></li>
					
				
			</ul>
		</li>
		
		<li><a href="#">Expenses&Payments</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=billApprovalsList">BillApprovals List</a></li>
				<!-- <li><a href="adminPannel.jsp?page=paymentsSendForApprovals">Payments Send for Approvals</a></li> -->
				
				<%if(session.getAttribute("Role").toString().equals("Manger")){ %>
					<li><a href="adminPannel.jsp?page=paymentbillApprovalsListbyManager">ExpensesApprovalRequire List</a></li>
				<%} %>
				<li><a href="adminPannel.jsp?page=multipleBills-ChequeIssue">Payments Issue</a></li>	
				<li><a href="adminPannel.jsp?page=paymentsApprovalsList">GS/FSPaymentsApproval List </a></li>
				<li><a href="adminPannel.jsp?page=Successful-payments-list">Successful Payments List</a></li>
				<!-- <li><a href="adminPannel.jsp?page=test">Successful Payments New</a></li>	 -->			
				<!-- <li><a href="adminPannel.jsp?page=expenses">Expenses Entry</a></li>
				<li><a href="adminPannel.jsp?page=paymentsToVendor">Payments to vendor</a></li> -->
			</ul>
		</li>
		<li><a href="adminPannel.jsp?page=Journal_Entry_Form">Journal Voucher</a>
		<ul class="level-2">
		<!-- <li><a href="adminPannel.jsp?page=Ewf_Entry_Form">EWF Entry</a></li>
		<li><a href="adminPannel.jsp?page=Ewf_Entry_List">EWF Entry List</a></li> -->
		<li><a href="adminPannel.jsp?page=Journal_Entry_Form">Journal Voucher</a></li>
		<li><a href="adminPannel.jsp?page=Journal_Entry_list">Journal Voucher Entry List</a></li>
		</ul>
		</li>
	</ul>
</li>
<%-- <%if(session.getAttribute("Role").toString().equals("Manger")){ %> --%>
<li><a href="#">Indenting</a>
	<ul>
		<!-- <li><a href="adminPannel.jsp?page=indentReport">Raise Indent</a></li> -->
		<li><a href="adminPannel.jsp?page=indentReportList">Indents List</a></li>
		<li><a href="adminPannel.jsp?page=purchaseOrdersList">Purchase orders</a></li>
		<li><a href="adminPannel.jsp?page=billEdit">Bill Edit</a></li>
		
	</ul>
</li>
<li><a href="#">Reports</a>
	<ul>
		<!-- <li><a href="adminPannel.jsp?page=Total-Collection-Summary-Report">Collection Summary</a></li> -->
		<li><a href="#">Collection Summary</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=Total-Collection-Summary-Report">Collection Summary</a></li>
			<li><a href="adminPannel.jsp?page=cardsalesreport">Card Sales Report</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport">Offer Kinds DaliyReport</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport_new">Offer Kinds DaliyReport(kind-gold)</a></li>
			<li><a href="adminPannel.jsp?page=offerKinds_dailySalesSilver">Offer Kinds DaliyReport(kind-silver)</a></li>
			<li><a href="adminPannel.jsp?page=offerKindsProductWise">Offer Kinds-Product Wise</a></li>
			</ul>
			</li>
		<li><a href="adminPannel.jsp?page=Total-Pending-Bills">Total Pending Bills</a></li>
				<li><a href="adminPannel.jsp?page=income_and_payments">Payments & Income  Report</a></li>
						<li><a href="#">Receipts & Payments</a>
			<ul class="level-2">
			<!-- <li><a href="adminPannel.jsp?page=receipts-payments-report">Receipts & Payments</a></li> -->
			<!-- <li><a href="adminPannel.jsp?page=receipts-payments-report_new">Receipts & Payments OLD</a></li> -->
			 <li><a href="adminPannel.jsp?page=receipts-payments-report_neww">Receipts & Payments NEW</a></li> 
			</ul>
			</li>
			<li><a href="#">Income & Expenditure Report</a>
			<ul class="level-2">
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report">Income & Expenditure Report (Weekly)</a></li>
					<!-- <li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_new">Income & Expenditure Report OLD</a></li> --> 
					<li><a href="adminPannel.jsp?page=DayWise-Income-Expenditure-Report_neww">Income & Expenditure Report (NEW)</a></li>
			</ul>
		</li>

		<li><a href="adminPannel.jsp?page=productReport">Godown Inventory Report</a></li>
		<li><a href="adminPannel.jsp?page=ledgerReport">Ledger Report</a></li>
		<li><a href="adminPannel.jsp?page=comparison-Report-Expenses">Comparison Report Of Expenses</a></li>
		<li><a href="adminPannel.jsp?page=productReport">Product Ledger Report</a></li>
				
		<li><a href="#">Expenses Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=expensesReport">Expenses Report-Vendor</a></li>
				<li><a href="adminPannel.jsp?page=expensesReport-subhead">Expenses Report-Sub head</a></li>
			</ul>
		</li>
		 <li><a href="#">Pooja Store Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=shopSalesList">shop Sales Report</a></li>
				<li><a href="adminPannel.jsp?page=shopSales-DetailReport">ShopSales Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=PS-DayStock-ValuationReport">Stock Valuation</a></li>
				<li><a href="adminPannel.jsp?page=ProductWise-sale-report">Product wise sale report</a></li>
				<li><a href="adminPannel.jsp?page=poojaStore-StockReport">Pooja store Stock</a></li>
				<li><a href="adminPannel.jsp?page=productReportNegtiveDetailReport">Pooja store Stock Checked</a></li>
			</ul>
		</li> 
		<!-- <li><a href="adminPannel.jsp?page=trailBalanceReport">Trail Balance</a></li> --><!-- OLD LINK -->
		
		<!-- NEW CREATED STARTS-->
		<li><a href="#">Trail Balance</a>
		<ul class="level-2">
				<!-- <li><a href="adminPannel.jsp?page=trailBalanceReport">Trail Balance OLD</a></li> -->
				<li><a href="adminPannel.jsp?page=trailBalanceReport_new">Trail Balance NEW</a></li>
			</ul>
		</li>
		<!-- NEW CREATED ENDS-->
		
		
		<li><a href="adminPannel.jsp?page=Profit-Report">Profit Report</a></li>
		<li><a href="#">PRO Office Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=collectionSummary-Report">Collection Summary</a></li>
				<li><a href="adminPannel.jsp?page=proCollectionDetails-Report">Collection Detail Report</a></li>
				<li><a href="adminPannel.jsp?page=comparisionReport">Comparison Report</a></li>
				<li><a href="adminPannel.jsp?page=comparison-Report-Month-Wise">Comparison Report Month Wise</a></li>
				<li><a href="adminPannel.jsp?page=proOffice-salesReport">Sales Detail Report</a></li>
				<!-- <li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport">OfferKind Detail Report</a></li> -->
				<li><a href="adminPannel.jsp?page=offerKinds-dailySalesReport_ALL">Offer Kinds DaliyReport</a></li>
			</ul>
		</li>
		<!-- <li><a href="#">Vat Reports</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=Vat-Summary-ReportNew">Vat Summary-Report-NEW</a></li>
			<li><a href="adminPannel.jsp?page=VatSummary-Report">Vat Summary-Report OLD</a></li>
			<li><a href="adminPannel.jsp?page=productPurchase-VatReport">Purchase Vat Report</a></li>
			<li><a href="adminPannel.jsp?page=productSale-VatReport">Sale Vat Report</a></li>
		</ul>
		</li> -->
		<li><a href="#">Stock Reports</a>
			<ul class="level-2">
				<li><a href="adminPannel.jsp?page=medicalStock">Medical Stock</a></li>
				<li><a href="adminPannel.jsp?page=kitchenStore-StockReport">Kitchen Stock</a></li>
				<li><a href="kitchenStock2.jsp">Total Assets & Provisions Report </a></li>
				<li><a href="kitchenStockAssets.jsp">Assets Report</a></li>
				<li><a href="kitchenStockProvisions.jsp">Provisions Report</a></li>
				<li><a href="medicalStockReport.jsp">Medical Stock Report</a></li>
				<li><a href="GoldSilverReport.jsp">Gold Silver Report</a></li>
				<li><a href="GoldSilverPostionReport.jsp">Gold Silver Position Report</a></li>
				<!-- <li><a href="adminPannel.jsp?page=stock-performance-report">Performance Report</a></li> -->
	
							</ul>
		</li>
			<li><a href="#">Performance Reports</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=performance-detail-report">Performance Report</a></li>
			<li><a href="adminPannel.jsp?page=performance-detail-reportwithoutIndent">Performance Report With Out Indent</a></li>
			</ul>
		</li>		
		<li><a href="adminPannel.jsp?page=totalsalereport">Online Sales</a></li>
		<li><a href="#">Balance Sheet</a>
			<ul class="level-2">
			<!-- <li><a href="adminPannel.jsp?page=balanceSheet">Balance Sheet</a></li> -->
			<!-- <li><a href="adminPannel.jsp?page=balanceSheet_new">Balance Sheet OLD</a></li> -->
			<li><a href="adminPannel.jsp?page=balanceSheet_neww">Balance Sheet NEW</a></li>
			</ul>
			</li>
						<!--  <li><a href="adminPannel.jsp?page=only_gross">Gross Amount Between The Dates</a></li> -->
						<li><a href="adminPannel.jsp?page=advance_amount">Advance Amount</a></li>
						<!-- <li><a href="employeePaySlip">Employee Pay Slip</a></li> -->
						<li><a href="adminPannel.jsp?page=paysliplist-monthwise">Employee Pay Slip</a></li>
						<!-- <li><a href="adminPannel.jsp?page=PettyCashReport">PettyCash Report</a></li> -->
						<li><a href="adminPannel.jsp?page=acquittanceReport">Acquittance Report</a></li>
						<li><a href="adminPannel.jsp?page=bank_lr">Bank LR Report</a></li>
						<!-- <li><a href="adminPannel.jsp?page=EWFReport">EWF Report</a></li> -->			
			<li><a href="adminPannel.jsp?page=prasadamExpenseReport">Prasadam Expenses Report</a></li>		
			<li><a href="#">Daily Bill Status</a>
			<ul class="level-2">
			<li><a href="adminPannel.jsp?page=uploadBillStatus">Upload Daily Bill Status</a></li>
			<!-- <li><a href="adminPannel.jsp?page=balanceSheet_new">Balance Sheet OLD</a></li> -->
			<li><a href="adminPannel.jsp?page=billStatusList">Daily Bill Status List</a></li>
			</ul>
			</li>
	</ul>
</li>
<%-- <%} %> --%>
<%} %>
</ul>
</div>
<div class="name">
<ul>
<li><div style="float:left; margin:0 20px 0 0;"><%=session.getAttribute("name")%></div>
<div class="dropmenu">
<dl>
<!--<dt><div class="title">Account</div></dt>-->
<dt><a href="#" onclick="openMyModal('changePassword.jsp');">Change Password</a></dt>
<dt><a href="logOut.jsp">Log out</a></dt>
</dl>

</div>

</li>
</ul></div>

</div>