<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
	}

</style>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRO OFFICE KINDS DAILY REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
function displayDiv(selectId){
	var headId=$('#'+selectId).val();
	if(headId=='1'){
		$('#productId').show();
		$('#subheadId').hide();
	}else if(headId=='4') {
		$('#subheadId').show();
		$('#productId').hide();
	}
}
/*  $(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});  */
</script>
<script>
function productsearch(){
	var data1=$('#productId').val();
	  var headID="1";
	  $.post('searchKindProducts.jsp',{q:data1,hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;
		$( "#productId" ).autocomplete({source: availableTags,
		}); 
	});
} 
function subheadsearch(){
	  var data1=$('#subheadId').val();
	  var headID="4";
	  $.post('searchSubhead.jsp',{q:data1,hoa:headID,page :'Rec'},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#subheadId" ).autocomplete({source: availableTags}); 
			});
}
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page"><br></br>
		<div class="vendor-list">
		
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<div class="sj">
					 <form action="offerKinds-dailySalesReport.jsp" method="post">  
					    <select name="headID">
						<!-- <option value="offer" selected="selected">OFFERED KINDS</option> -->
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %> selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %> selected="selected" <%} %>>CHARITY</option>
						<option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %> selected="selected" <%} %>>POOJA COUNTER</option>
						</select>
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form name="searchForm" id="searchForm" action="offerKinds-dailySalesReport.jsp" method="post">
				<div class="search-list">
					<ul>
						<li><select name="headID" id="headID" onChange="displayDiv('headID')" >
						<!-- <option value="offer" selected="selected">OFFERED KINDS</option> -->
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %> selected="selected" <%} %> >SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %> selected="selected" <%} %>  >CHARITY</option>
						<option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %> selected="selected" <%} %>>POOJA COUNTER</option>
						</select></li>
						
						<li><input <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){%>style="display:block"<%}else if(request.getParameter("headID") ==null){ %>style="display:block"<%}else{ %>style="display:none"<%} %>type="text" name="subheadId" id="subheadId" value="" onkeyup="subheadsearch()" placeHolder="Find a kind sub_head here" autocomplete="off"/></li>
						<li><input  <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){%>style="display:block"<%}else{ %>style="display:none"<%} %> type="text" name="productId" id="productId" value="" onkeyup="productsearch()" placeHolder="Find a kind product here" autocomplete="off"/></li>
						
						<%-- <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")) {%>
						<li><input type="text" name="productId" id="productId" value="" onkeyup="productsearch()" placeHolder="Find a kind product here" autocomplete="off"/></li>
						<%}else if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>
						<li><input type="text" name="productId" id="productId" value="" onkeyup="subheadsearch()" placeHolder="Find a kind sub_head here" autocomplete="off"/></li>
						<%} %> --%>
						
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					<span>
						<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	banktransactionsListing BKTRAL=new banktransactionsListing();
	productsListing PRDL=new productsListing();
	employeesListing EMPL=new employeesListing();
	
	List BANK_DEP=null;
	
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:01";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:01";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:01";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	/* String productId=request.getParameter("productId"); */
	
	String hoaID=session.getAttribute("headAccountId").toString();
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoaID=request.getParameter("headID");
	}
	String productId="";
	if(request.getParameter("productId") != null && !request.getParameter("productId").equals(""))
	{
		productId = request.getParameter("productId").split(" ")[0];
	}
	if(request.getParameter("subheadId") != null && !request.getParameter("subheadId").equals(""))
	{
		productId = request.getParameter("subheadId").split(" ")[0];
	}
	
	List SAL_list=SALE_L.getofferkindSEVAS(hoaID,fromDate,toDate,productId);
	subheadListing SUBHL=new subheadListing();
	List SAL_DETAIL=null;
	double totalAmount=0.00; 
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double bankDepositTot=0.00;
	double creaditSaleTot=0.00;
	double cashSaleAmt=0.00;
	if(SAL_list.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:97%; 
    background:#d0e3fb; 
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" class=" print_table">
						<tr><td colspan="10" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST </td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;">Offered Kinds Report</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
						
						
						
						
<tr><td colspan="11" style="padding:0px; margin:0px;"><table width="100%" cellpadding="0" cellspacing="0"  class="yourID">
<tr>
							<td class="bg" width="4%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="15%" style="font-weight: bold;">INVOICE DATE</td>
							<td class="bg" width="10%" style="font-weight: bold;">RECEIPT.NO</td>
							<!-- <td class="bg" width="6%" style="font-weight: bold;">CODE</td>
							<td class="bg" width="10%" style="font-weight: bold;">KIND NAME</td> -->
							<td class="bg" width="26%" style="font-weight: bold;">DEVOTEE</td>
							<!-- <td class="bg" width="10%" style="font-weight: bold;">APPROX VALUE</td> -->
							<td class="bg" width="5%" style="font-weight: bold;">KIND QTY</td>
							<td class="bg" width="20%" style="font-weight: bold;">NARRATION</td>
							<td class="bg" width="10%" style="font-weight: bold;">TOTAL AMOUNT</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">ENTERED BY</td>
							
						</tr>
</table></td></tr>
						
						<%
						Double totalSaleAmt=0.00;
						Double totDeposit=0.00;
						if(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate)!=null && !SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate).equals("")){
							 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate));
						}
						if(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate)!=null && !BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate).equals("")){
							 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate));}
						Double openingBal=totalSaleAmt-totDeposit;
						for(int i=0; i < SAL_list.size(); i++ ){ 
							double totalamount=0.00;
							SALE=(beans.customerpurchases)SAL_list.get(i);
							if(!presentDate.equals("")){
								prevDate=presentDate;	
							}%>
							 <tr>
								<td></td>
								<td colspan="7"  style="font-weight: bold;color: blue;" ><span><%if(PRDL.getProductsNameByHid(SALE.getproductId(),SALE.getextra1())!=null && !PRDL.getProductsNameByHid(SALE.getproductId(),SALE.getextra1()).equals("")){ %>
								<%=PRDL.getProductsNameByHid(SALE.getproductId(),SALE.getextra1())%>(<%=SALE.getproductId() %>)<%}else{ %>
								<%=SUBHL.getMsubheadnames(SALE.getproductId(),hoaID)%>(<%=SALE.getproductId() %>)
								<%} %></span></td> 
								<td style="font-weight: bold;"></td>
						</tr>
							<%
							
							productId=SALE.getproductId();
							//System.out.println(productId);
							//SAL_DETAIL=SALE_L.getSalesDetailsBasedOnCode(SALE.getproductId());
							SAL_DETAIL=SALE_L.getOfferKindsPROCollectionSummaryList(hoaID,fromDate,toDate,productId);
							Double codeTotAmt=0.00;
							Double rpsamt=0.00;
							Double poundamt=0.00;
							Double dlramt=0.00;
							if(SAL_DETAIL.size()>0){ %>
								<%
								for(int s=0;s<SAL_DETAIL.size();s++){
									
									
									SA=(beans.customerpurchases)SAL_DETAIL.get(s);
							presentDate=""+chngDateFormat.format(dbDateFormat.parse(SALE.getdate()));
							if(SALE.getcash_type().equals("cash")){
								cashSaleAmt=cashSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}else{
								creaditSaleTot=creaditSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}
							%>
						    <tr style="position: relative;">
							<td style="font-weight: bold;" width="4%"><%=s+1%></td>
							<td style="font-weight: bold;" width="15%"><a href="javascript:void(0)" ><%=chngDateFormat.format(dbDateFormat.parse(SA.getdate()))%></a></td>
							<td style="font-weight: bold;" width="10%"><a href="javascript:void(0)"><%=SA.getbillingId()%></a></td>
							<%-- <td style="font-weight: bold;" width="6%"><a href="javascript:void(0)"><%=SA.getproductId()%></a></td>
							<td style="font-weight: bold;" width="10%"><span><a href="javascript:void(0)" onclick="showDetails('<%=SA.getbillingId()%>')">
								<%if(PRDL.getProductsNameByHid(SA.getproductId(),SA.getextra1())!=null && !PRDL.getProductsNameByHid(SA.getproductId(),SA.getextra1()).equals("")){ %>
								<%=PRDL.getProductsNameByHid(SA.getproductId(),SA.getextra1())%><%}else{ %>
								<%=SUBHL.getMsubheadnames(SA.getproductId(),hoaID)%>
								<%} %>								
								</a></span></td> --%>
							<td style="font-weight: bold;" width="26%"><a href="javascript:void(0)"><%=SA.getcustomername()%></a></td>
							<%totalAmount=totalAmount+Double.parseDouble(SA.gettotalAmmount());
							%>
							<%-- <td width="10%"><%=SA.getrate()%></td> --%>
							<td width="5%"><%=SA.getquantity()%></td>
							<td style="font-weight: bold;" align="center" width="20%"><%=SA.getextra2()%></td>
							<td style="font-weight: bold;" align="right" width="10%">&#8377; <%=df.format(Double.parseDouble(SA.gettotalAmmount()))%></td>
							<td align="center" width="10%"><%=EMPL.getMemployeesName(SA.getemp_id()) %></td>
						</tr>
						<%totalamount = totalamount + Double.parseDouble(SA.gettotalAmmount());  %>
					   <%}}%>
					    
							
							<tr >
						<td colspan="6" align="right" style="font-weight: bold;">TOTAL  AMOUNT | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(totalamount)%></td>
						<td style="font-weight: bold;" align="right" colspan="1"></td>
						</tr>
						<%} %>
						<tr style="border: 1px solid #000;">
						<td colspan="6" align="right" style="font-weight: bold;">KINDS APPROXIMATE TOTAL  AMOUNT | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount)%></td>
						<td style="font-weight: bold; border-right: 1px solid #000;" align="right" colspan="1"></td>
						</tr>
						
						
						
					</table>
					</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no offered kinds list found for today!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ></div></div>
</body>
</html>