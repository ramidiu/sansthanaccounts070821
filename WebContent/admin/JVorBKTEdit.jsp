<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>JVorBKTEdit</title>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="../js/jquery.blockUI.js"></script>

<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<style type="text/css">
.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
</style>
<script>
function majorheadsearch(){
	var data1=$('#major_head_id').val().split(" ");
	
	  var hoid=$("#head_account_id").val();
	  
	  $.post('searchMajor.jsp',{q:data1[0],HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 		 
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
			
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#major_head_id').autocomplete({
			source: availableTags,
			 focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val("");
				$(this).val(ui.item.label);
			}
		});  
			});
}
</script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
		}
	});
		
});
</script>
<script>
function minorheadsearch(){
		  var data1=$('#minor_head_id').val();
		  var major=$('#major_head_id').val();
		  var hoid=$("#head_account_id").val();
		  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			$('#minor_head_id').autocomplete({
				source: availableTags,
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
						$(this).val("");
					$(this).val(ui.item.label);
				}
			}); 		
	  });
}
</script>

 
 <script type="text/javascript">
function subheadsearch(){
	  var data1=$('#subheadid').val();
	  var arr = [];
	  //var data1=$("#sub_head_id").val();
	  var minor=$('#minor_head_id').val();
	  var major=$('#major_head_id').val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#subheadid').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
} 
</script>
</head>
<body>
<div class="vender-details">
<form action="upadteJVorBKTMajorMinorSubHeads.jsp" method="post">
<%

    majorheadListing majorheadListing = new majorheadListing();
    minorheadListing minorheadListing = new minorheadListing();
    subheadListing SUBHL=new subheadListing();
    headofaccountsListing headofaccounts=new headofaccountsListing();
	   
	String purchaseId=request.getParameter("purchaseId");
	String expenseId=request.getParameter("expenseId");
	String bankTransactionId=request.getParameter("bankTransactionId");
	
	String headOfAccount=request.getParameter("headOfAccount");
	
	String majorHeadId=request.getParameter("majorHeadId");
	String minorHeadId=request.getParameter("minorHeadId");
	String subHeadId=request.getParameter("subHeadId");
	String amount=request.getParameter("amount");
	String date=request.getParameter("entryDate");
	String hhmmss="";
	String datepickerDate="";
	String formatedDate="";
	if(date!=null && date!=""){
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	Date convertedToDate=dateFormat.parse(date);
	formatedDate=dateFormat.format(convertedToDate);
	datepickerDate=date.substring(0,10);
	hhmmss=date.substring(11,19);
	}
	
%>

         <input type="hidden" name="purchaseId" value="<%=purchaseId%>">
         <input type="hidden" name="expenseId" value="<%=expenseId%>">
         <input type="hidden" name="bankTransactionId" value="<%=bankTransactionId%>">
         <input type="hidden" name="headOfAccount" id="head_account_id" value="<%=headOfAccount%>">
         <input type="hidden" name="hhmmss" id="hhmmss" value="<%=hhmmss%>">
HeadOfAccount<input type="text" name="headOfAccount" value="<%=headOfAccount%><%=headofaccounts.getHeadofAccountName(headOfAccount)%>" readonly="readonly"><br>MajorHead <input type="text" name="majorHeadId" id="major_head_id" value="<%=majorHeadId%> <%=majorheadListing.getmajorheadName(majorHeadId)%>" onkeyup="majorheadsearch()" required><br>MinorHead <input type="text" name="minorhead"  id="minor_head_id" value="<%=minorHeadId%> <%=minorheadListing.getminorheadName(minorHeadId)%>"  onkeyup="minorheadsearch()"  required><br>SubHead <input type="text" name="subhead" id="subheadid" value="<%=subHeadId%> <%=SUBHL.getSuheadName(subHeadId) %>" onkeyup="subheadsearch()" required><br>Amount <input type="text" name="amount" id="amount" value="<%=amount%>"><br> createdDate <input type="text" name="fromDate" id="fromDate" class="DatePicker " value="<%=formatedDate%>">
<center><input type="submit" value="Update" class="click" ></center>
</form>
</div>
</body>
</html>