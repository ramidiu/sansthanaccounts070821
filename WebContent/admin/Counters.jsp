<%@page import="beans.counters"%>
<%@page import="mainClasses.countersListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="CUNT" class="beans.counters"/>
<div class="vendor-page">
<%countersListing CUNT_L=new countersListing();
List CNT_List=CUNT_L.getcounters();
 %>
<div class="vendor-box">
<div class="vendor-title">Counters</div>
<div class="click floatright"><a href="./newEmployee.jsp" target="_blank" onclick="openMyModal('newCounter.jsp'); return false;">New Counter</a></div>
<div style="clear:both;"></div>

<div class="unpaid-box">
<div class="unpaid">Unpaid</div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OPEN BILL</li>
</ul>
</div>

<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OVERDUE</li>
</ul>
</div>
</div>
</div>
</div>
<div class="paid-box">
<div class="unpaid">Paid</div>
<div class="bills-cash">
<div class="bill-paid">
 <ul>
<li class="bill-amount"><%=CNT_List.size()%></li>
 <li>Employees</li>
</ul>
</div>
</div>
</div>
</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%
if(CNT_List.size()>0){%>
                 <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<div class="list-details" style="margin:10px 10px 0 0" id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="7"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="3%" align="center"><input type="checkbox"></td>
<td class="bg" width="10%" align="center">Counter Id</td>
<td class="bg" width="10%" align="center">Counter Name</td>
<td class="bg" width="20%" align="center">Role</td>
<td class="bg" width="10%" align="center">Counter Code</td>
<td class="bg" width="10%" align="center">Edit Counter</td>
<td class="bg" width="10%" align="center">Delete Counter</td>
</tr>
</table></td></tr></tr>
<%for(int i=0; i < CNT_List.size(); i++ ){
	CUNT=(counters)CNT_List.get(i); %>

<tr>
<td width="3%" align="center"><input type="checkbox"></td>
<td width="10%" align="center"><%=CUNT.getcounter_id() %></td>
<td width="10%" align="center"><%=CUNT.getcounter_name()%></td>
<td width="20%" align="center"><%=CUNT.getextra1()%></td>
<td width="10%" align="center"><%=CUNT.getcounter_code()%></td>
<td width="10%" align="center"><img src="../images/edit.png" alt="Edit" title="Edit Record " style="border:none"  onclick="openMyModal('newCounter.jsp?id=<%=CUNT.getcounter_id()%>');"/></td>
<td width="10%" align="center"><a href="counters_Delete.jsp?counter_id=<%=CUNT.getcounter_id()%>"><img src="../images/delete.png" alt="Delete" title="Delete Record " style="border:none"/></a></td>
</tr>

<%} %>
</table>
<%}else{%>
<div align="center"><h1>No Counters Added Yet</h1></div>
<%}%>
</div>
</div>
</div>
</div>




