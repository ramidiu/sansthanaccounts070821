<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bank Reconcilitation Report</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>

$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
$(document).ready(function(){
$("#id").keyup(function(){
		  var hoid=$("#head_account_id").val();
		  var data1=$("#id").val();
		  $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data)
		  {
			 var response = data.trim().split("\n");
			 var bankNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 bankNames.push(d[0] +" "+ d[1]);
			 }
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=bankNames;
			 $( "#id" ).autocomplete({source: availableTags}); 
				});
	});
	});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="BKT" class="beans.banktransactions"></jsp:useBean>
</head>
<body>
<%
double openingbal=0.0;
double paymentbal=0.0;
double depositbal=0.0;
double closingbal=0.0;

DecimalFormat formatter = new DecimalFormat("#,###.00");
DecimalFormat sjf = new DecimalFormat("####");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy");
bankcalculations BNKCAL=new bankcalculations();
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
banktransactionsListing BAK_L=new mainClasses.banktransactionsListing(); 
String bankSJID[]=null;
if(request.getParameter("id")!=null)
 bankSJID=request.getParameter("id").split(" ");
String bankId="";
String bankname="";
if(bankSJID!=null && bankSJID.length>0){
bankId=bankSJID[0];
bankname=bankSJID[1];
}
List HA_Lists=HOA_L.getheadofaccounts();
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}
%>
<div class="vendor-page">
<div class="vendor-list">
<div class="total-report">
<form action="adminPannel.jsp" method="post" autocomplete="off">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="4" class="bg-new" style="color: #F00;">*** SHRI SHIRDI SAI BABA SANSTHAN TRUST ***</td></tr>
<tr><td colspan="4" class="bg-new" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td class="bg-new" colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td class="bg-new" colspan="4" align="center" style="font-weight: bold;font-size: 20px;">Bank Reconciliation Report</td></tr>

<tr>
<td width="20%">
<select name="head_account_id" id="head_account_id">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select>

<input type="text" name="id" id="id" value="<%=bankId %> <%=bankname %>" placeHolder="Find a bank here" style="height: 22px;" required/>
</td>
<td width="30%"><input type="hidden" name="page" value="bank_reconciliationReportOld">From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>" required/> </td>
<td width="30%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>" required/> </td>
<td width="15%"><input type="submit" name="search" value="Search" class="click bordernone"/></td>
</tr>

</table>
</form>

<%
fromdate=fromdate+" 00:00:01";
todate=todate+" 23:59:59";
System.out.println("hi");
if(request.getParameter("toDate")!=null && request.getParameter("fromDate")!=null && request.getParameter("id")!=null ){
	//openingbal=BNKCAL.getBankReconciliationOpeningBalance(bankId, fromdate); /* commented by pradeep */
	openingbal=BNKCAL.getBankReconciliationClosingBalance(bankId, todate);
	List BANKL=BAK_L.getBankTransactionBetweenDates(bankId,fromdate,todate,"",true,"sub");
%>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="43%">Particulars</td>
<td class="bg" width="28%">Amount</td>
<td class="bg" width="28%">Amount</td>
</tr>
<tr>
<td ><span style="font-weight: bold;">Bank balance as per cash book</span></td>
<td >0</td>
<td><%=formatter.format(openingbal)%></td>
</tr>
<%		if(BANKL.size()>0){
	for(int i=0;i<BANKL.size();i++){
	BKT=(beans.banktransactions)BANKL.get(i);
	if(i==0){
%>
<tr>
<td ><span style="font-weight: bold;">Add:</span><br/>Payments issued but not presented for payment </td>
<td ></td>
<td></td>
</tr>
<tr>
<td>TransactionId  Type Narration Date</td>
<td ></td>
<td></td>
</tr>
<%}
	//if(BKT.gettype().equals("withdraw") || BKT.gettype().equals("pettycash") || BKT.gettype().equals("cashpaid") || BKT.gettype().equals("cheque"))
	if(BKT.gettype().equals("withdraw") || BKT.gettype().equals("cheque"))
	{
		paymentbal=paymentbal+Double.parseDouble(BKT.getamount());
	%>
<tr>
<td ><%=BKT.getbanktrn_id() %>  <%=BKT.gettype() %> <%=BKT.getnarration() %> <%=CDF.format(dateFormat.parse(BKT.getdate())) %> </td>
<td ><%=formatter.format(Double.parseDouble(BKT.getamount()))%></td>
<td></td>
</tr>
<%}}%>
<tr style="font-weight: bold;">
<td>Total </td>
<td><%=formatter.format(paymentbal)%></td>
<td></td>
</tr>
<%}
 BANKL=BAK_L.getBankTransactionBetweenDates(bankId,fromdate,todate,"",true,"add");
	if(BANKL.size()>0){
	for(int i=0;i<BANKL.size();i++){
	BKT=(beans.banktransactions)BANKL.get(i);
	if(i==0){
%>

<tr>
<td ><span style="font-weight: bold;">Less:</span><br/>Deposit  not credited by bank </td>
<td ></td>
<td></td>
</tr>
<tr>
<td>TransactionId  Type Narration Date</td>
<td ></td>
<td></td>
</tr>
<%}
	if(BKT.gettype().equals("deposit")){
		depositbal=depositbal+Double.parseDouble(BKT.getamount());
	%>
<tr>
<td><%=BKT.getbanktrn_id() %>  <%=BKT.gettype() %> <%=BKT.getnarration() %> <%=CDF.format(dateFormat.parse(BKT.getdate())) %> </td>
<td><%=formatter.format(Double.parseDouble(BKT.getamount()))%></td>
<td></td>
</tr>
<%}
}} 
	closingbal=openingbal+paymentbal-depositbal;
%>
<tr style="font-weight: bold;">
<td>Total </td>
<td><%=formatter.format(depositbal)%></td>
<td></td>
</tr>
<tr  style="font-weight: bold;">
<td>Bank Balance as per book</td>
<td></td>
<td><%=formatter.format(closingbal) %></td>
</tr>
</table>
<%} %>
</div></div>
</div>
</body>
</html>