<%@page import="mainClasses.roomtypeListing"%>
<%@page import="beans.counter_balance_update"%>
<%@page import="mainClasses.counter_balance_updateListing"%>
<%@page import="beans.cashier_report"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.cashier_reportListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sainivas Deposits</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="CSHRPRT" class="beans.cashier_report"></jsp:useBean>
<jsp:useBean id="COUNTBAL" class="beans.counter_balance_update"></jsp:useBean>
</head>
<body>
<%
int no=0;
double roomrent,servicetax,luxarytax,computercharges,advanceroombooking,extrachrges,Room_roomrent,Room_servicetax,Room_luxarytax,Room_computercharges,Room_advanceroombooking,roomextrapersoncharges;
roomrent=servicetax=luxarytax=computercharges=advanceroombooking=Room_roomrent=extrachrges=Room_servicetax=Room_luxarytax=Room_computercharges=Room_advanceroombooking=roomextrapersoncharges=0.0;
cashier_reportListing CSHRPERTL=new cashier_reportListing();
counter_balance_updateListing CUNTRBAL=new counter_balance_updateListing();
roomtypeListing RMTYL=new roomtypeListing();
List CounterBookings=null; 
String fromdate="";
String RoomType[]={"RTY1000023","RTY1000024","RTY1000025","RTY1000026","RTY1000027","RTY1000028","RTY1000029","RTY100030"};
String onlyfromdate="";
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DecimalFormat df=new DecimalFormat("#.##");
c1.getActualMinimum(Calendar.DAY_OF_MONTH);
c1.add(Calendar.DATE, -2); 
fromdate=(dateFormat2.format(c1.getTime())).toString();
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
	 fromdate=request.getParameter("fromDate");
}

List counterspending=CSHRPERTL.getSainivasCounterBalanceNotDepositedList(fromdate);
%>
	<div>
			<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form  name="departmentsearch" id="departmentsearch" method="post">
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="2">From Date</td>
</tr>
			<tr>
				<td colspan="2"> <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
				

<td><input type="submit" value="SEARCH" class="click" /></td>
</tr>
</table></form>
	<div class="list-details">

	<div class="printable">
	
	<div class="total-report">
	<form name="sainivasdeposites" action="">
						<div style="float:left; width:1270px; margin:0 0px 10px 0">
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr>
						<td colspan="7" align="center"  class="bg-new" style="font-weight: bold;color: teal;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td>
					</tr>
					<tr><td colspan="7" align="center" class="bg-new" style="font-weight: bold;color: teal;">Dilsukhnagar
					<input type="hidden" name="page" id="page" value="Journal_Entry_Form" />
					</td></tr>
					<tr><td colspan="7" align="center"  class="bg-new" style="font-weight: bold;color: teal;">Sainivas Deposits</td></tr>
		<%
					if(counterspending.size()>0){
						for(int i=0;i<counterspending.size();i++){
							CSHRPRT=(cashier_report)counterspending.get(i);
							if(RoomType.length>0){
								%>
								<tr style="color: #EE0E70;"><td colspan="7"><%=CSHRPRT.getcash6()%></td></tr>
								<%
								for(int l=0;l<RoomType.length;l++){
							CounterBookings=CUNTRBAL.getSainivasCounter_Booking(CSHRPRT.getlogin_id(), CSHRPRT.getcash1(),RoomType[l]);
							if(CounterBookings.size()>0){
								Room_roomrent=Room_servicetax=Room_luxarytax=Room_computercharges=Room_advanceroombooking=roomextrapersoncharges=0.0;
								%>
<tr style="color: #F3820B;"><td><%=RMTYL.getName(RoomType[l]) %></td><td>Room Rent</td><td>Service Tax</td><td>Luxury Tax</td><td>Computer Charges</td><td>ExtraPerson Charges</td><td>Advance Room Booking</td></tr>
								<%
								for(int j=0;j<CounterBookings.size();j++){
									COUNTBAL=(counter_balance_update)CounterBookings.get(j);
									roomrent=roomrent+Double.parseDouble(COUNTBAL.getcounter_ex4());
									servicetax=servicetax+Double.parseDouble(COUNTBAL.getcounter_ex2());
									luxarytax=luxarytax+Double.parseDouble(COUNTBAL.getcounter_ex3());
									computercharges=computercharges+Double.parseDouble(COUNTBAL.getcounter_ex5());
									advanceroombooking=advanceroombooking+Double.parseDouble(COUNTBAL.getcounter_ex1());
									Room_roomrent=Room_roomrent+Double.parseDouble(COUNTBAL.getcounter_ex4());
									Room_servicetax=Room_servicetax+Double.parseDouble(COUNTBAL.getcounter_ex2());
									Room_luxarytax=Room_luxarytax+Double.parseDouble(COUNTBAL.getcounter_ex3());
									Room_computercharges=Room_computercharges+Double.parseDouble(COUNTBAL.getcounter_ex5());
									Room_advanceroombooking=Room_advanceroombooking+Double.parseDouble(COUNTBAL.getcounter_ex1());
									extrachrges=extrachrges+Double.parseDouble(COUNTBAL.getc_b_id());	
									roomextrapersoncharges=roomextrapersoncharges+Double.parseDouble(COUNTBAL.getc_b_id());
									%>
 
<tr><td><%=COUNTBAL.getbooking_id()%></td><td><%=COUNTBAL.getcounter_ex4()%></td><td><%=COUNTBAL.getcounter_ex2()%></td><td><%=COUNTBAL.getcounter_ex3()%></td><td><%=COUNTBAL.getcounter_ex5()%></td>
<td><%=COUNTBAL.getc_b_id()%></td><td><%=COUNTBAL.getcounter_ex1()%></td></tr>
									<%}%>
									
													<tr style="font-weight: bold;color: teal;"><td style="color: #EE0E70;"><%=RMTYL.getName(RoomType[l]) %> Total</td><td><%=Room_roomrent%></td><td><%=Room_servicetax%></td><td><%=Room_luxarytax%></td><td><%=Room_computercharges%></td><td><%=roomextrapersoncharges %></td>
<td><%=Room_advanceroombooking%>
<input type="hidden" name="roomid<%=no%>"  value="<%=RMTYL.getName(RoomType[l]) %>" />
<input type="hidden" name="roomrent<%=no%>"  value="<%=Room_roomrent %>" />
<input type="hidden" name="servicetax<%=no%>"  value="<%= Room_servicetax%>" />
<input type="hidden" name="luxurytax<%=no%>"  value="<%=Room_luxarytax %>" />
<input type="hidden" name="computercharges<%=no%>" value="<%=Room_computercharges%>" /> 
<input type="hidden" name="extrapersoncharges<%=no%>" value="<%=roomextrapersoncharges%>" /> 
</td></tr>					
									<% no++;}}}}}%>
									<tr style="font-weight: bold;color: teal;background-color: #DBCAF1;"><td   >Grand Total</td><td><%=roomrent%></td><td><%=servicetax%></td><td><%=luxarytax%></td><td><%=computercharges%></td><td><%=computercharges%></td>
<td><%=advanceroombooking%>
<input type="hidden" name="advanceamount"  value="<%=advanceroombooking%>" />
<input type="hidden" name="noofrooms"  value="<%=no%>" />
</td></tr>
</table></div>

<input type="submit" value="Deposit" style="float: right; width: 100px; margin-top: 10px;border: 1px solid #65230d;"/>
</form>
</div></div></div>




</div></div></div>
</body>
</html>