<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sub Heads For Diagnostic</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="../js/jquery.blockUI.js"></script>

<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<style>
.modal-window {
    overflow: hidden;
    position: fixed;
    top: 118%;
    width: 567px !important;
    left: 63%;
    margin: 0; 
    padding: 0;
    z-index: 105;
    box-shadow: 0 0 8px #666;
    background: #fff;
    border-radius: 12px;
    border: 1px solid #999;
}
.click {
    border: 1px solid #65230d;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    padding: 5px 15px 5px 15px;
    text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
    font-weight: bold;
    text-align: center;
    color: #FFF;
    background-color: #ffc579;
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
    background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
    background-image: -moz-linear-gradient(top, #c88b2e, #671811);
    background-image: -ms-linear-gradient(top, #c88b2e, #671811);
    background-image: -o-linear-gradient(top, #c88b2e, #671811);
    background-image: linear-gradient(top, #c88b2e, #671811);
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
    cursor: pointer;
}
</style>
</head>
<body>
<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-list">	
       
				<div class="search-list">
					
				</div>
				
				
				<div class="clear"></div>
				<div class="list-details">
				<form action="updateDiagnosticSubHeads.jsp">
				<%
				  String subHeadId=request.getParameter("subHeadId");
				  String extra2=request.getParameter("extra2");
				  String extra4=request.getParameter("extra4");
				%>
				<input type="hidden" name="subHeadId" value="<%=subHeadId%>">
Patient Amount<input type="text"  name="extra2" value="<%=extra2%>"><br>
SansthanAmount<input type="text" name="extra4" value="<%=extra4%>">
<br>
<br>
<center>
<input type="submit" value="Update" class="click">
</center>
</form>
</div></div></div>
</body>
</html>