<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="beans.banktransactions"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
        <script type="text/javascript">
    function formsubmit(){
    	if($( "#major_head_id" ).val()!=""){    		
        	document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReport&majrhdid="+$( '#major_head_id' ).val()+"&cumltv="+$('#cumltv').val();
        	document.getElementById("departmentsearch").submit();
    		    	} else if($( "#minor_head_id" ).val()!=""){
    		    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReport&subhid="+$( '#minor_head_id' ).val()+"&cumltv="+$('#cumltv').val();
    		        	document.getElementById("departmentsearch").submit();
    		    	}  else{    	
    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReport&cumltv="+$('#cumltv').val();
    	document.getElementById("departmentsearch").submit();
    	}
    }</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">
<%DecimalFormat df=new DecimalFormat("#,###.00");	
SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
headofaccountsListing HOA_L=new headofaccountsListing();
mainClasses.subheadListing SUBH_L=new mainClasses.subheadListing();
SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
String subhid="";
if(request.getParameter("subhid")!=null){
	subhid=request.getParameter("subhid");
}
String hod=request.getParameter("hod");
String subhead=request.getParameter("subhd").toString();
String minorhead = request.getParameter("subhid").toString();
String finyr[]=null;
String finyrfrm="2014-04-01 00:00:01";
String finyrto="2015-03-31 23:59:59";
if(request.getParameter("financialyear")!=null){
	finyr=request.getParameter("financialyear").split("-");
	finyrfrm=finyr[0]+"-04-01 00:00:01";
	finyrto=finyr[1]+"-04-01 00:00:01";
}
String dt=request.getParameter("dt")+"-01";
String subheadName = SUBH_L.getSuheadNameBasedOnHOAandMINHD(subhead,minorhead,hod);%>
<div class="vendor-list">
				<div class="icons">
				<div style="margin-bottom: 20px;float: left;cursor: pointer;"><img src="../images/back-button (1).png" width="100" height="50" onclick="goBack()" title="print"/></div>
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
<div class="clear"></div>
<div class="list-details">
<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(SUBH_L.getHeadOFAccountID(subhid)) %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Day Wise Ledger Report Of Sub Head</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date  <%=request.getParameter("dt")+"-01" %> TO <%=request.getParameter("dt")+"-31" %> </td></tr>
						<tr>
						<td colspan="7">
						<span style="font-weight: bold; font-size:14px; padding-left:80px;"><%=subheadName %>(<%=subhead %>)</span>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="bg" width="10%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="20%" style="font-weight: bold;">Date</td>
							<td class="bg" width="25%" style="font-weight: bold;" align="right">DEBIT</td>
							<td class="bg" width="25%" style="font-weight: bold;" align="right">CREDIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="right">BALANCE</td>
						</tr>
<%

String cashtype="";
String cashtype1="";
String cashtype2="";
String cashtype3="";
String type=request.getParameter("typeserch");
double credit=0.0;
double debit=0.0;
double credit1=0.0;
double debit1=0.0;
double balance=0.0;
if(subhead.equals("20201"))
	cashtype="creditsale";
else if(subhead.equals("20206"))
	cashtype="creditsale";
else
	/* cashtype="cash";
	cashtype1="offerKind";
	cashtype2="online success";
	cashtype3="cheque";  */
	 cashtype="online pending";
	cashtype1="othercash";
	cashtype2="offerKind";
	cashtype3="journalvoucher"; 

int i=0;
productsListing PRD_L=new productsListing();
productexpensesListing PEXP_L=new productexpensesListing();
banktransactionsListing BNK_L = new banktransactionsListing();
bankbalanceListing BBAL_L=new bankbalanceListing();
minorheadListing MIN_L = new minorheadListing();

minorhead MINRHEAD = new minorhead(); 

String finYr = request.getParameter("finYear");

List MINRLIST = MIN_L.getminorhead(minorhead);
if(MINRLIST.size() > 0)
{
	MINRHEAD = (minorhead)MINRLIST.get(0);
}

String majorHeadId = MINRHEAD.getmajor_head_id();

customerpurchasesListing CUSTP_L=new customerpurchasesListing();
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
Calendar c = Calendar.getInstance();
c.setTime(sdf.parse(dt));
int month = c.get(Calendar.MONTH);
int month1=month;
while(month1==month)
{
	double subheadCreditOpeningBal = 0.0;
	double subheadDebitOpeningBal = 0.0;
	
	String onlydate = dt.substring(8, 10);
	String num1 = dt.substring(0, 4);
	int num2 = Integer.parseInt(dt.substring(2, 4))+1;
	
	if(month == 3 && onlydate.equals("01") && (request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")))
	{
		finYr = num1+"-"+num2;
		
		subheadDebitOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hod,majorHeadId,minorhead,subhead,"subhead","Assets", finYr, "debit", ""); 
	/* 	subheadCreditOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hod,majorHeadId,minorhead,subhead,"subhead","Assets", finYr, "credit", ""); */
	
		subheadCreditOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hod,majorHeadId,minorhead,subhead,"subhead","Liabilites", finYr, "credit", "");
	}

	double productSumOfExtra12 = 0.0;
	double subheadSumOfExtra12 = 0.0;
	
	if((subhead != null) && (subhead.equals("21083") || subhead.equals("21408") || subhead.equals("21521")))
	{
		if(subhead.equals("21083") && minorhead.equals("331"))
		{
			productSumOfExtra12 = Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22Extra12("","","","",dt,"","","","",hod));
			subheadSumOfExtra12 = Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22Extra12("","","","",dt,"","","","",hod));
		}
		
		else if(subhead.equals("21408") && minorhead.equals("283"))
		{
			productSumOfExtra12 = Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22Extra12("","","","",dt,"","","","",hod));
			subheadSumOfExtra12 = Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22Extra12("","","","",dt,"","","","",hod));
		}
		
		else if(subhead.equals("21521") && minorhead.equals("462"))
		{
			productSumOfExtra12 = Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22Extra12("","","","",dt,"","","","",hod));
			subheadSumOfExtra12 = Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22Extra12("","","","",dt,"","","","",hod));
		}
	}
	/* start */
	List otherAmountList2=BNK_L.getStringOtherDeposit("other-amount",majorHeadId,minorhead,subhead,hod,dt,"like");
			//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
	double otherAmount2 = 0.0;
	if(otherAmountList2.size()>0)
	{
		banktransactions OtherAmountList2 = (banktransactions)otherAmountList2.get(0);
		if(OtherAmountList2.getamount() != null)
		{
			otherAmount2 = Double.parseDouble(OtherAmountList2.getamount());
		}
	}
/* end */
%>
<tr>
<td><%=++i %></td>
<td><span style="font-weight: bold;"><a href="adminPannel.jsp?page=dateLedgerReport&typeserch=<%=type%>&date=<%=dt%>&subhd=<%=subhead%>&subhid=<%=subhid%>&hod=<%=hod%>&cumltv=<%=request.getParameter("cumltv")%>"><%=dt%></a></span></td>
<td align="right"><%=df.format(subheadDebitOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum2(subhead,"sub_head_id",minorhead,"minorhead_id",dt,"like",hod)) + Double.parseDouble(CUSTP_L.getLedgerSumBasedOnJV(subhead,"productId",minorhead,"extra7",dt,"like",cashtype3))) %></td>
	<%debit1=subheadDebitOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum2(subhead,"sub_head_id",minorhead,"minorhead_id",dt,"like",hod)) + Double.parseDouble(CUSTP_L.getLedgerSumBasedOnJV(subhead,"productId",minorhead,"extra7",dt,"like",cashtype3));
	debit=debit+subheadDebitOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum2(subhead,"sub_head_id",minorhead,"minorhead_id",dt,"like",hod)) + Double.parseDouble(CUSTP_L.getLedgerSumBasedOnJV(subhead,"productId",minorhead,"extra7",dt,"like",cashtype3));
	if(type.equals("Product")){ %>
<%-- <td align="right"><%=df.format(Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22(subhead,"sub_head_id",minorhead,"extra1",dt,cashtype,"","","",hod)) + Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22(subhead,"sub_head_id",minorhead,"extra3",dt,cashtype,"","","",hod)))%></td> --%>    <!--for getting offerkind amount will be added  -->
<td align="right"><%=df.format(subheadCreditOpeningBal+Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22(subhead,"sub_head_id",minorhead,"extra3",dt,cashtype,cashtype1,cashtype2,cashtype3,hod))-Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22Extra12(subhead,"sub_head_id",minorhead,"extra3",dt,"","","","",hod)) + productSumOfExtra12+otherAmount2)%></td>
<%
// credit1=subheadCreditOpeningBal+Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22(subhead,"sub_head_id",minorhead,"extra3",dt,cashtype,cashtype1,cashtype2,cashtype3,hod))-Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22Extra12(subhead,"sub_head_id",minorhead,"extra3",dt,"","","","",hod)) + productSumOfExtra12+otherAmount2;
credit=credit+subheadCreditOpeningBal+Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22(subhead,"sub_head_id",minorhead,"extra3",dt,cashtype,cashtype1,cashtype2,cashtype3,hod))-Double.parseDouble(CUSTP_L.getLedgerSumLikeDate22Extra12(subhead,"sub_head_id",minorhead,"extra3",dt,"","","","",hod)) + productSumOfExtra12+otherAmount2; 
		} else{ %>
<td align="right">
<%=df.format(subheadCreditOpeningBal+Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22(subhead,"sub_head_id",minorhead,"extra1",dt,cashtype,cashtype1,cashtype2,cashtype3,hod)) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(subhead,"sub_head_id",minorhead,"minorhead_id",dt,"like",cashtype3)) + Double.parseDouble(CUSTP_L.getLedgerSumDollarAmtLikeDate(subhead,"sub_head_id",dt,cashtype1,hod)) - Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22Extra12(subhead,"sub_head_id",minorhead,"extra1",dt,"","","","",hod)) + subheadSumOfExtra12+otherAmount2)%>

</td>

<%
// credit1=subheadCreditOpeningBal+Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22(subhead,"sub_head_id",minorhead,"extra1",dt,cashtype,cashtype1,cashtype2,cashtype3,hod)) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(subhead,"sub_head_id",minorhead,"minorhead_id",dt,"like",cashtype3)) + Double.parseDouble(CUSTP_L.getLedgerSumDollarAmtLikeDate(subhead,"sub_head_id",dt,cashtype1,hod)) - Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22Extra12(subhead,"sub_head_id",minorhead,"extra1",dt,"","","","",hod)) + subheadSumOfExtra12+otherAmount2;
credit=credit+subheadCreditOpeningBal+Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22(subhead,"sub_head_id",minorhead,"extra1",dt,cashtype,cashtype1,cashtype2,cashtype3,hod)) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(subhead,"sub_head_id",minorhead,"minorhead_id",dt,"like",cashtype3)) + Double.parseDouble(CUSTP_L.getLedgerSumDollarAmtLikeDate(subhead,"sub_head_id",dt,cashtype1,hod)) - Double.parseDouble(CUSTP_L.getLedgerSumOFSubheadLikeDate22Extra12(subhead,"sub_head_id",minorhead,"extra1",dt,"","","","",hod)) + subheadSumOfExtra12+otherAmount2;
} %>
<%
					                   if(debit>credit)
					                   {
					                	   balance=debit-credit;
					                   }
					                   if(credit>debit)
					                   {
					                	   balance=credit-debit;
					                   }
					                   if(credit==debit)
					                   {
					                	   balance=0.0;
					                   }
					              %>
<td style="font-weight: bold;" colspan="0" width="20%" align="right"><%=df.format(balance)%></td>
</tr>
<%
c.add(Calendar.DATE, 1);  // number of days to add
dt = sdf.format(c.getTime());
//c.setTime(sdf.parse(dt));
month = c.get(Calendar.MONTH);
}%>

</table>
			</td>
						</tr>
						<tr style="border: 1px solid #000;">
						<%-- <td colspan="5" align="right" style="font-weight: bold;">Total  </td>
						<td style="font-weight: bold;text-align: right;" >Rs.<%=df.format(debit) %></td>
						<td style="font-weight: bold;text-align: right;" >Rs.<%=df.format(credit) %> </td> --%>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="30%">Total  </td>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="25%">Rs.<%=df.format(debit) %></td>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="25%">Rs.<%=df.format(credit) %> </td>
						<%if(debit>credit){balance=debit-credit;}if(credit>debit){balance=credit-debit;}if(credit==debit){balance=0.0;}%>
						<td style="font-weight: bold;text-align: right;" colspan="0" width="20%">Rs.<%=df.format(balance) %> </td>
						</tr>
				
					</table>
			</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
