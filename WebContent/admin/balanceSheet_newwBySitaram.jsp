<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.CounterBalanceAndBankDeposits"%>
<%@page import="mainClasses.VendorsAndPayments"%>
<%@page import="beans.customerpurchases"%>
<%@page import="beans.productexpenses"%>
<%@page import="beans.bankbalance"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mainClasses.ReportesListClass"%>
<%@page import="model.SansthanAccountsDate"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.accounts.saisansthan.vendorBalances"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.minorhead" %>
<%@page import="mainClasses.minorheadListing" %>
<%@page import="beans.subhead" %>
<%@page import="mainClasses.subheadListing" %>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Major Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>

<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;

}
.new-table td{
padding: 2px 0 2px 5px !important;}


</style>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
  
 <script>

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 
</script>
   
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
</head>


<body>
<%if(session.getAttribute("adminId")!=null){ 
		customerpurchasesListing CP_L=new customerpurchasesListing();
		banktransactionsListing BNK_L = new banktransactionsListing();
		majorheadListing MH_L=new majorheadListing();
		vendorsListing VNDR=new vendorsListing();
		bankdetailsListing BNKL=new bankdetailsListing();
		bankcalculations BNKCAL=new bankcalculations();
		vendorBalances VNDBALL=new vendorBalances();
		bankbalanceListing BBL =new bankbalanceListing(); 
		Object key =null;
		Object value=null;
		Iterator VendorPayments=null;
		Calendar c1 = Calendar.getInstance(); 
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat df = new DecimalFormat("#,###.00");
		String fromdate="";
		String onlyfromdate="";
		String todate="";
		String Todate="";
		String reporttype="";
		String reporttype1="";
		String cashtype="online pending";
		String cashtype1="othercash";
		String cashtype2="offerKind";
		String cashtype3="journalvoucher";
// 		double bankopeningbal=0.0;
// 		double bankopeningbalcredit=0.0;
// 		String onlytodate="";
// 		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
// 		int lastday = c1.getActualMaximum(Calendar.DATE);
// 		c1.set(Calendar.DATE, lastday);  
// 		todate=(dateFormat2.format(c1.getTime())).toString();
// 		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
// 		int firstday = c1.getActualMinimum(Calendar.DATE);
// 		c1.set(Calendar.DATE, firstday); 
// 		fromdate=(dateFormat2.format(c1.getTime())).toString();
		
		if(request.getParameter("fromDate") != null && !request.getParameter("fromDate").equals("")){
			 fromdate=request.getParameter("fromDate");
		}
		if(request.getParameter("toDate") != null && !request.getParameter("toDate").equals("")){
			todate=request.getParameter("toDate");
		}
		headofaccountsListing HOA_L = new headofaccountsListing();
		List headaclist = HOA_L.getheadofaccounts();
		String hoaid="1";
		
		if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
			hoaid=request.getParameter("head_account_id");
		}
	%>
<div>
			<div class="vendor-page">
				<form  name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 70px;padding-top: 30px;">
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td class="border-nn" colspan="2">Select Fin Yr</td>
<td class="border-nn">From Date</td>
<td class="border-nn">To Date</td>
<td class="border-nn" colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<!-- <td class="border-nn">Report Type</td> -->
</tr>
				<tr>
			<td colspan="2">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<%@ include file="yearDropDown1.jsp" %>
					</select>
				</td>
				<td class="border-nn"> <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
				<td class="border-nn"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
				<input type="hidden" name="page" value="balanceSheet_neww"/>
				 </td>
<td colspan="2"><select name="head_account_id" id="head_account_id" onChange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
<option value="SasthanDev" >Sansthan Development</option>
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select>
<input type="hidden" name="cumltv" value="cumulative" />
</td>
<!-- <td><select name="cumltv" id="cumltv"> -->
<%-- <option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option> --%>
<%-- <option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option> --%>
<!-- </select></td> -->
<td><input type="submit" value="SEARCH" class="click" /></td>
</tr></tbody></table> </form>
				
				<% 
				String finYr = request.getParameter("finYear");			 
				String finStartYr = "";
				if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals(""))
				{
					finStartYr = request.getParameter("finYear").substring(0, 4);
				}
				
				String typeofbanks[]={"bank","cashpaid"};	
				String headgroup="";
				double total=0.0;
				double credit=0.0;
				double debit=0.0;
				double credit1=0.0;
				double debit1=0.0;
				double balance1 = 0.0;
				
				double totalPending = 0.0;
	if(fromdate != null && !fromdate.trim().equals("") && todate != null && !todate.trim().equals("")){	
	
	
	%>
				 
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>

					</span>
				</div>
				<div class="clear"></div>
				
				
				
				
				
				<div class="list-details">
	<div class="total-report">
<div class="printable" id="tblExport">
<table width="90%" cellpadding="0" cellspacing="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
<tbody><tr>
						<td colspan="6" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST  <%= HOA_L.getHeadofAccountName(hoaid) %></td>
					</tr>
					<tr><td colspan="6" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="6" align="center" style="font-weight: bold;">Balance Sheet Report</td></tr>
					<tr><td colspan="6" align="center" style="font-weight: bold;">Report From Date  <%=fromdate %> TO <%=todate %> </td></tr>
</tbody></table>
<div style="clear:both"></div>
<table width="90%" cellspacing="0" cellpadding="0" style="margin:0 auto;" class="new-table" border="1">
  <tbody><tr>
    <td width="30%" bgcolor="#CC6600">Liabilites</td>
    <td width="15%" bgcolor="#CC6600">Details-Credits</td>
    <td width="27%" bgcolor="#CC6600">Assets</td>
    <td width="15%" bgcolor="#CC6600">Details-Debits</td>
  </tr> 
  
    <tr>
  <%
  
  
  double bankopeningbal=0.0;
  double bankopeningbalcredit=0.0;
	double finOpeningBal=0.0;
	double finOpeningBal2=0.0;
	double onlinedeposits = 0.0;
	double totalBanksAndCashAmount = 0.0;
	double totalcountercash =0.0;
	double totalcashpaid =0.0;
	double grandtotal = 0.0;
	double proopeningbal=0.0;
  
	
	if(hoaid != null){
		if(hoaid.equals("SasthanDev")){
			headgroup="2";
			hoaid="4";
		} else if(hoaid.equals("4")){
			headgroup="1";
			reporttype = "pro-counter";
			reporttype1 = "Development-cash";	
		}else if(hoaid.equals("1")){
			reporttype = "Charity-cash";
			reporttype1 = "Not-Required";
		}
		else if(hoaid.equals("3")){
			reporttype = "poojastore-counter";
			reporttype1 = "Not-Required";
		}
		else if(hoaid.equals("5")){
			reporttype = "Sainivas";
			reporttype1 = "Not-Required";	
		}
	}
	
	
  ReportesListClass reportesListClass = new ReportesListClass();
  VendorsAndPayments vendorsAndPayments = new VendorsAndPayments();
  bankbalanceListing BBAL_L = new bankbalanceListing();
  
  List<majorhead> majorHeadList = new ArrayList<majorhead>();
  
 	List<majorhead> incomemajhdlist = reportesListClass.getMajorHeadesBasedOnNatcherAndHeadOfAccount(hoaid, "revenue");
 	List<majorhead> expendmajhdlist = reportesListClass.getMajorHeadesBasedOnNatcherAndHeadOfAccount(hoaid, "expenses");
 	List<majorhead> assetsmajhdlist = reportesListClass.getMajorHeadesBasedOnNatcherAndHeadOfAccount(hoaid, "Assets");
 	List<majorhead> liabilmajhdlist = reportesListClass.getMajorHeadesBasedOnNatcherAndHeadOfAccount(hoaid, "Liabilities");
 	
 	List banklist = BNKL.getBanksBasedOnHOA(hoaid);
   
 	List<bankbalance> debitAmountOpeningBalList = null;
 	List<bankbalance> creditAmountOpeningBalList = null;
 	
 	
 	if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
 	{
 		debitAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hoaid, "", "", "", finYr,"Assets", "debit", "extra5", "MONTHNAME");
 		creditAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hoaid, "", "", "", finYr,"Liabilites", "credit", "extra5", "MONTHNAME");
 	} 
  
//  	double incomeAmount = 0.0;
//  	double expenderAmount = 0.0;
 	double totalOfIncomeAndExpenderAmount = 0.0;
 	double liabilityTotalAmount = 0.0;
 	double assetTotalAmount = 0.0;

 	String fdate = fromdate + " 00:00:00";
 	String tdate = todate + " 23:59:59";
	
	Object key1 = null;
	Object value1 = null;
	double value2 = 0.0;
	String onlytodate="";
	onlyfromdate=fromdate;
	fromdate=fromdate+" 00:00:00";
	onlytodate=todate;
	todate=todate+" 23:59:59";
 	Todate=todate;
 	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
 		fromdate = finStartYr+"-04-01 00:00:00";
 		onlyfromdate =  finStartYr+"-04-01";
	}
	

// 	HashMap VendorPaymentPedning=VNDBALL.getVendorBalancesForFinYr1(hoaid,fdate,tdate,request.getParameter("finYear"),request.getParameter("cumltv"));
		 
	String subHeadIdsNotCome = "25640,25641,21137,20356,20357,25298";
	
	
		for(int aa = 1; aa <= 2; aa++){
			
			if(aa == 1){
	  			majorHeadList = incomemajhdlist;
				
	  		}else if(aa == 2){
	  			majorHeadList = expendmajhdlist;
	  		}
			
			if(majorHeadList.size() > 0 ){
				
				for(int i = 0 ; i < majorHeadList.size(); i++){
					
					MH = (majorhead) majorHeadList.get(i);
		  			
// 					List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "sub_head_id", "MONTHNAME");
					
					List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "sub_head_id", "MONTHNAME", "not a loan bank");
		  			List<banktransactions> otherAmountListForLoanBank = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "sub_head_id", "MONTHNAME", "loan bank");
		  			
					
					List<productexpenses> productexpensesList = reportesListClass.getSumOfAmountFromProductexpenses(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "pe.sub_head_id", "MONTHNAME");
					List<customerpurchases> customerpurchases = reportesListClass.getJVAmountsFromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, cashtype3, "productId", "MONTHNAME");
//						List<productexpenses> productexpensesJVDebit =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, MH.getmajor_head_id(), "", "", cashtype3, fdate, tdate, "pe.sub_head_id", "MONTHNAME", "debit");
					
// 					jv amount from productexpensec withbank and withloanbank and withvendor 
					List<productexpenses> productexpensesJV =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, MH.getmajor_head_id(), "", "", cashtype3, fdate, tdate, "pe.sub_head_id", "MONTHNAME", "credit");
					List<customerpurchases> DollarAmt = reportesListClass.getSumOfDollorAmountfromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, cashtype1, "s.sub_head_id", "MONTHNAME");
					List<customerpurchases> cardCashChequeJvAmountList = reportesListClass.getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "s.sub_head_id", "MONTHNAME");
//					List<customerpurchases> jvCreditAmountFromCustomerpurchasesList = reportesListClass.getJVCreditAmountFromCustomerPurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "s.sub_head_id", "MONTHNAME");
					
					
					
					
					double otherAmount = 0.0;
					if(otherAmountList.size()>0)
					{
						for(int j = 0 ; j < otherAmountList.size(); j++){
							banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
							if(banktransactions.getamount() != null && !subHeadIdsNotCome.contains(banktransactions.getSub_head_id()))
							{
								otherAmount = otherAmount + Double.parseDouble(banktransactions.getamount());
						
							}
						}
					}
					
				System.out.println("Other amount For Not a Loan Bank is::::  "+otherAmount);
					double otherAmountLoan = 0.0;
					if(otherAmountListForLoanBank.size()>0)
					{
						for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
							banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
							if(banktransactions.getamount() != null && !subHeadIdsNotCome.contains(banktransactions.getSub_head_id())) 
							{
								otherAmountLoan = otherAmountLoan + Double.parseDouble(banktransactions.getamount());
						
							}
						}
					}
					
					System.out.println("Other amount For Loan Bank is::::  "+otherAmountLoan);
		  			
					double debitAmountOpeningBal = 0.0;
					double creditAmountOpeningBal = 0.0;
					
					if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
						for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
							bankbalance bankbalance = debitAmountOpeningBalList.get(j);
							if(bankbalance.getExtra3().trim().equals(MH.getmajor_head_id())){
								debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
							}
						}
					}
					
					if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
						for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
							bankbalance bankbalance = creditAmountOpeningBalList.get(j);
							if(bankbalance.getExtra3().trim().equals(MH.getmajor_head_id())){
								creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
							}
						}
					}
					
						double LedgerSum = 0.0;
						double LedgerSumBasedOnJV = 0.0;
					
					
					if(productexpensesList != null && productexpensesList.size() > 0){
						for(int j = 0; j < productexpensesList.size(); j++){
							LedgerSum = LedgerSum + Double.parseDouble(productexpensesList.get(j).getamount());
						}
					}
					
					if(customerpurchases != null && customerpurchases.size() > 0){
						for(int j = 0; j < customerpurchases.size(); j++){
							LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases.get(j).gettotalAmmount());
						}
					}
					double productExpJVdebitAmount = 0.0;
//						if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
//							for(int j = 0; j < productexpensesJVDebit.size(); j++){
//								productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
//							}
//						}
					
						debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan;
						debit = debit + debit1;
						
						double productExpJVAmount = 0.0;
						
					
					if(productexpensesJV != null && productexpensesJV.size() > 0){
						for(int j = 0; j < productexpensesJV.size(); j++){
							productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpensesJV.get(j).getamount());
						}
					}
					
					double SumDollarAmt = 0.0;
					
					if(DollarAmt != null && DollarAmt.size() > 0){
						for(int j = 0; j < DollarAmt.size(); j++){
							SumDollarAmt = SumDollarAmt + Double.parseDouble(DollarAmt.get(j).gettotalAmmount());
						}
					}
					
					double cardCashChequeJvOnlineAmount = 0.0;
					
					if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
						for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
							cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(cardCashChequeJvAmountList.get(j).gettotalAmmount());
						}
					}
					double jvCreditAmountFromCustomerpurchases = 0.0;

//						if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
//							for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
//								jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
//							}
//						}
					
					credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount + jvCreditAmountFromCustomerpurchases;
					
					credit = credit + credit1;
					
				}
// 				System.out.println("debit ===== > "+df.format(debit));
// 				System.out.println("credit ===== > "+df.format(credit));
				
			}
			
			
			
		}
	
// 		System.out.println("debit 11111 ===== > "+df.format(debit));
// 		System.out.println("credit 11111 ===== > "+df.format(credit));
	
  
		double incomeAndExpDebit = debit;
		double incomeAndExpCredit = credit;
		double openingVendorAssetBalance = 0.0;
			
			/* List<godwanstock> openingVendorPendingBalances = reportesListClass.getVendorSuccessOpeningEntrysAmount(fromdate+" 00:00:00", tdate, "", hoaid); */
			
			List<godwanstock> openingVendorPendingBalances = reportesListClass.getVendorSuccessOpeningEntrysAmount(fromdate, tdate, "", hoaid);
			if(openingVendorPendingBalances != null && openingVendorPendingBalances.size() > 0){
				for(int i = 0 ; i < openingVendorPendingBalances.size(); i++){
					godwanstock godwanstock = openingVendorPendingBalances.get(i);
					openingVendorAssetBalance = openingVendorAssetBalance + Double.parseDouble(godwanstock.getExtra16());
				}
			}
		
		totalOfIncomeAndExpenderAmount = incomeAndExpCredit - incomeAndExpDebit;
		
		debit = 0.0;
		credit = 0.0;
		
 	for(int aa = 1; aa <= 2; aa++){
 	
  %>
  		<td colspan="2">
        	<table width="100%" cellspacing="0" cellpadding="0" border="1">
        			<tbody>
        			<%	
        			
				if(aa == 1){
					
					majorHeadList = liabilmajhdlist;
					
					
					
					List<String[]> vendorPendingBalances = new ArrayList<String[]>();
					
					
// 					System.out.println("fromdate =====> "+fromdate);
// 					System.out.println("tdate =====> "+tdate);
					
					if(request.getParameter("cumltv").equals("cumulative")){
						vendorPendingBalances = vendorsAndPayments.getVendorOpeningAndPendingAmount("", hoaid, request.getParameter("finYear"), "2015-03-31 00:00:00", tdate, "sum");
					}else{
						vendorPendingBalances = vendorsAndPayments.getVendorOpeningAndPendingAmount("", hoaid, request.getParameter("finYear"), fromdate, tdate, "sum");
					}
					
					if(vendorPendingBalances != null && vendorPendingBalances.size() > 0){
						for(int i = 0 ; i < vendorPendingBalances.size(); i++){
							String[] result = vendorPendingBalances.get(i);
							totalPending = totalPending + Double.parseDouble(result[5]);
						}
					
			         				%>
			         					<tr bgcolor="#CCFF00">
			         	                	<td width="47%" style="color:red">
			         	                    	<strong>SUNDRY CREDITORS</strong>
			         	                	</td>
			         	                    <td width="24%" align="right"><%=df.format(totalPending)%></td>
			         	                 </tr>
			         	              <%
			         	             if(vendorPendingBalances != null && vendorPendingBalances.size() > 0){
			     						for(int i = 0 ; i < vendorPendingBalances.size(); i++){
			     							String[] result = vendorPendingBalances.get(i);
			     							%>
			     							<tr>
							         	      	<td width="47%"><%=result[0] %> <%=result[1] %></td>
								       			 <td>
									       			 <a href="adminPannel.jsp?page=balanceSheet_VendorWise&vendor=<%=result[0] %>&date=<%=onlyfromdate%>to<%=onlytodate%>&type=<%=request.getParameter("cumltv")%>&hoa=<%=request.getParameter("head_account_id")%><%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")){%>&fy=<%=request.getParameter("finYear")%><%}%>" target="_blank">
									       			 	<%=df.format(Double.parseDouble(result[5]))%>
									       			 </a>
								       			 </td>
							         	  	 </tr>
			     							<%
			     						}
			     					}
			         	              
					}
			         	           
			 	}else if(aa == 2){
		  			majorHeadList = assetsmajhdlist;
		  		}
        			
        			
        			if(majorHeadList.size() > 0 ){
        				
        					for(int i = 0 ; i < majorHeadList.size(); i++){
        						
        						MH = (majorhead) majorHeadList.get(i);
        			  			
        			  			
        			  			List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "sub_head_id", "MONTHNAME", "not a loan bank");
        			  			List<banktransactions> otherAmountListForLoanBank = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "sub_head_id", "MONTHNAME", "loan bank");
        			  			
        			  			
        						double otherAmount = 0.0;
        						if(otherAmountList.size()>0)
        						{
        							for(int j = 0 ; j < otherAmountList.size(); j++){
        								banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
        								if(banktransactions.getamount() != null && !subHeadIdsNotCome.contains(banktransactions.getSub_head_id()))
        								{
        									otherAmount = otherAmount + Double.parseDouble(banktransactions.getamount());
        							
        								}
        							}
        						}
        			  			
        						double otherAmountLoan = 0.0;
        						if(otherAmountListForLoanBank.size()>0)
        						{
        							for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
        								banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
        								if(banktransactions.getamount() != null && !subHeadIdsNotCome.contains(banktransactions.getSub_head_id()))
        								{
        									otherAmountLoan = otherAmountLoan + Double.parseDouble(banktransactions.getamount());
        							
        								}
        							}
        						}
        						
        						
        						
        						double debitAmountOpeningBal = 0.0;
        						double creditAmountOpeningBal = 0.0;
        						
        						if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
        							for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
        								bankbalance bankbalance = debitAmountOpeningBalList.get(j);
        								if(bankbalance.getExtra3().trim().equals(MH.getmajor_head_id())){
        									debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
        								}
        							}
        						}
        						
        						if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
        							for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
        								bankbalance bankbalance = creditAmountOpeningBalList.get(j);
        								if(bankbalance.getExtra3().trim().equals(MH.getmajor_head_id())){
        									creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
        								}
        							}
        						}
        						
        							double LedgerSum = 0.0;
        							double LedgerSumBasedOnJV = 0.0;
        						
        						List<productexpenses> productexpensesList = reportesListClass.getSumOfAmountFromProductexpenses(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "pe.sub_head_id", "MONTHNAME");
        						List<customerpurchases> customerpurchases = reportesListClass.getJVAmountsFromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, cashtype3, "productId", "MONTHNAME");
//         						List<productexpenses> productexpensesJVDebit =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, MH.getmajor_head_id(), "", "", cashtype3, fdate, tdate, "pe.sub_head_id", "MONTHNAME", "debit");
        						
        						if(productexpensesList != null && productexpensesList.size() > 0){
        							for(int j = 0; j < productexpensesList.size(); j++){
        								LedgerSum = LedgerSum + Double.parseDouble(productexpensesList.get(j).getamount());
        							}
        						}
        						
        						if(customerpurchases != null && customerpurchases.size() > 0){
        							for(int j = 0; j < customerpurchases.size(); j++){
        								LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases.get(j).gettotalAmmount());
        							}
        						}
        						double productExpJVdebitAmount = 0.0;
//         						if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
//         							for(int j = 0; j < productexpensesJVDebit.size(); j++){
//         								productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
//         							}
//         						}
        						
        							debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan;
        							debit = debit + debit1;
        							
        							double productExpJVAmount = 0.0;
        							
        						// jv amount from productexpensec withbank and withloanbank and withvendor 
        						List<productexpenses> productexpensesJV =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, MH.getmajor_head_id(), "", "", cashtype3, fdate, tdate, "pe.sub_head_id", "MONTHNAME", "credit");
        						
        						if(productexpensesJV != null && productexpensesJV.size() > 0){
        							for(int j = 0; j < productexpensesJV.size(); j++){
        								productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpensesJV.get(j).getamount());
        							}
        						}
        						double SumDollarAmt = 0.0;
        						List<customerpurchases> DollarAmt = reportesListClass.getSumOfDollorAmountfromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, cashtype1, "s.sub_head_id", "MONTHNAME");
        						
        						if(DollarAmt != null && DollarAmt.size() > 0){
        							for(int j = 0; j < DollarAmt.size(); j++){
        								SumDollarAmt = SumDollarAmt + Double.parseDouble(DollarAmt.get(j).gettotalAmmount());
        							}
        						}
        						
        						double cardCashChequeJvOnlineAmount = 0.0;
        						List<customerpurchases> cardCashChequeJvAmountList = reportesListClass.getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "s.sub_head_id", "MONTHNAME");
        						
        						if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
        							for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
        								cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(cardCashChequeJvAmountList.get(j).gettotalAmmount());
        							}
        						}
        						double jvCreditAmountFromCustomerpurchases = 0.0;
//         						List<customerpurchases> jvCreditAmountFromCustomerpurchasesList = reportesListClass.getJVCreditAmountFromCustomerPurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "s.sub_head_id", "MONTHNAME");
//         						if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
//         							for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
//         								jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
//         							}
//         						}
        						
        						credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount + jvCreditAmountFromCustomerpurchases;
        						
        						credit = credit + credit1;
        			  			
        						if(debit1 > credit1)
        						{
        							balance1 = debit1 - credit1;
        						}
        						if(credit1 > debit1)
        						{
        							balance1 = credit1 - debit1;
        						}
        						if(credit1 == debit1)
        						{
        							balance1 = 0.0;
        						}
        						
        						
        						if(aa == 1){
        							liabilityTotalAmount = liabilityTotalAmount + balance1;
        			  			}else if(aa == 2){
        			  				assetTotalAmount = assetTotalAmount + balance1;
        			  			}
        						
        						
        						%>
        						<tr>
			                    	<td width="47%" bgcolor="#CCFF00">
				                    	<a href="adminPannel.jsp?page=minorheadLedgerReportBySitaram&hoaid=<%=hoaid %>&majrhdid=<%=MH.getmajor_head_id()%>&fromdate=<%=fromdate%>&todate=<%=todate%>&finYear=<%=finYr%>&report=Ledger Report&cumltv=<%=request.getParameter("cumltv")%>" target="_blank">
				                    		<%=MH.getmajor_head_id() %> <%=MH.getname() %>
				                    	</a>
			                    	</td>
			                        <td width="24%" align="right" bgcolor="#CCFF00"><%=df.format(balance1) %></td>
			                     </tr>
        						
        						<%
        						
        						List<minorhead> minhdlist = reportesListClass.getMinorHeadsByMajorHeadAndHeadOfAccountId(hoaid, MH.getmajor_head_id());
        						if(minhdlist.size() > 0){ 
        							
        							for(int a = 0 ; a < minhdlist.size() ; a++){
        								
        								MNH = (minorhead) minhdlist.get(a);
        								
        								debit1 = 0.0;
        								credit1 = 0.0;
        								
        								
        								double otherAmount1 = 0.0;
        								if( otherAmountList != null && otherAmountList.size()>0)
        								{
        									for(int j = 0 ; j < otherAmountList.size(); j++){
        										banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
        										if( banktransactions.getMinor_head_id().trim().equals(MNH.getminorhead_id()) && banktransactions.getamount() != null && !subHeadIdsNotCome.contains(banktransactions.getSub_head_id()))
        										{
        											otherAmount1 = otherAmount1 + Double.parseDouble(banktransactions.getamount());
        									
        										}
        									}
        								}
        								
        								double otherAmountLoan1 = 0.0;
        								if( otherAmountListForLoanBank != null && otherAmountListForLoanBank.size()>0)
        								{
        									for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
        										banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
        										if( banktransactions.getMinor_head_id().trim().equals(MNH.getminorhead_id()) && banktransactions.getamount() != null && !subHeadIdsNotCome.contains(banktransactions.getSub_head_id()))
        										{
        											otherAmountLoan1 = otherAmountLoan1 + Double.parseDouble(banktransactions.getamount());
        									
        										}
        									}
        								}
        								
        								debitAmountOpeningBal = 0.0;
        								creditAmountOpeningBal = 0.0;
        								
        								if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
        									for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
        										bankbalance bankbalance = debitAmountOpeningBalList.get(j);
        										if(bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())){
        											debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
        										}
        									}
        								}
        								
        								if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
        									for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
        										bankbalance bankbalance = creditAmountOpeningBalList.get(j);
        										if(bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())){
        											creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
        										}
        									}
        								}
        								
        								LedgerSum = 0.0;
        								LedgerSumBasedOnJV = 0.0;
        								productExpJVdebitAmount = 0.0;
        							
        						
        								if(productexpensesList != null && productexpensesList.size() > 0){
        									for(int j = 0; j < productexpensesList.size(); j++){
        										productexpenses productexpenses = productexpensesList.get(j);
        										if(productexpenses.getminorhead_id().trim().equals(MNH.getminorhead_id())){
        											LedgerSum = LedgerSum + Double.parseDouble(productexpenses.getamount());
        										}
        									}
        								}
        								
//         								if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
//         									for(int j = 0; j < productexpensesJVDebit.size(); j++){
//         										productexpenses productexpenses = productexpensesJVDebit.get(j);
//         										if(productexpenses.getminorhead_id().trim().equals(MNH.getminorhead_id())){
//         											productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
//         										}
//         									}
//         								}
        								
        								
        								if(customerpurchases != null && customerpurchases.size() > 0){
        									for(int j = 0; j < customerpurchases.size(); j++){
        										
        										customerpurchases customerpurchases2 = customerpurchases.get(j);
        										if(customerpurchases2.getextra7().trim().equals(MNH.getminorhead_id())){
        											LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases2.gettotalAmmount());
        										}
        									}
        								}
        								
        									debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan1;
//        		 							debit = debit + debit1; 
        									
        									productExpJVAmount = 0.0;
        										if(productexpensesJV != null && productexpensesJV.size() > 0){
        											for(int j = 0; j < productexpensesJV.size(); j++){
        												productexpenses productexpenses = productexpensesJV.get(j);
        												if(productexpenses.getminorhead_id().trim().equals(MNH.getminorhead_id())){
        													productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpenses.getamount());
        												}
        											}
        										}
        										
        										SumDollarAmt = 0.0;
        										if(DollarAmt != null && DollarAmt.size() > 0){
        											for(int j = 0; j < DollarAmt.size(); j++){
        												customerpurchases customerpurchases2 = DollarAmt.get(j);
        												if(customerpurchases2.getextra7().trim().equals(MNH.getminorhead_id())){
        													SumDollarAmt = SumDollarAmt + Double.parseDouble(customerpurchases2.gettotalAmmount());
        												}
        											}
        										}
        										
        										cardCashChequeJvOnlineAmount = 0.0;
        										if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
        											for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
        												customerpurchases customerpurchases2 = cardCashChequeJvAmountList.get(j);
        												if(customerpurchases2.getextra7().trim().equals(MNH.getminorhead_id())){
        													cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(customerpurchases2.gettotalAmmount());
        												}
        											}
        										}
        										
        										jvCreditAmountFromCustomerpurchases = 0.0;
//         										if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
//         											for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
//         												customerpurchases customerpurchases2 = jvCreditAmountFromCustomerpurchasesList.get(j);
//         												if(customerpurchases2.getextra7().trim().equals(MNH.getminorhead_id())){
//         													jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
//         												}
//         											}
//         										}
        										
        										credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount1 +jvCreditAmountFromCustomerpurchases;
//        		 								credit = credit + credit1;
        								if(debit1 > credit1)
        								{
        									balance1 = debit1 - credit1;
        								}
        								if(credit1 > debit1)
        								{
        									balance1 = credit1 - debit1;
        								}
        								if(credit1 == debit1)
        								{
        									balance1 = 0.0;
        								}
        								
        								%>
        								  <tr style="padding-left:15px;color: #F00;">
						                    <td width="47%">
							                    <a style="color:red" href="adminPannel.jsp?page=subheadLedgerReportBySitaram&hoaid=<%=hoaid %>&majorHeadId=<%=MH.getmajor_head_id() %>&minorHeadId=<%=MNH.getminorhead_id() %>&fromdate=<%=fromdate%>&todate=<%=todate%>&finYear=<%=finYr%>&report=Income and Expenditure&cumltv=<%=request.getParameter("cumltv")%>" target="_blank">
							                    	<%=MNH.getminorhead_id() %> <%=MNH.getname() %>
						                    	</a>
						                    </td>
						                    <td width="22%" align="right"><%=df.format(balance1)  %></td>
						                    </tr>
					   
        								<%
        								
        								List<subhead> subhdlist = reportesListClass.getSubheadsByMinorHeadAndMajorHeadAndHeadOfAccount(hoaid, MH.getmajor_head_id(), MNH.getminorhead_id());
        								if(subhdlist.size() > 0){ 
        									
        									for(int b = 0 ; b < subhdlist.size(); b++){
        										
        										SUBH = (subhead) subhdlist.get(b);
        										
        										debit1 = 0.0;
        										credit1 = 0.0;
        										
        										double otherAmount2 = 0.0;
        											if( otherAmountList != null && otherAmountList.size()>0)
        											{
        												for(int j = 0 ; j < otherAmountList.size(); j++){
        													banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
        													if(!subHeadIdsNotCome.contains(banktransactions.getSub_head_id())){
	        													if( banktransactions.getSub_head_id().trim().equals(SUBH.getsub_head_id()) && banktransactions.getamount() != null)
	        													{
	        														otherAmount2 = otherAmount2 + Double.parseDouble(banktransactions.getamount());
	        													}
        													}
        												}
        											}
        										
        											double otherAmountLoan2 = 0.0;
        											if( otherAmountListForLoanBank != null && otherAmountListForLoanBank.size()>0)
        											{
        												for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
        													banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
        													if(!subHeadIdsNotCome.contains(banktransactions.getSub_head_id())){
	        													if( banktransactions.getSub_head_id().trim().equals(SUBH.getsub_head_id()) && banktransactions.getamount() != null)
	        													{
	        														otherAmountLoan2 = otherAmountLoan2 + Double.parseDouble(banktransactions.getamount());
	        													}
        													}
        												}
        											}
        											
        											debitAmountOpeningBal = 0.0;
        											creditAmountOpeningBal = 0.0;
        											
        											if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
        												for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
        													bankbalance bankbalance = debitAmountOpeningBalList.get(j);
        													if(bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id())){
        														debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
        													}
        												}
        											}
        											if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
        												for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
        													bankbalance bankbalance = creditAmountOpeningBalList.get(j);
        													if(bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id())){
        														creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
        													}
        												}
        											}
        											
        											LedgerSum = 0.0;
        											LedgerSumBasedOnJV = 0.0;
        											productExpJVdebitAmount = 0.0;
        										
        									
        											if(productexpensesList != null && productexpensesList.size() > 0){
        												for(int j = 0; j < productexpensesList.size(); j++){
        												
        													productexpenses productexpenses = productexpensesList.get(j);
        													if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
        														LedgerSum = LedgerSum + Double.parseDouble(productexpenses.getamount());
        													}
        												}
        											}
        											
//         											if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
//         												for(int j = 0; j < productexpensesJVDebit.size(); j++){
//         													productexpenses productexpenses = productexpensesJVDebit.get(j);
//         													if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
//         														productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
//         													}
//         												}
//         											}
        											
        											if(customerpurchases != null && customerpurchases.size() > 0){
        												for(int j = 0; j < customerpurchases.size(); j++){
        													customerpurchases customerpurchases2 = customerpurchases.get(j);
        													if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
        														LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases2.gettotalAmmount());
        													}
        												}
        											}
        												debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan2;
//        		 										debit = debit + debit1; 
        											 	
        												productExpJVAmount = 0.0;
        												if(productexpensesJV != null && productexpensesJV.size() > 0){
        													for(int j = 0; j < productexpensesJV.size(); j++){
        														productexpenses productexpenses = productexpensesJV.get(j);
        														if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
        															productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpenses.getamount());
        														}
        														
        													}
        												}
        												
        												SumDollarAmt = 0.0;
        												if(DollarAmt != null && DollarAmt.size() > 0){
        													for(int j = 0; j < DollarAmt.size(); j++){
        														customerpurchases customerpurchases2 = DollarAmt.get(j);
        														if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
        															SumDollarAmt = SumDollarAmt + Double.parseDouble(customerpurchases2.gettotalAmmount());
        														}
        													}
        												}
        												
        												cardCashChequeJvOnlineAmount = 0.0;
        												if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
        													for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
        														customerpurchases customerpurchases2 = cardCashChequeJvAmountList.get(j);
        														if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
        															cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(customerpurchases2.gettotalAmmount());
        														}
        													}
        												}
        												jvCreditAmountFromCustomerpurchases = 0.0;
//         												if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
//         													for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
//         														customerpurchases customerpurchases2 = jvCreditAmountFromCustomerpurchasesList.get(j);
//         														if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
//         															jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
//         														}
//         													}
//         												}
        												
        												credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount2 + jvCreditAmountFromCustomerpurchases;
//        		 										credit = credit + credit1;

        												if(debit1>credit1)
        							                   {
        							                	   balance1=debit1-credit1;
        							                   }
        							                   if(credit1>debit1)
        							                   {
        							                	   balance1=credit1-debit1;
        							                   }
        							                   if(credit1==debit1)
        							                   {
        							                	   balance1=0.0;
        							                   }
        							                   if(!subHeadIdsNotCome.contains(SUBH.getsub_head_id())){
        							                   
        							                   %>
        							                   <tr>
										                    <td width="47%">
											                    <a href="adminPannel.jsp?page=dateLedgerReportBySitaram&typeserch=Subhead&subheadId=<%=SUBH.getsub_head_id()%>&minorheadId=<%=MNH.getminorhead_id()%>&majorHeadId=<%=MH.getmajor_head_id() %>&hod=<%=hoaid%>&finYr=<%=finYr%>&fromdate=<%=fromdate%>&todate=<%=todate%>&cumltv=<%=request.getParameter("cumltv")%>" target="_blank">
											                    	<%=SUBH.getsub_head_id() %> <%=SUBH.getname() %>
											                    </a>
										                    </td>
										                    <td width="22%" align="left"><%=df.format(balance1)  %></td>
										               </tr>
        							                   <%
        							                   
        							                   }
        									}
        								}
        							}
        							
        						}
        					}
        					
        					if(aa == 1){
        					
        						%>
        						<tr>
	        					    <td height="28" style="color:#F00; font-weight:bold;">Excess of Income over Expenditures</td>
	        					    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=df.format(totalOfIncomeAndExpenderAmount)%></td>
        						</tr>
        						<tr>
	        					    <td height="28" style="color:#F00; font-weight:bold;">VENDOR ASSET</td>
	        					    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=df.format(openingVendorAssetBalance)%></td>
        						</tr>
        						
        						<%
        						
        						liabilityTotalAmount = liabilityTotalAmount + totalOfIncomeAndExpenderAmount + totalPending;
        						
        						liabilityTotalAmount = liabilityTotalAmount + openingVendorAssetBalance;
        						
//         					  if(banklist.size()>0)
//         					  {
//         						  int i=0;
//         						for(i=0;i<banklist.size();i++)
//         						{
//         							BNK=(bankdetails)banklist.get(i);
//         							for(int j=0;j<typeofbanks.length;j++)
//         							{
//         								bankopeningbal = BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",fromdate,typeofbanks[j],"");

//         								finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], request.getParameter("finYear"));
//         								bankopeningbal += finOpeningBal;
//         								if(bankopeningbal >0 || bankopeningbal<0)
//         								{		
        				
//         	    			bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
//         								}
//         							}
//         								} 
        								
        						
//         					String addedDate1=SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
//         					 finOpeningBal2 = BBAL_L.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));				
//         					 //System.out.println("finOpeningBal2..."+DF.format(finOpeningBal2));
//         		  			if(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate) != null && !CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate).equals(""))
//         		  			{
//         						totalcountercash = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate2(hoaid,finStartYr+"-04-01", fromdate));
//         		  				//System.out.println("totalcountercash..."+DF.format(totalcountercash));
//         		  			}
//         		  			if(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1)!= null && !BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1).equals(""))
//         		  			{
//         						totalcashpaid = Double.parseDouble(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1));
//         						//System.out.println("totalcashpaid..."+DF.format(totalcashpaid));
//         		  			}
        		  			
//         		  			if(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1)!= null && !BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1).equals(""))
//         		  			{
//         						onlinedeposits = Double.parseDouble(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1));
//         						//System.out.println("onlinedeposits..."+DF.format(onlinedeposits));
//         		  			}
//         		  			grandtotal = finOpeningBal2+totalcountercash-totalcashpaid-onlinedeposits;
//         		  			proopeningbal = grandtotal;
//         					  }
        					  



			  				if(hoaid != null && hoaid.equals("5")){
			  				
			  					double bankopeningbal1 = 0.0;
			  					
			  				String bankIds[] = {"BNK1021", "BNK1022", "BNK1024", "BNK1025"};
			  				String bankNames[] = {"STATE BANK OF INDIA LOAN A/C.NO.478", "STATE BANK OF INDIA LOAN A/C.NO.502", "STATE BANK OF INDIA LOAN A/C.NO.8728", "STATE BANK OF INDIA LOAN A/C-645"};
			  				
			  				for(int vv = 0 ;vv < bankIds.length; vv++){
			  					
			  					bankopeningbal1 = BNKCAL.getCreditBankOpeningBalance(bankIds[vv], fromdate+"-04-01 00:00:01",todate+" 23:59:59","");
			  					double finOpeningBalNew = BBAL_L.getBankOpeningBal(bankIds[vv],"bank",request.getParameter("finYear"));
			  					bankopeningbal1 += finOpeningBalNew;
			  				
			  					credit = credit + bankopeningbal1;
			  					liabilityTotalAmount = liabilityTotalAmount + bankopeningbal1;
			  					%>
			  					<tr>
			  						<td height="28" style="color:#F00; font-weight:bold;"> <%=bankIds[vv] %> <%=bankNames[vv] %></td>
			  					   <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=df.format(bankopeningbal1)%></td>
			  					</tr> 
			  					
			  					<%
			  					
			  				}
			  				
			  				}

        					}
        					
        					if(aa == 2){
        						
        				  				bankopeningbalcredit=0.0;
        				  				
        								if(banklist.size()>0)
        								{ 	
        									int i=0;
        				  					for(i=0;i<banklist.size();i++)
        				  					{
        				  						BNK=(bankdetails)banklist.get(i);
        				  						int j=0;
        				  						for(j=0;j<typeofbanks.length;j++)
        				  						{
        										  	bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",todate+" 23:59:59",typeofbanks[j],"");
        										  	finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], request.getParameter("finYear"));
        										  	bankopeningbal += finOpeningBal;
        										  	if(bankopeningbal >0 || bankopeningbal<0)
        										  	{
        										  
        				  				%> 
        				   				<tr>
        				    				<td height="28" style="color:#F00; font-weight:bold;">
	        				    				<a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?page=bankDetails&id=<%=BNK.getbank_id() %>&reportType=<%=typeofbanks[j]%>&finYear=<%=finYr%>&fromdate=<%=fromdate %>&todate=<%=todate%>" target="_blank">
				  									<strong><%=BNK.getbank_id() %> <%=BNK.getbank_name() %> <%=typeofbanks[j]%></strong>
				  								</a>
        				    				</td>
        				    				<td height="28" align="center" style="color:#F00; font-weight:bold;"><%=df.format(bankopeningbal) %></td>
        				  				</tr>
        				  				<%
        								bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
        								//bankopeningbal=0.0;
        											}
        										 }
        				  					}
        				  					
        				  					
        				  					}
        				  				
        								debit = debit + bankopeningbalcredit;
        								
        								assetTotalAmount = assetTotalAmount + bankopeningbalcredit;
        								

        								 double finOpeningBalSansthan = BBAL_L.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));
        				  
        								  String counterBal = CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate);
        								  String bankDeposit = BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",fromdate);
        								  
        								  
        								  double cashAccount = 0.0;
        								  
        								  if(counterBal != null && !counterBal.trim().equals("")){
        									  cashAccount = finOpeningBalSansthan + Double.parseDouble(counterBal);
        								  }
        								  if(bankDeposit != null && !bankDeposit.trim().equals("")){
        									  cashAccount = cashAccount - Double.parseDouble(bankDeposit);
        								  }
        								  
        								  CounterBalanceAndBankDeposits balanceAndBankDeposits = new CounterBalanceAndBankDeposits();
        								  
        								  List<customerpurchases> customerpurchasesList = balanceAndBankDeposits.getcustomerpurchasesAfterFinYearToBeforeFromDate(hoaid, fromdate+" 00:00:00", todate+" 23:59:59", "");
        								  List<banktransactions> banktransactionsList = balanceAndBankDeposits.getbanktransactionsAfterFinYearToBeforeFromDate(reporttype, reporttype1, fromdate+" 00:00:00", todate+" 23:59:59", "");
        								  
        								  
        								  if(customerpurchasesList != null && customerpurchasesList.size() > 0 ){
        									  customerpurchases customerpurchases = customerpurchasesList.get(0);
        									  if(customerpurchases != null && customerpurchases.gettotalAmmount() != null && !customerpurchases.gettotalAmmount().equals("")){
        										  cashAccount = cashAccount + Double.parseDouble(customerpurchases.gettotalAmmount());
        									  }
        								  }
        								  
        								  if(banktransactionsList != null && banktransactionsList.size() > 0 ){
        									  banktransactions banktransactions = banktransactionsList.get(0);
        									  if(banktransactions != null && banktransactions.getamount() != null && !banktransactions.getamount().equals("")){
        										  cashAccount = cashAccount - Double.parseDouble(banktransactions.getamount());
        									  }
        								  }
        								  
        								
        				  				assetTotalAmount = assetTotalAmount + cashAccount;
//         				  				double proclosingbal = grandtotalNew;
        				 					
        				  				double vendorAssetBalance = 0.0;	
        				  				
        				  				List<godwanstock> vendorPendingBalances = reportesListClass.getVendorPendingAmountOld(fromdate, tdate, "", hoaid);
        				  				if(vendorPendingBalances != null && vendorPendingBalances.size() > 0){
        				  					for(int i = 0 ; i < vendorPendingBalances.size(); i++){
        				  						godwanstock godwanstock = vendorPendingBalances.get(i);
        				  						vendorAssetBalance = vendorAssetBalance + Double.parseDouble(godwanstock.getExtra16());
        				  					}
        				  				}
        				  				vendorAssetBalance=
        				  				/* assetTotalAmount = assetTotalAmount + vendorAssetBalance; */
        				  				assetTotalAmount = assetTotalAmount + totalPending;

        				  				
        				  				
        				  				
        								%>
        				  				<tr>
        				    				<td height="28" style="color:#F00; font-weight:bold;">
	        				    				<a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?page=DailycounterBalanceReport_new&finYear=<%=finYr %>&page=totalsalereport&fromDate=<%=fromdate %>&toDate=<%=todate %>&head_account_id=<%=hoaid %>" target="_blank">
							  						<strong>CASH ACCOUNT</strong>
							  					</a>
        				    				</td>
        								    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=df.format(cashAccount)%></td>
        				  				</tr> 
        				  				<tr>
											<td height="28" style="color:#F00; font-weight:bold;">
											  		<strong>VENDOR ASSET</strong>
											</td>
										    <%-- <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=df.format(vendorAssetBalance)%></td> --%>
										    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=df.format(totalPending)%></td>
										</tr>
        				  				<% //sysout(hello)asdf
        						
        					}
        					
        					
        				}
        			%>
                    
            </tbody>
            </table>
       </td>
       <%
        			}
       %>
      </tr>
      
      <tr>
      
       	<td colspan="2">
       		<table width="100%" cellspacing="0" cellpadding="0" border="1">
        		<tbody>
        		<tr></tr>
        		<tr>
                    	 <td width="47%" style="font-weight:bold">TOTAL</td>
						 <td width="27%" align="right" style="font-weight:bold"><%=df.format(liabilityTotalAmount) %></td>
                 </tr>
<!--         		<tr> -->
<!--                     	 <td width="47%" style="font-weight:bold">Excess of Incomes Over Expend</td> -->
<!-- 						 <td width="27%" align="right" style="font-weight:bold">246182431.46</td> -->
<!--                  </tr> -->
              </tbody>
            </table>
         </td>
         <td colspan="2">
            <table width="100%" cellspacing="0" cellpadding="0" border="1">
        	<tbody>
        		<tr>
                    	 <td width="47%" style="font-weight:bold">TOTAL</td>
						 <td width="27%" align="right" style="font-weight:bold"><%=df.format(assetTotalAmount) %></td>
                 </tr>
<!--         		<tr> -->
<!--                     	 <td width="47%" style="font-weight:bold"></td> -->
<!-- 						 <td width="27%" align="right" style="font-weight:bold"></td> -->
<!--                  </tr> -->
              </tbody>
        	</table>
           </td>
      </tr>
        
                    
 					<!-- Total column ends -->
                    
  
  
  <tr></tr>

</tbody></table>
</div>
</div>
</div>
				
				
				
				
				
				
				
				
</div>
</div>

<%}}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>