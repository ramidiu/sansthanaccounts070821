<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="beans.bankdetails"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="BKT" class="beans.banktransactionsService"/>
<jsp:useBean id="BKT1" class="beans.banktransactionsService"/>
<jsp:useBean id="BNK" class="beans.bankdetailsService"/>
<jsp:useBean id="BANK" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="BANKNEW" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="BNK1" class="beans.bankdetailsService"/>
<jsp:useBean id="CSH" class="beans.cashier_reportService"/>
<%
mainClasses.bankdetailsListing BAN= new mainClasses.bankdetailsListing();
List frombankList=null;
List tobankList=null;
String bkid[]=request.getParameter("frombank").split(" ");
String frombank_id=bkid[0]; 
String tobkid[]=request.getParameter("tobank").split(" ");
String tobank_id=tobkid[0];
String name="";
boolean bkIns=false;
boolean tobanktype=false;
boolean frombanktype=false;
boolean bkUp=false;
String narration=request.getParameter("Narration");
String amount=request.getParameter("amount");
String createdBy=session.getAttribute("adminId").toString();
String extra1=request.getParameter("extra1");
String date1=request.getParameter("date");
double bankpramount=0.0;
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
String type="";
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat datetime= new SimpleDateFormat("HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));

String date=(dateFormat.format(c1.getTime())).toString();
String time=date1+" "+(datetime.format(c1.getTime())).toString();

frombankList=BAN.getMbankdetails(frombank_id);
if(frombankList.size()>0){
	BANKNEW=(bankdetails)frombankList.get(0);
}
if("credit".equals(BANKNEW.getextra2())){
	frombanktype=true;
}
%>
<jsp:setProperty name="BKT" property="bank_id" value="<%=frombank_id%>"/>
<jsp:setProperty name="BKT" property="name" value="<%=name%>"/>
<jsp:setProperty name="BKT" property="narration" value="<%=narration%>"/>
<jsp:setProperty name="BKT" property="date" value="<%=time%>"/>
<jsp:setProperty name="BKT" property="amount" value="<%=amount%>"/>
<%if(frombanktype) {%>
<jsp:setProperty name="BKT" property="type" value="withdraw"/><%}else{ %>
<jsp:setProperty name="BKT" property="type" value="transfer"/><%} %>
<jsp:setProperty name="BKT" property="createdBy" value="<%=createdBy%>"/>
<jsp:setProperty name="BKT" property="extra1" value="<%=extra1%>"/>
<%bkIns=BKT.insert();%><%=BKT.geterror()%>

<%
type="transfer";
String bankTransId=BKT.getbanktrn_id();
if(bkIns){
String pettyCash=BAN.getPettyCashAmount(frombank_id);
String bankamount=BAN.getMbankdetailsAmount(frombank_id);
if(type.equals("transfer")){
	if(bankamount!=null && !bankamount.equals("")){
		if(frombanktype){
			bankpramount=(Double.parseDouble(bankamount)+Double.parseDouble(amount));}else{
			bankpramount=(Double.parseDouble(bankamount)-Double.parseDouble(amount));}
}else{
	if(frombanktype){
		bankpramount=Double.parseDouble(amount);}else{
		bankpramount=-Double.parseDouble(amount);}
}
}
bankamount=String.valueOf(bankpramount);%>
	<%-- <jsp:setProperty name="BNK" property="bank_id" value="<%=frombank_id%>"/>
	<jsp:setProperty name="BNK" property="total_amount" value="<%=bankamount%>"/>
	<%bkUp=BNK.update();%><%=BNK.geterror()%> --%>
<%}%>
<jsp:setProperty name="BKT1" property="bank_id" value="<%=tobank_id%>"/>
<jsp:setProperty name="BKT1" property="name" value="<%=name%>"/>
<jsp:setProperty name="BKT1" property="narration" value="<%=narration%>"/>
<jsp:setProperty name="BKT1" property="date" value="<%=time%>"/>
<jsp:setProperty name="BKT1" property="amount" value="<%=amount%>"/>
<jsp:setProperty name="BKT1" property="type" value="deposit"/>
<jsp:setProperty name="BKT1" property="createdBy" value="<%=createdBy%>"/>
<jsp:setProperty name="BKT1" property="extra1" value="<%=extra1%>"/>
<jsp:setProperty name="BKT1" property="extra3" value="<%=bankTransId%>"/>
<%bkIns=BKT1.insert();%><%=BKT1.geterror()%>
<%type="deposit";
bankTransId=BKT1.getbanktrn_id();
if(bkIns){
	tobankList=BAN.getMbankdetails(tobank_id);
	if(tobankList.size()>0){
		BANK=(bankdetails)tobankList.get(0);
	}
	if("credit".equals(BANK.getextra2())){
	tobanktype=true;
	}
String pettyCash=BANK.getextra1();
String bankamount=BANK.gettotal_amount();
if(type.equals("deposit")){
	if(tobanktype){
		bankpramount=(Double.parseDouble(bankamount)-Double.parseDouble(amount));
	} else{
		bankpramount=(Double.parseDouble(bankamount)+Double.parseDouble(amount));	
	}}
bankamount=String.valueOf(bankpramount);%>
	<%-- <jsp:setProperty name="BNK1" property="bank_id" value="<%=tobank_id%>"/>
	<jsp:setProperty name="BNK1" property="total_amount" value="<%=bankamount%>"/>
	<%bkUp=BNK1.update();%><%=BNK1.geterror()%> --%>
<%}
 response.sendRedirect("adminPannel.jsp?page=contraBankEntryForm&cid="+bankTransId); 
%>
