<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	function filter(){
		$("#filtration-form").submit();
		//location.reload('godwanForm.jsp?poid='+poid); 
		//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
	}
</script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<jsp:useBean id="EXP" class="beans.productexpenses"/>
<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<form id="filtration-form"  action="adminPannel.jsp" method="post">
<%String status="Not-Approve";
if(request.getParameter("status")!=null && !request.getParameter("status").equals("") ){
	status=request.getParameter("status");
} %>
<div class="search-list">
<ul>
<li><input type="hidden" name="page" value="paymentsApprovalsList"></li>
<li><select name="status" onchange="filter();">
<!-- <option value="">Sort by Approval Status</option> -->
<option value="Not-Approve"  <%if(request.getParameter("status")!=null && request.getParameter("status").equals("Not-Approve")){ %>  selected="selected" <%} %>>Not-Approved List</option>
<option value="Approved" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("Approved")){ %>  selected="selected" <%} %>>Approved List</option>

</select></li>
</ul>
</div>
</form>

<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%; margin:0 2%;
    background:#d0e3fb !important;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="9" height="10"></td> </tr>
	<tr><td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 20px;">Payments <%=status %> List </td></tr>
                        
</table>
<div class="icons">
<!-- <span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span> -->
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div id="tblExport">
<div class="clear"></div>
<div class="list-details">
<%mainClasses.indentListing IND_L = new indentListing();
indentapprovalsListing APPR_L=new indentapprovalsListing();
productexpensesListing PRO_EXPL=new productexpensesListing();
superadminListing SAD_l=new superadminListing();
List IND_List=IND_L.getindentGroupByIndentinvoice_id();
vendorsListing VENL=new vendorsListing();
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
List EXP_L=PRO_EXPL.getExpensesListGroupByInvoice(status);
//System.out.println("EXP_L=="+EXP_L.size());
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
//System.out.println("HOA_CL//"+HOA_CL);
List pendingIndents=IND_L.getPendingIndents();
//System.out.println("pendingIndents//"+pendingIndents);%>

<%
if(EXP_L.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="9"><table width="100%" cellpadding="0" cellspacing="0"  class="yourID">
<tr>
<td class="bg" width="5%" align="center">S.No.</td>
<td class="bg" width="6%">Invoice No / Unique No</td>
<td class="bg" width="10%">Company</td>
<td class="bg" width="15%">Vendor Name</td>
<td class="bg" width="10%">Date</td>
<td class="bg" width="15%">Narration</td>
<td class="bg" width="10%">Total Amount</td>
<td class="bg" width="8%">Status</td>
<td class="bg" width="28%">Approved By</td>
</tr>
</table></td></tr>
<%List APP_Det=null;
String approvedBy="";
for(int i=0; i < EXP_L.size(); i++ ){
	EXP=(productexpenses)EXP_L.get(i);
	APP_Det=APPR_L.getIndentDetails(EXP.getexpinv_id());
	if(APP_Det.size()>0){
		for(int j=0;j<APP_Det.size();j++){
			IAP=(beans.indentapprovals)APP_Det.get(j);
			if(j==0){
				approvedBy=SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME  ["+CDF.format(SDF.parse(IAP.getapproved_date()))+"]";
			}else{
				approvedBy=approvedBy+"<br></br>"+SAD_l.getSuperadminname(IAP.getadmin_id())+" --TIME ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"]";
			}
		}
	} %>
    
<tr>
<td width="5%" align="center"><%=i+1%></td>
<%-- <%if(!status.equals("Not-Approve")) {%>
<td><a href="adminPannel.jsp?page=expenses&invid=<%=EXP.getexpinv_id()%>&expflag=2"><b><%=EXP.getexpinv_id()%></b></a></td>
<%} else { %> --%>
<td width="6%"><a href="adminPannel.jsp?page=expenses&invid=<%=EXP.getexpinv_id()%>&expflag=2"><b><%=EXP.getexpinv_id()%></b></a><br>/
<div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=EXP.getExtra6()%>')"><span><%=EXP.getExtra6() %></span></div>
</td>
<td width="10%"><%=HOA_CL.getHeadofAccountName(EXP.gethead_account_id())%></td>
<td width="15%"><%=VENL.getMvendorsAgenciesName(EXP.getvendorId())%></td>
<td width="10%"><%=df2.format(dateFormat.parse(EXP.getentry_date()))%></td>
<td width="17%"><%=EXP.getnarration()%></td>
<td width="8%"><%=EXP.getamount()%></td>
<td width="8%"><%=EXP.getextra1()%></td>
<td width="28%"><%=approvedBy %></td>
</tr>
<%
approvedBy="";
} %>
</table>
<%}else{%>
<div align="center"><h1>Payments invoices not found! </h1></div>
<%}%>


</div>
</div>

</div>


</div>
