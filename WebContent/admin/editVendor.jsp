<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.util.List"   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Vendor</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

function validate(){
	$('#headOfAccountError').hide();
	$('#firstNameError').hide();
	$('#addressError').hide();
	$('#emailError').hide();
	$('#phoneError').hide();
	
/* 	if($('#headAccountId').val().trim()==""){
		$('#headOfAccountError').show();
		$('#headAccountId').focus();
		return false;
	} */
	
	if($('#firstName').val().trim()==""){
		$('#firstNameError').show();
		$('#firstName').focus();
		return false;
	}
	
	if($('#address').val().trim()==""){
		$('#addressError').show();
		$('#address').focus();
		return false;
	}
	
	if($('#email').val().trim()!=""){
		
		var email=$("#email").val();
		if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))  
		  {
			$('#emailError').show();
			$('#email').focus();
			return false;
			
		  }
	}
	
	if($('#phone').val().trim()==""){
		$('#phoneError').show();
		$('#phone').focus();
		return false;
	}
	var radioVal=$('input[type="radio"]:checked').val();
	var GSTfield=$('#GSTNo').val();
	if(radioVal=='no' && GSTfield!=''){
		$('#GSTNo').val('0');
		
		
	}
		
}
</script>

<script>

$(document).ready(function() {
	/* var radioVal=$('#no').val();
	if(radioVal=='no'){
		 $('#gstNum').hide();
	} */
	
	var radioVal=$('input[type="radio"]:checked').val();
	if(radioVal=='no'){
		$('#gstNum').hide();
		
	}
	
		

	/* $('#gstNum').hide(); */
	$('input[type="radio"]').click(function() {
		
		if($(this).attr('id') == 'yes') {
	$('#gstNum').show();
		}
		else{
			$('#gstNum').hide();
		}
	});
});

</script>
</head>
<jsp:useBean id="VEN" class="beans.vendors"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>	
<body>
<div class="vender-details">
<%
String vendorId=request.getParameter("id");
mainClasses.vendorsListing VEN_CL = new mainClasses.vendorsListing();
List VEN_List=VEN_CL.getMvendors(vendorId);
if(VEN_List.size()>0){
	VEN=(beans.vendors)VEN_List.get(0);
%>
<form method="POST" action="vendors_Update.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Vendor Information</span></td>
</tr>
<tr>
<td width="50%">
<table width="100%" cellpadding="0" cellspacing="0">
<input type="text" name="vendorId" id="vendorId" value="<%=vendorId%>"/>
<!-- <tr>
<td colspan="3">
<div class="warning" id="vendorIdError" style="display: none;">"Vendor Id" Required.</div>
Vendor Id*</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="vendorId" id="vendorId"/></td>
</tr> -->

<%--  <tr>
<td colspan="3">
<div class="warning" id="headOfAccountError" style="display: none;">please Select "Head Of Account".</div>
Head Of Account*</td>
</tr>
<tr>
<td colspan="3">
<select name="headAccountId" id="headAccountId" style="width:400px;">
<option value="">--select--</option>
<%mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();
for(int i=0; i < HOA_List.size(); i++ ){
	HOA=(beans.headofaccounts)HOA_List.get(i);%>
	<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname()%></option>
	<%}%>
</select>
</td>
</tr>  --%>
<tr>
<td colspan="2">
<div class="warning" id="firstNameError" style="display: none;">"First Name" Required</div>
Title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

First Name<span style="color: red;">*</span></td>

<td>Last Name</td>
</tr>
<tr>
<td colspan="2">
<select style="width:45px;height:23px;" name="title" >
<option value="M/s">M/S</option>
<option value="Mr.">Mr.</option>
<option value="Mrs.">Mrs.</option>
<option value="Ms.">Ms.</option>
<option value="Miss.">Miss.</option></select>
<select style="width:45px;height:23px;" name="title" >
<option value="M/s" <%if(VEN.gettitle().equals("M/s")){%> selected="selected" <%}%>>M/S</option>
<option value="Mr."  <%if(VEN.gettitle().equals("Mr.")){%> selected="selected" <%}%>>Mr.</option>
<option value="Mrs." <%if(VEN.gettitle().equals("Mrs.")){%> selected="selected" <%}%>>Mrs.</option>
<option value="Ms."  <%if(VEN.gettitle().equals("Ms.")){%> selected="selected" <%}%>>Ms.</option>
<option value="Miss."  <%if(VEN.gettitle().equals("Miss.")){%> selected="selected" <%}%>>Miss.</option></select>
<input type="text" style="width:170px;" name="firstName" id="firstName" value="<%=VEN.getfirstName()%>"/></td>
<td style="padding:0;"><input type="text" style="width:166px; margin:5px 0 0 0" name="lastName" id="lastName" value="<%=VEN.getlastName()%>"></td>
</tr>


<tr>
<td colspan="3">Agency Name</td>
</tr>
<tr>
<td colspan="3"><input type="text" style="width:99%;" name="agencyName" value="<%=VEN.getagencyName()%>"></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="addressError" style="display: none;">"Address" Required.</div>
Address<span style="color: red;">*</span></td>
</tr>
<tr>
<td colspan="3"><textarea placeholder="street" name="address" id="address"><%=VEN.getaddress() %></textarea></td>
</tr>
<tr>
<td colspan="2"><div class="warning" id="cityError" style="display: none;">Need city name</div>City</td>
<td>Zip Code</td>
</tr>
<tr>
<td colspan="2"><input type="text" style="width:225px" name="city" id="city" value="<%=VEN.getcity() %>"/></td>
<td><input type="text" name="pincode" id="pincode" value="<%=VEN.getpincode()%>"></td>
</tr>
<tr>
<td colspan="3">Description</td>
</tr>
<tr>
<td colspan="3"><textarea name="description" id="description" ><%=VEN.getdescription()%></textarea></td>
</tr>
</table>
</td>
<td width="2%"></td>
<td valign="top">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3">
<div class="warning" id="emailError" style="display: none;">Please Provide Valid "Email".</div>Email</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="email" id="email" style="width:280px;" value="<%=VEN.getemail()%>"></td>
</tr>
<tr>
<td colspan="1">Land Number</td>
<td colspan="1">
<div class="warning" id="phoneError" style="display: none;">"Phone Number" Required.</div>Phone Number<span style="color: red;">*</span></td>
<td></td>
</tr>
<tr>
<td colspan="1"><input type="text" name="landPhone" id="landPhone" onKeyPress="return numbersonly(this, event,true);" style="width:125px;"value="<%=VEN.getextra2()%>" /></td>
<td colspan="1"><input type="text" name="phone" id="phone" onKeyPress="return numbersonly(this, event,true);" style="width:125px;" value="<%=VEN.getphone()%>"/></td>
<td></td>
</tr>
<tr>
<td colspan="1">Website URL</td>
<!-- <td>Service tax number </td> -->
<td></td>
</tr>
<tr>
<td colspan="1"><input type="text" name="url" id="url" value="" style="width:125px;" value="<%=VEN.getnomineeName()%>"></td>
<%-- <td><input type="text" name="servicetaxNo" id="servicetaxNo" value="" style="width:125px;" value="<%=VEN.getgothram()%>"></td> --%>
<td></td>
</tr>
<tr>
<td><div class="warning" id="tinError" style="display: none;">TIN Number should be 11 digits.</div>TIN Number</td>
<td><div class="warning" id="tanError" style="display: none;">TAn Number should be 10 digits.</div>TAN Number</td>
<td><div class="warning" id="panError" style="display: none;">PAN Number should be 10 digits.</div>PAN Number</td>
</tr>
<tr>
<td><input type="text" name="tinNo" id="tinNo" maxlength="11" style="width:125px;" onKeyPress="return numbersonly(this, event,true);" value="<%=VEN.getextra3()%>"/></td>
<td><input type="text" name="tanNo" id="tanNo" maxlength="10" style="width:125px;" value="<%=VEN.getextra4()%>"/></td>
<td><input type="text" name="panNo" id="panNo"  maxlength="10" style="width:125px;" value="<%=VEN.getextra5()%>"/></td>
</tr>
<tr>
<td><div class="warning" style="display: none;"></div>GST</td>

<td><input type="radio" name="GSTFlag" id="yes" value="yes" <%if(VEN.getExtra7().equals("yes")){%>checked<%} %> required> yes</td>
<td><input type="radio" name="GSTFlag" id="no" value="no" <%if(VEN.getExtra7().equals("no")){%>checked<%} %> required> no</td>
</tr>

<tr id="gstNum">
<td><div class="warning" id="gstError" style="display: none;"></div>GST Number</td>
<td><input type="text" name="GSTNo" id="GSTNo"  maxlength="15" value="<%=VEN.getExtra8()%>" style="width:125px;"/></td>
</tr>
<!-- <tr>
<td colspan="3">Gothram</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="gothram" id="gothram" style="width:98%;"/></td>
</tr>
 <tr>
<td colspan="3">Nominee Name</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="nomineeName" id="nomineeName" style="width:98%;"></td>
</tr> -->
<tr>
<td colspan="1">Bank Name</td>
<td>Branch Name</td>
<td></td>
</tr>
<tr>
<td colspan="1"><input type="text" name="bankName" id="bankName" style="width:125px;" value="<%=VEN.getbankName()%>"/></td>
<td><input type="text" name="bankBranch" id="bankBranch" style="width:125px;" value="<%=VEN.getbankBranch()%>"/></td>
<td></td>
</tr>
<tr>
<td colspan="3">Account Number</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="accountNo" id="accountNo" onKeyPress="return numbersonly(this, event,true);" style="width:280px;" value="<%=VEN.getaccountNo()%>"/></td>
</tr>
<tr>
<td colspan="1">IFSC Code</td>
<td>Credit Days</td>
<td></td>
</tr>
<tr>
<td colspan="1"><input type="text" name="IFSCcode" id="IFSCcode" style="width:125px;" value="<%=VEN.getIFSCcode()%>"/></td>
<td><input type="text" name="creditDays" id="creditDays" onKeyPress="return numbersonly(this, event,true);" style="width:125px;" value="<%=VEN.getcreditDays()%>"/></td>
<td></td>
</tr>

</table>
</td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
<%}else{%>

<%} %>
</div>
</body>
</html>