<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.loginsListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.customerpurchases" %>
<%@page import="beans.productexpenses" %>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.countersListing"%>
<%@page import="beans.cashier_report"%>
<%@page import="mainClasses.cashier_reportListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script src="../js/jquery-1.4.2.js"></script> 
<script>
$(document).ready(function() {
	    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>
<script>

 var toDayDate=new Date();

 
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true, 
		 maxDate : toDayDate,
		dateFormat: 'yy-mm-dd'
			/* minDate : new Date(), */
	});		
});
$(function() {
	$( "#date1" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		 maxDate : toDayDate,
		dateFormat: 'yy-mm-dd'
	});		
});
$(function() {
	$( "#date2" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		 maxDate : toDayDate,
		dateFormat: 'yy-mm-dd'
	});		
});

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
$(document).ready(function(){
	 $( "#head_account_id" ).change(function() {
		  $( "#filterForm" ).submit();
		});
		
	$("#id").keyup(function(){
		  var hoid=$("#head_account_id").val();
		  var data1=$("#id").val();
		  $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data)
		  {
			 var response = data.trim().split("\n");
			 var bankNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 bankNames.push(d[0] +" "+ d[1]);
			 }
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=bankNames;
			 $( "#id" ).autocomplete({source: availableTags}); 
				});
	});
	
	$("#id1").keyup(function(){
		  var hoid=$("#head_account_id").val();
		  var data1=$("#id1").val();
		  $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data)
		  {
			 var response = data.trim().split("\n");
			 var bankNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 bankNames.push(d[0] +" "+ d[1]);
			 }
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=bankNames;
			 $( "#id1" ).autocomplete({source: availableTags}); 
				});
	});
	$("#id2").keyup(function(){
		  var hoid=$("#head_account_id").val();
		  var data1=$("#id2").val();
		  $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data)
		  {
			 var response = data.trim().split("\n");
			 var bankNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 bankNames.push(d[0] +" "+ d[1]);
			 }
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=bankNames;
			 $( "#id2" ).autocomplete({source: availableTags}); 
				});
	});
	
	});
function depositAmountCal(i){
	var selectAmount=$("#check"+i).val();
	var totAmt=$("#amount").val();
	if($("#check"+i).is(':checked')){
		totCalAmt=Number(totAmt)+Number(selectAmount);
		$("#amount").val(totCalAmt);
		$('#amount').attr('readonly', 'readonly');
	}else{
		totCalAmt=Number(totAmt)-Number(selectAmount);
		$("#amount").val(totCalAmt);
		$('#amount').attr('readonly', 'readonly');
	}
	var atleastonechecked =true;
	 var countersSize=$("#countersSize").val();
	 var casherIDs="";
		 for(var j=0;j<countersSize;j++){
			 if($("#check"+j).is(':checked')){
				 atleastonechecked =false;
				 if(casherIDs!=""){
						casherIDs=casherIDs+","+$("#sjID"+j).val();;
					}else{
						casherIDs=$("#sjID"+j).val();;
					}
					$("#cashierReportIds").val(casherIDs);
			 }
		 }
		 if(atleastonechecked){
				$("#cashierReportIds").val("");
		 }
	if($("#amount").val()=='0'){
		$("#amount").removeAttr("readonly");
	}
	
}
function validate(){
	 var amntdeposite=$('#amtDeposited').val();
	 
	
	 //var checkboxcount=documents.form2.check.length();
	
 /* if(amntdeposite=="pro-counter" || amntdeposite=="Charity-cash" || amntdeposite=="Development-cash"  || amntdeposite=="Sainivas"  ){
		 var len = $("[name='check']:checked").length;
		 if(len<1)
			 {
			 	alert("please checked atleast one checkbox");
			 	
			 	return false;
			 }
		  
		  
	 } */ 
	 
	 //verify name should be unique....
	
	 

}
function vendorsearch(){
	 var data1=$('#vendorId').val();
	  var headID=$('#head_account_id').val();
	  if($('#head_account_id').val().trim()==""){
			$('#head_account_id').focus();
			return false;
		}
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $( '#vendorId').autocomplete({source: availableTags}); 
			});
}
function vendorsearch2(){
	 var data1=$('#vendorId2').val();
	  var headID=$('#head_account_id').val();
	  if($('#head_account_id').val().trim()==""){
			$('#head_account_id').focus();
			return false;
		}
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $( '#vendorId2').autocomplete({source: availableTags}); 
			});
}
</script>
<script>
function majorheadsearch(j){
	var data1=$('#major_head_id'+j).val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#major_head_id'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
}
</script>

<script>
function minorheadsearch(j){
		  var data1=$('#minor_head_id'+j).val();
		  var major=$('#major_head_id'+j).val();
		  var hoid=$("#head_account_id").val();
		  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			$('#minor_head_id'+j).autocomplete({
				source: availableTags,
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				}
			}); 		
	  });
}

function productsearch(j){
	  var data1=$('#product'+j).val();
	  var arr = [];
	  //var data1=$("#sub_head_id").val();
	  var minor=$('#minor_head_id'+j).val();
	  var major=$('#major_head_id'+j).val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
} 

function showOrHide()
{
	var val1 = $("#amtDeposited").val();	
	var val2 = $("#categ").val();
	if(val1 == "PettyCash" && val2 == "pettycash")
	{
		document.getElementById('headin').style.display = "block";
		document.getElementById('selec').style.display = "block";
		
		document.getElementById("amount").readOnly = true;
	}
	
	else
	{
		document.getElementById('headin').style.display = "none";
		document.getElementById('selec').style.display = "none";
		
		document.getElementById("amount").value = "0";
		document.getElementById("amount").readOnly = false;
	}
}


function getAmount()
{
	var amount = $("#mseid").val();
	var actualamount = amount.substr(0,amount.indexOf(' '));
	var remaining=amount.substr(amount.indexOf(' ')+1,amount.indexOf(' ').length);
	var expid = remaining.substr(0,remaining.indexOf(' '));
	var mseInvId = remaining.substr(remaining.indexOf(' ')+1);

	document.getElementById("amount").value = actualamount;
	document.getElementById("expid").value = expid;
	document.getElementById("mseInvId").value = mseInvId;
	document.getElementById("amount").readOnly = true;
	
}

</script>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="CSH" class="beans.cashier_report"/>
<jsp:useBean id="PEX" class="beans.productexpenses"/>
<jsp:useBean id="CUS" class="beans.customerpurchases"></jsp:useBean>
</head>
<body>
<%
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
loginsListing LOGL=new loginsListing();
List HA_Lists=HOA_L.getheadofaccounts();
cashier_reportListing CSHR_L=new  cashier_reportListing();
customerpurchasesListing CUSTP_L=new customerpurchasesListing();
countersListing COUNL=new countersListing();
employeesListing EMPL=new employeesListing();
List CRL=null;
String hoid="1";
String aliashoid="4";
String aliascashDeposite="SansthanDeposit";
DecimalFormat DF=new DecimalFormat("#,##,##.00");
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
System.out.println("currentDate..."+currentDate);
double EmployeAmount=0.0; 
if(session.getAttribute("adminId")!=null){ %>
<div>
<div class="vendor-page">
<div class="vendor-box">
<div class="clear"></div>
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1500;
	width:96%;margin:0 2%; 
   
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () { 
	$("#vendorblock").hide();
	$("#amtDeposited").change(function(){
	    var req=$("#amtDeposited").val();
	    if(req==='vendor'){
	    	$("#vendorblock").show();
	    }else{
	    	$("#vendorblock").hide();
	    }
	 });
    $('#vendorblock2').hide();
    $('#amtDeposited2').change(function(){
	   var req2=$('#amtDeposited2').val();
	   if(req2==='vendor'){
	    	$("#vendorblock2").show();
	    }else{
	    	$("#vendorblock2").hide();
	    }
	    	
    });
	 
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
	

});
</script>
<Script>
function normaldeposit(){
	document.getElementById("nor-deposit").style.display="block";
	document.getElementById("dollar-deposit").style.display="none";
	document.getElementById("other-deposit").style.display="none";
	$('#depositType').val('nordeposit');
	
}
function dollardeposit(){
	document.getElementById("dollar-deposit").style.display="block";
	document.getElementById("nor-deposit").style.display="none";
	document.getElementById("other-deposit").style.display="none";
	$('#depositType').val('dollardeposit');
}
function otherdeposit(){
	document.getElementById("other-deposit").style.display="block";
	document.getElementById("dollar-deposit").style.display="none";
	document.getElementById("nor-deposit").style.display="none";
	$('#depositType').val('otherdeposit');
}

function calculateTotal(i){
	var amt=document.getElementById("currency"+i).value;
	var qty=document.getElementById("quantityhidden"+i).value;
	var amount=document.getElementById("amount1").value ;
	document.getElementById("totAmt"+i).value=Number(amt)*Number(qty);
	var TOT = document.getElementById("totAmt"+i).value;
		 if(document.getElementById("checkbox1"+i).checked == true){
			var amount1 = (Number(amount) + Number(TOT));
			document.getElementById("amount1").value = Number(amount1);
	}    
}
</Script>
<script>
function addvalue(i){
	var amount=document.getElementById("amount1").value ;
	var TOT = document.getElementById("totAmt"+i).value;
	/* alert(document.getElementById("checkbox1"+i).checked ); */
	 if(document.getElementById("checkbox1"+i).checked == true){
		var amount1 = (Number(amount) + Number(TOT));
		document.getElementById("amount1").value = Number(amount1);
}  else
	{
	var amount1 = (Number(amount) - Number(TOT));
	document.getElementById("amount1").value = Number(amount1);
	}
	
}
</script>
<script>

function datepickerchange()
{
	
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	$('#date').datepicker('option', 'minDate', new Date(startDate));
	$('#date').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
	
}); 
</script>
 <script>

function datepickerchange1()
{
	
	var finYear=$("#finYear1").val();
	var yr = finYear.split('-');
	var startDate1 = yr[0]+",04,01";
	var endDate1 = parseInt(yr[0])+1+",03,31";
	
	/* $('#date').datepicker('option', 'minDate', new Date(startDate));
	$('#date').datepicker('option', 'maxDate', new Date(endDate)); */
	$('#date1').datepicker('option', 'minDate', new Date(startDate1));
	$('#date1').datepicker('option', 'maxDate', new Date(endDate1));
	/* $('#date2').datepicker('option', 'minDate', new Date(startDate));
	$('#date2').datepicker('option', 'maxDate', new Date(endDate)); */
}

$(document).ready(function(){
	datepickerchange1();
	
}); 
</script>
<script>

function datepickerchange2()
{
	
	var finYear=$("#finYear2").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	/* $('#date').datepicker('option', 'minDate', new Date(startDate));
	$('#date').datepicker('option', 'maxDate', new Date(endDate)); */
	/* $('#date1').datepicker('option', 'minDate', new Date(startDate));
	$('#date1').datepicker('option', 'maxDate', new Date(endDate)); */
	 $('#date2').datepicker('option', 'minDate', new Date(startDate));
	$('#date2').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange2();
	
	
}); 
</script>

<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">BANK DEPOSITS FORM </td></tr>
</table>
<div class="vender-details">
<%
if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
	hoid=request.getParameter("head_account_id");
} else{
	hoid="1";
}
 %>
 <% String depositMode="nordeposit";
 if(request.getParameter("deposittype")!=null && !request.getParameter("deposittype").equals("") && !request.getParameter("deposittype").equals("null")){
	 depositMode = request.getParameter("deposittype");
 } %>
<div>
<form id="filterForm" action="adminPannel.jsp" method="post" onsubmit="validate()">
<input type="hidden" name="page" value="countersClosed-Deposits">
<input type="hidden" name="deposittype" id="depositType" value="<%=depositMode%>">
<select name="head_account_id" id="head_account_id">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("head_account_id")!=null && request.getParameter("head_account_id").equals(HOA.gethead_account_id())){ %> selected="selected" <%}%> ><%=HOA.getname() %></option>
<%}} %>
</select>
</form>
</div>
<br/>
<input type="radio" name="deposittype"  value="nordeposit" onclick="normaldeposit()" checked>Deposit
<input type="radio" name="deposittype"  value="dollardeposit" onclick="dollardeposit()" <%if("dollardeposit".equals(depositMode)) {%>checked<%} %>>Dollar Deposit
<input type="radio" name="deposittype"  value="otherdeposit" onclick="otherdeposit()" <%if("otherdeposit".equals(depositMode)) {%>checked<%} %>>Other Deposit

<!-- Normal Deposit Div -->
<div id="nor-deposit" <%if(depositMode !=null){ if("nordeposit".equals(depositMode)){%> style="display:block" <%} else{%> style="display:none" <%}}else{%>style="display:block"<%} %>>
<form  method="post" name="form2" action="banktransactionsInsert.jsp" onsubmit="return validate();">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr><td>&nbsp;</td></tr>
<tr>
<td>Select Fin Year</td>
<td>Deposit Date</td>
<td>Bank*</td>
<td>Amount depositing By*</td>
<td>Category</td>
<td id = "headin" style="display:none;">Select MSE</td>
</tr>
<tr>
<td><select name="finYear" id="finYear"  Onchange="datepickerchange();">
				   <option value="2021-22"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2021-22")) {%> selected="selected"<%} %>>2021-2022</option>
				   <option value="2020-21"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-21")) {%> selected="selected"<%} %>>2020-2021</option>
					<option value="2019-20"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option>
					<option value="2018-19"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%> selected="selected"<%} %>>2018-2019</option>
					<option value="2017-18"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%> selected="selected"<%} %>>2017-2018</option>
					<option value="2016-17"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-2017</option>
					</select></td>
<td>
<input type="hidden" name="cashierReportIds" id="cashierReportIds" value="">




<input type="text" name="date" id="date" value="<%=currentDate%>" readonly/></td>
<td><input type="text" name="bank_id" id="id" value="" placeHolder="Find a bank here" required style="height: 22px;"/></td>
<td><select name="amtDeposited" id="amtDeposited" onchange="showOrHide()">
	<option value="poojastore-counter">PoojaStore-counter</option>
	<option value="pro-counter">PRO Office-counter</option>
	<option value="Charity-cash">Charity-Cash</option>
	<option value="Development-cash">Development-cash</option>
	<option value="Sainivas">Sainivas(Shirdi)</option>
	<option value="vendor">Vendors</option>
	<option value="PettyCash">Petty Cash</option>
	<option value="Ewf-cash">EWF Cash</option>
</select></td>
<td><select name="category" id = "categ" onchange="showOrHide()">
		<option value="deposit">Amount Deposit (In Bank)</option>
		<option value="pettycash">Petty cash (with draw)</option>
</select></td>
<td style="display:none;" id = "selec"><select name="mseid" id="mseid" onchange="getAmount()">
<option value="">-- Select --</option>
<%
	productexpensesListing PEXPL = new productexpensesListing();
	List PC = PEXPL.getPettyCashIssueList(hoid);
	if(PC.size()>0){
	for(int i=0;i<PC.size();i++){
		PEX=(productexpenses)PC.get(i);
	%>
		<option value="<%=PEX.getamount()%> <%=PEX.getexp_id()%> <%=PEX.getextra5() %>"><%=PEX.getextra5() %></option><%}} %>
</select></td>
<input type="hidden" name="expid" id="expid" value="">
<input type="hidden" name="mseInvId" id="mseInvId" value="">
</tr>
<tr>
<td>Major Head</td>
<td>Minor Head</td>
<td>Sub Head</td>
</tr>
<%int a = 1;%>
<tr>
<td><input type="text" name="major_head_id<%=a%>" id="major_head_id<%=a%>" onkeyup="majorheadsearch('<%=a%>')" class="" value="" placeholder="find a major head here" autocomplete="off"/></td>
<td><input type="text" name="minor_head_id<%=a%>" id="minor_head_id<%=a%>" onkeyup="minorheadsearch('<%=a%>')" class="" value=""  placeholder="find a minor head here" autocomplete="off"/></td>
<td><input type="text" name="product<%=a%>" id="product<%=a%>"  onkeyup="productsearch('<%=a%>')" class="" value="" autocomplete="off" /></td>
</tr>
<tr>
<td>Amount*</td>
<td>Reference</td>
<td></td>
</tr>
<tr>
<td><input type="text" name="amount" id="amount" value="0" onKeyPress="return numbersonly(this, event,true);" />
<input type="hidden" name="HOA" value="<%=hoid%>"/></td>
<td><input type="text" name="extra1" id="extra1" value=""></td>
<td id="vendorblock"><input type="text" name="vendorId" id="vendorId" onkeyup="vendorsearch()" autocomplete="off" placeholder="Enter vendor name" value="" /></td>
</tr>
<tr>
<td colspan="2"><textarea rows="1" cols="4" name="Narration" id="Narration" required placeholder="Write your narration here"></textarea></td>
</tr>
<tr>
<td align="right" colspan="2"><input type="submit" value="DEPOSIT" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>

<!-- Dollar deposit Div -->

<div id="dollar-deposit"  <%if(depositMode !=null && !depositMode.equals("")){ if("dollardeposit".equals(depositMode)){%> style="display:block" <%} else{%> style="display:none" <%}}else{%>style="display:none;"<%} %> >
<form  method="post" name="form2" action="banktransactionsInsert.jsp" onsubmit="return validate();">
<tr><td>&nbsp;</td></tr>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr><td>&nbsp;</td></tr>

<thead style="background-color:#ccc;">
<%if(hoid.equals("4")){ %>
	<tr>
    	<!-- <th style=" padding:5px 0px 5px 0px" align="left"><input type="checkbox" id="selecctall"/>S.No-Receipt Id</th> -->
    	<th style=" padding:5px 0px 5px 0px" align="left">S.No-Receipt Id</th>
     	<th align="left">Entry Date</th>
        <th align="left">Subhead Name</th>
        <th align="left">Total Amount</th>
        </tr>
    <%}else{ %>
    <tr>
	
    	<!-- <th style=" padding:5px 0px 5px 0px" align="left"><input type="checkbox" id="selecctall"/>S.No-Receipt Id</th> -->
    	<th style=" padding:5px 0px 5px 0px" align="left">S.No-Receipt Id</th>
    	<th align="left">Subhead Name</th>
        <th align="left">Dollar Amount</th>
        <th align="left">Currency Amount</th>
        <th align="left">Total Amount</th>
    </tr>
    <%} %>
</thead>
<%
SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat datePickerDbFormat=new SimpleDateFormat("yyyy-MM-dd");
subheadListing SUBL = new subheadListing();
customerpurchasesListing C_LIST = new customerpurchasesListing();
List CLIST = C_LIST.getDollaramountBasedOnHOA(hoid);
if(CLIST.size()>0){
for(int i=0;i<CLIST.size();i++){
	CUS=(customerpurchases)CLIST.get(i);
	%>
	<%if(hoid.equals("4")){ %>
	<tr>
	<td><input type="checkbox" class="checkbox1" name="checkbox1" id="checkbox1<%=i%>" value="<%=CUS.getbillingId() %>" onclick="addvalue(<%=i%>)"/><%=(i+1) %>-<%=CUS.getbillingId() %></td>
	<td><input type="text" name="entrydate" id="entrydate<%=i %>" value="<%=datePickerDbFormat.format(dbDateFormat.parse(CUS.getdate()))%>" PlaceHolder="Total Amount" readonly/></td>
	<td><input type="text" name="quantity" id="quantity<%=i %>" value="<%=SUBL.getMsubheadnameWithDepartment(CUS.getproductId(), hoid)%> " readonly/></td>
	<td><input type="text" name="totAmt" id="totAmt<%=i %>" value="" PlaceHolder="Total Amount" /></td>
	</tr>
	<%}else{ %>
	<tr>
	<td><input type="checkbox" class="checkbox1" name="checkbox1" id="checkbox1<%=i%>" value="<%=CUS.getbillingId() %>" onclick="addvalue(<%=i%>)"/><%=(i+1) %>-<%=CUS.getbillingId() %></td>
	<td><input type="text" name="subheadname" id="subheadname<%=i %>" value="<%=SUBL.getMsubheadnameWithDepartment(CUS.getproductId(), hoid)%>" readonly/></td>
	<td><input type="hidden" id="quantityhidden<%=i %>" value="<%=CUS.getquantity()%>" readonly/>
	<input type="text" name="quantity" id="quantity<%=i %>" value="<%if(CUS.getextra5().equals("USD")){%>$<%}else if(CUS.getextra5().equals("POUND")){%>&pound;<%}else{ %><%=CUS.getextra5()%><%} %> <%=CUS.getquantity()%>" readonly/></td>
	<td><input type="text" name="currency" id="currency<%=i %>" value="" onblur="calculateTotal(<%=i%>);" placeHolder="Please Enter Currency Rate"/></td>
	<td><input type="text" name="totAmt" id="totAmt<%=i %>" value="" PlaceHolder="Total Amount" readonly/></td>
	</tr>
	<%} %>
	
<%}}%>

<tr><td>&nbsp;</td></tr>
<tr>
<td>Select Fin Year</td>
<td>Deposit Date</td>
<td>Bank*</td>
<td>Amount depositing By*</td>
<td>Category</td>
</tr>
<tr>
<td><select name="finYear1" id="finYear1"  Onchange="datepickerchange1();">
					<option value="2021-22"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2021-22")) {%> selected="selected"<%} %>>2021-2022</option>
					<option value="2020-21"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-21")) {%> selected="selected"<%} %>>2020-2021</option> 
					<option value="2019-20"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option>
					<option value="2018-19"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%> selected="selected"<%} %>>2018-2019</option>
					<option value="2017-18"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%> selected="selected"<%} %>>2017-2018</option>
					<option value="2016-17"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-2017</option>
					</select></td>

<td>
<input type="hidden" name="cashierReportIds" id="cashierReportIds" value="">
<input type="text" name="date" id="date1" value="<%=currentDate%>" readonly/></td>
<td><input type="text" name="bank_id" id="id1" value="" placeHolder="Find a bank here" required style="height: 22px;"/></td>
<td><select name="amtDeposited" id="amtDeposited">
	<option value="pro-counter">PRO Office-counter</option>
	<option value="Charity-cash">Charity-Cash</option>
	<option value="Development-cash">Development-cash</option>
</select></td>
<td><select name="category">
		<option value="deposit">Amount Deposit (In Bank)</option>
</select></td>
</tr>
<tr>
<td>Major Head</td>
<td>Minor Head</td>
<td>Sub Head</td>
</tr>
<%int b = 2; %>
<tr>
<td><input type="text" name="major_head_id<%=b%>" id="major_head_id<%=b%>" onkeyup="majorheadsearch('<%=b%>')" class="" value="" placeholder="find a major head here" autocomplete="off"/></td>
<td><input type="text" name="minor_head_id<%=b%>" id="minor_head_id<%=b%>" onkeyup="minorheadsearch('<%=b%>')" class="" value=""  placeholder="find a minor head here" autocomplete="off"/></td>
<td><input type="text" name="product<%=b%>" id="product<%=b%>"  onkeyup="productsearch('<%=b%>')" class="" value="" autocomplete="off" /></td>
</tr>
<tr>
<td>Amount*</td>
<td>Reference</td>
<td></td>
</tr>
<tr>
<td><input type="text"  name="amount" id="amount1" value="0" onKeyPress="return numbersonly(this, event,true);" />
<input type="hidden" name="HOA" value="<%=hoid%>"/></td>
<td><input type="text" name="extra1" id="extra1" value=""></td>
</tr>
<tr>
<td colspan="2"><textarea rows="1" cols="4" name="Narration" id="Narration" required placeholder="Write your narration here"></textarea></td>
</tr>
<tr>
<td align="right" colspan="2">
<input type="hidden" name="depositType" value="DLRDeposit"/>
<input type="submit" value="DEPOSIT" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>

<!-- other deposit Div -->

<div id="other-deposit" <%if(depositMode !=null){ if("otherdeposit".equals(depositMode)){%> style="display:block" <%} else{%> style="display:none" <%}}else{%>style="display:block"<%} %>>
<form  method="post" name="form2" action="banktransactionsInsert.jsp" onsubmit="return validate();">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr><td>&nbsp;</td></tr>
<tr>
<td>Select Fin Year</td>
<td>Deposit Date</td>
<td>Bank*</td>
<td>Amount depositing By*</td>
<td>Category</td>
</tr>
<tr>
<td><select name="finYear" id="finYear2"  Onchange="datepickerchange2();">
					<option value="2021-22"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2021-22")) {%> selected="selected"<%} %>>2021-2022</option> 					<option value="2019-20"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option>
					<option value="2020-21"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-21")) {%> selected="selected"<%} %>>2020-2021</option> 					<option value="2019-20"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option>
					<option value="2019-20"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option>
					<option value="2018-19"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%> selected="selected"<%} %>>2018-2019</option>
					<option value="2017-18"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%> selected="selected"<%} %>>2017-2018</option>
					<option value="2016-17"  <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-2017</option>
					</select></td>
<td>
<input type="hidden" name="cashierReportIds" id="cashierReportIds" value="">
<input type="text" name="date" id="date2" value="<%=currentDate %>" readonly/></td>
<td><input type="text" name="bank_id" id="id2" value="" placeHolder="Find a bank here" required style="height: 22px;"/></td>
<td><select name="amtDeposited" id="amtDeposited2">
	<option value="poojastore-counter">PoojaStore-counter</option>
	<option value="pro-counter">PRO Office-counter</option>
	<option value="Charity-cash">Charity-Cash</option>
	<option value="Development-cash">Development-cash</option>
	<option value="Sainivas">Sainivas(Shirdi)</option>
	<option value="Ewf-cash">EWF Cash</option>
	<option value="vendor">Vendors</option>
</select></td>
<td><select name="category">
		<option value="deposit">Amount Deposit (In Bank)</option>
</select></td>
</tr>
<tr>
<td>Major Head</td>
<td>Minor Head</td>
<td>Sub Head</td>
</tr>
<%int c = 3; %>
<tr>
<td><input type="text" name="major_head_id<%=c%>" id="major_head_id<%=c%>" onkeyup="majorheadsearch('<%=c%>')" class="" value="" placeholder="find a major head here" autocomplete="off"/></td>
<td><input type="text" name="minor_head_id<%=c%>" id="minor_head_id<%=c%>" onkeyup="minorheadsearch('<%=c%>')" class="" value=""  placeholder="find a minor head here" autocomplete="off"/></td>
<td><input type="text" name="product<%=c%>" id="product<%=c%>"  onkeyup="productsearch('<%=c%>')" class="" value="" autocomplete="off" /></td>
</tr>
<tr>
<td>Amount*</td>
<td>Reference</td>
<td></td>
</tr>
<tr>
<td><input type="text" name="amount" id="amount" value="0" onKeyPress="return numbersonly(this, event,true);" />
<input type="hidden" name="HOA" value="<%=hoid%>"/></td>
<td><input type="text" name="extra1" id="extra1" value=""></td>
<td id="vendorblock2"><input type="text" name="vendorId" id="vendorId2" onkeyup="vendorsearch2()" autocomplete="off" placeholder="Enter vendor name" value="" /></td>
</tr>
<tr>
<td colspan="2"><textarea rows="1" cols="4" name="Narration" id="Narration" required placeholder="Write your narration here"></textarea></td>
</tr>
<tr>
<td align="right" colspan="2">
<input type="hidden" name="depositType" value="OTHDeposit"/>
<input type="submit" value="DEPOSIT" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>

</div>
</div>
</div>
<div class="vendor-list">

<div class="clear"></div>
<!-- <div class="arrow-down"><img src="../images/Arrow-down.png"></div>

<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
</span></div> -->
<div class="clear"></div>



 <!--Commented by pradeep from here to start  -->
 
	<%-- <div class="list-details" style="margin:10px 10px 0 0">
	<%
	if(hoid.equals("1") || hoid.equals("4") || hoid.equals("5")){
		if(hoid.equals("4")){
			aliashoid="1";
			aliascashDeposite="CharityDeposit";
		}
	
		if(hoid.equals("5")){
			 CRL=CSHR_L.getSainivasCounterBalanceNotDeposited();
		} else {
		 CRL=CSHR_L.getCounterBalanceNotDepositedList("4");
		}%>
	<style>
.yourID.fixed {
    position: fixed !important;
    top: 0;
    left: 0px;
    z-index: 1500;
   width:96%;margin: 0 2%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
		<table width="100%" cellpadding="0" cellspacing="0" >
	<tr>
    
		<td class="bg" width="4%">S.NO</td>
		<td class="bg" width="4%">Check For add Amount</td>
		<%if(hoid.equals("4")){ %>
		<td class="bg" width="10%">CLOSING AMOUNT(Counter Balance - Charity Amount)</td>
		<%} else if(hoid.equals("1")){ %>
		<td class="bg" width="10%">CLOSING AMOUNT(Counter Balance - Sansthan Amount)</td>
		<%} else { %>
			<td class="bg" width="10%">CLOSING AMOUNT</td>
		<%} %>
		<td class="bg" width="10%">COUNTER NAME</td>
		<td class="bg" width="10%">COUNTER CLOSED DATE</td>
		<td class="bg" width="8%">EMPLOYEE</td>
	</tr>
	<%
	double totAmt=0.00;
	double DueAmt=0.00;
	double totamnt1=0.0;
	int j=0;
	if( CRL!=null && CRL.size()>0){
		if(hoid.equals("5")){
			j=0;
		for(int i=0;i<CRL.size();i++){
			DueAmt=0.0;
				CSH=(cashier_report)CRL.get(i);
				if(CSH.getcash6().equals("") ){
				EmployeAmount=Double.parseDouble(CSH.gettotal_amount()) ;
				totAmt=totAmt+EmployeAmount;
				j++;
				%>
		<tr>
			<td width="4%" align="center"><%=j %></td>
			<td width="4%" align="center">
			<input type="hidden" name="sjID" id="sjID<%=i%>" value="<%=CSH.getcashier_report_id()%>"> 
			<input type="checkbox" name="check" id="check<%=i%>" value="<%=EmployeAmount%>" onclick="depositAmountCal('<%=i%>')"></td>
			<td width="10%" align="center"><%=EmployeAmount%> </td>	
			<td width="10%" align="center"><%=COUNL.getShrisainivasName(CSH.getcounter_code())%></td>
			<td width="10%" align="center"><%=CSH.getcash2()%></td>
			<td width="8%" align="center"><%=LOGL.getName(CSH.getlogin_id())%></td>
			
			<%	if(hoid.equals("1") && !CSH.getcash6().equals("Due_NotDeposit") || hoid.equals("4") && !CSH.getcash6().equals("Due_NotDeposit")){ %>
			<td width="8%" align="center"><a href="adminPannel.jsp?page=cashier_balance_edit&q=<%=CSH.getcashier_report_id()%>">Edit</a></td>
			<%} %>
		</tr>	
		
		<%}}%>
		<input type="hidden" name="countersSize" id="countersSize" value="<%=j%>"> 
		
		<%} else {
			j=0;
		for(int i=0;i<CRL.size();i++){
		DueAmt=0.0;
			CSH=(cashier_report)CRL.get(i);
			if(CSH.getcash6().equals("Not-deposited")  || CSH.getcash6().equals(aliascashDeposite) || (CSH.getcash9().equals("Due") && hoid.equals("1"))){
			EmployeAmount=CUSTP_L.getDayWiseIncomeBasedEmployeID(hoid,CSH.getcash2(),CSH.getlogin_id());
			totAmt=totAmt+EmployeAmount;
			totamnt1=CUSTP_L.getDayWiseIncomeBasedEmployeID(aliashoid,CSH.getcash2(),CSH.getlogin_id());
			if(hoid.equals("1") && CSH.getcash9().equals("Due")){
				DueAmt=Double.parseDouble(CSH.getcash8());
				EmployeAmount=EmployeAmount-DueAmt;
				if(!CSH.getcash7().equals("")  && CSH.getcash9().equals("Due")){
					EmployeAmount=DueAmt;
				}
			}
			j++;
			%>
	<tr>
		<td width="4%" align="center"><%=j %></td>
		<td width="4%" align="center">
		<input type="hidden" name="sjID" id="sjID<%=i%>" value="<%=CSH.getcashier_report_id()%>"> 
		<input type="checkbox" name="check" id="check<%=i%>" value="<%=EmployeAmount%>" onclick="depositAmountCal('<%=i%>')"></td>
		<td width="10%" align="center"><%=EmployeAmount%> (<%=CSH.getclosing_balance()%>-<%=totamnt1 %>) <span style="font-weight: bold;color: red;"><%if(DueAmt>0){ %>Due : <%=DueAmt%><%} %></span></td>	
		<td width="10%" align="center"><%=COUNL.getName(CSH.getcounter_code())%></td>
		<td width="10%" align="center"><%=CSH.getcash2()%></td>
		<td width="8%" align="center"><%=EMPL.getMemployeesName(CSH.getlogin_id())%></td>
		<%	if(hoid.equals("1") && !CSH.getcash6().equals("Due_NotDeposit") || hoid.equals("4") && !CSH.getcash6().equals("Due_NotDeposit")){ %>
		<td width="8%" align="center"><a href="adminPannel.jsp?page=cashier_balance_edit&q=<%=CSH.getcashier_report_id()%>">Edit</a></td>
		<%} %>
	</tr>	
	
	<%}}%>
	<input type="hidden" name="countersSize" id="countersSize" value="<%=j%>"> 
	<% }%>
	<tr>
		<td class="bg" colspan="2">TOTAL AMOUNT</td>
		<td class="bg" colspan="1" align="right"><%=DF.format(totAmt) %></td>
		<td class="bg" colspan="3"></td>
	</tr>
	<%} %>
	</table> 
	<%} %>
	</div> --%>
	
	 <!--Commented by pradeep from here to end  -->
</div>
</div>

<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>