<%@page import="beans.vendors"%>
<%@page import="java.util.Map"%>
<%@page import="beans.godwanstock"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>VENDOR REPORTS</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
	</script>
   
	<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>

	

<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
	<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script> 
<link rel="stylesheet" href="/resources/demos/style.css"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
</head>
<body>

<jsp:useBean id="GODW" class="beans.godwanstock"/>
<form><div class="vendor-box">
<div class="vender-details">

				<%
				headofaccountsListing HOA_L=new headofaccountsListing();
				List headaclist=HOA_L.getheadofaccounts();
				String hoaid="1";
				if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
					hoaid=request.getParameter("head_account_id");
				}
				String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());  %>
<form action="#" method="post">
<input type="hidden" name="page" value="vendorReport"/>
<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:50px;margin-left:180px;">
<tr>
<td>From Date<input type="text" name="fromDate" id="fromDate" value="<%=currentDate %>" style="width: 150px;" readonly/></td>
<td>To Date<input type="text" name="toDate" id="toDate" value="<%=currentDate %>" style="width: 150px;" readonly/></td>
<td colspan="2"><select name="head_account_id" id="head_account_id">
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><input type="submit" name="button" id="" value="submit"/></td>

</tr>
</table>
</form>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>

<div style="clear:both;"></div>
</div>
<%mainClasses.godwanstockListing GODS_l=new mainClasses.godwanstockListing();
mainClasses.vendorsListing VEND = new mainClasses.vendorsListing();

String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
	 fromdate=request.getParameter("fromDate")+" 00:00:01";
}
if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
	todate=request.getParameter("toDate")+" 23:23:59";
}
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("") && request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
%>

<%
double sumOfQuantity = 0;
double sumOfAmount = 0;
int sumOfGST = 0;
double sumOfIGST = 0;
double sumOfCGST = 0;
double sumOfSGST = 0;
double sumOfTotalAmount = 0;
DecimalFormat df = new DecimalFormat("####0.00");
List<String> gsts = GODS_l.getGSTs(); 
List<godwanstock> GODS_DET=GODS_l.getStockVendorDetailsBasedOnDateAndHeadId(fromdate,todate,hoaid); 
Map<String, vendors> vendorsMap = VEND.getVendorsMap();
%>
<div class="printable" >
		<table width="95%" cellpadding="0" cellspacing="0" >
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">GST Report(Input)</td></tr>
				
<%
if(GODS_DET.size()>0){
	%>
	<table width="100%"  cellpadding="0" cellspacing="0"  border="1" id="tblExport">
<tr>
<th class="bgcolr">S.NO.</th>
<th class="bgcolr">Vendor</th>
<th class="bgcolr">Vendor GSTNO.</th>
<th class="bgcolr">INVOICE NUM</th>
<th class="bgcolr">Quantity</th>
<th class="bgcolr">AMOUNT</th>
<th class="bgcolr">GST(%)</th>
<th class="bgcolr">IGST</th>
<th class="bgcolr">CGST</th>
<th class="bgcolr">SGST</th>

<!-- <th class="bgcolr">IGST</th> -->
<th class="bgcolr">Total Amount</th>
<th class="bgcolr">Invoice Date</th>
<!-- <th class="bgcolr">Payment Date</th> -->
</tr>
	
<%	for(int b=0;b<gsts.size();b++){
		int n = 0;int testingVal = 0;
		for(int i=0;i<GODS_DET.size();i++){
			godwanstock godwanstock = GODS_DET.get(i);
			if(gsts.get(b).equals(godwanstock.getExtra17())){
				testingVal = testingVal+1;
			}
		}
		
		if(testingVal>0){
		%>
		
		  
		<%
	for(int i=0;i<GODS_DET.size();i++){
	GODW=(beans.godwanstock)GODS_DET.get(i);
//	System.out.println("GODS_DET >>>>> "+GODS_DET.size());
if(gsts.get(b).equals(GODW.getExtra17())){
%>
<tr>
<td style="text-align:center;"><%=n+1%></td>
<td style="text-align:center;"><%=vendorsMap.get(GODW.getvendorId()).getagencyName()%></td>
<td style="text-align:center;"><%=vendorsMap.get(GODW.getvendorId()).getExtra8()%></td>
<td style="text-align:center;"><%=GODW.getextra1() %></td>
<td style="text-align:center;"><%=GODW.getquantity() %></td>
<td style="text-align:center;"><%=Double.parseDouble(GODW.getpurchaseRate())%></td>
<td style="text-align:center;"><%=GODW.getExtra17() %></td>
<td style="text-align:center;"><%=Double.parseDouble(GODW.getExtra20()) %></td> 
<td style="text-align:center;"><%=Double.parseDouble(GODW.getExtra18()) %></td>
<td style="text-align:center;"><%=Double.parseDouble(GODW.getExtra19()) %></td>
 
<td style="text-align:center;"><%=df.format((Double.parseDouble(GODW.getpurchaseRate())+Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19()))*Double.parseDouble(GODW.getquantity())*(1+(Double.parseDouble(GODW.getvat())/100)))%></td>
<td style="text-align:center;"><%=GODW.getdate()%></td>
<!-- <td style="text-align:center;">10/11/2017</td> -->
<% sumOfQuantity = sumOfQuantity+Double.parseDouble(GODW.getquantity()); 
sumOfAmount = sumOfAmount+Double.parseDouble(GODW.getpurchaseRate());
// sumOfGST = sumOfGST+Integer.parseInt(GODW.getExtra17());
sumOfCGST = sumOfCGST+Double.parseDouble(GODW.getExtra18()) ;
sumOfSGST = sumOfSGST+Double.parseDouble(GODW.getExtra19());
sumOfIGST = sumOfIGST+Double.parseDouble(GODW.getExtra20());
sumOfTotalAmount = sumOfTotalAmount+Double.parseDouble(df.format((Double.parseDouble(GODW.getpurchaseRate())+Double.parseDouble(GODW.getExtra18())+Double.parseDouble(GODW.getExtra19()))*Double.parseDouble(GODW.getquantity())*(1+(Double.parseDouble(GODW.getvat())/100))));
%>
</tr>
<%n = n+1;}} %>
<tr>
<td colspan="4" align="center">Total</td>
<td align="center"><%out.println(sumOfQuantity);%></td>
<td align="center"><%out.println(sumOfAmount);%></td>
 <td align="center"></td>
 <td align="center"><%out.println(sumOfIGST);%></td>
<td align="center"><%out.println(sumOfCGST);%></td>
<td align="center"><%out.println(sumOfSGST);%></td>
<td align="center"><%out.println(sumOfTotalAmount);%></td>
<td></td>
</tr>
<%}
%>

<%} %>
<%	
} %>
</table>
</table>
</div>

<%} %>
</form>

</body>
</html>