<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.products"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="beans.ProductOpeningBalance"%>
<%@page import="beans.ProductOpeningBalanceService"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sai sansthan accounts-Admin Pannel</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<script type="text/javascript" lang="javascript"
	src="../js/modal-window.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

function datepickerchange()
{
	var finYear=$("#finacialyear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 

</script>
<script type="text/javascript">
function submission(){
	
	var fromDate=$("#fromDate").val();
	var toDate=$("#toDate").val();
	if(fromDate==""){
		alert('Please select From Date');
		return false;
	}
	if(toDate==""){
		alert('Please select To Date');
		return false;
	}
	$("#fulltable").show();
	return true;
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#cat1').change(function() {
			var cat = $(this).val();
			if ($.trim(cat) !== "") {
				$('#subcat1').empty();
				$('#subcat1').append($('<option>', {
					value : "",
					text : 'Select SubCategory'
				}));
				$.ajax({
					url : "searchSubHeadSj.jsp",
					type : "GET",
					data : {
						cat : cat
					},
					success : function(response) {
						var data = response.split(",");
						for (var i = 0; i < data.length; i++) {
							if (data[i].trim() !== "") {
								$('#subcat1').append($('<option>', {
									value : data[i],
									text : data[i].toUpperCase()
								}));
							}
						}
					}
				});
			}
		});
	});
</script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
	<form id="departmentsearch" method="post"
				style="padding-left: 70px; padding-top: 30px;">
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td colspan="2">Select Financial<br>Year
							</td>

							<td class="border-nn" colspan="2"><div class="warning"
									id="headIdErr" style="display: none;">Please Provide
									"head of account".</div>Category</td>
							<td class="border-nn">Sub Category</td>
							<td class="border-nn">&nbsp;&nbsp;From Date</td>
							<td class="border-nn">To Date</td>
						</tr>
						<tr>
							<td colspan="2"><select name="finacialyear"
								id="finacialyear" Onchange="datepickerchange();">
									<option value="2020-04-01">2020-2021</option>
									<option value="2021-04-01">2021-2022</option>
									<option value="2022-04-01">2022-2023</option>
									<option value="2023-04-01">2023-2024</option>
									<option value="2024-04-01">2024-2025</option>
							</select></td>

							<td colspan="2" class="border-nn"><select name="cat1"
								id="cat1">
									<option value="">Select Category</option>
									<%
							mainClasses.productsListing prodService = new mainClasses.productsListing();
							List catlist = prodService.getProductCategories();
						%>
									<%
									for (int i = 0; i < catlist.size(); i++) {
										String cat = (String) catlist.get(i);
								%>
									<option value="<%=cat%>"><%=cat.toUpperCase()%></option>
									<%
									}
								%>
							</select></td>
							<td><select name="subcat1" id="subcat1">
									<option value="">Select SubCategory</option>
							</select></td>
							<td class="border-nn">&nbsp;&nbsp;<input type="text" name="fromDate" id="fromDate"
					class="DatePicker" value="" placeholder="From Date"
					readonly="readonly" />
							</td>
							<td class="border-nn"><input type="text" name="toDate" id="toDate"
					class="DatePicker" value="" placeholder="To Date" readonly required />
								<input type="hidden" name="page"
								value="kitchenStockProvisions1">
								</td>
							<td class="border-nn"><input type="submit" value="SEARCH"
								class="click"></td>
						</tr>
					</tbody>
				</table>
			</form>
	<%      String fromDate=request.getParameter("fromDate");
			String toDate=request.getParameter("toDate");
			String cat1=request.getParameter("cat1");
			String subcat1=request.getParameter("subcat1");
			String finacialyear=request.getParameter("finacialyear");
			productsListing productsListing=new productsListing();
			List<products> productsList=productsListing.getProductsBasedOnCategory(cat1,subcat1);
			ProductOpeningBalanceService productOpeningBalanceService=new ProductOpeningBalanceService();
			Map<String,ProductOpeningBalance> productOpeningBalanceMap=productOpeningBalanceService.getProductOpeningBalancesMap(cat1, subcat1, finacialyear);
			double openingBalance=0.0;
			double clogingBal=0.0;
			String inwards="00";
			String outwards="00";
			String statusStockRequest="";
			godwanstockListing GDSTK_L=new godwanstockListing();
			shopstockListing SHPSTK_L=new shopstockListing();
			stockrequestListing SRQL=new stockrequestListing();
			%>
	
			<div class="icons">

				<span><a id="btnExport" href="#Export to excel"><img
						src="../images/excel.png" style="margin: 0 20px 0 0"
						title="export to excel"></a></span> <span> </span>
			</div>
			<div class="clear"></div>
			<div class="list-details">



				<div class="total-report">

					<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

				</div>
			</div>


	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered mb-0" id="tblExport">
					<thead class="thead-light">
					</thead>
					<tbody>

						<tr>
							<th>S.NO</th>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>Product OpeningBalance</th>
							<th>Received Quantity</th>
							<th>Issued Quantity</th>
							<th>Closing Balance</th>
						</tr>
						<% 
								for(int i=0;i<productsList.size();i++){
									
									 products products=productsList.get(i);
									 if(productOpeningBalanceMap.containsKey(products.getproductId())){
									 
									     ProductOpeningBalance productOpeningBalance=productOpeningBalanceMap.get(products.getproductId());
									     openingBalance= productOpeningBalance.getQuantity();
									 }
									 statusStockRequest=SRQL.getStatus(products.getproductId());
									 inwards=GDSTK_L.getgodwanstockbyDate(fromDate,toDate,products.getproductId()); 
									 outwards=SHPSTK_L.getgShopstockbyDate(fromDate,toDate,products.getproductId());
								%>
						<tr>
							<td><%=i+1%></td>
							<td><%=products.getproductId()%></td>
							<td><%=products.getproductName()%></td>
							<td><%=openingBalance%></td>
							<%if(inwards!=null && !inwards.equals("null") && !inwards.equals("")){ %>
					<td><%=inwards%></td>
					<%}else{ 
					inwards="0";
					%>
					<td>0</td>
					<%}if(outwards!=null && !outwards.equals("null") && !outwards.equals("") && statusStockRequest.equals("issued")){ %>
					<td><%=outwards%></td>
					<%}else{
					outwards="0";	%>
					<td>0</td>
					<%} %>
					<%
						clogingBal=openingBalance+Double.parseDouble(inwards)-Double.parseDouble(outwards);
					 %><td><%=String.format("%.2f", clogingBal)%></td>
						<% 		} %>
								
						</tr>
						
                      
					</tbody>
				</table>

			</div>
		</div>
	</div>
</body>
</html>