<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import="beans.employeeSalaryService,beans.employeeDetailsService,mainClasses.employeeSalarySlipListing"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page import="mainClasses.employeeDetailsListing,beans.employeeDetails"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%

	String checkedItem[]=request.getParameterValues("employee_id");
	String month1=request.getParameter("monthAndYear");
	
	/* change the date format
	SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy");
	Date d1=df.parse(month1);
	SimpleDateFormat df1=new SimpleDateFormat("yyyy-MM-dd");
	String dateAndMonth=df1.format(d1); 
	*/
	String monthAndYear=month1;
	
	StringBuffer ids=new StringBuffer("");
	for(int i=0;i<checkedItem.length;i++)
	{
		ids=ids.append("&employee_id=").append(checkedItem[i]);
	
		String  employee_id =request.getParameter("employee_id1"+checkedItem[i]);
		String  month =request.getParameter("month"+checkedItem[i]);
		String  ewf_loan_amount=request.getParameter("ewf_loan_amount"+checkedItem[i]);
		String  ewf_interest_amount=request.getParameter("ewf_interest_amount"+checkedItem[i]);
		String  personal_loan_amount=request.getParameter("personal_loan_amount"+checkedItem[i]);
		String  personal_interest_amount=request.getParameter("personal_interest_amount"+checkedItem[i]);
		String  basic_pay=request.getParameter("basic_pay"+checkedItem[i]);
		String  hra=request.getParameter("hra"+checkedItem[i]);
		String  otpay=request.getParameter("otpay"+checkedItem[i]);
		String  medical_insurance=request.getParameter("medical_insurance"+checkedItem[i]);
		String  salary_pay=request.getParameter("salary_pay"+checkedItem[i]);

		String LP=request.getParameter("extra1"+checkedItem[i]); //get LP
		String LA=request.getParameter("extra2"+checkedItem[i]); //get LA
		String OD=request.getParameter("extra3"+checkedItem[i]); //get OD
		String opening_cl=request.getParameter("extra4"+checkedItem[i]); //get opening_cl
		String earned_cl=request.getParameter("extra5"+checkedItem[i]);// get earned_cl 
		String closing_cl=request.getParameter("extra6"+checkedItem[i]); //get closing_cl
		String ot=request.getParameter("extra7"+checkedItem[i]); //get ot
		String deductions=request.getParameter("deductions"+checkedItem[i]);
		String allowances=request.getParameter("allowances"+checkedItem[i]);
		String ewf_deduction=request.getParameter("ewf_deduction"+checkedItem[i]); //get ewf_deduction
		
		
		/* String wa_amount=request.getParameter("wa_amount"+checkedItem[i]); //get wa_amount */
		
		String medicalinsurance=request.getParameter("medicalinsurance"+checkedItem[i]);
		
		
		String salary_cut_days_amount=request.getParameter("salary_cut_days_amount"+checkedItem[i]); //get salary_cut_days_amount
		
		
		if(allowances.equals(""))
		{
			allowances = Double.toString(0.0);
		}	
		if(deductions.equals(""))
		{
			deductions = Double.toString(0.0);
		}	

		
		employeeSalaryService service=new employeeSalaryService();
		service.setEmployee_id(employee_id);
		//service.setMonth(month);
		service.setMonth(monthAndYear);
		service.setEwf_loan_amount(ewf_loan_amount);
		service.setEwf_interest_amount(ewf_interest_amount);
		service.setPersonal_loan_amount(personal_loan_amount);
		service.setPersonal_interest_amount(personal_interest_amount);
		service.setBasic_pay(basic_pay);
		service.setHra(hra);
		service.setOtpay(otpay);
		service.setMedical_insurance(medical_insurance);
		service.setSalary_pay(salary_pay);
		service.setExtra1(LP);
		service.setExtra2(LA);
		service.setExtra3(OD);
		service.setExtra4(opening_cl);
		service.setExtra5(earned_cl);
		service.setExtra6(closing_cl);
		service.setExtra7(ot);
		service.setExtra8(ewf_deduction);
		
		/* service.setExtra9(wa_amount); */
		
		service.setMedical_insurance(medical_insurance);
		
		service.setExtra10(salary_cut_days_amount);
		
		service.setOther_deductions(deductions);
		
		employeeDetailsListing listing=new employeeDetailsListing();
		employeeDetails edetails =listing.selectSingleEmployeeDetailsBasedUponEmployeeId(employee_id);
		int ewf_bal_installments = Integer.parseInt(edetails.getExtra7());
		int personal_bal_installments = Integer.parseInt(edetails.getExtra9());
		
		if(ewf_bal_installments > 0)
		{
			ewf_bal_installments = ewf_bal_installments - 1;
		}
		
		if(personal_bal_installments > 0)
		{
			personal_bal_installments = personal_bal_installments - 1;
		}
		
		service.setOther_allowances(allowances);
		employeeSalarySlipListing  slip=new employeeSalarySlipListing ();
		if(!slip.validateEmpIdAndMonth(employee_id,monthAndYear))
		{
			
			boolean b=service.insert();
			if(b)
			{
				employeeDetailsService details=new employeeDetailsService();
				//details.setEwf_due_amount(ewf_due_amount);
				//details.setPersonal_due_amount(personal_due_amount);
				details.updateEwfAndPersonalLoad(ewf_loan_amount,personal_loan_amount,employee_id);
				details.updateEwfAndPersonalInstallments(String.valueOf(ewf_bal_installments),String.valueOf(personal_bal_installments),employee_id);
			}
		}
	}//for(int i=0;i<checkedItem.length;i++)
	
	String url="paysliplist.jsp?month="+monthAndYear+ids.toString();
 	response.sendRedirect(url); 
	
%>
</body>
</html>