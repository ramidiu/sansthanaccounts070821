<%@page import="beans.headofaccounts"%>
<%@page import="com.accounts.saisansthan.MapOfHeads"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.Collections"%>
<%@page import="beans.customerpurchases"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="org.apache.commons.collections.MapIterator"%>
<%@page import="org.apache.commons.collections.map.MultiValueMap"%>
<%@page import="com.accounts.saisansthan.CollectionSummary"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>COLLECTION SUMMARY REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<!--Date picker script  -->
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

</head>
<script>
$(function(){
	
	$( "#fromdate" ).datepicker({
		changeMonth : true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		onSelect: function( selectedDate ) {
			var reporttype = $('#reporttype').val();
			if ($.trim(reporttype) === "") {
				alert("Please Select Report Type First");
				$('#fromdate').val("");
			}
			else	{
				var finyear = $('#finyear').val();
				if ($.trim(finyear) === "")	{
					alert("Please Select Financial Year");
					$('#fromdate').val("");
				}
			}
			$( "#todate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	
$( "#todate" ).datepicker({
		changeMonth : true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		onSelect: function( selectedDate ) {
			$( "#fromdate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	
	$('#finyear').empty();
	$('#finyear').append($('<option>', { value: "", text: 'Select FinYear' }));
	var minyear = new Date("2016-04-01").getFullYear();
	var maxyear = new Date().getFullYear();
	while (minyear <= maxyear)	{
		$('#finyear').append($('<option>', { value: minyear+"-"+(minyear + 1) , text: minyear+"-"+(minyear + 1) }));
		minyear++;
	}
	
	$('#finyear').change(function(){
		datepickerchange();
	});
	
	datepickerchange();
});

function datepickerchange()
{
	var finYear=$("#finyear").val();
	var yr = finYear.split('-');
	var startDate = "01-04-"+yr[0];
	var endDate = "31-03-"+yr[1];
	$('#fromdate').val(startDate);
	$('#todate').val(endDate);
	$('#fromdate').datepicker('option', 'minDate', new Date(yr[0]+",04,01"));
	$('#fromdate').datepicker('option', 'maxDate', new Date(yr[1]+",03,31"));
	$('#todate').datepicker('option', 'minDate', new Date(yr[0]+",04,01"));
	$('#todate').datepicker('option', 'maxDate', new Date(yr[1]+",03,31"));
}

function checkData()	{
	$('#fromdate').datepicker('option', 'dateFormat','yy-mm-dd');
	$('#todate').datepicker('option', 'dateFormat','yy-mm-dd');
	
	var hoa = $('#hoa').val();
	var reporttype = $('#reporttype').val();
	var finyear = $('#finyear').val();
	var fromdate = $('#fromdate').val();
	var todate = $('#todate').val();
	if ($.trim(hoa) === "")	{ alert("Please Select Department"); $('#hoa').focus(); return false; }	
	if ($.trim(reporttype) === "")	{ alert("Please Select reporttype"); $('#reporttype').focus(); return false; }	
	if ($.trim(finyear) === "")	{ alert("Please Select finyear"); $('#finyear').focus(); return false; }	
	if ($.trim(fromdate) === "")	{ alert("Please Select fromdate"); $('#fromdate').focus(); return false; }	
	if ($.trim(todate) === "")	{ alert("Please Select todate"); $('#todate').focus(); return false; }
	
	return true;
}

</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<body>
<div class="clear"></div>
<%if(session.getAttribute("adminId")!=null){%>
<jsp:include page="title-bar.jsp"></jsp:include>
	<form method="post" onsubmit="return checkData()">
	<div class="icons" style="margin: 97px 19px 0;">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span>
					<!--  <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span> -->
				</div>
	<table style="margin: 24px 81px;position: relative;top: 30px;">
		<tr >
			<td width="10%">DepartMent</td>
			<td width="10%" >ReportType</td>
			<td width="10%">FinancialYear</td>
			<td width="10%">FromDate</td>
			<td width="10%">ToDate</td>
			<td width="10%"></td>
		</tr>
		<tr>
			<td>
			<select id="hoa" name="hoa">
			<option>Select A Department</option>
			<option value="1">CHARITY</option>
			<option value="4">SANSTHAN</option>
			<option value="3">POOJA-STORES</option>
			</select>
			</td>
			<td>
			<select id="reporttype" name="reporttype">
				<option value="">Select Report Type</option>
				<option value="year">YearWise</option>
				<option value="month">MonthWise</option>
				<option value="day">DayWise</option>
			</select>
			</td>
			<td>
				<select id="finyear" name = "finyear">
				</select>
			</td>
			<td ><input type="text" name="fromdate" id="fromdate"></td>
			<td ><input type="text" name="todate" id="todate"></td>
			<td><input type="submit" value="submit" class="click"></td>
		</tr>
	</table>
	</form>
	
	<%
	if (request.getParameter("fromdate") != null && request.getParameter("todate") != null && request.getParameter("reporttype") != null && request.getParameter("hoa") != null )	{
		String fromdate = "", todate = "",reporttype = "",hoa = "";
		fromdate = request.getParameter("fromdate");
		todate = request.getParameter("todate");
		reporttype = request.getParameter("reporttype");
		hoa = request.getParameter("hoa");
		MapOfHeads mp = new MapOfHeads();
		Map<String,headofaccounts> hmap = mp.getHoasMap();
		DecimalFormat DF = new DecimalFormat("#.00");
		CollectionSummary cs = new CollectionSummary();
		MultiValueMap map = cs.getcollectionDetails(fromdate+" 00:00:00",todate+" 23:59:59", hoa, reporttype);
		SortedSet<String> keys = new TreeSet(map.keySet());
		Iterator<String> itr = keys.iterator();%>
		
		<table align="center" width="80%" border="1" rules="all" style="margin: 50px auto 0;" class="printable">
		<tr><td colspan="9" align="center" style="font-size: 15px;font-weight: bold;background-color: #a35f22;color: #fff;"><%=hmap.get(hoa).getname() %></td></tr>
		<tr class="bgcolr">
			<td style="font-size: 15px;font-weight: bold" align="center">S.No</td>
			<td style="font-size: 15px;font-weight: bold" align="center">Particulars</td>
			<td style="font-size: 15px;font-weight: bold" align="center">card</td>
			<td style="font-size: 15px;font-weight: bold" align="center">cash</td>
			<td style="font-size: 15px;font-weight: bold" align="center">cheque</td>
			<td style="font-size: 15px;font-weight: bold" align="center">online</td>
			<td style="font-size: 15px;font-weight: bold" align="center">BharathPe</td>
			<td style="font-size: 15px;font-weight: bold" align="center">phonePe</td>
			<td style="font-size: 15px;font-weight: bold" align="center">Total</td>
		</tr>
		<% int count = 0;
		double cashtot=0d,cardtot=0d,chequetot=0d,onlinetot=0d,gPayTot=0d,phnPeTot=0d;
		while (itr.hasNext())	{
			count ++;
			String key = itr.next();
			List<customerpurchases> cplist  = (List<customerpurchases>)map.get(key);
			double cash=0d;
			double card=0d;
			double cheque=0d;
			double online=0d;
			double total=0d;
			double gPay=0.0d;
			double phnPe=0.0d;
			for (int i = 0 ; i < cplist.size() ; i ++)	{
				customerpurchases cp = cplist.get(i);
				if (cp.getcash_type().equals("cash"))	{
					cash = Double.parseDouble(cp.gettotalAmmount());
					total += Double.parseDouble(cp.gettotalAmmount());
					cashtot += Double.parseDouble(cp.gettotalAmmount());;
				}
				else if (cp.getcash_type().equals("cheque"))	{
					cheque = Double.parseDouble(cp.gettotalAmmount());
					total += Double.parseDouble(cp.gettotalAmmount());
					chequetot += Double.parseDouble(cp.gettotalAmmount());
				}
				else if  (cp.getcash_type().equals("card"))	{
					card = Double.parseDouble(cp.gettotalAmmount());
					total += Double.parseDouble(cp.gettotalAmmount());
					cardtot += Double.parseDouble(cp.gettotalAmmount());
				}
				else if  (cp.getcash_type().equals("googlepay"))	{
					gPay= Double.parseDouble(cp.gettotalAmmount());
					total += Double.parseDouble(cp.gettotalAmmount());
					gPayTot += Double.parseDouble(cp.gettotalAmmount());
				}
				else if  (cp.getcash_type().equals("phonepe"))	{
					phnPe= Double.parseDouble(cp.gettotalAmmount());
					total += Double.parseDouble(cp.gettotalAmmount());
					phnPeTot += Double.parseDouble(cp.gettotalAmmount());
				}
				else{
					online = Double.parseDouble(cp.gettotalAmmount());
					total += Double.parseDouble(cp.gettotalAmmount());
					onlinetot += Double.parseDouble(cp.gettotalAmmount());
				}
			}%>
		<tr class="bgcolr">
			<td style="font-size: 15px;font-weight: bold"><%=count%></td>
			<td style="font-size: 15px;font-weight: bold"><%=key %></td>
			<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(card)%></td>
			<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(cash) %></td>
			<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(cheque) %></td>
			<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(online) %></td>
			<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(gPay) %></td>
			<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(phnPe) %></td>
			<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(total) %></td>
		</tr>			
		<%}%>
		<tr>
		<td colspan="2" style="font-size: 15px;font-weight: bold">Total Amount</td>
		<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(cardtot) %></td>
		<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(cashtot) %></td>
		<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(chequetot) %></td>
		<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(onlinetot) %></td>
		<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(gPayTot) %></td>
		<td style="font-size: 15px;font-weight: bold" align="right"><%=DF.format(phnPeTot) %></td>
		<td></td>
		</tr>
		</table>
<%}
}
else	{
	response.sendRedirect("index.jsp");	
}%>
</body>
</html>