<%@page import="mainClasses.employeesListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Password</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript">

function validate(){
	$('#oldError').hide();
	$('#newError').hide();
	$('#confirmError').hide();
	$('#oldError1').hide();
	$('#confirmError1').hide();
	if($('#oldpassword').val().trim()==""){
		$('#oldError').show();
		$('#oldpassword').focus();
		return false;
	}
	if($('#oldpassword').val().trim()!=$('#password').val().trim()){
		$('#oldError1').show();
		$('#oldpassword').focus();
		return false;
	}
	
	if($('#newpassword').val().trim()==""){
		$('#newError').show();
		$('#newpassword').focus();
		return false;
	}
	
	if($('#confirmpassword').val().trim()==""){
		$('#confirmError').show();
		$('#confirmpassword').focus();
		return false;
	}
	
	if($('#newpassword').val().trim()!=$('#confirmpassword').val().trim()){
		$('#confirmError1').show();
		$('#confirmpassword').focus();
		return false;
	}
		
}
</script>


</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>	
<body>
<div class="vender-details">
<form method="POST" action="changepasswordupdate.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Change Password</span><%if((request.getParameter("status")!=null )&& (request.getParameter("status").equals("sucess")) ){%><div class="warning">(New Password Changed Successfully)</div><%} %></td>
</tr>
<%employeesListing EMP_L=new mainClasses.employeesListing();%>
<input type="hidden" name="password" id="password" value="<%=EMP_L.getMEmployeePassword(session.getAttribute("adminId").toString())%>"/>
<tr>

<td width="2%"></td>
<td valign="top">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3">
<div class="warning" id="oldError" style="display: none;">Please Provide  "Old Password".</div>
<div class="warning" id="oldError1" style="display: none;">Please Provide Correct "Old Password".</div>
Old Password*</td>
</tr>
<tr>
<td colspan="3"><input type="password" name="oldpassword" id="oldpassword" style="width:98%;"></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="newError" style="display: none;">"New Password" Required.</div>New Password*</td>
</tr>
<tr>
<td colspan="3"><input type="password" name="newpassword" id="newpassword" onKeyPress="return numbersonly(this, event,true);" style="width:98%;"/></td>
</tr>
<tr>
<td colspan="3"><div class="warning" id="confirmError" style="display: none;">"Confirm Password" Required.</div>
<div class="warning" id="confirmError1" style="display: none;">"Confirm Password" is not matching with "New Password".</div>
Confirm Password*</td>
</tr>
<tr>
<td colspan="3"><input type="password" name="confirmpassword" id="confirmpassword" style="width:98%;"/></td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>
</body>
</html>