<%@page import="java.lang.reflect.Array"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Major Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;
}
.new-table td{
padding: 2px 0 2px 5px !important;}

@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
</style>

<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 

</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
        function minorhead(id){
        	$('#'+id).toggle();
        }
    </script>
   

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
</head>

<body>
<%
	/* String minorheadid = request.getParameter("subhid");
	System.out.println(minorheadid); */
	if(session.getAttribute("adminId")!=null){ 
		DecimalFormat df=new DecimalFormat("#,###.00");
		/* DecimalFormat DF=new DecimalFormat(""); */
		Calendar c1 = Calendar.getInstance(); 
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		String fromdate="";
		String onlyfromdate="";
		String todate="";
		String cashtype="online pending";
		String cashtype1="othercash";
		String cashtype2="offerKind";
		String cashtype3="journalvoucher";
		List minhdlist=null;
		minorheadListing MINH_L=new minorheadListing();
		String onlytodate="";
		productexpensesListing PRDEXP_L=new productexpensesListing();
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		todate=(dateFormat2.format(c1.getTime())).toString();
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromdate=(dateFormat2.format(c1.getTime())).toString();
		if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
			 fromdate=request.getParameter("fromDate");
		}
		if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
			todate=request.getParameter("toDate");
		}
		headofaccountsListing HOA_L=new headofaccountsListing();
		List headaclist=HOA_L.getheadofaccounts();
		String hoaid="1";
		if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
			hoaid=request.getParameter("head_account_id");
		}
	%>
<div class="vendor-page">
		<div>
			<form name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 70px;padding-top: 30px;">
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td colspan="2">Select Financial<br/>Year</td>
<td class="border-nn">&nbsp;&nbsp;From Date</td>
<td class="border-nn">To Date</td>
<td class="border-nn" colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td class="border-nn">Report Type</td>
</tr>
				<tr>
				<%
// 		SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
// 		Calendar calld = Calendar.getInstance();
// 		calld.add(Calendar.DATE,0);
// 		String todayDate=SDF.format(calld.getTime());
// 		String date[] = todayDate.split("-");
// 		 String currentyear = date[0];// 2017
// 		 String currentyear1 = currentyear.replace(currentyear.substring(2),""); // 20 
// 		 String currentyear2 = currentyear.replace(currentyear.substring(0,2),"");// 17
// 		 String year2 ="";
// 		 String year22 = "";	 
// 		 int currentyearinint = Integer.parseInt(currentyear2);	 
// 		 if(todayDate.compareTo(currentyear+"-04-01") >=0){
// 			 year2 =String.valueOf(currentyearinint+1);// 18
			 %>	 
			<td colspan="2">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<%@ include file="yearDropDown1.jsp" %>
					</select>
				  </td>
			
				<%-- <td colspan="2">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
					<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
					</select>
				</td> --%>
				<td class="border-nn">&nbsp;&nbsp;<input type="text"  name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>" /> </td>
				<td class="border-nn"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
				<input type="hidden" name="page" value="DayWise-Income-Expenditure-Report_neww"/>
				 </td>
<td colspan="2" class="border-nn"><select name="head_account_id" id="head_account_id" onchange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
<!-- <option value="SasthanDev">Sansthan Development</option> -->
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><select name="cumltv" id="cumltv">
<option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option>
	<option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option>
</select></td>
<td class="border-nn"><input type="submit" value="SEARCH" class="click"/></td>
</tr></tbody></table> </form>
				<% String finyr[]=null;	
				String headgroup="";
				String subhid="";
				double total=0.0;
				double credit=0.0;
				double debit=0.0;
				double creditincome=0.0;
				double debitincome=0.0;
				double creditexpens=0.0;
				double debitexpens=0.0;
				double creditsTotalExp = 0.0;
				
				productexpensesListing PEXP_L=new productexpensesListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat onlydat2=new SimpleDateFormat("dd-MMM-yyyy");
				if(request.getParameter("head_account_id")!=null){
					hoaid=request.getParameter("head_account_id");
					if(hoaid.equals("SasthanDev")){
						headgroup="2";
						hoaid="4";
					} else if(hoaid.equals("4")){
						headgroup="1";
					}
				}
				subheadListing SUBH_L=new subheadListing();
				/* if(request.getParameter("subhid")!=null){
					subhid=request.getParameter("subhid");
					System.out.println(subhid);
					//hoaid=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid);
				} */
				/* onlyfromdate=fromdate;
				fromdate=fromdate+" 00:00:00";
				onlytodate=todate;
				todate=todate+" 23:59:59"; */
				customerpurchasesListing CP_L=new customerpurchasesListing();
				majorheadListing MH_L=new majorheadListing();
				banktransactionsListing BNK_L = new banktransactionsListing();
				
				/* List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid, "revenue",headgroup);
				List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid,"expenses",headgroup); */
				//List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid, "revenue",headgroup);
				List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew1(hoaid, "revenue",headgroup,"IE");
				//List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"expenses",headgroup);
				List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew1(hoaid,"expenses",headgroup,"IE");
				
				%>
				<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
														
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span> <span>
						
					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	
	
	<div class="total-report">

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<% if(request.getParameter("fromDate") != null && !request.getParameter("toDate").equals("") && request.getParameter("finYear") != null){
		bankbalanceListing BBAL_L=new bankbalanceListing();
		String finYr = request.getParameter("finYear");
		String finStartYr = request.getParameter("finYear").substring(0, 4);
		String fdate = finStartYr+"-04-01"+" 00:00:00";
		String fromDate = request.getParameter("fromDate");
		if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("noncumulative")){
			fdate = request.getParameter("fromDate")+" 00:00:00";
		}
		String tdate = request.getParameter("toDate")+" 23:59:59";
	%>				
	<div class="printable" id="tblExport">


<table width="90%" cellpadding="0" cellspacing="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
<tbody><tr>
						<td colspan="3" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hoaid) %></td>
					</tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Income &amp; Expenditure Report</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=onlydat2.format(onlydat.parse(fdate.substring(0, 10))) %> TO <%=onlydat2.format(onlydat.parse(request.getParameter("toDate"))) %> (<%=request.getParameter("cumltv")%>) </td></tr>



</tbody></table>
<div style="clear:both"></div>

<table width="90%" border="1" cellspacing="0" cellpadding="0"  style="margin:0 auto;" class="new-table">
  <tbody><tr>
    <th width="38%" height="29" align="center">Account Name</th>
    <th width="29%" align="center">Debits</th>
    <th width="33%" align="center">Credits</th>
  </tr>
  
  <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">INCOME</td>
  
  </tr>
  <%
  if(incomemajhdlist.size()>0){ 
	String corpusMajorHead = "";
	String corpusMinorHead = "";
	double corpusfundAmt = 0.0;
	String minorheads[] =new String[2];
	
	if(hoaid.equals("1"))
	{
		corpusMajorHead = "13";
		corpusMinorHead = "476";
		minorheads = new String[]{"122","469"};
	}
	else if(hoaid.equals("3"))
	{
		corpusMajorHead = "14";
		corpusMinorHead = "21532";
		minorheads = new String[]{"132"};
	}
	else if(hoaid.equals("4"))
	{
		corpusMajorHead = "11";
		corpusMinorHead = "473";
		minorheads = new String[]{"102","468"};
	}
	else if(hoaid.equals("5"))
	{
		corpusMajorHead = "15";
		corpusMinorHead = "478";
		minorheads = new String[]{"142","472"};
	}
	  
	double creditCorpusfund = 0.0; 
	List subh3=SUBH_L.getSubheadListMinorhead4(hoaid,corpusMajorHead,corpusMinorHead,"IE");
	String subhd3[]=new String[subh3.size()];
	int o=0;
	for(o=0;o<subh3.size();o++){
	SUBH=(subhead)subh3.get(o);
	//System.out.println("size..."+subh3.size());
		//System.out.println("report.."+report[i]+"....major_head.."+major_head_id);
		/*  subhd1=subhd1+subh1[m]+","; */
	//System.out.println("subhead.."+SUBH.getsub_head_id());	
	subhd3[o]=SUBH.getsub_head_id();
	
	//System.out.println("subhead.."+subhd1[m]);
	}
	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){	
		
	
		/* creditCorpusfund = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,corpusMajorHead,corpusMinorHead, "", "minorhead","Assets", finYr, "credit", "","extra5",subhd3); */
		creditCorpusfund = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,corpusMajorHead,corpusMinorHead, "", "minorhead","Liabilites", finYr, "credit", "","extra5",subhd3);
	}
	
	//System.out.println("creditCorpusfund..."+df.format(creditCorpusfund));
	corpusfundAmt = creditCorpusfund+Double.parseDouble(CP_L.getLedgerSumOfSubhead31(corpusMinorHead,"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(corpusMajorHead),"sub_head_id",subhd3))+Double.parseDouble(CP_L.getLedgerSum1(corpusMinorHead,"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(corpusMajorHead),"sub_head_id",subhd3)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(corpusMinorHead,"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt1(corpusMinorHead,"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd3));
	//System.out.println("corpusfundAmt..."+df.format(corpusfundAmt));
	  for(int i=0;i<incomemajhdlist.size();i++){
									MH=(majorhead)incomemajhdlist.get(i);
									//System.out.println(MH.getextra3());
									/* List MajorHd=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew1(hoaid, "revenue",headgroup,"IE");
									String MajorHead1[]=new String[MajorHd.size()];
									int s=0;
									for(s=0;s<MajorHd.size();s++){
										MH=(majorhead)MajorHd.get(s);
										MajorHead1[s]=" ";
									String majorvalue='"'+MH.getmajor_head_id()+'"';
									if(!MH.getmajor_head_id().equals(majorvalue)){ */
									//List MajorHd=MINH_L.getMinorHeadBasdOnCompany1(MH.getmajor_head_id(),MNH.getminorhead_id(),"IE");
									List subh2=SUBH_L.getSubheadListMinorhead3(MH.getmajor_head_id(),"IE");
									String subhd2[]=new String[subh2.size()];
									int n=0;
									for(n=0;n<subh2.size();n++){
									SUBH=(subhead)subh2.get(n);
									//System.out.println("size..."+subh2.size());
										//System.out.println("report.."+report[i]+"....major_head.."+major_head_id);
										/*  subhd1=subhd1+subh1[m]+","; */
									//System.out.println("subhead.."+SUBH.getsub_head_id());	
									subhd2[n]=SUBH.getsub_head_id();
									
									//System.out.println("subhead.."+subhd1[m]);
									}
									double incomeMjrdebitAmt = Double.parseDouble(PEXP_L.getLedgerSum1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2));
									double incomeMjrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2));
									double incomeMjrcreditAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead111(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd2));
									double incomeMjrcreditAmtJV = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2));
									double incomeMjrcreditAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd2));
									
									double incomeMnrdebitAmtt =0.0;
									double incomeMnrdebitAmtt1 =0.0;
									double incomeMjrdebitAmttJV=0.0;
									double incomeMjrdebitAmt1JV1=0.0;
									double incomeMjrcreditAmtt = 0.0;
									double incomeMjrcreditAmtt1 = 0.0;
									double incomeMjrcreditAmttJV=0.0;
									double incomeMjrcreditAmttJV1=0.0;
									/* String[] minorheads = {"122","469"}; */
									if(minorheads.length > 0){
										for(int m=0 ;m<minorheads.length;m++){
											
											//System.out.println("minor heads..."+MNH.getminorhead_id());
											incomeMnrdebitAmtt1 = Double.parseDouble(PRDEXP_L.getLedgerSum1(minorheads[m],"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd2));
											incomeMnrdebitAmtt = incomeMnrdebitAmtt +incomeMnrdebitAmtt1;
											incomeMjrdebitAmt1JV1 = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(minorheads[m],"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2));
											incomeMjrdebitAmttJV = incomeMjrdebitAmttJV +incomeMjrdebitAmt1JV1;
											incomeMjrcreditAmtt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead111(minorheads[m],"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd2));
											incomeMjrcreditAmtt = incomeMjrcreditAmtt+incomeMjrcreditAmtt1;
											incomeMjrcreditAmttJV1 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV1(minorheads[m],"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2));
											incomeMjrcreditAmttJV = incomeMjrcreditAmttJV+incomeMjrcreditAmttJV1;
										}
									}
									
									  double mjdbamt = incomeMjrdebitAmt + incomeMjrdebitAmtJV-incomeMnrdebitAmtt-incomeMjrdebitAmttJV;  
									//double mjdbamt = incomeMjrdebitAmt -incomeMnrdebitAmtt-incomeMjrdebitAmttJV;
									double mjcdamt = incomeMjrcreditAmt + incomeMjrcreditAmtJV+ incomeMjrcreditAmtDollar-incomeMjrcreditAmtt-incomeMjrcreditAmttJV;
									double debitAmountOpeningBal = 0.0;
									double creditAmountOpeningBal = 0.0;
									double debitAmountOpeningBal1 = 0.0;
									double creditAmountOpeningBal1 = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "","extra5",subhd2);
										/* creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", "","extra5",subhd2); */
										creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Liabilites", finYr, "credit", "","extra5",subhd2);
										//String[] minorheads = {"122","469"};
										if(minorheads.length > 0){
											for(int m=0 ;m<minorheads.length;m++){
										  		double creditAmountOpeningBal11 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),minorheads[m], "", "minorhead","Assets", finYr, "credit", "","extra5",subhd2);
										  		creditAmountOpeningBal1 = creditAmountOpeningBal1 + creditAmountOpeningBal11;
											}
										}
									}	
									
									double totcreditAmountOpeningBal =creditAmountOpeningBal-creditAmountOpeningBal1;
									if(!("0.0".equals(""+mjdbamt)) || !("0.0".equals(""+mjcdamt)) || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0 || totcreditAmountOpeningBal > 0){
								%>
  
  <tr>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;" ><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getmajor_head_id() %> <%=MH.getname() %> (<%=MH.getextra2() %>)</a></td>
    <%
    	double corpusfundAmtTemp = 0.0;
    	if(MH.getmajor_head_id().equals(corpusMajorHead))
    	{
    		corpusfundAmtTemp = corpusfundAmt; 
    	}
    %>
    <%
				/* start */
				
			    //String[] minorheads = {"122","469"};
			    double otherAmountList11= 0.0;
				if(minorheads.length > 0){
					for(int m=0 ;m<minorheads.length;m++){
						double otherAmountList1=Double.parseDouble(BNK_L.getStringOtherDepositByheads1("other-amount",MH.getmajor_head_id(),minorheads[m],"",hoaid,fromdate,todate,"sub_head_id",subhd2));
						otherAmountList11 = otherAmountList11 + otherAmountList1;	
					}
				}
				List otherAmountList=BNK_L.getStringOtherDeposit1("other-amount",MH.getmajor_head_id(),"","",hoaid,fromdate,todate,"sub_head_id",subhd2);
    			//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
				double otherAmount = 0.0;
				if(otherAmountList.size()>0)
				{
					banktransactions OtherAmountList = (banktransactions)otherAmountList.get(0);
					if(OtherAmountList.getamount() != null)
					{
						otherAmount = Double.parseDouble(OtherAmountList.getamount());
					}
				}
				
				
				/* List dollarAmountList=BNK_L.getStringOtherDeposit("dollar-amount",MH.getmajor_head_id(),"","",hoaid,fromdate,todate);
	  			//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
				double dollarAmount = 0.0;
				if(dollarAmountList.size()>0)
				{
					banktransactions DollarAmountList = (banktransactions)dollarAmountList.get(0);
					if(DollarAmountList.getamount() != null)
					{
						dollarAmount = Double.parseDouble(DollarAmountList.getamount());
					}
				} */
				
				
				
				
				/* end */
				%>
	<%if(MH.getmajor_head_id().equals("13") || MH.getmajor_head_id().equals("15") || MH.getmajor_head_id().equals("11") || MH.getmajor_head_id().equals("14")){ %>
		  <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+incomeMjrdebitAmt + incomeMjrdebitAmtJV-incomeMnrdebitAmtt-incomeMjrdebitAmttJV) %></td>  
	 	<%-- <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+incomeMjrdebitAmt -incomeMnrdebitAmtt-incomeMjrdebitAmttJV) %></td> --%> 
	     <%debitincome=debitincome+debitAmountOpeningBal+incomeMjrdebitAmt + incomeMjrdebitAmtJV-incomeMnrdebitAmtt-incomeMjrdebitAmttJV;
	     
	     System.out.println("majorhead id .........."+MH.getmajor_head_id()+".......amount......"+debitincome);
	     
	     %> 
	 <%--    <%System.out.println("incomeMnrdebitAmtt..."+incomeMnrdebitAmtt); %>
	     <%debitincome=debitincome+debitAmountOpeningBal+incomeMjrdebitAmt -incomeMnrdebitAmtt-incomeMjrdebitAmttJV;%> --%>
		<td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(totcreditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar+otherAmount-otherAmountList11-incomeMjrcreditAmtt-incomeMjrcreditAmttJV) %></td>
	  <%creditincome=creditincome+totcreditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar+otherAmount-otherAmountList11-incomeMjrcreditAmtt-incomeMjrcreditAmttJV;
	  
	  
	  
	 // creditsTotalExp=creditsTotalExp+totcreditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar+otherAmount-otherAmountList11-incomeMjrcreditAmtt-incomeMjrcreditAmttJV;
	  %>
	<%}else{ %>
	      <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+incomeMjrdebitAmt + incomeMjrdebitAmtJV) %></td>  
 	   <%--  <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+incomeMjrdebitAmt) %></td>  --%>
	      
	      
	      
	      
	      <%
	      
	      
	      
	      
	      
	      debitincome=debitincome+debitAmountOpeningBal+incomeMjrdebitAmt + incomeMjrdebitAmtJV;
	      
	      System.out.println("non majorhead id .........."+MH.getmajor_head_id()+".......amount......"+debitincome);
	      
	      %> 
	  <%--  <%debitincome=debitincome+debitAmountOpeningBal+incomeMjrdebitAmt;%> --%> 
 		<td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar+otherAmount) %></td>
  		<%creditincome=creditincome+creditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar+otherAmount; 
  		//creditsTotalExp=creditsTotalExp+creditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar+otherAmount;
  		%>
 	<%} %>
  </tr>
  
  <%
  
	
  minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									double incomeMnrdebitAmt=0.0;
									double incomeMnrdebitAmtJV=0.0;
									double incomeMnrcreditAmt =0.0;
									double incomeMnrcreditAmt1 =0.0;
									double incomeMnrcreditAmtJV =0.0;
									double incomeMnrcreditAmtDollar =0.0;
									double incomeMnrcreditAmtOfferKind =0.0;
									double mindbamt =0.0;
									double mincdamt = 0.0;
									double debitAmountOpeningBal2 = 0.0;
								  	double creditAmountOpeningBal2 = 0.0;
								  	double otherAmount1 = 0.0;
								  	List MinHd1=MINH_L.getMinorHeadBasdOnCompany1(MH.getmajor_head_id(),MNH.getminorhead_id(),"IE");
									String MinHead1[]=new String[MinHd1.size()];
									int p=0;
									for(p=0;p<MinHd1.size();p++){
										MNH=(beans.minorhead)MinHd1.get(p);
									//System.out.println("size..."+subh1.size());
										//System.out.println("report.."+report[i]+"....major_head.."+major_head_id);
										/*  subhd1=subhd1+subh1[m]+","; */
									//System.out.println("mnhead.."+MNH.getminorhead_id());	
									MinHead1[p]=" ";
									//MinHead1[p]=MinHead1[p]+"!MNH.getminorhead_id().equals("+MNH.getminorhead_id()+")"+" && ";
									//System.out.println("minorhead.."+"!MNH.getminorhead_id().equals("+MNH.getminorhead_id()+")"+" && ");
									//System.out.println("minorhd.."+equals(MinHead1));
									String minorvalue='"'+MNH.getminorhead_id()+'"';
									//System.out.println("minorhd.."+!equals(MinHead1));
									//MinHead1.substring(0, MinHead1. - 2);
								//	if(!MNH.getminorhead_id().equals("122") && !MNH.getminorhead_id().equals("469") && !MNH.getminorhead_id().equals("142") && !MNH.getminorhead_id().equals("472")&& !MNH.getminorhead_id().equals("102") && !MNH.getminorhead_id().equals("468")&& !MNH.getminorhead_id().equals("132") && !MNH.getminorhead_id().equals("112") ){
									//System.out.println("value..."+!MNH.getminorhead_id().equals(minorvalue));	
									if(!MNH.getminorhead_id().equals(minorvalue)){
										List subh1=SUBH_L.getSubheadListMinorhead2(MNH.getminorhead_id(),"IE");
										String subhd1[]=new String[subh1.size()];
										int m=0;
										for(m=0;m<subh1.size();m++){
										SUBH=(subhead)subh1.get(m);
										//System.out.println("size..."+subh1.size());
											//System.out.println("report.."+report[i]+"....major_head.."+major_head_id);
											/*  subhd1=subhd1+subh1[m]+","; */
										//System.out.println("subhead.."+SUBH.getsub_head_id());	
										subhd1[m]=SUBH.getsub_head_id();
										
										//System.out.println("subhead.."+subhd1[m]);
										}
										//subhd1[m]=subhd1[m].substring(0,subhd1[m].length()-2);
										/* System.out.println("subhead...1111111..."+subhd1[0]); */
									 incomeMnrdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum1(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd1));
									 incomeMnrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd1));
									 incomeMnrcreditAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead31(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
									 incomeMnrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSum1(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
									 incomeMnrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd1));
									 incomeMnrcreditAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd1));
									 incomeMnrcreditAmtOfferKind = Double.parseDouble(CP_L.getLedgerSumOfSubheads(MNH.getminorhead_id(),"extra1",fdate,tdate,"offerKind",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
										
									 mindbamt =  incomeMnrdebitAmt + incomeMnrdebitAmtJV;
									// mindbamt =  incomeMnrdebitAmt;
								  	 mincdamt = incomeMnrcreditAmt + incomeMnrcreditAmt1+ incomeMnrcreditAmtJV + incomeMnrcreditAmtDollar;
										
								  	/* start */
									List otherAmountList1=BNK_L.getStringOtherDeposit1("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),"",hoaid,fromdate,todate,"sub_head_id",subhd1);
					  				//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
										
									if(otherAmountList1.size()>0)
									{
										banktransactions OtherAmountList1 = (banktransactions)otherAmountList1.get(0);
										if(OtherAmountList1.getamount() != null)
										{
											otherAmount1 = Double.parseDouble(OtherAmountList1.getamount());
										}
									}
									
									

									/* List dollarAmountList1=BNK_L.getStringOtherDeposit("dollar-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),"",hoaid,fromdate,todate);
					  				//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
									double dollarAmount1 = 0.0;
									if(dollarAmountList1.size()>0)
									{	
										banktransactions DollarAmountList1 = (banktransactions)dollarAmountList1.get(0);
										if(DollarAmountList1.getamount() != null)
										{
											dollarAmount1 = Double.parseDouble(DollarAmountList1.getamount());
											//System.out.println("otherAmount1...."+otherAmount1);
										}
								} */
									
									
									
									/* end */
								  	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
									  debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "","extra5",subhd1);
									 /*  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", "","extra5",subhd1); */
									  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "","extra5",subhd1);
									
								  	}
								  //double amount =Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())));
			if((!("0.0".equals(""+mindbamt)) || !("0.0".equals(""+mincdamt)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0 || otherAmount1 != 0.0) && !MNH.getminorhead_id().equals(corpusMinorHead)){  
								%>
  
  <tr>
    <%-- <td height="28" style="color:#f00;"><strong><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Income and Expenditure" style="color:red;"><%=MNH.getname() %><%=MNH.getminorhead_id() %> </a></strong></td> --%>
    <td height="28" style="color:#f00;"><strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong></td>
   <td align="center" style="color:#f00;"><%=df.format(debitAmountOpeningBal2+incomeMnrdebitAmt + incomeMnrdebitAmtJV) %></td> 
     <%-- <td align="center" style="color:#f00;"><%=df.format(debitAmountOpeningBal2+incomeMnrdebitAmt) %></td>  --%>
    <%/* debitincome=debitincome+debitAmountOpeningBal2+incomeMnrdebitAmt + incomeMnrdebitAmtJV;   */
    /*  <%debitincome=debitincome+debitAmountOpeningBal2+incomeMnrdebitAmt ;  */
    if(!String.valueOf(incomeMnrcreditAmt1).equals("0.0")){ %>
    <%-- <td align="center" style="color:#f00;"><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) %></td> --%>
  	<%-- <td align="center" style="color:#f00;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))%></td> --%>
	<td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+incomeMnrcreditAmtOfferKind+otherAmount1)%></td>
  <%} else{ %>
  <%-- <td align="center" style="color:#f00;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))+df.format(Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td> --%>
  <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+incomeMnrcreditAmt+incomeMnrcreditAmt1 +incomeMnrcreditAmtJV  +incomeMnrcreditAmtDollar +otherAmount1) %></td>
  <%//System.out.println("creditAmountOpeningBal2..."+df.format(creditAmountOpeningBal2)+"...incomeMnrcreditAmt..."+df.format(incomeMnrcreditAmt)+"...incomeMnrcreditAmt1..."+df.format(incomeMnrcreditAmt1)+"..incomeMnrcreditAmtJV.."+df.format(incomeMnrcreditAmtJV)+"....incomeMnrcreditAmtDollar..."+df.format(incomeMnrcreditAmtDollar)+"...otherAmount1...."+df.format(otherAmount1));
  //System.out.println("minor head..."+MNH.getminorhead_id()+"...creditAmountOpeningBal2.."+df.format(creditAmountOpeningBal2));
  } %>	
  </tr>
  <%
				List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
          // List subhdlist=SUBH_L.getSubheadListMinorhead1(MNH.getminorhead_id(),"IE"); 
         String subhd="";
         if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									 subhd=SUBH_L.getSubheadListMinorhead1(MNH.getminorhead_id(),SUBH.getsub_head_id(),"IE");;
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									//else
										//cashtype="cash";
										 //cashtype1="othercash";
										 //cashtype2="offerKind";
										//cashtype3="journalvoucher";
										//double amount1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));
										//double amount1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));
										
										if(SUBH.getsub_head_id().equals("20020") || SUBH.getsub_head_id().equals("20022") || SUBH.getsub_head_id().equals("20023") || SUBH.getsub_head_id().equals("20025") || SUBH.getsub_head_id().equals("20024")){
											
										}//if ends
										else{
										double incomeSubdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum21(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd));
										double incomeSubdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd));
										double incomeSubcreditAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
										double incomeSubcreditAmt1 = Double.parseDouble(CP_L.getLedgerSum1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
										double incomeSubcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
										double incomeSubcreditAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd));
										
										  double subdbamt = incomeSubdebitAmt +incomeSubdebitAmtJV ;  
										//double subdbamt = incomeSubdebitAmt  ;
										double subcdamt = incomeSubcreditAmt+incomeSubcreditAmt1 + incomeSubcreditAmtJV  + incomeSubcreditAmtDollar;
										double debitAmountOpeningBal3 = 0.0;
										double creditAmountOpeningBal3 = 0.0;
										/* start */
										List otherAmountList2=BNK_L.getStringOtherDeposit1("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(),hoaid,fromdate,todate,"sub_head_id",subhd);
			 								//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
										double otherAmount2 = 0.0;
										if(otherAmountList2.size()>0)
										{
											banktransactions OtherAmountList2 = (banktransactions)otherAmountList2.get(0);
											if(OtherAmountList2.getamount() != null)
											{
												otherAmount2 = Double.parseDouble(OtherAmountList2.getamount());
											}
										}
										
									/* 	List dollarAmountList2=BNK_L.getStringOtherDeposit("dollar-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(),hoaid,fromdate,todate);
		 								//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
									double dollarAmount2 = 0.0;
									if(dollarAmountList2.size()>0)
									{
										banktransactions DollarAmountList2 = (banktransactions)dollarAmountList2.get(0);
										if(DollarAmountList2.getamount() != null)
										{
											dollarAmount2 = Double.parseDouble(DollarAmountList2.getamount());
										}
									} */
										
										
										
										
										
									/* end */
										if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
											debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "","extra5",subhd);
											/* creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", "","extra5",subhd); */
											creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "","extra5",subhd);
										}
										if(!("0.0".equals(""+subdbamt)) || !("0.0".equals(""+subcdamt)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0 || otherAmount2 != 0.0){  
								%>	
								
								
  
  <tr>
  <%if(!String.valueOf(incomeSubcreditAmt1).equals("0.0")){ %>
    <%-- <td height="28"><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></td> --%>
    <td height="28"><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></td>
    <%} else{ %> 
    <%-- <td height="28"><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></td> --%>
    <td height="28"><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></td>
     <%} %>
    <td><%=df.format(debitAmountOpeningBal3+incomeSubdebitAmt +incomeSubdebitAmtJV ) %></td>  
   <%--   <td><%=df.format(debitAmountOpeningBal3+incomeSubdebitAmt ) %></td> --%> 
      <%debit=debit+debitAmountOpeningBal3+incomeSubdebitAmt +incomeSubdebitAmtJV ;  
  /*    <%debit=debit+debitAmountOpeningBal3+incomeSubdebitAmt ;  */
     if(!String.valueOf(incomeSubcreditAmt1).equals("0.0")){ 
    // System.out.println("111111111111111111111111111111111111111111111111111111111111111111111111");%>
    <%-- <td><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) %></td> --%>
    <td><%=df.format(creditAmountOpeningBal3+incomeSubcreditAmt+otherAmount2) %></td>
    <%-- <%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));} else{ %> --%>
    <%credit=credit+creditAmountOpeningBal3+incomeSubcreditAmt+otherAmount2;} else{ 
  //  System.out.println("222222222222222222222222222222222222222222222222222222222222222222222222");%>
    <td><%=df.format(creditAmountOpeningBal3+incomeSubcreditAmt+incomeSubcreditAmt1 +incomeSubcreditAmtJV +incomeSubcreditAmtDollar +otherAmount2) %></td>
  <%credit=credit+creditAmountOpeningBal3+incomeSubcreditAmt+incomeSubcreditAmt1 +incomeSubcreditAmtJV + incomeSubcreditAmtDollar+otherAmount2;} %>
  </tr>
  <%}}}}}}}}}}}} %>
  
  
   <!-- <tr>
    <td height="28">Total(Rupees)</td>
    <td align="center">0.00</td>
    <td align="center">0.00</td>
  </tr> -->
  
  
 <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">EXPENDITURE</td>
  	</tr>
    
    <% if(expendmajhdlist.size()>0){ 
								for(int i=0;i<expendmajhdlist.size();i++){
									MH=(majorhead)expendmajhdlist.get(i);
									List subh2=SUBH_L.getSubheadListMinorhead3(MH.getmajor_head_id(),"IE");
									String subhd2[]=new String[subh2.size()];
									int n=0;
									for(n=0;n<subh2.size();n++){
									SUBH=(subhead)subh2.get(n);
									//System.out.println("size..."+subh2.size());
										//System.out.println("report.."+report[i]+"....major_head.."+major_head_id);
										/*  subhd1=subhd1+subh1[m]+","; */
									//System.out.println("subhead.."+SUBH.getsub_head_id());	
									subhd2[n]=SUBH.getsub_head_id();
									
									//System.out.println("subhead.."+subhd1[m]);
									}
									
									double debitAmountOpeningBal = 0.0;
									double creditAmountOpeningBal = 0.0;
									double expMjrdebitAmt=0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "","extra5",subhd2);
										/* creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", "","extra5",subhd2); */
										creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Liabilites", finYr, "credit", "","extra5",subhd2);
									}
									/* if(MH.getmajor_head_id().equals("31")){
									expMjrdebitAmt = Double.parseDouble(PEXP_L.getLedgerSum01(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2));	
									}
									else{ */
									expMjrdebitAmt = Double.parseDouble(PEXP_L.getLedgerSum1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2));
									/* } */
									double expMjrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2));
									double expMjrcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd2));
									double expMjrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead1b(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,hoaid,"sub_head_id",subhd2));
									double expMjrcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead111(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype3,"","",hoaid,"sub_head_id",subhd2));
									double expMjrcreditAmt3	= Double.parseDouble(CP_L.getLedgerSumOfSubheads(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd2));
									double expMjrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2));
									
									if(!String.valueOf(expMjrdebitAmt).equals("0.0") || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){
									
								%>
     <tr>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getmajor_head_id()%> <%=MH.getname() %> (<%=MH.getextra2() %>)</a></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+expMjrdebitAmt +expMjrdebitAmtJV ) %></td>
   <%debitexpens=debitexpens+debitAmountOpeningBal+expMjrdebitAmt + expMjrdebitAmtJV;%>
     <%if(!String.valueOf(expMjrcreditAmt).equals("0.0")){ %>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+expMjrcreditAmt+expMjrcreditAmt1) %></td>
    <%creditexpens=creditexpens+creditAmountOpeningBal+expMjrcreditAmt+expMjrcreditAmt1;
    // creditsTotalExp=creditsTotalExp+creditAmountOpeningBal+expMjrcreditAmt+expMjrcreditAmt1;
     
     } else{ %>
   <%-- <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td> --%>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+expMjrcreditAmt2 +expMjrcreditAmtJV )%></td>
 <%creditexpens=creditexpens+creditAmountOpeningBal+expMjrcreditAmt2 +expMjrcreditAmtJV  ; 
 //creditsTotalExp=creditsTotalExp+creditAmountOpeningBal+expMjrcreditAmt2 +expMjrcreditAmtJV ;	
 
    } %>
  </tr>
  
  <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
  //System.out.println("minor head.."+minhdlist);
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									List MinHd1=MINH_L.getMinorHeadBasdOnCompany1(MH.getmajor_head_id(),MNH.getminorhead_id(),"IE");
									String MinHead1[]=new String[MinHd1.size()];
									int p=0;
									for(p=0;p<MinHd1.size();p++){
										MNH=(beans.minorhead)MinHd1.get(p);
									//System.out.println("size..."+subh1.size());
										//System.out.println("report.."+report[i]+"....major_head.."+major_head_id);
										/*  subhd1=subhd1+subh1[m]+","; */
									//System.out.println("mnhead.."+MNH.getminorhead_id());	
									MinHead1[p]=" ";
									//MinHead1[p]=MinHead1[p]+"!MNH.getminorhead_id().equals("+MNH.getminorhead_id()+")"+" && ";
									//System.out.println("minorhead.."+"!MNH.getminorhead_id().equals("+MNH.getminorhead_id()+")"+" && ");
									//System.out.println("minorhd.."+equals(MinHead1));
									String minorvalue='"'+MNH.getminorhead_id()+'"';
									//System.out.println("minorhd.."+!equals(MinHead1));
									//MinHead1.substring(0, MinHead1. - 2);
								//	if(!MNH.getminorhead_id().equals("122") && !MNH.getminorhead_id().equals("469") && !MNH.getminorhead_id().equals("142") && !MNH.getminorhead_id().equals("472")&& !MNH.getminorhead_id().equals("102") && !MNH.getminorhead_id().equals("468")&& !MNH.getminorhead_id().equals("132") && !MNH.getminorhead_id().equals("112") ){
									//System.out.println("value..."+!MNH.getminorhead_id().equals(minorvalue));	
									if(!MNH.getminorhead_id().equals(minorvalue)){
									List subh1=SUBH_L.getSubheadListMinorhead2(MNH.getminorhead_id(),"IE");
									String subhd1[]=new String[subh1.size()];
									int m=0;
									for(m=0;m<subh1.size();m++){
									SUBH=(subhead)subh1.get(m);
									//System.out.println("size..."+subh1.size());
										//System.out.println("report.."+report[i]+"....major_head.."+major_head_id);
										/*  subhd1=subhd1+subh1[m]+","; */
									//System.out.println("subhead.."+SUBH.getsub_head_id());	
									subhd1[m]=SUBH.getsub_head_id();
									
									//System.out.println("subhead.."+subhd1[m]);
									}
									//System.out.println("minor head.."+MNH.getminorhead_id());
									/* if(!MNH.getminorhead_id().equals("281")&&!MNH.getminorhead_id().equals("207")&&!MNH.getminorhead_id().equals("404")){ */
									double expMnrdebitAmt = Double.parseDouble(PEXP_L.getLedgerSum1(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd1));
									double expMnrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd1));
									double expMnrdebitAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead41(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd1));
									double expMnrdebitAmt2 = Double.parseDouble(CP_L.getLedgerSum1(MNH.getminorhead_id(),"extra3",fdate,tdate,"",hoaid,"sub_head_id",subhd1));
									double expMnrcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
									double expMnrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubheads(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
									double expMnrcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead31(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,"","",cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
									double expMnrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd1));
									double expMnrdebitAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd1));
									
									String amountexp=String.valueOf(expMnrdebitAmt);
									//double amountexp = expMnrdebitAmt+expMnrdebitAmtJV;
									double amount2 = expMnrdebitAmt1+expMnrdebitAmt2 + expMnrcreditAmtJV +expMnrdebitAmtDollar ; 
									double debitAmountOpeningBal2 = 0.0;
									double creditAmountOpeningBal2 = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "","extra5",subhd1);
										/* creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", "","extra5",subhd1); */
										creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "","extra5",subhd1);
									}
									//System.out.println("minor head.."+MNH.getminorhead_id());
									if(!("0.0".equals(""+amountexp)) || !("0.0".equals(""+amount2)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){
								%>
    <tr>
    <%-- <td height="28" style="color:#f00;"><strong><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Income and Expenditure" style="color:red;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></strong></td> --%>
    <td height="28" style="color:#f00;"><strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong></td>
    <td align="center" style="color:#f00;"><%=df.format(debitAmountOpeningBal2+expMnrdebitAmt +expMnrdebitAmtJV ) %></td>
    <%debit=debit+debitAmountOpeningBal2+expMnrdebitAmt +expMnrdebitAmtJV ;
      if(!String.valueOf(expMnrcreditAmt).equals("0.0")){ %>
    <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+expMnrcreditAmt1+expMnrcreditAmt) %></td>
     <%} else{ %>
     <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+expMnrcreditAmt2+expMnrcreditAmt +expMnrcreditAmtJV) %></td>
     <%} %>
  </tr>
  <%
				List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
  				String subhd="";
         if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									subhd=SUBH_L.getSubheadListMinorhead1(MNH.getminorhead_id(),SUBH.getsub_head_id(),"IE");;
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										 cashtype1="othercash";
										 cashtype2="offerKind";
										cashtype3="journalvoucher";
										double expSubdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum21(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd));
										double expSubdebitAmt1 = Double.parseDouble(CP_L.getLedgerSum1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd));
										double expSubdebitAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
										double expSubdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd));
										double expSubcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
										double expSubcreditAmtJV1 =	Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
										double expSubcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
										double expSubcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
										double expSubcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
										
										String amountexp2=String.valueOf(expSubdebitAmt);
										//double amountexp2= expSubdebitAmt+expSubdebitAmtJV;
										String amount3 = String.valueOf(expSubdebitAmt1);
										String amount4 = String.valueOf(expSubdebitAmt2);
										String amount5= String.valueOf(expSubcreditAmtJV) ;
										double debitAmountOpeningBal3 = 0.0;
										double creditAmountOpeningBal3 = 0.0;
										if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
											debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "","extra5",subhd);
											/* creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", "","extra5",subhd); */
											creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "","extra5",subhd);
										}
										//System.out.println("1.."+amountexp2+"...2...."+amount3+"...3...."+amount4+"...4..."+amount5);
										 if(!("0.0".equals(""+amountexp2)) || !("0.0".equals(""+amount3)) || !("0.0".equals(""+amount4)) || !("0.0".equals(""+amount5)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){  
								//System.out.println("1.."+amountexp2+"...2...."+amount3+"...3...."+amount4+"...4..."+amount5); 
								%>	
  
  
  
   <tr>
   <%
   if(!String.valueOf(expSubcreditAmt).equals("0.0")){ %>
    <%-- <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td> --%>
    <td height="28"><strong><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></strong></td>
     <%} else{ %>
     <%-- <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td> --%>
     <td height="28"><strong><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></strong></td>
   <%} %>
    <td><%=df.format(debitAmountOpeningBal3+expSubdebitAmt +expSubdebitAmtJV ) %></td>
    <%debit=debit+debitAmountOpeningBal3+expSubdebitAmt +expSubdebitAmtJV ;
    if(!String.valueOf(expSubcreditAmt).equals("0.0")){ %>
    <td><%=df.format(creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt) %></td>
     <%credit=credit+creditAmountOpeningBal3+expSubcreditAmt2+expSubcreditAmt;} else{ %>
     <td><%=df.format(creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt+expSubcreditAmtJV1) %></td>
  <%credit=credit+creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt +expSubcreditAmtJV1;} 
  %>
  </tr>
  
 <%}}}}}}}}}}}
									
									
 double totalDebitAmount = debitincome+debitexpens;
 double totalCreditAmount = creditincome+creditexpens; 				
									
									%>
  
   

  <tr>
    <td align="center" style=" color: #934C1E; font-weight:bold;" height="28">TOTAL AMOUNT OF EXPENS </td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">Rs.<%=df.format(totalDebitAmount) %></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
  </tr>
 <!--  <tr>
    <td height="28"></td>
    <td align="center">0.00</td>
    <td align="center">0.00</td>
  </tr> -->
   <%System.out.println("credit==>"+df.format(credit));
System.out.println("debit===>"+df.format(debit));
System.out.println("creditincome==>"+df.format(creditincome));
System.out.println("creditexpens==>"+df.format(creditexpens));
System.out.println("debitincome==>"+df.format(debitincome));
System.out.println("debitexpens==>"+df.format(debitexpens));
 %> 
  
   <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Excess of Income over Expenditures</td>
    <%
    
    
    
    if(creditincome<debitexpens){%>
    <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
   <%--  <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitexpens-creditincome)%></td> --%>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(totalDebitAmount-totalCreditAmount)%></td>
   
   
    <%	/* creditincome=debitexpens; */
} else{	%>
<td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(totalCreditAmount-totalDebitAmount)%></td>
<td align="center" style="font-weight:bold; color: #934C1E;">-</td>
<%/* debitexpens=creditincome; */%>
<%} %>
  </tr>
  
   <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
  <%--  <%if(creditincome>debitexpens){	%>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome) %></td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome) %></td><%} else{%>
     <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitexpens) %></td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitexpens) %></td><%} %> --%><%//commenteed by sagar%>
    
    
     <%if(creditincome>debitexpens){	%>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(totalCreditAmount) %></td>
     <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(totalCreditAmount) %></td>
    <%} else{%>
     <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(totalDebitAmount) %></td>
     <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(totalDebitAmount) %></td>
    <%} %>
   
    
    
    
    
    
    
  </tr>
  <!--  <tr>
    <td height="28" colspan="3"></td>
  </tr> -->
  
  <!-- <tr>
    <td height="28"><strong>Total</strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr> -->


  
</tbody></table>
</div><%} %>
  </div>
			</div>
				
			</div>
			</div>
<%}
else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>