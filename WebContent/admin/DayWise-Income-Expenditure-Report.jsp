<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAY WISE INCOME AND EXPENDITURE Report</title>

<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});	
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 

</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>

<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<jsp:useBean id="BK" class="beans.bookingrooms"/>
<jsp:useBean id="LG" class="beans.logins"></jsp:useBean>
<jsp:useBean id="BKR" class="beans.booking_roomsdetails"></jsp:useBean>
<jsp:useBean id="CAN" class="beans.cancellation_details"></jsp:useBean>
<jsp:useBean id="CU" class="beans.counter_balance_update"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">


<div style="text-align: center;"></div>
<div class="icons">
<span> <a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="total-report">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing(); %>
<div class="printable" >
<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td><table width="100%" cellpadding="0" cellspacing="0" class="date-wise "  >
<tr><td colspan="4" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;" class="bg-new">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;" class="bg-new">Day Wise-Income And Expenditure Report</td></tr>
<form action="adminPannel.jsp" method="post">
<input type="hidden" name="page" value="DayWise-Income-Expenditure-Report"></input>
<tr>
<%
// 		SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
// 		Calendar calld = Calendar.getInstance();
// 		calld.add(Calendar.DATE,0);
// 		String todayDate=SDF.format(calld.getTime());
// 		String date[] = todayDate.split("-");
// 		 String currentyear = date[0];// 2017
// 		 String currentyear1 = currentyear.replace(currentyear.substring(2),""); // 20 
// 		 String currentyear2 = currentyear.replace(currentyear.substring(0,2),"");// 17
// 		 String year2 ="";
// 		 String year22 = "";	 
// 		 int currentyearinint = Integer.parseInt(currentyear2);	 
// 		 if(todayDate.compareTo(currentyear+"-04-01") >=0){
// 			 year2 =String.valueOf(currentyearinint+1);// 18
			 %>	 
			<td width="20%">
			Fin Yr 
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
						<%@ include file="yearDropDown1.jsp" %>
					</select>
				  </td>
			


<%-- 
<td width="20%">
	Fin Yr 
	<select name="finYear" id="finYear"  Onchange="datepickerchange();">
	<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
	<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
	</select> --%>
<!-- <ul> -->
<%List HA_Lists=HOA_L.getheadofaccounts();
SimpleDateFormat cdf = new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}
/* if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); */ %>
<%-- <li><input type="radio" name="hoid" value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> checked="checked" <%} %> /> <%=HOA.getname() %></li><%}} %> --%>
<!-- <li></li>
</ul></td> -->
<td width="30%"><input type="hidden" name="page" value="totalsalereport"/>From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="30%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" value="Submit" class="click bordernone"/></td>
</tr>
</form>
<!-- <form action="adminPannel.jsp" method="post">
<input type="hidden" name="page" value="DayWise-Income-Expenditure-Report"></input>
<tr>
<td><input type="submit" value="Today" class="click bordernone"/></td>
<td><input type="submit" value="Current Week" class="click bordernone"/></td>
<td><input type="submit" value="Current Month" class="click bordernone"/></td>
<td><input type="submit" value="Current Year" class="click bordernone"/></td>
</tr>
</form> -->
</table></td></tr></table>
<%customerpurchasesListing CUSPL=new customerpurchasesListing();
majorheadListing MHL = new majorheadListing();
productexpensesListing PEXPL = new productexpensesListing();
banktransactionsListing BNKTL = new banktransactionsListing();

mainClasses.loginsListing LOG_CL=new mainClasses.loginsListing();
mainClasses.bookingroomsListing BUK_CL=new mainClasses.bookingroomsListing();
mainClasses.counter_balance_updateListing COUNT_UP=new mainClasses.counter_balance_updateListing();

	if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("")){
		bankbalanceListing BBAL_L=new mainClasses.bankbalanceListing();
		String finStartYr = request.getParameter("finYear").substring(0, 4);		
%>
<div id="tblExport">
<div style="float:left; width:100%; margin:0 0px 10px 0" >

<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr>
<td colspan="1" class="bg-new"></td>
<td colspan="7" class="bg-new" style="font-weight: bold;color: teal;">INCOME</td>
<td colspan="9" class="bg-new" style="font-weight: bold;color: teal;">EXPENDITURE</td>
</tr>
<tr>
<td class="bg-new" style="font-size:14px;">DATE</td>
<!-- <td class="bg-new" style="width: 10%;">SANSTHAN</td> -->
<td class="bg-new" style="font-size:14px;">NON-DEV</td>
<td class="bg-new" style="font-size:14px;">DEV</td>
<td class="bg-new" style="font-size:14px;">CHARITY</td>
<td class="bg-new" style="font-size:14px;">SHRI SAINIVAS DSNR</td>
<td class="bg-new" style="font-size:14px;">SHRI SAINIVAS SHIRDI</td>
<td class="bg-new" style="font-size:14px;">SHRI SAINIVAS ONLINE</td>
<td class="bg-new" style="font-size:14px;">POOJA STORES</td>
<!-- <td class="bg-new" style="width: 10%;">SANSTHAN</td> -->
<td class="bg-new" style="font-size:14px;">PETTY CASH NON-DEV</td>
<td class="bg-new" style="font-size:14px;">NON-DEV</td>
<td class="bg-new" style="font-size:14px;">DEV</td>
<td class="bg-new" style="font-size:14px;">PETTY CASH CHARITY</td>
<td class="bg-new" style="font-size:14px;">CHARITY</td>
<td class="bg-new" style="font-size:14px;">PETTY CASH SAINIVAS</td>
<td class="bg-new" style="font-size:14px;">SHRI SAINIVAS</td>
<td class="bg-new" style="font-size:14px;">PETTY CASH POOJA STORES</td>
<td class="bg-new" style="font-size:14px;">POOJA STORES</td>
</tr>
<%
DateFormatSymbols dfs = new DateFormatSymbols();
DecimalFormat DF=new DecimalFormat("0.00");
productexpensesListing PROEXPL=new productexpensesListing();
String[] months = dfs.getMonths();
double sansthanTot=0.00;
double devTot=0.00;
double nonDevTot=0.00;
double devTotExp=0.00;
double nonDevTotExp=0.00;
double charityTot=0.00;
double psTot=0.00;
double sainivasdsnrTot=0.00;
double sainivasshirdiTot=0.00;
double sainivasonlineTot=0.00;
double expsansthanTot=0.00;
double expcharityTot=0.00;
double exppsTot=0.00;
double expsainivasTot=0.00;
double otherCharges=0.00;
String dt=fromdate;
Calendar c = Calendar.getInstance();
c.setTime(sdf.parse(dt));
int month = c.get(Calendar.MONTH);
int month1=month;
Date fdate;
Date tdate;
fdate = sdf.parse(dt);
tdate=sdf.parse(todate);

double pettyCashNonDevTotal = 0.0;
double pettyCashCharityTotal = 0.0;
double pettyCashSainivasTotal = 0.0;
double pettyCashPoojastoresTotal = 0.0;
double pettyCashNonDevTotal1=0.0;


String banksNew[] = new String[6];
banksNew[0] = "BNK1013";
banksNew[1] = "BNK1012";
banksNew[2] = "BNK1014";
banksNew[3] = "BNK1015";
banksNew[4] = "BNK1027";
banksNew[5] = "BNK1029";



while(fdate.before(tdate) || fdate.equals(tdate))
{
customerpurchases CP = new customerpurchases();
productexpenses PEXP = new productexpenses();
%>
<tr>
<td><%=(cdf.format(sdf.parse(dt)))%></td>
<%
double dev = 0.0;
double nonDev = 0.0;

String expenditre[] = new String[5];

if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
		HOA=(beans.headofaccounts)HA_Lists.get(i);
		if(HOA.gethead_account_id().equals("1")){
			charityTot=charityTot+CUSPL.getDayWiseIncome(HOA.gethead_account_id(),dt);
		}if(HOA.gethead_account_id().equals("3")){
			psTot=psTot+CUSPL.getDayWiseIncome(HOA.gethead_account_id(),dt);
		}if(HOA.gethead_account_id().equals("4")){
			/* sansthanTot=sansthanTot+CUSPL.getDayWiseIncome(HOA.gethead_account_id(),dt); */
			List list = CUSPL.getcustomerpurchasesListBasedOnDateAndHoa(HOA.gethead_account_id(),dt);
			for(int z = 0; z < list.size(); z++)
			{
				CP = (customerpurchases)list.get(z);
				String headgroup = MHL.getheadgroup(CP.getproductId(),CP.getextra1());
				if(headgroup.equals("1"))
				{
					nonDev += Double.parseDouble(CP.gettotalAmmount());
					nonDevTot += Double.parseDouble(CP.gettotalAmmount());
				}
				else if(headgroup.equals("2"))
				{
					dev += Double.parseDouble(CP.gettotalAmmount());
					devTot += Double.parseDouble(CP.gettotalAmmount());
				}
			}
		} if(HOA.gethead_account_id().equals("5")){
			 sainivasdsnrTot=sainivasdsnrTot+BUK_CL.getDayOnlineBookingsCash("dsnradmin",dt);
			 sainivasshirdiTot=sainivasshirdiTot+BUK_CL.getDayOnlineBookingsCash("receptionist",dt);
			 sainivasonlineTot=sainivasonlineTot+BUK_CL.getDayOnlineBookings(dt);
			
		} 
	}
	%>
	<%-- <td style="text-align:right"><%=DF.format(CUSPL.getDayWiseIncome("4",dt)) %></td> --%>
	<td style="text-align:right"><%=nonDev%></td>
	<td style="text-align:right"><%=dev%></td>
	<td style="text-align:right"><%=DF.format(CUSPL.getDayWiseIncome("1",dt)) %></td>
	<td style="text-align:right"><%=DF.format(BUK_CL.getDayOnlineBookingsCash("dsnradmin",dt)) %></td>
	<td style="text-align:right"><%=DF.format(BUK_CL.getDayOnlineBookingsCash("receptionist",dt)) %></td>
	<td style="text-align:right"><%=DF.format(BUK_CL.getDayOnlineBookings(dt)) %></td>
	<td style="text-align:right"><%=DF.format(CUSPL.getDayWiseIncome("3",dt)) %></td> 
	<%} %>


<%if(HA_Lists.size()>0){
	double devExp = 0.0;
	double nonDevExp = 0.0;
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);
	if(HOA.gethead_account_id().equals("1")){
		expcharityTot=expcharityTot+PROEXPL.getExpensesAmt(HOA.gethead_account_id(),dt)+PROEXPL.getOtherChargesTotAmt(HOA.gethead_account_id(),dt);
	}if(HOA.gethead_account_id().equals("3")){
		exppsTot=exppsTot+PROEXPL.getExpensesAmt(HOA.gethead_account_id(),dt)+PROEXPL.getOtherChargesTotAmt(HOA.gethead_account_id(),dt);
	}if(HOA.gethead_account_id().equals("4")){
		/* expsansthanTot=expsansthanTot+PROEXPL.getExpensesAmt(HOA.gethead_account_id(),dt)+PROEXPL.getOtherChargesTotAmt(HOA.gethead_account_id(),dt); */
		List list = PEXPL.getExpensesAmtList(HOA.gethead_account_id(),dt);
		for(int z = 0; z < list.size(); z++)
		{
			PEXP = (productexpenses)list.get(z);
			String headgroup = MHL.getheadgroup2(PEXP.getmajor_head_id(),PEXP.gethead_account_id());
			if(headgroup.equals("1"))
			{
				nonDevExp += Double.parseDouble(PEXP.getamount());
				nonDevTotExp += Double.parseDouble(PEXP.getamount());
				
				nonDevExp += PEXPL.getOtherChargesTotAmt2(PEXP.getextra5());
				nonDevTotExp += PEXPL.getOtherChargesTotAmt2(PEXP.getextra5());
			}
			else if(headgroup.equals("2"))
			{
				devExp += Double.parseDouble(PEXP.getamount());
				devTotExp += Double.parseDouble(PEXP.getamount());
				
				devExp += PEXPL.getOtherChargesTotAmt2(PEXP.getextra5());
				devTotExp += PEXPL.getOtherChargesTotAmt2(PEXP.getextra5());
			}
		}
		
	}if(HOA.gethead_account_id().equals("5")){
		 expsainivasTot=expsainivasTot+PROEXPL.getExpensesAmt(HOA.gethead_account_id(),dt)+PROEXPL.getOtherChargesTotAmt(HOA.gethead_account_id(),dt);
	}
   }

		expenditre[0] = nonDevExp+"";
		expenditre[1] = devExp+"";
		expenditre[2] = DF.format(PROEXPL.getExpensesAmt("1",dt)+PROEXPL.getOtherChargesTotAmt("1",dt))+"";
		expenditre[3] = DF.format(PROEXPL.getExpensesAmt("5",dt)+PROEXPL.getOtherChargesTotAmt("5",dt))+"";
		expenditre[4] = DF.format(PROEXPL.getExpensesAmt("3",dt)+PROEXPL.getOtherChargesTotAmt("3",dt))+"";
} 

double pettyCashNonDev = 0.0;
double pettyCashCharity = 0.0;
double pettyCashSainivas = 0.0;
double pettyCashPoojastores = 0.0;

for (String bankId : banksNew)
{
	if(bankId.equals("BNK1013"))
	{
		pettyCashNonDev = BNKTL.getPettyCashTransactions2(bankId,dt);
		pettyCashNonDevTotal += pettyCashNonDev;
	}
	else if(bankId.equals("BNK1012"))
	{
		pettyCashCharity = BNKTL.getPettyCashTransactions2(bankId,dt);
		pettyCashCharityTotal += pettyCashCharity;
	}
	else if(bankId.equals("BNK1014"))
	{
		pettyCashSainivas = BNKTL.getPettyCashTransactions2(bankId,dt);
		pettyCashSainivasTotal += pettyCashSainivas;
	}
	else if(bankId.equals("BNK1015"))
	{
		pettyCashPoojastores = BNKTL.getPettyCashTransactions2(bankId,dt);
		pettyCashPoojastoresTotal += pettyCashPoojastores;
	}
	else if(bankId.equals("BNK1027"))
	{
		pettyCashNonDev = BNKTL.getPettyCashTransactions2(bankId,dt);
		pettyCashNonDevTotal += pettyCashNonDev;
	}
	 else if(bankId.equals("BNK1029")){
		
		 pettyCashCharity = BNKTL.getPettyCashTransactions2(bankId,dt);
		 pettyCashCharityTotal += pettyCashCharity;
	} 
}
	 %>
<td style="text-align:right"><%=pettyCashNonDev%></td>
<td style="text-align:right"><%=expenditre[0]%></td>
<td style="text-align:right"><%=expenditre[1]%></td>
<td style="text-align:right"><%=pettyCashCharity%></td>
<td style="text-align:right"><%=expenditre[2]%></td>
<td style="text-align:right"><%=pettyCashSainivas%></td>
<td style="text-align:right"><%=expenditre[3]%></td>
<td style="text-align:right"><%=pettyCashPoojastores%></td>	 
<td style="text-align:right"><%=expenditre[4]%></td>
</tr>
<%
c.setTime(sdf.parse(dt));
c.add(Calendar.DATE, 1);  // number of days to add
dt = sdf.format(c.getTime());
c.setTime(sdf.parse(dt));
month = c.get(Calendar.MONTH);
fdate = sdf.parse(dt);
} %>
<tr>
<td colspan="1"  style="text-align:right"><span>Total</span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(nonDevTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(devTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(charityTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(sainivasdsnrTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(sainivasshirdiTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(sainivasonlineTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(psTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(pettyCashNonDevTotal) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(nonDevTotExp) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(devTotExp) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(pettyCashCharityTotal) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(expcharityTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(pettyCashSainivasTotal) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(expsainivasTot) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(pettyCashPoojastoresTotal) %></td>
<td class="total-amt" style="text-align:right"><%=DF.format(exppsTot) %></td>
</tr>
</table>  
</div><table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="7" class="bg-new" style="font-weight: bold;color: red;">BANK PARTICULARS</td></tr>
<tr>
<td class="bg-new" style="text-align:center;" width="24%" >BANKS</td>
<!-- <td style="text-align:center;font-size: 14px;" width="13%" class="bg-new">SANSTHAN</td> -->
<td style="text-align:center;font-size: 14px;" width="13%" class="bg-new">NON-DEVELOPMENT</td>
<td style="text-align:center;font-size: 14px;" width="13%" class="bg-new">DEVELOPMENT</td>
<td style="text-align:center;font-size: 14px;" width="13%" class="bg-new">CHARITY</td> 
<td style="text-align:center;font-size: 14px;" width="13%" class="bg-new">SAI NIVAS</td>
<td style="text-align:center;font-size: 14px;" width="13%" class="bg-new">POOJA STORES</td>
<td style="text-align:center;font-size: 14px;" width="13%" class="bg-new">GRAND TOTAL</td>
</tr>
 <%
 DecimalFormat df=new DecimalFormat("#,###.00"); 
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat dateFormat3= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
Calendar cal = Calendar.getInstance();
Date gameDate = new SimpleDateFormat("yyyy-MM-dd").parse(todate);
cal.setTime(gameDate);
cal.add(Calendar.DATE,1);
//String NextDay=dateFormat2.format(cal.getTime());
String NextDay=todate+" 23:59:59";
//NextDay=dateFormat3.format(dateFormat3.parse(NextDay));
Double ClosingBal=0.00;
mainClasses.bankdetailsListing BNKL=new  mainClasses.bankdetailsListing();
mainClasses.banktransactionsListing BAK_L=new mainClasses.banktransactionsListing();
headofaccountsListing HOAL=new headofaccountsListing();
double bankTotBal=0.00;
double pettyCashTotBal=0.00;
double BTotBal=0.00;
double SANSTHANTOTAL=0.00;
double SANSTHANNONDEV=0.00;
double SANSTHANDEV=0.00;
double CHARITYTOTAL=0.00;
double SAINIVASTOTAL=0.00;
double POOJATOTAL=0.00;
double sbttotal = 0.00;
double andhrabnktotal = 0.00;

double sbt[] = new double[5];
double andhrabnk[] = new double[5];

List BKDET=BNKL.getbankdetailsBasedOnType();
if(BKDET.size()>0){
	for(int i=0;i<BKDET.size();i++){
		BNK=(beans.bankdetails)BKDET.get(i);
		/* if(BAK_L.getBalanceByDeposit(BNK.getbank_id(),NextDay,"deposit")!=null && !BAK_L.getBalanceByDeposit(BNK.getbank_id(),NextDay,"deposit").equals("")){
			bankTotBal=Double.parseDouble(BAK_L.getBalanceByDeposit(BNK.getbank_id(),NextDay,"deposit"));
		} */
		if(BAK_L.getBalanceByDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"deposit")!=null && !BAK_L.getBalanceByDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"deposit").equals("")){
			bankTotBal=Double.parseDouble(BAK_L.getBalanceByDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"deposit"));
		}
		else{
			bankTotBal=0;
		}
		/* if(BAK_L.getBalanceByNOTDeposit(BNK.getbank_id(),NextDay,"cheque","transfer","pettycash","withdraw")!=null && !BAK_L.getBalanceByNOTDeposit(BNK.getbank_id(),NextDay,"cheque","transfer","pettycash","withdraw").equals("")){
			pettyCashTotBal=Double.parseDouble(BAK_L.getBalanceByNOTDeposit(BNK.getbank_id(),NextDay,"cheque","transfer","pettycash","withdraw"));
		} */
		/* if(BAK_L.getBalanceByNOTDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"cheque","transfer","pettycash","withdraw")!=null && !BAK_L.getBalanceByNOTDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"cheque","transfer","pettycash","withdraw").equals("")){
			pettyCashTotBal=Double.parseDouble(BAK_L.getBalanceByNOTDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"cheque","transfer","pettycash","withdraw"));
		} */
		if(BAK_L.getBalanceByNOTDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"cheque","transfer","","withdraw")!=null && !BAK_L.getBalanceByNOTDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"cheque","transfer","","withdraw").equals("")){
			pettyCashTotBal=Double.parseDouble(BAK_L.getBalanceByNOTDepositBetweenDates(BNK.getbank_id(),finStartYr+"-04-01 00:00:00",NextDay,"cheque","transfer","","withdraw"));
		}
		else{
			pettyCashTotBal=0.00;
		}
		double finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(),"bank",request.getParameter("finYear"));
		/* BTotBal=bankTotBal-pettyCashTotBal; */
		BTotBal = finOpeningBal+bankTotBal-pettyCashTotBal;
		
if(!("0.0".equals(""+BTotBal))) {

	if(BNK.getbank_id().equals("BNK1012") || BNK.getbank_id().equals("BNK1013") || BNK.getbank_id().equals("BNK1014") || BNK.getbank_id().equals("BNK1015") || BNK.getbank_id().equals("BNK1016"))
	{
		if(BNK.getbank_id().equals("BNK1012"))
		{
			sbt[2] = BTotBal;
			CHARITYTOTAL += BTotBal;
			sbttotal += BTotBal;
		}
		else if(BNK.getbank_id().equals("BNK1013"))
		{
			sbt[0] = BTotBal;
			SANSTHANNONDEV += BTotBal;
			sbttotal += BTotBal;
		}
		else if(BNK.getbank_id().equals("BNK1014"))
		{
			sbt[3] = BTotBal;
			SAINIVASTOTAL += BTotBal;
			sbttotal += BTotBal;
		}
		else if(BNK.getbank_id().equals("BNK1015"))
		{
			sbt[4] = BTotBal;
			POOJATOTAL += BTotBal;
			sbttotal += BTotBal;
		}
		else if(BNK.getbank_id().equals("BNK1016"))
		{
			sbt[1] = BTotBal;
			SANSTHANDEV += BTotBal;
			sbttotal += BTotBal;
		}
		/* else if(BNK.getbank_id().equals("BNK1027"))
		{
			sbt[5] = BTotBal;
			SANSTHANNONDEV += BTotBal;
			sbttotal += BTotBal;
		}
		else if(BNK.getbank_id().equals("BNK1029"))
		{
			sbt[6] = BTotBal;
			SANSTHANNONDEV += BTotBal;
			sbttotal += BTotBal;
		} */
	}

	else if(BNK.getbank_id().equals("BNK1011") || BNK.getbank_id().equals("BNK1019"))
	{
		if(BNK.getbank_id().equals("BNK1011"))
		{
			andhrabnk[3] = BTotBal;
			SAINIVASTOTAL += BTotBal;
			andhrabnktotal += BTotBal;
		}
		else if(BNK.getbank_id().equals("BNK1019"))
		{
			andhrabnk[0] = BTotBal;
			SANSTHANNONDEV += BTotBal; 
			andhrabnktotal += BTotBal;
		}	
	}

	else{
%>
<tr>
<td style="text-align:center;"> <span><%=BNK.getbank_name() %></span></td>
<%if(HOAL.getHeadofAccountName(BNK.getheadAccountId()).equals("SANSTHAN") && BNK.getextra4().equals("NON-DEVELOPMENT")){
	SANSTHANNONDEV = SANSTHANNONDEV +BTotBal; %>
<td style="text-align:center;"><%=df.format(BTotBal)%></td>
<%}else{ %>
<td style="text-align:center;">0.0</td>
<%} %>
<%if(HOAL.getHeadofAccountName(BNK.getheadAccountId()).equals("SANSTHAN") && BNK.getextra4().equals("DEVELOPMENT")){
	SANSTHANDEV = SANSTHANDEV +BTotBal; %>
<td style="text-align:center;"><%=df.format(BTotBal)%></td>
<%}else{ %>
<td style="text-align:center;">0.0</td>
<%} %>
<%if(HOAL.getHeadofAccountName(BNK.getheadAccountId()).equals("CHARITY")){
	CHARITYTOTAL =CHARITYTOTAL+BTotBal;%>
<td style="text-align:center;"><%=df.format(BTotBal)%></td>
<%}else{ %>
<td style="text-align:center;">0.0</td>
<%} %>
<%if(HOAL.getHeadofAccountName(BNK.getheadAccountId()).equals("SAI NIVAS")){
	SAINIVASTOTAL = SAINIVASTOTAL+ BTotBal;%>
<td style="text-align:center;"><%=df.format(BTotBal)%></td>
<%}else{ %>
<td style="text-align:center;">0.0</td>
<%} %>
<%if(HOAL.getHeadofAccountName(BNK.getheadAccountId()).equals("POOJA STORES")){ 
	POOJATOTAL = POOJATOTAL+BTotBal;%>
<td style="text-align:center;"><%=df.format(BTotBal)%></td>
<%}else{ %>
<td style="text-align:center;">0.0</td>
<%} %>
<td style="text-align:center;"><%=df.format(BTotBal)%></td>
<!-- <td style="text-align:center;" width="19%" class="bg-new">3000.00</td> -->
</tr>
<%}}%>



<% }%>
<tr>
<td style="text-align:center;"> <span>STATE BANK OF TRAVANCORE</span></td>
<% 
for (double amount : sbt)
{%>
	<td style="text-align:center;"><%=df.format(amount)%></td>
<% }%>
<td style="text-align:center;"><%=df.format(sbttotal)%></td>
</tr>
<tr>
<td style="text-align:center;"> <span>ANDHRA BANK</span></td>
<% 
for (double amount : andhrabnk)
{%>
	<td style="text-align:center;"><%=df.format(amount)%></td>
<% }%>
<td style="text-align:center;"><%=df.format(andhrabnktotal)%></td>
</tr>
<%
double pettycashTotal = 0.0;
String toDateNew = request.getParameter("toDate") + " 23:59:59";
double pettcash[] = new double[5];
String banks[] = new String[5];
banks[0] = "BNK1013";
banks[1] = "";
banks[2] = "BNK1012";
banks[3] = "BNK1014";
banks[4] = "BNK1015";
/* banks[5] = "BNK1027";
banks[6] = "BNK1029"; */
for (String bankId : banks)
{
	if(!bankId.equals(""))
	{
		/* double TotPaidAmtToVendors=Double.parseDouble(BAK_L.getOpeningBalAmountPaidToVendors(bankId,toDateNew,"cashpaid"));
		double openingBal=Double.parseDouble(BAK_L.getOpeningBalance(bankId,toDateNew,"cashpaid"))-TotPaidAmtToVendors; */
		
		double TotPaidAmtToVendors=Double.parseDouble(BAK_L.getOpeningBalAmountPaidToVendors(bankId,finStartYr+"-04-01 00:00:00",toDateNew,"cashpaid"));
		double finOpeningBal = BBAL_L.getBankOpeningBal(bankId,"cashpaid",request.getParameter("finYear"));
		double openingBal=Double.parseDouble(BAK_L.getOpeningBalanceBetweenDates(bankId,finStartYr+"-04-01 00:00:00",toDateNew,"cashpaid"))-TotPaidAmtToVendors+finOpeningBal;
		
		if(bankId.equals("BNK1012"))
		{
			pettcash[2] = openingBal;
			CHARITYTOTAL += openingBal;
			pettycashTotal += openingBal;
		}
		else if(bankId.equals("BNK1013"))
		{
			pettcash[0] = openingBal;
			SANSTHANNONDEV += openingBal;
			pettycashTotal += openingBal;
		}
		else if(bankId.equals("BNK1014"))
		{
			pettcash[3] = openingBal;
			SAINIVASTOTAL += openingBal;
			pettycashTotal += openingBal;
		}
		else if(bankId.equals("BNK1015"))
		{
			pettcash[4] = openingBal;
			POOJATOTAL += openingBal;
			pettycashTotal += openingBal;
		}
		/* else if(bankId.equals("BNK1027"))
		{
			pettcash[5] = openingBal;
			POOJATOTAL += openingBal;
			pettycashTotal += openingBal;
		}
		else if(bankId.equals("BNK1029"))
		{
			pettcash[6] = openingBal;
			SANSTHANNONDEV += openingBal;
			pettycashTotal += openingBal;
		} */
	}
}
		
%>

<tr>
<td style="text-align:center;"> <span>PETTY CASH (SBT) </span></td>
<% 
for (double pc : pettcash)
{%>
	<td style="text-align:center;"><%=df.format(pc)%></td>
<% }%>
<td style="text-align:center;"><%=df.format(pettycashTotal)%></td>
</tr>

<tr>
<td style="text-align:center;" width="13%" class="bg-new">TOTAL AMOUNT</td>
<td style="text-align:center;font-weight:bold;"><%=df.format(SANSTHANNONDEV)%></td>
<td style="text-align:center;font-weight:bold;"><%=df.format(SANSTHANDEV)%></td>
<td style="text-align:center;font-weight:bold;"><%=df.format(CHARITYTOTAL)%></td>
<td style="text-align:center;font-weight:bold;"><%=df.format(SAINIVASTOTAL)%></td>
<td style="text-align:center;font-weight:bold;"><%=df.format(POOJATOTAL)%></td>
<td style="text-align:center;font-weight:bold;"><%=df.format(SANSTHANNONDEV+SANSTHANDEV+CHARITYTOTAL+SAINIVASTOTAL+POOJATOTAL)%></td>
</tr> 
<%}%> 
</table>


</div><%} %>
</div>
</div>
</div>
</div>
</body>
</html>