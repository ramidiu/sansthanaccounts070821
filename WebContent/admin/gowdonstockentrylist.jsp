<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.util.List" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.godwanstock"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Gdown Stock Entry List</title>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<%

Calendar calendar=Calendar.getInstance();

SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");

 String formatedDate=dateFormat.format(calendar.getTime());

String hoaid="";
String fromDate="";
String toDate="";
if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
	hoaid=request.getParameter("head_account_id");
}
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
	fromDate=request.getParameter("fromDate")+" 00:00:00";
}

if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
	toDate=request.getParameter("toDate")+" 23:59:59";
}

%>
<div class="vendor-page">

		<div>
				<form  action="adminPannel.jsp" method="post" style="padding-left: 70px;padding-top: 30px;">
				<input type="hidden" name="page" value="gowdonstockentrylist"/>
				<table width="50%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td class="border-nn" colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td class="border-nn">&nbsp;&nbsp;From Date</td>
<td class="border-nn">To Date</td>
</tr>
				<tr>
					
			<td colspan="2"><select name="head_account_id" id="head_account_id" onchange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">

<%
headofaccountsListing HOA_L=new headofaccountsListing();
headofaccounts HOA=new headofaccounts();
List headaclist=HOA_L.getheadofaccounts();
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}
}%>
</select>
<input type="hidden" name="cumltv" value="cumulative">
</td>
		<td class="border-nn">&nbsp;&nbsp;<input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=formatedDate%>" style="width: 65%;"> </td>
				<td class="border-nn"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=formatedDate%>" style="width: 65%;">
				<input type="hidden" name="page" value="trailBalanceReport_newBySitaram">
				 </td>

<td class="border-nn"><input type="submit" value="SEARCH" class="click"></td>
</tr></tbody></table> </form>
				
				<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
														
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span> <span>
						
					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
				<%
				
				godwanstockListing  GL=new godwanstockListing();
			    List<godwanstock>	godwanStockList=GL.getMSEsBetweenDatesBasedOnHeadOfAccount(fromDate, toDate,hoaid);
			  
				%>
				
<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tbody><tr><td colspan="8" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;">Godown Stock Entry List </td></tr>
                        </tbody><tbody class="yourID">
                        <%
                        if(godwanStockList!=null && godwanStockList.size()>0){
                        %>
						<tr>
							<td class="bg"  style="font-weight: bold;">S.NO.</td>
							<td class="bg"  style="font-weight: bold;">MSE Number</td>
							<td class="bg" style="font-weight: bold;">VendorId</td>
							<td class="bg"  style="font-weight: bold;">Total Amount</td>
							<td class="bg"  style="font-weight: bold;">Update</td>
						
									</tr>
						</tbody>
						
							<tbody>	 
							<%
							   for(int i=0;i<godwanStockList.size();i++){
								   godwanstock gs=(godwanstock)godwanStockList.get(i);
							%>
							<tr>
								<td><%=i+1%></td>
								<td><%=gs.getextra1()%></td>
								<td><%=gs.getvendorId()%></td>
								<td><%=gs.getExtra16()%></td>
								<td><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?page=gowdonstockentryupdate&mseId=<%=gs.getextra1()%>">Update MSE Entry Data</a></td>
							</tr>
							<%
							   }
							  }
								%>
						
						</tbody></table></div>
	
	
	<div class="total-report">


	
  </div>
			</div>
				
			</div>
			</div>
</body>
</html>