<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Journal-Voucher</title>
<!--Date picker script  -->
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true, 
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});		
});
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function calculate(i){
	var u=0;
	var debitamount=0.0;
	var creditamount=0.0;
	u=document.getElementById("addcount").value;
//	alert("add count value=======>"+u);
		if(document.getElementById("majorhead"+i).value!="" && document.getElementById("minorhead"+i).value!="" && document.getElementById("subhead"+i).value!=""){
		//	alert("major minor subs are not empty");
			for(var j=0;j<u;j++){
			//	alert("add count value in loop=======>"+j);
			if(document.getElementById("debitAmt"+j).value!=""){
				debitamount=Number(parseFloat(debitamount)+parseFloat(document.getElementById("debitAmt"+j).value)).toFixed(2);
			//	alert("debit amount===>"+debitamount);
				}
			if(document.getElementById("creditAmt"+j).value!=""){
				creditamount=Number(parseFloat(creditamount)+parseFloat(document.getElementById("creditAmt"+j).value)).toFixed(2);
		//		alert("credit amount===>"+creditamount);
				}	
			}
			
		document.getElementById("credittotalamnt").value=creditamount;
		document.getElementById("debittotalamnt").value=debitamount;
	}

}
 
function majorheadsearch(j){
	  var data1=$('#majorhead'+j).val();
	  var hoid=$('#hoid').val();
var arr = [];
$.post('searchMajor.jsp',{q:data1,HAid:hoid},function(data)
{
	 
	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }

		var availableTags=arr;
		$( '#majorhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 
		 }); 
		});
} 
function minorheadsearch(j){
	  var data1=$('#minorhead'+j).val();
	  var majorhead=$('#majorhead'+j).val();
var arr = [];
$.post('searchMinior.jsp',{q:data1,q1:majorhead},function(data)
{	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }
		var availableTags=arr;
		$( '#minorhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 }); 
		});
}
function banksearch(){
		  var bankname=$('#bankname').val();
		  var date = $('#date').val();
		  var finyear = $('#finYear').val();
	  	var hoid=$('#hoid').val();
		var arr = [];
$.post('searchBanks3.jsp',{q:bankname,HOA:hoid,date:date,finyear:finyear},function(data)
{	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 // alert("bank result==>"+d);
		 arr.push({
			 label: d[0]+" "+d[1],
			 amount:d[2],
			 banktype:d[4],
		        sortable: true,
		        resizeable: true
		    });
	 }
	  var availableTags=arr;
		$( '#bankname').autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$("#bankbalance").val(ui.item.amount);
				$("#banktype").val(ui.item.banktype);
								}	 
		 }); 
		});
}
function loanbanksearch(){
	  var loanbankname=$('#loanbankname').val();
	var hoid=$('#hoid').val();
	var arr = [];
$.post('searchBanks3.jsp',{q:loanbankname,HOA:hoid,con: 'loanbank'},function(data)
{	var response = data.trim().split("\n");
for(var i=0;i<response.length;i++){
	 var d=response[i].split(",");
	 arr.push({
		 label: d[0]+" "+d[1],
		 amount:d[2],
		 banktype:d[4],
	        sortable: true,
	        resizeable: true
	    });
}
var availableTags=arr;
	$( '#loanbankname').autocomplete({
			 source: availableTags,
		 focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
			// prevent autocomplete from updating the textbox
			event.preventDefault();
			// manually update the textbox and hidden field
			$(this).val(ui.item.label);
			$("#loanbankbalance").val(ui.item.amount);
			$("#loanbanktype").val(ui.item.banktype);
							}	 
	 }); 
	});
}
function subheadsearch(j){
	  var minorhead=$('#minorhead'+j).val();
	  var majorhead=$('#majorhead'+j).val();
	  var subhead=$('#subhead'+j).val();
	  var hoa=$('#hoid'+j).val();
var arr = [];
$.post('searchSubhead.jsp',{q:subhead,HID:hoa,q1:majorhead,q2:minorhead},function(data)
{
	 
	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }

		var availableTags=arr;
		$( '#subhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 
		 }); 
		});
} 
function vendorsearch(){
	 var data1=$('#vendorname').val();
	  var headID=$('#hoid').val();
	  if($('#hoid').val().trim()==""){
			$('#hoid').focus();
			return false;
		}
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $( '#vendorname').autocomplete({source: availableTags}); 
			});
}
function Confirm(){
	var bankbalan=0;
	var debitamount=0;
	var creditamount=0;
	var bankflag=$('input:radio[name=jvtype]:checked').val();
	if(bankflag=="withbank"){
	//	alert("bank flag===>"+bankflag);
		bankbalan=$("#bankbalance").val();
	//	alert("bank balance=======>"+bankbalan);
		if(bankbalan==""){
			alert("Please Enter Bank Name");
			return false;
		}
		bankbalan=Number(bankbalan);
		debitamount=Number($("#debittotalamnt").val());
	//	alert("debit amountin validate function===>"+debitamount);
		
		creditamount=Number($("#credittotalamnt").val());
	//	alert("credit amount in validate function===>"+creditamount);
		if(!bankbalan>0){
			// alert("Availble Balance in Account is negative.");
			return false;
		}
		else if(debitamount>bankbalan && bankflag == "withloanbank"){
			alert("withloanbank");
			alert("Availble Balance in Account is RS : "+bankbalan+" /-.Entry cannot be processed.");
			return false;
		} 
		else if(debitamount>bankbalan && bankflag == "withbank"){
			alert("withbank");
			alert("Availble Balance in Account is RS : "+bankbalan+" /-.Entry cannot be processed.");
			return false;
		}else if(creditamount>0 && debitamount>0){
			alert("Both Debit and credit entry can not be processed at a time. ");
			return false;
		}
		
	}else if(bankflag=="withloanbank"){
		// alert("bank flag===>"+bankflag);
		bankbalan=$("#loanbankbalance").val();
		if(bankbalan==""){
			alert("Please Enter Bank Name");
			return false;
		}
		bankbalan=Number(bankbalan);
		debitamount=Number($("#debittotalamnt").val());
		creditamount=Number($("#credittotalamnt").val());
		if(!bankbalan>0){
			alert("Availble Balance in Account is negative.");
			return false;
		}
		else if(debitamount>bankbalan && bankflag == "withloanbank"){

			alert("Availble Balance in Account is RS : "+bankbalan+" /-.Entry cannot be processed.");
			return false;
		} 
		else if(debitamount>bankbalan && bankflag == "withbank"){
		
			alert("Availble Balance in Account is RS : "+bankbalan+" /-.Entry cannot be processed.");
			return false;
		}else if(creditamount>0 && debitamount>0){
			alert("Both Debit and credit entry is not processed at a time. ");
			return false;
		}
		
	}

var status = confirm('Are you sure you want to submit the Journal Voucher?  \n\n Click OK to proceed');
	   if(status == false){
	  	 return false;
	   }
	   else{
	  	 return true; 
	   } 
}
$(document).ready(function() {
	 $('input[type=radio][name=jvtype]').change(function() {
	        if (this.value == 'withoutbank') {
	        	$(".vendorclass").show();
	        	$(".bankclass").hide();
				$(".loanbankclass").hide();
				
	        }
	        else if (this.value == 'withbank') {
	        	$(".loanbankclass").hide();
	        	$(".bankclass").show();
	        	$(".vendorclass").hide();
	        }
	        else if (this.value == 'withloanbank') {
	        	$(".loanbankclass").show();
	        	$(".bankclass").hide();
	        	$(".vendorclass").hide();
	        } 
	        else if (this.value == 'withvendor') {
	        	$(".loanbankclass").hide();
	        	$(".bankclass").show();
	        	$(".vendorclass").show();
	        } 
	    });
	 
		var jvtype = $('input[type=radio][name=jvtype]:checked').val();
		if (jvtype == 'withoutbank') {
        	$(".vendorclass").show();
        	$(".bankclass").hide();
			$(".loanbankclass").hide();
        }
		  else if (jvtype == 'withbank') {
	        	$(".loanbankclass").hide();
	        	$(".bankclass").show();
	        	$(".vendorclass").hide();
	        }
	        else if (jvtype == 'withloanbank') {
	        	$(".loanbankclass").show();
	        	$(".bankclass").hide();
	        	$(".vendorclass").hide();
	        } 
	        else if (jvtype == 'withvendor') {
	        	$(".loanbankclass").hide();
	        	$(".bankclass").show();
	        	$(".vendorclass").show();
	        } 
        
	 
	});
	
$('#formSubmit').bind('submit', function (e) {
    var button = $('#send');
//	alert("on submitting form=====>"+button);
    // Disable the submit button while evaluating if the form should be submitted
    button.prop('disabled', true);

    var valid = true;    

    // Do stuff (validations, etc) here and set
    // "valid" to false if the validation fails
alert('111111');
    if (!valid) { 
        // Prevent form from submitting if validation failed
        e.preventDefault();
        
        // Reactivate the button if the form was not submitted
        button.prop('disabled', false);
    }
});


</script>
<script>

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#date').datepicker('option', 'minDate', new Date(startDate));
	$('#date').datepicker('option', 'maxDate', new Date(endDate));
	/* $('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate)); */
}

$(document).ready(function(){
	datepickerchange();
}); 
</script>
<script>
$(document).ready(function() {
	 $('input[type=radio][name=jvtype]').change(function() {
		if(this.value == "withloanbank"){
			document.getElementById("debit").innerHTML = "DEBIT AMOUNT(-)";
			document.getElementById("credit").innerHTML = "CREDIT AMOUNT(+)";
		}else if(this.value == "withbank"){
			document.getElementById("debit").innerHTML = "DEBIT AMOUNT(+)";
			document.getElementById("credit").innerHTML = "CREDIT AMOUNT(-)";
		}else if(this.value == "withoutbank"){
			document.getElementById("debit").innerHTML = "DEBIT AMOUNT(+)";
			document.getElementById("credit").innerHTML = "CREDIT AMOUNT(-)";
		}else if(this.value == "withvendor"){
			document.getElementById("debit").innerHTML = "DEBIT AMOUNT(+)";
			document.getElementById("credit").innerHTML = "CREDIT AMOUNT(-)";
		}
	 	});
	});
</script>
<!--Date picker script  -->
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
</head>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<form id="formSubmit" name="tablets_Update" method="post" action="Journal_entry_Insert.jsp" onsubmit="return Confirm();">
<div class="vendor-page">

<div class="vendor-box">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">Journal Voucher</td></tr>
</table>
<div class="vender-details">
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
double creditamount,debitamount;
creditamount=debitamount=0.0;
majorheadListing MJRL=new majorheadListing();
subheadListing SUBL=new subheadListing();
minorheadListing MINL=new minorheadListing();
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
List HA_Lists=HOA_L.getheadofaccounts();
int noofrooms,l;
noofrooms=l=0;

if(request.getParameter("noofrooms")!=null && request.getParameter("noofrooms")!=""){
	noofrooms=(Integer.parseInt(request.getParameter("noofrooms").toString()));
}
String subheadarray[]={"ROOM RENT","SERVICE TAX","LUXOURY TAX","COMPUTER CHARGES","EXTRA PERSON CHARGES" ,""};
String subheadamount[]={"roomrent","servicetax","luxurytax","computercharges","extrapersoncharges" ,""};
StringBuilder majorhead=new StringBuilder();
StringBuilder roomtype=new StringBuilder();
StringBuilder minorhead=new StringBuilder();
StringBuilder subhead=new StringBuilder();
StringBuilder amount=new StringBuilder();
%>
<table width="80%" border="0" cellspacing="0" cellpadding="0">finYear
<tr>
<!-- <td>Journal Voucher No</td> -->
<td>Select Fin Year</td>
<td>Date</td>
<td>Department</td>
</tr>
 <%-- <tr>
<%
/* String  journalVoucher   = ng_LST.getid("journal_voucher_no","Journal_voucher_prev");
String journalVoucherNumbers[]  = journalVoucher.split(":"); */
%>
<td id='vocher1'><input type="text" name="billnumb" id="billnumb" value="<%=ng_LST.getid("journal_voucher_no") %>"  readonly="readonly" /></td>
<td id='vocher2'><input type="text" name="billnumb" id="billnumb" value="<%=ng_LST.getid("Journal_voucher_prev") %>"  readonly="readonly"/></td> --%>
<td>

<select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<option value="2021-22" name="year" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2021-22")) {%> selected="selected"<%} %>>2021-2022</option>
					<option value="2020-21" name="year" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-21")) {%> selected="selected"<%} %>>2020-2021</option>
					<option value="2019-20" name="year" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option>
					<option value="2018-19" name="year" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%> selected="selected"<%} %>>2018-2019</option>
					<option value="2017-18" name="year" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%> selected="selected"<%} %>>2017-2018</option>
					</select>
</td>
<td><input type="text" name="date" id="date"  value="<%=currentDate%>" class="DatePicker" readonly/></td>
<td><select name="hoid" id="hoid">
<%if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%}else if(HOA.gethead_account_id().equals("3")){ %> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></td>
</tr>
<tr><td>Voucher Reference No</td><td>Narration*</td>
<td>Journal Voucher Type</td>
</tr>
<tr>
<td><input type="text" name="voucherRefNo" id="billnumb" value=""   placeHolder="Enter voucher reference no"/></td>
<td><input  name="narration" id="narration" required placeHolder="Write your narration" onkeyup="javascript:this.value=this.value.toUpperCase();"/></input></td>
<td>
<input type="radio" name="jvtype" id="jvtype" value="withoutbank" checked="checked"/>With out Bank 
<input type="radio" name="jvtype" id="jvtype"  value="withbank"/>With Bank
<input type="radio" name="jvtype" id="jvtype"  value="withloanbank"/>With Loan Bank
<input type="radio" name="jvtype" id="jvtype"  value="withvendor"/>With Vendor Name 
</td>
</tr>
<tr>
<td colspan="2" style="display:none;" class="bankclass">
Bank Name
</td>
<td colspan="1" style="display:none;" class="vendorclass">
Vendor Name
</td>
</tr>
<tr>
<td colspan="2" style="display:none;" class="bankclass">
<input type="text" name="bankname" id="bankname" value="" onkeyup="banksearch();" placeholder="Enter Bank Name" /><br/>
<input type="hidden" name="bankbalance" id="bankbalance" value=""/>
<input type="hidden" name="banktype" id="banktype" value=""/>
</td>
<td colspan="1" style="display:none;" class="vendorclass">
<input type="text" name="vendorname" id="vendorname" value="" onkeyup="vendorsearch();" placeholder="Enter Vendor Name" /><br/>
</td>
</tr>
 <!-- Loan Bank enable tr starts-->
<tr>
<td colspan="3" style="display:none;" class="loanbankclass">
Bank Name
</td>
</tr>
<tr>
<td colspan="3" style="display:none;" class="loanbankclass">
<input type="text" name="loanbankname" id="loanbankname" value="" onkeyup="loanbanksearch();" placeholder="Enter Bank Name" /><br/>
Enter Loan Bank Name or Bank Id
<input type="hidden" name="bankbalance" id="loanbankbalance" value=""/>
<input type="hidden" name="banktype" id="loanbanktype" value=""/>
</td>
</tr>
 <!-- Loan Bank enable tr ends-->

</table>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list" style="">
<div class="bgcolr yourID"  style=" float:left;width:100%;">
<div class="sno1 bgcolr">S.NO.</div>
<div class="ven-nme1 bgcolr" style="width:250px; ">MAJOR HEAD NAME</div>
<div class="ven-nme1 bgcolr" style="width:250px; ">MINOR HEAD NAME</div>
<div class="ven-nme1 bgcolr" style="width:250px; ">SUB HEAD NAME</div>
<div class="ven-nme1 bgcolr" id="debit">DEBIT AMOUNT(+)</div>
<div class="ven-nme1 bgcolr" id="credit">CREDIT AMOUNT(-)</div>
<!-- <div class="ven-amt1 bgcolr">VAT(%)</div>
<div class="ven-amt1 bgcolr" style="width:auto;float:left; ">GROSS AMOUNT</div> -->
</div>
<input type="hidden" name="addcount" id="addcount" value="5"/>
<%if(noofrooms<1){%>
	<h6>one</h6>
<%for(int i=0; i < 50; i++ ){
%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">
<div class="sno1 MT13"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="majorhead<%=i%>" id="majorhead<%=i%>"  onkeyup="majorheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a majorhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="minorhead<%=i%>" id="minorhead<%=i%>"  onkeyup="minorheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a minorhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="subhead<%=i%>" id="subhead<%=i%>"  onkeyup="subheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a subhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1">&#8377;<input type="text" name="debitAmt<%=i%>" id="debitAmt<%=i%>"  onkeyup="calculate('<%=i%>')" value="0"  style="width: 150px;"></input></div>
<div class="ven-nme1">&#8377;<input type="text" name="creditAmt<%=i%>" id="creditAmt<%=i%>"  value="0"  onkeyup="calculate('<%=i%>')" style="width: 150px;"></input></div>
<div class="clear"></div>
</div>
<%} } else{%>
	<h6>two</h6>
	<%for(int i=0; i < noofrooms;  i++){
				for(int j=0;j<6;j++ ){
					if(request.getParameter("roomid"+i)!=null && request.getParameter(subheadamount[j]+""+i)!=null){
					roomtype.append(request.getParameter("roomid"+i).toString());
					minorhead.append(MINL.getminorheadname(roomtype.toString(),"5"));
					majorhead.append(MJRL.getmajorheadname("ROOM RENT","5"));
					subhead.append(SUBL.getsubheadname(subheadarray[j],"5"));
					amount.append(request.getParameter(subheadamount[j]+""+i));
					creditamount=creditamount+Double.parseDouble(amount.toString());%>
<div  id="addcolumn<%=l%>">
<div class="sno1 MT13"><%=l+1%></div>
<input type="hidden" name="count" id="check<%=l%>"  value="<%=l %>"/> 
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="majorhead<%=l%>" id="majorhead<%=l%>"  onkeyup="majorheadsearch('<%=l%>');tabCall('<%=l%>');" class="" value="<%=majorhead.toString() %>" placeholder="Find a subhead here" <%if(l==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="minorhead<%=l%>" id="minorhead<%=l%>"  onkeyup="minorheadsearch('<%=l%>');tabCall('<%=l%>');" class="" value="<%=minorhead.toString() %>" placeholder="Find a subhead here" <%if(l==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="subhead<%=l%>" id="subhead<%=l%>"  onkeyup="subheadsearch('<%=l%>');tabCall('<%=l%>');" class="" value="<%=subhead.toString() %>" placeholder="Find a subhead here" <%if(l==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1">&#8377;<input type="text" name="debitAmt<%=l%>" id="debitAmt<%=l%>"  onkeyup="calculate('<%=l%>')" value="0"  style="width: 150px;"></input></div>
<div class="ven-nme1">&#8377;<input type="text" name="creditAmt<%=l%>" id="creditAmt<%=l%>"  value="<%=amount %>"  onkeyup="calculate('<%=l%>')" style="width: 150px;"></input></div>
<div class="clear"></div>
</div>
<% 	
l++;
roomtype.setLength(0);
minorhead.setLength(0);
majorhead.setLength(0);
subhead.setLength(0);
amount.setLength(0);
}}}}%>

<div class="add-ven">
<span style="margin:0 80px 0 0">Total Amount : </span>
&#8377;<input type="text" name="debittotalamnt" id="debittotalamnt" value="0.0" style="width: 150px;"/>
&#8377;<input type="text" name="credittotalamnt" id="credittotalamnt" value="<%=creditamount %>" style="width: 150px;"/>
</div>
<div class="add-ven">
<input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/>
<span style="margin:0 80px 0 0">&nbsp;</span>
<span style="margin:0 360px 0 0">&nbsp;</span>
<input type="reset" value="Reset" class="click" style="border:none;"/>
<input type="submit" id="send" name="saveonly" value="CREATE JV" class="click" style="border:none; "/>
</div>
<div class="tot-ven"></div>
</div>
</div>
</form>
</body>
</html>