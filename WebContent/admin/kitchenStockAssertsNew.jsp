<%@page import="beans.products"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>POOJA STORE STOCK REPORT</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

function productsearch(){

	  var data1=$('#productname').val();
	  var headID=$('#headofaccount').val();
	  $.post('searchProducts.jsp',{q:data1,page:"",hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
	</script>
	
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 							// Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<!--Date picker script  -->

<%@page import="java.util.List"%>
<%-- <div><%@ include file="title-bar.jsp"%></div> --%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SHP" class="beans.products" />
		<jsp:useBean id="PRD" class="beans.products" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<div class="vendor-page"><br /><br />
<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
				
				</div>
		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
					<%
					/* String hoa=session.getAttribute("headAccountId").toString(); */
					String hoa="4";
					if(hoa.equals("3")){ %>
				<form action="kitchenStore-StockReport.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
					
						<input type="text" name="productname" id="productname" <%if(request.getParameter("productname")!=null){ %> value="<%=request.getParameter("productname") %>" <%}else{ %> value="" <%} %> onkeyup="productsearch()" placeholder="Find product here" autocomplete="off"/></li>
						<!-- <li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li> -->
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div>
				
				 </form>
                 
				
					<%} %>
				<div class="clear"></div>
				<div class="list-details">
	   <%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	   shopstockListing SHPL=new shopstockListing();
	/* productsListing PRD_l=new productsListing (); */
	  DecimalFormat df = new DecimalFormat("00.00");
	 List SHP_STOCKL=SHPL.getpresentShopStock(request.getParameter("productname"));
	 String inwards="00";
		String outwards="00";
		String[] majorHeads={"32","42","52","72","82"};

		productsListing PRD_l=new productsListing ();
		List PRODL=PRD_l.getPresentShopStockAssets1(majorHeads,"25001","30316");
		godwanstockListing GDSTK_L=new godwanstockListing();
		shopstockListing SHPSTK_L=new shopstockListing();
		
	 
	 
	/*  List PRODL=PRD_l.getPresentShopStock(request.getParameter("productname"),hoa); */
	 double totQty=0;
	 double totgrossAmt=0.00;
	 double totGodownQty=0;
	 double totGodownGrossAmt=0.00;
	 if(SHP_STOCKL.size()>0 || PRODL.size()>0){ %>
	  <div class="printable">
	  <table width="95%"  cellspacing="0" cellpadding="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
	
			<tbody> 
				<tr>
					<td colspan="3" align="center" style="font-weight: bold;color:red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
				</tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">KITCHEN STOCK REPORT(ASSETS)</td></tr>
		
			</tbody>
			
		</table><br>
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border='1'>
					<tr>
							<td class="bg"  style="font-weight: bold;" align="center"  colspan="2">PRODUCT INFO</td>
							<td class="bg"  style="font-weight: bold;" align="center" colspan="3">GODOWN STOCK INFO</td>
							<%if(hoa.equals("3")){ %>
							<td class="bg"  style="font-weight: bold;" align="center" colspan="3">SHOP STOCK INFO</td>
							<td class="bg" width="4%"></td>   
								<%} %>
							<!-- <td class="bg" width="23%">Product Qty</td>
							<td class="bg" width="23%">Amount</td> -->
							</tr>
						<tr>
							<td class="bg" width="5%" style="font-weight: bold;">CODE</td>
							<input type="hidden" name="headofaccount" id="headofaccount" value="<%=hoa%>"/></td>
							<td class="bg" width="20%" style="font-weight: bold;">PRODUCT NAME</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">GODOWN QTY</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">UNITS</td>
									<%if(hoa.equals("3")){ %>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">PURCHASE PRICE</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">GROSS AMOUNT</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">SALE PRICE</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">SHOP QTY</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">GROSS AMOUNT</td>
							<td class="bg" width="4%"></td>	<%} %>
							<!-- <td class="bg" width="23%">Product Qty</td>
							<td class="bg" width="23%">Amount</td> -->
							</tr>
					
					<%-- 	<%

						
						for(int i=0;i<PRODL1.size();i++){
						SHP=(products)PRODL1.get(i);
						}
						
						%> --%>
						
						
						
					<%  double proQty=0.00;
						double amt=0.00;
						double godownQty=0.00;
						double godownGrossAmt=0.00;
						for(int i=0; i < PRODL.size(); i++ ){
							SHP=(beans.products)PRODL.get(i);
							if(SHP.getbalanceQuantityStore()!=null && !SHP.getbalanceQuantityStore().equals("")){
								proQty=Double.parseDouble(SHP.getbalanceQuantityStore());
								
							}
							if(SHP.getbalanceQuantityGodwan()!=null && !SHP.getbalanceQuantityGodwan().equals("")){
								godownQty=Double.parseDouble(SHP.getbalanceQuantityGodwan());
							}
							if(SHP.getsellingAmmount()!=null && !SHP.getsellingAmmount().equals("")){
								amt=Double.parseDouble(SHP.getsellingAmmount())*Double.parseDouble(SHP.getbalanceQuantityStore());
							}
							if(SHP.getpurchaseAmmount()!=null && !SHP.getpurchaseAmmount().equals("")){
								godownGrossAmt=Double.parseDouble(SHP.getpurchaseAmmount())*godownQty;
							}
							/* if(PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString())!=null && !PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString()).equals("")){
								proQty=Integer.parseInt(PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString()));
							}
							if(PRD_l.getProductsAmount(SHP.getproductId(),session.getAttribute("headAccountId").toString())!=null && PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString())!=null && !PRD_l.getProductsAmount(SHP.getproductId(),session.getAttribute("headAccountId").toString()).equals("") && !PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString()).equals("")){
								amt=Double.parseDouble(PRD_l.getProductsAmount(SHP.getproductId(),session.getAttribute("headAccountId").toString()))*Double.parseDouble(PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString()));
							} */
							totQty=(totQty+proQty);
							totgrossAmt=totgrossAmt+amt;
							totGodownQty=totGodownQty+godownQty;
							totGodownGrossAmt=totGodownGrossAmt+godownGrossAmt;
							%>
						<tr>
							<td><%=SHP.getproductId()%></td>
							<td><a href="adminPannel.jsp?page=productReport&finYear=since2015&majorId=<%=SHP.getmajor_head_id()%>&headAccountId=<%=SHP.gethead_account_id()%>&productname=<%=SHP.getproductId()%> <%=SHP.getproductName()%>"><%=SHP.getproductName()%></a></td>
													<td align="right"><span><a href="">
													<%-- <%=df.format(Double.parseDouble(SHP.getbalanceQuantityGodwan()))%> --%>
														<%if(Double.parseDouble(SHP.getbalanceQuantityGodwan())>0){ %>
													<%=df.format(Double.parseDouble(SHP.getbalanceQuantityGodwan()))%>
													<%}else{ %>00.00<%} %>
													
													</a></span></td>
													<td align="right"><span><%=SHP.getunits()%></span></td>
							<%if(hoa.equals("3")){ %>
							<td align="right"> <%=SHP.getpurchaseAmmount()%></td>
							<td align="right"> <%=df.format(godownGrossAmt) %></td>
							<td align="right"> <%=SHP.getsellingAmmount()%></td>
							<td align="right"><span><a href=""><%=SHP.getbalanceQuantityStore()%></a></span></td>
							<td align="right"> <%=df.format(amt) %></td>
							
							<td></td><%} %>
							<%-- <td> <%=SALE.gettotalAmmount()%></td> --%>
						</tr>
						<%}if(hoa.equals("3")){%>
						<tr>
						<td class="bg" colspan="3"></td>
						<td class="bg" align="right"><%=totGodownQty %></td>
						<td class="bg" align="right" style="font: bold;"> <%=df.format(totGodownGrossAmt) %></td>
						
						<td class="bg"></td>
						<td class="bg" align="right"><%=totQty %></td>
						<td class="bg" align="right" style="font: bold;"> <%=df.format(totgrossAmt) %></td>
					
						<td></td>
						</tr>
							<%} %>
					</table>
					</div>
					<%}else{%>
					<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no products found in shop!</span></div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	 response.sendRedirect("index.jsp");
} %>
<%-- <div><div ><jsp:include page="footer.jsp"></jsp:include></div></div> --%>
</body>
</html>