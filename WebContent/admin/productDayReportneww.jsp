<%@page import="mainClasses.medicineissuesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function idValidate(){
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	var id=$('#major_head_id').val().trim();
$.ajax({
	type:'post',
	url: 'IDValidate.jsp', 
	data: {
		Id : id,
		type : 'majorhead'
	},
	success: function(response) { 
		var msg=response.trim();
		$('#majorheadErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#major_head_id').focus();
			return false;
		}
	}});
}

</script>
<script type="text/javascript">

function validate(){
	$('#headIdErr').hide();
	$('#majorheadErr').hide();
	$('#headNameErr').hide();

	if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	}
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	if($('#name').val().trim()==""){
		$('#headNameErr').show();
		$('#name').focus();
		return false;
	}
}
function productsearch(){

	  var data1=$('#productname').val();
	  var headID=$('#headAccountId').val();
	  $.post('searchProducts.jsp',{q:data1,hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">

<div class="vendor-box">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Product Day Ledger Report</td></tr>
</table>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
productsListing PRD_L=new productsListing();
majorheadListing MajorHead_list=new majorheadListing();
String productid="";String hid="";
if(request.getParameter("headAccountId")!=null){
	hid=request.getParameter("headAccountId");
}
if(request.getParameter("prdid")!=null){
	productid=request.getParameter("prdid");
}
%>

<form name="tablets_Update" method="post"  onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<!-- <div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account<span style="color: red;">*</span> -->
</td>
<td>Product Name<span style="color: red;">*</span></td></tr>
<tr>
<td>
<%-- <select name="headAccountId" id="headAccountId">
<%
if(!hid.equals("")){
	%>
	<option value="<%=hid%>" selected="selected"><%=HOA_L.getHeadofAccountName(hid) %></option>
	<%
}%>

</select> --%>
</td>
<td><input type="text" name="productname" id="productname" value="<%=productid %> <%=PRD_L.getProductsNameByCat1(productid, "") %>" onkeyup="productsearch()" /></td>

</tr>

</table>
</form>

</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<!-- <div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div> -->
<div class="clear"></div>
<div class="list-details">
<%
if(request.getParameter("prdid")!=null){
 productid=request.getParameter("prdid");
if(PRD_L.getvalidProduct(productid)){

String majorHeadId = "";
	if(request.getParameter("majorId") != null && !request.getParameter("majorId").equals(""))
	{
		majorHeadId = request.getParameter("majorId");
	}
	%>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Month</td>
<td class="bg" width="10%">Opening Balance</td>
<td class="bg" width="23%">Inwards </td>
<td class="bg" width="23%">OutWards</td>
<td class="bg" width="20%">Closing Balance</td>

</tr>
<%

godwanstockListing GDSTK_L=new godwanstockListing();
shopstockListing SHPSTK_L=new shopstockListing();
DateFormatSymbols dfs = new DateFormatSymbols();
medicineissuesListing MED_L=new medicineissuesListing();
String[] months = dfs.getMonths();
int num=0;
String dt=request.getParameter("dt")+"-01";
System.out.println("dt======>"+dt);
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
Calendar c = Calendar.getInstance();
c.setTime(sdf.parse(dt));
int month = c.get(Calendar.MONTH);
int month1=month;
String inwards="00";
double opengbal=0;
if(GDSTK_L.getgodwanstockBefore(dt,productid)!=null){
	System.out.println("opening=11111=========="+opengbal);
	opengbal=Double.parseDouble(GDSTK_L.getgodwanstockBefore(dt,productid));
	System.out.println("openi11ng bal 111===="+opengbal);
	
}
if(SHPSTK_L.getgShopstockbyBefore(dt,productid)!=null){
	System.out.println("opening 2222==========="+opengbal);
	opengbal=opengbal-Double.parseDouble(SHPSTK_L.getgShopstockbyBefore(dt,productid));
	System.out.println("opening bal 2222===="+opengbal);//22.200000000000728
	
}
double closinbal=0;
double inwardsstock=0;
double closeingstock=0;
String outwards="";
 while(month1==month)
{
%>
<tr>
<td></td>
<td><span><%=dt%></span></td>
<td><%=opengbal %></td>

<%
		 

inwards=GDSTK_L.getgodwanstockbyDay(dt,productid);
System.out.println("inwards =====>"+inwards);

	if(hid.equals("1") && majorHeadId.equals("60"))
	{
		outwards=MED_L.getmedicineissuesByDay(dt,productid);
		//System.out.println("outwards medicuine =======>"+outwards);
	}
	else
	{
		 outwards=SHPSTK_L.getgShopstockbyDay(dt,productid);
		 System.out.println("outwards Shopstock =======>"+outwards);
	}
	
	 if(inwards!=null){
		 //System.out.println("iNWARDS=========>"+inwards);
		 opengbal=opengbal+Double.parseDouble(inwards);
		
		// System.out.println("opengbal DAY0 =========>"+opengbal);
	/* System.out.println(opengbal); */
		 } else{
			 inwards="0";
	 }
	 if(outwards!=null){
		 opengbal=opengbal-Double.parseDouble(outwards);
		
		 System.out.println("opengbal DAY1 =========>"+opengbal);
		 } else{
			 outwards="0";
	 }
		closinbal=opengbal;
	
	 c.add(Calendar.DATE, 1);  // number of days to add
	 dt = sdf.format(c.getTime());
	 //c.setTime(sdf.parse(dt));
	 month = c.get(Calendar.MONTH);
%>


<td><%=inwards %>pcs</td>
<td><%=outwards %>pcs</td>
<td><%=closinbal %></td>
</tr>
<%

} %>
<tr><td colspan="5" align="right"><h>Total Stock :</h></td><td><%=closinbal %></td></tr>
</table>
<%}else{%>
<span>No Reports</span>
<%} }%>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
