<%@page import="mainClasses.paymentsListing"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TDS Cheque</title>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
  @media print {
    @page {
      size: letter landscape;
	margin: 0cm;
	padding:0cm;
	top:0px; left:0px;
    }
  }
</style>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<script type="text/javascript">
    // When the document is ready, initialize the link so
    // that when it is clicked, the printable area of the
    // page will print.
    $(
        function(){
	// Hook up the print link.
            $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                    function(){
                        // Print the DIV.
                        $( "a" ).css("text-decoration","none");
                        $( ".printable" ).print();
                       

                        // Cancel click event.
                        return( false );
                    });
            $( "#printtable" ).attr( "href", "javascript:void( 0 )" ).click(
                    function(){
                        // Print the DIV.
                        $( ".printable" ).print();
                       

                        // Cancel click event.
                        return( false );
                    });
	});
    $(document).ready(function () {
        $("#btnExport").click(function () {
            $("#tblExport").btechco_excelexport({
                containerid: "tblExport"
               , datatype: $datatype.Table
            });
        });
    });
</script>
</head>
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<body>
<div>
<div class="vendor-page">
<div class="vendor-list">
		<div class="arrow-down"></div>
		<div class="icons">
	<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
</div>
<div class="clear"></div>
<div class="list-details">
<%
DecimalFormat DFF=new DecimalFormat("###.00");
NumberToWordsConverter NTW=new NumberToWordsConverter();
double totamount=0.0;
String payID=request.getParameter("payId");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
paymentsListing PAYL=new paymentsListing();

List PAY_DET=PAYL.getMpaymentsBasedOnPaymentInvoice(payID);
if(PAY_DET.size()>0){
		PAY=(beans.payments)PAY_DET.get(0);
	}
DecimalFormat DF=new DecimalFormat("0");
if(PAY.getextra7() != null && !PAY.getextra7().equals("")){
	totamount=Double.parseDouble(PAY.getextra7());
}
String amount=DF.format(totamount);
%>
<table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
	<tr>
		<td colspan="7" align="center" style="font-weight: bold;">TDS Cheque Print</td>
	</tr>
</table>
<div class="printable" style="">
<div style="position:absolute; top:145px; left:220px; font-size:20px; font-weight:bold;"><%=PAY.getextra8()%></div>
<div style="position: absolute;top: 185px;left: 260px; font-size: 20px;width: auto;height: auto;text-transform: uppercase;"><%=NTW.convert(Integer.parseInt(amount)) %>  only</div>
<div style="position: absolute;top: 70px;left: 920px;font-size: 32px;width: 200px;"><%=CDF.format(SDF.parse(PAY.getdate()))%></div>	
<div style="position: absolute;top: 215px;left: 890px;font-size: 32px;width: auto;"><strong><%=DFF.format(totamount)%></strong></div>
</div> 
</div>
</div>
</div>
</div>
</body>
</html>