<%@page import="mainClasses.paymentsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript"
	src="../js/modal-window.js"></script>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
	$(document).ready(
			function() {
				$(function() {
					$("#fromDate").datepicker(
							{
								minDate : new Date(2017, 03, 1),
								maxDate : new Date(2018, 02, 31),
								changeMonth : true,
								changeYear : true,
								numberOfMonths : 1,
								showOn : 'both',
								buttonImage : "../images/calendar-icon.png",
								buttonText : 'Select Date',
								buttonImageOnly : true,
								dateFormat : 'yy-mm-dd',
								onSelect : function(selectedDate) {
									$("#toDate").datepicker("option",
											"minDate", selectedDate);
								}
							});
					$("#toDate").datepicker(
							{
								minDate : new Date(2017, 03, 1),
								maxDate : new Date(2018, 02, 31),
								changeMonth : true,
								changeYear : true,
								numberOfMonths : 1,
								showOn : 'both',
								buttonImage : "../images/calendar-icon.png",
								buttonText : 'Select Date',
								buttonImageOnly : true,
								dateFormat : 'yy-mm-dd',
								onSelect : function(selectedDate) {
									$("#fromDate").datepicker("option",
											"maxDate", selectedDate);
								}
							});
				});
			});

	function datepickerchange() {
		var finYear = $("#finYear").val();
		var yr = finYear.split('-');
		var startDate = yr[0] + ",04,01";
		var endDate = parseInt(yr[0]) + 1 + ",03,31";

		$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
		$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
		$('#toDate').datepicker('option', 'minDate', new Date(startDate));
		$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
	}

	$(document).ready(function() {
		datepickerchange();
	});
</script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source) {
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();

	};
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                	
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" lang="javascript"
	src="../js/modal-window.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="BNK" class="beans.bankdetails" />
<jsp:useBean id="BKT" class="beans.banktransactions" />
</head>
<body>
	<%
		DecimalFormat formatter = new DecimalFormat("#,##,###.00");
		DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
		TimeZone tz = TimeZone.getTimeZone("IST");
		dateFormat2.setTimeZone(tz);

		Calendar c2 = Calendar.getInstance();
		String currentDateNew = (dateFormat2.format(c2.getTime()))
				.toString();

		if (request.getParameter("todate") != null
				&& !request.getParameter("todate").equals("")) {
			currentDateNew = request.getParameter("todate");
		}

		String monthInString = currentDateNew.substring(5, 7);
		int month = Integer.parseInt(monthInString);
		String yearInString = currentDateNew.substring(0, 4);
		int year = Integer.parseInt(yearInString);

		String finStartYr2 = "";

		if (month >= 4) {
			finStartYr2 = Integer.toString(year);
		} else {
			int finStartYrInInt = year - 1;
			finStartYr2 = Integer.toString(finStartYrInInt);
		}

		String finStartAndEndYr = "";

		String tempInString = finStartYr2.substring(2, 4);
		int tempInInt = Integer.parseInt(tempInString) + 1;
		finStartAndEndYr = finStartYr2 + "-" + tempInInt;
	%>

	
	<div class="clear"></div>
	<div class="arrow-down">
		<img src="../images/Arrow-down.png" style="margin-top:20px;">
	</div>
	<form id="searchForm" action="adminPannel.jsp" method="post">
		<input type="hidden" name="page"
			value="<%=request.getParameter("page")%>"> <input
			type="hidden" name="id" value="<%=request.getParameter("id")%>">
		<div class="search-list" style="padding-top: 30px;">
			<ul>
				<li><select name="finYear" id="finYear"
					Onchange="datepickerchange();">
						<option value="2021-22"
							<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2021-22")) {%>
							selected="selected" <%}%>>2021-22</option>
						<option value="2020-21"
							<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-21")) {%>
							selected="selected" <%}%>>2020-21</option>
						<option value="2019-20"
							<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%>
							selected="selected" <%}%>>2019-20</option>
						<option value="2018-19"
							<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%>
							selected="selected" <%}%>>2018-19</option>
						<option value="2017-18"
							<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%>
							selected="selected" <%}%>>2017-18</option>
						<option value="2016-17"
							<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%>
							selected="selected" <%}%>>2016-17</option>
						<option value="2015-16"
							<%if (request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>
							selected="selected" <%}%>>2015-16</option>
				</select></li>
				<li><input type="text" name="fromdate" id="fromDate"
					class="DatePicker" value="" readonly="readonly" /></li>
				<li><input type="text" name="todate" id="toDate"
					class="DatePicker" value="" readonly="readonly" /></li>
				<li><select name="amtDeposited" id="amtDeposited"
					onchange="showOrHide()">
						<option value="poojastore-counter">PoojaStore-counter</option>
						<option value="pro-counter">PRO Office-counter</option>
						<option value="Charity-cash">Charity-Cash</option>
						<option value="Development-cash">Development-cash</option>
						<option value="Sainivas">Sainivas(Shirdi)</option>
				</select></li>
				<li><input type="submit" class="click" name="search"
					value="Search"></input></li>
			</ul>
		</div>
	</form>
	<div class="icons" style="margin-top:20px;">
		<a id="print"><span><img src="../images/printer.png"
				style="margin: 0 20px 0 0" title="print" /></span></a> <span><a
			id="btnExport" href="#Export to excel"><img
				src="../images/excel.png" style="margin: 0 20px 0 0"
				title="export to excel" /></a></span>
	</div>

	<%
		if (request.getParameter("finYear") != null) {
	%>
	<div class="clear"></div>
	<div class="list-details" style="margin: 10px 10px 0 0">
		<div class="printable" style="margin: 0 0 50px 0;">
			<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">

				<%
					Calendar c1 = Calendar.getInstance();
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						SimpleDateFormat dateFormatsj = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						dateFormat.setTimeZone(tz.getTimeZone("IST"));

						//Calendar cal = Calendar.getInstance();
						c1.set(Calendar.DATE, 1);
						Date firstDateOfPreviousMonth = c1.getTime();
						String fromDate = (dateFormat2.format(c1.getTime())).toString();
						fromDate = fromDate + " 00:00:01";
						c1.set(Calendar.DATE, c1.getActualMaximum(Calendar.DATE));
						Date lastDateOfPreviousMonth = c1.getTime();
						String toDate = (dateFormat2.format(c1.getTime())).toString();
						toDate = toDate + " 23:59:59";
						//System.out.println("fromDate::"+fromDate+":::todate"+toDate);
						if (request.getParameter("fromdate") != null
								&& !request.getParameter("fromdate").equals("")) {
							fromDate = request.getParameter("fromdate") + " 00:00:01";
						}
						if (request.getParameter("todate") != null
								&& !request.getParameter("todate").equals("")) {
							toDate = request.getParameter("todate") + " 23:59:59";
						}
						banktransactionsListing BAK_L = new mainClasses.banktransactionsListing();
						bankdetailsListing BNKDETL = new bankdetailsListing();
						productexpensesListing PRDEXP_L = new productexpensesListing();
						subheadListing SUBL = new subheadListing();
						paymentsListing PAYL = new paymentsListing();
						String DepositCat=request.getParameter("amtDeposited");
						List BANKL = BAK_L.getBankTransactionByCategory(fromDate,
								toDate, DepositCat);
						SimpleDateFormat CDF = new SimpleDateFormat("dd-MMM-yyyy");
				%>
				
		<tr>
			<td colspan="7" align="center" style="font-weight: bold;"></td>
		</tr>
		<tr>
			<td colspan="7" align="center" style="font-weight: bold;">SHRI
				SHIRIDI SAI BABA SANSTHAN TRUST</td>
		</tr>
		<tr>
			<td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td>
		</tr>
		<tr>
			<td colspan="7" align="center" style="font-weight: bold;">Dilsukhnagar,Hyderabad,TS-500
				060</td>
		</tr>
		<tr>
			<td colspan="7" align="center"
				style="font-weight: bold; font-size: 20px;">BANK CATEGORY FORM
			</td>
		</tr>
		<tr>
			<td colspan="7" align="center"
				style="font-weight: bold; font-size: 20px;"><%=DepositCat %>
			</td>
		</tr>
		<tr><td colspan="8" align="center" style="font-weight: bold;">Report From Date <%=CDF.format(dateFormatsj.parse(fromDate))%>  TO  <%=CDF.format(dateFormatsj.parse(toDate)) %> </td></tr>
	
				<tr>
					<td class="bg" width="10%">DATE</td>
					<td class="bg" width="10%">VOUCHER</td>
					<td class="bg" width="10%">SUB_HEAD</td>
					<td class="bg" width="20%">NARRATION</td>
					<td class="bg" width="10%">CHEQUE NO</td>
					<%
						if ("credit".equals(BNK.getextra2())) {
					%>
					<!-- <td class="bg" width="15%" align="right">CREDIT</td>
<td class="bg" width="15%" align="right">DEBIT</td> -->
					<td class="bg" width="15%" align="right">DEBIT</td>
					<td class="bg" width="15%" align="right">CREDIT</td>
					<%
						} else {
					%>
					<td class="bg" width="15%" align="right">DEBIT</td>
					<td class="bg" width="15%" align="right">CREDIT</td>
					<%
						}
					%>
					<!-- <td class="bg" width="20%" align="right">BALANCE</td> -->
				</tr>
				<%
					if (BANKL.size() > 0) {
							for (int i = 0; i < BANKL.size(); i++) {
								BKT = (beans.banktransactions) BANKL.get(i);
								String id=BAK_L.getMbanktransactions1(BKT.getbanktrn_id());
								//request.getParameter("id")=bankId;
				%>
				<tr>
					<td><%=CDF.format(dateFormat.parse(BKT.getdate()))%></td>
					<td><a<% /* System.out.println("id..."+bankId); */  %>
						onclick="openMyModal('banktransactionsForm.jsp?txid=<%=BKT.getbanktrn_id()%>&id=<%=id%>'); return false;"
						target="_blank" href=""><%=BKT.getbanktrn_id()%></a></td>
					<%-- <%
						if (BKT.getExtra8().equals("other-amount")
											&& !BKT.getSub_head_id().equals("")) {
					%> --%>
					<%if(!BKT.getSub_head_id().equals("")&&BKT.getSub_head_id()!=null){ %>
					<td>(<%=BKT.getSub_head_id()%>)<%=SUBL.getMsubheadname(BKT.getSub_head_id())%></td>
					 <%} else {%><td></td> <%} %>
					<%-- <td>(<%=BKT.getSub_head_id()%>)<%=PRDEXP_L.getproductexpensesSubheadonBanks(BKT.getSub_head_id())%></td> --%>
					<%-- <%
						}
					%> --%>
					<td><%=BKT.getnarration()%></td>
					<td align="center"><%=PAYL.getChequeNo(BKT.getextra4())%></td>
					<td align="right">
						<%
							if (BKT.gettype().equals("deposit")) {
						%> <%=formatter.format(Double.parseDouble(BKT
									.getamount()))%></td>
					<%
						}
					%>
				</tr>
				<td align="right">
					<%
						if (BKT.gettype().equals("withdraw")) {
					%> <%=formatter.format(Double.parseDouble(BKT
									.getamount()))%>
					<%
						}
					%>
				</td>
				<%
					}
						}
				%>
				</table></div></div>
				<%
					}
				%>
			
</body>
</html>