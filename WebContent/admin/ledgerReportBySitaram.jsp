<%@page import="beans.customerpurchases"%>
<%@page import="beans.productexpenses"%>
<%@page import="beans.bankbalance"%>
<%@page import="mainClasses.ReportesListClass"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Major Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script> <script>
function combochangewithdefaultoption(denom,desti,jsppage){
	var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value;  
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
		$.post(jsppage,{id:com_id} ,function(data)
		{
			//$('#minor_head_id').empty();
			if(denom=="head_account_id"){
				document.getElementById("minor_head_id").options[0].selected = "true";	
			}
			if(desti == "minor_head_id"){
				document.getElementById("minor_head_id").options.length = 0;
				var x=document.createElement('option');
				x.text="Select minor head";
				x.value="";
				sda1.add(x,null);
			}
			var where_is_mytool=data.trim();
		var mytool_array=where_is_mytool.split("\n");
		for(var i=0;i<mytool_array.length;i++)
		{
		if(mytool_array[i] !="")
		{
		//alert (mytool_array[i]);
		var y=document.createElement('option');
		var val_array=mytool_array[i].split(":");
					y.text=val_array[1];
					y.value=val_array[0];
					try
					{
					sda1.add(y,null);
					}
					catch(e)
					{
					sda1.add(y);
					
					}
		}
		}
		}); 
}
</script>
<script>
$(function() {
	$( "#fromdate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#todate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#todate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromdate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>

<script>

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromdate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromdate').datepicker('option', 'maxDate', new Date(endDate));
	$('#todate').datepicker('option', 'minDate', new Date(startDate));
	$('#todate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 
</script>

  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script type="text/javascript">
    function formsubmit(){
    	
    	if($("#minor_head_id").val()!="" && $("#minor_head_id").val()!=null)
    	{
	   		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReportBySitaram&subhid="+$( '#minor_head_id' ).val()+"&finYear="+$('#finYear').val()+"&cumltv="+$('#cumltv').val();
	       	document.getElementById("departmentsearch").submit();
	   	}
    	else if($("#major_head_id").val() != "" && $("#fromdate").val() !="" && $("#todate").val() !="")
    	{
    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReportBySitaram&majrhdid="+$('#major_head_id').val()+"&fromdate="+$('#fromdate').val()+"&todate="+$('#todate').val()+"&report="+$('#report').val()+"&hoaid="+$('#head_account_id').val()+"&finYear="+$('#finYear').val()+"&cumltv="+$('#cumltv').val();
        	document.getElementById("departmentsearch").submit();
    	} 
    	else if($("#fromdate").val() !="" && $("#todate").val() !="" && $('#head_account_id').val() != "")
    	{
	    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReportBySitaram&hoid="+$('#head_account_id').val()+"&cumltv="+$('#cumltv').val();
	    	document.getElementById("departmentsearch").submit();
    	}
    	else if($("#fromdate").val() =="" && $("#todate").val() =="")
    	{
	    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReportBySitaram&cumltv="+$('#cumltv').val();
	    	document.getElementById("departmentsearch").submit();
    	}
    }</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
<%@page import="java.util.List"%>
</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<%
	headofaccountsListing HOA_L = new headofaccountsListing();
	List headaclist = HOA_L.getheadofaccounts();
	
	DecimalFormat df = new DecimalFormat("#,###.00");
	String fromdate = "";
	String todate = "";
	String report = "";
	if(request.getParameter("fromdate") != null && !request.getParameter("fromdate").equals("")){
		 fromdate=request.getParameter("fromdate");
	}
	if(request.getParameter("todate") != null && !request.getParameter("todate").equals("")){
		todate=request.getParameter("todate");
	}
	if(request.getParameter("report") != null){
		report=request.getParameter("report");
	}
	%>
	<div>
			<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form  name="departmentsearch" id="departmentsearch" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
<td style="width:14%;"><div class="warning" id="yearIdErr" style="display: none;">Please Provide  "Year".</div>Select Financial Year</td>
<td style="width:14%;"><div class="warning" id="fromdateErr" style="display: none;">Please Provide  "From Date".</div>From Date</td>
<td style="width:14%;"><div class="warning" id="todateErr" style="display: none;">Please Provide  "To Date".</div>To Date</td>
<td style="width:14%;"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td style="width:14%;"><div class="warning" id="MajorHeadErr" style="display: none;">Please select  "Major head".</div>Major head</td>
<td style="width:14%;"><div class="warning" id="MinorHeadErr" style="display: none;">Please select  "Minor head".</div>Minor head</td>
</tr>	
<tr>
	<%-- <td style="width:14%;padding-top:10px;">
		<select name="finYear" id="finYear" Onchange="datepickerchange();" style="width:95%;">
		<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
		<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
		</select>
	</td> --%>
	
		
			<td style="width:14%;padding-top:10px;">
			 <select name="finYear" id="finYear" Onchange="datepickerchange();" style="width:95%;">
						<%@ include file="yearDropDown1.jsp" %>
					</select>
			</td> 
	
	<td style="width:14%;padding-top:10px;">
		<input type="text" name="fromdate" id="fromdate" readonly="readonly" value="<%=fromdate%>" style="margin-right: 10px;"/>
		 <input type="hidden" name="report" id="report" readonly="readonly" value="<%=report%>" style="margin-right: 10px;"/>
	</td>
	<td style="width:14%;padding-top:10px;">
		<input type="text" name="todate" id="todate" readonly="readonly" value="<%=todate%>" style="margin-right: 10px;"/> 
	</td>
	<td style="width:14%;padding-top:10px;">
		<select name="head_account_id" id="head_account_id" onChange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')" style="margin-right: 10px;width:95%;">
			<option value="" selected="selected">Select headofaccount</option>
			<%
			if(headaclist.size() > 0){
				for(int i = 0 ; i < headaclist.size() ; i++){
				HOA = (headofaccounts) headaclist.get(i);
			%>
			<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
			<%}} %>
		</select>
	</td>
	<td style="width:14%;padding-top:10px;">
		<select name="major_head_id" id="major_head_id" onChange="combochangewithdefaultoption('major_head_id','minor_head_id','getMinorHeads.jsp')" style="margin-right: 10px;width:95%;">
			<option value="" selected="selected">Select major head</option>
		</select>
	</td>
	<td style="width:12%;padding-top:10px;">
		<select name="minor_head_id" id="minor_head_id" style="margin-right: 10px;width:95%;">
			<option value="" selected="selected">Select minor head</option>
		</select>
	</td>
</tr>
<tr>
<td style="width:14%;">Select Cumulative Type</td>
</tr>
<tr>
	<td style="width:14%;">
	<select name="cumltv" id="cumltv">
		<option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option>
		<option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option>
	</select>
	</td>
	<td><input type="button" value="SEARCH" class="click" onclick="formsubmit()" style="margin-top:20px;"/></td>
</tr>
</table> 
</form>
				<% 
				String finyr[] = null;	
				double credit = 0.0;
				double debit = 0.0;
				double balance = 0.0;
				double credit1 = 0.0;
				double debit1 = 0.0;
				double balance1 = 0.0;
				String cashtype = "online pending";
				String cashtype1 = "othercash";
				String cashtype2 = "offerKind";
				String cashtype3 = "journalvoucher";
				productexpensesListing PEXP_L = new productexpensesListing();
				SimpleDateFormat dbdat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat onlydat2 = new SimpleDateFormat("dd-MMM-yyyy");
				String hoaid="1";
				if(request.getParameter("head_account_id")!=null){
					hoaid=request.getParameter("head_account_id");
				}
				
				
				ReportesListClass reportesListClass = new ReportesListClass();
				
				customerpurchasesListing CP_L = new customerpurchasesListing();
// 				banktransactionsListing BNK_L = new banktransactionsListing();
// 				majorheadListing MH_L = new majorheadListing();
				
				List<majorhead> majhdlist = reportesListClass.getMajorHeadsBasedOnHOA(hoaid);
				%>
				<div class="icons">
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	<div class="printable">
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:96%;
    background:#d0e3fb;
}

</style>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<% if(request.getParameter("fromdate") != null && !request.getParameter("fromdate").equals("") && request.getParameter("todate") != null && !request.getParameter("todate").equals("")){ 
	bankbalanceListing BBAL_L=new bankbalanceListing();
	String finYr = request.getParameter("finYear");
	String finStartYr = request.getParameter("finYear").substring(0, 4);
	String onlyfdate = request.getParameter("fromdate");
	String onlytdate = request.getParameter("todate");
	String fdate = request.getParameter("fromdate")+" 00:00:00";
	String tdate = request.getParameter("todate")+" 23:59:59";
	
	if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
	{
		onlyfdate = finStartYr+"-04-01";
		fdate = finStartYr+"-04-01"+" 00:00:00";
	}
%>
				<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
					<tr>
						<td colspan="5">
                    		<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" class="yourID">
								<tr>
									<td colspan="5" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hoaid) %></td>
								</tr>
								<tr><td colspan="5" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
								<tr><td colspan="5" align="center" style="font-weight: bold;">Ledger Report Major Head</td></tr>
								<tr><td colspan="5" align="center" style="font-weight: bold;">Report From Date  <%=onlydat2.format(onlydat.parse(onlyfdate)) %> TO <%=onlydat2.format(onlydat.parse(onlytdate)) %> </td></tr>
             					<tr>
									<td class="bg" colspan="0" style="font-weight: bold;" width="10%" align="center">S.NO.</td>
									<td class="bg" colspan="0" style="font-weight: bold;" width="30%" align="left">MAJOR HEAD</td>
									<td class="bg" colspan="0" style="font-weight: bold;" width="10%" align="left">TYPE</td>
									<td class="bg" colspan="0" style="font-weight: bold;" width="20%" align="left">DEBIT</td>
									<td class="bg" colspan="0" style="font-weight: bold;" width="20%" align="left">CREDIT</td>
									<td class="bg" colspan="0" style="font-weight: bold;" width="20%" align="left">BALANCE</td>
								</tr>
							</table>
                        </td>
                       </tr>
					   <tr>
							<td colspan="5">
								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="">
								<% if(majhdlist.size()>0){ 
									
									
									List<bankbalance> debitAmountOpeningBalList = null;
									List<bankbalance> creditAmountOpeningBalList = null;
									
									
									if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
									{
										debitAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hoaid, "", "", "", finYr,"Assets", "debit", "extra3", "MONTHNAME");
										creditAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hoaid, "", "", "", finYr,"Liabilites", "credit", "extra3", "MONTHNAME");
									} 
									
// 									System.out.println("1111111111111111111111111111111111111111");
									
								for(int i = 0 ; i < majhdlist.size() ; i++){
									MH = (majorhead) majhdlist.get(i);
						  			
						  			List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "major_head_id", "MONTHNAME", "not a loan bank");
									List<banktransactions> otherAmountListForLoanBank = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, MH.getmajor_head_id(), "", "", fromdate, todate, "major_head_id", "MONTHNAME", "loan bank");
									
						  			
									double otherAmount = 0.0;
									if(otherAmountList.size()>0)
									{
										for(int j = 0 ; j < otherAmountList.size(); j++){
											banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
											if(banktransactions.getamount() != null)
											{
												otherAmount = otherAmount + Double.parseDouble(banktransactions.getamount());
										
											}
										}
									}
									
									double otherAmountLoan = 0.0;
									if(otherAmountListForLoanBank.size()>0)
									{
										for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
											banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
											if(banktransactions.getamount() != null)
											{
												otherAmountLoan = otherAmountLoan + Double.parseDouble(banktransactions.getamount());
										
											}
										}
									}
										
										double debitAmountOpeningBal = 0.0;
										double creditAmountOpeningBal = 0.0;
										
										if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
											for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
												bankbalance bankbalance = debitAmountOpeningBalList.get(j);
												if(bankbalance.getExtra3().trim().equals(MH.getmajor_head_id())){
													debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
												}
											}
										}
										
										if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
											for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
												bankbalance bankbalance = creditAmountOpeningBalList.get(j);
												if(bankbalance.getExtra3().trim().equals(MH.getmajor_head_id())){
													creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
												}
											}
										}
										
											double LedgerSum = 0.0;
											double LedgerSumBasedOnJV = 0.0;
										
										List<productexpenses> productexpensesList = reportesListClass.getSumOfAmountFromProductexpenses(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "pe.major_head_id", "MONTHNAME");
										
										List<customerpurchases> customerpurchases = reportesListClass.getJVAmountsFromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, cashtype3, "extra6", "MONTHNAME");
										
										if(productexpensesList != null && productexpensesList.size() > 0){
											for(int j = 0; j < productexpensesList.size(); j++){
												LedgerSum = LedgerSum + Double.parseDouble(productexpensesList.get(j).getamount());
											}
										}
										
										if(customerpurchases != null && customerpurchases.size() > 0){
											for(int j = 0; j < customerpurchases.size(); j++){
												LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases.get(j).gettotalAmmount());
											}
										}
										
										double productExpJVdebitAmount = 0.0;
// 										List<productexpenses> productexpensesJVDebit =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, MH.getmajor_head_id(), "", "", cashtype3, fdate, tdate, "pe.major_head_id", "MONTHNAME", "debit");
										
// 										if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
// 											for(int j = 0; j < productexpensesJVDebit.size(); j++){
// 												productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
// 											}
// 										}
											debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan;
// 										System.out.println("222222222222222222222222222222222222222222222222");
										%>
									
										<%
										debit = debit + debit1;
										
										double productExpJVAmount = 0.0;
										
									// jv amount from productexpensec withbank and withloanbank and withvendor 
									List<productexpenses> productexpensesJV =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, MH.getmajor_head_id(), "", "", cashtype3, fdate, tdate, "pe.major_head_id", "MONTHNAME", "credit");
									
									if(productexpensesJV != null && productexpensesJV.size() > 0){
										for(int j = 0; j < productexpensesJV.size(); j++){
											productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpensesJV.get(j).getamount());
										}
									}
									double SumDollarAmt = 0.0;
									List<customerpurchases> DollarAmt = reportesListClass.getSumOfDollorAmountfromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, cashtype1, "s.major_head_id", "MONTHNAME");
									
									if(DollarAmt != null && DollarAmt.size() > 0){
										for(int j = 0; j < DollarAmt.size(); j++){
											SumDollarAmt = SumDollarAmt + Double.parseDouble(DollarAmt.get(j).gettotalAmmount());
										}
									}
									
									double cardCashChequeJvOnlineAmount = 0.0;
									List<customerpurchases> cardCashChequeJvAmountList = reportesListClass.getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "s.major_head_id", "MONTHNAME");
									
									if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
										for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
											cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(cardCashChequeJvAmountList.get(j).gettotalAmmount());
										}
									}
									
									double jvCreditAmountFromCustomerpurchases = 0.0;
// 									List<customerpurchases> jvCreditAmountFromCustomerpurchasesList = reportesListClass.getJVCreditAmountFromCustomerPurchases(hoaid, MH.getmajor_head_id(), "", "", fdate, tdate, "s.major_head_id", "MONTHNAME");
									
// 									if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
// 										for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
// 											jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
// 										}
// 									}
// 									System.out.println("333333333333333333333333333333333333");
									credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount + jvCreditAmountFromCustomerpurchases;
									credit = credit + credit1;
										
										%>
										<tr style="">
										<td style="font-weight: bold;" width="10%" align="center"><%=i+1 %></td>
										<td style="font-weight: bold;" colspan="0" width="30%" align="left"><a href="adminPannel.jsp?page=minorheadLedgerReportBySitaram&hoaid=<%=hoaid %>&majrhdid=<%=MH.getmajor_head_id()%>&fromdate=<%=fromdate%>&todate=<%=todate%>&finYear=<%=finYr%>&report=Ledger Report&cumltv=<%=request.getParameter("cumltv")%>"><%=MH.getmajor_head_id()%> <%=MH.getname()%></a></td>
										<td style="font-weight: bold;" colspan="0" width="10%" align="left"><%=MH.getextra2().toUpperCase()%></td>
										<td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(debit1)%></td>
										<td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(credit1)%></td>
										<%
										if(debit1>credit1)
										{
											balance1=debit1-credit1;
										}
										if(credit1>debit1)
										{
											balance1=credit1-debit1;
										}
										if(credit1==debit1)
										{
											balance1=0.0;
										}
										%>		
										<td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(balance1)%></td>
									</tr>
										<%}}%>
								</table>
							</td>
						</tr>
						<tr style="border: 1px solid #000;">
							<td colspan="2" align="center" style="font-weight: bold;position: relative;
    left: 160px;" width="40%">Total  </td>
							<td style="font-weight: bold;padding-left:40px;position: relative;
    left: 118px;" colspan="0" align="left" width="20%">Rs.<%=df.format(debit) %></td> 
							<td style="font-weight: bold;
							position: relative;
    left: 100px;" colspan="0" align="left" width="20%">Rs.<%=df.format(credit) %> </td>   
							<%
							if(debit>credit)
							{
								balance=debit-credit;
							}
							if(credit>debit)
							{
								balance=credit-debit;
							}
							if(credit==debit)
							{
								balance=0.0;
							}
							%>
							<td style="font-weight: bold;position: relative;
    left: 67px;" colspan="0" align="left" width="20%">Rs.<%=df.format(balance)%></td>					
						</tr>
						<tr><td height="150"></td></tr>
				</table>
			<%} %>
			<div style="clear:both"></div>
		</div>
	</div>
	</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>