<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="mainClasses.subheadListing" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>updateDiagnosticSubHeads</title>
</head>
<body>

<%
   String subHeadId=request.getParameter("subHeadId");
   String extra2=request.getParameter("extra2");
   String extra4=request.getParameter("extra4");
   
   subheadListing subHeadListing=new subheadListing();
   subHeadListing.updateDiagnosticHeads(subHeadId, extra2, extra4);
   response.sendRedirect("subheadsfordiagnosticwithupdate.jsp?subHeadId="+subHeadId+"&extra2="+extra2+"&extra4="+extra4);
%>
</body>
</html>