<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.countersListing"%>
<%@page import="beans.cashier_report"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="mainClasses.cashier_reportListing"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Counter Balance Edit</title>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="CSH" class="beans.cashier_report"/>
<script>
function modifyamnt(){
	var total=$("#charitytotamnt").val();
	var dueamnt=$("#charitydueamnt").val();
	var chartyamnt;
	chartyamnt=Number(total)-Number(dueamnt);
	if(chartyamnt>1){
	$("#charityamnt").val(chartyamnt);
	} else{
		$("#charityamnt").val(total);
		$("#charitydueamnt").val('0');
	}
	
}

</script>
</head>
<body>
<div class="vendor-page">
<div class="vendor-box">
<div class="clear"></div>
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">BANK DEPOSITS FORM </td></tr>
</table>
<div class="vender-details">
<%double totamnt1=0.0;
countersListing COUNL=new countersListing();
String counterid="";
if(request.getParameter("q")!=null){
	counterid=request.getParameter("q").toString();
}
customerpurchasesListing CUSTP_L=new customerpurchasesListing();
double EmployeAmount=0.0;
cashier_reportListing CSHR_L=new  cashier_reportListing();
DecimalFormat DF=new DecimalFormat("#,##,##.00");
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
Map counters=CSHR_L.getMcashier_report(counterid);
Iterator countiter=counters.keySet().iterator();
while(countiter.hasNext()){
	  Object key   = countiter.next();
   	  Object value = counters.get(key);
   	CSH=(cashier_report)value;
}
EmployeAmount=CUSTP_L.getDayWiseIncomeBasedEmployeID("1",CSH.getcash2(),CSH.getlogin_id());
totamnt1=CUSTP_L.getDayWiseIncomeBasedEmployeID("4",CSH.getcash2(),CSH.getlogin_id());

%>
<form  method="post" action="cashier_balance_update.jsp" onsubmit="return validate();">
<table width="70%" border="0" cellspacing="2" cellpadding="4">
<tr>
<td >Date</td>
<td ><%=CSH.getcash2()%></td>
<td >Employee Name</td>
<td ><%=COUNL.getName(CSH.getcounter_code()) %></td>
</tr>
<tr>
<td >Total Amount</td>
<td ><%=CSH.gettotal_amount()%>
</td>
<td >Charity Amount : <%=EmployeAmount %> 
<input type="hidden" name="charitytotamnt" id="charitytotamnt" value="<%=EmployeAmount %> "/>
<input type="hidden" name="cashier_report_id" id="cashier_report_id" value="<%=counterid %> "/>
</td>
<td > Sansthan Amount : <%=totamnt1%></td>
</tr>

<tr>
<td>Charity Amount :
</td>
<td><input type="text" name="charityamnt" id="charityamnt" value="<%=EmployeAmount %>"  readonly="readonly"></td>
<td colspan="2">Due Amount : <input type="text" name="charitydueamnt" id="charitydueamnt" value="0" onkeyup="modifyamnt()" onKeyPress="return numbersonly(this, event,true);"></td>
</tr>
<tr>
<td align="right" colspan="2"><input type="submit" value="Edit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>

</div>
</div>
</body>
</html>