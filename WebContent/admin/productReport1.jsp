<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.TreeMap"%>
<%@page import="beans.products"%>
<%@page import="java.util.Map"%>
<%@page import="beans.shopstock"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/printThis.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<style>
@media print
{
#tblExport {
	border:0;
}
}
</style>
</head>
<script type="text/javascript">
function productsearch(){
	console.log("called");	
	var data1=$('#product').val();
		if($.trim(data1) == "")	{
		}
		else	{
			var headID=$('#headAccountId').val();
			  $.post('searchProducts.jsp',{prod:data1,hoa:headID},function(data)
				{		//alert("111"+data);
								 var productNames=new Array();
								 var prodmap = new Map();
							var response = data.trim().split("\n");
							 for(var i=0;i<response.length;i++){
								 var d=response[i].split(",");
//								 console.log(d[0]+"----"+d[1]+"---"+d[2]+"----"+d[3]+"----"+d[4]);
								 if (d[1] != undefined &&  d[2] != undefined && d[3] != undefined)	{
									 productNames.push(d[0] +" "+ d[1]);
								//	 productNames.push(d[1]);
								prodmap.set(d[0],d[2]+","+d[3]+","+d[4]);
								 }
							 }
							var availableTags=data.trim().split("\n");
							var availableIds=data.trim().split("\n");
							availableTags=productNames;

						 $("#product").autocomplete({
							source: availableTags,
							select : function(event,ui)	 {
								 event.stopPropagation();
								var prodid = ui.item.value;
								 var data = prodmap.get(prodid.split(" ")[0]);
								 console.log(typeof(data));
								 var d = data.split(",");
								 $('#blQtygdn').val(d[0]);
								 $('#openingbal').val(d[1]);
								 $('#units').val(d[2]);
							}
						 }); 
			});
/* 			$.ajax({
					url : "searchProducts.jsp",
					data : {prod:data1,hoa:headID},
					method : "POST",
					success : function(response)	{
						var productNames=new Array();
						var prodmap = new Map();
					//	console.log(response);
						var prodlist = response.split("/n");
						for (var i = 0 ; i < prodlist.length ; i++)	{
							console.log(prodlist[i].trim());
							var data = prodlist[i].trim();
							var product = data.split(",");
							productNames.push(product[0]+" "+product[1]);
							prodmap.set(product[0],product[2]+" "+product[3]);
						}
						console.log(productNames);
						$("#product").autocomplete({
							 source : productNames,
							 focus: function(event, ui)	{
								 event.preventDefault();
					//			 var prodid = ui.item.value;
					//			 var data = prodmap.get(prodid.split(" ")[0]);
							 },
						 	select : function(event,ui)	{
								 var prodid = ui.item.value;
								 var data = prodmap.get(prodid.split(" ")[0]);
						 		 $('#blQtygdn').val(data.split(" ")[0]);
						 		 $('#openingbal').val(data.split(" ")[1]);
						 	}
						 });
					},
					error : function(jqXHR, exception)	{
						console.log(jqXHR.responseText);
						console.log(jqXHR);
						console.log(exception);
					}
				}); */
		}
	}

function checkHeadOfAccount()	{
	var headID=$('#headAccountId').val();
	if ($.trim(headID) == "")	{
		alert("Please Select Head Of Account");
		$('#product').val('');
		$('#headAccountId').focus();
	}
}

$(function(){
	$('#searchBtn').click(function(){
		var hoa = $('#headAccountId').val();
		if ($.trim(hoa) == "")	{
			alert("Please Select Head Of Account");
			$('#headAccountId').focus();
		}
		else	{
			$('#dept').val($('#headAccountId').val());	
			$('#form1').submit();
		}
	});
	$("#fromDate").datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		minDate: new Date(2018,3,2),
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$("#toDate").datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		minDate: new Date(2018,3,2),
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
		$("#fromDate").datepicker("setDate",new Date());
		$("#toDate").datepicker("setDate",new Date());
		
	$('#print').click(function(){
		$('#tblExport').printThis();
	});
	
	 $("#btnExport").click(function () {
         $("#tblExport").btechco_excelexport({
             containerid: "tblExport"
            , datatype: $datatype.Table
         });
     });
});
</script>
<body>
<%
if(session.getAttribute("adminId")!=null)	{%>
	<div class="vendor-page">
	<div class="vendor-box">
		<h4 style="font-weight: bold;color: red;text-align: center;line-height: 0" >SHRI SHIRIDI SAI BABA SANSTHAN TRUST</h4>
		<h3  style="font-weight: bold;font-size: 14px;text-align: center;line-height: 0">(Regd.No.646/92)</h3>
		<h3 style="font-weight: bold;font-size: 14px;text-align: center;line-height: 0">Dilsukhnagar,Hyderabad,TS-500060</h3>
		<h3 style="font-weight: bold;font-size: 14px;text-align: center;line-height: 0">Product Ledger Report</h3>
		<%
		String fdate = "" , tdate = "";
		if (request.getParameter("fromDate") != null && request.getParameter("toDate") != null)	{
			fdate = request.getParameter("fromDate");
			tdate = request.getParameter("toDate");%>
		<h3 style="font-weight: bold;font-size: 12px;text-align: center;">Report From <%=fdate %> To <%=tdate %></h3>
		<%}%>
				<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span>
				</div>
	<div class="vender-details">
	<%
	headofaccountsListing HOA_L=new headofaccountsListing();
	List HA_Lists=HOA_L.getheadofaccounts();%>
	<form action="adminPannel.jsp?page=productReport1" method="POST" id="form1">
<table class="table table-bordered"> 
<tr>
<td>Head Of Account::</td>
<td>
		<select id="headAccountId">
	<option value="">Select HeadOfAccount</option>
	<%for (int i = 0 ; i < HA_Lists.size() ; i++)	{
		headofaccounts hoa = (headofaccounts)HA_Lists.get(i);%>
		<option value="<%=hoa.gethead_account_id()%>"><%=hoa.getname()%></option>
	<%}%>
	</select>
	
</td>
</tr>
<tr>
<td>Select Dates</td>
<td>
	<input type="text" id="fromDate" name="fromDate">
	<input type="text" id="toDate" name="toDate">
</td>
</tr>
<!-- <tr>
<td>MajorHead id</td>
<td><input type="text" id="mjid" name="mjid" value=""></td>
</tr>
<tr>
<td>MajorHead Name</td>
<td><input type="text" id="mjname"></td>
</tr>
<tr>
<td>MinorHead id</td>
<td><input type="text" id="mnid" name="mnid" value=""></td>
</tr>
<tr>
<td>MinorHead Name</td>
<td><input type="text" id="mnname"></td>
</tr>
<tr>
<td>SubHead id</td>
<td><input type="text" id="sid"></td>
</tr>
<tr>
<td>SubHead Name</td>
<td><input type="text" id="sname"></td>
</tr> -->
<tr>
<td>Enter Product</td>
<td><input type="text" id="product" onkeyup="productsearch()" onkeypress="checkHeadOfAccount()" name="product"></td>
</tr>
<tr>
<td colspan="5" align="center"><input type="button" id="searchBtn" value="submit" class="click"></td>
</tr>
</table>
	<input type="hidden" id="blQtygdn" name="blQtygdn">
	<input type="hidden" id="openingbal" name="openingbal">
	<input type="hidden" name="dept" id="dept">
	<input type="hidden" name="units" id="units">
</form>
	<%
	if(request.getParameter("fromDate") != null && request.getParameter("toDate") != null)	{
		String	fromdate = request.getParameter("fromDate");
		String	todate = request.getParameter("toDate");
		String productid = "",units="";
		double updatedQty=0,openingBal1=0,gdwnStk1=0,shpStk1=0,clsgBal1=0,gdwnStk2=0,shpStk2=0,clsgBal2=0;
		if(request.getParameter("product") != null && !request.getParameter("product").equals(""))	{
			productid = request.getParameter("product");
//			mjh = request.getParameter("mjid");
//			mnh = request.getParameter("mnid");
			updatedQty = Double.parseDouble(request.getParameter("blQtygdn")) ;
			openingBal1 = Double.parseDouble(request.getParameter("openingbal")) ;
			units = request.getParameter("units");
		}
		/* System.out.println("qty---"+updatedQty);
		System.out.println("openinbal---"+openingBal1);
		System.out.println("fromdate---"+fromdate);
		System.out.println("todate---"+todate);
		System.out.println("product---"+request.getParameter("product"));
		System.out.println("hoa---"+request.getParameter("dept")); */
		String hoa = request.getParameter("dept");
		DecimalFormat df=new DecimalFormat("#,###,##0.000");
		List<godwanstock> stockList1 = null , stockList2 = null;
		List<shopstock> shoplist1 = null , shoplist2 = null;
		godwanstockListing gdwnService = new godwanstockListing();
		shopstockListing shopservice = new shopstockListing();
		productsListing productService = new productsListing();
 		List<products> productList = null;
 		Map<String,Double> openingStkMap = null;
		if (!productid.trim().equals(""))	{
// 			System.out.println("one");
// 			System.out.println("hoaaaaa---"+request.getParameter("dept"));
 			stockList1 = gdwnService.getgdwnStkBetweenDatesBsdOnHoa("2018-04-02 00:00:00",fromdate+" 23:59:59",request.getParameter("dept"),productid.split(" ")[0],"yes");
			shoplist1 = shopservice.getShopStkListBsdOnDatesAndIds("2018-04-02", fromdate, productid.split(" ")[0], hoa,"yes");
			for (godwanstock gs : stockList1)	{
//				System.out.println("yester day gs1--"+gs.getProductId()+" "+gs.getQuantity());
				if (gs.getQuantity() != null && !gs.getQuantity().trim().equals(""))	{
					gdwnStk1 += Double.parseDouble(gs.getQuantity());
				}
			}
			for (shopstock sp : shoplist1)	{
//				System.out.println("yesterday sp1--"+sp.getproductId()+" "+sp.getquantity());
				if (sp.getquantity() != null && !sp.getquantity().trim().equals(""))	{
					shpStk1 += Double.parseDouble(sp.getquantity());
				}
			}
			
			openingBal1 += gdwnStk1; 
//			System.out.println("openingBal1-----"+openingBal1);
			clsgBal1 = openingBal1 - shpStk1;
//			System.out.println("clsngbal---"+clsgBal1);
			stockList1 = gdwnService.getgdwnStkBetweenDatesBsdOnHoa(fromdate+" 00:00:00",todate+" 23:59:59",request.getParameter("dept"),productid.split(" ")[0],"");
			shoplist1 = shopservice.getShopStkListBsdOnDatesAndIds(fromdate, todate, productid.split(" ")[0], hoa,"");
			for (godwanstock gs : stockList1)	{
//				System.out.println("today gs1--"+gs.getProductId()+" "+gs.getQuantity());
				if (gs.getQuantity() != null && !gs.getQuantity().trim().equals(""))	{
					gdwnStk2 = Double.parseDouble(gs.getQuantity());
				}
			}
			for (shopstock sp : shoplist1)	{
//				System.out.println("today sp1--"+sp.getproductId()+" "+sp.getquantity());
				if (sp.getquantity() != null && !sp.getquantity().trim().equals(""))	{
					shpStk2 = Double.parseDouble(sp.getquantity());
				}
			}%>
		
		<table class="table table-bordered" id="tblExport">
			<tr>
			<th>PRODUCT_ID</th>
			<th>PRODUCT_NAME</th>
			<th>UNITS</th>
			<th>OPENING_STOCK</th>
			<th>RECEIPT</th>
			<th>CONSUMPTION</th>
			<th>CLOSING_STOCK</th>
			</tr>
			<tr>
			<td><%=productid.split(" ")[0] %></td>
			<td><%=productid.split(" ")[1] %></td>
			<td><%=units %></td>
			<td><%=df.format(clsgBal1) %></td>
			<td><%=df.format(gdwnStk2) %></td>
			<td><%=df.format(shpStk2) %></td>
			<td><%=df.format((clsgBal1 + gdwnStk2) - shpStk2) %></td>
			</tr>
		</table>		
	<% }
		else {
			double openingStk3=0,gdwnQty3=0,shpQty3=0,gdwnQty4=0,shpQty4=0,clsngbal4=0;
			System.out.println("all");
			productList = new ArrayList<products>();
			openingStkMap = new TreeMap<String,Double>();
			productList = productService.getProductsListbsdOnHoa(request.getParameter("dept"));
			stockList2 = gdwnService.getgdwnStkBetweenDatesBsdOnHoa("2018-04-02 00:00:00",fromdate+" 23:59:59",request.getParameter("dept"),"","");
			shoplist2 = shopservice.getShopStkListBsdOnDatesAndIds("2018-04-02 00:00:00", fromdate+" 23:59:59"," ",request.getParameter("dept"),"yes");
			for (int i = 0; i < productList.size() ; i++)	{
				products prod = productList.get(i);
				for (godwanstock gs : stockList2)	{
//					System.out.println("opeing gs2--"+gs.getProductId()+" "+gs.getQuantity());
					if (gs.getProductId().equals(prod.getproductId()) && gs.getQuantity() != null)	{
						gdwnQty3 += Double.parseDouble(gs.getQuantity()); break;
					}
				}
				
				for (shopstock sp : shoplist2)	{
//					System.out.println("opening sp2--"+sp.getproductId()+" "+sp.getquantity());
					if (sp.getproductId().equals(prod.getproductId()) && sp.getquantity() != null)	{
						shpQty3 +=  Double.parseDouble(sp.getquantity()); break;
					}
				}
				
				if (prod.getextra7() != null && !prod.getextra7().trim().equals(""))	{
					openingStk3 = ( Double.parseDouble(prod.getextra7()) + gdwnQty3 ) - shpQty3 ;
				}
				
				openingStkMap.put(prod.getproductId(),openingStk3);
				gdwnQty3 = shpQty3 = openingStk3 = 0;
				
			} //for
		
			stockList2 = gdwnService.getgdwnStkBetweenDatesBsdOnHoa(fromdate+" 00:00:00",todate+" 23:59:59",request.getParameter("dept"),"","");
			shoplist2 = shopservice.getShopStkListBsdOnDatesAndIds(fromdate+" 00:00:00", todate+" 23:59:59"," ",request.getParameter("dept"),"yes");%>
			<table class="table table-bordered" id="tblExport">
			<tr>
				<th>SNO</th>
				<th>PRODUCT_ID</th>
				<th>PRODUCT_NAME</th>
				<th>UNITS</th>
				<th>OPENING_STOCK</th>
				<th>RECEIPT</th>
				<th>CONSUMPTION</th>
				<th>CLOSING_STOCK</th>
			</tr>
			<%for (int i = 0; i < productList.size() ; i++)	{
				products prod = productList.get(i);

				for (godwanstock gs : stockList2)	{
//					System.out.println("closing gs2--"+gs.getProductId()+" "+gs.getQuantity());
					if (prod.getproductId().equals(gs.getProductId()) && gs.getQuantity() != null)	{
						gdwnQty4 += Double.parseDouble(gs.getQuantity()); break;
					}
				} // for
				for (shopstock sp : shoplist2)	{
//					System.out.println("closing sp2--"+sp.getproductId()+" "+sp.getquantity());
					if (prod.getproductId().equals(sp.getproductId()) && sp.getquantity() != null)	{
						shpQty4 += Double.parseDouble(sp.getquantity()); break;
					}
				} // for
				clsngbal4 = ( openingStkMap.get(prod.getproductId()) + gdwnQty4 ) - shpQty4;
				%>
				<tr>
					<td><%=i+1 %></td>
					<td><%=prod.getproductId() %></td>
					<td><%=prod.getproductName() %></td>
					<td><%=prod.getunits() %></td>
					<td><%=df.format(openingStkMap.get(prod.getproductId())) %></td>
					<td><%=df.format(gdwnQty4) %></td>
					<td><%=df.format(shpQty4) %></td>
					<td><%=df.format(clsngbal4) %></td>				
				</tr>
		<%	
		clsngbal4 = gdwnQty4 = shpQty4 = 0;
		} // for%>
		</table>
		<%}
	}
	%>
	</div>
	</div>
	</div>
<%}
else	{
	response.sendRedirect("index.jsp");
}
%>
</body>
</html>