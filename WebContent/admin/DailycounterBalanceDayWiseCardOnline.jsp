<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.Date"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="beans.bankdetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.banktransactionsListing"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAILY COUNTER BALANCE REPORT</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>

<body>
<jsp:useBean id="CUS" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="BTR" class="beans.banktransactions"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">


<div style="text-align: center;"></div>
<div class="icons">
<span> <a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="total-report">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<%
DecimalFormat DF = new DecimalFormat("#.00;-#.00");
String date = request.getParameter("dt");
String type = request.getParameter("rt");
System.out.println("date......"+date+".....type...."+type);

String type1 = "";
/* if(type != null && type.equals("1")){
	type1 = "SANSTHAN";
}
if(type != null && type.equals("2")){
	type1 = "CHARITY";
}
if(type != null && type.equals("3")){
	type1 = "POOJA STORES";
} */
if(type != null && type.equals("1")){
	type1 = "CHARITY";
}
else if(type != null && type.equals("3")){
	type1 = "POOJA STORES";
}
else if(type != null && type.equals("4")){
	type1 = "SANSTHAN";
}
else if(type != null && type.equals("5")){
	type1 = "SAI NIVAS";
}
%>
<div class="printable" >
<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td><table width="100%" cellpadding="0" cellspacing="0" class="date-wise "  >
<tr><td colspan="4" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;" class="bg-new">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;" class="bg-new">Daily Counter Balance Report</td></tr>
</table></td></tr></table>
<div id="tblExport">
<table width="100%" border="1" cellspacing="0" cellpadding="0">
<tr><td colspan="8" style="font-weight:bold;color:#f00;text-align:center;padding:5px 0px 5px 0px;"><%=type1%> DEPOSITED AMOUNT (<%=date%>)</td></tr>
  <tr>
    <td align="center" style="font-weight: bold;color:#934C1E;">S No.</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Transaction Id</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Bank Deposited In</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Head Of Account</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Account Deposit Category</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Narration</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Deposit Amount</td>
  </tr>
  <%
  String reportType = "";
  /* if(type != null && type.equals("1")){
	  reportType = "pro-counter";
  }
  if(type != null && type.equals("2")){
	  reportType = "Charity-cash";
  }
  if(type != null && type.equals("3")){
	  reportType = "poojastore-counter";
  } */
  if(type != null && type.equals("1")){
	  reportType = "Charity-cash";
  }
 
  banktransactionsListing btl = new banktransactionsListing();
  List<banktransactions> btList = btl.getBankTransactionBasedOnHoidNewDayWiseCardOnline(reportType, date);
  bankdetailsListing bdl = new bankdetailsListing();
  int total = 0;
  for(int i=0;i<btList.size();i++){
	  banktransactions BtList = btList.get(i);
	  total = total + Integer.parseInt(BtList.getamount());
  %>
  <tr>
	    <td style="color:#000;"><%=i+1%></td>
	    <td style="color:#000;"><%=BtList.getbanktrn_id()%></td>
	    <td style="color:#000;"><%=BtList.getbank_id()%><br><%=bdl.getBankName(BtList.getbank_id())%></td>
	    <td style="color:#000;"><%=type1%></td>
	    <td style="color:#000;"><%=reportType%></td>
	    <td style="color:#000;"><%=BtList.getnarration()%></td>
	    <td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format(Double.parseDouble(BtList.getamount()))%></td>
 </tr>	 
  <%}%>	   
  <tr>
  <td colspan="6" align="right" style="font-weight: bold">TOTAL&nbsp;&nbsp;</td>
  <td colspan="1" style="font-weight: bold">&nbsp;<%=DF.format(total)%></td>
  </tr>
</table>
</div>
</div>
</div>
</div>
</div>

</body>
</html>
