<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.bankbalance"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mainClasses.headofaccountsListing"%>      
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
$(document).ready(function() {
    $(".yr12 td").hide();

      $('#selectYr').change(function () {
          var val = $(this).val();
            if (val == 'yr12') {
             $('.yr12 td').show();
             $('.yr13 td').hide();
        } else {
            $('.yr12 td').hide();
             $('.yr13 td').show();
        }
        });
    });
</script>
</head>
<body>
<jsp:useBean id="BNKBAL" class="beans.bankbalance"/>
<jsp:useBean id="BNKBAL1" class="beans.bankbalance"/>
<%
if(session.getAttribute("adminId")!=null)
{%>
<div>
<div class="vendor-page" style="min-height:0px;">
<div class="vendor-box">
<!-- <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style> -->

<script>
function majorheadsearch(){
	var data1=$('#major_head_id').val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#major_head_id').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
}
</script>

<script>
function minorheadsearch(j){
		  var data1=$('#minor_head_id').val();
		  var major=$('#major_head_id').val();
		  var hoid=$("#head_account_id").val();
		  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			$('#minor_head_id').autocomplete({
				source: availableTags,
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				}
			}); 		
	  });
}

function productsearch(){
	  var data1=$('#product').val();
	  var arr = [];
	  //var data1=$("#sub_head_id").val();
	  var minor=$('#minor_head_id').val();
	  var major=$('#major_head_id').val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
} 

function vendorsearch(){
	 var data1=$('#vendorname').val();
	  var headID="";
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $( '#vendorname').autocomplete({source: availableTags}); 
			});
}
</script>

<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<script type="text/javascript">
function showOrHide()
{
	document.getElementById('major_head_id').value="";
	document.getElementById('minor_head_id').value="";
	document.getElementById('product').value="";
	document.getElementById('vendorname').value="";
	document.getElementById('amount').value="";
	document.getElementById("cdtype").value="";
	var type = $("#type").val();
	var category = $('#category').val();
	if(type == "Assets" || type == "outstandingLiabilites")
	{
		document.getElementById('td1').style.display = "none";
		document.getElementById('td2').style.display = "none";

		/* document.getElementById('td3').style.display = "block"; */
		document.getElementById('major_head_id').style.display = "inline-block";
		document.getElementById('minor_head_id').style.display = "inline-block";
		document.getElementById('product').style.display = "inline-block";
		/* document.getElementById('td4').style.display = "inline-block";
		document.getElementById('td5').style.display = "inline-block"; */
		document.getElementById('td6').style.display = "inline-block";
		document.getElementById('td44').style.display = "inline-block";
		document.getElementById('td55').style.display = "inline-block";
		document.getElementById('td66').style.display = "inline-block";
		/* document.getElementById('cdtype').style.display = "inline-block"; */
	}
	if(type == "Liabilites")
	{
		document.getElementById('td1').style.display = "inline-block";
		document.getElementById('td2').style.display = "inline-block";
	}
		/* document.getElementById('td3').style.display = "none"; */
// 		document.getElementById('major_head_id').style.display = "none";
// 		document.getElementById('minor_head_id').style.display = "none";
// 		document.getElementById('product').style.display = "none";
		/* document.getElementById('td4').style.display = "none";
		document.getElementById('td5').style.display = "none";*/
// 		document.getElementById('td6').style.display = "none"; 
// 		document.getElementById('td44').style.display = "none";
// 		document.getElementById('td55').style.display = "none";
// 		document.getElementById('td66').style.display = "none";
		/* document.getElementById('cdtype').style.display = "none"; */
// 	}
}
</script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
</script>
<script type="text/javascript">
function validate(){
	var type = document.getElementById("type").value;
	var cdtype = document.getElementById("cdtype").value;
	var amount = document.getElementById('amount').value;
	var major_head_id = document.getElementById('major_head_id').value;
	var minor_head_id = document.getElementById('minor_head_id').value;
	var product = document.getElementById('product').value;
	var vendorname = document.getElementById('vendorname').value;
	 
	/* if(type == "Assets" && cdtype==""){
		alert("Please Select either Credit or Debit");
		document.getElementById("cdtype").focus();
		return false;
	} */
	
	if(cdtype==""){
		alert("Please Select either Credit or Debit");
		document.getElementById("cdtype").focus();
		return false;
	}
	if(amount=="" || amount=="0"){
		alert("Please Enter the Amount");
		document.getElementById("amount").focus();
		return false;
	}
	if(type == "Liabilites")
	{
		if(vendorname==""){
			alert("Please Enter Vendor Name");
			document.getElementById("vendorname").focus();
			return false;
		}
	}else{
		if(major_head_id==""){
			alert("Please Enter the Major Head");
			document.getElementById("major_head_id").focus();
			return false;
		}
		if(minor_head_id==""){
			alert("Please Enter Minor Head");
			document.getElementById("minor_head_id").focus();
			return false;
		}
		if(product==""){
			alert("Please Enter Sub Head");
			document.getElementById("product").focus();
			return false;
		}
		
	}
}
</script>
<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">LIABILITIES ASSETS ENTRY FORM </td></tr>
</table>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<%-- <div class="vender-details">
<form  method="post" action="liabilities_Assets_Insert.jsp" onsubmit="return validate();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>Select Type</td>
<td>Fin Yr</td>
<td id="td1" style="display:none;">Vendor</td>
<td>Enter Amount</td>
<td>&nbsp;&nbsp;</td>
</tr>
<tr>
<td>
	<select name="type" id="type" onchange="showOrHide()">
		<option value="Assets">Assets/Outstanding Liabilities/Corpus Fund</option>
		<option value="Liabilites">Liabilites</option>
	</select>
</td>
<td>
	<select name="financialYear" id="financialYear">
		<option value="2016-17">2016-17</option>
	</select>
</td>
<td id="td2" style="display:none;">
<input type="text" name="vendorname" id="vendorname"  value="" onkeyup="vendorsearch();" placeholder="Enter Vendor Name" />
</td>
<td><input type="text" name="amount" id="amount" value="0" onKeyPress="return numbersonly(this, event,true);" /></td>
<td><input type="submit" value="SUBMIT" class="click" style="border:none;"/></td>
</tr>
<tr>
<td>
<select name="head_account_id" id="head_account_id">
<%
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
		HOA=(beans.headofaccounts)HA_Lists.get(i);%>
		<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("head_account_id")!=null && request.getParameter("head_account_id").equals(HOA.gethead_account_id())){ %> selected="selected" <%}%> ><%=HOA.getname() %></option>
<%}} %>
</select>
<select name="cdtype" id="cdtype" style="margin-left:31px;padding:8px 28px 11px 9px;">
		<option value="">----SELECT----</option>
		<option value="credit">CREDIT</option>
		<option value="debit">DEBIT</option>
</select>
</td>
<td id="td4"><input type="text" name="major_head_id" id="major_head_id" onkeyup="majorheadsearch()" class="" value="" placeholder="find a major head here" autocomplete="off"/></td>
<td id="td5"><input type="text" name="minor_head_id" id="minor_head_id" onkeyup="minorheadsearch()" class="" value=""  placeholder="find a minor head here" autocomplete="off"/></td>
<td id="td6"><input type="text" name="product" id="product"  onkeyup="productsearch()" placeholder="find a sub head here" class="" value="" autocomplete="off" /></td>
</tr>
</table>
</form>
</div>  --%>
<div class="fieldz">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<form  method="post" action="liabilities_Assets_Insert.jsp" onsubmit="return validate();">
						<table class="table">
							<tr>
								<td>Fin Yr</td>
								<td>Select Type</td>
								<td>Category</td>
								<td>Credit/Debit</td>
								<td>Amount</td>
							</tr>
							<tr>
								<td>
									<select name="financialYear" id="financialYear">
														<option value="2021-22">2021-22</option>
														<option value="2020-21">2020-21</option>
														<option value="2019-20">2019-20</option>
														<option value="2018-19">2018-19</option>
														<option value="2017-18">2017-18</option>
														<option value="2016-17">2016-17</option>
												</select>
								</td>
								<td>
									<select name="type" id="type" onchange="showOrHide()">
										<option value="Assets">Assets</option>
										<option value="outstandingLiabilites">Outstanding Liabilities/Corpus Fund</option>
										<option value="Liabilites">Liabilites (Vendor)</option>
									</select>
								</td>
								<td>
									<select name="category" id="category">
										<option value="opening">Opening Balance</option>
									</select>
								</td>
								<td>
								<select name="cdtype" id="cdtype">
									<option value="">----SELECT----</option>
									<option value="credit">CREDIT</option>
									<option value="debit">DEBIT</option>
								</select>
								</td>
								<td><input type="text" name="amount" id="amount" value="0" onKeyPress="return numbersonly(this, event,true);" /></td>
							</tr>
							<tr>
								<td>Head Of Account</td>
								<td id="td1" style="display:none;">Vendor</td>
								<td><span id="td44">Major Head</span></td>
								<td><span id="td55">Minor Head</span></td>
								<td><span id="td66">Sub Head</span></td>
								<td></td>
							</tr>
							<tr>
								<td>
									<select name="head_account_id" id="head_account_id">
									<%
									mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
									List HA_Lists=HOA_L.getheadofaccounts();
									if(HA_Lists.size()>0){
										for(int i=0;i<HA_Lists.size();i++){
											HOA=(beans.headofaccounts)HA_Lists.get(i);%>
											<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("head_account_id")!=null && request.getParameter("head_account_id").equals(HOA.gethead_account_id())){ %> selected="selected" <%}%> ><%=HOA.getname() %></option>
									<%}} %>
									</select>
								</td>
								<td id="td2" style="display:none;"><input type="text" name="vendorname" id="vendorname"  value="" onkeyup="vendorsearch();" placeholder="Enter Vendor Name" /></td>
								<td id="td4"><input type="text" name="major_head_id" id="major_head_id" onkeyup="majorheadsearch()" class="" value="" placeholder="Find a Major Head Here" autocomplete="off"/></td>
								<td id="td5"><input type="text" name="minor_head_id" id="minor_head_id" onkeyup="minorheadsearch()" class="" value=""  placeholder="Find a Minor Head Here" autocomplete="off"/></td>
								<td id="td6"><input type="text" name="product" id="product"  onkeyup="productsearch()" placeholder="Find a Sub Head Here" class="" value="" autocomplete="off" /></td>
								<td><input type="submit" value="SUBMIT" class="click" style="border:none;margin-top:26px;"/></td>
							</tr>
						</table>
					</form>
					<form name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 170px;padding-top: 30px;">
					<input type="hidden" name="page" value="LiabilitiesAssetsOpeningBal"/>
						<table class="table">
							<tr>
								<td>Select Financial Year</td>
								<td style="padding-left:20px;">Select Department</td>
							</tr>
							<tr>
							<td>
									<select name="finYear" id="finYear">
<!-- 										<option value="2020-21">2020-21</option> -->
<!-- 										<option value="2019-20">2019-20</option> -->
<!-- 										<option value="2018-19">2018-19</option> -->
<!-- 										<option value="2017-18">2017-18</option> -->
<!-- 										<option value="2016-17">2016-17</option> -->
										<%@ include file="yearDropDown1.jsp" %>
									</select>
								</td>
								<%-- <%
		
		SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
		Calendar calld = Calendar.getInstance();
		calld.add(Calendar.DATE,0);
		String todayDate=SDF.format(calld.getTime());
		String date[] = todayDate.split("-");
		 String currentyear = date[0];// 2017
		 String currentyear1 = currentyear.replace(currentyear.substring(2),""); // 20 
		 String currentyear2 = currentyear.replace(currentyear.substring(0,2),"");// 17
		 String year2 ="";
		 String year22 = "";	 
		 int currentyearinint = Integer.parseInt(currentyear2);	 
		 if(todayDate.compareTo(currentyear+"-04-01") >=0){
			 year2 =String.valueOf(currentyearinint+1);// 18
			 %>	 
			 <td style="width:14%;">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();" style="width:165px;">
					<option value="<%=currentyear1 %><%= currentyear2%>-<%= year2%>" selected="selected"><%=currentyear1 %><%=currentyear2 %>-<%=currentyear1 %><%=year2 %></option>
						<%int i=0;
							for(i=currentyearinint;i>15;i--){
								%>
								<option value="<%=currentyear1 %><%=i-1%>-<%= i%>"><%=currentyear1 %><%=i-1%>-<%=currentyear1 %><%= i%></option>
								<%}
						%>
					</select>
				  </td>
			 <%
		 }
		 else{
			 year2 =String.valueOf(currentyearinint-1);
			 %>
			 		 <td style="width:14%;">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();" style="width:165px;">
					<option value="<%=currentyear1 %><%= year2%>-<%= currentyear2%>" selected="selected"><%=currentyear1 %><%=year2 %>-<%=currentyear1 %><%=currentyear2 %></option>
						<% int i=0;
							for(i=currentyearinint-1;i>15;i--){
								%>
								<option value="<%=currentyear1 %><%=i-1%>-<%= i%>"><%=currentyear1 %><%=i-1%>-<%=currentyear1 %><%= i%></option>
								<%}
						%>
					</select>
				  </td>
			 <%
		 }
		%> --%>
								<td  style="padding-left:19px">
		<select name="hoid" id="hoid" onchange="formSubmit();">
<option value="" >ALL DEPARTMENTS</option>
<%
DecimalFormat decimalFormat=new DecimalFormat("0.00");
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></li></td>
									<td>
										<input type="submit" value="SEARCH" class="click"/>
									</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<div class="icons">
<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="vendor-list">
<div class="clear"></div>
<div class="clear"></div>
<div class="list-details">
<div id="tblExport">
<%
String financialyear=request.getParameter("finYear");
String dept=request.getParameter("hoid");
/* System.out.println("dept..."+dept); */
headofaccountsListing HOAL = new headofaccountsListing();
bankbalanceListing BNK_BAL = new bankbalanceListing();
headofaccountsListing HODL=new headofaccountsListing();
majorheadListing MJHDL=new majorheadListing();
minorheadListing MINL=new minorheadListing();
subheadListing SUBL=new subheadListing();
vendorsListing VEN = new vendorsListing(); 

double debitAmount=0.0;
double creditAmount=0.0;

DecimalFormat df = new DecimalFormat("0.00");

System.out.println("dept is :: "+dept);
if(HA_Lists.size()>0){
	for(int j=0;j<HA_Lists.size();j++){
		int i=0;
	HOA=(beans.headofaccounts)HA_Lists.get(j); 
List BNKBAL_List=BNK_BAL.getAssetsLiabilitiesOfFinYrAndHOA1("Assets",financialyear,HOA.gethead_account_id());
List BNKBAL_List1=BNK_BAL.getbankbalanceFinYearAndDept(financialyear,HOA.gethead_account_id());
if(BNKBAL_List.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0" border="1" class="yourID" rules="all">
<tr align="Left"><td style="text-align:center;font-weight:bold;" colspan="10">ASSETS FOR <%=HOA.getname() %></td></tr>
<!-- <tr><td colspan="9"><table width="100%" cellpadding="0" cellspacing="0" class="yourID"> -->
<tr>
<td class="bg" width="5%" align="center" style="font-size: 13px;font-weight: bold;">S.No.</td>
<td class="bg" width="5%" style="font-size: 13px;font-weight: bold;">Financial Year</td>
<td class="bg" width="7%" style="font-size: 13px;font-weight: bold;">UniqId</td>
<td class="bg" width="7%" style="font-size: 13px;font-weight: bold;">Type</td>
<td class="bg" width="7%" style="font-size: 13px;font-weight: bold;">Credit/Debit</td>
<td class="bg" width="10%" style="font-size: 13px;font-weight: bold;">HOA</td>
<td class="bg" width="10%" style="font-size: 13px;font-weight: bold;">Major Head</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">Minor Head</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">Sub Head</td>
<!-- <td class="bg" width="6%">Amount</td> -->
<td class="bg" width="6%" style="font-size: 13px;font-weight: bold;">Debit Amount</td>
<td class="bg" width="6%" style="font-size: 13px;font-weight: bold;">Credit Amount</td>
</tr>
<!-- </table></td></tr> -->
<%for(i=0; i < BNKBAL_List.size(); i++ ){
	BNKBAL=(bankbalance)BNKBAL_List.get(i);
	
	
	%>
<tr>
<td width="5%" align="center" style="font-size: 11px;font-weight: bold;"><%=i+1%></td>
<td width="5%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra1() %></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getUniq_id() %></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getType()%></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL.getExtra7()!=null){if(BNKBAL.getExtra7().equals("credit")){%>CREDIT<%}else if(BNKBAL.getExtra7().equals("debit")){%>DEBIT<%}}%></td>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra2()%> <%=HODL.getHeadofAccountName(BNKBAL.getExtra2())%></td>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra3()%> <%=MJHDL.getmajorheadName(BNKBAL.getExtra3())%></td>
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra4()%> <%=MINL.getminorheadName(BNKBAL.getExtra4())%></td>
<%-- <td width="20%"><%=BNKBAL.getExtra5()%> <%=SUBL.getSuheadName(BNKBAL.getExtra5())%></td> --%> <!-- commented by madhav -->
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra5()%> <%=SUBL.getSuheadNameBasedOnMajorheadandMinorhead(BNKBAL.getExtra3(), BNKBAL.getExtra4(), BNKBAL.getExtra5())%></td>
<td width="6%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL.getExtra7().equals("debit")){%><%=decimalFormat.format(Double.parseDouble(BNKBAL.getBank_bal()))%><%}else{%>0.00<%}%></td>
<td width="6%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL.getExtra7().equals("credit")){%><%=decimalFormat.format(Double.parseDouble(BNKBAL.getBank_bal()))%><%}else{%>0.00<%}%></td>
</tr>	
<%}%>
<%for(int k=0; k < BNKBAL_List1.size(); k++ ){ 
BNKBAL1=(bankbalance)BNKBAL_List1.get(k);%>
<tr>
<td  width="5%" align="center" style="font-size: 11px;font-weight: bold;"><%=i+k+1%></td>
<td width="5%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL1.getExtra1() %></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL1.getUniq_id()%></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL1.getType()%></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL1.getExtra7()!=null){if(BNKBAL1.getExtra8().equals("credit")){%>CREDIT<%}else if(BNKBAL1.getExtra8().equals("debit")){%>DEBIT<%}}%></td>
<%if(BNKBAL1.getType().equals("Counter Cash")){ %>
<td width="25%" style="font-size: 11px;font-weight: bold;"><%=HODL.getHeadofAccountName(BNKBAL1.getExtra2())%></td><%}else{ %>
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL1.getBank_id()%></td><%} %>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL1.getExtra3()%> <%=MJHDL.getmajorheadName(BNKBAL1.getExtra3())%></td>
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL1.getExtra4()%> <%=MINL.getminorheadName(BNKBAL1.getExtra4())%></td>
<%-- <td width="20%"><%=BNKBAL.getExtra5()%> <%=SUBL.getSuheadName(BNKBAL.getExtra5())%></td> --%> <!-- commented by madhav -->
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL1.getExtra5()%> <%=SUBL.getSuheadNameBasedOnMajorheadandMinorhead(BNKBAL1.getExtra3(), BNKBAL1.getExtra4(), BNKBAL1.getExtra5())%></td>
<%-- <td width="6%"><%if(BNKBAL1.getExtra7().equals("debit")){%><%=decimalFormat.format(Double.parseDouble(BNKBAL1.getBank_bal()))%><%}else{%>0.00<%}%></td>
<td width="6%"><%if(BNKBAL1.getExtra7().equals("credit")){%><%=decimalFormat.format(Double.parseDouble(BNKBAL1.getBank_bal()))%><%}else{%>0.00<%}%></td> --%>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL1.getExtra8().equals("debit")){%>
<% debitAmount=Double.parseDouble(BNKBAL1.getBank_bal())+debitAmount;
df.format((debitAmount));%>
<%=df.format(Double.parseDouble(BNKBAL1.getBank_bal()))%><%}else{%>0.00<%}%></td>

<td width="10%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL1.getExtra8().equals("credit")){%>
<% creditAmount=Double.parseDouble(BNKBAL1.getBank_bal())+creditAmount;
df.format((creditAmount)); %>
<%=df.format(Double.parseDouble(BNKBAL1.getBank_bal()))%><%}else{%>0.00<%}%></td>
</tr>
<%} %>

</table>
<%}}}

List BNKBAL_List1 = BNK_BAL.getCreditLiabilitiesOfFinYrAndHOA2("Liabilites",financialyear,dept);

if(BNKBAL_List1.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0" border="1" class="yourID" rules="all">
<tr align="Left"><td style="text-align:center;font-weight:bold;" colspan="10">OUTSTANDING LIABILITIES/CORPUS FUND</td></tr>
<!-- <tr><td colspan="9"><table width="100%" cellpadding="0" cellspacing="0" class="yourID"> -->
<tr>
<td class="bg" width="5%" align="center" style="font-size: 13px;font-weight: bold;">S.No.</td>
<td class="bg" width="5%" style="font-size: 13px;font-weight: bold;">Financial Year</td>
<td class="bg" width="7%" style="font-size: 13px;font-weight: bold;">UniqId</td>
<td class="bg" width="7%" style="font-size: 13px;font-weight: bold;">Type</td>
<td class="bg" width="7%" style="font-size: 13px;font-weight: bold;">Credit/Debit</td>
<td class="bg" width="10%" style="font-size: 13px;font-weight: bold;">HOA</td>
<td class="bg" width="10%" style="font-size: 13px;font-weight: bold;">Major Head</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">Minor Head</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">Sub Head</td>
<!-- <td class="bg" width="6%">Amount</td> -->
<td class="bg" width="6%" style="font-size: 13px;font-weight: bold;">Debit Amount</td>
<td class="bg" width="6%" style="font-size: 13px;font-weight: bold;">Credit Amount</td>
</tr>
<!-- </table></td></tr> -->
<%for(int i=0; i < BNKBAL_List1.size(); i++ ){
	BNKBAL=(bankbalance)BNKBAL_List1.get(i); 
	%>
<tr>
<td  width="5%" align="center" style="font-size: 11px;font-weight: bold;"><%=i+1%></td>
<td width="5%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra1() %></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getUniq_id()%></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getType()%></td>
<td width="7%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL.getExtra7()!=null){if(BNKBAL.getExtra7().equals("credit")){%>CREDIT<%}else if(BNKBAL.getExtra7().equals("debit")){%>DEBIT<%}}%></td>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra2()%> <%=HODL.getHeadofAccountName(BNKBAL.getExtra2())%></td>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra3()%> <%=MJHDL.getmajorheadName(BNKBAL.getExtra3())%></td>
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra4()%> <%=MINL.getminorheadName(BNKBAL.getExtra4())%></td>
<%-- <td width="20%"><%=BNKBAL.getExtra5()%> <%=SUBL.getSuheadName(BNKBAL.getExtra5())%></td> --%> <!-- commented by madhav -->
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra5()%> <%=SUBL.getSuheadNameBasedOnMajorheadandMinorhead(BNKBAL.getExtra3(), BNKBAL.getExtra4(), BNKBAL.getExtra5())%></td>
<td width="6%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL.getExtra7().equals("debit")){%><%=decimalFormat.format(Double.parseDouble(BNKBAL.getBank_bal()))%><%}else{%>0.00<%}%></td>
<td width="6%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL.getExtra7().equals("credit")){%><%=decimalFormat.format(Double.parseDouble(BNKBAL.getBank_bal()))%><%}else{%>0.00<%}%></td>
</tr>	
<%}%>
</table>
<br/>
<%}


List BNKBAL_List2=BNK_BAL.getAssetsLiabilitiesOfFinYrAndHOA1("Liabilites",financialyear,dept);
if(BNKBAL_List2.size()>0){%>
<br>
<table width="100%" cellpadding="0" cellspacing="0" border="1" class="yourID" rules="all">
<tr align="left"><td style="text-align:center;font-weight:bold;" colspan="7"><strong>LIABILITES</strong></td></tr>
<!-- <tr><td colspan="7"><table width="100%" cellpadding="0" cellspacing="0" class="yourID"> -->
<tr>
<td class="bg" width="10%" align="center" style="font-size: 13px;font-weight: bold;">S.No.</td>
<td class="bg" width="10%" style="font-size: 13px;font-weight: bold;">Financial Year</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">UniqId</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">Type</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">HOA</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">Vendor</td>
<!-- <td class="bg" width="20%">Amount</td> -->
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">Debit Amount</td>
<td class="bg" width="20%" style="font-size: 13px;font-weight: bold;">Credit Amount</td>
</tr>
<!-- </table></td></tr> -->
<%for(int i=0; i < BNKBAL_List2.size(); i++ ){
	BNKBAL=(bankbalance)BNKBAL_List2.get(i);
	%>
<tr>
<td  width="10%" align="center" style="font-size: 11px;font-weight: bold;"><%=i+1%></td>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra1() %></td>
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getUniq_id()%></td>
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getType()%></td>
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra2()%> <%=HODL.getHeadofAccountName(BNKBAL.getExtra2())%></td>
<td width="20%" style="font-size: 11px;font-weight: bold;"><%=BNKBAL.getExtra6() %> <%=VEN.getMvendorsAgenciesName(BNKBAL.getExtra6()) %></td>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL.getExtra7().equals("debit")){%><%=decimalFormat.format(Double.parseDouble(BNKBAL.getBank_bal()))%><%}else{%>0.00<%}%></td>
<td width="10%" style="font-size: 11px;font-weight: bold;"><%if(BNKBAL.getExtra7().equals("credit")){%><%=decimalFormat.format(Double.parseDouble(BNKBAL.getBank_bal()))%><%}else{%>0.00<%}%></td>
</tr>	

<%} %>
</table>
<%}%> 
</div>
</div>
</div>
</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
