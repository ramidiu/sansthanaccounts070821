<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.bookingrooms"%>
<%@page import="beans.booking_roomsdetails"%>
<%@page import="beans.roomtype"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="mainClasses.roomtypeListing"%>
    <%@page import="java.util.List" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SAINIVAS REPORT</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style type="text/css">.menu ul li ul li ul.level-2{left:202px;}</style>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/shortcuts.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

	</script>
	<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
	
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script> 
<script src="js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="../resources/demos/style.css"/>

</head>
<body>

<%

    int sumofServiceCharges = 0;
    int sumofRoomRent = 0;
    int sumofCGST  = 0;
    int sumoftotal_amount = 0;
    int sumofSGST  = 0;
    
	 roomtype Roomtype  = new roomtype();	
     bookingrooms Bookingrooms = new bookingrooms();
	 mainClasses.roomtypeListing room_type = new mainClasses.roomtypeListing();
     mainClasses.bookingroomsListing bookingroomService = new mainClasses.bookingroomsListing();
    
     SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
	 SimpleDateFormat onlydat2=new SimpleDateFormat("dd-MMM-yyyy");
     String fromdate="";
     String todate="";
    
       if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
     	 fromdate=request.getParameter("fromDate")+" 00:00:01";
     }
       
     if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
     	todate=request.getParameter("toDate")+" 23:23:59";
     } 
//      System.out.println("55555555555");
//      System.out.println("fromdate >>>>>>"+fromdate);
//      System.out.println("todate >>>>>>"+todate);
     
    String fromDate = request.getParameter("fromDate");
    String toDate = request.getParameter("toDate");
    
    
    if(fromDate == null || fromDate.equals("null")){
    	fromDate = "";
    }
    
    if(toDate == null || toDate.equals("null")){
    	toDate = "";
    }	
%>

<div class=" mb-50 pt-50">
<form action="#" method="post">
<input type="hidden" name="page" value="total_sainivas_amounts"/>
<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:50px;margin-left:180px;">
<tr>
<td>From Date<input type="text" name="fromDate" id="fromDate" value="<%=fromDate %>" style="width: 250px; margin-right:10px" readonly/></td>
<td>To Date<input type="text" name="toDate" id="toDate" value="<%=toDate %>" style="width: 250px; margin-right:10px" readonly/></td>

<td><input type="submit" name="button" id="" value="submit"/></td>

</tr>
</table>
</form>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>


<% 
 if(fromdate != null && !fromdate.equals("") && todate != null && !todate.equals("")) 
     {
	 %>
		 <div class="printable" >
<div class="saiNivas">
<div class="saiNivas_amounts">	
<table width="95%" cellpadding="0" cellspacing="0">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">SAINIVAS TOTAL AMOUNTS</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=onlydat2.format(onlydat.parse(fromdate.substring(0, 10))) %> TO <%=onlydat2.format(onlydat.parse(request.getParameter("toDate"))) %> </td></tr>
				</table>
			
</div>
</div>



   

<!-- <div class="total-amounts" style="margin-top:50px;">
	<div class="container">
	 	<div class="row">
	 		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 			<div class="table-responsive"> -->
	 		
	 				<table width="100%" class="table table-bordered" cellpadding="0" cellspacing="0"  border="1"  id="tblExport">
	 					<thead>
	 					<tr style="background-color:#8b421b;color:#fff;">
	 					    <th>S.No</th>
	 					    <th>GSTIN/UIN of Recipient / Unregistered</th>
	 					    <th>Invoice Number</th>
	 					    <th>Invoice date</th>
	 					    <th>Taxable Value</th>
	 					    <th>State Of Supply</th>
	 					    <th>TAX Rate</th>
	 					    <th>IGST</th>
	 						<th>CGST</th>
	 						<th>SGST</th>
	 	</tr>
	 	</thead>
	 <tbody>
	 					
	 <%
	bookingrooms BookingRooms  =null;
	List<roomtype> roomTypeList = room_type.getroomtypeID("active");
	int k=0; 				
	 if(roomTypeList.size()>0) {
	 for(int i=0; i < roomTypeList.size();i++ ) {
	 Roomtype = roomTypeList.get(i);	
	 %>
 <tr>
		 						      
<td colspan="20" class="td_heading_new" align="center" style="background-color:yellow;font-weight:bold" ><%=Roomtype.getroomtype_name() %></tds>
</tr>
	<% 	
	List<bookingrooms> list=bookingroomService.getBookingRoomListing(Roomtype.getroomtype_id(),fromdate,todate);
	
	for(int j=0;j<list.size();j++){
		 %>
		<tr>
		<td><%=k+1%></td>
		<td><%="Unregistered"%></td>
		<td><%=list.get(j).getbooking_id()%></td>
		<td><%=list.get(j).getcurrent_date()%></td>
		<td><%=list.get(j).gettotal_amount()%></td>
		<td><%="27-Maharashtra"%></td>
		<td>
		<% if(Roomtype.getroomtype_name().equals("Master Suite") || Roomtype.getroomtype_name().equals("Executive Suite")){%>
		<%="18%"%></td>
		<% }else if(Roomtype.getroomtype_name().equals("Deluxe Suite") || Roomtype.getroomtype_name().equals("Suite")){%>
		<%="12%"%></td>
		<%}else{ %>
		<%="0%"%></td>
		<%} %>
		<td><%=""%></td>
		<td> <%  int gst = 0;
		 if(list.get(j).gettrans() != null && !list.get(j).gettrans().trim().equals("")){ 
			 gst = Integer.parseInt(list.get(j).gettrans()) / 2 ;
				 }
	%>
	<%=gst %>
	</td>
		<td><%=gst%></td>
		</tr> 
		 
	 <% k=k+1;}
	 }
	 }
    
	
	 
	 %>
	
	 
	 <%
     }
	%>	 					
	 					
	 					</tbody>
	 				</table>
	 				</div>
	 			<!-- </div>	
	 		</div>
	 	</div>	
	</div>
</div> -->
			
</body>
</html>
				 					
				 					
				 					
				 						
		 			