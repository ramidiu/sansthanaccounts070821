<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>POOJA STORE STOCK REPORT</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});

function productsearch(){

	  var data1=$('#productname').val();
	  var headID="3";
	  $.post('searchProducts.jsp',{q:data1,page:"",hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
	</script>
	
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 							// Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SHP" class="beans.products" />
		<jsp:useBean id="PRD" class="beans.products" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form action="adminPannel.jsp" method="post">  
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" name="page" value="poojaStore-StockReport"></input>
						<input type="text" name="productname" id="productname" <%if(request.getParameter("productname")!=null){ %> value="<%=request.getParameter("productname") %>" <%}else{ %> value="" <%} %> onkeyup="productsearch()" placeholder="Find product here" autocomplete="off"/></li>
						<!-- <li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li> -->
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div>
				</form>
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
					<span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details" style="width:100% !important;">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	shopstockListing SHPL=new shopstockListing();
	productsListing PRD_l=new productsListing ();
	DecimalFormat df = new DecimalFormat("0.00");
	 List SHP_STOCKL=SHPL.getpresentShopStock(request.getParameter("productname"));
	 List PRODL=PRD_l.getPresentShopStock(request.getParameter("productname"),"3");
	 double totQty=0;
	 double totgrossAmt=0.00;
	 double totGodownQty=0;
	 double totGodownGrossAmt=0.00;
	 if(SHP_STOCKL.size()>0){%>
	  <div class="printable">
          <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExpor" border='0'>
				<tr><td colspan="8"><table width="100%" cellpadding="0" cellspacing="0" id="tblExport" class="yourID">
	<tr>
							<td class="bg"  style="font-weight: bold;" align="center"  colspan="2">PRODUCT INFO</td>
							<td class="bg"  style="font-weight: bold;" align="center" colspan="3">GODOWN STOCK INFO</td>
							<td class="bg"  style="font-weight: bold;" align="center" colspan="3">SHOP STOCK INFO</td>
							
							<!-- <td class="bg" width="23%">Product Qty</td>
							<td class="bg" width="23%">Amount</td> -->
							</tr>
						<tr>
							<td class="bg" width="5%" style="font-weight: bold;">CODE</td>
							<td class="bg" width="20%" style="font-weight: bold;">PRODUCT NAME</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">PURCHASE PRICE</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">GODOWN QTY</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">GROSS AMOUNT</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">SALE PRICE</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">SHOP QTY</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">GROSS AMOUNT</td>
							
							<!-- <td class="bg" width="23%">Product Qty</td>
							<td class="bg" width="23%">Amount</td> -->
							</tr>
</table></td></tr>
						<%double proQty=0.00;
						double amt=0.00;
						double godownQty=0.00;
						double godownGrossAmt=0.00;
						for(int i=0; i < PRODL.size(); i++ ){
							SHP=(beans.products)PRODL.get(i);
							if(SHP.getbalanceQuantityStore()!=null && !SHP.getbalanceQuantityStore().equals("")){
								proQty=Double.parseDouble(SHP.getbalanceQuantityStore());
								
							}
							if(SHP.getbalanceQuantityGodwan()!=null && !SHP.getbalanceQuantityGodwan().equals("")){
								godownQty=Double.parseDouble(SHP.getbalanceQuantityGodwan());
							}
							if(SHP.getsellingAmmount()!=null && !SHP.getsellingAmmount().equals("")){
								amt=Double.parseDouble(SHP.getsellingAmmount())*Double.parseDouble(SHP.getbalanceQuantityStore());
							}
							if(SHP.getpurchaseAmmount()!=null && !SHP.getpurchaseAmmount().equals("")){
								godownGrossAmt=Double.parseDouble(SHP.getpurchaseAmmount())*godownQty;
							}
							/* if(PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString())!=null && !PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString()).equals("")){
								proQty=Integer.parseInt(PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString()));
							}
							if(PRD_l.getProductsAmount(SHP.getproductId(),session.getAttribute("headAccountId").toString())!=null && PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString())!=null && !PRD_l.getProductsAmount(SHP.getproductId(),session.getAttribute("headAccountId").toString()).equals("") && !PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString()).equals("")){
								amt=Double.parseDouble(PRD_l.getProductsAmount(SHP.getproductId(),session.getAttribute("headAccountId").toString()))*Double.parseDouble(PRD_l.getProductsBalanceQty(SHP.getproductId(),session.getAttribute("headAccountId").toString()));
							} */
							totQty=(totQty+proQty);
							totgrossAmt=totgrossAmt+amt;
							totGodownQty=totGodownQty+godownQty;
							totGodownGrossAmt=totGodownGrossAmt+godownGrossAmt;
							%>
						<tr>
							<td width="7%" align=""><%=SHP.getproductId()%></td>
							<td  width="12%" align=""><%=SHP.getproductName()%></td>
							<td width="12%" align="left"> <%=SHP.getpurchaseAmmount()%></td>
							<td width="12%" align="">
							<%if(!SHP.getbalanceQuantityGodwan().equals("") && Double.parseDouble(SHP.getbalanceQuantityGodwan())>0){ %>
							<span><%=SHP.getbalanceQuantityGodwan()%></span> <%}else{ %><span style="color: red;"><%=SHP.getbalanceQuantityGodwan()%></span> <%} %> </td>
							<td width="12%" align=""> <%=df.format(godownGrossAmt) %></td>
							
							<td width="12%" align="center"> <%=SHP.getsellingAmmount()%></td>
							<td width="12%" align="center"><%if(!SHP.getbalanceQuantityStore().equals("")&&Double.parseDouble(SHP.getbalanceQuantityStore())>0){ %><span><%=SHP.getbalanceQuantityStore()%></span><%}else{ %> <span style="color: red;"><%=SHP.getbalanceQuantityStore()%></span> <%} %></td>
							<td width="12%" align="center"> <%=df.format(amt) %></td>
							
							<%-- <td width="4%" align="center"></td><td> <%=SALE.gettotalAmmount()%></td> --%>
						</tr>
						<%} %>
						<tr>
						<td class="bg" colspan="3"></td>
						<td class="bg" align="left"><%=totGodownQty %></td>
						<td class="bg" align="left" style="font: bold;"> <%=df.format(totGodownGrossAmt) %></td>
						<td class="bg"></td>
						<td class="bg" align="center"><%=totQty %></td>
						<td class="bg" align="center" style="font: bold;"> <%=df.format(totgrossAmt) %></td>
						<!--<td></td>-->
						</tr>
					</table>
					</div>
					<%}else{%>
					<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no products found in shop!</span></div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>