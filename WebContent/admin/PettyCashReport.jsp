<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.banktransactionsListing,java.util.Date"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<!--Date picker script  -->
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
	<%

	String bankId=request.getParameter("id");
	mainClasses.bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	List BNK_List=BNK_CL.getMbankdetails(bankId); 
	if(BNK_List.size()>0){
		BNK=(beans.bankdetails)BNK_List.get(0);
			%>
<div class="vendor-page">
<div class="vendor-box">
<div class="name-title"><%=BNK.getbank_name()%> <span>(<%=HOA_CL.getHeadofAccountName(BNK.getheadAccountId())%>)</span></div>
<div class="click floatleft"><a href="./editBankDetails.jsp?id=<%=BNK.getbank_id()%>" target="_blank" onclick="openMyModal('editBankDetails.jsp?id=<%=BNK.getbank_id()%>'); return false;">Edit</a></div>

<%-- <div class="click floatleft" style="margin-left: 20px;"><a href="deleteBank.jsp?id=<%=BNK.getbank_id()%>" >Delete</a></div> --%>
<div class="click floatleft" style="margin-left: 20px;"><a href="editBankDetails.jsp?id=<%=BNK.getbank_id()%>" target="_blank" onclick="openMyModal('banktransactionsForm.jsp?id=<%=BNK.getbank_id()%>'); return false;">Deposit</a></div>

<div style="clear:both;"></div>
<div class="details-list">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="2"><span>Branch Name:</span><%=BNK.getbank_branch()%></td>
</tr>

<tr>
<td colspan="2"><span>Account Holder Name:</span><%=BNK.getaccount_holder_name()%></td>
</tr>

<tr>
<td colspan="2"><span>Account Number:</span><%=BNK.getaccount_number()%></td>
</tr>

<tr>
<td colspan="2"><span>IFSC Code:</span><%=BNK.getifsc_code()%></td>
</tr>

<tr>
<td colspan="2"><span>Total Amount:</span><%=BNK.gettotal_amount()%></td>
</tr>

</table>

</div>
<div class="details-amount">

 <ul>
<li class="colr">&#8377;<%=BNK.gettotal_amount()%><br>
 <span>CLOSING BALANCE</span></li>
 
  <li class="colr1">&#8377;<%if(BNK_CL.getPettyCashAmount(bankId)!=null && !BNK_CL.getPettyCashAmount(bankId).equals("")){%><%=BNK_CL.getPettyCashAmount(bankId)%><%}else{ %>00<%} %><br>
 <span>PETTY CASH AMOUNT</span></li>
</ul>


</div>
</div>

<div class="vendor-list">
<div class="trans">Bank Transactions Report</div>
<div class="clear"></div>
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
			<form action="adminPannel.jsp" method="post">
				<input type="hidden" name="page" value="<%=request.getParameter("page")%>">
				<input type="hidden" name="id" value="<%=request.getParameter("id")%>">
				<div class="search-list">
					<ul>
						<li><input type="text"  name="fromdate" id="fromDate" class="DatePicker" value=""  readonly="readonly" /></li>
						<li><input type="text" name="todate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="10%">DATE</td>
<td class="bg" width="10%">VOUCHER</td>
<td class="bg" width="10%">ACCOUNT</td>
<td class="bg" width="20%">NARRATION</td>
<td class="bg" width="15%" align="right">DEBIT</td>
<td class="bg" width="15%" align="right">CREDIT</td>
<td class="bg" width="20%" align="right">BALANCE</td>
</tr>
<%
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
DecimalFormat df = new DecimalFormat("0.00");
//Calendar cal = Calendar.getInstance();

c1.set(Calendar.DATE, 1);
Date firstDateOfPreviousMonth = c1.getTime();
	String fromDate=(dateFormat2.format(c1.getTime())).toString();
		   fromDate=fromDate+" 00:00:01";
c1.set(Calendar.DATE, c1.getActualMaximum(Calendar.DATE)); 
Date lastDateOfPreviousMonth = c1.getTime();
	String toDate=(dateFormat2.format(c1.getTime())).toString();
			toDate=toDate+" 59:00:00";
//System.out.println("fromDate::"+fromDate+":::todate"+toDate);
	if(request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals("")){
		fromDate=request.getParameter("fromdate")+" 00:00:01";
	}
	if(request.getParameter("todate")!=null && !request.getParameter("todate").equals("")){
		toDate=request.getParameter("todate")+" 59:00:00";
	}
banktransactionsListing BAK_L=new mainClasses.banktransactionsListing(); 
employeesListing EMP=new employeesListing();
//List BANKL=BAK_L.getMbanktransactionsId(bankId);
List BANKL=BAK_L.getPettyCashTransactions(bankId,fromDate,toDate);
SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy");
double debitBal=0.00;
double creditBal=0.00;

double openingBal=0.00;
if(BAK_L.getOpeningBalance(bankId,fromDate,"")!=null && !BAK_L.getOpeningBalance(bankId,fromDate,"").equals("")){
	openingBal=Double.parseDouble(BAK_L.getOpeningBalance(bankId,fromDate,""));
}

%>
<tr>
<td colspan="4" align="right" style="font-weight: bold;color: orange;">OPENING BALANCE | </td>
<td colspan="1" style="font-weight: bold;" align="right"><%=df.format(openingBal)%></td>
<td colspan="1" align="right">00</td>
<td colspan="1" align="right"><%=openingBal%></td>
<td></td>
</tr>
<%
if(BANKL.size()>0){
	for(int i=0;i<BANKL.size();i++){
	BKT=(beans.banktransactions)BANKL.get(i);	
%>
<tr>
<td><%=CDF.format(dateFormat.parse(BKT.getdate())) %></td>
<td><%=BKT.getbanktrn_id() %></td>
<td><%=BKT.gettype() %></td>
<td><%=BKT.getnarration() %></td>
<td align="right"><%if(BKT.gettype().equals("deposit")){
	debitBal=debitBal+Double.parseDouble(BKT.getamount());
	openingBal=openingBal+Double.parseDouble(BKT.getamount());
	%><%=BKT.getamount()%> <%}%></td>
<td align="right"> <%if(BKT.gettype().equals("withdraw") || BKT.gettype().equals("pettycash") || BKT.gettype().equals("cash")){
	creditBal=creditBal+Double.parseDouble(BKT.getamount());
	openingBal=openingBal-Double.parseDouble(BKT.getamount());%> <%=BKT.getamount()%> <%}%></td>
<td align="right"><%=openingBal %></td>
</tr>
<%}} %>
<tr>
<td colspan="4" align="right" style="font-weight: bold;color: orange;">CLOSING BALANCE | </td>
<td colspan="1" style="font-weight: bold;" align="right"><%=df.format(debitBal) %></td>
<td colspan="1" style="font-weight: bold;" align="right"><%=df.format(creditBal) %></td>
<td colspan="1" style="font-weight: bold;" align="right"><%=df.format(debitBal-creditBal)%></td>
<td></td>
</tr>
</table>
</div>
</div>
</div>
<%}else{response.sendRedirect("adminPannel.jsp?page=banks");}%>

