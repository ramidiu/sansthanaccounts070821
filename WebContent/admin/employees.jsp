<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<jsp:useBean id="EMP" class="beans.employees"/>
<div class="vendor-page">
                 <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">VENDORS </td></tr>
</table>
<div class="click floatright"><a href="./newEmployee.jsp" target="_blank" onclick="openMyModal('newEmployee.jsp'); return false;">New Employee</a></div>
<%mainClasses.employeesListing EMP_CL = new mainClasses.employeesListing();
List EMP_List=EMP_CL.getemployees();
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); %>
<%-- <div class="vendor-box">
<div class="vendor-title">Employees</div>
<div class="click floatright"><a href="./newEmployee.jsp" target="_blank" onclick="openMyModal('newEmployee.jsp'); return false;">New Employee</a></div>
<div style="clear:both;"></div>

<div class="unpaid-box">
<div class="unpaid">Unpaid</div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OPEN BILL</li>
</ul>
</div>

<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OVERDUE</li>
</ul>
</div>
</div>
</div>
</div>
<div class="paid-box">
<div class="unpaid">Paid</div>
<div class="bills-cash">
<div class="bill-paid">
 <ul>
<li class="bill-amount"><%=EMP_List.size()%></li>
 <li>Employees</li>
</ul>
</div>
</div>
</div>
</div>
 --%>
<div class="vendor-list">
<!-- <div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div> -->
<div class="clear"></div>
<div class="list-details">
<%
if(EMP_List.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">

<tr>
<td colspan="5">
<table width="100%" cellpadding="0" cellspacing="0" class="yourID" >
<tr><td class="bg" width="3%"><input type="checkbox"></td>
<td class="bg" width="23%">Employee Name</td>
<td class="bg" width="23%">EMAIL</td>
<td class="bg" width="20%">Salary</td>
<td class="bg" width="10%">Joining Date</td></tr></table></td>

</tr>
<%for(int i=0; i < EMP_List.size(); i++ ){
	EMP=(beans.employees)EMP_List.get(i); %>
<tr>
<td width="5%"><input type="checkbox"></td>
<td width="23%"><ul>
<li><span><a href="adminPannel.jsp?page=employeeDetails&id=<%=EMP.getemp_id()%>"><%=EMP.getfirstname()%> <%=EMP.getlastname()%></a></span><br />
<span><%=HOA_CL.getHeadofAccountName(EMP.getheadAccountId()) %></span></li>
<%if(!EMP.getemail().equals("")){%>
<li><img src="../images/w.png" align="center" style="vertical-align:central"/></li>
<%}%>
</ul></td>
<td width="23%"><%=EMP.getemail()%></td>
<td width="20%"><%=EMP.getemp_sal()%></td>
<td width="10%"><%=EMP.getjoin_date()%></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>No Employees Added Yet</h1></div>
<%}%>


</div>
</div>




</div>
