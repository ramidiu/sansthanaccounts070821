<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Expenses entry</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>

<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#dateError').hide();
	$('#indentError').hide();
	$('#bankError').hide();
	$('#vendorError').hide();
/* 	$('#nameError').hide(); */

	if($('#date').val().trim()==""){
		$('#dateError').show();
		$('#date').focus();
		return false;
	}
	/* if($('#vendorId').val().trim()==""){
		$('#vendorError').show();
		$('#vendorId').focus();
		return false;
	} */
	/* if($('#paymentType').val().trim()=="cheque"){
	if($('#bankId').val().trim()==""){
		$('#bankError').show();
		$('#bankId').focus();
		return false;
	}
	} */
	if($('#major_head_id').val().trim()==""){
		$('#indentError').show();
		$('#major_head_id').focus();
		return false;
	}
	
	/* 
	if($('#nameOnCheck').val().trim()==""){
		$('#nameError').show();
		$('#nameOnCheck').focus();
		return false;
	} */
	if(($('#product0').val().trim()=="" || $('#product0').val().trim()=="")){
		$('#product0').addClass('borderred');
		$('#product0').focus();
		return false;
	}
	$.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    }}); 
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="../js/jquery.blockUI.js"></script>
 <script>
$(document).ready(function(){

	  /*  function majorHeadSearch(j){
		   
		   alert("function is calling");
	  var data1=$("#major_head_id"+j).val().split(" ");
	  var hoid=$("#head_account_id").val();
	  $.post('searchMajor.jsp',{q:data1[0],HAid :hoid},function(data)
				 {
		  alert("data:::"+data);
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#major_head_id"+j).autocomplete({source: availableTags}); 
			});
  } */
	
  /* $("#minor_head_id").keyup(function(){
	  var data1=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#minor_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#sub_head_id").keyup(function(){
	  var data1=$("#sub_head_id").val();
	  var minor=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#sub_head_id" ).autocomplete({source: availableTags}); 
			});
  });
   */
  
  
  $("#bankId").keyup(function(){
	  var hoid=$("#head_account_id").val();
	  var data1=$("#bankId").val();
	  $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data)
	  {
		 var response = data.trim().split("\n");
		 var bankNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 bankNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=bankNames;
		 $( "#bankId" ).autocomplete({source: availableTags}); 
			});
  });
});
</script>
<script>
function majorheadsearch(j){
	var data1=$('#major_head_id'+j).val().split(" ");
	
	  var hoid=$("#head_account_id").val();
	  
	  $.post('searchMajor.jsp',{q:data1[0],HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 		 
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
			
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#major_head_id'+j).autocomplete({
			source: availableTags,
			 focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val("");
				$(this).val(ui.item.label);
			}
		});  
			});
}
</script>
<script>
function minorheadsearch(j){
		  var data1=$('#minor_head_id'+j).val();
		  var major=$('#major_head_id'+j).val();
		  var hoid=$("#head_account_id").val();
		  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			$('#minor_head_id'+j).autocomplete({
				source: availableTags,
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
						$(this).val("");
					$(this).val(ui.item.label);
				}
			}); 		
	  });
}
</script>

<script>
function combochange(denom,desti,jsppage) { 
    var com_id = denom; 
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	$.post(jsppage,{ id: com_id } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");
	
				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
				sda1.add(y,null);
				}
				catch(e)
				{
				sda1.add(y);
				}
	}
	
	}
	}); 
}
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function vendorsearch(){
	  var hoid=$('#head_account_id').val();
	  var data1=$('#vendorId').val();
	  var arr = [];
	  $.post('searchVendor.jsp',{q:data1,b : hoid},function(data)
	{
		var response = data.trim().split("\n");
		var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
				 doctorIds:d[0],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		 $( '#vendorId').autocomplete({source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},	 
		 }); 
			});
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var arr = [];
	  //var data1=$("#sub_head_id").val();
	  var minor=$("#minor_head_id"+j).val();
	  var major=$("#major_head_id"+j).val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
} 
function calculate(i){

	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;
	u=document.getElementById("addcount").value;	
	if(document.getElementById("product"+i).value!=""){
		if(document.getElementById("purchaseRate"+i).value!="")
		{
				for(var j=0;j<u;j++){			
				if(document.getElementById("purchaseRate"+j).value!=""){
						grossprice=Number(parseFloat(document.getElementById("purchaseRate"+j).value)).toFixed(2);
							Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice)).toFixed(2);			
				}
				}
				document.getElementById("totalamont").value=Totalprice;
		}
	}
	}
function addOption(){
	$("#department").empty();
	/* $("#vendorId").val(" "); */
	var hoaid=$("#HOA").val();
	if(hoaid=="3"){
		$("#department").append('<option value=godown>Godown</option>');
		/* $("#department").append('<option value=shop>Shop</option>'); */
	}else if(hoaid=="4"){
		$("#department").append('<option value=santhanstores>Sansthan stores</option>');
	}
}
function chequeDetails(){
    var payType=$("#paymentType").val();
    if(payType=="cheque"){
    	$("#chequeDetails").show();
    }else{
    	$("#chequeDetails").hide();
    }
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="GDS" class="beans.godwanstock"/>
<jsp:useBean id="PDE" class="beans.productexpenses"></jsp:useBean>

<%if(session.getAttribute("adminId")!=null){
	String expInv_id=request.getParameter("invid");
String expenseflag=request.getParameter("expflag");
%>
<form name="tablets_Update" method="post" action="paymentsApprovedExpenses_update.jsp" onsubmit="return validate();">
<div class="vendor-page">

<div class="vendor-box">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Expenses Approvals Require List</td></tr>
</table>

<div class="vender-details">
<%
bankdetailsListing BNK_L=new bankdetailsListing();
majorheadListing MAJ_L=new majorheadListing();
minorheadListing MIN_L=new minorheadListing();
productexpensesListing PRDEXP_L=new productexpensesListing();
godwanstockListing GDS_L=new godwanstockListing();
 String expinvid=request.getParameter("expinvid"); 
List PRDEXP=PRDEXP_L.getMproductexpensesBasedOnExpInvoiceID(expinvid);
PDE=(productexpenses)PRDEXP.get(0);
%>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%"> <div class="warning" id="dateError" style="display: none;">
<input type="hidden" name="invoiceId" value="<%=request.getParameter("invid")%>">
</input>
<input type="hidden" name="expinvoice_id" value="<%=expinvid%>"/>
Please select date.</div>Date<span style="color: red;">*</span></td>
<td>Head account <span style="color: red;">*</span></td>
<td><div class="warning" id="bankError" style="display: none;">Please select a bank.</div>Bank</td>
<td ><div class="warning" id="vendorError" style="display: none;">Please select vendor.</div>Vendor</td>

</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<tr>
<td>
<input type="hidden" name="mseNum" id="mseNum" value="<%=PDE.getextra5()%>" />
<input type="text" name="date" id="date" value="<%=currentDate%>" /></td>

<td><select name="head_account_id" id="head_account_id" value="<%=PDE.getsub_head_id() %>" >
<%
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
productexpensesListing PRDE_L=new productexpensesListing();
subheadListing SUBH_L=new subheadListing();
productsListing PRD_L=new productsListing();
vendorsListing VENL=new vendorsListing();
List HA_Lists=HOA_L.getheadofaccounts();
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(PDE.gethead_account_id().equals(HOA.gethead_account_id())){ %> selected="selected"  <%} %> ><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><input type="text" name="bankId" id="bankId" value="<%=PDE.getextra3() %><%=BNK_L.getBankName(PDE.getextra3())%>" onkeyup="banksearch()" placeholder="find a bank here" autocomplete="off"/></td>
<td><input type="text" name="vendorId"  value="<%=PDE.getvendorId()%> <%=VENL.getMvendorsAgenciesName(PDE.getvendorId())%>" onkeyup="vendorsearch()" placeholder="find a vendor here" autocomplete="off" readonly="readonly" /></td>
</tr>
<tr>
<!-- <td><div class="warning" id="indentError" style="display: none;">Please select major head.</div>Major head<span style="color: red;">*</span></td>
<td><div class="warning" id="billError" style="display: none;">Please select minor head.</div>Minor head</td> -->
<td>Narration :</td>
<td></td>
</tr>
<tr>
<%-- <td><input type="text" name="major_head_id" id="major_head_id" value="<%=PDE.getmajor_head_id() %> <%=MAJ_L.getmajorheadName(PDE.getmajor_head_id()) %>" placeholder="find a major head here" autocomplete="off" style="vertical-align: top"/></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value="<%=PDE.getminorhead_id() %> <%=MIN_L.getminorheadName(PDE.getminorhead_id()) %>"  placeholder="find a minor head here" autocomplete="off" style="vertical-align: top"/></td> --%>
<td>
<textarea name="narration" id="narration" style="height: 50px;" placeholder="narration" ><%=PDE.getnarration() %></textarea>
<!-- <select name="paymentType" id="paymentType" onchange="chequeDetails();">
	<option value="cashpaid">Cash</option>
	<option value="cheque">Cheque</option>
</select> -->
</td>
<td></td>
</tr>
<tr>
<td colspan="4">
	<table id="chequeDetails" style="display: none;" width="70%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td>Cheque Number<span style="color: red;">*</span></td>
		<td>Name on cheque<span style="color: red;">*</span></td>
		<td></td>
		</tr>
		<tr>
		<td><input type="text" name="chequeNo" id="chequeNo"   maxlength="6"  value=""></input></td>
		<td><input type="text" name="nameOnCheque" id="nameOnCheque" value=""></input></td>
		<td></td>
		</tr>
	</table> 

</td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list">
<div class="sno1 bgcolr" style="width:10%">S.No.</div>
<div class="sno1 bgcolr"  style="width:20%">Major Head</div>
<div class="sno1 bgcolr"  style="width:20%">Minor Head</div>
<div class="ven-nme1 bgcolr" style="width:30%">Sub Head</div>
<!-- <div class="ven-nme1 bgcolr">Narration</div> -->
<div class="ven-amt1 bgcolr" style="width:2%">Amount</div>
<div class="ven-amt1 bgcolr" style="width:5%">Product Name</div>
<div class="clear"></div>
<input type="hidden" name="expscount" id="expscount" value="<%=PRDEXP.size()%>"/>
<%
double totAmt=0.00;
subheadListing SUBL=new subheadListing();
DecimalFormat df=new DecimalFormat("#0.00");
for(int i=0; i < PRDEXP.size(); i++ ){
	PDE=(productexpenses)PRDEXP.get(i);
	totAmt=totAmt+Double.parseDouble(PDE.getamount());
%>
<div <%-- <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>" --%>>

<div class="sno1" style="width:10%"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<input type="hidden" name="exp_id<%=i %>"   value="<%=PDE.getexp_id() %>"/>
<div class="ven-nme1" style="width:20%"><input type="text" name="major_head_id<%=i%>" id="major_head_id<%=i%>" value="<%=PDE.getmajor_head_id() %> <%=MAJ_L.getmajorheadName(PDE.getmajor_head_id())%>" onkeyup="majorheadsearch(<%=i%>)" autocomplete="off"/></div>
<div class="ven-nme1" style="width:20%"><input type="text" name="minor_head_id<%=i%>" id="minor_head_id<%=i%>" value="<%=PDE.getminorhead_id() %> <%=MIN_L.getminorheadName(PDE.getminorhead_id()) %>"  onkeyup="minorheadsearch(<%=i%>)"  autocomplete="off"/></div>
<div class="ven-nme1" style="width:30%"><input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>')" class="" value="<%=PDE.getsub_head_id() %> <%=SUBH_L.getMsubheadnameWithDepartment(PDE.getsub_head_id(), PDE.gethead_account_id()) %>" autocomplete="off" /></div>
<%-- <div class="ven-nme1"><input type="text" name="description<%=i%>" id="description<%=i%>"  value="<%=GDS.getdescription()%>" readonly="readonly"></input></div> --%>
<div class="ven-nme1" style="width:5%">&#8377;<input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" value="<%=df.format(Double.parseDouble(PDE.getamount()))%>" onkeypress="return numbersonly(this, event,true);" onkeyup="calculate('<%=i %>');" style="width: 120px;" ></input></div>
<div class="ven-nme1" style="width:0%"><input type="text" name="billproduct<%=i%>" id="billproduct<%=i%>"value="<%=PDE.getextra2()%> <%=PRD_L.getProductsNameByCat(PDE.getextra2(),PDE.gethead_account_id())%>" readonly="readonly"/></div>
<div class="clear"></div>
</div>
<%}if(GDS.getExtra8()!=null && !GDS.getExtra8().equals("")){
	%>
	<div style=" width: 380px; text-align: right; border-top: 1px solid #000;padding: 10px 0; margin-top: 10px;">Other Charges :  <span id="totAmt"><%=df.format(GDS.getExtra8()) %> </span></div>
	<%	totAmt=Double.parseDouble(GDS.getExtra8())+totAmt;
	}%>
	<input type="hidden" name="addcount" id="addcount" value="<%=PRDEXP.size()%>"/>
<div class="ven-nme1" align="right"><span class='bold'>Total Amount :</span> </div>
<div class="ven-nme1" align="right" ><input type="text" name="totalamont" id="totalamont" class="" value="<%=df.format(totAmt) %>" readonly="off" autocomplete="off"/></div>
<div class="ven-nme1" align="right"><input type="submit" value="APPROVE"  /></div>
</div>

</div>
</form>
<script>
window.onload=function a(){
		addOption();
	};
</script>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>