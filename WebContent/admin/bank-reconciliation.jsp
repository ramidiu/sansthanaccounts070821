<%@page import="mainClasses.paymentsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.banktransactionsListing,java.util.Date"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>


<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$(".date").datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
$(document).ready(function(){
$( "#reportType" ).change(function() {
	  $( "#searchForm" ).submit();
	});
	
$("#id").keyup(function(){
	  var hoid=$("#head_account_id").val();
	  var data1=$("#id").val();
	  $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data)
	  {
		 var response = data.trim().split("\n");
		 var bankNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 bankNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=bankNames;
		 $( "#id" ).autocomplete({source: availableTags}); 
			});
});
$( "#formsubmit" ).click(function() {
	  $( "#reconciliationForm" ).submit();
	});
});
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<!--Date picker script  -->
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
<jsp:useBean id="NUM" class="beans.NumberToWordsConverter"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
	
<div class="vendor-page">
<div class="vendor-box">
<form name="bank" action="adminPannel.jsp" method="post">
<input type="hidden" name="page" value="bank-reconciliation">
<div class="name-title">Bank Reconciliation report</div><br><br><br>
<div class="name-title">
<select name="head_account_id" id="head_account_id">
<%
paymentsListing PAYL=new paymentsListing();
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select>

<input type="text" name="id" id="id" value="" placeHolder="Find a bank here" style="height: 22px;"/> <input type="submit" name="submit" value="Search"></div><br><br><br>
</form>
<%if(request.getParameter("id")!=null && !request.getParameter("id").equals("")){ %>
<%	String bankSJID[]=request.getParameter("id").split(" ");
	String bankId=bankSJID[0];
	DecimalFormat formatter = new DecimalFormat("#,###.00");
	DecimalFormat sjf = new DecimalFormat("####");
	mainClasses.bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	List BNK_List=BNK_CL.getMbankdetails(bankId); 
	if(BNK_List.size()>0){
		BNK=(beans.bankdetails)BNK_List.get(0);%>
<div class="name-title"><%=BNK.getbank_name()%> <span>(<%=HOA_CL.getHeadofAccountName(BNK.getheadAccountId())%>)</span></div>

<div style="clear:both;"></div>
<div class="details-list">
<table cellpadding="0" cellspacing="0" width="100%" >
<tr>
<td><span>Branch Name:</span></td>
<td><%=BNK.getbank_branch()%></td>
</tr>

<tr>
<td><span>Account Holder Name:</span></td>
<td><%=BNK.getaccount_holder_name()%></td>
</tr>

<tr>
<td><span>Account Number:</span></td>
<td><%=BNK.getaccount_number()%></td>
</tr>

<tr>
<td ><span>IFSC Code:</span></td>
<td><%=BNK.getifsc_code()%></td>
</tr>

<tr>
<td ><span>Total Amount:</span></td>
<td><%=BNK.gettotal_amount()%></td>
</tr>

</table>

</div>
<div class="details-amount">

 <ul>
<li class="colr">&#8377;<%=formatter.format(Double.parseDouble(BNK.gettotal_amount()))%><br>
 <%-- <%=NUM.convert(Integer.parseInt(sjf.format(Double.parseDouble(BNK.gettotal_amount())))) %><br> --%>
 <span>CLOSING BALANCE</span></li>
 
  <li class="colr1">&#8377;<%if(BNK_CL.getPettyCashAmount(bankId)!=null && !BNK_CL.getPettyCashAmount(bankId).equals("")){%><%=formatter.format(Double.parseDouble(BNK_CL.getPettyCashAmount(bankId)))%><%}else{ %>00<%} %><br>
 <span>PETTY CASH AMOUNT</span></li>
</ul>


</div>
</div>

<div class="vendor-list">

<div class="clear"></div>
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
			<form id="searchForm" action="adminPannel.jsp" method="POST">
				<input type="hidden" name="page" value="<%=request.getParameter("page")%>">
				<input type="hidden" name="id" value="<%=request.getParameter("id")%>">
				<div class="search-list">
					<ul>
					<li><select name="reportType" id="reportType">
						<option value="bank" selected="selected">Bank</option>
						<option value="cashpaid" <%if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %> selected="selected" <%} %>>Petty Cash</option>
					</select></li>
						<li><input type="text"  name="fromdate" id="fromDate" class="DatePicker" value=""  readonly="readonly" /></li>
						<li><input type="text" name="todate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>

<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<div class="list-details" style="width:100%;">
<table width="100%" cellpadding="0" cellspacing="0" style="" >
<tr>
<td colspan="9" style="">

<table width="100%" cellpadding="0" cellspacing="0" class="yourID" >
<tr><td class="bg" width="8%" align="center">DATE</td>
<td class="bg" width="8%" align="center">VOUCHER</td>
<td class="bg" width="8%" align="center">ACCOUNT</td>
<td class="bg" width="8%" align="center">CHEQUE NO</td>
<td class="bg" width="20%" align="center">NARRATION</td>
<td class="bg" width="10%" align="center">DEBIT</td>
<td class="bg" width="10%" align="center">CREDIT</td>
<td class="bg" width="10%" align="center">CLEAR DATE</td>
<td class="bg" width="10%" align="center">BALANCE</td></tr>
</table></td>
</tr>
<%
String fromdateonly="";
String todateonly="";
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
DecimalFormat df = new DecimalFormat("0.00");
//Calendar cal = Calendar.getInstance();
c1.set(Calendar.DATE, 1);
Date firstDateOfPreviousMonth = c1.getTime();
	String fromDate=(dateFormat2.format(c1.getTime())).toString();
	fromdateonly=fromDate;
		   fromDate=fromDate+" 00:00:01";
c1.set(Calendar.DATE, c1.getActualMaximum(Calendar.DATE)); 
Date lastDateOfPreviousMonth = c1.getTime();
	String toDate=(dateFormat2.format(c1.getTime())).toString();
	todateonly=toDate;
			toDate=toDate+" 59:00:00";
//System.out.println("fromDate::"+fromDate+":::todate"+toDate);
	if(request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals("")){
		fromDate=request.getParameter("fromdate")+" 00:00:01";
		fromdateonly=request.getParameter("fromdate").toString();
	}
	if(request.getParameter("todate")!=null && !request.getParameter("todate").equals("")){
		toDate=request.getParameter("todate")+" 59:00:00";
		todateonly=request.getParameter("todate").toString();
	}
	
banktransactionsListing BAK_L=new mainClasses.banktransactionsListing(); 

employeesListing EMP=new employeesListing();
//List BANKL=BAK_L.getMbanktransactionsId(bankId);
List BANKL=BAK_L.getBankTransactionBetweenDates(bankId,fromDate,toDate,request.getParameter("reportType"));
SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy");
double debitBal=0.00;
double creditBal=0.00;
double bankBal=0.00;
double openingBal=0.00;
if(BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType"))!=null && !BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType")).equals("")){
	openingBal=Double.parseDouble(BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType")));
	bankBal=Double.parseDouble(BAK_L.getCreditOpeningBankBalance(bankId,fromDate,request.getParameter("reportType")))-Double.parseDouble(BAK_L.getDebitOpeningBankBalance(bankId,fromDate,request.getParameter("reportType")));
	}
%>
<tr>
<td colspan="5" align="right" style="font-weight: bold;color: orange;" width="25%">OPENING BALANCE | </td>
<td colspan="1" style="font-weight: bold;" align="center" width="15%"><%=formatter.format(openingBal)%></td>
<td colspan="1" align="center" width="10%">0.00</td>
<td colspan="1" align="center" width="10%"></td>
<td colspan="1" align="center" width="15%"><%=formatter.format(openingBal)%></td>

</tr>
<form name="reconciliationForm" id="reconciliationForm" action="bankReconciliationDate-update.jsp" method="post">
<input type="hidden" name="fromdate" value="<%=fromdateonly%>"/>
<input type="hidden" name="todate" value="<%=todateonly%>"/>
<input type="hidden" name="reportType" value="<%=request.getParameter("reportType")%>"/>
<input type="hidden" name="page" value="bank-reconciliation">
<input type="hidden" name="bankid" value="<%=bankId%>">
<input type="hidden" name="size" value="<%=BANKL.size()%>"/>
<%
if(BANKL.size()>0){
	for(int i=0;i<BANKL.size();i++){
	BKT=(beans.banktransactions)BANKL.get(i);	
	
%>
<tr>

<td width="9%" align="center">
<input type="hidden" name="bankTransId<%=i%>" value="<%=BKT.getbanktrn_id()%>"/>
<%=CDF.format(dateFormat.parse(BKT.getdate())) %></td>
<td width="9.2%" align="center"><%=BKT.getbanktrn_id() %></td>
<td  width="9.6%" align="center"><%=BKT.gettype() %></td>
<td width="8%" align="center"><%=PAYL.getChequeNo(BKT.getextra5()) %></td>
<td width="23%" align="center"><%=BKT.getnarration() %></td>
<!-- For petty cash transactions -->
<%if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %>
<td width="7%" align="center" style=""> 
	 <%if(BKT.gettype().equals("pettycash")){
	creditBal=creditBal+Double.parseDouble(BKT.getamount());
	if(BKT.getbrs_date()!=null && !BKT.getbrs_date().equals("")){ 
	bankBal=bankBal+Double.parseDouble(BKT.getamount());
	}
	openingBal=openingBal+Double.parseDouble(BKT.getamount());
	%> <%=formatter.format(Double.parseDouble(BKT.getamount()))%> <%}%>
</td>
<td  width="15%" align="center">
	<%if(BKT.gettype().equals("cashpaid")){
	debitBal=debitBal+Double.parseDouble(BKT.getamount());
	if(BKT.getbrs_date()!=null && !BKT.getbrs_date().equals("")){ 
	bankBal=bankBal-Double.parseDouble(BKT.getamount());
	}
	openingBal=openingBal-Double.parseDouble(BKT.getamount());%><%=BKT.getamount()%> <%}%>
</td>
<!-- For petty cash transactions -->
<%}else{ %>
<!-- For Bank deposits/cheque transactions -->
<td  width="8%" align="center" style="">
	<%if(BKT.gettype().equals("deposit")){
	debitBal=debitBal+Double.parseDouble(BKT.getamount());
	if(BKT.getbrs_date()!=null && !BKT.getbrs_date().equals("")){ 
	bankBal=bankBal-Double.parseDouble(BKT.getamount());
	}
	openingBal=openingBal+Double.parseDouble(BKT.getamount());%><%=formatter.format(Double.parseDouble(BKT.getamount()))%> <%}%>
</td>
<td  width="15%" align="center"> 
	<%if(BKT.gettype().equals("withdraw") || BKT.gettype().equals("pettycash") || BKT.gettype().equals("cashpaid") || BKT.gettype().equals("cheque")){
		if(BKT.getbrs_date()!=null && !BKT.getbrs_date().equals("")){ 
		bankBal=bankBal+Double.parseDouble(BKT.getamount());
		}
	creditBal=creditBal+Double.parseDouble(BKT.getamount());
	openingBal=openingBal-Double.parseDouble(BKT.getamount());%> <%=formatter.format(Double.parseDouble(BKT.getamount()))%> <%}%>
</td>
<!-- For Bank deposits/cheque transactions -->
<%} %>
<%-- <td  width="10%" align="center"><input type="text" name="date<%=i%>" id="date<%=i%>" class="date" value="<%=BKT.getbrs_date()%>" <%if(BKT.getbrs_date()!=null && !BKT.getbrs_date().equals("")){ %> readonly="readonly" <%} %> style="height: 20px; width: 65px;"></td> --%>
<td  width="10%" align="center"><input type="text" name="date<%=i%>" id="date<%=i%>" class="date" value="<%=BKT.getbrs_date()%>" readonly="readonly" style="height: 20px; width: 65px;"></td>
<td  width="10%" align="center" <%if(openingBal<0){%>style="color: red;"<%} %>><%=formatter.format(openingBal) %></td>
</tr>
<%}} %>
</form>
<%if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %>
<tr >
	<td class="bg" colspan="5" align="right" style="font-weight: bold;color: orange;">CLOSING BALANCE | </td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(creditBal) %></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(debitBal) %></td>
	<td class="bg" align="center"><input type="submit" name="submit" id="formsubmit" value="CLEAR ALL"></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(creditBal-debitBal)%></td>
	<td></td>
</tr>
<tr >
	<td class="bg" colspan="8" align="right" style="font-weight: bold;color: orange;">BALANCE AS PER BANK | </td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(bankBal) %></td>
</tr>
<%}else{ %>
<tr>
	<td class="bg" colspan="5" align="right" style="font-weight: bold;color: orange;">CLOSING BALANCE | </td>
	<td  class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(debitBal) %></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(creditBal) %></td>
	<td class="bg" align="center"><input type="submit" name="submit" id="formsubmit" Value="CLEAR ALL"></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(debitBal-creditBal)%></td>
	<td></td>
</tr>
<tr >
	<td class="bg" colspan="8" align="right" style="font-weight: bold;color: orange;">BALANCE AS PER BANK | </td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(bankBal) %></td>
</tr>

<%} %>
</table>
</div>
</div>
<%} %>
</div>
<%}else{response.sendRedirect("adminPannel.jsp?page=banks");}%>

