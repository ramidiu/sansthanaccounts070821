<%@page import="java.util.List"%>
<%@page import="beans.banktransactions"%>
<%@page import="beans.bankdetailsService"%>
<%@page import="beans.bankdetails"%>
<%@page import="beans.banktransactionsService"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
@media print {
	@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin-left: 24mm; /* equals to 0.8inches */ 
}
	.voucher
	{
		width:880px; height:auto;  padding:40px 20px 40px 20px; border:1px solid #727272; left:55px; position:relative;
	}
	 .voucher table.sri td span { font-size:30px; font-weight:bold;}
	.voucher table td.regd { text-align:center; vertical-align:top; font-size:12px;}
	.voucher table.cash-details{ margin:40px 0 0 0; font-size:20px; border-top:1px solid #727272; border-left:1px solid #727272;}
	.voucher table.cash-details td{padding:10px 0px; border-bottom:1px solid #727272; border-right:1px solid #727272;  padding:10px 20px;}
	.voucher table.cash-details td span {text-decoration:underline; padding:0 0 5px 0; font-weight:bold; }
	.voucher table.cash-details td.vide ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide ul li{ list-style:none; float:left; margin:0 45px 0 0; }
	/*.voucher table.cash-details td.vide ul li:first-child{ width:400px;  margin:0 20px 0 0;}*/
	.voucher table.cash-details td.vide1 ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide1 ul li{ list-style:none; float:left; margin:0 63px 0 0}
	.voucher table.cash-details td.vide1 ul li span{ text-decoration:underline;}
	.voucher table.cash-details td.vide1 ul li:last-child{  margin:0 0px 0 0px}
	.signature{ width:150px; height:100px; border:1px solid #000; margin:0 12px 0 0;}
.voucher table.cash-details td table.budget {border-top:1px solid #000; border-left:1px solid #000;}
.voucher table.cash-details td table.budget td {border-bottom:1px solid #000; border-right:1px solid #000; padding:5px;}
}
</style>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!-- main content -->
<jsp:useBean id="BNK" class="beans.banktransactions"></jsp:useBean>
<jsp:useBean id="BNK1" class="beans.banktransactions"></jsp:useBean>
	<div>
	<div class="vendor-page">
	<div class="vendor-list">
				<div class="icons"><a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
				<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
				</div>
				<div class="clear"></div>
				<div class="list-details">
				 <!--<table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
						<tr>
					<td colspan="7" align="center" style="font-weight: bold;">Cash / Cheque voucher PRINT</td>
					</tr>
				 </table>-->
		<div class="printable">
				<%String CId=request.getParameter("CId");
				SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
				SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat SDF1=new SimpleDateFormat("yyyy-MM-dd");
				mainClasses.paymentsListing PAYL=new mainClasses.paymentsListing();
		banktransactionsListing BNKT_L=new banktransactionsListing();
		bankdetailsListing BNKD_L=new bankdetailsListing();
								List BNK_TRNS=BNKT_L.getMbanktransactions(CId);
	
					 if(BNK_TRNS.size()>0){
						 BNK=(banktransactions)BNK_TRNS.get(0);
					 }
					 List BNK_FRMTRNS=BNKT_L.getMbanktransactions(BNK.getextra3());

					 if(BNK_FRMTRNS.size()>0){
						 BNK1=(banktransactions)BNK_FRMTRNS.get(0);
					 }
				DecimalFormat DF=new DecimalFormat("0");
				DecimalFormat DF1=new DecimalFormat("0.00");
                NumberToWordsConverter NTW=new NumberToWordsConverter();
                String amount=DF.format(Double.parseDouble(BNK.getamount()));
				%>
                <div class="voucher">
                <table width="100%" cellpadding="0" cellspacing="0" class="sri">
                <tr>
               	<td class="regd" colspan="2"><span>SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br /><br />
                ( Regd. No. 646/92 )<br />
				DILSUKHNAGAR, HYDERABAD - 500 060.<br /><br />
				<span  style="text-transform: uppercase; text-decoration:underline">Contra Bank Entry Voucher</span></td>
                </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%" class="cash-details">
                <tr>
                <td> Voucher No : <span><%=BNK.getbanktrn_id() %></span></td>
                <td>Date <span><%=CDF.format(SDF.parse(BNK.getdate()))%></span></td>
                </tr>
                        <tr>
                    <td colspan="2">
                    <span class="floatleft" style="text-decoration:none;">From Bank : </span>   
                    <span class="head-act"><%=BNKD_L.getBankName(BNK1.getbank_id())%></span>
                    </td>
                    
                  </tr>
                            <tr>
                    <td colspan="2">
                    <span class="floatleft" style="text-decoration:none;">To Bank : </span>   
                    <span class="head-act"><%=BNKD_L.getBankName(BNK.getbank_id())%></span>
                    </td>
                    
                  </tr>
               
                     <tr>
                    
                    </tr>
                <tr>
                <td colspan="2">A sum of Rupees : <span><%=NTW.convert(Integer.parseInt(amount)) %> only</span></td>
                </tr>
                <tr>
                <td colspan="2">Being <span><%=BNK.getnarration() %></span></td>
                </tr>
                         <tr>
                 <td colspan="2">Rs. <span style="font-weight:bold"><%=DF1.format(Math.round(Double.parseDouble(BNK.getamount()))) %></span></td>
                 </tr> 
                 <tr><td colspan="2" height="40" style="border-bottom:none;"></td></tr>
                <tr>
                <td align="right" colspan="2" style="border-bottom:none;"><div class="signature"></div></td>
                </tr>
                <tr>
                <td colspan="2" class="vide1">
                <ul>
                <li>Accountant</li>
                <li>Manager ( Acts )</li>
                 <li>GS/Chairman</li> 
                <li>Treasurer</li>
                 <li>Receiver's Signature</li>
                </ul>
                </td>
                </tr>
                </table>
                
                </div>
          	
			 </div>
			</div>
			</div>
</div>
</div>
