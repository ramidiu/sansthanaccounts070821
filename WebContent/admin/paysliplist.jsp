<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.employeeSalarySlipListing,beans.employeeSalary,mainClasses.employeeDetailsListing,beans.employeeDetails "%>
<%@page import="mainClasses.stafftimingsListing,beans.stafftimings" %>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<style>
@media print {
	@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin-left: 24mm; /* equals to 0.8inches */ 
}
	.voucher
	{
		width:880px; height:auto;  padding:40px 20px 40px 20px; border:1px solid #727272; left:55px; position:relative;
	}
	 .voucher table.sri td span { font-size:30px; font-weight:bold;}
	.voucher table td.regd { text-align:center; vertical-align:top; font-size:12px;}
	.voucher table.cash-details{ margin:40px 0 0 0; font-size:20px; border-top:1px solid #727272; border-left:1px solid #727272;}
	.voucher table.cash-details td{padding:10px 0px; border-bottom:1px solid #727272; border-right:1px solid #727272;  padding:10px 20px;}
	.voucher table.cash-details td span {text-decoration:underline; padding:0 0 5px 0; font-weight:bold; }
	.voucher table.cash-details td.vide ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide ul li{ list-style:none; float:left; margin:0 45px 0 0; }
	/*.voucher table.cash-details td.vide ul li:first-child{ width:400px;  margin:0 20px 0 0;}*/
	.voucher table.cash-details td.vide1 ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide1 ul li{ list-style:none; float:left; margin:0 63px 0 0}
	.voucher table.cash-details td.vide1 ul li span{ text-decoration:underline;}
	.voucher table.cash-details td.vide1 ul li:last-child{  margin:0 0px 0 0px}
	.signature{ width:150px; height:100px; border:1px solid #000; margin:0 12px 0 0;}
.voucher table.cash-details td table.budget {border-top:1px solid #000; border-left:1px solid #000;}
.voucher table.cash-details td table.budget td {border-bottom:1px solid #000; border-right:1px solid #000; padding:5px;}
}
</style>
<script type="text/javascript" lang="javascript">
	var openMyModalSj = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 450;
		modalWindow.height = 200;
		modalWindow.content = "<iframe width='450' height='200' frameborder='0' scrolling='yes' allowtransparency='false' src='pendingIndentsPopup.jsp'></iframe>";
		modalWindow.open();
	
	};	
</script>


 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function()
                        {
                            //alert("11111111");
                            $( "a" ).css("text-decoration","none");
                             $( "#tblExport" ).print();
                           
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>

<!-- main content -->
	<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<jsp:useBean id="EXP" class="beans.productexpenses"></jsp:useBean>
<jsp:useBean id="GOD" class="beans.godwanstock"></jsp:useBean>
	<div>
	<div class="vendor-page">
	<div class="vendor-list">
				<div class="icons">
<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>

				<div class="clear"></div>
				<div class="list-details">
				 <!--<table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
						<tr>
					<td colspan="7" align="center" style="font-weight: bold;">Cash / Cheque voucher PRINT</td>
					</tr>
				 </table>-->
		<div id="tblExport">
		           <div class="voucher">
                <table width="100%" cellpadding="0" cellspacing="0" class="sri" align="center">
                <%
              //getting month from employeeSalarySlipSubmit.jsp
              SimpleDateFormat DBDate=new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat Month=new SimpleDateFormat("MMM");
           		String month=request.getParameter("month");
			 	String employee_ids[]=request.getParameterValues("employee_id");
			 	float salary_cut,otherdeductions,wa,otherallowance;
           	 
                for(int i=0;i<employee_ids.length;i++)
			 	{
                %>
                
                </table>
               <br />
              
          <% 
          	employeeSalarySlipListing salarySlip =new employeeSalarySlipListing();
          	//List<employeeSalary> allSlip=salarySlip.selectAllSalarySlip();  
          	List<employeeSalary> allSlip=salarySlip.selectAllSalarySlipBasedOnMonthAndId(month,employee_ids[i]);
          	Iterator<employeeSalary> itr=allSlip.iterator();
          	while(itr.hasNext())
          	{
          		employeeDetailsListing empDetails=new employeeDetailsListing ();
          		employeeSalary empSal=itr.next();
          		
          		employeeDetails details= empDetails.selectSingleEmployeeDetailsBasedUponEmployeeId(empSal.getEmployee_id());
          		//to get emp name and designation 
          		stafftimingsListing  stafflisting=new stafftimingsListing ();
          		stafftimings stt=stafflisting.getStaffDetails(empSal.getEmployee_id());
          		salary_cut=otherdeductions =wa=otherallowance=0.0f;
          		
         	%>     
               
            <table width="100%" border="1" cellspacing="0" cellpadding="0" class="sheet">
            <tr>
               	<td class="regd" colspan="2"><span style=" font-weight:bold;">SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br />
                <span style="font-size:12px;">( Regd. No. 646/92 )</span><br />
				DILSUKHNAGAR, HYDERABAD - 500 060.<br /><br />
				 <%
				 if(month!=null){
				 	%>PAYSLIP FOR THE MONTH OF : <span style="text-decoration:underline;"><%=month.split("@")[0]%>(<%if(month.split("@").length>1){ %><%=month.split("@")[1]%><%}%>)</span><% 
				 }
                 %>
                
				</td>
                </tr>
                      <tr>
                        <td width="50%">Emp No :<%=details.getEmployee_id()%></td>
                        <td width="50%">Pay Scale :<%float payscale=(Float.parseFloat(empSal.getBasic_pay())+Float.parseFloat(empSal.getHra())); out.println(payscale);%></td>
                      </tr>
                      <tr>
                        <td>Emp Name : <%=stt.getstaff_name()%></td>
                        <td>Basic Pay :<%=details.getBasic_pay_salary()%></td>
                      </tr>
                      <tr>
                        <td rowspan="2">Designation :<%=stt.getdesignation()%> </td>
                        <td>Bank :<%=details.getExtra4()%></td>
                      </tr>
                      <tr>
                        <td>Bank A/C  No :<%=details.getExtra5()%></td>
                      </tr>
				</table>
            <table width="100%" border="1" cellspacing="0" cellpadding="0" class="sheet">
                  <tr>
                    <td align="center" width="50.1%" style="font-weight:bold;">Leaves </td>
                    <td align="center" style="font-weight: bold;">Installments (Balance / Total)</td>
                  </tr>
                  <tr>
                    <td>Opening Balance : <%=empSal.getExtra4() %></td><td>EWF Loan Installment : <%=details.getExtra7()%> / <%=details.getExtra6()%></td>
                  </tr>
                  <tr>
                    <td>Earned During the Month : <%=empSal.getExtra5() %></td><td>Personal Loan Installment : <%=details.getExtra9()%> / <%=details.getExtra8()%></td>
                  </tr>
                </table>
                
            <table width="100%" border="1" cellspacing="0" cellpadding="0" class="sheet">
                  <tr>
                    <td align="center" style="font-weight:bold;">Availed </td>
                    <td align="center" style="font-weight:bold;">Closing Balance </td>
                    
                  </tr>
                  <tr>
                    <td style="padding: 0 ; "><table width="100%" border="0" cellspacing="0" cellpadding="0" >
					<tr style="border: none;">
					<td style="width:50%;border: none; border-right: 1px solid;">C.L : <%float cl=(Float.parseFloat(empSal.getExtra4())+Float.parseFloat(empSal.getExtra5())-Float.parseFloat(empSal.getExtra6()));out.print(cl); %></td>
					<td style="border: none;">O.D : <%=empSal.getExtra3()%></td>
					</tr></table>
					</td>
					<td style="width:50%;border: none; border-right: 1px solid;">Closing Balance C.L : <%=empSal.getExtra6()%></td>
                  </tr>
                  <tr>
                    <td style="padding: 0 ; "><table width="100%" border="0" cellspacing="0" cellpadding="0" >
					<tr style="border: none;">
					<% float abscent=(Float.parseFloat(empSal.getExtra4())+Float.parseFloat(empSal.getExtra5())-Float.parseFloat(empSal.getExtra6()));
					if(abscent < 0){%>
					<td style="width:50%; border: none; border-right: 1px solid;">Absent :<%out.print(-abscent); %></td>
					<%}else{ %>
					<td style="width:50%; border: none; border-right: 1px solid;">Absent : 0.0</td>
					<%} %>
					<td style="border: none;">O.T(Hours): <%=empSal.getExtra7()%></td>
					</tr></table>
					</td>
					<td style="width:50%;border: none; border-right: 1px solid;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="padding: 0 ; "><table width="100%" border="0" cellspacing="0" cellpadding="0" >
					<tr style="border: none;">
					<td style="width:50%; border: none; border-right: 1px solid;">L.P' S : <%=empSal.getExtra1()%></td>
					<td style="border: none;">&nbsp;</td>
					</tr></table>
					</td>
					<td style="width:50%;border: none; border-right: 1px solid;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="padding: 0 ; "><table width="100%" border="0" cellspacing="0" cellpadding="0" >
					<tr style="border: none;">
					<td style="width:50%; border: none; border-right: 1px solid;">L.A' S : <%=empSal.getExtra2()%></td>
					<td style="border: none;">&nbsp;</td>
					</tr></table>
					</td>
					<td style="width:50%;border: none; border-right: 1px solid;">&nbsp;</td>
                  </tr>
                </table>
            <table width="100%" border="1" cellspacing="0" cellpadding="0" class="sheet">
              <tr>
                <td width="33.3%" align="center" style="font-weight:bold;">Earnings</td>
                <td width="33.3%" align="center" style="font-weight:bold;">Deductions</td>
                <td align="center" style="font-weight:bold;">Recoveries</td>
              </tr>
              <tr>
                <td width="33.3%">Basic Pay :<%=empSal.getBasic_pay()%> </td>
                <td width="33.3%">Salary Deduction :</td>
                <td>EWF Loan  : <%=empSal.getEwf_loan_amount()%> </td>
              </tr>
              <tr>
                <td width="33.3%">H.R.A : <%=empSal.getHra()%></td>
                <td width="33.3%">EWF : <% float ewf_deduction=0.0f; if(details.getExtra3().equals("eligible_for_ewf_deduction")){ewf_deduction=500;} out.println(ewf_deduction);%></td>
                <td>EWF Loan Interest : <%=empSal.getEwf_interest_amount()%></td>
              </tr>
              <tr>
                <td width="33.3%">O.T : <%=empSal.getOtpay()%></td>
                <%if(!empSal.getMedical_insurance().equals("null") ){ %>
				<td width="33.3%">Medical Insurance : <%=empSal.getMedical_insurance() %></td>
				<%}else{ %>
				<td width="33.3%">Medical Insurance : 0.0</td>
				<%} %>
                <td>Personal Loan :<%=empSal.getPersonal_loan_amount()%></td>
              </tr>
              <% 
               
if((empSal.getOther_allowances()!=null && !empSal.getOther_allowances().equals("null"))   ){ otherallowance=Float.parseFloat(empSal.getOther_allowances());} 	%>
              <tr>
                <td width="33.3%">Others :
                
                
                	<% 	
                			if(empSal.getExtra9()!=null && !empSal.getExtra9().equals("null") ) 
                			{
                				wa=Float.parseFloat(empSal.getExtra9());
                		                			}%> <%=otherallowance+wa %>
                		                			
                		                			
                		                			</td>
                <td width="33.3%">Others :<% 
               
if((empSal.getExtra10()!=null && !empSal.getExtra10().equals("null"))   ){ salary_cut=Float.parseFloat(empSal.getExtra10());} if( (empSal.getOther_deductions()!=null && !empSal.getOther_deductions().equals("null"))){otherdeductions=Float.parseFloat(empSal.getOther_deductions());}%>
<%=salary_cut+otherdeductions%> </td>
                <td>Others : </td>
              </tr>
            </table>
           	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="sheet">
              <tr>
                <td width="33.3%">Total : ₹ <%float totalEarnings=(Float.parseFloat(empSal.getBasic_pay())+Float.parseFloat(empSal.getHra())+Float.parseFloat(empSal.getOtpay())+wa)+otherallowance;out.println(totalEarnings); %></td>
                <%-- <td width="33.3%">Total : ₹ <%float totalDeductions=ewf_deduction-salary_cut;out.println(totalDeductions);%></td>
                <td>Total : ₹ <%float totalRecoveries=(Float.parseFloat(empSal.getEwf_loan_amount())+Float.parseFloat(empSal.getEwf_interest_amount())+Float.parseFloat(empSal.getPersonal_loan_amount())+salary_cut);out.println(totalRecoveries); %></td> --%>
                <td width="33.3%">Total : ₹ <%float totalDeductions=ewf_deduction+salary_cut+otherdeductions;out.println(totalDeductions);%></td>
                <td>Total : ₹ <%float totalRecoveries=(Float.parseFloat(empSal.getEwf_loan_amount())+Float.parseFloat(empSal.getEwf_interest_amount())+Float.parseFloat(empSal.getPersonal_loan_amount()));out.println(totalRecoveries); %></td>
              </tr>
             </table>
           	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="sheet">
              	<tr>
                	<%-- <td colspan=3 style="text-align:center;">Net Payment : <%= totalEarnings-totalDeductions-totalRecoveries-otherdeductions%></td> --%>
                	<td colspan=3 style="text-align:center;">Net Payment : <%= totalEarnings-totalDeductions-totalRecoveries%></td>
             	</tr>
            </table>
            
            <br><br><hr width="100%" color="red"><br><br>
            <%} %>
            <%}//for(int i=0;i<employee_ids.length;i++) %> 
            
            <p style="text-align:center; color:#666; padding-top:20px; word-spacing:2px;">This slip is  only for information and not for any other Purpose.</p>
                </div>
			 </div>
			</div>
			</div>
            
</div>
</div>
