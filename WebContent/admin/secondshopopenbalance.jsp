<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.products"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<<jsp:useBean id="PRD" class="beans.products"></jsp:useBean>
<jsp:useBean id="PRDS" class="beans.productsService"></jsp:useBean>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<jsp:useBean id="PRDU" class="beans.productsService"></jsp:useBean>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	function filter(){
		$("#filtration-form").submit();
		//location.reload('godwanForm.jsp?poid='+poid); 
		//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
	}
</script>

<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<form id="filtration-form"  action="adminPannel.jsp" method="post">
<div class="search-list">
<ul>
<li><input type="hidden" name="page" value="stockBalances"></li>
<li>
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
DecimalFormat df=new DecimalFormat("#0.000");
godwanstockListing GDS_L=new godwanstockListing();
shopstockListing SHPS_L=new shopstockListing();
customerpurchasesListing CUSTP_L=new customerpurchasesListing();
%>
<select name="head_account_id" id="head_account_id" onchange="filter();">
	<option value="" <%if(request.getParameter("head_account_id")!=null && request.getParameter("head_account_id").equals("")){ %> selected="selected"  <%} %>>ALL DEPARTMENTS</option>
	<%List HA_Lists=HOA_L.getheadofaccounts();
	if(HA_Lists.size()>0){
		for(int i=0;i<HA_Lists.size();i++){
		HOA=(beans.headofaccounts)HA_Lists.get(i);%>
	<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("") && request.getParameter("head_account_id").equals(HOA.gethead_account_id())){ %>  selected="selected" <%} %>><%=HOA.getname() %></option>
	<%}} %>
</select>
</li>
</ul>
</div>
</form>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
</span></div>
<div class="clear"></div>
<div class="list-details">

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
						<td colspan="10" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
						</tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;"> <%if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){ %> <%=HOA_L.getHeadofAccountName(request.getParameter("head_account_id")) %> <%}else{ %> ALL  <%} %>BILLS APPROVALS LIST </td></tr>
<tr>
<td class="bg" width="3%">S.NO.</td>
<td class="bg" width="3%">PRODUCT NAME</td>
<td class="bg" width="10%">GODWAN STOCK</td>
<td class="bg" width="10%">ACTUAL GODWAN STOCK</td>
<td class="bg" width="8%">SHOP STOCK</td>
<td class="bg" width="8%">ACTUAL SHOP STOCK</td>
<td class="bg" width="6%">PURCHASES</td>
<td class="bg" width="10%">TRANSFER</td>
<td class="bg" width="15%">SALE</td>
</tr>
<%String headofaccout="2";
int j=1;
if(request.getParameter("head_account_id")!=null){
	headofaccout=request.getParameter("head_account_id").toString();
} else{
	headofaccout="3";
}
double openingbalance=0.0;
double shoptransferentry=0.0;
double saleentry=0.0;
double actualgodwanstock=0.0;
double shopvarrience=0.0;
String shopstock="";
productsListing PRDL=new productsListing();
List PRODNEG_L=PRDL.getMedicalStore("3");
for(int i=0; i < 1; i++ ){
	PRD=(products)PRODNEG_L.get(i);
	if(SHPS_L.validProductOfSecondOpeningBalance(PRD.getproductId()))
	{
		openingbalance=Double.parseDouble(SHPS_L.getTotalReceivedQuantityBasedHOAOfSecondOpenBal(PRD.getproductId(),PRD.gethead_account_id()));
		shoptransferentry=Double.parseDouble(SHPS_L.getTotalReceivedQuantityEntryHOA(PRD.getproductId(),PRD.gethead_account_id()));
		saleentry=Double.parseDouble(CUSTP_L.getTotalSaleQuantityBasedHOAOnDate(PRD.getproductId(),PRD.gethead_account_id()));
		actualgodwanstock=openingbalance+shoptransferentry-saleentry;
		shopstock=""+actualgodwanstock;
		%>
		<jsp:setProperty name="PRDU" property="head_account_id" value="<%=PRD.gethead_account_id()%>"/>
		<jsp:setProperty name="PRDU" property="productId" value="<%=PRD.getproductId()%>"/>
		<jsp:setProperty name="PRDU" property="balanceQuantityStore" value="<%=shopstock %>"/>
		<%-- <%=PRDU.updateWithHOA()%><%=PRDU.geterror()%> --%>
		<%
	}
} %>
</table>
</div>
</div>
</div>