<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script type="text/javascript">

function validate(){
	$('#headIdErr').hide();
	$('#vendorErr').hide();

	if($('#headAccountId').val().trim()==""){
		$('#headIdErr').show();
		$('#headAccountId').focus();
		return false;
	}
	/* if($('#sub_head_id').val().trim()==""){
		$('#vendorErr').show();
		$('#sub_head_id').focus();
		return false;
	} */
}

</script>
<script>
$(document).ready(function(){
  $("#major_head_id").keyup(function(){
	  var data1=$("#major_head_id").val();
	  var hoid=$("#headAccountId").val();
	  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#major_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#minor_head_id").keyup(function(){
	  var data1=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#headAccountId").val();
	  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#minor_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#sub_head_id").keyup(function(){
	  var data1=$("#sub_head_id").val();
	  var minor=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#headAccountId").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#sub_head_id" ).autocomplete({source: availableTags}); 
			});
  });
});
$(document).ready(function(){
	$("#sortBy").change(function(){
	    var sortVal=$("#sortBy").val();
	    $("#majorhead").hide();
	    $("#minorhead").hide();
	    $("#subhead").hide();
	    if(sortVal=='majorHead'){
	    	$("#majorhead").show();
		    $("#minorhead").hide();
		    $("#subhead").hide();
	    }
	    if(sortVal=='minorHead'){
		    $("#minorhead").show();
	    }
	    if(sortVal=='subHead'){
		    $("#subhead").show();
	    }
	  });
	});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){%>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Expenses report </div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
%>

<form name="tablets_Update" method="post"  onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<input type="hidden" name="page" value="productReport.jsp"/>
<div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account<span style="color: red;">*</span></td>
<td/td>
<td></td>
<td>
<div class="warning" id="vendorErr" style="display: none;">Please Provide  sub head name.</div></td>
<td></td>
</tr>
<tr>
<td><select name="headAccountId" id="headAccountId">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("headAccountId")!=null && request.getParameter("headAccountId").equals(HOA.gethead_account_id())){ %> selected="selected"  <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><select name="sortBy" id="sortBy">
<option value="">Sort by</option>
<option value="majorHead" <%if(request.getParameter("sortBy")!=null && request.getParameter("sortBy").equals("majorHead")){ %> selected="selected"  <%} %> >Major Head</option>
<option value="minorHead" <%if(request.getParameter("sortBy")!=null && request.getParameter("sortBy").equals("minorHead")){ %> selected="selected"  <%} %> >Minor Head</option>
<option value="subHead" <%if(request.getParameter("sortBy")!=null && request.getParameter("sortBy").equals("subHead")){ %> selected="selected"  <%} %>>Sub Head</option>
</select></td>
<td id="majorhead" style="display: block;"><input type="text" name="major_head_id" id="major_head_id" <%if(request.getParameter("major_head_id")!=null){ %> value="<%=request.getParameter("major_head_id") %>"<%}else{%> value="" <%} %>  placeholder="find a major head" autocomplete="off"/></td>
<td id="minorhead" style="display: none;"><input type="text" name="minor_head_id" id="minor_head_id" <%if(request.getParameter("minor_head_id")!=null){ %> value="<%=request.getParameter("minor_head_id") %>"<%}else{%> value="" <%} %> placeholder="find a minor head" autocomplete="off"/></td>
<td id="subhead" style="display: none;"><input type="text" name="sub_head_id" id="sub_head_id" <%if(request.getParameter("sub_head_id")!=null){ %> value="<%=request.getParameter("sub_head_id") %>"<%}else{%> value="" <%} %> placeholder="find a sub head" autocomplete="off"/></td>
<%-- <td>
<select name="finYear" id="finYear">
<%if(request.getParameter("finYear") == null){ %>
<option value="">-- Select Fin Year --</option><%} %>
<option value="2014-15" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2014-15")) {%> selected="selected"<%} %>>2014-15</option>
<option value="since2015" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("since2015")) {%> selected="selected"<%} %>>Since 2015</option>
<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%> selected="selected"<%} %>>2015-16</option>
<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
</select>
</td> --%>
				
			<td colspan="2">
			<select name="finYear" id="finYear">
					<%@ include file="yearDropDown1.jsp" %>
						<option value="since2015" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("since2015")) {%> selected="selected"<%} %>>Since 2015</option>
						
						<option value="2014-15" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2014-15")) {%> selected="selected"<%} %>>2014-2015</option>
						
					</select>
			</td>



<td align="right"><input type="submit" value="Search" class="click" style="border:none;"/></td>
</tr>

<tr> 
<td colspan="3"></td>

</tr>
</table>
</form>

</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<!-- <span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span> -->
</div>
<div class="clear"></div>

<div class="list-details">
<%
DecimalFormat df = new DecimalFormat("0.00");
productsListing PRD_L=new productsListing();
if(request.getParameter("sub_head_id")!=null || request.getParameter("minor_head_id")!=null || request.getParameter("major_head_id")!=null){
	String vendorid="";
	if(request.getParameter("major_head_id")!=null && !request.getParameter("major_head_id").equals("") && request.getParameter("sortBy").equals("majorHead")){
		 vendorid=request.getParameter("major_head_id");
	}
	if(request.getParameter("minor_head_id")!=null && !request.getParameter("minor_head_id").equals("") && request.getParameter("sortBy").equals("minorHead")){
		 vendorid=request.getParameter("minor_head_id");
	}
	if(request.getParameter("sub_head_id")!=null && !request.getParameter("sub_head_id").equals("") && request.getParameter("sortBy").equals("subHead")){
		
		vendorid=request.getParameter("sub_head_id");
	}

String temp[]=vendorid.split(" ");
if(temp.length>1){
/* if(PRD_L.getvalidSubHead(temp[0])){ */%>
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<div class="printable">
<% 
if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("")){%>
<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tblExport">
<tr><td colspan="7"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="yourID">
<tr>
<td class="bg" width="5%">S.NO</td>
<td class="bg" width="15%">MONTH</td>
<td class="bg" width="23%" align="left">OPENING BALANCE</td>
<td class="bg" width="15%"  align="left" >DEBIT</td>
<td class="bg" width="15%"  align="left" >CREDIT </td>
<td class="bg" width="15%"  align="left" >BALANCE</td>
<td class="bg" width="10%" align="left"></td>


</tr>
</table></td></tr>
<%
String dos="2015-04-01";
SimpleDateFormat dateformat  = new SimpleDateFormat("yyyy-MM"); // Just the year, with 2 digits
String formattedDate = dateformat .format(Calendar.getInstance().getTime());

if(request.getParameter("finYear").equals("2015-16"))
{
	formattedDate = "2016-03";
}

else if(request.getParameter("finYear").equals("2016-17"))
{
	dos = "2016-04-01";
}

else if(request.getParameter("finYear").equals("2014-15"))
{
	dos = "2014-04-01";
	formattedDate = "2015-03";
}

godwanstockListing GDSTK_L=new godwanstockListing();
shopstockListing SHPSTK_L=new shopstockListing();
DateFormatSymbols dfs = new DateFormatSymbols();
String[] months = dfs.getMonths();
String month = "";
int num=0;
//String dos="2014-04-01";
String inwards="00";
double opengbal=0;
double closinbal=0;
double inwardsstock=0;
double closeingstock=0;
String outwards="";
//SimpleDateFormat dateformat  = new SimpleDateFormat("yyyy-MM"); // Just the year, with 2 digits
//String formattedDate = dateformat .format(Calendar.getInstance().getTime());
DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM");
java.util.Date d1=dateFormat.parse(dos);
java.sql.Date d=new Date(d1.getTime());
java.util.Date curent1=dateFormat.parse(formattedDate);
java.sql.Date curent=new Date(curent1.getTime());
Calendar c1 = Calendar.getInstance(); 
c1.setTime(d);
dos = (dateFormat.format(d)).toString();
 while(!(d.after(curent)))
{ num=c1.get(c1.MONTH);
if (num >= 0 && num <= 11 ) {
    month = months[num];
}
%>
<tr>
<td width="6%" align="center"></td>
<td width="15% " align="left"><span><a href="adminPannel.jsp?page=expenses-daywiseReport&subheadid=<%=temp[0]%>&dt=<%=dos%>&sortType=<%=request.getParameter("sortBy")%>"><%=month%></a></span></td>
 <td align="left" width="24%"> Rs.<%=df.format(opengbal) %></td>
<%productexpensesListing EXPL=new productexpensesListing();
	 inwards=GDSTK_L.getgodwanstockbymonthBasedOnvendor(dos,temp[0]);
	 //outwards=SHPSTK_L.getgShopstockbymonth(dos,temp[0]);
	 outwards=EXPL.getExpenseAmountByMonthBasedOnSubhead(dos,temp[0],request.getParameter("sortBy"));
	 if(inwards!=null){
		 opengbal=opengbal+Double.parseDouble(inwards);
/* 		 System.out.println(opengbal); */
		 } else{
			 inwards="0";
	 }
	 if(outwards!=null){
		 opengbal=opengbal+Double.parseDouble(outwards);
		 } else{
			 outwards="0";
	 }
	
	    c1.setTime(d);
	    
			c1.add(c1.MONTH, +1);
			closinbal=opengbal;
%>


<%-- <td>&#8377; <%=inwards %> </td> --%>
<td width="15% " align="left">Rs.<%=df.format(Double.parseDouble(outwards)) %></td>
<td width="15% " align="left">Rs.0.00</td>
<td width="15% " align="left">Rs.<%=df.format(Double.parseDouble(""+closinbal)) %></td>
<td width="10%" align="left"></td>
</tr>


<%
dos = (dateFormat.format(c1.getTime())).toString();
java.util.Date jud=dateFormat.parse(dos);
	d=new Date(jud.getTime());
} %>
<tr><td colspan="5" align="right" style="color: red;font-weight: bold;" width="75%"><h>TOTAL:</h></td><td style="color: red;" align="left" colspan="2">Rs.<%=df.format(Double.parseDouble(""+closinbal)) %></td></tr>
</table><%} %>
</div>
<%/* } */ 
}else{%>
<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no reports found for your search!</span></div>
<%} }%>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
