
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Advances-Dues</title>
<!--Date picker script  -->
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});		
});
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
/* function calculate(i){
	var u=0;
	var debitamount=0.0;
	var creditamount=0.0;
	u=document.getElementById("addcount").value;
		if(document.getElementById("majorhead"+i).value!="" && document.getElementById("minorhead"+i).value!="" && document.getElementById("subhead"+i).value!=""){
			for(var j=0;j<u;j++){
			if(document.getElementById("debitAmt"+j).value!=""){
				debitamount=Number(parseFloat(debitamount)+parseFloat(document.getElementById("debitAmt"+j).value)).toFixed(2);
				}
			if(document.getElementById("creditAmt"+j).value!=""){
				creditamount=Number(parseFloat(creditamount)+parseFloat(document.getElementById("creditAmt"+j).value)).toFixed(2);
				}	
			}
		document.getElementById("credittotalamnt").value=creditamount;
		document.getElementById("debittotalamnt").value=debitamount;
	}

} */
function calculate(i){
	var u=0;
	var amountNew=0.0;
	//alert('1.....'+i);
	u=document.getElementById("addcount").value;
		if(document.getElementById("majorhead"+i).value!="" && document.getElementById("minorhead"+i).value!="" && document.getElementById("subhead"+i).value!=""){
			for(var j=0;j<u;j++){
			/* if(document.getElementById("debitAmt"+j).value!=""){
				debitamount=Number(parseFloat(debitamount)+parseFloat(document.getElementById("debitAmt"+j).value)).toFixed(2);
				} */
				//alert(document.getElementById("Amt"+j).value);
			if(document.getElementById("Amt"+j).value!=""){
				amountNew=Number(parseFloat(amountNew)+parseFloat(document.getElementById("Amt"+j).value)).toFixed(2);
				}
			//alert(amountNew);
			}
		document.getElementById("totalamnt").value=amountNew;
		//document.getElementById("debittotalamnt").value=debitamount;
	}

}

function getInvoice(j){
	 var data1=$('#InvoiceValue'+j).val();
	//alert(data1);  
var arr = [];
$.post('detailsInvoice.jsp',{q:data1},function(data)
{
	 
	 var response = data.trim().split("\n");
	 //alert("response..."+response);
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0],
		        sortable: true,
		        resizeable: true
		    });
	 }

		var availableTags=arr;
		//alert('size....'+availableTags);
		$( '#InvoiceValue'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 
		 }); 
		});
} 
function majorheadsearch(j){
	  var data1=$('#majorhead'+j).val();
	  var hoid=$('#hoid'+j).val();
var arr = [];
$.post('searchMajor.jsp',{q:data1,HAid:hoid},function(data)
{
	 
	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }

		var availableTags=arr;
		$( '#majorhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 
		 }); 
		});
} 
function minorheadsearch(j){
	  var data1=$('#minorhead'+j).val();
	  var majorhead=$('#majorhead'+j).val();
var arr = [];
$.post('searchMinior.jsp',{q:data1,q1:majorhead},function(data)
{	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }
		var availableTags=arr;
		$( '#minorhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 }); 
		});
}
function subheadsearch(j){
	  var minorhead=$('#minorhead'+j).val();
	  var majorhead=$('#majorhead'+j).val();
	  var subhead=$('#subhead'+j).val();
	  var hoa=$('#hoid'+j).val();
var arr = [];
$.post('searchSubhead.jsp',{q:subhead,HID:hoa,q1:majorhead,q2:minorhead},function(data)
{
	 
	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }

		var availableTags=arr;
		$( '#subhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 
		 }); 
		});
} 
function vendorsearch(){
	 var data1=$('#vendorname').val();
	  var headID=$('#hoid').val();
	  if($('#hoid').val().trim()==""){
			$('#hoid').focus();
			return false;
		}
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $( '#vendorname').autocomplete({source: availableTags}); 
			});
}
function getInvoiceIDValue(){
	var hoid=$('#hoid').val();
	document.getElementById('EPIVID').options.length = 1;

	$.post('GetInvoice.jsp',{q:hoid},function(data)
	{
		
	    var sda1 = document.getElementById('EPIVID');
	    
	    var where_is_mytool=data.trim();
		var mytool_array=where_is_mytool.split("\n");
		//alert('11111...'+mytool_array);
		for(var i=0;i<mytool_array.length;i++)
		{
			if(mytool_array[i] !="")
			{
		var y=document.createElement('option');
		
		var val_array=mytool_array[i];
		//alert(val_array);
					y.text=val_array;
					try
					{
					sda1.add(y,null);
					}
					catch(e)
					{
					sda1.add(y);
					
					}
		
		}
		}
		
	});
	
}
function Confirm(){
	
	var response='';
	var total='';
	
	var advanceAmount=$('#advanceAmount').val();
	//alert('advanceAmount...'+advanceAmount);
	
		total=$('#totalamnt').val();
		total = total.slice(0, -3);
		//alert('total...'+total);
	 if(Number(total) > Number(advanceAmount)){
		//alert('111111111111111111');
		alert('pending amount is only Rs.'+Number(advanceAmount));
		return false;
	}
 	 else{
 		document.getElementById("saveonly").disabled = true;
		$('#saveonly').val("creating");
		return true;
	}  
	
	
	
}
function getAMOUNT(){
	var ActualInvoice=$('#EPIVID').val();
	$.post('GetInvoice.jsp',{q1:ActualInvoice},function(data)
			{
		response=data.trim().split("\n");
		document.getElementById("advanceAmount").value=response;
		
			});
	
}




$('form#formSubmit').submit(function(){
    $(this).find(':input[type=submit]').prop('disabled', true);
});


/* function submitForm(){
	
} */
</script>
<!--Date picker script  -->
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
</head>
<body>

<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<form id="formSubmit" action="advancesDuesInsert.jsp" method="Post" onsubmit="return Confirm();">
<input type="hidden" value="advancesDuesInsert"/>
<div class="vendor-page">

<div class="vendor-box">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">Advances</td></tr>
</table>
<div class="vender-details">
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
double creditamount,debitamount,amountNew;
creditamount=debitamount=amountNew=0.0;
majorheadListing MJRL=new majorheadListing();
subheadListing SUBL=new subheadListing();
minorheadListing MINL=new minorheadListing();
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

int noofrooms,l;
noofrooms=l=0;

if(request.getParameter("noofrooms")!=null && request.getParameter("noofrooms")!=""){
	noofrooms=(Integer.parseInt(request.getParameter("noofrooms").toString()));
}
String subheadarray[]={"ROOM RENT","SERVICE TAX","LUXOURY TAX","COMPUTER CHARGES","EXTRA PERSON CHARGES" ,""};
String subheadamount[]={"roomrent","servicetax","luxurytax","computercharges","extrapersoncharges" ,""};
StringBuilder majorhead=new StringBuilder();
StringBuilder roomtype=new StringBuilder();
StringBuilder minorhead=new StringBuilder();
StringBuilder subhead=new StringBuilder();
StringBuilder amount=new StringBuilder();
List HA_Lists=HOA_L.getheadofaccounts();
%>

<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>

<!-- <td>Select Fin Year</td> -->
<td>Date</td>
<td>Department</td>
<td>Advance Invoice Id</td>
<!-- <td>Vendor</td> -->
</tr>
<tr>
<!-- <td>
<select >
	<option>2017-2018</option>
	<option>2016-2017</option>
</select>
</td> -->

<td><input type="text" name="date" id="date"  value="<%=currentDate%>" class="DatePicker" readonly/></td>
<td><select name="hoid" id="hoid" onchange="getInvoiceIDValue();">
<%


if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%}else if(HOA.gethead_account_id().equals("3")){ %> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select>
</td>
 <td>
<select name="EPIVID" id="EPIVID" required>
	<option value="">Select</option>
</select>
</td> 
<!-- <td><input type="text" name="vendorname" id="vendorname" onkeyup="vendorsearch();"/></td> -->
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list" style="">
<div class="bgcolr"  style=" float:left;width:100%;">
<div class="sno1 bgcolr">S.NO.</div>
<div class="ven-nme1 bgcolr" style="width:170px; ">HEAD ACCOUNT ID</div>
<div class="ven-nme1 bgcolr" style="width:196px; ">MAJOR HEAD NAME</div>
<div class="ven-nme1 bgcolr" style="width:200px; ">MINOR HEAD NAME</div>
<div class="ven-nme1 bgcolr" style="width:200px; ">SUB HEAD NAME</div>
<div class="ven-nme1 bgcolr" id="debit" style="width:190px;"> INVOICE ID</div>
<div class="ven-nme1 bgcolr" id="credit">AMOUNT</div>
</div>
<!-- <div >
<div class="sno1">1</div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text"    class="" value="" placeholder="Find a majorhead here"   autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text"   class="" value="" placeholder="Find a minorhead here"   autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text"  class="" value="" placeholder="Find a subhead here"   autocomplete="off"/></div>
<div class="ven-nme1">&#8377;<input type="text" style="width: 150px;"/></div>
<div class="ven-nme1">&#8377;<input type="text"  onkeyup="" style="width: 150px;"/></div>
<div class="clear"></div>
</div>

<div>
<div class="sno1 ">2</div>

<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text"    class="" value="" placeholder="Find a majorhead here"   autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text"   class="" value="" placeholder="Find a minorhead here"   autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text"  class="" value="" placeholder="Find a subhead here"   autocomplete="off"/></div>
<div class="ven-nme1">&#8377;<input type="text"  style="width: 150px;"/></div>
<div class="ven-nme1">&#8377;<input type="text"  onkeyup="" style="width: 150px;"/></div>
<div class="clear"></div>
</div> -->

<input type="hidden" name="addcount" id="addcount" value="5"/>
<input type="hidden" name="advanceAmount" id="advanceAmount" value=""/>
<%

if(noofrooms<1){
for(int i=0; i < 50; i++ ){
%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">
<div class="sno1 MT13"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1" style="width:170px; cursor: pointer;">
<select name="hoid<%=i%>" id="hoid<%=i%>" onchange="getAMOUNT();">
<%


if(HA_Lists.size()>0){
	for(int j=0;j<HA_Lists.size();j++){
	HOA=(beans.headofaccounts)HA_Lists.get(j); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%}else if(HOA.gethead_account_id().equals("3")){ %> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></div>
<div class="ven-nme1" style="width:200px; cursor: pointer;">
<input type="text" name="majorhead<%=i%>" id="majorhead<%=i%>"  onkeyup="majorheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a majorhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:200px; cursor: pointer;">
<input type="text" name="minorhead<%=i%>" id="minorhead<%=i%>"  onkeyup="minorheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a minorhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:200px; cursor: pointer;">
<input type="text" name="subhead<%=i%>" id="subhead<%=i%>"  onkeyup="subheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a subhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
 <div class="ven-nme1" style="width:175px;"><input type="text" name="InvoiceValue<%=i%>" id="InvoiceValue<%=i%>"  onkeyup="getInvoice('<%=i%>')" value="EPIV"  maxlength="10" style="width: 150px;"></input></div>

<div class="ven-nme1" style="width:170px;">&#8377;<input type="text" name="Amt<%=i%>" id="Amt<%=i%>"  value="0"  onkeyup="calculate('<%=i%>')" style="width: 110px;"></input></div>
<div class="clear"></div>
</div>
<%} } else{

	for(int i=0; i < noofrooms;  i++){

		
				for(int j=0;j<6;j++ ){
					if(request.getParameter("roomid"+i)!=null && request.getParameter(subheadamount[j]+""+i)!=null){
					roomtype.append(request.getParameter("roomid"+i).toString());
					minorhead.append(MINL.getminorheadname(roomtype.toString(),"5"));
					majorhead.append(MJRL.getmajorheadname("ROOM RENT","5"));
					subhead.append(SUBL.getsubheadname(subheadarray[j],"5"));
					amount.append(request.getParameter(subheadamount[j]+""+i));
					creditamount=creditamount+Double.parseDouble(amount.toString());
					amountNew=amountNew+Double.parseDouble(amount.toString());
		

					%>
<div  id="addcolumn<%=l%>">
<div class="sno1 MT13"><%=l+1%></div>
<input type="hidden" name="count" id="check<%=l%>"  value="<%=l %>"/> 
<div class="ven-nme1" style="width:170px; cursor: pointer;">
<select name="hoid<%=i%>" id="hoid<%=i%>" onchange="getAMOUNT();">
<%


if(HA_Lists.size()>0){
	for(int k=0;k<HA_Lists.size();k++){
	HOA=(beans.headofaccounts)HA_Lists.get(j); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%}else if(HOA.gethead_account_id().equals("3")){ %> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="majorhead<%=l%>" id="majorhead<%=l%>" onkeyup="majorheadsearch('<%=l%>');tabCall('<%=l%>');" class="" value="<%=majorhead.toString() %>" placeholder="Find a subhead here" <%if(l==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="minorhead<%=l%>" id="minorhead<%=l%>" onkeyup="minorheadsearch('<%=l%>');tabCall('<%=l%>');" class="" value="<%=minorhead.toString() %>" placeholder="Find a subhead here" <%if(l==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="subhead<%=l%>" id="subhead<%=l%>"  onkeyup="subheadsearch('<%=l%>');tabCall('<%=l%>');" class="" value="<%=subhead.toString() %>" placeholder="Find a subhead here" <%if(l==0){ %> required <%} %>  autocomplete="off"/></div>
<%-- <div class="ven-nme1">&#8377;<input type="text" name="debitAmt<%=l%>" id="debitAmt<%=l%>"  onkeyup="calculate('<%=l%>')" value="0"  style="width: 150px;"></input></div> --%>
<div class="ven-nme1"><input type="text" name="InvoiceValue<%=l%>" id="InvoiceValue<%=l%>"  onkeyup="getInvoice('<%=l%>')" value="EPIV"  maxlength="10" style="width: 150px;"></input></div>
<div class="ven-nme1">&#8377;<input type="text" name="Amt<%=l%>" id="Amt<%=l%>"  value="<%=amount %>"  onkeyup="calculate('<%=l%>')" style="width: 150px;"></input></div>
<div class="clear"></div>
</div>
<% 	
l++;
roomtype.setLength(0);
minorhead.setLength(0);
majorhead.setLength(0);
subhead.setLength(0);
amount.setLength(0);
				}}}}%>



<div class="add-ven">
<span style="margin:0 80px 0 0">Total Amount : </span>
&#8377;<input type="text"  name="totalamnt" id="totalamnt"  value="0" style="width: 150px;"/>

</div>
<div class="add-ven">
<input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/>
<span style="margin:0 80px 0 0">&nbsp;</span>
<span style="margin:0 360px 0 0">&nbsp;</span>
<input type="reset" value="Reset" class="click" style="border:none;"/>
 <input type="submit" id="saveonly" name="saveonly" value="CREATE" class="click" style="border:none; " onclick="return submitForm()"/>

<div class="tot-ven"></div>
</div>
</div>
</form>
</body>
</html>