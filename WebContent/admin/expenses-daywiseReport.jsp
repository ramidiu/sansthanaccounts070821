<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.util.List" %>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function idValidate(){
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	var id=$('#major_head_id').val().trim();
$.ajax({
	type:'post',
	url: 'IDValidate.jsp', 
	data: {
		Id : id,
		type : 'majorhead'
	},
	success: function(response) { 
		var msg=response.trim();
		$('#majorheadErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#major_head_id').focus();
			return false;
		}
	}});
}

</script>
<script type="text/javascript">

function validate(){
	$('#headIdErr').hide();
	$('#majorheadErr').hide();
	$('#headNameErr').hide();

	if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	}
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	if($('#name').val().trim()==""){
		$('#headNameErr').show();
		$('#name').focus();
		return false;
	}
}
function productsearch(){

	  var data1=$('#productname').val();
	  var headID=$('#headAccountId').val();
	  $.post('searchProducts.jsp',{q:data1,hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="EXP" class="beans.productexpenses"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Expenses day-wise report</div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
%>

<form name="tablets_Update" method="post"  onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account<span style="color: red;">*</span></td>
	<td>Product Name</td>
</tr>
<tr>
<td><select name="headAccountId" id="headAccountId">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("headAccountId")!=null && request.getParameter("headAccountId").equals(HOA.gethead_account_id())){ %> selected="selected"  <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><input type="text" name="productname" id="productname" value="" onkeyup="productsearch()" /></td>

</tr>

</table>
</form>

</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Columns</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%productsListing PRD_L=new productsListing();
if(request.getParameter("subheadid")!=null){
String vendorid=request.getParameter("subheadid");
String sortType=request.getParameter("sortType");
/* if(PRD_L.getvalidSubHead(vendorid)){ */%>

<table width="98%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td class="bg" width="8%">DATE</td>
<td class="bg" width="10%">VOUCHER.NO.</td>
<td class="bg" width="10%">PAYMENT TYPE</td>
<td class="bg" width="12%">NARRATION</td>
<td class="bg" width="15%" align="right">OPENING BALANCE</td>
<td class="bg" width="15%" align="right">DEBIT </td>
<td class="bg" width="15%" align="right">CREDIT</td>
<td class="bg" width="15%" align="right">CLOSING BALANCE</td>
</tr>
<%
DecimalFormat df = new DecimalFormat("0.00");
mainClasses.productexpensesListing EXPL=new mainClasses.productexpensesListing();
godwanstockListing GDSTK_L=new godwanstockListing();
shopstockListing SHPSTK_L=new shopstockListing();
DateFormatSymbols dfs = new DateFormatSymbols();
String[] months = dfs.getMonths();
int num=0;
String dt=request.getParameter("dt")+"-01";
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
Calendar c = Calendar.getInstance();
c.setTime(sdf.parse(dt));
int month = c.get(Calendar.MONTH);
int month1=month;
String debit="00";
double opengbal=0;
banktransactionsListing BNKL=new banktransactionsListing();
if(EXPL.getPreviousClosingExpensesAmount(dt, vendorid,sortType)!=null){
	//opengbal=Integer.parseInt(EXPL.getPreviousClosingExpensesAmount(dt, vendorid,sortType));
	opengbal=Double.parseDouble(EXPL.getPreviousClosingExpensesAmount(dt, vendorid,sortType));
}
if(EXPL.getPreviousExpensesAmount(dt, vendorid,sortType)!=null){
	//opengbal=opengbal+Integer.parseInt(EXPL.getPreviousExpensesAmount(dt, vendorid,sortType));
	opengbal=opengbal+Double.parseDouble(EXPL.getPreviousExpensesAmount(dt, vendorid,sortType));
}
double closinbal=0;
double inwardsstock=0;
double closeingstock=0;
String credit="0";
List expDet=null;
while(month1==month)
{
	
	expDet=EXPL.getExpensesDetails(dt,vendorid,sortType);
	if(expDet.size()>0){
		for(int i=0;i<expDet.size();i++){
			EXP=(beans.productexpenses)expDet.get(i);%>
		<tr>
<td><span><%=dt%></span></td>
<td><%=EXP.getexpinv_id()%></td>
<td><%=BNKL.getStringPaymentType(EXP.getexpinv_id())%></td>
<td><%=EXP.getnarration() %></td>
<td align="right"> &#8377; <%=df.format(opengbal)%></td>
<%
	 //debit=EXPL.getExpenseAmountByMonth(dt,vendorid);
	debit=EXP.getamount();
	if(debit!=null){
		 //opengbal=opengbal+Integer.parseInt(debit);
		 opengbal=opengbal+Double.parseDouble(debit);
		 } else{
			 debit="0";
	 }
	 if(credit!=null){
		 //opengbal=opengbal+Integer.parseInt(credit);
		 opengbal=opengbal+Double.parseDouble(credit);
		 } else{
			 credit="0";
	 }
		closinbal=opengbal;
%>
<td align="right"> &#8377; <%=df.format(Double.parseDouble(debit))%></td>
<td align="right"> &#8377; <%=df.format(Double.parseDouble(credit))%></td>
<td align="right"> &#8377; <%=df.format(Double.parseDouble(""+closinbal))%></td>
</tr>

	<%}} %>

<%
c.add(Calendar.DATE, 1);  // number of days to add
dt = sdf.format(c.getTime());
//c.setTime(sdf.parse(dt));
month = c.get(Calendar.MONTH);
} %>
<tr><td colspan="7" align="right" style="color: orange;"><h>TOTAL AMOUNT :</h></td><td align="right">&#8377; <b><%=df.format(Double.parseDouble(""+closinbal))%></b></td>
</tr>


</table>
<%}else{%>
<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no reports found for your search!</span></div>
<%}%> 
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
