<%@page import="beans.ProductOpeningBalance"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="beans.ProductOpeningBalanceService" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ProductOpeningBalanceList</title>
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<style>
	.vendor-page {
    background: #FFF;
    width: 100%;
 min-height: 150px; 
    float: left;
    /* border: 1px solid #000; */
}
	.productlist h3{
	  text-align:center;
	}
	</style>
	<script type="text/javascript">
	$(document).ready(function() {
		$('#cat1').change(function() {
			var cat = $(this).val();
			if ($.trim(cat) !== "") {
				$('#subcat1').empty();
				$('#subcat1').append($('<option>', {
					value : "",
					text : 'Select SubCategory'
				}));
				$.ajax({
					url : "searchSubHeadSj.jsp",
					type : "GET",
					data : {
						cat : cat
					},
					success : function(response) {
						var data = response.split(",");
						for (var i = 0; i < data.length; i++) {
							if (data[i].trim() !== "") {
								$('#subcat1').append($('<option>', {
									value : data[i],
									text : data[i].toUpperCase()
								}));
							}
						}
					}
				});
			}
		});
	});
</script>
</head>
<body>
<div class="vendor-page">

    <form  action="adminPannel.jsp?page=productOpeningBalanceList"  method="post"
			style="padding-left: 70px; padding-top: 30px;">
  <table width="80%" border="0" cellspacing="0" cellpadding="0">

				<tbody>
					<tr>
							<td class="border-nn" colspan="2">Category</td>
						<td class="border-nn" colspan="2">SubCategory</td>
						<td class="border-nn" colspan="2">Select Fin Yr</td>

					</tr>
					<tr>
<%
							mainClasses.productsListing prodService = new mainClasses.productsListing();
							List catlist = prodService.getProductCategories();
						%>
						<td colspan="2"><select name="cat1" id="cat1">
								<option value="">Select Category</option>
								<%
									for (int i = 0; i < catlist.size(); i++) {
										String cat = (String) catlist.get(i);
								%>
								<option value="<%=cat%>"><%=cat.toUpperCase()%></option>
								<%
									}
								%>
						</select></td>
						<td colspan="2"><select name="subcat1" id="subcat1">
								<option value="">Select SubCategory</option>
						</select></td>
						<td colspan="2"><select name="finacialyear" id="finacialyear">
								<option value="2020-04-01">2020-2021</option>
								<option value="2021-04-01">2021-2022</option>
								<option value="2022-04-01">2022-2023</option>
								<option value="2023-04-01">2023-2024</option>
								<option value="2024-04-01">2024-2025</option>
						</select></td>

						<td><input type="submit" value="Submit" class="click"></td>
					</tr>
				</tbody>
			</table>
	</form>
</div>
<%
String cat1=request.getParameter("cat1");
String subcat1=request.getParameter("subcat1");
String finacialyear=request.getParameter("finacialyear");
ProductOpeningBalanceService  productOpeningBalanceService=new ProductOpeningBalanceService();
 List<ProductOpeningBalance>  productOpeningBalanceList=productOpeningBalanceService.getProductOpeningBalncesBasedOnCategorySubCategoryAndFinacialYear(cat1,subcat1,finacialyear);
%>
<section class="productlist">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>All ProductOpeningBalanceList</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered mb-0">
							<thead class="thead-light">
							</thead>
							<tbody>
					
								<tr>
									<th>S.NO</th>
									<th>Product Code</th>
									<th>Product Name</th>
									<th>Date</th>
									<th>Quanity</th>
									<th>Edit</th>

								</tr>
								<% 
								for(int i=0;i<productOpeningBalanceList.size();i++){
									 ProductOpeningBalance productOpeningBalance=productOpeningBalanceList.get(i);
									 
								%>
								<tr>
									<td><%=i+1%></td>
									<td><%=productOpeningBalance.getProductId()%></td>
									<td><%=productOpeningBalance.getProductName()%></td> 
									<td><%=productOpeningBalance.getFinacilaYearDate()%></td>
									<td><%=productOpeningBalance.getQuantity()%></td>
									<td><a href="productOpeningBalanceEdit.jsp?productOpeningBalanceId=<%=productOpeningBalance.getProductOpeningBalanceId()%>">Edit</a></td>
								</tr>
								<%} %>

							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>