<%@page import="beans.headofaccounts"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.banktransactions"%>
<%@page import="beans.majorhead"%>
<%@page import="model.SansthanAccountsDate"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="com.accounts.saisansthan.vendorBalances"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
<jsp:useBean id="BNK1" class="beans.banktransactions"></jsp:useBean>
<body>

<%
customerpurchasesListing CP_L=new customerpurchasesListing();
majorheadListing MH_L=new majorheadListing();
vendorsListing VNDR=new vendorsListing();
bankdetailsListing BNKL=new bankdetailsListing();
bankcalculations BNKCAL=new bankcalculations();
banktransactionsListing BNK_L = new banktransactionsListing();
vendorBalances VNDBALL=new vendorBalances();
String finYear="2017-18";
String reporttype="";
String reporttype1="";
String fromdate="";
String onlyfromdate="";
String startingdate="";
String endingdate="";
String todate="";
String Todate="";
String startdate="";
String enddate="";
double bankopeningbal=0.0;
double bankopeningbalcredit=0.0;
String onlytodate="";
String cashtype="online pending";
String cashtype1="othercash";
String cashtype2="offerKind";
String cashtype3="journalvoucher";
String finStartDate = "";
headofaccountsListing HOA_L=new headofaccountsListing();
List headaclist=HOA_L.getheadofaccounts();


/* Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DecimalFormat df=new DecimalFormat("#.##");
Date date=new Date();
c1.getActualMaximum(Calendar.DAY_OF_MONTH);
int lastday = c1.getActualMaximum(Calendar.DATE);
c1.set(Calendar.DATE, lastday);  
todate=(dateFormat2.format(c1.getTime())).toString();
enddate=(dateFormat2.format(c1.getTime())).toString();
c1.getActualMinimum(Calendar.DAY_OF_MONTH);
int firstday = c1.getActualMinimum(Calendar.DATE);
c1.set(Calendar.DATE, firstday); 
fromdate=(dateFormat2.format(c1.getTime())).toString();
//startdate=(dateFormat2.format(c1.getTime())).toString();

System.out.println("fromDate....."+fromdate); */
String htmlData="";
String message="";
for(int l=0;l<headaclist.size();l++){ 
	String hoaid=" ";

	HOA=(headofaccounts)headaclist.get(l);
	System.out.println("hwads...."+l);
	if(HOA.gethead_account_id().equals("1") || HOA.gethead_account_id().equals("4")){ 
		if(HOA.gethead_account_id().equals("4")){
			 hoaid="4";
			reporttype = "pro-counter";
			reporttype1 = "Development-cash";
			
		} if(HOA.gethead_account_id().equals("1")){
			hoaid="1";
		
			reporttype = "Charity-cash";
			reporttype1 = "Not-Required";	
		}
		

 /* SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date myDate = new Date();
Calendar cal1 = Calendar.getInstance();
cal1.setTime(myDate);
cal1.add(Calendar.DAY_OF_YEAR, 0);
Date previousDate = cal1.getTime();
String currentDate="";
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
currentDate=dateFormat2.format(previousDate);
 fromdate=currentDate;
todate=currentDate; */

DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd");

Calendar c = Calendar.getInstance();
c.add(Calendar.MONTH, -1);
c.set(Calendar.DATE, 1);

String firstDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();
c.set(Calendar.DATE,c.getActualMaximum(Calendar.DAY_OF_MONTH));
String lastDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();


System.out.println("fromDate....."+fromdate); 
%>

<%
DecimalFormat DF=new DecimalFormat("#,###.00"); 
String typeofbanks[]={"bank","cashpaid"};
String headgroup="";


double total=0.0;
double credit=0.0;
double debit=0.0;
double creditreceipt =0.0;
double debitreceipt =0.0;
double creditpayment=0.0;
double debitpayment =0.0;

subheadListing SUBH_L=new subheadListing();
productexpensesListing PEXP_L=new productexpensesListing();

SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");

List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid, "revenue",headgroup);
List banklist=BNKL.getBanksBasedOnHOA(hoaid);
//System.out.println("print hoid.."+hoaid);
List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"expenses",headgroup);
List assetsmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"Assets",headgroup);

	
	
	
	startingdate=fromdate;
	onlyfromdate=fromdate;
	fromdate=firstDateOfPreviousMonth+" 00:00:00";
	//endingdate=enddate;
	onlytodate=todate;
	todate=lastDateOfPreviousMonth+" 23:59:59";
	System.out.println("todate......"+todate);
	Todate=todate;
	String finStartYr = finYear.substring(0, 4);
	System.out.println("finStartYr....."+finStartYr);
	String finYr = finYear;
	System.out.println("finYr....."+finYr);
	
	List minhdlist=null;
	
	minorheadListing MINH_L=new minorheadListing();
	
		  htmlData=htmlData+"<div class='list-details'><div class='printable' id='tblExport'>";
		htmlData=htmlData+"<table width='95%'  cellspacing='0' cellpadding='0' style='   margin-bottom: 10px; margin-top:10px; margin:auto;'>";
		htmlData=htmlData+"<tbody><tr><td colspan='3' align='center' style='font-weight: bold;'>SHRI SHIRIDI SAI BABA SANSTHAN TRUST "+HOA_L.getHeadofAccountName(hoaid)+"</td></tr><tr><td colspan='3' align='center' style='font-weight: bold;'>Dilsukhnagar</td></tr><tr><td colspan='3' align='center' style='font-weight: bold;'>RECEIPTS &amp; PAYMENTS  Report</td></tr><tr><td colspan='3' align='center' style='font-weight: bold;'>Report From Date "+firstDateOfPreviousMonth+" and "+lastDateOfPreviousMonth+"</td></tr></tbody><br>";
		htmlData=htmlData+"<table width='90%' border='1' cellspacing='0' cellpadding='0' style='margin:0 auto;' class='new-table'><tbody><tr><td width='38%' height='29' align='center' style='font-size:15px'>Account(Debits)</td><td width='29%' align='center' style='font-size:15px'>Debits</td><td width='33%' align='center' style='font-size:15px'>Credits</td></tr><tr><td height='28' colspan='3' bgcolor='#99FF00' style='font-weight:bold;'>RECEIPTS</td></tr><tr><td height='28'>&nbsp;</td><td height='28' align='right'>0.00</td><td height='28' align='right'>0.00</td></tr>";
		
		
			  	double proopeningbal=0.0;
			  	double finOpeningBal=0.0;
			  	double totalcountercash =0.0;
	  			double totalcashpaid =0.0;
				double grandtotal = 0.0;
				double finOpeningBal2=0.0;
				double onlinedeposits = 0.0;
				  bankbalanceListing BBAL_L=new mainClasses.bankbalanceListing();
				  List banklist1=BNKL.getBanksBasedOnHOA(hoaid);
					/* String bank=""; */
					
					  if(banklist1.size()>0)
					  { int i=0;
						for(i=0;i<banklist1.size();i++)
						{
							BNK=(bankdetails)banklist1.get(i);
							for(int j=0;j<typeofbanks.length;j++)
							{
								//bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),"",fromdate,typeofbanks[j]);
								bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",fromdate,typeofbanks[j],"");
								//System.out.println("bankopeningbal..."+bankopeningbal);
								finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], finYear);
								bankopeningbal += finOpeningBal;
								if(bankopeningbal >0 || bankopeningbal<0)
								{		
					
									htmlData=htmlData+"<tr><td height='28' style='color:#F00; font-weight:bold;'>"+BNK.getbank_name()+" "+typeofbanks[j]+"</td><td height='28' align='center' style='color:#F00; font-weight:bold;'>0.00</td><td height='28' align='center' style='color:#F00; font-weight:bold;'>"+DF.format(bankopeningbal)+"</td></tr>";
					//s3=s3+s2;
				
	    			bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
	    			//System.out.println("bankopeningbalcredit..."+bankopeningbalcredit);
					//bankopeningbal=0.0;
								}
							}
							
							} 
					 
					String addedDate1=SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
					
					 finOpeningBal2 = BBAL_L.getOpeningBalOfCounterCash(hoaid, finYear);
					//System.out.println("finOpeningBal2..."+DF.format(finOpeningBal2));
		  			if(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate) != null && !CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate).equals(""))
		  			{
						totalcountercash = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate2(hoaid,finStartYr+"-04-01", fromdate));
		  				//System.out.println("totalcountercash..."+DF.format(totalcountercash));
		  			}
		  			if(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1)!= null && !BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1).equals(""))
		  			{
						totalcashpaid = Double.parseDouble(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1));
						//System.out.println("totalcashpaid..."+DF.format(totalcashpaid));
		  			}
		  			
		  			
		  			if(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1)!= null && !BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1).equals(""))
		  			{
						onlinedeposits = Double.parseDouble(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1));
						//System.out.println("onlinedeposits..."+DF.format(onlinedeposits));
		  			}
		  			grandtotal = finOpeningBal2+totalcountercash-totalcashpaid-onlinedeposits;
		  		
		  			proopeningbal = grandtotal;
					  }
				  
					  htmlData=htmlData+"<tr><td height='28' style='color:#F00; font-weight:bold;'>CASH ACCOUNT</td><td height='28' align='center' style='color:#F00; font-weight:bold;'>0.00</td><td height='28' align='center' style='color:#F00; font-weight:bold;'>"+DF.format(proopeningbal)+"</td></tr>";
				  
				
				
				 if(incomemajhdlist.size()>0)
				{ 	int q=0;
					for(q=0;q<incomemajhdlist.size();q++)
					{
						MH=(majorhead)incomemajhdlist.get(q); 
						
				    hoaid=MH_L.getHeadId(MH.getmajor_head_id());
					//System.out.println("headId1..."+MH.getname());	
				/* start */
				List otherAmountList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),"","",hoaid,fromdate,todate);
	  			//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
				double otherAmount = 0.0;
				if(otherAmountList.size()>0)
				{
					banktransactions OtherAmountList = (banktransactions)otherAmountList.get(0);
					if(OtherAmountList.getamount() != null)
					{
						otherAmount = Double.parseDouble(OtherAmountList.getamount());
					}
				}
				htmlData=htmlData+"<tr><td height='28' align='center'><a href='#' style=' color: #934C1E; font-weight:bold;'>"+MH.getmajor_head_id()+" "+MH.getname()+"</a></td><td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id" ,fromdate,todate,hoaid)))+"</td> <td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,hoaid))+otherAmount)+"</td>";
				
						debitreceipt=debitreceipt+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
    				 minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
				if(minhdlist.size()>0)
				{	int j=0;
					for(j=0;j<minhdlist.size();j++)
					{

						MNH=(beans.minorhead)minhdlist.get(j);
						double amount =Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) +Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())));
						double amount2=Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
						
						List otherAmountList1=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),"",hoaid,fromdate,todate);
		  				
						double otherAmount1 = 0.0;
						if(otherAmountList1.size()>0)
						{	
							banktransactions OtherAmountList1 = (banktransactions)otherAmountList1.get(0);
							if(OtherAmountList1.getamount() != null)
							{
								otherAmount1 = Double.parseDouble(OtherAmountList1.getamount());
								//System.out.println("otherAmount1...."+otherAmount1);
							}
					}
						
		
						if(!("0.0".equals(""+amount)) ||  !("0.0".equals(""+amount2))   || otherAmount1 != 0.0)
						{
						
							htmlData=htmlData+"<tr><td height='28' style='color:#F00;'>"+MNH.getminorhead_id()+" "+MNH.getname()+"</td><td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)))+"</td>";
						
					debitreceipt=debitreceipt+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
	    					/* if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){  */
	    					if(!CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ 
	    						htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+otherAmount1)+"</td></tr>";
							 
							   } else{ 
								   htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) +Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+otherAmount1)+"</td></tr>";
	    					
	    					} 
						
						
						
						List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id()); 
						String subhid= MNH.getminorhead_id();
						if(subhdlist.size()>0)
						{ 	int k=0;
							for(k=0;k<subhdlist.size();k++)
							{			
								SUBH=(subhead)subhdlist.get(k);
								if(SUBH.getsub_head_id().equals("20201"))
									cashtype="creditsale";
								else if(SUBH.getsub_head_id().equals("20206"))
									cashtype="creditsale";
								else
									//cashtype="cash";
									cashtype ="online pending";
									cashtype1="othercash";
									cashtype2="offerKind";
									cashtype3="journalvoucher";
									//onlyfromdate=fromdate;
									//fromdate=fromdate+" 00:00:00";
									//onlytodate=todate;
									//todate=todate+" 23:59:59";
									//double amount1= Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
	
							/* start */
								List otherAmountList2=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(),hoaid,fromdate,todate);
	 								//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
								double otherAmount2 = 0.0;
								if(otherAmountList2.size()>0)
								{
									banktransactions OtherAmountList2 = (banktransactions)otherAmountList2.get(0);
									if(OtherAmountList2.getamount() != null)
									{
										otherAmount2 = Double.parseDouble(OtherAmountList2.getamount());
									}
								}
							/* end */
		
								double amount1= Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
								double amount3=Double.parseDouble(PEXP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,hoaid));
								
								//if(!("0.0".equals(""+amount1))){
								if(!("0.0".equals(""+amount1)) || !("0.0".equals(""+amount3)) || otherAmount2 != 0.0)
								{ 
									String ledgerSum = CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid));
								
									htmlData=htmlData+"<tr>";
								if(!ledgerSum.equals("00"))
	  								{ 
	  									htmlData=htmlData+"<td height='28' style='padding-left:15px;'>"+SUBH.getsub_head_id()+" "+SUBH.getname()+"</td>";
		    							//s13=s13+s12;
		    							}else { 
	    								htmlData=htmlData+"<td height='28' style='padding-left:15px;'>"+SUBH.getsub_head_id()+" "+SUBH.getname()+"</td>";
	    								
	    								} 
	    							
	  								htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)))+"</td>";
	    							debit=debit+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
									if(!ledgerSum.equals("00"))
									{
										
										htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+otherAmount2)+"</td>";
										credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+otherAmount2;} else{ 
	   	 								htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+otherAmount2)+"</td>";
	   	 							
	   	 								credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+otherAmount2;} %>
	  							
				<%}}}}}}}}
				
				
				
				
				
				
				
				
				
				htmlData=htmlData+"<tr> <td height='28' colspan='3' bgcolor='#99FF00' style='font-weight:bold;'>PAYMENTS</td></tr><tr><td height='28'>&nbsp;</td><td height='28' align='right'>0.00</td><td height='28' align='right'>0.00</td></tr>";
				
				 
	  							if(expendmajhdlist.size()>0)
	  							{ 
	  								int i=0;
		  							for(i=0;i<expendmajhdlist.size();i++)
		  							{
		  								MH=(majorhead)expendmajhdlist.get(i);
		  								htmlData=htmlData+"<tr><td height='28'align='center'><a href='#' style=' color: #934C1E; font-weight:bold;'>"+MH.getmajor_head_id()+" "+MH.getname()+"</a></td>";					
		  								
											hoaid=MH_L.getHeadId(MH.getmajor_head_id());
											
											if(MH.getmajor_head_id().equals("81")) 
											{
			    								htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid)))+"</td>";
			    										
												debitpayment=debitpayment+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
											
											}
											else if(MH.getmajor_head_id().equals("31")){
												htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum0(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid)))+"</td>";
											debitpayment=debitpayment+Double.parseDouble(PEXP_L.getLedgerSum0(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
											
											}else{
												htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid)))+"</td>";
											%>
			    								
												<%debitpayment=debitpayment+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
											
											}
											if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype2,hoaid).equals("00"))
											{ 
												htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype2,hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid)))+"</td>";
											
												creditpayment=creditpayment+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype2,hoaid));
			    							} else{ 
			    								htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid)))+"</td>";
			    							
			    								creditpayment=creditpayment+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); 
			    							} 
											htmlData=htmlData+"</tr>";
			    								
										minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
										if(minhdlist.size()>0)
										{	int j=0;
											for(j=0;j<minhdlist.size();j++)	
											{
												MNH=(beans.minorhead)minhdlist.get(j);
												if(!MNH.getminorhead_id().equals("281")){
												String amountpayment = PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
												if((!("00".equals(""+amountpayment)) ) && !MNH.getminorhead_id().equals("669"))
												{
													
													htmlData=htmlData+"<tr><td height='28'><span style='padding-left:25px; color:#F00;'>"+MNH.getminorhead_id()+" "+MNH.getname()+"</span></td>";	
													htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)))+"</td>";
										 if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00"))
			   								{
											 htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))+"</td>";
			   								}else{
			   									htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))+"</td>";
			   								} 
										 htmlData=htmlData+"</tr>";
			   								
										List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
										String subhid= MNH.getminorhead_id();
										if(subhdlist.size()>0)
										{ 	int k=0;
											for(k=0;k<subhdlist.size();k++)
											{
												SUBH=(subhead)subhdlist.get(k);
												if(SUBH.getsub_head_id().equals("20201"))
													cashtype="creditsale";
												else if(SUBH.getsub_head_id().equals("20206"))
													cashtype="creditsale";
												else
													//cashtype="cash";
													cashtype ="online pending";
													cashtype1="othercash";
													cashtype2="offerKind";
													cashtype3="journalvoucher";
													//onlyfromdate=fromdate;
													//fromdate=fromdate+" 00:00:00";
													//onlytodate=todate;
													//todate=todate+" 23:59:59";
													String amountpayment2= PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
				 
													if(!("00".equals(""+amountpayment2)))
													{
														
												htmlData=htmlData+"<tr>";
												if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00"))
													{ 
													htmlData=htmlData+"<td height='28'><span style='padding-left:35px;'>"+SUBH.getsub_head_id()+" "+SUBH.getname()+"</span></td>";
													}else { 
														htmlData=htmlData+"<td height='28'><span style='padding-left:35px;'>"+SUBH.getsub_head_id()+" "+SUBH.getname()+"</span></td>";
													}
												
												htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)))+"</td>";
												debit=debit+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
													if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00"))
													{ 
													
														htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,cashtype2,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid))))+"</td>";
											credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,cashtype2,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
					   								} else{ 
					   									htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,cashtype2,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid))))+"";
					   								credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,cashtype2,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));} 
													htmlData=htmlData+"</tr>";
					   								%>
					  						
					  						
				  			<%}}}}}}}}} 
				  			
	  							
				  			
				  			
				  			
	  							htmlData=htmlData+"<tr><td height='28' colspan='3' bgcolor='#99FF00' style='font-weight:bold;'>ASSETS</td></tr><tr><td height='28'>&nbsp;</td><td height='28' align='right'>0.00</td><td height='28' align='right'>0.00</td></tr>";	
				  			%>
				
				
				<!-- Assets -->
	    
								
	  							<% 
							    String pettycashMajorhead = "";
								String pettycashMinorhead = "";
								String pettycashSubhead = "";
								double pettycashAmount = 0.0;
								
	  							if(assetsmajhdlist.size()>0)
	  							{ 
	  								
	  								int i=0;
		  							for(i=0;i<assetsmajhdlist.size();i++)
		  							{
		  								MH=(majorhead)assetsmajhdlist.get(i);
		  								 hoaid=MH_L.getHeadId(MH.getmajor_head_id());
		  								 
		  								if(hoaid.equals("1"))
										{
											pettycashMajorhead = "54";
											pettycashMinorhead = "425";
											pettycashSubhead = "21450";
										}else if(hoaid.equals("4"))
										{
											pettycashMajorhead = "34";
											pettycashMinorhead = "235";
											pettycashSubhead = "21452";
										}else if(hoaid.equals("5"))
										{
											pettycashMajorhead = "84";
											pettycashMinorhead = "633";
											pettycashSubhead = "21405";
										}
		  								
		  								double ledgerSum = Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
										 
										if(MH.getmajor_head_id().equals("54") || MH.getmajor_head_id().equals("34") || MH.getmajor_head_id().equals("84"))
										{
											pettycashAmount = Double.parseDouble(PEXP_L.getLedgerSum2(pettycashSubhead,"sub_head_id",pettycashMinorhead,"minorhead_id",fromdate,todate,hoaid));
											ledgerSum -= pettycashAmount;
											
										} 
										
										
										if(ledgerSum > 0 )
										{	
											htmlData=htmlData+"<tr><td height='28' align='center'><a href='#' style=' color: #934C1E; font-weight:bold;'>"+MH.getmajor_head_id()+" "+MH.getname()+"</a></td>";
								
	    							if(MH.getmajor_head_id().equals("54") || MH.getmajor_head_id().equals("34") || MH.getmajor_head_id().equals("84")) 
	    							{
	    								htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(pettycashAmount+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid)))+"</td>";
	    							
	    								debitpayment=debitpayment-pettycashAmount+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
									}else{
										htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid)))+"</td>";
									
									debitpayment=debitpayment+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
									}
	    							if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid).equals("00"))
									{
	    								
	    								htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid)))+"</td>";
	    			
	    								creditpayment=creditpayment+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); 
	    							} else{ 
	    								htmlData=htmlData+"<td height='28' align='center' style=' color: #934C1E; font-weight:bold;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid)))+"</td>";
	    							
	    								creditpayment=creditpayment+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid)); 
	    							} 
	    							htmlData=htmlData+"</tr>";
	    						
	  						minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
							if(minhdlist.size()>0)
							{	int j=0;
								for(j=0;j<minhdlist.size();j++)
								{
									MNH=(beans.minorhead)minhdlist.get(j);
									String amountpayment = PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									double amountpaymentInDouble = Double.parseDouble(amountpayment);
									if(MNH.getminorhead_id().equals("425") || MNH.getminorhead_id().equals("235") || MNH.getminorhead_id().equals("633"))
									{
										amountpaymentInDouble -= pettycashAmount;
									}
									
									if(amountpaymentInDouble > 0 )
									{
							
										htmlData=htmlData+"<tr><td height='28'><span style='padding-left:25px; color:#F00;'>"+MNH.getminorhead_id()+" "+MNH.getname()+"</span></td>";
							
								if(MNH.getminorhead_id().equals("425") || MNH.getminorhead_id().equals("235") || MNH.getminorhead_id().equals("633"))
	   							{ 
									htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(pettycashAmount+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)))+"</td>";
	   							}else{ 
	   								htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)))+"</td>";
	   							} 
	   							
	   							 if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00"))
	   							{ 
	   								htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))+"</td>";
	   							
	   							}else{ 
	   								htmlData=htmlData+"<td height='28' align='center' style='color:#F00;'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))+"</td>";
	   							} 
	   							htmlData=htmlData+"</tr>";
	   							 
							List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
							String subhid= MNH.getminorhead_id();
							if(subhdlist.size()>0)
							{ 	int k=0;
								for(k=0;k<subhdlist.size();k++)
								{
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										cashtype ="online pending";
										cashtype1="othercash";
										cashtype2="offerKind";
										cashtype3="journalvoucher";
										
										String amountpayment2= PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
										
										if(SUBH.getsub_head_id().equals("21450") || SUBH.getsub_head_id().equals("21452") || SUBH.getsub_head_id().equals("21405"))
										{
											amountpayment2 = "00";
										}
										if(!("00".equals(""+amountpayment2)) )
										{
								
											htmlData=htmlData+"<tr>";
								%>
			   					
			   						<%if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00"))
			   						{ 
			   						
			   							htmlData=htmlData+"<td height='28'><span style='padding-left:35px;'>"+SUBH.getsub_head_id()+" "+SUBH.getname()+"</span></td>";
			   						}else { 
			   							htmlData=htmlData+"<td height='28'><span style='padding-left:35px;'>"+SUBH.getsub_head_id()+" "+SUBH.getname()+"</span></td>";
			   						} 
			   						htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid)))+"</td>";
			   						
			   						debit=debit+Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
									if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00"))
									{ 
										htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid))))+"</td>";
									
										credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
			    					} else{ 
			    						htmlData=htmlData+"<td height='28' align='left'>"+DF.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid))))+"</td>";
			    					credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
			    					} 
									htmlData=htmlData+"</tr>";
			    					%>
							
			  	<%}}}}}}}}} %>
			 
			  
			  	<!-- Assests -->
			  	<%htmlData=htmlData+"<tr><td align='center' style=' color: #934C1E; font-weight:bold;' height='28'>TOTAL AMOUNT OF PAYMENTS</td><td align='center' style=' color: #934C1E; font-weight:bold;'>Rs."+DF.format(debitpayment)+"</td><td align='center' style=' color: #934C1E; font-weight:bold;'>.00</td></tr>"; 
			  	
			  	
			  	
			  
			  	
			  	
			  	%>
				<%List banklist2=BNKL.getBanksBasedOnHOA(hoaid);
	  				bankopeningbalcredit=0.0;
					if(banklist2.size()>0)
					{ 	int i=0;
	  					for(i=0;i<banklist2.size();i++)
	  					{
	  						BNK=(bankdetails)banklist2.get(i);
	  						int j=0;
	  						for(j=0;j<typeofbanks.length;j++)
	  						{
							  	//bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),"",todate,typeofbanks[j]);
							  	bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",todate,typeofbanks[j],"");
							  	finOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], finYear);
							  	bankopeningbal += finOpeningBal;
							  	if(bankopeningbal >0 || bankopeningbal<0)
							  	{
	  				
							  		htmlData=htmlData+"<tr><td height='28' bgcolor='#CCFF00' style='font-weight:bold;'>"+BNK.getbank_name()+" "+typeofbanks[j]+"</td><td height='28' align='center' style='font-weight:bold;'>"+DF.format(bankopeningbal)+"</td><td height='28' align='center' style='font-weight:bold;'>0.00</td></tr>";
	  				
	  				
					bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
					
								}
							 }
	  					}
	  					}
	  				
					debitpayment=debitpayment+bankopeningbalcredit;
	 				String addedDate1New=SansthanAccountsDate.getAddedDate(todate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
	 				double totalcountercashNew =0.0;
	   				double totalcashpaidNew =0.0;
	  				double grandtotalNew = 0.0;
	  				double finOpeningBal2New = BBAL_L.getOpeningBalOfCounterCash(hoaid, finYear);
	  				if(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", todate) != null && !CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", todate).equals("")){
		  				totalcountercashNew = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate2(hoaid,finStartYr+"-04-01 00:00:00", todate+" 23:59:59"));
	  				}
				  	if(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1New)!= null && !BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1New).equals("")){
					  	totalcashpaidNew = Double.parseDouble(BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01 00:00:00", addedDate1New+" 23:59:59"));
				  	}
	  				double onlinedepositsNew = 0.0;
	  				if(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1New)!= null && !BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1New).equals("")){
		  				onlinedepositsNew = Double.parseDouble(BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01 00:00:00", addedDate1New+" 23:59:59"));
	  				}
	  				grandtotalNew = finOpeningBal2New+totalcountercashNew-totalcashpaidNew-onlinedepositsNew;
	  			
	  				double proclosingbal = grandtotalNew;
	  				htmlData=htmlData+"<tr><td height='28' bgcolor='#CCFF00' style='font-weight:bold;'>CASH ACCOUNT</td><td height='28' align='center' style='font-weight:bold;'>"+DF.format(proclosingbal)+"</td><td height='28' align='center' style='font-weight:bold;'>0.00</td></tr>";
					
	  				debitpayment=debitpayment+proclosingbal;			 
								
	  				htmlData=htmlData+"<tr><td height='28' bgcolor='#996600' style='font-weight:bold; color:#fff'>Total (Rupees)</td><td align='center' style='font-weight:bold; color: #934C1E;'>Rs."+DF.format(debitpayment)+"</td><td align='center' style='font-weight:bold; color: #934C1E;'>Rs."+DF.format(debitpayment)+"</td>";
	  				htmlData=htmlData+"</tr></tbody></table></table></div></div>";
	  				
	  				/* out.println(htmlData); */
	  				message=message+" Sai Sansthan Last Month's Receipts and Payments Total Amount at "+HOA_L.getHeadofAccountName(hoaid)+" is Rs."+DF.format(proclosingbal);
					}	}
				 
					sms.CallSmscApi SMS=new sms.CallSmscApi();
					 String result1=SMS.SendSMS("8125745281",message.replace(" ", "%20"));
					   /*  String result2=SMS.SendSMS("9985508281",message.replace(" ", "%20"));
					    String result3=SMS.SendSMS("9347237255",message.replace(" ", "%20"));
					    String result4=SMS.SendSMS("9290743663",message.replace(" ", "%20"));
					    String result5=SMS.SendSMS("9880463555",message.replace(" ", "%20"));
					    String result6=SMS.SendSMS("8106668634",message.replace(" ", "%20"));
					    String result7=SMS.SendSMS("9949664297",message.replace(" ", "%20"));
					    String result8=SMS.SendSMS("9989121240",message.replace(" ", "%20"));
					    String result9=SMS.SendSMS("9885225434",message.replace(" ", "%20"));
					    String result10=SMS.SendSMS("9010080426",message.replace(" ", "%20"));
					    String result11=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
					    String result12=SMS.SendSMS("9246549773",message.replace(" ", "%20"));
					    String result13=SMS.SendSMS("8790317244",message.replace(" ", "%20"));
					    String result14=SMS.SendSMS("9703133294",message.replace(" ", "%20"));
					    String result15=SMS.SendSMS("9391072421",message.replace(" ", "%20"));
					    String result16=SMS.SendSMS("9603585958",message.replace(" ", "%20"));
					    String result17=SMS.SendSMS("9392483933",message.replace(" ", "%20"));
					    String result18=SMS.SendSMS("9440865333",message.replace(" ", "%20"));
					    String result19=SMS.SendSMS("9866919444",message.replace(" ", "%20"));
					    String result20=SMS.SendSMS("9700000303",message.replace(" ", "%20")); */
				 
					 MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
	  				sendMail.send("reports@saisansthan.in", "sankar@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16"); 
	  				 sendMail.send("reports@saisansthan.in", "ramidiu@kreativwebsolutions.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  			/*	sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in","saisansthan_dsnr@yahoo.co.in","","","RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "venkataiahb4849@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "karthik@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "ramidiu@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "naresh@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "srreddy@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "pradeep@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "pruthvi@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "sujith@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "aaditya@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "reports@saisansthan.in", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "adityai4u@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "vempatiravi66@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "kalyansai@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "msrao317@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "vanamyadaiah@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "sai@visa9.co.in", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "lakshmidurgasurapaneni@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "vempatiravi6@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "shyamkumar1967@yahoo.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "rachavulasivasankararao@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "anishatre4@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "srinivas@kreativwebsolutions.co.uk", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "raghukumarprabhala@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "spvsrao@yahoo.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "rajesh_B5@yahoo.co.in", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "lawyers_syndicate@yahoo.co.in", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "manvishenterprises@yahoo.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "varsha_sai@yahoo.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "lingalahrao@yahoo.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "bikkumandla77@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "honey.knti25@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "sruthi.batchu@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "sivareddy2969@yahoo.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "anrao2455@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "sirgasudeep@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "dr_klingaiah@yahoo.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "vanamkiran6@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "madhuvanam@gmailcom ", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "rajuyadav9600@gmail.com ", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "saivaraprasadarao@gmail.com ", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16");
	  				sendMail.send("reports@saisansthan.in", "ravikumar66@gmail.com", "","", "RECEIPTS AND PAYMENTS REPORT", htmlData,"68.169.54.16"); */
	  				
				%>
					
	   				
	    				
	    				
	  							
							  
				
				
				
		
	
	



</body>
</html>