<%@page import="java.util.Calendar"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="beans.headofaccounts"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="beans.bankbalance"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
<style type="text/css">.menu ul li ul li ul.level-2{left:195px important;}</style>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="../css/shortcuts.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="../css/styles.css" type="text/css" media="screen" />
  <link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div class="vendor-page">
<div class="vendor-list">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">Total Petty Cash </td></tr>
</table>

<div class="total-petty-cash pt-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<form  class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8" method="post">
					<input type="hidden" name="page" value="totalPettyCash"/>
					<div class="form-group col-lg-6 col-md-6">
					<tr>
					<td>
				
	<select name="head_account_id" id="head_account_id"  onchange="formSubmit();">
	<%
	headofaccounts Headofaccounts = new headofaccounts();
	bankbalance Bankbalance = new bankbalance();
	bankdetails Bankdetails = new bankdetails();
	bankcalculations Bankcalculations = new bankcalculations();
	mainClasses.banktransactionsListing BanktransactionsListing = new mainClasses.banktransactionsListing();
	 mainClasses.bankbalanceListing BankbalanceListing = new mainClasses.bankbalanceListing();
	 mainClasses.bankdetailsListing BankdetailsListing = new mainClasses.bankdetailsListing();
	 
	 DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	 TimeZone tz = TimeZone.getTimeZone("IST");
	 dateFormat2.setTimeZone(tz);
	 
	 Calendar c2 = Calendar.getInstance(); 
	 String currentDateNew=(dateFormat2.format(c2.getTime())).toString();
	 String monthInString=currentDateNew.substring(5, 7);
	 int month = Integer.parseInt(monthInString);
	 String yearInString=currentDateNew.substring(0, 4);
	 int year = Integer.parseInt(yearInString);
	 
	 String finStartYr = "";

	 if(month >= 4)
	 {
			finStartYr = Integer.toString(year);
	 }
	 else
	 {
		 int finStartYrInInt = year - 1;
		 finStartYr = Integer.toString(finStartYrInInt);
	 }
	 
	 String finStartAndEndYr = "";
	 
	 String tempInString = finStartYr.substring(2, 4);
	 int tempInInt = Integer.parseInt(tempInString)+1;
	 finStartAndEndYr = finStartYr+"-"+tempInInt;
	DecimalFormat decimalFormat=new DecimalFormat("0.00");
	mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
	%><option value="" selected="selected">ALL DEPARTMENTS</option><%
	List HA_Lists=HOA_L.getheadofaccounts();
	if(HA_Lists.size()>0){
		for(int i=0;i<HA_Lists.size();i++){
			Headofaccounts=(beans.headofaccounts)HA_Lists.get(i);%>
				<option value="<%=Headofaccounts.gethead_account_id()%>" 
				<%if(request.getParameter("head_account_id")!=null && request.getParameter("head_account_id").equals(Headofaccounts.gethead_account_id())){ %>
				 selected="selected"
				  <%} %> >
				 <%=Headofaccounts.getname() %></option>
			<%}} %>
	</select></td>
	
	</tr>
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<button type="submit" class="btn btn-brown">Submit</button>
					</div>
					</form>
					<table class="table table-bordered pt-20">
						<tr class="bg-brown">
						   <th>S.No</th>
							<th>Bank Name</th>
							<th>Bank ID</th>
							<th>Petty Cash</th>
						</tr>
						<tr>
							<%
							String headId = request.getParameter("head_account_id");
							System.out.println("HeadofaccountId >>>>>>>" +Headofaccounts.gethead_account_id());
							List<bankdetails>  bankdetailsList = BankdetailsListing.getPettyCashBasedOnHeadAccountId(headId);
							
							 if(bankdetailsList.size()>0) {
								 for(int i=0; i < bankdetailsList.size();i++ ) {
									 Bankdetails = (bankdetails)bankdetailsList.get(i);	
								
								double pettycashOpeningbal=Bankcalculations.getBankOpeningBalance(Bankdetails.getbank_id(),finStartYr+"-04-01 00:00:01",currentDateNew+" 23:59:59","cashpaid","");
								double pettycashFinOpeningBal = BankbalanceListing.getBankOpeningBal(Bankdetails.getbank_id(),"cashpaid",finStartAndEndYr);
								pettycashOpeningbal += pettycashFinOpeningBal;
							
						%>
						<tr>
 						<td style="text-align:center;"><%=i+1%></td> 
							<td><%=Bankdetails.getbank_id()%></td>
							<td><%=Bankdetails.getbank_name()%></td>
							<td><%=decimalFormat.format(pettycashOpeningbal)%></td>
		 					  
		 					  <% }} %>
						</tr>
						
					</table>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</body>
</html>