<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>

<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock purchase entry</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	
	$('#vendorError').hide();
	$('#billError').hide();
	$('#checkNoError').hide();
	$('#nameError').hide();
	if($('#headAccountId').val().trim()==""){
		$('#billError').show();
		$('#headAccountId').focus();
		return false;
	}
	if($('#vendorId').val().trim()==""){
		$('#vendorError').show();
		$('#vendorId').focus();
		return false;
	}
	/* if($('#checkNo').val().trim()==""){
		$('#checkNoError').show();
		$('#checkNo').focus();
		return false;
	}
	if($('#nameOnCheck').val().trim()==""){
		$('#nameError').show();
		$('#nameOnCheck').focus();
		return false;
	}
	if(($('#amount0').val().trim()=="" || $('#vendorname0').val().trim()=="")){
		$('#vendorname0').addClass('borderred');
		$('#vendorname0').focus();
		return false;
	} */
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function vendorsearch(j){
	 var data1=$('#vendorId'+j).val();
	  var headID=$('#headAccountId').val();
	  if($('#headAccountId').val().trim()==""){
			$('#billError').show();
			$('#headAccountId').focus();
			return false;
		}
	  $.post('searchVendor.jsp',{q:data1,b:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		 $( '#vendorId'+j).autocomplete({source: availableTags}); 
			});
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var headID=$('#headAccountId').val();
	  if($('#headAccountId').val().trim()==""){
			$('#billError').show();
			$('#headAccountId').focus();
			return false;
		}
	  var arr = [];
	  $.post('searchProducts.jsp',{q:data1,hId:headID},function(data)
	{
		var response = data.trim().split("\n");
		 var amount=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#instock'+j).val(ui.item.amount);
				
			}
		}); 
			});
} 
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;

	u=document.getElementById("addcount").value;

	if(document.getElementById("product"+i).value!=""){

		if(document.getElementById("purchaseRate"+i).value!="")
		{
		
			for(var j=0;j<=u;j++){
			
				if(document.getElementById("purchaseRate"+j).value!=""){

				grossprice=Number(parseFloat(document.getElementById("purchaseRate"+j).value)*Number(parseFloat(document.getElementById("quantity"+j).value)));

			document.getElementById("grossAmt"+j).value=grossprice;
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice));
			
				}
				}
			document.getElementById("total").value=Totalprice;
		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;

}
$(document).ready(function(){
	$("#spcDate").hide();
	$("#department").change(function(){
	    var req=$("#department").val();
	    if(req==='specific Date'){
	    	$("#spcDate").show();
	    }else{
	    	$("#spcDate").hide();
	    }
	  });
	});
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<%if(session.getAttribute("adminId")!=null){ %>

<form name="tablets_Update" method="post" action="indent_Insert.jsp" onsubmit="return validate();">
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Indent report</div>
<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="billError" style="display: none;">Please select head of account.</div>Head of account*</td>

<td>Description</td>
</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<tr>
<td >
<select name="headAccountId" id="headAccountId" style="width:200px;">
<option value="">--select--</option>
<%mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();
for(int i=0; i < HOA_List.size(); i++ ){
	HOA=(beans.headofaccounts)HOA_List.get(i);%>
	<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname()%></option>
	<%}%>
</select>
</td>

<td><textarea  name="narration" id="narration" style="height: 25px;"></textarea></td>
</tr>
<tr>
<td>Stock Requirement*</td>
<td></td>

</tr>
<tr>
<td>
<select name="requirement" id="department">
<option value="Standard">Standard</option>
<option value="Urgent">Urgent</option>
<option value="specific Date">Specific Date</option>
</select></td>
<td id="spcDate"><input type="text" name="date" id="date" placeholder="Select order specific date" value="" /></td>

</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>


<div class="vendor-list">
<div class="sno bgcolr">S.No.</div>
<div class="ven-nme bgcolr">Vendor Name</div>
<div class="ven-nme bgcolr">Product Name</div>
<div class="ven-nme bgcolr">Required Qty</div>
<div class="ven-amt bgcolr">In Stock Qty</div>
<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 20; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

<div class="sno"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme"><input type="text" name="vendorId<%=i%>" id="vendorId<%=i%>" value="" onkeyup="vendorsearch('<%=i%>')" autocomplete="off" /></div>
<div class="ven-nme"><input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>')" class="" value="" autocomplete="off"/></div>
<div class="ven-nme"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  onblur="calculate('<%=i %>')" value="" autocomplete="off" style="width:100px;"></input></div>
<div class="ven-amt"><input type="text" name="instock<%=i%>" id="instock<%=i%>"  value="" style="width:100px;" readonly="readonly"></input></div>
</div>
<%} %>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/></div>
<!-- <div class="tot-ven">Total<input type="text" name="total" id="total" value="0.00" readonly="readonly" style="width:50px;"/></div> -->

<input type="submit" value="Create" class="click" style="border:none; float:right; margin:0 145px 0 0"/>

</div>



</div>
</form>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>