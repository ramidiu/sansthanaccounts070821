<%@page import="mainClasses.stockrequestListing"%>
<%@page import="mainClasses.medicineissuesListing"%>
<%@page import="mainClasses.medicalpurchasesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DateFormatSymbols"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	

</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
        function minorhead(id){
        	$('#'+id).toggle();
        }
    </script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function idValidate(){
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	var id=$('#major_head_id').val().trim();
$.ajax({
	type:'post',
	url: 'IDValidate.jsp', 
	data: {
		Id : id,
		type : 'majorhead'
	},
	success: function(response) { 
		var msg=response.trim();
		$('#majorheadErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#major_head_id').focus();
			return false;
		}
	}});
}

</script>
<script type="text/javascript">

function validate(){
	$('#headIdErr').hide();
	$('#majorheadErr').hide();
	$('#headNameErr').hide();

	if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	}
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	if($('#name').val().trim()==""){
		$('#headNameErr').show();
		$('#name').focus();
		return false;
	}
}
function productsearch(){
//alert("1111");
	  var data1=$('#productname').val();
	  var headID=$('#headAccountId').val();
	  $.post('searchProducts.jsp',{q:data1,hId:headID},function(data)
	{		//alert("111"+data);
			 var productNames=new Array();
		var response = data.trim().split("\n");

		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>

<div class="vendor-page">

<div class="vendor-box">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Product Ledger Report </td></tr>
</table>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
String productid="";String hid="";
if(request.getParameter("headAccountId")!=null){
	hid=request.getParameter("headAccountId");
}
if(request.getParameter("productname")!=null){
	productid=request.getParameter("productname");
}
%>

<form name="tablets_Update" method="get"  onsubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<input type="hidden" name="page" value="productReport"/>
<div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account<span style="color: red;">*</span></td>
<td>Product Name<span style="color: red;">*</span></td>
<td>Select Financial Year<span style="color: red;">*</span></td>
</tr>
<tr>
<td>
<select name="headAccountId" id="headAccountId">
<%if(!hid.equals("")){
	%>
	<option value="<%=hid%>" selected="selected"><%=HOA_L.getHeadofAccountName(hid) %></option>
	<%
}
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select>
</td>
<td><input type="text" name="productname" id="productname" value="<%=productid %> " onKeyUp="productsearch()" /></td>

			 
			 <td>
			 <select name="finYear" id="finYear">
						<%@ include file="yearDropDown1.jsp" %>
				</select>
			</td> 
<%-- <td>
<select name="finYear" id="finYear">
<%if(request.getParameter("finYear") == null){ %>
<option value="">-- Select Fin Year --</option><%} %>
<option value="2014-15" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2014-15")) {%> selected="selected"<%} %>>2014-15</option>
<option value="since2015" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("since2015")) {%> selected="selected"<%} %>>Since 2015</option>
<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%> selected="selected"<%} %>>2015-16</option>
<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
</select>
</td> --%>
<td align="right"><input type="submit" value="Search" class="click" style="border:none;"/></td>
</tr>

<tr> 
<td colspan="3"></td>

</tr>
</table>
</form>

</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list">
<!-- <div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
</ul>
</div> -->
<div class="icons">
					<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
<div class="clear"></div>
<div class="list-details">
<%productsListing PRD_L=new productsListing();
if(request.getParameter("productname")!=null){
productid=request.getParameter("productname");
//System.out.println("productid======>"+productid);
String temp[]=productid.split(" ");
if(temp.length>1){
if(PRD_L.getvalidProduct(temp[0])){ %>
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<div class="printable" id="tblExport">
<% 
System.out.println("finYear...."+request.getParameter("finYear"));

if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("")){
	String majorHeadId = "";
	if(request.getParameter("majorId") != null && !request.getParameter("majorId").equals(""))
	{
		majorHeadId = request.getParameter("majorId");
	}
%>
<table width="100%" cellpadding="0" cellspacing="0" border="1">

<tr><td  colspan="7"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="yourID">
<tr>
<td class="bg" width="5%">S.No.</td>
<td class="bg" width="15%">Month</td>
 <td class="bg" width="15%">Year</td> 
<td class="bg" width="20%">Opening Balance</td>
<td class="bg" width="20%">Inwards </td>
<td class="bg" width="20%">OutWards</td>
<td class="bg" width="20%">Closing Balance</td>

</tr>
</table></td></tr>
<%
String dos="2019-04-01";
SimpleDateFormat dateformat  = new SimpleDateFormat("yyyy-MM"); // Just the year, with 2 digits
//System.out.println("dateformat=====>"+dateformat);
String formattedDate = dateformat .format(Calendar.getInstance().getTime());
//System.out.println("formaed=====>"+formattedDate);
String balanceQuantityGodwan=PRD_L.getProductsExtra6(temp[0]);// this is added by madhv
double opengbal=Double.parseDouble(balanceQuantityGodwan);// this is added by madhav

System.out.println("opengbal=====>"+opengbal);



/* if(request.getParameter("finYear").equals("2015-16"))
{
	formattedDate = "2016-03";
}

else if(request.getParameter("finYear").equals("2016-17"))
{
	dos = "2016-04-01";
}
else if(request.getParameter("finYear").equals("2017-18"))
{
	dos = "2017-04-01";
}
else */ if(request.getParameter("finYear").equals("2018-19"))
{
	dos = "2018-07-01";
}
else if(request.getParameter("finYear").equals("2014-15"))
{
	dos = "2014-04-01";
	formattedDate = "2015-03";
}

int k = 1;
godwanstockListing GDSTK_L=new godwanstockListing();
stockrequestListing SRQL=new stockrequestListing();
shopstockListing SHPSTK_L=new shopstockListing();
medicineissuesListing MED_L=new medicineissuesListing();
DateFormatSymbols dfs = new DateFormatSymbols();
String[] months = dfs.getMonths();
String month = "";
int num=0;
String inwards="00";
//System.out.println("opengbal====== productReport===>>"+opengbal);
double closinbal=0;
double inwardsstock=0;
double closeingstock=0;
String outwards="";
String status="";
String statusStockRequest="";


DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM");

java.util.Date utilDate = (java.util.Date)dateFormat.parse(dos);
java.sql.Date d = new java.sql.Date(utilDate.getTime());
System.out.println("d=====>"+d);

/* Date d=(Date)dateFormat.parse(dos); */
java.util.Date utilDate2 = (java.util.Date)dateFormat.parse(formattedDate);
java.sql.Date curent = new java.sql.Date(utilDate2.getTime());
System.out.println("cureent date====="+curent);
/* Date curent=(Date)dateFormat.parse(formattedDate); */
Calendar c1 = Calendar.getInstance();
c1.setTime(d);
dos = (dateFormat.format(d)).toString();

System.out.println("dos=====>"+dos);

DecimalFormat df = new DecimalFormat("#.##");      


 while(!(d.after(curent)))
{ 
	 num=c1.get(c1.MONTH);
	if (num >= 0 && num <= 11 ) {
    month = months[num];
}
	opengbal = Double.valueOf(df.format(opengbal)); 
%>
<tr>
<td width="5%"><%=k++%></td>
<td width="17%"><span><a href="adminPannel.jsp?page=productDayReport&headAccountId=<%=hid%>&majorId=<%=majorHeadId%>&prdid=<%=temp[0]%>&dt=<%=dos%>&opengbal=<%=opengbal%>"><%=month%></a></span></td>
 <td width="17%"><%=c1.get(Calendar.YEAR)%></td> 
<td width="19%" name=><%=opengbal %></td>
<%
//System.out.println("dos==========>"+dos); 
//System.out.println("temp values====>"+temp[0]); 
status=GDSTK_L.getProfCharges(temp[0]);//added by madhav
statusStockRequest=SRQL.getStatus(temp[0]);//added by madhav
//System.out.println("Status======>"+status);
//System.out.println("statusStockRequest======>"+statusStockRequest);
//System.out.println("inwards0000===>"+inwards);
inwards=GDSTK_L.getgodwanstockbymonth(dos,temp[0]);

if(hid.equals("1") && majorHeadId.equals("60"))
{
	 outwards=MED_L.getmedicineissuesByMonth(dos,temp[0]);
}
else
{
	outwards=SHPSTK_L.getgShopstockbymonth(dos,temp[0]);
	
//	System.out.println("outwards=====>"+outwards); // consumption
}
	 if(inwards!=null && status!=null && status.equals("checked")){
		// System.out.println(" before opengbal inwords1111....."+opengbal);
		 opengbal=opengbal+Double.parseDouble(inwards);
		 //System.out.println(" after  opengbal inword =1111=====>"+opengbal);
		
		 } else{
			 inwards="0";
	 }
	 if(outwards!=null && statusStockRequest!=null && statusStockRequest.equals("issued")){
		 //System.out.println(" before opengbal.....2222"+opengbal);
		 opengbal=opengbal-Double.parseDouble(outwards);
		 //System.out.println(" ofter outwards 222======>"+opengbal);
	
		 } else{
			 outwards="0";
	 }
	    c1.setTime(d);
			c1.add(c1.MONTH, +1);
			 //System.out.println(" ofter outwards 3333======>"+opengbal);
			closinbal=opengbal;
			//System.out.println("closing bal 3333======>"+closinbal);
		%>
<td align="left" width="20%"><%=inwards %></td>
<td align="left" width="20%"><%=outwards %></td>
<%%>
 <td align="left" width="20%"><%=String.format("%.2f", closinbal) %></td> 
<%-- <td align="left" width="20%"><%= closinbal%></td> --%>
</tr>

<%opengbal=closinbal;
//System.out.println("opengbal bal new==="+closinbal); 
dos = (dateFormat.format(c1.getTime())).toString();
java.util.Date utilDate3 = (java.util.Date)dateFormat.parse(dos);
d = new java.sql.Date(utilDate3.getTime());

	/* d=(Date)dateFormat.parse(dos); */
} 
//System.out.println("closinbal111====>"+closinbal);%>
<tr><td colspan="6" align="right"><h>Total Stock :</h></td><td><%=String.format("%.2f", closinbal) %></td></tr>


</table><%} %>
<%} }else{%>
<span>No Reports</span>
<%} }%>
</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
