<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="java.util.List" %>  
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.vendorsListing"%>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function calculateTotalAmount(){
	var count=$('#count').val();
	var totalAmount=0;
	for(var i=0;i<count;i++){
	   var quantity=$('#quantity'+i).val();
	   var purchaseRate=$('#purchaseRate'+i).val();
	   var cgst=$('#cgst'+i).val();
	   var sgst=$('#sgst'+i).val();
	   var igst=$('#igst'+i).val();
	 totalAmount=totalAmount+((parseFloat(purchaseRate)+parseFloat(cgst)+parseFloat(sgst)+parseFloat(igst))*(quantity));
	}
	$('#totalAmount').val(Number(parseFloat(totalAmount)).toFixed(2));
} 
</script>
<script type="text/javascript">
function validateForm(){
	var count=$('#count').val();
	for(var i=0;i<count;i++){
		var quantity=$('#quantity'+i).val();
		var purchaseRate=$('#purchaseRate'+i).val();
		var cgst=$('#cgst'+i).val();
		var sgst=$('#sgst'+i).val();
		var igst=$('#igst'+i).val();
		if(quantity==""){
			$('#quantity'+i).focus().css('outline',"solid red 2px");
			return false;
		}
		if(purchaseRate==""){
			$('#purchaseRate'+i).focus().css('outline','solid red 2px');
			return false;
		}
		if(cgst==""){
			$('#cgst'+i).focus().css('outline','soild red 2px');
			return false;
		}
		if(sgst==""){
			$('#sgst'+i).focus().css('outline','solid red 2px');
			return false;
		}
		if(igst==""){
			$('#igst'+i).focus().css('outline','solid red 2px');
			return false;
		}
	}
	
}
</script>
</head>
<body>
<% 

   productsListing PRD_L=new productsListing();
   vendorsListing VND_L=new vendorsListing();
   String mseId=request.getParameter("mseId");
   godwanstockListing GL=new godwanstockListing();
   List<godwanstock> godwanStockList= GL.getMSEDetails(mseId);
%>
<div class="vendor-page">

<div class="vendor-box">
<div class="col-md-6">
<h1>Master Stock  Entry <%=mseId%> Details</h1>&nbsp;&nbsp;
<h1>Formula : (quantity)*(purchaseRate+cgst+sgst+igst)</h1>
</div>
<div class="col-md-6">
	<a href="adminPannel.jsp?page=productexpensesdetails&mseId=<%=mseId%>" style="float: right;margin: 0 80px;">Go To Expenses Entry Details</a>
</div>

<div class="vender-details">
<form action="gowdwanstockentryupdatedb.jsp" onsubmit="return validateForm();">
<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tbody><tr>
							<td class="bg" width="5%" >S.NO.</td>
							<td class="bg" width="8%" >Product Name</td>
							<td class="bg" width="8%">VendorId</td>
							<td class="bg" width="8%">Quantity</td>
							<td class="bg" width="8%" >Purchase Rate</td>
							<td class="bg" width="15%" >CGST</td>
								<td class="bg" width="15%" >SGST</td>
									<td class="bg" width="15%" >IGST</td>
									</tr>
						</tbody>
						
							<tbody>
							
							<%
							
							 if(godwanStockList!=null && godwanStockList.size()>0){
								 godwanstock gs=null;
							for(int i=0;i<godwanStockList.size();i++){
								gs=godwanStockList.get(i);
							%>
							<input type="hidden" name="count" id="count" value="<%=godwanStockList.size()%>">
							<input type="hidden" name="godwanstockid<%=i%>" value="<%=gs.getgsId()%>">
							<input type="hidden" name="mseId" value="<%=mseId%>">
							<tr>
							    
							    <td><%=i+1%></td>
								<td><input type="text" name="productId<%=i%>" id="productId" value="<%=gs.getproductId()%> <%=PRD_L.getProductsNameByCat(gs.getproductId(),gs.getdepartment())%>" readonly></td>
								<td><input type="text" name="vendorId<%=i%>" id="vendorId" value="<%=gs.getvendorId()%> <%=VND_L.getMvendorsAgenciesName(gs.getvendorId())%>" readonly></td>
								<td><input type="text" name="quantity<%=i%>" id="quantity<%=i%>" value="<%=gs.getquantity()%>" onkeyup="calculateTotalAmount();"></td>
								<td><input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" value="<%=gs.getPurchaseRate()%>"  onkeyup="calculateTotalAmount();"></td>
								<td><input type="text" style="width:88px;" name="cgst<%=i%>" id="cgst<%=i%>" value="<%=gs.getExtra18()%>"  onkeyup="calculateTotalAmount();"></td>
								<td><input type="text"  style="width:88px;" name="sgst<%=i%>" id="sgst<%=i%>" value="<%=gs.getExtra19()%>" onkeyup="calculateTotalAmount();"></td>
								<td><input type="text"  style="width:88px;" name="igst<%=i%>" id="igst<%=i%>" value="<%=gs.getExtra20()%>" onkeyup="calculateTotalAmount();"></td>
							</tr>	
							<%} %>
							
							<tr>
								<td colspan="7" align="right">Total Amount</td>
								<td><input type="text" name="totalAmount" id="totalAmount" value="<%=gs.getExtra16()%>"  style="width:55px;"></td>
							</tr>
							<tr>
								<td colspan="6"><input type="submit" value="Update" class="click" style="border:none; float:right; margin:0 115px 0 0"></td>
							</tr>
							<%}%>
						</tbody></table>
  </form>						
						</div>
					</div>
				</div>
			
</body>
</html>