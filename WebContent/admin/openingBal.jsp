<%@page import="beans.vendors"%>
<%@page import="beans.bankdetails"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.accounts.saisansthan.MapOfHeads"%>
<%@page import="beans.subhead"%>
<%@page import="beans.minorhead"%>
<%@page import="beans.majorhead"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="org.apache.commons.collections.map.MultiValueMap"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="beans.bankbalance"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.VendorsAndPayments"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Opening Balance List</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

</head>
<script type="text/javascript">
function checkForm()	{
	var department = document.getElementById('department').value;
	var finyear = document.getElementById('finyear').value;
	if (department.trim() === "")	{
		alert("Please Select Department");
		document.getElementById('department').focus();
		return false;
	}
	if (finyear.trim() === "")	{
		alert("Please Select finyear");
		document.getElementById('finyear').focus();
		return false;
	}
}
function checkData()	{
	var department = document.getElementById('dt').value;
	var finyear = document.getElementById('fn').value;
	if (department.trim() !== "")	{
		document.getElementById('department').value = department;
	}
	if (finyear.trim() !== "")	{
		document.getElementById('finyear').value = finyear;
	}
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<body onload="checkData()">
<div class="vendor-page">

			<div class="vendor-list">
				<div class="arrow-down">
					
				</div>
				<div class="search-list">
					<form action="adminPannel.jsp" method="post" onsubmit="return checkForm()">
					<input type="hidden" name="page" value="openingBal"/>
					<ul>
						<li><select name="department" id="department">
								<option value="">Select Department</option>
								<option value="1">Charity</option>
								<option value="4">Sansthan</option>
								<option value="3">PoojaStores</option>
								<option value="5">SaiNivas</option>
						</select></li>
						<li><select id="finyear" name="finyear">
								<option value="">Select Fin Year</option>
								<option value="2019-2020">2019-2020</option>
								<option value="2018-2019">2018-2019</option>
								<option value="2016-2017">2016-2017</option>
								<option value="2015-2016">2015-2016</option>
						</select></li>
						<li><input type="submit" class="click" name="search" value="Search"></li>
					</ul>
					</form>
				</div>
				<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span> 
					<span>
						<ul>
							<li><img src="../images/Setting-icon.png">
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting">Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting">Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<%if (request.getParameter("department") != null && request.getParameter("finyear") != null)	{
					String dept = request.getParameter("department");
					String finyear = request.getParameter("finyear");%>
					<input type="hidden" value="<%=dept %>" id="dt">
					<input type="hidden" value="<%=finyear %>" id="fn">
					<%
					String[] finyears = finyear.split("-");
					bankbalanceListing bankslisting = new bankbalanceListing();
					
					@SuppressWarnings("unchecked")
					List<bankbalance> assetslist = bankslisting.getAssetsLiabilitiesOfFinYrAndHOA("Assets",finyears[0]+"-"+finyears[1].substring(2),dept);
					@SuppressWarnings("unchecked")
					List<bankbalance> liabilitieslist = bankslisting.getAssetsLiabilitiesOfFinYrAndHOA("Liabilites",finyears[0]+"-"+finyears[1].substring(2),dept);
					List<bankbalance> debitbankslist = bankslisting.getBanksBsdOnFinYearHoa("debit",finyears[0]+"-"+finyears[1].substring(2),dept,"Bank");
					List<bankbalance> creditbankslist = bankslisting.getBanksBsdOnFinYearHoa("credit",finyears[0]+"-"+finyears[1].substring(2),dept,"Bank");
					List<bankbalance> pettyCashlist = bankslisting.getBanksBsdOnFinYearHoa("debit",finyears[0]+"-"+finyears[1].substring(2),dept,"Petty Cash");
					@SuppressWarnings("unchecked")
					List<bankbalance> counterCashlist = bankslisting.getAssetsLiabilitiesOfFinYrAndHOA("Counter Cash",finyears[0]+"-"+finyears[1].substring(2),dept);
					@SuppressWarnings("unchecked")
					List<bankbalance> vendorAssetlist = bankslisting.getVendorsOfFinYrAndHOA("Assets",finyears[0]+"-"+finyears[1].substring(2),dept,"debit");
					@SuppressWarnings("unchecked")
					List<bankbalance> vendorLiablist = bankslisting.getVendorsOfFinYrAndHOA("Liabilites",finyears[0]+"-"+finyears[1].substring(2),dept,"credit");
					
					MultiValueMap assetsMap = new MultiValueMap();
					MultiValueMap liabsMap = new MultiValueMap();
					MapOfHeads mapService = new MapOfHeads();
					Map<String,majorhead> majorMap = mapService.getMajorHeadsMap();
					Map<String,minorhead> minorMap = mapService.getMinorHeadsMap();
					Map<String,subhead> submap = mapService.getSubHeadsMap();
					Map<String,bankdetails> banksmap = mapService.getBanksMap();
					Map<String,vendors> vendorsMap = mapService.getVendorsMap();
					DecimalFormat df = new DecimalFormat("#,###.00");
					for (int i = 0 ; i < assetslist.size() ; i++)	{
						if (assetslist.get(i).getExtra3().equals("") && assetslist.get(i).getExtra4().equals("") && assetslist.get(i).getExtra5().equals("") && !assetslist.get(i).getExtra6().equals(""))	{
							assetslist.remove(assetslist.get(i)); continue;
						}
						assetsMap.put(assetslist.get(i).getExtra3(),assetslist.get(i));
					}
					for (int i = 0 ; i < liabilitieslist.size() ; i++)	{
						if (liabilitieslist.get(i).getExtra3().equals("") && liabilitieslist.get(i).getExtra4().equals("") && liabilitieslist.get(i).getExtra5().equals("") && !liabilitieslist.get(i).getExtra6().trim().equals(""))	{
							liabilitieslist.remove(i); continue;
						}
						liabsMap.put(liabilitieslist.get(i).getExtra3(),liabilitieslist.get(i));
					}
					double assetTotalAmount = 0.0;
					double liabTotAmount = 0.0;
					
					%>
					
					<div class="clear"></div>
					<div class="list-details">
					<div class="printable">
					<table width="80%"  id="tblExport" border="1" style="margin: 0 auto;" rules="all">
						<tbody>
						<tr>
							<td class="bg" width="23%">Assets</td>
							<td class="bg" width="23%" align="right">Opening Balance</td>
							<td class="bg" width="23%">Liabilites</td>
							<td class="bg" width="23%" align="right">Opening Balance</td>
						</tr>
						<tr>
							<td colspan="2">
								<table width="100%" border="1" rules="all">
								<%
								if (debitbankslist != null && debitbankslist.size() > 0)	{
									for (int i = 0 ; i < debitbankslist.size() ; i++)	{
									assetTotalAmount += Double.parseDouble(debitbankslist.get(i).getBank_bal());
									%>
									<tr>
										<td style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;">(<%=debitbankslist.get(i).getBank_id()%>)<%=banksmap.get(debitbankslist.get(i).getBank_id()).getbank_name() %></td>
										<td align="right" style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;"><%=df.format(Double.parseDouble(debitbankslist.get(i).getBank_bal())) %></td>
									</tr>	
									<%}
								}
								if (pettyCashlist != null && pettyCashlist.size() > 0)	{
									for (int i = 0 ; i < pettyCashlist.size() ; i++)	{
									assetTotalAmount += Double.parseDouble(pettyCashlist.get(i).getBank_bal());
									%>
									<tr>
										<td style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;">(<%=pettyCashlist.get(i).getBank_id()%>)<%=banksmap.get(pettyCashlist.get(i).getBank_id()).getbank_name() %></td>
										<td align="right" style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;"><%=df.format(Double.parseDouble(pettyCashlist.get(i).getBank_bal())) %></td>
									</tr>	
									<%}
								}
								if (counterCashlist != null && counterCashlist.size() > 0)	{
									for (int i = 0 ; i < counterCashlist.size() ; i++)	{
										assetTotalAmount += Double.parseDouble(counterCashlist.get(i).getBank_bal());%>
									<tr>
										<td style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;"><%=counterCashlist.get(i).getType() %></td>
										<td align="right" style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;"><%=df.format(Double.parseDouble(counterCashlist.get(i).getBank_bal())) %></td>
									</tr>
									<%}
								}
								if (assetsMap != null && assetsMap.size() > 0)	{
									Set<String> keys = assetsMap.keySet();
									Iterator<String> itr = keys.iterator();
									while(itr.hasNext())	{
										double majortotAmount = 0.0;
										String majorId = itr.next();
										List<bankbalance> banksBalList = (List<bankbalance>)assetsMap.get(majorId);
										MultiValueMap minorAssets = new MultiValueMap();
										for (bankbalance bb : banksBalList)	{
											majortotAmount += Double.parseDouble(bb.getBank_bal());
											assetTotalAmount += Double.parseDouble(bb.getBank_bal());
											minorAssets.put(bb.getExtra4(),bb);
										}%>
									<tr>
										<td style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;">(<%=majorId%>)<%=majorMap.get(majorId).getname() %></td>
										<td align="right" style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;"><%=df.format(majortotAmount) %></td>
									</tr>
								  <%
								  	if (minorAssets.size() > 0)	{
										Set<String> minorkeys = minorAssets.keySet();
										Iterator<String> minoritr = minorkeys.iterator();
										while (minoritr.hasNext())	{
											double minortotAmount = 0.0;
											String minorId = minoritr.next();
											List<bankbalance> minorList = (List<bankbalance>)minorAssets.get(minorId);
											for (bankbalance bb : minorList)	{
												minortotAmount += Double.parseDouble(bb.getBank_bal());
											}%>
										<tr>
											<td style="font-size: 12px;font-weight: bold;color: blue;font-family: monospace;">(<%=minorId%>)<%=minorMap.get(minorId).getname() %></td>
											<td align="right" style="font-size: 12px;font-weight: bold;color: blue;font-family: monospace;"><%=df.format(minortotAmount) %></td>
										</tr>
										<%
										for (bankbalance bb : minorList)	{%>
										<tr>
											<td style="font-size: 11px;font-weight: bold;color: indigo;font-family: monospace;">(<%=bb.getExtra5()%>)<%=submap.get(bb.getExtra5()).getname() %></td>
											<td align="right" style="font-size: 11px;font-weight: bold;color: indigo;font-family: monospace;"><%=df.format(Double.parseDouble(bb.getBank_bal())) %></td>
										</tr>
										<%}
									} // inner while
								  } // if
								 } // outer while
							} // outer if
							if (vendorAssetlist != null && vendorAssetlist.size() > 0)	{
								for (int i = 0 ; i < vendorAssetlist.size() ; i++)	{
								assetTotalAmount += Double.parseDouble(vendorAssetlist.get(i).getBank_bal());
								%>
								<tr>
									<td style="font-size: 12px;font-weight: bold;color: blue;font-family: monospace;">(<%=vendorAssetlist.get(i).getExtra6()%>)<%=vendorsMap.get(vendorAssetlist.get(i).getExtra6()).getagencyName() %></td>
									<td align="right" style="font-size: 12px;font-weight: bold;color: blue;font-family: monospace;"><%=df.format(Double.parseDouble(vendorAssetlist.get(i).getBank_bal())) %></td>
								</tr>
								<%}
							}
							%>
							 </table>
							 </td>
							 
							<td colspan="2">
							  <%
							  
							  double totalPending = 0.0;
								VendorsAndPayments vendorsAndPayments = new VendorsAndPayments();
								String fromdate = "2015-03-01 00:00:00";
								String toDate=finyear.substring(0,4)+"-04-01 23:59:59";
								List<String[]> vendorPendingBalances = new ArrayList<String[]>();
								vendorPendingBalances = vendorsAndPayments.getVendorOpeningAndPendingAmount("", dept, request.getParameter("finYear"), fromdate, toDate, "sum");
								if(vendorPendingBalances != null && vendorPendingBalances.size() > 0){
									for(int i = 0 ; i < vendorPendingBalances.size(); i++){
										String[] result = vendorPendingBalances.get(i);
										System.out.println("Test Result:::"+result);
										
										totalPending = totalPending + Double.parseDouble(result[5]);
										System.out.println("Test totalPending:::"+totalPending);
									}
									
								}
							  %>
							   <table width="100%" border="1" rules="all">
							   <tr >
										 <td style="color:#34eb3a" width="23%">SUNDRY CREDITORS</td>
							             <td  width="23%"><%=df.format(totalPending)%></td>
							   </tr>
							   <%
			         	             if(vendorPendingBalances != null && vendorPendingBalances.size() > 0){
			     						for(int i = 0 ; i < vendorPendingBalances.size(); i++){
			     							String[] result = vendorPendingBalances.get(i);
			     			  %>
			     			  <tr>
							         	      	<td width="47%"><%=result[0] %> <%=result[1] %></td>
								       			 <td>
									       			 <%-- <a href="<%=result[0] %>&date=<%=onlyfromdate%>to<%=onlytodate%>&type=<%=request.getParameter("cumltv")%>&hoa=<%=request.getParameter("head_account_id")%><%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")){%>&fy=<%=request.getParameter("finYear")%><%}%>" target="_blank"> --%>
									       			 	<%=df.format(Double.parseDouble(result[5]))%>
									       			 <!-- </a> -->
								       			 </td>
							         	  	 </tr>
			     							<%
			     						}
			     					}%>
							</table>
							
							 <table width="100%" border="1" rules="all">
								<%
								if (liabsMap != null && liabsMap.size() > 0)	{
									Set<String> keys = liabsMap.keySet();
									Iterator<String> itr1 = keys.iterator();
									while(itr1.hasNext())	{
										double majortotAmount = 0.0;
										String majorHeadId = itr1.next();
										List<bankbalance> banksBalList = (List<bankbalance>)liabsMap.get(majorHeadId);
										MultiValueMap minorLiabs = new MultiValueMap();
										for (bankbalance bb : banksBalList)	{
								
											System.out.println("uniq:::"+bb.getUniq_id()+"bal:::"+bb.getBank_bal());
											majortotAmount += Double.parseDouble(bb.getBank_bal());
											liabTotAmount += Double.parseDouble(bb.getBank_bal());
											minorLiabs.put(bb.getExtra4(),bb);
										}
										%>
										
									<tr>
										<td style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;">(<%=majorHeadId%>)<%=majorMap.get(majorHeadId).getname() %></td>
										<td align="right" style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;"><%=df.format(majortotAmount) %></td>
									</tr>
								  <%
								  	if (minorLiabs.size() > 0)	{
										Set<String> minorkeys = minorLiabs.keySet();
										Iterator<String> minoritr = minorkeys.iterator();
										while (minoritr.hasNext())	{
											double minortotAmount = 0.0;
											String minorId = minoritr.next();
											List<bankbalance> minorList = (List<bankbalance>)minorLiabs.get(minorId);
											for (bankbalance bb : minorList)	{
												minortotAmount += Double.parseDouble(bb.getBank_bal());
											}%>
										<tr>
											<td style="font-size: 12px;font-weight: bold;color: blue;font-family: monospace;">(<%=minorId%>)<%=minorMap.get(minorId).getname() %></td>
											<td align="right" style="font-size: 12px;font-weight: bold;color: blue;font-family: monospace;"><%=df.format(minortotAmount) %></td>
										</tr>
										<%
										for (bankbalance bb : minorList)	{%>
										<tr>
											<td style="font-size: 11px;font-weight: bold;color: indigo;font-family: monospace;">(<%=bb.getExtra5()%>)<%=submap.get(bb.getExtra5()).getname() %></td>
											<td align="right" style="font-size: 11px;font-weight: bold;color: indigo;font-family: monospace;"><%=df.format(Double.parseDouble(bb.getBank_bal())) %></td>
										</tr>
										<%}
									} // inner while
								  } // if
								 } // outer while
							} // outer if
							if (vendorLiablist != null && vendorLiablist.size() > 0)	{
								for (int i = 0 ; i < vendorLiablist.size() ; i++)	{
								liabTotAmount += Double.parseDouble(vendorLiablist.get(i).getBank_bal());
								%>
								<tr>
									<td style="font-size: 12px;font-weight: bold;color: blue;font-family: monospace;">(<%=vendorLiablist.get(i).getExtra6()%>)<%=vendorsMap.get(vendorLiablist.get(i).getExtra6()).getagencyName() %></td>
									<td align="right" style="font-size: 12px;font-weight: bold;color: blue;font-family: monospace;"><%=df.format(Double.parseDouble(vendorLiablist.get(i).getBank_bal())) %></td>
								</tr>
								<%}
							}
							%>
							<%
								if (creditbankslist != null && creditbankslist.size() > 0)	{
									for (int i = 0 ; i < creditbankslist.size() ; i++)	{
										liabTotAmount += Double.parseDouble(creditbankslist.get(i).getBank_bal());
									%>
									<tr>
										<td style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;">(<%=creditbankslist.get(i).getBank_id()%>)<%=banksmap.get(creditbankslist.get(i).getBank_id()).getbank_name() %></td>
										<td align="right" style="font-size: 15px;font-weight: bold;color: red;font-family: monospace;"><%=df.format(Double.parseDouble(creditbankslist.get(i).getBank_bal())) %></td>
									</tr>	
									<%}
								}%>
							 </table>
							</td>
						</tr>
						<tr>
							<td style="color:black;font-family: monospace;font-weight: bold;font-size: 15px;">Total</td>
							<td align="right" style="color:black;font-family: monospace;font-weight: bold;font-size: 15px;"><%=df.format(assetTotalAmount) %></td>
							<td style="color:black;font-family: monospace;font-weight: bold;font-size: 15px;">Total</td>
							<td align="right" style="color:black;font-family: monospace;font-weight: bold;font-size: 15px;"><%=df.format(liabTotAmount+totalPending)%></td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
				<%}%>
		</div>
	</div>
</body>
</html>