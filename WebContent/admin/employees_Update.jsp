<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="EMP" class="beans.employeesService">
<%if(session.getAttribute("adminId")!=null){
String emp_id=request.getParameter("emp_id");
String firstname=request.getParameter("firstname");
String email=request.getParameter("email");
String lastname=request.getParameter("lastname");
String join_date=request.getParameter("join_date");
String emp_sal=request.getParameter("emp_sal");
String emp_address=request.getParameter("emp_address");
String emp_phone=request.getParameter("emp_phone");
String headAccountId=request.getParameter("headAccountId");
String employeerole=request.getParameter("employeerole");
if(employeerole!=null && employeerole.equals("")){
	employeerole="Employee";
}
%>
<jsp:setProperty name="EMP" property="emp_id" value="<%=emp_id%>"/>
<jsp:setProperty name="EMP" property="firstname" value="<%=firstname%>"/>
<jsp:setProperty name="EMP" property="email" value="<%=email%>"/>
<jsp:setProperty name="EMP" property="lastname" value="<%=lastname%>"/>
<jsp:setProperty name="EMP" property="join_date" value="<%=join_date%>"/>
<jsp:setProperty name="EMP" property="emp_sal" value="<%=emp_sal%>"/>
<jsp:setProperty name="EMP" property="emp_address" value="<%=emp_address%>"/>
<jsp:setProperty name="EMP" property="emp_phone" value="<%=emp_phone%>"/>
<jsp:setProperty name="EMP" property="headAccountId" value="<%=headAccountId%>"/>
<jsp:setProperty name="EMP" property="extra1" value="<%=employeerole%>"/>
<%=EMP.update()%><%=EMP.geterror()%>
	
<script type="text/javascript">
window.parent.location="adminPannel.jsp?page=employeeDetails&id=<%=emp_id%>";
</script>
<%
}else{
	response.sendRedirect("index.jsp");
} %>
</jsp:useBean>