<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 50px;padding-top: 30px;">
	<input type="hidden" name="page" value="receipts-payments-report_neww">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin: 55px 0;"> 
	<!-- <tr>
			<td colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
			<td colspan="2">Select Financial Year</td>
			<td id = "li5" style="display:none;">From Date</td>&nbsp;
			<td id = "li6" style="display:none;">To Date</td>
	</tr> -->
	<!-- <tr> -->
		<!-- <td colspan="2"> -->
		<tbody><tr>
			<td style="width:14%;">Select Financial Year</td>
			<td style="width:20%;">From Date</td>
			<td style="width:20%;">To Date</td>
			<td style="width:14%;"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
			<td style="width:14%;">Select Cumulative Type</td>
		</tr>
		<tr>
			 
			 <td style="width:14%;">
					<select name="finYear" id="finYear" onchange="datepickerchange();" style="width:165px;">
						



	
					<option value="2018-19">2018-2019</option>
				
					<option value="2017-18">2017-2018</option>
				
					<option value="2016-17">2016-2017</option>
				
					<option value="2015-16">2015-2016</option>
				
					<option value="2014-15">2014-2015</option>
				
					<option value="2013-14">2013-2014</option>
				
					<option value="2012-13">2012-2013</option>
				
					<option value="2011-12">2011-2012</option>
				
					<option value="2010-11">2010-2011</option>
				
					<option value="2009-10">2009-2010</option>
				


					</select>
				  </td>
			 
		
		
			
			<td style="width:20%;"> <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="2017-04-01" class="hasDatepicker"><img class="ui-datepicker-trigger" src="../images/calendar-icon.png" alt="Select Date" title="Select Date"></td>
			<td style="width:20%;"><input type="text" name="toDate" id="toDate" readonly="readonly" value="2017-04-04" class="hasDatepicker"><img class="ui-datepicker-trigger" src="../images/calendar-icon.png" alt="Select Date" title="Select Date"></td>
				<td style="width:14%;">
				<select name="head_account_id" id="head_account_id" onchange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
				<!-- <option value="SasthanDev" >Sansthan Development</option> -->
				
					<option value="" selected="selected">ALL DEPARTMENTS</option>
					
					<option value="1" selected="selected">CHARITY</option>
					
					<option value="3">POOJA STORES</option>
					
					<option value="5">SAI NIVAS</option>
					
					<option value="4">SANSTHAN</option>
					 
				</select>  
			</td>  
			<td style="width:14%;">  
				<select name="cumltv" id="cumltv">
					<option value="noncumulative" selected="selected">NON CUMULATIVE</option>
					<option value="cumulative">CUMULATIVE</option>
				</select>
			</td>
			<td><input type="submit" value="SEARCH" class="click"></td>
		</tr>
	</tbody></table> 
</form>
<div class="printable" id="tblExport">	
		
		
		<table width="95%" cellspacing="0" cellpadding="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
			<tbody> 
				<tr>
					<td colspan="3" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY</td>
				</tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">RECEIPTS &amp; PAYMENTS  Report</td></tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  2017-04-01 TO 2017-04-04 </td></tr>
			</tbody>
		</table>
		<table width="90%" border="1" cellspacing="0" cellpadding="0" style="margin: 20px auto; font-size:13px;" class="new-table">
  			<tbody>
  				<tr>
				    <td width="38%" height="29" align="center" style="font-size:15px">Account(Debits)</td>
				    <td width="29%" align="center" style="font-size:15px">Debits</td>
				    <td width="33%" align="center" style="font-size:15px">Credits</td>
  				</tr>
  				<tr>
    				<td height="28" colspan="3" bgcolor="#99FF00" style="font-weight:bold;">RECEIPTS</td>
    			</tr>
  				<tr>
				    <td height="28">&nbsp;</td>
				    <td height="28" align="right">0.00</td>
				    <td height="28" align="right">0.00</td>
				</tr>	
			  	
				
				<tr>
				    <td height="28" style="color:#F00; font-weight:bold;">STATE BANK OF INDIA - CHARITY bank</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">204,599.39</td>
				</tr>
    			
				
				<tr>
				    <td height="28" style="color:#F00; font-weight:bold;">STATE BANK OF INDIA - CHARITY cashpaid</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">5,855.00</td>
				</tr>
    			
				
				<tr>
				    <td height="28" style="color:#F00; font-weight:bold;">CASH ACCOUNT</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">111,430.00</td>
				</tr>	
				
				<tr>
    			
    			
					<td height="28" align="center"><a href="#" onclick="minorhead('13');" style=" color: #934C1E; font-weight:bold;">11 SAIPRASAD</a></td>
					<td height="28" align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
    				
						
						
						
						
						
    				<td height="28" align="center" style=" color: #934C1E; font-weight:bold;">353,816.00</td>
						
					
					
					</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">122 LOANS AND ADVANCES </td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
	    					<td height="28" align="center" style="color:#F00;">19,493.00</td>
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20004 LOAN FROM NON DEVELOPMENT A/C</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">19,493.00</td>
	    								
	  							</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">123 OTHER INCOME </td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
							    
							    
							    
							    <td height="28" align="center" style="color:#F00;">2,364.00</td>
	    					
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20015 GENERAL SALE PROCEEDS</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">2,364.00</td>
	    								
	  							</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">124 DONATIONS</td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
							    
							    
							    
							    <td height="28" align="center" style="color:#F00;">10,881.00</td>
	    					
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20080 DONATION FOR CHALIVANDRAM</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">2,011.00</td>
	    								
	  							</tr>
					
								<!-- <tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20081 DONATION FOR DIAGNOSTIC SERVICES</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">6,760.00</td>
	    								
	  							</tr>
					
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20083 DONATION FOR EDUCATION</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">2,010.00</td>
	    								
	  							</tr>
					
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20087 DONATION FOR MEDICAL CENTER</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">100.00</td>
	    								
	  							</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">125 SEVA</td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
							    
							    
							    
							    <td height="28" align="center" style="color:#F00;">256,678.00</td>
	    					
	the below lines are added by srinivas
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20091 ANNADANAM</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">190,561.00</td>
	    								
	  							</tr>
					
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20095 PRASADM - PULIHORA</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">116.00</td>
	    								
	  							</tr>
					
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20092 NITYA ANNADANA PADHAKAM</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">66,001.00</td>
	    								
	  							</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">126 SALE OF PRASADAM</td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
							    
							    
							    
							    <td height="28" align="center" style="color:#F00;">64,400.00</td>
	    					
	the below lines are added by srinivas
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20097 DOODH PEDA</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">6,000.00</td>
	    								
	  							</tr>
					
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20098 LADDU</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">19,950.00</td>
	    								
	  							</tr>
					
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20099 OTHER PRASADAMS</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">10,410.00</td>
	    								
	  							</tr>
					
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20100 PULIHORA</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">28,040.00</td>
	    								
	  							</tr> -->
				
								<!-- <tr>
								  <td height="28">Total Rupess)</td>
								  <td height="28" align="right">0.00</td>
								  <td height="28" align="right">0.00</td>
								</tr> -->
	  				<!-- payments -->
								<tr>
								 <td height="28" colspan="3" bgcolor="#99FF00" style="font-weight:bold;">PAYMENTS</td>
								</tr>
								<!-- <tr>
				    <td height="28">&nbsp;</td>
				    <td height="28" align="right">0.00</td>
				    <td height="28" align="right">0.00</td>
				</tr> -->	
			  	
				
				<!-- <tr>
				    <td height="28" style="color:#F00; font-weight:bold;">STATE BANK OF INDIA - CHARITY bank</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">204,599.39</td>
				</tr> -->
    			
				
				<!-- <tr>
				    <td height="28" style="color:#F00; font-weight:bold;">STATE BANK OF INDIA - CHARITY cashpaid</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">5,855.00</td>
				</tr> -->
    			
				
				<!-- <tr>
				    <td height="28" style="color:#F00; font-weight:bold;">CASH ACCOUNT</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
				    <td height="28" align="center" style="color:#F00; font-weight:bold;">111,430.00</td>
				</tr>	 -->
				
				<tr>
    			
    			
					<td height="28" align="center"><a href="#" onclick="minorhead('13');" style=" color: #934C1E; font-weight:bold;">11 SAIPRASAD</a></td>
					<td height="28" align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
    				
						
						
						
						
						
    				<td height="28" align="center" style=" color: #934C1E; font-weight:bold;">353,816.00</td>
						
					
					
					</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">122 LOANS AND ADVANCES </td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
	    					<td height="28" align="center" style="color:#F00;">19,493.00</td>
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20004 LOAN FROM NON DEVELOPMENT A/C</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">19,493.00</td>
	    								
	  							</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">123 OTHER INCOME </td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
							    
							    
							    
							    <td height="28" align="center" style="color:#F00;">2,364.00</td>
	    					
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20015 GENERAL SALE PROCEEDS</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">2,364.00</td>
	    								
	  							</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">124 DONATIONS</td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
							    
							    
							    
							    <td height="28" align="center" style="color:#F00;">10,881.00</td>
	    					
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20080 DONATION FOR CHALIVANDRAM</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">2,011.00</td>
	    								
	  							</tr>
								
	  						
	  						<!-- Assets -->
	  
								<tr>
								  <td height="28" colspan="3" bgcolor="#99FF00" style="font-weight:bold;">ASSETS</td>
								</tr>
								<tr>
    			
    			
					<td height="28" align="center"><a href="#" onclick="minorhead('13');" style=" color: #934C1E; font-weight:bold;">11 SAIPRASAD</a></td>
					<td height="28" align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
    				
						
						
						
						
						
    				<td height="28" align="center" style=" color: #934C1E; font-weight:bold;">353,816.00</td>
						
					
					
					</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">122 LOANS AND ADVANCES </td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
	    					<td height="28" align="center" style="color:#F00;">19,493.00</td>
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20004 LOAN FROM NON DEVELOPMENT A/C</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">19,493.00</td>
	    								
	  							</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">123 OTHER INCOME </td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
							    
							    
							    
							    <td height="28" align="center" style="color:#F00;">2,364.00</td>
	    					
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20015 GENERAL SALE PROCEEDS</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">2,364.00</td>
	    								
	  							</tr>
					
						<tr>
						    
						    <td height="28" style="color:#F00;">124 DONATIONS</td>
						    <td height="28" align="center" style="color:#F00;">.00</td>
	    					
							    
							    
							    
							    <td height="28" align="center" style="color:#F00;">10,881.00</td>
	    					
	<!-- the below lines are added by srinivas -->
	   						
						</tr>
							
								<tr>
	  								
	    								
	    								<td height="28" style="padding-left:15px;">20080 DONATION FOR CHALIVANDRAM</td>
	    							
	    							<td height="28" align="left">.00</td>
	   								
	    								<td height="28" align="left">2,011.00</td>
	    								
	  							</tr>
								<tr>
								  <td height="28">&nbsp;</td>
								  <td height="28" align="right">0.00</td>
								  <td height="28" align="right">0.00</td>
								</tr>
	  							
	  
	  
	  	<!-- Assests -->
					 <tr>
						<td align="center" style=" color: #934C1E; font-weight:bold;" height="28">TOTAL AMOUNT OF PAYMENTS</td>
						<td align="center" style=" color: #934C1E; font-weight:bold;">Rs..00</td>
						<td align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
					</tr>  
	  				
	  				
	  				 
	   				<tr>
	    				<td height="28" bgcolor="#CCFF00" style="font-weight:bold;">STATE BANK OF INDIA - CHARITY bank</td>
	    				<td height="28" align="center" style="font-weight:bold;">545,049.39</td>
	    				<td height="28" align="center" style="font-weight:bold;">0.00</td>
	  				</tr>
	  				 
	   				<tr>
	    				<td height="28" bgcolor="#CCFF00" style="font-weight:bold;">STATE BANK OF INDIA - CHARITY cashpaid</td>
	    				<td height="28" align="center" style="font-weight:bold;">6,649.00</td>
	    				<td height="28" align="center" style="font-weight:bold;">0.00</td>
	  				</tr>
	  				
	  				<tr>
	    				<td height="28" bgcolor="#CCFF00" style="font-weight:bold;">CASH ACCOUNT</td>
					    <td height="28" align="center" style="font-weight:bold;">30,713.00</td>
					    <td height="28" align="center" style="font-weight:bold;">0.00</td>
	  				</tr> 
	  				
					
	   				<tr>
	    				<td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
	   					<td align="center" style="font-weight:bold; color: #934C1E;">Rs.582,411.39</td>
	    				<td align="center" style="font-weight:bold; color: #934C1E;">Rs.582,411.39</td>
	    				
	  				</tr>
	  				
  			</tbody>
	</table>

</div>
</body>
</html>