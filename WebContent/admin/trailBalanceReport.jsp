<%@page import="java.math.BigDecimal"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Trail Balance REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
<%@page import="java.util.List"%>
</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<%headofaccountsListing HOA_L=new headofaccountsListing();
	List headaclist=HOA_L.getheadofaccounts();%>
	<div>
			<div class="vendor-page">

		<div class="vendor-list">
						<form action="adminPannel.jsp" method="post" id="formLoad">
				<div class="search-list">
					<ul>
						<%List HA_Lists=HOA_L.getheadofaccounts();
						String fromdate="2014-04-01";
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}
						%>
						<li>
						<input type="hidden" name="page" value="trailBalanceReport"></input>
						<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
						<li>
						<select name="hoid" id="hoid" required>
						<%if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%>  selected="selected" <%} %>/> <%=HOA.getname() %><%}} %>
<option value="" <%if(request.getParameter("hoid")!=null &&request.getParameter("hoid").equals("")){%> selected="selected"<%} %>>ALL</option>
</select></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					
					</ul>
				</div></form>
					<% String finyr[]=null;	
				double credit=0.0;
				double debit=0.0;
				DecimalFormat bd=new  DecimalFormat("#,###.00");
				productexpensesListing PEXP_L=new productexpensesListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				Calendar c1 = Calendar.getInstance(); 
				TimeZone tz = TimeZone.getTimeZone("IST");
				 String hoaid[]=null;
				if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
					hoaid=new String[]{""+request.getParameter("hoid")};
				} else {
				hoaid=new String[]{"1","3","4","5"};
				} 
				String currentDate=(onlydat.format(c1.getTime())).toString();
				String finyrfrm=fromdate+" 00:00:01";
				String finyrto=todate+" 23:59:59";
                 String prevhead="";
                 String preshead="";
				customerpurchasesListing CP_L=new customerpurchasesListing();
				majorheadListing MH_L=new majorheadListing();
				List majhdlist=null;
				%>
				<div class="icons">
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	<div class="printable">
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:96%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
					<tr><td colspan="4">
                    	<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" class="yourID">
<tr>
						<td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td>
					</tr>
					<tr><td colspan="4" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="4" align="center" style="font-weight: bold;">Trail Balance Report </td></tr>
					<tr><td colspan="4" align="center" style="font-weight: bold;">Report From Date  <%=onlydat.format(dbdat.parse(finyrfrm)) %> TO <%=onlydat.format(dbdat.parse(finyrto)) %> </td></tr>
                    <tr>
							<td class="bg"  style="font-weight: bold;" width="10%" align="center">Head Of Account</td>
							<td class="bg" colspan="0" style="font-weight: bold;" width="40%" align="left">MAJOR HEAD</td>
							<td class="bg" colspan="0" style="font-weight: bold;" width="25%" align="left">DEBIT</td>
							<td class="bg" colspan="0" style="font-weight: bold;" width="25%" align="left">CREDIT</td>
						</tr>
                        
</table>
                        </td></tr>
                    
						<tr>
						<td colspan="4">
						<table width="100%" cellpadding="0" cellspacing="0" border="0" style="">
						
								<%for(int h=0;h<hoaid.length;h++)
								{
								majhdlist=MH_L.getMajorHeadBasdOnCompany(hoaid[h]);
											if(majhdlist.size()>0)
											{ 
												int j=0;
												for(int i=0;i<majhdlist.size();i++){
												MH=(majorhead)majhdlist.get(i);
												%>	
						    				
												
								
								<%
								//to display only details which have some credit and debit...
								if(!(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]))==0.0 && Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,""))==0.0))
								{ 
								  %>	
								  <tr style="">
												<td style="font-weight: bold;" width="10%" align="center">
											<%if(h>0){
													if(!MH.gethead_account_id().equals(hoaid[h-1]) && i==0)
													{
														%><span><%=HOA_L.getHeadofAccountName(hoaid[h])%></span><%
													}
									
													} 
													else if(h==0 && j==0)
													{
														j++;
														%><span><%=HOA_L.getHeadofAccountName(hoaid[h])%></span><%
													}%>
										</td>
								
								
									<td style="font-weight: bold;" colspan="0" width="40%" align="left"><a href="adminPannel.jsp?page=minorheadTrailBalanceReport&majrhdid=<%=MH.getmajor_head_id()%>&fromDate=<%=onlydat.format(dbdat.parse(finyrfrm)) %>&toDate=<%=onlydat.format(dbdat.parse(finyrto)) %>"><%=MH.getname() %></a></td>
									<td style="font-weight: bold;" colspan="0" width="25%" align="left"><%=bd.format(Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"")))%></td>
									<% 
										debit=debit+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,""));
										if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]).equals("00"))
										{ %>
											<td style="font-weight: bold;" colspan="0" width="25%" align="left"><%=bd.format(Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]) ))%>		</td>
											<%credit=credit+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h])); 
										} 
										else
										{ %>
											<td style="font-weight: bold;" colspan="0" width="25%" align="left"><%=bd.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h]))) %>	</td>
											<%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",finyrfrm,finyrto,"",hoaid[h])); 
										} 
										
										%></tr><%
									}//outer if
								%>
						
					<%}			
				}
			}%>
				
					   </table>
						</td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="2" align="center" style="font-weight: bold;" width="50%">Total  </td>
						<td style="font-weight: bold;" colspan="0" align="left" width="25%">Rs.<%=bd.format(debit) %></td>
					<td style="font-weight: bold;" colspan="0" align="left" width="25%">Rs.<%=bd.format(credit) %> </td>
						</tr>
						
 						<tr style="border: 1px solid #000;">
						<td colspan="2" align="center" style="font-weight: bold;" width="50%">Balance Left  </td>
						<td style="font-weight: bold;" colspan="0" align="left" width="25%">Rs.<%=bd.format(credit-debit) %></td>
						<td style="font-weight: bold;" colspan="0" align="left" width="25%"> </td>
					
						</tr>
						
						<tr style="border: 1px solid #000;">
						<td colspan="2" align="center" style="font-weight: bold;" width="50%">Amount Left </td>
						<td style="font-weight: bold;" colspan="0" align="left" width="25%">Rs.<%=bd.format(credit-debit) %></td> 
						<td style="font-weight: bold;" colspan="0" align="left" width="25%"> </td>
						<%-- <td style="font-weight: bold;" colspan="0" align="left" width="25%">&#8377;<%=sun.misc.FloatingDecimal.readJavaFormatString((bd.format(credit)) - sun.misc.FloatingDecimal.readJavaFormatString((bd.format(debit))  %></td> --%>						</tr>
					</table>
			</div>
				
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>