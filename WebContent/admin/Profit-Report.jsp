<%@page import="beans.products"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SALE VAT REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!-- <link href="css/printStyle.css" type="text/css" rel="stylesheet" /> -->
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>

<link rel="stylesheet" href="admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
function productsearch(){

	  var data1=$('#productname').val();
	  var headID="3";
	  $.post('searchProducts.jsp',{q:data1,page:"",hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
function sortByVat(){
	var vat=$("#vat").val();
	$("#formLoad").submit();
	//location.reload('godwanForm.jsp?poid='+poid); 
	//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="PUR" class="beans.godwanstock" />
		<jsp:useBean id="SAL" class="beans.customerpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<div class="sj">
					  <form action="adminPannel.jsp" method="post">  
					    <input type="hidden" name="page" value="Profit-Report"></input>
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form action="adminPannel.jsp" method="post" id="formLoad">
				<div class="search-list">
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text" name="productname" id="productname" <%if(request.getParameter("productname")!=null){ %> value="<%=request.getParameter("productname") %>" <%}else{ %> value="" <%} %> onkeyup="productsearch()" placeholder="Find product here" autocomplete="off"/></li>
						<li ><select name="vat" id="vat">
						<option value="">Sort By VAT (%)</option>
						<option value="0" <%if(request.getParameter("vat")!=null && request.getParameter("vat").equals("0")){ %> selected="selected"  <%} %> >0 (%)</option>
						<option value="5" <%if(request.getParameter("vat")!=null && request.getParameter("vat").equals("5")){ %> selected="selected"  <%} %>>5 (%)</option>
						<option value="14.5" <%if(request.getParameter("vat")!=null && request.getParameter("vat").equals("14.5")){ %> selected="selected"  <%} %>>14.5 (%)</option>
						</select></li>
						<li>
						<input type="hidden" name="page" value="Profit-Report"></input>
						<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					
					</ul>
				</div></form>
				<div class="icons">
					<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	customerpurchasesListing CUS_PL=new customerpurchasesListing();
	godwanstockListing GOD_SL=new godwanstockListing();
	productsListing PRO_L=new productsListing();
	vendorsListing VENL=new vendorsListing();
	productsListing PRDL=new productsListing();
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:00";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	
	
	String product="";
	if(request.getParameter("productname")!=null && !request.getParameter("productname").equals("")){
		String	pro[]=request.getParameter("productname").split(" ");
		product=pro[0];
	}
	String vat[]={"0","5","14.5"}; 
	if(request.getParameter("vat")!=null && !request.getParameter("vat").equals("")){
		vat=request.getParameterValues("vat");
	}
	productsListing PROL=new productsListing();
	List PRODL=null;
	List Sale_List=null;
	DecimalFormat df = new DecimalFormat("0.00");
	
	double vatAmt=0.00;
	double vatTotalAmt=0.00;
	double totalAmount=0.00;
	double VatIOAmt=0.00;
	double VatOPAmt=0.00;
	double ProfitAmt=0.00;
	double TotProfitAmt=0.00;
	double totCollAmt=0.00;
	double totAmt=0.00;
	/* if(Sale_List.size()>0){ */%>
	<div class="printable">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}
</style>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<jsp:useBean id="PRO" class="beans.products"/>
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="11" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST POOJA STORES</td></tr>
						<tr><td colspan="11" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
						<tr><td colspan="11" align="center" style="font-weight: bold;">SALES PROFIT Report</td></tr>
						<tr><td colspan="11" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dateFormat.parse(fromDate))%>  TO  <%=df2.format(dateFormat.parse(toDate)) %> </td></tr>
						<tr>
						<td colspan="11">
						<table width="100%" cellpadding="0" cellspacing="0" border='1'>
						<tr><td colspan="11"><table width="100%" cellpadding="0" cellspacing="0" border='1' class="yourID">
						<tr>
							<td class="bg" width="3%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="6%" style="font-weight: bold;">CODE</td>
							<td class="bg" width="24%" style="font-weight: bold;" align="center">PRODUCT NAME</td>
							<td class="bg" width="8%" style="font-weight: bold;" align="center">PURCHASE RATE </td>
							<td class="bg" width="4%" style="font-weight: bold;" align="center">VAT % </td>
							<td class="bg" width="12%" style="font-weight: bold;" align="center">VAT I/O (VAT ON PURCHASES) </td>
							<td class="bg" width="8%" style="font-weight: bold;" align="center">SALE RATE </td>
							<td class="bg" width="4%" style="font-weight: bold;" align="center">VAT % </td>
							<td class="bg" width="12%" style="font-weight: bold;" align="center">VAT O/P (VAT ON SALES)</td>
							<td class="bg" width="6%" style="font-weight: bold;" align="center">SALE QTY </td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">PROFIT</td>
						</tr>
					</table></td></tr>
						<%
						if(vat.length>0){
						for(int v=0;v<vat.length;v++){
						Sale_List=CUS_PL.getSalesListForVatReportNew("3", fromDate, toDate,product,vat[v]);
						%>
						<tr align="center" style="font-size: large;"><td class="bg" colspan="11" width="2%" style="font-weight: bold;">"<%=vat[v] %> %" Vat product sales </td></tr>
						<%
						for(int i=0; i < Sale_List.size(); i++ ){ 
							SAL=(beans.customerpurchases)Sale_List.get(i);
							PRODL=PROL.getMproducts(SAL.getproductId());
							totCollAmt=CUS_PL.getTotAmt(fromDate,toDate);
							totAmt=CUS_PL.getTotAmtWithOutRoundOff(fromDate,toDate);
							if(PRODL.size()>0){
								PRO=(products)PRODL.get(0);
							}
							%>
						    <tr style="position: relative;">
								<td style="font-weight: bold;" width="3%"><%=i+1%></td>
								<td style="font-weight: bold;" width="6%"><%=SAL.getproductId()%></td>
								<td style="font-weight: bold;"width="24%" ><%=SAL.getproductId()%> - <%=PRDL.getProductsNameByCat(SAL.getproductId(), SAL.getextra1())%></td>
								<%
								vatAmt=Double.parseDouble(SAL.getrate())*(Double.parseDouble(vat[v])/100);
								vatTotalAmt=vatTotalAmt+vatAmt;
								
								totalAmount=totalAmount+Double.parseDouble(SAL.gettotalAmmount());
								VatIOAmt=((Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(SAL.getquantity()))*(Double.parseDouble(PRO.getvat())/100));
								VatOPAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(SAL.getquantity()))*(Double.parseDouble(SAL.getextra13())/100);
								
								ProfitAmt=(Double.parseDouble(PRO.getsellingAmmount())*Double.parseDouble(SAL.getquantity()))-(Double.parseDouble(PRO.getpurchaseAmmount())*Double.parseDouble(SAL.getquantity()))-VatOPAmt;
								TotProfitAmt=TotProfitAmt+ProfitAmt;
								%>
								<td width="8%" align="center"><%=PRO.getpurchaseAmmount()%></td>
								<td width="4%" align="center"><%=PRO.getvat()%> </td>
								<td width="12%" align="center"><%=df.format(VatIOAmt)%></td>
								<td  width="8%" align="center"><%=PRO.getsellingAmmount()%></td>
								<td width="4%" align="center"><%=SAL.getextra13()%> </td>
								<td width="12%" align="center"><%=df.format(VatOPAmt)%></td>
								<td  width="8%" align="center"><%=SAL.getquantity()%></td>
								<td style="font-weight: bold;" width="10%" align="center">Rs.<%=df.format(ProfitAmt)%></td>
								<%-- <td style="font-weight: bold;" width="10%" align="center">&#8377; <%=df.format(Double.parseDouble(SAL.gettotalAmmount()))%></td> --%>
							</tr>
						<%} %>
						<%}}
						//System.out.println("totAmt:::"+totAmt+"::::totCollAmt:::"+totCollAmt);
						%>
					   </table>
					   </td>
					   </tr>
						<tr style="border: 1px solid #000;">
							<td colspan="8" width="30%"></td>
							<td align="right" style="font-weight: bold;">Total  Profit amount | </td>
							<td style="font-weight: bold;"  align="center"><%-- &#8377; <%=df.format(vatTotalAmt)%> --%></td>
							<td style="font-weight: bold;" align="center" colspan="2"> Rs.<%=df.format(TotProfitAmt+(totCollAmt-totAmt))%></td>
						</tr>
					</table>
				</div>
				
					<%-- <%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There are no products purchases list found!</span></div>
					</div>
					<%}%> --%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>