<%@page import="java.util.List, java.util.ArrayList" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>



<jsp:useBean id="PER" class="beans.employepermissions"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Permissions</div></div>

<div class="vendor-list">
<%
mainClasses.employepermissionsListing PER_CL = new mainClasses.employepermissionsListing();
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
String employeeId=request.getParameter("empId");
String accountId=request.getParameter("accountId");
%>
<form action="employepermissions_Insert.jsp" >
<div><u>Permissions for <%=HOA_CL.getHeadofAccountName(accountId)%> :</u></div>
<table width="50%" cellpadding="0" cellspacing="0">
<input type="hidden" name="head_account_id" value="<%=accountId%>">
<input type="hidden" name="emp_id" value="<%=employeeId%>">
<%
List PER_List=PER_CL.getEmployeDetratmentpermissions(employeeId,accountId);
ArrayList permisions=new ArrayList();
if(PER_List.size()>0){
PER=(beans.employepermissions)PER_List.get(0);
%>
<input type="hidden" name="permission_id" id="permission_id" value="<%=PER.getpermission_id()%>"/>
<%
String perm[]=PER.getemp_permissions().split(",");

for(String p: perm)
permisions.add(p);

}
if(accountId.equals("1")){%>
<tr>
<td colspan="2">Medical Hall(Tablets purchases and maintenance)</td>
<td><input type="checkbox" id="medicalHall" name="medicalHall" <%if(permisions.contains("Medical Hall")){%> checked="checked" <%}%> value="Medical Hall"/>
</tr>
<tr>
<td colspan="2">Reception(Patient and Doctor maintenance )</td>
<td><input type="checkbox" id="reception" name="reception" <%if(permisions.contains("Reception")){%> checked="checked" <%}%> value="Reception"/>
</tr>
<%}
else if(accountId.equals("3") || accountId.equals("5")){
%>
<tr>
<td colspan="2">Shop(Products Sales)</td>
<td><input type="checkbox" id="shop" name="shop" <%if(permisions.contains("Shop")){%> checked="checked" <%}%> value="Shop"/>
</tr>
<tr>
<td colspan="2">Godwan(Purchase stock Entry)</td>
<td><input type="checkbox" id="godwan" name="godwan" <%if(permisions.contains("Godwan")){%> checked="checked" <%}%> value="Godwan"/>
</tr>
<tr>
<td colspan="2">Stock Issue</td>
<td><input type="checkbox" id="stockTransfer" name="stockTransfer" <%if(permisions.contains("stockTransfer")){%> checked="checked" <%}%> value="stockTransfer"/>
</tr>
<%}else if(accountId.equals("5")){ %>
<tr>
<td colspan="2">Give Receipts(Tickets sale)</td>
<td><input type="checkbox" id="Receipts" name="Receipts" <%if(permisions.contains("Receipts")){%> checked="checked" <%}%> value="Receipts"/>
</tr>
<%}else if(accountId.equals("4")){ %>
<tr>
<td colspan="2">Stock Issue</td>
<td><input type="checkbox" id="stockTransfer" name="stockTransfer" <%if(permisions.contains("stockTransfer")){%> checked="checked" <%}%> value="stockTransfer"/>
</tr>
<tr>
<td colspan="2">Give Receipts(Tickets sale)</td>
<td><input type="checkbox" id="Receipts" name="Receipts" <%if(permisions.contains("Receipts")){%> checked="checked" <%}%> value="Receipts"/>
</tr>
<%} %>
<tr>
<td colspan="2">PRODUCT RETURN FORM</td>
<td><input type="checkbox" id="returnProduct" name="returnProduct" <%if(permisions.contains("returnProduct")){%> checked="checked" <%}%> value="returnProduct"/>
</tr>
<tr>
<td colspan="2">Add Counter</td>
<td><input type="checkbox" id="counter" name="counter" <%if(permisions.contains("counter")){%> checked="checked" <%}%> value="counter"/>
</tr>
<tr>
<td colspan="2">Stock request option</td>
<td><input type="checkbox" id="stockrequest" name="stockrequest" <%if(permisions.contains("stockRequest")){%> checked="checked" <%}%> value="stockRequest"/>
</tr>
<tr>
<td colspan="2">Add Product</td>
<td><input type="checkbox" id="productadd" name="productadd" <%if(permisions.contains("ProductAdd")){%> checked="checked" <%}%> value="ProductAdd"/>
</tr>
<tr>
<td colspan="2">GATEPASS ENTRY</td>
<td><input type="checkbox" id="gatepass" name="gatepass" <%if(permisions.contains("gatepass")){%> checked="checked" <%}%> value="gatepass"/>
</tr>
 <tr>
<td colspan="3"><input type="submit"  value="Submit"/>
</tr> 

</table>
</form>
</div>
</div>

