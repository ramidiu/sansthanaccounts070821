<%@page import="mainClasses.minorheadListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.products"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script>
function idValidate(){
	/* if($('#Product_id').val().trim()==""){
		$('#ProductIdErr').show();
		$('#Product_id').focus();
		return false;
	} */
	var id=$('#Product_id').val().trim();
	var hid=$('#head_account_id').val().trim();
	var id1=$('#major_head_id').val().trim();
	var id2=$('#minor_head_id').val().trim();
	var id3=$('#sub_head_id').val().trim();
	
$.ajax({
	
	type:'post',
	url: 'IDValidate1.jsp', 
	data: {
		Id : id,
		Id1 : id1,
		Id2 : id2,
		Id3 : id3,
		type : 'product',
		hoid : hid
	},
	success: function(response) { 
		var msg=response.trim();
		$('#ProductIdErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#Product_id').focus();
			return false;
		}
	}});
}

</script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#ProductIdErr').hide();
	$('#ProductNameErr').hide();
	$('#MajorIdErr').hide();


	if($('#major_head_id').val().trim()==""){
		$('#MajorIdErr').show();
		$('#major_head_id').focus();
		return false;
	}
	 if($('#minor_head_id').val().trim()==""){
		$('#MinorIdErr').show();
		$('#minor_head_id').focus();
		return false;
	} 
	if($('#Product_id').val().trim()==""){
		$('#ProductIdErr').show();
		$('#Product_id').focus();
		return false;
	}
	if($('#Product_name').val().trim()==""){
		$('#ProductNameErr').show();
		$('#Product_name').focus();
		return false;
	}
	 var hoid=$("#head_account_id").val();
	 if(hoid=='3'){
		if($('#buyprice').val().trim()==""){
			$("#buyprice").attr('class', 'borderred');
			$('#buyprice').focus();
			return false;
		}
		 if($('#vatbuy').val().trim()==""){	
			$("#vatbuy").attr('class', 'borderred');
			$('#vatbuy').focus();
			return false;
		}
	 }
	 var category = $('#cat1').val();
	 if ($.trim(category) !== "")	{
		 var sub = $('#subcat1').val();
		 if ($.trim(sub) === "")	{
			 alert("Please Select Sub Category");
			 return false;
		 }
	 } 
	 
	 var category1 = $('#cat2').val();
	 if ($.trim(category1) !== "")	{
		 var sub1 = $('#subcat2').val();
		 if ($.trim(sub1) === "")	{
			 alert("Please Select Sub Category");
			 return false;
		 }
	 } 
}
function calculate(i){
	var vat=0;
	var bypr=0;	
	var bygr=0;	
	vat=document.getElementById("vat").options[document.getElementById("vat").selectedIndex].value;
	if(i=='1'){
if(vat!=""){
	document.getElementById("vatbuy").value=vat;
	if(document.getElementById("buyprice").value!=""){
		bygr=Number(parseFloat(document.getElementById("buyprice").value)+Number(parseFloat(document.getElementById("buyprice").value)*(parseFloat(vat)/100)));
		document.getElementById("buygross").value=bygr.toFixed(0);
	}
	else if(document.getElementById("buygross").value!=""){
		bypr=Number(parseFloat(document.getElementById("buygross").value)-Number(parseFloat(document.getElementById("buygross").value)*(parseFloat(vat)/100)));
		document.getElementById("buyprice").value=bypr.toFixed(0);
	}
}
	}
	else if(i=='2'){
		if(document.getElementById("vatsell").value!=""){
						if(document.getElementById("sellprice").value!=""){
				bygr=Number(parseFloat(document.getElementById("sellprice").value)+Number(parseFloat(document.getElementById("sellprice").value)*(parseFloat(document.getElementById("vatsell").value)/100)));
				document.getElementById("sellgross").value=bygr.toFixed(0);
			}
			else if(document.getElementById("sellgross").value!=""){
				bypr=Number(parseFloat(document.getElementById("sellprice").value)+Number(parseFloat(document.getElementById("sellprice").value)*(parseFloat(document.getElementById("vatsell").value)/100)));
				document.getElementById("sellgross").value=bypr.toFixed(0);
			}
		}
	}
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
$(document).ready(function(){
  $("#major_head_id").keyup(function(){
	  var data1=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#major_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#minor_head_id").keyup(function(){
	  var data1=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#minor_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#sub_head_id").keyup(function(){
	  var data1=$("#sub_head_id").val();
	  var minor=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#sub_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  
  $('#cat1').change(function(){
	 	var cat = $(this).val();
	 	if ($.trim(cat) !== "")	{
	 		$('#subcat1').empty();
	 		$('#subcat1').append($('<option>', { value: "",text: 'Select SubCategory'}));
	 		$.ajax({
	 			 url : "searchSubHeadSj.jsp",
	 			 type : "GET",
	 			 data : {cat : cat},
	 			 success : function(response)	{
	 				var data = response.split(",");
	 				for (var i = 0 ; i < data.length ; i++)	{
	 					if (data[i].trim() !== "")	{
	 	 					$('#subcat1').append($('<option>', { value: data[i],text: data[i].toUpperCase()}));
	 					}
	 				}
	 			 }
	 		});
	 	}
	  });
	  $('#cat2').change(function(){
		 	var cat = $(this).val();
		 	if ($.trim(cat) !== "")	{
		 		$('#subcat2').empty();
		 		$('#subcat2').append($('<option>', { value: "",text: 'Select SubCategory'}));
		 		$.ajax({
		 			 url : "searchSubHeadSj.jsp",
		 			 type : "GET",
		 			 data : {cat : cat},
		 			 success : function(response)	{
		 				var data = response.split(",");
		 				for (var i = 0 ; i < data.length ; i++)	{
		 					if (data[i].trim() !== "")	{
		 	 					$('#subcat2').append($('<option>', { value: data[i],text: data[i].toUpperCase()}));
		 					}
		 				}
		 			 }
		 		});
		 	}
		  });
	 	
	 	var cat2 = $('#cat2').val();
	 	var sub = $('#sub').val();
	 	if ($.trim(cat2) !== "")	{
	 		$('#subcat2').empty();
	 		$('#subcat2').append($('<option>', { value: "",text: 'Select SubCategory'}));
	 		$.ajax({
	 			 url : "searchSubHeadSj.jsp",
	 			 type : "GET",
	 			 data : {cat : cat2},
	 			 success : function(response)	{
	 				var data = response.split(",");
	 				for (var i = 0 ; i < data.length ; i++)	{
	 					if (data[i].trim() !== "")	{
	 	 					$('#subcat2').append($('<option>', { value: data[i].trim(),text: data[i].toUpperCase()}));
	 					}
	 				}
	 				if ($.trim(sub) !== "") $('#subcat2 option[value='+sub+']').attr('selected','selected');
	 			 }
	 		});
	 	}
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
        <script type="text/javascript">
	  function isNumber(evt) {
		    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;
		}

    </script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="PRD" class="beans.products"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"><%if(request.getParameter("id")!=null && request.getParameter("hoid")!=null) {%>Edit product<%}else{%>Create a new product<%} %></div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
productsListing PRD_LST=new productsListing();
majorheadListing MajorHead_list=new majorheadListing();
minorheadListing MinorHead_list=new minorheadListing();
subheadListing subHead_list=new subheadListing();
if(request.getParameter("id")!=null && request.getParameter("hoid")!=null) {  

	List<products> PRD_LS=PRD_LST.getMproductsBasedOnHoid(request.getParameter("id"),request.getParameter("hoid"));
	PRD=(products)PRD_LS.get(0);
%>
<form name="tablets_Update" id="tablets_Update" method="post" action="products_update.jsp" onSubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account*</td>
<td >
<div class="warning" id="MajorIdErr" style="display: none;">Please Provide  "Major Head Number".</div>
Major Head No*</td>
<td >
<div class="warning" id="MinorIdErr" style="display: none;">Please Provide  "Minor Head Name".</div>
Minor Head Name</td>
<td >
<div class="warning" id="subIdErr" style="display: none;">Please Provide  "Sub Head Name".</div>
Sub Head Name</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id" >
<%-- <option value="<%=MH.gethead_account_id()%>" selected="selected"><%=HOA_L.getHeadofAccountName(PRD.gethead_account_id()) %></option> --%>
<option value="<%=PRD.gethead_account_id()%>" selected="selected"><%=HOA_L.getHeadofAccountName(PRD.gethead_account_id()) %></option>
<%-- <%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %> --%>
</select></td>
<td><input type="text" name="major_head_id" id="major_head_id" value="<%=MajorHead_list.getmajorheadName(PRD.getmajor_head_id()) %>" /></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value="<%=MinorHead_list.getminorheadName(PRD.getextra3()) %>"  /></td>
<td><input type="text" name="sub_head_id" id="sub_head_id" value="<%=subHead_list.getMsubheadname(PRD.getsub_head_id()) %>"  /></td>
</tr>
<tr>
<td>
<div class="warning" id="ProductIdErr" style="display: none;">Please Provide  "Product Number".</div>
Product No*</td>
<td >
<div class="warning" id="ProductNameErr" style="display: none;">Please Provide  "Product Name".</div>
Product Name*</td>
<td>Units</td>
<td></td>
</tr>
<tr>
<td><input type="text" name="Product_id" id="Product_id" value="<%=PRD.getproductId() %>" readonly/></td>
<td><input type="text" name="Product_name" id="Product_name" value="<%=PRD.getproductName() %>" /></td>
<td>
<select name="productUnits">
<option value="<%=PRD.getunits()%>"><%=PRD.getunits() %></option>
<option value="Number/s">Number</option>
<option value="litre/s">Litre/s</option>
<option value=Gram/s>Gram/s</option>
<option value=Kg/s>Kg/s</option>

</select>
</td>
<td><input type="checkbox" name="applicationform" id="applicationform" value="application" <%if(PRD.getextra2()!=null && PRD.getextra2().equals("application")){ %>checked="checked" <%} %>/>Applicationform</td>
<td></td>
</tr>
<tr>
<td><input type="text" name="inventoryId" id="inventoryId"  value="<%=PRD.getextra4() %>" placeHolder="Enter product inventory Id"/></td>
<td><input type="text" name="location" id="location"  value="<%=PRD.getextra5() %>" placeHolder="Enter product location"/></td>
<td colspan="1"><input type="text" name="description" id="description" placeholder="Enter description" value="<%=PRD.getdescription() %>"></input></td>
<td></td>
</tr>
<tr>
<td>Godown</td>
<td >Shop</td>
<td>Vat*</td>
<td>OpeingBalance</td>
<td>Category</td>
<td>SubCategory</td>
</tr>  
<tr>
<td><input type="text" name="bqg" id="bqg" value="<%=PRD.getbalanceQuantityGodwan() %>" onKeyPress="return numbersonly(this, event,true);" /></td>
<td><input type="text" name="bqs" id="bqs" value="<%=PRD.getbalanceQuantityStore() %>" onKeyPress="return numbersonly(this, event,true);" readonly></td>
<td><select name="vat" id="vat" onclick="calculate('1')">
<option value="<%=PRD.getvat() %>"><%=PRD.getvat() %></option>
<option value="0">0</option>
<option value="2">2</option>
<option value="5">5</option>
<option value="14.5">14.5</option>
</select></td>
<%
mainClasses.productsListing prodService = new mainClasses.productsListing();
List catlist =prodService.getProductCategories();
%>
<td><input type="text" name="openinbal2" id="openinbal2" placeholder="Enter Opening Balance" onkeypress="return isNumber(event)" <%if(!PRD.getextra7().equals("")){%> value="<%=PRD.getextra7()%>"  <%}%>></td>
<td>
<select name="cat2" id="cat2">
<option value="">Select Category</option>
<%
for (int i = 0 ; i < catlist.size() ; i++)	{
String cat = (String)catlist.get(i);%>
<option <%if(PRD.getextra8().trim().equals(cat)) {%> selected <%}%> value="<%=cat%>"><%=cat.toUpperCase()%></option>	
<%}%>
</select>
</td>
<td>
<input type="hidden" id="sub" value="<%=PRD.getextra9()%>">
<select name="subcat2" id="subcat2">
 <option value="">Select SubCategory</option>
</select>
</td>
</tr>
<tr>
<td>Purchase</td>
<td>VAT</td>
<td>Gross</td>
<td></td>
</tr>
<tr>
<%DecimalFormat df=new DecimalFormat("###.00");

double bygr=0.0;
double sellgr=0.0;


String formate = df.format(Double.parseDouble(PRD.getpurchaseAmmount())*(1+(Double.parseDouble(PRD.getvat()))/100));
bygr=Double.parseDouble(formate);
//System.out.println(1+(Double.parseDouble(PRD.getvat1())/100));
formate = df.format(Double.parseDouble(PRD.getsellingAmmount())*(1+(Double.parseDouble(PRD.getvat1())/100)));
sellgr=Double.parseDouble(formate);

%>
<td><input type="text" name="buyprice" id="buyprice" value="<%=PRD.getpurchaseAmmount()%>" onBlur="calculate('1')" placeholder="Buying Price"/></td>
<td><input type="text" name="vatbuy" id="vatbuy" value="<%=PRD.getvat() %>" readonly/></td>
<td><input type="text" name="buygross" id="buygross" value="<%=bygr%>" onBlur="calculate('1')" /></td>
<td></td>
</tr>

<tr>

<td><input type="text" name="sellprice" id="sellprice" value="<%=PRD.getsellingAmmount()%>" placeholder="Selling Price" onBlur="calculate('2')"  /></td>
<td><input type="text" name="vatsell" id="vatsell" value="<%=PRD.getvat1() %>" onBlur="calculate('2')" /></td>
<td><input type="text" name="sellgross" id="sellgross" value="<%=sellgr%>" readonly /></td>
<td></td></tr>

<tr> 
<td colspan="2"></td>
<td align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>
<td></td>
</tr>
</table>
</form>
<% } else{%>
<form name="tablets_Create" id="tablets_Create" method="post" action="products_Insert.jsp" onSubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account*</td>
<td>
<div class="warning" id="MajorIdErr" style="display: none;">Please Provide  "Major Head Number".</div>
Major Head Name*</td>
<td >
<div class="warning" id="MinorIdErr" style="display: none;">Please Provide  "Minor Head Name".</div>
Minor Head Name</td>
<td >
<div class="warning" id="subIdErr" style="display: none;">Please Provide  "Sub Head Name".</div>
Sub Head Name</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><input type="text" name="major_head_id" id="major_head_id" value="" autocomplete="off" value="" onBlur="idValidate();"/></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value="" autocomplete="off" value="" onBlur="idValidate();"/></td>
<td><input type="text" name="sub_head_id" id="sub_head_id" value="" autocomplete="off" value="" onBlur="idValidate();"/></td>
</tr>
<tr>
<td>
<div class="warning" id="ProductIdErr" style="display: none;">Please Provide  "Product Number".</div>
<div class="warning" id="dupErr" style="display: none;">Duplicate entry.Please provide another.</div>
Product No*</td>
<td >
<div class="warning" id="ProductNameErr" style="display: none;">Please Provide  "Product Name".</div>
Product Name*</td>
<td>Units</td>
<td colspan="1"></td>
</tr>
<tr>
<td><input type="text" name="Product_id" id="Product_id" value="" onBlur="idValidate();"/></td>
<td><input type="text" name="Product_name" id="Product_name" value="" /></td>
<td>
<select name="productUnits">
<option value="Number/s">Number</option>
<option value="litre/s">Litre/s</option>
<option value=Gram/s>Gram/s</option>
<option value=Kg/s>Kg/s</option>
</select>
</td>
<td><input type="checkbox" name="applicationform" id="applicationform" value="application" />Applicationform</td>
</tr>
<tr>
<td><input type="text" name="inventoryId" id="inventoryId"  value="" placeHolder="Enter product inventory Id"/></td>
<td><input type="text" name="location" id="location"  value="" placeHolder="Enter product location"/></td>
<td colspan="1"><input type="text" name="description" id="description" placeholder="Enter description"></input></td>
<td></td>
<!-- <td><textarea name="description" id="description" placeholder="Enter description"></textarea></td> -->
</tr>
<tr>
<td>Godown</td>
<td >Shop</td>
<td>Vat*</td>
<td>OpeingBalance</td>
<td>Category</td>
<td>SubCategory</td>
</tr>
<tr>
<td><input type="text" name="bqg" id="bqg" value="0" onKeyPress="return numbersonly(this, event,true);" /></td>
<td><input type="text" name="bqs" id="bqs" value="0" onKeyPress="return numbersonly(this, event,true);"></td>
<td><select name="vat" id="vat" onclick="calculate('1')">
<option value="">-VAT-</option>
<option value="0">0</option>
<option value="2">2</option>
<option value="5">5</option>
<option value="14.5">14.5</option>
</select></td>
<%
mainClasses.productsListing prodService = new mainClasses.productsListing();
List catlist =prodService.getProductCategories();
%>
<td><input type="text" value="" id="openingBal" onkeypress="return isNumber(event)"  placeholder="Enter Opening Bal" name="openingBal"></td>
<td>
<select name="cat1" id="cat1">
<option value="">Select Category</option>
<%
for (int i = 0 ; i < catlist.size() ; i++)	{
String cat = (String)catlist.get(i);%>
<option <%if(PRD.getextra8().trim().equals(cat)) {%> selected <%}%> value="<%=cat%>"><%=cat.toUpperCase()%></option>	
<%}%>
</select>
</td>
<td>
<select name="subcat1" id="subcat1">
 <option value="">Select SubCategory</option>
</select>
</td>
</tr>
<tr>
<td>Purchase</td>
<td>VAT</td>
<td>Gross</td>
<td></td>
</tr>
<tr>

<td><input type="text" name="buyprice" id="buyprice" value="0" onBlur="calculate('1')" placeholder="Buying Price"/></td>
<td><input type="text" name="vatbuy" id="vatbuy" value="0" readonly/></td>
<td><input type="text" name="buygross" id="buygross" value="0" onBlur="calculate('1')" /></td>
<td></td>
</tr>
<tr>
<td><input type="text" name="sellprice" id="sellprice" value="0" placeholder="Selling Price" onBlur="calculate('2')"  /></td>
<td><input type="text" name="vatsell" id="vatsell" value="0" onBlur="calculate('2')" /></td>
<td><input type="text" name="sellgross" id="sellgross" value="0" readonly /></td></tr>
<td></td>
<tr> 
<td colspan="2"></td>
<td align="right"><input type="submit" value="Create" class="click" style="border:none;"/></td>
<td></td>
</tr>
</table>
</form>
<%} %>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<!-- <span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span> -->
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%

List<products> PRD_L=PRD_LST.getproducts();
if(PRD_L.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>

<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<div id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="5"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="5%" align="center">S.No.</td>
<td class="bg" width="10%" align="center">MajorHead</td>
<td class="bg" width="10%" align="center">MinorHead</td>
<td class="bg" width="10%" align="center">SubHead</td>
<td class="bg" width="21%" align="center">Product Id</td>
<td class="bg" width="22%" align="center">Product Name</td>
<td class="bg" width="22%" align="center">Description </td>
<td class="bg" width="20%" align="center">Edit</td>
<!-- <td class="bg" width="10%">Delete</td> -->
</tr>
</table></td></tr>
<%for(int i=0; i < PRD_L.size(); i++ ){
	PRD=(products)PRD_L.get(i); %>
<tr>
<td width="5%" align="center"><%=i+1%></td>
<td width="5%" align="center"><%=PRD.getmajor_head_id()%></td>
<td width="5%" align="center"><%=PRD.getextra3()%></td>
<td width="5%" align="center"><%=PRD.getsub_head_id()%></td>
<td width="21%" align="center"><span><%=PRD.getproductId()%></span></td>
<td width="22%" align="center"><span><%=PRD.getproductName()%></span></td>
<td width="22%" align="center"><%=PRD.getdescription()%></td>
<%-- <td width="20%" align="center"><a href="adminPannel.jsp?page=products&id=<%=PRD.getproductId()%>">Edit</a></td> --%>
<td width="20%" align="center"><a href="adminPannel.jsp?page=products&id=<%=PRD.getproductId()%>&hoid=<%=PRD.gethead_account_id()%>">Edit</a></td>
<%-- <td><a href="majorhead_Delete.jsp?Id=<%=PRD.getproductId()%>">Delete</a></td> --%>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no major heads</h1></div>
<%}%>
</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
