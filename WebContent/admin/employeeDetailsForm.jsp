<%@page import="mainClasses.stafftimingsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="mainClasses.employeesListing,java.util.List,java.util.Iterator,mainClasses.stafftimingsListing,mainClasses.employeeDetailsListing "%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>  
<script src="../js/jquery-1.8.2.js"></script> 
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.widget.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
    <script>  
    function validate()
    {  
    	var var1=document.frm.employee_id.value;  
    	var var2=document.frm.unique_registration_no.value;  
    	var var3=document.frm.basic_pay_salary.value;  
    	var var4=document.frm.ewf_total_loan_amount.value;  
    	var var5=document.frm.ewf_due_amount.value;  
    	var var6=document.frm.personal_total_loan_amount.value; 
    	var var7=document.frm.personal_due_amount.value; 
    	var status=false;  
    	
      
    	if(var1.length<1)
    	{  
    		document.getElementById("id1").innerHTML=  
    		"<img src='unchecked.gif'/> Please enter employee id";  
    		status=false;  
    	}
    	if(var2.length<1)
    	{  
    		document.getElementById("id2").innerHTML=  
    		"<img src='unchecked.gif'/> please enter unique registration no";  
    		status=false;  
    	}
    	if(var3.length<1)
    	{  
    		document.getElementById("id3").innerHTML=  
    		"<img src='unchecked.gif'/> please Enter basic pay salary";  
    		status=false;  
    	}
    	if(var4.length<1)
    	{  
    		document.getElementById("id4").innerHTML=  
    		"<img src='unchecked.gif'/> please Enter ewf total loan amount";  
    		status=false;  
    	}
    	if(var5.length<1)
    	{  
    		document.getElementById("id5").innerHTML=  
    		"<img src='unchecked.gif'/> please Enter ewf due amount";  
    		status=false;  
    	}
    	if(var6.length<1)
    	{  
    		document.getElementById("id6").innerHTML=  
    		"<img src='unchecked.gif'/> please Enter personal total loan amount";  
    		status=false;  
    	}
    	if(var7.length<1)
    	{  
    		document.getElementById("id7").innerHTML=  
    		"<img src='unchecked.gif'/>please Enter personal due amount";  
    		status=false;  
    	}
    	
   	 	return status;  
    }  
    </script>
    
       <script>
	$(function() {
	var currdate=new Date();
	$( "#next_date_of_increment" ).datepicker({	
		 yearRange: "2016:2020",
		changeMonth: true,
       changeYear: true,
		showOtherMonths: true,
       selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
       buttonImageOnly: true
		});
	$(".ui-datepicker-trigger").mouseover(function() {
	    $(this).css('cursor', 'pointer');
	});	
	});
	</script> 

 <script>
function validateForm()
{
	var ewfTotalLoan=parseInt(document.frm.ewf_total_loan_amount.value);
	var ewfDueAmount=parseInt(document.frm.ewf_due_amount.value);
	var personalTotalLoan=parseInt(document.frm.personal_total_loan_amount.value);
	var personalDueAmount=parseInt(document.frm.personal_due_amount.value);

    if(document.frm.employee_id.value=="")
    {
      alert("please enter employee id");
      document.frm.employee_id.focus();
      return false;
    } 
    if(document.frm.unique_registration_no.value=="")
    {
      alert("please enter unique registration no ");
      document.frm.unique_registration_no.focus();
      return false;
    }  
    if(document.frm.basic_pay_salary.value=="")
    {
    	alert("please Enter basic pay salary");
      document.frm.basic_pay_salary.focus();
      return false;
    } 
    if(document.frm.ewf_total_loan_amount.value=="")
    {
    	alert("please Enter ewf total loan amount");
      	document.frm.ewf_total_loan_amount.focus();
      	return false;
    }  
    if(document.frm.ewf_due_amount.value=="")
    {
    	alert("please Enter ewf due amount");
      	document.frm.ewf_due_amount.focus();
      	return false;
    }
    
    if(document.frm.personal_total_loan_amount.value=="")
    {
    	alert("please Enter personal total loan amount ");
      	document.frm.personal_total_loan_amount.focus();
      	return false;
    }
    
    if(document.frm.personal_due_amount.value=="")
    {
    	alert("please Enter personal due amount ");
      	document.frm.personal_due_amount.focus();
      	return false;
    }
    if(ewfDueAmount>ewfTotalLoan)
    {
    	alert("Ewf Loan Due Amount should not be greater than Ewf Loan Total Amount: ");
    	document.frm.ewf_due_amount.focus();
      	return false;
    }
    if(personalDueAmount>personalTotalLoan)
    {
    	alert("Personal Loan Due Amount should not be greater than Personal Loan Total Amount: ");
      	document.frm.personal_due_amount.focus();
      	return false;
    }
    
}
</script>
<script>
function isNumberKey(evt)
{ // Numbers only
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

</head>
<body><center>
<h1>Enter The Loan Details Of Employee</h1><hr width="40%">
<form action="employeeDetailsSubmit.jsp" name="frm" onsubmit="return validateForm()" >

<table>
	<tr><td> Employee Id:</td><td>
	
<select name="employee_id">
<%
//get all employee id from employees table

    stafftimingsListing stafflisting=new stafftimingsListing();
	employeeDetailsListing edlisting =new employeeDetailsListing();
	
	List allstaff_id=stafflisting.getAllStaffId();//selectAllEmployeesId();
	List inserted_staf_id=edlisting.selectAllEmployeeIdOnly();
	
	allstaff_id.removeAll(inserted_staf_id);
	
	Iterator itr=allstaff_id.iterator();
	if(request.getParameter("employeeId")!=null)
	{
		%><option><%=request.getParameter("employeeId")%></option><% 
	}
	else
	{
		while(itr.hasNext())
		{
		String emp_id=(String)itr.next();
		%>	
										
										<option><%=emp_id %></option>
									
		<%} 
	}%>
	</select></td></tr>
	
	<%if(request.getParameter("empName")!=null){%><tr><td> Employee Name:</td><td><input type="text" name="empName" value=<%=request.getParameter("empName")%>></td><%} %>
	
	<tr><td> Unique Registration No:</td><td><input type="text" name="unique_registration_no" value=<%if(request.getParameter("uniqueRegNo")!=null){out.println(request.getParameter("uniqueRegNo"));}%>></td>
	<tr><td> Basic Pay Scale: </td><td><input type="text" name="basic_pay_salary" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("basicPay")!=null){out.println(request.getParameter("basicPay"));}%>></td>
	<tr><td> Next Date Of Increment: </td><td><input type="text" name="next_date_of_increment" id="next_date_of_increment" value=<%if(request.getParameter("next_date_of_increment")!=null){out.println(request.getParameter("next_date_of_increment"));}%> /></td></tr>
	<tr><td> Ewf Loan Total Amount:</td>
	<td>
		<select name="ewf_total_loan_amount">
			<option  value="10000" selected>10000</option>
			<option  value="20000" <%if(request.getParameter("totalEwfLoanAmount")!=null && request.getParameter("totalEwfLoanAmount").equals("20000")){ %>selected <%}%>>20000</option>
			<option  value="25000" <%if(request.getParameter("totalEwfLoanAmount")!=null && request.getParameter("totalEwfLoanAmount").equals("25000")){ %>selected <%}%>>25000</option>
		</select>
		<%-- <input type="text" name="ewf_total_loan_amount" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("totalEwfLoanAmount")!=null){out.println(request.getParameter("totalEwfLoanAmount"));}%>> --%>	
		</td></tr>
	
	<tr><td>Ewf  Loan  Due Amount</td><td><input type="text" name="ewf_due_amount" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("totalEwfDueAmount")!=null){out.println(request.getParameter("totalEwfDueAmount"));}%>></td>
	<tr><td> Ewf Loan Total Installments:</td>
	<td>
		<select name="ewf_loan_total_installments">
		<%if((request.getParameter("totalEwfInstallments")==null) || request.getParameter("totalEwfInstallments").equals("null")){%>
			<option  value="0" selected>0</option>
			<% int i = 1;
			while(i <= 20){%>
			<option  value="<%=i%>"><%=i%></option>
			<%i++;} }%><%  else{String totalEwfInstallments = request.getParameter("totalEwfInstallments");%>
			<option  value="<%=totalEwfInstallments%>" selected><%=totalEwfInstallments%></option>
			<% int i = 0;
			while(i <= 20){%>
			<option  value="<%=i%>"><%=i%></option>
			<%i++;} }%>
		</select>
		<%-- <input type="text" name="ewf_total_loan_amount" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("totalEwfLoanAmount")!=null){out.println(request.getParameter("totalEwfLoanAmount"));}%>> --%>	
		</td></tr>
	<tr><td>Ewf Loan Balance Installments:</td>
	<td>
		<select name="ewf_loan_balance_installments">
			<%if((request.getParameter("balEwfInstallments")==null) || request.getParameter("balEwfInstallments").equals("null")){%>
			<option  value="0" selected>0</option>
			<% int i = 1;
			while(i <= 20){%>
			<option  value="<%=i%>"><%=i%></option>
			<%i++;} }%><%  else{String balEwfInstallments = request.getParameter("balEwfInstallments");%>
			<option  value="<%=balEwfInstallments%>" selected><%=balEwfInstallments%></option>
			<% int i = 0;
			while(i <= 20){%>
			<option  value="<%=i%>"><%=i%></option>
			<%i++;} }%>
		</select>
		<%-- <input type="text" name="ewf_total_loan_amount" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("totalEwfLoanAmount")!=null){out.println(request.getParameter("totalEwfLoanAmount"));}%>> --%>	
		</td></tr>	
	<tr>
	<td> Personal  Loan  Total Amount:</td>
	<td>
		<select name="personal_total_loan_amount">
			<option  value="5000" selected>5000</option>
			<option  value="10000" <%if(request.getParameter("totalPersonalLoanAmount")!=null && request.getParameter("totalPersonalLoanAmount").equals("10000")){ %>selected <%}%>>10000</option>
		</select>
		<%-- <input type="text" name="personal_total_loan_amount" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("totalPersonalLoanAmount")!=null){out.println(request.getParameter("totalPersonalLoanAmount"));}%>> --%>	
	</td>
	
	<tr><td> Personal  Loan  Due Amount:</td><td><input type="text" name="personal_due_amount" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("totalPersonalDueAmount")!=null){out.println(request.getParameter("totalPersonalDueAmount"));}%>></td>totalPersonalDueAmount
	<tr><td> Personal Loan Total Installments:</td>
	<td>
		<select name="personal_loan_total_installments">
			<%if((request.getParameter("totalPersonalInstallments")==null) || request.getParameter("totalPersonalInstallments").equals("null")){%>
			<option  value="0" selected>0</option>
			<% int i = 1;
			while(i <= 20){%>
			<option  value="<%=i%>"><%=i%></option>
			<%i++;} }%><%  else{String totalPersonalInstallments = request.getParameter("totalPersonalInstallments");%>
			<option  value="<%=totalPersonalInstallments%>" selected><%=totalPersonalInstallments%></option>
			<% int i = 0;
			while(i <= 20){%>
			<option  value="<%=i%>"><%=i%></option>
			<%i++;} }%>
		</select>
		<%-- <input type="text" name="ewf_total_loan_amount" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("totalEwfLoanAmount")!=null){out.println(request.getParameter("totalEwfLoanAmount"));}%>> --%>	
		</td></tr>
	<tr><td> Personal Loan Balance Installments:</td>
	<td>
		<select name="personal_loan_balance_installments">
			<%if((request.getParameter("balPersonalInstallments")==null) || request.getParameter("balPersonalInstallments").equals("null")){%>
			<option  value="0" selected>0</option>
			<% int i = 1;
			while(i <= 20){%>
			<option  value="<%=i%>"><%=i%></option>
			<%i++;} }%><%  else{String balPersonalInstallments = request.getParameter("balPersonalInstallments");%>
			<option  value="<%=balPersonalInstallments%>" selected><%=balPersonalInstallments%></option>
			<% int i = 0;
			while(i <= 20){%>
			<option  value="<%=i%>"><%=i%></option>
			<%i++;} }%>
		</select>
		<%-- <input type="text" name="ewf_total_loan_amount" OnKeypress="javascript:return isNumberKey(event);" value=<%if(request.getParameter("totalEwfLoanAmount")!=null){out.println(request.getParameter("totalEwfLoanAmount"));}%>> --%>	
		</td></tr>	
	
	<tr><td> Select Bank:</td>
	<td><select name="extra4" value=<%=request.getParameter("bank")%>>
		<option>----select-----</option>
		<%String bank=request.getParameter("bank");%>
		
		<option <%if(bank!=null && bank.equals("State Bank of India")){out.println("selected");}%>>State Bank of India</option>
		<option <%if(bank!=null && bank.equals("State Bank of Tuticorin")){out.println("selected");}%>>State Bank of Tuticorin</option>
		<option value="Andhra Bank" <%if(bank!=null && bank.equals("Andhra Bank")){out.println("selected");}%>>Andhra Bank</option>
		<option <%if(bank!=null && bank.equals("Indian Bank")){out.println("selected");}%>>Indian Bank</option>
		<option <%if(bank!=null && bank.equals("State Bank of Travancore")){out.println("selected");}%>>State Bank of Travancore</option>

	</select></td>
	
	<tr><td> Account No:</td><td><input type="text" name="extra5" value=<%if(request.getParameter("acc")!=null){out.println(request.getParameter("acc"));}%>></td>
	
	<%
		String eligible_for_hra=request.getParameter("eligible_for_hra");
		String eligible_for_ewf_deduction=request.getParameter("eligible_for_ewf_deduction");
	%>
	<tr><td></td><td><input type="checkbox" name="extra1" value="eligible_for_hra" <%if(request.getParameter("eligible_for_hra")!=null){if(!(request.getParameter("eligible_for_hra").equals("null"))){out.println("checked");}} %> >Eligible For Hra</td></tr>
	<tr><td></td><td><input type="checkbox" name="extra2" value="eligible_for_TDS_deduction" <%if(request.getParameter("eligible_for_TDS_deduction")!=null){if(!(request.getParameter("eligible_for_TDS_deduction").equals("null"))){out.println("checked");}} %> >Eligible For TDS Deduction</td></tr>
	<!--  <tr><td></td><td><input type="checkbox" name="extra2" value="eligible_for_medical">Eligible for Medical</td></tr>-->
	<tr><td></td><td><input type="checkbox" name="extra3" value="eligible_for_ewf_deduction" <%if(request.getParameter("eligible_for_ewf_deduction")!=null){if(!(eligible_for_ewf_deduction.equals("null"))){out.println("checked");}} %>>Eligible For Ewf Deduction</td></tr>

	
	<tr><td><input type="submit" value="ok" onclick="return displayDetails()"> </td><td><input type="reset" value="clear"></td>
	
</table></form>

</center></body>
</html>