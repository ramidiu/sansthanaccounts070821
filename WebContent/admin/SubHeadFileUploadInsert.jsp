<%@page import="java.util.ArrayList"%>
<%@page import="java.io.FileInputStream"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page import="org.apache.commons.fileupload.FileItemFactory"%>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%> 
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="java.io.FileInputStream"%> 
<%@ page import="java.io.File"%>
<%@ page import="beans.subhead"%>
<%@ page import="beans.subheadService"%>
<%@ page import="java.util.Iterator,org.apache.poi.xssf.usermodel.XSSFSheet,org.apache.poi.xssf.usermodel.XSSFWorkbook,org.apache.poi.openxml4j.exceptions.InvalidFormatException,org.apache.poi.openxml4j.opc.OPCPackage,org.apache.poi.ss.usermodel.Row,java.text.DateFormat,java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SubHeadFileUploadInsert</title>
</head>
<body>
<% 
   boolean isMultipart=ServletFileUpload.isMultipartContent(request);
   long headOfAccount;
   long majorHeadId;
   long minorHeadId;
   long subHeadId;
   String subHeadName="";
   long price;
   long percentage;
   List<subhead> subHeadsList=new ArrayList<subhead>(); 
   
   if(!isMultipart){
	   response.sendRedirect("adminPannel.jsp?page=fileupload");
   }else{
	   FileItemFactory factory=new DiskFileItemFactory();
	   ServletFileUpload upload=new ServletFileUpload(factory);
	     List items = null;
	     String fileName="";
	     try {
	    	   items = upload.parseRequest(request);
	    	 } catch (FileUploadException e) {
	    		out.print(e);
	    	 }
	       Iterator itr = items.iterator();
	       while (itr.hasNext()) 
	    	   {
	    			   FileItem item = (FileItem) itr.next();
	    			   if (item.isFormField())
	    				   {
	    				     break;
	    				   }
	    			   else
	    			   {	   
	    		          try {
	    		        	  
	    		    	         fileName = item.getName();
	    		    	         System.out.println("flename:::"+fileName);
	    		    	         String strDirectoy1=config.getServletContext().getRealPath("/tempimage")+"/"+fileName;
	    		 				 System.out.println("strDirectoy1:::::"+strDirectoy1);
	    		 				  File savedFile = new File(strDirectoy1);
	    		 				
	    		 				
	    		 				 item.write(savedFile); 

	    		 				FileInputStream fstream = new FileInputStream(new File(strDirectoy1));
	    		    	         OPCPackage fs = OPCPackage.open(fstream);   
	    		    	         XSSFWorkbook wb = new XSSFWorkbook(fs);
	    		    	         XSSFSheet sheet = wb.getSheetAt(0);   
	    		    	         
	    		    	         Row row;  
	    		    	         String strLine;
	    		    	         String cell="";
	    		    	         for(int i=1; i<=sheet.getLastRowNum(); i++){
	    		    	        	 row = sheet.getRow(i);
	    		    	        	
	    		    	        	 headOfAccount=(long)row.getCell(0).getNumericCellValue();
	    		    	        	 majorHeadId=(long)row.getCell(1).getNumericCellValue();
	    		    	        	 minorHeadId=(long)row.getCell(2).getNumericCellValue();
	    		    	        	 subHeadId=(long)row.getCell(3).getNumericCellValue();
	    		    	        	 subHeadName=row.getCell(4).getStringCellValue();
	    		    	        	 price=(long)row.getCell(5).getNumericCellValue();
	    		    	        	 percentage=(long)row.getCell(6).getNumericCellValue();
	    		    	        	 subhead subHead=new subhead();
	    		    	        	 subHead.sethead_account_id(String.valueOf(headOfAccount));
	    		    	        	 subHead.setmajor_head_id(String.valueOf(majorHeadId));
	    		    	        	 subHead.setextra1(String.valueOf(minorHeadId));
	    		    	        	 subHead.setsub_head_id(String.valueOf(subHeadId));
	    		    	        	 subHead.setname(subHeadName);
	    		    	        	 subHead.setextra2(String.valueOf(price));
	    		    	        	 subHead.setextra4(String.valueOf(percentage));
	    		    	        	 subHeadsList.add(subHead);
	    		    	        	 
	    		    	         } 
	    		              }catch(Exception e){
	    		        	  e.printStackTrace();
	    		              }
	    		          
	    		          subheadService subHeadService=new subheadService();
	    		          subHeadService.addNewSubHeads(subHeadsList);
	    			   }
	    	   }
	       
	    		    

   }
   
%>
</body>
</html>