<%@page import="mainClasses.headsgroupListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>

<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	

</script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script>
function idValidate(){
	if($('#major_head_id').val().trim()==""){
		$('#majorheadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	var id=$('#major_head_id').val().trim();
$.ajax({
	type:'post',
	url: 'IDValidate.jsp', 
	data: {
		Id : id,
		type : 'majorhead'
	},
	success: function(response) { 
		var msg=response.trim();
		$('#majorheadErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#major_head_id').focus();
			return false;
		}
	}});
}

</script>
<script type="text/javascript">

function validate(){
	/* $('#headIdErr').hide(); */
	$('#groupErr').hide();
	/* if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	} */
	if($('#groupname').val().trim()==""){
		$('#groupErr').show();
		$('#groupname').focus();
		return false;
	}

}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="HG" class="beans.headsgroup"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"><%if(request.getParameter("id")!=null) {%>Edit head group<%}else{%>Create a new head group<%} %></div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
%>
<form name="tablets_Update" method="post" action="headsgroup_Insert.jsp" onSubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="groupErr" style="display: none;">Please Provide  "group name".</div>Head Group Name*</td>
<td></td>
<td></td>
</tr>
<tr>
<%-- <td><select name="head_account_id" id="head_account_id">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td> --%>
<td><input type="text" name="groupname" id="groupname" value=""></td>
<td><input type="text" name="description" id="description" placeholder="Enter description">
<!-- <textarea name="description" id="description" placeholder="Enter description"></textarea> --></td>
<td align="right"><input type="submit" value="Create" class="click" style="border:none;"/></td>
</tr>
</table>
</form>

</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/><b>Heads Groups List</b></div>

<div class="clear"></div>
<div class="list-details">
<%
headsgroupListing GROPL=new headsgroupListing();
List GROPList=GROPL.getheadsgroup();
if(GROPList.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="3"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="13%" align="center">S.No.</td>
<td class="bg" width="23%" align="center">Group Head</td>
<td class="bg" width="23%" align="center">Description </td>

<!-- <td class="bg" width="20%">Edit</td>
<td class="bg" width="10%">Delete</td> -->
</tr>
</table></td></tr>
<%for(int i=0; i < GROPList.size(); i++ ){
	HG=(beans.headsgroup)GROPList.get(i); %>
<tr>
<td width="13%" align="center"><%=HG.getgroup_id()%></td>
<td width="23%" align="center"><span><%=HG.getgroup_name()%></span></td>
<td width="23%" align="center"><%=HG.getnarration()%></td>

<%-- <td><a href="adminPannel.jsp?page=majorHead&id=<%=MH.getmajor_head_id()%>">Edit</a></td>
<td><a href="majorhead_Delete.jsp?Id=<%=MH.getmajor_head_id()%>">Delete</a></td> --%>
</tr>

<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no head groups</h1></div>
<%}%>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
