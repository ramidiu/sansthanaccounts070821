<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="helperClasses.SaiMonths"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="mainClasses.employeeDetailsListing,beans.employeeDetails,java.util.List,java.util.Iterator,beans.employeeSalary,beans.employeeSalaryService"%>
   <%@ page import="mainClasses.employeeSalarySlipListing,mainClasses.employeeAttendce,mainClasses.stafftimingsListing,beans.stafftimings"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>


<style>th{border:1px solid black;color:red}


 
.bord td{border: 1px solid #000; }
.bord input{border: none;}
</style>
<script type="text/javascript">
$(function() {
	$("#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		
		});
	});
function salarypay(id){
	var deductions=0;
	var allwoance=0;
	var totalamount=0;
	var payamount=0;
	deductions=Number($("#deductions"+id).val());
	 allwoance=Number($("#allowances"+id).val());
	 totalamount=Number($("#totalsalary_pay"+id).val());
	 payamount=Number(totalamount)+Number(allwoance)-Number(deductions);
	 $("#salary_pay"+id).val(payamount);
	
}
</script>
<script>
function myFunction() 
{
	 	if(document.frm.month.value=="")
	    {
	       alert("please enter valid  month and year ");
	       document.frm.month.focus();
	       return false;
	    }
	 	
	 	else
		 {
    		document.getElementById("id1").submit();
		 }
}
</script>


<script type="text/javascript">
function CheckAll(chk)
{
	for (i = 0; i < chk.length; i++)
		chk[i].checked = true ;
}
function UnCheckAll(chk)
{
for (i = 0; i < chk.length; i++)
chk[i].checked = false ;
}
</script>
<script>
function form2_submit() 
{
	
	
	
	var n=document.querySelectorAll('input[type="checkbox"]:checked').length;
		if( n > 0 )
		{ 
		
			return true;
		}
		else
		{ 
			alert( 'please check atleast one list' );
			return false;
		}	
}
</script>

<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>

</head>
<body><center>

<form action="adminPannel.jsp"  name="frm" method="get">

	<select name="department">
	<option>ALL</option>
	<%
		DateFormat monthFormat=new SimpleDateFormat("MMMM");
		DateFormat yearFormat=new SimpleDateFormat("yyyy");
		stafftimingsListing stlisting=new stafftimingsListing(); 
		List stlist=stlisting.getAllDistinctStaffType();
		Iterator stitr=stlist.iterator();
		while(stitr.hasNext())
		{
			stafftimings stimings=(stafftimings)stitr.next();
	%>
		<option><%=stimings.getstaff_type()%></option>
	<% }%>
	</select>
		<!-- Month:<input type="month" name="month"> -->
		<%
			Date now=new Date();
			Calendar today=Calendar.getInstance();
			today.add(Calendar.MONTH, -1);
			String thisMonth=monthFormat.format(today.getTime());
			/* String thisYear=yearFormat.format(now); */
			String thisYear=yearFormat.format(today.getTime());
		%>
  		Month: 
  		 <select name="month">
  			<%List<String> listOfAllMonths= SaiMonths.getAllMonth(); 
  				//for(int i=0;i<listOfAllMonths.size();i++){
  			%>
  			 	<%-- <option value="<%=listOfAllMonths.get(i)%>" 
  				<%if(request.getParameter("month")!=null && request.getParameter("month").equalsIgnoreCase(listOfAllMonths.get(i)) ){ %>selected<% }
  					else{
  						if(thisMonth.equalsIgnoreCase(listOfAllMonths.get(i))){
  							%>selected<%
  						}else{
  							%>hidden<%
  						}
  					} %> >
  				<%=listOfAllMonths.get(i)%>
  				</option>  --%>
  				<%//} %>
  			<option value="<%=thisMonth%>"><%=thisMonth%></option>
  		</select>
  		Year: 
  		<select name="year">
  			<option value="2015">2015</option>
  			<option value="<%=thisYear%>"><%=thisYear%></option>
  		</select> 
  	 <!--  <input type="text" id="fromDate" name="month"/>  -->
	
	<input type="hidden" name="page" value="acquittanceReport"  >
	<input type="submit" id="id1" value="ok" onclick=" return myFunction()">
</form>	
<%
if((request.getParameter("month")!=null) && !request.getParameter("department").equals("ALL"))
{
	System.out.println("11111");
	String month=request.getParameter("month");
	String year=request.getParameter("year");
	String monthAndYear=month+"@"+year;
	//System.out.println("month======>"+request.getParameter("month"));
	String employee_id=null;
	
	String department=request.getParameter("department");
	int checkBoxCLick=0;
	float opening_cl=0.0f,earned_cl=0.0f,closing_cl=0.0f,ot=0.0f;
	String LP=null,LA=null,OD=null;
	employeeDetailsListing listing=new employeeDetailsListing();
	employeeSalarySlipListing salarylist = new employeeSalarySlipListing();
	List salarylists = salarylist.selectAllSalarySlipBasedOnMonth(monthAndYear);
	/* List list=listing.selectAllEmployeeDetailsBasedUponDepartment(department); */
	if(salarylists!=null){
	Iterator itr=salarylists.iterator();%>
	
	<div class="vendor-page">
<div class="vendor-list">
	<div class="icons">
<!-- <a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> -->
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>
<div class="clear"></div>
<div class="total-report">
<!--  <form action="employeeSalarySlipSubmit.jsp" name="frm2" onSubmit="return form2_submit()" method="post">  -->
	<%-- <table align="left">
	<tr><td></td>	
	<td><h6><%=department%></h6></td></tr>
	</table>
	<br> --%>
	<div id="tblExport">
	<table class="bord" >
	<tr>
				<td colspan=24 style="text-align:left;"><strong style="font-weight: bold; "><%=department%></strong></td>
			</tr> 
	<tr><th>s no:</th><th>Emp Id:</th><th>Emp Name:</th><th>Designation:</th><!--  <th>Month:</th>--><th>Basic Pay</th><th>HRA</th><th>OT Hrs</th><th>OT Price</th><th>OT Amount</th><th>Other Allowances</th><th>Total Gross Amount</th><th>EWF</th><th>EWF (P)</th><th>EWF (I)</th><th>Personal Loan Amount(Per month)</th><th>Personal Interest Amount(Per month)</th><!-- <th>W/A</th> --> <th>Salary Cut Days</th><th>Salary Cut Amount(P.D)</th><th>Salary Cut</th><th>Other Deductions/TDS</th><th>Total Deductions</th><th>Net Payment</th><th>Bank</th><th>Bank Account No.</th></tr>
	<%
	while(itr.hasNext())
	{		
		employeeSalary es=(employeeSalary)itr.next();	
	    employee_id=es.getEmployee_id();
	    String staftype = stlisting.getStafCategoryName(employee_id);
	    
	    if(staftype.equals(department))
	    {
	    	//System.out.println(es.getOther_allowances());
	    	employeeAttendce attendence=new employeeAttendce();
		    String dateToGetClOt=""+SaiMonths.getMonthValue(month)+"/01/"+year;
		    String data[]=attendence.calculateClOTs(employee_id, dateToGetClOt);
			opening_cl=Float.parseFloat(data[0]);
			earned_cl=Float.parseFloat(data[1]);
			closing_cl=Float.parseFloat(data[2]);
		 	ot=Float.parseFloat(data[3]);
			LP=data[4];
			LA=data[5];
			OD=data[6];
	    //pass the employeeId and month to employeeAttendce.java  to get all CL,OT...etc
			%>
			
			<tr>
				<td><%=++checkBoxCLick%></td>
				<td><%=employee_id%></td>
				<!--  getting employee name from stafftiming table of saisansthan schema ..,just to display no need to store in DB -->
				<%
					stafftimings stft=stlisting.getStaffDetails(es.getEmployee_id());
				%>
				<td><%=stft.getstaff_name() %></td>
				<td><%=stft.getdesignation() %></td>

				<td><%=es.getBasic_pay()%></td>
				<td><%=es.getHra()%></td>
				<td><%=ot%></td>
				<td><%=Math.round((Float.parseFloat(es.getBasic_pay())*12/(365*9)))%></td>
				<td><%=es.getOtpay()%></td>
				<td><%=es.getOther_allowances() %></td>
				<%double total_gross_amount = Double.parseDouble(es.getBasic_pay())+Double.parseDouble(es.getHra())+Double.parseDouble(es.getOtpay())+Double.parseDouble(es.getOther_allowances())+Double.parseDouble(es.getExtra9()); %>
				<td><%=total_gross_amount%></td>
				<td><%=es.getExtra8()%></td>
				<td><%=es.getEwf_loan_amount()%></td>
				<td><%=es.getEwf_interest_amount()%></td>
				<td><%=es.getPersonal_loan_amount()%></td>
				<td><%=es.getPersonal_interest_amount()%></td>	
				<% float salary_cut_days = 0;%>	
				<td><%if(closing_cl<0){salary_cut_days=-(closing_cl);%><%=-(closing_cl)%><%}else{%>0<%}%></td>	
				<td><%=Math.round((Float.parseFloat(es.getBasic_pay())*12/365))%></td>
				<%-- <td><input type="text" name="wa_amount<%=esalary.getEmployee_id()%>" value=<%=esalary.getExtra9()%> ></td> --%>
				<% 
					System.out.println("Salary cut days"+salary_cut_days); 
					System.out.println("Basic Pay"+es.getBasic_pay()); 
					System.out.println("Salary Cut"+Math.round(salary_cut_days*(Float.parseFloat(es.getBasic_pay())*12/365))); 
				%>
				<%float salary_cut = Math.round(salary_cut_days*(Float.parseFloat(es.getBasic_pay())*12/365));%>
				<td><%=salary_cut%></td>
				<td><%=es.getOther_deductions() %></td>
				<%double total_deductions = Double.parseDouble(es.getExtra8())+Double.parseDouble(es.getEwf_loan_amount())+Double.parseDouble(es.getEwf_interest_amount())+Double.parseDouble(es.getPersonal_loan_amount())+salary_cut+Double.parseDouble(es.getOther_deductions()); %>
				<td><%=total_deductions%></td>
				<td><%=total_gross_amount-total_deductions%></td>
				<%employeeDetails ed = listing.selectSingleEmployeeDetailsBasedUponEmployeeId(employee_id); %>
				<td><%=ed.getExtra4()%></td>
				<td><%=ed.getExtra5()%></td>		
				<!-- <td><input type="submit" value="ok"></td> -->
			</tr>
	    	
		<%}//if(itr2.hasNext())%>
<%} %>
 	<%}//while(itr.hasNext()))%> 
	
	</table><!-- </form>  -->
<%} 
%>
<%
if((request.getParameter("month")!=null) && request.getParameter("department").equals("ALL"))
{
	System.out.println("2222");
	String depttype = "";
	
	String month=request.getParameter("month");
	String year=request.getParameter("year");
	String monthAndYear=month+"@"+year;
	//System.out.println("month======>"+request.getParameter("month"));
	String employee_id=null;
	
	String department=request.getParameter("department");
	int checkBoxCLick=0;
	float opening_cl=0.0f,earned_cl=0.0f,closing_cl=0.0f,ot=0.0f;
	String LP=null,LA=null,OD=null;
	stafftimingsListing stlisting2=new stafftimingsListing(); 
	List stlist2=stlisting.getAllDistinctStaffType();
	Iterator stitr2=stlist.iterator();%>
	<div class="vendor-page">
<div class="vendor-list">
	<div class="icons">
<!-- <a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> -->
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>
<div class="clear"></div>
<div class="total-report">
	<% 
	while(stitr2.hasNext())
	{
		stafftimings stimings2=(stafftimings)stitr2.next();
		depttype = stimings2.getstaff_type();
		employeeDetailsListing listing=new employeeDetailsListing();
		employeeSalarySlipListing salarylist = new employeeSalarySlipListing();
		List salarylists = salarylist.selectAllSalarySlipBasedOnMonth(monthAndYear);
		/* List list=listing.selectAllEmployeeDetailsBasedUponDepartment(department); */
		if(salarylists!=null){
		Iterator itr=salarylists.iterator();%>

	
	
<!--  <form action="employeeSalarySlipSubmit.jsp" name="frm2" onSubmit="return form2_submit()" method="post">  -->
	<%-- <table align="left">
	<tr><td></td>
	<td><input type="button"  value=<%=month%>  ></td>
	
	 <td><h6><strong><%=depttype%></strong></h6></td></tr> 
	</table> --%>
	<br>
	<div id="tblExport">
	<table class="bord"  cellpadding="0" cellspacing="0"  >
	
	<%
	int count = 0;
	Iterator itrs=salarylists.iterator();
		while(itrs.hasNext())
		{
			employeeSalary es=(employeeSalary)itrs.next();	
		    employee_id=es.getEmployee_id();
		    String staftype = stlisting2.getStafCategoryName(employee_id);
		    System.out.println("Staff type:"+staftype);
		    System.out.println("Dept type:"+depttype);
		    if(staftype.equals(depttype))
		    {
		    	count += 1;
		    }
		    
		}
	%>
	<% if(count > 0)
		    {System.out.println("Count"+count);%>
		    <tr>
				<td colspan=24 style="text-align:left;"><strong style="font-weight: bold; "><%=depttype%></strong></td>
			</tr> 
		    <tr><th>s no:</th><th>Emp Id:</th><th>Emp Name:</th><th>Designation:</th><!--  <th>Month:</th>--><th>Basic Pay</th><th>HRA</th><th>OT Hrs</th><th>OT Price</th><th>OT Amount</th><th>Other Allowances</th><th>Total Gross Amount</th><th>EWF</th><th>EWF (P)</th><th>EWF (I)</th><th>Personal Loan Amount(Per month)</th><th>Personal Interest Amount(Per month)</th><!-- <th>W/A</th> --> <th>Salary Cut Days</th><th>Salary Cut Amount(P.D)</th><th>Salary Cut</th><th>Other Deductions/TDS</th><th>Total Deductions</th><th>Net Payment</th><th>Bank</th><th>Bank Account No.</th></tr>
		    	
		   <% }%>
	
	<%
	while(itr.hasNext())
	{	
		employeeSalary es=(employeeSalary)itr.next();	
	    employee_id=es.getEmployee_id();
	    String staftype = stlisting2.getStafCategoryName(employee_id);
	    
	    if(staftype.equals(depttype))
	    {
	    	//System.out.println(es.getOther_allowances());
	    	employeeAttendce attendence=new employeeAttendce();
		    String dateToGetClOt=""+SaiMonths.getMonthValue(month)+"/01/"+year;
		    String data[]=attendence.calculateClOTs(employee_id, dateToGetClOt);
			opening_cl=Float.parseFloat(data[0]);
			earned_cl=Float.parseFloat(data[1]);
			closing_cl=Float.parseFloat(data[2]);
		 	ot=Float.parseFloat(data[3]);
			LP=data[4];
			LA=data[5];
			OD=data[6];
	    //pass the employeeId and month to employeeAttendce.java  to get all CL,OT...etc
			%>
			
			<tr>
				<td ><%=++checkBoxCLick%></td>
				<td><%=employee_id%></td>
				<!--  getting employee name from stafftiming table of saisansthan schema ..,just to display no need to store in DB -->
				<%
					stafftimings stft=stlisting2.getStaffDetails(es.getEmployee_id());

				%>
				<td><%=stft.getstaff_name() %></td>
				<td><%=stft.getdesignation() %></td>

				<td><%=es.getBasic_pay()%></td>
				<td><%=es.getHra()%></td>
				<td><%=ot%></td>
				<td><%=Math.round((Float.parseFloat(es.getBasic_pay())*12/(365*9)))%></td>
				<td><%=es.getOtpay()%></td>
				<td><%=es.getOther_allowances() %></td>
				<%double total_gross_amount = Double.parseDouble(es.getBasic_pay())+Double.parseDouble(es.getHra())+Double.parseDouble(es.getOtpay())+Double.parseDouble(es.getOther_allowances())+Double.parseDouble(es.getExtra9()); %>
				<td><%=total_gross_amount%></td>
				<td><%=es.getExtra8()%></td>
				<td><%=es.getEwf_loan_amount()%></td>
				<td><%=es.getEwf_interest_amount()%></td>
				<td><%=es.getPersonal_loan_amount()%></td>
				<td><%=es.getPersonal_interest_amount()%></td>	
				<% float salary_cut_days = 0;%>	
				<td><%if(closing_cl<0){salary_cut_days=-(closing_cl);%><%=-(closing_cl)%><%}else{%>0<%}%></td>	
				<td><%=Math.round((Float.parseFloat(es.getBasic_pay())*12/365))%></td>
				<%-- <td><input type="text" name="wa_amount<%=esalary.getEmployee_id()%>" value=<%=esalary.getExtra9()%> ></td> --%>
				<%-- <% 
				
					System.out.println("Salary cut days"+salary_cut_days); 
					System.out.println("Basic Pay"+es.getBasic_pay()); 
					System.out.println("Salary Cut"+Math.round(salary_cut_days*(Float.parseFloat(es.getBasic_pay())*12/365))); 
					System.out.println("-------------------------------------------");
				%> --%>
				<%float salary_cut = Math.round(salary_cut_days*(Float.parseFloat(es.getBasic_pay())*12/365));%>
				<td><%=salary_cut%></td>
				<td><%=es.getOther_deductions() %></td>		
				<%-- <%
				System.out.println("emp id:"+employee_id);
					System.out.println("aa"+es.getExtra8());
				System.out.println("bb"+es.getEwf_loan_amount());
				System.out.println("cc"+es.getEwf_interest_amount());
				System.out.println("dd"+es.getPersonal_loan_amount());
				System.out.println("ee"+es.getOther_deductions());
				%>	 --%>	
				<%double total_deductions = Double.parseDouble(es.getExtra8())+Double.parseDouble(es.getEwf_loan_amount())+Double.parseDouble(es.getEwf_interest_amount())+Double.parseDouble(es.getPersonal_loan_amount())+salary_cut+Double.parseDouble(es.getOther_deductions()); %>			
				<td><%=total_deductions%></td>
				<td><%=total_gross_amount-total_deductions%></td>
				<%employeeDetails ed = listing.selectSingleEmployeeDetailsBasedUponEmployeeId(employee_id); %>
				<td><%=ed.getExtra4()%></td>
				<td><%=ed.getExtra5()%></td>		
				<!-- <td><input type="submit" value="ok"></td> -->
			</tr>
	    	
		<%}//if(itr2.hasNext())%>
<%} %>
 	<%}//while(itr.hasNext()))%> 
	
	</table><!-- </form>  -->
<%} }
%>

</div>
</div>
</div>
</div>



</center></body>
</html>