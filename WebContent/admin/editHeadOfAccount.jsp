<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.util.List"    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Head Of Account</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

function validate(){
	$('#nameError').hide();
	$('#phoneError').hide();
	if($('#name').val().trim()==""){
		$('#nameError').show();
		$('#name').focus();
		return false;
	}
	
	if($('#phone').val().trim()==""){
		$('#phoneError').show();
		$('#phone').focus();
		return false;
	}
		
}
</script>
</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<body>
<%String headOfAccountId=request.getParameter("id");
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	List HOA_List=HOA_CL.getMheadofaccounts(headOfAccountId);
	if(HOA_List.size()>0){
		HOA=(beans.headofaccounts)HOA_List.get(0);%>
<div class="vender-details">
<form method="POST" action="headofaccounts_Update.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Head Of Account Information</span></td>
</tr>
<tr>
<td width="50%">
<table width="100%" cellpadding="0" cellspacing="0">

<tr>
<td colspan="3">
<div class="warning" id="nameError" style="display: none;">"Name" Required.</div>Name*</td>
</tr>
<tr>
<td colspan="3">
<input type="hidden" name="head_account_id" id="head_account_id" value="<%=HOA.gethead_account_id()%>">
<input type="text" style="width:99%;" name="name" id="name" value="<%=HOA.getname()%>"></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="phoneError" style="display: none;">"Phone Number" Required.</div>Phone Number*</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="phone" id="phone" value="<%=HOA.getphone()%>" onKeyPress="return numbersonly(this, event,true);" style="width:98%;"/></td>
</tr>
<tr>
<td colspan="3">Address</td>
</tr>
<tr>
<td colspan="3"><textarea placeholder="street" name="address" id="address"><%=HOA.getaddress()%></textarea></td>
</tr>
<tr>
<td colspan="3">Description</td>
</tr>
<tr>
<td colspan="3"><textarea name="description" id="description"><%=HOA.getdescription()%></textarea></td>
</tr>
</table>
</td>
<td width="2%"></td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>
<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>
<%}else{%>
<script type="text/javascript">
window.location="adminPannel.jsp?page=mastersTree";
</script>
<%}%>
</body>
</html>