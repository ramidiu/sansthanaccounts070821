<%@page import="beans.bankdetails"%>
<%@page import="com.accounts.saisansthan.MapOfHeads"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="beans.banktransactions"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.commons.collections.map.MultiValueMap"%>
<%@page import="com.accounts.saisansthan.GenericClass"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.payments"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script>

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	$('#fromDate').val(startDate.split(",")[2]+"-"+startDate.split(",")[1]+"-"+startDate.split(",")[0]);
	$('#toDate').val(endDate.split(",")[2]+"-"+endDate.split(",")[1]+"-"+endDate.split(",")[0]);
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

function changeDateFormat()	{
	var fromdate = document.getElementById('fromDate').value; 
	var todate = document.getElementById('toDate').value;
	document.getElementById('fromDate').value = fromdate.split("-")[2]+"-"+fromdate.split("-")[1]+"-"+fromdate.split("-")[0];
	document.getElementById('toDate').value = todate.split("-")[2]+"-"+todate.split("-")[1]+"-"+todate.split("-")[0];
	return true;
}

$(document).ready(function(){
	datepickerchange();
}); 
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                     $( "a" ).css("text-decoration","none");
                    $( "#tblExport" ).print();

                    // Cancel click event.
                    return( false );
                });
			});
         $(document).ready(function () {
            $("#btnExport").click(function () {
                $( "a" ).css("text-decoration","none");
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="CUST" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="PRDEXP" class="beans.productexpenses"></jsp:useBean>
</head>
<body>
	<%
	DecimalFormat df=new DecimalFormat("#,###.00");
	productexpensesListing PRDEXP_L=new productexpensesListing();
	customerpurchasesListing CUSTPUR_L=new customerpurchasesListing();
	List CUSTLIST=null;
	List PRDEXPLIST=null;
	double creditamount,debitamount;
	int j=0;
		creditamount=debitamount=0.0;
	DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat  dateFormat2= new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
    String Vocherno[]=null;
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat DBDate= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(new SimpleDateFormat("yyyy-MM-dd").format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	int day=1;
	String toDate=currentDate+" 23:59:59";
	mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
	majorheadListing majorheadListing = new majorheadListing();
	minorheadListing minorheadListing = new minorheadListing();
	subheadListing SUBHL=new subheadListing();
	List HA_Lists=HOA_L.getheadofaccounts();
	String hoid="";
	if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
		hoid=request.getParameter("hoid");
	}
	 if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
			if(request.getParameter("fromDate")!=null){
				fromDate=request.getParameter("fromDate")+" 00:00:01";
			}
			if(request.getParameter("toDate")!=null){
				toDate=request.getParameter("toDate")+" 23:59:59";
			}
		}
	DecimalFormat DF=new DecimalFormat("0.00");
	String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
		 fromdate=request.getParameter("fromDate");
	}
	if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
		todate=request.getParameter("toDate");
	}

	GenericClass clazz = null;
	
	CUSTLIST=CUSTPUR_L.getcustomerpurchasesListBasedOnHOAandDatesAndCahsType(hoid,fromDate,toDate,"Jounral");
	PRDEXPLIST=PRDEXP_L.getPaymentsGroupByExpenseId(hoid,fromDate,toDate,"journalvoucher");
	
	MultiValueMap map1 = new MultiValueMap();
	MultiValueMap map2 = new MultiValueMap();
	Map<String,banktransactions> bktmap1 = new TreeMap<String,banktransactions>();
	Map<String,banktransactions> bktmap2 = new TreeMap<String,banktransactions>();
	StringBuilder[] jvnoList1 = new StringBuilder[CUSTLIST.size()];
 	StringBuilder[] jvnoList2 = new StringBuilder[PRDEXPLIST.size()];
	MapOfHeads map = new MapOfHeads();
 	List<banktransactions> cpBktsLst = null;
 	List<banktransactions> peBktsLst = null;
 	Map<String,bankdetails> banksMap = map.getBanksMap();
 	
 	for (int i = 0 ; i < CUSTLIST.size() ; i++)	{
		customerpurchases cp = (customerpurchases)CUSTLIST.get(i);
		jvnoList1[i] = new StringBuilder(cp.getvocharNumber());
		map2.put(cp.getvocharNumber(),cp);
	}

 	for (int i = 0 ; i < PRDEXPLIST.size() ; i++)	{
 		productexpenses pe = (productexpenses)PRDEXPLIST.get(i);
 		jvnoList2[i] = new StringBuilder(pe.getextra5());
 		map1.put(pe.getextra5(), pe);
	}
	
 	clazz = new GenericClass();
 	
 	cpBktsLst = clazz.getBankTransactionsBsdOnJvnumbers(jvnoList1);
 	peBktsLst = clazz.getBankTransactionsBsdOnJvnumbers(jvnoList2);
 	
 	if (cpBktsLst != null && cpBktsLst.size() > 0)	{
 	 	for (int i = 0 ; i < cpBktsLst.size() ; i++)	{
 	 		bktmap1.put(cpBktsLst.get(i).getextra3(), cpBktsLst.get(i));
 	 	}
 	}
	
 	if (peBktsLst != null && peBktsLst.size() > 0)	{
 	 	for (int i = 0 ; i < peBktsLst.size() ; i++)	{
 	 		bktmap2.put(peBktsLst.get(i).getextra3(), peBktsLst.get(i));
 	 	}
 	}
 	
%>
<div class="vendor-page">
<div class="vendor-list">
<form action="adminPannel.jsp" method="post" onsubmit="return changeDateFormat();">
				<div class="search-list">
					<ul>
			<li>
<select name="hoid" id="hoid" onchange="formSubmit();">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></li>
						<li>
						<input type="hidden" name="page" value="Journal_Entry_list"></input>
						<%-- <%SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
		Calendar calld = Calendar.getInstance();
		calld.add(Calendar.DATE,0);
		String todayDate=SDF.format(calld.getTime());
		String date[] = todayDate.split("-");
		 String currentyear = date[0];// 2017
		 String currentyear1 = currentyear.replace(currentyear.substring(2),""); // 20 
		 String currentyear2 = currentyear.replace(currentyear.substring(0,2),"");// 17
		 String year2 ="";
		 String year22 = "";	 
		 int currentyearinint = Integer.parseInt(currentyear2);	 
		 if(todayDate.compareTo(currentyear+"-04-01") >=0){
			 year2 =String.valueOf(currentyearinint+1);// 18
			 %>	  --%>
			 <td style="width:14%;">
					<%-- <select name="finYear" id="finYear"  Onchange="datepickerchange();" style="width:165px;">
					<option value="<%=currentyear1 %><%= currentyear2%>-<%= year2%>" selected="selected"><%=currentyear1 %><%=currentyear2 %>-<%=currentyear1 %><%=year2 %></option>
						<%int i=0;
							for(i=currentyearinint;i>15;i--){
								%>
								<option value="<%=currentyear1 %><%=i-1%>-<%= i%>"><%=currentyear1 %><%=i-1%>-<%=currentyear1 %><%= i%></option>
								<%}
						%>
					</select> --%>
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
						<%@ include file="yearDropDown1.jsp" %>
					</select>
				  </td>
			<%--  <%
		 }
		 else{
			 year2 =String.valueOf(currentyearinint-1);
			 %>
			 		 <td style="width:14%;">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();" style="width:165px;">
					<option value="<%=currentyear1 %><%= year2%>-<%= currentyear2%>" selected="selected"><%=currentyear1 %><%=year2 %>-<%=currentyear1 %><%=currentyear2 %></option>
						<% int i=0;
							for(i=currentyearinint-1;i>15;i--){
								%>
								<option value="<%=currentyear1 %><%=i-1%>-<%= i%>"><%=currentyear1 %><%=i-1%>-<%=currentyear1 %><%= i%></option>
								<%}
						%>
					</select>
				  </td>
			 <%
		 }
		%> --%>
						<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
<div class="icons">
<span id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span id="btnExport"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="list-details" id="tblExport">

<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="8" height="10"></td> </tr>
<tr><td colspan="8" align="center" style="font-weight: bold;">SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 20px;">Journal Voucher List  </td></tr>
</table>
<%


%>
	<table width="100%" cellpadding="0" cellspacing="0" border = "1">
    <tr>
    	<td  class="bg" style="font-size: 11px;font-weight: bold">S.No</td>
    	<td  class="bg" style="font-size: 11px;font-weight: bold">JV ID</td>
		<td class="bg" style="font-size: 11px;font-weight: bold">Date</td>
		<td class="bg" style="font-size: 11px;font-weight: bold">Naration</td>
		<td class="bg" style="font-size: 11px;font-weight: bold"> Department</td>
		<td class="bg" style="font-size: 11px;font-weight: bold"> Major Head</td>
		<td class="bg" style="font-size: 11px;font-weight: bold"> Minor Head</td>
		<td class="bg" style="font-size: 11px;font-weight: bold">Sub Head</td>
		<td class="bg" style="font-size: 11px;font-weight: bold">Debit Amount</td>
		<td class="bg" style="font-size: 11px;font-weight: bold">Credit Amount</td>
		<td class="bg" style="font-size: 11px;font-weight: bold">Print</td>
	</tr>
		<%if(CUSTLIST.size()>0){
			for(int i=0 ; i < CUSTLIST.size(); i++){
				CUST=(customerpurchases)CUSTLIST.get(i);
				debitamount=debitamount+Double.parseDouble(CUST.gettotalAmmount());
				j++;
				String ddd=CUST.gettotalAmmount();
				System.out.println("ddd..."+ddd);
				%>
		<tr style="background-color: #E6D6A8">
    	<td style="font-size: 10px;font-weight: bold"><%=j %></td>
    	<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('JVorBKTEdit.jsp?purchaseId=<%=CUST.getpurchaseId()%>&majorHeadId=<%=CUST.getextra6()%>&minorHeadId=<%=CUST.getextra7()%>&subHeadId=<%=CUST.getproductId()%>&headOfAccount=<%=CUST.getextra1()%>&amount=<%=CUST.gettotalAmmount()%>&entryDate=<%=CUST.getdate()%>'); return false;"><%=CUST.getvocharNumber()%></span></td>
		<td style="font-size: 10px;font-weight: bold"><%=df2.format(dateFormat.parse(CUST.getdate())) %></td>
		<td style="font-size: 10px;font-weight: bold"><%=CUST.getextra2() %></td>
		<td style="font-size: 10px;font-weight: bold"> <%=HOA_L.getHeadofAccountName(CUST.getextra1()) %></td>
		<td style="font-size: 10px;font-weight: bold"> <%=CUST.getextra6() %> <%=majorheadListing.getmajorheadName(CUST.getextra6()) %></td>
		<td style="font-size: 10px;font-weight: bold"> <%=CUST.getextra7() %> <%=minorheadListing.getminorheadName(CUST.getextra7()) %></td>
		<td style="font-size: 10px;font-weight: bold"><%=CUST.getproductId() %> <%=SUBHL.getMsubheadnameWithDepartment(CUST.getproductId(), CUST.getextra1()) %></td>
		<td style="font-size: 10px;font-weight: bold"><%=df.format(Double.parseDouble(CUST.gettotalAmmount())) %></td>
		<td style="font-size: 10px;font-weight: bold">00</td>
		<td width="7%" style="font-size: 10px;font-weight: bold;"><a href="adminPannel.jsp?page=VoucherPrint&CUST=<%=CUST.getvocharNumber()%>&majorid=<%=CUST.getextra6() %> <%=majorheadListing.getmajorheadName(CUST.getextra6()) %>&minid=<%=CUST.getextra7() %> <%=minorheadListing.getminorheadName(CUST.getextra7()) %>&subid=<%=CUST.getproductId() %> <%=SUBHL.getMsubheadnameWithDepartment(CUST.getproductId(), CUST.getextra1()) %>&dt=<%=df2.format(dateFormat.parse(CUST.getdate())) %>&hoid=<%=HOA_L.getHeadofAccountName(CUST.getextra1()) %>&amount=<%=ddd %>&nrr=<%=CUST.getextra2() %>">VOUCHER PRINT</a></td>
		</tr>
		
		<%
			if (bktmap1.containsKey(CUST.getvocharNumber()))	{
				banktransactions bkt = bktmap1.get(CUST.getvocharNumber());
				map2.remove(CUST.getvocharNumber());
				creditamount=creditamount+Double.parseDouble(bkt.getamount());
				%>
				<tr style="background-color: #A8E3E6">
		    		<td style="font-size: 10px;font-weight: bold"><%=j %></td>
		    		<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('JVorBKTEdit.jsp?bankTransactionId=<%=bkt.getbanktrn_id()%>&majorHeadId=<%=bkt.getMajor_head_id()%>&minorHeadId=<%=bkt.getMinor_head_id()%>&subHeadId=<%=bkt.getSub_head_id()%>&headOfAccount=<%=bkt.getHead_account_id()%>&amount=<%=bkt.getamount()%>&entryDate=<%=bkt.getdate()%>'); return false;"> <%=CUST.getvocharNumber() %><%-- <%=bkt.getbanktrn_id()%> --%></span></td>
					<td style="font-size: 10px;font-weight: bold"><%=df2.format(dateFormat.parse(bkt.getdate())) %></td>
					<td style="font-size: 10px;font-weight: bold"><%=bkt.getnarration() %></td>
					<td style="font-size: 10px;font-weight: bold"> <%=HOA_L.getHeadofAccountName(bkt.getHead_account_id()) %></td>
					<%-- <td style="font-size: 10px;font-weight: bold"> <%=bkt.getMajor_head_id() %> <%=bkt.getExtra10()%></td>
					<td style="font-size: 10px;font-weight: bold"> <%=bkt.getMinor_head_id() %> <%=bkt.getExtra11()%></td>
					<td style="font-size: 10px;font-weight: bold"><%=bkt.getSub_head_id()%> <%=bkt.getExtra12()%></td> --%>
					<td style="font-size: 10px;font-weight: bold" colspan="3" align="center"><%=banksMap.get(bkt.getbank_id()).getbank_name() %></td>
					<td style="font-size: 10px;font-weight: bold">00</td>
					<td style="font-size: 10px;font-weight: bold"><%=df.format(Double.parseDouble(bkt.getamount())) %></td>
					<td style="font-size: 10px;font-weight: bold"> </td>
				</tr>
			<%}
			else if (bktmap2.containsKey(CUST.getvocharNumber()))	{
				banktransactions bkt = bktmap2.get(CUST.getvocharNumber());
				map2.remove(CUST.getvocharNumber());
				creditamount=creditamount+Double.parseDouble(bkt.getamount());
				%>
				<tr style="background-color: #A8E3E6">
		    		<td style="font-size: 10px;font-weight: bold"><%=j %></td>
		    		<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('JVorBKTEdit.jsp?bankTransactionId=<%=bkt.getbanktrn_id()%>&majorHeadId=<%=bkt.getMajor_head_id()%>&minorHeadId=<%=bkt.getMinor_head_id()%>&subHeadId=<%=bkt.getSub_head_id()%>&headOfAccount=<%=bkt.getHead_account_id()%>&entryDate=<%=bkt.getdate()%>'); return false;"><%=CUST.getvocharNumber() %><%-- <%=bkt.getbanktrn_id()%> --%></span></td>
					<td style="font-size: 10px;font-weight: bold"><%=df2.format(dateFormat.parse(bkt.getdate())) %></td>
					<td style="font-size: 10px;font-weight: bold"><%=bkt.getnarration() %></td>
					<td style="font-size: 10px;font-weight: bold"> <%=HOA_L.getHeadofAccountName(bkt.getHead_account_id()) %></td>
						<%-- <td style="font-size: 10px;font-weight: bold"> <%=bkt.getMajor_head_id() %> <%=bkt.getExtra10()%></td>
					<td style="font-size: 10px;font-weight: bold"> <%=bkt.getMinor_head_id() %> <%=bkt.getExtra11()%></td>
					<td style="font-size: 10px;font-weight: bold"><%=bkt.getSub_head_id()%> <%=bkt.getExtra12()%></td> --%>
					<td style="font-size: 10px;font-weight: bold" colspan="3" align="center"><%=banksMap.get(bkt.getbank_id()).getbank_name() %></td>
					<td style="font-size: 10px;font-weight: bold">00</td>
					<td style="font-size: 10px;font-weight: bold"><%=df.format(Double.parseDouble(bkt.getamount())) %></td>
					<td width="7%" style="font-size: 10px;font-weight: bold;"></td>
				</tr>
			<%}
			else if (map1.containsKey(CUST.getvocharNumber()))	{
				List<productexpenses> pexpLst = (List<productexpenses>)map1.get(CUST.getvocharNumber());
				map1.remove(CUST.getvocharNumber());
				map2.remove(CUST.getvocharNumber());
				for (int l = 0 ; l < pexpLst.size() ; l++)	{
					productexpenses pe = pexpLst.get(l);
					creditamount=creditamount+Double.parseDouble(pe.getamount());
				String ddrr=pe.getamount();
					%>
	<tr style="background-color: #A8E3E6">
    	<td style="font-size: 10px;font-weight: bold"><%=j %></td>
    	<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('JVorBKTEdit.jsp?expenseId=<%=pe.getexp_id()%>&majorHeadId=<%=pe.getmajor_head_id()%>&minorHeadId=<%=pe.getminorhead_id()%>&subHeadId=<%=pe.getsub_head_id()%>&headOfAccount=<%=pe.gethead_account_id()%>&amount=<%=pe.getamount()%>&entryDate=<%=pe.getentry_date()%>'); return false;"><%=pe.getextra5() %></span></td>
		<td style="font-size: 10px;font-weight: bold"><%=df2.format(dateFormat.parse(pe.getentry_date())) %></td>
		<td style="font-size: 10px;font-weight: bold"><%=pe.getnarration() %></td>
		<td style="font-size: 10px;font-weight: bold"> <%=HOA_L.getHeadofAccountName(pe.gethead_account_id()) %></td>
		<td style="font-size: 10px;font-weight: bold"> <%=pe.getmajor_head_id() %> <%=majorheadListing.getmajorheadName(pe.getmajor_head_id()) %></td>
		<td style="font-size: 10px;font-weight: bold"> <%=pe.getminorhead_id() %> <%=minorheadListing.getminorheadName(pe.getminorhead_id()) %></td>
		<td style="font-size: 10px;font-weight: bold"><%=pe.getsub_head_id() %> <%=SUBHL.getMsubheadnameWithDepartment(pe.getsub_head_id() ,pe.gethead_account_id())%></td>
		<td style="font-size: 10px;font-weight: bold">00</td>
		<td style="font-size: 10px;font-weight: bold"><%=df.format(Double.parseDouble(pe.getamount())) %></td>
	<td width="7%" style="font-size: 10px;font-weight: bold;"><a href="adminPannel.jsp?page=VoucherPrint&CUST=<%=pe.getextra5() %>&majorid=<%=pe.getmajor_head_id() %> <%=majorheadListing.getmajorheadName(pe.getmajor_head_id()) %>&minid=<%=pe.getminorhead_id() %> <%=minorheadListing.getminorheadName(pe.getminorhead_id()) %>&subid=<%=pe.getsub_head_id() %> <%=SUBHL.getMsubheadnameWithDepartment(pe.getsub_head_id() ,pe.gethead_account_id())%>&dt=<%=df2.format(dateFormat.parse(pe.getentry_date())) %>&hoid=<%=HOA_L.getHeadofAccountName(CUST.getextra1()) %>&amount=<%=ddrr %>&nrr=<%=pe.getnarration() %>">VOUCHER PRINT</a></td>

	</tr>
	<%}%>
 <%}
   else		{%>
			<tr style="background-color: #A8E3E6"><td colspan="12" align="center">xxxxxxxx</td></tr>	
	
	<%}
  }
}

		Set<String> keys = map1.keySet();
		Iterator<String> itr = keys.iterator();
		while (itr.hasNext())	{
			String key = itr.next();
			List<productexpenses> pexpLst = (List<productexpenses>)map1.get(key);
			for (productexpenses pe : pexpLst)	{
			debitamount = debitamount + Double.parseDouble(pe.getamount());
			j++;%>
		<tr style="background-color: #E6D6A8">
    		<td style="font-size: 10px;font-weight: bold"><%=j %></td>
    		<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('JVorBKTEdit.jsp?expenseId=<%=pe.getexp_id()%>&majorHeadId=<%=pe.getmajor_head_id()%>&minorHeadId=<%=pe.getminorhead_id()%>&subHeadId=<%=pe.getsub_head_id()%>&headOfAccount=<%=pe.gethead_account_id()%>&entryDate=<%=pe.getentry_date()%>'); return false;"><%=pe.getextra5() %></span></td>
			<td style="font-size: 10px;font-weight: bold"><%=df2.format(dateFormat.parse(pe.getentry_date())) %></td>
			<td style="font-size: 10px;font-weight: bold"><%=pe.getnarration() %></td>
			<td style="font-size: 10px;font-weight: bold"> <%=HOA_L.getHeadofAccountName(pe.gethead_account_id()) %></td>
			<td style="font-size: 10px;font-weight: bold"> <%=pe.getmajor_head_id() %> <%=majorheadListing.getmajorheadName(pe.getmajor_head_id()) %></td>
			<td style="font-size: 10px;font-weight: bold"> <%=pe.getminorhead_id() %> <%=minorheadListing.getminorheadName(pe.getminorhead_id()) %></td>
			<td style="font-size: 10px;font-weight: bold"><%=pe.getsub_head_id() %> <%=SUBHL.getMsubheadnameWithDepartment(pe.getsub_head_id() ,pe.gethead_account_id())%></td>
			<td style="font-size: 10px;font-weight: bold"><%=df.format(Double.parseDouble(pe.getamount())) %></td>
			<td style="font-size: 10px;font-weight: bold">00</td>
		<td width="7%" style="font-size: 10px;font-weight: bold;">HFGJ</td>
		</tr>
		  <%}
			if (bktmap1.containsKey(key))	{
				banktransactions bkt = bktmap1.get(key);
				creditamount = creditamount + Double.parseDouble(bkt.getamount());%>
				<tr style="background-color: #A8E3E6">
		    		<td style="font-size: 10px;font-weight: bold"><%=j %></td>
		    		<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('JVorBKTEdit.jsp?bankTransactionId=<%=bkt.getbanktrn_id()%>&majorHeadId=<%=bkt.getMajor_head_id()%>&minorHeadId=<%=bkt.getMinor_head_id()%>&subHeadId=<%=bkt.getSub_head_id()%>&headOfAccount=<%=bkt.getHead_account_id()%>&entryDate=<%=bkt.getdate()%>'); return false;"><%=bkt.getbanktrn_id()%></span></td>
					<td style="font-size: 10px;font-weight: bold"><%=df2.format(dateFormat.parse(bkt.getdate())) %></td>
					<td style="font-size: 10px;font-weight: bold"><%=bkt.getnarration() %></td>
					<td style="font-size: 10px;font-weight: bold"> <%=HOA_L.getHeadofAccountName(bkt.getHead_account_id()) %></td>
						<%-- <td style="font-size: 10px;font-weight: bold"> <%=bkt.getMajor_head_id() %> <%=bkt.getExtra10()%></td>
					<td style="font-size: 10px;font-weight: bold"> <%=bkt.getMinor_head_id() %> <%=bkt.getExtra11()%></td>
					<td style="font-size: 10px;font-weight: bold"><%=bkt.getSub_head_id()%> <%=bkt.getExtra12()%></td> --%>
					<td style="font-size: 10px;font-weight: bold" colspan="3" align="center"><%=banksMap.get(bkt.getbank_id()).getbank_name() %></td>
					<td style="font-size: 10px;font-weight: bold">00</td>
					<td style="font-size: 10px;font-weight: bold"><%=df.format(Double.parseDouble(bkt.getamount())) %></td>
	<td width="7%" style="font-size: 10px;font-weight: bold;">SF</td>
				</tr>
			<%}
			else if (bktmap2.containsKey(key))	{
				banktransactions bkt = bktmap2.get(key);
				creditamount = creditamount + Double.parseDouble(bkt.getamount());%>
				<tr style="background-color: #A8E3E6">
		    		<td style="font-size: 10px;font-weight: bold"><%=j %></td>
		    		<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('JVorBKTEdit.jsp?bankTransactionId=<%=bkt.getbanktrn_id()%>&majorHeadId=<%=bkt.getMajor_head_id()%>&minorHeadId=<%=bkt.getMinor_head_id()%>&subHeadId=<%=bkt.getSub_head_id()%>&headOfAccount=<%=bkt.getHead_account_id()%>'); return false;"><%=bkt.getbanktrn_id()%></span></td>
					<td style="font-size: 10px;font-weight: bold"><%=df2.format(dateFormat.parse(bkt.getdate())) %></td>
					<td style="font-size: 10px;font-weight: bold"><%=bkt.getnarration() %></td>
					<td style="font-size: 10px;font-weight: bold"> <%=HOA_L.getHeadofAccountName(bkt.getHead_account_id()) %></td>
						<%-- <td style="font-size: 10px;font-weight: bold"> <%=bkt.getMajor_head_id() %> <%=bkt.getExtra10()%></td>
					<td style="font-size: 10px;font-weight: bold"> <%=bkt.getMinor_head_id() %> <%=bkt.getExtra11()%></td>
					<td style="font-size: 10px;font-weight: bold"><%=bkt.getSub_head_id()%> <%=bkt.getExtra12()%></td> --%>
					<td style="font-size: 10px;font-weight: bold" colspan="3" align="center"><%=banksMap.get(bkt.getbank_id()).getbank_name() %></td>
					<td style="font-size: 10px;font-weight: bold">00</td>
					<td style="font-size: 10px;font-weight: bold"><%=df.format(Double.parseDouble(bkt.getamount())) %></td>
					<td width="7%" style="font-size: 10px;font-weight: bold;">GF</td>
				</tr>
			<%}
			else if (map2.containsKey(key))	{
				List<customerpurchases> cpLst = (List<customerpurchases>)map2.get(key);
				for (int i = 0 ; i < cpLst.size() ; i++)	{
					customerpurchases cust = cpLst.get(i);
					creditamount = creditamount + Double.parseDouble(cust.gettotalAmmount()); %>
			<tr style="background-color: #A8E3E6">
    			<td style="font-size: 10px;font-weight: bold"><%=j %></td>
    			<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('JVorBKTEdit.jsp?purchaseId=<%=cust.getpurchaseId()%>&majorHeadId=<%=cust.getextra6()%>&minorHeadId=<%=cust.getextra7()%>&subHeadId=<%=cust.getproductId()%>&headOfAccount=<%=cust.getextra1()%>'); return false;"><%=cust.getvocharNumber()%></span></td>
				<td style="font-size: 10px;font-weight: bold"><%=df2.format(dateFormat.parse(cust.getdate())) %></td>
				<td style="font-size: 10px;font-weight: bold"><%=cust.getextra2() %></td>
				<td style="font-size: 10px;font-weight: bold"> <%=HOA_L.getHeadofAccountName(cust.getextra1()) %></td>
				<td style="font-size: 10px;font-weight: bold"> <%=cust.getextra6() %> <%=majorheadListing.getmajorheadName(cust.getextra6()) %></td>
				<td style="font-size: 10px;font-weight: bold"> <%=cust.getextra7() %> <%=minorheadListing.getminorheadName(cust.getextra7()) %></td>
				<td style="font-size: 10px;font-weight: bold"><%=cust.getproductId() %> <%=SUBHL.getMsubheadnameWithDepartment(cust.getproductId(), cust.getextra1()) %></td>
				<td style="font-size: 10px;font-weight: bold"><%=df.format(Double.parseDouble(cust.gettotalAmmount())) %></td>
				<td style="font-size: 10px;font-weight: bold">00</td>
			</tr>
				<%}
			}
			else	{%>
				<tr style="background-color: #A8E3E6"><td colspan="10" align="center">-----------</td></tr>
			<%}
		}%>
		
	<%if(debitamount>0 || creditamount>0){%>
	<tr>
		<td style="font-weight: bold;font-size: 12px;color: #E18D25;text-align: right;" colspan="8">Total  &nbsp; &nbsp;</td>
		<td style="font-weight: bold;font-size: 12px;color: #E18D25;">Rs.<%=df.format(debitamount) %>  /-</td>
		<td style="font-weight: bold;font-size: 12px;color: #E18D25;">Rs.<%=df.format(creditamount) %>  /-</td>
	</tr>
	<%}else { %>
	<tr>
		<td style="font-weight: bold;font-size: 13px;color: #E12530;text-align: center;" colspan="10">No, Journal Entry's.</td>
	</tr>
	<%} %>
	</table>
</div>
</div>
</div>
</body>
</html>