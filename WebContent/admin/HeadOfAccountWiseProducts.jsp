<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.products"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.util.List" %>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="PRD" class="beans.products"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HeadOfAccountWiseProducts</title>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
        function minorhead(id){
        	$('#'+id).toggle();
        }
    </script>
</head>
<body>
<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
														
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span> <span>
						
					</span>
				</div>
<form action="adminPannel.jsp" method="post">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="page" value="HeadOfAccountWiseProducts">
<tr> 
   <th>Head Of Account</th>
<%
headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
productsListing PRD_LST=new productsListing();

majorheadListing MJR_L=new majorheadListing();
minorheadListing MIN_L=new minorheadListing();
productsListing PRD_L=new productsListing();
subheadListing SBH_L=new subheadListing();

String hoaid="1";
if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
	hoaid=request.getParameter("head_account_id");
}

%>
<td><select name="head_account_id" id="head_account_id" >
 <%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %> 
</select>
<input type="submit" value="Search" class="click"/>
</td>

</tr>


</table>
</form>
<%

    String head_account_id=request.getParameter("head_account_id");

    List PRDL=new ArrayList();
    if(head_account_id!=null && head_account_id!=""){  
    	 PRDL=PRD_LST.getProductsListbsdOnHeadOfAccount(head_account_id);
    	   
    	   if(PRDL!=null && PRDL.size()>0){
%>
  
   <table width="100%" cellpadding="0" cellspacing="0"  id="tblExport">
<tr><td colspan="6"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td  width="3%"><strong>S.No.</strong></td>
<td width="10%"><strong>Head Of Account</strong></td>
<td  width="20%"><strong>Major Head</strong></td>
<td  width="23%"><strong>Minor Head</strong></td>
<td  width="23%"><strong>SubHead</strong></td>
<td  width="20%"><strong>Product</strong></td>
</tr>
</table></td></tr>
<%
   

if(PRDL!=null && PRDL.size()>0){
	  
	  for(int i=0;i<PRDL.size();i++){
	  PRD=(products)PRDL.get(i);%>
<tr>
<td width="3%"><%=i+1%></td>
<td width="10%"><%=head_account_id%><%=HOA_L.getHeadofAccountName(head_account_id)%></td>
<td width="20%"><%=MJR_L.getMajorHeadName(PRD.getmajor_head_id(),head_account_id)%></td>
<td width="23%"> <%=PRD.getextra3()%> <%=MIN_L.getminorheadName(PRD.getextra3())%></td>
<td width="23%"><%=PRD.getsub_head_id()%> <%=SBH_L.getMsubheadnameWithDepartment(PRD.getsub_head_id(),head_account_id) %></td>
<td width="23%"><%=PRD.getproductId()%>  <%=PRD_L.getProductName(PRD.getproductId(),head_account_id)%></td>

</tr>
<%} 
}  
}else{
	%>
	   <center><h1 style='color:red'><strong>No Products Are Avilable</strong></h1></center>
	<%
}
}
%>
</table> 
      
     
      
</body>
</html>