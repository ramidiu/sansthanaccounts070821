<%@page import="beans.bankbalance"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%

headofaccountsListing HOA_L = new headofaccountsListing();
headofaccounts HOA = new headofaccounts();
List headaclist = HOA_L.getheadofaccounts();

String fromDate = request.getParameter("fromDate");
String toDate = request.getParameter("toDate");
String hoaid = request.getParameter("head_account_id");
String cumltv = request.getParameter("cumltv");
String finYr = request.getParameter("finYear");

if(hoaid == null){
	hoaid = "1";
}

%>


<form name="departmentsearch" id="departmentsearch" action="adminPannel.jsp" method="get" style="padding-left: 70px;padding-top: 30px;">
			<input type="hidden" name="page" value="DayWiseIncomeExpenditureReportNeww"> 
			<table width="80%" border="0" cellspacing="0" cellpadding="0" style="margin: 39px 0;">
<tbody>
				<tr>
				<td colspan="2">Select Financial<br>Year</td>
				<td class="border-nn">&nbsp;&nbsp;From Date</td>
				<td class="border-nn">To Date</td>
				<td class="border-nn" colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
				<td class="border-nn">Report Type</td>
				</tr>
				<tr>
					 
			<td colspan="2">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<%@ include file="yearDropDown1.jsp" %>
					</select>
				  </td>
			
				
				<td class="border-nn">&nbsp;&nbsp;<input type="text" name="fromDate" id="fromDate" value="2017-04-01" class="hasDatepicker"><img class="ui-datepicker-trigger" src="../images/calendar-icon.png" alt="Select Date" title="Select Date"> </td>
				<td class="border-nn"><input type="text" name="toDate" id="toDate" value="2018-03-31" class="hasDatepicker"><img class="ui-datepicker-trigger" src="../images/calendar-icon.png" alt="Select Date" title="Select Date">
				<input type="hidden" name="page" value="DayWise-Income-Expenditure-Report_neww">
				 </td>
				<td colspan="2" class="border-nn">
					<select name="head_account_id" id="head_account_id" onchange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
						<%
							if(headaclist.size()>0){
								for(int i=0;i<headaclist.size();i++){
								HOA = (headofaccounts) headaclist.get(i);%>
							<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
							<%}
								} %>
					</select>
				</td>
				
				<td>
					<select name="cumltv" id="cumltv">
						<option value="noncumulative" selected="selected">NON CUMULATIVE</option>
						<option value="cumulative">CUMULATIVE</option>
					</select>
				</td>
				<td class="border-nn">
					<input type="submit" value="SEARCH" class="click">
				</td>
			</tr>
		</tbody>
</table>
 </form>
 
 <%
 

 
//  if(session.getAttribute("adminId")!=null){ 
 
 if(fromDate !=null && !fromDate.trim().equals("") && toDate !=null && !toDate.trim().equals("") && hoaid !=null && !hoaid.trim().equals("") && cumltv !=null && !cumltv.trim().equals("")){
 
	 
	 
	 customerpurchasesListing CP_L = new customerpurchasesListing();
		majorheadListing MH_L = new majorheadListing();
		majorhead MH = new majorhead(); 
		banktransactionsListing BNK_L = new banktransactionsListing();
		subheadListing SUBH_L = new subheadListing();
		subhead SUBH = new subhead();
		bankbalanceListing BBAL_L = new bankbalanceListing();
		productexpensesListing PRDEXP_L=new productexpensesListing();
		minorheadListing MINH_L=new minorheadListing();
		
	 
	 
	 DecimalFormat df=new DecimalFormat("#,###.00");
	 
	 
	 	String headgroup = "";
	 	
	 	String cashtype = "online pending";
		String cashtype1 = "othercash";
		String cashtype2 = "offerKind";
		String cashtype3 = "journalvoucher";
		
		double creditincome = 0.0;
		double debitincome = 0.0;
		double creditexpens = 0.0;
		double debitexpens = 0.0;
		
		double credit = 0.0;
		double debit = 0.0;
	 
		if(hoaid.equals("SasthanDev")){
			headgroup = "2";
			hoaid = "4";
		} else if(hoaid.equals("4")){
			headgroup = "1";
		}
	 
		
		
		List<majorhead> incomemajhdlist = MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew1(hoaid, "revenue",headgroup,"IE");
		
		List<majorhead> expendmajhdlist = MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew1(hoaid,"expenses",headgroup,"IE");
 
		String finStartYr = request.getParameter("finYear").substring(0, 4);
		String fdate = finStartYr+"-04-01"+" 00:00:00";
		
		if(cumltv.equals("noncumulative")){
			fdate = request.getParameter("fromDate")+" 00:00:00";
		}
		
		String tdate = request.getParameter("toDate")+" 23:59:59";
		
 %>
 
<div class="printable" id="tblExport">


<table width="90%" cellpadding="0" cellspacing="0" style="    margin-bottom: 10px; font-size: 13px; margin-top:10px; margin:auto;">
<tbody><tr>
						<td colspan="3" align="center" style="font-weight: bold; margin-bottom:12px;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY</td>
					</tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Income &amp; Expenditure Report</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  01-Apr-2017 TO 04-Apr-2017 (noncumulative) </td></tr>



</tbody></table>
<div style="clear:both"></div>

<table width="90%" border="1" cellspacing="0" cellpadding="0" style="margin: 23px auto; font-size: 13px;" class="new-table">
  <tbody><tr>
    <th width="38%" height="29" align="center">Account Name</th>
    <th width="29%" align="center">Debits</th>
    <th width="33%" align="center">Credits</th>
  </tr>
  
  <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">INCOME</td>
  
  </tr>
  
  <%
  
  			if( incomemajhdlist.size() > 0 ){ 
  
  				String corpusMajorHead = "";
  				String corpusMinorHead = "";
  				double corpusfundAmt = 0.0;
  				String minorheads[] = new String[2];
  				
  				if(hoaid.equals("1"))
  				{
  					corpusMajorHead = "13";
  					corpusMinorHead = "476";
  					minorheads = new String[]{"122","469"};
  				}
  				else if(hoaid.equals("3"))
  				{
  					corpusMajorHead = "14";
  					corpusMinorHead = "21532";
  					minorheads = new String[]{"132"};
  				}
  				else if(hoaid.equals("4"))
  				{
  					corpusMajorHead = "11";
  					corpusMinorHead = "473";
  					minorheads = new String[]{"102","468"};
  				}
  				else if(hoaid.equals("5"))
  				{
  					corpusMajorHead = "15";
  					corpusMinorHead = "478";
  					minorheads = new String[]{"142","472"};
  				}
  
  				double creditCorpusfund = 0.0; 
  				
  				List subh3 = SUBH_L.getSubheadListMinorhead4(hoaid,corpusMajorHead,corpusMinorHead,"IE");
  				
  				String subhd3[]=new String[subh3.size()];
  
  				for(int i = 0; i < subh3.size(); i++){
  					
  					SUBH = (subhead)subh3.get(i);
  					subhd3[i] = SUBH.getsub_head_id();
  					
  				}
  				
  				if( cumltv.equals("cumulative") ){	
  					
  					creditCorpusfund = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,corpusMajorHead,corpusMinorHead, "", "minorhead","Liabilites", finYr, "credit", "","extra5",subhd3);
  					
  				}
  				
  				String LedgerSumOfSubhead31 = CP_L.getLedgerSumOfSubhead31(corpusMinorHead,"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd3);
  				String LedgerSum1 = CP_L.getLedgerSum1(corpusMinorHead,"extra3",fdate,tdate,"",hoaid,"sub_head_id",subhd3);
  				String LedgerSumBasedOnJV1 = PRDEXP_L.getLedgerSumBasedOnJV1(corpusMinorHead,"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd3);
  				String LedgerSumDollarAmt1 = CP_L.getLedgerSumDollarAmt1(corpusMinorHead,"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd3);
  				
  				
  				corpusfundAmt = creditCorpusfund+Double.parseDouble(LedgerSumOfSubhead31) + Double.parseDouble(LedgerSum1) + Double.parseDouble(LedgerSumBasedOnJV1) + Double.parseDouble(LedgerSumDollarAmt1);
  				
  				System.out.println("corpusfundAmt ========> "+corpusfundAmt);
  				
  				for(int i = 0 ; i < incomemajhdlist.size() ; i++){
  					
  					MH = (majorhead) incomemajhdlist.get(i);
  					
  					List subh2 = SUBH_L.getSubheadListMinorhead3(MH.getmajor_head_id(),"IE");
					String subhd2[] = new String[subh2.size()];
					
					for( int j = 0 ; j < subh2.size(); j++){
						
						SUBH = (subhead) subh2.get(j);
						subhd2[j] = SUBH.getsub_head_id();
						
					}
					System.out.println("111111111111111111111111");
					
					List<String[]> incomeMjrdebitAmtList = PRDEXP_L.getLedgerSum11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2);
					List<String[]> incomeMjrcreditAmtList = CP_L.getLedgerSumOfSubhead1111(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd2);
					List<String[]> incomeMjrdebitAmtJVList = CP_L.getLedgerSumBasedOnJV11(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2);
					List<String[]> incomeMjrcreditAmtJVList = PRDEXP_L.getLedgerSumBasedOnJV11(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2);
					List<String[]> incomeMjrcreditAmtDollarList = CP_L.getLedgerSumDollarAmt11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd2);
					
					double incomeMjrdebitAmt = 0.0;
					double incomeMjrcreditAmt = 0.0;
					double incomeMjrdebitAmtJV = 0.0;
					double incomeMjrcreditAmtJV =0.0;
					double incomeMjrcreditAmtDollar = 0.0;
					
					if(incomeMjrdebitAmtList != null && incomeMjrdebitAmtList.size()> 0){
						for(int j = 0 ; j < incomeMjrdebitAmtList.size(); j++){
							incomeMjrdebitAmt = incomeMjrdebitAmt + Double.parseDouble(incomeMjrdebitAmtList.get(j)[2].toString());
						}
					}
					
					if(incomeMjrcreditAmtList != null && incomeMjrcreditAmtList.size()> 0){
						for(int j = 0 ; j < incomeMjrcreditAmtList.size(); j++){
							incomeMjrcreditAmt = incomeMjrcreditAmt + Double.parseDouble(incomeMjrcreditAmtList.get(j)[2].toString());
						}
					}
					
					if(MH.getmajor_head_id().equals("11")){
						System.out.println("incomeMjrcreditAmt =====> "+incomeMjrcreditAmt);
					}
					
					if(incomeMjrdebitAmtJVList != null && incomeMjrdebitAmtJVList.size()> 0){
						for(int j = 0 ; j < incomeMjrdebitAmtJVList.size(); j++){
							incomeMjrdebitAmtJV = incomeMjrdebitAmtJV + Double.parseDouble(incomeMjrdebitAmtJVList.get(j)[2].toString());
						}
					}
					
					if(incomeMjrcreditAmtJVList != null && incomeMjrcreditAmtJVList.size()> 0){
						for(int j = 0 ; j < incomeMjrcreditAmtJVList.size(); j++){
							incomeMjrcreditAmtJV = incomeMjrcreditAmtJV + Double.parseDouble(incomeMjrcreditAmtJVList.get(j)[2].toString());
						}
					}
					
					if(MH.getmajor_head_id().equals("11")){
						System.out.println("incomeMjrcreditAmtJV =====> "+incomeMjrcreditAmtJV);
					}
					
					if(incomeMjrcreditAmtDollarList != null && incomeMjrcreditAmtDollarList.size()> 0){
						for(int j = 0 ; j < incomeMjrcreditAmtDollarList.size(); j++){
							incomeMjrcreditAmtDollar = incomeMjrcreditAmtDollar + Double.parseDouble(incomeMjrcreditAmtDollarList.get(j)[2].toString());
						}
					}
					
					if(MH.getmajor_head_id().equals("11")){
						System.out.println("incomeMjrcreditAmtDollar =====> "+incomeMjrcreditAmtDollar);
					}
					
					
					double incomeMnrdebitAmtt = 0.0;
					double incomeMjrcreditAmtt = 0.0;
					double incomeMjrdebitAmttJV = 0.0;
					double incomeMjrcreditAmttJV = 0.0;
					
					if(minorheads.length > 0){
						for(int m = 0 ; m < minorheads.length; m++){
							
							double incomeMnrdebitAmtt1 = 0.0;
							double incomeMjrcreditAmtt1 = 0.0;
							double incomeMjrdebitAmt1JV1 = 0.0;
							double incomeMjrcreditAmttJV1 = 0.0;
							

// 							incomeMnrdebitAmtt1 = Double.parseDouble(PRDEXP_L.getLedgerSum1(minorheads[m],"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd2));
// 							incomeMnrdebitAmtt = incomeMnrdebitAmtt + incomeMnrdebitAmtt1;
							
// 							incomeMjrdebitAmt1JV1 = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(minorheads[m],"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2));
// 							incomeMjrdebitAmttJV = incomeMjrdebitAmttJV +incomeMjrdebitAmt1JV1;
							
// 							incomeMjrcreditAmtt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead111(minorheads[m],"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd2));
// 							incomeMjrcreditAmtt = incomeMjrcreditAmtt+incomeMjrcreditAmtt1;
							
// 							incomeMjrcreditAmttJV1 = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(minorheads[m],"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2));
// 							incomeMjrcreditAmttJV = incomeMjrcreditAmttJV+incomeMjrcreditAmttJV1;
							
							
							if(incomeMjrdebitAmtList != null && incomeMjrdebitAmtList.size()> 0){
								for(int j = 0 ; j < incomeMjrdebitAmtList.size(); j++){
									if(incomeMjrdebitAmtList.get(j)[0].toString().trim().equals(minorheads[m])){
										incomeMnrdebitAmtt1 = Double.parseDouble(incomeMjrdebitAmtList.get(j)[2].toString());
										incomeMnrdebitAmtt = incomeMnrdebitAmtt + incomeMnrdebitAmtt1;
									}
								}
							}
							
							if(incomeMjrcreditAmtList != null && incomeMjrcreditAmtList.size()> 0){
								for(int j = 0 ; j < incomeMjrcreditAmtList.size(); j++){
									if(incomeMjrcreditAmtList.get(j)[0].toString().trim().equals(minorheads[m])){
										incomeMjrcreditAmtt1 = Double.parseDouble(incomeMjrcreditAmtList.get(j)[2].toString());
										incomeMjrcreditAmtt = incomeMjrcreditAmtt + incomeMjrcreditAmtt1;
									}
								}
							}
							
							if(MH.getmajor_head_id().equals("11")){
								System.out.println("incomeMjrcreditAmtt =====> "+incomeMjrcreditAmtt);
							}
							
							if(incomeMjrdebitAmtJVList != null && incomeMjrdebitAmtJVList.size()> 0){
									for(int j = 0 ; j < incomeMjrdebitAmtJVList.size(); j++){
										if(incomeMjrdebitAmtJVList.get(j)[0].toString().trim().equals(minorheads[m])){
											incomeMjrdebitAmt1JV1 = Double.parseDouble(incomeMjrdebitAmtJVList.get(j)[2].toString());
											incomeMjrdebitAmttJV = incomeMjrdebitAmttJV + incomeMjrdebitAmt1JV1;
									}
								}
							}
							
							if(incomeMjrcreditAmtJVList != null && incomeMjrcreditAmtJVList.size()> 0){
								for(int j = 0 ; j < incomeMjrcreditAmtJVList.size(); j++){
									if(incomeMjrcreditAmtJVList.get(j)[0].toString().trim().equals(minorheads[m])){
										incomeMjrcreditAmttJV1 = Double.parseDouble(incomeMjrcreditAmtJVList.get(j)[2].toString());
										incomeMjrcreditAmttJV = incomeMjrcreditAmttJV + incomeMjrcreditAmttJV1;
									}
								}
							}
							
							if(MH.getmajor_head_id().equals("11")){
								System.out.println("incomeMjrcreditAmttJV =====> "+incomeMjrcreditAmttJV);
							}

						}
					}
  					
					 double mjdbamt = incomeMjrdebitAmt + incomeMjrdebitAmtJV - incomeMnrdebitAmtt - incomeMjrdebitAmttJV;  
						double mjcdamt = incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar - incomeMjrcreditAmtt - incomeMjrcreditAmttJV;
						
						double debitAmountOpeningBal = 0.0;
						double creditAmountOpeningBal = 0.0;
						double debitAmountOpeningBal1 = 0.0;
						double creditAmountOpeningBal1 = 0.0;
					
						List<bankbalance> totalAmountOpeningBal = null;
								if( cumltv.equals("cumulative") ){
									totalAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal11(hoaid, MH.getmajor_head_id(), finYr,subhd2);
									if(totalAmountOpeningBal != null && totalAmountOpeningBal.size() > 0){
										for(int j = 0 ; j < totalAmountOpeningBal.size(); j++){
											bankbalance bankbalance = totalAmountOpeningBal.get(j);
											if(bankbalance != null && bankbalance.getType().trim().equals("Assets") && bankbalance.getExtra7().trim().equals("debit")){
												debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
											}
											if(bankbalance != null && bankbalance.getType().trim().equals("Liabilites") && bankbalance.getExtra7().trim().equals("credit")){
												creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
											}
										}
									}
								}
						
// 						if( cumltv.equals("cumulative") ){
// 							debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "","extra5",subhd2);
// 							creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Liabilites", finYr, "credit", "","extra5",subhd2);
// // 							if(minorheads.length > 0){
// // 								for(int m=0 ;m<minorheads.length;m++){
// // 							  		double creditAmountOpeningBal11 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),minorheads[m], "", "minorhead","Assets", finYr, "credit", "","extra5",subhd2);
// // 							  		creditAmountOpeningBal1 = creditAmountOpeningBal1 + creditAmountOpeningBal11;
// // 								}
// // 							}
// 						}	
						
							
				
						double totcreditAmountOpeningBal = creditAmountOpeningBal - creditAmountOpeningBal1;
						
						
						System.out.println("creditAmountOpeningBal ======> "+df.format(creditAmountOpeningBal));
						System.out.println("creditAmountOpeningBal1 ======> "+df.format(creditAmountOpeningBal1));
						System.out.println("totcreditAmountOpeningBal ======> "+df.format(totcreditAmountOpeningBal));
						
						if(!("0.0".equals(""+mjdbamt)) || !("0.0".equals(""+mjcdamt)) || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0 || totcreditAmountOpeningBal > 0){
					%>
					<tr>
				    	<td height="28" align="center" style=" color: #934C1E; font-weight:bold;" ><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getmajor_head_id() %> <%=MH.getname() %></a></td>
				    <%
				    
							double corpusfundAmtTemp = 0.0;
					    	if(MH.getmajor_head_id().equals(corpusMajorHead))
					    	{
					    		corpusfundAmtTemp = corpusfundAmt; 
					    	}
					    	
					    	List<banktransactions> otherAmountList = BNK_L.getStringOtherDeposit11("other-amount",MH.getmajor_head_id(),"","",hoaid,fromDate,toDate,"sub_head_id",subhd2);
					    	
					    	 double otherAmountList11 = 0.0;
								if(minorheads.length > 0){
									if(otherAmountList != null && otherAmountList.size() > 0){
										for(int m = 0 ; m < minorheads.length; m++){
											for(int k = 0 ; k < otherAmountList.size(); k++){
												banktransactions banktransactions = otherAmountList.get(k);
												if(banktransactions.getMinor_head_id().trim().equals(minorheads[m])){
													otherAmountList11 = otherAmountList11 + Double.parseDouble(banktransactions.getamount());
												}
// 												double otherAmountList1 = Double.parseDouble(BNK_L.getStringOtherDepositByheads1("other-amount",MH.getmajor_head_id(),minorheads[m],"",hoaid,fromDate,toDate,"sub_head_id",subhd2));
// 												otherAmountList11 = otherAmountList11 + otherAmountList1;
											}
										}
									}
								}
								
								
								
								double otherAmount = 0.0;
								
								if(otherAmountList.size()>0)
								{
									banktransactions OtherAmountList = (banktransactions)otherAmountList.get(0);
									if(OtherAmountList.getamount() != null)
									{
										otherAmount = Double.parseDouble(OtherAmountList.getamount());
									}
								}
								
								if(MH.getmajor_head_id().equals("11")){
									System.out.println("otherAmount =====> "+otherAmount);
									System.out.println("otherAmountList11 =====> "+otherAmountList11);
								}
								
								if(MH.getmajor_head_id().equals("13") || MH.getmajor_head_id().equals("15") || MH.getmajor_head_id().equals("11") || MH.getmajor_head_id().equals("14")){
									
										debitincome = debitincome + debitAmountOpeningBal + incomeMjrdebitAmt + incomeMjrdebitAmtJV - incomeMnrdebitAmtt - incomeMjrdebitAmttJV;
										creditincome = creditincome + totcreditAmountOpeningBal - corpusfundAmtTemp + incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar + otherAmount - otherAmountList11 - incomeMjrcreditAmtt - incomeMjrcreditAmttJV; 
									%> 
								  <td align="center" style=" color: #934C1E; font-weight:bold;">
								  	<%=df.format(debitAmountOpeningBal + incomeMjrdebitAmt + incomeMjrdebitAmtJV - incomeMnrdebitAmtt - incomeMjrdebitAmttJV) %>
								  </td>  
									<td align="center" style=" color: #934C1E; font-weight:bold;">
										<%=df.format(totcreditAmountOpeningBal - corpusfundAmtTemp + incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar + otherAmount - otherAmountList11 - incomeMjrcreditAmtt - incomeMjrcreditAmttJV) %>
										<%
										
										System.out.println("totcreditAmountOpeningBal =====> "+df.format(totcreditAmountOpeningBal));
										System.out.println("corpusfundAmtTemp =====> "+df.format(corpusfundAmtTemp));
										System.out.println("incomeMjrcreditAmt =====> "+df.format(incomeMjrcreditAmt));
										System.out.println("incomeMjrcreditAmtJV =====> "+df.format(incomeMjrcreditAmtJV));
										System.out.println("incomeMjrcreditAmtDollar =====> "+df.format(incomeMjrcreditAmtDollar));
										System.out.println("otherAmount =====> "+df.format(otherAmount));
										System.out.println("otherAmountList11 =====> "+df.format(otherAmountList11));
										System.out.println("incomeMjrcreditAmtt =====> "+df.format(incomeMjrcreditAmtt));
										System.out.println("incomeMjrcreditAmttJV =====> "+df.format(incomeMjrcreditAmttJV));
										
										
										
											System.out.println("totcreditAmountOpeningBal - corpusfundAmtTemp + incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar + otherAmount - otherAmountList11 - incomeMjrcreditAmtt - incomeMjrcreditAmttJV");
										System.out.println(" ==========================   > "+df.format(totcreditAmountOpeningBal - corpusfundAmtTemp + incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar + otherAmount - otherAmountList11 - incomeMjrcreditAmtt - incomeMjrcreditAmttJV));
										%>
									</td>
							  
								<% }else{
									
									debitincome = debitincome + debitAmountOpeningBal + incomeMjrdebitAmt + incomeMjrdebitAmtJV;
									creditincome = creditincome + creditAmountOpeningBal - corpusfundAmtTemp + incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar + otherAmount;
									
									%>
								      <td align="center" style=" color: #934C1E; font-weight:bold;">
								      	<%=df.format(debitAmountOpeningBal + incomeMjrdebitAmt + incomeMjrdebitAmtJV) %>
								      </td>  
								 		<td align="center" style=" color: #934C1E; font-weight:bold;">
								 			<%=df.format(creditAmountOpeningBal - corpusfundAmtTemp + incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar + otherAmount) %>
								 		</td>
							 	<% }
								
								System.out.println("======================================================================================================================");
								
								%>
							 	</tr>
							 	
							 	<%
							////////////////
							
							 	 List minhdlist = MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
							 	minorhead MNH = new minorhead();
								if(minhdlist.size() > 0){ 
									
									for(int k = 0 ; k < minhdlist.size(); k++){
										
										MNH = (beans.minorhead) minhdlist.get(k);
										
										double incomeMnrdebitAmt = 0.0;
										double incomeMnrdebitAmtJV = 0.0;
										double incomeMnrcreditAmt = 0.0;
										double incomeMnrcreditAmt1 = 0.0;
										double incomeMnrcreditAmtJV = 0.0;
										double incomeMnrcreditAmtDollar = 0.0;
										double incomeMnrcreditAmtOfferKind = 0.0;
										double mindbamt = 0.0;
										double mincdamt = 0.0;
										double debitAmountOpeningBal2 = 0.0;
									  	double creditAmountOpeningBal2 = 0.0;
									  	double otherAmount1 = 0.0;
// 									  	List MinHd1 = MINH_L.getMinorHeadBasdOnCompany1(MH.getmajor_head_id(),MNH.getminorhead_id(),"IE");
// 										String MinHead1[] = new String[MinHd1.size()];
										
										if(MNH.getextra1().contains("IE")){
// 										for(int p = 0 ; p < MinHd1.size() ; p++){
// 											MNH = (beans.minorhead) MinHd1.get(p);
// 											MinHead1[p]=" ";
											String minorvalue='"'+MNH.getminorhead_id()+'"';
											
											
											if(!MNH.getminorhead_id().equals(minorvalue)){
												List subh1=SUBH_L.getSubheadListMinorhead2(MNH.getminorhead_id(),"IE");
												String subhd1[]=new String[subh1.size()];
												int m=0;
												
												for(m=0;m<subh1.size();m++){
													
													SUBH=(subhead)subh1.get(m);
													subhd1[m]=SUBH.getsub_head_id();
													
												}
												
// 												 incomeMnrdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum1(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd1));
												 
												 if(incomeMjrdebitAmtList != null && incomeMjrdebitAmtList.size()> 0){
														for(int j = 0 ; j < incomeMjrdebitAmtList.size(); j++){
															if(incomeMjrdebitAmtList.get(j)[0].toString().trim().equals(MNH.getminorhead_id())){
																incomeMnrdebitAmt = incomeMnrdebitAmt + Double.parseDouble(incomeMjrdebitAmtList.get(j)[2].toString());
// 																incomeMnrdebitAmt = incomeMnrdebitAmt + incomeMnrdebitAmtt1;
															}
														}
													}
												 
												 
												 
// 												 incomeMnrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd1));
												 
												 if(incomeMjrdebitAmtJVList != null && incomeMjrdebitAmtJVList.size()> 0){
														for(int j = 0 ; j < incomeMjrdebitAmtJVList.size(); j++){
															if(incomeMjrdebitAmtJVList.get(j)[0].toString().trim().equals(MNH.getminorhead_id())){
																incomeMnrdebitAmtJV = incomeMnrdebitAmtJV + Double.parseDouble(incomeMjrdebitAmtJVList.get(j)[2].toString());
// 																incomeMjrdebitAmttJV = incomeMjrdebitAmttJV + incomeMjrdebitAmt1JV1;
														}
													}
												}
												 
												 incomeMnrcreditAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead31(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MNH.gethead_account_id(),"sub_head_id",subhd1));
// 												 if(incomeMjrcreditAmtList != null && incomeMjrcreditAmtList.size()> 0){
// 														for(int j = 0 ; j < incomeMjrcreditAmtList.size(); j++){
// 															if(incomeMjrcreditAmtList.get(j)[0].toString().trim().equals(MNH.getminorhead_id())){
// 																incomeMnrcreditAmt = incomeMnrcreditAmt + Double.parseDouble(incomeMjrcreditAmtList.get(j)[2].toString());
// // 																incomeMjrcreditAmtt = incomeMjrcreditAmtt + incomeMjrcreditAmtt1;
// 															}
// 														}
// 													}
												 
												 
// 												 incomeMnrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSum1(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MNH.gethead_account_id(),"sub_head_id",subhd1));
												 
												 List<String[]> incomeMnrcreditAmt1List = CP_L.getLedgerSum11(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MNH.gethead_account_id(),"sub_head_id",subhd1);
												 
												 if(incomeMnrcreditAmt1List != null && incomeMnrcreditAmt1List.size()> 0){
														for(int j = 0 ; j < incomeMnrcreditAmt1List.size(); j++){
															incomeMnrcreditAmt1 = incomeMnrcreditAmt1 + Double.parseDouble(incomeMnrcreditAmt1List.get(j)[2].toString());
													}
												}
												 
// 												 incomeMnrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd1));
												 
												 if(incomeMjrcreditAmtJVList != null && incomeMjrcreditAmtJVList.size()> 0){
														for(int j = 0 ; j < incomeMjrcreditAmtJVList.size(); j++){
															if(incomeMjrcreditAmtJVList.get(j)[0].toString().trim().equals(MNH.getminorhead_id())){
																incomeMnrcreditAmtJV = incomeMnrcreditAmtJV + Double.parseDouble(incomeMjrcreditAmtJVList.get(j)[2].toString());
// 																incomeMjrcreditAmttJV = incomeMjrcreditAmttJV + incomeMjrcreditAmttJV1;
															}
														}
													}
												 
// 												 incomeMnrcreditAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd1));
												 if(incomeMjrcreditAmtDollarList != null && incomeMjrcreditAmtDollarList.size()> 0){
														for(int j = 0 ; j < incomeMjrcreditAmtDollarList.size(); j++){
															if(incomeMjrcreditAmtDollarList.get(j)[0].toString().trim().equals(MNH.getminorhead_id())){	
																incomeMjrcreditAmtDollar = incomeMjrcreditAmtDollar + Double.parseDouble(incomeMjrcreditAmtDollarList.get(j)[2].toString());
															}
														}
													}
												 
												 incomeMnrcreditAmtOfferKind = Double.parseDouble(CP_L.getLedgerSumOfSubheads(MNH.getminorhead_id(),"extra1",fdate,tdate,"offerKind",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
												 
												 
												 
													
												 mindbamt =  incomeMnrdebitAmt + incomeMnrdebitAmtJV;
											  	 mincdamt = incomeMnrcreditAmt + incomeMnrcreditAmt1+ incomeMnrcreditAmtJV + incomeMnrcreditAmtDollar;
													
// 											  	List otherAmountList1=BNK_L.getStringOtherDeposit1("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),"",hoaid,fromDate,toDate,"sub_head_id",subhd1);
													
											  	
													if(otherAmountList != null && otherAmountList.size() > 0){
															for(int q = 0 ; q < otherAmountList.size(); q++){
																banktransactions banktransactions = otherAmountList.get(q);
																if(MNH.getminorhead_id().equals(banktransactions.getMinor_head_id())){
																	otherAmount1 = otherAmount1 + Double.parseDouble(banktransactions.getamount());
																}
														}
													}
											  	
											  	
// 												if(otherAmountList.size()>0)
// 												{
// 													banktransactions OtherAmountList1 = (banktransactions) otherAmountList.get(0);
// 													if(OtherAmountList1.getamount() != null)
// 													{
// 														otherAmount1 = Double.parseDouble(OtherAmountList1.getamount());
// 													}
// 												}
												/* end */
											  	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
											  		
											  		if(totalAmountOpeningBal != null && totalAmountOpeningBal.size() > 0){
														for(int j = 0 ; j < totalAmountOpeningBal.size(); j++){
															bankbalance bankbalance = totalAmountOpeningBal.get(j);
															if(bankbalance != null && bankbalance.getType().trim().equals("Assets") && bankbalance.getExtra7().trim().equals("debit") && bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())){
																debitAmountOpeningBal2 = debitAmountOpeningBal2 + Double.parseDouble(bankbalance.getBank_bal());
															}
															if(bankbalance != null && bankbalance.getType().trim().equals("Liabilites") && bankbalance.getExtra7().trim().equals("credit") && bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())){
																creditAmountOpeningBal2 = creditAmountOpeningBal2 + Double.parseDouble(bankbalance.getBank_bal());
															}
														}
													}	
											  		
											  	
// 												  debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "","extra5",subhd1);
												 /*  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", "","extra5",subhd1); */
// 												  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "","extra5",subhd1);
												
											  	}
											  	if((!("0.0".equals(""+mindbamt)) || !("0.0".equals(""+mincdamt)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0 || otherAmount1 != 0.0) && !MNH.getminorhead_id().equals(corpusMinorHead)){  
													%>
													
													<tr>
													    <td height="28" style="color:#f00;">
													    	<strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong>
													    </td>
													   <td align="center" style="color:#f00;">
													   		<%=df.format(debitAmountOpeningBal2+incomeMnrdebitAmt + incomeMnrdebitAmtJV) %>
													   	</td> 
													    	<%debitincome=debitincome+debitAmountOpeningBal2+incomeMnrdebitAmt + incomeMnrdebitAmtJV;  
													    if(!String.valueOf(incomeMnrcreditAmt1).equals("0.0")){ %>
														<td align="center" style="color:#f00;">
															<%=df.format(creditAmountOpeningBal2+incomeMnrcreditAmtOfferKind+otherAmount1)%>
														</td>
													  <%} else{ %>
													  	<td align="center" style="color:#f00;">
													  		<%=df.format(creditAmountOpeningBal2+incomeMnrcreditAmt+incomeMnrcreditAmt1 +incomeMnrcreditAmtJV  +incomeMnrcreditAmtDollar + otherAmount1) %>
													  	</td>
													  <%
													  } %>	
													  </tr>
													  <%
													  
													  List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
													  
													  String subhd="";
												         if(subhdlist.size()>0){ 
												        	 
												        	 for(int g = 0 ; g < subhdlist.size() ; g++){
																	SUBH = (subhead) subhdlist.get(g);
																	 subhd=SUBH_L.getSubheadListMinorhead1(MNH.getminorhead_id(),SUBH.getsub_head_id(),"IE");;
																	 
																	 if(SUBH.getsub_head_id().equals("20201")){
																			cashtype="creditsale";
																	 }else if(SUBH.getsub_head_id().equals("20206")){
																			cashtype="creditsale";
																	}
																	 
																	if(SUBH.getsub_head_id().equals("20020") || SUBH.getsub_head_id().equals("20022") || SUBH.getsub_head_id().equals("20023") || SUBH.getsub_head_id().equals("20025") || SUBH.getsub_head_id().equals("20024")){
																			
																	}else{
																		
																		double incomeSubdebitAmt = 0.0;
																		double incomeSubdebitAmtJV = 0.0;
																		double incomeSubcreditAmtJV = 0.0;
																		double incomeSubcreditAmtDollar = 0.0;
																		
																		double incomeSubcreditAmt1 = 0.0;
																		
// 																		double incomeSubdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum21(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd));
																		
																		
																		 if(incomeMjrdebitAmtList != null && incomeMjrdebitAmtList.size()> 0){
																				for(int j = 0 ; j < incomeMjrdebitAmtList.size(); j++){
																					if(incomeMjrdebitAmtList.get(j)[0].toString().trim().equals(MNH.getminorhead_id()) && incomeMjrdebitAmtList.get(j)[1].toString().trim().equals(SUBH.getsub_head_id())){
																						incomeSubdebitAmt = incomeSubdebitAmt + Double.parseDouble(incomeMjrdebitAmtList.get(j)[2].toString());
																					}
																				}
																			}
																		
// 																		double incomeSubdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd));
																		
																		if(incomeMjrdebitAmtJVList != null && incomeMjrdebitAmtJVList.size()> 0){
																			for(int j = 0 ; j < incomeMjrdebitAmtJVList.size(); j++){
																				if(incomeMjrdebitAmtJVList.get(j)[0].toString().trim().equals(MNH.getminorhead_id()) && incomeMjrdebitAmtList.get(j)[1].toString().trim().equals(SUBH.getsub_head_id())){
																					incomeSubdebitAmtJV = incomeSubdebitAmtJV + Double.parseDouble(incomeMjrdebitAmtJVList.get(j)[2].toString());
																				}
																			}
																		}
																		
																		
																		double incomeSubcreditAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
// 																		double incomeSubcreditAmt1 = Double.parseDouble(CP_L.getLedgerSum1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
																		
																		
																		 if(incomeMnrcreditAmt1List != null && incomeMnrcreditAmt1List.size()> 0){
																				for(int j = 0 ; j < incomeMnrcreditAmt1List.size(); j++){
																					if(incomeMnrcreditAmt1List.get(j)[1].toString().equals(SUBH.getsub_head_id())){
																						incomeSubcreditAmt1 = incomeSubcreditAmt1 + Double.parseDouble(incomeMnrcreditAmt1List.get(j)[2].toString());
																					}
																			}
																		}
																		
// 																		double incomeSubcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
																		
																		
																		
																		if(incomeMjrcreditAmtJVList != null && incomeMjrcreditAmtJVList.size()> 0){
																			for(int j = 0 ; j < incomeMjrcreditAmtJVList.size(); j++){
																				if(incomeMjrcreditAmtJVList.get(j)[0].toString().trim().equals(MNH.getminorhead_id()) && incomeMjrdebitAmtList.get(j)[1].toString().trim().equals(SUBH.getsub_head_id())){
																					incomeSubcreditAmtJV = incomeSubcreditAmtJV + Double.parseDouble(incomeMjrcreditAmtJVList.get(j)[2].toString());
//					 																incomeMjrcreditAmttJV = incomeMjrcreditAmttJV + incomeMjrcreditAmttJV1;
																				}
																			}
																		}
																		
// 																		double incomeSubcreditAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd));
																		
																		
																		 if(incomeMjrcreditAmtDollarList != null && incomeMjrcreditAmtDollarList.size()> 0){
																				for(int j = 0 ; j < incomeMjrcreditAmtDollarList.size(); j++){
																					if(incomeMjrcreditAmtDollarList.get(j)[0].toString().trim().equals(MNH.getminorhead_id()) && incomeMjrdebitAmtList.get(j)[1].toString().trim().equals(SUBH.getsub_head_id())){	
																						incomeSubcreditAmtDollar = incomeSubcreditAmtDollar + Double.parseDouble(incomeMjrcreditAmtDollarList.get(j)[2].toString());
																					}
																				}
																			}
																		
																		  double subdbamt = incomeSubdebitAmt +incomeSubdebitAmtJV ;  
																		//double subdbamt = incomeSubdebitAmt  ;
																		double subcdamt = incomeSubcreditAmt+incomeSubcreditAmt1 + incomeSubcreditAmtJV  + incomeSubcreditAmtDollar;
																		double debitAmountOpeningBal3 = 0.0;
																		double creditAmountOpeningBal3 = 0.0;
																		
// 																		List otherAmountList2=BNK_L.getStringOtherDeposit1("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(),hoaid,fromDate,toDate,"sub_head_id",subhd);
																		


																		
																		double otherAmount2 = 0.0;
																		
																		if(otherAmountList != null && otherAmountList.size() > 0){
																			for(int q = 0 ; q < otherAmountList.size(); q++){
																				banktransactions banktransactions = otherAmountList.get(q);
																				if(MNH.getminorhead_id().equals(banktransactions.getMinor_head_id()) && SUBH.getsub_head_id().equals(banktransactions.getSub_head_id())){
																					otherAmount2 = otherAmount2 + Double.parseDouble(banktransactions.getamount());
																				}
																			}
																		}
																		
																		
// 																		if(otherAmountList2.size()>0)
// 																		{
// 																			banktransactions OtherAmountList2 = (banktransactions)otherAmountList2.get(0);
// 																			if(OtherAmountList2.getamount() != null)
// 																			{
// 																				otherAmount2 = Double.parseDouble(OtherAmountList2.getamount());
// 																			}
// 																		}
																		
																		if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
																			
																		
																			if(totalAmountOpeningBal != null && totalAmountOpeningBal.size() > 0){
																				for(int j = 0 ; j < totalAmountOpeningBal.size(); j++){
																					bankbalance bankbalance = totalAmountOpeningBal.get(j);
																					if(bankbalance != null && bankbalance.getType().trim().equals("Assets") && bankbalance.getExtra7().trim().equals("debit") && bankbalance.getExtra4().trim().equals(MNH.getminorhead_id()) && bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id()) ){
																						debitAmountOpeningBal3 = debitAmountOpeningBal3 + Double.parseDouble(bankbalance.getBank_bal());
																					}
																					if(bankbalance != null && bankbalance.getType().trim().equals("Liabilites") && bankbalance.getExtra7().trim().equals("credit") && bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())  && bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id()) ){
																						creditAmountOpeningBal3 = creditAmountOpeningBal3 + Double.parseDouble(bankbalance.getBank_bal());
																					}
																				}
																			}	
																			
																		
// 																			debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "","extra5",subhd);
																			/* creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", "","extra5",subhd); */
// 																			creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "","extra5",subhd);
																		}
																		
																		if(!("0.0".equals(""+subdbamt)) || !("0.0".equals(""+subcdamt)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0 || otherAmount2 != 0.0){  
																			
																			%>	
																			  <tr>
																			  <%if(!String.valueOf(incomeSubcreditAmt1).equals("0.0")){ %>
																			    <td height="28"><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></td>
																			    <%} else{ %> 
																			    <td height="28"><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></td>
																			     <%} 
																			     	debit=debit+debitAmountOpeningBal3+incomeSubdebitAmt +incomeSubdebitAmtJV ;  
																			     %>
																			    <td><%=df.format(debitAmountOpeningBal3+incomeSubdebitAmt +incomeSubdebitAmtJV ) %></td>  
																			      <%
																			     	if(!String.valueOf(incomeSubcreditAmt1).equals("0.0")){ 
																			    	 credit=credit+creditAmountOpeningBal3+incomeSubcreditAmt+otherAmount2;
																			    	%>
																			    <td><%=df.format(creditAmountOpeningBal3+incomeSubcreditAmt+otherAmount2) %></td>
																			    <%} else{ 
																			    	credit=credit+creditAmountOpeningBal3+incomeSubcreditAmt+incomeSubcreditAmt1 +incomeSubcreditAmtJV + incomeSubcreditAmtDollar+otherAmount2;
																			     %>
																			    <td><%=df.format(creditAmountOpeningBal3+incomeSubcreditAmt+incomeSubcreditAmt1 +incomeSubcreditAmtJV +incomeSubcreditAmtDollar +otherAmount2) %></td>
																			  	<%} %>
																			  </tr>
																			  <%
																		}
																	}
												        	 }
												        	 
												         }
											  	}
											}
										}
										
									}
									
								}
					}	
  				}
  			}
  %>
  
  
  
 <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">EXPENDITURE</td>
  	</tr>
    
    
    <% if(expendmajhdlist.size() > 0){ 
    	
    	minorhead MNH= new minorhead();
    	
    	for(int i = 0 ; i < expendmajhdlist.size(); i++){
			MH = (majorhead) expendmajhdlist.get(i);
			List subh2 = SUBH_L.getSubheadListMinorhead3(MH.getmajor_head_id(),"IE");
			String subhd2[] = new String[subh2.size()];
			
			for(int n = 0 ; n < subh2.size() ; n++){
				SUBH = (subhead) subh2.get(n);
				subhd2[n] = SUBH.getsub_head_id();
			}
			
			double debitAmountOpeningBal = 0.0;
			double creditAmountOpeningBal = 0.0;
			double expMjrdebitAmt=0.0;
			
			List<bankbalance> totalAmountOpeningBal = null;
			
			if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
				
				totalAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal11(hoaid, MH.getmajor_head_id(), finYr,subhd2);
				if(totalAmountOpeningBal != null && totalAmountOpeningBal.size() > 0){
					for(int j = 0 ; j < totalAmountOpeningBal.size(); j++){
						bankbalance bankbalance = totalAmountOpeningBal.get(j);
						if(bankbalance != null && bankbalance.getType().trim().equals("Assets") && bankbalance.getExtra7().trim().equals("debit")){
							debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
						}
						if(bankbalance != null && bankbalance.getType().trim().equals("Liabilites") && bankbalance.getExtra7().trim().equals("credit")){
							creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
						}
					}
				}	
// 				debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "","extra5",subhd2);
// 				creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Liabilites", finYr, "credit", "","extra5",subhd2);
			}
			
			
// 			if(MH.getmajor_head_id().equals("31")){
// 				expMjrdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum01(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2));	
// 			}else{
// 				expMjrdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2));
// 			}
// 			double expMjrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2));
// 			double expMjrcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd2));
// 			double expMjrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead1b(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,hoaid,"sub_head_id",subhd2));
// 			double expMjrcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead111(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype3,"","",hoaid,"sub_head_id",subhd2));
// 			double expMjrcreditAmt3	= Double.parseDouble(CP_L.getLedgerSumOfSubheads(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd2));
// 			double expMjrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2));
			
			
			List<String[]> expMjrdebitAmtList = PRDEXP_L.getLedgerSum11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2);
			List<String[]> expMjrdebitAmtJVList = CP_L.getLedgerSumBasedOnJV11(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2);
			List<String[]> expMjrcreditAmtList = CP_L.getLedgerSum1a1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd2);
			List<String[]> expMjrcreditAmt1List = CP_L.getLedgerSumOfSubhead1b1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,hoaid,"sub_head_id",subhd2);
			List<String[]> expMjrcreditAmt2List = CP_L.getLedgerSumOfSubhead1111(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd2);
			List<String[]> expMjrcreditAmt3List = CP_L.getLedgerSumOfSubheads1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd2);
			List<String[]> expMjrcreditAmtJVList = PRDEXP_L.getLedgerSumBasedOnJV11(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2);
		
			
			double expMjrdebitAmtJV = 0.0;
			double expMjrcreditAmt = 0.0;
			double expMjrcreditAmt1 = 0.0;
			double expMjrcreditAmt2 = 0.0;
			double expMjrcreditAmt3	= 0.0;
			double expMjrcreditAmtJV = 0.0;


			if(expMjrdebitAmtList != null && expMjrdebitAmtList.size() > 0){
				for(int j = 0 ; j < expMjrdebitAmtList.size(); j++){
					if(!expMjrdebitAmtList.get(j)[0].equals("281")){
						expMjrdebitAmt = expMjrdebitAmt + Double.parseDouble(expMjrdebitAmtList.get(j)[2].toString());
					}
				}
			}

			if(expMjrdebitAmtJVList != null && expMjrdebitAmtJVList.size()> 0){
				for(int j = 0 ; j < expMjrdebitAmtJVList.size(); j++){
					expMjrdebitAmtJV = expMjrdebitAmtJV + Double.parseDouble(expMjrdebitAmtJVList.get(j)[2].toString());
				}
			}
			
			if(expMjrcreditAmtList != null && expMjrcreditAmtList.size()> 0){
				for(int j = 0 ; j < expMjrcreditAmtList.size(); j++){
					expMjrcreditAmt = expMjrcreditAmt + Double.parseDouble(expMjrcreditAmtList.get(j)[2].toString());
				}
			}
			
			if(expMjrcreditAmt1List != null && expMjrcreditAmt1List.size()> 0){
				for(int j = 0 ; j < expMjrcreditAmt1List.size(); j++){
					expMjrcreditAmt1 = expMjrcreditAmt1 + Double.parseDouble(expMjrcreditAmt1List.get(j)[2].toString());
				}
			}
			
			if(expMjrcreditAmt2List != null && expMjrcreditAmt2List.size()> 0){
				for(int j = 0 ; j < expMjrcreditAmt2List.size(); j++){
					expMjrcreditAmt2 = expMjrcreditAmt2 + Double.parseDouble(expMjrcreditAmt2List.get(j)[2].toString());
				}
			}
			
			if(expMjrcreditAmt3List != null && expMjrcreditAmt3List.size()> 0){
				for(int j = 0 ; j < expMjrcreditAmt3List.size(); j++){
					expMjrcreditAmt3 = expMjrcreditAmt3 + Double.parseDouble(expMjrcreditAmt3List.get(j)[2].toString());
				}
			}
			
			if(expMjrcreditAmtJVList != null && expMjrcreditAmtJVList.size()> 0){
				for(int j = 0 ; j < expMjrcreditAmtJVList.size(); j++){
					expMjrcreditAmtJV = expMjrcreditAmtJV + Double.parseDouble(expMjrcreditAmtJVList.get(j)[2].toString());
				}
			}
			
			
			
			if(!String.valueOf(expMjrdebitAmt).equals("0.0") || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){
				
				%>
				
				
				<tr>
<td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getmajor_head_id()%> <%=MH.getname() %></a></td>
<td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+expMjrdebitAmt +expMjrdebitAmtJV ) %></td>
<%debitexpens=debitexpens+debitAmountOpeningBal+expMjrdebitAmt + expMjrdebitAmtJV;%>
<%if(!String.valueOf(expMjrcreditAmt).equals("0.0")){ %>
<td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+expMjrcreditAmt+expMjrcreditAmt1) %></td>
<%creditexpens=creditexpens+creditAmountOpeningBal+expMjrcreditAmt; } else{ %>
<td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+expMjrcreditAmt2 +expMjrcreditAmtJV )%></td>
<%creditexpens=creditexpens+creditAmountOpeningBal+expMjrcreditAmt3 +expMjrcreditAmtJV ; 
} %>
</tr>

<%	
		List minhdlist = MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
				

if(minhdlist.size()>0){  %>
<%

for(int j=0;j<minhdlist.size();j++){
	MNH=(beans.minorhead)minhdlist.get(j);
	List MinHd1=MINH_L.getMinorHeadBasdOnCompany1(MH.getmajor_head_id(),MNH.getminorhead_id(),"IE");
	String MinHead1[]=new String[MinHd1.size()];
	int p=0;
	
	for(p=0;p<MinHd1.size();p++){
		MNH=(beans.minorhead)MinHd1.get(p);
		
		MinHead1[p]=" ";
		String minorvalue='"'+MNH.getminorhead_id()+'"';
		
		if(!MNH.getminorhead_id().equals(minorvalue)){
			List subh1=SUBH_L.getSubheadListMinorhead2(MNH.getminorhead_id(),"IE");
			String subhd1[]=new String[subh1.size()];
			int m=0;
			
			for(m=0;m<subh1.size();m++){
				SUBH=(subhead)subh1.get(m);
				subhd1[m]=SUBH.getsub_head_id();
				
				}
			double expMnrdebitAmt = 0.0;
			double expMnrdebitAmtJV = 0.0;
			
			double expMnrdebitAmt2 = 0.0;
			double expMnrcreditAmt = 0.0;
			double expMnrcreditAmt1 = 0.0;
			double expMnrcreditAmtJV = 0.0;
			

// 			double expMnrdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum1(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd1));
// 			double expMnrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd1));
// 			double expMnrdebitAmt2 = Double.parseDouble(CP_L.getLedgerSum1(MNH.getminorhead_id(),"extra3",fdate,tdate,"",hoaid,"sub_head_id",subhd1));
// 			double expMnrcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
// 			double expMnrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubheads(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
// 			double expMnrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd1));
			
			
			double expMnrdebitAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead41(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd1));
			double expMnrcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead31(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,"","",cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
			double expMnrdebitAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd1));
			
			if(expMjrdebitAmtList != null && expMjrdebitAmtList.size() > 0){
				for(int a = 0 ; a < expMjrdebitAmtList.size(); a++){
					if(!expMjrdebitAmtList.get(a)[0].equals("281") && expMjrdebitAmtList.get(a)[0].equals(MNH.getminorhead_id())){
						expMnrdebitAmt = expMnrdebitAmt + Double.parseDouble(expMjrdebitAmtList.get(a)[2].toString());
					}
				}
			}
			
			if(expMjrdebitAmtJVList != null && expMjrdebitAmtJVList.size()> 0){
				for(int a = 0 ; a < expMjrdebitAmtJVList.size(); a++){
					if(expMjrdebitAmtJVList.get(a)[0].equals(MNH.getminorhead_id())){	
						expMnrdebitAmtJV = expMnrdebitAmtJV + Double.parseDouble(expMjrdebitAmtJVList.get(a)[2].toString());
					}
				}
			}
			
			List<String[]> expMnrdebitAmt2List = CP_L.getLedgerSum11(MNH.getminorhead_id(),"extra3",fdate,tdate,"",hoaid,"sub_head_id",subhd1);
			
			if(expMnrdebitAmt2List != null && expMnrdebitAmt2List.size() > 0){
				for(int a = 0 ; a < expMnrdebitAmt2List.size(); a++){
					expMnrdebitAmt2 = expMnrdebitAmt2 + Double.parseDouble(expMnrdebitAmt2List.get(a)[2].toString());
				}
			}
			
			if(expMjrcreditAmtList != null && expMjrcreditAmtList.size()> 0){
				for(int a = 0 ; a < expMjrcreditAmtList.size(); a++){
					if(expMjrcreditAmtList.get(a)[0].equals(MNH.getminorhead_id())){	
						expMnrcreditAmt = expMnrcreditAmt + Double.parseDouble(expMjrcreditAmtList.get(a)[2].toString());
					}
				}
			}
			
			if(expMjrcreditAmt3List != null && expMjrcreditAmt3List.size()> 0){
				for(int a = 0 ; a < expMjrcreditAmt3List.size(); a++){
					if(expMjrcreditAmt3List.get(a)[0].equals(MNH.getminorhead_id())){		
						expMnrcreditAmt1 = expMnrcreditAmt1 + Double.parseDouble(expMjrcreditAmt3List.get(a)[2].toString());
					}
				}
			}
			
			if(expMjrcreditAmtJVList != null && expMjrcreditAmtJVList.size()> 0){
				for(int a = 0 ; a < expMjrcreditAmtJVList.size(); a++){
					if(expMjrcreditAmtJVList.get(a)[0].equals(MNH.getminorhead_id())){	
						expMjrcreditAmtJV = expMjrcreditAmtJV + Double.parseDouble(expMjrcreditAmtJVList.get(a)[2].toString());
					}
				}
			}
			
			String amountexp=String.valueOf(expMnrdebitAmt);
			double amount2 = expMnrdebitAmt1+expMnrdebitAmt2 + expMnrcreditAmtJV +expMnrdebitAmtDollar ; 
			double debitAmountOpeningBal2 = 0.0;
			double creditAmountOpeningBal2 = 0.0;
			
			if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
// 				debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "","extra5",subhd1);
// 				creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "","extra5",subhd1);
				
				if(totalAmountOpeningBal != null && totalAmountOpeningBal.size() > 0){
					for(int a = 0 ; a < totalAmountOpeningBal.size(); a++){
						bankbalance bankbalance = totalAmountOpeningBal.get(a);
						if(bankbalance != null && bankbalance.getType().trim().equals("Assets") && bankbalance.getExtra7().trim().equals("debit") && bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())){
							debitAmountOpeningBal2 = debitAmountOpeningBal2 + Double.parseDouble(bankbalance.getBank_bal());
						}
						if(bankbalance != null && bankbalance.getType().trim().equals("Liabilites") && bankbalance.getExtra7().trim().equals("credit") && bankbalance.getExtra4().trim().equals(MNH.getminorhead_id())){
							creditAmountOpeningBal2 = creditAmountOpeningBal2 + Double.parseDouble(bankbalance.getBank_bal());
						}
					}
				}
				
			}
			
			if(!("0.0".equals(""+amountexp)) || !("0.0".equals(""+amount2)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){
				%>
<tr>
<td height="28" style="color:#f00;"><strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong></td>
<td align="center" style="color:#f00;"><%=df.format(debitAmountOpeningBal2+expMnrdebitAmt +expMnrdebitAmtJV ) %></td>
<%debit=debit+debitAmountOpeningBal2+expMnrdebitAmt +expMnrdebitAmtJV ;
if(!String.valueOf(expMnrcreditAmt).equals("0.0")){ %>
<td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+expMnrcreditAmt1+expMnrcreditAmt) %></td>
<%} else{ %>
<td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+expMnrcreditAmt2+expMnrcreditAmt +expMnrcreditAmtJV) %></td>
<%} %>
</tr>
<%

List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
	String subhd="";
if(subhdlist.size()>0){ 
	
	for(int k=0;k<subhdlist.size();k++){
		SUBH=(subhead)subhdlist.get(k);
		subhd=SUBH_L.getSubheadListMinorhead1(MNH.getminorhead_id(),SUBH.getsub_head_id(),"IE");;
		if(SUBH.getsub_head_id().equals("20201"))
			cashtype="creditsale";
		else if(SUBH.getsub_head_id().equals("20206"))
			cashtype="creditsale";
		else
			//cashtype="cash";
			 cashtype1="othercash";
			 cashtype2="offerKind";
			cashtype3="journalvoucher";
			
			
			double expSubdebitAmt = 0.0;
			double expSubdebitAmt1 = 0.0;
			double expSubdebitAmtJV = 0.0;
			double expSubcreditAmtJV = 0.0;
			double expSubcreditAmt = 0.0;
			
// 			double expSubdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum21(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd));
			
			
			if(expMjrdebitAmtList != null && expMjrdebitAmtList.size() > 0){
				for(int a = 0 ; a < expMjrdebitAmtList.size(); a++){
					if(!expMjrdebitAmtList.get(a)[0].equals("281") && expMjrdebitAmtList.get(a)[0].equals(MNH.getminorhead_id()) && expMjrdebitAmtList.get(a)[1].equals(SUBH.getsub_head_id())){
						expSubdebitAmt = expSubdebitAmt + Double.parseDouble(expMjrdebitAmtList.get(a)[2].toString());
					}
				}
			}
			
// 			double expSubdebitAmt1 = Double.parseDouble(CP_L.getLedgerSum1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd));
			
			if(expMnrdebitAmt2List != null && expMnrdebitAmt2List.size() > 0){
				for(int a = 0 ; a < expMnrdebitAmt2List.size(); a++){
					if(expMnrdebitAmt2List.get(a)[0].equals(MNH.getminorhead_id()) && expMnrdebitAmt2List.get(a)[1].equals(SUBH.getsub_head_id()))
					expSubdebitAmt1 = expSubdebitAmt1 + Double.parseDouble(expMnrdebitAmt2List.get(a)[2].toString());
				}
			}
			
// 			double expSubdebitAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd));
			double expSubcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd));
// 			double expSubcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,"","","","",hoaid,"sub_head_id",subhd));
// 			double expSubdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd));
			
			if(expMjrdebitAmtJVList != null && expMjrdebitAmtJVList.size()> 0){
				for(int a = 0 ; a < expMjrdebitAmtJVList.size(); a++){
					if(expMjrdebitAmtJVList.get(a)[0].equals(MNH.getminorhead_id()) && expMjrdebitAmtJVList.get(a)[1].equals(SUBH.getsub_head_id())){	
						expSubdebitAmtJV = expSubdebitAmtJV + Double.parseDouble(expMjrdebitAmtJVList.get(a)[2].toString());
					}
				}
			}
			
// 			double expSubcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
			
			
			if(expMjrcreditAmtJVList != null && expMjrcreditAmtJVList.size()> 0){
				for(int a = 0 ; a < expMjrcreditAmtJVList.size(); a++){
					if(expMjrcreditAmtJVList.get(a)[0].equals(MNH.getminorhead_id()) && expMjrcreditAmtJVList.get(a)[1].equals(SUBH.getsub_head_id())){	
						expSubcreditAmtJV = expSubcreditAmtJV + Double.parseDouble(expMjrcreditAmtJVList.get(a)[2].toString());
					}
				}
			}
			
// 			double expSubcreditAmtJV1 =	Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
// 			double expSubcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
			

			if(expMjrcreditAmtList != null && expMjrcreditAmtList.size()> 0){
				for(int a = 0 ; a < expMjrcreditAmtList.size(); a++){
					if(expMjrcreditAmtList.get(a)[0].equals(MNH.getminorhead_id()) && expMjrcreditAmtList.get(a)[1].equals(SUBH.getsub_head_id())){	
						expSubcreditAmt = expSubcreditAmt + Double.parseDouble(expMjrcreditAmtList.get(a)[2].toString());
					}
				}
			}
			
			String amountexp2=String.valueOf(expSubdebitAmt);
			String amount3 = String.valueOf(expSubdebitAmt1);
			String amount4 = String.valueOf(expSubcreditAmt1);
			String amount5= String.valueOf(expSubcreditAmtJV) ;
			double debitAmountOpeningBal3 = 0.0;
			double creditAmountOpeningBal3 = 0.0;
			if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
// 				debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "","extra5",subhd);
// 				creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal1(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "","extra5",subhd);
				
				if(totalAmountOpeningBal != null && totalAmountOpeningBal.size() > 0){
					for(int a = 0 ; a < totalAmountOpeningBal.size(); a++){
						bankbalance bankbalance = totalAmountOpeningBal.get(a);
						if(bankbalance != null && bankbalance.getType().trim().equals("Assets") && bankbalance.getExtra7().trim().equals("debit") && bankbalance.getExtra4().trim().equals(MNH.getminorhead_id()) && bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id())){
							debitAmountOpeningBal3 = debitAmountOpeningBal3 + Double.parseDouble(bankbalance.getBank_bal());
						}
						if(bankbalance != null && bankbalance.getType().trim().equals("Liabilites") && bankbalance.getExtra7().trim().equals("credit") && bankbalance.getExtra4().trim().equals(MNH.getminorhead_id()) && bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id())){
							creditAmountOpeningBal3 = creditAmountOpeningBal3 + Double.parseDouble(bankbalance.getBank_bal());
						}
					}
				}
			}
			 if(!("0.0".equals(""+amountexp2)) || !("0.0".equals(""+amount3)) || !("0.0".equals(""+amount4)) || !("0.0".equals(""+amount5)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){  
					%>	



<tr>
<%
if(!String.valueOf(expSubcreditAmt).equals("0.0")){ %>
<td height="28"><strong><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></strong></td>
<%} else{ %>
<td height="28"><strong><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></strong></td>
<%} %>
<td><%=df.format(debitAmountOpeningBal3+expSubdebitAmt +expSubdebitAmtJV ) %></td>

<%debit=debit+debitAmountOpeningBal3+expSubdebitAmt +expSubdebitAmtJV ;
if(!String.valueOf(expSubcreditAmt).equals("0.0")){ %>
<td><%=df.format(creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt) %></td>
<%credit=credit+creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt;} else{ %>
<td><%=df.format(creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt+expSubcreditAmtJV) %></td>
<%credit=credit+creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt +expSubcreditAmtJV;} 
%>
</tr>

<%}
			
			
	}
}
			}
		}
		
		
	}
	
}

}

				}
			
    	}
    	
    	
    	
    	}
    %>

  <tr>
    <td align="center" style=" color: #934C1E; font-weight:bold;" height="28">TOTAL AMOUNT OF EXPENS </td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">Rs.<%=df.format(debitexpens) %></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
  </tr>

   <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Excess of Income over Expenditures</td>
   <%
    if(creditincome<debitexpens){ %>
    <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitexpens-creditincome)%></td>
    <%	/* creditincome=debitexpens; */
} else{	%>
<td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome-debitexpens)%></td>
<td align="center" style="font-weight:bold; color: #934C1E;">-</td>
<%/* debitexpens=creditincome; */%>
<%} %>
  </tr>

  
  
   <tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
   <%if(creditincome>debitexpens){	%>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome) %></td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome) %></td>
    <%} else{%>
     <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitexpens) %></td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitexpens) %></td><%} %>
  </tr>

  
</tbody>
</table>
</div>

<%
 }
//  }else{
// 	response.sendRedirect("index.jsp");
// }
%>
</body>
</html>