<%@page import="mainClasses.banktransactionsListing2"%>
<jsp:useBean id="BNK" class="beans.banktransactionsService"/>
<jsp:useBean id="BNK2" class="beans.banktransactionsService2"/>

<jsp:useBean id="GWS" class="beans.godwanstockService"/>
<jsp:useBean id="PES" class="beans.productexpensesService"/>
<%
mainClasses.banktransactionsListing2 bnkl = new mainClasses.banktransactionsListing2();
int transSize=0;
if(request.getParameter("size")!=null){
transSize=Integer.parseInt(request.getParameter("size"));
}
String reconDate="";
	String transID="";
	String fromdate=request.getParameter("fromdate");
	String todate=request.getParameter("todate");
	String reportType=request.getParameter("reportType");
	String editedName=session.getAttribute("adminId").toString();
if(transSize>0){
	for(int i=0;i<transSize;i++){
		if(request.getParameter("date"+i)!=null && !request.getParameter("date"+i).equals("")){
			reconDate=request.getParameter("date"+i)+"";
			transID=request.getParameter("bankTransId"+i);%>
			<jsp:setProperty name="BNK" property="banktrn_id" value="<%=transID%>"/>
			<jsp:setProperty name="BNK" property="editedBy" value="<%=editedName%>"/>
			<jsp:setProperty name="BNK" property="brs_date" value="<%=reconDate%>"/>
			<jsp:setProperty name="BNK" property="name" value="reconciliation"/>
			<%BNK.update(); %><%=BNK.geterror() %>
			<%
				GWS.updateBRSDateBasedOnBktId(reconDate, transID);
				PES.updateBrsDateBasedOnBktId(reconDate, transID);
			%>
		<%}
	}
	
	for(int i=0;i<transSize;i++){
		if(request.getParameter("date"+i)!=null && !request.getParameter("date"+i).equals("")){
			reconDate=request.getParameter("date"+i)+"";
			transID=request.getParameter("bankTransId"+i);%>
			<jsp:setProperty name="BNK2" property="banktrn_id" value="<%=transID%>"/>
			<jsp:setProperty name="BNK2" property="editedBy" value="<%=editedName%>"/>
			<jsp:setProperty name="BNK2" property="brs_date" value="<%=reconDate%>"/>
			<jsp:setProperty name="BNK2" property="name" value="reconciliation"/>
			
			<%
				String countInString = bnkl.getCountOfRecords(transID);
				int count = Integer.parseInt(countInString);
				
				if(count == 1)
				{%>
					<%BNK2.update(); %><%=BNK2.geterror() %>
				<%}
				else if(count > 1)
				{
					String date = bnkl.getMaxDateForRecord(transID);
					%>
					<%BNK2.updateDuplicateEntryBasedOnMaxDateAndTransId(transID, date); %><%=BNK2.geterror() %>
				<% 	
				}			
		}
	}
}
response.sendRedirect("adminPannel.jsp?page="+request.getParameter("page")+"&id="+request.getParameter("bankid")+"&fromdate="+fromdate+"&todate="+todate+"&reportType="+reportType);
%>
