<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="beans.products"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ProductOpeningCategory</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<style>
	.productlist{

	}
	.productlist h3{
	  text-align:center;
	}
	.vendor-page {
    background: #FFF;
    width: 100%;
 min-height: 150px; 
    float: left;
    /* border: 1px solid #000; */
}
.submitbtn{
border: 1px solid #65230d;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    padding: 5px 15px 5px 15px;
    text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
    font-weight: bold;
    text-align: center;
    color: #FFF;
    background-color: #ffc579;
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
    background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
    background-image: -moz-linear-gradient(top, #c88b2e, #671811);
    background-image: -ms-linear-gradient(top, #c88b2e, #671811);
    background-image: -o-linear-gradient(top, #c88b2e, #671811);
    background-image: linear-gradient(top, #c88b2e, #671811);
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
    cursor: pointer;
}
	</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('#cat1').change(function() {
			var cat = $(this).val();
			if ($.trim(cat) !== "") {
				$('#subcat1').empty();
				$('#subcat1').append($('<option>', {
					value : "",
					text : 'Select SubCategory'
				}));
				$.ajax({
					url : "searchSubHeadSj.jsp",
					type : "GET",
					data : {
						cat : cat
					},
					success : function(response) {
						var data = response.split(",");
						for (var i = 0; i < data.length; i++) {
							if (data[i].trim() !== "") {
								$('#subcat1').append($('<option>', {
									value : data[i],
									text : data[i].toUpperCase()
								}));
							}
						}
					}
				});
			}
		});
	});
</script>
</head>
<body>
	
	
	<div class="vendor-page">
		<form  action="adminPannel.jsp?page=productopeningcategory"  method="post"
			style="padding-left: 70px; padding-top: 30px;">
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td class="border-nn" colspan="2">Category</td>
						<td class="border-nn" colspan="2">SubCategory</td>
						<td class="border-nn" colspan="2">Select Fin Yr</td>

					</tr>
					<tr>

						<%
							mainClasses.productsListing prodService = new mainClasses.productsListing();
							List catlist = prodService.getProductCategories();
						%>
						<td colspan="2"><select name="cat1" id="cat1">
								<option value="">Select Category</option>
								<%
									for (int i = 0; i < catlist.size(); i++) {
										String cat = (String) catlist.get(i);
								%>
								<option value="<%=cat%>"><%=cat.toUpperCase()%></option>
								<%
									}
								%>
						</select></td>
						<td colspan="2"><select name="subcat1" id="subcat1">
								<option value="">Select SubCategory</option>
						</select></td>
						<td colspan="2"><select name="finacialyear" id="finacialyear">
								<option value="2020-04-01">2020-2021</option>
								<option value="2021-04-01">2021-2022</option>
								<option value="2022-04-01">2022-2023</option>
								<option value="2023-04-01">2023-2024</option>
								<option value="2024-04-01">2024-2025</option>
						</select></td>

						<td><input type="submit" value="Submit" class="click"></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
	<%
    String cat1=request.getParameter("cat1");
   String subcat1=request.getParameter("subcat1");
   String finacialyear=request.getParameter("finacialyear");
   
   if((cat1!=null && !cat1.equals("")) && (subcat1!=null && !subcat1.equals(""))){
%>
	<section class="productlist">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>All Products List</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
					<% 	
                       productsListing productsListing=new productsListing();
                      List<products> productsList=productsListing.getProductsBasedOnCategoryAndSubCategory(cat1,subcat1);
                      String headOfaccount="";
                      if(productsList!=null && productsList.size()>0){
                      for(int i=0;i<productsList.size();i++){
	                          products products=productsList.get(i);	                          
	                           if(products.gethead_account_id().equals("1")){
	                        	   headOfaccount="CHARITY";
	                           }if(products.gethead_account_id().equals("3")){
	                        	   headOfaccount="POOJA STORES";
	                           }if(products.gethead_account_id().equals("4")){
	                        	   headOfaccount="SANSTHAN";
	                           }if(products.gethead_account_id().equals("5")){
	                        	   headOfaccount="SAI NIVAS";
	                           }
                        %> 
					<form action="<%=request.getContextPath()%>/productOpeningBalanceInsert" method="POST">
						<table class="table table-bordered mb-0">
							<thead class="thead-light">
							</thead>
							<tbody>
					
                            <input type="hidden" name="totalCount" value="<%=productsList.size()%>">
                            <input type="hidden" name="headOfAccount<%=i%>" value="<%=products.gethead_account_id()%>">
                            <input type="hidden" name="majorHeadId<%=i%>" value="<%=products.getmajor_head_id()%>">
                            <input type="hidden" name="minorHeadId<%=i%>" value="<%=products.getextra3()%>">
                            <input type="hidden" name="subHeadId<%=i%>" value="<%=products.getsub_head_id()%>">
                            <input type="hidden" name="category" value="<%=cat1%>">
                            <input type="hidden" name="subcategory" value="<%=subcat1%>">
                            <input type="hidden" name="finacialyear" value="<%=finacialyear%>">
								<tr>
									<td><label>S.NO</label>   <span class="form-control"><%=i+1%></span></td>
									<td><label>Product Code</label> <input type="text"
										name="productId<%=i%>" id="productId<%=i%>" value="<%=products.getproductId()%>" class="form-control" readonly="readonly"></td>
									<td><label>Product Name</label> <input type="text"
										name="productName<%=i%>" id="productName<%=i%>" value="<%=products.getproductName()%>"
								      class="form-control" readonly="readonly">
									</td>
									<td><label>Head Of Account</label> <input type="text"
										name="headOfAccount"  value="<%=headOfaccount%>"  class="form-control" readonly="readonly">
									</td>
									<td><label>Date</label> <input type="text"
										 name="finacialyear<%=i%>" id="finacialyear<%=i%>" value="<%=finacialyear%>"
										 class="form-control" readonly="readonly">
									</td>
									<td><label>Quanity</label> <input type="text"
										name="quantity<%=i%>" id="quantity<%=i%>" class="form-control" value="0.0">
									</td>

								</tr>
								<%
                                 }
								%>
								<tr>
									<td colspan="6"><button type="submit"
											class="submitbtn">Submit</button></td>
								</tr>

							</tbody>
						</table>
						</form>
						<%}else{
							%>
							  <h1 style="align:center;color:red">No Products Are Avilable</h1>
							<%
						}%>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%
   }
	%>
</body>
</html>