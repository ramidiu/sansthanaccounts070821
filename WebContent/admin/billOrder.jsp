<%@page import="beans.duesService"%>
<%@page import="mainClasses.dueListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.AdvanceService"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.advance_Listing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
@media print {
	@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin-left: 24mm; /* equals to 0.8inches */ 
}
	.voucher
	{
		width:1000px; height:auto;  padding:40px 20px 40px 20px; border:1px solid #727272; left:55px; position:relative;
	}
	 .voucher table.sri td span { font-size:20px; font-weight:normal;}
	.voucher table td.regd { text-align:center; vertical-align:top; font-size:12px;}
	.voucher table.cash-details{ margin:40px 0 0 0; font-size:20px; border-top:1px solid #727272; border-left:1px solid #727272;}
	.voucher table.cash-details td{padding:10px 0px; border-bottom:1px solid #727272; border-right:1px solid #727272;  padding:10px 20px;}
	.voucher table.cash-details td span {text-decoration:none; padding:0 0 5px 0; font-weight:normal; }
	.voucher table.cash-details td.vide ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide ul li{ list-style:none; float:left; margin:0 45px 0 0; }
	/*.voucher table.cash-details td.vide ul li:first-child{ width:400px;  margin:0 20px 0 0;}*/
	.voucher table.cash-details td.vide1 ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide1 ul li{ list-style:none; float:left; margin:0 63px 0 0}
	.voucher table.cash-details td.vide1 ul li span{ text-decoration:none;}
	.voucher table.cash-details td.vide1 ul li:last-child{  margin:0 0px 0 0px}
	.signature{ width:150px; height:100px; border:1px solid #000; margin:0 12px 0 0;}
.voucher table.cash-details td table.budget {border-top:1px solid #000; border-left:1px solid #000;}
.voucher table.cash-details td table.budget td {border-bottom:1px solid #000; border-right:1px solid #000; padding:5px;}
}
</style>
<script src="../js/jquery-1.4.2.js"></script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        
        $(
            function(){
 				// Hook up the print link.
 				 
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
 function displayBlock(){
	 $("#mseRep").toggle();
 }
 function billdisplayBlock(){
	 $("#billAtc").toggle();
 }
 
    </script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<jsp:useBean id="ADV" class="beans.AdvanceService"></jsp:useBean>
<jsp:useBean id="DUE" class="beans.duesService"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
	<div>
	<div><%@ include file="title-bar.jsp"%></div>
	<%
	advance_Listing adv_List=new advance_Listing();
	dueListing due_List=new dueListing();
	headofaccountsListing HODL=new headofaccountsListing();
    majorheadListing MJHDL=new majorheadListing();
    minorheadListing MINL=new minorheadListing();
    subheadListing SUBL=new subheadListing();
	String MSEID=request.getParameter("MSEID");
	String InvoiceId=request.getParameter("INVOICE");
	String narration=request.getParameter("NARRATION");
	List advDetails=adv_List.getTotalDetailsBasedOnID(InvoiceId, MSEID);
	
	%>
	<div class="vendor-page">
	<div class="vendor-list">
				<div class="icons"><a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
			<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
				 <!--<table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
						<tr>
					<td colspan="7" align="center" style="font-weight: bold;">Cash / Cheque voucher PRINT</td>
					</tr>
				 </table>-->
		<div class="printable">
				
                <div class="voucher">
                <table width="100%" cellpadding="0" cellspacing="0" class="sri">
                <tr>
               	<td class="regd" colspan="2"><span>SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br /><br />
                ( Regd. No. 646/92 )<br />
				DILSUKHNAGAR, HYDERABAD - 500 060.<br /><br />
				<span  style="text-transform: uppercase; text-decoration:underline"> Cash / Cheque Voucher</span></td>
                </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%" class="cash-details">
                <tr>
                <td class="text-center">ADVANCE AMOUNT DETAILS</td>
                
                </tr>
                <tr>
                <td colspan="2"><span>Stock ledger Entry Number: <%=MSEID%></span></td>
                </tr>
                <tr>
                    <td colspan="2">
                    <span class="floatleft" style="text-decoration:none;">Narration :<%if(narration!=null && !narration.equals("")){ %> <%=narration%><%} %></span>   
               
                    </td>
                    
                  </tr>
                  <tr>  
                    <td colspan="2">
                   		<table width="100%" cellpadding="0" cellspacing="0" class="cash-details">
                   			<tr>
                   				<td>Invoice</td>
                   				<td>Head Account Id</td>
                   				<td>Major Head</td>
                   				<td>Minor Head</td>
                   				<td>Sub Head</td>
                   				<td>Amount</td>
                   			</tr>
                   			<tr>
                   				<td colspan="6">ADVANCE</td>
                   			</tr>
                   			<%
                   			Double advAmount=0.0;
                   			if(advDetails.size()>0){ 
                   				String subheadName="";
                   			for(int i=0;i<advDetails.size();i++){
                   				ADV=(AdvanceService)advDetails.get(i);
                   				String hoaName = HODL.getHeadofAccountName(ADV.getExtra1());
								String majName = MJHDL.getMajorHeadNameByCat1(ADV.getMajorHeadId(),ADV.getExtra1());
								String minName = MINL.getMinorHeadNameByCat1(ADV.getMinorHeadId(),ADV.getExtra1());
								List SUBLST = SUBL.getsubheadBasedOnHeads(ADV.getSubHeadId(),ADV.getMinorHeadId(),ADV.getMajorHeadId(),ADV.getExtra1());
					    		if(SUBLST.size() > 0)
					    		{
					    			SUBH = (subhead)SUBLST.get(0);
					    			subheadName = SUBH.getname();
					    		}
                   			%>
                   			<tr>
                   				<td><%=ADV.getInvoiceId()%></td>
                   				<td><%=ADV.getExtra1()%> <%=hoaName%></td>
                   				<td><%=ADV.getMajorHeadId()%> <%=majName%></td>
                   				<td><%=ADV.getMinorHeadId()%> <%=minName%></td>
                   				<td><%=ADV.getSubHeadId()%> <%=subheadName%></td>
                   				<td><%=ADV.getAmount()%></td>
                   			</tr>
                   			<%
                   			advAmount=advAmount+Double.parseDouble(ADV.getAmount());
                   			
                   			}} %>
                   			<tr>
                   				<td colspan="6">BILLS</td>
                   			</tr>
                   			<%
                   			List duelist=due_List.getDueDetails(InvoiceId);
                   			String subheadName1="";
                   			Double dueAmount=0.0;
                   			//System.out.println("dues size..."+dueDetails.size());
                   			if(duelist.size()>0){
                   			for(int j=0;j<duelist.size();j++){
                   				DUE=(duesService)duelist.get(j);
                   				String hoaName = HODL.getHeadofAccountName(DUE.getHeadAccountId());
								String majName = MJHDL.getMajorHeadNameByCat1(DUE.getMajorHeadId(),DUE.getHeadAccountId());
								String minName = MINL.getMinorHeadNameByCat1(DUE.getMinorHeadId(),DUE.getHeadAccountId());
								List SUBLST = SUBL.getsubheadBasedOnHeads(DUE.getSubHeadId(),DUE.getMinorHeadId(),DUE.getMajorHeadId(),DUE.getHeadAccountId());
					    		if(SUBLST.size() > 0)
					    		{	
					    			SUBH = (subhead)SUBLST.get(0);
					    			subheadName1 = SUBH.getname();
					    		}
                   			%>
                   			
                   			<tr>
                   				<td><%=DUE.getInvoiceId()%></td>
                   				<td><%=DUE.getHeadAccountId()%> <%=hoaName%></td>
                   				<td><%=DUE.getMajorHeadId()%> <%=majName%></td>
                   				<td><%=DUE.getMinorHeadId()%> <%=minName%></td>
                   				<td><%=DUE.getSubHeadId()%> <%=subheadName1%></td>
                   				<td>-<%=DUE.getAmount()%></td>
                   			</tr>
                   			
                   			<%
                   			dueAmount=dueAmount+Double.parseDouble(DUE.getAmount());
                   			}}
                   			Double finalAmount=advAmount-dueAmount;
                   			%>
                   			<tr>
                   				<td colspan="4"></td>
                   				<td style="border-right:none">Balance Amount:</td>
                   				<td style="border-left:none"><%=finalAmount%></td>
                   			</tr>
                   		</table>  
                     </td>
                    </tr>
                
              
                </table>
                
                </div>
          	
			 </div>
			</div>
			</div>
</div>
</div>
