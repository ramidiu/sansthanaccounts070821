<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Receipts And Payments Report</title>

<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>

<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MJH" class="beans.majorhead"/>
<jsp:useBean id="CUS" class="beans.customerpurchases"/>
<jsp:useBean id="PEXP" class="beans.productexpenses"/>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<div class="vendor-page">
<div class="vendor-list">
<div style="text-align: center;"></div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="clear"></div>
<div class="total-report" >
<form action="adminPannel.jsp" method="get">
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();

DecimalFormat DF=new DecimalFormat("0.00");
majorheadListing MJRHL=new majorheadListing();
headofaccountsListing HOAL=new headofaccountsListing();
customerpurchasesListing PURL=new customerpurchasesListing();
subheadListing SUBL=new subheadListing();
productsListing PROL=new productsListing();
minorheadListing MINHL=new minorheadListing();
String hoid="3";
if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
	hoid=request.getParameter("hoid");
} %>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="4" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;" class="bg-new">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;" class="bg-new">RECIPTS AND PAYMENTS REPORTS FOR <%=HOAL.getHeadofAccountName(hoid) %></td></tr>
<tr>
<td width="20%">
<input type="hidden" name="page" value="receipts-payments-report"></input>
<ul>
<%List HA_Lists=HOA_L.getheadofaccounts();
String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<li><input type="radio" name="hoid" value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> checked="checked" <%}else if(HOA.gethead_account_id().equals("3")){ %> checked="checked" <%} %>/> <%=HOA.getname() %></li><%}} %>
</ul></td>
<td width="30%"><input type="hidden" name="page" value="totalsalereport"/>From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="30%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" value="Search" name="search" class="click bordernone"/></td>
</tr>
<tr>
<td><input type="submit" value="Today" class="click bordernone"/></td>
<!-- <td><input type="submit" value="Current Week" class="click bordernone"/></td>
<td><input type="submit" value="Current Month" class="click bordernone"/></td>
<td><input type="submit" value="Current Year" class="click bordernone"/></td> -->
		 <td>   <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/></td>
					<td>  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/></td>
					<td>	<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/></td>
</tr>
</table>
</form>
<div id="tblExport">
<div style="float:left; width:628px; margin:0 0px 10px 0">
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr>
<td colspan="3" class="bg-new">Receipts</td>
</tr>
<tr>
<td>MajorHeadId</td>
<td>Receipts</td>
<td>Amount</td>
</tr>

<%
banktransactionsListing BAK_L=new banktransactionsListing();
SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
customerpurchasesListing SALE_L = new customerpurchasesListing();
productsListing PRDL=new productsListing();
List BANK_DEP=null;

Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");

DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
DateFormat  onlyYear= new SimpleDateFormat("yyyy");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String currentDate=(dateFormat2.format(c1.getTime())).toString();
String fromDate=currentDate+" 00:00:01";
String toDate=currentDate+" 23:59:59";
if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
	c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
	fromDate=currentDate+" 00:00:00";
	c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
	toDate=(dateFormat2.format(c1.getTime())).toString();
	toDate=toDate+" 23:59:59";
}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
	c1.getActualMaximum(Calendar.DAY_OF_MONTH);
	int lastday = c1.getActualMaximum(Calendar.DATE);
	c1.set(Calendar.DATE, lastday);  
	toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
	c1.getActualMinimum(Calendar.DAY_OF_MONTH);
	int firstday = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday); 
	fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
	int lastday_y = c1.get(Calendar.YEAR);
	c1.set(Calendar.DATE, lastday_y);  
	c1.getActualMinimum(Calendar.DAY_OF_YEAR);
	int firstday_y = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday_y); 
	Calendar cld = Calendar.getInstance();
	cld.set(Calendar.DAY_OF_YEAR,1); 
	fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
	cld.add(Calendar.YEAR,+1); 
	toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate")+" 00:00:01";
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate")+" 23:59:59";
	}
}
Calendar cal = Calendar.getInstance();
cal.setTime(dateFormat2.parse(fromDate));
cal.add(Calendar.DATE,-1);
String prevDay=dateFormat2.format(cal.getTime());
double openingBal=0.00;
double bankTotBal=0.00;
double pettyCashTotBal=0.00;
double pettyCashClosingTotBal=0.00;

bankdetailsListing BNKL=new  bankdetailsListing();
List BKDET=BNKL.getBanksBasedOnHOA(hoid);
if(BKDET.size()>0){
	for(int i=0;i<BKDET.size();i++){
		BNK=(beans.bankdetails)BKDET.get(i);
		//openingBal=openingBal+Double.parseDouble(BNK.gettotal_amount())+Double.parseDouble(BNK.getextra1());
		if(BAK_L.getOpeningBalance(BNK.getbank_id(),fromDate,"")!=null && !BAK_L.getOpeningBalance(BNK.getbank_id(),fromDate,"").equals("")){
			bankTotBal=Double.parseDouble(BAK_L.getOpeningBalance(BNK.getbank_id(),fromDate,""));
		}else{
			bankTotBal=0;
		}
		if(BAK_L.getOpeningBalance(BNK.getbank_id(),fromDate,"cashpaid")!=null && !BAK_L.getOpeningBalance(BNK.getbank_id(),fromDate,"cashpaid").equals("")){
			pettyCashTotBal=pettyCashTotBal+Double.parseDouble(BAK_L.getOpeningBalance(BNK.getbank_id(),fromDate,"cashpaid"));
			openingBal=openingBal+bankTotBal+pettyCashTotBal;
		}else{
			openingBal=openingBal+bankTotBal;
		}
		
%>
<tr>
<td colspan="2" style="text-align: right;"><%=BNK.getbank_name() %></td>
<%-- <td style="text-align: right;"><%=DF.format(Double.parseDouble(BNK.gettotal_amount())) %></td> --%>
<td style="text-align: right;"><%=DF.format(bankTotBal)%></td>
</tr>
<%}} %>
<tr>
<td colspan="2" style="text-align: right;">PETTY CASH</td>
<%-- <td style="text-align: right;"><%=DF.format(Double.parseDouble(BNK.getextra1())) %></td> --%>
<td style="text-align: right;"><%=DF.format(pettyCashTotBal) %></td>
</tr>
 <tr>
<td colspan="2" style="text-align:right;color: gray;" class="bg-new">OPENING BALANCE</td>
<td class="total-amt" style="text-align:right"><%=DF.format(openingBal) %></td>
</tr>
<%

List MajorHeads=null;
	MajorHeads=SUBL.getMajorHeadForRecipts(hoid,fromDate,toDate);
	
List RecDet=null;
double countercash=0.0;
double toDateCountercash=0.0;
double subHeadTotAmt=0.00;
double receiptsGrandTot=0.00;
if(MajorHeads.size()>0){
	for(int i=0;i<MajorHeads.size();i++){
		if(hoid.equals("3")){
			 countercash=countercash+PURL.getCounterCashReceipts(MajorHeads.get(i).toString(),hoid,prevDay);
			RecDet=PURL.getPoojastoresReceipts(MajorHeads.get(i).toString(),hoid,fromDate,toDate);
		}else{
			countercash=countercash+PURL.getReceiptsCounterCash(MajorHeads.get(i).toString(),hoid,prevDay);
			RecDet=PURL.getReceipts(MajorHeads.get(i).toString(),hoid,fromDate,toDate);
		}
%>
<%if(i==0){ %>
 <tr>
<td colspan="2" style="text-align:right;color: gray;" class="bg-new">Counter Cash</td>
<td class="total-amt" style="text-align:right"><%=DF.format(countercash) %></td>
</tr><%} %>
<tr>
	<td class="bg-new"><%=MajorHeads.get(i) %></td>
	<td colspan="2" class="bg-new" style="text-align:left;"><span><%=MJRHL.getmajorheadName(MajorHeads.get(i).toString()) %></span></td>
</tr>
<%

if(RecDet.size()>0){
	for(int j=0;j<RecDet.size();j++){
		CUS=(customerpurchases)RecDet.get(j);
		subHeadTotAmt=subHeadTotAmt+Double.parseDouble(CUS.gettotalAmmount());
%>
<tr>
	<td><%=CUS.getproductId() %></td>
	<%if(hoid.equals("3")){ %>
		<td style="text-align:left;"><%=CUS.getSub_head_name() %></td>
	<%}else{ %>
	<td style="text-align:left;"><%=PROL.getProductsNameByCat(CUS.getproductId(), CUS.getextra1()) %></td><%} %>
	<td style="text-align:right"><%=DF.format(Double.parseDouble(CUS.gettotalAmmount())) %></td>
</tr>
<%}} %>
<tr>
<td colspan="2"  style="text-align:right"><span>Total</span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(subHeadTotAmt) %></td>
</tr>
<%
receiptsGrandTot=receiptsGrandTot+subHeadTotAmt;
subHeadTotAmt=0.00;
if(hoid.equals("3")){
	toDateCountercash=toDateCountercash+PURL.getCounterCashReceipts(MajorHeads.get(i).toString(),hoid,request.getParameter("toDate"));
}else{
	toDateCountercash=toDateCountercash+PURL.getReceiptsCounterCash(MajorHeads.get(i).toString(),hoid,request.getParameter("toDate"));
}
}} %>

</table>
</div>

<%productexpensesListing PROEXPL=new productexpensesListing();
List EXPL=null;//PROEXPL.getPaymentsGroupByMajorheadId(hoid,fromDate,toDate);
List EXPL_Minor=null;
List EXPL_Subhead=null;
double expSubHeadTotAmt=0.00;
double expGrandTot=0.00;
%>
<div style="float:left; width:628px; margin:0 0px 10px 0">
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<tr>
<td colspan="5" class="bg-new">Payments</td>
</tr>
<tr>
<td>MajorHeadId</td>
<td>MinorHeadID</td>
<td>SubHeadId</td>
<td>Payments</td>
<td>Amount</td>
</tr>
<!-- <tr>
<td colspan="4">Opening Balance</td>
<td>2111</td>
</tr> -->
<%if(EXPL.size()>0){ 
for(int i=0;i<EXPL.size();i++){
%>
<tr>
<td class="bg-new"><%=EXPL.get(i) %></td>
<td colspan="4" class="bg-new" style="text-align:left;"><span><%=MJRHL.getmajorheadName(EXPL.get(i).toString()) %></span></td>
</tr>
<%EXPL_Minor=PROEXPL.getPaymentsBasedOnMajorheadId(hoid,EXPL.get(i).toString(),fromDate,toDate); 
if(EXPL_Minor.size()>0){
	for(int j=0;j<EXPL_Minor.size();j++){
%>
<tr>
	<td></td>
	<td><%=EXPL_Minor.get(j) %></td>
	<td style="text-align:left;" colspan="2"><span><%=MINHL.getminorheadName(EXPL_Minor.get(j).toString()) %> </span></td>
	<td></td>
</tr>
<%EXPL_Subhead=PROEXPL.getPaymentsBasedOnMinorheadId(hoid,EXPL_Minor.get(j).toString(),fromDate,toDate); 
if(EXPL_Subhead.size()>0){
	for(int k=0;k<EXPL_Subhead.size();k++){
		PEXP=(productexpenses)EXPL_Subhead.get(k);
		expSubHeadTotAmt=expSubHeadTotAmt+Double.parseDouble(PEXP.getamount());
%>
<tr>
<td></td>
<td></td>
<td><%=PEXP.getsub_head_id() %></td>
<td style="text-align:left;"><%=SUBL.getMsubheadname(PEXP.getsub_head_id()) %></td>
<td style="text-align:right"><%=DF.format(Double.parseDouble(PEXP.getamount())) %></td>
</tr>
<%}%>
<tr>
<td colspan="4"  style="text-align:right"><span>Total</span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(expSubHeadTotAmt) %></td>
</tr>
<%	expGrandTot=expGrandTot+expSubHeadTotAmt;
expSubHeadTotAmt=0.00;}
}}}
}else{ %>
<tr><td colspan="5" style="color: red;">There is no payments issued!</td></tr>
<%}%>
</table>
</div>
<div style="float:left; width:1250px; margin:0 0px 10px 0">
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise">
<%-- <tr>
<td colspan="3"  style="text-align:right" class="bg-new"><span></span></td>
<td colspan="4"  style="text-align:right" class="bg-new"><span>CLOSING BALANCE </span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(expGrandTot) %></td>
</tr> --%>
<%
cal.setTime(dateFormat2.parse(toDate));
cal.add(Calendar.DATE,1);
String NextDay=dateFormat2.format(cal.getTime());
Double ClosingBal=0.00;
if(BKDET.size()>0){
	for(int i=0;i<BKDET.size();i++){
		BNK=(beans.bankdetails)BKDET.get(i);
		//openingBal=openingBal+Double.parseDouble(BNK.gettotal_amount())+Double.parseDouble(BNK.getextra1());
		if(BAK_L.getOpeningBalance(BNK.getbank_id(),NextDay,"")!=null && !BAK_L.getOpeningBalance(BNK.getbank_id(),NextDay,"").equals("")){
			bankTotBal=Double.parseDouble(BAK_L.getOpeningBalance(BNK.getbank_id(),NextDay,""));
		}else{
			bankTotBal=0;
		}
		if(BAK_L.getOpeningBalance(BNK.getbank_id(),NextDay,"cashpaid")!=null && !BAK_L.getOpeningBalance(BNK.getbank_id(),NextDay,"cashpaid").equals("")){
			pettyCashClosingTotBal=pettyCashClosingTotBal+Double.parseDouble(BAK_L.getOpeningBalance(BNK.getbank_id(),NextDay,"cashpaid"));
			ClosingBal=ClosingBal+bankTotBal+pettyCashClosingTotBal;
		}else{
			ClosingBal=ClosingBal+bankTotBal;
		}
		
%>
<tr>
<td colspan="3"  style="text-align:right" ><span></span></td>
<td colspan="4"  style="text-align:right" ><span><%=BNK.getbank_name() %> </span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(bankTotBal)%></td>
</tr>

<%}} %>
<tr>
<td colspan="3"  style="text-align:right" ><span></span></td>
<td colspan="4"  style="text-align:right" ><span>PETTY CASH </span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(pettyCashClosingTotBal) %></td>
</tr>
<tr>
<td colspan="3"  style="text-align:right" class="bg-new"><span></span></td>
<td colspan="4"  style="text-align:right" class="bg-new"><span>Closing Balance </span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(ClosingBal) %></td>
</tr>
<tr>
<td colspan="3"  style="text-align:right" class="bg-new"><span></span></td>
<td colspan="4"  style="text-align:right" class="bg-new"><span>Counter Closing Balance </span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(toDateCountercash) %></td>
</tr>

<tr>
<td colspan="2"  style="text-align:right" class="bg-new"><span> TOTAL AMOUNT</span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(receiptsGrandTot+openingBal+countercash) %></td>
<td colspan="4"  style="text-align:right" class="bg-new"><span>EXPENSES TOTAL AMOUNT</span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(expGrandTot+toDateCountercash) %></td>
</tr>
<tr>
<td colspan="3"  style="text-align:right" class="bg-new"><span></span></td>
<td colspan="4"  style="text-align:right" class="bg-new"><span>TOTAL</span></td>
<td class="total-amt" style="text-align:right"><%=DF.format(ClosingBal-expGrandTot+toDateCountercash) %></td>
</tr>
<%-- <tr>
<td colspan="3"  style="text-align:right" class="bg-new"><span></span></td>
<td colspan="4"  style="text-align:right" class="bg-new"><span>CLOSING BALANCE </span></td>
<td class="total-amt" style="text-align:right"><%=DF.format((receiptsGrandTot+openingBal+countercash)-(expGrandTot+ClosingBal+toDateCountercash)) %></td>
</tr> --%>
</table>
</div>

</div>

</div>
</div>
</div>

</body>
</html>