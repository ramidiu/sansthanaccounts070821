<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="beans.payments"%>
<%@page import="mainClasses.paymentsListing"%>


<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>

<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.godwanstock"%>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<title>New Vendor</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}
.clear{
 clear:both;
}
</style>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}


</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="../js/jquery.blockUI.js"></script>

<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>

<script>
function majorheadsearch(j){
	var data1=$('#major_head_id'+j).val().split(" ");
	
	  var hoid=$("#head_account_id"+j).val().split(" ");
	  
	  $.post('searchMajor.jsp',{q:data1[0],HAid :hoid[0]},function(data)
				 {
		 var response = data.trim().split("\n");
		 		 
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
			
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#major_head_id'+j).autocomplete({
			source: availableTags,
			 focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val("");
				$(this).val(ui.item.label);
			}
		});  
			});
}
</script>
<script>
function minorheadsearch(j){
		  var data1=$('#minor_head_id'+j).val();
		  var major=$('#major_head_id'+j).val();
		  var hoid=$("#head_account_id"+j).val().split("");
		  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid[0]},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			$('#minor_head_id'+j).autocomplete({
				source: availableTags,
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
						$(this).val("");
					$(this).val(ui.item.label);
				}
			}); 		
	  });
}
</script>

 
 <script type="text/javascript">
function subheadsearch(j){
	  var data1=$('#subheadid'+j).val();
	  var arr = [];
	  //var data1=$("#sub_head_id").val();
	  var minor=$('#minor_head_id'+j).val();
	  var major=$('#major_head_id'+j).val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#subheadid'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
} 
</script>


<script type="text/javascript">
function subheadasproductsearch(j){
	  var data1=$('#subheadasproduct'+j).val();
	  var arr = [];
	  //var data1=$("#sub_head_id").val();
	  var minor=$('#minor_head_id'+j).val();
	  var major=$('#major_head_id'+j).val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#subheadasproduct'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
} 
</script>
<script type="text/javascript">


     function productsearch(j){
      var productid=$('#productid'+j).val();
      
      var major=$("#major_head_id"+j).val();
	  var minor=$("#minor_head_id"+j).val();

	  var sub=$("#subheadid"+j).val();
	  	 
	  var hoid=$("#head_account_id"+j).val();
	  $.post('ProductSearch.jsp',{major:major,minor:minor,sub:sub,hoid:hoid,productid:productid},function(data)
	    {
		 var response=data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#productid'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		});
	    });
  }
</script>

<script type="text/javascript">

 $(document).ready(function(){
	
	 
	 $('#getHeads').click(function(){
		 
		 var headOfAccount=$('#headOfAccount').val().trim();
		 
		 var productId=$("#enteredproductId").val().trim();
		 
		 if(headOfAccount==""){
			 $('#headOfAccount').css('outline','solid red 2px');
			 return false;
		 }
		 if(productId==""){
			 $('#enteredproductId').css('outline','solid red 2px');
			 return false;
		 }
		 
		 
		 if(headOfAccount!="" && productId!=""){ 
			 
			  $.ajax({
					url : "getProductHead.jsp",
					type : "POST",
			        data : 'headOfAccount='+headOfAccount+'&productId='+productId,
					success : function(response) {
						var data=response.split(",");
						var majorheadid=data[0];
						var majorheadname=data[1];
						if(response!=null && response!=""){
							$('#result').empty();
							$('#result').append("MajorHead:<input type='text' value='"+data[0]+" "+data[1]+"'> MinorHead:<input type='text' value='"+data[2]+" "+data[3]+"'> Subhead:<input type='text' value='"+data[4]+" "+data[5]+"'>");
						}
						if(response==""){
							$('#result').empty();
							$('#result').append("MajorHead:<input type='text' value=''> MinorHead:<input type='text' value=''> Subhead:<input type='text' value=''>");
						}
					},
					error : function(xhr, status, error) {
						alert(xhr.responseText);
					}
				}); 
			  
		 } 
		 
		
	 });
	 
	 
	 
 });
 
 
 function removeError(id){
		$('#'+id).css('outline', '#B0C4DE');
	}
</script>

</head>
<jsp:useBean id="PAY" class="beans.payments"/>
<body>
<div class="vender-details">
<%
DecimalFormat DF=new DecimalFormat("0.00");
String paymentid=request.getParameter("q");

String expensesId=request.getParameter("expensesId");


productexpensesListing PROE_L=new productexpensesListing();
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
majorheadListing mjl = new majorheadListing();
minorheadListing mnl = new minorheadListing();
subheadListing subl = new subheadListing();
productsListing prodl = new productsListing();

godwanstockListing GODL=new godwanstockListing();



vendorsListing VEN=new vendorsListing();
paymentsListing PAYL=new paymentsListing();
bankdetailsListing BNKL=new bankdetailsListing();
List Payment=PAYL.getMpaymentsBasedOnPaymentInvoice(paymentid);
if(Payment.size()>0){
	PAY=(payments)Payment.get(0);
}

%>
<form method="POST" action="check_update.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Payment Information</span></td>
</tr>

<tr>
<td colspan="1">
Payment ID: <%=PAY.getpaymentId()%></td>
<td colspan="1">
<input type="hidden" name="paymentid" id="paymentid" value="<%=PAY.getpaymentId() %>" />
Vendor Name : <%=VEN.getMvendorsAgenciesName(PAY.getvendorId()) %></td>
<td colspan="1">
Bank  Name : <%=BNKL.getBankName(PAY.getbankId()) %></td>
</tr>
<tr>
<td colspan="1">
Total Amount : <%=DF.format(Math.round(Double.parseDouble(PAY.getamount())))%></td>
<td>Check no<span style="color: red;">*</span> : <input type="text" name="checkno" id="checkno" required style="width:125px;"value="<%=PAY.getchequeNo()%>" /></td>
</tr>

<tr><td colspan="6">Narration::<input type="text" name="narration" id="narration" required style="width:500px;"value="<%=PAY.getnarration()%>" /></td>
</tr>
<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
<form action="UpdateMajMinSubPro.jsp" method="post">
<input type="hidden" name="paymentid" value="<%=paymentid%>">
<%
if(expensesId!=null && expensesId!=""){
	List prodLst = PROE_L.getMproductexpensesBasedOnExpInvoiceID(expensesId);	
	List godwnstockidList=GODL.getGodownStockId(expensesId);
	for (int j = 0 ; j < prodLst.size() ; j++)	{
		productexpenses pe = (productexpenses)prodLst.get(j);
		godwanstock	gstock =(godwanstock)godwnstockidList.get(j);
		
		%>
		
		<table width="100%" cellpadding="0" cellspacing="0">
		<input type="hidden" name="count" value=<%=prodLst.size()%>>
		
		<tr>
		<td style="font-size: 9px;font-weight: bold;"><input type="hidden" name="expensesId<%=j%>" value="<%=pe.getexpinv_id()%>"></td>
		
		<td style="font-size: 9px;font-weight: bold;"><input type="hidden" name="productexpensesid<%=j%>" value="<%=pe.getexp_id()%>"></td>
		
		<td style="font-size: 9px;font-weight: bold;"><input type="hidden" name="godownstockid<%=j%>" value="<%=gstock.getgsId()%>"></td>
	
		<td style="font-size: 9px;font-weight: bold;">HeadOFAccount:<input type="text" name="headOfAccount<%=j%>"  id="head_account_id<%=j%>"  value="<%=pe.gethead_account_id()%> <%=HOA_L.getHeadofAccountName(pe.gethead_account_id())%>" required></td>
		<td style="font-size: 9px;font-weight: bold;">MajorHead:<input type="text" name="MajorHead<%=j%>" id="major_head_id<%=j%>" value="<%=pe.getmajor_head_id()%> <%=mjl.getMajorHeadName(pe.getmajor_head_id(),pe.gethead_account_id()).split(" ")[1]%>" required     onkeyup="majorheadsearch('<%=j%>')" autocomplete="off"></td>
		<td style="font-size: 9px;font-weight: bold;">MinorHead:<input type="text" name="MinorHead<%=j%>" id="minor_head_id<%=j%>" value="<%=pe.getminorhead_id()%> <%=mnl.getminorheadname(pe.getminorhead_id(),pe.getmajor_head_id(),pe.gethead_account_id()).split(" ")[1] %>" onkeyup="minorheadsearch('<%=j%>')"  required autocomplete="off"></td>
		<td style="font-size: 9px;font-weight: bold;">SubHead:<input type="text" name="SubHead<%=j%>" id="subheadid<%=j%>" value="<%=pe.getsub_head_id()%> <%=subl.getSuheadNameBasedOnMajorheadandMinorhead(pe.getmajor_head_id(),pe.getminorhead_id(),pe.getsub_head_id())  %>"  onkeyup="subheadsearch('<%=j%>')"   required autocomplete="off"></td>
		<%if (pe.getsub_head_id().equals(pe.getextra2()))	{%>
			<td style="font-size: 9px;font-weight: bold;">Product:<input type="text" name="SubHeadasProduct<%=j%>" id="subheadasproduct<%=j%>" value="<%=pe.getsub_head_id()%> <%=subl.getSuheadNameBasedOnMajorheadandMinorhead(pe.getmajor_head_id(),pe.getminorhead_id(),pe.getsub_head_id()) %>" required autocomplete="off" onkeyup="subheadasproductsearch('<%=j%>')"></td>
			<%}
			else	{%>
				<td style="font-size: 9px;font-weight: bold;">Product:<input type="text" name="Product<%=j%>" id="productid<%=j%>" value="<%=pe.getextra2()%> <%=prodl.getProductNameByHeads(pe.gethead_account_id(),pe.getmajor_head_id(),pe.getminorhead_id(),pe.getsub_head_id(),pe.getextra2())%>" autocomplete="off" onkeyup="productsearch('<%=j%>')"></td>
			<%}%>
		</tr>
		
	
		</table>
		<% 
   }
}






%>

<input type="submit" value="Update" class="click" style="border:none;"/>
</form>

<div>
            HeadOfAccount:<input type="text" name="headOfAccount" id="headOfAccount" placeholder="Enter Head Of Account Here"  onkeyup="removeError('headOfAccount');"> 
             ProductId:<input type="text" name="enteredproductId" id="enteredproductId" placeholder="Eneter Product Id Here"  onkeyup="removeError('enteredproductId');"><br>
                      
           <button id="getHeads" class="click" >Get Product Heads</button> 
           
           <div id="result">
           </div>
</div>
</div>
</body>
</html>