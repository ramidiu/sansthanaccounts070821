<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;
	u=document.getElementById("rowstotal").value;
	if(document.getElementById("purchaseRate"+i).value!=""){
		if(document.getElementById("purchaseRate"+i).value!="" && document.getElementById("vat"+i).value!="")
		{
				for(var j=0;j<u;j++){
									if(document.getElementById("purchaseRate"+j).value!=""){
					document.getElementById("purseRatewtdiscnt"+j).value=document.getElementById("purchaseRate"+j).value;
				grossprice=Number((parseFloat(document.getElementById("purchaseRate"+j).value)*parseFloat(document.getElementById("quantity"+j).value))+((parseFloat(document.getElementById("purchaseRate"+j).value)*parseFloat(document.getElementById("quantity"+j).value)))*parseFloat(document.getElementById("vat"+j).value)/100).toFixed(2);
				if(document.getElementById("discountAmt"+i).value!=""){
					discountprice=Number(grossprice)*(1-parseFloat(document.getElementById("discountAmt"+j).value)/100).toFixed(2);
					document.getElementById("purseRatewtdiscnt"+j).value=Number(document.getElementById("purchaseRate"+j).value)*(1-parseFloat(document.getElementById("discountAmt"+j).value)/100).toFixed(2);
					grossprice=discountprice;
					}
			document.getElementById("grossAmt"+j).value=grossprice.toFixed(2);
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice)).toFixed(2);
	
				}
					}
					if(document.getElementById("otherCharges").value!=""){
				othercharges=document.getElementById("otherCharges").value;
			}
			Totalprice=Number(Totalprice)+Number(othercharges);
			document.getElementById("total").value=Totalprice;
			document.getElementById("total1").value=Totalprice;
		}
	}
	vatamntcal();
		
}
function vatamntcal(){
	var u=0;
	var vatamount=00;
	u=document.getElementById("rowstotal").value;
	for(var j=0;j<u;j++){
		if(document.getElementById("purchaseRate"+j).value!="" && document.getElementById("purseRatewtdiscnt"+j).value!="" && document.getElementById("vat"+j).value!=""){
			vatamount=Number(vatamount)+(Number(document.getElementById("purseRatewtdiscnt"+j).value)*((Number(document.getElementById("vat"+j).value)/100)*Number(document.getElementById("quantity"+j).value)));
		}
	}

	document.getElementById("totalVatAmount").value=vatamount.toFixed(2);
}
function discountamount(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var discountprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;
	var othercharges=0;
	var othercharges=0;
	u=document.getElementById("rowstotal").value;
	if(document.getElementById("discountAmt"+i).value!=""){

		if(document.getElementById("purchaseRate"+i).value!="" && document.getElementById("vat"+i).value!="" && document.getElementById("discountAmt"+i).value!="")
		{
			for(var j=0;j<u;j++){
				if(document.getElementById("purchaseRate"+j).value!=""   ){
					document.getElementById("purseRatewtdiscnt"+j).value=document.getElementById("purchaseRate"+j).value;
				grossprice=Number((parseFloat(document.getElementById("purchaseRate"+j).value)*parseFloat(document.getElementById("quantity"+j).value))+((parseFloat(document.getElementById("purchaseRate"+j).value)*parseFloat(document.getElementById("quantity"+j).value)))*parseFloat(document.getElementById("vat"+j).value)/100).toFixed(2);
				if(document.getElementById("discountAmt"+i).value!=""){
				discountprice=Number(grossprice)*(1-parseFloat(document.getElementById("discountAmt"+j).value)/100).toFixed(2);
				document.getElementById("purseRatewtdiscnt"+j).value=Number(document.getElementById("purchaseRate"+j).value)*(1-parseFloat(document.getElementById("discountAmt"+j).value)/100).toFixed(2);
				grossprice=discountprice;
				}
			document.getElementById("grossAmt"+j).value=grossprice.toFixed(2);
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice)).toFixed(2);
	
				}
				}
			if(document.getElementById("otherCharges").value!=""){
				othercharges=document.getElementById("otherCharges").value;
			}
			Totalprice=Number(Totalprice)+Number(othercharges);
			document.getElementById("total").value=Totalprice;
			document.getElementById("total1").value=Totalprice;
		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	vatamntcal();
	document.getElementById("check"+i).checked = true;

}
function validate(){
	var beforetotal=$('#beforetotal').val();
	var total=$('#total').val();
	if((Number(beforetotal)-Number(total))<1 && (Number(beforetotal)-Number(total))>-1){
	} else {
		alert("Previous total amount Should be matched with present amount ");
		return false;
	}

}
function temp(){
	calculate('0');
		roundoff(document.getElementById("typeofround").value);
		document.getElementById("beforetotal").value=document.getElementById("total").value;
	
}
function roundoff(name){
	var total=$("#total1").val();
	var roundtotal=$("#roundofftotal").val();
	if(roundtotal==""){
		alert("please enter amount to add or subract from total");
			}else if(Number(roundtotal)>1 ){
				alert("please enter amount below one rupee");
			}  else if(name=="plus"){
				total=Number(total)+Number(roundtotal);
				total=total.toFixed(2);
				$("#total").val(total);
				$("#typeofround").val('plus');
			} else if(name=="minus"){
				total=Number(total)-Number(roundtotal);
				total=total.toFixed(2);
				$("#total").val(total);
				$("#typeofround").val('minus');
					}
}
</script>
<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body onload="temp();" >
	<%
	if(session.getAttribute("adminId")!=null){ 
	%>
	<div>
		<jsp:useBean id="GSTK" class="beans.godwanstock" />
				<jsp:useBean id="GDSTK" class="beans.godwanstock" />
		<jsp:useBean id="SHP" class="beans.shopstock" />
		<jsp:useBean id="PRD" class="beans.products" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
		
<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Bill Edit</td></tr>
				</table>
		
				<div class="clear"></div>
				<div class="vender-details">
<%
String IdType="";
String BillId="";
if(request.getParameter("IdType")!=null){
	IdType=request.getParameter("IdType");
}
if(request.getParameter("BillId")!=null){
	BillId=request.getParameter("BillId");
}
%>

<form name="tablets_Update" method="get"  >
<table width="70%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td>
<input type="radio" name="IdType" value="MSE" checked="checked" required/>MSE
<input type="radio" name="IdType" value="UniqueId" required/>Unique No
</td>
<td><input type="text" name="BillId" id="BillId" value="<%=BillId %>"  placeholder="MSE  or Unique No" required />
<input type="hidden" name="page" value="billEdit"/> </td>
<td align="right"><input type="submit" value="Search" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>
<form name="tablets_Update" method="get" action="billUpdate.jsp" onsubmit="return validate();">
				<div class="list-details">
	<%
	if(!BillId.equals("")){
	double totalamount=0.0;
	double othercharges=0.0;
	SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	godwanstockListing GSK_L=new godwanstockListing();
	productsListing PROL=new productsListing();
	vendorsListing VND_L=new vendorsListing();
	List STOC_Apr=null;
List Godwnl=GSK_L.getGodwannoBasedUniquenoOrMSEno(BillId, IdType);
	  if(Godwnl.size()>0){
		  for(int j=0; j < Godwnl.size(); j++ ){
			  GDSTK=(godwanstock)Godwnl.get(j);	
      STOC_Apr=GSK_L.getgodwanstockAllApproved(GDSTK.getextra1());
	  if(STOC_Apr.size()>0){ %>
	  <div  style="display: blcok">
	  							<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td width="5%" style="font-weight: bold;display: none;"></td>
							<td  width="5%" style="font-weight: bold;">S.NO</td>
							<td  width="20%" style="font-weight: bold;">PRODUCT NAME.</td>
							<td  width="10%" style="font-weight: bold;">QTY</td>
							<td  width="10%" style="font-weight: bold;">PRICE</td>
							<td  width="10%" style="font-weight: bold;">VAT</td> 
							<td  width="10%" style="font-weight: bold;">DISCOUNT</td>
							<td  width="10%" style="font-weight: bold;">GROSS</td>  
							</tr>
						<%for(int i=0; i < STOC_Apr.size(); i++ ){
							GSTK=(godwanstock)STOC_Apr.get(i); 
							totalamount=totalamount+Double.parseDouble(GSTK.getMRNo());
							%>
						<tr><td  style="font-weight: bold; text-align:center;display: none;"><input type="checkbox" name="god_id" value="<%=GSTK.getgsId()%>" checked="checked"></input></td>
							<td><%=i+1%></td>
							<td><%=GSTK.getproductId() %>-<%=PROL.getProductsNameByCat(GSTK.getproductId(), GSTK.getdepartment()) %>
							<input type="hidden" name="prdid<%=GSTK.getgsId() %>" value="<%=GSTK.getproductId() %>" />
							</td>
							<td><%=GSTK.getquantity() %>
							<input type="hidden" name="quantity<%=i %>" id="quantity<%=i %>" value="<%=GSTK.getquantity() %>" />
										<input type="hidden" name="gsId<%=i %>" id="gsId<%=i %>" value="<%=GSTK.getgsId() %>" />
							</td>
							<td><input type="text" name="purchaseRate<%=i %>" id="purchaseRate<%=i %>" value="<%=GSTK.getpurchaseRate()%>"  onchange="calculate('<%=i%>')"/></td>
							<td><input type="text" name="vat<%=i %>" id="vat<%=i %>" onkeyup="calculate('<%=i%>')" value="<%=GSTK.getvat()%>"/></td>
							<td><input type="text" name="discountAmt<%=i %>" id="discountAmt<%=i %>" onkeyup="discountamount('<%=i%>')" <%if(!GSTK.getExtra10().equals("")){ %>value="<%=GSTK.getExtra10()%>" <%} else { %>value="0.0" <%} %>/></td>
							<td><input type="text" name="grossAmt<%=i %>" id="grossAmt<%=i %>" readonly="readonly" value="<%=df.format(Double.parseDouble(GSTK.getMRNo()))%>"/>
							<input type="hidden" name="purseRatewtdiscnt<%=i%>" id="purseRatewtdiscnt<%=i%>" value="" />
							</td></tr>
						<%} 
						if(!GSTK.getExtra8().equals("")){
						totalamount=totalamount+Double.parseDouble(GSTK.getExtra8());
						othercharges=Double.parseDouble(GSTK.getExtra8());
										}
						%>
 			<tr><td colspan="6" style="text-align: right;">Other Charges</td><td> <input  name="otherCharges" id="otherCharges"  value="<%=df.format(othercharges)%>" onkeyup="calculate('0')" /></td></tr>
 			<tr><td colspan="1" >Narration : </td>
 			<td colspan="2" ><textarea type="text" name="narration" id="narration" placeholder="Write your narration here" value="" cols="25" rows="5"><%=GSTK.getdescription()%> </textarea></td>
 			<td  colspan="3" style="text-align: right;">Round Off Total
 			<%
 			String roundoff="";
 			if(GSTK.getExtra11().equals("")){
 				roundoff="0";
 			} else {
 				roundoff=GSTK.getExtra11();
 			}
 			%>
<img alt="plus" src="../images/plus.png" onclick="roundoff('plus')">
 <input type="text" name="roundofftotal" id="roundofftotal" class="" value="<%=roundoff %>" onKeyPress="return numbersonly(this, event,true);" />
<input type="hidden" name="typeofround" id="typeofround" class="" value="<%=GSTK.getExtra12() %>"  /> <img alt="minus" src="../images/minus.png" onclick="roundoff('minus')"> </div>
</td></tr>
						<tr><td colspan="6"  style="text-align: right;">Total</td><td><input  name="total" id="total"  value="<%=df.format(totalamount) %>" />
						<input type="hidden" name="total1" id="total1" class="" value="0.00" onKeyPress="return numbersonly(this, event,true);" required/>
						<input type="hidden" name="beforetotal" id="beforetotal" class="" value="<%=df.format(totalamount) %>" onKeyPress="return numbersonly(this, event,true);" required/>
						</td></tr>
</table>
						<div >
					<ul>
								<li style="list-style:none; margin:10px 0 0 0"><input type="submit"  name="search" value="Edit" class="click"></input>
								<input type="hidden" name="rowstotal" id="rowstotal"  value="<%=STOC_Apr.size()%>"/> 
								<input type="hidden" name="headAccountId" value="<%=GSTK.getdepartment() %>"/>
								<input type="hidden" name="invoiceId" value="<%=GDSTK.getextra1() %>"/>
								<input type="hidden" name="bilApprvStatus" value="<%=GDSTK.getExtra6() %>"/>
								<input type="hidden" name="totalVatAmount" id="totalVatAmount"  value="" />
								</li>
					</ul>
				</div>
					</div><%
		totalamount=0.0;			
					othercharges=0.0;
	  } } } else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>Bill Can't Be Edited</span></div>
					</div>
					<%}
						}%>
			</div>
			</form>
			</div>
</div>
</div>

	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>
