<%@page import="java.util.ArrayList"%>
<%@page import="mainClasses.VendorsAndPayments"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.invoice"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.invoiceListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.medicalpurchases"%>
<%@page import="mainClasses.medicalpurchasesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List" %>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
$('#vendorDetails').hide;
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
            	$('.replaceme').html('Rs.');
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
                $('.replaceme').html('&#8377;');
            });
            $("#hoid").change(function () {
              $("#search").submit();
            });
        });
    </script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="VEN" class="beans.vendors"/>
<jsp:useBean id="PEXP" class="beans.productexpenses"/>
<jsp:useBean id="INV" class="beans.invoice"/>
<jsp:useBean id="GOD" class="beans.godwanstock"/>
<jsp:useBean id="GOD123" class="beans.godwanstock"/>
<jsp:useBean id="PMT" class="beans.payments"/>
<body>
	
	<form action="adminPannel.jsp">
		<table style="margin-left:40px;">
		<tr>
		<td><input type="hidden" name="page" value="vendorDetails"></td>
	
			<td colspan="2">Select Financial Year: </td>
		
	<%
	
	String id = request.getParameter("id");
	SimpleDateFormat SDF8=new SimpleDateFormat("yyyy-MM-dd");
	Calendar calld8 = Calendar.getInstance();
	calld8.add(Calendar.DATE,0);
	String todayDate8 = SDF8.format(calld8.getTime());
	String date8[] = todayDate8.split("-");
	 String currentyear8 = date8[0];
	 System.out.println("currentyear8...from vendordetails..."+currentyear8);
	 String year2 ="";
	 int currentyearinint = Integer.parseInt(date8[0]);
	 %> 
	 	<td><input type="hidden" name="id" value="<%=id %>"></td>	
	 	<%
	 if(todayDate8.compareTo(currentyear8+"-04-01") >=0){
		 year2 =String.valueOf(currentyearinint+1);
		 %>	 
		 	<td>
				<select name="finYear" id="finYear" style="width:150px;margin-left:20px;margin-right:20px;margin-top:10px;">
				<%
				if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("")){
					String finYear = request.getParameter("finYear");
					String fineYear[] = finYear.split("to");
					System.out.println("finYear...from vendor details.."+finYear);
					%>
					<option value="<%= request.getParameter("finYear")%>"><%=fineYear[0] %>-<%=fineYear[1] %></option>
					<%
				}else{
				%>
				<option value="<%= currentyear8%>to<%= year2%>" selected="selected"><%=currentyear8 %>-<%=year2 %></option>
					<%
				}
				%>
				<option value="<%= currentyear8%>to<%= year2%>"><%=currentyear8 %>-<%=year2 %></option>
				<%
				for(int i=currentyearinint;i>2015;i--){
					%>
					<option value="<%=i-1%>to<%= i%>"><%=i-1%>-<%= i%></option>
					<%
				}
			%>
				</select>
			  </td>
		 <%
	 }
	 else{
		 year2 =String.valueOf(currentyearinint-1);
		 %>
		 	<td>
				<select name="finYear" id="finYear" style="width:150px;margin-left:20px;margin-right:20px;margin-top:10px;">
				<%
				if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("")){
					String finYear = request.getParameter("finYear");
					String fineYear[] = finYear.split("to");
					%>
					<option value="<%= request.getParameter("finYear")%>"><%=fineYear[0] %>-<%=fineYear[1] %></option>
					<%
				}else{
				%>
				<option value="<%= year2%>to<%= currentyear8%>" selected="selected"><%=year2 %>-<%=currentyear8 %></option>
				<%} %>
				<option value="<%= year2%>to<%= currentyear8%>"><%=year2 %>-<%=currentyear8 %></option>
					<%
						for(int i=currentyearinint-1;i>2015;i--){
							%>
							<option value="<%=i-1%>to<%= i%>"><%=i-1%>-<%= i%></option>
							<%
						}
					%>
				</select>
			  </td>
		 <%
	 }
	%>
			<td><input type="submit" value="Submit" class="click" id="finyear"></td>
		</tr>
	
	</table>
	</form>
		
		
	<%String vendorId=request.getParameter("id");
	int ss=0;
	String financialyear = request.getParameter("finYear");
	System.out.println("financialyear...."+financialyear);
	String date[] = financialyear.split("to");
	String startYear = date[0]+"-04-01";
	String endYear = date[1]+"-03-31";
	String startYearBeforeDate = "";
	String startingdate = "2015-04-01";
	productexpensesListing PROEL1=new productexpensesListing();
	SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");	
	Calendar calld = Calendar.getInstance();
	calld.add(Calendar.DATE,0);
	String todayDate=SDF.format(calld.getTime());
	String todaydate[] = todayDate.split("-");
	 String currentyear = todaydate[0];
	 System.out.println("todayDate..."+todayDate);
	if(date[0].compareTo("2015") > 0){
		startYearBeforeDate = date[0]+"-03-31";
	}else if(date[0].compareTo("2015") <= 0){  
		startYearBeforeDate = date[0]+"-03-31";
	}
	
	
	mainClasses.vendorsListing VEN_CL = new mainClasses.vendorsListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	List VEN_List=VEN_CL.getMvendors(vendorId);
	if(VEN_List.size()>0){
		VEN=(beans.vendors)VEN_List.get(0);%>	
<div class="vendor-page">
<div class="vendor-box">
<div class="name-title"><%=VEN.gettitle()%> <%=VEN.getfirstName()%> <%=VEN.getlastName()%> </div>
<div class="click floatleft"><a href="./editVendor.jsp?id=<%=VEN.getvendorId()%>" target="_blank" onclick="openMyModal('editVendor.jsp?id=<%=VEN.getvendorId()%>'); return false;">Edit</a></div>
<div style="clear:both;"></div>
<div class="details-list">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="2"><span>Agency Name:</span><%=VEN.getagencyName()%></td>
</tr>

<tr>
<td width="40%"><span>Email:</span><%=VEN.getemail()%></td>
<td><span>Address:</span><%=VEN.getaddress()%> <%=VEN.getpincode()%></td>
</tr>
<tr>
<td><span>Phone:</span><%=VEN.getphone()%></td>
<td><span>City:</span><%=VEN.getcity()%></td>
</tr>
<tr>
<td><span>Gothram:</span><%=VEN.getgothram()%></td>
<td><span>Nominee:</span><%=VEN.getnomineeName()%></td>
</tr>

<tr>
<td colspan="2"><span>Other Details:</span><%=VEN.getdescription()%></td>
</tr>

<tr>
<td><span>Bank Name:</span><%=VEN.getbankName()%></td>
<td><span>Bank Branch:</span><%=VEN.getbankBranch()%></td>
</tr>

<tr>
<td><span>IFSC code:</span><%=VEN.getIFSCcode()%></td>
<td><span>Account Number:</span><%=VEN.getaccountNo()%></td>
</tr>
<tr>
<td colspan="2"><span>Credit Days:</span><%=VEN.getcreditDays()%></td>
</tr>
</table>

</div>
<%mainClasses.medicalpurchasesListing MED_Purchases=new mainClasses.medicalpurchasesListing();
mainClasses.invoiceListing INV_LST=new mainClasses.invoiceListing();
mainClasses.paymentsListing PAY_L=new mainClasses.paymentsListing();
godwanstockListing GODSL=new godwanstockListing();
double totVendorAmt=0.0;
double totBillAmt=0.0;
double totalPendingBalance = 0.0;
double totalpaidAmt=0.0;
double balanceAmt=0.0;
double thisYeartotVendorAmt = 0.0;
double thisYeartotBillAmt = 0.0;
double thisYearpaidAmt = 0.0;
double thisYearbalanceAmt=0.0;
double openingInvoiceAmount = 0.0;
double thisYearPendingBalance = 0.0;
String opeBal = "";
String bilamount="";
double openingPaidAmount = 0.0;
DecimalFormat df = new DecimalFormat("#,###.00");
String status1="";
if(request.getParameter("status")!=null && !request.getParameter("status").equals("")){
	status1=request.getParameter("status");
}

	 
	if(INV_LST.getTotalInvoicesAmount(VEN.getvendorId())!=null && !INV_LST.getTotalInvoicesAmount(VEN.getvendorId()).equals("") && INV_LST.getTotalOtherChargesAmount(VEN.getvendorId()) !=null && !INV_LST.getTotalOtherChargesAmount(VEN.getvendorId()).equals(""))
		{
		 
		if(GODSL.totalBillByVendorId(VEN.getvendorId(), startYear , endYear) != null){			
			thisYeartotVendorAmt = Double.parseDouble(GODSL.totalBillByVendorId(VEN.getvendorId(), startYear , endYear));		
		System.out.println("thisYeartotVendorAmt..."+thisYeartotVendorAmt);
			System.out.println(VEN.getvendorId()+"..."+startYear +" .... "+ endYear);
		}
		
		}
	else{
		totVendorAmt=0.0;
		thisYeartotVendorAmt = 0.0;
	}
	
	if(PAY_L.getPaidAmountToVendor(VEN.getvendorId())!=null && !PAY_L.getPaidAmountToVendor(VEN.getvendorId()).equals(""))
	{
		//totalpaidAmt = Double.parseDouble(PAY_L.getPaidAmountToVendor(VEN.getvendorId()));
		if(PAY_L.getPaidAmountToVendor(VEN.getvendorId() , startYear, endYear)!=null){
		thisYearpaidAmt = Double.parseDouble(PAY_L.getPaidAmountToVendor(VEN.getvendorId() , startYear, endYear));
		}
		
		opeBal = PAY_L.getPaidAmountToVendor(VEN.getvendorId() , startingdate, startYearBeforeDate);
	}
	else{
		totalpaidAmt=0.0;
		thisYearpaidAmt = 0.0;
		}
	
	String amount = GODSL.thisYearPendingAmountByVendorIdAndDate(VEN.getvendorId(), startYear , endYear);
	if(amount!=null){	
		thisYearPendingBalance = Double.parseDouble(amount);
	}
	
	 
	openingInvoiceAmount = GODSL.getBillTOT(VEN.getvendorId() ,startingdate , startYearBeforeDate);
	
	if(opeBal!=null && !opeBal.equals("")){
		//System.out.println(opeBal);
		openingPaidAmount = Double.parseDouble(opeBal);
	}else{
		openingPaidAmount = 0.0;
	}
	
	bankbalanceListing bankBalanceList = new bankbalanceListing();
	 double lastYearPendingbal = 0.0;
	 String finYr = "2017-18";
	 VendorsAndPayments vendorsAndPayments = new VendorsAndPayments();
		List<String[]> vendorPendingBalances = vendorsAndPayments.getVendorOpeningAndPendingAmount(VEN.getvendorId(), "", request.getParameter("finYear"), "2015-03-31 00:00:00", startYearBeforeDate+" 23:59:59", "sum");
		
		for(int i = 0 ; i < vendorPendingBalances.size(); i++){
			String[] result = vendorPendingBalances.get(i);
			lastYearPendingbal = lastYearPendingbal + Double.parseDouble(result[5]);
		}
		
		%>
		
<div class="details-amount">
 <ul>
<li class="colr">&#8377; <%=df.format(Math.round(thisYeartotVendorAmt))%> <br>
 <span>THIS YEAR BILL AMOUNT</span></li>
 
 <li style="color: green;">&#8377; <%=df.format(Math.round(thisYearpaidAmt))%><br>
 <span>THIS YEAR PAID AMOUNT</span></li>
  <li class="colr1">&#8377; <%=df.format(Math.round(thisYearPendingBalance))%><br> 
 <span>THIS YEAR PENDING BALANCE</span></li>
</ul>
</div>

<div class="vendor-list">

<div class="clear"></div>

<div class="icons">
<span><img src="../images/back.png" height="50" width="55" style="margin: 0 20px 0 0" title="back" onclick="goBack()"/></span> 
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<div class="printable" id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0" border="1">
<tr>
<tr><td colspan="10" height="10"></td> </tr>
	<tr><td colspan="10" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 14px;"><%=VEN.getagencyName()%> PAYMENT TRANSACTIONS (<%=date[0]+"-"+date[1] %>)</td></tr>
<tr>
<!-- <td class="bg" width="7%">S.No.</td> -->
<td class="bg" width="7%">Invoice No.</td>
<td class="bg" width="7%">Bill No.</td>
<td class="bg" width="10%">PAYMENT DATE</td>
<td class="bg" width="20%">Vendor</td>
<td class="bg" width="20%">SUBHEAD</td>
<td class="bg" width="8%">TOTAL INVOICE AMOUNT</td>
<td class="bg" width="8%">PAID AMOUNT</td>
<td class="bg" width="15%">PAYMENT MODE</td>
<td class="bg" width="10%">CHEQUE NO</td>
<td class="bg" width="10%">VOUCHER NO</td>
</tr>
<tr>
	<td colspan="5" align="right">OPENING PENDING BALANCE</td>
	<td align="right" colspan="2"><%=df.format(Math.round(lastYearPendingbal)) %></td>
	
	<td></td>
</tr>
<jsp:useBean id="GODW" class="beans.godwanstock"/>
<jsp:useBean id="GODW1" class="beans.godwanstock"/>
<jsp:useBean id="GODW11" class="beans.godwanstock"/>
<jsp:useBean id="GODW2" class="mainClasses.godwanstockListing"/>
<%
subheadListing SUBHL=new subheadListing();
productexpensesListing PROEL=new productexpensesListing();
paymentsListing PAYL=new paymentsListing();

mainClasses.godwanstockListing GODS_l=new mainClasses.godwanstockListing();
List PAYDET=null;
List EXPD = null;
String status="Payment Done";
String billNo;
double tot=0.00;
double invoicetot = 0.0;
if(request.getParameter("status")!=null && !request.getParameter("status").equals("")){
	status=request.getParameter("status");
}

PAYDET=PAYL.getMpaymentsBasedOnExpID(VEN.getvendorId(),startYear,endYear );
if(PAYDET.size()>0){
	
	for(int i=0;i<PAYDET.size();i++){
		PMT=(beans.payments)PAYDET.get(i);
		 if(PMT.getamount()!=null && !PMT.getamount().equals("")){
				tot=tot+Double.parseDouble(PMT.getamount());
			}
		EXPD=PROEL.getproductexpensesBasedOnVendorAndStatusAndExpinvId(request.getParameter("id"),status,PMT.getextra3());
		if( EXPD != null && EXPD.size()>0){
			PEXP=(productexpenses)EXPD.get(0);
		}
		List GODS_DET=GODS_l.getStockDetailsBasedOnInvoiceId(PEXP.getextra5());
		if(GODS_DET.size()>0){
			GODW=(beans.godwanstock)GODS_DET.get(0);
		}
		%>
		<tr>
		<%-- <td><%=ss=ss+1 %> --%>
<td><%=PEXP.getexpinv_id()%></td>
<td><%=GODW.getbillNo()%></td>
<td><%if(PMT.getdate()!=null && !PMT.getdate().equals("")){ %> <%=CDF.format(SDF.parse(PMT.getdate()))%><%}else{ %> N/A <%} %></td>
<td><%=VEN.getfirstName()%><%=VEN.getlastName()%></td>
<td><%=SUBHL.getMsubheadname(PEXP.getsub_head_id()) %></td>
<%
			String totBill = GODSL.getBillTOT(PEXP.getextra5());
if(df.format(Double.parseDouble(totBill)).equals(df.format(Double.parseDouble(PMT.getamount())))){
if(totBill!=null && !totBill.equals("")){
	invoicetot = invoicetot+Double.parseDouble(totBill);					
					%>					
	<td align="right"> 	<%= df.format(Double.parseDouble(totBill)) %>  
					<%} %></td> 
<td align="right"> <%if(PMT.getamount()!=null && !PMT.getamount().equals("")){ %>
							<%=df.format(Double.parseDouble(PMT.getamount()))%>
						<%}
}
else{
	if(totBill!=null && !totBill.equals("")){
		invoicetot = invoicetot+Double.parseDouble(totBill);%>					
		<td align="right" bgcolor="red" ><%= df.format(Double.parseDouble(totBill)) %>  
						<%} %></td> 
	<td align="right" bgcolor="red"> <%if(PMT.getamount()!=null && !PMT.getamount().equals("")){ %>
				<%=df.format(Double.parseDouble(PMT.getamount()))%>
		<%}
}

%>
</td>
<td align="center"><%=PMT.getpaymentType() %></td>
<td ><%=PMT.getchequeNo() %></td>
<td><%=PMT.getreference() %></td>
</tr>
		<%
		/* System.out.println("HELLO :"+PMT.getdate()); */
	}
totBillAmt=0.0;	
// tot = tot + billAmount;

%>
<tr><td colspan="5" align="right" class="bg">TOTAL AMOUNT</td><td align="right" class="bg"><%-- <%=invoicetot %> --%></td><td colspan="1" align="right" class="bg" style="font-weight: bold;"><!-- <span class="replaceme">&#8377;</span> --> <%=df.format(tot)%></td><td colspan="4" class="bg"></td></tr>
<%
}else{ %>
<tr><td colspan="10" align="center" style="font-size: 20px;color: red;"> There is no transactions found for this vendor!..</td></tr>
<%} %>



</table> 

<!-- This pending transactions code is written by pradeep on 17/06/2016 starts -->
<br/>

<%
if(GODSL.pendingAmountByVendorIdAndDate(VEN.getvendorId(), startingdate , startYearBeforeDate)!=null){
	thisYearPendingBalance = Double.parseDouble(GODSL.pendingAmountByVendorIdAndDate(VEN.getvendorId(), startingdate , startYearBeforeDate));
	//System.out.println("thisYearPendingBalance: "+thisYearPendingBalance);
}
String openingPendingbal = GODSL.openingPendingAmountByVendorIdAndDate(VEN.getvendorId(),startingdate, startYearBeforeDate);
if(openingPendingbal == null || openingPendingbal.equals("")){
	openingPendingbal = "0.0";
}
%>
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border="1">
<tr>

<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 14px;"><%=VEN.getagencyName()%> PENDING PAYMENT TRANSACTIONS (<%=date[0]+"-"+date[1] %>) </td></tr>

<tr>
<!-- <td class="bg" width="7%">S.No</td> -->
<td class="bg" width="7%">Invoice No.</td>
<td class="bg" width="7%">Bill No.</td>
<td class="bg" width="10%">BILL APPROVED DATE BY A/C MANAGER</td>
<td class="bg" width="20%">Vendor</td>
<td class="bg" width="20%">SUBHEAD</td>

<td class="bg" width="15%">TOTAL INVOICE AMOUNT</td>
<td class="bg" width="10%">BILL AMOUNT</td>
<td class="bg" width="35%">PAYMENT STATUS</td>

</tr>
<tr>
	<td colspan="5" align="right">OPENING PENDING BALANCE</td>
	<td align="right" colspan="2"><%= openingPendingbal%> </td>
	<td></td>
</tr>
<%
subheadListing SUBHL1=new subheadListing();

paymentsListing PAYL1=new paymentsListing();
godwanstockListing GODSL1=new godwanstockListing();
godwanstockListing GODSL2=new godwanstockListing();
invoiceListing INVL = new invoiceListing();
productsListing PRODL = new productsListing();
List PAYDET1=null;

List EXPD1=PROEL1.getproductexpensesBasedOnVendorAndStatusAndDate(VEN.getvendorId(),status1,startYear ,endYear);

List INV11 = INVL.getMNOTAdminApprovedinvoiceList(request.getParameter("id"),startYear ,endYear);
double tot1=0.00;
SimpleDateFormat SDF1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF1=new SimpleDateFormat("dd-MM-yyyy/HH:mm");
if(EXPD1.size()>0 || INV11.size() > 0){
	
	for(int i=0;i<EXPD1.size();i++){
	PEXP=(productexpenses)EXPD1.get(i);
	
	 if(GODSL1.getBillTOT(PEXP.getextra5())!=null && !GODSL1.getBillTOT(PEXP.getextra5()).equals("")){
		 tot1=tot1+Double.parseDouble(GODSL1.getBillTOT(PEXP.getextra5()));
	 }
	 
	 List GODS_DET1=GODS_l.getStockDetailsBasedOnInvoiceId(PEXP.getextra5());
		if(GODS_DET1.size()>0){
			GODW1=(beans.godwanstock)GODS_DET1.get(0);
		}		
	%>
<tr>
<%-- <td><%=ss=ss+1 %></td> --%>
<td><%=PEXP.getexpinv_id()%></td>
<td><%=GODW1.getbillNo()%></td>
<td><%if(PEXP.getentry_date() !=null && !PEXP.getentry_date().equals("")) {%><%=CDF1.format(SDF1.parse(PEXP.getentry_date()))%><%} %></td>
<td><%=VEN.getfirstName()%><%=VEN.getlastName()%></td>
<td><%=SUBHL1.getMsubheadname(PEXP.getsub_head_id()) %></td>
<td align="right"> <%if(GODSL1.getBillTOT(PEXP.getextra5())!=null && !GODSL1.getBillTOT(PEXP.getextra5()).equals("")){ %><!--  &#8377;  --><%=df.format(Double.parseDouble(GODSL1.getBillTOT(PEXP.getextra5()))) %>  <%} %> </td>
<td align="right"> &#8377; <%=df.format(Double.parseDouble(GODSL1.getBillTOT(PEXP.getextra5()))) %> </td>
<!-- <td align="center"><div class="print-btn">Print Check</div>  PAYMENT PENDING</td> -->
<td align="center"><% if(PEXP.getextra1().equals("Not-Approve") && PEXP.getextra4().equals("")){%>Waiting For GS Approval<%}else if(PEXP.getextra1().equals("Not-Approve") && PEXP.getextra4().equals("BoardApprove")) {%>Waiting For FS Approval<%}else if(PEXP.getextra1().equals("Approved") && PEXP.getextra4().equals("BoardApprove")){ %>PAYMENT PENDING<%}else if(PEXP.getextra1().equals("WaitingMgrApproval") && PEXP.getextra4().equals("")) {%>Waiting For MGR Approval<%} %>
</td>
</tr>
<%}
	List pendingAmountList = null;
	
	pendingAmountList = GODSL1.getPendingAmountRecords(request.getParameter("id"),startYear ,endYear/* ,EXPD1.size() */);
//	List INV1 = INVL.getMNOTAdminApprovedinvoiceList(request.getParameter("id"),startYear ,endYear);
	if(pendingAmountList.size()>0){
	 for(int j = 0 ; j < pendingAmountList.size() ; j++){
		 
		//INV=(invoice)INV1.get(j);
		GODW1 = (godwanstock)pendingAmountList.get(j);
		
		List GODS_DET2 = GODS_l.getStockDetailsBasedOnInvoiceId(PEXP.getextra5());
		if(GODS_DET2.size()>0){
			GODW11=(beans.godwanstock)GODS_DET2.get(0);
		}
		
		%>
	<tr>
	
	<td><%= GODW1.getextra1()%></td>
	<td><%=GODW11.getbillNo()%></td>
	<td><%
		if(GODW1.getdate()!= null && !GODW1.getdate().equals("")){
	%>
		<%=CDF1.format(SDF1.parse(GODW1.getdate()))%>
		<%} %>
	</td>
	<td>
		<%=VEN.getfirstName()%><%=VEN.getlastName()%>
	</td>
	
	<td><%
	String subHeadName = SUBHL1.getMsubheadname(GODW1.getproductId());
	if(!subHeadName.equals("")){
	%>
		<%=SUBHL1.getMsubheadname(GODW1.getproductId())%>
	<%}else{ 
	%>
		<%=PRODL.getProductNameByCat(GODW1.getproductId(), GODW1.getdepartment())%>
	<%} %></td>
	<td align="right"> 
		
		<%=df.format(Double.parseDouble(GODW1.getExtra16())) %>
		
		<%
		tot1 = tot1 + Double.parseDouble(GODW1.getExtra16());
		%>
		 </td>
	<td align="right"> &#8377;<%=df.format(Double.parseDouble(GODW1.getExtra16())) %></td>
	<td align="center">
	<%
	if(GODW1.getprofcharges()!=null && GODW1.getApproval_status()!=null ){
	
		if(GODW1.getprofcharges().equals("UncheckedByPO") && GODW1.getApproval_status().equals("") && GODW1.getPayment_status().equals("") ){
			%>Waiting For PO Approval<%
		}else if(GODW1.getprofcharges().equals("checked") && GODW1.getApproval_status().equals("Not-Approve") && GODW1.getPayment_status().equals("")){ 
			%>Waiting For Board Members Approval<%
		}else if(GODW1.getprofcharges().equals("checked") && GODW1.getApproval_status().equals("Approved") && GODW1.getPayment_status().equals("")){ 
			%>Waiting For Admin Verfication<%
		} 
	}
	%>
	</td>
	</tr>
	<%
	}
	}
	totBillAmt=0.0;	%>
	<tr><td colspan="5" align="right" class="bg">TOTAL AMOUNT</td>
	<td colspan="1" align="right" class="bg" style="font-weight: bold;"></td>
	<td colspan="1" align="right" class="bg" style="font-weight: bold;">
		 <%=df.format(tot1 + Double.parseDouble(openingPendingbal) ) %>
	</td>
		<td colspan="4" class="bg"></td>
	</tr>
<%
	}else{ %>
<tr><td colspan="10" align="center" style="font-size: 20px;color: red;"> There is no Pending transactions found for this vendor!..</td></tr>
<%} %>
</table>

<!-- This pending transactions code is written by pradeep on 17/06/2016 and modified by sitaram on 24/08/2018-->

</div>
</div>
</div>
</div>
<%}else{response.sendRedirect("adminPannel.jsp?page=vendors");}%>
</body>