<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.paymentsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PAY-ORDER</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
@media print {
	@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin-left: 24mm; /* equals to 0.8inches */ 
}
	.voucher
	{
		width:880px; height:auto;  padding:40px 20px 40px 20px; border:1px solid #727272; left:55px; position:relative;
	}
	 .voucher table.sri td span { font-size:30px; font-weight:bold;}
	.voucher table td.regd { text-align:center; vertical-align:top; font-size:12px;}
	.voucher table.cash-details{ margin:40px 0 0 0; font-size:20px; border-top:1px solid #727272; border-left:1px solid #727272;}
	.voucher table.cash-details td{padding:10px 0px; border-bottom:1px solid #727272; border-right:1px solid #727272;  padding:10px 20px;}
	.voucher table.cash-details td span {text-decoration:underline; padding:0 0 5px 0; font-weight:bold; }
	.voucher table.cash-details td.vide ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide ul li{ list-style:none; float:left; margin:0 45px 0 0; }
	/*.voucher table.cash-details td.vide ul li:first-child{ width:400px;  margin:0 20px 0 0;}*/
	.voucher table.cash-details td.vide1 ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide1 ul li{ list-style:none; float:left; margin:0 63px 0 0}
	.voucher table.cash-details td.vide1 ul li span{ text-decoration:underline;}
	.voucher table.cash-details td.vide1 ul li:last-child{  margin:0 0px 0 0px}
	.signature{ width:150px; height:100px; border:1px solid #000; margin:0 12px 0 0;}
.voucher table.cash-details td table.budget {border-top:1px solid #000; border-left:1px solid #000;}
.voucher table.cash-details td table.budget td {border-bottom:1px solid #000; border-right:1px solid #000; padding:5px;}
}
</style>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $(".hide").hide();
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           // Cancel click event.
                            return( false );
                        });
 				});
    </script>
    <script src="../js/jquery.print.js"></script>
</head>
<body>
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">
<div class="icons">
	<a id="print"><span><img src="../images/printer.png" style="margin: 0 480px 0 0" title="print" /></span></a> 
</div>
<div class="clear"></div>
<div class="list-details">
<div class="printable">
<div class="voucher">
 <table width="100%" cellpadding="0" cellspacing="0" class="sri">
     <tr><td class="regd" colspan="2"><span>SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br /><br />( Regd. No. 646/92 )<br /><br />DILSUKHNAGAR, HYDERABAD - 500 060.<br /><br />
		<span  style="text-transform: uppercase; text-decoration:underline">PAY ORDER</span></td>
      </tr>
 </table>
<table cellpadding="0" cellspacing="0" width="100%" class="cash-details">
	<%paymentsListing PAYL=new paymentsListing();
	DecimalFormat DF=new DecimalFormat("#,##,###.00");
	String cheque=request.getParameter("chequestatus");
	  List PAYDL=PAYL.getMpaymentsBasedOnPaymentInvoice(request.getParameter("payId"));
	  if(PAYDL.size()>0){
		for(int i=0;i<PAYDL.size();i++){
			PAY=(beans.payments)PAYDL.get(i);
			if(PAY.getextra7().equals("") || PAY.getextra7().equals("0")){
			if(!PAY.getService_tax_amount().equals("") && !PAY.getService_tax_amount().equals("0")){
				%>
				<tr><td>VENDOR AMOUNT</td><td colspan="1"><%=DF.format(Double.parseDouble(PAY.getamount())-Double.parseDouble(PAY.getService_tax_amount())) %>      <%if(cheque==null){ %><span class="hide"sty > <a href="adminPannel.jsp?page=cheque&payId=<%=request.getParameter("payId")%>&con=vp" target="_blank" >PRINT CHEQUE </a> </span>  <%} %>  </td></tr>
				<% 
			} else{
			%>
		<tr><td>VENDOR AMOUNT</td><td colspan="1"><%=DF.format(Double.parseDouble(PAY.getamount())) %>      <%if(cheque==null){ %><span class="hide"sty > <a href="adminPannel.jsp?page=cheque&payId=<%=request.getParameter("payId")%>&con=vp" target="_blank" >PRINT CHEQUE </a> </span>  <%} %>  </td></tr>
		
		<%}} else{
			if(!PAY.getService_tax_amount().equals("") && !PAY.getService_tax_amount().equals("0")){
			%>
			
			<tr><td>VENDOR AMOUNT</td><td colspan="1"><%=DF.format(Double.parseDouble(PAY.getamount())-Double.parseDouble(PAY.getextra7())-Double.parseDouble(PAY.getService_tax_amount())) %>       <%if(cheque==null){ %> <span class="hide"sty > <a href="adminPannel.jsp?page=cheque&payId=<%=request.getParameter("payId")%>&con=vp" target="_blank" >PRINT CHEQUE </a> </span>   <%} %> </td></tr>
		<tr><td>TDS CHARGES AMOUNT</td><td colspan="1"><%=DF.format(Double.parseDouble(PAY.getextra7())) %>      <%if(cheque==null){ %>  <span class="hide" > <a href="adminPannel.jsp?page=cheque&payId=<%=request.getParameter("payId")%>&con=tdsc" target="_blank">PRINT CHEQUE </a> </span> <%} %>  </td></tr>
		<tr><td>SERVICE TAX AMOUNT</td><td colspan="1"><%=DF.format(Double.parseDouble(PAY.getService_tax_amount())) %>      <%if(cheque==null){ %><span class="hide"sty > <a href="adminPannel.jsp?page=cheque&payId=<%=request.getParameter("payId")%>&con=servicetax" target="_blank" >PRINT CHEQUE </a> </span>  <%} %>  </td></tr>
			<% 
		} else{
		%>
		<tr><td>VENDOR AMOUNT</td><td colspan="1"><%=DF.format(Double.parseDouble(PAY.getamount())-Double.parseDouble(PAY.getextra7())) %>       <%if(cheque==null){ %> <span class="hide"sty > <a href="adminPannel.jsp?page=cheque&payId=<%=request.getParameter("payId")%>&con=vp" target="_blank" >PRINT CHEQUE </a> </span>   <%} %> </td></tr>
		<tr><td>TDS CHARGES AMOUNT</td><td colspan="1"><%=DF.format(Double.parseDouble(PAY.getextra7())) %>      <%if(cheque==null){ %>  <span class="hide" > <a href="adminPannel.jsp?page=cheque&payId=<%=request.getParameter("payId")%>&con=tdsc" target="_blank">PRINT CHEQUE </a> </span> <%} %>  </td></tr>
		
		
	<%} } %>
		<tr><td>DISCOUNT/DEDUCTIONS</td><td colspan="1">0.00</td></tr>		
		<tr><td colspan="1">TOTAL AMOUNT</td><td colspan="1"><%=DF.format(Double.parseDouble(PAY.getamount())) %></td></tr>	
		<tr><td colspan="2" height="40" style="border-bottom:none;"></td></tr>
         <tr><td align="right" colspan="2" style="border-bottom:none;"><div class="signature"></div></td></tr>
         <tr><td colspan="2" class="vide1">
                <ul>
                <li>Accountant</li>
                <li>Manager ( Acts )</li>
                 <li>GS/Chairman</li> 
                 <li>Treasurer</li>
                 <li>Receiver's Signature</li>
                </ul>
                </td>
        </tr>	
		<%}}%>
</table>
</div>
</div>
</div>
</div>
</div>
</body>
</html>