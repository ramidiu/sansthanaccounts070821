<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<Script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});

function check1(){
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one");
	return false;} else{
	document.getElementById("bankss").action="makeBanksActiveOrDeactive.jsp?type=Active";
	document.getElementById("bankss").submit();
	}
}

</script>

<jsp:useBean id="BNK" class="beans.bankdetails"/>
<div class="vendor-page">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">DEACTIVE BANK ACCOUNTS </td></tr>
</table>
<input type="submit" name="button" id="button" class="click floatright" style="margin-left: 10px" value="Make Active" onclick="return check1();"></input>
<div class="click floatright"><a href="./newBankAccount.jsp" target="_blank" onclick="openMyModal('newBankAccount.jsp'); return false;">New Bank Account</a></div>
<!-- <div class="vendor-box">
<div class="vendor-title">Banks</div>
<div class="click floatright"><a href="./newBankAccount.jsp" target="_blank" onclick="openMyModal('newBankAccount.jsp'); return false;">New Bank Account</a></div>
<div style="clear:both;"></div>

<div class="unpaid-box">
<div class="unpaid">Unpaid</div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OPEN BILL</li>
</ul>
</div>
<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OVERDUE</li>
</ul>
</div>
</div>
</div>
</div>
<div class="paid-box">
<div class="unpaid">Paid</div>
<div class="bills-cash">
<div class="bill-paid">
 <ul>
<li class="bill-amount">&#8377;2000</li>
 <li>Total Bank Balance</li>
</ul>
</div>
</div>
</div>
</div> -->

<div class="vendor-list">
<!-- <div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div> -->
<div class="clear"></div>
<div class="list-details">
<%mainClasses.bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
/* List BNK_List=BNK_CL.getbankdetails(); */
List BNK_List=BNK_CL.getdeactivebankdetails();
DecimalFormat formatter = new DecimalFormat("#,###.00");
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();

if(BNK_List.size()>0){%>
<form name="bankss" id="bankss" method="POST"  >
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<!-- <td class="bg" width="3%"><input type="checkbox"></td> -->
 <td class="bg" width="6%"><input type="checkbox" name="SelectAll" id="selecctall"/>Check All </td> 
<td class="bg" width="6%">S.No.</td>
<td class="bg" width="23%">Bank Name</td>
<td class="bg" width="23%">Bank Branch</td>
<td class="bg" width="20%">Account Holder Name</td>
<td class="bg" width="10%">Total Amount</td>
</tr>
<%for(int i=0; i < BNK_List.size(); i++ ){
	BNK=(beans.bankdetails)BNK_List.get(i); %>
<tr>
<!-- <td><input type="checkbox"></td> -->
<td width="6%"><input type="checkbox" name="select1" class="checkbox1" value = "<%=BNK.getbank_id()%>"></td>
<td width="6%"><%=BNK.getbank_id()%></td>
<td><ul>
<li><span><a href="adminPannel.jsp?page=bankDetails&id=<%=BNK.getbank_id()%>"><%=BNK.getbank_name()%>  </a></span><br />
<span><%=HOA_CL.getHeadofAccountName(BNK.getheadAccountId()) %></span></li>
</ul></td>
<td><%=BNK.getbank_branch()%></td>
<td><%=BNK.getaccount_holder_name()%></td>
<td><%=formatter.format(Double.parseDouble(BNK.gettotal_amount()))%></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>No Deactivated Banks Added Yet</h1></div>
<%}%>
</form>

</div>
</div>




</div>
