<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="beans.ProductOpeningBalanceService"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>productopeningbalanceupdate</title>

</head>
<body>
 <%
   double quantity=Double.parseDouble(request.getParameter("quantity"));
   String productOpeningBalanceId=request.getParameter("productOpeningBalanceId");
   
   ProductOpeningBalanceService productOpeningBalanceService=new ProductOpeningBalanceService();
   productOpeningBalanceService.updateProductOpeningBalance(productOpeningBalanceId,quantity);
   response.sendRedirect("adminPannel.jsp?page=productOpeningBalanceList");
 %>
</body>
</html>