<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script src="../js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	function formSubmit(){
		$.blockUI({ css: { 
		    border: 'none', 
		    padding: '15px', 
		    backgroundColor: '#000', 
		    '-webkit-border-radius': '10px', 
		    '-moz-border-radius': '10px', 
		    opacity: .5, 
		    color: '#fff' 
		}});
		setTimeout(function() {
			$("#sj").submit();
		}, 500);

	}
	
	function checkHoa(){
		var hoa = document.getElementById("hoid").value;
		document.getElementById("head").value = hoa; 
	}
</script>
<script>
$(function(){
	$('#fromdate').datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect : function(date){
			var selectedDate = new Date(date);
			$("#todate").datepicker( "option", "minDate", selectedDate );
		}
	});
	$('#todate').datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect : function(date){
			var selectedDate = new Date(date);
			$("#fromdate").datepicker( "option", "maxDate", selectedDate );
		}
	});
	$("#fromdate").datepicker("setDate", new Date());
	$("#todate").datepicker("setDate", new Date());
});
</script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<jsp:useBean id="PO" class="beans.purchaseorder"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<form id="sj" action="adminPannel.jsp" method="post">
<div class="search-list">
<ul>
<li>
<input type="hidden" name="page" value="purchaseOrdersList">
<select id="hoid" onchange="checkHoa();"> <!-- onchange="formSubmit();" -->
<option value="">ALL DEPARTMENTS</option>
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></li>
</ul>
</div></form>
<!-- <div class="icons">
<span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div> -->
<div class="clear"></div>
<div class="list-details">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">PURCHASE ORDERS FOR <%if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){ %> <%=HOA_L.getHeadofAccountName(request.getParameter("hoid")) %> <%}else{ %> ALL DEPARTMENTS <%} %> </td></tr>
</table>
<form action="adminPannel.jsp" method="get">
<input type="hidden" name="page" value="purchaseOrdersList">
<input type="text" id="fromdate" name="fromdate">
<input type="text" id="todate" name="todate">
<input type="submit" value="submit" class="click">
<input type = "hidden" name="hoid" id="head" value="">
</form>
<%
if (request.getParameter("fromdate") != null && request.getParameter("todate") != null)	{
	purchaseorderListing PUR_L = new purchaseorderListing();
	indentapprovalsListing APPR_L=new indentapprovalsListing();
	superadminListing SAD_l=new superadminListing();
	vendorsListing VEN_L=new vendorsListing();
	String hoid="";
	if(request.getParameter("hoid")!= null && !request.getParameter("hoid").equals("")){
		hoid=request.getParameter("hoid");
	}
	List PUR_List=PUR_L.getpurchaseordersBasedOnVendorAndHoa(hoid,request.getParameter("fromdate")+" 00:00:00",request.getParameter("todate")+" 23:59:59");
	SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat CDF=new SimpleDateFormat("dd MMMM yyyy HH:mm");
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 

	%>
	<%
	if(PUR_List.size()>0){%>
	    <style>
	.yourID.fixed {
	    position: fixed;
	    top: 0;
	    left: 0px;
	    z-index: 1;
	    width:96%;margin:0 2%;
	    background:#d0e3fb;
	}

	</style>
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
	<script>
	$(document).ready(function () {  
	  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
	  $(window).scroll(function (event) {
	    // what the y position of the scroll is
	    var y = $(this).scrollTop();

	    // whether that's below the form
	    if (y >= top) {
	      // if so, ad the fixed class
	      $('.yourID').addClass('fixed');
		      
	    } else {
	      // otherwise remove it
	      $('.yourID').removeClass('fixed');
		        
	    }
	  });
	});
	</script>

	<table width="100%" cellpadding="0" cellspacing="0">
	<tr><td colspan="7"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
	<tr>

	<td class="bg" width="3%" align="center">S.No.</td>
	<td class="bg" width="8%" align="center"><b>PurchaseOrder Invoice</b></td>
	<td class="bg" width="13%" align="center">Company</td>
	<td class="bg" width="22%" align="left">Vendor Name</td>
	<td class="bg" width="15%" align="center">Date</td>
	<td class="bg" width="10%" align="center">Requirement</td>
	<td class="bg" width="8%"  align="center">Status</td>

	</tr>
	</table></td></tr>
	<%List APP_Det=null;
	String approvedBy="";
	for(int i=0; i < PUR_List.size(); i++ ){
		PO=(beans.purchaseorder)PUR_List.get(i);
		 %>
	<tr>

	<td width="3%" align="center"><%=i+1%></td>
	<td width="8%" align="center"><a href="adminPannel.jsp?page=purchaseOrderDetailedReport&poinvid=<%=PO.getpoinv_id()%>"><b><%=PO.getpoinv_id() %></b></a></td>
	<td width="13%" align="center"><%=HOA_CL.getHeadofAccountName(PO.gethead_account_id())%></td>
	<td width="22%" align="left"><%=VEN_L.getMvendorsAgenciesName(PO.getvendor_id()) %></td>
	<%-- <td><ul>
	<li><span><a href="adminPannel.jsp?page=indentDetailedReport&invid=<%=INDT.getindentinvoice_id()%>"><%=INDT.getvendor_id()%>  </a></span><br /></li>
	</ul></td> --%>
	<td width="15%" align="center"><%=CDF.format(SDF.parse(PO.getcreated_date()))%></td>
	<td width="10%" align="center"><%=PO.getrequiremrnt()%></td>
	<td width="8%" align="center"> <%=PO.getstatus()%></td>

	</tr>
	<%

	} %>
	</table>
	<%}else{%>
	<div align="center"><h1>No Purchase orders Raised Yet</h1></div>
	<%}
}
%>
</div>
</div>
</div>
