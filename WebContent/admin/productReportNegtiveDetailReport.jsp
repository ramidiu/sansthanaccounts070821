<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.products"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<jsp:useBean id="PRD" class="beans.products"></jsp:useBean>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!-- <link href="css/printStyle.css" type="text/css" rel="stylesheet" /> -->
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>

<link rel="stylesheet" href="admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
function productsearch(){

	  var data1=$('#productname').val();
	  var headID="3";
	  $.post('searchProducts.jsp',{q:data1,page:"",hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productname" ).autocomplete({source: availableTags}); 
			});
} 
function sortByVat(){
	var vat=$("#vat").val();
	$("#formLoad").submit();
	//location.reload('godwanForm.jsp?poid='+poid); 
	//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">
          <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;margin:0 2%;
    width:96%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<div class="icons">
					<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						

					</span>
				</div>
<div class="vendor-box" style="">
<div class="list-details">
<div class="printable">
<table width="100%" cellpadding="0" cellspacing="0" class="date-wise " id="tblExport">

<tr><td colspan="10"><table width="100%" cellpadding="0" cellspacing="0" class="date-wise yourID">
<tr><td colspan="10" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Product Negative Balance Report</td></tr>
<tr>
<td class="bg" width="5%">S.No.</td>
<td class="bg" width="10%">PRODUCT ID</td>
<td class="bg" width="10%">Godwan Quantity</td>
<td class="bg" width="10%">Shop Quantity</td>
<td class="bg" width="10%">GODWAN OPENING STOCK</td>
<td class="bg" width="10%">MSE ENTRY</td>
<td class="bg" width="10%">STOCK TRANSFER</td>
<td class="bg" width="10%">SHOP OPENING STOCK</td>
<td class="bg" width="10%">SHOP TRANSFER </td>
<td class="bg" width="10%">SHOP SALE</td>
</tr></table></td></tr>


<tr><td><table width="100%" cellpadding="0" cellspacing="0" border="0"  id="" class="" style="border:none !important;">
<%
mainClasses.productsListing PRDL=new mainClasses.productsListing();
List PRODNEG_L=PRDL.getNegativeProductReport("3");
double openingbalance=0.0;
double mseentry=0.0;
double stocktransfer=0.0;
double actualgodwanstock=0.0;
double shopvarrience=0.0;
double shoptransferentry=0.0;
double saleentry=0.0;
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
DecimalFormat df=new DecimalFormat("#0.000");
godwanstockListing GDS_L=new godwanstockListing();
shopstockListing SHPS_L=new shopstockListing();
customerpurchasesListing CUSTP_L=new customerpurchasesListing();
for(int i=0; i < PRODNEG_L.size(); i++ ){
	PRD=(products)PRODNEG_L.get(i);
	openingbalance=Double.parseDouble(GDS_L.getTotalReceivedQuantityBasedHOAOfSecondOpenBal(PRD.getproductId(),PRD.gethead_account_id()));
	mseentry=Double.parseDouble(GDS_L.getTotalReceivedQuantityEntryHOA(PRD.getproductId(),PRD.gethead_account_id()));
	stocktransfer=Double.parseDouble(SHPS_L.getTotalTransferQuantityBasedDate(PRD.getproductId(),PRD.gethead_account_id()));
	actualgodwanstock=openingbalance+mseentry-stocktransfer;
		%>
	<tr>
<td class="bg" width="5%"><%=i+1 %></td>
<td class="bg" width="10%"><%=PRD.getproductId() %></td>
<td class="bg" width="10%"><%=PRD.getbalanceQuantityGodwan() %></td>
<td class="bg" width="10%"><%=PRD.getbalanceQuantityStore() %></td>
<td class="bg" width="10%"><%=openingbalance %></td>
<td class="bg" width="10%"><%=mseentry %> </td>
<td class="bg" width="10%"><%=stocktransfer %></td>
<%openingbalance=Double.parseDouble(SHPS_L.getTotalReceivedQuantityBasedHOAOfSecondOpenBal(PRD.getproductId(),PRD.gethead_account_id()));
shoptransferentry=Double.parseDouble(SHPS_L.getTotalReceivedQuantityEntryHOA(PRD.getproductId(),PRD.gethead_account_id()));
saleentry=Double.parseDouble(CUSTP_L.getTotalSaleQuantityBasedHOAOnDate(PRD.getproductId(),PRD.gethead_account_id()));
actualgodwanstock=openingbalance+shoptransferentry-saleentry; %>
<td class="bg" width="10%"><%=openingbalance %></td>
<td class="bg" width="10%"><%=shoptransferentry %> </td>
<td class="bg" width="10%"><%=saleentry %></td>
</tr>
	<%
	}%>


	</table></td></tr>
</table>
</div>
    
    
    
    </div>
<!--<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>


<div class="list-details">


    
    
</div>
</div>-->
</div>

</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>

