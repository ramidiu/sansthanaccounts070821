<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.util.List"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.productsListing"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>productexpensesdetails</title>

<script type="text/javascript">

function calculateAmount(){
	var count=$('#count').val();
	var totalAmount=0;
	for(var i=0;i<count;i++){
		var amount=$('#amount'+i).val();
		totalAmount=Number(Number(parseFloat(parseFloat(totalAmount))+Number(parseFloat(amount))));
	}
	
	$('#totalAmount').val(Number(totalAmount).toFixed(2));
}
</script>
<script type="text/javascript">
function validateForm(){
	var count=$('#count').val();
	for(var i=0;i<count;i++){
		var amount=$('#amount'+i).val();
		if(amount==""){
			$('#amount'+i).focus().css('outline','solid red 2px');
			return false;
		}
	}
}
</script>
</head>
<body>

<%
   majorheadListing MJR_L=new majorheadListing();
   minorheadListing MIN_L=new minorheadListing();
   productsListing PRD_L=new productsListing();
   String mseId=request.getParameter("mseId");
   productexpensesListing PL=new productexpensesListing();
   List<productexpenses> expensesList=PL.getProdExpensesDetails(mseId);
   
   if(expensesList!=null && expensesList.size()>0){
 
%>

<div class="vendor-page">

<div class="vendor-box">
<div class="col-md-6">
<h1>Expenses Entry Details</h1>
</div>
<div class="col-md-6">
	<a href="adminPannel.jsp?page=gowdonstockentryupdate&mseId=<%=mseId%>"" style="float: right;margin: 0 80px;">Go To Master Stock Entry Details</a>
</div>
<div class="vender-details">

					<form action="productexpensesdbupdate.jsp" onsubmit="return validateForm();">	
                     <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
                     <input type="hidden" name="mseId"  id="mseId"value="<%=mseId%>">
						<tbody>
						<tr>
							    <td class="bg" width="5%">S.NO.</td>
							    <td class="bg" width="8%">EpivId</td>
							     <td class="bg" width="8%">MajorHead</td>
							    <td class="bg" width="8%">Minor Head</td>
							    <td class="bg" width="8%">SubHead</td>
							    <td class="bg" width="15%">Amount</td>
						</tr>
						</tbody>
						
							<tbody>
							
							<%
							double totalAmount=0.0;
							  for(int i=0;i<expensesList.size();i++){
								  productexpenses pe=(productexpenses)expensesList.get(i);
								  totalAmount=totalAmount+Double.parseDouble(pe.getamount());
							%>
							<input type="hidden" name="expensesId<%=i%>" value="<%=pe.getexp_id()%>">
							<input type="hidden" name="count"  id="count"value="<%=expensesList.size()%>">
							<tr>
							    
							    <td><%=i+1%></td>
								<td><input type="text" name="expenseId<%=i%>" id="expenseId<%=i%>" value="<%=pe.getexpinv_id()%>" readonly></td>
								<td><input type="text" name="majorHead<%=i%>" id="majorHead<%=i%>" value="<%=pe.getmajor_head_id()%> <%=MJR_L.getmajorheadName(pe.getmajor_head_id()) %>" readonly></td>
								<td><input type="text" name="minorHead<%=i%>" id="minorHeadId<%=i%>" value="<%=pe.getminorhead_id()%> <%=MIN_L.getminorheadName(pe.getminorhead_id()) %>" readonly></td>
								<td><input type="text" name="subHeadId<%=i%>" id="subHeadId<%=i%>" value="<%=pe.getsub_head_id()%> <%=PRD_L.getProductsNameByCat(pe.getextra2(), pe.gethead_account_id())%>" readonly></td>
								<td><input type="text" name="amount<%=i%>" id="amount<%=i%>" value="<%=pe.getamount()%>" onkeyup="calculateAmount();"></td>
							</tr>	
							
							
							<%
							
							  }
							%>
							
							<tr>
								<td colspan="6" align="right">Total Amount</td>
								<td><input type="text" name="totalAmount" id="totalAmount" value="<%=totalAmount%>" style="width:55px;"></td>
							</tr>
							<tr>
								<td colspan="6"><input type="submit" value="Update" class="click" style="border:none; float:right; margin:0 115px 0 0"></td>
							</tr>
							
						</tbody></table>
 					</form>
						</div>
					</div>
				</div>
				
				<%
				} 
				
				%>

</body>
</html>