<%@page import="beans.vendors"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PAYMENTS ISSUE</title>
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>

function roundoff(name){
	var total=$("#totalamont1").val();
	var roundtotal=$("#roundofftotal").val();
	if(roundtotal==""){
		alert("please enter amount to add or subract from total");
			} else if(name=="plus"){
				total=Number(total)+Number(roundtotal);
				total=total.toFixed(2);
				$("#totalamont").val(total);
				$('#totalamountcharges').val(total);
				$("#typeofround").val('plus');
			} else if(name=="minus"){
				total=Number(total)-Number(roundtotal);
				total=total.toFixed(2);
				$("#totalamont").val(total);
				$('#totalamountcharges').val(total);
				$("#typeofround").val('minus');
					}

}
$(document).ready(function(){
$("#bankId").keyup(function(){
	  var hoid=$("#head_account_id").val();
	  var data1=$("#bankId").val();
	  var arr = [];
	  /* $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data) */
	  /* $.post('searchBanks2.jsp',{q:data1,HOA:hoid},function(data) */
	$.post('searchBanks3.jsp',{q:data1,HOA:hoid},function(data)		  
	  {
		  var response = data.trim().split("\n");
		 var bankNames=new Array();
		 var doctorIds=new Array();
		 var bankTotAmt=new Array();
		 var bankPettyCashAmt=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 bankNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
				 bankTotAmt:d[2],
				 bankPettyCashAmt:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		 $( "#bankId" ).autocomplete({source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$("#bankTotalAmt").val(ui.item.bankTotAmt);
					$("#bankPettyCashAmt").val(ui.item.bankPettyCashAmt);
					$("#bnkTotAmt").text("BANK AMOUNT : "+ui.item.bankTotAmt);
					$("#bnkPettyCash").text("PETTYCASH AMOUNT : "+ui.item.bankPettyCashAmt);
					
				}	 
			 }); 
			});
});
$(".NEFTBlock").hide();
$("#chequeMode").on('change', function() {
	  var type=$("#chequeMode").val();
	  var vendorId=$("#vendorId").val();
	  if(vendorId!="Other Bills"){
	  if(type=="NEFT"){
		  $(".NEFTBlock").show();
		  }else{
			  $(".NEFTBlock").hide();
	  }
	  }
});
});

$(document).ready(function(){
	$("#toBankId").keyup(function(){
		  var data1=$("#toBankId").val();
		  var arr = [];
		  /* $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data) */
		  $.post('searchBanks2.jsp',{q:data1},function(data)		  
		  {
			 var response = data.trim().split("\n");
			 var bankNames=new Array();
			 var doctorIds=new Array();
			 var bankTotAmt=new Array();
			 var bankPettyCashAmt=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 bankNames.push(d[0] +" "+ d[1]);
				 arr.push({
					 label: d[0]+" "+d[1],
					 bankTotAmt:d[2],
					 bankPettyCashAmt:d[3],
				        sortable: true,
				        resizeable: true
				    });
			 }
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=arr;
			 $( "#toBankId" ).autocomplete({source: availableTags,
				 focus: function(event, ui) {
						// prevent autocomplete from updating the textbox
						event.preventDefault();
						// manually update the textbox
						$(this).val(ui.item.label);
						$("#toBankName").val(ui.item.bankNames);
					}	 
				 }); 
				});
	});
	});


$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#TBECDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});

$(document).ready(function() {
	 $('input[type=radio][name=tranf]').click(function() {
	        if (this.value == 'tranf') {
				$(".td1").show();
				$(".td2").show();
	        }
	    });
	});

function chequeDetails(){
  $(".chequeDetails").toggle(); 
  var type=$("#paymentType").val();
  document.getElementById('bankId').value="";
  document.getElementById('totalamont').value="0.00";
  var countersSize=$("#invoicesSize").val();
  for(var j=0;j<countersSize;j++){
		$("#check"+j).attr('checked', false);
	}
  if(type=="cheque"){
	  $("#chequeNo").attr('required','required');
	  $("#nameOnCheque").attr('required','required');
  }else{
	  $("#chequeNo").removeAttr('required','required');
	  $("#nameOnCheque").removeAttr('required','required');
  }
}
function depositAmountCal(i){
	var countersSize=$("#invoicesSize").val();
	 if($("#bankId").val()!=null && $("#bankId").val()!=""){
		var selectAmount=$("#check"+i).val();
		var totAmt=$("#totalamont").val();
		if($("#check"+i).is(':checked')){
			totCalAmt=Number(totAmt)+Number(selectAmount);
			$("#totalamont1").val(totCalAmt);
			$("#totalamont").val(totCalAmt);
			$('#totalamountcharges').val(totCalAmt);
			$('#totalamont').attr('readonly', 'readonly');
		}else{
			totCalAmt=Number(totAmt)-Number(selectAmount);
			$("#totalamont1").val(totCalAmt);
			$("#totalamont").val(totCalAmt);
			$('#totalamountcharges').val(totCalAmt);
			$('#totalamont').attr('readonly', 'readonly');
		}
		
	    var casherIDs="";
	    var billAmt="";
	    var dcNos = ""; // added by gowri shankar
		 for(var j=0;j<countersSize;j++){
			 if($("#check"+j).is(':checked')){
					var dc = $('#dcno'+j).val(); // added by gowri shankar
					if (dc !== undefined && $.trim(dc) !== "")	{
						dcNos=dc;
					} // added by gowri shankar
				 if(casherIDs!="" && billAmt!=""){
						casherIDs=casherIDs+","+$("#sjID"+j).val();
						billAmt=billAmt+","+$("#billAmount"+j).val();
					}else{
						casherIDs=$("#sjID"+j).val();
						billAmt=$("#billAmount"+j).val();
					}
				 	$("#submit").removeAttr("disabled");
					$("#expInvIds").val(casherIDs);
					$("#billAmt").val(billAmt);
					$("#vendorId").val($("#vendor").val());
					$('#dcNumbers').val(dcNos); // added by gowri shankar
				}
		 }
	if($("#totalamont").val()=='0'){
		$("#submit").attr("disabled","disabled");
		$("#totalamont").removeAttr("readonly");
	}
	if($("#paymentType").val()=="cashpaid"){
		var pettyCash=$("#bankPettyCashAmt").val();
		if(totCalAmt>pettyCash){
			$("#totalamont").val("0");
			$('#totalamountcharges').val('0');
			$("#submit").attr("disabled","disabled");
			alert("Total Bank Petty Cash Amount is : "+pettyCash+"\n \n This Payment will not be proceed because the bank amount is  negative");
			for(var j=0;j<countersSize;j++){
				$("#check"+j).attr('checked', false);
			}
		}
	}else if($("#paymentType").val()=="cheque"){
		var message="";
		var bankTotCash=$("#bankTotalAmt").val();
		if(totCalAmt>0){
		if(totCalAmt>bankTotCash){
			message="Amount to vendor :"+totCalAmt;
		
		
		if($("#tdscatgy").is(':checked'))
		{
			message=message+"\n Amount For TDS Charges :"+$('#TDSCharges').val();
		}
		if($("#servicetaxcatgy").is(':checked'))
		{
			message=message+"\n Amount For Service Tax:"+$('#Servicetaxcharges').val();
		}
		alert("Total Bank "+message+"\n \n This Payment will not be proceed because the bank amount is  negative");
		$('#totalamont').val("0");
		$('#expInvIds').val("");
		for(var j=0;j<countersSize;j++){
			$("#check"+j).attr('checked', false);
		}
		}
		} else{
			alert("Total amount should not be less than Rs: 0/-");
		}
	}
	 }else{
		 alert("Please Select Bank First");
		 $("#bankId").focus();
		 for(var j=0;j<countersSize;j++){
				$("#check"+j).attr('checked', false);
			}
	 }
}
function formSubmit(){
	var vendor=$("#vendor").val();
	$("#vendorDet").submit();
	$.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    }});
	if(vendor!=null && vendor==""){
		alert("Please select vendor");
		$("#vendor").focus();
	}
}
$(document).ready(function() {
	$('#payments').on('submit', function(e){
     	var count = $('#count').val();
		for (var i = 0 ; i < count ; i++)	{
			var dcno = $('#dcno'+i).val();
			if ($.trim(dcno) !== "")	{
				if($("#check"+i).is(':checked'))	{
				}
				else	{
					alert("Please Select all Dc Bills");
					return false;
				}
			}
		}
     	var vendor=$("#vendor option:selected").text();
     	var head_account_id=$("#head_account_id option:selected").text();
     	var paymentType=$("#paymentType option:selected").text();
     	var totalamont=$("#totalamont").val();
		var status = confirm('You have entered bellow details.Please check : \n\n HEAD OF ACCOUNT : '+head_account_id+' \n VENDOR : '+vendor+'\n PAYMENT TYPE : '+paymentType+'\n TOTAL AMOUNT : '+totalamont+' \n\n Click OK to Proceed ');
		   if(status == false){
		   return false;
		   }
		   else{
			   $.blockUI({ css: { 
			        border: 'none', 
			        padding: '15px', 
			        backgroundColor: '#000', 
			        '-webkit-border-radius': '10px', 
			        '-moz-border-radius': '10px', 
			        opacity: .5, 
			        color: '#fff' 
			    }});
		   return true; 
		   }
    });
	
	$('#TDSCharges').keyup(function() {
		if(Number($('#totalamont').val())>0){
		var tdscharges=$('#TDSCharges').val();
		var servicetaxcharges=0;
		var totAmt=$('#totalamont').val();
		var totAmt1=$('#totalamont1').val();
		if($("#servicetaxcatgy").is(':checked'))
		{
			servicetaxcharges=$('#Servicetaxcharges').val();
		} 
		
		var totCalcAmt=Number(totAmt1)-Number(tdscharges)-Number(servicetaxcharges);
		$('#totalamont').val(totCalcAmt);
		$('#totalamountcharges').val(totCalcAmt);
		//$('#totalamont1').val(totCalcAmt);
		} else{
			$('#TDSCharges').val('0');
		}
	});
	$('#Servicetaxcharges').keyup(function() {
		if(Number($('#totalamont').val())>0){
		var tdscharges=0;
		var servicetaxcharges=$('#Servicetaxcharges').val();
		var totAmt=$('#totalamont').val();
		var totAmt1=$('#totalamont1').val();
		if($("#tdscatgy").is(':checked'))
		{
			tdscharges=$('#TDSCharges').val();
		}
	
		var totCalcAmt=Number(totAmt1)-Number(servicetaxcharges)-Number(tdscharges);
		$('#totalamont').val(totCalcAmt);
		$('#totalamountcharges').val(totCalcAmt);
		//$('#totalamont1').val(totCalcAmt);
	}
else{
	$('#Servicetaxcharges').val('0');	
		}
	});

});
$(document).ready(function(){
	$('.serviceblock input').attr('readonly', 'readonly');
	$('.tdsblock input').attr('readonly', 'readonly');
		$('.catchange').change(function() {
	if($("#servicetaxcatgy").is(':checked'))
		{
		$('.serviceblock input').removeAttr("readonly");
		} else{
			$('.serviceblock input').attr('readonly', 'readonly');
		}
	if($("#tdscatgy").is(':checked'))
	{	$('.tdsblock input').removeAttr("readonly");
	}else{
		$('.tdsblock input').attr('readonly', 'readonly');
	}
	});
	});
	</script>
	 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.blockUI.js"></script>
<!--Date picker script  -->
</head>
<body>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<jsp:useBean id="EXP" class="beans.productexpenses"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="VEN" class="beans.vendors"/>
<div class="vendor-page">
<div class="vendor-list">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">Payments Issue </td></tr>
</table>
<form name="vendorDet" id="vendorDet" action="adminPannel.jsp" method="get">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td><input type="hidden" name="page" value="multipleBills-ChequeIssue">Head Of account</td>
	<td>Vendors</td>
	
</tr>
<tr>
	<td>
	<select name="head_account_id" id="head_account_id" onchange="formSubmit();">
	<%
	mainClasses.productexpensesListing PRO_EXPL=new mainClasses.productexpensesListing();
	DecimalFormat DF=new DecimalFormat("#.00");
	mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
	List HA_Lists=HOA_L.getheadofaccounts();
	if(HA_Lists.size()>0){
		for(int i=0;i<HA_Lists.size();i++){
			HOA=(beans.headofaccounts)HA_Lists.get(i);%>
				<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("head_account_id")!=null && request.getParameter("head_account_id").equals(HOA.gethead_account_id())){ %> selected="selected" <%} %> ><%=HOA.getname() %></option>
			<%}} %>
	</select></td>
	<td>
	<select name="vendor" id="vendor" onchange="formSubmit();">
	<option value="">Select Vendor</option>
	<%vendorsListing VENL=new  vendorsListing();
	String hoid="1";
	if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
		hoid=request.getParameter("head_account_id");
	}
	List VEND=PRO_EXPL.getVendors(hoid);
	//List VEND=VENL.getvendors();
	if(VEND.size()>0){
		for(int i=0;i<VEND.size();i++){
		EXP=(beans.productexpenses)VEND.get(i);
		%>
		<option value="<%=EXP.getvendorId()%>" <%if(request.getParameter("vendor")!=null && request.getParameter("vendor").equals(EXP.getvendorId())){ %> selected="selected" <%} %>><%=VENL.getMvendorsAgenciesName(EXP.getvendorId()) %></option>
	<%}} %>
	<option value="Other Bills">OTHERS (BILLS WITH OUT VENDORS)</option>
	</select>
	</td>
</tr>
</table>

</form>
<%if(request.getParameter("vendor")!=null && (!request.getParameter("vendor").equals("") || request.getParameter("vendor").equals("Other Bills"))){ %>
<div class="vender-details">
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
List VendorL=VENL.getMvendors(request.getParameter("vendor"));
if(VendorL.size()>0){
	VEN=(vendors)VendorL.get(0);
}
%>
<form name="payments" id="payments" method="post" action="expensesInsert.jsp" onsubmit="blockPage();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>&nbsp;</td>
<td style="display:none;" class="td1">To Bank</td>
</tr>
<tr>
<td>
	<input type="radio" name="tranf" id="tranf" value="tranf" />Transfer
	</td>
	<td style="display:none;" class="td2">
	<input type="text" name="toBankId" id="toBankId" value="" onkeyup="banksearch()" placeholder="find a bank here" autocomplete="off" />
</td>
</tr>
<tr>
<td>Date : </td>
<td>Payment type</td>
<td>Bank name <span style="color: red;">*</span></td>
<td>Amount</td>
<td>Round Off Total </td>
<td>Narration<span style="color: red;">*</span></td>
</tr>
<tr>
<td>
<input type="hidden" name="expInv_id" id="expInvIds" value="">
<input type="hidden" name="vendorId" id="vendorId" value="<%=request.getParameter("vendor")%>">
<input type="hidden" name="billAmt" id="billAmt" value="">
<input type="hidden" name="bankTotalAmt" id="bankTotalAmt" value="">
<input type="hidden" name="bankPettyCashAmt" id="bankPettyCashAmt" value="">
<input type="hidden" name="toBankName" id="toBankName" value="">
<input type="text" name="date" id="date" value="<%=currentDate%>" style="width: 80px;" /> </td>
<td>
<select name="paymentType" id="paymentType" onchange="chequeDetails();">
	<option value="cashpaid">Cash</option>
	<option value="cheque">Cheque</option>
</select>
</td>
<td><input type="text" name="bankId" id="bankId" value="" onkeyup="banksearch()" placeholder="find a bank here" autocomplete="off" required/></td>
<td><input type="text" name="totalamont" id="totalamont" class="" value="0.00" onKeyPress="return numbersonly(this, event,true);" required/>
<input type="hidden" name="totalamont1" id="totalamont1" class="" value="0.00" onKeyPress="return numbersonly(this, event,true);" required/></td>
<td><img alt="plus" src="../images/plus.png" onclick="roundoff('plus')"> <input type="text" name="roundofftotal" id="roundofftotal" class="" value="0.0" onKeyPress="return numbersonly(this, event,true);"  />
<input type="hidden" name="typeofround" id="typeofround" class="" value=""  /> <img alt="minus" src="../images/minus.png" onclick="roundoff('minus')"> </td>
<td><input type="text" name="narration" id="narration" value="" required placeholder="Enter Narration"  onkeyup="javascript:this.value=this.value.toUpperCase();"></input></td>
</tr>
		<tr class="chequeDetails" style="display: none;">
				<td>
			<select name="chequeMode" id="chequeMode">
				<option value="RTGS">RTGS</option>
				<option value="NEFT">NEFT</option>
				<option value="CrossedCheque">CROSSED CHEQUE</option>
				<option value="Yourself">YOURSELF</option>
				<option value="Self">SELF CHEQUE</option>
			</select>
		</td>
<td colspan="1"><input type="text" name="nameOnCheque" id="nameOnCheque" value="" placeholder="Vendor Name on cheque" ></input></td>
<td colspan="1"><input type="text" name="chequeNo" id="chequeNo"   maxlength="6" placeholder="Vendor cheque no" value="" ></input></td>
<td colspan="1">
<input type="text" name="totalamountcharges" id="totalamountcharges" value="0.0" class="NEFTBlock" readonly/>
</td>
<td colspan="1">
<input type="text" name="vendorbankname" id="vendorbankname" value="<%=VEN.getbankName() %>" class="NEFTBlock" placeholder="vendor bank name"/></td>
<td colspan="1">
<input type="text" name="vendorbankaccountno" id="vendorbankaccountno" value="<%=VEN.getaccountNo() %>"  class="NEFTBlock" placeholder="vendor Account no"/></td>
<td colspan="1">
<input type="text" name="vendorifscode" id="vendorifscode" value="<%=VEN.getIFSCcode() %>" class="NEFTBlock" placeholder=" IFS code"/></td>


</tr>

<tr class="chequeDetails" style="display: none;">
<td><input type="checkbox" name="tdscatgy" id="tdscatgy" class="catchange" value="tdstax"/>T.D.S Charges</td>

<td colspan="1" class="tdsblock"><input type="text" name="TDSnameOnCheque" id="TDSnameOnCheque" value="" placeholder="Enter T.D.S name on cheque" ></input></td>
<td colspan="1" class="tdsblock"><input type="text" name="TDSchequeNo" id="TDSchequeNo"   maxlength="6" placeholder="T.D.S cheque no" value="" ></input></td>
<td colspan="2" class="tdsblock">
<input type="text" name="TDSCharges" id="TDSCharges" value=""  placeholder="Enter TDS Charges" onKeyPress="return numbersonly(this, event,true);">
</td>
</tr>
<tr class="chequeDetails" style="display: none;">
<td><input type="checkbox" name="servicetaxcatgy" id="servicetaxcatgy" class="catchange" value="servicetax"/>Service Tax</td>
<td colspan="1" class="serviceblock"><input type="text" name="ServiceTaxnameOnCheque" id="ServiceTaxnameOnCheque" value="" placeholder="Service Tax on cheque" ></input></td>
<td colspan="1" class="serviceblock"><input type="text" name="ServiceTaxchequeNo" id="ServiceTaxchequeNo"   maxlength="6" placeholder="Service Tax cheque no" value="" ></input></td>
<td colspan="2" class="serviceblock">
<input type="text" name="Servicetaxcharges" id="Servicetaxcharges" value=""  placeholder="Enter Service Tax Charges" onKeyPress="return numbersonly(this, event,true);">
</td>
</tr>

<tr>
<td colspan="1"><input type="text" name="TBECResNo" id="TBECResNo" value="" placeholder="Enter TB/EC Resolution Number"></td>
<td colspan="1"><input type="text" name="TBECDate" id="TBECDate" value="" readonly placeholder="Select TB/EC Resolution Date"></td>

<td colspan="2" align="right"><input type="submit" name="submit" id="submit" value="PRINT PAY ORDER" disabled="disabled"></td></tr>
</table>
<input type="hidden" name="dcNumbers" id="dcNumbers" value=""> <!-- added by goeri shnkar -->
</form>
<div style="color: blue;"><span id="bnkTotAmt"> </span>  ||  <span id="bnkPettyCash"> </span> </div>
</div>
<div style="clear:both;"></div>
<div class="list-details">
<%
String status="Approved";
if(request.getParameter("status")!=null && !request.getParameter("status").equals("") ){
	status=request.getParameter("status");
}
mainClasses.indentListing IND_L = new mainClasses.indentListing();
mainClasses.indentapprovalsListing APPR_L=new mainClasses.indentapprovalsListing();
bankdetailsListing BNKL=new bankdetailsListing();
mainClasses.superadminListing SAD_l=new mainClasses.superadminListing();
godwanstockListing GODSL=new godwanstockListing();
List IND_List=IND_L.getindentGroupByIndentinvoice_id();
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
//List EXP_L=PRO_EXPL.getExpensesListGroupByInvoice(status);
System.out.println("status......."+status);
List EXP_L=PRO_EXPL.getExpensesListGroupByInvoiceAndVendor(status,request.getParameter("vendor"),request.getParameter("head_account_id"));
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
List pendingIndents=IND_L.getPendingIndents();%>
<%
if(EXP_L.size()>0){%>
<input type="hidden" name="invoicesSize" id="invoicesSize" value="<%=EXP_L.size()%>"> 
 <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();
    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
    }
  });
});
</script>
<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="12"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="2%"></td>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="6%">Invoice No  / Unique No</td>
<td class="bg" width="8%">DEPARTMENT</td>
<td class="bg" width="9%">Vendor Name</td>
<td class="bg" width="8%">VENDOR BILL NO</td>
<td class="bg" width="9%">Date</td>
<td class="bg" width="15%">Narration</td>
<td class="bg" width="8%">Other Charges</td>
<td class="bg" width="9%">Total Amount</td>
<td class="bg" width="8%">Status</td>
<td class="bg" width="15%">Approved By</td>
</tr>
</table></td></tr>
<%List APP_Det=null;
String approvedBy="";
String othercharges="";
double totalamount=0.0;
subheadListing SUBHL=new subheadListing();%>
<input type="hidden" id="count" value="<%=EXP_L.size()%>">
<%for(int i=0; i < EXP_L.size(); i++ ){
	EXP=(beans.productexpenses)EXP_L.get(i);
	APP_Det=APPR_L.getIndentDetails(EXP.getexpinv_id());
	if(APP_Det.size()>0){
		for(int j=0;j<APP_Det.size();j++){
			IAP=(beans.indentapprovals)APP_Det.get(j);
			if(j==0){
				approvedBy=SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME  ["+CDF.format(SDF.parse(IAP.getapproved_date()))+"]";
			}else{
				approvedBy=approvedBy+"<br></br>"+SAD_l.getSuperadminname(IAP.getadmin_id())+" --TIME ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"]";
			}
		}
	}
	totalamount=Double.parseDouble(EXP.getamount());
	othercharges=PRO_EXPL.getOtherCharges(EXP.getexp_id());
	if(othercharges!=null && !othercharges.equals("") && !othercharges.equals("0")){
		totalamount=totalamount+Double.parseDouble(othercharges);
	}
	%>
<tr>
<td width="2%">
<input type="hidden" name="sjID" id="sjID<%=i%>" value="<%=EXP.getexpinv_id()%>">
<input type="hidden" name="billAmount" id="billAmount<%=i%>" value="<%=totalamount%>"> 
<input type="hidden" name="vendor" id="vendor" value="<%=EXP.getvendorId()%>"> 
<input type="checkbox" name="check" id="check<%=i%>" value="<%=totalamount%>" onclick="depositAmountCal('<%=i%>')"></td>
<td width="3%"><%=i+1%></td>
<%-- <%if(!status.equals("Not-Approve")) {%>
<td><a href="adminPannel.jsp?page=expenses&invid=<%=EXP.getexpinv_id()%>&expflag=2"><b><%=EXP.getexpinv_id()%></b></a></td>
<%} else { %>
<td><a ><b><%=EXP.getexpinv_id()%></b></a></td>
<%} %> --%>
<td width="6%"><a ><b><%=EXP.getexpinv_id()%></b></a><br><%=EXP.getExtra6() %></td>
<td width="8%"><%=HOA_CL.getHeadofAccountName(EXP.gethead_account_id())%> </td>
<td width="9%"> <%if(VENL.getMvendorsAgenciesName(EXP.getvendorId())!=null && !VENL.getMvendorsAgenciesName(EXP.getvendorId()).equals("")){ %> <%=VENL.getMvendorsAgenciesName(EXP.getvendorId())%> <%}else{ %> <%=SUBHL.getMsubheadname(EXP.getsub_head_id()) %> <%} %> </td>
<td width="8%"><%=GODSL.getBillNumber(EXP.getextra5()) %></td>
<td width="9%"><%=df2.format(dateFormat.parse(EXP.getentry_date()))%></td>
<td width="15%"><%=EXP.getnarration()%></td>
<%	if(othercharges!=null && !othercharges.equals("") && !othercharges.equals("0") && EXP.getExtra11().trim().equals("")){ %>
<td width="8%"><%=othercharges%></td>
<%} else if (!EXP.getExtra11().trim().equals("")){ %> <!-- added by goeri shnkar -->
<td width="8%"><%=EXP.getExtra11() %><input type="hidden" id="dcno<%=i%>" name="dcno<%=i%>" value="<%=EXP.getExtra11().trim()%>"></td>
<%} 
else	{%>
<td width="8%"></td>
<%}%> <!-- added by goeri shnkar -->

<td width="9%"><%=DF.format(totalamount)%></td>
<td width="8%" ><%=EXP.getextra1()%></td>
<td width="15%"><%=approvedBy %></td>
</tr>



<%
totalamount=0.0;
approvedBy="";
othercharges="";
} %>
</table>
<%}else{%>
<div align="center"><h1>Payments invoices not found! </h1></div>
<%}%>

</div>
<%} %>
</div>
</div>
</body>
</html>