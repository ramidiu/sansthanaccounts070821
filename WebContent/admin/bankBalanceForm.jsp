<%@page import="beans.bankdetails"%>
<%@page import="java.util.Map"%>
<%@page import="com.accounts.saisansthan.MapOfHeads"%>
<%@page import="java.util.List"%>
<%@page import="beans.bankbalance"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.headofaccountsListing"%>      
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#id").keyup(function(){
	var data1=$("#id").val();
	  $.post('searchBanks2.jsp',{q:data1},function(data)
	  {
		  var response = data.trim().split("\n");
		 var bankNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 bankNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=bankNames;
		 $( "#id" ).autocomplete({source: availableTags}); 
			});
});
});
</script>
<script type="text/javascript">
function validate(){

	var cdtype = document.getElementById("cdtype").value;
	
	if(cdtype==""){
		alert("Please Select either Credit or Debit");
		document.getElementById("cdtype").focus();
		return false;
	}
}
	</script>	
</head>
<body>
<jsp:useBean id="BNKBAL" class="beans.bankbalance"/>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<%
if(session.getAttribute("adminId")!=null)
{%>
<div>
<div class="vendor-page" style="min-height:0px;">
<div class="vendor-box">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>

<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<script type="text/javascript">
function showOrHide()
{
	var type = $("#type").val();	
	if(type == "Counter Cash")
	{
		document.getElementById('td1').style.display = "none";
		document.getElementById('td3').style.display = "none";

		document.getElementById('td2').style.display = "block";
		document.getElementById('td4').style.display = "block";
	}
	
	else
	{
		document.getElementById('td1').style.display = "block";
		document.getElementById('td3').style.display = "block";

		document.getElementById('td2').style.display = "none";
		document.getElementById('td4').style.display = "none";
	}
}
</script>

<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">BANK BALANCE ENTRY FORM </td></tr>
</table>
<div class="vender-details">
<form  method="post" action="bankbalance_Insert.jsp" onsubmit="return validate();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>Type</td>
<td id="td1">Select Bank</td>
<td id="td2" style="display:none;">Select Head Of Account</td>
<td>Select Finanacial Year</td>
<td>Credit/Debit</td>
<td>Enter Amount</td>
<td>&nbsp;&nbsp;</td>
</tr>
<tr>
<td>
	<select name="type" id="type" onchange="showOrHide()">
		<option value="Bank">Bank</option>
		<option value="Petty Cash">Petty Cash</option>
		<option value="Counter Cash">Counter Cash</option>
	</select>
</td>
<td id="td3"><input type="text" name="id" id="id" value="" placeHolder="Find a bank here" style="height: 22px;width: 300px;" /></td>
<td id="td4" style="display:none;">
	<select name="hoaid" id="hoaid" style="width:65%">
		<option value="4">Sansthan</option>
		<option value="1">Charity</option>
		<option value="5">Sainivas</option>
	</select>
</td>

<td>
	<select name="financialYear" id="financialYear">
	<option value="2021-22">2021-22</option>
	<option value="2020-21">2020-21</option>
	<option value="2019-20">2019-20</option>
	<option value="2018-19">2018-19</option>
	<option value="2017-18">2017-18</option>
		<option value="2016-17">2016-17</option>
	</select>
</td>
<td>
    <select name="cdtype" id="cdtype">
	<option value="">----SELECT----</option>
	<option value="credit">CREDIT</option>
	<option value="debit">DEBIT</option>
	</select>
</td>

<td><input type="text" name="amount" id="amount" value="0" onKeyPress="return numbersonly(this, event,true);" /></td>
<td><input type="submit" value="SUBMIT" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
</div>

</div>
</div>
<form  name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 50px;padding-top: 30px;">
	<input type="hidden" name="page" value="bankBalanceForm"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
	
		<tr>
			<td style="width:14%;">Select Financial Year</td>
			<td style="width:15%;padding-left:19px;">Select Department</td>
		</tr>
		<tr>
		<%
		DecimalFormat formatter = new DecimalFormat("#,##,###.00");
		DecimalFormat sjf = new DecimalFormat("####");
		DecimalFormat df = new DecimalFormat("0.00");
		mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
		List HA_Lists=HOA_L.getheadofaccounts();
		MapOfHeads maps = new MapOfHeads();
		Map<String,bankdetails> banksMap = maps.getBanksMap();	 
	    %>	 
			 <td style="width:14%;">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();" style="width:165px;">
					<%@ include file="yearDropDown1.jsp" %>
					</select>
				  </td>
			
		<td  style="padding-left:19px">
		<select name="hoid" id="hoid" onchange="formSubmit();">
<option value="" >ALL DEPARTMENTS</option>
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></li></td>
			<td><input type="submit" value="SEARCH" class="click" style="margin-left:10px;"/></td>
		</tr>
	</table> 
</form>

<%

String fincialYear=request.getParameter("finYear");
String dept=request.getParameter("hoid");
System.out.println("dept..."+dept);
headofaccountsListing HOAL = new headofaccountsListing();
bankbalanceListing BNK_BAL = new bankbalanceListing();
List BNKBAL_List=BNK_BAL.getbankbalanceFinYearAndDept(fincialYear,dept); %>
 <div class="vendor-list">
<div class="clear"></div>
<div class="clear"></div>
<div class="list-details">
<%

 if(BNKBAL_List.size()>0){ %>
<table width="100%" cellpadding="0" cellspacing="0"  border="1">
<tr align="Left"><td style="text-align:center;font-weight:bold;" colspan="10">Reports</td></tr>
<tr>
<td class="bg" width="5%" align="center">S.No.</td>
<td class="bg" width="10%">Financial Year</td>
<td class="bg" width="25%">Bank Name/HOA</td>
<td class="bg" width="20%">Type</td>
<td class="bg" width="10%">Credit/Debit</td>
<td class="bg" width="10%">Amount</td>
<td class="bg" width="10%">Debit Amount</td>
<td class="bg" width="10%">Credit Amount</td>
</tr>
<% Double totalAmount1=0.0;

   double debitAmount=0.0;
   double creditAmount=0.0;

for(int i=0; i < BNKBAL_List.size(); i++ ){
	BNKBAL=(bankbalance)BNKBAL_List.get(i); 
	%>
<tr>
<td  width="5%" align="center"><%=i+1%></td>
<td width="10%"><%=BNKBAL.getExtra1() %></td>
<%if(BNKBAL.getType().equals("Counter Cash")){ %>
<td width="25%"><%=HOAL.getHeadofAccountName(BNKBAL.getExtra2())%></td><%}else{ %>
<td width="20%">(<%=BNKBAL.getBank_id()%>)<%=banksMap.get(BNKBAL.getBank_id()).getbank_name() %></td><%} %>
<td width="20%"><%=BNKBAL.getType()%></td>
<td width="10%"><%=BNKBAL.getExtra8()%></td>

 <%totalAmount1=Double.parseDouble(BNKBAL.getBank_bal())+totalAmount1;
 formatter.format((totalAmount1));%>
<td width="10%"><%=df.format(Double.parseDouble(BNKBAL.getBank_bal()))%></td>

<td width="10%"><%if(BNKBAL.getExtra8().equals("debit")){%>
<% debitAmount=Double.parseDouble(BNKBAL.getBank_bal())+debitAmount;
df.format((debitAmount));%>
<%=df.format(Double.parseDouble(BNKBAL.getBank_bal()))%><%}else{%>0.00<%}%></td>

<td width="10%"><%if(BNKBAL.getExtra8().equals("credit")){%>
<% creditAmount=Double.parseDouble(BNKBAL.getBank_bal())+creditAmount;
df.format((creditAmount)); %>
<%=df.format(Double.parseDouble(BNKBAL.getBank_bal()))%><%}else{%>0.00<%}%></td>
</tr>	

<%} %>
<tr>
<td colspan="5" align="right" style="color:orange;margin-right:20px;"><b style="margin-right:20px;">Total Amount: </b></td>
<td><%=formatter.format((totalAmount1))%></td>
<td><%=formatter.format((debitAmount))%></td>
<td><%=formatter.format((creditAmount))%></td>
</tr>
</table>
<%}%>


</div>
</div> 

</div>

<%}else{
	response.sendRedirect("index.jsp");
} %>

</body>
</html>