<%@page import="beans.vendors"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.accounts.saisansthan.vendorBalances"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Major Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>


<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;

}
.new-table td{
padding: 2px 0 2px 5px !important;}

@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
</style>

<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 

</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
        function minorhead(id){
        	$('#'+id).toggle();
        }
    </script>
   

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
</head>

<body>
<%
	/* String minorheadid = request.getParameter("subhid");
	System.out.println(minorheadid); */
	if(session.getAttribute("adminId")!=null){ 
		DecimalFormat df=new DecimalFormat("#,###.00");
		/* DecimalFormat DF=new DecimalFormat(""); */
		Calendar c1 = Calendar.getInstance(); 
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		String fromdate="";
		String onlyfromdate="";
		String todate="";
		String cashtype="online pending";
		String cashtype1="othercash";
		String cashtype2="offerKind";
		String cashtype3="journalvoucher";
		List minhdlist=null;
		minorheadListing MINH_L=new minorheadListing();
		String onlytodate="";
		productexpensesListing PRDEXP_L=new productexpensesListing();
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		todate=(dateFormat2.format(c1.getTime())).toString();
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromdate=(dateFormat2.format(c1.getTime())).toString();
		if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
									 fromdate=request.getParameter("fromDate");
								}
								if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
									todate=request.getParameter("toDate");
								}
		headofaccountsListing HOA_L=new headofaccountsListing();
	List headaclist=HOA_L.getheadofaccounts();
	String hoaid="1";
	if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
		hoaid=request.getParameter("head_account_id");
	}
	
	%>
<div class="vendor-page">

		<div>
				
		
			
				<form name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 70px;padding-top: 30px;">
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td colspan="2">Select Financial<br/>Year</td>
<td class="border-nn">&nbsp;&nbsp;From Date</td>
<td class="border-nn">To Date</td>
<td class="border-nn" colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td class="border-nn">Report Type</td>
</tr>
				<tr>
				<%-- <td colspan="2">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
					<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
					</select>
				</td> --%>
				
					
			<td colspan="2">
			<select name="finYear" id="finYear"  Onchange="datepickerchange();">
						<%@ include file="yearDropDown1.jsp" %>
					</select>
			</td> 
		<td class="border-nn">&nbsp;&nbsp;<input type="text"  name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>" /> </td>
				<td class="border-nn"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
				<input type="hidden" name="page" value="trailBalanceReport_new"/>
				 </td>
<td colspan="2" class="border-nn"><select name="head_account_id" id="head_account_id" onchange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
<!-- <option value="SasthanDev">Sansthan Development</option> -->
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><select name="cumltv" id="cumltv">
<option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option>
<option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option>
</select></td>
<td class="border-nn"><input type="submit" value="SEARCH" class="click"/></td>
</tr></tbody></table> </form>
				<% String finyr[]=null;	
				String headgroup="";
				String subhid="";
				double total=0.0;
				double credit=0.0;
				double debit=0.0;
				double creditincome=0.0;
				double debitincome=0.0;
				double creditexpens=0.0;
				double debitexpens=0.0;
				double creditliab=0.0;
				double debitliab=0.0;
				Object key =null;
				Object value=null;
				Object key1 =null;
				Object value1=null;
				double value2=0.0;
				double amt1=0.0;
				double amt2=0.0;
				double amt3=0.0;
				double amt4=0.0;
				productexpensesListing PEXP_L=new productexpensesListing();
				banktransactionsListing BNK_L = new banktransactionsListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat onlydat2=new SimpleDateFormat("dd-MMM-yyyy");
				vendorsListing VNDR=new vendorsListing();
				vendors vendor = new vendors(); 
				vendorBalances VNDBALL=new vendorBalances();
				bankbalanceListing BBL =new bankbalanceListing();
				if(request.getParameter("head_account_id")!=null){
					hoaid=request.getParameter("head_account_id");
					if(hoaid.equals("SasthanDev")){
						headgroup="2";
						hoaid="4";
					} else if(hoaid.equals("4")){
						headgroup="1";
					}
				}
				subheadListing SUBH_L=new subheadListing();
				/* if(request.getParameter("subhid")!=null){
					subhid=request.getParameter("subhid");
					System.out.println(subhid);
					//hoaid=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid);
				} */
				/* onlyfromdate=fromdate;
				fromdate=fromdate+" 00:00:00";
				onlytodate=todate;
				todate=todate+" 23:59:59"; */
				customerpurchasesListing CP_L=new customerpurchasesListing();
				majorheadListing MH_L=new majorheadListing();
				
				/* List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid, "revenue",headgroup);
				List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid,"expenses",headgroup); */
				List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid, "revenue",headgroup);
				List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"expenses",headgroup);
				List assetsmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"Assets",headgroup);
				List liabilmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"Liabilities",headgroup);
				
				%>
				<div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
														
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span> <span>
						
					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	
	
	<div class="total-report">

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<% if(request.getParameter("fromDate") != null && !request.getParameter("toDate").equals("") && request.getParameter("finYear") != null){
		bankbalanceListing BBAL_L=new bankbalanceListing();
		String finYr = request.getParameter("finYear");
		String finStartYr = request.getParameter("finYear").substring(0, 4);
		String fdate = finStartYr+"-04-01"+" 00:00:00";
		if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("noncumulative")){
			fdate = request.getParameter("fromDate")+" 00:00:00";
		}
		String tdate = request.getParameter("toDate")+" 23:59:59";
		System.out.println(fdate+"<---->"+tdate);
	%>				
	<div class="printable" id="tblExport">	


<table width="90%" cellpadding="0" cellspacing="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
<tbody><tr>
						<td colspan="3" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hoaid) %></td>
					</tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Trail Balance Report</td></tr>
					<tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=onlydat2.format(onlydat.parse(fdate.substring(0, 10))) %> TO <%=onlydat2.format(onlydat.parse(request.getParameter("toDate"))) %> </td></tr>



</tbody></table>
<div style="clear:both"></div>

<table width="90%" border="1" cellspacing="0" cellpadding="0"  style="margin:0 auto;" class="new-table">
  <tbody><tr>
    <th width="38%" height="29" align="center">Account Name</th>
    <th width="29%" align="center">Debits</th>
    <th width="33%" align="center">Credits</th>
  </tr>
  
<!--   <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">INCOME</td>
  
  </tr> -->
  <% if(incomemajhdlist.size()>0){ 
								for(int i=0;i<incomemajhdlist.size();i++){
									MH=(majorhead)incomemajhdlist.get(i);
									double mjdbamt = Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3));
									double mjcdamt = Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid)) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3))+ Double.parseDouble(CP_L.getLedgerSumDollarAmt(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype1,hoaid));
									double debitAmountOpeningBal = 0.0;
									double creditAmountOpeningBal = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "");
										/* creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", ""); */
										creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Liabilites", finYr, "credit", "");
									}
									
									List otherAmountList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),"","",hoaid,fromdate,todate);
						  			//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
									double otherAmount = 0.0;
									if(otherAmountList.size()>0)
									{
										banktransactions OtherAmountList = (banktransactions)otherAmountList.get(0);
										if(OtherAmountList.getamount() != null)
										{
											otherAmount = Double.parseDouble(OtherAmountList.getamount());
										}
									}
									if(!("0.0".equals(""+mjdbamt)) || !("0.0".equals(""+mjcdamt)) || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){
								%>
  
  <tr>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;" ><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getmajor_head_id() %> <%=MH.getname() %></a></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
    <%debitincome=debitincome+debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3));%>
  <%-- <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td> --%>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+otherAmount+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid)) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype1,hoaid))) %></td>
  <%-- <%creditincome=creditincome+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid)); --%>
  <%creditincome=creditincome+creditAmountOpeningBal+otherAmount+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid)) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3))+ Double.parseDouble(CP_L.getLedgerSumDollarAmt(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype1,hoaid)); %>
  </tr>
  
  <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									double mindbamt = Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3));
								  double mincdamt = Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid));
								  double debitAmountOpeningBal2 = 0.0;
								  double creditAmountOpeningBal2 = 0.0;
								  if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
									  debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
									 /*  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", ""); */
									  creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "");
									  
								  }
								  otherAmountList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),"",hoaid,fromdate,todate);
						  			//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
									otherAmount = 0.0;
									if(otherAmountList.size()>0)
									{
										banktransactions OtherAmountList = (banktransactions)otherAmountList.get(0);
										if(OtherAmountList.getamount() != null)
										{
											otherAmount = Double.parseDouble(OtherAmountList.getamount());
										}
									}
								  //double amount =Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())));
									if(!("0.0".equals(""+mindbamt)) || !("0.0".equals(""+mincdamt)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){  
								%>
  
  <tr>
    <%-- <td height="28" style="color:#f00;"><strong><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Income and Expenditure" style="color:red;"><%=MNH.getname() %><%=MNH.getminorhead_id() %> </a></strong></td> --%>
    <td height="28" style="color:#f00;"><strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong></td>
    <td align="center" style="color:#f00;"><%=df.format(debitAmountOpeningBal2+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
    <%debitincome=debitincome+debitAmountOpeningBal2+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3));
    
    
    if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ %>
    <%-- <td align="center" style="color:#f00;"><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) %></td> --%>
  	<%-- <td align="center" style="color:#f00;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))%></td> --%>
	<td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+otherAmount+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fdate,tdate,"offerKind",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))%></td>
  <%} else{ %>
  <%-- <td align="center" style="color:#f00;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))))+df.format(Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td> --%>
  <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+otherAmount+Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid))) %></td>
  <%} %>	
  </tr>
  <%
				List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
         if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									//else
										//cashtype="cash";
										 //cashtype1="othercash";
										 //cashtype2="offerKind";
										//cashtype3="journalvoucher";
										//double amount1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));
										//double amount1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));
										double subdbamt = Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3));
										double subcdamt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) +  Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,cashtype1,hoaid));
										double debitAmountOpeningBal3 = 0.0;
										double creditAmountOpeningBal3 = 0.0;
										if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
											debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "");
											/* creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", ""); */
											creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "");
										}
										 otherAmountList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(),hoaid,fromdate,todate);
								  			//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
											otherAmount = 0.0;
											if(otherAmountList.size()>0)
											{
												banktransactions OtherAmountList = (banktransactions)otherAmountList.get(0);
												if(OtherAmountList.getamount() != null)
												{
													otherAmount = Double.parseDouble(OtherAmountList.getamount());
												}
											}
										if(!("0.0".equals(""+subdbamt)) || !("0.0".equals(""+subcdamt)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){  
								%>	
								
								
  
  <tr>
  <%if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <%-- <td height="28"><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></td> --%>
    <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname() %></td>
    <%} else{ %>
    <%-- <td height="28"><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></td> --%>
    <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname() %></td>
     <%} %>
    <td><%=df.format(debitAmountOpeningBal3+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
     <%debit=debit+debitAmountOpeningBal3+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3));
     if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <%-- <td><%=Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) %></td> --%>
    <td><%=df.format(creditAmountOpeningBal3+otherAmount+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))) %></td>
    <%-- <%credit=credit+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));} else{ %> --%>
    <%credit=credit+creditAmountOpeningBal3+otherAmount+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));} else{ %>
    <td><%=df.format(creditAmountOpeningBal3+otherAmount+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) +  Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,cashtype1,hoaid))) %></td>
  <%credit=credit+creditAmountOpeningBal3+otherAmount+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) +  Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,cashtype1,hoaid));} %>
  </tr>
  <%}}}}}}}}} %>
  
  
   <!-- <tr>
    <td height="28">Total(Rupees)</td>
    <td align="center">0.00</td>
    <td align="center">0.00</td>
  </tr> -->
  
  
   <!--  <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">EXPENDITURE</td>
  	</tr> -->
    
    <% if(expendmajhdlist.size()>0){ 
								for(int i=0;i<expendmajhdlist.size();i++){
									MH=(majorhead)expendmajhdlist.get(i);
									double debitAmountOpeningBal = 0.0;
									double creditAmountOpeningBal = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "");
										/* creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", ""); */
										creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Liabilites", finYr, "credit", "");
										
									}
									if(!PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid).equals("00") || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){
									
								%>
     <tr>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getmajor_head_id() %> <%=MH.getname() %></a></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
   <%debitexpens=debitexpens+debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3));%>
     <%if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid).equals("00")){ %>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,hoaid))) %></td>
    <%creditexpens=creditexpens+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid)); } else{ %>
   <%-- <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td> --%>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype3,"","",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)))%></td>
 <%creditexpens=creditexpens+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)); } %>
  </tr>
  
  <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									String amountexp = PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid);
									double amount2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid)); 
									double debitAmountOpeningBal2 = 0.0;
									double creditAmountOpeningBal2 = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
										/* creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", ""); */
										creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "");
									}
									if(!("00".equals(""+amountexp)) || !("0.0".equals(""+amount2)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){
								%>
    <tr>
    <%-- <td height="28" style="color:#f00;"><strong><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Income and Expenditure" style="color:red;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></strong></td> --%>
    <td height="28" style="color:#f00;"><strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong></td>
    <td align="center" style="color:#f00;"><%=df.format(debitAmountOpeningBal2+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
    <%debit=debit+debitAmountOpeningBal2+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3));
      if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ %>
    <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
     <%} else{ %>
     <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,"","",cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) +Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3))) %></td>
     <%} %>
  </tr>
  <%
				List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
         if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										 cashtype1="othercash";
										 cashtype2="offerKind";
										cashtype3="journalvoucher";
										String amountexp2= PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid);
										String amount3 = CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",hoaid);
										String amount4 = CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid);
										String amount5= PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,cashtype3);
										double debitAmountOpeningBal3 = 0.0;
										double creditAmountOpeningBal3 = 0.0;
										if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
											debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "");
											/* creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", ""); */
											creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "");
										}
										if(!("00".equals(""+amountexp2)) || !("00".equals(""+amount3)) || !("00".equals(""+amount4)) || !("00".equals(""+amount5)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){  
								%>	
  
  
  
   <tr>
   <%
   if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <%-- <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td> --%>
    <td height="28"><strong><%=SUBH.getsub_head_id()%> <%=SUBH.getname() %></strong></td>
     <%} else{ %>
     <%-- <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td> --%>
     <td height="28"><strong><%=SUBH.getsub_head_id()%> <%=SUBH.getname() %></strong></td>
   <%} %>
    <td><%=df.format(debitAmountOpeningBal3+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
    <%debit=debit+debitAmountOpeningBal3+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3));
    if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <td><%=df.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))) %></td>
     <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));} else{ %>
     <td><%=df.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) ) %></td>
  <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) ;} %>
  </tr>
  
 <%}}}}}}}}} %>
<%-- <tr>
    <td align="center" style=" color: #934C1E; font-weight:bold;" height="28">TOTAL AMOUNT OF EXPENSES</td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">Rs.<%=df.format(debitexpens) %></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
</tr>
<tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Excess of Income over Expenditures</td>
  <%if(creditincome<debitexpens){	%>
    <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitexpens-creditincome)%></td>
    <%	/* creditincome=debitexpens; */
  }else{%>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome-debitexpens)%></td>
   <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
 <%/* debitexpens=creditincome; */%>
 <%}%>
</tr> 
<tr>
   <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome) %></td>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome) %></td>
</tr> --%>
<!-- <tr><td height="28" colspan="3"></td></tr> -->

<tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">ASSETS</td>
</tr>
    
<% if(assetsmajhdlist.size()>0){ 
	for(int i=0;i<assetsmajhdlist.size();i++){
		MH=(majorhead)assetsmajhdlist.get(i);
		double debitAmountOpeningBal = 0.0;
		double creditAmountOpeningBal = 0.0;
		if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
			debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "");
			/* creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", ""); */
			
			creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Liabilites", finYr, "credit", "");
		}
		if(!PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid).equals("00") || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){									
%>
     <tr>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getmajor_head_id() %> <%=MH.getname() %></a></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
   <%debitexpens=debitexpens+debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3));%>
     <%if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid).equals("00")){ %>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,hoaid))) %></td>
    <%creditexpens=creditexpens+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid)); } else{ %>
   <%-- <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td> --%>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype3,"","",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)))%></td>
 <%creditexpens=creditexpens+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)); } %>
  </tr>
  
  <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									String amountexp = PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid);
									double amount2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid)); 
									double debitAmountOpeningBal2 = 0.0;
									double creditAmountOpeningBal2 = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
										/* creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", ""); */
										creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "");
									}
									if(!("00".equals(""+amountexp)) || !("0.0".equals(""+amount2)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){
								%>
    <tr>
    <%-- <td height="28" style="color:#f00;"><strong><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Income and Expenditure" style="color:red;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></strong></td> --%>
    <td height="28" style="color:#f00;"><strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong></td>
    <td align="center" style="color:#f00;"><%=df.format(debitAmountOpeningBal2+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
    <%debit=debit+debitAmountOpeningBal2+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3));
      if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ %>
    <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
     <%} else{ %>
     <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,"","",cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) +Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3))) %></td>
     <%} %>
  </tr>
  <%
				List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
         if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										 cashtype1="othercash";
										 cashtype2="offerKind";
										cashtype3="journalvoucher";
										String amountexp2= PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid);
										String amount3 = CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",hoaid);
										String amount4 = CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid);
										String amount5= PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,cashtype3);
										double debitAmountOpeningBal3 = 0.0;
										double creditAmountOpeningBal3 = 0.0;
										if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
											debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "");
											/* creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", ""); */
											creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "");
										}
										if(!("00".equals(""+amountexp2)) || !("00".equals(""+amount3)) || !("00".equals(""+amount4)) || !("00".equals(""+amount5)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){  
								%>	
  
  
  
   <tr>
   <%
   if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <%-- <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td> --%>
    <td height="28"><strong><%=SUBH.getsub_head_id()%> <%=SUBH.getname() %></strong></td>
     <%} else{ %>
     <%-- <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td> --%>
     <td height="28"><strong><%=SUBH.getsub_head_id()%> <%=SUBH.getname() %></strong></td>
   <%} %>
    <td><%=df.format(debitAmountOpeningBal3+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
    <%debit=debit+debitAmountOpeningBal3+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3));
    if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <td><%=df.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))) %></td>
     <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));} else{ %>
     <td><%=df.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) ) %></td>
  <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) ;} %>
  </tr>
  
 <%}}}}}}}}} %>
 <%
 todate=todate+" 23:59:59";
 HashMap VendorPaymentPedning=VNDBALL.getVendorBalancesForFinYr1(hoaid,fdate,todate,request.getParameter("finYear"),request.getParameter("cumltv"));
 %>
 
 <tr>
    <td height="28" colspan="3" align="left" bgcolor="#CC9933" style="color:#fff; font-weight:bold;">LIABILITIES</td>
 </tr>
    
    <% if(liabilmajhdlist.size()>0){
								for(int i=0;i<liabilmajhdlist.size();i++){
									MH=(majorhead)liabilmajhdlist.get(i);
									double debitAmountOpeningBal = 0.0;
									double creditAmountOpeningBal = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "");
									/* 	creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "credit", ""); */
										creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Liabilites", finYr, "credit", "");
									}
									if(!PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid).equals("00") || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){
									
								%>
     <tr>
    <td height="28" align="center" style=" color: #934C1E; font-weight:bold;"><a href="#" onclick="minorhead('<%=MH.getmajor_head_id()%>');"><%=MH.getmajor_head_id() %> <%=MH.getname() %></a></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
   <%debitexpens=debitexpens+debitAmountOpeningBal+Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3));%>
     <%if(!CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid).equals("00")){ %>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid))+Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,hoaid))) %></td>
    <%creditexpens=creditexpens+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid)); } else{ %>
   <%-- <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead1(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,hoaid))) %></td> --%>
    <td align="center" style=" color: #934C1E; font-weight:bold;"><%=df.format(creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype3,"","",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)))%></td>
 <%creditexpens=creditexpens+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)); } %>
  </tr>
  
  <%	minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){  %>
			<%						for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									String amountexp = PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid);
									double amount2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",hoaid)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid)); 
									double debitAmountOpeningBal2 = 0.0;
									double creditAmountOpeningBal2 = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
										debitAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
										/* creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", ""); */
										creditAmountOpeningBal2 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "");
									}
									if(!("00".equals(""+amountexp)) || !("0.0".equals(""+amount2)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){
								%>
    <tr>
    <%-- <td height="28" style="color:#f00;"><strong><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Income and Expenditure" style="color:red;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></strong></td> --%>
    <td height="28" style="color:#f00;"><strong><%=MNH.getminorhead_id() %> <%=MNH.getname() %></strong></td>
    <td align="center" style="color:#f00;"><%=df.format(debitAmountOpeningBal2+Double.parseDouble(PEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
    <%debit=debit+debitAmountOpeningBal2+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3));
      if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())).equals("00")){ %>
    <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))) %></td>
     <%} else{ %>
     <td align="center" style="color:#f00;"><%=df.format(creditAmountOpeningBal2+Double.parseDouble(CP_L.getLedgerSumOfSubhead3(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,"","",cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id())))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()))) +Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3))) %></td>
     <%} %>
  </tr>
  <%
				List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
         if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										 cashtype1="othercash";
										 cashtype2="offerKind";
										cashtype3="journalvoucher";
										String amountexp2= PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid);
										String amount3 = CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",hoaid);
										String amount4 = CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid);
										String amount5= PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,cashtype3);
										double debitAmountOpeningBal3 = 0.0;
										double creditAmountOpeningBal3 = 0.0;
										if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
											debitAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "");
											/* creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", ""); */
											creditAmountOpeningBal3 = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "");
										}
										if(!("00".equals(""+amountexp2)) || !("00".equals(""+amount3)) || !("00".equals(""+amount4)) || !("00".equals(""+amount5)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){  
								%>	
  
  
  
   <tr>
   <%
   if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <%-- <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td> --%>
    <td height="28"><strong><%=SUBH.getsub_head_id()%> <%=SUBH.getname() %></strong></td>
     <%} else{ %>
     <%-- <td height="28" style="color:#C30;"><strong><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())%>" style="color:#000;"><%=SUBH.getname() %></a></strong></td> --%>
     <td height="28"><strong><%=SUBH.getsub_head_id()%> <%=SUBH.getname() %></strong></td>
   <%} %>
    <td><%=df.format(debitAmountOpeningBal3+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3))) %></td>
    <%debit=debit+debitAmountOpeningBal3+Double.parseDouble(PRDEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3));
    if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())).equals("00")){ %>
    <td><%=df.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))) %></td>
     <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())));} else{ %>
     <td><%=df.format(creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) ) %></td>
  <%credit=credit+creditAmountOpeningBal3+Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id())))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()))) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) ;} %>
  </tr>
  
 <%}}}}}}}}} %>
 
 <%
 if(VendorPaymentPedning.size()>0){
 Iterator VendorPayments = VendorPaymentPedning.keySet().iterator();
 	while(VendorPayments.hasNext()){
	 	key1 = VendorPayments.next();
	 	value1 = VendorPaymentPedning.get(key1);
	 	value2=value2+Double.parseDouble(value1.toString());
	}%>
<tr>
 	<td height="28" style="color:#f00;"><strong>SUNDRY CREDITORS</strong></td>
    <td align="center" style="color:#f00;"><%=df.format(value2)%></td>
    <td align="center" style="color:#f00;">0.00</td>
</tr> 
<% Iterator VendorPayments1 = VendorPaymentPedning.keySet().iterator(); 
      while(VendorPayments1.hasNext()){
	  key = VendorPayments1.next();
	  value = VendorPaymentPedning.get(key);
	  
	  double amount = Double.parseDouble(value.toString());
		  
		  if(key.equals("1435")){
			  System.out.println("amountamountamountamount =====? "+amount);
		  }
		if(amount <= 5 && amount >= -5){
			amount = 0;
		}
	  
		if(amount != 0){ 
	  /* if(Double.parseDouble(value.toString()) != 0 && !df.format(Double.parseDouble(value.toString())).equals("-0") && !df.format(Double.parseDouble(value.toString())).equals("0")){ */ %>
		  <tr>
	      	<td height="28"><%if(!key.toString().equals("")){
	      	System.out.println("Id "+key.toString());
	      	%>
				<%=key.toString()%> <%=VNDR.getMvendorsAgenciesName(key.toString())%>
				
				<%} else{%>
					Bill without Vendor
					<%}%>
			</td>
	        <td><%=df.format(Double.parseDouble(value.toString())) %></td>
	          <%debitliab=debitliab+Double.parseDouble(value.toString());%>
	         <td>0.00</td>
      	 </tr>
<%}}}%>
<%
String corpusFundMajorHead = "";
String corpusFundMinorHead = "";
String corpusFundSubHead = "";

if(hoaid.equals("4")){
	corpusFundMajorHead = "11";
   	corpusFundMinorHead = "473";
   	corpusFundSubHead = "21487";
}else if(hoaid.equals("1")){
	corpusFundMajorHead = "13";
   	corpusFundMinorHead = "476";
   	corpusFundSubHead = "21537";
}
if(hoaid.equals("4") || hoaid.equals("1")){ %>
<tr>
 <td height="28" style="color:#f00;"><strong>CORPUS FUND</strong></td>
<%
double income = 0.0; 
double expense = 0.0;
double incomeOverExpenditure = 0.0;

List incomemajhdlist2=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid, "revenue",headgroup);
List expendmajhdlist2=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"expenses",headgroup);

	if(incomemajhdlist2.size()>0){ 
		for(int i=0;i<incomemajhdlist2.size();i++){
			MH=(majorhead)incomemajhdlist2.get(i);
			/* income += Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",finStartYr+"-04-01 00:00:00",todate,cashtype,cashtype1,cashtype2,cashtype3,hoaid)) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",finStartYr+"-04-01 00:00:00",todate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MH.getmajor_head_id(),"major_head_id",finStartYr+"-04-01 00:00:00",todate,cashtype1,hoaid)); */
			income += Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid)) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype1,hoaid));
		}
	}
	if(expendmajhdlist2.size()>0){ 
		for(int i=0;i<expendmajhdlist2.size();i++){
			MH=(majorhead)expendmajhdlist2.get(i);
			/* expense += (Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",finStartYr+"-04-01 00:00:00",todate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",finStartYr+"-04-01 00:00:00",todate,cashtype3))); */
			expense += (Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3)));
		}
	}	

incomeOverExpenditure = income - expense;
%>

<%double amount = 0.0; 
if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	amount = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, corpusFundMajorHead, corpusFundMinorHead, corpusFundSubHead, "subhead","Assets", finYr, "", "");
}%>
<td align="center" style="color:#f00;"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(corpusFundSubHead,"sub_head_id",corpusFundMinorHead,"extra1",fdate,tdate,"","","","",hoaid)))%></td>
<td align="center" style="color:#f00;"><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(corpusFundSubHead,"sub_head_id",corpusFundMinorHead,"minorhead_id",fdate,tdate,cashtype3))+amount+incomeOverExpenditure)%></td>
<%debitliab = debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(corpusFundSubHead,"sub_head_id",corpusFundMinorHead,"minorhead_id",fdate,tdate,cashtype3))+amount+incomeOverExpenditure; %>
</tr> 
<%}%>
<tr>
 <td height="28" style="color:#f00;"><strong>OUTSTANDING LIABILITIES</strong></td>
 <td align="center" style="color:#f00;">0.00 </td>
 <td align="center" style="color:#f00;">
 <%
 double amt=0.0;
 if(request.getParameter("cumltv").equals("cumulative")){
 if(hoaid.equals("4")){amt=BBL.getMajorOrMinorOrSubheadOpeningBal("4", "11", "468", "", "minorhead","Assets", finYr, "", "");}
 if(hoaid.equals("1")){amt=BBL.getMajorOrMinorOrSubheadOpeningBal("1", "13", "469", "", "minorhead","Assets", finYr, "", "")+BBL.getMajorOrMinorOrSubheadOpeningBal("1", "61", "470", "", "minorhead","Assets", finYr, "", "")+BBL.getMajorOrMinorOrSubheadOpeningBal("1", "60", "471", "", "minorhead","Assets", finYr, "", "");}
 if(hoaid.equals("5")){amt=BBL.getMajorOrMinorOrSubheadOpeningBal("5", "15", "472", "", "minorhead","Assets", finYr, "", "");}
 }%>
 
 <%if(hoaid.equals("4")){%><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("468","minorhead_id","4","head_account_id",fdate,tdate,cashtype3))+amt)%><%}%>
 <%if(hoaid.equals("1")){%><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("469","minorhead_id","1","head_account_id",fdate,tdate,cashtype3))+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("470","minorhead_id","1","head_account_id",fdate,tdate,cashtype3))+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("471","minorhead_id","1","head_account_id",fdate,tdate,cashtype3))+amt)%><%}%>
 <%if(hoaid.equals("5")){%><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("472","minorhead_id","5","head_account_id",fdate,tdate,cashtype3))+amt)%><%}%>
 </td>
</tr>
<%if(hoaid.equals("4")){
 List subhdlist=SUBH_L.getSubheadListMinorhead("468");
 if(subhdlist.size()>0){ 
   for(int i=0;i<subhdlist.size();i++){	
		SUBH = (subhead)subhdlist.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	amount = BBL.getMajorOrMinorOrSubheadOpeningBal("4", "11", "468", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","468","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","468","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","468","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}} 
if(hoaid.equals("1")){
List subhdlist=SUBH_L.getSubheadListMinorhead("469");
	if(subhdlist.size()>0){ 
	  for(int i=0;i<subhdlist.size();i++){
	SUBH = (subhead)subhdlist.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "13", "469", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","469","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","469","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab = debitliab +Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","469","minorhead_id",fdate,tdate,cashtype3))+amount;%>
</tr>
<%}}
 List subhdlist1=SUBH_L.getSubheadListMinorhead("470");
 if(subhdlist1.size()>0){ 
 	for(int i=0;i<subhdlist1.size();i++){ 
 	SUBH = (subhead)subhdlist1.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "61", "470", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","470","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","470","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","470","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}
 List subhdlist2=SUBH_L.getSubheadListMinorhead("471");
 if(subhdlist2.size()>0){ 
	for(int i=0;i<subhdlist2.size();i++){
	SUBH = (subhead)subhdlist2.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "60", "471", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","471","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","471","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","471","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}}
if(hoaid.equals("5")){
 List subhdlist3=SUBH_L.getSubheadListMinorhead("472");
   if(subhdlist3.size()>0){ 
     	for(int i=0;i<subhdlist3.size();i++){   
     	SUBH = (subhead)subhdlist3.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	 amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "15", "472", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","472","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","472","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","472","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}}%>
<tr>
 <td height="28" style="color:#f00;"><strong>EMD</strong></td>
 <td align="center" style="color:#f00;">0.00</td>
 <td align="center" style="color:#f00;">
 <%double amt0=0.0;
 if(request.getParameter("cumltv").equals("cumulative")){
 if(hoaid.equals("4")){amt0=BBL.getMajorOrMinorOrSubheadOpeningBal("4", "31", "207", "", "minorhead","Assets", finYr, "", "");}
 if(hoaid.equals("SasthanDev")){amt0=BBL.getMajorOrMinorOrSubheadOpeningBal("4", "41", "303", "", "minorhead","Assets", finYr, "", "");}
 if(hoaid.equals("1")){amt0=BBL.getMajorOrMinorOrSubheadOpeningBal("1", "51", "404", "", "minorhead","Assets", finYr, "", "");}
 if(hoaid.equals("3")){amt0=BBL.getMajorOrMinorOrSubheadOpeningBal("3", "71", "505", "", "minorhead","Assets", finYr, "", "");}
 if(hoaid.equals("5")){amt0=BBL.getMajorOrMinorOrSubheadOpeningBal("5", "84", "639", "", "minorhead","Assets", finYr, "", "");}
 }%>
 <%if(hoaid.equals("4")){%><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("207","minorhead_id","4","head_account_id",fdate,tdate,cashtype3))+amt0)%><%}%>
 <%if(hoaid.equals("SasthanDev")){%><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("303","minorhead_id","4","head_account_id",fdate,tdate,cashtype3))+amt0)%><%}%>
 <%if(hoaid.equals("1")){%><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("404","minorhead_id","1","head_account_id",fdate,tdate,cashtype3))+amt0)%><%}%>
 <%if(hoaid.equals("3")){%><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("505","minorhead_id","3","head_account_id",fdate,tdate,cashtype3))+amt0)%><%}%>
 <%if(hoaid.equals("5")){%><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("639","minorhead_id","5","head_account_id",fdate,tdate,cashtype3))+amt0)%><%}%>
 </td>
</tr> 
<% 
if(hoaid.equals("4")){
  List subhdlist4=SUBH_L.getSubheadListMinorhead("207");
  if(subhdlist4.size()>0){ 
  	for(int i=0;i<subhdlist4.size();i++){  
  	SUBH = (subhead)subhdlist4.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	 amount = BBL.getMajorOrMinorOrSubheadOpeningBal("4", "31", "207", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","207","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","207","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","207","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}}
if(hoaid.equals("SasthanDev")){
 List subhdlist5=SUBH_L.getSubheadListMinorhead("303");
 if(subhdlist5.size()>0){ 
   for(int i=0;i<subhdlist5.size();i++){
   SUBH = (subhead)subhdlist5.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	 amount = BBL.getMajorOrMinorOrSubheadOpeningBal("4", "41", "303", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","303","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","303","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","303","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}}
if(hoaid.equals("1")){
 List subhdlist6=SUBH_L.getSubheadListMinorhead("404");
 if(subhdlist6.size()>0){ 
   for(int i=0;i<subhdlist6.size();i++){
	SUBH = (subhead)subhdlist6.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	 amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "51", "404", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","404","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","404","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","404","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}}
if(hoaid.equals("3")){
  List subhdlist7=SUBH_L.getSubheadListMinorhead("505");
  if(subhdlist7.size()>0){ 
   	for(int i=0;i<subhdlist7.size();i++){  
    	SUBH = (subhead)subhdlist7.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	 amount = BBL.getMajorOrMinorOrSubheadOpeningBal("3", "71", "505", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","505","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","505","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","505","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}}
if(hoaid.equals("5")){
   List subhdlist8=SUBH_L.getSubheadListMinorhead("639");
   if(subhdlist8.size()>0){ 
    	for(int i=0;i<subhdlist8.size();i++){ 
    	SUBH = (subhead)subhdlist8.get(i);
%>
<tr>
 <td height="28"><%=SUBH.getsub_head_id()%> <%=SUBH.getname()%></td>
 <%double amount = 0.0;
 if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
	 amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "84", "639", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
 }%>
 <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","639","extra1",fdate,tdate,"","","","",hoaid)))%></td>
 <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","639","minorhead_id",fdate,tdate,cashtype3))+amount)%></td>
 <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","639","minorhead_id",fdate,tdate,cashtype3))+amount; %>
</tr>
<%}}}%>
<tr>
    <td align="center" style=" color: #934C1E; font-weight:bold;" height="28">TOTAL AMOUNT OF EXPENSES</td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">Rs.<%=df.format(debitexpens) %></td>
    <td align="center" style=" color: #934C1E; font-weight:bold;">.00</td>
</tr>
<tr>
    <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Excess of Income over Expenditures</td>
  <%if(creditincome<debitexpens){	%>
    <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
    <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(debitexpens-creditincome)%></td>
    <%	/* creditincome=debitexpens; */
  }else{%>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome-debitexpens)%></td>
   <td align="center" style="font-weight:bold; color: #934C1E;">-</td>
 <%/* debitexpens=creditincome; */%>
 <%}%>
</tr> 
<tr>
   <td height="28" bgcolor="#996600" style="font-weight:bold; color:#fff">Total (Rupees)</td>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome) %></td>
   <td align="center" style="font-weight:bold; color: #934C1E;">Rs.<%=df.format(creditincome) %></td>
</tr>
 <!--  <tr>
    <td height="28"></td>
    <td align="center">0.00</td>
    <td align="center">0.00</td>
  </tr> -->
  <%-- <%System.out.println("credit==>"+credit);
System.out.println("debit===>"+debit);
System.out.println("creditincome==>"+creditincome);
System.out.println("creditexpens==>"+creditexpens);
System.out.println("debitincome==>"+debitincome);
System.out.println("debitexpens==>"+debitexpens);
 %> --%>
  
  <!--  <tr>
    <td height="28" colspan="3"></td>
  </tr> -->
  
  <!-- <tr>
    <td height="28"><strong>Total</strong></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr> -->


  
</tbody></table>
</div><%} %>
  </div>
			</div>
				
			</div>
			</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>