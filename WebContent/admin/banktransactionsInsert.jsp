<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.cashier_reportListing"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<jsp:useBean id="BKT" class="beans.banktransactionsService"/>
<jsp:useBean id="BNK" class="beans.bankdetailsService"/>
<jsp:useBean id="PEXP" class="beans.productexpensesService"/>
<jsp:useBean id="CSH" class="beans.cashier_reportService"/>
<jsp:useBean id="CASH" class="beans.cashier_report"/>
<jsp:useBean id="CUS" class="beans.customerpurchasesService"></jsp:useBean>
<%
cashier_reportListing CSHRPT=new cashier_reportListing();
mainClasses.bankdetailsListing BAN= new mainClasses.bankdetailsListing();
String bkid[]=request.getParameter("bank_id").split(" ");
String bank_id=bkid[0];
String name="";
String counterstatus="";
boolean bkIns=false;
boolean seconBktxId=false;
boolean bkUp=false;
String depositestauts="";
String HOA=request.getParameter("HOA");
if(HOA!=null && HOA.equals("1")){
	depositestauts="CharityDeposit";
} else if(HOA!=null && HOA.equals("4")){
	depositestauts="SansthanDeposit";
}
String narration=request.getParameter("Narration");
String amount=request.getParameter("amount");
String type=request.getParameter("category");
String vendorid="";
String mseid="";
String amtDeposit=request.getParameter("amtDeposited");
if("vendor".equals(amtDeposit)){
	String vendor[]=request.getParameter("vendorId").toString().toString().split(" ");
	vendorid=vendor[0];
}
String createdBy=session.getAttribute("adminId").toString();
String extra1=request.getParameter("extra1");
String empid=request.getParameter("employeeId");
String date1=request.getParameter("date");

String major_head_id = "";
String minor_head_id = "";
String product = "";

String depositTyp = request.getParameter("depositType"); 

if(depositTyp != null && depositTyp.equals("DLRDeposit")) {
	
	if(request.getParameter("major_head_id2") != null && !request.getParameter("major_head_id2").equals(""))
	{
		String major[] = request.getParameter("major_head_id2").toString().split(" ");	
		major_head_id = major[0];
	}

	if(request.getParameter("minor_head_id2") != null && !request.getParameter("minor_head_id2").equals(""))
	{
		String minor[] = request.getParameter("minor_head_id2").toString().split(" ");	
		minor_head_id = minor[0];
	}

	if(request.getParameter("product2") != null && !request.getParameter("product2").equals(""))
	{
		String prod[] = request.getParameter("product2").toString().split(" ");	
		product = prod[0];
	}	
}

else if(depositTyp != null && depositTyp.equals("OTHDeposit")){
	
	if(request.getParameter("major_head_id3") != null && !request.getParameter("major_head_id3").equals(""))
	{
		String major[] = request.getParameter("major_head_id3").toString().split(" ");	
		major_head_id = major[0];
	}

	if(request.getParameter("minor_head_id3") != null && !request.getParameter("minor_head_id3").equals(""))
	{
		String minor[] = request.getParameter("minor_head_id3").toString().split(" ");	
		minor_head_id = minor[0];
	}

	if(request.getParameter("product3") != null && !request.getParameter("product3").equals(""))
	{
		String prod[] = request.getParameter("product3").toString().split(" ");	
		product = prod[0];
	}
}

else{
	if(request.getParameter("major_head_id1") != null && !request.getParameter("major_head_id1").equals(""))
	{
		String major[] = request.getParameter("major_head_id1").toString().split(" ");	
		major_head_id = major[0];
	}

	if(request.getParameter("minor_head_id1") != null && !request.getParameter("minor_head_id1").equals(""))
	{
		String minor[] = request.getParameter("minor_head_id1").toString().split(" ");	
		minor_head_id = minor[0];
	}

	if(request.getParameter("product1") != null && !request.getParameter("product1").equals(""))
	{
		String prod[] = request.getParameter("product1").toString().split(" ");	
		product = prod[0];
	}
}

double bankpramount=0.0;
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat datetime= new SimpleDateFormat("HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));

String date=(dateFormat.format(c1.getTime())).toString();
String time=date1+" "+(datetime.format(c1.getTime())).toString();
%>
<jsp:setProperty name="BKT" property="bank_id" value="<%=bank_id%>"/>
<jsp:setProperty name="BKT" property="name" value="<%=name%>"/>
<jsp:setProperty name="BKT" property="narration" value="<%=narration%>"/>
<jsp:setProperty name="BKT" property="date" value="<%=time%>"/>
<jsp:setProperty name="BKT" property="amount" value="<%=amount%>"/>
<jsp:setProperty name="BKT" property="type" value="<%=type%>"/>
<jsp:setProperty name="BKT" property="createdBy" value="<%=createdBy%>"/>
<jsp:setProperty name="BKT" property="extra1" value="<%=extra1%>"/>
<jsp:setProperty name="BKT" property="extra2" value="<%=empid%>"/>
<jsp:setProperty name="BKT" property="extra4" value="<%=amtDeposit%>"/>
<jsp:setProperty name="BKT" property="extra6" value="<%=vendorid%>"/>

<jsp:setProperty name="BKT" property="head_account_id" value="<%=HOA%>"/>
<jsp:setProperty name="BKT" property="sub_head_id" value="<%=product%>"/>
<jsp:setProperty name="BKT" property="minor_head_id" value="<%=minor_head_id%>"/>
<jsp:setProperty name="BKT" property="major_head_id" value="<%=major_head_id%>"/>

<%if(type.equals("pettycash")){ 
	mseid=request.getParameter("mseInvId");%>
<jsp:setProperty name="BKT" property="extra10" value="<%=mseid%>"/>
<%} %>
<%String depositType = request.getParameter("depositType"); %>
<%if(depositType != null && depositType.equals("DLRDeposit")) {%>
<jsp:setProperty name="BKT" property="extra8" value="dollar-amount"/>

<%bkIns=BKT.insert();%><%=BKT.geterror()%>
<%}else if(depositType != null && depositType.equals("OTHDeposit")){ %>
<jsp:setProperty name="BKT" property="extra8" value="other-amount"/>
<%bkIns=BKT.insert();%><%=BKT.geterror()%>
<%}else{ %>
<%bkIns=BKT.insert();%><%=BKT.geterror()%>
<%} %>
<%
	if(request.getParameter("mseid") != null && !request.getParameter("mseid").equals(""))
	{
		if(request.getParameter("expid") != null && !request.getParameter("expid").equals(""))
		{
			String exp_id = request.getParameter("expid");%>
			<jsp:setProperty name="PEXP" property="extra8" value="petty cash withdrawn"/>
			<jsp:setProperty name="PEXP" property="exp_id" value="<%=exp_id%>"/>
	<% 
	PEXP.update();
	}}
%>

<%
String billingIds[]=request.getParameterValues("checkbox1");
String totAmt[] = request.getParameterValues("totAmt");
String status = "Deposited";
if(depositType != null && depositType.equals("DLRDeposit")){
if(billingIds.length>0){
	for(int i=0;i<billingIds.length;i++){%>
<jsp:setProperty name="CUS" property="billingId" value="<%=billingIds[i] %>"/>
<jsp:setProperty name="CUS" property="othercash_deposit_status" value="<%=status %>"/>
<jsp:setProperty name="CUS" property="othercash_totalamount" value="<%=totAmt[i] %>"/>
<%=CUS.updateBasedOnId(billingIds[i],totAmt[i],status)%><%=CUS.geterror() %>
<%}}} %>

<%
String bankTransId=BKT.getbanktrn_id();
if(bkIns){
String pettyCash=BAN.getPettyCashAmount(bank_id);
String bankamount=BAN.getMbankdetailsAmount(bank_id);
if(type.equals("deposit")){
	bankpramount=(Double.parseDouble(bankamount)+Double.parseDouble(amount));
}else if(type.equals("pettycash")){
	//bankpramount=(Double.parseDouble(bankamount)-Double.parseDouble(amount)); // commented on 14/04/2016 
	bankpramount=Double.parseDouble(bankamount);
	if(pettyCash!=null && !pettyCash.equals("")){
		pettyCash=""+(Double.parseDouble(pettyCash)+Double.parseDouble(amount));
	}else{
		pettyCash=amount;
	}%>
	<%-- <jsp:setProperty name="BNK" property="bank_id" value="<%=bank_id%>"/>
	<jsp:setProperty name="BNK" property="extra1" value="<%=pettyCash%>"/>
	<%BNK.update();%><%=BNK.geterror()%> --%>
	<%}bankamount=String.valueOf(bankpramount);%>
	<%-- <jsp:setProperty name="BNK" property="bank_id" value="<%=bank_id%>"/>
	<jsp:setProperty name="BNK" property="total_amount" value="<%=bankamount%>"/>
	<%bkUp=BNK.update();%><%=BNK.geterror()%> --%>
<%}
if(type.equals("deposit")){
	if(bkUp){
		String cashier_report_ids[]=request.getParameter("cashierReportIds").split(",");
		if(cashier_report_ids.length>0){
			if(HOA.equals("5")){
				for(int i=0;i<cashier_report_ids.length;i++){
					Map counters=CSHRPT.getSainivascashier_report(cashier_report_ids[i]);
					Iterator countiter=counters.keySet().iterator();
					if(countiter.hasNext()){
						  Object key   = countiter.next();
					   	  Object value = counters.get(key);
					   	CASH=(beans.cashier_report)value;
					}
					counterstatus=CASH.getcash6();
					if(counterstatus.equals("")){
						%>
					<jsp:setProperty name="CSH" property="cashier_report_id" value="<%=cashier_report_ids[i]%>"/>
					<jsp:setProperty name="CSH" property="cash6" value="Deposited"/>	
							<%CSH.SainivasUpdate(); %><%=CSH.geterror()%>
						<%}}} else{
			for(int i=0;i<cashier_report_ids.length;i++){
				Map counters=CSHRPT.getMcashier_report(cashier_report_ids[i]);
				Iterator countiter=counters.keySet().iterator();
				if(countiter.hasNext()){
					  Object key   = countiter.next();
				   	  Object value = counters.get(key);
				   	CASH=(beans.cashier_report)value;
				}
				counterstatus=CASH.getcash6();%>
	<jsp:setProperty name="CSH" property="cashier_report_id" value="<%=cashier_report_ids[i]%>"/>
	<jsp:setProperty name="CSH" property="cash6" value=""/>
	<jsp:setProperty name="CSH" property="cash10" value=""/>
	<jsp:setProperty name="CSH" property="cash7" value=""/>
	<jsp:setProperty name="CSH" property="cash9" value=""/>
	<jsp:setProperty name="CSH" property="due_banktrnsid" value=""/>
	<%if(counterstatus.equals("Not-deposited")){ %>
		<jsp:setProperty name="CSH" property="cash6" value="<%=depositestauts%>"/>
				<%if(depositestauts.equals("SansthanDeposit")){%>
			<jsp:setProperty name="CSH" property="cash10" value="<%=bankTransId%>"/>
			<%} else { %>
					<jsp:setProperty name="CSH" property="cash7" value="<%=bankTransId%>"/>
		<%}} else {if( HOA.equals("1") ){
							if(CASH.getcash7().equals("")){
							%>
								<jsp:setProperty name="CSH" property="cash7" value="<%=bankTransId%>"/>
				<%} else if (!CASH.getcash7().equals("")){
								%>
					<jsp:setProperty name="CSH" property="cash9" value="DueDeposited"/>
					<jsp:setProperty name="CSH" property="due_banktrnsid" value="<%=bankTransId%>"/>
					<% }} else if(HOA.equals("4")){%>
			<jsp:setProperty name="CSH" property="cash10" value="<%=bankTransId%>"/>
			<%}if( !CASH.getcash9().equals("") && !CASH.getcash7().equals("") && !CASH.getcash10().equals("")){
							%>
					<jsp:setProperty name="CSH" property="cash6" value="Deposited"/>
							<%} else if(CASH.getcash9().equals("DueDeposited")){%>
							<jsp:setProperty name="CSH" property="cash6" value="Deposited"/>
					<%} else {%>
							<jsp:setProperty name="CSH" property="cash6" value="<%=depositestauts%>"/>
							<%}}%>
			<%CSH.update(); %><%=CSH.geterror()%>
<%}}}}}
   response.sendRedirect("adminPannel.jsp?page=countersClosed-Deposits");   
%>
<%-- <script type="text/javascript">
window.parent.location="adminPannel.jsp?page=bankDetails&id=<%=bank_id%>";
</script> --%>