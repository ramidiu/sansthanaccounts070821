<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@page import="beans.products"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Expenses entry</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	

</script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>

<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$("#submitbut").prop('disabled', true);
	
	$('#dateError').hide();
	$('#indentError').hide();
	$('#bankError').hide();
	$('#vendorError').hide();
/* 	$('#nameError').hide(); */

	if($('#date').val().trim()==""){
		$('#dateError').show();
		$('#date').focus();
		return false;
	}
	/* if($('#vendorId').val().trim()==""){
		$('#vendorError').show();
		$('#vendorId').focus();
		return false;
	} */

/* 	if($('#bankId').val().trim()==""){
		$('#bankError').show();
		$('#bankId').focus();
		return false;

	} */
	if($('#major_head_id').val().trim()==""){
		$('#indentError').show();
		$('#major_head_id').focus();
		return false;
	}
	/* 
	if($('#nameOnCheck').val().trim()==""){
		$('#nameError').show();
		$('#nameOnCheck').focus();
		return false;
	} */
	if(($('#product0').val().trim()=="" || $('#product0').val().trim()=="")){
		$('#product0').addClass('borderred');
		$('#product0').focus();
		return false;
	}
	/* var bank=$("#bankId").val(); */
	var majorHead=$("#major_head_id").val();
	var minorHead=$("#minor_head_id").val();
	var totalAmt=$("#totAmt").text();
	 var status = confirm('You have entered bellow details.Please check : \n  \n Major Head : '+majorHead+'\n MINOR HEAD : '+minorHead+'\n TOTAL AMOUNT : '+totalAmt+' \n\n Click OK to proceed');
	   if(status == false){
	   return false;
	   }
	   else{
		   $.blockUI({ css: { 
		         border: 'none', 
		         padding: '15px', 
		         backgroundColor: '#000', 
		         '-webkit-border-radius': '10px', 
		         '-moz-border-radius': '10px', 
		         opacity: .5, 
		         color: '#fff' 
		     } }); 
	   return true; 
	   }
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="../js/jquery.blockUI.js"></script>
<script>
function majorheadsearch(j){
	var data1=$('#major_head_id'+j).val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#major_head_id'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
}
</script>

<script>
function minorheadsearch(j){
		  var data1=$('#minor_head_id'+j).val();
		  var major=$('#major_head_id'+j).val();
		  var hoid=$("#head_account_id").val();
		  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			$('#minor_head_id'+j).autocomplete({
				source: availableTags,
				focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				}
			}); 		
	  });
}
</script>

<script>
$(document).ready(function(){
	/* $("#major_head_id").keyup(function(){
		  var data1=$("#major_head_id").val();
		  var hoid=$("#head_account_id").val();
		  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			 $( "#major_head_id" ).autocomplete({source: availableTags}); 
				});
	  });
	  $("#minor_head_id").keyup(function(){
		  var data1=$("#minor_head_id").val();
		  var major=$("#major_head_id").val();
		  var hoid=$("#head_account_id").val();
		  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			 $( "#minor_head_id" ).autocomplete({source: availableTags}); 
				});
	  }); */     //coommented by srinivas
	
  $("#sub_head_id").keyup(function(){
	  var data1=$("#sub_head_id").val();
	  var minor=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#sub_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#bankId").keyup(function(){
	  var hoid=$("#head_account_id").val();
	  var data1=$("#bankId").val();
	  $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data)
	  {
		 var response = data.trim().split("\n");
		 var bankNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 bankNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=bankNames;
		 $( "#bankId" ).autocomplete({source: availableTags}); 
			});
  });
});
</script>
<script>
function combochange(denom,desti,jsppage) { 
    var com_id = denom; 
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	$.post(jsppage,{ id: com_id } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");
	
				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
				sda1.add(y,null);
				}
				catch(e)
				{
				sda1.add(y);
				}
	}
	
	}
	}); 
}
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function vendorsearch(){
	  var hoid=$('#head_account_id').val();
	  var data1=$('#vendorId').val();
	  var arr = [];
	  $.post('searchVendor.jsp',{q:data1,b : hoid},function(data)
	{
		var response = data.trim().split("\n");
		var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
				 doctorIds:d[0],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		 $( '#vendorId').autocomplete({source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},	 
		 }); 
			});
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var arr = [];
	  //var data1=$("#sub_head_id").val();
	  var minor=$('#minor_head_id'+j).val();
	  var major=$('#major_head_id'+j).val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
} 
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;

	u=document.getElementById("addcount").value;

	if(document.getElementById("product"+i).value!=""){

		if(document.getElementById("purchaseRate"+i).value!="")
		{
		
			for(var j=0;j<=u;j++){
			
				if(document.getElementById("purchaseRate"+j).value!=""){

				grossprice=Number(parseFloat(document.getElementById("purchaseRate"+j).value)*Number(parseFloat(document.getElementById("quantity"+j).value))).toFixed(2);

			document.getElementById("grossAmt"+j).value=grossprice;
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice)).toFixed(2);
			
				}
				}
			document.getElementById("total").value=Totalprice;
		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;

}
function addOption(){
	$("#department").empty();
	/* $("#vendorId").val(" "); */
	var hoaid=$("#HOA").val();
	if(hoaid=="3"){
		$("#department").append('<option value=godown>Godown</option>');
		/* $("#department").append('<option value=shop>Shop</option>'); */
	}else if(hoaid=="4"){
		$("#department").append('<option value=santhanstores>Sansthan stores</option>');
	}
}
function chequeDetails(){
    var payType=$("#paymentType").val();
    if(payType=="cheque"){
    	$("#chequeDetails").show();
    }else{
    	$("#chequeDetails").hide();
    }
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="GDS" class="beans.godwanstock"/>
<jsp:useBean id="PDE" class="beans.productexpenses"></jsp:useBean>

<%
DecimalFormat df=new DecimalFormat("#0.00");
DecimalFormat decimal=new DecimalFormat("0.00");
products products=new products();
subheadListing SBH_L=new subheadListing();
indentapprovalsListing IAPL=new indentapprovalsListing();
if(session.getAttribute("adminId")!=null){
	String expInv_id=request.getParameter("invid");
String expenseflag=request.getParameter("expflag");
if(expInv_id!=null && !expInv_id.equals("") && expenseflag.equals("1")){
%>
<form name="tablets_Update" method="post" action="paymentsApprovedExpenses_Insert.jsp" onsubmit="return validate();">
<div class="vendor-page">

<div class="vendor-box">
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="1" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">(Regd.No.646/92)</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 12px;" class="bg-new">Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="1" align="center" style="font-weight: bold;font-size: 16px;" class="bg-new"><br/>Expenses Entry</td></tr>
</table>
<div class="vender-details">
<%
godwanstockListing GDS_L=new godwanstockListing();
List GDSBLAPRV=GDS_L.getgodwanstockByinvoiceId(expInv_id);
if(GDSBLAPRV.size()>0){
	GDS=(godwanstock)GDSBLAPRV.get(0);
}
%>
<input type="hidden" name="trackingno" id="trackingno" value="<%=GDS.getExtra9() %>" />
<input type="hidden" name="expInvoiceId" id="expInvoiceId" value="<%=GDS.getExtra21() %>" />
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%"> <div class="warning" id="dateError" style="display: none;">
<input type="hidden" name="invoiceId" value="<%=request.getParameter("invid")%>"></input>
Please select date.</div>Date<span style="color: red;">*</span></td>
<td>Head account <span style="color: red;">*</span></td>
<!-- <td><div class="warning" id="bankError" style="display: none;">Please select a bank.</div>Bank</td> -->
<td ><div class="warning" id="vendorError" style="display: none;">Please select vendor.</div>Vendor</td>

</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<tr>

<td><input type="text" name="date" id="date" value="<%=currentDate%>" /></td>
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
productexpensesListing PRDE_L=new productexpensesListing();
productsListing PRD_L=new productsListing();
vendorsListing VENL=new vendorsListing();
List HA_Lists=HOA_L.getheadofaccounts(); %>
<td><%-- <select name="head_account_id" id="head_account_id"  READONLY=true>
<%if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"<%if(GDS.getdepartment().equals(HOA.gethead_account_id())){ %> selected="selected"   <%} %> ><%=HOA.getname() %></option>
<%}} %>
</select> --%>
<input type="hidden" name="head_account_id" id="head_account_id" readonly="readonly" value="<%=GDS.getdepartment()%>"/>
<input type="text" name="headName" id="headName" value="<%=HOA_L.getHeadofAccountName(GDS.getdepartment()) %>" readonly="readonly" value=""></input>
</td>
<!-- <td><input type="text" name="bankId" id="bankId" value="" onkeyup="banksearch()" placeholder="find a bank here" autocomplete="off"/></td> -->
<td><input type="text" name="vendorId"  value="<%=GDS.getvendorId()%> <%=VENL.getMvendorsAgenciesName(GDS.getvendorId())%>" onkeyup="vendorsearch()" placeholder="find a vendor here" autocomplete="off" readonly="readonly" />
</td>
</tr>
<tr>
<!-- <td><div class="warning" id="indentError" style="display: none;">Please select major head.</div>Major head<span style="color: red;">*</span></td>
<td><div class="warning" id="billError" style="display: none;">Please select minor head.</div>Minor head</td> -->
<td>Narration :</td>
<td></td>
</tr>
<tr>
<!-- <td><input type="text" name="major_head_id" id="major_head_id" value="" placeholder="find a major head here" autocomplete="off" style="vertical-align: top"/></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value=""  placeholder="find a minor head here" autocomplete="off" style="vertical-align: top"/></td> -->
<td>
<textarea name="narration" id="narration" style="height: 50px;" placeholder="narration" ></textarea>
<!-- <select name="paymentType" id="paymentType" onchange="chequeDetails();">
	<option value="cashpaid">Cash</option>
	<option value="cheque">Cheque</option>
</select> -->
</td>
<td><%if(GDS.getBill_update_by()!=null && GDS.getBill_update_by().equals("")){ %><div class="ven-nme1" align="right" onclick="openMyModal('approvebyaccounts.jsp?TId=<%=GDS.getExtra9()%>&InvId=<%=expInv_id%>')"><input type="button" value="Verify" class="click" /></div><%} %></td>
</tr>
<tr>
<td colspan="4">
	<table id="chequeDetails" style="display: none;" width="70%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td>Cheque Number<span style="color: red;">*</span></td>
		<td>Name on cheque<span style="color: red;">*</span></td>
		<td></td>
		</tr>
		<tr>
		<td><input type="text" name="chequeNo" id="chequeNo"   maxlength="6"  value=""></input></td>
		<td><input type="text" name="nameOnCheque" id="nameOnCheque" value=""></input></td>
		<td></td>
		</tr>
	</table> 

</td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list">
<div class="sno1 bgcolr" style="width:4%">S.No.</div>
<div class="ven-nme1 bgcolr" style="width:15%">Major Head</div>
<div class="ven-nme1 bgcolr" style="width:15%">Minor Head</div>
<div class="ven-nme1 bgcolr" style="width:13%">Sub Head</div>
<!-- <div class="ven-nme1 bgcolr">Narration</div> -->
<div class="ven-nme1 bgcolr" style="width:10%">Amount</div>
<div class="ven-nme1 bgcolr" style="width:20%">Product Name</div>
<div class="ven-nme1 bgcolr" style="width:8%">Quantity</div>
<div class="ven-nme1 bgcolr" style="width:8%">Rate</div>
<div class="clear"></div>
<input type="hidden" name="addcount" id="addcount" value="5"/>
<input type="hidden" name="expscount" id="expscount" value="<%=GDSBLAPRV.size()%>"/>
<%
subheadListing SUBL=new subheadListing();

double totAmt=0.00;
double GSTVal=0.00;
double otherchargesrdf=0.00;
for(int i=0; i < GDSBLAPRV.size(); i++ ){
	GDS=(godwanstock)GDSBLAPRV.get(i);
	if(GDS.getExtra18()!=null && !GDS.getExtra18().equals("null") && !GDS.getExtra18().equals("") && GDS.getExtra19()!=null && !GDS.getExtra19().equals("null") && !GDS.getExtra19().equals("") && GDS.getExtra20()!=null && !GDS.getExtra20().equals("null") && !GDS.getExtra20().equals("")){
	GSTVal=Double.parseDouble(GDS.getExtra18())+Double.parseDouble(GDS.getExtra19())+Double.parseDouble(GDS.getExtra20());
	}
	else{
		GSTVal=0.00;	
	}
	totAmt=totAmt+Double.parseDouble(GDS.getquantity())*(Double.parseDouble(GDS.getpurchaseRate())+GSTVal);

if(GDS.getExtra7().equals("serviceBill")){
	products=PRD_L.getMajorMinorAndSubHeadForServiceBillApproval(GDS.getproductId(),GDS.getdepartment());
}else{
	products=PRD_L.getMajorMinorSubheadName(GDS.getproductId(),GDS.getdepartment());	
}



%>
<div <%-- <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>" --%>>


<div class="sno1" style="width:4%"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1" style="width:15%"><input type="text" name="major_head_id<%=i%>" id="major_head_id<%=i%>" onkeyup="majorheadsearch('<%=i%>')" class="" value="<%=products.getmajor_head_id()%> <%=products.getMajorHeadName()%>" required placeholder="find a major head here" autocomplete="off"/></div>
<div class="ven-nme1" style="width:15%"><input type="text" name="minor_head_id<%=i%>" id="minor_head_id<%=i%>" onkeyup="minorheadsearch('<%=i%>')" class="" value="<%=products.getextra3()%> <%=products.getMinorHeadName()%>" required  placeholder="find a minor head here" autocomplete="off"/></div>
<div class="ven-nme1" style="width:13%"><input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>')" class="" value="<%=products.getsub_head_id()%> <%=products.getSubHeadName()%>" required placeholder="find a sub head here" autocomplete="off" style="width: 150px;" /></div>
<%-- <div class="ven-nme1"><input type="text" name="description<%=i%>" id="description<%=i%>"  value="<%=GDS.getdescription()%>" readonly="readonly"></input></div> --%>
<div class="ven-nme1" style="width:10%">&#8377;<input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" value="<%=df.format(Double.parseDouble(GDS.getquantity())*(Double.parseDouble(GDS.getpurchaseRate())+GSTVal))%>" readonly="readonly"  onkeypress="return numbersonly(this, event,true);" style="width: 90px;" ></input></div>
<div class="ven-nme1" style="width:20%"><input type="text" name="billproduct<%=i%>" id="billproduct<%=i%>" <%if(!PRD_L.getProductsNameByCat(GDS.getproductId(),GDS.getdepartment()).equals("")) {%>value="<%=GDS.getproductId() %> <%=PRD_L.getProductsNameByCat(GDS.getproductId(),GDS.getdepartment())%>"<%} else { %>value="<%=GDS.getproductId() %> <%=PRD_L.getProductsNameByCat1(GDS.getproductId(),GDS.getdepartment())%>"<%} %>
<%-- <%if(PRD_L.getProductsNameByCat(GDS.getproductId(),GDS.getdepartment())!=null && !PRD_L.getProductsNameByCat(GDS.getproductId(),GDS.getdepartment()).equals("")){ %> value="<%=GDS.getproductId()%> <%=PRD_L.getProductsNameByCat(GDS.getproductId(),GDS.getdepartment())%>" <%}else{ %> value="<%=GDS.getproductId()%> <%=SBH_L.getMsubheadname(GDS.getproductId())%>" <%} %> --%> readonly="readonly"/></div>
<div class="ven-nme1" style="width:8%;margin-top: 13px;"><%=GDS.getquantity() %></div>
<div class="ven-nme1" style="width:8%;margin-top: 13px;"><%=df.format(Double.parseDouble(GDS.getpurchaseRate())+GSTVal)%></div>
<div class="clear"></div>
</div>
<%}

if(GDS.getExtra8()!=null && !GDS.getExtra8().equals("")){
	%>
	<div style=" width: 380px; text-align: right; padding: 10px 0; margin-top: 10px;">Other Charges :  <span ><%=GDS.getExtra8()%> </span></div>
	<%
	totAmt=Double.parseDouble(GDS.getExtra8())+totAmt;
	otherchargesrdf=Double.parseDouble(GDS.getExtra8());
}
if(!GDS.getExtra12().equals("") && !GDS.getExtra11().equals(""))
{
	if(GDS.getExtra12().equals("minus")){
			otherchargesrdf=otherchargesrdf-Double.parseDouble(GDS.getExtra11());
			totAmt=totAmt-Double.parseDouble(GDS.getExtra11());
			%>
			<div style=" width: 380px; text-align: right; padding: 10px 0; margin-top: 10px;">Round Off:  <span >-<%=GDS.getExtra11()%> </span></div>
			<%
	} else if(GDS.getExtra12().equals("plus")) {

		otherchargesrdf=otherchargesrdf+Double.parseDouble(GDS.getExtra11());
		totAmt=totAmt+Double.parseDouble(GDS.getExtra11());
		%>
		<div style=" width: 380px; text-align: right; padding: 10px 0; margin-top: 10px;">Round Off :  <span ><%=GDS.getExtra11()%> </span></div>
		<%
	}
	
}
%>
<div style=" width: 380px; text-align: right; border-top: 1px solid #000;padding: 10px 0; margin-top: 10px;">Total Amount :  <span id="totAmt"><%=totAmt%> </span></div>
<input type="hidden" name="othercharges" id="othercharges" value="<%=otherchargesrdf %>" />
<!-- <div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/> -->
<%if(!PRDE_L.getPRoductExpensForInvoice(GDS.getextra1())) { 
if(GDS.getBill_update_by()!=null && GDS.getBill_update_by().equals("")){ %>
<div class="ven-nme1" align="right" onclick="openMyModal('approvebyaccounts.jsp?TId=<%=GDS.getExtra9()%>&InvId=<%=expInv_id%>')"><input type="button" value="Verify" class="click" /></div>
<%} else{ %>
<input type="submit" id="submitbut" value="Submit" class="click" style="border:none; float:right; margin:0 380px 0 0"/>
<%} }%>
</div>
<!-- <div class="tot-ven">Total<input type="text" name="total" id="total" value="0.00" readonly="readonly" style="width:50px;"/></div> -->
</div>
</div>
</form>
<%} else if(expInv_id!=null && expenseflag.equals("2")){
	productexpensesListing PRDE_L=new productexpensesListing();
	bankdetailsListing BNK_L=new bankdetailsListing();
	majorheadListing MJR_L=new majorheadListing();
	minorheadListing MIN_L=new minorheadListing();
	
	productsListing PRD_L=new productsListing();
	vendorsListing VND_L=new vendorsListing();
	headofaccountsListing HOA_L=new headofaccountsListing();
	List PRDEXP=PRDE_L.getExpListBasedOnInvoiceID(expInv_id,"");
	double totamnt=0.0;
	if(PRDEXP.size()>0){
		PDE=(productexpenses)PRDEXP.get(0);}
	%>

	<form name="tablets_Update" method="post" action="productexpensesupdate.jsp" onsubmit="return validate();">
<div class="vendor-page">
<div class="vendor-box">
<div class="vender-details">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%"> <div class="warning" id="dateError" style="display: none;">Please select date.</div>Date<span style="color: red;">*</span></td>
<td>Head account <span style="color: red;">*</span></td>
<!-- <td><div class="warning" id="bankError" style="display: none;">Please select a bank.</div>Bank</td> -->
<td ><div class="warning" id="vendorError" style="display: none;">Please select vendor.</div>Vendor</td>

</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<tr>
<td><input type="text" name="date" id="date" value="<%=currentDate%>" />
<input type="hidden" name="expInv_id" id="expInv_id" value="<%=expInv_id %>" /></td>

<td><select name="head_account_id" id="head_account_id">
<option  value="<%=PDE.gethead_account_id()%>" ><%=HOA_L.getHeadofAccountName(PDE.gethead_account_id()) %></option>
</select></td>
<%-- <td><input type="text" name="bankId" id="bankId" value="<%=PDE.getextra3() %> <%=BNK_L.getBankName(PDE.getextra3()) %>" readonly="readonly" placeholder="find a bank here" autocomplete="off"/></td> --%>
<td><input type="text" name="vendorId" id="vendorId" value="<%=PDE.getvendorId() %> <%=VND_L.getMvendorsAgenciesName(PDE.getvendorId()) %>" readonly="readonly" placeholder="find a vendor here" autocomplete="off"/></td>
</tr>
<tr>
<td><div class="warning" id="indentError" style="display: none;">Please select major head.</div>Major head<span style="color: red;">*</span></td>
<td><div class="warning" id="billError" style="display: none;">Please select minor head.</div>Minor head</td>
<!-- <td>Payment type</td> -->
<td>Narration*<span style="color: red;"></span></td>
</tr>
<tr>
<td><input type="text" name="major_head_id" id="major_head_id" value="<%=PDE.getmajor_head_id()%> <%=MJR_L.getmajorheadName(PDE.getmajor_head_id()) %>"  placeholder="find a major head here" autocomplete="off"/></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value="<%=PDE.getminorhead_id()%> <%=MIN_L.getminorheadName(PDE.getminorhead_id()) %>"  placeholder="find a minor head here" autocomplete="off"/></td>
<!-- <td>
<select name="paymentType" id="paymentType" onchange="chequeDetails();">
	<option value="cashpaid">Cash</option>
	<option value="cheque">Cheque</option>
</select>
</td> -->
<td>
<textarea name="narration" id="narration" style="height: 50px;" placeholder="narration" ><%=PDE.getnarration() %></textarea>
</td>
</tr>
<tr>
<td colspan="4">
	<table id="chequeDetails" style="display: none;" width="70%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td>Cheque Number<span style="color: red;">*</span></td>
		<td>Name on cheque<span style="color: red;">*</span></td>
		
		</tr>
		<tr>
		<td><input type="text" name="chequeNo" id="chequeNo"   maxlength="6"  value=""></input></td>
		<td><input type="text" name="nameOnCheque" id="nameOnCheque" value=""></input></td>
		
		</tr>
	</table> 

</td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list">
<div class="sno1 bgcolr">S.No.</div>
<div class="ven-nme1 bgcolr">Sub Head</div>
<div class="ven-nme1 bgcolr">Product</div>
<div class="ven-amt1 bgcolr">Amount</div>
<div class="clear"></div>
<input type="hidden" name="expscount" id="expscount" value="<%=PRDEXP.size()%>"/>
<%	if(PRDEXP.size()>0){
for(int i=0; i < PRDEXP.size(); i++ ){
	PDE=(productexpenses)PRDEXP.get(i);
	totamnt=totamnt+Double.parseDouble(PDE.getamount());
%>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="sno1"><%=i+1 %>
<input type="hidden" name="expensid<%=i %>" id="expensid<%=i %>" value="<%=PDE.getexp_id() %>"/>
</div>

<div class="ven-nme1"><input type="text" name="product<%=i %>" id="product<%=i %>" onkeyup="productsearch('<%=i %>')"  value="<%=PDE.getsub_head_id() %> <%=SBH_L.getMsubheadnameWithDepartment(PDE.getsub_head_id(), PDE.gethead_account_id()) %>" autocomplete="off"/></div>
<div class="ven-nme1"><input type="text"    class=""
<%if(!PRD_L.getProductsNameByCat(PDE.getextra2(), PDE.gethead_account_id()).equals("")) {%>value="<%=PDE.getextra2()%> <%=PRD_L.getProductsNameByCat(PDE.getextra2(), PDE.gethead_account_id())%>"<%} else { %>value="<%=PDE.getextra2() %>  <%=PRD_L.getProductsNameByCat1(PDE.getextra2(), PDE.gethead_account_id())%>"<%} %> readonly="off" autocomplete="off"/></div>
<div class="ven-nme1">&#8377;<input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" value="<%=PDE.getamount()%>" readonly="off" onkeypress="return numbersonly(this, event,true);" style="width: 150px;"></input></div>
<div class="clear"></div>

<%}} %>
<div class="ven-nme1" align="right"><span class='bold'>Total Amount :</span> </div>
<div class="ven-nme1" ><input type="text" name="totalamont" id="totalamont" class="" value="<%=decimal.format(totamnt)%>" readonly="off" autocomplete="off"/></div>
<%if(!IAPL.getBillWithApprove(PDE.getexpinv_id())){ %>
<div class="ven-nme1" align="right"><input type="submit" id="updatebut" value="Update" class="click" /></div>
<%} else{ %>
<br/>
<span class='bold'>sorry,you can't edit this bill.It is approved by one of boardmembers.</span> 
<%} %>

</div>
<!-- <div class="tot-ven">Total<input type="text" name="total" id="total" value="0.00" readonly="readonly" style="width:50px;"/></div> -->



</div>



</div>
</form>


<%} %>


<script>
window.onload=function a(){
		addOption();
	};
</script>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>