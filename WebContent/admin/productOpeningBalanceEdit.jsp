<%@page import="beans.ProductOpeningBalance"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="beans.ProductOpeningBalanceService" %>  
<%@page import="beans.ProductOpeningBalance"%>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ProductOpeningBalanceEdit</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
/* table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
} */
th, td {
  padding: 5px;
  text-align: left;
}
.update{
    padding: 40px 40px;
}
.updatebtn{
border: 1px solid #65230d;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    padding: 5px 15px 5px 15px;
    text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
    font-weight: bold;
    text-align: center;
    color: #FFF;
    background-color: #ffc579;
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
    background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
    background-image: -moz-linear-gradient(top, #c88b2e, #671811);
    background-image: -ms-linear-gradient(top, #c88b2e, #671811);
    background-image: -o-linear-gradient(top, #c88b2e, #671811);
    background-image: linear-gradient(top, #c88b2e, #671811);
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
    cursor: pointer;}
</style>
<script type="text/javascript">
 $(document).ready(function(){
	$('#updatebtn').click(function(){
		var quantity=$('#quantity').val();
		if(quantity===''){
			$('#quantity').focus().css('outline','solid red 2px');
			return false;
		}
		$('#submitForm').attr("action","productopeningbalanceupdate.jsp");
		$('#submitForm').submit();
	});
 });
</script>
<script type="text/javascript">
 function removeError(id){
	 $('#'+id).css('outline','#BCD04E');
 }
</script>
<body>
<%
  String productOpeningBalanceId=request.getParameter("productOpeningBalanceId");
  ProductOpeningBalanceService productOpeningBalanceService=new ProductOpeningBalanceService();
  ProductOpeningBalance productOpeningBalance=productOpeningBalanceService.getProductOpeningBalanceDetails(productOpeningBalanceId);
%>
<div class="vendor-page">

                 <h3 style="text-align:center";>Product Opening Balance Details</h3>
                  <form  action="" id="submitForm" method="post"
			style="padding-left: 250px; padding-top: 30px;">
			<input type="hidden" name="productOpeningBalanceId" value="<%=productOpeningBalanceId%>">
<table style="width:100%">
  <tr>
    <th>Product Code</th>
    <td> <input type="text"  name="productId" id="productId" value="<%=productOpeningBalance.getProductId()%>" readonly></td>
  </tr>
  <tr>
    <th>Product Name</th>
    <td> <input type="text" name="productName" id="productName" value="<%=productOpeningBalance.getProductName()%>" readonly></td>
  </tr>
  <tr>
    <th>Date</th>
    <td> <input type="text"  name="finacialyeardate" id="finacialyeardate" value="<%=productOpeningBalance.getFinacilaYearDate()%>" readonly></td>
  </tr>
  <tr>
  <th>Quanity</th>
  <td> <input type="text"  name="quantity" id="quantity" value="<%=productOpeningBalance.getQuantity()%>" onkeyup="removeError('quantity')"></td>
  </tr>
</table>
	
        </form>
        <div class="col-md-2 col-md-offset-5 update">
            	<button type="button" class="updatebtn" id="updatebtn">Update</button>
        </div>
</div>
</body>
</html>