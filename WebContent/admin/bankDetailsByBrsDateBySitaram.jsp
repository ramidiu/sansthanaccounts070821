<%@page import="mainClasses.ReportesListClass1"%>
<%@page import="mainClasses.ReportesListClass"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.banktransactionsListing,java.util.Date"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(document).ready(function(){
$(function() {
	$( "#fromDate" ).datepicker({
		minDate: new Date(2017, 03, 1),
		maxDate: new Date(2018, 02, 31),
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		} 
	});
		$( "#toDate" ).datepicker({
			minDate: new Date(2017, 03, 1),
			maxDate: new Date(2018, 02, 31),
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		} 
	});
});
});

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
});

function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
$(document).ready(function(){
$( "#reportType" ).change(function() {
	  $( "#searchForm" ).submit();
	});
});
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                	
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<jsp:useBean id="BNK" class="beans.bankdetails"/>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
<jsp:useBean id="NUM" class="beans.NumberToWordsConverter"/>
<jsp:useBean id="BNKBAL" class="beans.bankbalance"/>
	<%String bankId=request.getParameter("id");
	System.out.println("bankId.... ===>"+bankId);

	DecimalFormat formatter = new DecimalFormat("#,##,###.00");
	DecimalFormat sjf = new DecimalFormat("####");
	DecimalFormat df = new DecimalFormat("0.00");
	mainClasses.bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	List BNK_List=BNK_CL.getMbankdetails(bankId); 
	if(BNK_List.size()>0){
		BNK=(beans.bankdetails)BNK_List.get(0);%>
<div class="vendor-page">
<div class="vendor-box">
<div class="name-title"><%=BNK.getbank_name()%> <span>(<%=HOA_CL.getHeadofAccountName(BNK.getheadAccountId())%>)</span></div>
<div class="click floatleft"><a href="./editBankDetails.jsp?id=<%=BNK.getbank_id()%>" target="_blank" onclick="openMyModal('editBankDetails.jsp?id=<%=BNK.getbank_id()%>'); return false;">Edit</a></div>

<%-- <div class="click floatleft" style="margin-left: 20px;"><a href="deleteBank.jsp?id=<%=BNK.getbank_id()%>" >Delete</a></div> --%>
<%-- <div class="click floatleft" style="margin-left: 20px;"><a href="editBankDetails.jsp?id=<%=BNK.getbank_id()%>" target="_blank" onclick="openMyModal('banktransactionsForm.jsp?id=<%=BNK.getbank_id()%>'); return false;">Deposit</a></div> --%>

<div style="clear:both;"></div>
<div class="details-list">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td><span>Branch Name:</span></td>
<td><%=BNK.getbank_branch()%></td>
</tr>

<tr>
<td><span>Account Holder Name:</span></td>
<td><%=BNK.getaccount_holder_name()%></td>
</tr>

<tr>
<td><span>Account Number:</span></td>
<td><%=BNK.getaccount_number()%></td>
</tr>

<tr>
<td ><span>IFSC Code:</span></td>
<td><%=BNK.getifsc_code()%></td>
</tr>

<tr>
<td ><span>Total Amount:</span></td>
<td><%=formatter.format(Double.parseDouble(BNK.gettotal_amount()))%></td>
</tr>

</table>

</div>
<div class="details-amount">

 <ul>
 <%
 bankcalculations BNKCAL=new bankcalculations();
 bankbalanceListing BBAL_L=new mainClasses.bankbalanceListing();
 
 DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
 TimeZone tz = TimeZone.getTimeZone("IST");
 dateFormat2.setTimeZone(tz);
 
 Calendar c2 = Calendar.getInstance(); 
 String currentDateNew=(dateFormat2.format(c2.getTime())).toString();
 
 if(request.getParameter("todate") != null && !request.getParameter("todate").equals(""))
 {
	 currentDateNew = request.getParameter("todate");
 }
 
 String monthInString=currentDateNew.substring(5, 7);
 int month = Integer.parseInt(monthInString);
 String yearInString=currentDateNew.substring(0, 4);
 int year = Integer.parseInt(yearInString);
 
 String finStartYr2 = "";

 if(month >= 4)
 {
		finStartYr2 = Integer.toString(year);
 }
 else
 {
	 int finStartYrInInt = year - 1;
	 finStartYr2 = Integer.toString(finStartYrInInt);
 }
 
 String finStartAndEndYr = "";
 
 String tempInString = finStartYr2.substring(2, 4);
 int tempInInt = Integer.parseInt(tempInString)+1;
 finStartAndEndYr = finStartYr2+"-"+tempInInt;
 
 double bankopeningbal = 0.0;
	double pettycashOpeningbal = 0.0;
	
	 if(BNK.getextra2().equals("debit"))
	 {
	 	bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","bank","");
		double finOpeningBalNew = BBAL_L.getBankOpeningBal(BNK.getbank_id(),"bank",finStartAndEndYr);
		bankopeningbal += finOpeningBalNew;
		
		pettycashOpeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","cashpaid","");
		double pettycashFinOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(),"cashpaid",finStartAndEndYr);
		pettycashOpeningbal += pettycashFinOpeningBal;
	 }	
	 else if(BNK.getextra2().equals("credit"))
	 {
		 
// 		 System.out.println("111111111111  "+finStartYr2+"-04-01 00:00:01");
// 		 System.out.println("111111111111  "+currentDateNew+" 23:59:59");
// 		 System.out.println("111111111111  "+finStartAndEndYr);
		 
		 
		 bankopeningbal=BNKCAL.getCreditBankOpeningBalance(BNK.getbank_id(),finStartYr2+"-04-01 00:00:01",currentDateNew+" 23:59:59","");
			double finOpeningBalNew = BBAL_L.getBankOpeningBal(BNK.getbank_id(),"bank",finStartAndEndYr);
			bankopeningbal += finOpeningBalNew;
	 }
 %>
<li class="colr">&#8377;
<%=formatter.format(bankopeningbal)%>
<%-- <%=formatter.format(Double.parseDouble(BNK.gettotal_amount()))%> --%>
<br>
 <%-- <%=NUM.convert(Integer.parseInt(sjf.format(Double.parseDouble(BNK.gettotal_amount())))) %><br> --%>
 <span>CLOSING BALANCE</span></li>
 
  <li class="colr1">&#8377;
  <%=formatter.format(pettycashOpeningbal)%>
  <%-- <%if(BNK_CL.getPettyCashAmount(bankId)!=null && !BNK_CL.getPettyCashAmount(bankId).equals("")){%><%=formatter.format(Double.parseDouble(BNK_CL.getPettyCashAmount(bankId)))%><%}else{ %>00<%} %> --%>
  <br>
 <span>PETTY CASH AMOUNT</span></li>
</ul>


</div>
</div>

<div class="vendor-list">
<div class="clear"></div>
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
			<form id="searchForm" action="adminPannel.jsp" method="get">
				<input type="hidden" name="page" value="<%=request.getParameter("page")%>">
				<input type="hidden" name="id" value="<%=request.getParameter("id")%>">
				<div class="search-list">
					<ul>
					<li><select name="reportType" id="reportType">
						<option value="bank" selected="selected">Bank</option>
						<option value="cashpaid" <%if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %> selected="selected" <%} %>>Petty Cash</option>
					</select></li>
					<li><select name="finYear" id="finYear"  Onchange="datepickerchange();">
					<option value="2020-21" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-21")) {%> selected="selected"<%} %>>2020-21</option>
					<option value="2019-20" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-20</option>
					<option value="2018-19" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%> selected="selected"<%} %>>2018-19</option>
					<option value="2017-18" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%> selected="selected"<%} %>>2017-18</option>
					<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
					<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
					</select></li>
						<li><input type="text"  name="fromdate" id="fromDate" class="DatePicker" value=""  readonly="readonly" /></li>
						<li><input type="text" name="todate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li>
					<li><input type="submit" class="click" value="Search"></input></li>
					</ul>
				</div></form>
<div class="icons">
<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
<span>
<!-- <ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul> -->

</span></div>
<%if(request.getParameter("finYear") != null){%>
<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<div class="printable" style="margin: 0 0 50px 0;">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">

<%
Calendar c1 = Calendar.getInstance(); 
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat dateFormatsj= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
dateFormat.setTimeZone(tz.getTimeZone("IST"));

//Calendar cal = Calendar.getInstance();
c1.set(Calendar.DATE, 1);
Date firstDateOfPreviousMonth = c1.getTime();
	String fromDate=(dateFormat2.format(c1.getTime())).toString();
		   fromDate=fromDate+" 00:00:01";
c1.set(Calendar.DATE, c1.getActualMaximum(Calendar.DATE)); 
Date lastDateOfPreviousMonth = c1.getTime();
	String toDate=(dateFormat2.format(c1.getTime())).toString();
			toDate=toDate+" 23:59:59";
//System.out.println("fromDate::"+fromDate+":::todate"+toDate);
	if(request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals("")){
		fromDate=request.getParameter("fromdate")+" 00:00:01";
	}
	if(request.getParameter("todate")!=null && !request.getParameter("todate").equals("")){
		toDate=request.getParameter("todate")+" 23:59:59";
	}
	
banktransactionsListing BAK_L=new mainClasses.banktransactionsListing();
bankdetailsListing BNKDETL=new bankdetailsListing();
productexpensesListing PRDEXP_L=new productexpensesListing();
subheadListing SUBL = new subheadListing();
employeesListing EMP=new employeesListing();
//List BANKL=BAK_L.getMbanktransactionsId(bankId);
// List BANKL=BAK_L.getBankTransactionBetweenDates(bankId,fromDate,toDate,request.getParameter("reportType"));

ReportesListClass1 reportesListClass = new ReportesListClass1();

List<banktransactions> BANKL = reportesListClass.getBankBalanceByBankIdAndBrsDate(bankId, fromDate.split(" ")[0], toDate.split(" ")[0], request.getParameter("reportType"), "");

paymentsListing PAYL=new paymentsListing();
SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy");
double debitBal=0.00;
double creditBal=0.00;

double openingBal=0.00;
double TotPaidAmtToVendors=0.00;

bankcalculations bankcal = new bankcalculations();
double finOpeningBal = BBAL_L.getBankOpeningBal(bankId, request.getParameter("reportType"), request.getParameter("finYear"));
String finStartYr = request.getParameter("finYear").substring(0, 4);
if(BNK.getextra2().equals("debit"))
	openingBal = bankcal.getBankOpeningBalance(bankId, finStartYr+"-04-01 00:00:01", fromDate, request.getParameter("reportType"),"");
else if(BNK.getextra2().equals("credit"))
	openingBal = bankcal.getCreditBankOpeningBalance(bankId, finStartYr+"-04-01 00:00:01", fromDate,"");


/* if(BAK_L.getOpeningBalAmountPaidToVendors(bankId,fromDate,request.getParameter("reportType"))!=null && !BAK_L.getOpeningBalAmountPaidToVendors(bankId,fromDate,request.getParameter("reportType")).equals("")){
	TotPaidAmtToVendors=Double.parseDouble(BAK_L.getOpeningBalAmountPaidToVendors(bankId,fromDate,request.getParameter("reportType")));
}
if(BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType"))!=null && !BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType")).equals("")){
	openingBal=Double.parseDouble(BAK_L.getOpeningBalance(bankId,fromDate,request.getParameter("reportType")))-TotPaidAmtToVendors;
} */
%>
<tr>
						<td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<%-- <tr><td colspan="7" align="center" style="font-weight: bold;"><%if(request.getParameter("reportType")!=null){if(request.getParameter("reportType").equals("cashpaid")){ %> Petty Cash <%}else{ %> Bank <%}}else{ %> Bank <%} %>Payments Reports</td></tr> --%>
					 <tr><td colspan="8" align="center" style="font-weight: bold;"><%=BNKDETL.getBankName(request.getParameter("id")) %> ACCOUNT</td></tr>
					<tr><td colspan="8" align="center" style="font-weight: bold;">Report From Date <%=CDF.format(dateFormatsj.parse(fromDate))%>  TO  <%=CDF.format(dateFormatsj.parse(toDate)) %> </td></tr>
<tr>
 <td class="bg" width="10%">DATE</td>
 <td class="bg" width="10%">BRS DATE</td>
<td class="bg" width="10%">VOUCHER</td>
<td class="bg" width="10%">EXPINV_ID</td>
<td class="bg" width="10%">SUB_HEAD</td>
<td class="bg" width="20%">NARRATION</td>
<td class="bg" width="10%">CHEQUE NO</td> 
<%if("credit".equals(BNK.getextra2())){ %>
<!-- <td class="bg" width="15%" align="right">CREDIT</td>
<td class="bg" width="15%" align="right">DEBIT</td> -->
<td class="bg" width="10%" align="right">DEBIT</td>
<td class="bg" width="10%" align="right">CREDIT</td>
<%} else {%>
<td class="bg" width="10%" align="right">DEBIT</td>
<td class="bg" width="10%" align="right">CREDIT</td>
<%} %>
<td class="bg" width="20%" align="right">BALANCE</td>
</tr>
<%-- <tr>
<td colspan="5" align="right" style="font-weight: bold;color: orange;">FINANCIAL OPENING BALANCE | </td>

<%if("credit".equals(BNK.getextra2())){ %>
<td colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(finOpeningBal)%></td>  
<td colspan="1" align="right">0.00</td>
<%} else { %>
<td colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(finOpeningBal)%></td>
<td colspan="1" align="right">0.00</td>
<%} %>
<td colspan="1" align="right"><%=formatter.format(finOpeningBal)%></td> 
<td></td>
</tr>


<tr>
<td colspan="5" align="right" style="font-weight: bold;color: orange;">OPENING BALANCE | </td>

<%if("credit".equals(BNK.getextra2())){ %>
<td colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(openingBal)%></td>  
<td colspan="1" align="right">0.00</td>
<%} else { %>
<td colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(openingBal)%></td>
<td colspan="1" align="right">0.00</td>
<%} %>
<td colspan="1" align="right"><%=formatter.format(openingBal)%></td> 
<td></td>
</tr> --%>

<tr>
<td colspan="5" align="right" style="font-weight: bold;color: orange;">OPENING BALANCE | </td>

<%


if("credit".equals(BNK.getextra2())){ %>
<td colspan="1" align="right">0.00</td>
<td colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(openingBal+finOpeningBal)%></td>  
<%} else { %>
<td colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(openingBal+finOpeningBal)%></td>
<td colspan="1" align="right">0.00</td>
<%} %>
<td colspan="1" align="right"><%=formatter.format(openingBal+finOpeningBal)%></td> 
<td></td>
</tr>
<%
openingBal += finOpeningBal;
String uniqueID="";
String uniqueValue="";
if(BANKL.size()>0){
	for(int i=0;i<BANKL.size();i++){
	BKT=(beans.banktransactions)BANKL.get(i);
	
%>
<tr>
<td><%=CDF.format(dateFormat.parse(BKT.getdate())) %></td>
<td><%=BKT.getbrs_date() %></td>
<td><a onclick="openMyModal('banktransactionsForm.jsp?txid=<%=BKT.getbanktrn_id() %>&id=<%=request.getParameter("id") %>'); return false;" target="_blank" href=""><%=BKT.getbanktrn_id() %></a></td>
<td><%=BKT.getextra3() %></td>
<td><%=BKT.getSub_head_id()%></td>
<td><%=BKT.getnarration() %></td>

<td align="center"><%=PAYL.getChequeNo(BKT.getextra5()) %></td>
<!-- For petty cash transactions -->
<%
if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %>
<td align="right"> 
	<%if(BKT.gettype().equals("pettycash")){
	creditBal = creditBal + Double.parseDouble(BKT.getamount());
	openingBal = openingBal + Double.parseDouble(BKT.getamount());
	%> <%=formatter.format(Double.parseDouble(BKT.getamount()))%> 
	<%}%>
</td>
<td align="right">
	<%
	if(BKT.gettype().equals("cashpaid")){
	debitBal=debitBal+Double.parseDouble(BKT.getamount());
	openingBal=openingBal-Double.parseDouble(BKT.getamount());%><%=BKT.getamount()%> <%}%>
</td>
<!-- For petty cash transactions -->
<%}else{ %>
<!-- For Bank deposits/cheque transactions -->
<td align="right">
	<%
	if(BKT.gettype().equals("deposit") ){
		if("credit".equals(BNK.getextra2()) ){ 
	debitBal=debitBal+Double.parseDouble(BKT.getamount());
	openingBal=openingBal-Double.parseDouble(BKT.getamount());} else{
		debitBal=debitBal+Double.parseDouble(BKT.getamount());
		openingBal=openingBal+Double.parseDouble(BKT.getamount());
	}
	%><%=formatter.format(Double.parseDouble(BKT.getamount()))%> <%}%>
</td>
<td align="right"> 
	<%
	
	
	if(BKT.gettype().equals("withdraw") || BKT.gettype().equals("pettycash") || BKT.gettype().equals("cashpaid") || BKT.gettype().equals("cheque") || BKT.gettype().equals("transfer") || BKT.gettype().equals("opening")){
		if("credit".equals(BNK.getextra2()) && BKT.gettype().equals("opening") && !BKT.gettype().equals("withdraw") ){ 
			creditBal=creditBal+Double.parseDouble(BKT.getamount());
			   openingBal=openingBal+Double.parseDouble(BKT.getamount());} else if("credit".equals(BNK.getextra2()) && !BKT.gettype().equals("withdraw") ){ 
			   creditBal=creditBal+Double.parseDouble(BKT.getamount());
				openingBal=openingBal-Double.parseDouble(BKT.getamount());
			   }else if("credit".equals(BNK.getextra2()) && BKT.gettype().equals("withdraw") ){ 
				creditBal=creditBal+Double.parseDouble(BKT.getamount());
				  openingBal=openingBal+Double.parseDouble(BKT.getamount());} else if(!"credit".equals(BNK.getextra2())){
				creditBal=creditBal+Double.parseDouble(BKT.getamount());
				openingBal=openingBal-Double.parseDouble(BKT.getamount());
				}
		%> <%=formatter.format(Double.parseDouble(BKT.getamount()))%> <% }%>
</td>
<!-- For Bank deposits/cheque transactions -->
<%} %>
 <td align="right" <%if(openingBal<0){%>style="color: red;"<%} %>><%=formatter.format(openingBal) %></td> 
</tr>
<%}} %>
<%-- <%if(request.getParameter("reportType")!=null && request.getParameter("reportType").equals("cashpaid")){ %> --%>
<tr>
	<td class="bg" colspan="7" align="right" style="font-weight: bold;color: orange;">CLOSING BALANCE | </td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(debitBal) %></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(creditBal) %></td>
	
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%-- <%=formatter.format(creditBal-debitBal)%> --%><%=formatter.format(openingBal) %></td> 
	<td></td>
</tr>
<%-- <%}else{ %>
<tr>
	<td class="bg" colspan="5" align="right" style="font-weight: bold;color: orange;">CLOSING BALANCE | </td>
	<td  class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(debitBal) %></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(creditBal) %></td>
	<td class="bg" colspan="1" style="font-weight: bold;" align="right"><%=formatter.format(debitBal-creditBal)%><%=openingBal %></td>
	<td></td>
</tr>

<%} %> --%>
</table>
</div>
</div>
</div>
</div><%} %>
<%}else{response.sendRedirect("adminPannel.jsp?page=banks");}%>

