<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<div class="vendor-page">

<!-- <div class="vendor-box">
<div class="vendor-title">Banks</div>
<div style="clear:both;"></div>

<div class="unpaid-box">
<div class="unpaid">Unpaid</div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OPEN BILL</li>
</ul>
</div>
<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OVERDUE</li>
</ul>
</div>
</div>
</div>
</div>
<div class="paid-box">
<div class="unpaid">Paid</div>
<div class="bills-cash">
<div class="bill-paid">
 <ul>
<li class="bill-amount">&#8377;2000</li>
 <li>Total Bank Balance</li>
</ul>
</div>
</div>
</div>
</div> -->

<div class="vendor-list">
<form id="filtration-form"  action="adminPannel.jsp" method="post">
<div class="search-list">
<ul>
<li><input type="hidden" name="page" value="indentReportList"></li>
<li><select name="status" onchange="filter();">
<option value="">Sort by Approval Status</option>
<option value="closed" <%if(request.getParameter("status")!=null && request.getParameter("status").equals("closed")){ %>  selected="selected" <%} %>>Approved List</option>
<option value="pending"  <%if(request.getParameter("status")!=null && request.getParameter("status").equals("pending")){ %>  selected="selected" <%} %>>Not-Approved List</option>
</select></li>
<li><select name="hoid" id="hoid" >
<option value="" >ALL</option>
<option value="5" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("5")){ %> selected="selected" <%} %>>SHRI SAI NIVAS</option>
<option value="3" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("3")){ %> selected="selected" <%} %>>Pooja stores</option>
<option value="4" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("4")){ %> selected="selected" <%} %>>Sansthan</option>
<option value="1" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("1")){ %> selected="selected" <%} %>>CHARITY</option>
</select></li>
<li>
<input type="submit" value="Search"/>
</li>
</ul>
</div>
</form>
<div class="clear"></div>
<div class="list-details">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
<tr><td colspan="4" height="10"></td> </tr>
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">INDENTS REPORT </td></tr>
</table>
<%
String status="pending";
String hoid="";
if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
	hoid=request.getParameter("hoid");
}
if(request.getParameter("status")!=null && !request.getParameter("status").equals("") ){
	if(request.getParameter("status").equals("pending")){
	status="pending";
	} else{
		status="closed";
	}
}
mainClasses.indentListing IND_L = new indentListing();
indentapprovalsListing APPR_L=new indentapprovalsListing();
superadminListing SAD_l=new superadminListing();
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat time=new SimpleDateFormat("HH:mm");
List IND_List=IND_L.getIndentsBasedOnDeptAndStatus(hoid,status);
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
List pendingIndents=IND_L.getPendingIndents();%>
<%
if(IND_List.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="7"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="5%" align="center">S.No.</td>
<td class="bg" width="8%" align="center">Indent Invoice</td>
<td class="bg" width="13%" align="center">Company</td>
<!-- <td class="bg" width="20%">Vendor Name</td> -->
<td class="bg" width="15%" align="center">Date/Time</td>
<td class="bg" width="10%" align="center">Requirement</td>
<td class="bg" width="15%" align="center">Status</td>
<td class="bg" width="35%" align="center">Approved By</td>
</tr>
</table></td>
</tr>
<%List APP_Det=null;
String approvedBy="";
for(int i=0; i < IND_List.size(); i++ ){
	INDT=(indent)IND_List.get(i);
	APP_Det=APPR_L.getIndentDetails(INDT.getindentinvoice_id());
	if(APP_Det.size()>0){
		for(int j=0;j<APP_Det.size();j++){
			IAP=(beans.indentapprovals)APP_Det.get(j);
			if(j==0){
				approvedBy=SAD_l.getSuperadminname(IAP.getadmin_id())+"--TIME  ["+CDF.format(SDF.parse(IAP.getapproved_date()))+"]";
			}else{
				approvedBy=approvedBy+"<br></br>"+SAD_l.getSuperadminname(IAP.getadmin_id())+" --TIME ["+CDF.format(SDF.parse(IAP.getapproved_date()))+" "+time.format(SDF.parse(IAP.getapproved_date()))+"]";
			}
		}
	} %>
<tr>
<td width="5%" align="center"><%=i+1%></td>
<td width="8%" align="center"><a href="adminPannel.jsp?page=indentDetailedReport&invid=<%=INDT.getindentinvoice_id()%>&approvedBy=<%=approvedBy%>"><b><%=INDT.getindentinvoice_id() %></b></a></td>
<td width="13%" align="center"><%=HOA_CL.getHeadofAccountName(INDT.gethead_account_id())%></td>
<%-- <td><ul>
<li><span><a href="adminPannel.jsp?page=indentDetailedReport&invid=<%=INDT.getindentinvoice_id()%>"><%=INDT.getvendor_id()%>  </a></span><br /></li>
</ul></td> --%>
<td width="15%" align="center"><%=CDF.format(SDF.parse(INDT.getdate()))%> <%=time.format(SDF.parse(INDT.getdate()))%></td>
<td width="10%" align="center"><%=INDT.getrequirement()%></td>
<td width="15%"align="center"><%=INDT.getstatus()%></td>
<td width="34%" align="left"><%=approvedBy %></td>
</tr>
<%
approvedBy="";
} %>
</table>
<%}else{%>
<div align="center"><h1>No Indents Raised Yet</h1></div>
<%}%>


</div>
</div>




</div>
