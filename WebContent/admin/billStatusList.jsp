<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="beans.daily_bill_status" %>
<%@page import="mainClasses.daily_bill_statusListing" %>
<%@page import="java.util.List"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Daily Bill Status List</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;

}
.new-table td{
padding: 2px 0 2px 5px !important;}


</style>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>

<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
</script>

<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<jsp:useBean id="DBS" class="beans.daily_bill_status"></jsp:useBean>
</head>
<body>
<%if(session.getAttribute("adminId")!=null)
{ 
	daily_bill_statusListing DBSL = new daily_bill_statusListing();	
	Calendar c1 = Calendar.getInstance(); 
	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat dateFormat3= new SimpleDateFormat("dd-MMM-yyyy");
	String fromdate="";
	String todate="";
	String onlyfromdate="";
	String onlytodate="";
	
	c1.getActualMaximum(Calendar.DAY_OF_MONTH);
	int lastday = c1.getActualMaximum(Calendar.DATE);
	c1.set(Calendar.DATE, lastday);  
	todate=(dateFormat2.format(c1.getTime())).toString();
	c1.getActualMinimum(Calendar.DAY_OF_MONTH);
	int firstday = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday); 
	fromdate=(dateFormat2.format(c1.getTime())).toString();
	if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
		 fromdate=request.getParameter("fromDate");
	}
	if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
		todate=request.getParameter("toDate");
	} 
%>
<div>
<div class="vendor-page">
<div class="vendor-list">
				<div class="arrow-down">
		</div>
	<form  name="billstatusform" id="billstatusform" method="post" style="padding-left: 50px;padding-top: 30px;">
				 <table width="80%" border="0" cellspacing="0" cellpadding="0">
	<tr>
<td>From Date</td>
<td>To Date</td>
</tr>			 
	<tr>
	<td> <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
	<td><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
	<input type="hidden" name="page" value="billStatusList"/>
	 </td>		
	 <td><input type="submit" value="SEARCH" class="click" /></td>
	</tr></table>
	</form>
	<%
	onlyfromdate=fromdate;
	fromdate=fromdate+" 00:00:00";
	onlytodate=todate;
	todate=todate+" 23:59:59";
	%>
	<div class="icons">
	<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
	<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
	</div>
	<div class="clear"></div>
	<div class="list-details">
	<div class="printable" id="tblExport">
	<% 
if(request.getParameter("fromDate") != null && !request.getParameter("toDate").equals("")){%>
<table width="95%"  cellspacing="0" cellpadding="0" style="margin-bottom: 10px; margin-top:10px; margin:auto;">
<tbody> <tr>
<td colspan="3" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td>
</tr>
<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;">DAILY BILL STATUS LIST</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;"><%=onlyfromdate %> TO <%=onlytodate %> </td></tr>
</tbody></table>

<table width="100%"  cellpadding="1" cellspacing="1">
<tr>
	<td class="bg" width="20%" style="font-weight: bold;">S.NO.</td>
	<td class="bg" width="30%" style="font-weight: bold;">CREATED DATE</td>
	<td class="bg" width="40%" style="font-weight: bold;">EXCEL</td>
	<td class="bg" width="10%" style="font-weight: bold;">DELETE</td>
</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
  <td  colspan="9">
  <table width="100%"  cellpadding="1" cellspacing="1">
  <%
  List DBSList = DBSL.getDailyBillStatusBetweenDates(fromdate, todate);
 if(DBSList != null && DBSList.size() > 0)
 {
  for(int i=0; i < DBSList.size(); i++ )
	{ 
	  DBS=(daily_bill_status)DBSList.get(i); 
  %>
<tr style="position: relative;" >
<td width="20%" ><%=i+1%></td>
<td width="30%"><%=dateFormat3.format(dateFormat.parse(DBS.getCreatedDate()))%></td>
<td width="50%">
<iframe src="https://docs.google.com/gview?url=http://www.accounts.saisansthan.in/dailybillstatus/<%=DBS.getFileName()%>&embedded=true" style="width:350px; height:250px; padding-bottom:10px;" frameborder="0"></iframe> 
</td>
<td><a href="billstatus_Delete.jsp?Id=<%=DBS.getBill_id()%>">Delete</a></td>
</tr>

<tr><td colspan="3" width="100%"><hr></td></tr>
<%}}else{ %>
<div align="center">
	<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>EMPTY LIST ..!</span></div>
</div><%} %>
</table>
</td>
</tr>
</table>
<%} %>
</div>
</div>
</div>
</div>
</div>
<%}%>
</body>
</html>