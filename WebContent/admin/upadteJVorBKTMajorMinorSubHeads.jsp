<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="beans.productexpensesService"%>
<%@ page import="beans.customerpurchasesService"%>
<%@ page import="beans.banktransactionsService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>upadteJVorBKTMajorMinorSubHeads</title>

</head>
<body>

<%

    productexpensesService productexpenses=new productexpensesService();
    customerpurchasesService customerpurchases=new customerpurchasesService();
    banktransactionsService banktransactions=new banktransactionsService();

    String majorHeadId="";
    String minorHeadId="";
    String subHeadId="";
   
    if(request.getParameter("majorHeadId")!=null && request.getParameter("majorHeadId")!=""){
    
    	String majorHead[]=request.getParameter("majorHeadId").split(" ");
    	majorHeadId=majorHead[0];
    	
    }
    if(request.getParameter("minorhead")!=null && request.getParameter("minorhead")!=""){
    	String minorHead[]=request.getParameter("minorhead").split(" ");
    	minorHeadId=minorHead[0];
    	
    }
    if(request.getParameter("subhead")!=null && request.getParameter("subhead")!=""){
    	String subHead[]=request.getParameter("subhead").split(" ");
    	subHeadId=subHead[0];
    }
    
    
    String expenseId=request.getParameter("expenseId");
    
    String bankTransactionId=request.getParameter("bankTransactionId");
    
    String purchaseId=request.getParameter("purchaseId");
    
    String headOfAccount=request.getParameter("headOfAccount");
    
    String amount=request.getParameter("amount");
    
    
    String entryDate=request.getParameter("fromDate");
    System.out.println("entryDate::::"+entryDate);
    
    String hhmmss=request.getParameter("hhmmss");
    System.out.println("hhmmss:::"+hhmmss);
    
    if(request.getParameter("expenseId").trim()!=null && !request.getParameter("expenseId").equals("null")){
    	Double amount1=Double.parseDouble(amount);
    	productexpenses.updateMajorMinorSubHeadsProductExpenses(majorHeadId, minorHeadId, subHeadId,request.getParameter("expenseId"),amount1,entryDate+" "+hhmmss);
     }
    if(request.getParameter("bankTransactionId")!=null && !request.getParameter("bankTransactionId").equals("null")){
    	banktransactions.updateMajorMinorSubHeadsBKT(majorHeadId, minorHeadId, subHeadId, request.getParameter("bankTransactionId"),amount,entryDate+" "+hhmmss);
    }if(request.getParameter("purchaseId").trim()!=null && !request.getParameter("purchaseId").equals("null")){
    	customerpurchases.updateMajorMinorSubHeadCUP(majorHeadId, minorHeadId, subHeadId, request.getParameter("purchaseId"),amount,entryDate+" "+hhmmss);
    }
    
    response.sendRedirect("JVorBKTEdit.jsp?purchaseId="+purchaseId+"&expenseId="+expenseId+"&bankTransactionId="+bankTransactionId+"&majorHeadId="+majorHeadId+"&minorHeadId="+minorHeadId+"&subHeadId="+subHeadId+"&headOfAccount="+headOfAccount+"&amount="+amount+"&entryDate="+entryDate+" "+hhmmss);
%>

</body>
</html>