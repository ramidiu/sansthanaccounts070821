<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard</title>
  <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
  <style type="text/css">.menu ul li ul li ul.level-2{left:202px;}</style>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="../css/shortcuts.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="../css/styles.css" type="text/css" media="screen" />
</head>
<body>
<!-- <div class="top-nav">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>   
      </button>
      <a class="navbar-brand" href="#"><img class="img-responsive" src="../admin/images/finallogo.jpg" style="display:block !important;width:200px;"> </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav ml-250 mt-25">
      	<li class="mt-5" ><b class="mr-10">FIND:</b></li>
        <li><input type="text" class="form-control" placeholder="type and press enter"></li>
         
      </ul>
     
       <ul class="list-unstyled pull-right">
       	<li><a href="#" class="btn btn-primary"> Sign Out</a></li>
       	<li><a href="#">Monday 19 May 2017</a></li>
        <li><a href="#" class="text-blue text-bold"> 9:41 am</a></li>
       </ul>
    </div>
  </div>
</nav>
</div> -->
<!-- <div class="navigation">
<nav class="navbar navbar-inverse">
  <div class="container-fluid padding0">
    <div class="navbar-header">
       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
     
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Enquaries</a></li>
        <li><a href="#">Diary</a></li> 
        <li><a href="#">Applicants</a></li> 
        <li><a href="#">Vendors</a></li>
        <li><a href="#">Contact</a></li> 
        <li><a href="#">Appraisals</a></li> 
        <li><a href="#">Property</a></li>
        <li><a href="#">Viewing</a></li> 
        <li><a href="#">Offers</a></li>
        <li><a href="#">sales</a></li> 
        <li><a href="#">Reports</a></li> 
        <li><a href="#">Admin</a></li>  
      </ul>
      
    </div>
  </div>
</nav>
</div> -->
<div class="dashboard">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
					<div class="left-section">
						<div class="col-lg-2 bg-light-blue">
								<div class="account-holder">
									<h4 class="text-left pt-10 text-brown"><i class="fas fa-list"></i>&nbsp;&nbsp;Ramidi's Account</h4>
								</div>
								<div class="line"></div>
								<div class="new-account">
									<H3 class="text-brown"><i class="fas fa-plus-square"></i>&nbsp;&nbsp; Create New</H3>
									<ul class="list-unstyled">
											<li><a href="#">New Enquiry</a></li>
											<li><a href="#">New Applicant</a></li>
											<li><a href="#">New Unqualified Applicant</a></li>
											<li><a href="#">New Market Appraisal</a></li>
											<li><a href="#">New Property</a></li>
											<li><a href="#">New Viewing</a></li>
									</ul>
								</div>
								<div class="line"></div>
								<div class="shortcuts">
									<h3 class="text-brown"><i class="fas fa-thumbtack"></i>&nbsp;&nbsp;Shortcuts</h3>
									<ul class="list-unstyled">
										<li><a href="#">Send Property Details</a></li>
										<li><a href="#">Print Tray</a></li>
									</ul>
								</div>
								<div class="line"></div>
								<div class="get-help">
										<h3 class="text-brown"><i class="fab fa-hire-a-helper"></i>&nbsp;&nbsp;Get Help</h3>
										<ul class="list-unstyled">
											<li><a href="#">View Help Center</a></li>
										</ul>
								</div>
								<div class="line"></div>
								<div class="to-do">
									<h3 class="text-brown"><i class="far fa-calendar-check"></i>&nbsp;&nbsp;To Do  <a href="#"><i class="fas fa-plus pull-right"></i></a></h3>
									<h5><strong><span class="text-brown">Overdue:</span> Showing 3 of 10</strong></h5>
									<ul class="list-unstyled">
										<li><i class="fas fa-gavel"></i>&nbsp;&nbsp;12-00 pm - Follow up offser, if not heard? &nbsp;&nbsp;<input type="checkbox" name="c1" style="margin-left:45px;"></li>
										<li><i class="fas fa-gavel"></i>&nbsp;&nbsp;Add Photos &nbsp;&nbsp;<input type="checkbox" name="c1" style="margin-left:74px;"></li>
										<li><i class="fas fa-gavel"></i>&nbsp;&nbsp;Chase Instructions &nbsp;&nbsp;<input type="checkbox" name="c1" style="margin-left:29px;"></li>
									</ul>
								</div>
						</div>
					</div>
					<div class="right-section">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
							<ul class="nav nav-tabs mt-20">
									<li class="active"><a href="#" data-toggle="tab" data-target="#dashboard">My Dashboard</a></li>
									<li><a href="#" data-toggle="tab" data-target="#sales">Residential Sales</a></li>
									<li><a href="#" data-toggle="tab" data-target="#lettings">Residential Lettings</a></li>
									<li><a href="#" data-toggle="tab" data-target="#commercial"> Commercial</a></li>
									<li><a href="#" data-toggle="tab" data-target="#agriculture">Agriculture</a></li>
							</ul>
							<div class="tab-content mt-30">
								<div id="dashboard">
										
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
										<div class="panel panel-md ">
											<div class="panel-heading">
													<h5>Receipt and Payment<a href="#"><i class="fas fa-times pull-right text-white"></i></a></h5>
											</div>
											<div class="panel-body">
													<ul class="list-unstyled">
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?page=receipts-payments-report_neww&finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&head_account_id=1&cumltv=cumulative" target="_blank" class="btn btn-brown btn-submit">Charity</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?page=receipts-payments-report_neww&finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&head_account_id=3&cumltv=cumulative" target="_blank" class="btn btn-brown">Pooja Store</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?page=receipts-payments-report_neww&finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&head_account_id=4&cumltv=cumulative" target="_blank" class="btn btn-brown">Sansthan</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?page=receipts-payments-report_neww&finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&head_account_id=5&cumltv=cumulative" target="_blank" class="btn btn-brown">SaiNivas</a></li>
													</ul>
											</div>
										</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
										<div class="panel panel-md ">
											<div class="panel-heading">
													<h5>Income and Expenditure<a href="#"><i class="fas fa-plus pull-right text-white"></i></a></h5>
											</div>
											<div class="panel-body">
													<ul class="list-unstyled">
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=DayWise-Income-Expenditure-Report_neww&head_account_id=1&cumltv=cumulative" target="_blank" class="btn btn-brown btn-submit">Charity</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=DayWise-Income-Expenditure-Report_neww&head_account_id=3&cumltv=cumulative" target="_blank" class="btn btn-brown">Pooja Store</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=DayWise-Income-Expenditure-Report_neww&head_account_id=4&cumltv=cumulative" target="_blank" class="btn btn-brown">Sansthan</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=DayWise-Income-Expenditure-Report_neww&head_account_id=5&cumltv=cumulative" target="_blank" class="btn btn-brown">SaiNivas</a></li>
													</ul>
													
											</div>
										</div>
										</div>
										
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="panel panel-md ">
											<div class="panel-heading">
													<h5>Balance Sheet<a href="#"><i class="fas fa-wrench pull-right text-white"></i></a></h5>
											</div>
											<div class="panel-body">
													<ul class="list-unstyled">
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=balanceSheet_neww&head_account_id=1&cumltv=cumulative" target="_blank" class="btn btn-brown btn-submit">Charity</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=balanceSheet_neww&head_account_id=3&cumltv=cumulative" target="_blank" class="btn btn-brown">Pooja Store</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=balanceSheet_neww&head_account_id=4&cumltv=cumulative" target="_blank" class="btn btn-brown">Sansthan</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=balanceSheet_neww&head_account_id=5&cumltv=cumulative" target="_blank" class="btn btn-brown">SaiNivas</a></li>
													</ul>
													
											</div>
										</div>
										</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="panel panel-md ">
											<div class="panel-heading">
													<h5>Trail Balance<a href="#"><i class="fas fa-times pull-right text-white"></i></a></h5>
											</div>
											<div class="panel-body">
													<ul class="list-unstyled">
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=trailBalanceReport_new&head_account_id=1&cumltv=cumulative" target="_blank" class="btn btn-brown btn-submit">Charity</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=trailBalanceReport_new&head_account_id=3&cumltv=cumulative" target="_blank" class="btn btn-brown">Pooja Store</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=trailBalanceReport_new&head_account_id=4&cumltv=cumulative" target="_blank" class="btn btn-brown">Sansthan</a></li>
														<li><a href="<%=request.getContextPath()%>/admin/adminPannel.jsp?finYear=2017-18&fromDate=2017-04-01&toDate=2018-03-31&page=trailBalanceReport_new&head_account_id=5&cumltv=cumulative" target="_blank" class="btn btn-brown">SaiNivas</a></li>
													</ul>
													
											</div>
										</div>
										</div>
										</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>

</body>
</html>