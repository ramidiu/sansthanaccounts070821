<%@page import="beans.indentapprovals"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.util.List" %>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
<jsp:useBean id="VND" class="beans.vendors"/>
	<%String INVId=request.getParameter("invid");
	String statuss=request.getParameter("statuss");
	System.out.println("status..."+statuss);
	String Producttype="";
	mainClasses.indentListing INDT_L = new indentListing();
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
	mainClasses.employeesListing EMP_L = new employeesListing();
	productsListing PRD_L = new productsListing();
	vendorsListing VND_L = new mainClasses.vendorsListing();
	List INDT_List=INDT_L.getMindentsByIndentinvoice_id(INVId); 
	if(INDT_List.size()>0){
		INDT=(indent)INDT_List.get(0);
		String temp[]=INDT.getvendor_id().split(" ");
		List VNDT_LIST=VND_L.getMvendors(temp[0]);
		VND=(vendors)VNDT_LIST.get(0);
		Producttype=INDT.gethead_account_id();
		%>
<div class="vendor-page">
<div class="icons">
<!-- <span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span> -->
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Columns</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>

<div class="vendor-box">
<div class="name-title"><%=INDT.getvendor_id()%> <span>(<%=HOA_CL.getHeadofAccountName(INDT.gethead_account_id())%>)</span></div>
<div style="clear:both;"></div>
<div class="details-list">

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td colspan="1"><span>Vendor Name :</span></td><td><%=VND.gettitle()%> <%=VND.getfirstName()%> <%=VND.getlastName()%></td>
</tr>
<tr>
<td colspan="1"><span>Vendor Address :</span></td><td><%=VND.getaddress()%>,<%=VND.getcity()%>,<%=VND.getpincode()%></td>
</tr>
<tr>
<td colspan="1"><span>Vendor Phone No :</span></td><td><%=VND.getphone()%></td>
</tr>
<tr>
<td colspan="1"><span>Vendor Email Id :</span></td><td><%=VND.getemail()%></td>
</tr>
<tr>
<%-- <td colspan="2"><span>Indent Raised By:</span><%=EMP_L.getMemployeesName(INDT.getemp_id())%></td> --%>
<td colspan="1"><span>Indent Raised By :</span></td><td>GS</td>
</tr>
<tr>
<td colspan="1"><span>Indent Raised Date :</span></td><td><%=INDT.getdate()%></td>
</tr>

</table>

</div>
 <div><center>Approved By:<br><br>
<%
	if(request.getParameter("approvedBy")!=null)
	{
		String approvedBy=request.getParameter("approvedBy");
		out.println(approvedBy);
		System.out.println("approved by.."+approvedBy);
	}
	else
	{
		System.out.println("approved by.."+request.getParameter("approvedBy"));
		out.println("Not-Approved");
	}

%>
</center></div> 
</div>
<%mainClasses.indentapprovalsListing APPR_L=new mainClasses.indentapprovalsListing();
List APP_Det=APPR_L.getIndentDetails(INVId);
%>
<div class="vendor-list">
<div class="trans">Indent report details</div>
<div class="clear"></div>
<div class="arrow-down"><img src="../images/Arrow-down.png"></div>
<form method="post" action="indentStatusUpdate.jsp">
<div class="search-list">
<ul>
<li>
<input type="hidden" name="indentid" value="<%=INVId%>"/>
<select name="reportStatus">
<option value="">Report Status</option>
<%if(!INDT.getstatus().equals("indentSubmitToVendor")){ %>
<option value="indentSubmitToVendor">Indent sent to vendor</option><%} %>
<option value="itemsReceived">Indent Items Received</option>
</select></li>
<li><input type="submit" name="submit" value="Update"  <%if(APP_Det.size()<3){ %> disabled="disabled" <%} %>></li>
<!-- <li><input type="search" placeholder="find a vendor"/></li> -->
</ul>
</div>
</form>
<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<div id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%"><input type="checkbox"></td>
<td class="bg" width="20%">Vendor Name</td>
<td class="bg" width="8%">Product Name</td>
<td class="bg" width="20%">Quantity Raised</td>
<td class="bg" width="20%">Balance Quantity</td>
</tr>
<%banktransactionsListing BAK_L=new mainClasses.banktransactionsListing(); 
employeesListing EMP=new employeesListing();
INDT_List=INDT_L.getMindentsByIndentinvoice_id(INVId); 
if(INDT_List.size()>0){
	for(int i=0;i<INDT_List.size();i++){

		INDT=(indent)INDT_List.get(i);
		
%>
<tr>
<td><input type="checkbox"></td>
<td><%=INDT.getvendor_id()%> [<%=VND_L.getMvendorsAgenciesName(INDT.getvendor_id()) %>]</td>
<td><%=PRD_L.getProductsNameByCat1(INDT.getproduct_id(),Producttype) %></td>
<td><%=INDT.getquantity() %></td>
<td><%=PRD_L.getProductsStockByCat(INDT.getproduct_id(),Producttype) %></td>
</tr>
<%}} %>
</table>
</div>
</div>
</div>
</div>
<%}else{response.sendRedirect("adminPannel.jsp?page=banks");}%>

