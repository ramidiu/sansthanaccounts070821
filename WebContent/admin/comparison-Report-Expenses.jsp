<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="helperClasses.SplitDatesInToMonths"%>
<%@page import="helperClasses.GetDaysInAMonth"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!--Date picker script  -->
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>

<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$('#'+billId).toggle();
	/* if(document.getElementById(billId).style.display=="none"){
		//document.getElementById(billId).style.display='block';
		$('#'+billId).toggle();
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	} */
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<%
	DateFormat extra1DateFormat=new SimpleDateFormat("MM-dd");
	DateFormat userDateFormat=new SimpleDateFormat("MMMM  dd");
	DateFormat userDateFormat2=new SimpleDateFormat("MMM  yyyy");
	%>
	<!-- main content -->
	
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="PRODEXP" class="beans.productexpenses" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form name="searchForm" id="searchForm" action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" name="page" value="comparison-Report-Expenses"></input>
						<select name="headID" id="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="5" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("5")){ %>selected="selected" <%} %>>SAI NIVAS</option>
						<option value="3" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("3")){ %>selected="selected" <%} %>>POOJA STORES</option>
						</select>
						
						</li>
						<%
						Calendar c1 = Calendar.getInstance(); 
						TimeZone tz = TimeZone.getTimeZone("IST");

						DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
						DateFormat  onlyYear= new SimpleDateFormat("yyyy");
						dateFormat.setTimeZone(tz.getTimeZone("IST"));
						
						/* String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); */
						
						c1.getActualMaximum(Calendar.DAY_OF_MONTH);
						int lastday = c1.getActualMaximum(Calendar.DATE);
						c1.set(Calendar.DATE, lastday);  
						String todate=(dateFormat2.format(c1.getTime())).toString();
						c1.getActualMinimum(Calendar.DAY_OF_MONTH);
						int firstday = c1.getActualMinimum(Calendar.DATE);
						c1.set(Calendar.DATE, firstday); 
						String fromdate=(dateFormat2.format(c1.getTime())).toString();
						
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	
	
	
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	productexpensesListing PEXP_L = new productexpensesListing();
	banktransactionsListing BKTRAL=new banktransactionsListing();
	productsListing PRDL=new productsListing();
	employeesListing EMPL=new employeesListing();
	subheadListing SUBHL=new subheadListing();
	List BANK_DEP=null;
	
	
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	
	String hoaID="4";
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoaID=request.getParameter("headID");
	}
	
	
	SplitDatesInToMonths getDates = new SplitDatesInToMonths();
	List<String> list = getDates.getListOfDates(fromdate, todate);
	
	List PEXP_list=null;
	PEXP_list=PEXP_L.getList("distinct","",hoaID,fromdate,todate,"","","");
	List SAL_DETAIL=null;
	double totalAmount=0.00; 
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){%>
	 <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>	
    <div class="clear"></div>
  <div class="printable">
     <table width="100%" cellpadding="0" cellspacing="0" >
						<tr><td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;">Comparison Report Of Expenses</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date <%=df2.format(chngDF.parse(fromdate))%>  TO  <%=df2.format(chngDF.parse(todate)) %> </td></tr></table>
          
					 
						<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
                       <tr>
						<td colspan="20">
						<table width="100%" cellpadding="0" cellspacing="0" border="1">
						<tr>
							<!-- <td colspan="20" class="yourID"><table width="100%" cellpadding="0" cellspacing="0" border="1"> -->
							<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="30%" style="font-weight: bold;">PARTICULARS</td> 
							
							<%
							int f = (list.size())/2;
							for(int k = 0; k < list.size(); k += 2)
							{
								String date = list.get(k);
								String displayDate = userDateFormat2.format(chngDF.parse(date));
							%>
							 <td class="bg" style="font-weight: bold;"><%=displayDate%></td> 
							<%} %>
<td class="bg" style="font-weight: bold;">TOTAL</td> 
<!-- </table> -->
						</tr>

						<%
						List PEXP_List2 = null;
						double grandtotal = 0.0;
						for(int i=0; i < PEXP_list.size(); i++){ 
							String prodId=(String)PEXP_list.get(i);
							%>
							<tr>
							<td width="5%"><%=i+1%></td>
							<td  width="30%"><%=prodId%>  <%=PRDL.getProductsNameByCat(prodId,hoaID)%></td>
							<% 
							double total = 0.0;
							for(int k = 0; k < list.size(); k += 2)
							{
								String fdate=list.get(k)+" 00:00:00";
								String tdate=list.get(k+1)+" 23:59:59";	
								PEXP_List2=PEXP_L.getList("","",hoaID,fdate,tdate,"sub_head_id",prodId,"");
								
								double amount = 0.0;
								
								if(PEXP_List2.size() > 0)
								{
									PRODEXP=(beans.productexpenses)PEXP_List2.get(0);
									amount = Double.parseDouble(PRODEXP.getamount());
								}
								%>
								<td><%=amount%></td>
								<% 
								total += amount;
								grandtotal += amount;
							}%>
							<td><%=total%></td>
							<%} %>
							</tr>
						    
						
					  
					   </table>
					   </td>
					   </tr>
					   <%-- <tr><td colspan="18" text-align="center">GRAND TOTAL</td><td><%=grandtotal%></td></tr> --%>
					   <tr><td>&nbsp;</td><td>&nbsp;</td><tr>
                       
					</table>
					
				</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>Please provide your search!</span></div>
					</div>
					<%}%>
					<div style="clear:both; padding-bottom:10px;"></div>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>