<%@page import="model.SansthanAccountsDate"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.accounts.saisansthan.vendorBalances"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.minorhead" %>
<%@page import="mainClasses.minorheadListing" %>
<%@page import="beans.subhead" %>
<%@page import="mainClasses.subheadListing" %>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Major Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>

<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;

}
.new-table td{
padding: 2px 0 2px 5px !important;}


</style>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
  
 <script>

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 
</script>
   
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
</head>


<body>
<%if(session.getAttribute("adminId")!=null){ 
		customerpurchasesListing CP_L=new customerpurchasesListing();
		banktransactionsListing BNK_L = new banktransactionsListing();
		majorheadListing MH_L=new majorheadListing();
		vendorsListing VNDR=new vendorsListing();
		bankdetailsListing BNKL=new bankdetailsListing();
		bankcalculations BNKCAL=new bankcalculations();
		vendorBalances VNDBALL=new vendorBalances();
		bankbalanceListing BBL =new bankbalanceListing(); 
		Object key =null;
		Object value=null;
		Iterator VendorPayments=null;
		Calendar c1 = Calendar.getInstance(); 
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat df=new DecimalFormat("#.##");
		String fromdate="";
		String onlyfromdate="";
		String todate="";
		String Todate="";
		String reporttype="";
		String reporttype1="";
		String cashtype="online pending";
		String cashtype1="othercash";
		String cashtype2="offerKind";
		String cashtype3="journalvoucher";
		double bankopeningbal=0.0;
		double bankopeningbalcredit=0.0;
		String onlytodate="";
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		todate=(dateFormat2.format(c1.getTime())).toString();
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromdate=(dateFormat2.format(c1.getTime())).toString();
		if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
			 fromdate=request.getParameter("fromDate");
		}
		if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
			todate=request.getParameter("toDate");
		}
		headofaccountsListing HOA_L=new headofaccountsListing();
		List headaclist=HOA_L.getheadofaccounts();
		String hoaid="1";
		/* if(hoaid.equals("1")){
			reporttype = "Charity-cash";
			reporttype1 = "Not-Required";	
			} */
		if(request.getParameter("head_account_id")!=null && !request.getParameter("head_account_id").equals("")){
			hoaid=request.getParameter("head_account_id");
		}
	%>
<div>
			<div class="vendor-page">
				<form  name="departmentsearch" id="departmentsearch" method="post" style="padding-left: 70px;padding-top: 30px;">
				<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr>
<td class="border-nn" colspan="2">Select Fin Yr</td>
<td class="border-nn">From Date</td>
<td class="border-nn">To Date</td>
<td class="border-nn" colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td class="border-nn">Report Type</td>
</tr>
				<tr>
			<td colspan="2">
					<select name="finYear" id="finYear"  Onchange="datepickerchange();">
<%-- 					<option value="2020-21" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2020-21")) {%> selected="selected"<%} %>>2020-2021</option>		 --%>
<%-- 					<option value="2019-20" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option> --%>
					<option value="2018-19" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%>selected="selected"<%} %>>2018-2019</option>
					<option value="2017-18" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%> selected="selected"<%} %>>2017-2018</option>		
					<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-2017</option>
					<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-2016</option>
					</select>
				</td>
				<td class="border-nn"> <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
				<td class="border-nn"><input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/>
				<input type="hidden" name="page" value="balanceSheet_neww"/>
				 </td>
<td colspan="2"><select name="head_account_id" id="head_account_id" onChange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
<option value="SasthanDev" >Sansthan Development</option>
<%
if(headaclist.size()>0){
	for(int i=0;i<headaclist.size();i++){
	HOA=(headofaccounts)headaclist.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><select name="cumltv" id="cumltv">
<option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option>
<option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option>
</select></td>
<td><input type="submit" value="SEARCH" class="click" /></td>
</tr></tbody></table> </form>
				
				<% 
				String finYr = request.getParameter("finYear");			 
				String finStartYr = "";
				if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals(""))
				{
					finStartYr = request.getParameter("finYear").substring(0, 4);
				}
				
				String typeofbanks[]={"bank","cashpaid"};	
				String headgroup="";
				double total=0.0;
				double credit=0.0;
				double debit=0.0;
				double creditliab=0.0;
				double debitliab=0.0;
				double creditassest=0.0;
				double debitassest=0.0;
				subheadListing SUBH_L=new subheadListing();
				minorheadListing MINH_L=new minorheadListing();
				productexpensesListing PEXP_L=new productexpensesListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				if(request.getParameter("head_account_id")!=null){
					hoaid=request.getParameter("head_account_id");
					if(hoaid.equals("SasthanDev")){
						headgroup="2";
						hoaid="4";
					} else if(hoaid.equals("4")){
						headgroup="1";
						reporttype = "pro-counter";
						reporttype1 = "Development-cash";	
					}else if(hoaid.equals("1")){
						reporttype = "Charity-cash";
						reporttype1 = "Not-Required";
					}
					else if(hoaid.equals("3")){
						reporttype = "poojastore-counter";
						reporttype1 = "Not-Required";
					}
					else if(hoaid.equals("5")){
						reporttype = "Sainivas";
						reporttype1 = "Not-Required";	
					}
				}
				onlyfromdate=fromdate;
				fromdate=fromdate+" 00:00:00";
				onlytodate=todate;
				todate=todate+" 23:59:59";
			 	Todate=todate;
			 	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
			 		fromdate = finStartYr+"-04-01 00:00:00";
			 		onlyfromdate =  finStartYr+"-04-01";
				}
				
if(request.getParameter("fromDate") != null && !request.getParameter("toDate").equals("")){	
	List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid, "Assets",headgroup);
	
	List banklist=BNKL.getBanksBasedOnHOA(hoaid);
	List creditbanklist=BNKL.getCreditBanksBasedOnHOA(hoaid);
	List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueType(hoaid,"Liabilities",headgroup);
	/* HashMap VendorPaymentPedning=VNDBALL.getVendorBalances(hoaid, fromdate, todate); */
	/* HashMap VendorPaymentPedning=VNDBALL.getVendorBalancesForFinYr(hoaid, fromdate, todate,request.getParameter("finYear")); */
	HashMap VendorPaymentPedning=VNDBALL.getVendorBalancesForFinYr1(hoaid, fromdate, todate,request.getParameter("finYear"),request.getParameter("cumltv"));
	
	
	%>
				 
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<div class="total-report">
<div class="printable" id="tblExport">
<table width="90%" cellpadding="0" cellspacing="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
<tbody><tr>
						<td colspan="6" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hoaid) %></td>
					</tr>
					<tr><td colspan="6" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="6" align="center" style="font-weight: bold;">Balance Sheet Report</td></tr>
					<tr><td colspan="6" align="center" style="font-weight: bold;">Report From Date  <%=onlyfromdate %> TO <%=onlytodate %> </td></tr>
</tbody></table>
<div style="clear:both"></div>
<table width="90%"  cellspacing="0" cellpadding="0" style="margin:0 auto;" class="new-table" border="1">
  <tr>
    <td width="30%" bgcolor="#CC6600">Liabilites</td>
    <td width="15%" bgcolor="#CC6600">Details-Credits</td>
  <!--   <td width="11%" bgcolor="#CC6600">Amount-Credits</td> -->
    <td width="27%" bgcolor="#CC6600">Assets</td>
    <td width="15%" bgcolor="#CC6600">Details-Debits</td>
    <!-- <td width="13%" bgcolor="#CC6600">Amount-Credits</td> -->
  </tr> 
  <tr>
  		<td colspan="2">
        	<table width="100%"  cellspacing="0" cellpadding="0" border="1">
        		<% 
        		if(expendmajhdlist.size()>0){ 
						for(int i=0;i<expendmajhdlist.size();i++){
							MH=(majorhead)expendmajhdlist.get(i);
							double amount = 0.0;
							if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative"))
							{
								amount = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "", "");
							}
							double majExpendAmt = Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
							if(!String.valueOf(majExpendAmt).equals("00") || amount > 0){
				 %>
        			<tr>
                    	<%-- <td width="51%" bgcolor="#CCFF00"><a href="adminPannel.jsp?page=minorheadLedgerReport&majrhdid=<%=MH.getmajor_head_id()%>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Balance Sheet"><%=MH.getname() %></a></td> --%>
                    	<td width="47%" bgcolor="#CCFF00"><%=MH.getname()%></td>
                        <!-- <td width="24%" align="right">0.00</td> -->
                        <td width="24%" align="right"><%=majExpendAmt+amount %></td>
                        <%debitliab=debitliab+majExpendAmt+amount;%>
                     </tr>
                    <%}}}if(VendorPaymentPedning.size()>0){ %>
                      <tr>
                    	 <td style="padding-left:15px;color: #F00;" width="47%">SUNDRY CREDITORS</td>
                         <td align="right" width="24%">0.00</td>
                         <!-- <td align="right">0.00</td> -->
                      </tr>
                    <%
						VendorPayments=VendorPaymentPedning.keySet().iterator();            
						while(VendorPayments.hasNext()){
						  key   = VendorPayments.next();
	   	 			   	  value = VendorPaymentPedning.get(key);
	   	 			  double amount = Double.parseDouble(value.toString());
	   	 			  
	   	 			  if(key.equals("1435")){
	   	 				  System.out.println("amountamountamountamount =====? "+amount);
	   	 			  }
						if(amount <= 5 && amount >= -5){
							amount = 0;
						}
	   	 					 if(amount != 0){ 
	   	 			%>
                    <tr>
                    	<td style="padding-left:25px;" width="47%"><%if(!key.toString().equals("")){%>
						<%=VNDR.getMvendorsAgenciesName(key.toString()) %>
						<%} else{%>
							Bill with out Vendor
							<%}
							%>
						</td>
						<%
						
						
						%>
                        <td><a href="adminPannel.jsp?page=balanceSheet_VendorWise&vendor=<%=(key.toString())%>&date=<%=onlyfromdate%>to<%=onlytodate%>&type=<%=request.getParameter("cumltv")%>&hoa=<%=request.getParameter("head_account_id")%><%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")){%>&fy=<%=request.getParameter("finYear")%><%}%>" target="_blank"><%=df.format(amount)%></a></td>
                        <%debitliab=debitliab+Double.parseDouble(value.toString());%>
                     <!--    <td>0.00</td> -->
                    </tr>
                     <%}
	   	 					 }
						} %> 
                     
                     
                     
                     <tr>
                    <td style="padding-left:15px;color: #F00;" width="47%">LOANS AND ADVANCES</td>
                    <td width="22%" align="right">0.00</td>
                    <!-- <td width="27%" align="right">0.00</td> -->
                    </tr>
					   <% banktransactionsListing BAKL = new banktransactionsListing();
                     if(hoaid.equals("3")){
                   	 List subhdlist=SUBH_L.getSubheadListMinorhead("132");
                   	 if(subhdlist.size()>0){ 
                    	for(int i=0;i<subhdlist.size();i++)
                    	{
                    		SUBH = (subhead)subhdlist.get(i);
                    		double amount = 0.0;
                        	double SSprovisionAmt = 0.0;
                        	double SSOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","132","extra1",fromdate,todate,"","","","",hoaid));
                        	double SSOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","132","minorhead_id",fromdate,todate,cashtype3));
                        	double SSOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","132","minorhead_id",fromdate,todate,hoaid));
                        	double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "14", "132", SUBH.getsub_head_id(), "3", fromdate, todate));
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){	
                        		amount = BBL.getMajorOrMinorOrSubheadOpeningBal("3", "14", "132", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                        		double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","132","minorhead_id",fromdate,todate,hoaid));
                        		SSprovisionAmt = amount - amountt11;
                        	}
					
                        	
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                    if(!df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount)%></td>
                    <%debitliab=debitliab+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount; %>
                    </tr>
                    <%}}else{
                    if(!df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount)%></td>
                    <%debitliab=debitliab+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount; %>
                    </tr>
                    <%}} %>
                    
					<%}}} %>
					
					
					
					
					   <% 
                     if(hoaid.equals("1")){
                   	 List subhdlist=SUBH_L.getSubheadListMinorhead("122");
                   	 if(subhdlist.size()>0){ 
                    	for(int i=0;i<subhdlist.size();i++)
                    	{
                    		SUBH = (subhead)subhdlist.get(i);
                    		double amount = 0.0;
                        	double SSprovisionAmt = 0.0;
                        	double SSOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","122","extra1",fromdate,todate,"","","","",hoaid));
                        	double SSOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","122","minorhead_id",fromdate,todate,cashtype3));
                        	double SSOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","122","minorhead_id",fromdate,todate,hoaid));
                        	double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "13", "122", SUBH.getsub_head_id(), "1", fromdate, todate));
                        	
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){	
                        		amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "13", "122", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                        		double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","122","minorhead_id",fromdate,todate,hoaid));
                        		SSprovisionAmt = amount - amountt11;
                        	}
					
                        	
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                    if(!df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount)%></td>
                    <%debitliab=debitliab+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount; %>
                    </tr>
                    <%}}else{
                    if(!df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount)%></td>
                    <%debitliab=debitliab+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount; %>
                    </tr>
                    <%}} %>
                    
					<%}}}//added by sagar %>
					
					
					   <% 
                     if(hoaid.equals("4")){
                   	 List subhdlist=SUBH_L.getSubheadListMinorhead("102");
                   	 if(subhdlist.size()>0){ 
                    	for(int i=0;i<subhdlist.size();i++)
                    	{
                    		SUBH = (subhead)subhdlist.get(i);
                    		double amount = 0.0;
                        	double SSprovisionAmt = 0.0;
                        	double SSOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","102","extra1",fromdate,todate,"","","","",hoaid));
                        	double SSOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","102","minorhead_id",fromdate,todate,cashtype3));
                        	double SSOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","102","minorhead_id",fromdate,todate,hoaid));
                        	double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "11", "102", SUBH.getsub_head_id(), "4", fromdate, todate));
                        	
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){	
                        		amount = BBL.getMajorOrMinorOrSubheadOpeningBal("4", "11", "102", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                        		double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","102","minorhead_id",fromdate,todate,hoaid));
                        		SSprovisionAmt = amount - amountt11;
                        	}
					
//                         System.out.println("SSOutstandCreditLiabAmt............."+SSOutstandCreditLiabAmt);
//                         System.out.println("SSOutstandDebitLiabAmt............."+SSOutstandDebitLiabAmt);
//                         System.out.println("SSOutstandDebitLiabAmt1............."+SSOutstandDebitLiabAmt1);
                        	
                        	
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                    if(!df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount)%></td>
                    <%debitliab=debitliab+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount; %>
                    </tr>
                    <%}}else{
                    if(!df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount)%></td>
                    <%debitliab=debitliab+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount; %>
                    </tr>
                    <%}} %>
                    
					<%}}}//added by sagar %>
					
					
					 <% 
                     if(hoaid.equals("4")){
                   	 List subhdlist=SUBH_L.getSubheadListMinorhead("112");
                   	 if(subhdlist.size()>0){ 
                    	for(int i=0;i<subhdlist.size();i++)
                    	{
                    		SUBH = (subhead)subhdlist.get(i);
                    		double amount = 0.0;
                        	double SSprovisionAmt = 0.0;
                        	double SSOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","112","extra1",fromdate,todate,"","","","",hoaid));
                        	double SSOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","112","minorhead_id",fromdate,todate,cashtype3));
                        	double SSOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","112","minorhead_id",fromdate,todate,hoaid));
                        	double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "12", "112", SUBH.getsub_head_id(), "4", fromdate, todate));
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){	
                        		amount = BBL.getMajorOrMinorOrSubheadOpeningBal("4", "12", "112", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                        		double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","112","minorhead_id",fromdate,todate,hoaid));
                        		SSprovisionAmt = amount - amountt11;
                        	}
					
                        	
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                    if(!df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount)%></td>
                    <%debitliab=debitliab+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount; %>
                    </tr>
                    <%}}else{
                    if(!df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount)%></td>
                    <%debitliab=debitliab+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount; %>
                    </tr>
                    <%}} %>
                    
					<%}}}//added by sagar %>
					
					
					 <% 
                     if(hoaid.equals("5")){
                   	 List subhdlist=SUBH_L.getSubheadListMinorhead("142");
                   	 if(subhdlist.size()>0){ 
                    	for(int i=0;i<subhdlist.size();i++)
                    	{
                    		SUBH = (subhead)subhdlist.get(i);
                    		double amount = 0.0;
                        	double SSprovisionAmt = 0.0;
                        	double SSOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","142","extra1",fromdate,todate,"","","","",hoaid));
                        	double SSOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","142","minorhead_id",fromdate,todate,cashtype3));
                        	double SSOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","142","minorhead_id",fromdate,todate,hoaid));
                        	double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "15", "142", SUBH.getsub_head_id(), "5", fromdate, todate));
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){	
                        		amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "15", "142", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                        		double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","142","minorhead_id",fromdate,todate,hoaid));
                        		SSprovisionAmt = amount - amountt11;
                        	}
					
                        	
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                    if(!df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount)%></td>
                    <%debitliab=debitliab+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount; %>
                    </tr>
                    <%}}else{
                    if(!df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount)%></td>
                    <%debitliab=debitliab+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount; %>
                    </tr>
                    <%}} %>
                    
					<%}}}//added by sagar %>
					
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                    <%
                    	String corpusFundMajorHead = "";
                    	String corpusFundMinorHead = "";
                    	String corpusFundSubHead = "";
                    	
                    	if(hoaid.equals("4"))
                    	{
                    		corpusFundMajorHead = "11";
                        	corpusFundMinorHead = "473";
                        	corpusFundSubHead = "21487";
                    	}
                    	
                    	else if(hoaid.equals("1"))
                    	{
                    		corpusFundMajorHead = "13";
                        	corpusFundMinorHead = "476";
                        	corpusFundSubHead = "21537";
                    	}
                    	
                    	else if(hoaid.equals("5"))
                    	{
                    		corpusFundMajorHead = "15";
                        	corpusFundMinorHead = "478";
                        	corpusFundSubHead = "21594";
                    	}
                    	else if(hoaid.equals("3"))
                    	{
                    		corpusFundMajorHead = "14";
                        	corpusFundMinorHead = "21532";
                        	corpusFundSubHead = "21534";
                    	}
                    
                    %>
                    
                    <%if(hoaid.equals("4") || hoaid.equals("1") || hoaid.equals("5") || hoaid.equals("3")){ %>
                    <tr>
                    <td style="padding-left:15px;color: #F00;" width="47%">CORPUS FUND</td>
                    <%
                    double income = 0.0; 
                    double expense = 0.0;
                    double incomeOverExpenditure = 0.0;
                    
                    List incomemajhdlist2=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid, "revenue",headgroup);
    				List expendmajhdlist2=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew(hoaid,"expenses",headgroup);
                    if(incomemajhdlist2.size()>0)
                    { 
						for(int i=0;i<incomemajhdlist2.size();i++)
						{
							MH=(majorhead)incomemajhdlist2.get(i);
							double headsAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,hoaid));
							double JVAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fromdate,todate,cashtype3));
							double dollarAmt = Double.parseDouble(CP_L.getLedgerSumDollarAmt(MH.getmajor_head_id(),"major_head_id",fromdate,todate,cashtype1,hoaid));
							income +=  headsAmt+ JVAmt + dollarAmt;
						}
					}
                    if(expendmajhdlist2.size()>0)
                    { 
						for(int i=0;i<expendmajhdlist2.size();i++)
						{
							MH=(majorhead)expendmajhdlist2.get(i);
							double headsAmt = Double.parseDouble(PEXP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,hoaid));
							double JVAmt = Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fromdate,todate,cashtype3)); 
							expense += headsAmt + JVAmt;
						}
                    }	
                    
                    incomeOverExpenditure = income - expense; 
                    %>
                    
                    <%
                   	double amount = 0.0;
                    if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){	
                    	/* amount = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, corpusFundMajorHead, corpusFundMinorHead, corpusFundSubHead, "subhead","Assets", finYr, "", ""); */
                    	amount = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, corpusFundMajorHead, corpusFundMinorHead, corpusFundSubHead, "subhead","Liabilites", finYr, "", "");
                    }
                    double corpusCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(corpusFundSubHead,"sub_head_id",corpusFundMinorHead,"extra1",fromdate,todate,"","","","",hoaid));
                	double corpusDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(corpusFundSubHead,"sub_head_id",corpusFundMinorHead,"minorhead_id",fromdate,todate,cashtype3));
                	
                    %>
                    <td width="22%" align="right"><%=df.format(corpusCreditLiabAmt+corpusDebitLiabAmt+amount+incomeOverExpenditure)%></td>
                    <%//System.out.println("debit.."+df.format(corpusCreditLiabAmt+corpusDebitLiabAmt+amount+incomeOverExpenditure)); %>
                    <%-- <td width="27%" align="right"><%=df.format(corpusDebitLiabAmt+amount+incomeOverExpenditure)%></td> --%>
                    <%debitliab = debitliab+corpusDebitLiabAmt+amount+incomeOverExpenditure; %>
                    </tr> 
                    <%} %>
                    
                    <%-- <%if(hoaid.equals("4")){ %>
                    <tr>
                    <td style="padding-left:15px;color: #F00;" width="47%">CORPUS FUND</td>
                    <%double amount = BBL.getMajorOrMinorOrSubheadOpeningBal("4", "11", "473", "21487", "subhead","Assets", finYr, "", ""); %>
                    <td width="22%" align="right"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22("21487","sub_head_id","473","extra1",finStartYr+"-04-01 00:00:00",todate,"","","","",hoaid)))%></td>
                    <td width="27%" align="right"><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("21487","sub_head_id","473","minorhead_id",finStartYr+"-04-01 00:00:00",todate,cashtype3))+amount)%></td>
                    <%debitliab =debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV("21487","sub_head_id","473","minorhead_id",finStartYr+"-04-01 00:00:00",todate,cashtype3))+amount; %>
                    </tr> 
                    <%} %> --%>
                  <!--   <tr>
                    <td style="padding-left:15px;color: #F00;" width="47%">OUTSTANDING LIABILITIES</td>
                    <td width="22%" align="right">0.00</td>
                    <td width="27%" align="right">0.00</td>
                    </tr>   -->
                     <% 
                     if(hoaid.equals("4")){
                   	 List subhdlist=SUBH_L.getSubheadListMinorhead("468");
                   	 if(subhdlist.size()>0){ 
                    	for(int i=0;i<subhdlist.size();i++)
                    	{
                    		SUBH = (subhead)subhdlist.get(i);
                    		double amount = 0.0;
                        	double SSprovisionAmt = 0.0;
                        	double SSOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","468","extra1",fromdate,todate,"","","","",hoaid));
                        	double SSOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","468","minorhead_id",fromdate,todate,cashtype3));
                        	double SSOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","468","minorhead_id",fromdate,todate,hoaid));
                        	double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "11", "468", SUBH.getsub_head_id(), "4", fromdate, todate));
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){	
                        		amount = BBL.getMajorOrMinorOrSubheadOpeningBal("4", "11", "468", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
                        		double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","468","minorhead_id",fromdate,todate,hoaid));
                        		SSprovisionAmt = amount - amountt11;
                        	}
					
                        	
                        	if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                    if(!df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSprovisionAmt+SSOutstandDebitLiabAmt+bktAmount)%></td>
                    <%debitliab=debitliab+SSprovisionAmt+SSOutstandDebitLiabAmt; %>
                    </tr>
                    <%}}else{
                    if(!df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SSOutstandCreditLiabAmt+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1+bktAmount)%></td>
                    <%debitliab=debitliab+SSOutstandDebitLiabAmt+SSOutstandDebitLiabAmt1; %>
                    </tr>
                    <%}} %>
                    
					<%}}} %>
					
					
					
					
					
					
					
					
					
					
					
					<% 
                     if(hoaid.equals("1")){
                     List subhdlist=SUBH_L.getSubheadListMinorhead("469");
                     if(subhdlist.size()>0){ 
                    	for(int i=0;i<subhdlist.size();i++)
                    	{
                    	SUBH = (subhead)subhdlist.get(i);
                    	double amount = 0.0;
                        double CHprovisionAmt = 0.0;
                        double CHOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","469","extra1",fromdate,todate,"","","","",hoaid));
                        double CHOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","469","minorhead_id",fromdate,todate,cashtype3));
                        double CHOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","469","minorhead_id",fromdate,todate,hoaid));
                        double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "13", "469", SUBH.getsub_head_id(), "1", fromdate, todate));
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        	/* amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "13", "469", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", ""); */
                        	amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "13", "469", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                        	double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","469","minorhead_id",fromdate,todate,hoaid));
                        	CHprovisionAmt = amount - amountt11;
                        }
					
                    if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                    if(!df.format(CHOutstandCreditLiabAmt+CHprovisionAmt+CHOutstandDebitLiabAmt+bktAmount).equals("0")){
                    %>
                     <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(CHOutstandCreditLiabAmt+CHprovisionAmt+CHOutstandDebitLiabAmt+bktAmount)%></td>
                    <%debitliab = debitliab +CHprovisionAmt+CHOutstandDebitLiabAmt;%>
                     </tr>
                    <%}}else{
                    if(!df.format(CHOutstandCreditLiabAmt+CHOutstandDebitLiabAmt+CHOutstandDebitLiabAmt1+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(CHOutstandCreditLiabAmt+CHOutstandDebitLiabAmt+CHOutstandDebitLiabAmt1+bktAmount)%></td>
                    <%debitliab = debitliab +CHOutstandDebitLiabAmt+CHOutstandDebitLiabAmt1;%>
                     </tr>
                    <%}} %>
                    
                   
					<%}}
                    List subhdlist1=SUBH_L.getSubheadListMinorhead("470");
                    if(subhdlist1.size()>0){ 
                    	for(int i=0;i<subhdlist1.size();i++)
                    	{
                    
                    	SUBH = (subhead)subhdlist1.get(i);
                    	 double amount = 0.0;
                         double CHprovisionAmt = 0.0;
                         double CHOutstandCreditLiabAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","470","extra1",fromdate,todate,"","","","",hoaid));
                         double CHOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","470","minorhead_id",fromdate,todate,cashtype3));
                         double CHOutstandDebitLiabAmt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","470","minorhead_id",fromdate,todate,hoaid));
                         double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "61", "470", SUBH.getsub_head_id(), "1", fromdate, todate));
                         if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        		amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "61", "470", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
                        		double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","470","minorhead_id",fromdate,todate,hoaid));
                         	CHprovisionAmt = amount - amountt11;
                         }
					
                         if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                         if(!df.format(CHOutstandCreditLiabAmt1+CHprovisionAmt+CHOutstandDebitLiabAmt1+bktAmount).equals("0")){
                         %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    
                    <td><%=df.format(CHOutstandCreditLiabAmt1+CHprovisionAmt+CHOutstandDebitLiabAmt1+bktAmount)%></td>
                    <%debitliab=debitliab+CHprovisionAmt+CHOutstandDebitLiabAmt1; %>
                     </tr>
                    <%}}else{
                    if(!df.format(CHOutstandCreditLiabAmt1+CHOutstandDebitLiabAmt1+CHOutstandDebitLiabAmt11+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    
                    <td><%=df.format(CHOutstandCreditLiabAmt1+CHOutstandDebitLiabAmt1+CHOutstandDebitLiabAmt11+bktAmount)%></td>
                    <%debitliab=debitliab+CHOutstandDebitLiabAmt1+CHOutstandDebitLiabAmt11; %>
                    <%}} %>
                    </tr>
					<%}}
                    List subhdlist2=SUBH_L.getSubheadListMinorhead("471");
                    if(subhdlist2.size()>0){ 
                    	for(int i=0;i<subhdlist2.size();i++)
                    	{
                    	SUBH = (subhead)subhdlist2.get(i);
                    	double amount = 0.0;
                        double CHprovisionAmt = 0.0;
                        double CHOutstandCreditLiabAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","471","extra1",fromdate,todate,"","","","",hoaid));
                        double CHOutstandDebitLiabAmt2 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","471","minorhead_id",fromdate,todate,cashtype3));
                        double CHOutstandDebitLiabAmt21 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","471","minorhead_id",fromdate,todate,hoaid));
                        double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "60", "471", SUBH.getsub_head_id(), "1", fromdate, todate));
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        	 amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "60", "471", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
                        	 double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","471","minorhead_id",fromdate,todate,hoaid));
                         	 CHprovisionAmt = amount - amountt11;
                        }
					%>
					
                   
                    <%if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                 if(!df.format(CHOutstandCreditLiabAmt2+CHOutstandDebitLiabAmt2+CHOutstandDebitLiabAmt21+bktAmount).equals("")){   
            %>
            <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(CHOutstandCreditLiabAmt2+CHprovisionAmt+CHOutstandDebitLiabAmt2+bktAmount)%></td>
                    <%debitliab=debitliab+CHprovisionAmt+CHOutstandDebitLiabAmt2; %>
                    </tr>
                    <%}}else{
                    if(!df.format(CHOutstandCreditLiabAmt2+CHOutstandDebitLiabAmt2+CHOutstandDebitLiabAmt21+bktAmount).equals("")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(CHOutstandCreditLiabAmt2+CHOutstandDebitLiabAmt2+CHOutstandDebitLiabAmt21+bktAmount)%></td>
                    <%debitliab=debitliab+CHOutstandDebitLiabAmt2+CHOutstandDebitLiabAmt21; %>
                    </tr>
                    <%}} %>
                    
					<%}}
                     } %>
					<% 
                     if(hoaid.equals("5")){
                    	// System.out.println("111111111111111111111111111111111111");
                 
                     List subhdlist=SUBH_L.getSubheadListMinorhead("472");
                     if(subhdlist.size()>0){ 
                    	for(int i=0;i<subhdlist.size();i++)
                    	{
                    	SUBH = (subhead)subhdlist.get(i);
                    	 double amount = 0.0;
                         double SNprovisionAmt = 0.0;
                         double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","472","extra1",fromdate,todate,"","","","",hoaid));
                         double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","472","minorhead_id",fromdate,todate,cashtype3));
                         double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","472","minorhead_id",fromdate,todate,hoaid));
                         double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "15", "472", SUBH.getsub_head_id(), "5", fromdate, todate));
                         if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                             amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "15", "472", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
                             double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","472","minorhead_id",fromdate,todate,hoaid));
                             SNprovisionAmt = amount - amountt11;
                         }
                         
					if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
					if(!df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt).equals("0")){
					%>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount)%></td>
                     <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %>
                       </tr>
                    <%}}else{
                    if(!df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount).equals("0")){
                    %>
                    <tr>
                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                    <td><%=df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount)%></td>
                     <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1; %>
                    <%}} %>
                  
                    </tr>
					<%}}
                     
                    
                     
                     }%>
					<tr>
                    <td style="padding-left:15px;color: #F00;" width="47%">LOANS</td>
                    <td width="22%" align="right">0.00</td>
                    <!-- <td width="27%" align="right">0.00</td> -->
                    </tr>
					
				<%	if(hoaid.equals("5")){
                     	
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("634");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
                     	double amount = 0.0;
                        double SNprovisionAmt = 0.0;
                        double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","634","extra1",fromdate,todate,"","","","",hoaid));
                        double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","634","minorhead_id",fromdate,todate,cashtype3));
                        double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","634","minorhead_id",fromdate,todate,hoaid));
                        double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "84", "634", SUBH.getsub_head_id(), "5", fromdate, todate));
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                            amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "84", "634", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
                            double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","634","minorhead_id",fromdate,todate,hoaid));
                           if(amount!=0.0){
                            SNprovisionAmt = amount - amountt11;
                           } 
                            //SNprovisionAmt = amount - amountt11;
                        }
 					
//                         System.out.println("SNOutstandCreditLiabAmt....."+SNOutstandCreditLiabAmt);
//                         System.out.println("SNOutstandDebitLiabAmt....."+SNOutstandDebitLiabAmt);
//                         System.out.println("SNOutstandDebitLiabAmt1....."+SNOutstandDebitLiabAmt1);
//                         System.out.println("SNprovisionAmt....."+SNprovisionAmt);
                        
                        
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        if(!df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount).equals("0")){
                        %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %>
                      <tr>
                     <%}}else{
                     if(!df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount).equals("0")){
                     %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1; %>
                      <tr>
                     <%}} %>
                   
                     </tr>
 					<%}}
                     }%>
                     <tr>
                    <td style="padding-left:15px;color: #F00;" width="47%">LIABILITY</td>
                    <td width="22%" align="right">0.00</td>
                    <!-- <td width="27%" align="right">0.00</td> -->
                    </tr>
                     <%	if(hoaid.equals("5")){
                     	
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("680");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
                     	double amount = 0.0;
                        double SNprovisionAmt = 0.0;
                        double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","680","extra1",fromdate,todate,"","","","",hoaid));
                        double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","680","minorhead_id",fromdate,todate,cashtype3));
                        double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","680","minorhead_id",fromdate,todate,hoaid));
                        double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "15", "680", SUBH.getsub_head_id(), "5", fromdate, todate));
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                            amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "15", "680", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                            double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","680","minorhead_id",fromdate,todate,hoaid));
                           
                            SNprovisionAmt = amount - amountt11;
                          
                        }
 					
                      
                        
                        
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        if(!df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount).equals("0")){
                        %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %>
                      <tr>
                     <%}}else{
                     if(!df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount).equals("0")){
                     %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1; %>
                      <tr>
                     <%}} %>
                   
                     </tr>
 					<%}}
                     }%>
                     
                     
                        <%	if(hoaid.equals("3")){
                     	
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("682");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
                     	double amount = 0.0;
                        double SNprovisionAmt = 0.0;
                        double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","682","extra1",fromdate,todate,"","","","",hoaid));
                        double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","682","minorhead_id",fromdate,todate,cashtype3));
                        double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","682","minorhead_id",fromdate,todate,hoaid));
                        double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "14", "682", SUBH.getsub_head_id(), "3", fromdate, todate));
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                            amount = BBL.getMajorOrMinorOrSubheadOpeningBal("3", "14", "682", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                            double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","682","minorhead_id",fromdate,todate,hoaid));
                           
                            SNprovisionAmt = amount - amountt11;
                          
                        }
 					
                      
                        
                        
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        if(!df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount).equals("0")){
                        %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount; %>
                      <tr>
                     <%}}else{
                     if(!df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount).equals("0")){
                     %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount; %>
                      <tr>
                     <%}} %>
                   
                     </tr>
 					<%}}
                     }//added by sagar%>
                     
                     
                     
                     
                     
                     <%	if(hoaid.equals("1")){
                     	
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("679");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
                     	double amount = 0.0;
                        double SNprovisionAmt = 0.0;
                        double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","679","extra1",fromdate,todate,"","","","",hoaid));
                        double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","679","minorhead_id",fromdate,todate,cashtype3));
                        double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","679","minorhead_id",fromdate,todate,hoaid));
                        double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "13", "679", SUBH.getsub_head_id(), "1", fromdate, todate));
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                            amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "13", "679", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                            double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","679","minorhead_id",fromdate,todate,hoaid));
                           
                            SNprovisionAmt = amount - amountt11;
                          
                        }
 					
                      
                        
                        
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        if(!df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount).equals("0")){
                        %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %>
                      <tr>
                     <%}}else{
                     if(!df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount).equals("0")){
                     %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1; %>
                      <tr>
                     <%}} %>
                   
                     </tr>
 					<%}}
                     }%>
                     
                     
                     <%	if(hoaid.equals("4")){
                     	
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("681");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
                     	double amount = 0.0;
                        double SNprovisionAmt = 0.0;
                        double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","681","extra1",fromdate,todate,"","","","",hoaid));
                        double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","681","minorhead_id",fromdate,todate,cashtype3));
                        double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","681","minorhead_id",fromdate,todate,hoaid));
                        double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "11", "681", SUBH.getsub_head_id(), "1", fromdate, todate));
                        
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                            amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "11", "681", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "", "");
                            double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","681","minorhead_id",fromdate,todate,hoaid));
                           
                            SNprovisionAmt = amount - amountt11;
                          
                        }
 					
                      
                        
                        
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        if(!df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount).equals("0")){
                        %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %>
                      <tr>
                     <%}}else{
                     if(!df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount).equals("0")){
                     %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1; %>
                      <tr>
                     <%}} %>
                   
                     </tr>
 					<%}}
                     }%>
                     
                     
                     
                     <%-- 
                      <%	if(hoaid.equals("1")){
                     	
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("679");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
                     	double amount = 0.0;
                        double SNprovisionAmt = 0.0;
                        double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","679","extra1",fromdate,todate,"","","","",hoaid));
                        double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","679","minorhead_id",fromdate,todate,cashtype3));
                        double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","679","minorhead_id",fromdate,todate,hoaid));
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                            amount = BBL.getMajorOrMinorOrSubheadOpeningBal("1", "13", "679", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
                            double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","679","minorhead_id",fromdate,todate,hoaid));
                           
                            SNprovisionAmt = amount - amountt11;
                          
                        }
 					
                      
                        
                        
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                        if(!df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt).equals("0")){
                        %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %>
                      <tr>
                     <%}}else{
                     if(!df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1).equals("0")){
                     %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1; %>
                      <tr>
                     <%}} %>
                   
                     </tr>
 					<%}}
                     }%> --%>
                     
                     
                     
                     
                     
                <!--     <td style="padding-left:15px;color: #F00;" width="47%">OTHER INCOME</td>
                    <td width="22%" align="right">0.00</td> -->
                    <!-- <td width="27%" align="right">0.00</td> -->
                    </tr>
					
				<%	if(hoaid.equals("5")){
                     	
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("143");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
                     	double amount = 0.0;
                        double SNprovisionAmt = 0.0;
                        double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","143","extra1",fromdate,todate,"","","","",hoaid));
                        double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","143","minorhead_id",fromdate,todate,cashtype3));
                        double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","143","minorhead_id",fromdate,todate,hoaid));
                        double bktAmount =Double.parseDouble( BAKL.getStringOtherDepositByheads("other-amount", "15", "143", SUBH.getsub_head_id(), "5", fromdate, todate));
                        if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                            amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "15", "143", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
                            double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","143","minorhead_id",fromdate,todate,hoaid));
                            SNprovisionAmt = amount - amountt11;
                        }
                        
 					%>
 				
                     <%if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                    if(!df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount).equals("0")){	 
                     
                     %>
                     	<tr>
                     	<%if(!SUBH.getsub_head_id().equals("20014") && !SUBH.getsub_head_id().equals("20034")){ %><!-- condition added ny madhav  -->
                     	
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNprovisionAmt+SNOutstandDebitLiabAmt+bktAmount)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %> <%} %><!--added ny madhav  -->
                      </tr>
                     <%}}else{
                     if(!df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount).equals("0")){
                     %>
                     	<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     <td><%=df.format(SNOutstandCreditLiabAmt+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1+bktAmount)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1; %>
                      </tr>
                     <%}} 
                     }}
                     }%>
                     
                     
                   <%--   <tr> 
                       <td style="padding-left:15px;color: #F00;" width="47%">SHRI SAINIVAS</td>
                    <td width="22%" align="right">0.00</td>
                    </tr>
                     
                     
                   <%if(hoaid.equals("5")){
                     	// System.out.println("111111111111111111111111111111111111");
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("142");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
 					%>
 					 
 								<tr>
                     
                     <%//System.out.println("SUBH.getname()........"+SUBH.getname());
 	                double amount = 0.0;
                     double SNprovisionAmt = 0.0;
                   
                     double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","142","minorhead_id",fromdate,todate,cashtype3));
                    
                     if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                         amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "15", "142", SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
                       //  System.out.println("1111111.......amount......."+amount);
                         
                         double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","142","minorhead_id",fromdate,todate,hoaid));
                         SNprovisionAmt = amount - amountt11;
                     }
                     
                     %>
                      <td><%=df.format(SNOutstandCreditLiabAmt)%></td>
                      <%//System.out.println("h55.."); %>
                     <%if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                     if(!df.format(SNprovisionAmt+SNOutstandDebitLiabAmt).equals("0")){
                     %>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     <td><%=df.format(SNprovisionAmt+SNOutstandDebitLiabAmt)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %>
                     <%}}else{
                     if(!df.format(SNOutstandDebitLiabAmt).equals("0")){
                     %>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     <td><%=df.format(SNOutstandDebitLiabAmt)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt; %>
                     <%}} %>
                   
                     </tr>
 					<%}}
                     }%> --%>
					
				<%	if(hoaid.equals("5")){
                     	// System.out.println("111111111111111111111111111111111111");
                  
                      List subhdlist1=SUBH_L.getSubheadListMinorhead("671");
                      if(subhdlist1.size()>0){ 
                     	for(int i=0;i<subhdlist1.size();i++)
                     	{
                     	SUBH = (subhead)subhdlist1.get(i);
 					%>
 					<tr>
                    <td style="padding-left:15px;color: #F00;" width="47%">ADVANCE ROOM BOOKING</td>
                    <td width="22%" align="right">0.00</td>
                    <!-- <td width="27%" align="right">0.00</td> -->
                    </tr>
 								<tr>
                     <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
                     <%//System.out.println("SUBH.getname()........"+SUBH.getname());
 	                double amount = 0.0;
                     double SNprovisionAmt = 0.0;
                    // double SNOutstandCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id","671","extra1",fromdate,todate,"","","","",hoaid));
                    
                     double SNOutstandDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id","671","minorhead_id",fromdate,todate,cashtype3));
                     double SNOutstandDebitLiabAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","671","minorhead_id",fromdate,todate,hoaid));
                     
                     if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
                         amount = BBL.getMajorOrMinorOrSubheadOpeningBal("5", "90", "671", SUBH.getsub_head_id(), "subhead","Liabilites", finYr, "credit", "");
                         double amountt11 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id","671","minorhead_id",fromdate,todate,hoaid));
                         SNprovisionAmt = amount - amountt11;
                     }
                     
                     %>
                      <%-- <td><%=df.format(SNOutstandCreditLiabAmt)%></td> --%>
                      <%//System.out.println("h55.."); %>
                     <%if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){%>
                     <td><%=df.format(SNprovisionAmt+SNOutstandDebitLiabAmt)%></td>
                      <%debitliab=debitliab+SNprovisionAmt+SNOutstandDebitLiabAmt; %>
                     <%}else{%>
                     <td><%=df.format(SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1)%></td>
                      <%debitliab=debitliab+SNOutstandDebitLiabAmt+SNOutstandDebitLiabAmt1; %>
                     <%} %>
                   
                     </tr>
 					<%}}
                     }%>
					
					<!-- <tr>
                    <td style="padding-left:15px;color: #F00;" width="47%">EMD</td>
                    <td width="22%" align="right">0.00</td>
                    <td width="27%" align="right">0.00</td>
                    </tr>  --> 
					<% 
					String emdMajorHead = "";
					String emdMinorHead = "";
					String emdSubhead = "";
					
					if(hoaid.equals("1"))
					{
						/* emdMajorHead = "51";
						emdMinorHead = "404"; */
						emdMajorHead = "13";
						emdMinorHead = "123";
						emdSubhead = "20013";
					}
					else if(hoaid.equals("3"))
					{
						/* emdMajorHead = "71";
						emdMinorHead = "505"; */
						
						emdMajorHead = "14";
						emdMinorHead = "133";
						emdSubhead = "20013";
					} 
					else if(hoaid.equals("4"))
					{
						/* emdMajorHead = "31";
						emdMinorHead = "207"; */
						emdMajorHead = "11";
						emdMinorHead = "103";
						emdSubhead = "20013";
					}
					/* else if(hoaid.equals("5"))
					{
						emdMajorHead = "84";
						emdMinorHead = "639";
					} */
					 if(hoaid.equals("1") || hoaid.equals("4") || hoaid.equals("3"))
					 {
						 /* List subhdlist=SUBH_L.getSubheadListMinorhead(emdMinorHead); */
						 List subhdlist=SUBH_L.getsubheadBasedOnHeads(emdSubhead,emdMinorHead,emdMajorHead,hoaid);
		                    if(subhdlist.size()>0){ 
		                    	for(int i=0;i<subhdlist.size();i++)
		                    	{
		                    	SUBH = (subhead)subhdlist.get(i);%>
							<tr>
		                    <td style="padding-left:15px;color: #F00;" width="47%"><%=SUBH.getname()%></td>
		                    <%	
		                    	double amount = 0.0;
		                    if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
		                        amount = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid,emdMajorHead,emdMinorHead,SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
		                    }
		                    double EMDCreditLiabAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",emdMinorHead,"extra1",fromdate,todate,"","","","",hoaid));
		                    double EMDDebitLiabAmt = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",emdMinorHead,"minorhead_id",fromdate,todate,cashtype3));
		           			 %> 
		                     <td width="22%" align="right"><%=df.format(EMDCreditLiabAmt+amount)%></td>
		                    <%-- <td width="27%" align="right"><%=df.format(EMDDebitLiabAmt+amount)%></td> --%>
		                   <%debitliab=debitliab+EMDDebitLiabAmt+amount; %>
		                    </tr>
							<%}}}%> 
					
					 <%-- <%if(hoaid.equals("1") || hoaid.equals("3") || hoaid.equals("4") || hoaid.equals("5"))
					 {
						 List subhdlist=SUBH_L.getSubheadListMinorhead(emdMinorHead);
		                    if(subhdlist.size()>0){ 
		                    	for(int i=0;i<subhdlist.size();i++){
		                    
		                    	SUBH = (subhead)subhdlist.get(i);
										%>
										<tr>
		                    <td align="left" style="color:#000;"><%=SUBH.getname()%></td>
		                    <%
		                    	double amount = 0.0;
		                    if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
		                        amount = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid,emdMajorHead,emdMinorHead,SUBH.getsub_head_id(), "subhead","Assets", finYr, "", "");
		                    }
		                    %> 
		                     <td><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",emdMinorHead,"extra1",fromdate,todate,"","","","",hoaid)))%></td>
		                    <td><%=df.format(Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",emdMinorHead,"minorhead_id",fromdate,todate,cashtype3))+amount)%></td>
		                   <%debitliab=debitliab+Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",emdMinorHead,"minorhead_id",fromdate,todate,cashtype3))+amount; %>
		                    </tr>
							<%}}}%> --%>
					
					
               <!--  credit banks -->
                <%if(creditbanklist.size()>0){ 
						for(int i=0;i<creditbanklist.size();i++){
							BNK=(bankdetails)creditbanklist.get(i);
								bankopeningbal=BNKCAL.getCreditBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",todate,"");
								double finOpeningBalNew = BBL.getBankOpeningBal(BNK.getbank_id(),"bank",request.getParameter("finYear"));
								bankopeningbal += finOpeningBalNew;
			
  								if(bankopeningbal >0 || bankopeningbal<0){ %>
                    <tr>
                    	 <td><span style="padding-left:15px; font-weight:bold;"><%=BNK.getbank_name() %> bank</span></td>
    <td align="right" style="font-weight:bold;"><%=df.format(bankopeningbal) %></td>
   <!--  <td align="right" style="font-weight:bold;">0.0</td> -->
                    </tr>
        <% 
bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
bankopeningbal=0.0;
}} }
/* debitassest=debitassest+bankopeningbalcredit; */
%>        
            </table>
       </td>
       
       <!-- Assests -->
         <td colspan="2">
        	<table width="100%"  cellspacing="0" cellpadding="0" border="1">
        			<% if(incomemajhdlist.size()>0){ 
								for(int i=0;i<incomemajhdlist.size();i++){
									MH=(majorhead)incomemajhdlist.get(i);
								%>
	<tr>
	         	<%-- <td width="47%" bgcolor="#CCFF00"><a href="adminPannel.jsp?page=minorheadLedgerReport&majrhdid=<%=MH.getmajor_head_id()%>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Balance Sheet"><%=MH.getname() %></a></td> --%>
		<td width="47%" bgcolor="#CCFF00"><a href="#"><%=MH.getmajor_head_id() %> <%=MH.getname() %></a></td>
		   <% 
		   		double amountNew = 0.0;
		   		double amountNew2 = 0.0;
		   		double totalPettyCashAmountMajor =0.0;
		   		if(MH.getmajor_head_id().equals("54")){
		   			totalPettyCashAmountMajor=BNK_L.getPettyCashTotalAmount("21450","sub_head_id","425","minorhead_id",fromdate,todate,hoaid);
		   		}else if(MH.getmajor_head_id().equals("34")){
		   			totalPettyCashAmountMajor=BNK_L.getPettyCashTotalAmount("21452","sub_head_id","235","minorhead_id",fromdate,todate,hoaid);
		   		}else if(MH.getmajor_head_id().equals("84")){
		   			totalPettyCashAmountMajor=BNK_L.getPettyCashTotalAmount("21405","sub_head_id","633","minorhead_id",fromdate,todate,hoaid);
		   		}
		   if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
		    amountNew = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), "", "", "majorhead","Assets", finYr, "debit", "");
		    amountNew2 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(),"","", "majorhead","Assets", finYr, "credit", "");
		   }
		   double creditAssestAmt = Double.parseDouble(CP_L.getLedgerSum(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid));
		   double pettyVal = 0.0;
			if(MH.getmajor_head_id().equals("54") || MH.getmajor_head_id().equals("34") || MH.getmajor_head_id().equals("84")){
				String subHeadId = "";
				String headOfAccountId = "";
				String minorHeadId = "";
				
				if(MH.getmajor_head_id().equals("54")){
					subHeadId = "21450";
					minorHeadId = "425";
					headOfAccountId = "1";
				}else if(MH.getmajor_head_id().equals("34")){
					subHeadId = "21452";
					minorHeadId = "235";
					headOfAccountId = "4";
				}else if(MH.getmajor_head_id().equals("84")){
					subHeadId = "21405";
					minorHeadId = "633";
					headOfAccountId = "5";
				}
			
			   double debitAmountNew3 = 0.0;
			double creditAmountNew3 = 0.0;
			
			if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
				 debitAmountNew3 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), minorHeadId, subHeadId, "subhead","Assets", finYr, "debit", "");
				 creditAmountNew3 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), minorHeadId, subHeadId, "subhead","Assets", finYr, "credit", "");
				}
			double subDebitAssestAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(subHeadId,"sub_head_id",minorHeadId,"extra1",fromdate,todate,"","","","",headOfAccountId));
			double subDebitAssestAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(subHeadId,"sub_head_id",minorHeadId,"minorhead_id",fromdate,todate,hoaid));
			double subDebitAssestAmt2 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(subHeadId,"sub_head_id",minorHeadId,"minorhead_id",fromdate,todate,cashtype3));
			double totalPettyCashAmount =BNK_L.getPettyCashTotalAmount(subHeadId,"sub_head_id",minorHeadId,"minorhead_id",fromdate,todate,hoaid);
			
			pettyVal = debitAmountNew3 +subDebitAssestAmt + subDebitAssestAmt1 - subDebitAssestAmt2-totalPettyCashAmount;
			 
			}
		   if(!String.valueOf(creditAssestAmt).equals("00") || (amountNew > 0)){ %>
		   <td width="27%" align="right"><%=df.format(creditAssestAmt+amountNew-totalPettyCashAmountMajor-pettyVal) %></td>
		   <%debitassest=debitassest+creditAssestAmt+amountNew-totalPettyCashAmountMajor-pettyVal; }else{ 
		   double creditAssestAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead(MH.getmajor_head_id(),"major_head_id",fromdate,todate,"",hoaid));
		   double creditAssestAmt2 = Double.parseDouble(PEXP_L.getLedgerSum2(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fromdate,todate,hoaid));
		   double creditAssestAmt3 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fromdate,todate,cashtype3));%>
		   <td width="26%" align="right"><%=df.format(creditAssestAmt1 + creditAssestAmt2 - creditAssestAmt3 +amountNew)%></td>
		   <%debitassest=debitassest+creditAssestAmt1 + creditAssestAmt2 - creditAssestAmt3 +amountNew; } %>
		    <%-- <td width="26%" align="right"><%=df.format(amountNew2)%></td> --%>
			</tr>
                    <%List minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id()); 
                    String majrhdid=MH.getmajor_head_id();
                    if(minhdlist.size()>0){ 
								for(int j=0;j<minhdlist.size();j++){
									double totalPettyCashAmountMinor =0.0;
									MNH=(minorhead)minhdlist.get(j);
									if(MNH.getminorhead_id().equals("425")){
										totalPettyCashAmountMinor=BNK_L.getPettyCashTotalAmount("21450","sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									}//if ends
									else if(MNH.getminorhead_id().equals("235")){
										totalPettyCashAmountMinor=BNK_L.getPettyCashTotalAmount("21452","sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									}//else if ends
									else if(MNH.getminorhead_id().equals("633")){
										totalPettyCashAmountMinor=BNK_L.getPettyCashTotalAmount("21405","sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									}//else if ends
									double amount = Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) + Double.parseDouble(PEXP_L.getLedgerSum2(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fromdate,todate,hoaid));
									double amounts = Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fromdate,todate,cashtype3));
									double debitAmountNew2 = 0.0;
									double creditAmountNew2 = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
									 debitAmountNew2 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
									 creditAmountNew2 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", "");
									//System.out.println("credit amount..."+creditAmountNew2);
									}
									/* double staffWelfare = 0.0;
									if(hoaid.equals("4") && MH.getmajor_head_id().equals("34") && MNH.getminorhead_id().equals("477"))
									{
										staffWelfare = (amountNew2 + Double.parseDouble(PEXP_L.getLedgerSum2(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",finStartYr+"-04-01 00:00:00",todate,hoaid)) - Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",finStartYr+"-04-01 00:00:00",todate,cashtype3)));
									} */
									if(!("0.0".equals(""+amount)) || !("0.0".equals(""+amounts)) || debitAmountNew2 > 0 || creditAmountNew2 > 0){ 
										double testing = 0.0;
									double minDebitAssestAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)));
									double minDebitAssestAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fromdate,todate,hoaid));
									double minDebitAssestAmt2 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fromdate,todate,cashtype3));
									double minCreditAssestAmt = Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)));
								%>
                    
         
                    <tr>
                    	    <%-- <td style="color:#F00;"><span style="padding-left:15px;"><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=onlyfromdate%>&todate=<%=onlytodate%>&report=Income and Expenditure" style="color:#F00;"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></span></td> --%>
                   	<%
			if(!MNH.getminorhead_id().equals("636") && !MNH.getminorhead_id().equals("638")){%>  <!--condition added by madhav  -->
			
			
			<%
			
			if(MNH.getminorhead_id().equals("425") || MNH.getminorhead_id().equals("235") || MNH.getminorhead_id().equals("633")){
				String subHeadId = "";
				String headOfAccountId = "";
				
				if(MNH.getminorhead_id().equals("425")){
					subHeadId = "21450";
					headOfAccountId = "1";
				}else if(MNH.getminorhead_id().equals("235")){
					subHeadId = "21452";
					headOfAccountId = "4";
				}else if(MNH.getminorhead_id().equals("633")){
					subHeadId = "21405";
					headOfAccountId = "5";
				}
			
			   double debitAmountNew3 = 0.0;
			double creditAmountNew3 = 0.0;
			
			if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
				 debitAmountNew3 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), MNH.getminorhead_id(), subHeadId, "subhead","Assets", finYr, "debit", "");
				 creditAmountNew3 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), MNH.getminorhead_id(), subHeadId, "subhead","Assets", finYr, "credit", "");
				}
			double subDebitAssestAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(subHeadId,"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",headOfAccountId));
			double subDebitAssestAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(subHeadId,"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
			double subDebitAssestAmt2 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(subHeadId,"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,cashtype3));
			double totalPettyCashAmount =BNK_L.getPettyCashTotalAmount(subHeadId,"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
			
			 testing = debitAmountNew3 +subDebitAssestAmt + subDebitAssestAmt1 - subDebitAssestAmt2-totalPettyCashAmount;
// 			 System.out.println("testing:::::::::::"+testing);
			}
			
			 %>
                    	    <td style="color:#F00;"><span style="padding-left:15px;"><%=MNH.getminorhead_id() %>&nbsp;<%= MNH.getname() %></span></td>
                    	    
                            <td align="right"><%=df.format(minDebitAssestAmt + minDebitAssestAmt1 - minDebitAssestAmt2 +debitAmountNew2-totalPettyCashAmountMinor-testing) %></td> 
                            <%--  <%
                    	    if(MNH.getminorhead_id().equals("425")){
                    	    	System.out.println("minDebitAssestAmt::::::::"+minDebitAssestAmt);
                    	    	System.out.println("minDebitAssestAmt1::::::::"+minDebitAssestAmt1);
                    	    	System.out.println("minDebitAssestAmt2::::::::"+minDebitAssestAmt2);
                    	    	System.out.println("debitAmountNew2::::::::"+debitAmountNew2);
                    	    	System.out.println("totalPettyCashAmountMinor::::::::"+totalPettyCashAmountMinor);
                    	    }
                    	    %> --%> <%} %> <!--condition added by madhav  -->
                            <%debit=debit+minDebitAssestAmt + minDebitAssestAmt1 - minDebitAssestAmt2 + debitAmountNew2-totalPettyCashAmountMinor-testing;
                            if(!String.valueOf(minCreditAssestAmt).equals("00")){ %>
                            <%-- <td align="right"><%=creditAmountNew2+minDebitAssestAmt+minCreditAssestAmt%></td> --%>
                            <%//System.out.println("amount..."+df.format(creditAmountNew2+minDebitAssestAmt+minCreditAssestAmt));
                            //System.out.println("minCreditAssestAmt..."+minCreditAssestAmt);%>
							<% credit=credit+creditAmountNew2+minDebitAssestAmt+minCreditAssestAmt;%> 
							<%} else{ 
							double minCreditAssestAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)));%>
                            <%-- <td align="right"><%=df.format(creditAmountNew2+minCreditAssestAmt1+minCreditAssestAmt)%></td> --%>
                            <%//System.out.println("amount.11.."+df.format(creditAmountNew2+minDebitAssestAmt+minCreditAssestAmt));%>
                            <%credit=credit+creditAmountNew2+minCreditAssestAmt1+minCreditAssestAmt;} %>
                    </tr>
                    <% 
                    List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
                    String subhid = MNH.getminorhead_id();
                 
               if(!subhid.equals("636") && !subhid.equals("638")){ /* <!--condition added by madhav  --> */
                	
                    if(subhdlist.size()>0){ 
								for(int k=0;k<subhdlist.size();k++){
									SUBH=(subhead)subhdlist.get(k);
									if(!SUBH.getsub_head_id().equals("21450") && !SUBH.getsub_head_id().equals("21452") && !SUBH.getsub_head_id().equals("21405")){
									if(SUBH.getsub_head_id().equals("20201"))
										cashtype="creditsale";
									else if(SUBH.getsub_head_id().equals("20206"))
										cashtype="creditsale";
									else
										//cashtype="cash";
										cashtype ="online pending";
										cashtype1="othercash";
										cashtype2="offerKind";
										cashtype3="journalvoucher";
									onlyfromdate=fromdate;
									/* fromdate=fromdate+" 00:00:00"; */
									fromdate=fromdate;
									onlytodate=todate;
									/* todate=todate+" 23:59:59"; */
									todate=todate;
									double amount1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid),"sub_head_id","21450")) + Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
								  //  System.out.println("qqqqqq====="+amount1);//added by madhav
									double amount2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)))+Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid))) + Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,cashtype3));
									//System.out.println("wwwww====="+amount2);//added by madhav	
								    double debitAmountNew3 = 0.0;
									double creditAmountNew3 = 0.0;
									if(request.getParameter("cumltv")!=null && request.getParameter("cumltv").equals("cumulative")){
									 debitAmountNew3 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), MNH.getminorhead_id(), SUBH.getsub_head_id(), "subhead","Assets", finYr, "debit", "");
									 creditAmountNew3 = BBL.getMajorOrMinorOrSubheadOpeningBal(hoaid, MH.getmajor_head_id(), MNH.getminorhead_id(), SUBH.getsub_head_id(), "subhead","Assets", finYr, "credit", "");
									}
									 /* double staffWelfareSubhead = 0.0;
									if(hoaid.equals("4") && MH.getmajor_head_id().equals("34") && MNH.getminorhead_id().equals("477"))
									{
										staffWelfareSubhead = (amountNew3 + Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",finStartYr+"-04-01 00:00:00",todate,hoaid)) - Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",finStartYr+"-04-01 00:00:00",todate,cashtype3)));
									} */
									if(!("0.0".equals(""+amount1)) || !("0.0".equals(""+amount2)) || debitAmountNew3 > 0 || creditAmountNew3 > 0){ 
									double subDebitAssestAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
															   
									double subDebitAssestAmt1 = Double.parseDouble(PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid));
									double subDebitAssestAmt2 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,cashtype3));
									double subCreditAssestAmt = Double.parseDouble(CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
									double subCreditAssestAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead22(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fromdate,todate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)));
									double totalPettyCashAmount =BNK_L.getPettyCashTotalAmount(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid);
									%>	
                    <tr>
                   <%--  <%if(!CP_L.getLedgerSum(SUBH.getsub_head_id(),"sub_head_id",fromdate,todate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)).equals("00")){ %> --%>
                    <%if(!String.valueOf(subDebitAssestAmt).equals("00") && !df.format(debitAmountNew3+subDebitAssestAmt + subDebitAssestAmt1 - subDebitAssestAmt2-totalPettyCashAmount).equals("0")){ %>
                    	   <%-- <td><span style="padding-left:25px;"><a href="adminPannel.jsp?page=productLedgerReport&typeserch=Product&subhead=<%=SUBH.getsub_head_id()%>&subhid=<%=SUBH.getextra1()%>&hod=<%=SUBH_L.getHeadOFAccountIDOFMinorhead(subhid)%>" style="color:#000"><%=SUBH.getname() %></a></span></td> --%>
                    	   <td><span style="padding-left:25px;"><%=SUBH.getsub_head_id() %> <%=SUBH.getname() %></span></td>
                    	  <%--  <%}if(!df.format(debitAmountNew3+subDebitAssestAmt + subDebitAssestAmt1 - subDebitAssestAmt2-totalPettyCashAmount).equals("00")){ %> --%>
                            <%-- <%if(!PEXP_L.getLedgerSum2(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid).equals("00")){ %> --%>
                            <td align="left" style="color:#000;"><%=df.format(debitAmountNew3+subDebitAssestAmt + subDebitAssestAmt1 - subDebitAssestAmt2-totalPettyCashAmount)%></td>
                            <%}
                            debit=debit+debitAmountNew3+subDebitAssestAmt + subDebitAssestAmt1 - subDebitAssestAmt2-totalPettyCashAmount;
                           if(!String.valueOf(subCreditAssestAmt).equals("00")){ %>
                            <%-- <td align="left" style="color:#000;"><%=df.format(creditAmountNew3+subCreditAssestAmt1+subCreditAssestAmt) %></td> --%>
                           <%credit=credit+creditAmountNew3+subCreditAssestAmt1+subCreditAssestAmt;} else{ %>
                            <%-- <td align="left" style="color:#000;"><%=df.format(creditAmountNew3+subCreditAssestAmt1+subCreditAssestAmt) %></td> --%>
                            <%credit=credit+creditAmountNew3+subCreditAssestAmt1+subCreditAssestAmt;} %>
                    </tr>
                    <%}}}}}}
							 }}} }%>
                    

                    <%if(banklist.size()>0){ 
						for(int i=0;i<banklist.size();i++){
							BNK=(bankdetails)banklist.get(i);
							for(int j=0;j<typeofbanks.length;j++){
													//bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),fromdate,todate,typeofbanks[j]);
													bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartYr+"-04-01 00:00:01",todate,typeofbanks[j],"");
  													double finOpeningBal = BBL.getBankOpeningBal(BNK.getbank_id(), typeofbanks[j], request.getParameter("finYear"));
  													bankopeningbal += finOpeningBal;
  													if(bankopeningbal >0 || bankopeningbal<0){ %>
                    
                    
                    <tr>
                    	 <td><span style="padding-left:15px; font-weight:bold;"><%=BNK.getbank_name() %> <%=typeofbanks[j]%></span></td>
    <td align="right" style="font-weight:bold;"><%=df.format(bankopeningbal) %></td>
   <!--  <td align="right" style="font-weight:bold;">0.0</td> -->
                    </tr>
        <%
bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
bankopeningbal=0.0;
}}} }
/* debitassest=debitassest+bankopeningbalcredit; */
%>  
 <%
 String addedDate1New=SansthanAccountsDate.getAddedDate(todate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
 double totalcountercashNew =0.0;
  double totalcashpaidNew =0.0;
  double grandtotalNew = 0.0;
  double finOpeningBal2New = BBL.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));
  double totalAmtBeforeFromDate = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", todate));
  if(String.valueOf(totalAmtBeforeFromDate) != null && !String.valueOf(totalAmtBeforeFromDate).equals("")){
	  totalcountercashNew = Double.parseDouble(CP_L.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01 00:00:00", todate+" 23:59:59"));
  }
  String totalonlineDeposit = BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1New);
  double totalAmtDepositBeforeFromDate = 0.00;
  if(totalonlineDeposit != null && !totalonlineDeposit.equals("")){
	  totalAmtDepositBeforeFromDate = Double.parseDouble(totalonlineDeposit);
  }
  if(String.valueOf(totalAmtDepositBeforeFromDate) != null && !String.valueOf(totalAmtDepositBeforeFromDate).equals("")){
	  String totalOnlineDepositedBeforeFromdate = BNK_L.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01 00:00:00", addedDate1New+" 23:59:59");
  if(totalOnlineDepositedBeforeFromdate != null && !totalOnlineDepositedBeforeFromdate.equals("")){
	  totalcashpaidNew = Double.parseDouble(totalOnlineDepositedBeforeFromdate);
  }
  }
  double onlinedepositsNew = 0.00;
  totalonlineDeposit = BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1New);
 
  double totalOnlineDepositBeforeFromDate = 0.00;
  if(totalonlineDeposit != null && !totalonlineDeposit.equals("")){
	  totalOnlineDepositBeforeFromDate = Double.parseDouble(totalonlineDeposit);
  }
  if(String.valueOf(totalOnlineDepositBeforeFromDate)!= null && !String.valueOf(totalOnlineDepositBeforeFromDate).equals("")){
		String totalOnlineDepositedBeforeFromdate = BNK_L.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01 00:00:00", addedDate1New+" 23:59:59");
		if(totalOnlineDepositedBeforeFromdate != null && !totalOnlineDepositedBeforeFromdate.equals("")){
			onlinedepositsNew = Double.parseDouble(totalOnlineDepositedBeforeFromdate);
  }
  }
  grandtotalNew = finOpeningBal2New+totalcountercashNew-totalcashpaidNew-onlinedepositsNew;
 
  double proclosingbal = grandtotalNew;
 
 
/* double proclosingbal = 0.0;
proclosingbal = BNKCAL.getProCashClosingBalance(Todate, hoaid, reporttype, reporttype1); */

debitassest=debitassest+bankopeningbalcredit+proclosingbal;
%>
					<tr>
				      <td><span style="padding-left:15px; font-weight:bold;">CASH ACCOUNT</span></td>
				      <td align="right" style="font-weight:bold;"><%=df.format(proclosingbal) %></td>
				     <!--  <td align="right" style="font-weight:bold;">0.0</td> -->
		           </tr>
            </table>
       </td> 
      
			
      
  <%-- <%
  System.out.println("credit==>"+credit);
  System.out.println("debit==>"+debit);
  System.out.println("creditliab==>"+creditliab);
  System.out.println("debitliab==>"+debitliab);
  System.out.println("creditassest==>"+creditassest);
  System.out.println("debitasset==>"+debitassest);
  %> --%>
      
     			<!--  Excess Of Incomes Over Expenditure start -->
      <tr>
      
       	<td colspan="2" >
       		<table  width="100%"  cellspacing="0" cellpadding="0" border="1">
        		<tr>
        			<%
//         			System.out.println("proclosingbal................................."+proclosingbal);
//         			System.out.println("bankopeningbalcredit................................."+bankopeningbalcredit);
//         			System.out.println("debitassest................................."+debitassest);
//         			System.out.println("debitliab................................."+debitliab);
        			
        			if(debitassest>debitliab){ %>
                    	 <td width="47%" style="font-weight:bold">Excess of Incomes Over Expend</td>
						<!--  <td width="27%" align="right" style="font-weight:bold">0.00</td> -->
						 <td width="27%" align="right" style="font-weight:bold"><%=df.format((debitassest-debitliab))%></td>
						 <%debitliab=debitassest;
					} else{	%>
								-
					<%} %>
                 </tr>
              </table>
         </td>
         <td colspan="2">
       		   <table width="100%"  cellspacing="0" cellpadding="0" border="1">
		       		
        			<tr>
 						<%if(debitassest<debitliab){	%>
                    	 	 <td width="47%" style="font-weight:bold">Excess of Incomes Over Expend</td>
						    <!--  <td width="27%" align="right" style="font-weight:bold">0.00</td> -->
						     <td width="27%" align="right" style="font-weight:bold"><%=df.format((debitliab-debitassest+proclosingbal))%></td>
						    <%	debitassest=debitliab;
 						} else{	%>-<%} %>
                   </tr>
               </table>
           </td>
      </tr>
                    
 				<!--  Excess Of Incomes Over Expenditure ends  -->
                    
                 <!-- Total column starts -->
                    
        <tr>
       		<td colspan="2" >
       			<table  width="100%"  cellspacing="0" cellpadding="0" border="1">
					<tr>
                    	<td width="47%" style="font-weight:bold">Total(Rupees)</td>
    				<!-- 	<td width="27%" align="right" style="font-weight:bold">0.0</td> -->
    					<td width="27%" align="right" style="font-weight:bold">Rs.<%=df.format(debitassest) %></td>
                   </tr>
                     
                </table>
           	 </td>
             <td colspan="2">
       			 <table width="100%"  cellspacing="0" cellpadding="0" border="1">
       				 <tr>
                    	 <td width="47%" style="font-weight:bold">Total(Rupees)</td>
   						 <!-- <td width="27%" align="right" style="font-weight:bold">0.0</td> -->
   						 <td width="27%" align="right" style="font-weight:bold">Rs.<%=df.format(debitliab) %></td>
                    </tr>
                 </table>
              </td>
        </tr>
                    
 					<!-- Total column ends -->
                    
  </tr>
  
  <tr></tr>

</table>
</div>
</div>
</div>
</div>
</div>

<%}}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>