<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="mainClasses.employeeDetailsListing,beans.employeeDetails,java.util.List,java.util.Iterator,beans.employeeSalary"%>
<%@ page import="mainClasses.employeeSalarySlipListing,mainClasses.employeeAttendce,mainClasses.stafftimingsListing,beans.stafftimings"%>
 <%@ page import="java.text.SimpleDateFormat"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>


<style>th{border:1px solid black;color:red}</style>
<script type="text/javascript">
$(function() {
	$("#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		
		});
	});

</script>
<script>
function myFunction() 
{
	 	if(document.frm.month.value=="")
	    {
	       alert("please enter valid  month and year ");
	       document.frm.month.focus();
	       return false;
	    }
	 	
	 	else
		 {
    		document.getElementById("id1").submit();
		 }
}
</script>

<title>Insert title here</title>
</head>
<body>
<form action="adminPannel.jsp"  name="frm" >

	<select name="department">
	<%
		stafftimingsListing stlisting=new stafftimingsListing(); 
		List stlist=stlisting.getAllDistinctStaffType();
		Iterator stitr=stlist.iterator();
		while(stitr.hasNext())
		{
			stafftimings stimings=(stafftimings)stitr.next();
	%>
		<option><%=stimings.getstaff_type()%></option>
	<% }%>
	</select>
		<!-- Month:<input type="month" name="month"> -->
  		Month: <input type="text" id="fromDate" name="month"/> 
	
	<input type="hidden" name="page" value="employeePaySlip"  >
	<input type="submit" id="id1" value="ok" onclick=" return myFunction()">
</form>
<%

//list of all employee Id belongs to particular month 
if(!(request.getParameter("month")==null))
{
	StringBuffer ids=new StringBuffer("");
	String month=request.getParameter("month");
	String department=request.getParameter("department");
	
	SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy");
	Date d1=df.parse(month);
	SimpleDateFormat df1=new SimpleDateFormat("yyyy-MM-dd");
	String dateAndMonth=df1.format(d1);
	
	employeeDetailsListing  listing =new employeeDetailsListing ();
	List l=listing.selectAllEmployeeIdOnly();
	Iterator itr=l.iterator();
	while(itr.hasNext())
	{
		String eid=(String)itr.next();
		ids=ids.append("&employee_id=").append(eid);
	}
	
	String url="paysliplist.jsp?month="+dateAndMonth+ids.toString();%>
	<script type="text/javascript">
	window.parent.location="adminPannel.jsp?page=<%=url%>";
	</script>
	<%

}
%>
</body>
</html>