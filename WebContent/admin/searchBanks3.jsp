<%@page import="java.util.TimeZone"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.List"%>
<jsp:useBean id="BNK" class="beans.bankdetails" />
<%
	if (request.getParameter("q") != null) {
		
		String date = "",finStartYr = "",finStartAndEndYr = "",currentDateNew = "",finYear="";
		
		bankcalculations BNKCAL = new bankcalculations();
		bankbalanceListing BBAL_L = new mainClasses.bankbalanceListing();

		if (request.getParameter("date") != null && !request.getParameter("date").trim().equals("") && request.getParameter("finyear") != null && !request.getParameter("finyear").trim().equals(""))	{
				date = request.getParameter("date");
				finYear = request.getParameter("finyear");
		}
//		System.out.println("date-----"+date);
//		System.out.println("finYear-----"+finYear);
		
		if (date.trim().equals(""))	{
			
			DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
			TimeZone tz = TimeZone.getTimeZone("IST");
			dateFormat2.setTimeZone(tz);

			Calendar c2 = Calendar.getInstance();
			currentDateNew = (dateFormat2.format(c2.getTime())).toString();
			String monthInString = currentDateNew.substring(5, 7);
			int month = Integer.parseInt(monthInString);
			String yearInString = currentDateNew.substring(0, 4);
			int year = Integer.parseInt(yearInString);


			if (month >= 4) {
				finStartYr = Integer.toString(year);
			} else {
				int finStartYrInInt = year - 1;
				finStartYr = Integer.toString(finStartYrInInt);
			}


			String tempInString = finStartYr.substring(2, 4);
			int tempInInt = Integer.parseInt(tempInString) + 1;
			finStartAndEndYr = finStartYr + "-" + tempInInt;
		} // if
		else	{
			currentDateNew = date;
			finStartYr = finYear.split("-")[0];
			finStartAndEndYr = finYear;
		}
		
		String s = request.getParameter("q");
		String hoa = request.getParameter("HOA");
		List Banks_List = null;
		DecimalFormat DF = new DecimalFormat("0.00");
		String banktype = "";
		
//		System.out.println("finStartAndEndYr------"+finStartAndEndYr);
//		System.out.println("currentDateNew------"+currentDateNew);
//		System.out.println("finStartYr----"+finStartYr);
		if (s.length() >= 1) {
			bankdetailsListing BNK_CL = new mainClasses.bankdetailsListing();
			if (hoa != null && !hoa.equals("")) {
				Banks_List = BNK_CL.getNormalAndLoanBanksBasedOnKeys(s,hoa);
			} else {
				Banks_List = BNK_CL.getNormalAndLoanBanksBasedOnKeys(s,"");
			}
			for (int i = 0; i < Banks_List.size(); i++) {

				BNK = (beans.bankdetails) Banks_List.get(i);

				double bankopeningbal = 0.0;
				double pettycashOpeningbal = 0.0;

				if (BNK.getextra2().equals("debit")) {
					bankopeningbal = BNKCAL.getBankOpeningBalance(BNK.getbank_id(), finStartYr+ "-04-01 00:00:01", currentDateNew + " 23:59:59", "bank", "");
					double finOpeningBalNew = BBAL_L.getBankOpeningBal(BNK.getbank_id(), "bank", finStartAndEndYr);
					bankopeningbal += finOpeningBalNew;

					pettycashOpeningbal = BNKCAL.getBankOpeningBalance(BNK.getbank_id(), finStartYr + "-04-01 00:00:01", currentDateNew + " 23:59:59", "cashpaid", "");
//					System.out.println("pettycashOpeningbal1111111 >>>> "+ pettycashOpeningbal);
					double pettycashFinOpeningBal = BBAL_L.getBankOpeningBal(BNK.getbank_id(),"cashpaid", finStartAndEndYr);
//					System.out.println("pettycashFinOpeningBalgetBankOpeningBal >>>> "+ pettycashFinOpeningBal);
					pettycashOpeningbal += pettycashFinOpeningBal;
//					System.out.println("pettycashOpeningbal222222222 >>>> "+ pettycashOpeningbal);

				} else if (BNK.getextra2().equals("credit")) {
					bankopeningbal = BNKCAL.getCreditBankOpeningBalance(BNK.getbank_id(), finStartYr + "-04-01 00:00:01",currentDateNew + " 23:59:59", "");
					double finOpeningBalNew = BBAL_L.getBankOpeningBal(BNK.getbank_id(), "bank", finStartAndEndYr);
					bankopeningbal += finOpeningBalNew;
				}

				out.println(BNK.getbank_id() + "," + BNK.getbank_name()+ "," + DF.format(bankopeningbal) + ","+ pettycashOpeningbal + "," + BNK.getextra2());
			}
			if (Banks_List.size() == 0) {
				out.print(request.getParameter("q"));
			}
		}

	}
%>