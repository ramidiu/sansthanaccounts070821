<%@page import="java.util.Arrays"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="beans.products"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!--Date picker script  -->

<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
	<%if(session.getAttribute("adminId")!=null){
		customerpurchasesListing cpl = new customerpurchasesListing();
		List<String[]> pList = cpl.getOfferKindProductList();
		productsListing pl = new productsListing();
		headofaccountsListing hoal = new headofaccountsListing();
		String pname = "";
		String[] arr = new String[2];
		if(request.getParameter("productId") != null){
			arr = request.getParameter("productId").split(",");
			pname = pl.getProductsNameByCats(arr[0], arr[1]);
		}
		String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
			 fromdate=request.getParameter("fromDate");
		}
		if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
			todate=request.getParameter("toDate");
		}
	%>
	<!-- main content -->
	<div>
		<div class="vendor-page"><br></br>
		<div class="vendor-list">
		
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form name="searchForm" id="searchForm" action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" name="page" value="offerKindsProductWise"/>
						<select name="productId" id="productId">
						<option value="All, " <%if(request.getParameter("productId") != null && arr[0].equals("All,")){%>selected<%}%>>All</option>
						<%for(int i=0;i<pList.size();i++){
							String[] arry = pList.get(i);
							String id = arry[0];
							String hid = arry[1];
							String pName = "";
							pName = pl.getProductsNameByCats(id, hid);
							if(pName.trim().equals("")){
								pName = pl.getProductsNameByCats(id, "");
							}%>
						<option value="<%=id%>,<%=hid%>" <%if(request.getParameter("productId") != null && arr[0].equals(id)){%>selected<%}%>><%=id%> <%=pName%></option>
						<%}%>
						</select></li>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate%>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate%>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
				<%
				if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
					 fromdate=request.getParameter("fromDate");
				}
				if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
					todate=request.getParameter("toDate");
				}
				SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
				Calendar c1 = Calendar.getInstance();
				String currentDate=(dateFormat2.format(c1.getTime())).toString();
				String fromDate=currentDate+" 00:00:01";
				String toDate=currentDate+" 23:59:59";
				if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
					if(request.getParameter("fromDate")!=null){
						fromDate=request.getParameter("fromDate")+" 00:00:01";
					}
					if(request.getParameter("toDate")!=null){
						toDate=request.getParameter("toDate")+" 23:59:59";
					}
				}
				%>
	<div class="printable">    
             <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;
	margin:0 2%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="10" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;">Offered Kinds Report Based On Product</td></tr>
						<tr><td colspan="10" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
						<tr>
						<td colspan="10">
						<table width="100%" cellpadding="0" cellspacing="0" >
						<tr><td colspan="10"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
						
							<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
							<td>
							<%
							String pid = "All";
							String prdName = "";
							String hoa = "";
							double totalQuantity = 0.00;
							double totalAmount = 0.00;
							if(request.getParameter("productId") != null)
							{
								pid = arr[0];
							}
							List<customerpurchases> cpList = cpl.getOfferKindsListingBasedOnProductId(fromdate, todate, pid);
							if(cpList.size()>0)
							{
							%>
							<table  width="100%" cellpadding="0" cellspacing="0" border="1">
							<tr>
								<td class="bg" style="font-weight: bold;">INVOICE DATE</td>
								<td class="bg" style="font-weight: bold;">HEAD OF ACCOUNT</td>
								<td class="bg" style="font-weight: bold;">CODE-KIND NAME</td>
								<td class="bg" style="font-weight: bold;">RECEIPT.NO</td>
								<td class="bg" style="font-weight: bold;">DEVOTEE</td>
								<td class="bg" style="font-weight: bold;">QTY</td>
								<td class="bg" style="font-weight: bold;">RUPEES</td>
						</tr>
						<%	for(int j=0;j<cpList.size();j++){
								customerpurchases CpList = cpList.get(j);
								prdName = pl.getProductsNameByCats(CpList.getproductId(), CpList.getextra1());
								if(prdName.equals("")){
									prdName = pl.getProductsNameByCats(CpList.getproductId(), "");
								}
								hoa = hoal.getHeadofAccountName(CpList.getextra1());
						%>							
							<tr>
								<td><%=chngDateFormat.format(dbDateFormat.parse(CpList.getdate()))%></td>
								<td><%=hoa%></td>
								<td><%=CpList.getproductId()%> <%=prdName%></td>
								<td><%=CpList.getbillingId()%></td>
								<td><%=CpList.getcustomername()%></td>
								<td><%=CpList.getquantity()%></td>
								<td>&#8377;<%=CpList.getrate()%></td>
							</tr>
						<%totalQuantity = totalQuantity + Double.parseDouble(CpList.getquantity());
						  totalAmount = totalAmount + Double.parseDouble(CpList.getrate());
							}
							if(!pid.equals("All")){%>
							<tr>
								<td colspan="5" align="right" style="color:orange;font-weight:bold;">GRAND TOTAL&nbsp;</td>
								<td colspan="1" align="left" style="font-weight:bold;"><%=totalQuantity%></td>
								<td colspan="1" align="left" style="font-weight:bold;">&#8377;<%=totalAmount%></td>
							</tr>
							<%}%>
							</table>
							<%
							}else{
								if(!pid.equals("All")){%>
								<tr><td colspan="7" align="center" style="color:red;padding-top:50px;font:bold;font-size:large;">There are no offer kinds of <%=pname%> <%if(!fromdate.equals(todate)){%>between <%=fromdate%> and <%=todate%><%}else{%>on <%=fromdate%><%}%></td></tr>
							<%  }
								else{%>
								<tr><td colspan="7" align="center" style="color:red;padding-top:50px;font:bold;font-size:large;">There are no offer kinds <%if(!fromdate.equals(todate)){%>between <%=fromdate%> and <%=todate%><%}else{%>on <%=fromdate%><%}%></td></tr>
							<%	}  	
							  }
							%>
							</td>
													
							<td style="padding:0px;"><table  width="100%" cellpadding="0" cellspacing="0">

							</table>
</table></td></tr>
						
						  
						</table>
						
						</td>
						</tr>
					</table>
					</div>
			</div>
			</div>
</div>
</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>