<%@page import="beans.sent_sms_emailService"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.util.List"%>
<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@page import="sms.CallSmsApi1"%>
<%@page import="sms.CallSmscApi"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
DateFormat  dateFormat5= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
TimeZone tz = TimeZone.getTimeZone("IST");
dateFormat5.setTimeZone(tz);
//CallSmsApi1 SMS = new CallSmsApi1();
CallSmscApi SMS = new CallSmscApi();
String mailBody="";
String subject = "NITYA ANNADANAM INVITATION";
String emailTemplate = "<div style='width:650px;margin:0px auto; background:#ff6600;padding:20px 0px;'><div style='width:580px;margin:0px auto;border:3px solid #fab001;padding:10px 10px;border-radius:15px;'><h3 style='text-align:center;color:#fff;font-family:Open Sans, sans-serif ;'>Shri Shirdi Saibaba Sansthan Trust</h3><h4 style='padding-left:18px;color:#fff;font-family:Open Sans, sans-serif ;'>Dear Saibandhu,</h4><p style='letter-spacing:0.5px;text-align:center;color:#fff;font-family:Open Sans, sans-serif;font-size:14px;'>Greetings and blessings from Shri Shirdi Saibaba Sansthan Trust, Dilsukhnagar, Hyderabad.</p><p style='letter-spacing:0.5px;line-height:20px;text-align:center;padding:0px 25px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'> The Sansthan Trust is very thankful and grateful for your generous contribution toward the Nitya Annadana Padhakam the sacred tradition of food offering, being organized by the Sansthan Trust on every day to serve the hunger.  </p><p style='letter-spacing:0.5px;line-height:20px;text-align:center;padding:0px 25px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>On this occasion, the Sansthan Trust performs Archana on the name of donor.  The Sansthan Trust cordially invites you, your family members and friends (maximum of 5 members) to participate in Archana which will be performed before 11.00 am. Attend Shri Saibaba Akanda Harathi and                Sri Sai Prasadam (Annadanam) and to receive the blessings of Shri Saibaba at Sansthan, Dilsukhnagar, Hyderabad.</p><h5 style='text-align:center;font-size:17px;color:#fff;font-family:cursive, sans-serif ;'> AnnidanalaKanna &#45; AnnadanamMinna</h5><h5 style='text-align:center;font-size:17px;color:#fff;font-family:cursive, sans-serif;margin-top:-20px;'>Annadata Sukhibhava</h5><p style='padding:0px 0px 0px 30px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;font-weight:bold;'>- With the blessings of Shri Shirdi Saibaba </p><p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Shri Shirdi Saibaba Sansthan Trust,</p> <p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>Dilsukhnagar, Hyderabad. </p><p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>www.saisansthan.in</p> <p style='padding:0px 0px 0px 30px;line-height:5px;color:#fff;font-family:Open Sans, sans-serif ;font-size:14px;'>91-40-24066566</p></div></div> <div style='margin:0px auto;padding-left:20px;'><ul style='list-style:none;'><li style='float:left;color:#ff6600;padding:10px 10px 10px 10px;font-family:Open Sans, sans-serif;letter-spacing:0.5px;font-weight:bold;'>Follow us :</li><li style='padding:10px 10px 10px 10px;'><a href='http://www.facebook.com/dilsuknagarsaibabatemple' target='_blank'><img src='http://www.accounts.saisansthan.in/images/fb.png'/></a></li></ul></div>";
SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd");
SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MMM");

DateFormat dateFormat4= new SimpleDateFormat("MM-dd");

Calendar c1 = Calendar.getInstance();
Calendar c2 = Calendar.getInstance(); 
c1.add(Calendar.DATE, 1);
c2.add(Calendar.DATE, 7);

String str1=(dateFormat4.format(c1.getTime())).toString();
String str2=(dateFormat4.format(c2.getTime())).toString();

ArrayList<String> ar = new ArrayList<String>();
ar.add(str1);
ar.add(str2);

model.sendmailservice sendMail1=new model.sendmailservice();
customerapplicationListing CAPL=new customerapplicationListing();
customerapplication CAP = new customerapplication();
customerpurchasesListing SALE_L = new customerpurchasesListing();
customerpurchases CUSPUR = new customerpurchases();

sssst_registrationsListing SSREGL=new sssst_registrationsListing();
sssst_registrations SSSST = new sssst_registrations();

String nadTemp = ".u r cordially invited to participate with 5 members before 11.00 AM for Archana n Annadanam at Sansthan SSSST, DSNR.";
String ltaTemp = ".u r cordially invited to participate in Archana before 11.00 AM n receive blessings of Shri Saibaba. SSSST, DSNR.";

for(int j=0; j < ar.size(); j++)
{
String date = ar.get(j);
List CAPLIST= CAPL.getListForDOBOrAnnivOrNADLTAReminder("extra1",date,"","withPhone","NAD");

if(CAPLIST.size() > 0)
{
	for(int i=0; i < CAPLIST.size(); i++)
	{
		String mobilenum = "";
		String email = "";
		String name = "";
		String daysRemainder="";
		String nadOrLtaId = "";
		String perfDate = "";
		String details = "";
		
		if(j == 0)
		{
			daysRemainder = "1 day";
		}
		
		else if(j == 1)
		{
			daysRemainder = "7 days";
		}
		
		CAP=(customerapplication)CAPLIST.get(i);
		List CUSPURLIST=SALE_L.getBillDetails(CAP.getbillingId());
		CUSPUR=(customerpurchases)CUSPURLIST.get(0);
		mobilenum = CUSPUR.getphoneno().trim();
		email = CUSPUR.getextra8().trim();
		name = CAP.getlast_name().trim().replace("&", "");
		nadOrLtaId = CAP.getphone().trim();
		perfDate = CAP.getextra1().trim();
		
		if(!nadOrLtaId.equals(""))
		{
			details += " (YOUR REF NO: "+nadOrLtaId+")";
		}
		if(!perfDate.equals("") && !perfDate.equals("-"))
		{
			String dt = "";
			try{
			dt = dateFormat2.format(dateFormat1.parse(perfDate));
			}
			catch(ParseException parseEx){
				parseEx.printStackTrace();
			}
			perfDate = dt;
		}
		
		if(!mobilenum.equals(""))
		{
			String template = "Shri/Smt, NAD on "+name+", "+details+". Greetings n blessings ur NAD is on "+perfDate+nadTemp;
			SMS.SendSMS(mobilenum,template.replace(" ", "%20"));
			
			Calendar c5 = Calendar.getInstance(); 
			String date2=(dateFormat5.format(c5.getTime())).toString();
			
			sent_sms_emailService SNE_SER=new sent_sms_emailService();
			
			SNE_SER.setRegId(nadOrLtaId);
			SNE_SER.setName(name);
			SNE_SER.setPurpose("NAD");
			SNE_SER.setDaysremainder(daysRemainder);
			SNE_SER.setEmail("");
			SNE_SER.setPhone(mobilenum);
			SNE_SER.setCreatedDate(date2);
			
			SNE_SER.insert();
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(!email.equals(""))
		{
			mailBody="<h4 style='color:#ff6600;'>Hello, NAD on "+name+" ,"+details+".Please attend on "+perfDate+".</h4>"+emailTemplate; 
			try 
			{
				/*sendMail1.sendmail("reports@saisansthan.in", email,subject,mailBody);*/
				sendMail1.sendmailsansthan("info@saisansthan.in", email,subject ,mailBody);
				
				Calendar c5 = Calendar.getInstance(); 
				String date2=(dateFormat5.format(c5.getTime())).toString();
				
				sent_sms_emailService SNE_SER=new sent_sms_emailService();
				
				SNE_SER.setRegId(nadOrLtaId);
				SNE_SER.setName(name);
				SNE_SER.setPurpose("NAD");
				SNE_SER.setDaysremainder(daysRemainder);
				SNE_SER.setEmail(email);
				SNE_SER.setPhone("");
				SNE_SER.setCreatedDate(date2);
				
				SNE_SER.insert();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
}
List CAPLIST2= CAPL.getListForDOBOrAnnivOrNADLTAReminder("extra1",ar.get(0),"","withPhone","LTA");
if(CAPLIST2.size() > 0)
{
for(int i=0; i < CAPLIST2.size(); i++)
{
	String mobilenum = "";
	String email = "";
	String name = "";
	String daysRemainder="1 day";
	String nadOrLtaId = "";
	String perfDate = "";
	String details = "";
	
	CAP=(customerapplication)CAPLIST2.get(i);
	List CUSPURLIST=SALE_L.getBillDetails(CAP.getbillingId());
	CUSPUR=(customerpurchases)CUSPURLIST.get(0);
	mobilenum = CUSPUR.getphoneno().trim();
	email = CUSPUR.getextra8().trim();
	name = CAP.getlast_name().trim().replace("&", "");
	nadOrLtaId = CAP.getphone().trim();
	perfDate = CAP.getextra1().trim();
	
	if(!nadOrLtaId.equals(""))
	{
		details += " (YOUR REF NO: "+nadOrLtaId+")";
	}
	if(!perfDate.equals("") && !perfDate.equals("-"))
	{
		String dt = "";
		try{
		dt = dateFormat2.format(dateFormat1.parse(perfDate));
		}
		catch(ParseException parseEx){
			parseEx.printStackTrace();
		}
		perfDate = dt;
	}
	
	if(!mobilenum.equals(""))
	{
		String template = "Shri/Smt, LTA on "+name+", "+details+". Greetings n blessings ur LTA is on "+perfDate+ltaTemp;
		SMS.SendSMS(mobilenum,template.replace(" ", "%20"));
		
		Calendar c5 = Calendar.getInstance(); 
		String date2=(dateFormat5.format(c5.getTime())).toString();
		
		sent_sms_emailService SNE_SER=new sent_sms_emailService();
		
		SNE_SER.setRegId(nadOrLtaId);
		SNE_SER.setName(name);
		SNE_SER.setPurpose("LTA");
		SNE_SER.setDaysremainder(daysRemainder);
		SNE_SER.setEmail("");
		SNE_SER.setPhone(mobilenum);
		SNE_SER.setCreatedDate(date2);
		
		SNE_SER.insert();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
}
%>
</body>
</html>