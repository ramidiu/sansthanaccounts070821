<%@ page import="java.util.List" errorPage="" %>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- <script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script> -->
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( "#printable" ).print();
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>    
    
    <style>
	
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
/*.bod-nn{
	border-right:none !important;}*/
	}
/*	.bod-nn{
	border-right:1px solid #000 !important;
	font-weight: bold;}*/
</style>
</head>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="ROM" class="beans.integerToRomanNumber"/>	
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Head Of Account</div>
<div class="click floatright"><a href="./newHeadOfAccount.jsp" target="_blank" onclick="openMyModal('newHeadOfAccount.jsp'); return false;">New Head Of Account</a></div>
</div>

<div class="vendor-list">
<div class="search-list">
<ul>
<li><input type="search" placeholder="Search"/></li>
</ul>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="list-details" >
                 <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<div id="printable">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
<tr><td colspan="5"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="3%" align="center"><input type="checkbox" /></td>
<td class="bg"  width="3%" align="center">S.No.</td>
<td class="bg" width="23%" align="center">Name</td>
<td class="bg" width="23%" align="center">Phone</td>
<td class="bg" width="20%" align="center">Address</td>
</tr>
</table></td></tr>
<%mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();
if(HOA_List.size()>0){
for(int i=0; i < HOA_List.size(); i++ ){
	HOA=(beans.headofaccounts)HOA_List.get(i);%>
<tr>
<td  width="3%" align="center"><input type="checkbox"></td>
<td  width="3%" align="center"><%=ROM.IntegerToRomanNumeral(i+1)%></td>
<td  width="23%" align="center" style="text-align:center"><ul style="text-align:center;">
<li style="text-align:center;"><span style="text-align:center"><a href="adminPannel.jsp?page=headOfAccountDetails&id=<%=HOA.gethead_account_id()%>"><%=HOA.getname()%></a></span><br /></li>
</ul></td>
<td  width="23%" align="center"><%=HOA.getphone()%></td>
<td  width="20%" align="center"><%=HOA.getaddress()%></td>
</tr>

<%}%>
</table>
<%}else{%>
<div><h2>No Head Of Accounts Created</h2></div>
<%}%>
</div>

</div>
</div>
</div>
</body>
</html>