<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.ewf_entriesListing"%>
<%@page import="beans.ewf_entries"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="beans.bankdetails"%>
<%@page import="com.accounts.saisansthan.bankcalculations"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EWF Report</title>
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                     $( "a" ).css("text-decoration","none");
                    $( "#tblExport" ).print();

                    // Cancel click event.
                    return( false );
                });
			});
         $(document).ready(function () {
            $("#btnExport").click(function () {
                $( "a" ).css("text-decoration","none");
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="EWFO" class="beans.ewf_entries"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
</head>
<body>
	<%
	double bankopeningbal=0.0;
	double bankopeningbalcredit=0.0;
	double bankopeningbaldebit=0.0;
	bankcalculations BNKCAL=new bankcalculations();
	bankdetailsListing BNKL=new bankdetailsListing();
	List banklist=BNKL.getBanksBasedOnHOA("10");
	String typeofbanks[]={"bank","cashpaid"};
	DecimalFormat DF=new DecimalFormat("#,###.00"); 
	
	DecimalFormat df=new DecimalFormat("#,###.00");
	ewf_entriesListing ewflist = new ewf_entriesListing();
	List EWFLISTS = null;
	double amount;
	amount=0.0;
	DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat  dateFormat2= new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
    String Vocherno[]=null;
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat DBDate= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(new SimpleDateFormat("yyyy-MM-dd").format(c1.getTime())).toString();
	/* String fromDate=currentDate+" 00:00:01"; */
	String fromDate="2016-04-01";
	int day=1;
	/* String toDate=currentDate+" 23:59:59"; */
	String toDate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	
	mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
	mainClasses.majorheadListing MAJOR_L=new mainClasses.majorheadListing();
	mainClasses.minorheadListing MINOR_L=new mainClasses.minorheadListing();
	subheadListing SUBHL=new subheadListing();
	List HA_Lists=HOA_L.getheadofaccounts();
	String hoid="";
	if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
		hoid=request.getParameter("hoid");
	}
	 if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
			if(request.getParameter("fromDate")!=null){
				/* fromDate=request.getParameter("fromDate")+" 00:00:01"; */
				fromDate=request.getParameter("fromDate");
			}
			if(request.getParameter("toDate")!=null){
				/* toDate=request.getParameter("toDate")+" 23:59:59"; */
				toDate=request.getParameter("toDate");
			}
		}
//	DecimalFormat DF=new DecimalFormat("0.00");
	String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
	if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
		 fromdate=request.getParameter("fromDate");
	}
	if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
		todate=request.getParameter("toDate");
	}
	EWFLISTS=ewflist.getOpeningBalRecordsForCurrentFinYear();
	%>
<div class="vendor-page">
<div class="vendor-list">
<form action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
			<li>
<select name="hoid" id="hoid" onchange="formSubmit();">
<option value="10" > E.W.F </option>
</select></li>
						<li>
						<input type="hidden" name="page" value="EWFReport"></input>
						<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromDate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=toDate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
<div class="icons">
<span id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span id="btnExport"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="list-details" id="tblExport">

<table width="100%" cellpadding="0" cellspacing="0"   >
<tr><td colspan="9" height="10"></td> </tr>
<tr><td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
<tr><td colspan="9" align="center" style="font-weight: bold;font-size: 20px;">E.W.F REPORT  </td></tr>
</table>

<%
	String frmDat = fromDate+" 00:00:01";
	String toDat = toDate+" 23:59:59";
	
	double totalDebits = 0.0;
	double totalCredits = 0.0;
	
	
	%>

	<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
    	<td  class="bg">S.No</td>
		<td class="bg" > Major Head</td>
		<td class="bg" > Minor Head</td>
		<td class="bg" >Sub Head</td>
		<!-- <td class="bg">Amount</td> -->
		<td class="bg">Debits</td>
		<td class="bg">Credits</td>
	</tr>
	<% if(banklist.size()>0){ 
		for(int i=0;i<banklist.size();i++){
			BNK=(bankdetails)banklist.get(i);
			for(int j=0;j<typeofbanks.length;j++){
			bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),"",frmDat,typeofbanks[j]);
			//bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartDate,fromdate,typeofbanks[j],"");
			 if(bankopeningbal >0 || bankopeningbal<0 || typeofbanks[j].equals("bank")){ 
	%>
<tr>
<td height="28" style="color:#F00; font-weight:bold;" colspan="4"><%=BNK.getbank_name() %> <%=typeofbanks[j]%></td>
<td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
<td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(bankopeningbal) %></td>

</tr>
<%bankopeningbalcredit=bankopeningbalcredit+bankopeningbal;
				bankopeningbal=0.0;
					 } }} }
%>
    
		<%if(EWFLISTS.size()>0){
			for(int i=0;i<EWFLISTS.size();i++){
				EWFO=(ewf_entries)EWFLISTS.get(i);
				//debitamount=debitamount+Double.parseDouble(CUST.gettotalAmmount());
				//j++;
				%>
				<tr>
    	<td><%=i+1 %></td>
		<td ><%=EWFO.getMajor_head_id() %> <%=MAJOR_L.getmajorheadName(EWFO.getMajor_head_id()) %></td>
		<td ><%=EWFO.getMinor_head_id() %> <%=MINOR_L.getminorheadName(EWFO.getMinor_head_id()) %></td>
		<td ><%=EWFO.getSub_head_id() %> <%=SUBHL.getMsubheadnameWithDepartment(EWFO.getSub_head_id(), EWFO.getHead_account_id()) %></td>
		<%
			String openingBalInString = ewflist.getCurrentFinYearOpeningBalBasedOnSubHead(EWFO.getSub_head_id());
			Double openingBal = Double.parseDouble(openingBalInString);
			
			/* String sumOfDebitsInString = ewflist.getSumOfDebitsBasedOnSubHead(EWFO.getSub_head_id(), frmDat, toDat);
			Double sumOfDebits = Double.parseDouble(sumOfDebitsInString);
			
			String sumOfCreditsInString = ewflist.getSumOfCreditsBasedOnSubHead(EWFO.getSub_head_id(), frmDat, toDat);
			Double sumOfCredits = Double.parseDouble(sumOfCreditsInString); */
			
			if(EWFO.getMinor_head_id().equals("492"))
			{%>
			<td></td>
			<%String sumOfCreditsInString = ewflist.getSumOfCreditsBasedOnSubHead(EWFO.getSub_head_id(), frmDat, toDat);
			Double sumOfCredits = Double.parseDouble(sumOfCreditsInString); 
			double result = openingBal - sumOfCredits;
			totalCredits += result;
			%>
			<td><%=df.format(result) %></td>
				<% 
			}else{
				String sumOfDebitsInString = ewflist.getSumOfDebitsBasedOnSubHead(EWFO.getSub_head_id(), frmDat, toDat);
				Double sumOfDebits = Double.parseDouble(sumOfDebitsInString); 
				double result = openingBal + sumOfDebits;
				totalDebits += result;					
			
			%>
			<td><%=df.format(result) %></td>
			<td></td>
			<%} %>
			
			
			<%-- Double result = openingBal + sumOfDebits - sumOfCredits;
			amount = amount + result;%>
			<td><%=df.format(result) %></td> --%>
		
		</tr>
				<%}
			}%>
	<%
  if(banklist.size()>0){ 
  	for(int i=0;i<banklist.size();i++){
  		BNK=(bankdetails)banklist.get(i);
  		for(int j=0;j<typeofbanks.length;j++){
  			bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),"",toDat,typeofbanks[j]);
  			//bankopeningbal=BNKCAL.getBankOpeningBalance(BNK.getbank_id(),finStartDate,todate,typeofbanks[j],"");
  			if(bankopeningbal >0 || bankopeningbal<0){
  %> 
   <tr>
    <td height="28" style="color:#F00; font-weight:bold;" colspan="4"><%=BNK.getbank_name() %> <%=typeofbanks[j]%></td>
    
    <td height="28" align="center" style="color:#F00; font-weight:bold;"><%=DF.format(bankopeningbal) %></td>
    <td height="28" align="center" style="color:#F00; font-weight:bold;">0.00</td>
  </tr>
  <%
  bankopeningbaldebit=bankopeningbaldebit+bankopeningbal;
bankopeningbal=0.0;
}}} }

%>

<tr>
	<td style="font-weight: bold;font-size: 15px;color: #E18D25;text-align: right;" colspan="4">Total  &nbsp; &nbsp;</td>
	<td align="left" style="font-weight: bold;font-size: 15px;color: #E18D25;">Rs.<%=df.format(totalDebits + bankopeningbaldebit) %>  /-</td>
	<td align="left" style="font-weight: bold;font-size: 15px;color: #E18D25;">Rs.<%=df.format(totalCredits + bankopeningbalcredit) %>  /-</td>
</tr>
	</table>
	
		
			<%--if(amount>0){
					
		 <tr>
	<td style="font-weight: bold;font-size: 15px;color: #E18D25;text-align: right;" colspan="4">Total  &nbsp; &nbsp;</td>
	<td style="font-weight: bold;font-size: 15px;color: #E18D25;">Rs.<%=df.format(amount) %>  /-</td>
	</tr>
	<%}else { %>
	<tr>
	<td style="font-weight: bold;font-size: 15px;color: #E12530;text-align: center;" colspan="5">No Results To Display.</td>
	</tr>
	<%} %> --%>
</div>
</div>
</div>
</body>
</html>