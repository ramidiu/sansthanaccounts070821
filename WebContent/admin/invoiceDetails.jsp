<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.indentListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<jsp:useBean id="IND" class="beans.indent"></jsp:useBean>
<jsp:useBean id="PO" class="beans.purchaseorder"></jsp:useBean>
<jsp:useBean id="GOD" class="beans.godwanstock"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">

<div style="text-align: center;">REPORT DETAILS </div>
<%String id=request.getParameter("invID");
String reportType=request.getParameter("report");
employeesListing EMPL=new employeesListing();
vendorsListing VENL=new vendorsListing();
productsListing PRODL=new productsListing();
purchaseorderListing POL=new purchaseorderListing();
godwanstockListing GODL=new godwanstockListing();
indentListing INDL=new indentListing();
DecimalFormat DF=new DecimalFormat("0.00");
/* int totQty=0; */
double totQty=0.00;
double totAmt=0.00;
%>
<div class="clear"></div>
<div class="total-report">
<%if(reportType!=null &&reportType.equals("indent")){ %>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="5" class="bg-new">INDENT DETAIL REPORT</td></tr>
<tr>
<td>S.NO</td>
<td>PRODUCT-ID/NAME</td>
<td>QTY</td>
<td>VENDOR</td>
<td>ENTERED BY</td>
</tr>
<%
List INDET=INDL.getMindentsByIndentinvoice_id(id);
if(INDET.size()>0){ 
for(int i=0;i<INDET.size();i++){
	IND=(beans.indent)INDET.get(i);
	totQty=totQty+Integer.parseInt(IND.getquantity());
%>
<tr>
<td><%=i+1 %></td>
<td><%=IND.getproduct_id() %>/<%=PRODL.getProductsNameByCat1(IND.getproduct_id(), "")%></td>
<td><%=IND.getquantity() %></td>
<td><%=VENL.getMvendorsAgenciesName(IND.getvendor_id()) %></td>
<td><%=EMPL.getMemployeesName(IND.getemp_id()) %></td>
</tr>
<%}%>
<tr>
<td colspan="2"  style="text-align:right"><span>Grand Total</span></td>
<td><%=totQty %></td>
<td colspan="2"  style="text-align:right"></td>
</tr>
<%} %>
</table>
<%totQty=0;
}else if(reportType!=null &&reportType.equals("purchaseorder")){ %>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="5" class="bg-new">PURCHASE ORDER DETAIL REPORT</td></tr>
<tr>
<td>S.NO</td>
<td>PRODUCT-ID/NAME</td>
<td>QTY</td>
<td>VENDOR</td>
<td>ENTERED BY</td>
</tr>
<%
List PODET=POL.getMpurchaseorderBasedONPoINVID(id);
if(PODET.size()>0){ 
for(int i=0;i<PODET.size();i++){
	PO=(beans.purchaseorder)PODET.get(i);
	totQty=totQty+Integer.parseInt(PO.getquantity());
%>
<tr>
<td><%=i+1 %></td>
<td><%=PO.getproduct_id() %>/<%=PRODL.getProductsNameByCat1(PO.getproduct_id(), "")%></td>
<td><%=PO.getquantity() %></td>
<td><%=VENL.getMvendorsAgenciesName(PO.getvendor_id()) %></td>
<td><%=EMPL.getMemployeesName(PO.getemp_id()) %></td>
</tr>
<%}%>
<tr>
<td colspan="2"  style="text-align:right"><span>Grand Total</span></td>
<td><%=totQty %></td>
<td colspan="2"  style="text-align:right"></td>
</tr>
<%} %>

</table>
<%}else if(reportType!=null &&reportType.equals("mse")){  %>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="9" class="bg-new">MASTER STOCK ENTRY DETAIL REPORT</td></tr>
<tr>
<td>S.NO</td>
<td>PRODUCT-ID/NAME</td>
<td>QTY</td>
<td>PURCHASE RATE</td>
<td>VAT</td>
<td>TOTAL</td>
<td>VENDOR</td>
<td>ENTERED BY</td>
<td>VERIFIED BY</td>
</tr>
<%
List GODDET=GODL.getInvoiceDetails(id);
if(GODDET.size()>0){ 
for(int i=0;i<GODDET.size();i++){
	GOD=(beans.godwanstock)GODDET.get(i);
	/* totQty=totQty+Integer.parseInt(GOD.getquantity()); */
	totQty=totQty+Double.parseDouble(GOD.getquantity());
	totAmt=totAmt+(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100)));
%>
<tr>
<td><%=i+1 %></td>
<td><%=GOD.getproductId() %>/<%=PRODL.getProductsNameByCat1(GOD.getproductId(), "")%></td>
<td><%=GOD.getquantity() %></td>
<td><%=GOD.getpurchaseRate() %></td>
<td><%=GOD.getvat() %></td>
<td><%=DF.format(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100))) %></td>
<td><%=VENL.getMvendorsAgenciesName(GOD.getvendorId()) %></td>
<td><%=EMPL.getMemployeesName(GOD.getemp_id()) %></td>
<td><%=EMPL.getMemployeesName(GOD.getCheckedby()) %></td>
</tr>
<%}%>
<tr>
<td colspan="2"  style="text-align:right"><span>Grand Total</span></td>
<td><%=totQty %></td>
<td colspan="2"></td>
<td><%=DF.format(totAmt) %></td>
<td colspan="3"  style="text-align:right"></td>
</tr>
<%} %>

</table>
<%} %>
</div>
</div>
</div>
</body>
</html>