<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.util.List"   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Vendor</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif; font-size:16px;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../js/jquery-1.8.2.js"></script>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="../css/demos.css" />
<script>
$(function() {
	$( "#join_date" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

function validate(){
	
}
function combochange(denom,desti,jsppage) {
var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value; 
document.getElementById(desti).options.length = 0;
var sda1 = document.getElementById(desti);
$.post(jsppage,{ id: com_id } ,function(data)
	{
var where_is_mytool=data;
//alert(where_is_mytool);
var mytool_array=where_is_mytool.split("\n");
var ab=mytool_array.length-5;
//alert(mytool_array.length);
for(var i=0;i<mytool_array.length;i++)
{
if(mytool_array[i] !="")
{
//alert (mytool_array[i]);
var y=document.createElement('option');
var val_array=mytool_array[i].split(":");

			y.text=val_array[1];
			y.value=val_array[0];
			try
			{
			sda1.add(y,null);
			}
			catch(e)
			{
			sda1.add(y);
			}
}
}
});
} 
</script>


</head>
<jsp:useBean id="COU" class="beans.counters"/>
<body>
<div class="vender-details">
<%if( request.getParameter("id")!=null ){ %>
<form method="POST" action="counters_Update.jsp" onsubmit="return validate();">
 <%
String counter_id=request.getParameter("id");
mainClasses.countersListing COU_CL = new mainClasses.countersListing();
List COU_List=COU_CL.getMcounters(counter_id);
for(int i=0; i < COU_List.size(); i++ ){
COU=(beans.counters)COU_List.get(i);%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Create New Counters</span></td>
</tr>
<tr>
<td width="50%">
<table width="100%" cellpadding="0" cellspacing="0">

<tr>
<td>Counter_code</td>
<td>Counter_name</td>
<td>Role</td>
</tr>
<tr>
<td><input type="hidden" name="counter_id" id="counter_id" value="<%=COU.getcounter_id() %>" />
<input type="text"  name="counter_code" id="counter_code" value="<%=COU.getcounter_code()%>" /></td>
<td><input type="text"  name="counter_name" id="counter_name" value="<%=COU.getcounter_name() %>"  /></td>
<td><input type="text" name="Role" id="Role" value="<%=COU.getextra1() %>"  /></td>
</tr>


</table>
</td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

<tr>
<td colspan="3" align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>
</tr>
</table>
<%} %>
</form>
<%} else{ %>
<form method="POST" action="counters_Insert.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Create New Counters</span></td>
</tr>
<tr>
<td width="50%">
<table width="100%" cellpadding="0" cellspacing="0">

<tr>
<td>Counter_code</td>
<td>Counter_name</td>
<td>Role</td>
</tr>
<tr>
<td><input type="text"  name="counter_code" id="counter_code"></td>
<td><input type="text"  name="counter_name" id="counter_name"></td>
<td><input type="text" name="Role" id="Role"></td>
</tr>


</table>
</td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
<%} %>
</div>
</body>
</html>