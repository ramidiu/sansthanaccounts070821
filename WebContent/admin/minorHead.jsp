<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->
<script>
function idValidate(){
	if($('#minorhead_id').val().trim()==""){
		$('#minorIdErr').show();
		$('#minorhead_id').focus();
		return false;
	}
	var id=$('#minorhead_id').val().trim();
	 var hid=$('#head_account_id').val().trim();
$.ajax({
	type:'post',
	url: 'IDValidate.jsp', 
	data: {
		Id : id,
		type : 'minorhead',
		hoid : hid
	},
	success: function(response) { 
		var msg=response.trim();
		$('#minorIdErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#minorhead_id').focus();
			return false;
		}
	}});
}

</script>
<script type="text/javascript">

function validate(){
	$('#headIdErr').hide();
	$('#MajorHeadErr').hide();
	$('#minorIdErr').hide();
	$('#headNameErr').hide();

	if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	}
	if($('#major_head_id').val().trim()==""){
		$('#MajorHeadErr').show();
		$('#major_head_id').focus();
		return false;
	}
 if($('#minorhead_id').val().trim()==""){
		$('#minorIdErr').show();
		$('#minorhead_id').focus();
		return false;
	}
	if($('#name').val().trim()==""){
		$('#headNameErr').show();
		$('#name').focus();
		return false;
	}
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="MIN" class="beans.minorhead"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"><%if(request.getParameter("id")!=null) {%>Edit minorhead<%}else{%>Create a new minor heads <%} %></div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
minorheadListing MINH_L=new minorheadListing();
if(request.getParameter("id")!=null) {
List MINHD_details=MINH_L.getminorhead(request.getParameter("id"));
MIN=(minorhead)MINHD_details.get(0);
%>
<form name="tablets_Update" method="post" action="minorhead_Update.jsp" onSubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="minorhead_id" id="minorhead_id" value="<%=MIN.getminorhead_id()%>"/>
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td><div class="warning" id="MajorHeadErr" style="display: none;">Please Provide  "major head".</div>Major Head*</td>
<td width="35%">
<div class="warning" id="minorIdErr" style="display: none;">Please Provide  "Minor Head Number".</div>
<div class="warning" id="dupErr" style="display: none;">Duplicate entry.Please provide another</div>
Minor Head No*</td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Minor Head Name".</div>
Minor Head Name*</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id" onChange="combochange('head_account_id','major_head_id','getMajorHeads.jsp')">
<option value="<%=MIN.gethead_account_id()%>"><%=HOA_L.getHeadofAccountName(MIN.gethead_account_id()) %></option>
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}}%>
</select></td>
<td><select name="major_head_id" id="major_head_id">
<option value="<%=MIN.getmajor_head_id()%>"><%=MajorHead_list.getmajorheadName(MIN.getmajor_head_id()) %></option>

</select></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value="<%=MIN.getminorhead_id() %>" readonly disabled="disabled"/></td>
<td><input type="text" name="name" id="name" value="<%=MIN.getname()%>" /></td>
</tr>

<tr>
<td>IncomeExpenditure</td>&nbsp;
<td>ReceiptsPayments</td>&nbsp;
<!-- <td>BalanceSheet</td>&nbsp;
<td>LedgerReport</td>&nbsp; -->
<td>TrailBalance</td>&nbsp;
</tr> 
<tr><%-- <%System.out.println("minor head...."+MIN.getextra1()+"...minor_head_id..."+MIN.getminorhead_id() ); %> --%>
<td><input type="checkbox" name="report" value="IE" <%if(MIN.getextra1().contains("IE")){%>checked<%}%>></td>&nbsp;
<td><input type="checkbox" name="report" value="RP" <%if(MIN.getextra1().contains("RP")){%>checked<%}%>></td>&nbsp;
<%-- <td><input type="checkbox" name="report" value="BS" <%if(MIN.getextra1().contains("BS")){%>checked<%}%>></td>&nbsp;
<td><input type="checkbox" name="report" value="LR" <%if(MIN.getextra1().contains("LR")){%>checked<%}%>></td>&nbsp; --%>
<td><input type="checkbox" name="report" value="TB" <%if(MIN.getextra1().contains("TB")){%>checked<%}%>></td>&nbsp;
</tr>

<tr>
<td colspan="3">
<textarea name="description" id="description" placeholder="Enter description"><%=MIN.getdescription()%></textarea></td>
</tr>
<tr> 
<td colspan="2"></td>
<td align="right"><input type="submit" value="Update" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<% } else{%>
<form name="tablets_Update" method="post" action="minorhead_Insert.jsp" onSubmit="return validate();">
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account*</td>
<td><div class="warning" id="MajorHeadErr" style="display: none;">Please select  "Major head".</div>Major head*</td>
<td width="35%">
<div class="warning" id="minorIdErr" style="display: none;">Please Provide  "Minor Head Number".</div>
<div class="warning" id="dupErr" style="display: none;">Duplicate entry.Please provide another</div>
Minor Head No*</td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Minor Head Name".</div>
Minor Head Name*</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id" onChange="combochange('head_account_id','major_head_id','getMajorHeads.jsp')">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><select name="major_head_id" id="major_head_id">
<option value="">Select major head</option>
</select></td>
<td><input type="text" name="minorhead_id" id="minorhead_id" value="" onBlur="idValidate();"/></td>
<td><input type="text" name="name" id="name" value="" /></td>
</tr>
<tr>
<td>IncomeExpenditure</td>&nbsp;
<td>ReceiptsPayments</td>&nbsp;
<!-- <td>BalanceSheet</td>&nbsp;
<td>LedgerReport</td>&nbsp; -->
<td>TrailBalance</td>&nbsp;
</tr> 
<tr>
<td><input type="checkbox" name="report" value="IE"></td>&nbsp;
<td><input type="checkbox" name="report" value="RP"></td>&nbsp;
<!-- <td><input type="checkbox" name="report" value="BS"></td>&nbsp;
<td><input type="checkbox" name="report" value="LR"></td>&nbsp; -->
<td><input type="checkbox" name="report" value="TB"></td>&nbsp;
</tr>

<tr>
<td colspan="3">
<textarea name="description" id="description" placeholder="Enter description"></textarea></td>
</tr>
<tr> 
<td colspan="2"></td>
<td align="right"><input type="submit" value="Create" class="click" style="border:none;"/></td>

</tr>
</table>
</form>
<%} %>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%
List<minorhead> Min_list=MINH_L.getminorhead();
if(Min_list.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

<div id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="7"><table width="100%" cellpadding="0" cellspacing="0" class="yourID">
<tr>
<td class="bg" width="3%" align="center">S.No.</td>
<td class="bg" width="20%" align="center">Minor Head Name</td>
<td class="bg" width="20%" align="center">Description </td>
<td class="bg" width="19%" align="center">Major Head </td>
<td class="bg" width="10%" align="center">Head of account</td>
<td class="bg" width="10%" align="center">Edit</td>
<td class="bg" width="10%" align="center">Delete</td>
</tr>
</table></td></tr>
<%for(int i=0; i < Min_list.size(); i++ ){
	MIN=(minorhead)Min_list.get(i); %>
<tr>
<td  width="3%" align="center"><%=MIN.getminorhead_id()%></td>
<td  width="20%" align="center"><span><%=MIN.getname()%></span></td>
<td  width="20%" align="center"><%=MIN.getdescription()%></td>
<td  width="19%" align="center"><%=MajorHead_list.getmajorheadName(MIN.getmajor_head_id())%></td>
<td  width="10%" align="center"><%=HOA_L.getHeadofAccountName(MIN.gethead_account_id())%></td>
<td  width="10%"align="center"><a href="adminPannel.jsp?page=minorHead&id=<%=MIN.getminorhead_id()%>">Edit</a></td>
<td  width="10%" align="center"><a href="minorhead_Delete.jsp?Id=<%=MIN.getminorhead_id()%>">Delete</a></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no minor heads</h1></div>
<%}%>
</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
<script>
window.onload=function a(){
	combochange('head_account_id','major_head_id','getMajorHeads.jsp')
	};
</script>
