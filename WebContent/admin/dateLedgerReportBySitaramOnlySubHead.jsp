<%@page import="beans.subhead"%>
<%@page import="beans.customerpurchases"%>
<%@page import="beans.bankbalance"%>
<%@page import="mainClasses.ReportesListClass"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.productexpensesService"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
    		
        });
    </script>
        <script type="text/javascript">
    function formsubmit(){
    	if($( "#major_head_id" ).val()!=""){    		
        	document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReportBySitaram&majrhdid="+$( '#major_head_id' ).val();
        	document.getElementById("departmentsearch").submit();
    		    	} else if($( "#minor_head_id" ).val()!=""){
    		    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReportBySitaram&subhid="+$( '#minor_head_id' ).val();
    		        	document.getElementById("departmentsearch").submit();
    		    	}  else{    	
    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReport";
    	document.getElementById("departmentsearch").submit();
    	}
    }</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
function showDetails(id){
	  $("#"+id).toggle();
	
}
</script>

    
<script>

$(function() {
	$( "#fromdate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#todate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#todate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromdate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

	function datepickerchange()
	{
		var finYear = $("#finYear").val();
		var yr = finYear.split('-');
		var startDate = yr[0]+",04,01";
		var endDate = parseInt(yr[0])+1+",03,31";
		
		
		$('#fromdate').datepicker('option', 'minDate', new Date(startDate));
		$('#fromdate').datepicker('option', 'maxDate', new Date(endDate));
		$('#todate').datepicker('option', 'minDate', new Date(startDate));
		$('#todate').datepicker('option', 'maxDate', new Date(endDate));
	}
	
	$(document).ready(function(){
		datepickerchange();
	}); 
</script>

</head>

		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="PRDEXP" class="beans.productexpenses"></jsp:useBean>
				<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		
<%

response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<%

	headofaccountsListing HOA_L=new headofaccountsListing();
	mainClasses.subheadListing SUBH_L=new mainClasses.subheadListing();
	
	
	
	
	String typeserch = request.getParameter("typeserch");
	String fromdate = request.getParameter("fromdate");
	String todate = request.getParameter("todate");
	String subheadId = request.getParameter("subheadId");
	String minorheadId = request.getParameter("minorheadId");
	String majorHeadId = request.getParameter("majorHeadId");
	String hod = request.getParameter("hod");
	String cumltv = "";
	if(request.getParameter("cumltv")!=null && !request.getParameter("cumltv").trim().equals("")){
		cumltv = request.getParameter("cumltv");
	}
	String finYr = request.getParameter("finYr");
	
	List subhdlist = SUBH_L.getSubheadListMinorhead(minorheadId);

%>

<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">
<div class="vendor-list">

<form action="adminPannel.jsp" method="get">
<input type="hidden" name="page" value="dateLedgerReportBySitaramOnlySubHead" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_table">
<tbody><tr>
<td style="width:14%;">Select Financial Year</td>
<td style="width:14%;"><div class="warning" id="fromdateErr" style="display: none;">Please Provide  "From Date".</div>From Date</td>
<td style="width:14%;"><div class="warning" id="todateErr" style="display: none;">Please Provide  "To Date".</div>To Date</td>
<td style="width:14%;"><div class="warning" id="MinorHeadErr" style="display: none;">Please select  "Sub head".</div>Sub head</td>
</tr>
<tr>
	<td style="width:14%;">
			<select name="finYr" id="finYear" Onchange="datepickerchange();" style="width:90%;">
				<option value="2019-20" <%if(request.getParameter("finYr") != null && request.getParameter("finYr").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option>
				<option value="2019-19" <%if(request.getParameter("finYr") != null && request.getParameter("finYr").equals("2018-19")) {%> selected="selected"<%} %>>2018-2019</option>
				<option value="2017-18" <%if(request.getParameter("finYr") != null && request.getParameter("finYr").equals("2017-18")) {%> selected="selected"<%} %>>2017-2018</option>
				<option value="2016-17" <%if(request.getParameter("finYr") != null && request.getParameter("finYr").equals("2016-17")) {%> selected="selected"<%} %>>2016-2017</option>
				<option value="2015-16" <%if(request.getParameter("finYr") != null && request.getParameter("finYr").equals("2015-16")) {%>selected="selected"<%} %>>2015-2016</option>
			</select>
	</td>
	<td style="width:14%;"> 
		<input type="text" name="fromdate" id="fromdate" readonly="readonly" value="<%=fromdate %>" ><!-- <img class="ui-datepicker-trigger" src="../images/calendar-icon.png" alt="Select Date" title="Select Date"> -->
	</td>
	<td style="width:14%;">
		<input type="text" name="todate" id="todate" readonly="readonly" value="<%=todate %>" ><!-- <img class="ui-datepicker-trigger" src="../images/calendar-icon.png" alt="Select Date" title="Select Date"> -->
	 </td>
	
	<td style="width:14%;">
	
	<input type="hidden" name="typeserch" value="<%=typeserch %>">
	<input type="hidden" name="hod" value="<%=hod %>">
	<input type="hidden" name="majorHeadId" value="<%=majorHeadId %>">
	<input type="hidden" name="minorheadId" value="<%=minorheadId %>">
	<input type="hidden" name="typeserch" value="<%=typeserch %>">
	<input type="hidden" name="typeserch" value="<%=typeserch %>">
	
		<select name="subheadId" id="subheadId" style="width:95%;">
			<option value="" selected="selected">Select Sub Head</option>
			<%
				for(int i = 0; i < subhdlist.size(); i++){
					
					subhead subhead = (subhead) subhdlist.get(i);
					
					%>
					
					<option value="<%=subhead.getsub_head_id() %>" <%if(subhead.getsub_head_id().trim().equals(subheadId) ) {%> selected="selected" <%} %> ><%=subhead.getsub_head_id() %> <%=subhead.getname() %></option>
					
					<%
				}
			%>
		</select>
	</td>
</tr>
<tr>
<td style="width:14%;">Select Cumulative Type</td>
</tr>
<tr>
	<td style="width:14%;">
	<select name="cumltv" id="cumltv">
		<option value="noncumulative">NON CUMULATIVE</option>
		<option value="cumulative"  <%if(cumltv.equals("cumulative")){ %> selected="selected" <%} %> >CUMULATIVE</option>
	</select>
	</td>
	<td><input type="submit" value="SEARCH" class="click"  style="margin-top:20px;"></td>
</tr>
</tbody></table>
</form>
				<div class="icons">
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
<div class="clear"></div>
<div class="list-details">
<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tbody><tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hod) %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Day Wise Ledger Report Of Sub Head</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date  <%=fromdate %> TO <%=todate %> </td></tr>
						<tr>
						<td colspan="7">
<table width="100%" cellpadding="0" cellspacing="0" border="0">

	<tbody>
						
						
						
			<%			
			
			if(subheadId != null && !subheadId.trim().equals("")){
			
			SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
			
			DecimalFormat df = new DecimalFormat("#,###.00");
			
			String cashtype="online pending";
			String cashtype1="othercash";
			String cashtype2="offerKind";
			String cashtype3="journalvoucher";		
			
			double credit=0.0;
			double debit=0.0;
			double credit1=0.0;
			double debit1=0.0;
			double balance=0.0;
			
			int k = 0;
		
				ReportesListClass reportesListClass = new ReportesListClass();
				
				List<bankbalance> debitAmountOpeningBalList = null;
				List<bankbalance> creditAmountOpeningBalList = null;
				
				if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
				{
					debitAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hod, "", "", subheadId, finYr,"Assets", "debit", "createdDate", "date");
					creditAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hod, "", "", subheadId, finYr,"Liabilites", "credit", "createdDate", "date");
				} 
				
				List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hod, "", "", subheadId, fromdate, todate, "date", "date", "not a loan bank");
				List<banktransactions> otherAmountListForLoanBank = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hod, "", "", subheadId, fromdate, todate, "date", "date", "loan bank");
				
				
				List<productexpenses> productexpensesList = reportesListClass.getSumOfAmountFromProductexpenses(hod, "", "", subheadId, fromdate+" 00:00:00", todate+" 23:59:59", "p.date", "date");
		// 		List<productexpenses> productexpensesJVDebit =	reportesListClass.getJVAmountsFromproductexpenses(hod, majorHeadId, minorheadId, subheadId, cashtype3, date+" 00:00:00", date+" 23:59:59", "pe.entry_date", "date", "debit");
				List<customerpurchases> customerpurchasesJV = reportesListClass.getJVAmountsFromcustomerpurchases(hod, "", "", subheadId, fromdate+" 00:00:00", todate+" 23:59:59", cashtype3, "date", "date");
				
				// jv amount from productexpensec withbank and withloanbank and withvendor 
				List<productexpenses> productexpensesJV =	reportesListClass.getJVAmountsFromproductexpenses(hod, "", "", subheadId, cashtype3, fromdate+" 00:00:00", todate+" 23:59:59", "pe.entry_date", "date", "credit");
				List<customerpurchases> DollarAmt = reportesListClass.getSumOfDollorAmountfromcustomerpurchases(hod, "", "", subheadId, fromdate+" 00:00:00", todate+" 23:59:59", cashtype1, "cp.date", "date");
				List<customerpurchases> cardCashChequeJvAmountList = reportesListClass.getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(hod, "", "", subheadId, fromdate+" 00:00:00", todate+" 23:59:59", "cp.date", "date");
		// 		List<customerpurchases> jvCreditAmountFromCustomerpurchasesList = reportesListClass.getJVCreditAmountFromCustomerPurchases(hod, majorHeadId, minorheadId, subheadId, date+" 00:00:00", date+" 23:59:59", "cp.date", "date");
				
				double debitAmountOpeningBal = 0.0;
				double creditAmountOpeningBal = 0.0;
				if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
					for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
						bankbalance bankbalance = debitAmountOpeningBalList.get(j);
						if(bankbalance != null && bankbalance.getExtra10() != null ){
							debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
						}
					}
				}
				if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
					for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
						bankbalance bankbalance = creditAmountOpeningBalList.get(j);
						if(bankbalance != null && bankbalance.getExtra10() != null ){
							creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
						}
					}
				}
				
						if(debitAmountOpeningBal > 0){
							debit = debitAmountOpeningBal;
				%>
					 <tr style="position: relative;">
					 	<td style="font-weight: bold;"><%=++k%></td>
					 	<td style="font-weight: bold;">&nbsp; <%=debitAmountOpeningBalList.get(0).getExtra2() %></td>
					 	<td style="font-weight: bold;">DEBIT OPENING BALANCE FOR <%=finYr%></td>
					 	<td style="font-weight: bold;">&nbsp; <%=debitAmountOpeningBalList.get(0).getExtra3() %></td>
					 	<td style="font-weight: bold;">&nbsp; <%=debitAmountOpeningBalList.get(0).getExtra4() %></td>
					    <td style="font-weight: bold;"><%=df.format(debitAmountOpeningBal)%></td>
					    <td style="font-weight: bold;">&nbsp;</td>
				     </tr>
				     <%} 
						if(creditAmountOpeningBal > 0){
						credit = creditAmountOpeningBal;	
						%>
				     <tr style="position: relative;">
					 	<td style="font-weight: bold;"><%=++k%></td>
					 	<td style="font-weight: bold;">&nbsp; <%=creditAmountOpeningBalList.get(0).getExtra2() %></td>
					 	<td style="font-weight: bold;">CREDIT OPENING BALANCE FOR <%=finYr%></td>
					 	<td style="font-weight: bold;">&nbsp; <%=creditAmountOpeningBalList.get(0).getExtra3() %></td>
					 	<td style="font-weight: bold;">&nbsp; <%=creditAmountOpeningBalList.get(0).getExtra4() %></td>
						<td style="font-weight: bold;">&nbsp;</td>
					    <td style="font-weight: bold;"><%=df.format(creditAmountOpeningBal)%></td>
				     </tr>
				     <%}
						
						
						if((customerpurchasesJV != null && customerpurchasesJV.size() > 0) || (cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0) || ( DollarAmt != null && DollarAmt.size() > 0)){
							
							%>
							
							<tr>
								<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
								<td class="bg" width="15%" style="font-weight: bold;">INVOICE DATE</td>
								<td class="bg" width="5%" style="font-weight: bold;">MAJOR HEAD</td>
								<td class="bg" width="5%" style="font-weight: bold;">MINOR HEAD</td>
								<td class="bg" width="8%" style="font-weight: bold;">INVOICE.NO.</td>
								<td class="bg" width="20%" style="font-weight: bold;">ACCOUNT</td>
								<td class="bg" width="20%" style="font-weight: bold;">NARRATION</td>
								<td class="bg" width="10%" style="font-weight: bold;">QTY</td>
								<td class="bg" width="20%" style="font-weight: bold;">DEBIT</td>
								<td class="bg" width="20%" style="font-weight: bold;">CREDIT</td>
							</tr>
							
							
							<%
							if(customerpurchasesJV != null && customerpurchasesJV.size() > 0 ){
								debit1 = 0.0;
								for(int i = 0 ; i < customerpurchasesJV.size(); i++){
									customerpurchases customerpurchases2 = customerpurchasesJV.get(i);
									if(customerpurchases2 != null && customerpurchases2.gettotalAmmount() != null && !customerpurchases2.gettotalAmmount().equals("")){
										debit1 = debit1 + Double.parseDouble(customerpurchases2.gettotalAmmount());
										
										%>
											<tr>
												<td><%=i+1 %></td>
												<td><%=customerpurchases2.getExtra20() %></td>
												<td><%=customerpurchases2.getextra6()%></td>
												<td><%=customerpurchases2.getextra7()%></td>
												<td><%=customerpurchases2.getvocharNumber() %></td>
												<td><%=customerpurchases2.getcash_type() %></td>
												<td><%=customerpurchases2.getextra2() %></td>
												<td><%=customerpurchases2.getquantity() %></td>
												<td><%=df.format(Double.parseDouble(customerpurchases2.gettotalAmmount())) %></td>
												<td>0.00</td>
											</tr>
										<%
									}
								}
							}
							
							if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0 ){
								credit1 = 0.0;
								for(int i = 0 ; i < cardCashChequeJvAmountList.size(); i++){
									customerpurchases customerpurchases2 = cardCashChequeJvAmountList.get(i);
									if(customerpurchases2 != null && customerpurchases2.gettotalAmmount() != null && !customerpurchases2.gettotalAmmount().equals("")){
										
										credit1 = credit1 + Double.parseDouble(customerpurchases2.gettotalAmmount());
										
										
										%>
											<tr>
												<td><%=i+1 %></td>
												<td><%=customerpurchases2.getExtra20() %></td>
												<td><%=customerpurchases2.getextra6()%></td>
												<td><%=customerpurchases2.getextra7()%></td>
												<td><%=customerpurchases2.getbillingId() %></td>
												<td><%=customerpurchases2.getcash_type() %></td>
												<td><%=customerpurchases2.getextra2() %></td>
												<td><%=customerpurchases2.getquantity() %></td>
												<td>0.00</td>
												<td><%=df.format(Double.parseDouble(customerpurchases2.gettotalAmmount())) %></td>
											</tr>
										<%
									}
								}
							}
							
							if(DollarAmt != null && DollarAmt.size() > 0){
								double tempcredit = 0.0;
								for(int i = 0 ; i < DollarAmt.size(); i++){
									customerpurchases customerpurchases2 = DollarAmt.get(i);
									if(customerpurchases2 != null && customerpurchases2.gettotalAmmount() != null && !customerpurchases2.gettotalAmmount().equals("")){
										credit1 = credit1 + Double.parseDouble(customerpurchases2.gettotalAmmount());
										
										%>
											<tr>
												<td><%=i+1 %></td>
												<td><%=customerpurchases2.getExtra20() %></td>
												<td><%=customerpurchases2.getextra6()%></td>
												<td><%=customerpurchases2.getextra7()%></td>
												<td><%=customerpurchases2.getbillingId() %></td>
												<td><%=customerpurchases2.getcash_type() %></td>
												<td><%=customerpurchases2.getextra2() %></td>
												<td><%=customerpurchases2.getquantity() %></td>
												<td>0.00</td>
												<td><%=df.format(Double.parseDouble(customerpurchases2.gettotalAmmount())) %></td>
											</tr>
										<%
									}
								}
							}
							debit = debit + debit1;
							credit = credit + credit1;
							%>
							<tr style="border: 1px solid #000;"><td colspan="8" align="center"><b> TOTAL </b></td> <td colspan="1"><b><%= df.format(debit1) %></b></td> <td colspan="1"><b><%=df.format(credit1) %></b></td> </tr>
							<%
						}
						
						
						
						if((productexpensesList != null && productexpensesList.size() > 0) || (productexpensesJV != null && productexpensesJV.size() > 0) || (otherAmountList != null && otherAmountList.size() > 0) || (otherAmountListForLoanBank != null && otherAmountListForLoanBank.size() > 0)){ 
							debit1 = 0.0;
							credit1 = 0.0;
							%>
							<tr>
								<td class="bg" width="5%" style="font-weight: bold;" colspan="1">S.NO.</td>
								<td class="bg" width="5%" style="font-weight: bold;" colspan="1">MAJOR HEAD</td>
								<td class="bg" width="5%" style="font-weight: bold;" colspan="1">MINOR HEAD</td>
								<td class="bg" width="15%" style="font-weight: bold;" colspan="1">Bill Date</td>
								<td class="bg" width="15%" style="font-weight: bold;" colspan="1">Bill Id</td>
								<td class="bg" width="5%" style="font-weight: bold;" colspan="3">NARRATION</td>
								<td class="bg" width="20%" style="font-weight: bold;" colspan="1">DEBIT</td>
								<td class="bg" width="20%" style="font-weight: bold;" colspan="1">CREDIT</td>
							</tr>
							<%
							
							if(productexpensesList != null && productexpensesList.size() > 0){
								for(int i = 0 ; i < productexpensesList.size(); i++){
									productexpenses productexpenses = productexpensesList.get(i);
									if(productexpenses != null && productexpenses.getexpinv_id() != null && !productexpenses.getexpinv_id().equals("")){
										debit1 = debit1 + Double.parseDouble(productexpenses.getamount());
										
										%>
										<tr>
											<td><%=i+1 %></td>
											<td><%=productexpenses.getExtra10() %></td>
											<td><%=productexpenses.getmajor_head_id() %></td>
											<td><%=productexpenses.getminorhead_id() %></td>
											<td colspan="1"><%=productexpenses.getexpinv_id() %></td>
											<td colspan="3"><%=productexpenses.getnarration() %></td>
											<td colspan="1"><%=df.format(Double.parseDouble(productexpenses.getamount())) %></td>
											<td colspan="1">0.00</td>
										</tr>
										<%
									}
								}
							}
							
							if(productexpensesJV != null && productexpensesJV.size() > 0){
								for(int i = 0 ; i < productexpensesJV.size(); i++){
									productexpenses productexpenses = productexpensesJV.get(i);
									if(productexpenses != null && productexpenses.getexpinv_id() != null && !productexpenses.getexpinv_id().equals("")){
										credit1 = credit1 + Double.parseDouble(productexpenses.getamount());
										
										%>
										<tr>
											<td><%=i+1 %></td>
											<td><%=productexpenses.getExtra10() %></td>
											<td><%=productexpenses.getmajor_head_id() %></td>
											<td><%=productexpenses.getminorhead_id() %></td>
											<td colspan="1"><%=productexpenses.getextra5() %></td>
											<td colspan="3"><%=productexpenses.getnarration() %></td>
											<td colspan="1">0.00</td>
											<td colspan="1"><%=df.format(Double.parseDouble(productexpenses.getamount())) %></td>
										</tr> 
										<%
									}
								}
							}
							
							if(otherAmountList != null && otherAmountList.size() > 0){
								double tempCredit = 0.0;
								for(int i = 0 ; i < otherAmountList.size(); i++){
									banktransactions banktransactions = otherAmountList.get(i);
									if(banktransactions != null && banktransactions.getbanktrn_id() != null && !banktransactions.getbanktrn_id().trim().equals("")){
										credit1 = credit1 + Double.parseDouble(banktransactions.getamount());
										
										%>
										<tr>
											<td><%=i+1 %></td>
											<td><%=banktransactions.getdate() %></td>
											<td><%=banktransactions.getMajor_head_id() %></td>
											<td><%=banktransactions.getMinor_head_id() %></td>
											<td colspan="1"><%=banktransactions.getbanktrn_id() %></td>
											<td colspan="3"><%=banktransactions.getnarration() %></td>
											<td colspan="1">0.00</td>
											<td colspan="1"><%=df.format(Double.parseDouble(banktransactions.getamount())) %></td>
										</tr>   
										<%
									}
								}
							}
							
							if(otherAmountListForLoanBank != null && otherAmountListForLoanBank.size() > 0){
								double tempCredit = 0.0;
								for(int i = 0 ; i < otherAmountListForLoanBank.size(); i++){
									banktransactions banktransactions = otherAmountListForLoanBank.get(i);
									if(banktransactions != null && banktransactions.getbanktrn_id() != null && !banktransactions.getbanktrn_id().trim().equals("")){
										debit1 = debit1 + Double.parseDouble(banktransactions.getamount());
										
										%>
										<tr>
											<td><%=i+1 %></td>
											<td><%=banktransactions.getdate() %></td>
											<td><%=banktransactions.getMajor_head_id() %></td>
											<td><%=banktransactions.getMinor_head_id() %></td>
											<td colspan="1"><%=banktransactions.getbanktrn_id() %></td>
											<td colspan="3"><%=banktransactions.getnarration() %></td>
											<td colspan="1"><%=df.format(Double.parseDouble(banktransactions.getamount())) %></td>
											<td colspan="1">0.00</td>
										</tr>   
										<%
									}
								}
							}
							
							debit = debit + debit1;
							credit = credit + credit1;
							
							%>
							<tr style="border: 1px solid #000;"><td colspan="8" align="center" ><b> TOTAL </b></td> <td colspan="1"><b><%= df.format(debit1) %></b></td> <td colspan="1"><b><%=df.format(credit1) %></b></td> </tr>
							<%
						}
				    	 %> 
				    	 
				    	 
				    	<tr style="border: 1px solid #000;"><td colspan="8" align="center"><b> TOTAL AMOUNT </b></td> <td colspan="1"><b><%= df.format(debit) %></b></td> <td colspan="1"><b><%=df.format(credit) %></b></td> </tr>
				    	
					<%} %>	
					   </tbody>   
					   </table>
						</td>
						</tr>
					</tbody></table>
			</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}
		else{
	response.sendRedirect("index.jsp");
} %>
