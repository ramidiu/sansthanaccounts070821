<%@page import="mainClasses.banktransactionsListing2"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<jsp:useBean id="BKT" class="beans.banktransactionsService"/>
<jsp:useBean id="BKT2" class="beans.banktransactionsService2"/>
<%
mainClasses.bankdetailsListing BAN= new mainClasses.bankdetailsListing();
String banktx_id=request.getParameter("banktx_id");
String bank_id=request.getParameter("bank_id");
String name="";
String narration=request.getParameter("Narration");
String prevamount=request.getParameter("prevamount");
String amount=request.getParameter("amount");
String type=request.getParameter("category");
String ptype=request.getParameter("prevcategory");
String amtDeposit=request.getParameter("amtDeposited");
String createdBy=session.getAttribute("adminId").toString();
String extra1=request.getParameter("extra1");
String empid=request.getParameter("employeeId");
String date1=request.getParameter("date");
String depositType=request.getParameter("depositType");
String HOA = request.getParameter("head_account_id");

String majorHead = request.getParameter("major_head_id");
String minorHead = request.getParameter("minor_head_id");
String product1 = request.getParameter("product");

String major_head_id[] = {"", ""};
String minor_head_id[] = {"", ""};
String product[] = {"", ""};


if(majorHead != null && majorHead.split(" ").length > 1){
	major_head_id = majorHead.split(" ");
}

if(minorHead != null && minorHead.split(" ").length > 1){
	minor_head_id = minorHead.split(" ");
}


if(product1 != null && product1.split(" ").length > 1){
	product = product1.split(" ");
}



double bankpramount = 0.0;
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");

DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat datetime= new SimpleDateFormat("HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));

//String date=(dateFormat.format(c1.getTime())).toString();
//String time=date1+" "+(datetime.format(c1.getTime())).toString();

String date=request.getParameter("date");
String hhmmss=request.getParameter("hhmmss");
String formattedDate=date+" "+hhmmss;
%>
<jsp:setProperty name="BKT" property="banktrn_id" value="<%=banktx_id%>"/>
<jsp:setProperty name="BKT" property="name" value="<%=name%>"/>
<jsp:setProperty name="BKT" property="narration" value="<%=narration%>"/>
<jsp:setProperty name="BKT" property="date" value="<%=formattedDate%>"/>
<jsp:setProperty name="BKT" property="amount" value="<%=amount%>"/>
<jsp:setProperty name="BKT" property="type" value="<%=type%>"/>
<jsp:setProperty name="BKT" property="createdBy" value="<%=createdBy%>"/>
<jsp:setProperty name="BKT" property="extra1" value="<%=extra1%>"/>
<jsp:setProperty name="BKT" property="extra2" value="<%=empid%>"/>
<jsp:setProperty name="BKT" property="extra4" value="<%=amtDeposit%>"/>

<%BKT.update();%><%=BKT.geterror()%>

<jsp:setProperty name="BKT" property="banktrn_id" value="<%=banktx_id%>"/>
<%BKT.updateByCondition(depositType,HOA,major_head_id[0],minor_head_id[0],product[0]);%><%=BKT.geterror()%>
<%
banktransactionsListing2 BKTL2 = new banktransactionsListing2();
String count = BKTL2.getCountOfRecords(banktx_id);
if(Integer.parseInt(count) > 0)
{%>
	<jsp:setProperty name="BKT2" property="banktrn_id" value="<%=banktx_id%>"/>
	<jsp:setProperty name="BKT2" property="name" value="<%=name%>"/>
	<jsp:setProperty name="BKT2" property="narration" value="<%=narration%>"/>
	<jsp:setProperty name="BKT2" property="date" value="<%=formattedDate%>"/>
	<jsp:setProperty name="BKT2" property="amount" value="<%=amount%>"/>
	<jsp:setProperty name="BKT2" property="type" value="<%=type%>"/>
	<jsp:setProperty name="BKT2" property="createdBy" value="<%=createdBy%>"/>
	<jsp:setProperty name="BKT2" property="extra1" value="<%=extra1%>"/>
	<jsp:setProperty name="BKT2" property="extra2" value="<%=empid%>"/>
	<jsp:setProperty name="BKT2" property="extra4" value="<%=amtDeposit%>"/>

	<%BKT2.update();%><%=BKT2.geterror()%>
	
	<jsp:setProperty name="BKT" property="banktrn_id" value="<%=banktx_id%>"/>
	<%BKT2.updateByCondition(depositType,HOA,major_head_id[0],minor_head_id[0],product[0]);%><%=BKT.geterror()%>
<%}
%>
<%
String pettyCash=BAN.getPettyCashAmount(bank_id);
String bankamount=BAN.getMbankdetailsAmount(bank_id);
if(ptype.equals("deposit")){
	bankpramount=(Double.parseDouble(bankamount)-Double.parseDouble(prevamount));
}else if(ptype.equals("pettycash")){
	bankpramount=(Double.parseDouble(bankamount)+Double.parseDouble(prevamount));
	if(pettyCash!=null && !pettyCash.equals("")){
		pettyCash=""+(Double.parseDouble(pettyCash)-Double.parseDouble(prevamount));
	}else{
		pettyCash=prevamount;
	}%>
<jsp:useBean id="BNK" class="beans.bankdetailsService"/>
	<jsp:setProperty name="BNK" property="bank_id" value="<%=bank_id%>"/>
	<jsp:setProperty name="BNK" property="extra1" value="<%=pettyCash%>"/>
	<%-- <%BNK.update();%><%=BNK.geterror()%> --%>
	<%}
bankamount=String.valueOf(bankpramount);
%>
<jsp:useBean id="BNK1" class="beans.bankdetailsService"/>
<jsp:setProperty name="BNK1" property="bank_id" value="<%=bank_id%>"/>
<jsp:setProperty name="BNK1" property="total_amount" value="<%=bankamount%>"/>
<%-- <%BNK1.update();%><%=BNK1.geterror()%> --%>

<%
pettyCash=BAN.getPettyCashAmount(bank_id);
 bankamount=BAN.getMbankdetailsAmount(bank_id);
if(type.equals("deposit")){
	bankpramount=(Double.parseDouble(bankamount)+Double.parseDouble(amount));
}else if(type.equals("pettycash")){
	bankpramount=(Double.parseDouble(bankamount)-Double.parseDouble(amount));
	if(pettyCash!=null && !pettyCash.equals("")){
		pettyCash=""+(Double.parseDouble(pettyCash)+Double.parseDouble(amount));
	}else{
		pettyCash=amount;
	}%>
<jsp:useBean id="BNK2" class="beans.bankdetailsService"/>
	<jsp:setProperty name="BNK2" property="bank_id" value="<%=bank_id%>"/>
	<jsp:setProperty name="BNK2" property="extra1" value="<%=pettyCash%>"/>
	<%-- <%BNK2.update();%><%=BNK2.geterror()%> --%>
	<%}
bankamount=String.valueOf(bankpramount);
%>
<jsp:useBean id="BNK3" class="beans.bankdetailsService"/>
<jsp:setProperty name="BNK3" property="bank_id" value="<%=bank_id%>"/>
<jsp:setProperty name="BNK3" property="total_amount" value="<%=bankamount%>"/>
<%-- <%BNK3.update();%><%=BNK3.geterror()%> --%>
<%-- <script type="text/javascript">
window.parent.location="adminPannel.jsp?page=bankDetails&id=<%=bank_id%>";
</script> --%>
<%
response.sendRedirect("adminPannel.jsp?page=banktransactionsForm&txid="+banktx_id);
%>
