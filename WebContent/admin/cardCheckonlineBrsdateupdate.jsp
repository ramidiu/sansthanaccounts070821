<%@page import="beans.customerapplicationService"%>
<%@page import="beans.customerpurchases"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="beans.customerpurchasesService"%>
<%@page import="java.util.Date" %>
<%@page import="mainClasses.no_genaratorListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="CSP" class="beans.customerapplicationService"/> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>cardcheckonline</title>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
  <script>
    function datepickerchange()
  {
  	var finYear=$("#finYear").val();
  	var yr = finYear.split('-');
  	var startDate = yr[0]+",04,01";
  	var endDate = parseInt(yr[0])+1+",03,31";
  	
  	$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
  	$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
  	$('#toDate').datepicker('option', 'minDate', new Date(startDate));
  	$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
  } 
    function validate()	{
    	 var loopcount=document.getElementById("listcount").value;
    	 // alert("no of records==========>"+loopcount);
 		/*  var checkcount=$("[name='check']:checked").length;
    	if(checkcount==0)	{
    	alert("please check atleast one checkbox");
        return false;
    	}  */
    	/* for(var i=0; i<loopcount;i++){
    	     var ischeck=document.getElementById("ch"+i).checked;
    		//	alert("is check==============>"+ischeck);
    	     if(ischeck){
    	    	//  alert("if checked");
    	        	var dt= document.getElementById("depositdate"+i).value;
    	        //	alert("date value=====>"+dt);
    	        	if(dt.trim()=="")	{
    	        		alert("Slect Brs Date for checkbox");
    	        		$('#depositdate'+i).focus();
    	        		return false;
    	        	}
    	          } 
    	      
    	} */
    }
    /* function checkValue(i){
    	var checkdate=document.getElementById("depositdate"+i).value;
    	if(checkdate==null || checkdate=="")	{
    		document.getElementById("depositdate"+i).focus();
    		return false;
    	}
 
    } */
    function clearDate(i)	{
    	document.getElementById("depositdate"+i).value="";
    	 document.getElementById("ch"+i).checked = false;
    }
    function validate2(i)	{
    	var date=document.getElementById("depositdate"+i).value;
    	if(date=="")	{
    		document.getElementById("depositdate"+i).focus();
    	}
    }
    function clearAll()	{
    	$('input:checkbox').prop('checked',false);
    	$('.form-controll').val("");
    }
  </script>
  
  <script>
  $(function() {
		$( "#fromDate" ).datepicker({
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: 'both',
			buttonImage: "../images/calendar-icon.png",
			buttonText: 'Select Date',
			 buttonImageOnly: true,
			dateFormat: 'yy-mm-dd',
			onSelect: function( selectedDate ) {
				$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
			}
		});
			$( "#toDate" ).datepicker({
			changeMonth: true,
			changeYear: true,
			numberOfMonths:1,
			showOn: 'both',
			buttonImage: "../images/calendar-icon.png",
			buttonText: 'Select Date',
			 buttonImageOnly: true,
			dateFormat: 'yy-mm-dd',
			onSelect: function( selectedDate ) {
				$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
			
			datepickerchange();
			
			/* $('#finYear').change(function (){
				alert($('#finYear').val());
			}) */
			
			$('.form-controll').datepicker({
				changeMonth: true,
				changeYear: true,
				numberOfMonths:1,
				showOn: 'both',
				buttonImage: "../images/calendar-icon.png",
				buttonText: 'Select Date',
				buttonImageOnly: true,
				dateFormat: 'yy-mm-dd',
			});
			/* $('#frmsubmit').submit( function (e) {
				var button=$('#sub');
				 button.prop('disabled', true);
			} */
	});
  </script>
  
</head>

  <style>
   table td.bg {
    background: #e3eaf3;
    font-size: 13px;
    color: #7490ac;
    padding: 5px 0 5px 5px;
    border-right: 1px solid #c0d0e4;
    border-bottom: 1px solid #c0d0e4;
}
  </style>
<body> 

<%

	if(session.getAttribute("adminId")!=null)	{
		session.setAttribute("hidden","1000");
		Calendar c1=Calendar.getInstance();
		DateFormat df2=new SimpleDateFormat("yyyy-MM-dd");
		String fromdate="";
		String todate="";
		int j=0;

		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		
		c1.set(Calendar.DATE, lastday);  
		todate=(df2.format(c1.getTime())).toString();
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		
		int firstday = c1.getActualMinimum(Calendar.DATE);
		
		c1.set(Calendar.DATE, firstday); 
		fromdate=(df2.format(c1.getTime())).toString();
		
		if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
									 fromdate=request.getParameter("fromDate");
								}
								if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
									todate=request.getParameter("toDate");
								}
								headofaccountsListing HOA_L=new headofaccountsListing();
								List headaclist=HOA_L.getheadofaccounts();%>
<div class="vendor-page">
<form action="adminPannel.jsp?page=cardCheckonlineBrsdateupdate" method="post"  style="padding-left: 70px;padding-top: 30px;">
<table width="80%" border="0" cellspacing="0" cellpadding="0" style="margin: 37px 31px;position: relative;top: 28px;">
<tbody><tr>
<td colspan="2"> Financial Year</td>
<td class="border-nn">&nbsp;&nbsp;From Date</td>
<td class="border-nn">To Date</td>
<td class="border-nn" colspan="2"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td class="border-nn">Payment Type</td>
</tr>			
<tr>
<td colspan="2">
<select name="finYear" id="finYear" onchange="datepickerchange();">
					<%@ include file="yearDropDown1.jsp" %>
					</select>
			</td> 
		<td class="border-nn">&nbsp;&nbsp;<input type="text" name="fromDate" id="fromDate" readonly="readonly"  value=<%=fromdate%> ></td>
		  		<td class="border-nn"><input type="text" name="toDate" id="toDate" readonly="readonly" value=<%=todate%> >
				<!-- <input type="hidden" name="page" value="trailBalanceReport_newBySitaram"> -->
				 </td>
<td colspan="2" class="border-nn">
<select name="head_account_id" id="head_account_id" onchange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')">
		<option value="1">CHARITY</option>
	</select>
</td>  

<%
String pay1 = request.getParameter("payment");
%>

<td><select name="payment" id="Payment">
<%-- <option value="cash" <%if(pay1 !=null && pay1.equals("cash")) {%> selected="selected" <%} %> >CASH</option> --%>
<%-- <option value="cheque"  <%if(pay1 !=null && pay1.equals("cheque")) {%> selected="selected" <%} %> >CHEQUE</option> --%>
<option value="card"  <%if(pay1 !=null && pay1.equals("card")) {%> selected="selected" <%} %> >CARD</option>
<option value="online success"  <%if(pay1 !=null && pay1.equals("online success")) {%> selected="selected" <%} %> >ONLINE</option>
<option value="googlepay"  <%if(pay1 !=null && pay1.equals("googlepay")) {%> selected="selected" <%} %> >BHARATHPE</option>
<option value="phonepe"  <%if(pay1 !=null && pay1.equals("phonepe")) {%> selected="selected" <%} %> >PHONE PE</option>
</select></td>
<td><input type="hidden" value="123" name="hid"></input></td>
<td class="border-nn"><input type="submit" value="SEARCH" class="click"></td>
</tr></tbody></table>
</form >

<%}
	String hidden=request.getParameter("hid");
	if(hidden==null)	{
		System.out.println("request urlllll.........if..!"+request.getContextPath());
	}
	else if(hidden!=null)	{
		System.out.println("request urlllll...........!"+request.getContextPath());
	
	%>

		<form id="frmsubmit" action="<%=request.getContextPath()%>/brsurl" method="post" style="padding-left: 70px;padding-top: 30px;" onsubmit="return validate();">
		<table width="100%" cellpadding="0" cellspacing="0" class="list-details" style="position: relative;top: 50px;margin: 10px auto;">
		<tbody>
		<%
				String element=request.getParameter("hid");
				if(element==null)	{}
				else if(element.equals("123") && element!=null)	{
				if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("") && request.getParameter("toDate")!=null && !request.getParameter("toDate").equals(""))	{
					String fromdate=request.getParameter("fromDate");
					String todate=request.getParameter("toDate");
					String paymenttype=request.getParameter("payment");
					String hoa=request.getParameter("head_account_id");
					DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		
					int k=0;
					customerpurchasesService cpservice= new customerpurchasesService();  
					ArrayList<customerpurchases> custlist=new ArrayList<customerpurchases>();
					custlist=cpservice.getCustomerDetails(fromdate, todate,paymenttype ,hoa);
					if(custlist.size()>0 && custlist!=null)	{ %>
						<tr>
					 <td class="bg"  align="center">Checkall</td> 
						<td class="bg" align="center">Date</td>
						<td class="bg" align="center">Amount</td>
						<td class="bg" align="center">JV Number</td>
						<td class="bg" align="center" width= "26%;">BRS-Date</td>
						</tr>
						<% for(k=0;k<custlist.size();k++)	{ 
								customerpurchases cp=new customerpurchases(); 
								cp= custlist.get(k);
								String date=cp.getdate();
								String[] datte=date.split(" ");
							System.out.println("date..brssssssssssssss.!"+date+".."+cp.getvocharNumber());
							
								
							%>
							<tr> 
								 <td align="center"><label><input type="checkbox" value="depositdate<%=k%>" class="form-control" name="check" onchange="checkValue(<%=k%>);" id="ch<%=k%>"></label></td> 
								<td align="center"><%=datte[0]%><input type="hidden" name="hid2<%=k%>" value="<%=datte[0]%>"></td>
								<td align="center"><%=cp.gettotalAmmount()%><input type="hidden" name="amount<%=k%>" value="<%=cp.gettotalAmmount()%>"></td>
								<td align="center" id="jv<%=k%>"><%=cp.getvocharNumber()%><input type="hidden" name="jv<%=k%>" value="<%=cp.getvocharNumber()%>"/></td>
								<td align="center"><input type="text" name="depositdate<%=k%>" class="form-controll" id="depositdate<%=k%>" value="<%=cp.getDepositeddate()%>" ></td><td><input type="hidden" value="<%=cp.getDepositeddate()%>" name="dep<%=k%>"></td>
								<td align="center" class="border-nn"><input type="button" value="Clear" class="click" onclick="clearDate(<%=k%>)"></td>
							</tr>	
							<tr><td><input type="hidden" name="count" value="<%=k%>"></td><td><input type="hidden" id="listcount" value="<%=custlist.size()%>"></td></tr>
						<% } %>
						 <tr><td><input type="hidden" value="<%=paymenttype%>" name="type"></td>
						 <td><input type="hidden" value="456" name="hid1"/></td><td><input type="hidden" value="<%=hoa%>" name="head"/></td></tr>
						 <tr><td align="center"><input type="submit" id="sub" value="Update" class="click"></input></td><td><input type="button" class="click" value="ClearAll" onclick="clearAll();"/></td></tr>
					   
					  </tbody>
					  </table> 
					<% } 
					if(custlist.size()==0 || custlist==null)	{%>
					<div style="align:center;color:red;font-family:verdana;font-weight:bold;font-size:20px">No Records Found Between <%=fromdate %> And <%=todate %></div>
				<%} }
			}%>
		</form> 
		<%}%>
</div>
</body>
</html>
