<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<jsp:useBean id="GOD" class="beans.productexpensesService"/>
<jsp:useBean id="GWS" class="beans.godwanstockService"/>
<jsp:useBean id="MEP" class="beans.medicalpurchasesService"/>
<jsp:useBean id="PRDE" class="beans.productexpensesService"/>
<jsp:useBean id="PRDES" class="beans.productexpenses_scService"/>
<jsp:useBean id="inv" class="beans.invoiceService"/>
<jsp:useBean id="PR" class="beans.products"/>
<jsp:useBean id="NOG" class="beans.no_genaratorService"/>
<jsp:useBean id="PY" class="beans.paymentsService"/>
<jsp:useBean id="IN" class="beans.invoice"/>
<%
Calendar c1 = Calendar.getInstance(); 
c1.setTimeZone(TimeZone.getTimeZone("IST"));
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat  dateFormat3= new SimpleDateFormat("HH:mm:ss");
DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
mainClasses.no_genaratorListing NG_l=new mainClasses.no_genaratorListing();
mainClasses.tabletsListing TABL= new mainClasses.tabletsListing();
mainClasses.bankdetailsListing BAN= new mainClasses.bankdetailsListing();
mainClasses.invoiceListing INV_L=new mainClasses.invoiceListing();
productsListing PROL=new productsListing();
productsListing PRD_L=new productsListing(); 
String othercharges="";
if(request.getParameter("othercharges")!=null){
	othercharges=request.getParameter("othercharges");
}
String head_account_id="";
String vendorId="";
if(request.getParameter("vendorId")!=null && !request.getParameter("vendorId").equals(" ")){
	vendorId=request.getParameter("vendorId");
}
String date1=(dateFormat.format(c1.getTime())).toString();
//String time=dateFormat3.format(dateFormat.parse(date1));
//String date=request.getParameter("date")+" "+time;
String billamount="";
String productId = "";
String major_head_id = "";
String minor_head_id = "";
String billproduct = "";
double totalamount=0.0;
String quantity="";
String purchaseRate="";
String trackingno="";
String expinvoice_id="";
String emp_id=session.getAttribute("adminId").toString();
String count=request.getParameter("expscount");
String invoiceID="";
String dc = request.getParameter("dcNumber").trim();
//double Totalamount=0.00;
String description=request.getParameter("narration");
String dcAmount = request.getParameter("dcAmount");
int i=0;
boolean value=true;
List PRODET=null;
//System.out.println("count---"+count);
if(count != null){
	i=Integer.parseInt(count);
for(int h=1;h<=i;h++){
	if(request.getParameter("purchaseRate"+h)!=""  && request.getParameter("product"+h)!=""  ){
 productId=request.getParameter("product"+h); // subhead
 major_head_id=request.getParameter("major_head_id"+h); 
 minor_head_id=request.getParameter("minor_head_id"+h);
 billproduct=request.getParameter("billproduct"+h); // productid
  purchaseRate=request.getParameter("prodAmt"+h);
  invoiceID = request.getParameter("invoiceId"+h); // mse no
  trackingno=request.getParameter("trackingno"+h);
  expinvoice_id= request.getParameter("expInvoiceId"+h);
  head_account_id = request.getParameter("head_account_id"+h);
  /* System.out.println("h--"+h);
 System.out.println("purcase rate---"+request.getParameter("purchaseRate"+h));
 System.out.println("subhead-----"+request.getParameter("product"+h));
 System.out.println("major_head_id----"+request.getParameter("major_head_id"+h));
 System.out.println("minor_head_id--"+request.getParameter("minor_head_id"+h));
 System.out.println("product----"+request.getParameter("billproduct"+h));
 System.out.println("invoiceId----"+request.getParameter("invoiceId"+h));
 System.out.println("trackingno----"+request.getParameter("trackingno"+h));
 System.out.println("dc----"+dc);
 System.out.println("dcAmount-----"+dcAmount); */
// System.out.println("dept---"+request.getParameter("trackingno"+h));
 //Totalamount=Totalamount+Double.parseDouble(purchaseRate);
%>
	<jsp:setProperty name="PRDE" property="vendorId" value="<%=vendorId%>"/>
	<jsp:setProperty name="PRDE" property="narration" value="<%=description%>"/>
	<jsp:setProperty name="PRDE" property="major_head_id" value="<%=major_head_id%>"/>
	<jsp:setProperty name="PRDE" property="minorhead_id" value="<%=minor_head_id%>"/>
	<jsp:setProperty name="PRDE" property="sub_head_id" value="<%=productId%>"/>
	<jsp:setProperty name="PRDE" property="amount" value="<%=purchaseRate%>"/>
	<jsp:setProperty name="PRDE" property="expinv_id" value="<%=expinvoice_id%>"/>
	<jsp:setProperty name="PRDE" property="extra2" value="<%=billproduct%>"/>
	<jsp:setProperty property="extra5" name="PRDE" value="<%=invoiceID%>"/>
	<jsp:setProperty name="PRDE" property="head_account_id" value="<%=head_account_id%>"/>
	<jsp:setProperty name="PRDE" property="entry_date" value="<%=date1%>"/>
	<jsp:setProperty name="PRDE" property="extra6" value="<%=trackingno %>"/>		
	<%if(session.getAttribute("Role").equals("AsstManger")){ %>
		<jsp:setProperty name="PRDE" property="extra1" value="WaitingMgrApproval"/>
		<jsp:setProperty name="PRDE" property="enter_by" value="<%=emp_id%>"/>
		<jsp:setProperty name="PRDE" property="enter_date" value="<%=date1%>"/>			
	<%}
	else{ %>
		<jsp:setProperty name="PRDE" property="extra1" value="Not-Approve"/>					
		<jsp:setProperty name="PRDE" property="emp_id" value="<%=emp_id%>"/>
	<%}%>
		<jsp:setProperty name="PRDE" property="extra11" value="<%=dc%>"/>
		<jsp:setProperty name="PRDE" property="extra12" value="<%=dcAmount%>"/>
		<jsp:setProperty name="PRDE" property="extra7" value="<%=othercharges%>"/>
	 <%=PRDE.insert()%><%=PRDE.geterror()%>
	
<%
GWS.updateExpensesEntryDateBasedOnExpvId(date1, expinvoice_id);
}
}
/* NOG.updatInvoiceNumber(expinvoice_id,"expenseinvoice"); */	
}
//mainClasses.superadminListing SUPA_L=new mainClasses.superadminListing();
//mainClasses.headofaccountsListing HOFA_L=new mainClasses.headofaccountsListing();
//String heodofacname=HOFA_L.getHeadofAccountName(head_account_id);
//String html="";
/* if(session.getAttribute("Role").equals("Manger")){ 
MailBox.SendMailBean Mail=new MailBox.SendMailBean();
if(expinvoice_id!=null && head_account_id!=null){
	html="<div style='width:750px; height:500px; margin:0px auto; font-family:Verdana, Geneva, sans-serif; font-size:14px;'><table width='100%' cellpadding='0' cellspacing='0'><tr><td style='text-align:center; font-size:18px; color:#7f3519; font-weight:bold;'>SHRI SHIRDI SAI BABA SANSTHAN TRUST</td></tr><tr><td style='text-align:center'>Dilsuknagar, Hyderabad</td></tr><tr><td>Dear  Nageshwar rao,</td></tr><tr><td><p style='text-align:justify;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; One Payment is raised by "+heodofacname+" department. </p></td></tr><tr><td style='font-weight:bold; font-size:16px;'>Please <a href='www.true-local.co.uk/superadmin/adminPannel.jsp?page=payments-detailsReport&invid="+expinvoice_id+"' style='text-decoration:none; color:#F00;'>Click Here</a> to find the payment details  and please approve it. </td></tr><tr><td height='20'></td></tr><tr><td>Thanking you,</td></tr><tr><td>warm regards</td></tr><tr><td style='text-transform:capitalize;'>Sai Sansthan Trust.</td></tr></table></div>";
	//html="<br></br> Dear "+SUPA.getadmin_name()+", <br> One Indent is Raised by  "+heodofacname+" department. Please follow this link for approve the indent <br></br><br></br> <a href='www.true-local.co.uk/superadmin/adminPannel.jsp?page=indentDetailedReport&invid="+IND.getindent_id()+"' >Click Here </a>Indent No "+IND.getindent_id()+".<br></br>Thanks & Regards,<br></br>Sai Sansthan Trust.";
	out.println(Mail.send("accounts@saisansthan.in","nagesh4444@yahoo.com", "", "", "Payment is awaiting for approval  Raised By "+heodofacname+".Please approve the Payment", html, "localhost"));
	
} } */
    response.sendRedirect("adminPannel.jsp?page=dcBillApproval"); 
   
   %>