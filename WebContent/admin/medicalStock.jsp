<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title><%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List"%>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">

function validate(){
	$('#oldError').hide();

	if($('#tabletName').val().trim()==""){
		$('#oldError').show();
		$('#tabletName').focus();
		return false;
	}		
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="TBL" class="beans.tablets" />
		<jsp:useBean id="PRD" class="beans.products" />
		<div class="vendor-page">
<div class="vendor-box"><div class="vendor-title">Medical Store Stock Report</div></div>

			<div class="vendor-list">
				<div class="arrow-down">
					<img src="../images/Arrow-down.png" />
				</div>
				<div class="search-list">
					<ul>
						<li><select>
								<option>Batch actions</option>
								<option>Email</option>
						</select></li>
						<li><select>
								<option>Sort by name</option>
								<option>Sort by company</option>
								<option>Sort by overdue balance</option>
								<option>Sort by open balance</option>
						</select></li>
						<li><input type="search" placeholder="find a vendor" /></li>
					</ul>
				</div>
				<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
					<span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				
				<div class="list-details">
					<%mainClasses.productsListing PRD_L=new mainClasses.productsListing();
List PRD_List=PRD_L.getMedicalStoreStock("1");
if(PRD_List.size()>0){%>
<div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>

							<td class="bg" width="3%">S.No.</td>
							<td class="bg" width="23%">Tablet Name</td>
							<td class="bg" width="23%">Quantity</td>
							<td class="bg" width="10%">Units</td>
						
										</tr>
						<%for(int i=0; i < PRD_List.size(); i++ ){
							PRD=(beans.products)PRD_List.get(i); %>
						<tr>

							<td><%=i+1%></td>
							<td><span><%=PRD.getproductName()%></span></td>
						<%if(Double.parseDouble(PRD.getbalanceQuantityGodwan())>0){ %>
							<td><%=PRD.getbalanceQuantityGodwan()%></td>
							<%}else{ %>
							<td>0.0</td>
							<%} %>
							<td><%=PRD.getunits()%></td>
							<%-- <td><a href="#"
								onclick="openMyModal('tablets_Edit.jsp?ID=<%=PRD.getproductId() %>');">Edit</a></td> --%>
												</tr>
						<%} %>
					</table>
					<%}else{%>
					<div align="center">
						<h1>No stock found</h1>
					</div>
					<%}%>


				</div>
			</div>




		</div>

	</div>
	<!-- main content -->

	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>
</head>
<body>

</body>
</html>