<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sai sansthan accounts-Admin Pannel</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<script>
$(function() {
	
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
function submission(){
	
	var fromDate=$("#fromDate").val();
	var toDate=$("#toDate").val();
	if(fromDate==""){
		alert('Please select From Date');
		return false;
	}
	if(toDate==""){
		alert('Please select To Date');
		return false;
	}
	$("#fulltable").show();
	return true;
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>

<%if(session.getAttribute("adminId")!=null){ %>
<jsp:useBean id="CUST" class="beans.customerpurchases" />

<div><%@ include file="title-bar.jsp"%></div>
<div class="list-details" >
	<form name="kitchenStockAssets.jsp" method="post">
	<div class="search-list">
		
					<ul style="margin:80px 0px 220px 400px;">
						<li>
							<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  placeholder="From Date" readonly="readonly" />
						</li>
						<li>
							<input type="text" name="toDate" id="toDate"  class="DatePicker" value="" placeholder="To Date" readonly required/>
						</li>
						<li>
							<input type="submit" class="click" name="search" value="Search" onclick="return submission();"/>
						</li>
					</ul>
			
	</div></form>
	<div class="icons">
					<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
					<span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
			<%String fromDate=request.getParameter("fromDate");
			String toDate=request.getParameter("toDate"); %>	
	<div class="printable" id="fulltable">
	<table width="95%"  cellspacing="0" cellpadding="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
			<%if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("")){ %>
			<tbody> 
				<tr>
					<td colspan="3" align="center" style="font-weight: bold;color:red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
				</tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">KIND SHAWLS AND SALE OF SHAWLS REPORT</td></tr>
				 <tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=fromDate %> TO <%=toDate %> </td></tr> 
			</tbody>
			
		</table><br>
			<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border='1'>
				<%if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("")){ %>
				<tr>
					 <td class="bg" width="5%" style="font-weight: bold;">SNO</td> 
					
					<td class="bg" width="20%" style="font-weight: bold;">OFFERKIND NO</td>
					<td class="bg" width="20%" style="font-weight: bold;">ACTUAL DATE</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">ACTUAL AMOUNT</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">SOLD DATE</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">SOLD AMOUNT</td>
					
				</tr>
				<%}
				
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date myDate = dateFormat.parse(fromDate);
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(myDate);
				cal1.add(Calendar.DAY_OF_YEAR, -1);
				Date previousDate = cal1.getTime();
				String previousDate1="";
				TimeZone tz = TimeZone.getTimeZone("IST");
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				previousDate1=dateFormat2.format(previousDate);
				//System.out.println("previous date..."+previousDate1);
				customerpurchasesListing CUST_L=new customerpurchasesListing();

				List CSP=CUST_L.getcustomerpurchasesListBasedDates(fromDate, toDate,"20026");
				godwanstockListing GDSTK_L=new godwanstockListing();
				shopstockListing SHPSTK_L=new shopstockListing();
				
				String[] offerKindSold=new String[2];
				//String[] totalQuantity=new String[2050];;
				if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("") && toDate!=null && !toDate.equals("")){
				for(int i=0; i < CSP.size(); i++ ){
					CUST=(customerpurchases)CSP.get(i);
					offerKindSold=CUST_L.getTotalSaleQuantityBasedHOAOnOfferKind(CUST.getbillingId());
					//System.out.println("value...."+offerKindSold); %>
				<tr>
				   <td><%=i+1%></td>
					
					<td><%=CUST.getbillingId()%></td>
					<td><%=CUST.getdate()%></td>
					<td><%=CUST.getrate() %></td>
					<%if(offerKindSold[0]!=null && !offerKindSold[0].equals("null") && !offerKindSold[0].equals("")){ %>
					<td><%=offerKindSold[1]%></td>
					<td><%=offerKindSold[0]%></td>
					<%}else{ %>
					<td></td>
					<td></td>
					<%} %>
				</tr>
				<%}}}else{ %>
				
				<%} %>
				
		</table>
	</div>
</div>
<%}else{
	 response.sendRedirect("index.jsp");
} %>
</body>
</html>