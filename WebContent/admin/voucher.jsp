<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
@media print {
	@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin-left: 24mm; /* equals to 0.8inches */ 
}
	.voucher
	{
		width:880px; height:auto;  padding:40px 20px 40px 20px; border:1px solid #727272; left:55px; position:relative;
	}
	 .voucher table.sri td span { font-size:30px; font-weight:bold;}
	.voucher table td.regd { text-align:center; vertical-align:top; font-size:12px;}
	.voucher table.cash-details{ margin:40px 0 0 0; font-size:20px; border-top:1px solid #727272; border-left:1px solid #727272;}
	.voucher table.cash-details td{padding:10px 0px; border-bottom:1px solid #727272; border-right:1px solid #727272;  padding:10px 20px;}
	.voucher table.cash-details td span {text-decoration:underline; padding:0 0 5px 0; font-weight:bold; }
	.voucher table.cash-details td.vide ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide ul li{ list-style:none; float:left; margin:0 45px 0 0; }
	/*.voucher table.cash-details td.vide ul li:first-child{ width:400px;  margin:0 20px 0 0;}*/
	.voucher table.cash-details td.vide1 ul { margin:0px; padding:0px; }
	.voucher table.cash-details td.vide1 ul li{ list-style:none; float:left; margin:0 63px 0 0}
	.voucher table.cash-details td.vide1 ul li span{ text-decoration:underline;}
	.voucher table.cash-details td.vide1 ul li:last-child{  margin:0 0px 0 0px}
	.signature{ width:150px; height:100px; border:1px solid #000; margin:0 12px 0 0;}
.voucher table.cash-details td table.budget {border-top:1px solid #000; border-left:1px solid #000;}
.voucher table.cash-details td table.budget td {border-bottom:1px solid #000; border-right:1px solid #000; padding:5px;}
}
</style>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!-- main content -->
	<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<jsp:useBean id="EXP" class="beans.productexpenses"></jsp:useBean>
<jsp:useBean id="GOD" class="beans.godwanstock"></jsp:useBean>
	<div>
	<div class="vendor-page">
	<div class="vendor-list">
				<div class="icons"><a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
				<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
				</div>
				<div class="clear"></div>
				<div class="list-details">
				 <!--<table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
						<tr>
					<td colspan="7" align="center" style="font-weight: bold;">Cash / Cheque voucher PRINT</td>
					</tr>
				 </table>-->
		<div class="printable">
				<%
				String vochertype="";
				if(request.getParameter("vochertype")!=null){
					vochertype=request.getParameter("vochertype").toString();
				}
				
				String payID=request.getParameter("payId");
				SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
				SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat SDF1=new SimpleDateFormat("yyyy-MM-dd");
				mainClasses.paymentsListing PAYL=new mainClasses.paymentsListing();
				mainClasses.productexpensesListing EXPL=new mainClasses.productexpensesListing();
				mainClasses.godwanstockListing GODL=new mainClasses.godwanstockListing();
				bankdetailsListing BNKL=new bankdetailsListing();
				majorheadListing MJHL=new majorheadListing();
				subheadListing SUBL=new subheadListing();
				mainClasses.headofaccountsListing HODL=new mainClasses.headofaccountsListing();
				mainClasses.majorheadListing MJHDL=new mainClasses.majorheadListing();
				mainClasses. minorheadListing MINL=new mainClasses.minorheadListing();
				List PAY_DET=PAYL.getPassOrderInfo(payID);
				List EXP_Det=null;
				if(PAY_DET.size()>0){
					PAY=(beans.payments)PAY_DET.get(0);
					EXP_Det=EXPL.getMproductexpensesBasedOnExpInvoiceID(PAY.getextra3());
					 if(EXP_Det.size()>0){
						 EXP=(beans.productexpenses)EXP_Det.get(0);
					 }		 
				}
				DecimalFormat DF=new DecimalFormat("0");
				DecimalFormat DF1=new DecimalFormat("0.00");
                NumberToWordsConverter NTW=new NumberToWordsConverter();
                String amount="";
                String chequeno="";
                String Vocherno[]=PAY.getreference().split(",");
                		if(!vochertype.equals("") && Vocherno.length>1){
                			if(vochertype.equals("tds") && Vocherno.length>1){
                				Vocherno[0]=Vocherno[1];
                				amount=DF.format(Double.parseDouble(PAY.getextra7()));
                				chequeno=PAY.getextra9();
                			} else if(vochertype.equals("servicetax")){
                   				if( Vocherno.length>2 && Vocherno[2]!=null){
                				Vocherno[0]=Vocherno[2];
                				} else {
                					Vocherno[0]=Vocherno[1];
                				}
                   				chequeno=PAY.getService_cheque_no();
            					amount=DF.format(Double.parseDouble(PAY.getService_tax_amount()));
                			}
                			
                		} else{
                			Vocherno=PAY.getreference().split(",");
                			amount=DF.format(Double.parseDouble(PAY.getamount())-Double.parseDouble(PAY.getextra7())-Double.parseDouble(PAY.getService_tax_amount()));
                			chequeno=PAY.getchequeNo();
                		}
                		
                		//these lines are added by srinivas on 6/9/2016
                		if(PAY.getextra6().equals("minus")){
                			if(!PAY.getextra5().equals("")){
                				amount=DF.format(Double.parseDouble(amount)-Double.parseDouble(PAY.getextra5()));
                			}
               		 } else if(PAY.getextra6().equals("plus")){
                			if(!PAY.getextra5().equals("")){
                				amount=DF.format(Double.parseDouble(amount)+Double.parseDouble(PAY.getextra5()));
                        		}
             	     }
				%>
                <div class="voucher">
                <table width="100%" cellpadding="0" cellspacing="0" class="sri">
                <tr>
               	<td class="regd" colspan="2"><span>SHRI SHIRDI SAIBABA SANSTHAN TRUST</span> <br /><br />
                ( Regd. No. 646/92 )<br />
				DILSUKHNAGAR, HYDERABAD - 500 060.<br /><br />
				<span  style="text-transform: uppercase; text-decoration:underline"><%=HODL.getHeadofAccountName(EXP.gethead_account_id())%> Cash / Cheque Voucher</span></td>
                </tr>
                </table>
                <table cellpadding="0" cellspacing="0" width="100%" class="cash-details">
                <tr>
                <td> Voucher No : <span><%=Vocherno[0] %></span>
                <%if (!PAY.getExtra10().equals(""))	{%>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DC No : <span><%=PAY.getExtra10() %></span>	
                <%}%>
                </td>
                <td>Date <span><%=CDF.format(SDF.parse(PAY.getdate()))%></span></td>
                </tr>
                <tr>
                <td colspan="2"><!-- <span>DEBIT : </span> -->
                <%
                godwanstockListing gl = new godwanstockListing();
                godwanstock gd = gl.getgodwanstockByExpoinvoiceId(PAY.getextra3()).get(0);
                %>
               <span> &nbsp;&nbsp;&nbsp;Unique No:&nbsp;&nbsp;&nbsp;<%=gd.getExtra9() %></span>
                </td>
                </tr>
                <tr>
                    <td colspan="2">
                    <span class="floatleft" style="text-decoration:none;">Head of Account : </span>   
                    <span class="head-act"><%=HODL.getHeadofAccountName(EXP.gethead_account_id())%></span>
                    </td>
                    
                  </tr>
                  <tr>  
                    <td colspan="2">
                     <span class="floatleft" style="text-decoration:none;">Major Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span>
                     <span class="head-act"><%=EXP.getmajor_head_id()%>&nbsp;&nbsp;&nbsp;&nbsp;<%=MJHDL.getmajorheadName(EXP.getmajor_head_id())%></span>   
                     </td>
                    </tr>
                
                <tr>
                    <td colspan="2">
                    <span class="floatleft" style="text-decoration:none;">Minor Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span>
                    <span class="head-act"><%=EXP.getminorhead_id()%>&nbsp;&nbsp;&nbsp;&nbsp;<%=MINL.getminorheadName(EXP.getminorhead_id())%></span>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="2">
                    <span class="floatleft" style="text-decoration:none;">Sub Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span>
                    <span class="head-act"><%=EXP.getsub_head_id()%>&nbsp;&nbsp;&nbsp;&nbsp;<%=SUBL.getMsubheadnameWithDepartment(EXP.getsub_head_id(),EXP.gethead_account_id()) %></span>
                    </td>
                    </tr>
                     <tr>
                    
                    </tr>
                <tr>
                <td colspan="2">A sum of Rupees : <span><%=NTW.convert(Integer.parseInt(amount)) %> only</span></td>
                </tr>
                <tr>
                <td colspan="2">Being <span><%=PAY.getnarration() %></span></td>
                </tr>
                 <tr>
                <td colspan="2" class="vide">
                <ul>
                <li>Paid vide Cheque No. <span><%=chequeno %></span></li>
                <li>Date <span><%=CDF.format(SDF.parse(PAY.getdate()))%></span></li>
                <li>Bank <span><%=BNKL.getBankName(PAY.getbankId()) %></span></li>
                </ul>
                </td>
                </tr>
                <tr>
                 <td colspan="2">Rs. <span style="font-weight:bold"><%=DF1.format(Math.round(Double.parseDouble(amount))) %></span></td>
                 </tr> 
                 <tr><td colspan="2" height="40" style="border-bottom:none;"></td></tr>
                <tr>
                <td align="right" colspan="2" style="border-bottom:none;"><div class="signature"></div></td>
                </tr>
                <tr>
                <td colspan="2" class="vide1">
                <ul>
                <li>Accountant</li>
                <li>Manager ( Acts )</li>
                 <li>GS/Chairman</li> 
                <li>Treasurer</li>
                 <li>Receiver's Signature</li>
                </ul>
                </td>
                </tr>
                </table>
                
                </div>
          	
			 </div>
			</div>
			</div>
</div>
</div>
