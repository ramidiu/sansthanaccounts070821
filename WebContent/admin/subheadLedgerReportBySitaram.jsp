<%@page import="mainClasses.ReportesListClass"%>
<%@page import="beans.customerpurchases"%>
<%@page import="beans.productexpenses"%>
<%@page import="beans.bankbalance"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
/*.bod-nn{
	border-right:none !important;}*/
	}
/*	.bod-nn{
	border-right:1px solid #000 !important;
	font-weight: bold;}*/
</style>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Sub Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <script>
function combochangewithdefaultoption(denom,desti,jsppage){
	var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value;  
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
		$.post(jsppage,{id:com_id} ,function(data)
		{
			//$('#minor_head_id').empty();
			if(denom=="head_account_id"){
				document.getElementById("minor_head_id").options[0].selected = "true";	
			}
			if(desti == "minor_head_id"){
				var x=document.createElement('option');			
				x.text="Select minor head";
				x.value="";
				sda1.add(x,null);
			}
			var where_is_mytool=data.trim();
		var mytool_array=where_is_mytool.split("\n");
		for(var i=0;i<mytool_array.length;i++)
		{
		if(mytool_array[i] !="")
		{
		//alert (mytool_array[i]);
		var y=document.createElement('option');
		var val_array=mytool_array[i].split(":");
					y.text=val_array[1];
					y.value=val_array[0];
					try
					{
					sda1.add(y,null);
					}
					catch(e)
					{
					sda1.add(y);
					
					}
		}
		}
		});
}
</script> 

<script>
$(function() {
	$( "#fromdate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#todate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#todate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromdate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    
    <script>

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromdate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromdate').datepicker('option', 'maxDate', new Date(endDate));
	$('#todate').datepicker('option', 'minDate', new Date(startDate));
	$('#todate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 
</script>
    
        <script type="text/javascript">
    function formsubmit(){
    	
    	if($("#minor_head_id").val()!="" && $("#minor_head_id").val()!=null)
    	{
	   		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReportBySitaram&subhid="+$( '#minor_head_id' ).val()+"&finYear="+$('#finYear').val()+"&cumltv="+$('#cumltv').val();
	       	document.getElementById("departmentsearch").submit();
	   	} 
    	else if($("#major_head_id").val() != "" && $("#fromdate").val() !="" && $("#todate").val() !="")
    	{  
    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReportBySitaram&majrhdid="+$('#major_head_id').val()+"&fromdate="+$('#fromdate').val()+"&todate="+$('#todate').val()+"&report="+$('#report').val()+"&hoid="+$('#head_account_id').val()+"&finYear="+$('#finYear').val()+"&cumltv="+$('#cumltv').val();
        	document.getElementById("departmentsearch").submit();
    	} 
    	else if($("#fromdate").val() !="" && $("#todate").val() !="" && $('#head_account_id').val() != "")
    	{  
	    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReportBySitaram&hoid="+$('#head_account_id').val()+"&cumltv="+$('#cumltv').val();
	    	document.getElementById("departmentsearch").submit();
    	}
    	else if($("#fromdate").val() =="" && $("#todate").val() =="")
    	{  
	    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReportBySitaram&cumltv="+$('#cumltv').val();
	    	document.getElementById("departmentsearch").submit();
    	}
    	
    	/* if($( "#major_head_id" ).val()!=""){    		
        	document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReport&majrhdid="+$( '#major_head_id' ).val()+"&fromdate="+$( '#fromdate' ).val()+"&todate="+$( '#todate' ).val()+"&report="+$( '#report' ).val();
        	document.getElementById("departmentsearch").submit();
    		    	} else if($( "#minor_head_id" ).val()!=""){
    		    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReport&subhid="+$( '#minor_head_id' ).val()+"&fromdate="+$( '#fromdate' ).val()+"&todate="+$( '#todate' ).val()+"&report="+$( '#report' ).val();
    		        	document.getElementById("departmentsearch").submit();
    		    	}  else{    	
    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReport";
    	document.getElementById("departmentsearch").submit();
    	} */
    }</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
<%@page import="java.util.List"%>
</head>

<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body onload="onloadcombo();">
	<%if(session.getAttribute("adminId")!=null){%>
	<!-- main content -->
	<%
	DecimalFormat df=new DecimalFormat("#,###.00");
	headofaccountsListing HOA_L=new headofaccountsListing();
	List headaclist = HOA_L.getheadofaccounts();

	minorheadListing MIN_L = new minorheadListing();
	productexpensesListing PRDEXP_L=new productexpensesListing();
	double credit=0.0;
	double debit=0.0;
	double balance=0.0;
	double credit1=0.0;
	double debit1=0.0;
	double balance1=0.0;
	
	SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat onlydat2=new SimpleDateFormat("dd-MMM-yyyy");
	
	String fromdate="";
	String onlyfromdate="";
	String todate="";
	String report="";
	String onlytodate="";
	
	String cashtype="online pending";
	String cashtype1="othercash";
	String cashtype2="offerKind";
	String cashtype3="journalvoucher";
	
	String hoaid = request.getParameter("hoaid");
	String majorHeadId = request.getParameter("majorHeadId");
	String minorHeadId = request.getParameter("minorHeadId");
	
	subheadListing SUBH_L = new subheadListing();



	if(request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals("")){
		 fromdate=request.getParameter("fromdate");
	}
	if(request.getParameter("todate")!=null && !request.getParameter("todate").equals("")){
		todate=request.getParameter("todate");
	}

	if(request.getParameter("report")!=null){
		report=request.getParameter("report");
	}
	
	String finYr = request.getParameter("finYear");
	String finStartYr = request.getParameter("finYear").substring(0, 4);
	
	String onlyfdate = request.getParameter("fromdate");
	String onlytdate = request.getParameter("todate");
	String fdate = request.getParameter("fromdate")+" 00:00:00";
	String tdate = request.getParameter("todate")+" 23:59:59";
	
	if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
	{
		onlyfdate = finStartYr+"-04-01";
		fdate = finStartYr+"-04-01"+" 00:00:00";
	}
	
	%>
	<div>
			<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form  name="departmentsearch" id="departmentsearch" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_table">
<tr>
<td style="width:14%;">Select Financial Year</td>
<td style="width:14%;"><div class="warning" id="fromdateErr" style="display: none;">Please Provide  "From Date".</div>From Date</td>
<td style="width:14%;"><div class="warning" id="todateErr" style="display: none;">Please Provide  "To Date".</div>To Date</td>
<td style="width:14%;"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td style="width:14%;"><div class="warning" id="MajorHeadErr" style="display: none;">Please select  "Major head".</div>Major head</td>
<td style="width:14%;"><div class="warning" id="MinorHeadErr" style="display: none;">Please select  "Minor head".</div>Minor head</td>
</tr>
<tr>
	<td style="width:14%;">
		<select name="finYear" id="finYear" Onchange="datepickerchange();" style="width:90%;">
		<option value="2019-20" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2019-20")) {%> selected="selected"<%} %>>2019-2020</option>
		<option value="2019-19" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2018-19")) {%> selected="selected"<%} %>>2018-2019</option>
		<option value="2017-18" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2017-18")) {%> selected="selected"<%} %>>2017-2018</option>
		<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-2017</option>
		<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-2016</option>
		</select>
	</td>
	<td style="width:14%;"> <input type="text" name="fromdate" id="fromdate" readonly="readonly" value="<%=onlyfdate%>"/>
		<input type="hidden" name="report" id="report" readonly="readonly" value="<%=report%>"/>  
	</td>
	<td style="width:14%;">
		<input type="text" name="todate" id="todate" readonly="readonly" value="<%=onlytdate%>"/>
	 </td>
	<td style="width:14%;">
		<select name="head_account_id" id="head_account_id" onChange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')" style="width:95%;">
			<option value="SasthanDev" >Sansthan Development</option>
			<%
			if(headaclist.size()>0){
				for(int i=0;i<headaclist.size();i++){
				HOA=(headofaccounts)headaclist.get(i);%>
			<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
			<%}} %>
		</select>
	</td>
	<td style="width:14%;">
		<select name="major_head_id" id="major_head_id" onChange="combochangewithdefaultoption('major_head_id','minor_head_id','getMinorHeads.jsp')" style="width:95%;">
			<option value="" selected="selected">Select major head</option>
		</select>
	</td>
	<td style="width:14%;">
		<select name="minor_head_id" id="minor_head_id" style="width:95%;">
		<option value="" selected="selected">Select minor head</option>
		</select>
	</td>
</tr>
<tr>
<td style="width:14%;">Select Cumulative Type</td>
</tr>
<tr>
	<td style="width:14%;">
	<select name="cumltv" id="cumltv">
		<option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option>
		<option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option>
	</select>
	</td>
	<td><input type="button" value="SEARCH" class="click" onclick="formsubmit()" style="margin-top:20px;"/></td>
</tr>
</table> </form>
				<% 
				List subhdlist = SUBH_L.getSubheadListMinorhead(minorHeadId);
				
				String minorheadName = MIN_L.getMinorHeadNameByCat1(minorHeadId, hoaid);
				
				%>
				<div class="icons">
				<div style="margin-bottom: 20px;float: left;cursor: pointer;"><img src="../images/back-button (1).png" width="100" height="50" onclick="goBack()" title="print"/></div>
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="5" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(hoaid)%></td>
					</tr>
					<tr><td colspan="5" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="5" align="center" style="font-weight: bold;"><%=report %> Report Of Minor Head</td></tr>
					<tr><td colspan="5" align="center" style="font-weight: bold;">Report From Date <%=onlydat2.format(onlydat.parse(onlyfdate)) %> TO <%=onlydat2.format(onlydat.parse(onlytdate)) %> </td></tr>
						<tr>
						<td colspan="5">
						<span style="font-weight: bold; font-size:14px; padding-left:80px;"><%=minorheadName %>(<%=minorHeadId %>)</span>
						<table width="100%" cellpadding="0" cellspacing="0" class="print_table">
						<tr>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">S.NO.</td>
							<td class="bg" width="30%" style="font-weight: bold;" align="left"  colspan="0">Sub Head</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="left" colspan="0">DEBIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="left" colspan="0">CREDIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="left" colspan="0">BALANCE</td>
						</tr>
						
								<% 
								
								if(subhdlist.size()>0){ 
									
									ReportesListClass reportesListClass = new ReportesListClass();
									
									List<bankbalance> debitAmountOpeningBalList = null;
									List<bankbalance> creditAmountOpeningBalList = null;
									
									if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
									{
										debitAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hoaid, majorHeadId, minorHeadId, "", finYr,"Assets", "debit", "extra5", "MONTHNAME");
										creditAmountOpeningBalList = reportesListClass.getMajorOrMinorOrSubheadOpeningBal(hoaid, majorHeadId, minorHeadId, "", finYr,"Liabilites", "credit", "extra5", "MONTHNAME");
									} 
									
									
									List<banktransactions> otherAmountList = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, majorHeadId, minorHeadId, "", fromdate, todate, "sub_head_id", "MONTHNAME", "not a loan bank");
									List<banktransactions> otherAmountListForLoanBank = reportesListClass.getOtherDepositsAmountGroupBySubheadId("other-amount", hoaid, majorHeadId, minorHeadId, "", fromdate, todate, "sub_head_id", "MONTHNAME", "loan bank");
									
									
									List<productexpenses> productexpensesList = reportesListClass.getSumOfAmountFromProductexpenses(hoaid, majorHeadId, minorHeadId, "", fdate, tdate, "pe.sub_head_id", "MONTHNAME");
// 									List<productexpenses> productexpensesJVDebit =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, majorHeadId, minorHeadId, "", cashtype3, fdate, tdate, "pe.sub_head_id", "MONTHNAME", "debit");
									List<customerpurchases> customerpurchases = reportesListClass.getJVAmountsFromcustomerpurchases(hoaid, majorHeadId, minorHeadId, "", fdate, tdate, cashtype3, "productId", "MONTHNAME");
									
									// jv amount from productexpensec withbank and withloanbank and withvendor 
									List<productexpenses> productexpensesJV =	reportesListClass.getJVAmountsFromproductexpenses(hoaid, majorHeadId, minorHeadId, "", cashtype3, fdate, tdate, "pe.sub_head_id", "MONTHNAME", "credit");
									List<customerpurchases> DollarAmt = reportesListClass.getSumOfDollorAmountfromcustomerpurchases(hoaid, majorHeadId, minorHeadId, "", fdate, tdate, cashtype1, "s.sub_head_id", "MONTHNAME");
									List<customerpurchases> cardCashChequeJvAmountList = reportesListClass.getcardCashChequeJvOnlineSuccessAmountFromcustomerpurchases(hoaid, majorHeadId, minorHeadId, "", fdate, tdate, "s.sub_head_id", "MONTHNAME");
// 									List<customerpurchases> jvCreditAmountFromCustomerpurchasesList = reportesListClass.getJVCreditAmountFromCustomerPurchases(hoaid, majorHeadId, minorHeadId, "", fdate, tdate, "s.sub_head_id", "MONTHNAME");
									
									
								for(int i = 0 ; i < subhdlist.size(); i++){
									
									SUBH = (subhead) subhdlist.get(i);
									double otherAmount2 = 0.0;
								if( otherAmountList != null && otherAmountList.size()>0)
								{
									for(int j = 0 ; j < otherAmountList.size(); j++){
										banktransactions banktransactions = (banktransactions) otherAmountList.get(j);
										if( banktransactions.getSub_head_id().trim().equals(SUBH.getsub_head_id()) && banktransactions.getamount() != null)
										{
											otherAmount2 = otherAmount2 + Double.parseDouble(banktransactions.getamount());
										}
									}
								}
								
								 
								
								double otherAmountLoan2 = 0.0;
								if( otherAmountListForLoanBank != null && otherAmountListForLoanBank.size()>0)
								{
									for(int j = 0 ; j < otherAmountListForLoanBank.size(); j++){
										banktransactions banktransactions = (banktransactions) otherAmountListForLoanBank.get(j);
										if( banktransactions.getSub_head_id().trim().equals(SUBH.getsub_head_id()) && banktransactions.getamount() != null)
										{
											otherAmountLoan2 = otherAmountLoan2 + Double.parseDouble(banktransactions.getamount());
										}
									}
								}
								
								%>	
						    <tr style="position: relative;">
									<td style="font-weight: bold;" width="10%" align="center"><%=i+1 %></td>
									<td style="font-weight: bold;" colspan="0" width="30%" align="left"> 
										<a href="adminPannel.jsp?page=productLedgerReportBySitaram&typeserch=Subhead&subhead=<%=SUBH.getsub_head_id()%>&minorHeadId=<%=minorHeadId%>&majorHeadId=<%=majorHeadId %>&hod=<%=hoaid%>&finYear=<%=finYr%>&fromdate=<%=fromdate%>&todate=<%=todate%>&cumltv=<%=request.getParameter("cumltv")%>"><%=SUBH.getname() %> <%=SUBH.getsub_head_id() %></a>
									</td>
									<%
									double debitAmountOpeningBal = 0.0;
									double creditAmountOpeningBal = 0.0;
									if(debitAmountOpeningBalList != null && debitAmountOpeningBalList.size() > 0){
										for(int j = 0 ; j < debitAmountOpeningBalList.size(); j++){
											bankbalance bankbalance = debitAmountOpeningBalList.get(j);
											if(bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id())){
												debitAmountOpeningBal = debitAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
											}
										}
									}
									if(creditAmountOpeningBalList != null && creditAmountOpeningBalList.size() > 0){
										for(int j = 0 ; j < creditAmountOpeningBalList.size(); j++){
											bankbalance bankbalance = creditAmountOpeningBalList.get(j);
											if(bankbalance.getExtra5().trim().equals(SUBH.getsub_head_id())){
												creditAmountOpeningBal = creditAmountOpeningBal + Double.parseDouble(bankbalance.getBank_bal());
											}
										}
									}
									

									double LedgerSum = 0.0;
									double LedgerSumBasedOnJV = 0.0;
									double productExpJVdebitAmount = 0.0;
								
							
									if(productexpensesList != null && productexpensesList.size() > 0){
										for(int j = 0; j < productexpensesList.size(); j++){
										
											productexpenses productexpenses = productexpensesList.get(j);
											if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
												LedgerSum = LedgerSum + Double.parseDouble(productexpenses.getamount());
											}
										}
									}
									
									if(customerpurchases != null && customerpurchases.size() > 0){
										for(int j = 0; j < customerpurchases.size(); j++){
											
											customerpurchases customerpurchases2 = customerpurchases.get(j);
											if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
												LedgerSumBasedOnJV = LedgerSumBasedOnJV + Double.parseDouble(customerpurchases2.gettotalAmmount());
											}
										}
									}
									
// 									if(productexpensesJVDebit != null && productexpensesJVDebit.size() > 0){
// 										for(int j = 0; j < productexpensesJVDebit.size(); j++){
// 											productexpenses productexpenses = productexpensesJVDebit.get(j);
// 											if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
// 												productExpJVdebitAmount = productExpJVdebitAmount + Double.parseDouble(productexpensesJVDebit.get(j).getamount());
// 											}
// 										}
// 									}
									
										debit1 = LedgerSum + LedgerSumBasedOnJV + debitAmountOpeningBal + productExpJVdebitAmount + otherAmountLoan2;
									  %>
									<td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(debit1)%></td>
									<%
									
									debit = debit + debit1; 
									 	
									double productExpJVAmount = 0.0;
									if(productexpensesJV != null && productexpensesJV.size() > 0){
										for(int j = 0; j < productexpensesJV.size(); j++){
											productexpenses productexpenses = productexpensesJV.get(j);
											if(productexpenses.getsub_head_id().trim().equals(SUBH.getsub_head_id())){
												productExpJVAmount = productExpJVAmount + Double.parseDouble(productexpenses.getamount());
											}
											
										}
									}
									
									double SumDollarAmt = 0.0;
									if(DollarAmt != null && DollarAmt.size() > 0){
										for(int j = 0; j < DollarAmt.size(); j++){
											customerpurchases customerpurchases2 = DollarAmt.get(j);
											if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
												SumDollarAmt = SumDollarAmt + Double.parseDouble(customerpurchases2.gettotalAmmount());
											}
										}
									}
									
									double cardCashChequeJvOnlineAmount = 0.0;
									if(cardCashChequeJvAmountList != null && cardCashChequeJvAmountList.size() > 0){
										for(int j = 0; j < cardCashChequeJvAmountList.size(); j++){
											customerpurchases customerpurchases2 = cardCashChequeJvAmountList.get(j);
											if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
												cardCashChequeJvOnlineAmount = cardCashChequeJvOnlineAmount + Double.parseDouble(customerpurchases2.gettotalAmmount());
											}
										}
									}
									
									double jvCreditAmountFromCustomerpurchases = 0.0;
// 									if(jvCreditAmountFromCustomerpurchasesList != null && jvCreditAmountFromCustomerpurchasesList.size() > 0){
// 										for(int j = 0; j < jvCreditAmountFromCustomerpurchasesList.size(); j++){
// 											customerpurchases customerpurchases2 = jvCreditAmountFromCustomerpurchasesList.get(j);
// 											if(customerpurchases2.getproductId().trim().equals(SUBH.getsub_head_id())){
// 												jvCreditAmountFromCustomerpurchases = jvCreditAmountFromCustomerpurchases + Double.parseDouble(jvCreditAmountFromCustomerpurchasesList.get(j).gettotalAmmount());
// 											}
// 										}
// 									}
									
									credit1 = productExpJVAmount + SumDollarAmt + cardCashChequeJvOnlineAmount + creditAmountOpeningBal + otherAmount2 + jvCreditAmountFromCustomerpurchases;
									credit = credit + credit1;
									 	
									 	%>
					                   <td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(credit1)%></td>
					                   <%
					                   if(debit1>credit1)
					                   {
					                	   balance1=debit1-credit1;
					                   }
					                   if(credit1>debit1)
					                   {
					                	   balance1=credit1-debit1;
					                   }
					                   if(credit1==debit1)
					                   {
					                	   balance1=0.0;
					                   }
					                   %>
					                   <td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(balance1)%></td>
						</tr>
					<% }} %>
				
					   </table>
						</td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td style="font-weight: bold;text-align: center;" colspan="0" width="40%">Total  </td>
						<td style="font-weight: bold;text-align: left;" colspan="0" width="20%">Rs.<%=df.format(debit) %></td>
						<td style="font-weight: bold;text-align: left;" colspan="0" width="20%">Rs.<%=df.format(credit) %> </td>
						<%if(debit>credit){balance=debit-credit;}if(credit>debit){balance=credit-debit;}if(credit==debit){balance=0.0;}%>
						<td style="font-weight: bold;text-align: left;" colspan="0" width="20%">Rs.<%=df.format(balance) %> </td>
						</tr>
				
					</table>
			</div>
				
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<script>
function onloadcombo()
{
	combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp');
	combochangewithdefaultoption('major_head_id','minor_head_id','getMinorHeads.jsp');
	}
</script>
</body>
</html>