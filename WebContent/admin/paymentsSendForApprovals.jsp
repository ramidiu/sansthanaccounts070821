<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payments Send For Approvals</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#dateError').hide();
	$('#indentError').hide();
	$('#bankError').hide();
	$('#vendorError').hide();
/* 	$('#nameError').hide(); */

	if($('#date').val().trim()==""){
		$('#dateError').show();
		$('#date').focus();
		return false;
	}
	/* if($('#vendorId').val().trim()==""){
		$('#vendorError').show();
		$('#vendorId').focus();
		return false;
	} */
	if($('#paymentType').val().trim()=="cheque"){
	if($('#bankId').val().trim()==""){
		$('#bankError').show();
		$('#bankId').focus();
		return false;
	}
	}
	if($('#major_head_id').val().trim()==""){
		$('#indentError').show();
		$('#major_head_id').focus();
		return false;
	}
	
	/* 
	if($('#nameOnCheck').val().trim()==""){
		$('#nameError').show();
		$('#nameOnCheck').focus();
		return false;
	} */
	if(($('#product0').val().trim()=="" || $('#product0').val().trim()=="")){
		$('#product0').addClass('borderred');
		$('#product0').focus();
		return false;
	}	
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>

<script>
$(document).ready(function(){
  $("#major_head_id").keyup(function(){
	  var data1=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#major_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#minor_head_id").keyup(function(){
	  var data1=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#minor_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#sub_head_id").keyup(function(){
	  var data1=$("#sub_head_id").val();
	  var minor=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#sub_head_id" ).autocomplete({source: availableTags}); 
			});
  });
  $("#bankId").keyup(function(){
	  var hoid=$("#head_account_id").val();
	  var data1=$("#bankId").val();
	  $.post('searchBanks.jsp',{q:data1,HOA:hoid},function(data)
	  {
		 var response = data.trim().split("\n");
		 var bankNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 bankNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=bankNames;
		 $( "#bankId" ).autocomplete({source: availableTags}); 
			});
  });
});
</script>
<script>
function combochange(denom,desti,jsppage) { 
    var com_id = denom; 
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	$.post(jsppage,{ id: com_id } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");
	
				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
				sda1.add(y,null);
				}
				catch(e)
				{
				sda1.add(y);
				}
	}
	
	}
	}); 
}
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function vendorsearch(){
	  var hoid=$('#head_account_id').val();
	  var data1=$('#vendorId').val();
	  var arr = [];
	  $.post('searchVendor.jsp',{q:data1,b : hoid},function(data)
	{
		var response = data.trim().split("\n");
		var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
				 doctorIds:d[0],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		 $( '#vendorId').autocomplete({source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},	 
		 }); 
			});
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var arr = [];
	  //var data1=$("#sub_head_id").val();
	  var minor=$("#minor_head_id").val();
	  var major=$("#major_head_id").val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#purchaseRate'+j).val(ui.item.amount);
				$('#vat'+j).val(ui.item.vat);
				
			}
		}); 
			});
} 
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;

	u=document.getElementById("addcount").value;

	if(document.getElementById("product"+i).value!=""){

		if(document.getElementById("purchaseRate"+i).value!="")
		{
		
			for(var j=0;j<=u;j++){
			
				if(document.getElementById("purchaseRate"+j).value!=""){

				grossprice=Number(parseFloat(document.getElementById("purchaseRate"+j).value)*Number(parseFloat(document.getElementById("quantity"+j).value))).toFixed(2);

			document.getElementById("grossAmt"+j).value=grossprice;
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice)).toFixed(2);
			
				}
				}
			document.getElementById("total").value=Totalprice;
		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;

}
function addOption(){
	$("#department").empty();
	$("#vendorId").val(" ");
	var hoaid=$("#HOA").val();
	if(hoaid=="3"){
		$("#department").append('<option value=godown>Godown</option>');
		/* $("#department").append('<option value=shop>Shop</option>'); */
	}else if(hoaid=="4"){
		$("#department").append('<option value=santhanstores>Sansthan stores</option>');
	}
}
function chequeDetails(){
    var payType=$("#paymentType").val();
    if(payType=="cheque"){
    	$("#chequeDetails").show();
    }else{
    	$("#chequeDetails").hide();
    }
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<%if(session.getAttribute("adminId")!=null){ %>

<form name="tablets_Update" method="post" action="expensesInsert.jsp" onsubmit="return validate();">
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Expenses entry</div>
<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%"> <div class="warning" id="dateError" style="display: none;">Please select date.</div>Date<span style="color: red;">*</span></td>
<td>Head account <span style="color: red;">*</span></td>
<td ><div class="warning" id="vendorError" style="display: none;">Please select vendor.</div>Vendor</td>

</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<tr>
<td><input type="text" name="date" id="date" value="<%=currentDate%>" /></td>

<td><select name="head_account_id" id="head_account_id">
<%
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><input type="text" name="vendorId" id="vendorId" value="" onkeyup="vendorsearch()" placeholder="find a vendor here" autocomplete="off"/></td>
</tr>
<tr>
<td><div class="warning" id="indentError" style="display: none;">Please select major head.</div>Major head<span style="color: red;">*</span></td>
<td><div class="warning" id="billError" style="display: none;">Please select minor head.</div>Minor head</td>
<td></td>
</tr>
<tr>
<td><input type="text" name="major_head_id" id="major_head_id" value="" placeholder="find a major head here" autocomplete="off"/></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" value=""  placeholder="find a minor head here" autocomplete="off"/></td>
<td></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list">
<div class="sno1 bgcolr">S.No.</div>
<div class="ven-nme1 bgcolr">Sub Head</div>
<div class="ven-nme1 bgcolr">Narration</div>
<div class="ven-amt1 bgcolr">Amount</div>
<div class="clear"></div>
<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 50; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

<div class="sno1"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1"><input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>')" class="" value="" autocomplete="off"/></div>
<div class="ven-nme1"><input type="text" name="description<%=i%>" id="description<%=i%>"  value=""></input></div>
<div class="ven-nme1">&#8377;<input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" value="" onkeypress="return numbersonly(this, event,true);"></input></div>
<%-- <div class="ven-amt1"><input type="text" name="vat<%=i%>" id="vat<%=i%>"  value="" style="width:50px;"/></div>
<div class="ven-amt1"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  onblur="calculate('<%=i %>')" value="" style="width:50px;"></input></div>
<div class="ven-nme1">&#8377;<input type="text" name="grossAmt<%=i%>" id="grossAmt<%=i%>" readonly="readonly" style="width:80px;"></input></div> --%>
<div class="clear"></div>
</div>
<%} %>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/>
<input type="submit" value="Submit" class="click" style="border:none; float:right; margin:0 380px 0 0"/>
</div>
<!-- <div class="tot-ven">Total<input type="text" name="total" id="total" value="0.00" readonly="readonly" style="width:50px;"/></div> -->



</div>



</div>
</form>

<script>
window.onload=function a(){
		addOption()
	};
</script>
<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>