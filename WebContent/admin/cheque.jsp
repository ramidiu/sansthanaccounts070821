<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
  <style>
    @media print {
      @page {
        size: letter landscape;
		margin: 0cm;
		padding:0cm;
		top:0px; left:0px;
      }
    }
  </style>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
                $( "#printtable" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    
    </script>
    
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
	<!-- main content -->
	<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
		<jsp:useBean id="VEN" class="beans.vendors"></jsp:useBean>
	<div>

		<div class="vendor-page">
		<div class="vendor-list">
				<div class="arrow-down"></div>
				<div class="icons">
			<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
				</div>
				<div class="clear"></div>
				<div class="list-details">
			
			<%
			int i=1;
			DecimalFormat DFF=new DecimalFormat("###.00");
			vendorsListing VNDRL=new vendorsListing();
			double totamount=0.0;
			double venpayngamnt=0.0;
			double tdsamnt=0.0;
			double servtaxamnt=0.0;
			String payID=request.getParameter("payId");
			SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat SDF1=new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat time=new SimpleDateFormat("hh:mm a");
			mainClasses.paymentsListing PAYL=new mainClasses.paymentsListing();
		
			List PAY_DET=PAYL.getMpaymentsBasedOnPaymentInvoice(payID);
			if(PAY_DET.size()>0){
					PAY=(beans.payments)PAY_DET.get(0);		 
				}
			DecimalFormat DF=new DecimalFormat("0");
            NumberToWordsConverter NTW=new NumberToWordsConverter();
			if(request.getParameter("con")!=null && request.getParameter("con").equals("vp")){
				
					if(!PAY.getextra7().equals("")){
           		 		totamount=Double.parseDouble(PAY.getamount())-Double.parseDouble(PAY.getextra7());
					}
                  if(PAY.getremarks().equals("minus")){
            			if(!PAY.getextra5().equals("")){
           				 totamount=totamount-Double.parseDouble(PAY.getextra5());
            			}
           		 } else if(PAY.getremarks().equals("plus")){
            			if(!PAY.getextra5().equals("")){
                    			totamount=totamount+Double.parseDouble(PAY.getextra5());
                    		}
         	     }
			}else if(request.getParameter("con")!=null && request.getParameter("con").equals("tdsc")){
				totamount=Double.parseDouble(PAY.getextra7());
			}
			else if(request.getParameter("con")!=null && request.getParameter("con").equals("servicetax")){
				totamount=Double.parseDouble(PAY.getService_tax_amount());
			}
			List VendorL=VNDRL.getMvendors(PAY.getvendorId());
			if(VendorL.size()>0){
				VEN=(vendors)VendorL.get(0);
			}
                  String amount=DF.format(totamount);
				%>
		    <input type="hidden" name="paymentid" id="sj" value="<%=PAY.getpaymentId()%>" />
		    <input type="hidden" name="tds" id="tds" value="<%=PAY.getextra7()%>"/>
		    <input type="hidden" name="payId" id="payId" value="<%=payID%>"/>
		    	<%if(request.getParameter("q")==null){ %>
		    		<table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
					<tr>
						<td colspan="7" align="center" style="font-weight: bold;">Cheque Print</td>
					</tr>
				</table>			
			<%-- <div class="printable" style="">
			<div style="position:absolute; top:150px; left:220px; font-size:40px;"> <%if(request.getParameter("con")!=null && request.getParameter("con").equals("tdsc")){ %> TDS CHARGES <%} else if(request.getParameter("con")!=null && request.getParameter("con").equals("servicetax")){ %> SERVICE TAX CHARGES <%} else{%> <%=PAY.getpartyName() %>  <%} %> </div>
           		<div style="position:absolute; top:250px; left:380px; font-size:40px; width:auto; height:auto; text-transform:uppercase;"><%=NTW.convert(Integer.parseInt(amount)) %>  only</div>
           		<div style="position:absolute; top:40px; left:1550px;font-size:40px; background:#900; width:300px"><%=CDF.format(SDF.parse(PAY.getdate()))%></div>	
            	<div style="position:absolute; top:300px; left:1550px; font-size:40px; background:#3C0"><%=DFF.format(totamount)%></div>
           	</div> --%>
           	
            <div class="printable" style="">
			<div style="position:absolute; top:170px; left:220px; font-size:20px; font-weight:bold;"> <%if(request.getParameter("con")!=null && request.getParameter("con").equals("tdsc")){ %> TDS CHARGES <%} else if(request.getParameter("con")!=null && request.getParameter("con").equals("servicetax")){ %> SERVICE TAX CHARGES <%} else{%> <%=PAY.getpartyName() %>  <%} %> </div>
           		<div style="position: absolute;top: 210px;left: 260px; font-size: 20px;width: auto;height: auto;text-transform: uppercase;"><%=NTW.convert(Integer.parseInt(amount)) %>  only</div>
           		<div style="position: absolute;top: 95px;left: 920px;font-size: 32px;width: 200px;"><%=CDF.format(SDF.parse(PAY.getdate()))%></div>	
            	<div style="position: absolute;top: 240px;left: 890px;font-size: 32px;width: auto;"><strong><%=DFF.format(totamount)%></strong></div>
           	</div> 
           	
           	<%} else{
           		totamount=Double.parseDouble(PAY.getamount());
           		if(PAY.getextra7()!=null ){
           			tdsamnt=Double.parseDouble(PAY.getextra7());
           		}
           		if(PAY.getService_tax_amount()!=null ){
           			servtaxamnt=Double.parseDouble(PAY.getService_tax_amount());
           		}
           		venpayngamnt=Double.parseDouble(PAY.getamount())-(tdsamnt)-(servtaxamnt);
                if(PAY.getremarks().equals("minus")){
        			if(!PAY.getextra5().equals("")){
        				venpayngamnt=venpayngamnt-Double.parseDouble(PAY.getextra5());
        				 totamount=totamount-Double.parseDouble(PAY.getextra5());
        			}
       		 } else if(PAY.getremarks().equals("plus")){
        			if(!PAY.getextra5().equals("")){
        				venpayngamnt=venpayngamnt+Double.parseDouble(PAY.getextra5());
        				 totamount=totamount+Double.parseDouble(PAY.getextra5());
                		}
     	     }
           		
           		%>
           <div class="printable" style="">
           <table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
					<tr>
						<td colspan="7" align="center" style="font-weight: bold;">SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
				</table>
           	<table width="95%" cellpadding="0" cellspacing="0" border="1" class="pass-order">
                  		<tr>
                            <td>S.No</td>
                            <td>NAME OF THE PARTY</td>
                            <td>NAME OF THE BANK</td>
                            <td>BRANCH NAME</td>
                            <td>ACCOUNT NO</td>
                            <td>IFS CODE</td>
                            <td>AMOUNT</td>
                            </tr>
                                                   <tr>
                            <td>1</td>
                            <td><%=VEN.getagencyName() %></td>
                            <td><%=VEN.getbankName() %></td>
                            <td><%=VEN.getbankBranch() %></td>
                            <td><%=VEN.getaccountNo() %></td>
                            <td><%=VEN.getIFSCcode() %></td>
                            <td>&#8377; <%=venpayngamnt %> /-</td>
                             </tr>
                           <%--  <%if(tdsamnt>0){
                            	i++;
                            %>
                       <tr>
                            <td><%=i %></td>
                            <td>TDS Charges</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>&#8377; <%=tdsamnt %> /-</td>
                             </tr>
            <%} %> --%>
                                          <%if(servtaxamnt>0){ 
                                             	i++;%>
                       <tr>
                            <td><%=i %></td>
                            <td>Service Tax Charges</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>&#8377; <%=servtaxamnt %> /-</td>
                             </tr>
            <%} %>           
                        <tr>
                            <td colspan="6" style="text-align: right;">Total Amount</td>
                         
                            <td>&#8377; <%=venpayngamnt %> /-</td>
                             </tr>            
                
           	</table>
           	<div style="margin-left: 65px;margin-top: 101px;"> Chairman/General Secreatary <div style="text-align:center">Accounts Manager</div><div style="text-align: right;margin-right: 65px;">  Treasurer</div></div>
           <div style="height:140px"></div>
           	<table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
					<tr>
						<td colspan="7" align="center" style="font-weight: bold;">SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
				</table>
           	<table width="95%" cellpadding="0" cellspacing="0" border="1" class="pass-order">
                  		<tr>
                            <td>S.No</td>
                            <td>NAME OF THE PARTY</td>
                            <td>NAME OF THE BANK</td>
                            <td>BRANCH NAME</td>
                            <td>ACCOUNT NO</td>
                            <td>IFS CODE</td>
                            <td>AMOUNT</td>
                            </tr>
                                                   <tr>
                            <td>1</td>
                            <td><%=VEN.getagencyName() %></td>
                            <td><%=VEN.getbankName() %></td>
                            <td><%=VEN.getbankBranch() %></td>
                            <td><%=VEN.getaccountNo() %></td>
                            <td><%=VEN.getIFSCcode() %></td>
                            <td>&#8377; <%=venpayngamnt %> /-</td>
                             </tr>
                            <%-- <%if(tdsamnt>0){
                            	i++;
                            %>
                       <tr>
                            <td><%=i %></td>
                            <td>TDS Charges</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>&#8377; <%=tdsamnt %> /-</td>
                             </tr>
            <%} %>
                          --%>                 <%if(servtaxamnt>0){ 
                                             	i++;%>
                       <tr>
                            <td><%=i %></td>
                            <td>Service Tax Charges</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>&#8377; <%=servtaxamnt %> /-</td>
                             </tr>
            <%} %>           
                        <tr>
                            <td colspan="6" style="text-align: right;">Total Amount</td>
                         
                            <td>&#8377; <%=venpayngamnt %> /-</td>
                             </tr>            
                
           	</table>
           	<div style="margin-left: 65px;margin-top: 101px;"> Chairman/General Secreatary
           	<div style="text-align:center">Accounts Manager</div><div style="text-align: right;margin-right: 65px;">Treasurer </div>
           	</div>
           
           	<div style="margin-left: 65px;margin-top: 60px;"> NARRATION : <%=PAY.getnarration() %></div>
           	<%
	            Date d = SDF.parse(PAY.getdate()); 
	            Calendar cal = Calendar.getInstance();
	            cal.setTime(d);
	            cal.add(Calendar.MINUTE, 330);
	            String newTime = CDF.format(cal.getTime());
	            String newTime2 = SDF.format(cal.getTime());
           	%>
           	<div style="margin-left: 65px;margin-top: 50px;margin-bottom:20px;"> DATE :  <%=newTime%>&nbsp;&nbsp;<%=time.format(SDF.parse(newTime2)) %>
           </div>	

<%} %>
			</div>
			</div>
</div>

</div>
	<%if(request.getParameter("con")!=null && request.getParameter("con").equals("vp")){ %>
 <script type="text/javascript">
     function print()
 	{    	
    	 var id=$("#sj").val();
    	 var tds=$("#tds").val();
    	 var payID=$("#payId").val();
          newwindow1=window.open('adminPannel.jsp?page=PassOrder&payId='+id,'BillImageUpload','height=300,width=800,menubar=yes,status=yes,scrollbars=yes');
          newwindow=window.open('adminPannel.jsp?page=voucher&payId='+id,'name','height=400,width=500,menubar=yes,status=yes,scrollbars=yes');
          if(tds != '0'){
          	newwindow2=window.open('adminPannel.jsp?page=tdsCheque&payId='+payID,'name','height=400,width=500,menubar=yes,status=yes,scrollbars=yes');
          }
		if (window.focus) {newwindow.focus();}
 	}
		window.onload=print();
     </script>
     <%}%>