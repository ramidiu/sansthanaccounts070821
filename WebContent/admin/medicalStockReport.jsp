<%@page import="mainClasses.medical_product_staticListing"%>
<%@page import="mainClasses.medicineissuesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sai sansthan accounts-Admin Pannel</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<script>
$(function() {

	 $( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
	
		 minDate: new Date(2017, 9, 23), 
	 onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	}); 
	/* $(function() {
	    $( "#datepicker" ).datepicker({  maxDate: new Date() });
	  }); */
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	/* 	$("#fromDate").datepicker({ maxDate: new Date, minDate: new Date(2017, 10, 23) }); */
		
});
</script>
<script type="text/javascript">
function submission(){
	
	var fromDate=$("#fromDate").val();
	var toDate=$("#toDate").val();
	if(fromDate==""){
		alert('Please select From Date');
		return false;
	}
	if(toDate==""){
		alert('Please select To Date');
		return false;
	}
	$("#fulltable").show();
	return true;
}
</script>
<script type="text/javascript">
$(
        function(){
	// Hook up the print link.
            $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                    function(){
                        // Print the DIV.
                         $( "a" ).css("text-decoration","none");
                        $( "#tblExport" ).print();

                        // Cancel click event.
                        return( false );
                    });
	});
    $(document).ready(function () {
        $("#btnExport").click(function () {
            $("#tblExport").btechco_excelexport({
                containerid: "tblExport"
               , datatype: $datatype.Table
            });
        });
    });
    function minorhead(id){
    	$('#'+id).toggle();
    }
    </script>
</head>
<body>

<%if(session.getAttribute("adminId")!=null){ %>
<jsp:useBean id="SHP" class="beans.products" />
<%

String inwards="00";
String inwards1="00";
String outwards="00";


productsListing PRD_l=new productsListing ();

List PRODL=PRD_l.getMedicalStoreStock("1");
godwanstockListing GDSTK_L=new godwanstockListing();
medicineissuesListing MDIS_L=new medicineissuesListing();
medical_product_staticListing MPSLST=new medical_product_staticListing();
%>

<div><%@ include file="title-bar.jsp"%></div>
<div class="list-details" >
	<form name="medicalStockReport.jsp" method="post">
	<div class="search-list">
		
					<ul style="margin:80px 0px 220px 400px;">
						<li>
							<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  placeholder="From Date" readonly="readonly" />
						</li>
						<li>
							<input type="text" name="toDate" id="toDate"  class="DatePicker" value="" placeholder="To Date" readonly required/>
						</li>
						<li>
							<input type="submit" class="click" name="search" value="Search" onclick="return submission();"/>
						</li>
					</ul>
			
	</div></form>
	<!-- <div class="icons">
	<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
	<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div> -->
			<%String fromDate=request.getParameter("fromDate");
			String toDate=request.getParameter("toDate"); %>	
	<div class="printable" id="fulltable">
	<table width="95%"  cellspacing="0" cellpadding="0" style="    margin-bottom: 10px; margin-top:10px; margin:auto;">
			<%if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("")){ %>
			<tbody> 
				<tr>
					<td colspan="3" align="center" style="font-weight: bold;color:red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
				</tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
				<tr><td colspan="3" align="center" style="font-weight: bold;">KITCHEN STOCK REPORT(ASSETS)</td></tr>
				 <tr><td colspan="3" align="center" style="font-weight: bold;">Report From Date  <%=fromDate %> TO <%=toDate %> </td></tr> 
			</tbody>
			<%} %>
		</table><br>
			<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border='1'>
				<%if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("")){ %>
				<tr>
					<!-- <td class="bg" width="5%" style="font-weight: bold;">SNO</td> -->
					<td class="bg" width="5%" style="font-weight: bold;">CODE</td>
					<td class="bg" width="20%" style="font-weight: bold;">PRODUCT NAME</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">OPENING BALANCE</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">RECEIVED QUANTITY</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">ISSUED QUANTITY</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">CLOSING BALANCE</td>
					<td class="bg" width="10%" style="font-weight: bold;" align="right">UNITS</td>
				</tr>
				<%
				
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date myDate = dateFormat.parse(fromDate);
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(myDate);
				cal1.add(Calendar.DAY_OF_YEAR, -1);
				Date previousDate = cal1.getTime();
				String previousDate1="";
				TimeZone tz = TimeZone.getTimeZone("IST");
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				previousDate1=dateFormat2.format(previousDate);
				//System.out.println("previous date..."+previousDate1);
				
				
				String closingBal="0";
				String openingbal="0";
				String openingbalExtra="0";
				String staticopeningbal="0";
				double StartingBal=0.0;
				double StartingBal1=0.0;
				double EndingBal=0.0;
				double value=0.0;
				double value1=0.0;
				//String[] totalQuantity=new String[2050];;
				if(fromDate!=null && !fromDate.equals("null") && !fromDate.equals("") && toDate!=null && !toDate.equals("")){
				for(int i=0; i < PRODL.size(); i++ ){
					SHP=(beans.products)PRODL.get(i);
					//totalQuantity=new String[i]; getgodwanstockbyENDDate
					 inwards=GDSTK_L.getgodwanstockbyDateMedicalStock1(fromDate+" 00:00:01",toDate+" 23:23:59",SHP.getproductId()); 
					 inwards1=GDSTK_L.getgodwanstockbyDateMedicalStock2(fromDate+" 00:00:01",toDate+" 23:23:59",SHP.getproductId());
					
					 outwards=MDIS_L.getgMedicinestockbyDate(fromDate+" 00:00:01",toDate+" 23:23:59",SHP.getproductId());
					
					 openingbal=GDSTK_L.getgodwanstockbyDateMedicalStock1("2017-10-23 00:00:01",previousDate1+" 23:23:59",SHP.getproductId()); 
					 openingbalExtra=GDSTK_L.getgodwanstockbyDateMedicalStock2("2017-10-23 00:00:01",previousDate1+" 23:23:59",SHP.getproductId()); 
					 closingBal=MDIS_L.getMedicinestockbyENDDate("2017-10-23 00:00:01",previousDate1+" 23:23:59",SHP.getproductId());
					 staticopeningbal=MPSLST.getMedicalProductStaticQuantity(SHP.getproductId());
					 
					 if(openingbal!=null && !openingbal.equals("") && !openingbal.trim().equals("null")){
						 StartingBal=Double.parseDouble(openingbal);
					 }
					 else{
						 StartingBal=0.0;
					 }
					 
					 if(openingbalExtra!=null && !openingbalExtra.equals("") && !openingbalExtra.trim().equals("null")){
						 StartingBal1=Double.parseDouble(openingbalExtra);
					 }
					 else{
						 StartingBal1=0.0;
					 }
					 StartingBal=StartingBal+StartingBal1;
					 
					 if(closingBal!=null && !closingBal.equals("") && !closingBal.trim().equals("null")){
						 EndingBal=Double.parseDouble(closingBal);
					 }else{
						 EndingBal=0.0;
					 }
					 StartingBal=Double.parseDouble(staticopeningbal)+StartingBal-EndingBal;
					 %>
				<tr>
				    <%-- <td><%=i+1%></td> --%>
					<td><%=SHP.getproductId()%></td>
					<td><%=SHP.getproductName()%></td>
					
					<td><%=StartingBal %></td>
					<%if(inwards!=null && !inwards.equals("null") && !inwards.equals("")){ %>
					<%-- <td><%=inwards%></td> --%>
					<%inwards=inwards;
					}else{ 
					inwards="0";
					}	
					
					if(inwards1!=null && !inwards1.equals("null") && !inwards1.equals("")){ 
						inwards1=inwards1;
					}else{ 
						inwards1="0";
					}
					value=Double.parseDouble(inwards)+Double.parseDouble(inwards1);
					%>
					<td><%=value%></td>
					<!-- <td>0</td> -->
					<%if(outwards!=null && !outwards.equals("null") && !outwards.equals("")){ %>
					<td><%=Double.parseDouble(outwards)%></td>
					<%}else{
					outwards="0";	%>
					<td>0.0</td>
					<%} 
					
					 EndingBal=StartingBal+value-Double.parseDouble(outwards);
					%>
					<td><%=EndingBal %></td>
					<td><%=SHP.getunits()%></td>
					
				</tr>
				<%}}}else{ %>
				
				<%} %>
				
		</table>
	</div>
</div>
<%}else{
	 response.sendRedirect("index.jsp");
} %>
</body>
</html>