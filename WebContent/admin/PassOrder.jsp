<%@page import="beans.indentapprovals"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="beans.godwanstock"%>
<%@page import="beans.majorheadService"%>
<%@page import="beans.subhead"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="beans.billimages"%>
<%@page import="mainClasses.billimagesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.paymentsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
.st{
font-size: 14px !important;
    text-decoration: none !important;
    display: block;
    float: left;
    padding: 83px 0 31px 0;
    text-align: center;
    width: 33%;
}
@media print {
		@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin-left: 24mm; /* equals to 0.8inches */ 
}
.st{
  width: 33%;
font-size: 14px !important;
    text-decoration: none !important;
       padding: 83px 0 31px 0;
    text-align: center;
    display: block;
    float: left;
   
}

	
	table{width:100%; border-bottom:1px solid #727272; border-left:1px solid #727272; position:relative; left:8px; top:0;}
	table.pass-order td { font-size:12px; padding:7px 5px; margin:0 0 0px 0; font-family:Arial, Helvetica, sans-serif; border-top:1px solid #727272; border-right:1px solid #727272; }
	table.pass-order td ul{ margin:0; padding:0px;}
	table.pass-order td ul li{ float:left; list-style:none; margin:0 0 10px 0;}
table.pass-order td ul li.second{ border-bottom:1px solid #727272;  width:150px; padding:0 0 0 10px; margin:0 10px 0 0;}
table.pass-order td span{ text-decoration:none; font-size:12px;}
table.pass-order td table.sub-order{ border-bottom:1px solid #727272; border-left:1px solid #727272; margin:0 0 0px 0;}
table.pass-order td table.sub-order td{ border-top:1px solid #727272; border-right:1px solid #727272; font-size:12px; padding:5px;}
.member-list
{
	margin:5px 0 5px 40px;  
}
p
{
	line-height:25px;
}
	
}
</style>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
 function displayBlock(){
	 $("#mseRep").toggle();
 }
 function billdisplayBlock(){
	 $("#billAtc").toggle();
 }
 
    </script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!-- main content -->
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<jsp:useBean id="EXP" class="beans.productexpenses"></jsp:useBean>
<jsp:useBean id="GOD" class="beans.godwanstock"></jsp:useBean>
<jsp:useBean id="IND" class="beans.indent"></jsp:useBean>
<jsp:useBean id="PO" class="beans.purchaseorder"></jsp:useBean>
<jsp:useBean id="INDAP" class="beans.indentapprovals"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="VEN" class="beans.vendors"></jsp:useBean>
<jsp:useBean id="BIL" class="beans.billimages"></jsp:useBean>
	<div>
		<div class="vendor-page">
		<div class="vendor-list">
		<div class="arrow-down"></div>
			<div class="icons"><a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
			<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
		<%
		double roundofftotal = 0.00;
		String payID=request.getParameter("payId");
		double servicetaxamt,TDSAmnt;
		servicetaxamt=TDSAmnt=0.0;
		  DecimalFormat DFSJ=new DecimalFormat("0.00");
			SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat SDF2=new SimpleDateFormat("yyyy-MM-dd HH:mm");
			SimpleDateFormat SDF1=new SimpleDateFormat("yyyy-MM-dd");
			paymentsListing PAYL=new paymentsListing();
			productexpensesListing EXPL=new productexpensesListing();
			godwanstockListing GODL=new godwanstockListing();
			purchaseorderListing POL=new purchaseorderListing();
			indentListing INDL=new indentListing();
			indentapprovalsListing INDAPL=new indentapprovalsListing();
			List PAY_DET=PAYL.getPassOrderInfo(payID);
			List EXP_Det=null;
			List GOD_DET=null;
			List PO_DET=null;
			List IND_DET=null;
			List INDAP_DET=null;
			if(PAY_DET.size()>0){
				PAY=(beans.payments)PAY_DET.get(0);
				 EXP_Det=EXPL.getMproductexpensesBasedOnExpInvoiceID(PAY.getextra3());
				 if(EXP_Det.size()>0){
					 EXP=(beans.productexpenses)EXP_Det.get(0);
					 GOD_DET=GODL.getgodwanstockByinvoiceId(EXP.getextra5());
					 if(GOD_DET.size()>0){
						 GOD=(beans.godwanstock)GOD_DET.get(0);
						 PO_DET=POL.getMpurchaseorderBasedONPoINVID(GOD.getextra2());
						 if(PO_DET.size()>0){
							 PO=(beans.purchaseorder)PO_DET.get(0);
							 IND_DET=INDL.getMindentsByIndentinvoice_id(PO.getindentinvoice_id());
							 if(IND_DET.size()>0){
								 IND=(beans.indent)IND_DET.get(0);
							 }
						 }
					 }
				 }
			}
			headofaccountsListing HODL=new headofaccountsListing();
            majorheadListing MJHDL=new majorheadListing();
            minorheadListing MINL=new minorheadListing();
            subheadListing SUBL=new subheadListing();
            superadminListing SADL=new superadminListing();
            productsListing PROL=new productsListing();
            INDAP_DET=INDAPL.getIndentDetails(IND.getindentinvoice_id());
            SimpleDateFormat time=new SimpleDateFormat("HH:mm");
            employeesListing EMPL=new employeesListing();
            if(!"".equals(PAY.getService_tax_amount()) && Double.parseDouble(PAY.getService_tax_amount())>0){
            	 servicetaxamt=Double.parseDouble(PAY.getService_tax_amount());
            }
            if(!"".equals(PAY.getextra7()) && Double.parseDouble(PAY.getextra7())>0){
            	TDSAmnt=Double.parseDouble(PAY.getextra7());
            }
           
			%>
		<div class="printable">
			<table width="100%" cellpadding="0" cellspacing="0" class="pass-order">
				<tr>
					<td colspan="7" align="center"> 
             <div style="font-size:16px; font-weight:bold; margin:0 0 3px 0;">SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%=HODL.getHeadofAccountName(EXP.gethead_account_id())%></div>
				
              <div style="font-size:11px; font-weight:bold;  margin:0 0 3px 0;">( Regd.No.646/92 )</div>
				<div style="font-size:13px; font-weight:bold;">Dilsukhnagar, Hyderabad,TS-500 060, Ph:24066566</div></td>
                </tr>
				<tr><td colspan="7" align="center" style="font-weight: bold; font-size:18px;">PASS ORDER</td></tr>
				<tr>
                    <td colspan="7" >
                    <ul>
						 <li>Pass Order NO :</li>
 						 <li class="second"><%=payID %></li>
 						 <li>On</li>
 						<li class="second"><%=CDF.format(SDF.parse(PAY.getdate()))%></li>
					</ul></td>
                 </tr>
                <%if(!GOD.getextra2().equals("")&&GOD.getExtra6()!=null&&!GOD.getExtra6().equals("BillApproveNotRequire")){ %>
                 <tr>
                    <td colspan="7" ><ul><li>Indent NO : </li>   <li class="second"><%=IND.getindentinvoice_id()%></li>
                   <li>On</li> <li class="second"><%=CDF.format(SDF.parse(IND.getdate()))%></li>  </ul> </td>
                 </tr>
                  <tr>
                    <td colspan="7" height="30">
                    <div>Indent Approved by:&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    	<%if(INDAP_DET.size()>0){ 
                    		for(int i=0;i<INDAP_DET.size();i++){
                    		INDAP=(beans.indentapprovals)INDAP_DET.get(i);
                    		%>
                   		 <div class="member-list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=i+1 %>&nbsp;.&nbsp;<%=SADL.getSuperadminname(INDAP.getadmin_id()) %>&nbsp;&nbsp; <span style="font-size:11px; text-decoration:none;">( <%=SADL.getSuperadminnameRole(INDAP.getadmin_id()) %> )</span>  On&nbsp;&nbsp;<u><%=CDF.format(SDF.parse(INDAP.getapproved_date())) %></u>&nbsp;&nbsp;at&nbsp;&nbsp;<u><%=time.format(SDF.parse(INDAP.getapproved_date())) %></u></div>
                   		<%}} %>
                   	</td>
                  </tr>	
                  <tr>
                    <td colspan="7" ><ul>
				<li>Purchase Order NO : </li>   <li class="second"><%=PO.getpoinv_id() %></li>
                    <li>On</li><li class="second"><%=CDF.format(SDF.parse(PO.getcreated_date()))%></li>
				</ul></td>
                 </tr><%} %>
                 <tr>
                    <td colspan="7" >
                    	<ul>
                    		<li >Unique No : </li>   
                    		<li class="second" onclick="displayBlock();" style="cursor:pointer;"><%=GOD.getExtra9()%></li>
                    	</ul>
                    	<!-- added by gowri shankar -->
                    	<%if(!GOD.getExtra25().trim().equals("")) {%>
                    	<ul>
                    		<li>Daily Challan No : </li>   
                    		<li class="second" onclick="displayBlock();" style="cursor:pointer;"><%=GOD.getExtra25()%></li>
                    	</ul>
                    	<%}%>
                    	<!-- added by gowri shankar -->
                    </td>
                 </tr>
                 <tr>
                 	<td colspan="7">
                 		<div style="display: block;" id="mseRep"><table width="100%" cellpadding="0" cellspacing="0" class="sub-order" border="0" > 
                 			<tr>
                            <td colspan="8">Stock ledger Entry No : <%=GOD.getextra1()%>
                            On <%if(GOD.getdate()!=null && !GOD.getdate().equals("")){%><%=CDF.format(SDF1.parse(GOD.getdate()))%> <%}else{ %> N/A<%} %>
                            </td>
                 			</tr>
                 			<tr>
                            <td>Product Id</td>
                            <td>Name</td>
                            <td>Qty</td>
                            <td>Price</td>
                            <!-- <td>Vat</td> -->
                            <td>CGST</td>
                            <td>SGST</td>
                            <td>IGST</td>
                            <td>Tot Amt</td>
                            </tr>
                 			<%
                 			double totAmt=0.00;
                 			double othercharges=0.00;
                 			StringBuilder productID= new StringBuilder("");
                 			if(GOD_DET.size()>0){
                 				for(int i=0;i<GOD_DET.size();i++){
       						 GOD=(beans.godwanstock)GOD_DET.get(i);
       						//totAmt=totAmt+Math.round(Double.parseDouble(GOD.getquantity())*Double.parseDouble(GOD.getpurchaseRate()));
       						if(GOD.getExtra18()!=null && !GOD.getExtra18().equals("null") && !GOD.getExtra18().equals("") && GOD.getExtra19()!=null && !GOD.getExtra19().equals("null") && !GOD.getExtra19().equals("") && GOD.getExtra20()!=null && !GOD.getExtra20().equals("null") && !GOD.getExtra20().equals("")){
       							totAmt=totAmt+Math.round(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())+Double.parseDouble(GOD.getExtra18())+Double.parseDouble(GOD.getExtra19())+Double.parseDouble(GOD.getExtra20())));	
       							}else{
       								totAmt=totAmt+Math.round(Double.parseDouble(GOD.getquantity())*(Double.parseDouble(GOD.getpurchaseRate())));	
       							}
       						 %>
       						 <tr>
                             <td><%=GOD.getproductId() %></td>
                             <%if(GOD.getExtra7()!=null&&GOD.getExtra7().equals("serviceBill")){ %>
								<td> <%=SUBL.getMsubheadnameWithDepartment(GOD.getproductId(),GOD.getdepartment())%></td>
							<%}else{ 
								productID.append(PROL.getProductsNameByCat(GOD.getproductId(),GOD.getdepartment()));
								if(!productID.toString().equals("")){
							%>
								<td> <%=productID.toString()%></td><%} else{
									productID.append(PROL.getProductsNameByCat1(GOD.getproductId(),""));
								%>
								<td> <%=productID.toString()%></td>
									<%}productID.delete( 0, productID.length() );}%>
								<td><%=GOD.getquantity() %></td>
                             <td><%=GOD.getpurchaseRate() %></td>
                            <%--  <td><%=GOD.getvat() %></td> --%>
                             
                              
                              <%if(GOD.getExtra18()!=null && !GOD.getExtra18().equals("null") && !GOD.getExtra18().equals("") && GOD.getExtra19()!=null && !GOD.getExtra19().equals("null") && !GOD.getExtra19().equals("") && GOD.getExtra20()!=null && !GOD.getExtra20().equals("null") && !GOD.getExtra20().equals("")){ %>
<td><%=GOD.getExtra18() %></td>
<td><%=GOD.getExtra19() %></td>
<td><%=GOD.getExtra20() %></td>
<td><%=(Math.round(Double.parseDouble(GOD.getquantity())*((Double.parseDouble(GOD.getpurchaseRate())+Double.parseDouble(GOD.getExtra18())+Double.parseDouble(GOD.getExtra19())+Double.parseDouble(GOD.getExtra20()))))) %></td>
<%}else{ %>
<td>0</td>
<td>0</td>
<td>0</td>
<td><%=Math.round(Double.parseDouble(GOD.getquantity())*Double.parseDouble(GOD.getpurchaseRate()))%></td>
<%} %>
                           <%--   <td><%=Math.round((Double.parseDouble(GOD.getquantity()))*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100)))%></td> --%>
                             <%-- <td><%=Math.round(Double.parseDouble(GOD.getquantity())*Double.parseDouble(GOD.getpurchaseRate()))%></td> --%>
                             </tr>
       						 <%} }
                 			if(GOD.getExtra8()!=null && !GOD.getExtra8().equals("") && !GOD.getExtra8().equals("0")){
                 				othercharges=Double.parseDouble(GOD.getExtra8());
                 				totAmt=totAmt+Double.parseDouble(GOD.getExtra8());
                 				%>
                 				 	 <tr><td colspan="7" align="right">Other Charges</td>
                             <td><%=othercharges%></td></tr><%}%>
       						
       						<!-- these lines are added by srinivas on 6/9/2016 -->
                   		<%if(PAY.getextra6().equals("minus")){
                  			if(!PAY.getextra5().equals("")){%>
                  			<tr><td colspan="7" align="right">Round Off Total Subtracted</td>
                             <td><%=PAY.getextra5()%></td></tr>
                  			<% 
                  			roundofftotal = -Double.parseDouble(PAY.getextra5());
                  				totAmt=totAmt-Double.parseDouble(PAY.getextra5());
                  			}
                 		 } else if(PAY.getextra6().equals("plus")){
                  			if(!PAY.getextra5().equals("")){%>
                  			<tr><td colspan="7" align="right">Round Off Total Added</td>
                             <td><%=PAY.getextra5()%></td></tr>
                  			<% 
                  			roundofftotal = Double.parseDouble(PAY.getextra5());
                  				totAmt=totAmt+Double.parseDouble(PAY.getextra5());
                          		}
               	     	}
    				%>
       						
       						 <tr><td colspan="7" align="right"><%if(TDSAmnt>0 || servicetaxamt>0){ %>(<%if(TDSAmnt>0){%>TDS : <%=TDSAmnt%><%}if(servicetaxamt>0){%> + service Tax: <%=servicetaxamt %><%} %>)
       						 <%} %>
       						 Total Amount</td>
       						  
                             <td><%=totAmt%></td></tr>
                 		</table></div>
                 	</td>
                 </tr>
				<tr>
                    <td colspan="7" >
                    <ul>
                    <li>Bill NO : </li>
                    <li onclick="billdisplayBlock();" class="second" style="cursor:pointer;"><%=GOD.getbillNo() %></li>
                    <li>On</li>
                    <li class="second"><%if(GOD.getdate()!=null && !GOD.getdate().equals("")){%><%=CDF.format(SDF1.parse(GOD.getdate()))%><%}else{ %> N/A<%} %></li> 
                    </ul>
                    </td>
                 </tr>
                <%billimagesListing BILIMG=new  billimagesListing();
                List Bil_Det=BILIMG.getListImage(GOD.getextra1());%>
                 <%if(Bil_Det.size()>0){ %>
                <tr>
                <td colspan="7">
                 <div  id="billAtc" style="display: block;"><table  width="100%" cellpadding="0" cellspacing="0" class="sub-order" border="1">
                 <tr>
                    <td colspan="6" style="border:none"><div style="float: left;">Scanned Bills Attached Documents,No of enclosures <u><%=Bil_Det.size()%></u></div>  </td>
                  </tr>
                  <tr>
                     
                    <td colspan="6" align="left" style="border:none">
                    	<%for(int i=0;i<Bil_Det.size();i++){
                    	BIL=(billimages)Bil_Det.get(i);
                    	%>
                    	<div style="margin:0 0 0 80px"><%=i+1 %>.<a href="../BillImages/<%=BIL.getextra1()%>" target="_blank">DOC<%=i+1 %></a></div>
                    	<%} %>
                      </td>
                 </tr>
                </table></div>
                 </td>
                 </tr>
                 <%} else{ %>
                     <tr>
                <td colspan="7">
                 <div  id="billAtc" style="display: block;"><table  width="100%" cellpadding="0" cellspacing="0" class="sub-order" border="1">
                 <tr>
                    <td colspan="6" style="border:none"><div style="float: left;">No,attachments found</div>  </td>
                  </tr>
               
                </table></div>
                 </td>
                 </tr>
                 <%} %>
                 <tr>
                    <td colspan="7">
                    <%/* System.out.println("get date.."+GOD.getdate());
                    String newDate=GOD.getdate();
                    Date d = time.parse(time.format(SDF.parse(newDate)));
                   Calendar gc = new GregorianCalendar();
                    gc.setTime(d);
                    gc.add(Calendar.MINUTE, 330);
                    Date d2 = gc.getTime();
                    String d3= time.format(d2); */
                    %>
                    <div class="member-list"><p>Stock Received by <u><%=EMPL.getMemployeesName(GOD.getemp_id()) %></u>&nbsp;<span style="font-size:11px; text-decoration:none;">( <%=EMPL.getMemployeesRole(GOD.getemp_id()) %> )</span> On&nbsp;<%if(GOD.getdate()!=null && !GOD.getdate().equals("")){%><%= CDF.format( SDF.parse(GOD.getdate())  )%>&nbsp;at&nbsp;<%=time.format(SDF.parse(GOD.getdate()))%><%}else{ %> N/A<%} %>
                    </p>
                    </div>
                      <%
                   /*  String newDate1=GOD.getCheckedtime();
                      Date date1 = time.parse(time.format(SDF.parse(newDate1)));
                   Calendar gc1 = new GregorianCalendar();
                    gc1.setTime(date1);
                    gc1.add(Calendar.MINUTE, 330);
                    Date date2 = gc1.getTime();
                    String date3= time.format(date2); */
                    %>
                    <div class="member-list"><p>Stock Certified by <%if(EMPL.getMemployeesName(GOD.getCheckedby())!=null&&!EMPL.getMemployeesName(GOD.getCheckedby()).equals("")&&!EMPL.getMemployeesName(GOD.getCheckedby()).equals(EMPL.getMemployeesName(GOD.getemp_id()))){%><u><%=EMPL.getMemployeesName(GOD.getCheckedby()) %></u>&nbsp;<span style="font-size:11px; text-decoration:none;">( <%=EMPL.getMemployeesRole(GOD.getCheckedby()) %> )</span> On&nbsp;<%if(GOD.getCheckedtime()!=null && !GOD.getCheckedtime().equals("")){%><%=CDF.format(SDF.parse(GOD.getCheckedtime()))%>&nbsp;at&nbsp;<%=time.format(SDF.parse(GOD.getCheckedtime()))%><%}else{ %> N/A<%} %>
                    <%}else{ %><u><%=EMPL.getMemployeesName(GOD.getBill_update_by()) %></u>&nbsp;<span style="font-size:11px; text-decoration:none;">( <%=EMPL.getMemployeesRole(GOD.getBill_update_by()) %> )</span>On&nbsp;<%if(GOD.getBill_update_time()!=null && !GOD.getBill_update_time().equals("")){%><%=CDF.format(SDF.parse(GOD.getBill_update_time()))%>&nbsp;at&nbsp;<%=time.format(SDF.parse(EXP.getentry_date()))%><%}else{ %> N/A<%} %><%} %>
                     </p>
                     </div>
                     </td>
                    
                 </tr>
                <%--  <%if(GOD.getExtra6()!=null&&GOD.getExtra6().equals("BillApproveNotRequire")){ %> --%>
                 <%if(GOD.getExtra6()!=null&&!GOD.getExtra6().equals("BillApproveNotRequire")){ %> 
                  <tr>
                    <td colspan="7">
                    <div>Bill Approved by:&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    	<%
                    	 INDAP_DET=INDAPL.getIndentDetails(GOD.getextra1());
                    	if(INDAP_DET.size()>0){ 
                    		for(int i=0;i<INDAP_DET.size();i++){
                    		INDAP=(beans.indentapprovals)INDAP_DET.get(i);
                    		
                    	
                    		%>
                   		 <div class="member-list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=i+1 %>&nbsp;.&nbsp;<%=SADL.getSuperadminname(INDAP.getadmin_id()) %>&nbsp;&nbsp; <span style="font-size:11px; text-decoration:none;">( <%=SADL.getSuperadminnameRole(INDAP.getadmin_id()) %> )</span> On <u><%=CDF.format(SDF.parse(INDAP.getapproved_date())) %></u> at <u><%=time.format(SDF.parse(INDAP.getapproved_date())) %></u></div>
                   		<%}} %>
                   	</td>
                  </tr>
                  <%} %>
                 <!--<tr><td colspan="7" height="35"></td></tr>-->
                  <tr>
                 <td colspan="7" style="padding:0px; border: none">
                 <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px; left:0px; border: none">
                   <%--  <tr>
                    <td style="font-weight: bold;"><u>DEBIT</u></td>
                    </tr>
                    <tr>
                    <td>
                    <span class="floatleft">Head of Account : </span>   
                    <span class="head-act"><%=EXP.gethead_account_id()%> <%=HODL.getHeadofAccountName(EXP.gethead_account_id())%></span>
                    </td>
                    </tr>
                    
                   <tr>
                     <td>
                     <span class="floatleft">Major Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span>
                     <span class="head-act"><%=EXP.getmajor_head_id()%> <%=MJHDL.getmajorheadName(EXP.getmajor_head_id())%></span>   
                     </td>
                     </tr>
                <tr>
                    <td>
                    <span class="floatleft">Minor Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span>
                    <span class="head-act"><%=EXP.getminorhead_id()%> <%=MINL.getminorheadName(EXP.getminorhead_id())%></span>
                    </td>
                    </tr> --%>
                    <tr>
                    <td>
                    <table width="100%" cellpadding="0" cellspacing="0">
                   <!--  <tr>
                    <td></td>
                    <td colspan="4">Budget</td>
                    </tr> -->
                    <tr style="font-weight: bold;">
                   <!--  <td>Sub Head </td> -->
                   <td>Head of Account | Major Head | Minor Head | Sub Head | Product Id</td>
                    <td>Budget Estimate</td>
                    <td>Previous Expenditure</td>
                    <td>Present Expenditure</td>
                    <td>Budget Available</td>
                    </tr>
                    
                    <%String sunHead1="";
                    String sunHead2="";
                    double budgetExpenditure=0.00;
                    double preExpenditure=0.00;
                    double prevExpenditure=0.00;
                    double currExpenditure=0.00;
                  
                    subhead SUBH = new subhead();
                    ArrayList<String> subHead = new ArrayList<String>();
                    int size=1;
                    EXP_Det=EXPL.getMproductexpensesBasedOnExpInvoiceID(PAY.getextra3());
                    if(EXP_Det.size()>0){
                    	for(int i=0;i<EXP_Det.size();i++){
   					    EXP=(beans.productexpenses)EXP_Det.get(i);
   					    	sunHead1=EXP.getsub_head_id();
   					    	if(i==0 || !sunHead2.equals(sunHead1) ){
   					    		if(SUBL.getSubHeadExpenditure(sunHead1)!=null && !SUBL.getSubHeadExpenditure(sunHead1).equals("")){
   					    			budgetExpenditure=Double.parseDouble(SUBL.getSubHeadExpenditure(sunHead1));}
   					    		if(EXPL.getPresentExpenditure(PAY.getextra3(),sunHead1,"Payment Done",SDF1.format(SDF.parse(EXP.getentry_date())),"present")!=null && !EXPL.getPresentExpenditure(PAY.getextra3(),sunHead1,"Payment Done",SDF1.format(SDF.parse(EXP.getentry_date())),"present").equals(""))
   					    			preExpenditure=Double.parseDouble(EXPL.getPresentExpenditure(PAY.getextra3(),sunHead1,"Payment Done",SDF1.format(SDF.parse(EXP.getentry_date())),"present"));
   					    		if(EXPL.getPresentExpenditure(PAY.getextra3(),sunHead1,"Payment Done",SDF1.format(SDF.parse(EXP.getentry_date())),"previous")!=null && !EXPL.getPresentExpenditure(PAY.getextra3(),sunHead1,"Payment Done",SDF1.format(SDF.parse(EXP.getentry_date())),"previous").equals(""))
   					    			prevExpenditure=Double.parseDouble(EXPL.getPresentExpenditure(PAY.getextra3(),sunHead1,"Payment Done",SDF1.format(SDF.parse(EXP.getentry_date())),"previous"));
								String hoaName = HODL.getHeadofAccountName(EXP.gethead_account_id());
								String majName = MJHDL.getMajorHeadNameByCat1(EXP.getmajor_head_id(), EXP.gethead_account_id());
								String minName = MINL.getMinorHeadNameByCat1(EXP.getminorhead_id(), EXP.gethead_account_id());
								String subheadName = "";
								String prodName = "";
								String prodId = "";
								String head_id="";

   					    		List SUBLST = SUBL.getsubheadBasedOnHeads(sunHead1,EXP.getminorhead_id(),EXP.getmajor_head_id(),EXP.gethead_account_id());
					    		if(SUBLST.size() > 0)
					    		{
					    			SUBH = (subhead)SUBLST.get(0);
					    			subheadName = SUBH.getname();
					    		}
   					    		%>
   					    		 <tr>
   					    			<%-- <td><div><%=i+1%>.&nbsp;&nbsp;<%=sunHead1%> <%=SUBL.getMsubheadname(sunHead1) %></div></td> --%>
   					    			 <td><div><%=i+1%>)&nbsp;&nbsp;<%=EXP.gethead_account_id()%> <%=hoaName%> | <%=EXP.getmajor_head_id()%> <%=majName%> | <%=EXP.getminorhead_id()%> <%=minName%> | <%=sunHead1%> <%=subheadName%></div></td> 
   					    			
   					    			<td style="vertical-align:top"><%=budgetExpenditure %></td>
                   					<td style="vertical-align:top"><%=DFSJ.format(prevExpenditure) %> </td>
                  					<td style="vertical-align:top"><%=DFSJ.format(preExpenditure) %></td> 
                  					<%-- <td style="vertical-align:top"><%=DFSJ.format(preExpenditure+(roundofftotal)) %></td> --%>
                   			   	    <td style="vertical-align:top"><%=DFSJ.format(budgetExpenditure-(preExpenditure+prevExpenditure)) %></td> 
                   			   	    <%-- <td style="vertical-align:top"><%=DFSJ.format(budgetExpenditure-(preExpenditure+prevExpenditure+(roundofftotal))) %> --%>
                   			   	    </td>
                   			    </tr>
   					    		<%
   					    	} else{
   					    		size=size+1;
   					    	}
   					    	sunHead2=sunHead1;
   					    	
   					   }}%>
   					   <%-- <span>(<%=size %>)</span> --%>
                   
               				
                    
                 
				</table></td></tr>
                    </table>
                    </td>
                  </tr>
                  
                  <tr>
                    <td colspan="7" style="vertical-align:top;"><div style="float: left;">Being: </div>   <div style="text-decoration:underline; float: left; padding:0 0 0 10px;width: auto;"><%=PAY.getnarration()%></div></td>
                  </tr>
                  <tr>
                    <td colspan="4">T.B/E.C Resulation No : <%if(PAY.getTBECResolutionNO()!=null && !PAY.getTBECResolutionNO().equals("")){ %><u><%=PAY.getTBECResolutionNO() %></u><%} else{ %>N/A<%} %></td>
                    <td colspan="3">Date : <%if(PAY.getTBECDate()!=null && !PAY.getTBECDate().equals("")){ %><u><%=CDF.format(SDF1.parse(PAY.getTBECDate())) %></u><%} else{ %>N/A<%} %></td>
                  </tr>
                 	<tr>
                    	<td colspan="7" >
                        <p>
                         <%-- <%
                    String AccountDate=EXP.getentry_date();
                         
                    Date AD = time.parse(time.format(SDF.parse(AccountDate)));
                   Calendar account = new GregorianCalendar();
                   account.setTime(AD);
                   account.add(Calendar.MINUTE, 330);
                    Date AD1 = account.getTime();
                    String AD2= time.format(AD1);
                    System.out.println("enter by...payment.."+EXP.getEnter_by());
                    %> --%>
                    Payment Entered By <u><%if(EXP.getEnter_by()!=null&&!EXP.getEnter_by().equals("")){%><%=EMPL.getMemployeesName(EXP.getEnter_by()) %><%}else{ %><%=EMPL.getMemployeesName(EXP.getemp_id()) %><%} %></u>&nbsp;<span style="font-size:11px; text-decoration:none;">(<%if(EMPL.getMemployeesRole(EXP.getEnter_by())!=null&&!EMPL.getMemployeesRole(EXP.getEnter_by()).equals("")){ %> <%=EMPL.getMemployeesRole(EXP.getEnter_by()) %><%}else{ %><%=EMPL.getMemployeesRole(EXP.getemp_id())%><%} %> )</span> On&nbsp;<%=CDF.format(SDF.parse(EXP.getentry_date()))%>&nbsp;at&nbsp;<%=time.format(SDF.parse(EXP.getentry_date()))%><br />
                    Payment verified By <u><%=EMPL.getMemployeesName(EXP.getemp_id()) %></u>&nbsp;&nbsp;<span style="font-size:11px; text-decoration:none;">( <%=EMPL.getMemployeesRole(EXP.getemp_id()) %> )</span>On&nbsp;<%=CDF.format(SDF.parse(EXP.getentry_date()))%> at <%=time.format(SDF.parse(EXP.getentry_date()))%>
                        </p>
                        </td>
                    	 
                 	</tr>
                 	 <tr>
                    <td colspan="7">
                    <div>Payments Approved by :&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    	<%
                    	 INDAP_DET=INDAPL.getIndentDetails(EXP.getexpinv_id());
                    	if(INDAP_DET.size()>0){ 
                    		for(int i=0;i<INDAP_DET.size();i++){
                    		INDAP=(beans.indentapprovals)INDAP_DET.get(i);
                    		
                    		
                    		%>
                   		 <div class="member-list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=i+1 %>&nbsp;.&nbsp;<%=SADL.getSuperadminname(INDAP.getadmin_id()) %>&nbsp;&nbsp; <span style="font-size:11px; text-decoration:none;">( <%=SADL.getSuperadminnameRole(INDAP.getadmin_id()) %> )</span> On <u><%=CDF.format(SDF.parse(INDAP.getapproved_date())) %></u> at <u><%=time.format(SDF.parse(INDAP.getapproved_date())) %></u></div>
                   		<%}} %>
                   	</td>
                  </tr>
                  <%vendorsListing VENL=new vendorsListing();
                  List Ven_Det=VENL.getMvendors(PAY.getvendorId());
                  if(Ven_Det.size()>0){
                	  VEN=(beans.vendors)Ven_Det.get(0);
                  }
                  bankdetailsListing BNKL=new bankdetailsListing();
                  
                  %>
                  <tr>
                    <td colspan="7">Payment may be debited to&nbsp;:</td>
                  </tr>
                  <tr>
                  <td colspan="7">
                  <table width="100%" cellpadding="0" cellspacing="0" class="sub-order">
                  <tr>
                    
                    <td width="65%">A/C Name &nbsp;: M/S <%=VEN.getagencyName() %></td>
                    <td > Voucher No : <%=PAY.getreference() %></td>
                    </tr>
                  
                  <tr>
                    <td>A/C No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;<%=VEN.getaccountNo() %></td>
                    <td>Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <%=CDF.format(SDF.parse(PAY.getdate()))%></td>
                  </tr>
                  <tr>
                   <td colspan="2">IFSC Code&nbsp;:&nbsp; <%=VEN.getIFSCcode() %></td>
                  </tr>
                  <tr>
                    <td colspan="2">Branch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <%=VEN.getbankBranch() %></td>
                  </tr>
                  <tr>
                   <td colspan="2">Mode of Payment&nbsp; <%if(PAY.getpaymentType().equals("cashpaid")){ %>Cash <%}else{ %>Cheque <%} %></td>
                  </tr>
                  </table>
                  </td>
                  </tr>
                  <%
             		DecimalFormat DF=new DecimalFormat("0");
                  DecimalFormat DF1=new DecimalFormat("0");
                    NumberToWordsConverter NTW=new NumberToWordsConverter();
                    String amount=DF.format(Double.parseDouble(PAY.getamount())); %>
                    <tr>
                    <td colspan="7">
                   <p>
                    The Bill No . <u><%=GOD.getbillNo() %></u>&nbsp; on <%if(GOD.getdate()!=null && !GOD.getdate().equals("")){%><u><%=CDF.format(SDF1.parse(GOD.getdate()))%><%}else{ %> N/A<%} %></u> was approved and passed for payment. The payment was made via <u><%=BNKL.getBankName(PAY.getbankId())%></u> , Cheque No. <%=PAY.getchequeNo() %> On <u><%=CDF.format(SDF.parse(PAY.getdate()))%></u> 
                    for Rs <u><%=DF1.format(Double.parseDouble(PAY.getamount())+(roundofftotal)) %></u> ( in words Rupees <u> <%=NTW.convert(Integer.parseInt(amount)+(int)(roundofftotal)) %></u> only) through voucher no <u><%=PAY.getreference() %></u>  on <u> <%=CDF.format(SDF.parse(PAY.getdate()))%></u>
                   </p>
                   </td>
                    </tr>
                    <tr>
                    <td colspan="9">
                    	<span class="st">Accountant</span>
                    	<span class="st">Manager (Finance)</span>
                    	<span class="st">Treasurer</span>
                    </td>
                    <%-- <td width="60%" colspan="4">
						<table border="1" style="margin:0 auto;" rules="all">
                    		<%
                    		List<indentapprovals> lst = INDAPL.getBillApprovalsBsdOnUniqueNo(GOD.getExtra1());%>
                    		<tr>
                    		<%for (int i = 0 ; i < lst.size() ; i++)	{
                    		indentapprovals ip = lst.get(i);
                    		%>
                    			<td>Time:<%=SDF2.format(SDF.parse(ip.getapproved_date()))%><br><%=ip.getextra2()%><br><%=ip.getextra3()%></td>
                    		<%}%>
                    		</tr>
                    	</table>
					</td> --%>
                    </tr>
                   
				</table>
			</div>
			</div>
		</div>
</div>
</div>