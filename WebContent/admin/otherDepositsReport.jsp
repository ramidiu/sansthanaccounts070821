<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.banktransactionsListing,java.util.Date"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>

<script>
$(document).ready(function(){
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		} 
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		} 
	});
});
});
</script>

<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                	
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    
    
     <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>


<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>

<jsp:useBean id="BNK" class="beans.bankdetails"/>
<jsp:useBean id="BKT" class="beans.banktransactions"/>
<jsp:useBean id="NUM" class="beans.NumberToWordsConverter"/>
<jsp:useBean id="BNKBAL" class="beans.bankbalance"/>


<div class="vendor-list">
<div class="clear"></div>
<div style="height:30px;"></div>
			
			<form id="searchForm" action="adminPannel.jsp" method="post">
				<input type="hidden" name="page" value="<%=request.getParameter("page")%>">
				
				<div class="search-list" style="margin-left:300px;">
					<ul>
						<li><input type="text"  name="fromdate" id="fromDate" class="DatePicker" value=""  readonly="readonly" placeholder="From Date"/></li>
						<li><input type="text" name="todate" id="toDate"  class="DatePicker" value="" readonly="readonly" placeholder="To Date"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div>
				
				</form>
				
<div class="icons">
<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
</div>
<%if(request.getParameter("fromdate") != null && !request.getParameter("fromdate").equals("") && request.getParameter("todate") != null && !request.getParameter("todate").equals("")){%>

<div class="clear"></div>
<div class="list-details" style="margin:10px 10px 0 0">
<div class="printable" style="margin: 0 0 50px 0;">
<table width="100%" border="1" cellpadding="0" cellspacing="0" id="tblExport">

<%


    DecimalFormat decimalFormat=new DecimalFormat("0.00");
	String fromDate = request.getParameter("fromdate")+" 00:00:01";
	String toDate=request.getParameter("todate")+" 23:59:59";
	
	SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy");
	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	banktransactionsListing BAK_L=new mainClasses.banktransactionsListing();
	bankdetailsListing BDL = new mainClasses.bankdetailsListing();
	
	List OtherDeposits = BAK_L.getOtherDepositsBankTransaction(fromDate,toDate);
	
	%>
	
	<tr>
						<td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<%-- <tr><td colspan="7" align="center" style="font-weight: bold;"><%if(request.getParameter("reportType")!=null){if(request.getParameter("reportType").equals("cashpaid")){ %> Petty Cash <%}else{ %> Bank <%}}else{ %> Bank <%} %>Payments Reports</td></tr> --%>
					<tr><td colspan="8" align="center" style="font-weight: bold;">Report From Date <%=CDF.format(dateFormat.parse(fromDate))%>  TO  <%=CDF.format(dateFormat.parse(toDate)) %> </td></tr>
<tr>
<td class="bg" width="10%">DATE</td>
<td class="bg" width="10%">BKT ID</td>
<td class="bg" width="30%">BANK</td>
<td class="bg" width="30%">NARRATION</td>
<td class="bg" width="10%">AMOUNT</td>
<td class="bg" width="10%">AMOUNT DEPOSITED BY</td>
<td class="bg" width="10%">HEAD OF ACCOUNT</td>
</tr>
<%	
	if(OtherDeposits.size()>0){
	for(int i=0;i<OtherDeposits.size();i++){
	BKT=(beans.banktransactions)OtherDeposits.get(i);	
	
%>
<tr>
<td><%=CDF.format(dateFormat.parse(BKT.getdate())) %></td>
<td><%=BKT.getbanktrn_id() %></td>
<td><%=BKT.getbank_id()%> <%=BDL.getBankName(BKT.getbank_id()) %></td>
<td><%=BKT.getnarration() %></td>
<td><%=decimalFormat.format(Double.parseDouble(BKT.getamount()))%></td>
<td><%=BKT.getextra4()%></td>
<td><%=BKT.getHead_account_id()%></td>
</tr>	
	<%}} %>
	<%} %>
</table>
</div>
</div>
</div>
