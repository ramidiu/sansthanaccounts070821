
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.AdvanceService"%>
<%@page import="mainClasses.advance_Listing"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Journal-Voucher</title>
<!--Date picker script  -->
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});


$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                    $( "a" ).css("text-decoration","none");
                    $( ".printable" ).print();
                   

                    // Cancel click event.
                    return( false );
                });
			});
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
});
</script>
<!--Date picker script  -->
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
</head>
<body>

<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="ADV" class="beans.AdvanceService"></jsp:useBean>
<div class="vendor-page">
  <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<div class="vendor-box">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">Advances</td></tr>
</table>
<div class="vender-details">
<%
advance_Listing adv=new advance_Listing();
vendorsListing VENl=new vendorsListing();
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
String hoid="";
DecimalFormat DF=new DecimalFormat("0.00");
String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String startDate="";
String endDate="";
if(request.getParameter("hoid")!=null && !request.getParameter("hoid").equals("")){
	hoid=request.getParameter("hoid");
}
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
	 fromdate=request.getParameter("fromDate");
}
if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
	todate=request.getParameter("toDate");
}
startDate=fromdate+" 00:00:01";
endDate=todate+" 23:23:59";
List adv_list=adv.getTotalDetails(hoid,startDate,endDate);
%>
<form action="adminPannel.jsp" method="post">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<input type="hidden" name="page" value="advancesDuesList"></input>
<tr>
<td>
<select name="hoid" id="hoid" onchange="formSubmit();">
<option value="">ALL DEPARTMENTS</option>
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select>
</td>
<td><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate%>"  readonly="readonly" /></td>
<td><input type="text" name="toDate" id="toDate"  class="DatePicker " value="<%=todate%>"  readonly="readonly"></td>

<td>
<input type="submit" class="click" />
</td>
<!-- <div class="arrow-down"></div>
			<div class="icons"><a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
			<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div> -->
</tr>
</table>
</form>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list" style="">
<div class="bgcolr"  style=" float:left;width:100%;">
<div class="list-details" id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg">S.NO.</td>
<td class="bg">EPIV NO.</td>
<td class="bg">DATE</td>
<td class="bg" style="width:240px;">NARRATION</td>
<td class="bg">VENDOR NAME</td>
<td class="bg">AMOUNT</td>
<td class="bg">PAYMENT</td>
<td class="bg" style="width:200px;">PRINT</td>
</tr>
<%

if(adv_list.size()>0){
	for(int i=0;i<adv_list.size();i++){
		ADV=(AdvanceService)adv_list.get(i);
%>

<tr>
<td><%=i+1%></td>
<td>
<%=ADV.getInvoiceId()%>
</td>
<td>
<%=ADV.getDate()%>
</td>
<td>
<%=ADV.getNarration() %>
</td>
<td>
<%=VENl.getMvendorsAgenciesName(ADV.getVendorId()) %>
</td>
<td>
<%=DF.format(Double.parseDouble(ADV.getAmount()))%>
</td>
<td>
<%
if(ADV.getAmount().equals(ADV.getExtra2())){%>
	Completed
<%}else{
%>
Not Completed
<%} %>
</td>
<td>
<a href="billOrder.jsp?MSEID=<%=ADV.getMSEId()%>&INVOICE=<%=ADV.getInvoiceId()%>&NARRATION=<%=ADV.getNarration()%>" target="_blank">BILL DETAILS PRINT </a>
</td>
</tr>
<%}
	
} %>
</table>
</div>




<div class="tot-ven"></div>
</div>
</div>

</body>
</html>