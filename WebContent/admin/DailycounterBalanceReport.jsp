<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.TimeZone"%>
<%@page import="model.SansthanAccountsDate"%>
<%@page import="beans.banktransactions"%>
<%@page import="java.util.Date"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.cashier_reportListing"%>
<%@page import="mainClasses.banktransactionsListing" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAILY COUNTER BALANCE REPORT</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>

<body>
<jsp:useBean id="CUS" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="BTR" class="beans.banktransactions"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">


<div style="text-align: center;"></div>
<div class="icons">
<span> <a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="total-report">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<div class="printable" >
<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td><table width="100%" cellpadding="0" cellspacing="0" class="date-wise "  >
<tr><td colspan="4" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;" class="bg-new">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;" class="bg-new">Daily Counter Balance Report</td></tr>
<form action="adminPannel.jsp" method="post">
<input type="hidden" name="page" value="DailycounterBalanceReport"></input>
<tr>
<td width="20%">

<ul>
<%
customerpurchasesListing c_list =new customerpurchasesListing();
headofaccountsListing HODL = new headofaccountsListing();
subheadListing SUBL = new subheadListing();
banktransactionsListing blist = new banktransactionsListing();

SimpleDateFormat cdf = new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
							}
 String hoid[]=new String[]{"4","1"};
String reportType[]=new String[]{"pro-counter","Charity-cash"};
						%>
<li></li>
</ul></td>
<td width="30%"><input type="hidden" name="page" value="totalsalereport"/>From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="30%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" value="Submit" class="click bordernone"/></td>
</tr>
</form>
</table></td></tr></table>
<br/>

<div id="tblExport">
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3"  align="center" style="font-weight: bold;color: teal;">INCOME</td>
    <td colspan="4" align="center"  style="font-weight: bold;color: teal;">DEPOSIT</td>
  </tr>
  <tr>
    <td align="center" style="font-weight: bold;color:#934C1E;">Date</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Head Of Account</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Amount</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Date</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Account Deposit Category</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Deposit Amount</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Balance Amount</td>
  </tr>
  <%
  DecimalFormat DF = new DecimalFormat("#.00;-#.00");
  String addedDate1=SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 1);
  double sansthantotalcountercash =0.0;
  double sansthantotalcashpaid =0.0;
  double sansthangrandtotal = 0.0;
  if(c_list.getTotalAmountBeforeFromdate("4","2015-04-01", fromdate) != null && !c_list.getTotalAmountBeforeFromdate("4","2015-04-01", fromdate).equals("")){
	  sansthantotalcountercash = Double.parseDouble(c_list.getTotalAmountBeforeFromdate("4","2015-04-01", fromdate));
  }
  if(blist.getTotalamountDepositedBeforeFromdate("pro-counter","Development-cash","2015-04-02",addedDate1)!= null && !blist.getTotalamountDepositedBeforeFromdate("pro-counter","Development-cash","2015-04-02", addedDate1).equals("")){
	  sansthantotalcashpaid = Double.parseDouble(blist.getTotalamountDepositedBeforeFromdate("pro-counter","Development-cash","2015-04-02", addedDate1));
  }
  sansthangrandtotal = sansthantotalcountercash-sansthantotalcashpaid;
  double charitycountercash=0.0;
  double charitytotalcashpaid=0.0;
  double charitygrandtotal=0.0;
  if(c_list.getTotalAmountBeforeFromdate("1","2015-04-01", fromdate) != null && !c_list.getTotalAmountBeforeFromdate("1","2015-04-01", fromdate).equals("")){
	  charitycountercash = Double.parseDouble(c_list.getTotalAmountBeforeFromdate("1","2015-04-01", fromdate));
  }
  if(blist.getTotalamountDepositedBeforeFromdate("Charity-cash","Not-Required","2015-04-02", addedDate1)!= null && !blist.getTotalamountDepositedBeforeFromdate("Charity-cash","Not-Required","2015-04-02", addedDate1).equals("")){
	  charitytotalcashpaid = Double.parseDouble(blist.getTotalamountDepositedBeforeFromdate("Charity-cash","Not-Required","2015-04-02", addedDate1));
  }
  charitygrandtotal = charitycountercash-charitytotalcashpaid;
  double openingbal=0.0;
  openingbal = sansthangrandtotal + charitygrandtotal;
  %>
  <tr>
  <td colspan="6" align="center" style="font-weight: bold">OPENING BALANCE</td>
  <td style="font-weight: bold"><%=DF.format(openingbal) %></td>
  </tr>
  <%
  List<String> listOfDates= SansthanAccountsDate.getAddedBetweenDates(fromdate, todate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
  
  double grandTotal=0.0;
  for(int i=0;i<listOfDates.size();i++){
		for(int y=0;y<hoid.length;y++){
			 double totalLocal=0.0;
		  String dateNow=listOfDates.get(i);
		  String[] CLIST = c_list.getTotalamountBasedOnHoid(hoid[y],dateNow+" 00:00:01",dateNow+" 23:59:59");
	  %>
	  <tr>
	  
	    <td style="color:#000;"><%=dateNow%></td>
	    <td style="color:#000;"><%=HODL.getHeadofAccountName(CLIST[0])%></td>
	    <td style="color:#000;"><%=CLIST[1] %></td>
	    
	    <%
	    String addedDate=SansthanAccountsDate.getAddedDate(dateNow, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 1);
	    
	    String[]  BLIST=null;
	    if("4".equals(hoid[y])){
	    	BLIST= blist.getBankTransactionBasedOnHoid("pro-counter","Development-cash", addedDate+" 00:00:00", addedDate+" 23:59:59");
	    }else if("1".equals(hoid[y])){
	    	BLIST= blist.getBankTransactionBasedOnHoid("Charity-cash","Not-Required", addedDate+" 00:00:00", addedDate+" 23:59:59");
	    }
	    totalLocal = Double.parseDouble(CLIST[1])-Double.parseDouble(BLIST[1]);
	    %>
	    <td style="color:#000;"><%=addedDate%></td>
	    <td style="color:#000;"><%=BLIST[0] %></td>
	    <td style="color:#000;"><%=BLIST[1] %></td>
	    <td style="color:#000;"><%=totalLocal%></td>
	  </tr>
	  <%grandTotal = grandTotal+ totalLocal; %>
	  <%} %> 
  <%} %>
 
  <tr>
  <td colspan="6" align="center" style="font-weight: bold">TOTAL</td>
  <td style="font-weight: bold"><%=DF.format(grandTotal + openingbal) %></td>
  </tr>
</table>
</div>
</div>
</div>
</div>
</div>

</body>
</html>
