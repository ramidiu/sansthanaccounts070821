<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
/*.print_table th{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
}*/
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
/*.bod-nn{
	border-right:none !important;}*/
	}
/*	.bod-nn{
	border-right:1px solid #000 !important;
	font-weight: bold;}*/
</style>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Minor Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <script>
function combochangewithdefaultoption(denom,desti,jsppage){
	var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value;  
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
		$.post(jsppage,{id:com_id} ,function(data)
		{
			//$('#minor_head_id').empty();
			if(denom=="head_account_id"){
				document.getElementById("minor_head_id").options[0].selected = "true";	
			}
			if(desti == "minor_head_id"){
				var x=document.createElement('option');
				x.text="Select minor head";
				x.value="";
				sda1.add(x,null);
			}
			var where_is_mytool=data.trim();
		var mytool_array=where_is_mytool.split("\n");
		for(var i=0;i<mytool_array.length;i++)
		{
		if(mytool_array[i] !="")
		{
		//alert (mytool_array[i]);
		var y=document.createElement('option');
		var val_array=mytool_array[i].split(":");
					y.text=val_array[1];
					y.value=val_array[0];
					try
					{
					sda1.add(y,null);
					}
					catch(e)
					{
					sda1.add(y);
					
					}
		}
		}
		});
}
</script> 
<script>
$(function() {
	$( "#fromdate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#todate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#todate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromdate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>

<script>

function datepickerchange()
{
	var finYear=$("#finYear").val();
	var yr = finYear.split('-');
	var startDate = yr[0]+",04,01";
	var endDate = parseInt(yr[0])+1+",03,31";
	
	$('#fromdate').datepicker('option', 'minDate', new Date(startDate));
	$('#fromdate').datepicker('option', 'maxDate', new Date(endDate));
	$('#todate').datepicker('option', 'minDate', new Date(startDate));
	$('#todate').datepicker('option', 'maxDate', new Date(endDate));
}

$(document).ready(function(){
	datepickerchange();
}); 
</script>

  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
    <script type="text/javascript">
    function formsubmit(){
    	
    	if($("#minor_head_id").val()!="" && $("#minor_head_id").val()!=null)
    	{
	   		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReport&subhid="+$( '#minor_head_id' ).val()+"&finYear="+$('#finYear').val()+"&cumltv="+$('#cumltv').val();
	       	document.getElementById("departmentsearch").submit();
	   	} 
    	else if($("#major_head_id").val() != "" && $("#fromdate").val() !="" && $("#todate").val() !="")
    	{  
    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReport&majrhdid="+$('#major_head_id').val()+"&fromdate="+$('#fromdate').val()+"&todate="+$('#todate').val()+"&report="+$('#report').val()+"&hoid="+$('#head_account_id').val()+"&finYear="+$('#finYear').val()+"&cumltv="+$('#cumltv').val();
        	document.getElementById("departmentsearch").submit();
    	} 
    	else if($("#fromdate").val() !="" && $("#todate").val() !="" && $('#head_account_id').val() != "")
    	{  
	    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReport&hoid="+$('#head_account_id').val()+"&cumltv="+$('#cumltv').val();
	    	document.getElementById("departmentsearch").submit();
    	}
    	else if($("#fromdate").val() =="" && $("#todate").val() =="")
    	{  
	    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReport&cumltv="+$('#cumltv').val();
	    	document.getElementById("departmentsearch").submit();
    	}
    	
    	/* if($( "#major_head_id" ).val()!=""){    		
        	document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReport&majrhdid="+$( '#major_head_id' ).val()+"&fromdate="+$( '#fromdate' ).val()+"&todate="+$( '#todate' ).val()+"&report="+$( '#report' ).val();
        	document.getElementById("departmentsearch").submit();
    		    	} else if($( "#minor_head_id" ).val()!=""){
    		    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReport&subhid="+$( '#minor_head_id' ).val()+"&fromdate="+$( '#fromdate' ).val()+"&todate="+$( '#todate' ).val()+"&report="+$( '#report' ).val();
    		        	document.getElementById("departmentsearch").submit();
    		    	}   */
    }</script>
<%@page import="java.util.List"%>
</head>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<%
		String finYr = request.getParameter("finYear");
		String finStartYr = request.getParameter("finYear").substring(0, 4);
		
		String onlyfdate = request.getParameter("fromdate");
		String onlytdate = request.getParameter("todate");
		String fdate = request.getParameter("fromdate")+" 00:00:00";
		String tdate = request.getParameter("todate")+" 23:59:59";
		
		if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
		{
			onlyfdate = finStartYr+"-04-01";
			fdate = finStartYr+"-04-01"+" 00:00:00";
		}
	
	DecimalFormat df=new DecimalFormat("#,###.00");
	headofaccountsListing HOA_L=new headofaccountsListing();
	minorheadListing MINH_L=new minorheadListing();
	customerpurchasesListing CP_L=new customerpurchasesListing();
	List headaclist=HOA_L.getheadofaccounts();
	productexpensesListing PRDEXP_L=new productexpensesListing();
				String finyr[]=null;
				String report="";
				String fromdate="";
				String onlyfromdate="";
				String todate="";
				String onlytodate="";
				String cashtype="online pending";
				String cashtype1="othercash";
				String cashtype2="offerKind";
				String cashtype3="journalvoucher";
				double credit=0.0;
				double debit=0.0;
				double balance=0.0;
				double credit1=0.0;
				double debit1=0.0;
				double balance1=0.0;
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat onlydat2=new SimpleDateFormat("dd-MMM-yyyy");
				String majrhdid="";
				String hoaid="1";
				if(request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals("")){
					 fromdate=request.getParameter("fromdate");
				}
				if(request.getParameter("todate")!=null && !request.getParameter("todate").equals("")){
					todate=request.getParameter("todate");
				}
				if(request.getParameter("majrhdid")!=null){
					majrhdid=request.getParameter("majrhdid");
					hoaid=MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid);
				}
				if(request.getParameter("report")!=null){
					report=request.getParameter("report");
				}
				
			%>
	<div>
			<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form  name="departmentsearch" id="departmentsearch" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="print_table">
<tr>
<td style="width:14%;">Select Financial Year</td>
<td style="width:14%;" >From Date</td>
<td style="width:14%;">To Date</td>
<td style="width:14%;"><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account</td>
<td style="width:14%;"><div class="warning" id="MajorHeadErr" style="display: none;">Please select  "Major head".</div>Major head</td>
<td style="width:14%;"><div class="warning" id="MinorHeadErr" style="display: none;">Please select  "Minor head".</div>Minor head</td>
</tr>
<tr>
	<td style="width:14%;">
		<select name="finYear" id="finYear" Onchange="datepickerchange();" style="width:90%;">
			<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
			<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
		</select>
	</td>
	<td style="width:14%;"> 
		<input type="text" name="fromdate" id="fromdate" readonly="readonly" value="<%=onlyfdate%>" style="margin-right: 10px;"/>
		<input type="hidden" name="report" id="report" readonly="readonly" value="<%=report%>" style="    margin-right: 10px;"/>
	 </td>
	<td style="width:14%;">
		<input type="text" name="todate" id="todate" readonly="readonly" value="<%=onlytdate%>" style="margin-right: 10px;"/>
	 </td>
	<td style="width:14%;">
		<select name="head_account_id" id="head_account_id" onChange="combochangewithdefaultoption('head_account_id','major_head_id','getMajorHeads.jsp')" style="margin-right: 10px;width:95%;">
			<option value="SasthanDev" >Sansthan Development</option> 
			<%
			if(headaclist.size()>0){
				for(int i=0;i<headaclist.size();i++){
				HOA=(headofaccounts)headaclist.get(i);%>
			<option value="<%=HOA.gethead_account_id()%>" <%if(HOA.gethead_account_id().equals(hoaid)){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
			<%}} %>
		</select>
		</td>
	<td style="width:14%;">
	<select name="major_head_id" id="major_head_id" onChange="combochangewithdefaultoption('major_head_id','minor_head_id','getMinorHeads.jsp')" required style="margin-right: 10px;width:95%;">
		<option value="" selected="selected">Select major head</option> <%
		//System.out.println("head===>"+request.getParameter("hoid"));
		//System.out.println("head111===>"+request.getParameter("head_account_id"));
		majorheadListing MJR_L=new majorheadListing();
		List mjrlist;
		if(request.getParameter("hoid") != null){
			mjrlist=MJR_L.getMajorHeadBasdOnCompany(request.getParameter("hoid"));
		}else{
			mjrlist=MJR_L.getMajorHeadBasdOnCompany(request.getParameter("head_account_id"));
		}
		if(request.getParameter("hoid") != null || request.getParameter("head_account_id") != null ){
		if(mjrlist.size()>0){
			for(int j=0;j<mjrlist.size();j++){
				MH=(majorhead)mjrlist.get(j);%>
				<option value="<%=MH.getmajor_head_id() %>"><%=MH.getmajor_head_id() %> <%=MH.getname() %></option>
			<%}}}%>
	</select>
	</td>
	<td style="width:14%;">
		<select name="minor_head_id" id="minor_head_id" style="margin-right: 10px;width:95%;">
			<option value="" selected="selected">Select minor head</option>
		</select>
	</td>

<!-- <td></td> -->
</tr>
<tr>
<td style="width:14%;">Select Cumulative Type</td>
</tr>
<tr>
	<td style="width:14%;">
	<select name="cumltv" id="cumltv">
		<option value="noncumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("noncumulative")) {%> selected="selected"<%} %>>NON CUMULATIVE</option>
		<option value="cumulative" <%if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")) {%>selected="selected"<%} %>>CUMULATIVE</option>
	</select>
	</td>
	<td><input type="button" value="SEARCH" class="click" onclick="formsubmit()" style="margin-top:20px;"/></td>
</tr>
</table>
 </form>
				
				<% 
				
				/* onlyfromdate=fromdate;
				fromdate=fromdate+" 00:00:00";
				onlytodate=todate;
				todate=todate+" 23:59:59"; */
				List minhdlist=MINH_L.getMinorHeadBasdOnCompany(majrhdid);
				String majorheadName = MJR_L.getMajorHeadNameByCat1(majrhdid, hoaid);
				bankbalanceListing BBAL_L=new bankbalanceListing();
				banktransactionsListing BNK_L = new banktransactionsListing();
				%>
				<div class="icons">
				<!-- <div style="margin-bottom: 20px;float: left;cursor: pointer;"><img src="../images/back-button (1).png" width="100" height="50" onclick="goBack()" title="print"/></div> -->
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">

	<div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="5" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%=HOA_L.getHeadofAccountName(MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)) %></td>
					</tr>
					<tr><td colspan="5" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="5" align="center" style="font-weight: bold;"><%=report %> Report Of Minor Head</td></tr>
					<tr><td colspan="5" align="center" style="font-weight: bold;">Report From Date  <%=onlydat2.format(onlydat.parse(onlyfdate)) %> TO <%=onlydat2.format(onlydat.parse(onlytdate)) %> </td></tr>
						<tr>
						<td colspan="5">
						<span style="font-weight: bold; font-size:14px; padding-left:80px;"><%=majorheadName %>(<%=majrhdid %>)</span>
						<table width="100%" cellpadding="0" cellspacing="0" class="print_table">
						<tr>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">S.NO.</td>
							<td class="bg" width="30%" style="font-weight: bold;" align="left" colspan="0">Minor HEAD</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="left" colspan="0">DEBIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="left" colspan="0">CREDIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="left" colspan="0">BALANCE</td>
						</tr>
						<%
						double sumOfExtra12 = 0.0;
							if((majrhdid != null) && (majrhdid.equals("31") || majrhdid.equals("51") || majrhdid.equals("41")))
							{								
								sumOfExtra12 = Double.parseDouble(CP_L.getLedgerSumOfExtra12("","",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)));
							}

							if(minhdlist.size()>0){ 
							for(int i=0;i<minhdlist.size();i++){
								MNH=(minorhead)minhdlist.get(i);
								double newSumOfExtra12 = 0.0;
								if(MNH.getminorhead_id().equals("331") || MNH.getminorhead_id().equals("283") || MNH.getminorhead_id().equals("462"))
								{
									newSumOfExtra12 = sumOfExtra12;
								}
								else
								{
									newSumOfExtra12 = 0.0;
								}
								/* start */
								List otherAmountList1=BNK_L.getStringOtherDeposit("other-amount",majrhdid,MNH.getminorhead_id(),"",hoaid,fromdate,todate);
				  				//List otherDepositList=BNK_L.getStringOtherDeposit("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),hoaid,fromdate,todate);
								double otherAmount1 = 0.0;
								if(otherAmountList1.size()>0)
								{
									banktransactions OtherAmountList1 = (banktransactions)otherAmountList1.get(0);
									if(OtherAmountList1.getamount() != null)
									{
										otherAmount1 = Double.parseDouble(OtherAmountList1.getamount());
									}
								}
								/* end */
							%>	
	<tr style="position: relative;">
		<td style="font-weight: bold;" width="10%" align="center"><%=i+1 %></td>
		<td style="font-weight: bold;" colspan="0" width="30%" align="left"><a href="adminPannel.jsp?page=subheadLedgerReport&subhid=<%=MNH.getminorhead_id() %>&fromdate=<%=fromdate%>&todate=<%=todate%>&finYear=<%=finYr%>&report=Income and Expenditure&cumltv=<%=request.getParameter("cumltv")%>"><%=MNH.getname() %><%=MNH.getminorhead_id() %></a></td>
		<%-- <td style="font-weight: bold;text-align: right;" colspan="2"><%=df.format(Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fromdate,todate,hoaid))) %></td> --%>
		<%double debitAmountOpeningBal = 0.0;
		if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
		{
			debitAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid,majrhdid,MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "debit", "");
		}
			debit1=debitAmountOpeningBal+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3));%>
		<td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(debit1) %></td> 
		<%debit=debit+debitAmountOpeningBal+Double.parseDouble(PRDEXP_L.getLedgerSum(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid)) + Double.parseDouble(CP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3)); 
		double creditAmountOpeningBal = 0.0;
		if(request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative"))
		{
		/* 	creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, majrhdid,MNH.getminorhead_id(), "", "minorhead","Assets", finYr, "credit", ""); */
			creditAmountOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hoaid, majrhdid,MNH.getminorhead_id(), "", "minorhead","Liabilites", finYr, "credit", "");
		}
		if(!CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)).equals("00")){
			//<td style="font-weight: bold;text-align: right;" colspan="2"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) )</td> By using this line offerkind products amount also be added
		 //<td style="font-weight: bold;text-align: right;" colspan="2"><%=df.format(Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fromdate,todate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))))</td> <!--By using this line only subhead offerkind amount will be added
		credit1=creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) - Double.parseDouble(CP_L.getLedgerSumOfExtra12(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) + newSumOfExtra12+otherAmount1;
		credit=credit+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) - Double.parseDouble(CP_L.getLedgerSumOfExtra12(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) + newSumOfExtra12+otherAmount1;
		} else{
			credit1=creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+ Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) +  Double.parseDouble(CP_L.getLedgerSumDollarAmt(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid)) - Double.parseDouble(CP_L.getLedgerSumOfExtra12(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) + newSumOfExtra12+otherAmount1;
			credit=credit+creditAmountOpeningBal+Double.parseDouble(CP_L.getLedgerSumOfSubhead11(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(CP_L.getLedgerSum(MNH.getminorhead_id(),"extra3",fdate,tdate,cashtype2,MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid)))+Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid)) - Double.parseDouble(CP_L.getLedgerSumOfExtra12(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(majrhdid))) + newSumOfExtra12+otherAmount1;
		} %>
		<td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(credit1)%></td>
		<%
		if(debit1>credit1)
		{
			balance1=debit1-credit1;
		}
		if(credit1>debit1)
		{
			balance1=credit1-debit1;
		}
		if(credit1==debit1)
		{
			balance1=0.0;
		}
		%>
		<td style="font-weight: bold;" colspan="0" width="20%" align="left"><%=df.format(balance1)%></td>
	</tr>
				<% }} %>
				
					   </table>
						</td>
						</tr>
							<tr style="border: 1px solid #000;">
						<td style="font-weight: bold;text-align: center;" colspan="0" width="40%">Total  </td>
						<td style="font-weight: bold;text-align: left;" colspan="0" width="20%">Rs.<%=df.format(debit) %></td>
						<td style="font-weight: bold;text-align: left;" colspan="0" width="20%">Rs.<%=df.format(credit) %> </td>
						<%if(debit>credit){balance=debit-credit;}if(credit>debit){balance=credit-debit;}if(credit==debit){balance=0.0;}%>
						<td style="font-weight: bold;text-align: left;" colspan="0" width="20%">Rs.<%=df.format(balance)%></td>
					
						</tr>
					</table>
			</div>
				
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>