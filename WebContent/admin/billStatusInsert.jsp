<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.io.File" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page import="org.apache.commons.fileupload.disk.*"%>
<%@ page import="org.apache.commons.fileupload.*"%>	    
<jsp:useBean id="DBS" class="beans.daily_bill_status"></jsp:useBean>
<jsp:useBean id="DBSS" class="beans.daily_bill_statusService"></jsp:useBean>
<jsp:useBean id="NOG" class="beans.no_genaratorService"/>
<%
if(session.getAttribute("adminId")!=null){
mainClasses.no_genaratorListing NG_l=new mainClasses.no_genaratorListing();

DateFormat  dateFormat5= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
TimeZone tz = TimeZone.getTimeZone("IST");
dateFormat5.setTimeZone(tz);

Calendar c1 = Calendar.getInstance();
String str1=(dateFormat5.format(c1.getTime())).toString();

String billId=NG_l.getid("daily_bill_status");
String createdBy=session.getAttribute("adminId").toString();
String createdDate=str1;
String extra1="";
String extra2="";
String extra3="";
String extra4="";
String extra5="";
String image="";
String filename="";

boolean isMultipart = ServletFileUpload.isMultipartContent(request);

if (!isMultipart) {

} else {
	FileItemFactory factory = new DiskFileItemFactory();
	ServletFileUpload upload = new ServletFileUpload(factory);
	List items = null;
	try {
	items = upload.parseRequest(request);
	} catch (FileUploadException e) {
	out.print(e);
	}
	Iterator itr = items.iterator();
	while (itr.hasNext()) 
	  {
		FileItem item = (FileItem) itr.next();
		if (item.isFormField())
		{
		}
		else
	    {
		try {
			
			String strDirectoy3=config.getServletContext().getRealPath("/dailybillstatus");
			 //String strDirectoy3="E://workspace//Saisansthan_accounts//WebContent//dailybillstatus"; 
			String fname=item.getFieldName();
			String name=item.getName();
			int dotPos = name.lastIndexOf(".")+1;
			String extension =name.substring(dotPos);
			   if(fname.equals("billfile")&&(!name.equals("")))
			   {
				   filename=billId+"."+extension;
				  image=filename;
				 	File savedFile1 = new File(strDirectoy3,filename);
				   item.write(savedFile1);
				 }
		} catch (Exception e) {
			   
			   out.print(e);
			   }
		}
	  }
	%>
	
	<jsp:setProperty property="bill_id" name="DBSS" value="<%=billId %>"/>
	<jsp:setProperty property="createdBy" name="DBSS" value="<%=createdBy %>"/>
	<jsp:setProperty property="createdDate" name="DBSS" value="<%=createdDate %>"/>
	<jsp:setProperty property="fileName" name="DBSS" value="<%=filename %>"/>
	<jsp:setProperty property="extra1" name="DBSS" value="<%=extra1 %>"/>
	<jsp:setProperty property="extra2" name="DBSS" value="<%=extra2 %>"/>
	<jsp:setProperty property="extra3" name="DBSS" value="<%=extra3 %>"/>
	<jsp:setProperty property="extra4" name="DBSS" value="<%=extra4 %>"/>
	<jsp:setProperty property="extra5" name="DBSS" value="<%=extra5 %>"/>
	
	<%DBSS.insert();%><%=DBSS.geterror()%>
	<%
	NOG.updatInvoiceNumber(billId,"daily_bill_status");
	response.sendRedirect("adminPannel.jsp?page=billStatusList");   
}}else{
	response.sendRedirect("index.jsp");
}
	  %>