<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bill Approval</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<script type="text/javascript">
$(function(){
	$('#vendor').empty();
	var myOption = document.createElement("option");
	myOption.text = "Select Vendor";
	myOption.value = "";
	$('#vendor').append(myOption);
	$.ajax({
		url : 'searchVendor.jsp',
		type : 'POST',
		data : {p : 'Approved'},
		success : function(response)	{
			var ids = response.trim().split("\n");
			var vendors = new Array();
			for (var i = 0 ; i < ids.length ; i++)	{
					$('#vendor').append($('<option>', {
					    value: ids[i].split(":")[0]+","+ids[i].split(":")[2],
					    text: ids[i].split(":")[1]
					}));
			}
		}
	});
});
</script>
<script type="text/javascript">
function changeData()	{
	var vd = $('#vendor').val();
	if ($.trim(vd) !== "")	{
		$.ajax({
			url : "<%=request.getContextPath()%>/DCApprovedList",
			type : "POST",
			data : {dcNo : vd.split(",")[1]},
			success : function(response)	{
				var data = response.split(",");
				var count = 0,total=0;
				var row = "";
				var table = "<form id='form1'>";
				table += "<br><input type = 'text' name='narration' id='narration' value='' placeholder='Enter Narration Here' style='display: block; margin: -45px auto 29px;'>"
				table += "<table class='table table-bordered' style='width: 100%;display: block;position: relative;margin: 16px auto 0;left: 165px;top: 32px;' id='table'><tr class='header'><th style='font-size: 11px;font-weight: bold;'>S.no</th><th style='font-size: 11px;font-weight: bold;'>MSENumber</th><th style='font-size: 11px;font-weight: bold;'>Date</th><th style='font-size: 11px;font-weight: bold;'>DcNo</th><th style='font-size: 11px;font-weight: bold;'>Hoa</th><th style='font-size: 11px;font-weight: bold;'>MajorHead</th><th style='font-size: 11px;font-weight: bold;'>MinorHead</th><th style='font-size: 11px;font-weight: bold;'>SubHead</th><th style='font-size: 11px;font-weight: bold;'>Product</th><th style='font-size: 11px;font-weight: bold;'>Qty</th><th style='font-size: 11px;font-weight: bold;'>Rate</th><th style='font-size: 11px;font-weight: bold;'>Price</th></tr>";
//				$('#table').append($('<tr>').append($('<td>').append("S.no").append($('<td>').append("MseNumber")).append($('<td>').append("Date")).append($('<td>').append("DCNo")).append($('<td>').append("HOA")).append($('<td>').append("MajorHead")).append($('<td>').append("MinorHead")).append($('<td>').append("SubHead")).append($('<td>').append("Product")).append($('<td>').append("Qty")).append($('<td>').append("Rate")).append($('<td>').append("Price"))));
				for (var i = 0 ; i < data.length ; i++)	{
					if ($.trim(data[i]) != "")	{
					count ++;
					row = data[i].split("#");	
					total =  Number(total) + Number(row[15]);
					table += "<tr>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+(i+1)+"<input type='hidden' name='trackingno"+count+"' id='trackingno"+count+"' value='"+row[2]+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[0]+"<input type='hidden' name='invoiceId"+count+"' id='invoiceId"+count+"' value='"+row[0]+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[1]+"</td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[3]+"</td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[17]+"<input type='hidden' name='head_account_id"+count+"' id='head_account_id"+count+"' value='"+row[4]+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[6]+"<input type='hidden' name='major_head_id"+count+"' id='' value='"+row[5]+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[8]+"<input type='hidden' name='minor_head_id"+count+"' id='minor_head_id"+count+"' value='"+row[7]+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[10]+"<input type='hidden' name='product"+count+"' id='' value='"+row[9]+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[12]+"<input type='hidden' name='billproduct"+count+"' id='billproduct"+count+"' value='"+row[11]+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[13]+"<input type='hidden' name='expInvoiceId"+count+"' id='expInvoiceId"+count+"' value='"+row[18]+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[14]+"<input type='hidden' name='prodAmt"+count+"' id='prodAmt"+count+"' value='"+Number(row[15]).toFixed(2)+"'></td>";
					table += "<td style='font-size: 11px;font-weight: bold;'>"+row[15]+"</td>";
					table += "</tr>";
//					$("#table").append($('<tr>').append($('<td>').append(i+1)).append($('<td>').append(row[0])).append($('<td>').append(row[1])).append($('<td>').append(row[3])).append($('<td>').append(row[17])).append($('<td>').append(row[6])).append($('<td>').append(row[8])).append($('<td>').append(row[10])).append($('<td>').append(row[12])).append($('<td>').append(row[13])).append($('<td>').append(row[14])).append($('<td>').append(row[15])));
					}
				}
				
				table += "<tr><td colspan='11' align='right' style='font-size: 11px;font-weight: bold;'>Total&nbsp;&nbsp;&nbsp;</td><td style='font-size: 11px;font-weight: bold;'>"+Number(total).toFixed(2)+"</td></tr>";
				table += "<tr><td colspan='12' align='center'><button class='click' onclick='submitFrm()'>Submit</button></td></tr>";
				table += "</table>";
				table += "<input type='hidden' name='expscount' id='expscount' value='"+count+"'>";
				table += "<input type='hidden' name='dcNumber' id='dcNumber' value=''>";
				table += "<input type='hidden' name='dcAmount' id='dcAmount' value='"+total+"'>";
				table += "<input type='hidden' name='vendorId' id='vendorId' value=''>";
				table += "</form>";
				$('#data').html(table);
				$('#data').show();
			},
			error : function(jqXHR, textStatus, errorThrown) {
			      alert(jqXHR+" - "+textStatus+" - "+errorThrown);
			} 
		});
	}
}
function submitFrm()	{
	var narration = $('#narration').val();
	if ($.trim(narration) !== "")	{
		var vendor = $('#vendor').val();
		$('#vendorId').val(vendor.split(",")[0]);
		$('#dcNumber').val(vendor.split(",")[1])
		$('#form1').attr("action","productExpensesInsert.jsp");
		$('#form1').attr("method","POST");
		$('#form1').submit();
	}
	else	{
		alert("Please Enter Narration");
		$('#narration').focus();
		event.preventDefault();
	}
}
</script>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if (session.getAttribute("adminId") != null)	{%>
<%-- <div><%@ include file="title-bar.jsp"%></div> --%>
<div class="vendor-page">
<div class="search-list">
<select style="margin: 35px 45px 0;" id="vendor" onchange="changeData()">
<option value="">Select Vendor</option>
</select>
<div id="data" style="display: none;">

</div>
</div>
</div>	
<%}else	{
	response.sendRedirect("index.jsp");	
}%>
</body>
</html>