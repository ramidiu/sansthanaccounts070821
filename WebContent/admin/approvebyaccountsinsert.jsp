<%@page import="mainClasses.godwanstock_scListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="beans.godwanstockService"%>
<%@page import="beans.godwanstock"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<jsp:useBean id="GODS" class="beans.godwanstockService"/>
<jsp:useBean id="GODSS" class="beans.godwanstock_scService"/>
</head>
<body>
<%
godwanstock_scListing GODSList = new godwanstock_scListing();
DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String date=(dateFormat.format(c1.getTime())).toString();
String EmployeeId=session.getAttribute("adminId").toString();
String uniqueid=request.getParameter("uniqueno");
String invid=request.getParameter("invid");
%>

<jsp:setProperty name="GODS" property="extra9" value="<%=uniqueid%>"/>
<jsp:setProperty name="GODS" property="bill_update_by" value="<%= EmployeeId%>"/>
<jsp:setProperty name="GODS" property="bill_update_time" value="<%= date%>"/>
<%=GODS.updateBasedUniqueKey() %>


<!--this lines are written by pradeep to update latest entry in new table i.e godwanstock_sc  -->
<jsp:setProperty name="GODSS" property="extra9" value="<%=uniqueid%>"/>
<jsp:setProperty name="GODSS" property="bill_update_by" value="<%= EmployeeId%>"/>
<jsp:setProperty name="GODSS" property="bill_update_time" value="<%= date%>"/>
		<%
				String countInString = GODSList.getCountOfRecordsBasedOnMseId(invid);
				int count = Integer.parseInt(countInString);
				if(count == 1)
				{%>
					<%=GODSS.update1(invid)%><%=GODS.geterror()%>
				<%}
				else if(count > 1)
				{
					String entrydate = GODSList.getMaxDateForRecordBasedOnMseId(invid);
					%>
					<%GODSS.updateBillUpdateDateBasedOnDate(invid,entrydate, EmployeeId); %><%=GODS.geterror() %>
				<% 	
				}%>

<!--END  -->

<script type="text/javascript">
window.parent.location="adminPannel.jsp?page=expenses&expflag=1&invid=<%=invid%>";
</script>
</body>
</html>