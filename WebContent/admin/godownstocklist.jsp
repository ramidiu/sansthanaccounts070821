<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="mainClasses.godwanstockListing"%>
<%@ page import="beans.godwanstock"%> 
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Godown list stock</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<!-- <link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" /> -->
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<style>
 .modal-window {
   
    position: fixed;
    top: 40%;
    width: 567px !important;
    left: 43%;
    margin: 0; 
    padding: 34px 0 0;
   
    box-shadow: 0 0 8px #666;
    background: #fff;
    border-radius: 12px;
    border: 1px solid #999;
} 
.close-window {
    position: absolute;
    width: 25px;
    height: 25px;
    right: 8px;
    top: 8px;
    /* background: url(images/Button-Close-icon.png) no-repeat top left; */
    background: url(../images/close.png) no-repeat top left;
    text-indent: -99999px;
    z-index: 9999;
    cursor: pointer;
    opacity: .5;
    filter: alpha(opacity=50);
    -moz-opacity: 0.5;
}
</style> 
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 500;
		modalWindow.height = 400;
		modalWindow.margin
		modalWindow.content = "<iframe width='500' height='400' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    
<Script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});

function check1(){
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one");
	return false;} else{
	document.getElementById("godwnheads").action="godownstockheadsupdate.jsp";
	document.getElementById("godwnheads").submit();
	}
}

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>

</head>
<body>
<div class="vendor-page"><br><br>
		<div class="vendor-list">
		
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form  action="godownstocklist.jsp" method="post">
				<div class="search-list">
				<%
				   String toDayDate="";
				   Calendar calendar=Calendar.getInstance();
				   SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
				   toDayDate=dateFormat.format(calendar.getTime());
				   
				   
				%>
					<ul>
						
						<li>FromDate:<input type="text" name="fromDate" id="fromDate" class="DatePicker " value="<%=toDayDate%>" readonly="readonly"></li>
						<li>ToDate:<input type="text" name="toDate" id="toDate" class="DatePicker " value="<%=toDayDate%>" readonly="readonly"></li>
					<li><input type="submit" class="click" name="search" value="Search"></li>
					</ul>
				</div></form>
				<input type="submit" class="click" name="search" value="UpdateHeads" style="float:right" onclick="return check1();"></li>
				<div class="clear"></div>
				<div class="list-details">
				
	<div class="printable">    
             <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;
	margin:0 2%;
    background:#d0e3fb;
}

</style>

         <%
           String fromDate="";
           String toDate="";
          
           if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){
        	    fromDate=request.getParameter("fromDate");
        	    fromDate=fromDate+" 00:00:00";
           }
           if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
       	    toDate=request.getParameter("toDate");
       	    toDate=toDate+" 23:59:59";
          }
           if((fromDate!=null && fromDate!="") &&(toDate!=null && toDate!="")){
           godwanstockListing GDL=new godwanstockListing();
           List<godwanstock> godownStockList=GDL.getMSEsBetweenDates(fromDate,toDate);
           if(godownStockList!=null && godownStockList.size()>0){  
         %>
         <form id="godwnheads" method="POST">
           <input type="hidden" name="fromDate" value="<%=fromDate%>">
           <input type="hidden" name="toDate" value="<%=toDate%>">
		  <table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border="1">
					<tbody>
						<tr>
						<td class="bg" width="10%" style="font-weight: bold;"><input type="checkbox" name="select1"  id="selecctall" value=" ">checkAll</td>
							<td class="bg" width="10%" style="font-weight: bold;">Godown stock Id</td>
							
							<td class="bg" width="10%" style="font-weight: bold;">Vendor Id</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">Product Id</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">Quantity</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">Purchase Rate</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">EntryDate</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">MSE No</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">Head of Account</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">Amount</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="center">EPI Id</td>
							</tr>
							  <% for(int i=0;i<godownStockList.size();i++){
        		                  godwanstock godwan=(godwanstock)godownStockList.get(i);
        		               %>
							<tr>
							<td><input type="checkbox" name="select1" class="checkbox1" value = "<%=godwan.getproductId()%>,<%=godwan.getgsId()%>,<%=godwan.getDepartment()%>"></td>
							<td><%=godwan.getgsId()%></td>
							<td><%=godwan.getvendorId()%></td>
							<td><%=godwan.getProductId()%></td>
							<td><%=godwan.getquantity()%></td>
							<td><%=godwan.getPurchaseRate()%></td>
							<td><%=godwan.getDate()%></td>
							<td><%=godwan.getextra1()%></td>
							<td><%=godwan.getDepartment()%></td>
							<td><%=godwan.getExtra16()%></td>
							<td><%=godwan.getExtra21()%></td>
							</tr>
							<%}%>
							</tbody>
							</table>
							</form>
			 <%
               }else{
            	   %>
            	    <center><h3 style="color:red">No Records Are Avilable</h3></center>
            	   <% 
               }
           }
			%> 	
					</div>
			</div>
			</div>
</div>
</body>
</html>