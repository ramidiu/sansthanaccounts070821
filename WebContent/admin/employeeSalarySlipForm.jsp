<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="helperClasses.SaiMonths"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="mainClasses.employeeDetailsListing,beans.employeeDetails,java.util.List,java.util.Iterator,beans.employeeSalary"%>
   <%@ page import="mainClasses.employeeSalarySlipListing,mainClasses.employeeAttendce,mainClasses.stafftimingsListing,beans.stafftimings"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" language="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>


<style>th{border:1px solid black;color:red}</style>
<script type="text/javascript">
$(function() {
	$("#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		
		});
	});
function salarypay(id){
	var deductions=0;
	var allwoance=0;
	var totalamount=0;
	var payamount=0;
	deductions=Number($("#deductions"+id).val());
	 allwoance=Number($("#allowances"+id).val());
	 totalamount=Number($("#totalsalary_pay"+id).val());
	 payamount=Number(totalamount)+Number(allwoance)-Number(deductions);
	 $("#salary_pay"+id).val(payamount);
	
}
</script>
<script>
function updateTotal(id) 
{
	var basic_pay=Number($("#basic_pay"+id).val());
	var hra=Number($("#hra"+id).val());
	var otpay=Number($("#otpay"+id).val());
	var ewf_loan_amount=Number($("#ewf_loan_amount"+id).val());
	var ewf_interest_amount=Number($("#ewf_interest_amount"+id).val());
	var personal_loan_amount=Number($("#personal_loan_amount"+id).val());
	var personal_interest_amount=Number($("#personal_interest_amount"+id).val());
	var ewf_deduction=Number($("#ewf_deduction"+id).val());
	var salary_cut_days_amount=Number($("#salary_cut_days_amount"+id).val());
	var deductions=Number($("#deductions"+id).val());
	var allowances=Number($("#allowances"+id).val());
	var finall = Number(basic_pay)+Number(hra)+Number(otpay)-Number(ewf_loan_amount)-Number(ewf_interest_amount)-Number(personal_loan_amount)-Number(personal_interest_amount)-Number(ewf_deduction)-Number(salary_cut_days_amount)-Number(deductions)+Number(allowances);
	$("#salary_pay"+id).val(Number(finall));
}
</script>
<script>
function myFunction() 
{
	 	if(document.frm.month.value=="")
	    {
	       alert("please enter valid  month and year ");
	       document.frm.month.focus();
	       return false;
	    }
	 	
	 	else
		 {
    		document.getElementById("id1").submit();
		 }
}
</script>
<script type="text/javascript">
$("#checkAll").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});
</script>
<script>
function form2_submit() 
{
	
	var n=document.querySelectorAll('input[type="checkbox"]:checked').length;
		if( n > 0 )
		{ 
		
			return true;
		}
		else
		{ 
			alert( 'please check atleast one list' );
			return false;
		}	
}
</script>

</head>
<body><center>

<form action="adminPannel.jsp"  name="frm" method="get">

	<select name="department">
	<%
		DateFormat monthFormat=new SimpleDateFormat("MMMM");
		DateFormat yearFormat=new SimpleDateFormat("yyyy");
		stafftimingsListing stlisting=new stafftimingsListing(); 
		List stlist=stlisting.getAllDistinctStaffType();
		Iterator stitr=stlist.iterator();
		while(stitr.hasNext())
		{
			stafftimings stimings=(stafftimings)stitr.next();
	%>
		<option><%=stimings.getstaff_type()%></option>
	<% }%>
	</select>
		<!-- Month:<input type="month" name="month"> -->
		<%
			Date now=new Date();
			Calendar today=Calendar.getInstance();
			today.add(Calendar.MONTH, -1);
			String thisMonth=monthFormat.format(today.getTime());
			/* String thisYear=yearFormat.format(now); */
			String thisYear=yearFormat.format(today.getTime());
		%>
  		Month: 
  		 <select name="month">
  			<%List<String> listOfAllMonths= SaiMonths.getAllMonth(); 
  				//for(int i=0;i<listOfAllMonths.size();i++){
  			%>
  			 	<%-- <option value="<%=listOfAllMonths.get(i)%>" 
  				<%if(request.getParameter("month")!=null && request.getParameter("month").equalsIgnoreCase(listOfAllMonths.get(i)) ){ %>selected<% }
  					else{
  						if(thisMonth.equalsIgnoreCase(listOfAllMonths.get(i))){
  							%>selected<%
  						}else{
  							%>hidden<%
  						}
  					} %> >
  				<%=listOfAllMonths.get(i)%>
  				</option>  --%>
  				<%//} %>
  			<option value="<%=thisMonth%>"><%=thisMonth%></option>
  		</select>
  		Year: 
  		<select name="year">
  			<option value="<%=thisYear%>"><%=thisYear%></option>
  		</select> 
  	 <!--  <input type="text" id="fromDate" name="month"/>  -->
	
	<input type="hidden" name="page" value="employeeSalarySlipForm"  >
	<input type="submit" id="id1" value="ok" onclick=" return myFunction()">   <!-- comment -->
</form>
	
<%
if(!(request.getParameter("month")==null))
{
	String month=request.getParameter("month");
	String year=request.getParameter("year");
	String monthAndYear=month+"@"+year;
	System.out.println("month======>"+request.getParameter("month"));
	String employee_id=null;
	
	String department=request.getParameter("department");
	int checkBoxCLick=0;
	float opening_cl=0.0f,earned_cl=0.0f,closing_cl=0.0f,ot=0.0f;
	String LP=null,LA=null,OD=null;
	employeeDetailsListing listing=new employeeDetailsListing();
	List list=listing.selectAllEmployeeDetailsBasedUponDepartment(department,monthAndYear);
	if(list!=null){
	Iterator itr=list.iterator();%>
<form action="employeeSalarySlipSubmit.jsp" name="frm2" onSubmit="return form2_submit()" method="post">
	<input type="hidden" name="monthAndYear" value=<%=monthAndYear%>>
	<table align="left">
	<tr><td></td><td><input type="button"  value="SelectAll" onClick="CheckAll(document.frm2.employee_id)"></td>
	<td><input type="button"  value="Uncheck All" onClick="UnCheckAll(document.frm2.employee_id)"></td>
	<td><input type="button"  value=<%=month%>  ></td>
	
	<td><h1>Department--><%=department%></h1></td></tr>
	</table>
	<br>
	<table>
	<tr><th>s no:</th><th>select:</th><th>Employee Id:</th><th>Employee Name:</th><!--  <th>Month:</th>--><th>Basic Pay</th><th>Hra</th><th>OT Pay</th><th>EWF Loan Amount(Per month)</th><th>EWF Interest Amount(Per month)</th><th>Personal Loan Amount(Per month)</th><th>Personal Interest Amount(Per month)</th><th>Ewf Deduction</th> <th>Medical Insurance</th> <th>Salary Cut Amount(Day Wise)</th><th>Extra Amount Deduction</th><th>Extra Amount Allowance</th><th>Salary Pay</th></tr>
	<%
	while(itr.hasNext())
	{
		employeeDetails ed=(employeeDetails)itr.next();	
	    employee_id=ed.getEmployee_id();
	    //pass the employeeId and month to employeeAttendce.java  to get all CL,OT...etc
	    employeeAttendce attendence=new employeeAttendce();
	    String dateToGetClOt=""+SaiMonths.getMonthValue(month)+"/01/"+year;
	    String data[]=attendence.calculateClOTs(employee_id, dateToGetClOt);
		opening_cl=Float.parseFloat(data[0]);
		earned_cl=Float.parseFloat(data[1]);
		closing_cl=Float.parseFloat(data[2]);
	 	ot=Float.parseFloat(data[3]);
		LP=data[4];
		LA=data[5];
		OD=data[6];
		//now calculate interest and other by passing eid,cls  etc....
		List l=listing.calculateInterest2(employee_id,opening_cl,earned_cl,closing_cl,ot);
		Iterator itr2=l.iterator();
		if(itr2.hasNext())
		{	
			employeeSalary esalary=(employeeSalary)itr2.next();
			%>
			<tr>
				<td><%=++checkBoxCLick%></td>
				<td><input type="checkbox"  name="employee_id"  name="form2_checkbox_id" value=<%=esalary.getEmployee_id()%>></td>
				<td><input type="text" name="employee_id1<%=esalary.getEmployee_id()%>" value=<%=esalary.getEmployee_id() %> readonly></td>
				<!--  getting employee name from stafftiming table of saisansthan schema ..,just to display no need to store in DB -->
				<%
					stafftimings stft=stlisting.getStaffDetails(esalary.getEmployee_id());

				%>
				<td><input type="text" value="<%=stft.getstaff_name() %>" readonly></td>
				
				<input type="hidden" name="month<%=esalary.getEmployee_id()%>" value=<%=monthAndYear%> readonly> 
				<td><input type="text" name="basic_pay<%=esalary.getEmployee_id()%>" id="basic_pay<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getBasic_pay()%> ></td>
				<td><input type="text" name="hra<%=esalary.getEmployee_id()%>" id="hra<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getHra()%> ></td>
				<td><input type="text" name="otpay<%=esalary.getEmployee_id()%>" id="otpay<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getOtpay()%> ></td>
				<td><input type="text" name="ewf_loan_amount<%=esalary.getEmployee_id()%>" id="ewf_loan_amount<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getEwf_loan_amount()%> ></td>
				<td><input type="text" name="ewf_interest_amount<%=esalary.getEmployee_id()%>" id="ewf_interest_amount<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getEwf_interest_amount()%> ></td>
				<td><input type="text" name="personal_loan_amount<%=esalary.getEmployee_id()%>" id="personal_loan_amount<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getPersonal_loan_amount()%> ></td>
				<td><input type="text" name="personal_interest_amount<%=esalary.getEmployee_id()%>" id="personal_interest_amount<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getPersonal_interest_amount()%> ></td>				
				<td><input type="text" name="ewf_deduction<%=esalary.getEmployee_id()%>" id="ewf_deduction<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getExtra8()%> ></td>
				
				<%-- <td><input type="text" name="wa_amount<%=esalary.getEmployee_id()%>" value=<%=esalary.getMedical_insurance()%> ></td> --%>
				<%
				
  				  if(esalary.getMedical_insurance()!=null && !esalary.getMedical_insurance().equals("null")){
  					  
  					  %>
  				<td><input type="text" name="medicalinsurance<%=esalary.getEmployee_id()%>" value=<%=esalary.getMedical_insurance()%>></td>	  
  					  <%
  					  
  				  }else{
  					  %>
  					  
  					  <td><input type="text" name="medicalinsurance<%=esalary.getEmployee_id()%>" value="0.0"></td>
  					  <% 
  					  
  				  }
				%>
				
				
				<td><input type="text" name="salary_cut_days_amount<%=esalary.getEmployee_id()%>" id="salary_cut_days_amount<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value=<%=esalary.getExtra10()%> ></td>
				<td><input type="text" name="deductions<%=esalary.getEmployee_id()%>"  id="deductions<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value="0.0"></td>
				<td><input type="text" name="allowances<%=esalary.getEmployee_id()%>"  id="allowances<%=esalary.getEmployee_id()%>" onblur="updateTotal('<%=esalary.getEmployee_id()%>');" value="0.0"></td>
				<td><input type="text" name="salary_pay<%=esalary.getEmployee_id()%>" id="salary_pay<%=esalary.getEmployee_id()%>" value=<%=esalary.getSalary_pay()%> >
				<input type="hidden" name="totalsalary_pay<%=esalary.getEmployee_id()%>" id="totalsalary_pay<%=esalary.getEmployee_id()%>" value=<%=esalary.getSalary_pay()%> readonly>
				<input type="hidden" name="extra1<%=esalary.getEmployee_id()%>" value=<%=LP%>>
				<input type="hidden" name="extra2<%=esalary.getEmployee_id()%>" value=<%=LA%>>
				<input type="hidden" name="extra3<%=esalary.getEmployee_id()%>" value=<%=OD%>>
				<input type="hidden" name="extra4<%=esalary.getEmployee_id()%>" value=<%=opening_cl%>>
				<input type="hidden" name="extra5<%=esalary.getEmployee_id()%>" value=<%=earned_cl%>>
				<input type="hidden" name="extra6<%=esalary.getEmployee_id()%>" value=<%=closing_cl%>>
				<input type="hidden" name="extra7<%=esalary.getEmployee_id()%>" value=<%=ot%>></td>
				
		
				<!-- <td><input type="submit" value="ok"></td> -->
			</tr>
	    	
		<%}//if(itr2.hasNext())%>

 	<%}//while(itr.hasNext()))%> 
	
	<tr><td><input type="submit" value="create pay slip" ></td></tr>
	
	</table></form>
<%} }
%>

</center></body>
</html>