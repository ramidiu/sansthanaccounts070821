<%@page import="java.util.Date"%>
<%@page import="java.lang.reflect.Array"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ledger Major Head REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<style>
	.table-head td{
	font-size:16px; 
	line-height:28px; 
	text-align:center; 
	font-weight:bold;
}
.new-table td{
padding: 2px 0 2px 5px !important;}

@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}

.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
</style>

<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
function minorhead(id){
	$('#'+id).toggle();
}
</script>
   
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="MNH" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="MH" class="beans.majorhead"></jsp:useBean>
<jsp:useBean id="SUBH" class="beans.subhead"></jsp:useBean>
</head>

<body>
<%
String htmlData="";
	
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String finYear="2017-18";
		String fromDate1=sdf.format(date);
		String toDate1=sdf.format(date);
		String cumltv="noncumulative";
		String hoaids[]={"1","4"};
		
		for(int b=0;b<hoaids.length;b++){
		String hoaid=hoaids[b];
		//String hoaid1="1";
		DecimalFormat df=new DecimalFormat("#,###.00");
		/* DecimalFormat DF=new DecimalFormat(""); */
	/* 	Calendar c1 = Calendar.getInstance(); 
		TimeZone tz = TimeZone.getTimeZone("IST");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd"); */
		String fromdate="";
		String onlyfromdate="";
		String todate="";
		String cashtype="online pending";
		String cashtype1="othercash";
		String cashtype2="offerKind";
		String cashtype3="journalvoucher";
		List minhdlist=null;
		minorheadListing MINH_L=new minorheadListing();
		String onlytodate="";
		productexpensesListing PRDEXP_L=new productexpensesListing();
		/* c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		todate=(dateFormat2.format(c1.getTime())).toString();
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromdate=(dateFormat2.format(c1.getTime())).toString(); */

DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd");

Calendar c = Calendar.getInstance();
c.add(Calendar.MONTH, -1);
c.set(Calendar.DATE, 1);

String firstDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();
c.set(Calendar.DATE,c.getActualMaximum(Calendar.DAY_OF_MONTH));
String lastDateOfPreviousMonth = (dateFormat.format(c.getTime())).toString();


		if(fromDate1!=null && !fromDate1.equals("")){
			 fromdate=fromDate1;
		}
		if(toDate1!=null && !toDate1.equals("")){
			todate=toDate1;
		}
		headofaccountsListing HOA_L=new headofaccountsListing();
		List headaclist=HOA_L.getheadofaccounts();
		
	%>
<div class="vendor-page">
		<div>
				<% String finyr[]=null;	
				String headgroup="";
				String subhid="";
				double total=0.0;
				double credit=0.0;
				double debit=0.0;
				double creditincome=0.0;
				double debitincome=0.0;
				double creditexpens=0.0;
				double debitexpens=0.0;
				productexpensesListing PEXP_L=new productexpensesListing();
				SimpleDateFormat dbdat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat onlydat=new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat onlydat2=new SimpleDateFormat("dd-MMM-yyyy");
				if(request.getParameter("head_account_id")!=null){
					hoaid=request.getParameter("head_account_id");
					if(hoaid.equals("SasthanDev")){
						headgroup="2";
						hoaid="4";
					} else if(hoaid.equals("4")){
						headgroup="1";
					}
				}
				subheadListing SUBH_L=new subheadListing();
				
				customerpurchasesListing CP_L=new customerpurchasesListing();
				majorheadListing MH_L=new majorheadListing();
				banktransactionsListing BNK_L = new banktransactionsListing();
				
				List incomemajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew1(hoaid, "revenue",headgroup,"IE");
				List expendmajhdlist=MH_L.getMajorHeadBasdOnCompanyAndRevenueTypeNew1(hoaid,"expenses",headgroup,"IE");
				
				%>
				
				<div class="clear"></div>
				<div class="list-details">

	
	
	<div class="total-report">

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function ()	 {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    var y = $(this).scrollTop();

    if (y >= top) {
      $('.yourID').addClass('fixed');
	      
    } else {
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<% if(fromDate1 != null && !toDate1.equals("") && finYear != null){
		bankbalanceListing BBAL_L=new bankbalanceListing();
		String finYr = finYear;
		String finStartYr = finYear.substring(0, 4);
		String fdate = firstDateOfPreviousMonth+" 00:00:00";
		String fromDate = fromDate1;
		if(cumltv!=null && cumltv.equals("noncumulative")){
			fdate = firstDateOfPreviousMonth+" 00:00:00";
		}
		String tdate = lastDateOfPreviousMonth+" 23:59:59";
	%>				
	<div class="printable" id="tblExport">

<%
htmlData=htmlData+"<table width='90%' cellpadding='0' cellspacing='0' style='margin-bottom: 10px; margin-top:10px; margin:auto;'><tbody><tr><td colspan='3' align='center' style='font-weight: bold;'> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <span style='color:red'>"+HOA_L.getHeadofAccountName(hoaid)+"</span></td></tr>";
htmlData=htmlData+"<tr><td colspan='3' align='center' style='font-weight: bold;'>Dilsukhnagar</td></tr><tr><td colspan='3' align='center' style='font-weight: bold;'>Income &amp; Expenditure Report</td></tr><tr><td colspan='3' align='center' style='font-weight: bold;'>Report From Date";
htmlData=htmlData+onlydat2.format(onlydat.parse(firstDateOfPreviousMonth))+" TO "+onlydat2.format(onlydat.parse(lastDateOfPreviousMonth))+"</td></tr></tbody></table>";

%>
<div style="clear:both"></div>
<%
htmlData=htmlData+"<table width='90%' border='1' cellspacing='0' cellpadding='0'  style='margin:0 auto;' class='new-table'><tbody><tr><th width='38%' height='29' align='center'>Account Name</th><th width='29%' align='center'>Debits</th><th width='33%' align='center'>Credits</th></tr>";
htmlData=htmlData+"<tr><td height='28' colspan='3' align='left' bgcolor='#CC9933' style='color:#fff; font-weight:bold;'>INCOME</td></tr>";

  if(incomemajhdlist.size()>0){ 
	String corpusMajorHead = "";
	String corpusMinorHead = "";
	double corpusfundAmt = 0.0;
	String minorheads[] =new String[2];
	
	if(hoaid.equals("1"))
	{
		corpusMajorHead = "13";
		corpusMinorHead = "476";
		minorheads = new String[]{"122","469"};
	}
	else if(hoaid.equals("3"))
	{
		corpusMajorHead = "14";
		corpusMinorHead = "21532";
		minorheads = new String[]{"132"};
	}
	else if(hoaid.equals("4"))
	{
		corpusMajorHead = "11";
		corpusMinorHead = "473";
		minorheads = new String[]{"102","468"};
	}
	else if(hoaid.equals("5"))
	{
		corpusMajorHead = "15";
		corpusMinorHead = "478";
		minorheads = new String[]{"142","472"};
	}
	  
	double creditCorpusfund = 0.0; 
	List subh3=SUBH_L.getSubheadListMinorhead4(hoaid,corpusMajorHead,corpusMinorHead,"IE");
	String subhd3[]=new String[subh3.size()];
	int o=0;
	for(o=0;o<subh3.size();o++){
	SUBH=(subhead)subh3.get(o);	
	subhd3[o]=SUBH.getsub_head_id();

	}
	corpusfundAmt = creditCorpusfund+Double.parseDouble(CP_L.getLedgerSumOfSubhead31(corpusMinorHead,"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(corpusMajorHead),"sub_head_id",subhd3))+Double.parseDouble(CP_L.getLedgerSum1(corpusMinorHead,"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(corpusMajorHead),"sub_head_id",subhd3)) + Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(corpusMinorHead,"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd3)) + Double.parseDouble(CP_L.getLedgerSumDollarAmt1(corpusMinorHead,"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd3));
	  for(int i=0;i<incomemajhdlist.size();i++){
									MH=(majorhead)incomemajhdlist.get(i);
									List subh2=SUBH_L.getSubheadListMinorhead3(MH.getmajor_head_id(),"IE");
									String subhd2[]=new String[subh2.size()];
									int n=0;
									for(n=0;n<subh2.size();n++){
									SUBH=(subhead)subh2.get(n);
									subhd2[n]=SUBH.getsub_head_id();
									
									}
									double incomeMjrdebitAmt = Double.parseDouble(PEXP_L.getLedgerSum1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2));
									double incomeMjrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2));
									double incomeMjrcreditAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead111(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd2));
									double incomeMjrcreditAmtJV = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2));
									double incomeMjrcreditAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd2));
									
									double incomeMnrdebitAmtt =0.0;
									double incomeMnrdebitAmtt1 =0.0;
									double incomeMjrdebitAmttJV=0.0;
									double incomeMjrdebitAmt1JV1=0.0;
									double incomeMjrcreditAmtt = 0.0;
									double incomeMjrcreditAmtt1 = 0.0;
									double incomeMjrcreditAmttJV=0.0;
									double incomeMjrcreditAmttJV1=0.0;
									if(minorheads.length > 0){
										for(int m=0 ;m<minorheads.length;m++){
										
											incomeMnrdebitAmtt1 = Double.parseDouble(PRDEXP_L.getLedgerSum1(minorheads[m],"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd2));
											incomeMnrdebitAmtt = incomeMnrdebitAmtt +incomeMnrdebitAmtt1;
											incomeMjrdebitAmt1JV1 = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(minorheads[m],"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2));
											incomeMjrdebitAmttJV = incomeMjrdebitAmttJV +incomeMjrdebitAmt1JV1;
											incomeMjrcreditAmtt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead111(minorheads[m],"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd2));
											incomeMjrcreditAmtt = incomeMjrcreditAmtt+incomeMjrcreditAmtt1;
											incomeMjrcreditAmttJV1 = Double.parseDouble(PEXP_L.getLedgerSumBasedOnJV1(minorheads[m],"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2));
											incomeMjrcreditAmttJV = incomeMjrcreditAmttJV+incomeMjrcreditAmttJV1;
										}
									}
									
									  double mjdbamt = incomeMjrdebitAmt + incomeMjrdebitAmtJV-incomeMnrdebitAmtt-incomeMjrdebitAmttJV;
									double mjcdamt = incomeMjrcreditAmt + incomeMjrcreditAmtJV+ incomeMjrcreditAmtDollar-incomeMjrcreditAmtt-incomeMjrcreditAmttJV;
									double debitAmountOpeningBal = 0.0;
									double creditAmountOpeningBal = 0.0;
									double debitAmountOpeningBal1 = 0.0;
									double creditAmountOpeningBal1 = 0.0;
									
									double totcreditAmountOpeningBal =creditAmountOpeningBal-creditAmountOpeningBal1;
									if(!("0.0".equals(""+mjdbamt)) || !("0.0".equals(""+mjcdamt)) || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0 || totcreditAmountOpeningBal > 0){
								
										htmlData=htmlData+"<tr><td height='28' align='center' style='color: #934C1E; font-weight:bold;' ><a href='#' onclick='minorhead("+MH.getmajor_head_id()+");'>"+MH.getmajor_head_id()+" "+MH.getname() +"</a></td>";
    	double corpusfundAmtTemp = 0.0;
    	if(MH.getmajor_head_id().equals(corpusMajorHead))
    	{
    		corpusfundAmtTemp = corpusfundAmt; 
    	}
    %>
    <%
				/* start */
				
			    double otherAmountList11= 0.0;
				if(minorheads.length > 0){
					for(int m=0 ;m<minorheads.length;m++){
						double otherAmountList1=Double.parseDouble(BNK_L.getStringOtherDepositByheads1("other-amount",MH.getmajor_head_id(),minorheads[m],"",hoaid,fromdate,todate,"sub_head_id",subhd2));
						otherAmountList11 = otherAmountList11 + otherAmountList1;	
					}
				}
				List otherAmountList=BNK_L.getStringOtherDeposit1("other-amount",MH.getmajor_head_id(),"","",hoaid,fromdate,todate,"sub_head_id",subhd2);
				double otherAmount = 0.0;
				if(otherAmountList.size()>0)
				{
					banktransactions OtherAmountList = (banktransactions)otherAmountList.get(0);
					if(OtherAmountList.getamount() != null)
					{
						otherAmount = Double.parseDouble(OtherAmountList.getamount());
					}
				}
				/* end */
				%>
	<%if(MH.getmajor_head_id().equals("13") || MH.getmajor_head_id().equals("15") || MH.getmajor_head_id().equals("11") || MH.getmajor_head_id().equals("14")){ 
		htmlData=htmlData+"<td align='center' style='color: #934C1E; font-weight:bold;'>"+df.format(debitAmountOpeningBal+incomeMjrdebitAmt + incomeMjrdebitAmtJV-incomeMnrdebitAmtt-incomeMjrdebitAmttJV) +"</td>";
		    
	     debitincome=debitincome+debitAmountOpeningBal+incomeMjrdebitAmt + incomeMjrdebitAmtJV-incomeMnrdebitAmtt-incomeMjrdebitAmttJV;
	     htmlData=htmlData+"<td align='center' style='color: #934C1E; font-weight:bold;'>"+df.format(totcreditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar+otherAmount-otherAmountList11-incomeMjrcreditAmtt-incomeMjrcreditAmttJV)+"</td>";
		
	 creditincome=creditincome+totcreditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt +incomeMjrcreditAmtJV +incomeMjrcreditAmtDollar +otherAmount-otherAmountList11-incomeMjrcreditAmtt-incomeMjrcreditAmttJV; 
	}else{ 
		htmlData=htmlData+"<td align='center' style='color: #934C1E; font-weight:bold;'>"+df.format(debitAmountOpeningBal+incomeMjrdebitAmt + incomeMjrdebitAmtJV)+"</td>";
	
		debitincome=debitincome+debitAmountOpeningBal+incomeMjrdebitAmt + incomeMjrdebitAmtJV;
	      htmlData=htmlData+"<td align='center' style='color: #934C1E; font-weight:bold;'>"+df.format(creditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt + incomeMjrcreditAmtJV + incomeMjrcreditAmtDollar+otherAmount) +"</td>";
	      
	      creditincome=creditincome+creditAmountOpeningBal-corpusfundAmtTemp+incomeMjrcreditAmt +incomeMjrcreditAmtJV +incomeMjrcreditAmtDollar +otherAmount; 
 	} 
	 htmlData=htmlData+"</tr>";
 	
  minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
			if(minhdlist.size()>0){ 
									for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									double incomeMnrdebitAmt=0.0;
									double incomeMnrdebitAmtJV=0.0;
									double incomeMnrcreditAmt =0.0;
									double incomeMnrcreditAmt1 =0.0;
									double incomeMnrcreditAmtJV =0.0;
									double incomeMnrcreditAmtDollar =0.0;
									double incomeMnrcreditAmtOfferKind =0.0;
									double mindbamt =0.0;
									double mincdamt = 0.0;
									double debitAmountOpeningBal2 = 0.0;
								  	double creditAmountOpeningBal2 = 0.0;
								  	double otherAmount1 = 0.0;
								  	List MinHd1=MINH_L.getMinorHeadBasdOnCompany1(MH.getmajor_head_id(),MNH.getminorhead_id(),"IE");
									String MinHead1[]=new String[MinHd1.size()];
									int p=0;
									for(p=0;p<MinHd1.size();p++){
										MNH=(beans.minorhead)MinHd1.get(p);
									MinHead1[p]=" ";
									String minorvalue='"'+MNH.getminorhead_id()+'"';
									if(!MNH.getminorhead_id().equals(minorvalue)){
										List subh1=SUBH_L.getSubheadListMinorhead2(MNH.getminorhead_id(),"IE");
										String subhd1[]=new String[subh1.size()];
										int m=0;
										for(m=0;m<subh1.size();m++){
										SUBH=(subhead)subh1.get(m);
										subhd1[m]=SUBH.getsub_head_id();
										
										}
									 incomeMnrdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum1(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd1));
									 incomeMnrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd1));
									 incomeMnrcreditAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead31(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
									 incomeMnrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSum1(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
									 incomeMnrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd1));
									 incomeMnrcreditAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd1));
									 incomeMnrcreditAmtOfferKind = Double.parseDouble(CP_L.getLedgerSumOfSubheads(MNH.getminorhead_id(),"extra1",fdate,tdate,"offerKind",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
										
									 mindbamt =  incomeMnrdebitAmt + incomeMnrdebitAmtJV;
								  	 mincdamt = incomeMnrcreditAmt + incomeMnrcreditAmt1+ incomeMnrcreditAmtJV + incomeMnrcreditAmtDollar;
										
								  	/* start */
									List otherAmountList1=BNK_L.getStringOtherDeposit1("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),"",hoaid,fromdate,todate,"sub_head_id",subhd1);
										
									if(otherAmountList1.size()>0)
									{
										banktransactions OtherAmountList1 = (banktransactions)otherAmountList1.get(0);
										if(OtherAmountList1.getamount() != null)
										{
											otherAmount1 = Double.parseDouble(OtherAmountList1.getamount());
										}
									}
									/* end */
			if((!("0.0".equals(""+mindbamt)) || !("0.0".equals(""+mincdamt)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0 || otherAmount1 != 0.0) && !MNH.getminorhead_id().equals(corpusMinorHead)){  
				htmlData=htmlData+"<tr><td height='28' style='color:#f00;'><strong>"+MNH.getminorhead_id()+" "+MNH.getname() +"</strong></td>";
				htmlData=htmlData+"<td align='center' style='color:#f00;'>"+df.format(debitAmountOpeningBal2+incomeMnrdebitAmt + incomeMnrdebitAmtJV) +"</td>";	
				
   debitincome=debitincome+debitAmountOpeningBal2+incomeMnrdebitAmt + incomeMnrdebitAmtJV;  
    if(!String.valueOf(incomeMnrcreditAmt1).equals("0.0")){ 
    	htmlData=htmlData+"<td align='center' style='color:#f00;'>"+df.format(creditAmountOpeningBal2+incomeMnrcreditAmtOfferKind+otherAmount1)+"</td>";
  } else{ 
	  htmlData=htmlData+"<td align='center' style='color:#f00;'>"+df.format(creditAmountOpeningBal2+incomeMnrcreditAmt+incomeMnrcreditAmt1 +incomeMnrcreditAmtJV  +incomeMnrcreditAmtDollar +otherAmount1) +"</td>";
  }
    htmlData=htmlData+"</tr>";
    
	List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
    String subhd="";
    if(subhdlist.size()>0){ 
		for(int k=0;k<subhdlist.size();k++){
			SUBH=(subhead)subhdlist.get(k);
			subhd=SUBH_L.getSubheadListMinorhead1(MNH.getminorhead_id(),SUBH.getsub_head_id(),"IE");;
			if(SUBH.getsub_head_id().equals("20201"))
				cashtype="creditsale";
			else if(SUBH.getsub_head_id().equals("20206"))
				cashtype="creditsale";
										
			if(SUBH.getsub_head_id().equals("20020") || SUBH.getsub_head_id().equals("20022") || SUBH.getsub_head_id().equals("20023") || SUBH.getsub_head_id().equals("20025") || SUBH.getsub_head_id().equals("20024")){
											
			}//if ends
			else{
				double incomeSubdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum21(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd));
				double incomeSubdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd));
				double incomeSubcreditAmt = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
				double incomeSubcreditAmt1 = Double.parseDouble(CP_L.getLedgerSum1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
				double incomeSubcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
				double incomeSubcreditAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd));
										
				double subdbamt = incomeSubdebitAmt +incomeSubdebitAmtJV ;  
				double subcdamt = incomeSubcreditAmt+incomeSubcreditAmt1 + incomeSubcreditAmtJV  + incomeSubcreditAmtDollar;
				double debitAmountOpeningBal3 = 0.0;
				double creditAmountOpeningBal3 = 0.0;
				/* start */
				List otherAmountList2=BNK_L.getStringOtherDeposit1("other-amount",MH.getmajor_head_id(),MNH.getminorhead_id(),SUBH.getsub_head_id(),hoaid,fromdate,todate,"sub_head_id",subhd);
				double otherAmount2 = 0.0;
				if(otherAmountList2.size()>0){
					banktransactions OtherAmountList2 = (banktransactions)otherAmountList2.get(0);
					if(OtherAmountList2.getamount() != null){
						otherAmount2 = Double.parseDouble(OtherAmountList2.getamount());
					}
				}
				/* end */
				if(!("0.0".equals(""+subdbamt)) || !("0.0".equals(""+subcdamt)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0 || otherAmount2 != 0.0){  
					htmlData=htmlData+"<tr>";
  					if(!String.valueOf(incomeSubcreditAmt1).equals("0.0")){ 
  						htmlData=htmlData+"<td height='28'>"+SUBH.getsub_head_id()+" "+SUBH.getname()+"</td>";
  					}else{ 
  						htmlData=htmlData+"<td height='28'>"+SUBH.getsub_head_id()+" "+SUBH.getname() +"</td>";
  					} 
  					htmlData=htmlData+"<td>"+df.format(debitAmountOpeningBal3+incomeSubdebitAmt +incomeSubdebitAmtJV ) +"</td>";
  					debit=debit+debitAmountOpeningBal3+incomeSubdebitAmt +incomeSubdebitAmtJV ;  
     				if(!String.valueOf(incomeSubcreditAmt1).equals("0.0")){
     					htmlData=htmlData+"<td>"+df.format(creditAmountOpeningBal3+incomeSubcreditAmt+otherAmount2) +"</td>";
     					credit=credit+creditAmountOpeningBal3+incomeSubcreditAmt+otherAmount2;
     				} else{ 
     					htmlData=htmlData+"<td>"+df.format(creditAmountOpeningBal3+incomeSubcreditAmt+incomeSubcreditAmt1 +incomeSubcreditAmtJV +incomeSubcreditAmtDollar +otherAmount2) +"</td>";
     					credit=credit+creditAmountOpeningBal3+incomeSubcreditAmt+incomeSubcreditAmt1 +incomeSubcreditAmtJV + incomeSubcreditAmtDollar+otherAmount2;
     				} 
     				htmlData=htmlData+"</tr>";
     			}}}}}}}}}}}} 
  				htmlData=htmlData+"<tr><td height='28' colspan='3' align='left' bgcolor='#CC9933' style='color:#fff; font-weight:bold;'>EXPENDITURE</td></tr>";
     			
  				if(expendmajhdlist.size()>0){ 
					for(int i=0;i<expendmajhdlist.size();i++){
						MH=(majorhead)expendmajhdlist.get(i);
						List subh2=SUBH_L.getSubheadListMinorhead3(MH.getmajor_head_id(),"IE");
						String subhd2[]=new String[subh2.size()];
						int n=0;
						for(n=0;n<subh2.size();n++){
							SUBH=(subhead)subh2.get(n);
							subhd2[n]=SUBH.getsub_head_id();
						}
									
						double debitAmountOpeningBal = 0.0;
						double creditAmountOpeningBal = 0.0;
						double expMjrdebitAmt=0.0;
									
						if(MH.getmajor_head_id().equals("31")){
							expMjrdebitAmt = Double.parseDouble(PEXP_L.getLedgerSum01(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2));	
						}else{
							expMjrdebitAmt = Double.parseDouble(PEXP_L.getLedgerSum1(MH.getmajor_head_id(),"major_head_id",fdate,tdate,hoaid,"sub_head_id",subhd2));
						}
						double expMjrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"extra6",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd2));
						double expMjrcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd2));
						double expMjrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead1b(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,hoaid,"sub_head_id",subhd2));
						double expMjrcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead111(MH.getmajor_head_id(),"major_head_id",fdate,tdate,cashtype,cashtype3,"","",hoaid,"sub_head_id",subhd2));
						double expMjrcreditAmt3	= Double.parseDouble(CP_L.getLedgerSumOfSubheads(MH.getmajor_head_id(),"major_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd2));
						double expMjrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MH.getmajor_head_id(),"major_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd2));
									
						if(!String.valueOf(expMjrdebitAmt).equals("0.0") || debitAmountOpeningBal > 0 || creditAmountOpeningBal > 0){
							htmlData=htmlData+"<tr><td height='28' align='center' style='color: #934C1E; font-weight:bold;'><a href='#' onclick=minorhead('"+MH.getmajor_head_id()+"');>"+MH.getmajor_head_id()+MH.getname() +"</a></td>";
							htmlData=htmlData+"<td align='center' style='color: #934C1E; font-weight:bold;'>"+df.format(debitAmountOpeningBal+expMjrdebitAmt +expMjrdebitAmtJV ) +"</td>";
								
							debitexpens=debitexpens+debitAmountOpeningBal+expMjrdebitAmt + expMjrdebitAmtJV;
     						if(!String.valueOf(expMjrcreditAmt).equals("0.0")){ 
     							htmlData=htmlData+"<td align='center' style='color: #934C1E; font-weight:bold;'>"+df.format(creditAmountOpeningBal+expMjrcreditAmt+expMjrcreditAmt1) +"</td>";
     							creditexpens=creditexpens+creditAmountOpeningBal+expMjrcreditAmt; 
     						}else{ 
     							htmlData=htmlData+"<td align='center' style='color: #934C1E; font-weight:bold;'>"+df.format(creditAmountOpeningBal+expMjrcreditAmt2 +expMjrcreditAmtJV )+"</td>";
     							creditexpens=creditexpens+creditAmountOpeningBal+expMjrcreditAmt3 +expMjrcreditAmtJV ; 
    						} 
     						htmlData=htmlData+"</tr>";
    						
  							minhdlist=MINH_L.getMinorHeadBasdOnCompany(MH.getmajor_head_id());
							if(minhdlist.size()>0){  
								for(int j=0;j<minhdlist.size();j++){
									MNH=(beans.minorhead)minhdlist.get(j);
									List MinHd1=MINH_L.getMinorHeadBasdOnCompany1(MH.getmajor_head_id(),MNH.getminorhead_id(),"IE");
									String MinHead1[]=new String[MinHd1.size()];
									int p=0;
									for(p=0;p<MinHd1.size();p++){
										MNH=(beans.minorhead)MinHd1.get(p);
										MinHead1[p]=" ";
										String minorvalue='"'+MNH.getminorhead_id()+'"';
										if(!MNH.getminorhead_id().equals(minorvalue)){
											List subh1=SUBH_L.getSubheadListMinorhead2(MNH.getminorhead_id(),"IE");
											String subhd1[]=new String[subh1.size()];
											int m=0;
											for(m=0;m<subh1.size();m++){
												SUBH=(subhead)subh1.get(m);
												subhd1[m]=SUBH.getsub_head_id();
											}
											double expMnrdebitAmt = Double.parseDouble(PEXP_L.getLedgerSum1(MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd1));
											double expMnrdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"extra7",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd1));
											double expMnrdebitAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead41(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,hoaid,"sub_head_id",subhd1));
											double expMnrdebitAmt2 = Double.parseDouble(CP_L.getLedgerSum1(MNH.getminorhead_id(),"extra3",fdate,tdate,"",hoaid,"sub_head_id",subhd1));
											double expMnrcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(MNH.getminorhead_id(),"extra3",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
											double expMnrcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubheads(MNH.getminorhead_id(),"extra1",fdate,tdate,"",MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
											double expMnrcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead31(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,"","",cashtype3,MINH_L.getHeadOFAccountIDOFMajorhead_id(MH.getmajor_head_id()),"sub_head_id",subhd1));
											double expMnrcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(MNH.getminorhead_id(),"minorhead_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd1));
											double expMnrdebitAmtDollar = Double.parseDouble(CP_L.getLedgerSumDollarAmt1(MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype1,hoaid,"sub_head_id",subhd1));
									
											String amountexp=String.valueOf(expMnrdebitAmt);
											double amount2 = expMnrdebitAmt1+expMnrdebitAmt2 + expMnrcreditAmtJV +expMnrdebitAmtDollar ; 
											double debitAmountOpeningBal2 = 0.0;
											double creditAmountOpeningBal2 = 0.0;
									
											if(!("0.0".equals(""+amountexp)) || !("0.0".equals(""+amount2)) || debitAmountOpeningBal2 > 0 || creditAmountOpeningBal2 > 0){
												htmlData=htmlData+"<tr><td height='28' style='color:#f00;'><strong>"+MNH.getminorhead_id()+" "+MNH.getname()+"</strong></td>";
												htmlData=htmlData+"<td align='center' style='color:#f00;'>"+df.format(debitAmountOpeningBal2+expMnrdebitAmt +expMnrdebitAmtJV ) +"</td>";
												debit=debit+debitAmountOpeningBal2+expMnrdebitAmt +expMnrdebitAmtJV ;
      											if(!String.valueOf(expMnrcreditAmt).equals("0.0")){ 
      												htmlData=htmlData+"<td align='center' style='color:#f00;'>"+df.format(creditAmountOpeningBal2+expMnrcreditAmt1+expMnrcreditAmt) +"</td>";
      											} else{ 
      												htmlData=htmlData+"<td align='center' style='color:#f00;'>"+df.format(creditAmountOpeningBal2+expMnrcreditAmt2+expMnrcreditAmt +expMnrcreditAmtJV) +"</td>";
      											} 
      											htmlData=htmlData+"</tr>";
      											
      											List subhdlist=SUBH_L.getSubheadListMinorhead(MNH.getminorhead_id());
  												String subhd="";
        										if(subhdlist.size()>0){ 
													for(int k=0;k<subhdlist.size();k++){
														SUBH=(subhead)subhdlist.get(k);
														subhd=SUBH_L.getSubheadListMinorhead1(MNH.getminorhead_id(),SUBH.getsub_head_id(),"IE");;
														if(SUBH.getsub_head_id().equals("20201"))
															cashtype="creditsale";
														else if(SUBH.getsub_head_id().equals("20206"))
															cashtype="creditsale";
														else{
															cashtype1="othercash";
										 					cashtype2="offerKind";
															cashtype3="journalvoucher";
														}
															
														double expSubdebitAmt = Double.parseDouble(PRDEXP_L.getLedgerSum21(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,hoaid,"sub_head_id",subhd));
														double expSubdebitAmt1 = Double.parseDouble(CP_L.getLedgerSum1(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",hoaid,"sub_head_id",subhd));
														double expSubdebitAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
														double expSubdebitAmtJV = Double.parseDouble(CP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"productId",hoaid,"extra1",fdate,tdate,cashtype3,"sub_head_id",subhd));
														double expSubcreditAmtJV = Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"minorhead_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
														double expSubcreditAmtJV1 =	Double.parseDouble(PRDEXP_L.getLedgerSumBasedOnJV1(SUBH.getsub_head_id(),"sub_head_id",hoaid,"head_account_id",fdate,tdate,cashtype3,"sub_head_id",subhd));
														double expSubcreditAmt = Double.parseDouble(CP_L.getLedgerSum1a(SUBH.getsub_head_id(),"sub_head_id",fdate,tdate,"",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
														double expSubcreditAmt1 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,cashtype,cashtype1,cashtype2,cashtype3,SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
														double expSubcreditAmt2 = Double.parseDouble(CP_L.getLedgerSumOfSubhead221(SUBH.getsub_head_id(),"sub_head_id",MNH.getminorhead_id(),"extra1",fdate,tdate,"","","","",SUBH_L.getHeadOFAccountIDOFMinorhead(MNH.getminorhead_id()),"sub_head_id",subhd));
										
														String amountexp2=String.valueOf(expSubdebitAmt);
														String amount3 = String.valueOf(expSubdebitAmt1);
														String amount4 = String.valueOf(expSubdebitAmt2);
														String amount5= String.valueOf(expSubcreditAmtJV) ;
														double debitAmountOpeningBal3 = 0.0;
														double creditAmountOpeningBal3 = 0.0;
										
										 				if(!("0.0".equals(""+amountexp2)) || !("0.0".equals(""+amount3)) || !("0.0".equals(""+amount4)) || !("0.0".equals(""+amount5)) || debitAmountOpeningBal3 > 0 || creditAmountOpeningBal3 > 0){
										 					htmlData=htmlData+"<tr>";
															if(!String.valueOf(expSubcreditAmt).equals("0.0")){ 
																htmlData=htmlData+"<td height='28'><strong>"+SUBH.getsub_head_id()+" "+SUBH.getname() +"</strong></td>";
															} else{ 
																htmlData=htmlData+"<td height='28'><strong>"+SUBH.getsub_head_id()+" "+SUBH.getname() +"</strong></td>";
															} 
															htmlData=htmlData+"<td>"+df.format(debitAmountOpeningBal3+expSubdebitAmt +expSubdebitAmtJV ) +"</td>";
															debit=debit+debitAmountOpeningBal3+expSubdebitAmt +expSubdebitAmtJV ;
    														if(!String.valueOf(expSubcreditAmt).equals("0.0")){ 
    															htmlData=htmlData+"<td>"+df.format(creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt) +"</td>";
    															credit=credit+creditAmountOpeningBal3+expSubcreditAmt2+expSubcreditAmt;} else{ 
    															htmlData=htmlData+"<td>"+df.format(creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt+expSubcreditAmtJV1) +"</td>";
    															credit=credit+creditAmountOpeningBal3+expSubcreditAmt1+expSubcreditAmt +expSubcreditAmtJV1;
    														} 
    														htmlData=htmlData+"</tr>";
    													}}}}}}}}}}} 
    													
  				htmlData=htmlData+"<tr><td align='center' style='color: #934C1E; font-weight:bold;' height='28'>TOTAL AMOUNT OF EXPENS </td>";
  				htmlData=htmlData+"<td align='center' style='color: #934C1E; font-weight:bold;'>Rs."+df.format(debitexpens) +"</td><td align='center' style='color: #934C1E; font-weight:bold;'>.00</td></tr>";								
  				htmlData=htmlData+"<tr><td height='28' bgcolor='#996600' style='font-weight:bold; color:#fff'>Excess of Income over Expenditures</td>";
  						
    			if(creditincome<debitexpens){
    				htmlData=htmlData+"<td align='center' style='font-weight:bold; color: #934C1E;'>-</td><td align='center' style='font-weight:bold; color: #934C1E;'>Rs."+df.format(debitexpens-creditincome)+"</td>";	
				} else{	
					htmlData=htmlData+"<td align='center' style='font-weight:bold; color: #934C1E;'>Rs."+df.format(creditincome-debitexpens)+"</td><td align='center' style='font-weight:bold; color: #934C1E;'>-</td>";
				}
    			htmlData=htmlData+"</tr><tr><td height='28' bgcolor='#996600' style='font-weight:bold; color:#fff'>Total (Rupees)</td>";
    			
    			if(creditincome>debitexpens){	
    				htmlData=htmlData+"<td align='center' style='font-weight:bold; color: #934C1E;'>Rs."+df.format(creditincome)+"</td><td align='center' style='font-weight:bold; color: #934C1E;'>Rs."+df.format(creditincome) +"</td>";
    			}else{
    				htmlData=htmlData+"<td align='center' style='font-weight:bold; color: #934C1E;'>Rs."+df.format(debitexpens)+"</td><td align='center' style='font-weight:bold; color: #934C1E;'>Rs."+df.format(debitexpens) +"</td>";
    			    
    			}
    			htmlData=htmlData+"</tr></tbody></table>";
    			%>
		</div><%} %>
  </div>
			</div>
				
			</div>
			</div>
<%} 

out.println(htmlData);
MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
//sendMail.send("reports@saisansthan.in", "ramidiu@kreativwebsolutions.com", "","", "SSSST- DAILY INCOME & EXPENDITURE REPORT", htmlData,"localhost");
//sendMail.send("reports@saisansthan.in","nagesh4444@yahoo.com","","","SSSST-  DAILY INCOME & EXPENDITURE REPORT", htmlData,"localhost");
//sendMail.send("reports@saisansthan.in","saisansthan_dsnr@yahoo.co.in","","","SSSST-  DAILY INCOME & EXPENDITURE REPORT", htmlData,"localhost");
sendMail.send("reports@saisansthan.in","sagar@kreativwebsolutions.co.uk","","","SSSST-  DAILY INCOME & EXPENDITURE REPORT", htmlData,"localhost");
%>
</body>
</html>