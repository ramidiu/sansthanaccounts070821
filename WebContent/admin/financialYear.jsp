<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
	<div class="financialyr">
	
		<form action="adminPannel.jsp">
		<table>
		<tr>
		<td><input type="hidden" name="page" value="vendorDetails"></td>
	
			<td colspan="2">Select Financial Year: </td>
		
	<%
	String id = request.getParameter("id");
	SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
	Calendar calld = Calendar.getInstance();
	calld.add(Calendar.DATE,0);
	String todayDate=SDF.format(calld.getTime());
	String date[] = todayDate.split("-");
	 String currentyear = date[0];
	 String year2 ="";
	 int currentyearinint = Integer.parseInt(date[0]);
	 %> 
	 	<td><input type="hidden" name="id" value="<%=id %>"></td>	
	 	<%
	 if(todayDate.compareTo(currentyear+"-04-01") >=0){
		 year2 =String.valueOf(currentyearinint+1);
		 %>	 
		 	<td>
				<select name="finYear" id="finYear" >
				<option value="<%= currentyear%>to<%= year2%>" selected="selected"><%=currentyear %>-<%=year2 %></option>
					<%
						for(int i=currentyearinint;i>2015;i--){
							%>
							<option value="<%=i-1%>to<%= i%>"><%=i-1%>-<%= i%></option>
							<%
						}
					%>
				</select>
			  </td>
		 <%
	 }
	 else{
		 year2 =String.valueOf(currentyearinint-1);
		 %>
		 	<td>
				<select name="finYear" id="finYear" >
				<option value="<%= year2%>to<%= currentyear%>" selected="selected"><%=year2 %>-<%=currentyear %></option>
					<%
						for(int i=currentyearinint-1;i>2015;i--){
							%>
							<option value="<%=i-1%>to<%= i%>"><%=i-1%>-<%= i%></option>
							<%
						}
					%>
				</select>
			  </td>
		 <%
	 }
	%>
			<td><input type="submit" value="Submit" class="click"></td>
		</tr>
	
	</table>
	</form>
	</div>
</body>
</html>