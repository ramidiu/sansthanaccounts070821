<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />

 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body onload="window.print()">
	<%if(session.getAttribute("empId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">
<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
customerpurchasesListing SALE_L = new customerpurchasesListing();
banktransactionsListing BKTRAL=new banktransactionsListing();
productsListing PRDL=new productsListing();
employeesListing EMPL=new employeesListing();

List BANK_DEP=null;

Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");

DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String currentDate=(dateFormat2.format(c1.getTime())).toString();
String fromDate=currentDate+" 00:00:01";
String toDate=currentDate+" 59:00:00";
String fromDate1=currentDate+"";
String toDate1=currentDate+"";
String departname="Sai Sansthan Accounts";
mainClasses.headofaccountsListing HOAS_L=new mainClasses.headofaccountsListing();
if(session.getAttribute("headAccountId")!=null){
	departname=HOAS_L.getHeadofAccountName(session.getAttribute("headAccountId").toString());
}
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
	fromDate=request.getParameter("fromDate")+" 00:00:01";
	fromDate1=request.getParameter("fromDate");
}
if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
	toDate=request.getParameter("toDate")+" 59:00:00";
	toDate1=request.getParameter("toDate");
} 
toDate1=chngDateFormat.format(dateFormat2.parse(toDate1));
fromDate1=chngDateFormat.format(dateFormat2.parse(fromDate1));
%>
		<div class="vendor-list">
				<div class="clear"></div>
				<div align="center">SHRI SHIRIDI SAI BABA SANSTHAN TRUST<br><%=departname%><br></br> <%=fromDate1 %> To <%=toDate1 %>
				</div>
				<div class="list-details">
	<%
	List SAL_list=SALE_L.getcustomerpurchasesListBasedOnHOAandDates(session.getAttribute("headAccountId").toString(),fromDate,toDate);
	List SAL_DETAIL=null;
	double totalAmount=0.00;
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double bankDepositTot=0.00;
	double creaditSaleTot=0.00;
	double cashSaleAmt=0.00;
	if(SAL_list.size()>0){%>
	<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="20%" style="font-weight: bold;">INVOICE DATE</td>
							<td class="bg" width="5%" style="font-weight: bold;">INVOICE.NO.</td>
							<td class="bg" width="20%" style="font-weight: bold;">ACCOUNT</td>
							<td class="bg" width="10%" style="font-weight: bold;">PRODUCT QTY</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="right">DEBIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="right">CREDIT</td>
						</tr>
						<%
						Double totalSaleAmt=0.00;
						Double totDeposit=0.00;
						if(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate)!=null && !SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate).equals("")){
							 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount(session.getAttribute("headAccountId").toString(),session.getAttribute("empId").toString(),fromDate));
						}
						if(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate)!=null && !BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate).equals("")){
							 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),fromDate));}
						 %>
						<tr>
							<td colspan="3"></td>
							<td colspan="2">OPENING BALANCE</td>
							<td align="right" style="font-weight: bold;">&#8377; <%=df.format(totalSaleAmt-totDeposit)%></td>
							<td align="right" style="font-weight: bold;">&#8377;0.00</td>
						</tr>
						<%for(int i=0; i < SAL_list.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_list.get(i);
							if(!presentDate.equals("")){
								prevDate=presentDate;	
							}
							presentDate=""+chngDateFormat.format(dbDateFormat.parse(SALE.getdate()));
							if(SALE.getcash_type().equals("cash")){
								cashSaleAmt=cashSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}else{
								creaditSaleTot=creaditSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}
							%>
						    <tr style="position: relative;">
								<td style="font-weight: bold;"><%=i+1%></td>
								<td style="font-weight: bold;"><a href="#" onclick="showDetails('<%=SALE.getbillingId()%>')"><%=chngDateFormat.format(dbDateFormat.parse(SALE.getdate()))%></a></td>
								<td style="font-weight: bold;"><a href="#" onclick="showDetails('<%=SALE.getbillingId()%>')"><%=SALE.getbillingId()%></a>
								</td>
								<td style="font-weight: bold;"><span>
								<%=SALE.getcash_type() %></span></td>
								<td style="font-weight: bold;"><%=SALE.getquantity()%></td>
								<%totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());%>
								<td style="font-weight: bold;" align="right">&#8377; <%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td>
								<%if(!prevDate.equals(presentDate)){ 
								String sjDate=chngDF.format(dbDateFormat.parse(SALE.getdate()));
								String depositAmt=BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),sjDate);%>
								<td style="font-weight: bold;" align="right">&#8377;<%if(depositAmt!=null){
									//bankDepositTot=bankDepositTot+Double.parseDouble(depositAmt);
									%> <%=depositAmt%><%}else{ %>00<%} %></td>
							<%}else{ %>
								<td></td>
							<%} %>
						</tr>
						<%SAL_DETAIL=SALE_L.getBillDetails(SALE.getbillingId());
						if(SAL_DETAIL.size()>0){ %>
						<tr>
							<td colspan="7"   >
							<div style="display: none;" id="<%=SALE.getbillingId()%>">
							<table width="95%" style="border: 1px solid #000;">
							<tr>
							<td width="5%"></td>
							<td width="5%">CODE</td>
							<td width="20%">PRODUCT NAME</td>
							<td width="20%">PRODUCT SALE QTY</td>
							<td width="10%">PRICE</td>
							<td width="20%">TOTAL AMOUNT</td>
							<td width="20%">Entered by</td>
						</tr>
						<%for(int s=0;s<SAL_DETAIL.size();s++){
								SA=(beans.customerpurchases)SAL_DETAIL.get(s);%>
								<tr style="position: relative;">
									<td ></td>
									<td><%=SA.getproductId()%></td>
									<td><%=PRDL.getProductsNameByCat(SA.getproductId(), SA.getextra1())%></td>
									<td><%=SA.getquantity()%></td>
									<td><%=SA.getrate() %></td>
									<td><%=SA.gettotalAmmount()%></td>
									<td ><%=EMPL.getMemployeesName(SA.getemp_id()) %></td>
								</tr>
								<%} %>
						</table>
						</div>
						</td>
						</tr>
					   <%}} %>
					   <%//List BankDeposits=BKTRAL.getDepositsList(session.getAttribute("empId").toString(), fromDate,toDate);
					   List BankDeposits=BKTRAL.getDepositsListBasedOnHead("poojastore-counter", fromDate,toDate);
					   if(BankDeposits.size()>0){
					   		for(int j=0;j<BankDeposits.size();j++){
					   		BNK=(banktransactions)BankDeposits.get(j);
					   		bankDepositTot=bankDepositTot+Double.parseDouble(BNK.getamount());
					   %>
					   <tr style="color: fuchsia;">
					   		<td style="font-weight: bold;"><%=j+1%></td>
					   		<td style="font-weight: bold;"><%=chngDateFormat.format(dbDateFormat.parse(BNK.getdate()))%></td>
					   		<td style="font-weight: bold;"><%=BNK.getbanktrn_id()%></td>
					   		<td style="font-weight: bold;"><%=BNK.getnarration() %></td>
					   		<td style="font-weight: bold;" colspan="2" align="right"></td>
					   		<td style="font-weight: bold;" align="right"><%=df.format(Double.parseDouble(BNK.getamount()))%></td>
					   </tr>
					   
					   <%}} %>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total sales amount | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount)%></td>
						<td style="font-weight: bold;" align="right">&#8377;<%=df.format(bankDepositTot) %></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total cash sales | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(cashSaleAmt) %></td>
						<td style="font-weight: bold;"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total credit sales| </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(creaditSaleTot) %></td>
						<td style="font-weight: bold;"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Closing Balance | </td>
						<td style="color: orange;font-weight: bold;" align="right"> &#8377; <%=df.format(cashSaleAmt-bankDepositTot) %></td>
						<td style="font-weight: bold;"></td>
						</tr>
					</table>
			</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no Sales list found!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>