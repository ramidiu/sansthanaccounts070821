<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="beans.banktransactions"%>
<%@page import="java.util.Date"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.cashier_reportListing"%>
<%@page import="mainClasses.banktransactionsListing" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAILY COUNTER BALANCE REPORT</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>

<body>
<jsp:useBean id="CUS" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="BTR" class="beans.banktransactions"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">


<div style="text-align: center;"></div>
<div class="icons">
<span> <a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="total-report">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<div class="printable" >
<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td><table width="100%" cellpadding="0" cellspacing="0" class="date-wise "  >
<tr><td colspan="4" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;" class="bg-new">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="4" align="center" style="font-weight: bold;" class="bg-new">Day Wise-Income And Expenditure Report</td></tr>
<form action="adminPannel.jsp" method="post">
<input type="hidden" name="page" value="report-new"></input>
<tr>
<td width="20%">

<ul>
<%
SimpleDateFormat cdf = new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

Date date = new Date();

Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -2);
Calendar cal3 = Calendar.getInstance();
cal3.add(Calendar.DATE, -1);
Date fromdates = cal.getTime();
Date todates = cal3.getTime();
String fromdate=sdf.format(fromdates)+" 00:00:01"; 
String todate=sdf.format(todates)+" 23:59:59"; 
System.out.println("111===>"+request.getParameter("fromDate"));
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
	System.out.println("111===>"+request.getParameter("fromDate"));
							 fromdate=request.getParameter("fromDate")+" 00:00:01";
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							System.out.println("111===>"+request.getParameter("toDate"));
							todate=request.getParameter("toDate")+" 23:59:59";
						}
 /* String hoid[];
hoid=new String[]{"4","1"}; */
						%>
<li></li>
</ul></td>
<td width="30%"><input type="hidden" name="page" value="totalsalereport"/>From Date <input type="text" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="30%">To Date <input type="text" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" value="Submit" class="click bordernone"/></td>
</tr>
</form>
</table></td></tr></table>
<br/>

<div id="tblExport">
<table width="100%" cellspacing="0" cellpadding="0" border="1">
  <tr>
    <td height="36" align="center" style="font-weight: bold;color: teal;"><strong>INCOME</strong></td>
    <td align="center" style="font-weight: bold;color: teal;"><strong>DEPOSIT</strong></td>
  </tr>
  <tr>
    <td height="31">
        <table width="100%"  cellspacing="0" cellpadding="0" border="1">
          <tr>
            <td height="29" align="center" style="font-weight: bold;color:#934C1E;"><strong>Date</strong></td>
            <td align="center" style="font-weight: bold;color:#934C1E;"><strong>Head Of Account</strong></td>
            <!-- <td align="center" style="font-weight: bold;color:#934C1E;"><strong>Sub-Head</strong></td> -->
            <td align="center" style="font-weight: bold;color:#934C1E;"><strong>Amount</strong></td>
          </tr>
          <%
          customerpurchasesListing c_list =new customerpurchasesListing();
          headofaccountsListing HODL = new headofaccountsListing();
          subheadListing SUBL = new subheadListing();
           /* if(hoid.length>0){
      		for(int s=0;s<hoid.length;s++){
          List CLIST = c_list.getTotalamountBasedOnHoid(hoid[s],fromdate,todate); */
          List CLIST = null;//c_list.getTotalamountBasedOnHoid("4",fromdate,todate);
   			
          for(int i=0;i<CLIST.size();i++){ 
        	 CUS=(customerpurchases)CLIST.get(i);
          %>
          
          <tr>
          	<td height="24" align="center" style="color:#000"><%=sdf.format(dateformat.parse(CUS.getdate()))%></td>
            <td  align="center" style="color:#000"><%=HODL.getHeadofAccountName("4")%></td>
           <%--  <td  align="center" style="color:#000"><%=SUBL.getMsubheadname(CUS.getproductId())%></td> --%>
            <td  align="center" style="color:#000"><%=CUS.gettotalAmmount()%></td>
          </tr>
          
         <%
         List CLIST1 = null;//c_list.getTotalamountBasedOnHoid("1", fromdate,todate);
         
         for(int j=0;j<CLIST1.size();j++){
        	 if(j == i){
         
        	 CUS=(customerpurchases)CLIST1.get(j);
          %>
          
           <tr>
          	<td height="24" align="center" style="color:#000"><%=sdf.format(dateformat.parse(CUS.getdate()))%></td>
            <td align="center" style="color:#000"><%=HODL.getHeadofAccountName("1")%></td>
            <%-- <td align="center" style="color:#000"><%=SUBL.getMsubheadname(CUS.getproductId())%></td> --%>
            <td align="center" style="color:#000"><%=CUS.gettotalAmmount()%></td>
          </tr> 
          <%}}}%>
          
        </table>
   </td>
    <td>
        <table width="100%"  cellspacing="0" cellpadding="0" border="1">
          <tr>
            <td height="29" align="center" style="font-weight: bold;color:#934C1E;"><strong>Date</strong></td>
            <td align="center" style="font-weight: bold;color:#934C1E;"><strong>Amount Deposit category</strong></td>
            <td align="center" style="font-weight: bold;color:#934C1E;"><strong>Deposited Amount</strong></td>
            <td align="center" style="font-weight: bold;color:#934C1E;"><strong>Balance Amount</strong></td>
          </tr>
          <%
          /*  String fdate = request.getParameter("fromdate");
          System.out.println("ttt"+fromdate); */
          /* String fdate1 = sdf.format(dateformat.parse(fromdate));
          System.out.println("ttt"+fdate1); */
          Date date1 = new Date(); 
          Calendar cal1 = Calendar.getInstance();
            cal1.add(Calendar.DATE, -1);
            Calendar cal2 = Calendar.getInstance();
            cal2.add(Calendar.DATE, +1);
          Date fromdates1 = cal1.getTime();
          Date todates1=cal2.getTime(); 
          String fromdate1=sdf.format(fromdates1)+" 00:00:01";
          String todate1=sdf.format(todates1)+" 23:59:59"; 
          String reportType[];
          reportType=new String[]{"pro-counter","Charity-cash"};
          banktransactionsListing blist = new banktransactionsListing();
          if(reportType.length > 0){
        	  for(int x= 0;x< reportType.length ;x++){
          List BLIST = null;//blist.getBankTransactionBasedOnHoid(reportType[x], fromdate1, todate1);
          
          for(int y=0;y<BLIST.size();y++){
        	  BTR=(banktransactions)BLIST.get(y);
          
          %>
           <tr>
          	<td  height="24" align="center"><%=sdf.format(dateformat.parse(BTR.getdate()))%></td>
            <td align="center" style="color:#000"><%=BTR.getextra4() %></td>
            <td align="center" style="color:#000"><%=BTR.getamount() %></td>
            <td align="center" style="color:#000">10000</td>
          </tr>
          <%}}} %>
        </table>
    </td>
  </tr>
 
</table>
</div>
</div>
</div>
</div>
</div>

</body>
</html>
