<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.godwanstock"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	function filter(){
		$("#formSj").submit();
	}
	function formBlock(){
		$.blockUI({ css: { 
	        border: 'none', 
	        padding: '15px', 
	        backgroundColor: '#000', 
	        '-webkit-border-radius': '10px', 
	        '-moz-border-radius': '10px', 
	        opacity: .5, 
	        color: '#fff' 
	    } }); 
	}
	function formUnblock(){
		$.unblockUI();
	}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="../js/jquery.blockUI.js"></script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<jsp:useBean id="PRDE" class="beans.productexpenses"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">
<%headofaccountsListing HOAL=new  headofaccountsListing();
List HOALIS=HOAL.getheadofaccounts();
%>
<form action="adminPannel.jsp" method="post" id="formSj">
<div>
<input type="hidden" name="page" value="paymentbillApprovalsListbyManager">
<select name="hoa" id="hoa" onchange="filter();formBlock();">
	<option value="">ALL DEPARTMENTS</option>
	<%if(HOALIS.size()>0){ 
		for(int i=0;i<HOALIS.size();i++){
		HOA=(headofaccounts)HOALIS.get(i);
		%>
		<option value="<%=HOA.gethead_account_id() %>" <%if(request.getParameter("hoa")!=null && request.getParameter("hoa").equals(HOA.gethead_account_id())){ %> selected="selected" <%} %>> <%=HOA.getname() %> </option>
	<%}} %>
</select></div></form>
<div class="icons">
<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
</div>
<div class="clear"></div>
<div class="list-details">
<div class="printable">

<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%; margin:0 2%;
    background:#d0e3fb;
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" onload="formUnblock();" >
<tr><td colspan="7" height="10"></td> </tr>
	<tr><td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 20px;">Expenses Approval Require List </td></tr>
                      <tr>
<tr><td colspan="7"><table width="100%" cellpadding="0" cellspacing="0" id="tblExport" class="yourID">
<td class="bg" width="3%" align="center">S.No.</td>
<td class="bg" width="10%" align="center">TRACK NUMBER</td>
<td class="bg" width="10%" align="center">INVOICE NO.</td>
<td class="bg" width="10%" align="center">DEPARTMENT</td>
<td class="bg" width="15%" align="center">VENDOR NAME</td>
<td class="bg" width="10%" align="center">ENTERED DATE</td>
<td class="bg" width="10%" align="center">TOTAL BILL AMOUNT</td>
</table></td></tr>
</tr>  

<%
godwanstockListing GSK_L=new godwanstockListing();
DecimalFormat df=new DecimalFormat("0.00");
DecimalFormat formatter = new DecimalFormat("#,##,###.00");
vendorsListing VENL=new vendorsListing();
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
productexpensesListing PRDEX_L=new productexpensesListing();
String hoaid="";
if(request.getParameter("hoa")!=null && !request.getParameter("hoa").equals("")){
	hoaid=request.getParameter("hoa");
}
List PRDEXP=PRDEX_L.getproductexpensesNotApprovedByManagerByHoa(hoaid);
double totAmt=0.00;
%>
<%
if(PRDEXP.size()>0){%>


<%
for(int i=0; i < PRDEXP.size(); i++ ){
	PRDE=(productexpenses)PRDEXP.get(i);
	totAmt=totAmt+Double.parseDouble(PRDE.getamount());
%>
<tr>
<td width="3%" align="center"><%=i+1%></td>
<td width="10%" align="center" ><div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>')"><span><%=PRDE.getExtra6() %></span></div></td>
<td width="10%" align="center"><a href="adminPannel.jsp?page=payments_approval_Mananger&expinvid=<%=PRDE.getexpinv_id()%>"><%=PRDE.getexpinv_id()%></a></td>
<td width="10%"><%=HOA_CL.getHeadofAccountName(PRDE.gethead_account_id())%></td>
<td width="15%"><%=VENL.getMvendorsAgenciesName(PRDE.getvendorId())%></td>
<td width="10%" align="center"><%=df2.format(SDF.parse(PRDE.getnarration()))%></td>
<td width="10%" align="center"><%=formatter.format(Double.parseDouble(PRDE.getamount())) %></td>
</tr>
<%-- <tr>
<td width="3%" align="center"><%=i+1%></td>
<td width="10%" align="center" ><div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PRDE.getExtra6()%>')"><span><%=PRDE.getExtra6() %></span></div></td>
<td width="10%" align="center"><a href="adminPannel.jsp?page=payments_approval_Mananger&expinvid=<%=PRDE.getexpinv_id()%>"><%=PRDE.getexpinv_id()%></a></td>
<td width="10%"><%=HOA_CL.getHeadofAccountName(PRDE.gethead_account_id())%></td>
<td width="15%"><%=VENL.getMvendorsAgenciesName(PRDE.getvendorId())%></td>
<td width="10%" align="center"><%=df2.format(SDF.parse(PRDE.getnarration()))%></td>
<td width="10%" align="center"><%=formatter.format(Double.parseDouble(PRDE.getamount())) %></td>
</tr> --%>

<%
} %>
<tr><td colspan="6" align="right" class="bg" style="color: maroon;">TOTAL</td><td class="bg" style="color: maroon;"><%=formatter.format(totAmt) %></td></tr>
</table>
<%}else{%>
<div align="center"><h1>There are no list found for expenses approvals! </h1></div>
<%}%>
</div>
</div>
</div>
</div>