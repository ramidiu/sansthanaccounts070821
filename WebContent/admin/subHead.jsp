<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.subhead"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
function idValidate(){
	
	var hoa=$('#head_account_id').val();
	var id2=$('#major_head_id').val().trim();
	var id3=$('#minor_head_id').val().trim();
	var id=$('#sub_head_id').val().trim();
	/* if($('#sub_head_id').val().trim()==""){
		$('#minorIdErr').show();
		$('#sub_head_id').focus();
		return false;
	} */
	
 $.ajax({
	type:'post',
	url: 'IDValidate.jsp', 
	data: {
		Id : id,
		Id2 : id2,
		Id3 : id3,
		type : 'subhead',
		hoid : hoa
	},
	success: function(response) { 
		var msg=response.trim();
		//alert("message..."+msg);
		$('#minorIdErr').hide();
		$('#dupErr').hide();
		if(msg==="idExists"){
			$('#dupErr').show();
			$('#sub_head_id').focus();
			return false;
		}
	}}); 
}

</script>
<script type="text/javascript">

function validate(){
	/* /* $('#headIdErr').hide(); 
	$('#MajorHeadErr').hide();
	 $('#minorIdErr').hide(); 
	$('#headNameErr').hide(); */
	$('#headIdErr').hide();
	$('#MajorHeadErr').hide();
	$('#minorIdErr').hide();
	$('#headNameErr').hide();

	if($('#head_account_id').val().trim()==""){
		$('#headIdErr').show();
		$('#head_account_id').focus();
		return false;
	}
	if($('#major_head_id').val().trim()==""){
		$('#MajorHeadErr').show();
		$('#major_head_id').focus();
		return false;
	}
	 if($('#minorhead_id').val().trim()==""){
		 	$('#minorIdErr').show();
			$('#minorhead_id').focus();
			return false;
		}
    if($('#sub_head_id').val().trim()==""){
		$('#minorIdErr').show();
		$('#sub_head_id').focus();
		return false;
	}
	if($('#name').val().trim()==""){
		$('#headNameErr').show();
		$('#name').focus();
		return false;
	}
	/* if(form == "tablets_Create"){
		checkSubhead(form);	
	}else{
		$("#tablets_Update").submit();
	} */
}
</script>
<script>
/*  function checkSubhead(form){
 	var sub_head_id = $("#sub_head_id").val();
	$.post('subheadVerification.jsp',{sub_head_id:sub_head_id}, function(response){  
			if(response == 0){
					$("#tablets_Create").submit();
			}else{
				//alert("Subhead with the Id-"+sub_head_id+" is already present");
				$('#dupErr').show();
				$("#sub_head_id").focus();
				return false;
			}
	});
}  */
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<jsp:useBean id="SUB" class="beans.subhead"/>
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"><%if(request.getParameter("id")!=null) {%>Edit Sub head<%}else{%>Create a new Sub heads <%} %></div>
<div class="vender-details">
<%headofaccountsListing HOA_L=new headofaccountsListing();
List HA_Lists=HOA_L.getheadofaccounts();
majorheadListing MajorHead_list=new majorheadListing();
minorheadListing MinorHead_list=new minorheadListing();
subheadListing SUBH_L=new subheadListing();
if(request.getParameter("id")!=null) {
List SUBHD_details=SUBH_L.getMsubheadnamesBasedOnMajorhead(request.getParameter("id"),request.getParameter("major_head"));
SUB=(beans.subhead)SUBHD_details.get(0);
%>
<form name="tablets_Update" id="tablets_Update" method="post" action="subhead_Update.jsp" onSubmit="return validate();">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<input type="hidden" name="sub_head_id" id="sub_head_id" value="<%=SUB.getsub_head_id()%>"/>
<div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account*</td>
<td><div class="warning" id="MajorHeadErr" style="display: none;">Please select  "Major head".</div>Major head*</td>
<td width="35%"><div class="warning" id="MinorHeadErr" style="display: none;">Please select  "Minor head".</div>Minor head</td>
<td>
<div class="warning" id="minorIdErr" style="display: none;">Please Provide  "Sub Head Number".</div>
<div class="warning" id="dupErr" style="display: none;">Duplicate entry.Please provide another</div>
Sub Head No*</td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Sub Head Name".</div>
Sub Head Name*</td>
<td>Price</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id" onChange="combochange('head_account_id','major_head_id','getMajorHeads.jsp')" >
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>" <%if(SUB.gethead_account_id().equals(HOA.gethead_account_id())){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><select name="major_head_id" id="major_head_id" onChange="combochange('major_head_id','minor_head_id','getMinorHeads.jsp')">
<option value="<%=SUB.getmajor_head_id()%>" ><%=MajorHead_list.getmajorheadName(SUB.getmajor_head_id()) %> </option>
</select></td>
<td width="35%"><select name="minor_head_id" id="minor_head_id" >
<option value="<%=SUB.getextra1()%>"><%=MinorHead_list.getminorheadName(SUB.getextra1()) %></option>
</select></td>
<td><input type="text" name="sub_head_id" id="sub_head_id" value="<%=SUB.getsub_head_id() %>" readonly /></td>
<td><input type="text" name="name" id="name" value="<%=SUB.getname() %>" /></td>
<td><input type="text" name="subhead_price" id="subhead_price" value="<%=SUB.getextra2() %>" onKeyPress="return numbersonly(this, event,true);"/></td>
</tr>


<tr>
<td colspan="1"><input type="text" name="budgetExpenditure" value="<%=SUB.getdescription() %>" placeHolder="Enter Budget expenditure" onKeyPress="return numbersonly(this, event,true);"></td>
<td colspan="1"><input type="checkbox" name="application" id="application" value="application" <%if(SUB.getextra3().equals("application")){ %> checked <%} %>/>Application form</td>
<tr>
<td>IncomeExpenditure</td>&nbsp;
<td>ReceiptsPayments</td>&nbsp;
<!-- <td>BalanceSheet</td>&nbsp;
<td>LedgerReport</td>&nbsp; -->
<td>TrailBalance</td>&nbsp;
</tr> 
<tr>
<td><input type="checkbox" name="report" value="IE" <%if(SUB.getextra5().contains("IE")){%>checked<%}%>></td>&nbsp;
<td><input type="checkbox" name="report" value="RP" <%if(SUB.getextra5().contains("RP")){%>checked<%}%>></td>&nbsp;
<%-- <td><input type="checkbox" name="report" value="BS" <%if(SUB.getextra5().contains("BS")){%>checked<%}%>></td>&nbsp;
<td><input type="checkbox" name="report" value="LR" <%if(SUB.getextra5().contains("LR")){%>checked<%}%>></td>&nbsp; --%>
<td><input type="checkbox" name="report" value="TB" <%if(SUB.getextra5().contains("TB")){%>checked<%}%>></td>&nbsp;
</tr>
<td colspan="3">
<%-- <textarea name="description" id="description" value="<%=SUB.getdescription() %>" placeholder="Enter description"></textarea> --%></td>
<td colspan="1"></td>
</tr>
<tr> 
<td colspan="2"></td>
<td align="right" colspan="3"><input type="submit" value="UPDATE" class="click" style="border:none;" /></td>
<td colspan="1"></td>
</tr>
</table>
</form>
<% } else{%>
<form name="tablets_Create" id="tablets_Create" method="post" action="subhead_Insert.jsp" onSubmit="return validate();">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><div class="warning" id="headIdErr" style="display: none;">Please Provide  "head of account".</div>Head Of Account*</td>
<td><div class="warning" id="MajorHeadErr" style="display: none;">Please select  "Major head".</div>Major head*</td>
<td width="35%"><div class="warning" id="MinorHeadErr" style="display: none;">Please select  "Minor head".</div>Minor head</td>
<td>
<div class="warning" id="minorIdErr" style="display: none;">Please Provide  "Sub Head Number".</div>
<div class="warning" id="dupErr" style="display: none;">Duplicate entry.Please provide another</div>
Sub Head No*</td>
<td >
<div class="warning" id="headNameErr" style="display: none;">Please Provide  "Sub Head Name".</div>
Sub Head Name*</td>
<td>Price</td>
</tr>
<tr>
<td><select name="head_account_id" id="head_account_id" onChange="combochange('head_account_id','major_head_id','getMajorHeads.jsp')">
<%
if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(headofaccounts)HA_Lists.get(i);%>
<option value="<%=HOA.gethead_account_id()%>"><%=HOA.getname() %></option>
<%}} %>
</select></td>
<td><select name="major_head_id" id="major_head_id" onChange="combochange('major_head_id','minor_head_id','getMinorHeads.jsp')" value="" onBlur="idValidate();">
<option value="">Select major head</option>
</select></td>
<td width="35%"><select name="minor_head_id" id="minor_head_id" value="" onBlur="idValidate();">
<option value="">Select minor head</option>
</select></td>
<td><input type="text" name="sub_head_id" id="sub_head_id" value="" onBlur="idValidate();"/></td>
<td><input type="text" name="name" id="name" value="" /></td>
<td><input type="text" name="subhead_price" id="subhead_price" value="" onKeyPress="return numbersonly(this, event,true);"/></td>
</tr>


<tr>
<td colspan="1"><input type="text" name="budgetExpenditure" value="" placeHolder="Enter Budget expenditure" onKeyPress="return numbersonly(this, event,true);"></td>
<td colspan="1"><input type="checkbox" name="application" id="application" value="application"/>Application form</td>
<tr>
<td>IncomeExpenditure</td>&nbsp;
<td>ReceiptsPayments</td>&nbsp;
<!-- <td>BalanceSheet</td>&nbsp;
<td>LedgerReport</td>&nbsp; -->
<td>TrailBalance</td>&nbsp;
</tr> 

<tr>
<td><input type="checkbox" name="report" value="IE"></td>&nbsp;
<td><input type="checkbox" name="report" value="RP"></td>&nbsp;
<!-- <td><input type="checkbox" name="report" value="BS"></td>&nbsp;
<td><input type="checkbox" name="report" value="LR"></td>&nbsp; -->
<td><input type="checkbox" name="report" value="TB"></td>&nbsp;
</tr>

 <tr>
<td colspan="3">
<!-- <textarea name="description" id="description" placeholder="Enter description"></textarea> --></td>
<td colspan="1"></td>
</tr>
<tr> 
<td colspan="2"></td>
<td align="right" colspan="3"><input type="submit" value="Create" class="click" style="border:none;"/></td>
<td colspan="1"></td>
</tr>
</table>
</form>
<%} %>
</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="arrow-down"><img src="../images/Arrow-down.png"/></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<!-- <span><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span> -->
<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="../images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>

</span></div>
<div class="clear"></div>
<div class="list-details">
<%
List<subhead> Sub_list=SUBH_L.getsubhead();
if(Sub_list.size()>0){%>


             <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 0px;
    z-index: 1;
    width:96%;margin:0 2%;
    background:#d0e3fb;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<div id="tblExport">
<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="8"><table cellpadding="0" cellspacing="0" class="yourID" width="100%">

<tr>
<td class="bg" width="5%" align="center">S.No.</td>
<td class="bg" width="18%" align="center">Sub Head Name</td>
<td class="bg" width="15%" align="center">BUDGET EXPENDITURE </td>
<td class="bg" width="12%" align="center">Major Head </td>
<td class="bg" width="10%" align="center">Minor Head </td>
<td class="bg" width="15%" align="center">Head of account</td>
<td class="bg" width="10%" align="center">Edit</td>
<td class="bg" width="10%" align="center">Delete</td>
</tr>
</table></td></tr>
<%for(int i=0; i < Sub_list.size(); i++ ){
	SUB=(beans.subhead)Sub_list.get(i); %>
<tr>
<td width="5%" align="center"><%=SUB.getsub_head_id()%></td>
<td  width="18%" align="center"><span><%=SUB.getname()%></span></td>
<td width="15%" align="center"><%=SUB.getdescription()%></td>
<td width="12%" align="center"><%=MajorHead_list.getmajorheadName(SUB.getmajor_head_id())%></td>
<td width="10%" align="center"><%=MinorHead_list.getminorheadName(SUB.getextra1()) %></td>
<td width="15%" align="center"><%=HOA_L.getHeadofAccountName(SUB.gethead_account_id())%></td>

<td width="10%" align="center"><a href="adminPannel.jsp?page=subHead&id=<%=SUB.getsub_head_id()%>&major_head=<%=SUB.getmajor_head_id()%>">Edit</a></td>
<td width="10%" align="center"><a href="subhead_Delete.jsp?Id=<%=SUB.getsub_head_id()%>">Delete</a></td>
</tr>
<%} %>
</table>
<%}else{%>
<div align="center"><h1>There are no sub heads</h1></div>
<%}%>
</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
<script>
window.onload=function a(){
	combochange('head_account_id','major_head_id','getMajorHeads.jsp')
	setTimeout(function() {
		combochange('major_head_id','minor_head_id','getMinorHeads.jsp')
	}, 10000);
	};
</script>