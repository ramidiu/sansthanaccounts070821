<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.util.TimeZone"%>
<%@page import="model.SansthanAccountsDate"%>
<%@page import="beans.banktransactions"%>
<%@page import="java.util.Date"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="beans.customerpurchases"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.cashier_reportListing"%>
<%@page import="mainClasses.banktransactionsListing" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAILY COUNTER BALANCE REPORT</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 600;
		modalWindow.content = "<iframe width='800' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
	
	
	/* $(document).ready(function(){ */
		$(function() {
			$( "#fromDate" ).datepicker({
				minDate: new Date(2016, 03, 1),
				maxDate: new Date(2017, 02, 31),
				changeMonth: true,
				changeYear: true,
				numberOfMonths: 1,
				showOn: 'both',
				buttonImage: "../images/calendar-icon.png",
				buttonText: 'Select Date',
				 buttonImageOnly: true,
				dateFormat: 'yy-mm-dd',
				onSelect: function( selectedDate ) {
					$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
				} 
			});
				$( "#toDate" ).datepicker({
					minDate: new Date(2016, 03, 1),
					maxDate: new Date(2017, 02, 31),
				changeMonth: true,
				changeYear: true,
				numberOfMonths:1,
				showOn: 'both',
				buttonImage: "../images/calendar-icon.png",
				buttonText: 'Select Date',
				 buttonImageOnly: true,
				dateFormat: 'yy-mm-dd',
				onSelect: function( selectedDate ) {
					$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
				} 
			});
		});
		/* }); */

		function datepickerchange()
		{
			var finYear=$("#finYear").val();
			var yr = finYear.split('-');
			var startDate = yr[0]+",04,01";
			var endDate = parseInt(yr[0])+1+",03,31";
			
			$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
			$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
			$('#toDate').datepicker('option', 'minDate', new Date(startDate));
			$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
		}

		$(document).ready(function(){
			datepickerchange();
		});
</script>
<script>
 /*  $(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
	});	
}); */   
function showDetails(billId){
	$("#"+billId).slideToggle();
}
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>

<body>
<jsp:useBean id="CUS" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<jsp:useBean id="BTR" class="beans.banktransactions"></jsp:useBean>
<div class="vendor-page">
<div class="vendor-list">


<div style="text-align: center;"></div>
<div class="icons">
<span> <a id="print"><img src="../images/printer.png" style="margin:0 20px 0 0" title="print"/></span>
<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin:0 20px 0 0" title="export to excel"/></span>
</div>
<div class="clear"></div>
<div class="total-report">
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:100%;
    background:#d0e3fb;
}

</style>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>

<div class="printable" >
<table width="100%" cellpadding="0" cellspacing="0" >
<tr><td><table width="100%" cellpadding="0" cellspacing="0" class="date-wise "  >
<tr><td colspan="5" align="center" style="font-weight: bold;color: red;" class="bg-new"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;" class="bg-new">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500060</td></tr>
<tr><td colspan="5" align="center" style="font-weight: bold;" class="bg-new">Daily Counter Balance Report(Online And Card)</td></tr>
<form action="adminPannel.jsp" method="post">
<input type="hidden" name="page" value="DailycounterBalanceReport_cardonline"></input>
<tr>
<ul>
<%
DecimalFormat DF = new DecimalFormat("#.00;-#.00");
customerpurchasesListing c_list =new customerpurchasesListing();
bankbalanceListing BBAL_L=new mainClasses.bankbalanceListing();
headofaccountsListing HODL = new headofaccountsListing();
subheadListing SUBL = new subheadListing();
banktransactionsListing blist = new banktransactionsListing();

SimpleDateFormat cdf = new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");

c1.getActualMaximum(Calendar.DAY_OF_MONTH);
int lastday = c1.getActualMaximum(Calendar.DATE);
c1.set(Calendar.DATE, lastday);  
String todate=(sdf.format(c1.getTime())).toString();
c1.getActualMinimum(Calendar.DAY_OF_MONTH);
int firstday = c1.getActualMinimum(Calendar.DATE);
c1.set(Calendar.DATE, firstday); 
String fromdate=(sdf.format(c1.getTime())).toString();



//String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
//String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
							}
/*  String hoid[]=new String[]{"4","1"};
String reportType[]=new String[]{"pro-counter","Charity-cash"}; */
						%>
<li></li>
</ul>
<%-- <td width="20%">
Select Fin Year : 
	<select name="finYear" id="finYear"  Onchange="datepickerchange();">
		<option value="2016-17" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2016-17")) {%> selected="selected"<%} %>>2016-17</option>
		<option value="2015-16" <%if(request.getParameter("finYear") != null && request.getParameter("finYear").equals("2015-16")) {%>selected="selected"<%} %>>2015-16</option>
	</select>
</td>		 --%>


			<td width="20%">Select Fin Year : 
			<select name="finYear" id="finYear"  Onchange="datepickerchange();">
			<%@ include file="yearDropDown1.jsp" %>
					</select>
			</td> 

<%String hoaid="";
String headgroup="";
String reporttype="";
String reporttype1="";
headofaccountsListing HOA_L=new headofaccountsListing();
List headaclist=HOA_L.getheadofaccounts(); 
if(request.getParameter("head_account_id")!=null){
	hoaid=request.getParameter("head_account_id");
	System.out.println("head account111..."+hoaid);
	}

%>

<td width="20%"><input type="hidden" name="page" value="totalsalereport"/>From Date <input type="text" name="fromDate" id="fromDate" class="DatePicker" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="20%">To Date <input type="text" name="toDate" id="toDate" class="DatePicker" readonly="readonly" value="<%=todate%>"/> </td>
<td width="20%"><select name="head_account_id" id="head_account_id">
<option value="">Select Department</option>

<option value="1" <%if(hoaid != null && hoaid.equals("1")) {%> selected="selected" <%} %> >CHARITY</option>
<option value="4" <%if(hoaid != null && hoaid.equals("4")) {%> selected="selected" <%} %> >SANSTHAN</option>

</select></td>
<td width="15%"><input type="submit" value="Submit" class="click bordernone"/></td>
</tr>
</form>
</table></td></tr></table>

<br/>
<%  
if(request.getParameter("head_account_id")!=null){
	hoaid=request.getParameter("head_account_id");
	 //System.out.println("hoid value123..."+hoaid);

 if(hoaid.equals("4")){
		//System.out.println("sansthan...");
		headgroup="1";
		reporttype = "pro-counter";
		reporttype1 = "Development-cash";	
	}else if(hoaid.equals("1")){
		reporttype = "Charity-cash";
		reporttype1 = "Not-Required";	
	}
	else if(hoaid.equals("3")){
		reporttype = "poojastore-counter";
		reporttype1 = "Not-Required";
	}
	else if(hoaid.equals("5")){
		reporttype = "Sainivas";
		reporttype1 = "Not-Required";	
	} 
}
%>
<%if(request.getParameter("finYear") != null && !request.getParameter("finYear").equals("") && !hoaid.trim().equals("")) {

	String head_account_name = HOA_L.getHeadofAccountName(hoaid);
%>
<div id="tblExport">

<!-- SANSTHAN -->
<table width="100%" border="1" cellspacing="0" cellpadding="0">
<tr><td colspan="8" style="font-weight:bold;color:#f00;text-align:center;padding:5px 0px 5px 0px;"><%=head_account_name %> COUNTER BALANCE AND DEPOSITED AMOUNT</td></tr>
  <tr>
    <td colspan="3"  align="center" style="font-weight: bold;color: teal;">INCOME</td>
    <td colspan="5" align="center"  style="font-weight: bold;color: teal;">DEPOSIT</td>
  </tr>
  
  <tr>
    <td align="center" style="font-weight: bold;color:#934C1E;">Date</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Head Of Account</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Counter Amount</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Date</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Account Deposit Category</td>
    <td align="center" style="font-weight: bold;color:#934C1E;">Deposit Amount</td>
   <!--  <td align="center" style="font-weight: bold;color:#934C1E;">Balance Amount</td> -->
    <td align="center" style="font-weight: bold;color:#934C1E;">Total Bal Amount</td>
  </tr>
  <%
  String addedDate1=SansthanAccountsDate.getAddedDate(fromdate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
  double sansthantotalcountercash =0.0;
  double sansthantotalcashpaid =0.0;
  double sansthangrandtotal = 0.0;
  String finStartYr = request.getParameter("finYear").substring(0, 4);
  double finOpeningBalSansthan = BBAL_L.getOpeningBalOfCounterCash(hoaid, request.getParameter("finYear"));
  if(c_list.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate) != null && !c_list.getTotalAmountBeforeFromdate(hoaid,finStartYr+"-04-01", fromdate).equals("")){
	  sansthantotalcountercash = Double.parseDouble(c_list.getTotalAmountBeforeFromdateCardOnline(hoaid,finStartYr+"-04-01", fromdate));
  }
   if(blist.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01",addedDate1)!= null && !blist.getTotalamountDepositedBeforeFromdate(reporttype,reporttype1,finStartYr+"-04-01", addedDate1).equals("")){
	  sansthantotalcashpaid = Double.parseDouble(blist.getTotalamountDepositedBeforeFromdateCardOnline(reporttype,reporttype1,finStartYr+"-04-01", addedDate1));
  } 
 /*  double sansthanonlinedeposits = 0.0;
  if(blist.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1)!= null && !blist.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1).equals("")){
	  sansthanonlinedeposits = Double.parseDouble(blist.getTotalOnlineDepositedBeforeFromdate(reporttype,finStartYr+"-04-01", addedDate1));
  } */
  /* sansthangrandtotal = sansthantotalcountercash-sansthantotalcashpaid; */
  sansthangrandtotal = finOpeningBalSansthan+sansthantotalcountercash-sansthantotalcashpaid;

  double openingbal=0.0;
  openingbal = sansthangrandtotal;
  %>
  <tr>
  <td colspan="6" align="center" style="font-weight: bold">OPENING BALANCE</td>
  <td style="font-weight: bold"><%=DF.format(openingbal) %></td>
  </tr>
  <%
  System.out.println("111111111111111111");
  List<String> listOfDates= SansthanAccountsDate.getAddedBetweenDates(fromdate, todate, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 0);
  
  double totalamt11=0.0;
  double totalamt22=0.0;
  double grandTotal=0.0;
  double totalamt=openingbal;
  double santhanprototal=0.0;
  double santhandeposittotal=0.0;%>
  <tr>
	    <td style="color:#000;">--</td>
	    <td style="color:#000;">--</td>
	    <td style="color:#000;">--</td>
	    <%
	    //System.out.println("22222222222222222222222222");
	    String[]  BLIST11= blist.getBankTransactionBasedOnOnlineCard(reporttype,reporttype1, fromdate+" 00:00:00", fromdate+" 23:59:59");
	   // System.out.println("333333333333333333333333");
	    totalamt = (Double.parseDouble("0.0")-Double.parseDouble(BLIST11[1])) + openingbal;
	    totalamt11 = totalamt11 + (Double.parseDouble("0.0")-Double.parseDouble(BLIST11[1]));
	    %>
	   
	    <td style="color:#000;background:rgba(228, 220, 220,1);"><%=fromdate%></td>
	    <td style="color:#000;background:rgba(228, 220, 220, 1);"><%=BLIST11[0] %></td>
	    <%System.out.println("blist...1111"+BLIST11[0]);%>
	    <td style="color:#000;background:rgba(228, 220, 220, 1);"><a href="adminPannel.jsp?page=DailycounterBalanceDayWiseCardOnline&dt=<%=fromdate%>&rt=<%=hoaid %>" target="_blank"><%=DF.format(Double.parseDouble(BLIST11[1]))%></a></td>
	    <%-- <td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format(Double.parseDouble("0.0")-Double.parseDouble(BLIST11[1])) %></td> --%>
	    <td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format(totalamt)%></td>
	    <%System.out.println("total amount is........"+DF.format(totalamt)); %>
	  </tr>
 <% for(int i=0;i<listOfDates.size()-1;i++){
			 double totalLocal=0.0;
			// double totalLocal1=0.0;
		  String dateNow=listOfDates.get(i);
		  //System.out.println("dateNow======>"+dateNow);
		  String[] CLIST = c_list.getTotalamountBasedOnOnlineCard(hoaid,dateNow+" 00:00:01",dateNow+" 23:59:59");
	  %>
	  <tr>
	    <td style="color:#000;"><%=dateNow%></td>
	    <td style="color:#000;"><%=HODL.getHeadofAccountName(CLIST[0])%></td>
	    <td style="color:#000;"><%=CLIST[1] %></td>
	
	    <%
	    System.out.println("Clist amounts==>"+CLIST[1]);
	    String addedDate=SansthanAccountsDate.getAddedDate(dateNow, "yyyy-MM-dd", "yyyy-MM-dd", TimeZone.getTimeZone("IST"), 1);
	    
	    /* String[]  BLIST= blist.getBankTransactionBasedOnHoid("pro-counter","Development-cash", addedDate+" 00:00:00", addedDate+" 23:59:59"); */
			String[]  BLIST= blist.getBankTransactionBasedOnOnlineCard(reporttype,reporttype1, addedDate+" 00:00:00", addedDate+" 23:59:59");	   
	       totalLocal =  Double.parseDouble(CLIST[1])-Double.parseDouble(BLIST[1]);
	   // totalLocal1=  -Double.parseDouble(CLIST[1])+Double.parseDouble(BLIST[1]);
	   totalamt =  totalamt + totalLocal;
	   System.out.println("totalamt......==>"+totalamt);
	   System.out.println("Blist amounts.....==>"+BLIST[0]); 
	    %>
	   
	    <td style="color:#000;background:rgba(228, 220, 220,1);"><%=addedDate%></td>
	    <td style="color:#000;background:rgba(228, 220, 220, 1);"><%=BLIST[0] %></td>
	    <td style="color:#000;background:rgba(228, 220, 220, 1);"><a href="adminPannel.jsp?page=DailycounterBalanceDayWiseCardOnline&dt=<%=addedDate%>&rt=<%=hoaid%>" target="_blank"><%=DF.format(Double.parseDouble(BLIST[1]))%></a></td>
	    <%-- <td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format(totalLocal)%></td> --%>
	    <td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format(totalamt)%></td>
	    <%System.out.println("Blist amount is.."+DF.format(totalLocal)); %>
	  </tr>
	  <%santhanprototal= santhanprototal + Double.parseDouble(CLIST[1]);
	  santhandeposittotal = santhandeposittotal + Double.parseDouble(BLIST[1]);
	  grandTotal = grandTotal+ totalLocal; %>
	  <%} %> 
	  <tr>
	  <%
	  String[] CLIST11 = c_list.getTotalamountBasedOnOnlineCard(hoaid,todate+" 00:00:01",todate+" 23:59:59");
	  %>
	    <td style="color:#000;"><%=todate%></td>
	    <td style="color:#000;"><%=HODL.getHeadofAccountName(CLIST11[0])%></td>
	    <td style="color:#000;"><%=CLIST11[1] %></td>
	    <%
	    System.out.println("deposited amount......."+todate); 
	    totalamt22 = totalamt22 + (Double.parseDouble(CLIST11[1])-Double.parseDouble("0.0"));
	    System.out.println("111111111111....."+totalamt22); 
	    %>
	   
	    <td style="color:#000;background:rgba(228, 220, 220,1);">--</td>
	    <td style="color:#000;background:rgba(228, 220, 220, 1);">--</td>
	    <td style="color:#000;background:rgba(228, 220, 220, 1);">--</td>
	    <%-- <td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format(Double.parseDouble(CLIST11[1])-Double.parseDouble("0.0")) %></td> --%>
	    <td style="color:#000;background:rgba(120, 45, 22, 0.34);"><%=DF.format(totalamt + totalamt22)%></td>
	  </tr>
	  
  <tr>
  <td colspan="2" align="center" style="font-weight: bold">TOTAL</td>
  <td style="font-weight: bold"><%=DF.format(santhanprototal+totalamt22) %></td>
  <td colspan="2" style="font-weight: bold">&nbsp;</td>
  <td colspan="1" style="font-weight: bold"><%=DF.format(santhandeposittotal) %></td>
 <!--  <td colspan="1" style="font-weight: bold">&nbsp;</td> -->
  <td style="font-weight: bold"><%=DF.format(grandTotal + openingbal + totalamt22 + totalamt11) %></td>
  </tr>
</table>
<br/>
<br/>

<br/>
</div>
<%} %>
</div>
</div>
</div>
</div>

</body>
</html>
