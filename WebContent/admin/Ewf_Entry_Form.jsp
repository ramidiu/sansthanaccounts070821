<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.majorhead"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EWF-ENTRY</title>
<!--Date picker script  -->
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});		
});
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function calculate(i){
	var u=0;
	var debitamount=0.0;
	var creditamount=0.0;
	u=document.getElementById("addcount").value;
		if(document.getElementById("majorhead"+i).value!="" && document.getElementById("minorhead"+i).value!="" && document.getElementById("subhead"+i).value!=""){
			for(var j=0;j<u;j++){
			if(document.getElementById("debitAmt"+j).value!=""){
				debitamount=Number(parseFloat(debitamount)+parseFloat(document.getElementById("debitAmt"+j).value)).toFixed(2);
				}
			if(document.getElementById("creditAmt"+j).value!=""){
				creditamount=Number(parseFloat(creditamount)+parseFloat(document.getElementById("creditAmt"+j).value)).toFixed(2);
				}	
			}
		document.getElementById("credittotalamnt").value=creditamount;
		document.getElementById("debittotalamnt").value=debitamount;
	}

}
 
function majorheadsearch(j){
	  var data1=$('#majorhead'+j).val();
	  var hoid=$('#hoid').val();
var arr = [];
$.post('searchMajor.jsp',{q:data1,HAid:hoid},function(data)
{
	 
	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }

		var availableTags=arr;
		$( '#majorhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 
		 }); 
		});
} 
function minorheadsearch(j){
	  var data1=$('#minorhead'+j).val();
	  var majorhead=$('#majorhead'+j).val();
var arr = [];
$.post('searchMinior.jsp',{q:data1,q1:majorhead},function(data)
{	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }
		var availableTags=arr;
		$( '#minorhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 }); 
		});
}
function subheadsearch(j){
	  var minorhead=$('#minorhead'+j).val();
	  var majorhead=$('#majorhead'+j).val();
	  var subhead=$('#subhead'+j).val();
	  var hoa=$('#hoid'+j).val();
var arr = [];
$.post('searchSubhead.jsp',{q:subhead,HID:hoa,q1:majorhead,q2:minorhead},function(data)
{
	 
	var response = data.trim().split("\n");
	  for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 arr.push({
			 label: d[0]+" "+d[1],
		        sortable: true,
		        resizeable: true
		    });
	 }

		var availableTags=arr;
		$( '#subhead'+j).autocomplete({
				 source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
								}	 
		 
		 }); 
		});
} 

</script>
<script>
$(document).ready(function() {
	 $('input[type=radio][name=jvtype]').change(function() {
		if(this.value == "withloanbank"){
			document.getElementById("debit").innerHTML = "DEBIT AMOUNT(-)";
			document.getElementById("credit").innerHTML = "CREDIT AMOUNT(+)";
		}else if(this.value == "withbank"){
			document.getElementById("debit").innerHTML = "DEBIT AMOUNT(+)";
			document.getElementById("credit").innerHTML = "CREDIT AMOUNT(-)";
		}else if(this.value == "withoutbank"){
			document.getElementById("debit").innerHTML = "DEBIT AMOUNT(+)";
			document.getElementById("credit").innerHTML = "CREDIT AMOUNT(-)";
		}
	 	});
	});
</script>
<!--Date picker script  -->
<script type='text/javascript' src='../main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='../main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="../main/jquery.autocomplete.css"/>
<script type='text/javascript' src='../main/jquery.autocomplete.js'></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
</head>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<form name="tablets_Update" method="post" action="Ewf_entry_Insert.jsp" onsubmit="return Confirm();">
<div class="vendor-page">

<div class="vendor-box">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">EWF ENTRY</td></tr>
</table>
<div class="vender-details">
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
double creditamount,debitamount;
creditamount=debitamount=0.0;
majorheadListing MJRL=new majorheadListing();
subheadListing SUBL=new subheadListing();
minorheadListing MINL=new minorheadListing();
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
List HA_Lists=HOA_L.getheadofaccounts();
int noofrooms,l;
noofrooms=l=0;

if(request.getParameter("noofrooms")!=null && request.getParameter("noofrooms")!=""){
	noofrooms=(Integer.parseInt(request.getParameter("noofrooms").toString()));
}
String subheadarray[]={"ROOM RENT","SERVICE TAX","LUXOURY TAX","COMPUTER CHARGES","EXTRA PERSON CHARGES" ,""};
String subheadamount[]={"roomrent","servicetax","luxurytax","computercharges","extrapersoncharges" ,""};
StringBuilder majorhead=new StringBuilder();
StringBuilder roomtype=new StringBuilder();
StringBuilder minorhead=new StringBuilder();
StringBuilder subhead=new StringBuilder();
StringBuilder amount=new StringBuilder();
%>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- <td>EWF No</td> -->
<td>Date</td>
<td>Department</td>
</tr>
<tr>
<%-- <td><input type="text" name="billnumb" id="billnumb" value="<%=ng_LST.getid("ewf_entries")%>"  readonly="readonly"/></td> --%>
<td><input type="text" name="date" id="date"  value="<%=currentDate%>" class="DatePicker" readonly/></td>
<td><select name="hoid" id="hoid">
<option value="10"> E.W.F </option>
</select></td>
</tr>
<tr><td>Reference No</td><td>Narration*</td>
<!-- <td>Journal Voucher Type</td> -->
</tr>
<tr>
<td><input type="text" name="voucherRefNo" id="billnumb" value=""   placeHolder="Enter reference no"/></td>
<td><input  name="narration" id="narration" required placeHolder="Write your narration" onkeyup="javascript:this.value=this.value.toUpperCase();"/></input></td>

</table>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list" style="">
<div class="bgcolr yourID"  style=" float:left;width:100%;">
<div class="sno1 bgcolr">S.NO.</div>
<div class="ven-nme1 bgcolr" style="width:250px; ">MAJOR HEAD NAME</div>
<div class="ven-nme1 bgcolr" style="width:250px; ">MINOR HEAD NAME</div>
<div class="ven-nme1 bgcolr" style="width:250px; ">SUB HEAD NAME</div>
<div class="ven-nme1 bgcolr" id="debit">DEBIT AMOUNT(+)</div>
<div class="ven-nme1 bgcolr" id="credit">CREDIT AMOUNT(-)</div>
<!-- <div class="ven-amt1 bgcolr">VAT(%)</div>
<div class="ven-amt1 bgcolr" style="width:auto;float:left; ">GROSS AMOUNT</div> -->
</div>
<input type="hidden" name="addcount" id="addcount" value="5"/>

<%
for(int i=0; i < 50; i++ ){
%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">
<div class="sno1 MT13"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="majorhead<%=i%>" id="majorhead<%=i%>"  onkeyup="majorheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a majorhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="minorhead<%=i%>" id="minorhead<%=i%>"  onkeyup="minorheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a minorhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="subhead<%=i%>" id="subhead<%=i%>"  onkeyup="subheadsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a subhead here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1">&#8377;<input type="text" name="debitAmt<%=i%>" id="debitAmt<%=i%>"  onkeyup="calculate('<%=i%>')" value="0"  style="width: 150px;"></input></div>
<div class="ven-nme1">&#8377;<input type="text" name="creditAmt<%=i%>" id="creditAmt<%=i%>"  value="0"  onkeyup="calculate('<%=i%>')" style="width: 150px;"></input></div>
<div class="clear"></div>
</div>
<%} %>

<div class="add-ven">
<span style="margin:0 80px 0 0">Total Amount : </span>
&#8377;<input type="text" name="debittotalamnt" id="debittotalamnt" value="0.0" style="width: 150px;"/>
&#8377;<input type="text" name="credittotalamnt" id="credittotalamnt" value="<%=creditamount %>" style="width: 150px;"/>
</div>
<div class="add-ven">
<input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/>
<span style="margin:0 80px 0 0">&nbsp;</span>
<span style="margin:0 360px 0 0">&nbsp;</span>
<input type="reset" value="Reset" class="click" style="border:none;"/>
<input type="submit" name="saveonly" value="CREATE EWF" class="click" style="border:none; "/>
</div>
<div class="tot-ven"></div>
</div>
</div>
</form>
</body>
</html>