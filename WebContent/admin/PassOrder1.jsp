<%@page import="java.util.List"%>
<%@page import="beans.billimages"%>
<%@page import="mainClasses.billimagesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.NumberToWordsConverter"%>
<%@page import="beans.bankdetails"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.paymentsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
@media print {
	table{width:100%; border-bottom:1px solid #000; border-left:1px solid #000; position:relative; left:8px;}
	table.pass-order td { font-size:15px; padding:7px 5px; margin:0 0 0px 0; font-family:Arial, Helvetica, sans-serif; border-top:1px solid #000; border-right:1px solid #000;}
	table.pass-order td ul{ margin:0; padding:0px;}
	table.pass-order td ul li{ float:left; list-style:none; margin:0 0 10px 0;}
table.pass-order td ul li.second{ /*border-bottom:1px solid #000;  width:150px;*/ padding:0 0 0 10px; margin:0 10px 0 0; font-weight:bold;}
table.pass-order td span{ text-decoration:none; font-size:14px;}
table.pass-order td table.sub-order{ border-bottom:1px solid #000; border-left:1px solid #000; margin:0 0 0px 0;}
table.pass-order td table.sub-order td{ border-top:1px solid #000; border-right:1px solid #000; font-size:15px; padding:5px;}
.member-list
{
	margin:5px 0 5px 40px;  
}
p
{
	line-height:25px;
}
	
}
</style>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
 function displayBlock(){
	 $("#mseRep").toggle();
 }
 function billdisplayBlock(){
	 $("#billAtc").toggle();
 }
 
    </script>
    <script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!-- main content -->
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
<jsp:useBean id="EXP" class="beans.productexpenses"></jsp:useBean>
<jsp:useBean id="GOD" class="beans.godwanstock"></jsp:useBean>
<jsp:useBean id="IND" class="beans.indent"></jsp:useBean>
<jsp:useBean id="PO" class="beans.purchaseorder"></jsp:useBean>
<jsp:useBean id="INDAP" class="beans.indentapprovals"></jsp:useBean>
<jsp:useBean id="BNK" class="beans.bankdetails"></jsp:useBean>
<jsp:useBean id="VEN" class="beans.vendors"></jsp:useBean>
<jsp:useBean id="BIL" class="beans.billimages"></jsp:useBean>
	<div>
		<div class="vendor-page">
		<div class="vendor-list">
		<div class="arrow-down"></div>
			<div class="icons"><a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
			<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
		<%String payID=request.getParameter("payId");
			SimpleDateFormat CDF=new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat SDF1=new SimpleDateFormat("yyyy-MM-dd");
			paymentsListing PAYL=new paymentsListing();
			productexpensesListing EXPL=new productexpensesListing();
			godwanstockListing GODL=new godwanstockListing();
			purchaseorderListing POL=new purchaseorderListing();
			indentListing INDL=new indentListing();
			indentapprovalsListing INDAPL=new indentapprovalsListing();
			List PAY_DET=PAYL.getPassOrderInfo(payID);
			List EXP_Det=null;
			List GOD_DET=null;
			List PO_DET=null;
			List IND_DET=null;
			List INDAP_DET=null;
			if(PAY_DET.size()>0){
				PAY=(beans.payments)PAY_DET.get(0);
				 EXP_Det=EXPL.getMproductexpensesBasedOnExpInvoiceID(PAY.getextra3());
				 if(EXP_Det.size()>0){
					 EXP=(beans.productexpenses)EXP_Det.get(0);
					 GOD_DET=GODL.getgodwanstockByinvoiceId(EXP.getextra5());
					 if(GOD_DET.size()>0){
						 GOD=(beans.godwanstock)GOD_DET.get(0);
						 PO_DET=POL.getMpurchaseorderBasedONPoINVID(GOD.getextra2());
						 if(PO_DET.size()>0){
							 PO=(beans.purchaseorder)PO_DET.get(0);
							 IND_DET=INDL.getMindentsByIndentinvoice_id(PO.getindentinvoice_id());
							 if(IND_DET.size()>0){
								 IND=(beans.indent)IND_DET.get(0);
							 }
						 }
					 }
				 }
			}
			%>
		<div class="printable">
			<table width="95%" cellpadding="0" cellspacing="0" class="pass-order">
				<tr>
					<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST POOJA STORES</td>
				</tr>
				<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 14px; border-top:none;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
				<tr><td colspan="7" align="center" style="font-weight: bold; font-size:18px;">PASS ORDER</td></tr>
				<tr>
                    <td colspan="7" >
                    <ul>
<li>Pass Order NO :</li>
 <li class="second"><%=payID %></li>
 <li>On</li>
 <li class="second"><%=CDF.format(SDF.parse(PAY.getdate()))%></li>
</ul></td>
                    
                 </tr>
                 <tr>
                    <td colspan="7" ><ul><li>Indent NO : </li>   <li class="second"><%=IND.getindentinvoice_id()%></li>
                   <li>On</li> <li class="second"><%=CDF.format(SDF.parse(IND.getdate()))%></li>  </ul> </td>
                 </tr>
                  <%
                 headofaccountsListing HODL=new headofaccountsListing();
                  majorheadListing MJHDL=new majorheadListing();
                  minorheadListing MINL=new minorheadListing();
                  subheadListing SUBL=new subheadListing();
                  superadminListing SADL=new superadminListing();
                  productsListing PROL=new productsListing();
                  INDAP_DET=INDAPL.getIndentDetails(IND.getindentinvoice_id());
                  SimpleDateFormat time=new SimpleDateFormat("HH:mm");
                  employeesListing EMPL=new employeesListing();
                  %>
                  <tr>
                    <td colspan="7" height="30">
                    <div>Indent Approved by:&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    	<%if(INDAP_DET.size()>0){ 
                    		for(int i=0;i<INDAP_DET.size();i++){
                    		INDAP=(beans.indentapprovals)INDAP_DET.get(i);
                    		%>
                   		 <div class="member-list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=i+1 %>&nbsp;.&nbsp;<%=SADL.getSuperadminname(INDAP.getadmin_id()) %>&nbsp;&nbsp;On&nbsp;&nbsp;<strong><%=CDF.format(SDF.parse(INDAP.getapproved_date())) %></strong>&nbsp;&nbsp;at&nbsp;&nbsp;<strong><%=time.format(SDF.parse(INDAP.getapproved_date())) %></strong></div>
                   		<%}} %>
                   	</td>
                  </tr>	
                  <tr>
                    <td colspan="7" ><ul>
<li>Purchase Order NO : </li>   <li class="second"><%=PO.getpoinv_id() %></li>
                    <li>On</li><li class="second"><%=CDF.format(SDF.parse(PO.getcreated_date()))%></li>
</ul></td>
                 </tr>
                 <tr>
                    <td colspan="7" >
                    <ul>
                    <li >M.S.E NO : </li>   
                    <li class="second" onclick="displayBlock();" style="cursor:pointer;"><%=GOD.getextra1()%></li>
                    <li>On</li>
                    <li class="second"><%=CDF.format(SDF1.parse(GOD.getdate()))%></li>
                     </ul>
                     </td>
                 </tr>
                 <tr>
                 	<td colspan="7">
                 		<div style="display: block;" id="mseRep"><table width="100%" cellpadding="0" cellspacing="0" class="sub-order" border="0" > 
                 			<tr>
                            <td colspan="6">Stock ledger Entry No : <%=GOD.getextra1()%></td>
                 			</tr>
                 			<tr>
                            <td>Product Id</td>
                            <td>Name</td>
                            <td>Qty</td>
                            <td>Price</td>
                            <td>Vat</td>
                            <td>Tot Amt</td>
                            </tr>
                 			<%
                 			double totAmt=0.00;
                 			if(GOD_DET.size()>0){
                 				for(int i=0;i<GOD_DET.size();i++){
       						 GOD=(beans.godwanstock)GOD_DET.get(i);
       						totAmt=totAmt+Math.round((Double.parseDouble(GOD.getquantity()))*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100)));
       						 %>
       						 <tr>
                             <td><%=GOD.getproductId() %></td>
                             <td><%=PROL.getProductsNameByCat(GOD.getproductId(), "") %></td>
                             <td><%=GOD.getquantity() %></td>
                             <td><%=GOD.getpurchaseRate() %></td>
                             <td><%=GOD.getvat() %></td>
                             <td><%=Math.round((Double.parseDouble(GOD.getquantity()))*(Double.parseDouble(GOD.getpurchaseRate())*(1+Double.parseDouble(GOD.getvat())/100)))%></td>
                             </tr>
       						 <%} }%>
       						 <tr><td colspan="5" align="right">Total Amount-</td>
                             <td><%=totAmt%></td></tr>
                 		</table></div>
                 	</td>
                 </tr>
				<tr>
                    <td colspan="7" >
                    <ul>
                    <li>Bill NO : </li>
                    <li onclick="billdisplayBlock();" class="second" style="cursor:pointer;"><%=GOD.getbillNo() %></li>
                    <li>On</li>
                    <li class="second"><%=CDF.format(SDF1.parse(GOD.getdate()))%></li> 
                    </ul>
                    </td>
                 </tr>
                <%billimagesListing BILIMG=new  billimagesListing();
                List Bil_Det=BILIMG.getListImage(GOD.getextra1());%>
                 <%if(Bil_Det.size()>0){ %>
                <tr>
                <td colspan="7">
                 <div  id="billAtc" style="display: block;"><table  width="100%" cellpadding="0" cellspacing="0" class="sub-order" border="1">
                 <tr>
                    <td colspan="6" style="border:none"><div style="float: left;">Scaned Bills Attached Documents,No of enclosures <strong><%=Bil_Det.size()%></strong></div>  </td>
                  </tr>
                  <tr>
                     
                    <td colspan="6" align="left" style="border:none">
                    	<%for(int i=0;i<Bil_Det.size();i++){
                    	BIL=(billimages)Bil_Det.get(i);
                    	%>
                    	<div style="margin:0 0 0 80px"><%=i+1 %>.<a href="../BillImages/<%=BIL.getextra1()%>" target="_blank">DOC<%=i+1 %></a></div>
                    	<%} %>
                      </td>
                 </tr>
                </table></div>
                 </td>
                 </tr>
                 <%} %>
                 <tr>
                    <td colspan="7">
                    <div class="member-list"><p>Stock Received by <strong><%=EMPL.getMemployeesName(GOD.getemp_id()) %></strong>&nbsp;On&nbsp;<%=CDF.format(SDF1.parse(GOD.getdate()))%>&nbsp;at&nbsp;<%=time.format(SDF1.parse(GOD.getdate()))%>
                    </p>
                    </div>
                    <div class="member-list">
                    <p>Stock Certified by <strong><%=EMPL.getMemployeesName(GOD.getCheckedby()) %></strong>&nbsp;On&nbsp;<%=CDF.format(SDF.parse(GOD.getCheckedtime()))%>&nbsp;at&nbsp;<%=time.format(SDF.parse(GOD.getCheckedtime()))%>
                     </p>
                     </div>
                     </td>
                    
                 </tr>
                  <tr>
                    <td colspan="7">
                    <div>Bill Approved by:&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    	<%
                    	 INDAP_DET=INDAPL.getIndentDetails(GOD.getextra1());
                    	if(INDAP_DET.size()>0){ 
                    		for(int i=0;i<INDAP_DET.size();i++){
                    		INDAP=(beans.indentapprovals)INDAP_DET.get(i);
                    		%>
                   		 <div class="member-list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=i+1 %>&nbsp;.&nbsp;<%=SADL.getSuperadminname(INDAP.getadmin_id()) %>&nbsp;&nbsp; On <strong><%=CDF.format(SDF.parse(INDAP.getapproved_date())) %></strong> at <strong><%=time.format(SDF.parse(INDAP.getapproved_date())) %></strong></div>
                   		<%}} %>
                   	</td>
                  </tr>
                  <br /><br />
                 <tr><td colspan="7" height="35"></td></tr>
                  <tr>
                 <td colspan="7">
                 <table width="100%" cellpadding="0" cellpadding="0">
                    <tr><td width="63%"><u>Debit</u></td>
                    <td> <u>Budget </u></td></tr>
                 
                  
                    <tr>
                    <td>
                    <span>Head of Account : </span>   
                    <span class="head-act"><%=HODL.getHeadofAccountName(EXP.gethead_account_id())%></span>
                    </td>
                  	<td> Budget Expenditure : </td></tr>
                   <tr>
                     <td>
                     <span class="floatleft">Major Head : </span>
                     <span class="head-act"><%=MJHDL.getmajorheadName(EXP.getmajor_head_id())%></span>   
                     </td>
                  	<td> Present Expenditure : </td></tr>
                  
                
                    <tr>
                    <td>
                    <span class="floatleft">Minor Head : </span>
                    <span class="head-act"><%=MINL.getminorheadName(EXP.getminorhead_id())%></span>
                    </td>
                  	<td> Previous Expenditure : </td></tr>
                    <tr>
                    <td>Sub Head : 
                    <%if(EXP_Det.size()>0){
                    	for(int i=0;i<EXP_Det.size();i++){
   					    EXP=(beans.productexpenses)EXP_Det.get(i);
   					    if(EXP_Det.size()<2)
   					    %>
   					 	<div class="member-list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=i+1 %>&nbsp;.&nbsp;<%=SUBL.getMsubheadname(EXP.getsub_head_id()) %></div>
   					 <%}}%>
                    
                    </td>
                    <td> Current Balance :  </td>
                  </tr>
                    </table>
                    </td>
                  </tr>
                  
                  <tr>
                    <td colspan="7" height="60" style="vertical-align:top;"><div style="float: left;">Being: </div>   <div style="float: left; padding:0 0 0 10px;width: auto;"><%=PAY.getnarration()%></div></td>
                  </tr>
                  <tr>
                    <td colspan="4">T.B/E.C Resulation No. __________________</td>
                    <td colspan="3">Date:______________</td>
                  </tr>
                 	<tr>
                    	<td colspan="7" >
                        <p>
                        Payment Entered By :<strong><%=EMPL.getMemployeesName(EXP.getemp_id()) %></strong>&nbsp;On&nbsp;<%=CDF.format(SDF.parse(EXP.getentry_date()))%>&nbsp;at&nbsp;<%=time.format(SDF.parse(EXP.getentry_date()))%><br />
Payment verified By <strong><%=EMPL.getMemployeesName(EXP.getemp_id()) %></strong>&nbsp;On&nbsp;<%=CDF.format(SDF.parse(EXP.getentry_date()))%> at <%=time.format(SDF.parse(EXP.getentry_date()))%>
                        </p>
                        </td>
                    	
                 	</tr>
                 	 <tr>
                    <td colspan="7">
                    <div>Payments Approved by:&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    	<%
                    	 INDAP_DET=INDAPL.getIndentDetails(EXP.getexpinv_id());
                    	if(INDAP_DET.size()>0){ 
                    		for(int i=0;i<INDAP_DET.size();i++){
                    		INDAP=(beans.indentapprovals)INDAP_DET.get(i);
                    		%>
                   		 <div class="member-list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=i+1 %>&nbsp;.&nbsp;<%=SADL.getSuperadminname(INDAP.getadmin_id()) %>&nbsp;&nbsp; On <strong><%=CDF.format(SDF.parse(INDAP.getapproved_date())) %></strong> at <strong><%=time.format(SDF.parse(INDAP.getapproved_date())) %></strong></div>
                   		<%}} %>
                   	</td>
                  </tr>
                  <%vendorsListing VENL=new vendorsListing();
                  List Ven_Det=VENL.getMvendors(PAY.getvendorId());
                  if(Ven_Det.size()>0){
                	  VEN=(beans.vendors)Ven_Det.get(0);
                  }
                  bankdetailsListing BNKL=new bankdetailsListing();
                  
                  %>
                  <tr>
                    <td colspan="7">Payment may be debited to&nbsp;:</td>
                  </tr>
                  
                  <tr>
                  <td colspan="7">
                  <table width="100%" cellpadding="0" cellspacing="0" class="sub-order">
                  <tr>
                    
                    <td width="65%">A/C Name &nbsp;:&nbsp; <%=VEN.getagencyName() %></td>
                    <td > Voucher No : <%=PAY.getreference() %></td>
                    </tr>
                  
                  <tr>
                    <td>A/C No&nbsp; : &nbsp;<%=VEN.getaccountNo() %></td>
                    <td>Date : <%=CDF.format(SDF.parse(PAY.getdate()))%></td>
                  </tr>
                  <tr>
                   <td colspan="2">IFSC Code&nbsp;:&nbsp; <%=VEN.getIFSCcode() %></td>
                  </tr>
                  <tr>
                    <td colspan="2">Branch&nbsp;:&nbsp; <%=VEN.getbankBranch() %></td>
                  </tr>
                  <tr>
                   <td colspan="2">Mode of Payment&nbsp; <%if(PAY.getpaymentType().equals("cashpaid")){ %>Cash <%}else{ %>Cheque <%} %></td>
                  </tr>
                  </table>
                  </td>
                  </tr>
                  <%
             		DecimalFormat DF=new DecimalFormat("0");
                    NumberToWordsConverter NTW=new NumberToWordsConverter();
                    String amount=DF.format(Double.parseDouble(PAY.getamount())); %>
                    <tr>
                    <td colspan="7">
                   <p>
                    The Bill No . <strong><%=GOD.getbillNo() %></strong>&nbsp; on <strong><%=CDF.format(SDF1.parse(GOD.getdate()))%></strong> was approved and passed for payment. The payment was made vide <strong><%=BNKL.getBankName(PAY.getbankId())%></strong> , Cheque No. <%=PAY.getchequeNo() %> On <strong><%=CDF.format(SDF.parse(PAY.getdate()))%></strong> 
                    for Rs <strong><%=amount %></strong> ( in words Rupees <u> <%=NTW.convert(Integer.parseInt(amount)) %></strong> only) through voucher no <strong><%=PAY.getreference() %></strong>  on <strong> <%=CDF.format(SDF.parse(PAY.getdate()))%></strong>
                   </p>
                   </td>
                    </tr>
                    

                    <tr>
                    <td colspan="2" align="center" height="50" style="vertical-align:bottom">(Accountant)</td>
                    <td colspan="2" align="center" height="50" style="vertical-align:bottom">Manager (Finance)</td>
                    <td colspan="3" align="center" height="50" style="vertical-align:bottom">Treasurer</td>
                    </tr>
                   
				</table>
			</div>
			</div>
		</div>
</div>
</div>