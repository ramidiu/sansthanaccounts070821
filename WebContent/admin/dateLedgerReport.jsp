<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.productexpensesService"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="beans.majorhead"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@page import="java.util.List" %>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
        <script type="text/javascript">
    function formsubmit(){
    	if($( "#major_head_id" ).val()!=""){    		
        	document.getElementById("departmentsearch").action="adminPannel.jsp?page=minorheadLedgerReport&majrhdid="+$( '#major_head_id' ).val();
        	document.getElementById("departmentsearch").submit();
    		    	} else if($( "#minor_head_id" ).val()!=""){
    		    		document.getElementById("departmentsearch").action="adminPannel.jsp?page=subheadLedgerReport&subhid="+$( '#minor_head_id' ).val();
    		        	document.getElementById("departmentsearch").submit();
    		    	}  else{    	
    	document.getElementById("departmentsearch").action="adminPannel.jsp?page=ledgerReport";
    	document.getElementById("departmentsearch").submit();
    	}
    }</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
function showDetails(id){
	  $("#"+id).toggle();
	
}
</script>
</head>
<%String minorhead = request.getParameter("subhid").toString(); %>
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="PRDEXP" class="beans.productexpenses"></jsp:useBean>
				<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%
if(session.getAttribute("adminId")!=null){ %>
<!-- main content -->
<%String subhead=request.getParameter("subhd").toString();
headofaccountsListing HOA_L=new headofaccountsListing();
mainClasses.subheadListing SUBH_L=new mainClasses.subheadListing();
String subhid="";
if(request.getParameter("subhid")!=null){
	subhid=request.getParameter("subhid");	
}
%>

<div>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<jsp:useBean id="MH" class="beans.majorhead"/>
<div class="vendor-page">
<div class="vendor-list">
				<div class="icons">
					<a id="print"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="../images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
<div class="clear"></div>
<div class="list-details">
<div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%= HOA_L.getHeadofAccountName(SUBH_L.getHeadOFAccountID(subhid)) %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Day Wise Ledger Report Of Sub Head</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date  <%=request.getParameter("date")%> TO <%=request.getParameter("date")%> </td></tr>
						<tr>
						<td colspan="7">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<%customerpurchasesListing SALE_L = new customerpurchasesListing();
productexpensesListing PEXP_L=new productexpensesListing();
String type=request.getParameter("typeserch");
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
DateFormat  onlyYear= new SimpleDateFormat("yyyy");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String currentDate=(dateFormat2.format(c1.getTime())).toString();
double totalAmount=0.00; 
DecimalFormat df = new DecimalFormat("0.00");
mainClasses.banktransactionsListing BKTRAL=new mainClasses.banktransactionsListing();
productsListing PRDL=new productsListing();
vendorsListing vndrs=new vendorsListing();
godwanstockListing GDWNSTL=new godwanstockListing();
mainClasses.employeesListing EMPL=new mainClasses.employeesListing();
banktransactionsListing BAKL = new banktransactionsListing();
String prevDate="";
String presentDate="";
double bankDepositTot=0.00;
double creaditSaleTot=0.00;
double bnkcreaditSaleTot=0.00;
double debitsale=0.0;
double cashSaleAmt=0.00;
String fromDate=request.getParameter("date").toString()+" 00:00:01";
String toDate=request.getParameter("date").toString()+" 23:59:59";
List SAL_DETAIL=null;
List PRDEPL=null;
List SAL_list=null;
List PRDEXP_L=null;
List PRDEXP_JV = null;
List BNK_list=null;
List BNK_DETAIL=null;
String Amount =null;
String hod=request.getParameter("hod");
if(type.equals("Product")){ 
 SAL_list=SALE_L.getcustomerpurchasesListBasedOnLedgerProduct(subhead,fromDate,toDate);
  //System.out.println(SAL_list.size());
	/* if(SAL_list.size() <= 0 ){
		SAL_list=SALE_L.getcustomerpurchasesListBasedOnsubheadInProductType(hod,subhead,minorhead,fromDate,toDate);
	 } */  /*for getting offerkind records  */
 Amount = SALE_L.getLedgerSumDollarAmtBasedOnProduct(subhead, "sub_head_id", fromDate, toDate, "othercash", hod);
 //PRDEXP_L=PEXP_L.getLedgerSumDetails(subhead,hod,"sub_head_id",fromDate,toDate,"");
 PRDEXP_L=PEXP_L.getLedgerSumDetailsNew(subhead,hod,"sub_head_id",fromDate,toDate,"");
 PRDEXP_JV=PEXP_L.getLedgerSumDetailsNewJV(subhead,hod,"sub_head_id",fromDate,toDate,"");
 } else{ 
	 SAL_list=SALE_L.getcustomerpurchasesListBasedOnLedgerSubhead22(hod,subhead,minorhead,fromDate,toDate);
	 Amount = SALE_L.getLedgerSumDollarAmt(subhead, "sub_head_id", fromDate, toDate, "othercash", hod); //written by pradeep(06/04/2016)
	 //PRDEXP_L=PEXP_L.getLedgerSumDetails(subhead,hod,"sub_head_id",fromDate,toDate,minorhead);
	 PRDEXP_L=PEXP_L.getLedgerSumDetailsNew(subhead,hod,"sub_head_id",fromDate,toDate,minorhead);
	 PRDEXP_JV=PEXP_L.getLedgerSumDetailsNewJV(subhead,hod,"sub_head_id",fromDate,toDate,minorhead);
 }

if(PRDEXP_JV.size() > 0)
{
	PRDEXP_L.addAll(PRDEXP_JV);
}

// below lines are added by srinivas on 13/7/2016
if((type.equals("Subhead")) && (subhead.equals("21083") || subhead.equals("21408") || subhead.equals("21521"))){
	SAL_list=SALE_L.getcustomerpurchasesListBasedOnLedgerSubhead22New(hod,"","",fromDate,toDate);
	 Amount = "00"; 
	 //PRDEXP_L=PEXP_L.getLedgerSumDetails(subhead,hod,"sub_head_id",fromDate,toDate,minorhead);
	 PRDEXP_L=PEXP_L.getLedgerSumDetailsNew(subhead,hod,"sub_head_id",fromDate,toDate,minorhead);
	
}
//below line written by pradeep
BNK_list = BAKL.getDepositsListBasedOnHOD("other-amount",hod,minorhead,subhead,fromDate,toDate);
// 13/7/2016
double sumOfExtra12 = 0.0;
int k = 0;


String date = request.getParameter("date");
String monthAndDate = date.substring(5, 10);//error is with this

String num1 = date.substring(0, 4);
int num2 = Integer.parseInt(date.substring(2, 4))+1;
String finYr = num1+"-"+num2;

bankbalanceListing BBAL_L=new bankbalanceListing();
minorheadListing MIN_L = new minorheadListing();

minorhead MINRHEAD = new minorhead(); 

List MINRLIST = MIN_L.getminorhead(minorhead);
if(MINRLIST.size() > 0)
{
	MINRHEAD = (minorhead)MINRLIST.get(0);
}

String majorHeadId = MINRHEAD.getmajor_head_id();


if(SAL_list.size()>0 || monthAndDate.equals("04-01") || BNK_list.size()>0){
 %>
	<tr>
							<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="15%" style="font-weight: bold;">INVOICE DATE</td>
							<td class="bg" width="8%" style="font-weight: bold;">INVOICE.NO.</td>
							<td class="bg" width="20%" style="font-weight: bold;">ACCOUNT</td>
							<td class="bg" width="10%" style="font-weight: bold;">QTY</td>
							<td class="bg" width="20%" style="font-weight: bold;" align=right>DEBIT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="right">CREDIT</td>
						</tr>
		<%
			
			if(monthAndDate.equals("04-01") && (request.getParameter("cumltv") != null && request.getParameter("cumltv").equals("cumulative")))
			{
				
				
				double subheadDebitOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hod,majorHeadId,subhid,subhead,"subhead","Assets", finYr, "debit", ""); 
				/* double subheadCreditOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hod,majorHeadId,subhid,subhead,"subhead","Assets", finYr, "credit", ""); */
				double subheadCreditOpeningBal = BBAL_L.getMajorOrMinorOrSubheadOpeningBal(hod,majorHeadId,subhid,subhead,"subhead","Liabilites", finYr, "credit", "");
				if(subheadDebitOpeningBal > 0){
				%>
					 <tr style="position: relative;">
					 	<td style="font-weight: bold;"><%=++k%></td>
					 	<td style="font-weight: bold;">&nbsp;</td>
					 	<td style="font-weight: bold;text-align: right;">DEBIT OPENING BALANCE FOR <%=finYr%></td>
					 	<td style="font-weight: bold;">&nbsp;</td>
					 	<td style="font-weight: bold;">&nbsp;</td>
					    <td style="font-weight: bold;text-align: right;"><%=subheadDebitOpeningBal%></td>
					    <td style="font-weight: bold;">&nbsp;</td>
				     </tr>
				     <%} if(subheadCreditOpeningBal > 0){ %>
				     <tr style="position: relative;">
					 	<td style="font-weight: bold;"><%=++k%></td>
					 	<td style="font-weight: bold;">&nbsp;</td>
					 	<td style="font-weight: bold;text-align: right;">CREDIT OPENING BALANCE FOR <%=finYr%></td>
					 	<td style="font-weight: bold;">&nbsp;</td>
					 	<td style="font-weight: bold;">&nbsp;</td>
						<td style="font-weight: bold;">&nbsp;</td>
					    <td style="font-weight: bold;text-align: right;"><%=subheadCreditOpeningBal%></td>
				     </tr>
				     <%} %>
				<% 
			}
			
		%>				
		<%
		if(SAL_list.size()>0){
		for(int i=0; i < SAL_list.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_list.get(i);
// 							if(SALE.getextra12().equals("10"))
// 							{
// 								sumOfExtra12 += Double.parseDouble(SALE.getextra12());
// 							}
							if(!presentDate.equals("")){
								prevDate=presentDate;	
							}
							presentDate=""+chngDateFormat.format(dbDateFormat.parse(SALE.getdate()));
							if(SALE.getcash_type().equals("cash")){
								cashSaleAmt=cashSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}else{
								creaditSaleTot=creaditSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}
							%>
    						<tr style="position: relative;">
								<%-- <td style="font-weight: bold;"><%=++k%></td> --%>
								<td style="font-weight: bold;"><%=i+1%></td>
								<td style="font-weight: bold;"><a href="javascript:void(0)" onclick="showDetails('<%=SALE.getbillingId()%>')"><%=chngDateFormat.format(dbDateFormat.parse(SALE.getdate()))%></a></td>
								<%-- <td style="font-weight: bold;"><a href="javascript:void(0)" onclick="showDetails('<%=SALE.getbillingId()%>')"><%=SALE.getbillingId()%></a></td> --%>
								<%if(SALE.getcash_type().equals("journalvoucher") && SALE.getbillingId().equals("") ) {%>
								<td style="font-weight: bold;"><a href="javascript:void(0)" onclick="showDetails('<%=SALE.getvocharNumber()%>')"><%=SALE.getvocharNumber()%></a></td>
								<%}else{ %>
								<td style="font-weight: bold;"><a href="javascript:void(0)" onclick="showDetails('<%=SALE.getbillingId()%>')"><%=SALE.getbillingId()%></a></td>
								<%} %>
								
								<td style="font-weight: bold;"><span>
								<%=SALE.getcash_type() %></span></td>
								<td style="font-weight: bold;"><%=SALE.getquantity()%></td>
								<%totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());	if(SALE.getcash_type().equals("cash")){%>
								<td style="font-weight: bold;"></td>
								<td style="font-weight: bold;" align="right">Rs.<%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td> 
								<%} else if(SALE.getcash_type().equals("cheque")){ %>	
								<td style="font-weight: bold;">Rs.<%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td> 
								<td style="font-weight: bold;"></td>
								<%}else if(SALE.getcash_type().equals("journalvoucher") || SALE.getcash_type().equals("offerKind")){ %>
								<td style="font-weight: bold;">Rs.<%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td>
								<%}else{%>
								<%if(!Amount.equals("")){ %>
								<td style="font-weight: bold;">Rs.<%=df.format(Double.parseDouble(Amount))%></td>
								<%}else{ %>
								<td style="font-weight: bold;">0.0</td>
								<%} %>	
								<td style="font-weight: bold;"></td>
								<%} %>
							</tr>
								<%SAL_DETAIL=SALE_L.getBillDetails(SALE.getbillingId());
						if(SAL_DETAIL.size()>0){ %>
						<tr>
							<td colspan="7"   >
							<div style="display: none;" id="<%=SALE.getbillingId()%>">
							<table width="95%" style="border: 1px solid #000;">
							<tr>
							<td width="5%">S.NO</td>
							<td width="8%">CODE</td>
							<td width="20%">PRODUCT NAME</td>
							<td width="8%">QUANTITY</td>
							<td width="10%">AMOUNT</td>
							<td width="10%">NARRATIONUNT</td>
							<td width="20%">TOTAL AMOUNT</td>
							<td width="20%">Entered by</td>
						</tr>
						<%for(int s=0;s<SAL_DETAIL.size();s++){
								SA=(beans.customerpurchases)SAL_DETAIL.get(s);%>
								<tr style="position: relative;">
									<td ><%=++k %></td>
									<td><%=SA.getproductId()%></td>
									<%if(!PRDL.getProductsNameByCat(SA.getproductId(), SA.getextra1()).equals("")){ %>
									<td><%=PRDL.getProductsNameByCat(SA.getproductId(), SA.getextra1())%></td>
									<%}else{ %>
									<td><%=PRDL.getProductNameByCat(SA.getproductId(), SA.getextra1())%></td>
									<%} %>
									<td><%=SA.getquantity()%></td>
									<td><%=SA.getrate() %></td>
									<td><%=SA.getextra2()%></td>
									<td><%=SA.gettotalAmmount()%></td>
									<td ><%=EMPL.getMemployeesName(SA.getemp_id()) %></td>
								</tr>
								<%} %>
						</table>
						</div>
						</td>
						</tr>
						 <%}}}%>
						 <%
						if(BNK_list.size()>0){
						for(int i=0; i < BNK_list.size(); i++ ){ 
							BNK=(banktransactions)BNK_list.get(i);
							bnkcreaditSaleTot=creaditSaleTot+Double.parseDouble(BNK.getamount());
							%>
    						<tr style="position: relative;">
								<%-- <td style="font-weight: bold;"><%=++k%></td> --%>
								<td style="font-weight: bold;"><%=i+1%></td>
								<td style="font-weight: bold;"><a href="javascript:void(0)" onclick="showDetails('<%=BNK.getbanktrn_id()%>')"><%=chngDateFormat.format(dbDateFormat.parse(BNK.getdate()))%></a></td>
								<%-- <td style="font-weight: bold;"><a href="javascript:void(0)" onclick="showDetails('<%=SALE.getbillingId()%>')"><%=SALE.getbillingId()%></a></td> --%>
								<td style="font-weight: bold;"><a href="javascript:void(0)" onclick="showDetails('<%=BNK.getbanktrn_id()%>')"><%=BNK.getbanktrn_id()%></a></td>
								<td style="font-weight: bold;"><span><%=BNK.getextra4()%></span></td>
								<td style="font-weight: bold;"></td>
								<td style="font-weight: bold;"></td>
								<td style="font-weight: bold;">Rs.<%=df.format(Double.parseDouble(BNK.getamount()))%></td>
							</tr>
								<%BNK_DETAIL=BAKL.getMbanktransactions(BNK.getbanktrn_id());
						if(BNK_DETAIL.size()>0){ %>
						<tr>
							<td colspan="8"   >
							<div style="display: none;" id="<%=BNK.getbanktrn_id()%>">
							<table width="95%" style="border: 1px solid #000;">
							<tr>
							<td width="5%">S.NO</td>
							<td width="8%">CODE</td>
							<td width="20%">PRODUCT NAME</td>
							<td width="8%">DEPOSIT TYPE</td>
							<td width="10%">CATEGORY</td>
							<td width="10%">NARRATION</td>
							<td width="20%">TOTAL AMOUNT</td>
							<td width="20%">Entered by</td>
						</tr>
						<%for(int s=0;s<BNK_DETAIL.size();s++){
							BNK=(banktransactions)BNK_DETAIL.get(s);%>
								<tr style="position: relative;">
									<%-- <td ><%=++k %></td> --%>
									<td ><%=s+1%></td>
									<td><%=BNK.getSub_head_id()%></td>
									<%if(!PRDL.getProductsNameByCat(BNK.getSub_head_id(), BNK.getHead_account_id()).equals("")){ %>
									<td><%=PRDL.getProductsNameByCat(BNK.getSub_head_id(), BNK.getHead_account_id())%></td>
									<%}else{ %>
									<td><%=PRDL.getProductNameByCat(BNK.getSub_head_id(), BNK.getHead_account_id())%></td>
									<%} %>
									<td><%if(BNK.getExtra8().equals("other-amount")){%>Other Deposit<%} %></td>
									<td><%=BNK.getextra4()%></td>
									<td><%=BNK.getnarration() %></td>
									<td><%=BNK.getamount()%></td>
									<td ><%=EMPL.getMemployeesName(BNK.getcreatedBy()) %></td>
								</tr>
								<%} %>
						</table>
						</div>
						</td>
						</tr>
						 <%}}}%>
						 
						 <%if((subhead != null) && ((subhead.equals("21083") && subhid.equals("331")) || subhead.equals("21408") || subhead.equals("21521"))){ %>
						 <tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total </td>
						<%-- <td style="font-weight: bold;text-align: right;padding-right: 40px;" >Rs.<%=df.format(creaditSaleTot) %></td> --%>  <!--commented by pradeep(06/04/2016)  -->
						<td style="font-weight: bold;text-align: right;padding-right: 40px;" >Rs.<%=df.format(sumOfExtra12) %></td>
						<td style="font-weight: bold;text-align: right;" >Rs.<%=df.format(cashSaleAmt) %> </td>
						
						</tr><%}else{ %>
						 
						 <tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total </td>
						<%-- <td style="font-weight: bold;text-align: right;padding-right: 40px;" >Rs.<%=df.format(creaditSaleTot) %></td> --%>  <!--commented by pradeep(06/04/2016)  -->
						<%if(!Amount.equals("")){ %>
						<td style="font-weight: bold;text-align: right;padding-right: 40px;" >Rs.<%=df.format(creaditSaleTot + Double.parseDouble(Amount) - sumOfExtra12) %></td>
						<%}else{ %>
						<td style="font-weight: bold;text-align: right;padding-right: 40px;" >Rs.<%=df.format(creaditSaleTot - sumOfExtra12) %></td>
						<%} %>
						<td style="font-weight: bold;text-align: right;" >Rs.<%=df.format(cashSaleAmt + bnkcreaditSaleTot) %> </td>
						
						</tr>	
						<%}} 

							if(PRDEXP_L != null && PRDEXP_L.size()>0){%>

							<tr>
							<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="18%" style="font-weight: bold;">Bill DATE</td>
							<td class="bg" width="20%" style="font-weight: bold;">AMOUNT</td>
												</tr>

<%
				for(int i=0;i<PRDEXP_L.size();i++){
		PRDEXP=(productexpenses)PRDEXP_L.get(i);%>
		    <tr style="position: relative;">
								<td style="font-weight: bold;"><%=i+1%></td>
								<%
									if(PRDEXP.getextra5().startsWith("JVN"))
									{%>
										<td style="font-weight: bold;"><a href="#" onclick="showDetails('<%=PRDEXP.getexpinv_id()%>')"><%=PRDEXP.getextra5() %></a></td>
									<%}
								else{%>
								<td style="font-weight: bold;"><a href="#" onclick="showDetails('<%=PRDEXP.getexpinv_id()%>')"><%=PRDEXP.getexpinv_id() %></a></td>
								<%} %>
								<td style="font-weight: bold;"><%=PRDEXP.getamount()%>
								</td>																														
								</tr>
										<%
										if(PRDEXP.getamount()!=null){
										debitsale=debitsale+Double.parseDouble(PRDEXP.getamount());
										
										PRDEPL=PEXP_L.getMproductexpensesBasedOnExpInvoiceIDAndSubId(PRDEXP.getexpinv_id(),subhead);
										if(PRDEPL.size()>0){ %>
						<tr>
							<td colspan="8"   >
							<div style="display: none;" id="<%=PRDEXP.getexpinv_id()%>">
							<table width="95%" style="border: 1px solid #000;">
							<tr>
							<td width="5%"></td>
							<td width="5%">CODE</td>
							<td width="20%" colspan="2">PRODUCT NAME</td>
							<td width="20%" colspan="2">Bill Number</td>
							<td width="20%" colspan="2">Vendor Name</td>
							<td width="20%" colspan="2">NARRATION</td>
							<td width="20%" colspan="2">TOTAL AMOUNT</td>
							<td width="20%">Entered by</td>
						</tr>
						<% 
						for(int s=0;s<PRDEPL.size();s++){
							PRDEXP=(productexpenses)PRDEPL.get(s);%>
								<tr style="position: relative;">
									<td ></td>
									<td><%=PRDEXP.getsub_head_id()%></td>
									<%-- <td colspan="2"><%if(!PRDL.getProductsNameByCat(PRDEXP.getextra2(), PRDEXP.gethead_account_id()).equals("")){%><%=PRDL.getProductsNameByCat(PRDEXP.getextra2(), PRDEXP.gethead_account_id())%><%} else{ %><%=SUBH_L.getMsubheadname(PRDEXP.getsub_head_id())%><%} %></td> --%>  <!-- commented by pradeep because sub_head_id and extra2 are different -->
									<td colspan="2"><%if(!PRDL.getProductsNameByCat(PRDEXP.getsub_head_id(), PRDEXP.gethead_account_id()).equals("")){%><%=PRDL.getProductsNameByCat(PRDEXP.getsub_head_id(), PRDEXP.gethead_account_id())%><%} else{ %><%=SUBH_L.getMsubheadname(PRDEXP.getsub_head_id())%><%} %></td>
									<td colspan="2"><%=GDWNSTL.getBillNumber(PRDEXP.getextra5())%></td>
									<%if(PRDEXP.getvendorId()!=null && !PRDEXP.getvendorId().equals("")){ %>
									<td colspan="2"><% vendors vdrs=(vendors)vndrs.getMvendors(PRDEXP.getvendorId()).get(0);%><%=vdrs.getfirstName()%> <%=vdrs.getlastName()%></td>
									<%}else{ %>
									<td colspan="2"></td>
									<%} %>
									<td colspan="2"><%=PRDEXP.getnarration()%></td>
									<td colspan="2"><%=PRDEXP.getamount()%></td>
									<td ><%=EMPL.getMemployeesName(PRDEXP.getemp_id()) %></td>
								</tr>
								<%} %>
						</table>
						</div>
						</td>
						</tr>
						 <%}}
		} } %>
					   </table>
			</td>
						</tr>
						<%if((subhead != null) && ((subhead.equals("21083") && subhid.equals("331")) || subhead.equals("21408") || subhead.equals("21521"))){ %>
						<tr style="border: 1px solid #000;">
						<td colspan="3" align="right" style="font-weight: bold;">Total </td>
						<%-- <td colspan="2" align="right" style="font-weight: bold;"><%=df.format(creaditSaleTot) %> </td> --%> <!--commented by pradeep(06/04/2016)  -->
						<td colspan="2" align="right" style="font-weight: bold;">Rs.<%=df.format(sumOfExtra12) %></td>
						<td style="font-weight: bold;text-align: right;" colspan="2">Rs.<%=df.format(debitsale) %></td>
											
						</tr><%}else{ %>
						
						<tr style="border: 1px solid #000;">
						<td colspan="3" align="right" style="font-weight: bold;">Total </td>
						<%-- <td colspan="2" align="right" style="font-weight: bold;"><%=df.format(creaditSaleTot) %> </td> --%> <!--commented by pradeep(06/04/2016)  -->
						<%if(!Amount.equals("")){ %>
						<td colspan="2" align="right" style="font-weight: bold;">Rs.<%=df.format(creaditSaleTot + Double.parseDouble(Amount) - sumOfExtra12) %></td>
						<%}else{ %> 
						<td colspan="2" align="right" style="font-weight: bold;">Rs.<%=df.format(creaditSaleTot - sumOfExtra12) %></td>
						 <%} %> 
						<td style="font-weight: bold;text-align: right;" colspan="2">Rs.<%=df.format(debitsale) %></td>
											
						</tr><%} %>
				
					</table>
			</div>
</div>
</div>
</div>
</div>
<!-- main content -->
<%}
		else{
	response.sendRedirect("index.jsp");
} %>
