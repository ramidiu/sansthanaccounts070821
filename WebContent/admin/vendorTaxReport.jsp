<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.payments"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TDS & Service Tax Report</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
var openMyModal = function(source)
{
	modalWindow.windowId = "myModal";
	modalWindow.width = 800;
	modalWindow.height = 500;
	modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
	modalWindow.open();

};	
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});

$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<jsp:useBean id="PAY" class="beans.payments"></jsp:useBean>
</head>
<body>
<%
vendorsListing VNDRL=new vendorsListing();
List PaymntList=null;
double totalamount=0.0;
double tdsamount=0.0;
double servicertaxamnt=0.0;
double vendoramount=0.0;
paymentsListing PAYMNTL=new paymentsListing();
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat  onlyYear= new SimpleDateFormat("yyyy");
String currentDate=(dateFormat2.format(c1.getTime())).toString();
String fromDate=currentDate+" 00:00:01";
String toDate=currentDate+" 23:59:59";
if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
	c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
	fromDate=currentDate+" 00:00:00";
	c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
	toDate=(dateFormat2.format(c1.getTime())).toString();
	toDate=toDate+" 23:59:59";
}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
	c1.getActualMaximum(Calendar.DAY_OF_MONTH);
	int lastday = c1.getActualMaximum(Calendar.DATE);
	c1.set(Calendar.DATE, lastday);  
	toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
	c1.getActualMinimum(Calendar.DAY_OF_MONTH);
	int firstday = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday); 
	fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
	int lastday_y = c1.get(Calendar.YEAR);
	c1.set(Calendar.DATE, lastday_y);  
	c1.getActualMinimum(Calendar.DAY_OF_YEAR);
	int firstday_y = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday_y); 
	Calendar cld = Calendar.getInstance();
	cld.set(Calendar.DAY_OF_YEAR,1); 
	fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
	cld.add(Calendar.YEAR,+1); 
	toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate")+" 00:00:01";
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate")+" 23:59:59";
	}
}
 PaymntList=PAYMNTL.getPaymentsByDate(fromDate,toDate);
%>
	<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
				</div>
<div class="sj">
					 <form action="adminPannel.jsp" method="post">  
						<input type="hidden" name="page" value="vendorTaxReport"/>
						 <%-- <select name="headID">
						<option value="4" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("4")){ %>selected="selected" <%} %>>SANSTHAN</option>
						<option value="1" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("1")){ %>selected="selected" <%} %>>CHARITY</option>
						<option value="of" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("of")){ %>selected="selected" <%} %>>OFFER KINDS</option>
						</select> --%>
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
		</div>
				<form name="searchForm" id="searchForm" action="adminPannel.jsp" method="post">
				<div class="search-list">
					<ul>
						<li>
						<input type="hidden" name="page" value="vendorTaxReport"/>
						<input type="hidden" class="click" name="search" value="Search"></input>
						</li>
					
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=" <%=dateFormat2.format(dbDateFormat.parse(fromDate)) %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value=" <%=dateFormat2.format(dbDateFormat.parse(toDate)) %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
								<div class="clear"></div>
				<div class="list-details">
 <div class="printable">
					<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="8" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST </td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;">TDS Report</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
                        <tbody class="yourID">
						<tr>
							<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="8%" style="font-weight: bold;">DATE</td>
							<td class="bg" width="8%" style="font-weight: bold;">Unique No</td>
							<td class="bg" width="8%" style="font-weight: bold;">Payment Id</td>
							<td class="bg" width="25%" style="font-weight: bold;">Vendor</td>
							<td class="bg" width="10%" style="font-weight: bold;">Total Amount</td>
							<td class="bg" width="10%" style="font-weight: bold;">Paid To Vendor</td>
							<td class="bg" width="10%" style="font-weight: bold;">TDS</td>
<!-- 							<td class="bg" width="10%" style="font-weight: bold;">Service Tax</td> -->
									</tr>
						</tbody>
						<%
						if(PaymntList.size()>0){
							for(int i=0;i<PaymntList.size();i++){
								PAY=(payments)PaymntList.get(i);
								vendoramount=Double.parseDouble(PAY.getamount())-Double.parseDouble(PAY.getextra7())-Double.parseDouble(PAY.getService_tax_amount());
						%>
						<tr>
							<td><%=i+1 %></td>
							<td><%=PAY.getdate() %></td>
							<td><div style="font-weight: bold;color: red;cursor: pointer;" onclick="openMyModal('Trackingno_Details.jsp?TId=<%=PAY.getextra6() %>')"><span><%=PAY.getextra6()  %></span></div></td>
							<td><%=PAY.getpaymentId() %></td>
							<td><%=VNDRL.getMvendorsAgenciesName(PAY.getvendorId()) %></td>
							<td><%=PAY.getamount() %></td>
							<td><%=vendoramount %></td>
							<td><%=PAY.getextra7()%></td>
<%-- 							<td><%=PAY.getService_tax_amount() %></td> --%>
						</tr>
						<%
// 						servicertaxamnt=servicertaxamnt+Double.parseDouble(PAY.getService_tax_amount());
						tdsamount=tdsamount+Double.parseDouble(PAY.getextra7());
							}%>
							<tr>
							<td class="bg" colspan="7">Total Charges</td>
						
							<td class="bg"> &#8377; <%=tdsamount%> /-</td>
<%-- 							<td class="bg">  &#8377; <%=servicertaxamnt %> /-</td> --%>
							</tr>
							<%} else { %>
							<tr>
							<td colspan="9" class="bg" style="font-weight: bold;color: red;text-align: center;">Sorry,No TDS & Service Tax Entry.</td>
										</tr>	
							<%} %>
							
						</table></div>
						</div></div></div>
</body>
</html>