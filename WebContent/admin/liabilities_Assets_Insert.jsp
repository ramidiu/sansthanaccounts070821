<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.DateFormat"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="BNKBALS" class="beans.bankbalanceService">
<%
DateFormat  dateFormat5= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
TimeZone tz = TimeZone.getTimeZone("IST");
dateFormat5.setTimeZone(tz);
Calendar c5 = Calendar.getInstance(); 
String date2 = (dateFormat5.format(c5.getTime())).toString();

if(session.getAttribute("adminId")!=null){
String vendor = "";
String majorHead = "";
String minorHead = "";
String product = "";
String hoaid = request.getParameter("head_account_id");;
String type=request.getParameter("type");
String category=request.getParameter("category");
String cdtype =  request.getParameter("cdtype");
// if(type.equals("Assets") || type.equals("outstandingLiabilites"))
// {
	String[] majorHeadArray=request.getParameter("major_head_id").split(" ");
	majorHead = majorHeadArray[0];
	String[] minorHeadArray=request.getParameter("minor_head_id").split(" ");
	minorHead = minorHeadArray[0];
	String[] productArray=request.getParameter("product").split(" ");
	product = productArray[0];
	
	if(type.equals("Liabilites"))
	{
		String[] vendorArray=request.getParameter("vendorname").split(" ");
		vendor = vendorArray[0];
	}
// }
// else if(type.equals("Liabilites"))
// {
// 	String[] vendorArray=request.getParameter("vendorname").split(" ");
// 	vendor = vendorArray[0];
// }

String financialYear = request.getParameter("financialYear");
String amount=request.getParameter("amount");
String createdBy=session.getAttribute("adminId").toString();

String createdDate = financialYear.split("-")[0]+"-04-01 11:10:00";
%>

<jsp:setProperty name="BNKBALS" property="bank_id" value=""/>
<%
	if(type != null && type.equals("outstandingLiabilites")){
		type = "Liabilites";
	}
%>
<jsp:setProperty name="BNKBALS" property="type" value="<%=type%>" />
<jsp:setProperty name="BNKBALS" property="bank_bal" value="<%=amount%>"/>
<jsp:setProperty name="BNKBALS" property="bank_flag" value="<%=category%>"/>
<jsp:setProperty name="BNKBALS" property="createdDate" value="<%=createdDate%>"/>
<jsp:setProperty name="BNKBALS" property="createdBy" value="<%=createdBy%>"/>
<jsp:setProperty name="BNKBALS" property="extra1" value="<%=financialYear%>"/>
<jsp:setProperty name="BNKBALS" property="extra2" value="<%=hoaid%>"/>
<jsp:setProperty name="BNKBALS" property="extra3" value="<%=majorHead%>"/>
<jsp:setProperty name="BNKBALS" property="extra4" value="<%=minorHead%>"/>
<jsp:setProperty name="BNKBALS" property="extra5" value="<%=product%>"/>
<jsp:setProperty name="BNKBALS" property="extra6" value="<%=vendor%>"/>
<jsp:setProperty name="BNKBALS" property="extra7" value="<%=cdtype%>"/>

<%BNKBALS.insert();%><%=BNKBALS.geterror()%>
<%response.sendRedirect("adminPannel.jsp?page=LiabilitiesAssetsOpeningBal"); %>  
<%
}else{
	response.sendRedirect("index.jsp");
}
%>
</jsp:useBean>