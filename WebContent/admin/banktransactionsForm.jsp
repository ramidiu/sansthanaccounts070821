<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Head Of Account</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

function validate(){ 

	$('#NarrationError').hide();
	$('#AmountError').hide();
	if($('#bank_id').val().trim()==""){
		$('#bankError').show();
		$('#bank_id').focus();
		return false;
	}
	if($('#amount').val().trim()==""){
		$('#AmountError').show();
		$('#amount').focus();
		return false;
	}
	if($('#Narration').val().trim()==""){
		$('#NarrationError').show();
		$('#Narration').focus();
		return false;
	}
	
	
}
</script>

<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});		
});

</script>

<script>
function majorheadsearch(){
	var data1=$('#major_head_id').val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMajor.jsp',{q:data1,HAid :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#major_head_id').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 
			});
}
function minorheadsearch(){
	  var data1=$('#minor_head_id').val();
	  var major=$('#major_head_id').val();
	  var hoid=$("#head_account_id").val();
	  $.post('searchMinior.jsp',{q:data1,q1:major,HID :hoid},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		$('#minor_head_id').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			}
		}); 		
});
}

function productsearch(){
var data1=$('#product').val();
var arr = [];
//var data1=$("#sub_head_id").val();
var minor=$('#minor_head_id').val();
var major=$('#major_head_id').val();
var hoid=$("#head_account_id").val();
$.post('searchSubhead.jsp',{q:data1,q1:major,q2:minor,HID :hoid},function(data){
	var response = data.trim().split("\n");
	 var doctorNames=new Array();
	 var amount=new Array();
	 var amount1=new Array();
	 var vat=new Array();
	 for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		 amount.push(d[2]);
		 vat.push(d[3]);
		 doctorNames.push(d[0] +" "+ d[1]);
		 arr.push({
			 label: d[0]+" "+d[1],
		        amount:d[2],
		        vat:d[3],
		        sortable: true,
		        resizeable: true
		    });
	 }
	var availableTags=data.trim().split("\n");
	var availableIds=data.trim().split("\n");
	availableTags=arr;
	//alert(arr.length);
	$('#product').autocomplete({
		source: availableTags,
		focus: function(event, ui) {
			// prevent autocomplete from updating the textbox
			event.preventDefault();
			// manually update the textbox
			$(this).val(ui.item.label);
		}
	}); 
		});
} 
</script>
</head>
<jsp:useBean id="EMP" class="beans.employees"></jsp:useBean>
<jsp:useBean id="BNKT" class="beans.banktransactions"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<body>
<div class="vender-details">
<%DateFormat DbDate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat usrdate=new SimpleDateFormat("yyyy-MM-dd");
employeesListing EMPL=new employeesListing();
banktransactionsListing BNKT_L=new banktransactionsListing();
mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
majorheadListing MJL = new majorheadListing();
minorheadListing MIL = new minorheadListing();
subheadListing SUBL = new subheadListing();
if(request.getParameter("txid")!=null) {
	List bankreport=BNKT_L.getMbanktransactions(request.getParameter("txid"));
	BNKT=(banktransactions)bankreport.get(0);
	%>
<form method="POST" action="banktransactionsupdate.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Bank Deposits/Withdrawal Edit</span></td>
</tr>
<tr>
<td width="50%">
<table width="100%" cellpadding="0" cellspacing="0">

<input type="hidden" name="banktx_id" id="banktx_id" value="<%=request.getParameter("txid") %>" />
<input type="hidden" name="bank_id" id="bank_id" value="<%=request.getParameter("id") %>" />
<input type="hidden" name="hhmmss" value="<%=BNKT.getdate().substring(10,19)%>">
<tr>
<td>Deposit Date</td>
<td>Category<span style="color: red;">*</span></td>
<td>Amount deposit By</td>
</tr>
<tr>
<%-- <td><input type="text" name="date" id="date" value="<%=usrdate.format(DbDate.parse(BNKT.getdate()))%>"/></td> --%>
<td><input type="text" name="date" id="date"value="<%=usrdate.format(DbDate.parse(BNKT.getdate().trim()))%>"/></td>
<td>
<input type="hidden" name="prevcategory" id="prevcategory" value="<%=BNKT.gettype() %>"  />
<select name="category" style="width:184px;padding:5px;">
	<option value="<%=BNKT.gettype()%>" selected="selected"><%=BNKT.gettype() %></option>
</select></td>


<td><select name="amtDeposited" readonly="readonly" style="width:184px;padding:5px;">
	<option value="<%=BNKT.getextra4()%>" selected="selected"><%=BNKT.getextra4() %></option>
	<option value="pro-counter">PRO Office-counter</option>
	<option value="poojastore-counter">PoojaStore-counter</option>
	<option value="Charity-cash">Charity-Cash</option>
	<option value="Development-cash">Development-cash</option>
	<option value="Sainivas">Sainivas(Shirdi)</option>
</select></td>
</tr>
<tr>
<td>Type Of Deposit</td>
<td><div class="warning" id="AmountError" style="display: none;">Please Enter Amount.</div>Amount<span style="color: red;">*</span></td>
<td>Amount given by</td>
</tr>
<tr>
<td>
<select name="depositType" style="width:184px;padding:5px;">
	 
	<option value="">Normal Deposit</option>
	<option value="dollar-amount"  <%if(BNKT.getExtra8().equals("dollar-amount")) {%> selected="selected" <%} %>> Dollar Deposit </option>
	<option value="other-amount" <%if(BNKT.getExtra8().equals("other-amount")) {%> selected="selected" <%} %>> Other Deposit </option>
	<option value="cash-amount" <%if(BNKT.getExtra8().equals("cash-amount")) {%> selected="selected" <%} %>> Cash Deposit </option>
	<option value="card-amount" <%if(BNKT.getExtra8().equals("card-amount")) {%> selected="selected" <%} %>> Card Deposit </option>
	<option value="cheque-amount" <%if(BNKT.getExtra8().equals("cheque-amount")) {%> selected="selected" <%} %>> Cheque Deposit </option>
	<option value="online-amount" <%if(BNKT.getExtra8().equals("online-amount")) {%> selected="selected" <%} %>> Online Deposit </option>
	
	
</select>
</td>
<td><input type="hidden" name="prevamount" id="prevamount" value="<%=BNKT.getamount() %>"  />
<input type="text" name="amount" id="amount" value="<%=BNKT.getamount() %>" onKeyPress="return numbersonly(this, event,true);" /></td>
<td>
<select name="employeeId" id="employeeId" style="width:184px;padding:5px;">
<option value="<%=BNKT.getextra2()%>" selected="selected"><%=EMPL.getMemployeesName(BNKT.getextra2()) %></option>
	<%
	List EMPS=EMPL.getemployees();
	if(EMPS.size()>0){
	for(int i=0;i<EMPS.size();i++){
		EMP=(beans.employees)EMPS.get(i);
	%>
	<option value="<%=EMP.getemp_id()%>"><%=EMP.getfirstname()%></option>
	<%}} %>
</select></td>

<td></td>
</tr>
<%-- <%if(BNKT.getHead_account_id()!=null && !BNKT.getHead_account_id().equals("")) {%> --%>
<tr>
<td>Head Of Dept</td>
<td>Major Head</td>
<td>Minor Head</td>
</tr>
<tr>
<td>
<select name="head_account_id" id="head_account_id" style="width:184px;padding:5px;">
<%List HA_Lists=HOA_L.getheadofaccounts();
if(HA_Lists.size()>0){%>
	<option value="" selected>-Select HOA--</option>
	<%for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i);%>
	<option value="<%=HOA.gethead_account_id()%>" <%if(BNKT.getHead_account_id()!=null && BNKT.getHead_account_id().equals(HOA.gethead_account_id())){ %> selected="selected" <%}%> ><%=HOA.getname() %></option>
<%}}%>
</select>
</td>
<td><input type="text" name="major_head_id" id="major_head_id" onkeyup="majorheadsearch()" class="" value="<%=BNKT.getMajor_head_id()%> <%=MJL.getmajorheadName(BNKT.getMajor_head_id())%>" placeholder="find a major head here" autocomplete="off"/></td>
<td><input type="text" name="minor_head_id" id="minor_head_id" onkeyup="minorheadsearch()" class="" value="<%=BNKT.getMinor_head_id() %> <%=MIL.getminorheadName(BNKT.getMinor_head_id()) %>"  placeholder="find a minor head here" autocomplete="off"/></td>

</tr>
<%-- <%} %> --%>
<tr>
<td>Sub Head</td>
<td>Reference</td>
</tr>
<tr>
<td><input type="text" name="product" id="product"  onkeyup="productsearch()" class="" value="<%=BNKT.getSub_head_id() %> <%=SUBL.getMsubheadname(BNKT.getSub_head_id()) %>" autocomplete="off" /></td>
<td><input type="text" name="extra1" id="extra1" value="<%=BNKT.getextra1() %>" /></td>
</tr>
<tr>
<td colspan="2"><div class="warning" id="NarrationError" style="display: none;">Please select Narration.</div>Narration*</td>
<td></td>
</tr>
<tr>
<td colspan="2"><textarea  name="Narration" id="Narration"><%=BNKT.getnarration() %></textarea></td>
<td style="text-align:center;"><input type="submit" value="Update" class="click" style="border:none;"/> </td>
</tr>
<!-- <tr>
	<td colspan="3" style="text-align:center;"><input type="submit" value="Update" class="click" style="border:none;margin-top:13px;"/></td>
</tr> -->
</table>
</td>
<td width="1%"></td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

</table>
</form>
<%} else{ %>

<form method="POST" action="banktransactionsInsert.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Bank Deposits/Withdrawal</span></td>
</tr>
<tr>
<td width="50%">
<table width="100%" cellpadding="0" cellspacing="0">

<input type="hidden" name="bank_id" id="bank_id" value="<%=request.getParameter("id") %>" />
<tr>
<td>Deposit Date</td>
<td>Category<span style="color: red;">*</span></td>
<td>Amount deposit By</td>
</tr>
<tr>
<td><input type="text" name="date" id="date" value=""/></td>
<td><select name="category" style="width:184px;padding:5px;">
	<option value="pettycash">Petty cash(with draw)</option>
	<option value="deposit">Deposit</option>
</select></td>

<td><select name="amtDeposited" style="width:184px;padding:5px;">
	<option value="pro-counter">PRO Office-counter</option>
	<option value="poojastore-counter">PoojaStore-counter</option>
	<option value="Charity-cash">Charity-Cash</option>
	<option value="Development-cash">Development-cash</option>
</select></td>
</tr>
<tr>
<td>Type Of Deposit</td>
<td><div class="warning" id="AmountError" style="display: none;">Please Enter Amount.</div>Amount<span style="color: red;">*</span></td>
<td>Amount given by</td>

</tr>

<tr>
<td>
<select name="depositType" style="width:184px;padding:5px;">
	<option value="" selected="selected">Normal Deposit</option>
	<option value="OTHDeposit" >Other Deposit</option>
	<option value="DLRDeposit">Dollar Deposit</option>
</select>
</td>
<td><input type="text" name="amount" id="amount" value="" onKeyPress="return numbersonly(this, event,true);" /></td>
<td>
<select name="employeeId" id="employeeId" style="width:184px;padding:5px;">
	<option value="">Select employee</option>
	<%
	List EMPS=EMPL.getemployees();
	if(EMPS.size()>0){
	for(int i=0;i<EMPS.size();i++){
		EMP=(beans.employees)EMPS.get(i);
	%>
	<option value="<%=EMP.getemp_id()%>"><%=EMP.getfirstname()%></option>
	<%}} %>
</select></td>

<td></td>
</tr>
<tr>
<td>Reference</td>
</tr>
<tr>
<td><input type="text" name="extra1" id="extra1" value="" /></td>
</tr>
<tr>
<td colspan="2"><div class="warning" id="NarrationError" style="display: none;">Please select Narration.</div>Narration*</td>
<td></td>
</tr>
<tr>
<td colspan="3"><textarea  name="Narration" id="Narration"></textarea></td>
<td ><!-- <input type="submit" value="Submit" class="click" style="border:none;"/> --></td>
</tr>
<tr>
	<td colspan="3" style="text-align:center;"><input type="submit" value="Submit" class="click" style="border:none;margin-top:13px;"/></td>
</tr>
</table>
</td>
<td width="1%"></td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

</table>
</form>
<%} %>
</div>
</body>
</html>