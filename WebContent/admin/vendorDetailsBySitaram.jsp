<%@page import="beans.payments"%>
<%@page import="java.util.List"%>
<%@page import="beans.vendors"%>
<%@page import="mainClasses.VendorsAndPayments"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
</head>
<body>

<%


	String finYear = request.getParameter("finYear");
	String vendorId = request.getParameter("vendorId");
	String hoaid = request.getParameter("hoaid");

	if(hoaid == null || hoaid.trim().equals("") || hoaid.equals("all")){
		hoaid = "";
	}



%>






<form action="adminPannel.jsp">
		<table style="margin-left:40px;">
		<tbody>
		<tr>
			<td colspan="2">Select Financial Year: </td>
		 	<td>
				<select name="finYear" id="finYear" style="width:150px;margin-left:20px;margin-right:20px;margin-top:10px;">
					<jsp:include page="yearDropDown1.jsp"></jsp:include>
				</select>
				<input type="hidden" name="page" value="vendorDetailsBySitaram">
				<input type="hidden" name="vendorId" value="<%=vendorId %>">
			</td>
			<td colspan="2">HEAD OF ACCOUNT:</td>
			<td>
				<select name="hoaid" id="hoaid" style="width:150px;margin-left:20px;margin-right:20px;margin-top:10px;">
					<option value="all">ALL</option>
					<option value="1">CHARITY</option>
					<option value="3">POOJA STORES</option>
					<option value="4">SANSTHAN</option>
					<option value="5">SAI NIVAS</option>
				</select>
			</td>
			
			<td><input type="submit" value="Submit" class="click" id="finyear"></td>
		</tr>
	
	</tbody></table>
	</form>
	
		<div class="vendor-page">
<div class="vendor-box">
	
	<%
	
			if(vendorId != null && !vendorId.trim().equals("")){
				
				VendorsAndPayments vendorsAndPayments = new VendorsAndPayments();
				
				vendors vendors = vendorsAndPayments.getVendorByVendorId(vendorId);
				
				%>
				
				<div class="name-title"><%=vendors.gettitle() %> <%=vendors.getfirstName() %>  </div>
				<div class="click floatleft"><a href="./editVendor.jsp?id=<%=vendorId %>" target="_blank" onclick="openMyModal('editVendor.jsp?id=<%=vendorId %>'); return false;">Edit</a></div>
				<div style="clear:both;"></div>
				
								
				<div class="details-list">
					<table cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td colspan="2"><span>Agency Name:</span><%=vendors.getagencyName() %></td>
							</tr>
							
							<tr>
							<td width="40%"><span>Email:</span><%=vendors.getemail() %></td>
							<td><span>Address:</span><%=vendors.getaddress() %></td>
							</tr>
							<tr>
							<td><span>Phone:</span><%=vendors.getphone() %></td>
							<td><span>City:</span><%=vendors.getcity() %></td>
							</tr>
							<tr>
							<td><span>Gothram:</span><%=vendors.getgothram() %></td>
							<td><span>Nominee:</span><%=vendors.getnomineeName()%></td>
							</tr>
							
							<tr>
							<td colspan="2"><span>Other Details:</span><%=vendors.getdescription()%></td>
							</tr>
							
							<tr>
							<td><span>Bank Name:</span><%=vendors.getbankName()%></td>
							<td><span>Bank Branch:</span><%=vendors.getbankBranch()%></td>
							</tr>
							
							<tr>
							<td><span>IFSC code:</span><%=vendors.getIFSCcode()%></td>
							<td><span>Account Number:</span><%=vendors.getaccountNo()%></td>
							</tr>
							<tr>
							<td colspan="2"><span>Credit Days:</span><%=vendors.getcreditDays()%></td>
							</tr>
						</tbody>
					</table>
				
				</div>
				
				<div class="details-amount">
					 <ul>
					<li class="colr">Rs.138,400.00 <br>
					 <span>THIS YEAR BILL AMOUNT</span></li>
					 
					 <li style="color: green;">Rs.163,311.00<br>
					 <span>THIS YEAR PAID AMOUNT</span></li>
					  <li class="colr1">Rs.00<br> 
					 <span>THIS YEAR PENDING BALANCE</span></li>
					</ul>
				</div>
				
				<div class="vendor-list">
				
				<div class="clear"></div>
				
				<div class="icons">
				<span><img src="../images/back.png" height="50" width="55" style="margin: 0 20px 0 0" title="back" onclick="goBack()"></span> 
				<span><a id="print" href="javascript:void( 0 )"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
				<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details" style="margin:10px 10px 0 0">
				<div class="printable" id="tblExport">
				<table width="100%" cellpadding="0" cellspacing="0" border="1">
				<tbody><tr>
				</tr><tr><td colspan="10" height="10"></td> </tr>
					<tr><td colspan="10" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
										<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
										<tr><td colspan="10" align="center" style="font-weight: bold;font-size: 14px;"><%=vendors.getagencyName() %> PAYMENT TRANSACTIONS (<%=finYear %>)</td></tr>
				<tr>
				<td class="bg" width="7%">Invoice No.</td>
				<td class="bg" width="7%">Bill No.</td>
				<td class="bg" width="10%">PAYMENT DATE</td>
				<td class="bg" width="20%">Vendor</td>
				<td class="bg" width="20%">SUBHEAD</td>
				<td class="bg" width="8%">TOTAL INVOICE AMOUNT</td>
				<td class="bg" width="8%">PAID AMOUNT</td>
				<td class="bg" width="15%">PAYMENT MODE</td>
				<td class="bg" width="10%">CHEQUE NO</td>
				<td class="bg" width="10%">VOUCHER NO</td>
				</tr>
				
				<%
				
					String openingFromDate = "2015-03-31 00:00:00";
					String openingToDate = finYear.split("-")[0]+"-03-31 23:59:59";
					
					String fromDate = finYear.split("-")[0]+"-04-01 00:00:00";
					String toDate = "20"+finYear.split("-")[1]+"-03-31 23:59:59";
				
				List<String[]> openingList = vendorsAndPayments.getVendorOpeningAndPendingAmount(vendorId, hoaid, finYear, openingFromDate, openingToDate, "sum");
				
				
				%>
				<tr>
					<td colspan="5" align="right">OPENING PENDING BALANCE</td>
					<td align="right" colspan="2">
						<%
							if(openingList != null && openingList.size() > 0){
								%>
								<%=openingList.get(0)[5] %>
								<%
							}
						%>
					</td>
					<td></td>
				</tr>
				
				<%
					
					List<payments> currentsYearPayments = vendorsAndPayments.getVendorPaymentsByVendorID(vendorId, hoaid, fromDate, toDate);
					
				
					if(currentsYearPayments != null && currentsYearPayments.size() > 0){
						
						for(int i = 0 ; i < currentsYearPayments.size(); i++){
							payments payments = currentsYearPayments.get(i);
							
							%>
								<tr>
									<td><%=payments.getextra3() %></td>
									<td></td>
									<td><%=payments.getdate() %></td>
									<td><%=vendors.getfirstName() %></td>
									<td></td>
									<td align="right"></td> 
									<td align="right"><%=payments.getamount() %></td>
									<td align="center"><%=payments.getpaymentType() %></td>
									<td><%=payments.getchequeNo() %></td>
									<td><%=payments.getreference() %></td>
									</tr>
							
							<%
						}
						
						
					}
					
				%>
				
						
				<tr>
					<td colspan="5" align="right" class="bg">TOTAL AMOUNT</td>
					<td align="right" class="bg"></td>
					<td colspan="1" align="right" class="bg" style="font-weight: bold;"> 163,311.40</td>
					<td colspan="4" class="bg"></td>
				</tr>
				
				
				
				
				</tbody></table> 
				
				<!-- This pending transactions code is written by pradeep on 17/06/2016 starts -->
				<br>
				
				
				<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" border="1">
				<tbody><tr>
				
				</tr><tr><td colspan="10" align="center" style="font-weight: bold;font-size: 14px;">KREATIVE WEB SOLUTIONS PENDING PAYMENT TRANSACTIONS (2018-2019) </td></tr>
				
				<tr>
				<!-- <td class="bg" width="7%">S.No</td> -->
				<td class="bg" width="7%">Invoice No.</td>
				<td class="bg" width="7%">Bill No.</td>
				<td class="bg" width="10%">BILL APPROVED DATE BY A/C MANAGER</td>
				<td class="bg" width="20%">Vendor</td>
				<td class="bg" width="20%">SUBHEAD</td>
				
				<td class="bg" width="15%">TOTAL INVOICE AMOUNT</td>
				<td class="bg" width="10%">BILL AMOUNT</td>
				<td class="bg" width="35%">PAYMENT STATUS</td>
				
				</tr>
				<tr>
					<td colspan="5" align="right">OPENING PENDING BALANCE</td>
					<td align="right" colspan="2">1470.0 </td>
					<td></td>
				</tr>
				
				<tr><td colspan="10" align="center" style="font-size: 20px;color: red;"> There is no Pending transactions found for this vendor!..</td></tr>
				
				</tbody></table>
				
				<!-- This pending transactions code is written by pradeep on 17/06/2016 end -->
				
				</div>
				</div>
				</div>
				
				
				<%
				
			}
	%>
	
	



		



</div>

</div>
</body>
</html>