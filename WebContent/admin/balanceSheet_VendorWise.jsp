<%@page import="mainClasses.VendorsAndPayments"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="beans.productexpenses"%>
<%@page import="mainClasses.bankbalanceListing"%>
<%@page import="mainClasses.bankdetailsListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="beans.payments"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="beans.godwanstock"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VENDOR BALANCE SHEET REPORT | SHRI SHIRIDI SAI BABA SANSTHAN
	TRUST</title>

<style>
.table-head td {
	font-size: 16px;
	line-height: 28px;
	text-align: center;
	font-weight: bold;
}

.new-table td {
	padding: 2px 0 2px 5px !important;
}
</style>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript"
	src="../js/modal-window.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
	$(function() {
		$("#fromDate").datepicker({

			changeMonth : true,
			changeYear : true,
			numberOfMonths : 1,
			showOn : 'both',
			buttonImage : "../images/calendar-icon.png",
			buttonText : 'Select Date',
			buttonImageOnly : true,
			dateFormat : 'yy-mm-dd',
			onSelect : function(selectedDate) {
				$("#toDate").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#toDate").datepicker({

			changeMonth : true,
			changeYear : true,
			numberOfMonths : 1,
			showOn : 'both',
			buttonImage : "../images/calendar-icon.png",
			buttonText : 'Select Date',
			buttonImageOnly : true,
			dateFormat : 'yy-mm-dd',
			onSelect : function(selectedDate) {
				$("#fromDate").datepicker("option", "maxDate", selectedDate);
			}
		});
	});
	function showDetails(billId) {
		$("#" + billId).slideToggle();
	}
	function showDetails(id){
		  $("#"+id).toggle();
		
	}
</script>
<script type="text/javascript">
	// When the document is ready, initialize the link so
	// that when it is clicked, the printable area of the
	// page will print.
	$(function() {
		// Hook up the print link.
		$("#print").attr("href", "javascript:void( 0 )").click(function() {
			// Print the DIV.
			$("a").css("text-decoration", "none");
			$("#tblExport").print();

			// Cancel click event.
			return (false);
		});
	});
	$(document).ready(function() {
		$("#btnExport").click(function() {
			$("#tblExport").btechco_excelexport({
				containerid : "tblExport",
				datatype : $datatype.Table
			});
		});
	});
</script>

<script>
	function datepickerchange() {
		var finYear = $("#finYear").val();
		var yr = finYear.split('-');
		var startDate = yr[0] + ",04,01";
		var endDate = parseInt(yr[0]) + 1 + ",03,31";

		$('#fromDate').datepicker('option', 'minDate', new Date(startDate));
		$('#fromDate').datepicker('option', 'maxDate', new Date(endDate));
		$('#toDate').datepicker('option', 'minDate', new Date(startDate));
		$('#toDate').datepicker('option', 'maxDate', new Date(endDate));
	}

	$(document).ready(function() {
		datepickerchange();
	});
</script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script>
	$(document).ready(
			function() {
				var top = $('.yourID').offset().top
						- parseFloat($('.yourID').css('marginTop').replace(
								/auto/, 100));
				$(window).scroll(function(event) {
					// what the y position of the scroll is
					var y = $(this).scrollTop();

					// whether that's below the form
					if (y >= top) {
						// if so, ad the fixed class
						$('.yourID').addClass('fixed');

					} else {
						// otherwise remove it
						$('.yourID').removeClass('fixed');

					}
				});
			});
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<%
if(session.getAttribute("adminId") != null) {
	String vId = "";
	String[] date = new String[2];
	String fromDate = "";
	String toDate = "";
	String type = "";
	String hoa = "";
	String finYr = "";
	if(request.getParameter("vendor") != null && !request.getParameter("vendor").equals("")){
		vId = request.getParameter("vendor");
	}
	if(request.getParameter("date") != null && !request.getParameter("date").equals("")){
		date = request.getParameter("date").split("to");
		fromDate = date[0];
		toDate = date[1];
	}
	if(request.getParameter("type") != null && !request.getParameter("type").equals("")){
		type = request.getParameter("type");
	}
	if(request.getParameter("hoa") != null && !request.getParameter("hoa").equals("")){
		hoa = request.getParameter("hoa");
		//System.out.println("head_account_id : head_account_id : head_account_id : head_account_id :" + hoa);
	}
	if(request.getParameter("fy") != null && !request.getParameter("fy").equals("")){
		finYr = request.getParameter("fy");
	}
	vendorsListing VNDR = new vendorsListing();
	String vName = VNDR.getMvendorsAgenciesName(vId);
	godwanstockListing gsl = new godwanstockListing();
	paymentsListing pl = new paymentsListing();
	productsListing pdl = new productsListing();
	bankdetailsListing bdl = new bankdetailsListing();
	vendorsListing vl = new vendorsListing();
	bankbalanceListing bbl = new bankbalanceListing();
	productexpensesListing pel = new productexpensesListing();
	%>
<div>
	<div class="vendor-page">
		<div class="icons">
			<span><a id="print"><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
			<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel"/></a></span><span></span>
		</div>
		<div class="clear"></div>
		<div class="list-details">
			<div class="total-report">
				<div class="printable" id="tblExport">
					<table width="90%" cellpadding="0" cellspacing="0"
						style="margin-bottom: 10px; margin-top: 10px; margin: auto;">
						<tbody>
							<tr>
								<td colspan="6" align="center" style="font-weight: bold;">
									SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
							</tr>
							<tr>
								<td colspan="6" align="center" style="font-weight: bold;">Dilsukhnagar</td>
							</tr>
							<tr>
								<td colspan="6" align="center" style="font-weight: bold;">Balance
									Sheet Report of <u><%=vName%></u></td>
							</tr>
							<tr>
								<td colspan="6" align="center" style="font-weight: bold;">Report
									From Date <%=fromDate%> to <%=toDate%>
								</td>
							</tr>
						</tbody>
					</table>
<%
String fromDate1 = fromDate+" 00:00:00";
String toDate1 = toDate+" 23:59:59";
//System.out.println(hoa+" "+ fromDate1+" "+ toDate1+" "+ vId);
List<godwanstock> gList = gsl.getMSEbyVendorBtwDatesBasedOnVendorWithGroupBy(hoa, fromDate1, toDate1, vId);
List<godwanstock> gList1 = gsl.getMSEbyVendorBtwDatesBasedOnVendor(hoa, fromDate1, toDate1, vId);
List<payments> pList = pl.getpaymentsBtwDatesByHOABasedOnVendor(hoa, fromDate1, toDate1, vId);
godwanstock GList = new godwanstock();
godwanstock GList1 = new godwanstock();
payments PList = new payments();
DecimalFormat df = new DecimalFormat("####0.00");
String pName = "";
String pName1 = "";
double gTotal = 0.0;
double gTotal1 = 0.0;
double pTotal = 0.0;
double pTotal1 = 0.0;
double pTotal2 = 0.0;
String bName = "";
double cum = 0.0;
String lastFinfirstYear[]  = toDate.split("-");
String lastFinlastYera[] = fromDate.split("-");
String lastFinYearfirst = (Integer.parseInt(lastFinlastYera[0]) - 1) + "-04-01" ;
String lastFinYearlase  = (Integer.parseInt(lastFinfirstYear[0]) - 1) + "-03-31" ;
//System.out.println(lastFinYearfirst+" ----> "+lastFinYearlase);
String expensesAmount = "";
String mseId="";
double lastYearPendingbal = 0.0;
double sumValue=0.0;
int b=1;
%>
					<div style="clear: both"></div>
					<table width="90%" cellspacing="0" cellpadding="0" style="margin:0 auto;" class="new-table" border="1">
						<tr><td colspan="4" align="center" style="font-weight:bold">TOTAL ENTRIES IN GODOWN</td></tr>
						<tr>	
					 	 <td width="10%" bgcolor="#CC6600">S.No</td> 
						<td width="25%" bgcolor="#CC6600">INVOICE.NO</td>	
							<td width="30%" bgcolor="#CC6600">Entry Date</td>
							<td width="30%" bgcolor="#CC6600">Total Amount</td>
						</tr>
						<%if(!type.equals("") && type.equals("cumulative")){
							
							VendorsAndPayments vendorsAndPayments = new VendorsAndPayments();
							List<String[]> pendingAmount = vendorsAndPayments.getVendorOpeningAndPendingAmount(vId, hoa, finYr, "2015-03-31 00:00:00", fromDate.split("-")[0]+"-03-31 23:59:59", "sum");
							
							
// 							String openginpending = gsl.OpeningPendingBalanceByDept(vId, lastFinYearfirst ,lastFinYearlase, hoa);
							//System.out.println("IN BALANCESHEE_VENDORWISE PENDING BALANCE YEARS : "+ lastFinYearfirst+" ----> "+lastFinYearlase);
							if(pendingAmount != null && pendingAmount.size() > 0){
								lastYearPendingbal = Double.parseDouble(pendingAmount.get(0)[5]);
							}
					%>
						 <tr>
							<td colspan="3" align="center" style="color:orange;font-weight:bold">OPENING PENDING BALANCE</td>
							<td colspan="1" style="font-weight:bold"><%=df.format(lastYearPendingbal)%></td>
						</tr> 
						<%}%>
						<%
						if(gList.size() > 0){
							
							String mse1  = "";
							
							for(int i = 0 ; i < gList.size() ; i++){
								int c = 1;
								GList = gList.get(i);
// 								pName = pdl.getProductNameByCat(GList.getproductId(), hoa);
								
// 								gTotal = Double.parseDouble(GList.getExtra16());
								
								%>
						<tr>
							<td width="10%"><%=b%><%b=b+1; %></td>
							<td width="20%"><a href="#" onclick="showDetails('<%=GList.getextra1()%>')"><%=GList.getextra1()%></a></td>
							<td width="20%"><%=GList.getdate()%></td>
							
							<%
								gTotal1 = gTotal1 + Double.parseDouble(GList.getExtra16());
							%>
							<td width="25%"><%=GList.getExtra16()%></td>
							</tr><tr>
							<td colspan="7">
							<div style="display: none;" id="<%=GList.getextra1()%>">
							<table width="95%" border="1">
							<tr>
							<td width="7%" bgcolor="#CC6600">SNo</td>
							<td width="30%" bgcolor="#CC6600">Product</td>
							<td width="7%" bgcolor="#CC6600">Quantity</td>
							<td width="10%" bgcolor="#CC6600">Rate</td>
						<td width="5%" bgcolor="#CC6600">Vat</td>
						<td width="12%" bgcolor="#CC6600">Amount</td>
							</tr>
							<%
							gList1 = gsl.getMSEbyVendorBtwDatesBasedOnVendorWithMSCId(hoa, fromDate1, toDate1, vId,GList.getextra1()); 
							String totalAmount ="0.0";
							if(gList1.size()>0){
								String mse2  = "";
								
								for(int n=0;n<gList1.size();n++){
									GList1 = gList1.get(n);
									pName1 = pdl.getProductNameByCat(GList1.getproductId(), hoa);
									if(pName1.equals("") || pName == null){
										pName1 = pdl.getProductNameByCat1(GList1.getproductId(), hoa);
										//System.out.println(pName);
									}//Inner if closed
// 									if(mse1.equals("") && GList1.getExtra8()!=null){
// 										if(!GList1.getExtra8().equals("")){
// 											gTotal = Double.parseDouble(GList1.getquantity())*(Double.parseDouble(GList1.getpurchaseRate())+Double.parseDouble(GList1.getExtra18())+Double.parseDouble(GList1.getExtra19()) + Double.parseDouble(GList1.getExtra20()))*(1+(Double.parseDouble(GList1.getvat())/100)) + Double.parseDouble(GList1.getExtra8());
// 									}else{
// 										gTotal = Double.parseDouble(GList1.getquantity())*(Double.parseDouble(GList1.getpurchaseRate())+Double.parseDouble(GList1.getExtra18())+Double.parseDouble(GList1.getExtra19()) + Double.parseDouble(GList1.getExtra20()))*(1+(Double.parseDouble(GList1.getvat())/100));
// 									}
									
// 								}else if(!mse1.equals(GList1.getextra1()) && GList1.getExtra8()!=null){
// 									if(!GList1.getExtra8().equals("")){
// 										gTotal = Double.parseDouble(GList1.getquantity())*(Double.parseDouble(GList1.getpurchaseRate())+Double.parseDouble(GList1.getExtra18())+Double.parseDouble(GList1.getExtra19()) + Double.parseDouble(GList1.getExtra20()))*(1+(Double.parseDouble(GList1.getvat())/100)) + Double.parseDouble(GList1.getExtra8());
// 								}else{
// 									gTotal = Double.parseDouble(GList1.getquantity())*(Double.parseDouble(GList1.getpurchaseRate())+Double.parseDouble(GList1.getExtra18())+Double.parseDouble(GList1.getExtra19()) + Double.parseDouble(GList1.getExtra20()))*(1+(Double.parseDouble(GList1.getvat())/100));
// 								}
// 									}
// 									else if(mse1.equals(GList1.getextra1())){
// 										gTotal = Double.parseDouble(GList1.getquantity())*(Double.parseDouble(GList1.getpurchaseRate())+Double.parseDouble(GList1.getExtra18())+Double.parseDouble(GList1.getExtra19()) + Double.parseDouble(GList1.getExtra20()))*(1+(Double.parseDouble(GList1.getvat())/100));
// 									}
									
// 									gTotal = Double.parseDouble(GList1.getExtra16());
							%>
							<tr>
							<td width="7%"><%=c %><%c+=1; %></td>
							<td width="30%"><%=GList1.getproductId()%> <%=pName1%></td>
							<td width="7%"><%=GList1.getquantity()%></td>
							<td width="10%"><%=df.format(Double.parseDouble(GList1.getpurchaseRate())+Double.parseDouble(GList1.getExtra18())+Double.parseDouble(GList1.getExtra19()) + Double.parseDouble(GList1.getExtra20())) %></td>
						<td width="5%"><%=GList1.getvat()%></td>
						<td width="12%"><%=df.format((Double.parseDouble(GList1.getpurchaseRate())+Double.parseDouble(GList1.getExtra18())+Double.parseDouble(GList1.getExtra19()) + Double.parseDouble(GList1.getExtra20()))*Double.parseDouble(GList1.getquantity())*(1+(Double.parseDouble(GList1.getvat())/100)))%></td>
							</tr><%
							totalAmount = df.format(Double.parseDouble(totalAmount) + (Double.parseDouble(GList1.getpurchaseRate())+Double.parseDouble(GList1.getExtra18())+Double.parseDouble(GList1.getExtra19()) + Double.parseDouble(GList1.getExtra20()))*Double.parseDouble(GList1.getquantity())*(1+(Double.parseDouble(GList1.getvat())/100)));
								}
								}
								%>
							<tr>
								<td width="15%" colspan="5" align="right">Extra Charges : </td>
								<%-- <td><td width="15%" colspan="1"><%=GList1.getExtra8()%></td></td> --%>
								<td width="15%" colspan="1"><%=GList1.getExtra8()%></td>
							</tr>
							
							<tr>
								<td colspan="5" align="right">Total Amount :</td><td colspan="1"><%=totalAmount%></td>
							</tr>
							</table>
							<table>
							
							</table>
							</div></td>
							</tr>
							<%
							mse1 = GList.getextra1();
							
							}%>
							<tr>
								<td colspan="3" align="center" style="color:orange;font-weight:bold">TOTAL AMOUNT TO BE PAID&nbsp;</td>
								<td colspan="1" style="font-weight:bold"><%=df.format(gTotal1+lastYearPendingbal)%></td>
							</tr>
							<%}else{%>
						<tr>
							<td colspan="6" align="center" style="color:red;">No entries in godown from <%=fromDate%> to <%=toDate%></td>
						</tr>
						<%}%>
						</table>
						<br/>
						<table width="90%" cellspacing="0" cellpadding="0" style="margin: 0 auto;" class="new-table" border="1">
						<tr><td colspan="6" align="center" style="font-weight:bold">TOTAL PAYMENTS MADE</td></tr>
						<tr>
					<!-- 	<td>S.No</td> -->
							<td width="15%" bgcolor="#CC6600">Payment Made Date</td>
							<td width="10%" bgcolor="#CC6600">Payment Mode</td>
							<td width="10%" bgcolor="#CC6600">Mse No</td>
							<td width="20%" bgcolor="#CC6600">Bank</td>
							<td width="20%" bgcolor="#CC6600">Narration</td>
							<td width="10%" bgcolor="#CC6600">Bill Amount</td>
							<td width="15%" bgcolor="#CC6600">Actual Amount Paid</td>
						</tr>
						<%if(pList.size()>0){
							for(int j=0;j<pList.size();j++){
								PList = pList.get(j);
								bName = bdl.getBankName(PList.getbankId());
								expensesAmount = pel.getExpensesInvoiceTotAmtNew(PList.getextra3());
								mseId=pel.getMseIdBasedOnExpInvId(PList.getextra3());
								//System.out.println("mseid==== >"+mseId);
								pTotal = Double.parseDouble(PList.getamount());
								pTotal2 = Double.parseDouble(expensesAmount);
								pTotal1 = pTotal+pTotal1;
								
								%>
						<tr>
						<%-- <td><%= j+1 %></td> --%>
							<td width="15%"><%=PList.getdate()%></td>
							<td width="10%"><%=PList.getpaymentType()%><%if(PList.getpaymentType().equals("cheque")){%> (<%=PList.getchequeNo()%>)<%}%></td>
							<td width="15%"><%=mseId%></td>
							<td width="20%"><%=PList.getbankId()%> <%=bName%></td>
							<td width="20%"><%=PList.getnarration()%></td>	
							<%
								if(df.format(pTotal2).equals(df.format(pTotal))){
							%>	
							<td width="15%"><%=df.format(pTotal2)%></td>
							<td width="10%"><%=df.format(pTotal)%></td>
							<%}else{%>
								<td width="15%" bgcolor="red"><%=df.format(pTotal2)%></td>
							<td width="10%" bgcolor="red"><%=df.format(pTotal)%></td>
								
							<%} %>
						</tr>
						<%}%>
						<tr>
								<td colspan="5" align="right" style="color:orange;font-weight:bold">TOTAL AMOUNT PAID&nbsp;</td>
								<td colspan="1" style="font-weight:bold"><%=df.format(pTotal1)%></td>
							</tr>
							<%}else{%>
						<tr>
							<td colspan="6" align="center" style="color:red;">No payments made from <%=fromDate%> to <%=toDate%></td>
						</tr>
						<%}%>
					</table>
					<br/>
					<table width="90%" cellspacing="0" cellpadding="0" style="margin: 0 auto;" class="new-table" border="1">
					<tr>
						<td width="75%" align="right" style="color:orange;font-weight:bold">GRAND TOTAL&nbsp;(<%=df.format(gTotal1+lastYearPendingbal)%> - <%=df.format(pTotal1)%>)&nbsp;</td>
						<td width="15%" align="left" style="font-weight:bold"><%=df.format((gTotal1+lastYearPendingbal)-pTotal1)%></td>
					</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
	<%
		} else {
			response.sendRedirect("index.jsp");
		}
	%>
</body>
</html>