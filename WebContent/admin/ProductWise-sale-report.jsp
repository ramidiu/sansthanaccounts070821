<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SHOP SALES DETAIL REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />
<!-- <link href="css/printStyle.css" type="text/css" rel="stylesheet" /> -->
<!--Date picker script  -->
<script src="../js/jquery-1.4.2.js"></script>

<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
  function productsearch(){
      	  var data1=$('#productname').val();
      	  var headID="3";
      	  $.post('searchProducts.jsp',{q:data1,hId:headID},function(data)
      	{
      			 var productNames=new Array();
      		var response = data.trim().split("\n");
      		 for(var i=0;i<response.length;i++){
      			 var d=response[i].split(",");
      			 productNames.push(d[0] +" "+ d[1]);
      		 }
      		var availableTags=data.trim().split("\n");
      		var availableIds=data.trim().split("\n");
      		availableTags=productNames;

      	 $( "#productname" ).autocomplete({source: availableTags}); 
      			});
      }
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../js/jquery.print.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("adminId")!=null){ %>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<!-- <div class="sj">
					  <form action="ProductWise-sale-report.jsp" method="post">  
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div> -->
				<form action="ProductWise-sale-report.jsp" method="post">
				
			    <div class="search-list">
					
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text" name="productname" id="productname" <%if(request.getParameter("productname")!=null){ %> value="<%=request.getParameter("productname") %>" <%}else{ %> value="" <%} %> onkeyup="productsearch()" placeholder="Find product here" autocomplete="off"/></li>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				
				<div class="icons">
					<a id="print"><span><img src="../images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a> 
					<span><a id="btnExport" href="#Export to excel"><img src="../images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
						
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	customerpurchasesListing SALE_L = new customerpurchasesListing();
	banktransactionsListing BKTRAL=new banktransactionsListing();
	productsListing PRDL=new productsListing();
	employeesListing EMPL=new employeesListing();
	
	List BANK_DEP=null;
	
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:00";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	String productID="";
	if(request.getParameter("productname")!=null && !request.getParameter("productname").equals("")){
		String pro[]=request.getParameter("productname").split(" ");
		productID=pro[0];
	}
	List SAL_list=SALE_L.getProdcutWiseSalesListBasedOnProduct("3",fromDate,toDate,productID);
	List SAL_DETAIL=null;
	double totalAmount=0.00;
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double bankDepositTot=0.00;
	double creaditSaleTot=0.00;
	double cashSaleAmt=0.00;
	double totQty=0;
	if(SAL_list.size()>0){%>
    <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 9px;
    z-index: 1;
    width:97.3%; 
    background:#d0e3fb; 
}

</style>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	<div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST POOJA STORES</td>
						</tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;">Product Wise Sales Detail-Report</td></tr>
						<tr><td colspan="8" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
                        
						
				<tr><td colspan="8"><table width="100%">
<tbody class="yourID">
<tr>
							<td class="bg" width="2%" style="font-weight: bold;">S.NO.</td>
							<!-- <td class="bg" width="18%" style="font-weight: bold;">INVOICE DATE</td>
							<td class="bg" width="5%" style="font-weight: bold;">INVOICE.NO.</td> -->
							<td class="bg" width="6%" style="font-weight: bold;">DEPT</td>
							<td class="bg" width="6%" style="font-weight: bold;">CODE</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="center">PRODUCT NAME</td>
							<td class="bg" width="5%" style="font-weight: bold;" align="right">QTY</td>
							<td class="bg" width="8%" style="font-weight: bold;" align="right">RATE</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">AMOUNT</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">Entered BY</td>
						</tr>
</tbody>
	


						<%
						Double totalSaleAmt=0.00;
						Double totDeposit=0.00;
						Double openingBal=0.00;
						if(SALE_L.getTotalSalesAmount("3","",fromDate)!=null && !SALE_L.getTotalSalesAmount("3","",fromDate).equals("")){
							 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount("3","",fromDate));
						}
						if(BKTRAL.getEmployeePrevDatesDepositAmount("",fromDate)!=null && !BKTRAL.getEmployeePrevDatesDepositAmount("",fromDate).equals("")){
							 totDeposit=Double.parseDouble(BKTRAL.getEmployeePrevDatesDepositAmount("",fromDate));}
						openingBal=totalSaleAmt-totDeposit;
						 %>
                         
                         
						<tr>
							<td width="2%"></td>
                            <!-- <td width="18%"></td>
                            <td width="5%"></td> -->
                            <td width="6%"></td>
                            <td width="6%"></td>
							<td width="20%">OPENING BALANCE</td>
                            <td width="5%"></td>
							<td align="right" style="font-weight: bold;" width="8%">&#8377; <%=df.format(totalSaleAmt-totDeposit)%></td>
							<td align="right" style="font-weight: bold;" width="10%">&#8377;0.00</td>
							<td width="10%"></td>
						</tr>
						<%for(int i=0; i < SAL_list.size(); i++ ){ 
							SALE=(beans.customerpurchases)SAL_list.get(i);
							if(!presentDate.equals("")){
								prevDate=presentDate;	
							}
							presentDate=""+chngDateFormat.format(dbDateFormat.parse(SALE.getdate()));
							if(SALE.getcash_type().equals("cash")){
								cashSaleAmt=cashSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}else{
								creaditSaleTot=creaditSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}
							totQty=totQty+Double.parseDouble(SALE.getquantity());
							%>
						   <tr>
								<td style="font-weight: bold;" width="2%"><%=i+1%></td>
								<%-- <td style="font-weight: bold;" width="18%"><%=chngDateFormat.format(dbDateFormat.parse(SALE.getdate()))%></td>
								<td style="font-weight: bold;" width="5%"><%=SALE.getbillingId()%>
								</td> --%>
								<td style="font-weight: bold;" width="6%"><span>
								SHOP</span></td>
								<td style="font-weight: bold;" width="6%"><%=SALE.getproductId()%></td>
								<%totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());%>
								<td style="font-weight: bold;" width="20%"><%=PRDL.getProductsNameByCat(SALE.getproductId(), SALE.getextra1())%></td>
								<td align="right" width="5%"><%=SALE.getquantity()%></td>
								<td align="right" width="8%"><%=SALE.getrate() %></td>
								<td style="font-weight: bold;" align="right" width="10%">&#8377; <%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td>
								<td align="right" width="10%"><%=EMPL.getMemployeesName(SALE.getemp_id()) %></td>
								
								<%-- <%if(!prevDate.equals(presentDate)){ 
								String sjDate=chngDF.format(dbDateFormat.parse(SALE.getdate()));
								String depositAmt=BKTRAL.getEmployeeDepositAmount(session.getAttribute("empId").toString(),sjDate);%>
								<td style="font-weight: bold;" align="right">&#8377;<%if(depositAmt!=null){
									%> <%=depositAmt%><%}else{ %>00<%} %></td>
							<%}else{ %>
								<td></td>
							<%} %> --%>
						</tr>
						<%} %>
					
					 
					   <tr style="border: 1px solid #000;">
						<td colspan="6" align="right" style="font-weight: bold;">Total sales Quantity | </td>
						<td style="font-weight: bold;" align="center"> <%=df.format(totQty)%></td>
						<td  colspan="1" style="font-weight: bold;" align="right"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="6" align="right" style="font-weight: bold;">Total sales amount | </td>
						<td style="font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount)%></td>
						<td style="font-weight: bold;" align="right">&#8377;<%=df.format(bankDepositTot) %></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="6" align="right" style="font-weight: bold;">Total cash sales | </td>
						<td style="font-weight: bold;" align="right"> &#8377; <%=df.format(cashSaleAmt) %></td>
						<td style="font-weight: bold;" colspan="1"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="6" align="right" style="font-weight: bold;">Total credit sales| </td>
						<td style="font-weight: bold;" align="right"> &#8377; <%=df.format(creaditSaleTot) %></td>
						<td style="font-weight: bold;" colspan="1"></td>
						</tr>
						<tr style="border: 1px solid #000;">
						<td colspan="6" align="right" style="font-weight: bold;">Closing Balance | </td>
						<td style="font-weight: bold;" align="right"> &#8377; <%=df.format(openingBal+cashSaleAmt-bankDepositTot) %></td>
						<td style="font-weight: bold;" colspan="1"></td>
						</tr>
                        </table></td></tr>	
					</table>
				</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no Sales list found!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>