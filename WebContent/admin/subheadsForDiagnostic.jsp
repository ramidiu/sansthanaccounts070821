<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="mainClasses.subheadListing"%>
<%@ page import="java.util.List"%>
<%@ page import="beans.subhead"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sub heads for Diagnostic</title>
<script type="text/javascript" lang="javascript" src="../js/modal-window.js"></script>	

<style>
 .modal-window {
   
    position: fixed;
    top: 40%;
    width: 567px !important;
    left: 43%;
    margin: 0; 
    padding: 34px 0 0;
   
    box-shadow: 0 0 8px #666;
    background: #fff;
    border-radius: 12px;
    border: 1px solid #999;
} 
</style> 
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 500;
		modalWindow.height = 400;
		modalWindow.margin
		modalWindow.content = "<iframe width='500' height='400' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
</head>
<body>
<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-list">	
       
				<div class="search-list">
					
				</div>
				<table width="100%" cellpadding="0" cellspacing="0">
<tbody><tr>
  
</tr>

<tr><td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</td>
</tr><tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td>

<tr><td colspan="7" align="center" style="font-weight: bold;">DIAGNOSTIC  SUBHEADS  LIST</td></tr>
</tr>

</tbody>
</table>
				
				<div class="clear"></div>
				<div class="list-details">
				

<table width="100%" cellpadding="0" cellspacing="0" border="1">
<tbody><tr>
    <td class="bg" width="10%">Subheads Id</td>
	<td class="bg" width="15%">Subheads Name</td>
	<td class="bg" width="15%">Patient Amount</td>
	<td class="bg" width="20%">SansthanAmount</td>
	<td class="bg" width="15%">Upload</td>
</tr>

<%
    subheadListing subHeadListing=new subheadListing(); 
    List<subhead>  subHeadsList=subHeadListing.getDiagnosticHeads();
   if(subHeadsList!=null && subHeadsList.size()>0){
	   for(int i=0;i<subHeadsList.size();i++){
       subhead subHead=subHeadsList.get(i);
  %>
<tr>
	<td><%=subHead.getsub_head_id()%></td>
	<td><%=subHead.getname()%></td>
	<td><%=subHead.getextra2()%></td>
	<td><%=subHead.getextra4()%></td>
	<%-- <td><a href="#" onclick="openMyModal('subheadsfordiagnosticwithupdate.jsp?subHeadId=<%=subHead.getsub_head_id()%>&extra2=<%=subHead.getextra2()%>&extra4=<%=subHead.getextra4()%>');">Update</a></td> --%>
<td style="font-size: 10px;font-weight: bold"><span style="color: #8b421b;cursor: pointer;" target="_blank" onclick="openMyModal('subheadsfordiagnosticwithupdate.jsp?subHeadId=<%=subHead.getsub_head_id()%>&extra2=<%=subHead.getextra2()%>&extra4=<%=subHead.getextra4()%>');">Update</span></td>
</tr>
<%} }%>
</tbody>
</table>

</div></div></div>
</body>
</html>