
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import="mainClasses.employeeDetailsListing,java.util.List,java.util.Iterator,beans.employeeDetails"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>
</head>
<body>
<table  border="1" style="width:100%" >
<tr><th>Employee Id:</th><th>Employee Name:</th><th>Unique Registration No:</th><th>Basic Pay Scale:</th><th>Ewf Loan Total Amount:</th><th>Ewf Loan Due Amount</th><th>Personal Loan Total Amount:</th><th>Personal Loan Due Amount:</th><th>Eligible For Hra:</th><th>Eligible For TDS Deduction:</th><th>Eligible For ewf Deduction:</th><th>Select Bank:</th><th>Account No:</th><th>Action</th><th></th><th></th><th></th></tr>
<% 
		employeeDetailsListing listing=new employeeDetailsListing();
		List l=listing.selectAllEmployeeDetails();
		Iterator itr=l.iterator();
		
		while(itr.hasNext())
		{
			employeeDetails edetails=(employeeDetails)itr.next();
			String employeeName=listing.getEmployeeName(edetails.getEmployee_id());
		if(employeeName!=null && !employeeName.trim().equals("")){
		%>
		<tr>
		<td><%=edetails.getEmployee_id() %></td>
		<td><%=employeeName%></td>
		<td><%=edetails.getUnique_registration_no() %></td>
		<td><%=edetails.getBasic_pay_salary() %></td>
		<td><%=edetails.getEwf_total_loan_amount() %></td>
		<td><%=edetails.getEwf_due_amount() %></td>
		<td><%=edetails.getPersonal_total_loan_amount() %></td>
		<td><%=edetails.getPersonal_due_amount() %></td>
		<td><%if(edetails.getExtra1().equals("null")){out.println("no");}else{out.println("yes");}%></td>
		<td><%if(edetails.getExtra2().equals("null")){out.println("no");}else{out.println("yes");}%></td>
		<td><%if(edetails.getExtra3().equals("null")){out.println("no");}else{out.println("yes");}%></td>
		<td><%=edetails.getExtra4()%></td>
		<td><%=edetails.getExtra5() %></td>
		<td><a href="adminPannel.jsp?page=employeeDetailsForm&employeeId=<%=edetails.getEmployee_id()%>&empName='<%=employeeName%>'&uniqueRegNo=<%=edetails.getUnique_registration_no() %>
				&basicPay=<%=edetails.getBasic_pay_salary() %>
				&totalEwfLoanAmount=<%=edetails.getEwf_total_loan_amount() %>
				&totalEwfDueAmount=<%=edetails.getEwf_due_amount() %>
				&totalPersonalLoanAmount=<%=edetails.getPersonal_total_loan_amount() %>
				&totalPersonalDueAmount=<%=edetails.getPersonal_due_amount() %>
				&eligible_for_hra=<%=edetails.getExtra1()%>
				&eligible_for_TDS_deduction=<%=edetails.getExtra2()%>
				&eligible_for_ewf_deduction=<%=edetails.getExtra3()%>
				&bank=<%=edetails.getExtra4()%>
				&acc=<%=edetails.getExtra5()%>
				&totalEwfInstallments=<%=edetails.getExtra6()%>
				&balEwfInstallments=<%=edetails.getExtra7()%>
				&totalPersonalInstallments=<%=edetails.getExtra8()%>
				&balPersonalInstallments=<%=edetails.getExtra9()%>
				&next_date_of_increment=<%=edetails.getExtra10()%>">edit</a>
				</td>
		<td></td>
		<td></td>
		</tr>
	<%} %>
<%} %>
</table>



</body>
</html>