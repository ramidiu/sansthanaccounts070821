<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.no_genaratorListing"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<jsp:useBean id="EWFS" class="beans.ewf_entriesService"/>
<jsp:useBean id="NOG" class="beans.no_genaratorService"/>
<jsp:useBean id="BANK" class="beans.bankdetails"></jsp:useBean>
<%
boolean x = false;
no_genaratorListing NG_l=new no_genaratorListing();
double bankpramount=0.0;
String bankflag="";
List bankList=null;
String amount="0";
String ewfNum=NG_l.getid("ewf_entries");
String HOAID=request.getParameter("hoid");
String narration=request.getParameter("narration");
Calendar c1 = Calendar.getInstance(); 
c1.setTimeZone(TimeZone.getTimeZone("IST"));
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat  dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat  dateFormat3= new SimpleDateFormat("HH:mm:ss");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String date1=(dateFormat.format(c1.getTime())).toString();
String hour=""+c1.get(Calendar.HOUR);
String min=""+c1.get(Calendar.MINUTE);
String sec=""+c1.get(Calendar.SECOND);
//String time=dateFormat3.format((c1.get(Calendar.HOUR)+":"+c1.get(Calendar.MINUTE)+":"+c1.get(Calendar.SECOND)));
String time=dateFormat3.format(dateFormat.parse(date1));
String date=request.getParameter("date")+" "+time;
String temp[]=null;

String emp_id=session.getAttribute("adminId").toString();
String subheadId="";
String majorhead="";
String minorhead="";
String vocharNumber=request.getParameter("voucherRefNo");
String debitAmt="";
String creditAmt="";
String count=request.getParameter("addcount");
DecimalFormat df2 = new DecimalFormat("###.##");
Double totalbillAMt=0.00;
String totAmt="";
int i=0; 
if(count != null){
	i=Integer.parseInt(count);
	for(int h=0;h<i;h++){
	if(request.getParameter("subhead"+h)!="" && request.getParameter("minorhead"+h)!="" && request.getParameter("creditAmt"+h)!="0"  && request.getParameter("majorhead"+h)!="" && request.getParameter("debitAmt"+h)!="0"  ){
subheadId=request.getParameter("subhead"+h);
temp=subheadId.split(" ");
subheadId=temp[0];
majorhead=request.getParameter("majorhead"+h);
temp=majorhead.split(" ");
majorhead=temp[0];
minorhead=request.getParameter("minorhead"+h);
temp=minorhead.split(" ");
minorhead=temp[0];
debitAmt=request.getParameter("debitAmt"+h);
creditAmt=request.getParameter("creditAmt"+h);
if(debitAmt!="" && Double.parseDouble(debitAmt)>0){
	bankflag="debit";
		 %>		 
<jsp:setProperty name="EWFS" property="sub_head_id" value="<%=subheadId%>"/>
<jsp:setProperty name="EWFS" property="entry_date" value="<%=date%>"/>
<jsp:setProperty name="EWFS" property="narration" value="<%=narration%>"/>
<jsp:setProperty name="EWFS" property="enter_by" value="<%=emp_id%>"/>
<jsp:setProperty name="EWFS" property="minor_head_id" value="<%=minorhead%>"/>
<jsp:setProperty name="EWFS" property="major_head_id" value="<%=majorhead%>"/>
<jsp:setProperty name="EWFS" property="head_account_id" value="<%=HOAID%>"/>
<jsp:setProperty name="EWFS" property="amount" value="<%=debitAmt%>"/>
<jsp:setProperty name="EWFS" property="credit_or_debit" value="<%=bankflag%>"/>
<jsp:setProperty name="EWFS" property="extra1" value="<%=vocharNumber%>"/>

<%x=EWFS.insert();%><%=EWFS.geterror()%>
<%
} else if(creditAmt!="" &&  Double.parseDouble(creditAmt)>0 ){
	bankflag="credit";
%>
	
<jsp:setProperty name="EWFS" property="sub_head_id" value="<%=subheadId%>"/>
<jsp:setProperty name="EWFS" property="entry_date" value="<%=date%>"/>
<jsp:setProperty name="EWFS" property="narration" value="<%=narration%>"/>
<jsp:setProperty name="EWFS" property="enter_by" value="<%=emp_id%>"/>
<jsp:setProperty name="EWFS" property="minor_head_id" value="<%=minorhead%>"/>
<jsp:setProperty name="EWFS" property="major_head_id" value="<%=majorhead%>"/>
<jsp:setProperty name="EWFS" property="head_account_id" value="<%=HOAID%>"/>
<jsp:setProperty name="EWFS" property="amount" value="<%=creditAmt%>"/>
<jsp:setProperty name="EWFS" property="credit_or_debit" value="<%=bankflag%>"/>
<jsp:setProperty name="EWFS" property="extra1" value="<%=vocharNumber%>"/>	
		
	<%=EWFS.insert()%><%=EWFS.geterror()%>
	<%
}
}}}
   response.sendRedirect("adminPannel.jsp?page=Ewf_Entry_Form");   
%>
