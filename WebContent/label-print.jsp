<%@page import="beans.customerpurchases"%>
<%@page import="model.SansthanAccountsDate"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>

<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRO OFFICE DAILY POOJAS LIST | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});	
});

$(function() {
	var year = (new Date).getFullYear();	//current year
	$( "#fromDate2" ).datepicker({
		minDate: new Date(year, 0, 1),
        maxDate: new Date(year, 11, 31),
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate2" ).datepicker( "option", "minDate", selectedDate );
		}
	});	
	$( "#toDate2" ).datepicker({
		minDate: new Date(year, 0, 1),
        maxDate: new Date(year, 11, 31),
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate2" ).datepicker( "option", "maxDate", selectedDate );
		}
	});	
});

function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
}
$(document).ready(function(){
	
	$("#checkAll").click(function(){
	    $('input:checkbox').not(this).prop('checked', this.checked);
	    $('.specialDays').attr('checked', false);
	});
	
	$("#pujaDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
		/* dateFormat: 'yy-mm-dd' */
	});	
	/* $( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		}); */
	});
function productsearch(){
	  var data1=$('#productId').val();
	  var headID=null;
	  $.post('searchSubhead.jsp',{q:data1,hoa:headID,page :'Rec'},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productId" ).autocomplete({source: availableTags}); 
			});
}
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                    $( "a" ).css("text-decoration","none");
                    $( ".printable" ).print();
                   

                    // Cancel click event.
                    return( false );
                });
			});
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
    /* $( "#subheadId" ).change(function() {
		  $( "#searchForm" ).submit();
		}); */
});

function validateForm() 
{
    var subheadId = document.forms["searchForm2"]["subheadId2"].value;
    var email = document.forms["searchForm2"]["email"].value;
    var fromDate = document.forms["searchForm2"]["fromDate2"].value;
    var toDate = document.forms["searchForm2"]["toDate2"].value;
    var splDay = document.forms["searchForm2"]["splDay"].value;
    
    if (subheadId == null || subheadId == "") 
    {
        alert("Please select Seva Type");
        return false;
    }
    
    if (email == null || email == "") 
    {
        alert("Please select Email Type");
        return false;
    }
    
    if ((fromDate != null && fromDate != "" && toDate != null && toDate != "" && splDay != null && splDay != "") || (fromDate != null && fromDate == "" && toDate != null && toDate == "" && splDay != null && splDay == "")) 
    {
        alert("Please Select Either Performed Date Or Special Day");
        return false;
    }
} 

</script>
<style>
.label
{
	width:350px; height:auto;
}

.label-print
{
	width: 336px;
 height:144px;
margin:13px 0px;
	border-radius:7px;
	background:#F5F5F5;
	
}

.label-print table td
{
	text-transform:uppercase;
	font-size:8px !important; 
	line-height:17px;
	font-family:Tahoma, Geneva, sans-serif !important; 
	
	
	
}

.label-print table td span
{
font-size:8px !important; 
	
}
</style>

<script language="javascript">
var gAutoPrint = true;
function processPrint() {
  if (document.getElementById != null) {
    var html = '<HTML>\n<HEAD>\n<style>.inp input[type=checkbox]{display:none;}</style>  ';
    if (document.getElementsByTagName != null) {
      var headTags = document.getElementsByTagName("head");
      if (headTags.length > 0) html += headTags[0].innerHTML;
    }
    html += '\n</HE' + 'AD>\n<BODY class="inp" '+((gAutoPrint)?'onLoad="window.print()"':'')+'>\n';
    
    var ids=[]; 
    $('input[type=checkbox]:checked').each(function(){
        ids.push($(this).val());
    });
    var divContent = "";
    for (var i=0;i<ids.length;i++) {
    	var elm=$("#readyToPrint"+ids[i]);
    	/* divContent += elm.html()+'<br />'; */
    	divContent += elm.html();
    	divContent=divContent.replace('undefined', '');
    }
   /*  var elems = document.getElementsByTagName("div");
    for (var i=0;i<elems.length;i++) {
      if (elems[i].className=="printable") 
   	  {
   	  	divContent += elems[i].innerHTML+'<br />';
   	  }
    } */
    
    if (divContent!="") {
      html += divContent;
      html += '\n</BO' + 'DY>\n</HT' + 'ML>';
      var printWin = window.open("","processPrint");
      printWin.document.write(html);
      printWin.document.close();
    }
    else alert("Error, no contents.");
    return false;
  }
}
</script>



<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="CAP" class="beans.customerapplication"/>
<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">
        <div class="clear"></div>
		<div class="vendor-title" style="padding:30px 30px 50px 20px;"> DEVOTEES LIST</div>
		<div class="vendor-list">
		<div class="arrow-down">
			<!-- <img src="images/Arrow-down.png" /> -->
		</div>
		<%
		String splDay = request.getParameter("splDay"); 
		
		String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		
		String fromdate2=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		String todate2=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
		
		if(request.getParameter("fromDate2")!=null && !request.getParameter("fromDate2").equals(""))
		{
			 fromdate2=request.getParameter("fromDate2");
			 todate2=request.getParameter("toDate2");
		}
		
		%>
		<form name="searchForm2" id="searchForm2" action="label-print.jsp" method="post" onsubmit="return validateForm()">
				<div style="width:100%;">
				<%
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dateFormat3= new SimpleDateFormat("dd-MMM-yyyy");
				DateFormat dateFormat4= new SimpleDateFormat("MM-dd");
				DateFormat dateFormat5= new SimpleDateFormat("dd MMM");
				%>
					<ul style="list-style:none;">
					<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="text"  name="fromDate2" id="fromDate2" class="DatePicker" value=""  readonly/></li>
					<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="text" name="toDate2" id="toDate2"  class="DatePicker" value="" readonly="readonly"/></li> 
					<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;">
				<select name="splDay" id="splDay" style="width:240px;">
<%
	if(splDay == null)
	{%>
		<option value="">-- SELECT --</option>
	<%}
%>
<option value="Guru poornima" <%if(splDay != null && splDay.equals("Guru poornima")) {%> selected="selected"<%} %>>Guru poornima</option>
<option value="Dasara" <%if(splDay != null && splDay.equals("Dasara")) {%> selected="selected"<%} %>>Dasara</option>
<option value="Sri Rama navami" <%if(splDay != null && splDay.equals("Sri Rama navami")) {%> selected="selected"<%} %>>Sri Rama navami</option>
<option value="Datta jayanthi" <%if(splDay != null && splDay.equals("Datta jayanthi")) {%> selected="selected"<%} %>>Datta jayanthi</option>
</select>
				</li>
				</ul>
				<div style="clear:both"></div>
				<ul style="list-style:none;">	
				<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;">
				<select name="email" id="email">
				<%if(request.getParameter("email") == null) {%><option value="">-- SELECT --</option><%} %>
				<option value="WithEmail"<%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithEmail")){ %> selected="selected" <%} %>>With Email</option>
				<option value="WithOutEmail" <%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithOutEmail")){ %> selected="selected" <%} %>>Without Email</option>
				<option value="WithMobile"<%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithMobile")){ %> selected="selected" <%} %>>With Mobile</option>
				<option value="WithOutMobile" <%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithOutMobile")){ %> selected="selected" <%} %>>Without Mobile</option>
				<option value="WithEmail&WithMobile"<%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithEmail&WithMobile")){ %> selected="selected" <%} %>>With Email & With Mobile</option>
				<option value="WithEmail&WithOutMobile" <%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithEmail&WithOutMobile")){ %> selected="selected" <%} %>>With Email & Without Mobile</option>
				<option value="WithOutEmail&WithMobile"<%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithOutEmail&WithMobile")){ %> selected="selected" <%} %>>Without Email & With Mobile</option>
				<option value="WithOutEmail&WithOutMobile" <%if(request.getParameter("email")!=null && request.getParameter("email").equals("WithOutEmail&WithOutMobile")){ %> selected="selected" <%} %>>Without Email & Without Mobile</option>
				</select>
				</li>
						
						<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;">
						<select name="subheadId2" id="subheadId2">
						<%if(request.getParameter("subheadId2") == null) {%><option value="">-- SELECT --</option><%} %>
							<option value="20092"<%if(request.getParameter("subheadId2")!=null && request.getParameter("subheadId2").equals("20092")){ %> selected="selected" <%} %>>NITYA ANNADANA PADHAKAM</option>
							<option value="20063" <%if(request.getParameter("subheadId2")!=null && request.getParameter("subheadId2").equals("20063")){ %> selected="selected" <%} %>>LIFE TIME ARCHANA MEMBERSHIP</option>
							
						</select>
						</li>
						<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="submit" class="click" name="search" value="Search"></input></li>
						
					</ul>
				</div></form>
				<div style="clear:both;height:100px;"></div>
		
		<form name="searchForm" id="searchForm" action="label-print.jsp" method="post">
		<div>
			<ul style="list-style:none;">
				<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;">
				<select name="subheadId" id="subheadId">
					<option value="20092"<%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20092")){ %> selected="selected" <%} %>>NITYA ANNADANA PADHAKAM</option>
					<option value="20063" <%if(request.getParameter("subheadId")!=null && request.getParameter("subheadId").equals("20063")){ %> selected="selected" <%} %>>LIFE TIME ARCHANA MEMBERSHIP</option>
				</select>
				</li>
				<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="text" name="fromDate"  id="fromDate"  placeholder="Enter from Date"  /></li>
				<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="text" name="toDate"  id="toDate"  placeholder="Enter to Date"  /></li>
				<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="submit" class="click" name="SEARCH" value="SEARCH"/></li>
			</ul>
		</div>
		
			
		<div style="clear:both"></div>
		<div>
		<ul style="list-style:none;">
			<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="checkbox" name="specialDays" class="specialDays" value="Guru poornima" />Guru poornima</li>
			<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="checkbox" name="specialDays" class="specialDays" value="Dasara" /> Dasara</li>
			<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="checkbox" name="specialDays" class="specialDays" value="Sri Rama navami" />Sri Rama navami</li>
			<li style="padding:0px 30px 0px 30px;margin:0 15px 0 0;float:left;"><input type="checkbox" name="specialDays" class="specialDays" value="Datta jayanthi" />Datta jayanthi</li>
		</ul>
		 </div> 
		
		
		</form>
		
		
		<!-- <div><input type="checkbox" id="checkAll" />check All</div> -->
		<div class="icons">
			<span><a id="print1" onclick="return processPrint();"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
			<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
			 <span>
				<ul>
					<li><img src="images/Setting-icon.png" />
						<div class="mini-menu">
							<dl>
								<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit Columns</dt>
								<dt><input type="checkbox" class="edit-setting" />Address</dt>
								<dt><input type="checkbox" class="edit-setting" />Email</dt>
							</dl>
					</div></li>
				</ul>

			</span>
		</div>
		<div class="clear"></div>
	<!-- <div><input type="checkbox" id="checkAll" />check All</div> -->
	<% DateFormat extra1DateFormat=new SimpleDateFormat("MM-dd");
		DateFormat userDateFormat=new SimpleDateFormat("MMMM  dd");
	if(request.getParameter("subheadId") != null || request.getParameter("specialDays") != null){%>
	<div><input type="checkbox" id="checkAll" />check All</div>
	<div class="list-details">
		<%
		SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
		//SimpleDateFormat chngDateFormat2=new SimpleDateFormat("MM-dd");
		//SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
		
		DateFormat datePickerDbFormat=new SimpleDateFormat("yyyy-MM-dd");
		customerpurchasesListing SALE_L = new customerpurchasesListing();
		productsListing PRDL=new productsListing();
	
		String subhead="20092";
		if(request.getParameter("subheadId")!=null && !request.getParameter("subheadId").equals("")){
			subhead=request.getParameter("subheadId");
		}
		String hoaID="4";
		customerapplicationListing CUSAL=new customerapplicationListing();
		List SAL_list=SALE_L.getDevoteesList(subhead);
		
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		
		List<String> allBetweenDates=null;
		if(fromDate!=null && toDate!=null && !fromDate.equals("") && !toDate.equals("")){
			allBetweenDates=SansthanAccountsDate.getBetweenDates(fromDate, toDate, "yyyy-MM-dd" ,"MM-dd",TimeZone.getTimeZone("IST"));
		}
		//String pujaDate=request.getParameter("pujaDate");
		
		
		String specialDays[]=request.getParameterValues("specialDays");
		
		//System.out.print("pujaDate=="+pujaDate);
		//System.out.print("specialDays=="+specialDays);
		List CUS_DETAIL=null;
		DecimalFormat df = new DecimalFormat("0.00");
		if(SAL_list.size()>0){%>
		<div class="printable">
			<div class="label">
				<%for(int i=0; i < SAL_list.size(); i++ ){ 
					
						boolean flag=false;
						SALE=(beans.customerpurchases)SAL_list.get(i);
						if(allBetweenDates!=null){
							flag=true;
							//String date=extra1DateFormat.format(datePickerDbFormat.parse(pujaDate));
							//CUS_DETAIL=CUSAL.getMcustomerDetailsBasedOnBillingIdDateWise(SALE.getbillingId(),date);
							CUS_DETAIL=CUSAL.getMcustomerDetailsBasedOnBillingId(SALE.getbillingId());
							
						}else{
							if(specialDays==null){
								flag=true;
								CUS_DETAIL=CUSAL.getMcustomerDetailsBasedOnBillingId(SALE.getbillingId());
							}
						}
						
						
						if(flag && CUS_DETAIL.size()>0 ){
						CAP=(customerapplication)CUS_DETAIL.get(0);
						//System.out.println("allBetweenDates="+allBetweenDates);
						//System.out.println("CAP.getextra1()="+CAP.getextra1());
						//System.out.println("===>"+allBetweenDates.contains(CAP.getextra1()));
							if(allBetweenDates!=null ){
								if(allBetweenDates.contains(CAP.getextra1())){
								%>
								   <div class="label-print" id="readyToPrint<%=SALE.getbillingId()%>">
									<input type="checkbox"  value="<%=SALE.getbillingId()%>"></input>
									<table width="100%" cellpadding="0" cellspacing="0" style="margin-top:0px;">
										<tr><td style="font-size:14px;"><%=CAP.getphone() %></td>
                                        <td align="right" style="padding-right:30px;font-size:14px;"><span><%if(CAP.getextra1() != null && !CAP.getextra1().trim().equals("") && CAP.getextra1().trim().length()>=4){ %><%=userDateFormat.format(extra1DateFormat.parse(CAP.getextra1()))%><%}else{ %><%=SALE.getextra7() %><%} %></span></td></tr>
										<tr><td colspan="2" style="font-weight:bold;font-size:14px; width:366px; overflow:hidden;" ><strong><%if(SALE.getcustomername().trim().equals("")){%>&nbsp;<%}else if(SALE.getcustomername().length() > 33){%><%=SALE.getcustomername().substring(0, 33) %><%} else{%><%=SALE.getcustomername() %><%} %></strong></td></tr>
										<tr><td colspan="2" style="font-size:12px;"><%if(SALE.getextra6().trim().equals("")){%>&nbsp;<%}else if(SALE.getextra6().length() > 38){%><%=SALE.getextra6().substring(0, 38) %><%} else{%><%=SALE.getextra6() %><%} %></td></tr>
										<tr><td colspan="2" style="font-size:12px;"><%if(CAP.getextra2().trim().equals("")){%>&nbsp;<%}else if(CAP.getextra2().length() > 38){%><%=CAP.getextra2().substring(0, 38) %><%} else{%><%=CAP.getextra2()%><%} %></td></tr>
										<tr><td colspan="2" style="font-size:12px;"><%if(CAP.getaddress2().trim().equals("")){%>&nbsp;<%}else if(CAP.getaddress2().length() > 38){%><%=CAP.getaddress2().substring(0, 38) %><%} else{%><%=CAP.getaddress2()%><%} %></td></tr>
										<tr><td colspan="2" style="font-size:12px;"><%=CAP.getextra3() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=CAP.getpincode()%></span></td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
									</div>
								 <%
								}
							}else{
								%>
								   <div class="label-print" id="readyToPrint<%=SALE.getbillingId()%>">
									<input type="checkbox"  value="<%=SALE.getbillingId()%>"></input>
									<table width="100%" cellpadding="0" cellspacing="0" style="margin-top:0px;">
										<tr><td style="font-size:14px;"><%=CAP.getphone() %></td>
                                        <td align="right" style="padding-right:30px;font-size:14px;"><span><%if(CAP.getextra1() != null && !CAP.getextra1().trim().equals("") && CAP.getextra1().trim().length()>=4){ %><%=userDateFormat.format(extra1DateFormat.parse(CAP.getextra1()))%><%}else{ %><%=SALE.getextra7() %><%} %></span></td></tr>
										<tr><td colspan="2" style="font-weight:bold;font-size:14px; width:366px; overflow:hidden"><strong><%if(SALE.getcustomername().trim().equals("")){%>&nbsp;<%}else if(SALE.getcustomername().length() > 33){%><%=SALE.getcustomername().substring(0, 33) %><%} else{%><%=SALE.getcustomername() %><%} %></strong></td></tr>
										<tr><td colspan="2" style="font-size:12px;"><%if(SALE.getextra6().trim().equals("")){%>&nbsp;<%}else if(SALE.getextra6().length() > 38){%><%=SALE.getextra6().substring(0, 38) %><%} else{%><%=SALE.getextra6() %><%} %></td></tr>
										<tr><td colspan="2" style="font-size:12px;"><%if(CAP.getextra2().trim().equals("")){%>&nbsp;<%}else if(CAP.getextra2().length() > 38){%><%=CAP.getextra2().substring(0, 38) %><%} else{%><%=CAP.getextra2()%><%} %></td></tr>
										<tr><td colspan="2" style="font-size:12px;"><%if(CAP.getaddress2().trim().equals("")){%>&nbsp;<%}else if(CAP.getaddress2().length() > 38){%><%=CAP.getaddress2().substring(0, 38) %><%} else{%><%=CAP.getaddress2()%><%} %></td></tr>
										<tr><td colspan="2" style="font-size:12px;"><%=CAP.getextra3() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=CAP.getpincode()%></span></td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
									</div>
								 <%
							}
						
						}
						if(specialDays!=null && SALE.getextra7()!=null){
							for(int k=0;k<specialDays.length;k++){
								Set<String> setOfSpecialDayFromDb = new HashSet<String>(Arrays.asList(SALE.getextra7().split(",")));
								if(setOfSpecialDayFromDb.contains(specialDays[k])){
									CUS_DETAIL=CUSAL.getMcustomerDetailsBasedOnBillingId(SALE.getbillingId());
									if(CUS_DETAIL.size()>0){
										CAP=(customerapplication)CUS_DETAIL.get(0);
										if(CAP.getextra1().trim().length()<5){
											%>
										 <div class="label-print" id="readyToPrint<%=SALE.getbillingId()%>">
											<input type="checkbox"   value="<%=SALE.getbillingId()%>"></input>
											<table width="100%" cellpadding="0" cellspacing="0" style="margin-top: 0px;">
												<tr><td style="font-size:14px;"><%=CAP.getphone() %></td>
                                                <td align="right" style="font-size:14px;"><span><%if(CAP.getextra1() != null && !CAP.getextra1().trim().equals("") && CAP.getextra1().trim().length()>=4){ %><%=userDateFormat.format(extra1DateFormat.parse(CAP.getextra1()))%><%}else{ %><%=SALE.getextra7().replaceFirst(",","") %><%} %></span></td></tr>
												<tr><td colspan="2" style="font-weight:bold; width:366px; overflow:hidden; font-size:14px;"><strong><%if(SALE.getcustomername().trim().equals("")){%>&nbsp;<%}else if(SALE.getcustomername().length() > 33){%><%=SALE.getcustomername().substring(0, 33) %><%} else{%><%=SALE.getcustomername() %><%} %></strong></td></tr>
												<tr><td colspan="2" style="font-size:12px;"><%if(SALE.getextra6().trim().equals("")){%>&nbsp;<%}else if(SALE.getextra6().length() > 38){%><%=SALE.getextra6().substring(0, 38) %><%} else{%><%=SALE.getextra6() %><%} %></td></tr>
												<tr><td colspan="2" style="font-size:12px;"><%if(CAP.getextra2().trim().equals("")){%>&nbsp;<%}else if(CAP.getextra2().length() > 38){%><%=CAP.getextra2().substring(0, 38) %><%} else{%><%=CAP.getextra2()%><%} %></td></tr>
												<tr><td colspan="2" style="font-size:12px;"><%if(CAP.getaddress2().trim().equals("")){%>&nbsp;<%}else if(CAP.getaddress2().length() > 38){%><%=CAP.getaddress2().substring(0, 38) %><%} else{%><%=CAP.getaddress2()%><%} %></td></tr>
												<tr><td colspan="2" style="font-size:12px;"><%=CAP.getextra3() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=CAP.getpincode()%></span></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</div>
										<%}
									}
								}//if(setOfSpecialDayFromDb.contains(specialDays[k]))
							}//for(int k=0;k<specialDays.length;k++)
						}//if(specialDays!=null && SALE.getextra7()!=null)
					}%>
			</div>
		</div>
	</div>
	<%
	}else{%>
		<div align="center">
			<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no list found for today!</span></div>
		</div>
	<%}}
	else if(request.getParameter("subheadId2") != null && !request.getParameter("subheadId2").equals("") && request.getParameter("email") != null && !request.getParameter("email").equals("")){%>
	<div><input type="checkbox" id="checkAll" />check All</div>
	<div class="list-details">
		<%
		SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
		customerapplicationListing CA_L = new customerapplicationListing();
		
		String subhead="";
		if(request.getParameter("subheadId2")!=null && !request.getParameter("subheadId2").equals("")){
			subhead=request.getParameter("subheadId2");
		}
		
		String emails="";
		if(request.getParameter("email")!=null && !request.getParameter("email").equals("")){
			emails=request.getParameter("email");
		}
		
		String fromDate="";
		String toDate = "";
		
		if(request.getParameter("fromDate2")!=null && !request.getParameter("fromDate2").equals("") && request.getParameter("toDate2")!=null && !request.getParameter("toDate2").equals("")){
			fromDate=request.getParameter("fromDate2");
			fromDate=dateFormat4.format(dateFormat2.parse(fromDate));
			
			toDate=request.getParameter("toDate2");
			toDate=dateFormat4.format(dateFormat2.parse(toDate));
		}
		else if(request.getParameter("splDay") != null && !request.getParameter("splDay").equals(""))
		{
			fromDate=request.getParameter("splDay");
		}
		
		List email_list=CA_L.getNADLTAhavingEmail(subhead,emails,fromDate,toDate);
		DecimalFormat df = new DecimalFormat("0.00");
		if(email_list.size()>0){%>
		<div class="printable">
			<div class="label">
			 	<%
 	customerpurchasesListing SALE_L = new customerpurchasesListing();
 	List CUS_DETAIL=null;
	for(int i=0; i < email_list.size(); i++ ){ 
			CAP=(customerapplication)email_list.get(i);				
			CUS_DETAIL=SALE_L.getBillDetails(CAP.getbillingId());
			if(CUS_DETAIL.size()>0)
			{
				SALE=(customerpurchases)CUS_DETAIL.get(0);}%>
				
				   <div class="label-print" id="readyToPrint<%=SALE.getbillingId()%>">
					<input type="checkbox"  value="<%=SALE.getbillingId()%>"></input>
					<table width="100%" cellpadding="0" cellspacing="0" style="margin-top:0px;">
						<tr><td style="font-size:14px;"><%=CAP.getphone() %></td>
                                    <td align="right" style="padding-right:30px;font-size:14px;"><span><%if(CAP.getextra1() != null && !CAP.getextra1().trim().equals("") && CAP.getextra1().trim().length()>=4){ %><%=userDateFormat.format(extra1DateFormat.parse(CAP.getextra1()))%><%}else{ %><%=SALE.getextra7() %><%} %></span></td></tr>
						<tr><td colspan="2" style="font-weight:bold;font-size:14px; width:366px; overflow:hidden;" ><strong><%if(SALE.getcustomername().trim().equals("")){%>&nbsp;<%}else if(SALE.getcustomername().length() > 33){%><%=SALE.getcustomername().substring(0, 33) %><%} else{%><%=SALE.getcustomername() %><%} %></strong></td></tr>
						<tr><td colspan="2" style="font-size:12px;"><%if(SALE.getextra6().trim().equals("")){%>&nbsp;<%}else if(SALE.getextra6().length() > 38){%><%=SALE.getextra6().substring(0, 38) %><%} else{%><%=SALE.getextra6() %><%} %></td></tr>
						<tr><td colspan="2" style="font-size:12px;"><%if(CAP.getextra2().trim().equals("")){%>&nbsp;<%}else if(CAP.getextra2().length() > 38){%><%=CAP.getextra2().substring(0, 38) %><%} else{%><%=CAP.getextra2()%><%} %></td></tr>
						<tr><td colspan="2" style="font-size:12px;"><%if(CAP.getaddress2().trim().equals("")){%>&nbsp;<%}else if(CAP.getaddress2().length() > 38){%><%=CAP.getaddress2().substring(0, 38) %><%} else{%><%=CAP.getaddress2()%><%} %></td></tr>
						<tr><td colspan="2" style="font-size:12px;"><%=CAP.getextra3() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=CAP.getpincode()%></span></td></tr>
						<tr><td>&nbsp;</td></tr>
					</table>
					</div>
				 <%
				}}}
			%>
	</div>
	</div>
	</div>
<!-- main content -->
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>