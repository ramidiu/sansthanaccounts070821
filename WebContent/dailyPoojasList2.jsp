<%@page import="beans.customerpurchases"%>
<%@page import="beans.customerapplication"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}
	}

</style>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRO OFFICE DAILY POOJAS LIST | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
$(document).ready(function(){
	$( "#headID" ).change(function() {
		  $( "#searchForm" ).submit();
		});
	});
function productsearch(){
	  var data1=$('#productId').val();
	  var headID=null;
	  $.post('searchSubhead.jsp',{q:data1,hoa:headID,page :'Rec'},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;

	 $( "#productId" ).autocomplete({source: availableTags}); 
			});
}
</script>
<script type="text/javascript">
// When the document is ready, initialize the link so
// that when it is clicked, the printable area of the
// page will print.
$(
    function(){
			// Hook up the print link.
        $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                function(){
                    // Print the DIV.
                    $( "a" ).css("text-decoration","none");
                    $( ".printable" ).print();
                   

                    // Cancel click event.
                    return( false );
                });
			});
$(document).ready(function () {
    $("#btnExport").click(function () {
        $("#tblExport").btechco_excelexport({
            containerid: "tblExport"
           , datatype: $datatype.Table
        });
    });
});
</script>
  <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:96%; 
    background:#d0e3fb; 
}
</style>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="CAP" class="beans.customerapplication"/>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-list">
		<div class="arrow-down">
				<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<!-- <form action="dailyPoojasList.jsp" method="post">  	
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
				</form> -->
				<form name="searchForm" id="searchForm" action="dailyPoojasList2.jsp" method="post">
				<div class="search-list">
				<input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
				<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
					<ul>
						<li><input type="text" name="productId" id="productId" value="" onkeyup="productsearch()" placeHolder="Find a seva here" autocomplete="off"/></li>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String fromDateForWeekOrMonth = "";
						String toDateForWeekOrMonth = "";	
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals(""))
						{
							 fromdate=request.getParameter("fromDate");
							 todate=request.getParameter("toDate");
						}
						%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly/></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li> 
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div style="padding-top:140px"></div>
				<form action="dailyPoojasList2.jsp" method="post">
				<div style="clear:both"></div>
				 <div class="srch">
				<ul style="list-style:none;">
				<li style="float:left;padding-right:17px;" ><select name="nadlta" style="width:220px;">
					<option value="NAD">NAD</option>
					<option value="LTA">LTA</option>
					<option value="nadlta">NAD & LTA</option>
					</select>
				</li>
				<li style="float:left;" ><select name="splDay" style="width:220px;">
					<option value="">-- SELECT --</option>
					<option value="Guru poornima">Guru poornima</option>
					<option value="Dasara">Dasara</option>
					<option value="Sri Rama navami">Sri Rama navami</option>
					<option value="Datta jayanthi">Datta jayanthi</option>
					</select>
				</li>
				<li style="float:left;"><input type="submit" class="click" name="search" value="Search" style="margin-top:5px;margin-left:15px;"></input></li></ul> </div>
				</form> 
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
					
				</div>
				<div class="clear"></div>
				<div class="list-details">
				<table width="100%" cellpadding="10" cellspacing="10" >
					<%if(request.getParameter("splDay") != null && !request.getParameter("splDay").equals("")){%>
						<tr><td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;"> (Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;"><%=request.getParameter("splDay").toUpperCase()%> DEVOTEES LIST</td></tr>
						<%} else if((request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")) || (request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")) || (request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report"))){ %>
						<tr><td colspan="9" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO-OFFICE</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;"> (Regd.No.646/92),Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="9" align="center" style="font-weight: bold;">DAILY PERFORM POOJAS DEVOTEES LIST</td></tr>
					<%} %> 
				</table>
				</div>
				<%-- <%if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){ %> --%>
				<%if((request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")) || (request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")) || (request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report"))){ %>
				<div class="list-details ">
				  <div class="printable">
				  <table width="100%"  cellpadding="1" cellspacing="1" class="print_table">
						<tr>
							<td class="bg" width="10%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="45%" style="font-weight: bold;">SEVA PERFORM IN THE NAME OF</td>
							<td class="bg" width="25%" style="font-weight: bold;">GOTHRAM</td>
							<td class="bg" width="20%" style="font-weight: bold;">APP REF.NO</td>
						</tr>
						</table>
				<%
				SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
				
				customerpurchasesListing SALE_L = new customerpurchasesListing();
				customerapplicationListing CAPP_L = new customerapplicationListing();
				banktransactionsListing BKTRAL=new banktransactionsListing();
				productsListing PRDL=new productsListing();
				employeesListing EMPL=new employeesListing();
				
				List BANK_DEP=null;
				double grandtotal =0.00;
				Calendar c1 = Calendar.getInstance(); 
				TimeZone tz = TimeZone.getTimeZone("IST");
				DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
				DateFormat dateFormat3= new SimpleDateFormat("dd-MMM-yyyy");
				DateFormat dateFormat4= new SimpleDateFormat("MM-dd");
				 DateFormat userDateFormat=new SimpleDateFormat("MMMM  dd");
				dateFormat.setTimeZone(tz.getTimeZone("IST"));
				
				String currentDate=(dateFormat2.format(c1.getTime())).toString();
				String fromDate2=(dateFormat2.format(c1.getTime())).toString();
				String toDate2=currentDate+" 23:59:59";
				String fromDate=currentDate;
				String toDate=currentDate+" 23:59:59";
				toDate=dateFormat4.format(dateFormat.parse(toDate));
				if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
					/* c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
					fromDate=currentDate+" 00:00:00";
					fromDate=dateFormat4.format(dateFormat.parse(fromDate));
					c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
					toDate=(dateFormat2.format(c1.getTime())).toString();
					toDate=toDate+" 23:59:59";
					toDate=dateFormat4.format(dateFormat.parse(toDate));
					fromDate=dateFormat4.format(dateFormat.parse(fromDate)); */
					
					c1.set(c1.DAY_OF_WEEK, c1.SUNDAY);
					fromDateForWeekOrMonth = dateFormat2.format(c1.getTime());
					fromDate2=dateFormat.format(c1.getTime()); 
					fromDate=dateFormat4.format(c1.getTime()); 
					
					
					Calendar c2=Calendar.getInstance();
					c2.set(c2.DAY_OF_WEEK, c2.SATURDAY);
					toDateForWeekOrMonth = dateFormat2.format(c2.getTime());
					toDate=dateFormat4.format(c2.getTime());
					toDate2=dateFormat.format(c2.getTime()); 
					
					
				}
				else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
					/* c1.getActualMaximum(Calendar.DAY_OF_MONTH);
					int lastday = c1.getActualMaximum(Calendar.DATE);
					c1.set(Calendar.DATE, lastday);  
					toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
					toDate=dateFormat4.format(dateFormat.parse(toDate));
					c1.getActualMinimum(Calendar.DAY_OF_MONTH);
					int firstday = c1.getActualMinimum(Calendar.DATE);
					c1.set(Calendar.DATE, firstday); 
					fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
					fromDate=dateFormat4.format(dateFormat.parse(fromDate)); */
					
					
					c1.set(c1.DAY_OF_MONTH, c1.getActualMinimum(Calendar.DAY_OF_MONTH));
					fromDateForWeekOrMonth = dateFormat2.format(c1.getTime());
					fromDate=dateFormat4.format(c1.getTime());
					fromDate2=dateFormat.format(c1.getTime()); 
					
					Calendar c2 = Calendar.getInstance();
					c2.set(Calendar.DAY_OF_MONTH, c2.getActualMaximum(Calendar.DAY_OF_MONTH));
					toDateForWeekOrMonth = dateFormat2.format(c2.getTime());
					toDate=dateFormat4.format(c2.getTime());
					toDate2=dateFormat.format(c2.getTime()); 

				}
				else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
					if(request.getParameter("fromDate")!=null){
						fromDate=request.getParameter("fromDate")+" 00:00:01";
						fromDate2=request.getParameter("fromDate")+" 00:00:01";
						fromDate=dateFormat4.format(dateFormat2.parse(fromDate));
						
					}
					if(request.getParameter("toDate")!=null){
						toDate=request.getParameter("toDate")+" 23:59:59";
						toDate=dateFormat4.format(dateFormat2.parse(toDate));
						toDate2=request.getParameter("toDate")+" 23:59:59";
					}
				}
				String productCode[]=new String[3];
				if(request.getParameter("productId")!=null && !request.getParameter("productId").equals(""))
				{
					productCode=request.getParameter("productId").split(" ");
				}
				String hoaID="4";
				customerapplicationListing CUSAL=new customerapplicationListing();
				List SAL_list=null;
				List CUS_DETAIL=null;
				DecimalFormat df = new DecimalFormat("0.00");
	//String poojaIds[]={"20063","20064","20066","20067","20070","20071","20091","20092"};
	String poojaIds[];
	if(request.getParameter("productId")!=null && !request.getParameter("productId").equals("")){
		productCode=request.getParameter("productId").split(" ");
		poojaIds=new String[]{productCode[0]};
	}else{
		/* poojaIds=new String[]{"20063","20064","20066","20067","20070","20071","20091","20092"}; */
		poojaIds=new String[]{"20092","20091"};
	}%>
	 <div id="tblExport">
	<% 
	if(poojaIds.length>0){
		for(int s=0;s<poojaIds.length;s++){
			
			if(poojaIds[s].equals("20092") || poojaIds[s].equals("20063")){
				if(poojaIds[s].equals("20092"))
				{
					SAL_list=CUSAL.getMcustomerDetailsBasedOnBillingId("RCT237482");
					List newList = CUSAL.GetDetailsCustomerPerformedDate(fromDate,toDate,poojaIds[s]);
					SAL_list.addAll(newList);
					
				}	
				else{
					SAL_list=CUSAL.GetDetailsCustomerPerformedDate(fromDate,toDate,poojaIds[s]);
				}
		
		//System.out.println("array"+poojaIds[s]);
		//System.out.println("11111");
		
	if(SAL_list.size()>0){
	//System.out.println("222222");%>
			<table width="100%" cellpadding="0" cellspacing="0" class="print_table">
						
						<tr>
						<td colspan="9" style="padding:0px;margin:0px;" class="yourID" >
						
						</td></tr>
						<tr>
						<%if(fromDateForWeekOrMonth != "" && toDateForWeekOrMonth != "") { %>
							<td class="bg" colspan="9" align="center" style="font-size: 16px;color: red;font-weight: bold;"><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%> FOR THE DAY OF <%=dateFormat3.format(dateFormat2.parse(fromDateForWeekOrMonth))%> to <%=dateFormat3.format(dateFormat2.parse(toDateForWeekOrMonth))%></td><%}else{ %>
							<td class="bg" colspan="9" align="center" style="font-size: 16px;color: red;font-weight: bold;"><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%> FOR THE DAY OF <%=dateFormat3.format(dateFormat2.parse(request.getParameter("fromDate")))%> to <%=dateFormat3.format(dateFormat2.parse(request.getParameter("toDate")))%></td><%} %>
						</tr>
						<%double totalamount=0.00;
						for(int i=0; i < SAL_list.size(); i++ )
							{ 
							 CAP=(customerapplication)SAL_list.get(i); 
							 CUS_DETAIL=SALE_L.getBillDetails(CAP.getbillingId());
								if(CUS_DETAIL.size()>0)
								{
									SALE=(customerpurchases)CUS_DETAIL.get(0);}%>
							
						    <tr>
						    <td  colspan="9">
						    <table width="100%"  cellpadding="1" cellspacing="1" class="print_table">
								<tr style="position: relative;" >
									<td width="10%" ><%=i+1%></td>
									<td  width="45%" ><%=CAP.getlast_name() %></td>
									<td  width="25%"  ><%=CAP.getgothram()%></td>
									<td width="20%"  ><%=CAP.getphone()%></td>
								</tr>
							</table>
							</td></tr>
							
			<%} %>
					
					</table>
					<%}}
			else if(poojaIds[s].equals("20091"))
			{
				List ADDTOSAL_list2 = null;
				List SAL_list2=SALE_L.GetDetailsCustomerBasedOnDates(fromDate2,toDate2,poojaIds[s]);
				ADDTOSAL_list2=SALE_L.getOnlineBookingsBasedOnDates(fromDate2,toDate2,poojaIds[s]); //added by srinivas
				if(ADDTOSAL_list2 != null && ADDTOSAL_list2.size() > 0)
				{
					SAL_list2.addAll(ADDTOSAL_list2);
				}
				//System.out.println("array"+poojaIds[s]);
				//System.out.println("11111");
				
			if(SAL_list2.size()>0){
			//System.out.println("222222");%>
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport" class="print_table">
								
								<tr>
								<td colspan="9" style="padding:0px;margin:0px;" class="yourID" >
								
								</td></tr>
								<tr>
								<%if(fromDateForWeekOrMonth != "" && toDateForWeekOrMonth != "") { %>
									<td class="bg" colspan="9" align="center" style="font-size: 16px;color: red;font-weight: bold;"><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%> FOR THE DAY OF <%=dateFormat3.format(dateFormat2.parse(fromDateForWeekOrMonth))%> to <%=dateFormat3.format(dateFormat2.parse(toDateForWeekOrMonth))%></td><%}else{ %>
									<td class="bg" colspan="9" align="center" style="font-size: 16px;color: red;font-weight: bold;"><%=PRDL.getProductsNameByCats(poojaIds[s], hoaID)%> FOR THE DAY OF <%=dateFormat3.format(dateFormat2.parse(request.getParameter("fromDate")))%> to <%=dateFormat3.format(dateFormat2.parse(request.getParameter("toDate")))%></td><%} %>
								</tr>
								<%double totalamount=0.00;
								for(int i=0; i < SAL_list2.size(); i++ )
									{ 
									SALE=(customerpurchases)SAL_list2.get(i); 
									 List CUS_DETAIL2=SALE_L.getBillDetails(SALE.getbillingId());
										if(CUS_DETAIL2.size()>0)
										{
											SALE=(customerpurchases)CUS_DETAIL2.get(0);}%>
									
								    <tr>
								    <td  colspan="9">
								    <table width="100%"  cellpadding="1" cellspacing="1" class="print_table">
										<tr style="position: relative;" >
											<td width="10%" ><%=i+1%></td>
											<%
												if(SALE.getcash_type().equals("online success"))
												{
													String perfName = "";
													
													List capp = CAPP_L.getMcustomerDetailsBasedOnBillingId(SALE.getbillingId());
													if(capp.size() > 0)
													{
														customerapplication cappObj = (customerapplication)capp.get(0);
														perfName = cappObj.getlast_name();
													}
													%>
													<td  width="45%" ><%=perfName%></td>
													<% 
												}
												else{
											%>
											<td  width="45%" ><%=SALE.getextra2()%></td><%} %>
											<td  width="25%"  ><%=SALE.getgothram()%></td>
											<td width="20%" ><%=SALE.getbillingId()%></td>
										</tr>
									</table>
									</td></tr>
									
					<%} %>
							
							</table>
							<%}
			}
	} %>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no list found for today!</span></div>
					</div>
					<%}%> 
					</div>
					</div>
					 <%} %> 
	<%if(request.getParameter("splDay") != null && !request.getParameter("splDay").equals(""))
	{	
		String splDay = request.getParameter("splDay");
		mainClasses.customerapplicationListing CAL = new mainClasses.customerapplicationListing();	
		mainClasses.customerpurchasesListing CPL = new mainClasses.customerpurchasesListing();	
		
		DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dateFormat3= new SimpleDateFormat("dd-MMM-yyyy");
		
		
		List CA_List = null;
		List CP_List = null;
		
		CA_List=CAL.getNADLTABasedOnSpecialDayNew(splDay,request.getParameter("nadlta"));%>
		<div id="tblExport">
		<div class="list-details ">
		<div class="printable">
		<table width="95%"  cellpadding="1" cellspacing="1" class="print_table" style="margin: 0px auto;">
			<tr>
				<td class="bg" width="10%" style="font-weight: bold;">S.NO.</td>
				<td class="bg" width="45%" style="font-weight: bold;">SEVA PERFORM IN THE NAME OF</td>
				<td class="bg" width="25%" style="font-weight: bold;">GOTHRAM</td>
				<td class="bg" width="20%" style="font-weight: bold;">APP REF.NO</td>
			</tr>
			<tr>
				<td class="bg" colspan="9" align="center" style="font-size: 16px;color: red;font-weight: bold;"><%=splDay.toUpperCase()%> DEVOTEES LIST</td>
			</tr>
		</table>
		<% 
		int k = 0;

			for(int i=0; i < CA_List.size(); i++ )
			{
				customerapplication CAPPOBJ=(beans.customerapplication)CA_List.get(i);
				CP_List=CPL.getBillDetails(CAPPOBJ.getbillingId().toString());
				customerpurchases CPOBJ = (customerpurchases)CP_List.get(0);			
					k+=1;
					%>
					<tr>
					    <td  colspan="9">
					    <table width="95%"  cellpadding="1" cellspacing="1" class="print_table" style="margin: 0px auto;">
					<tr style="position: relative;">
					 	<td width="10%" ><%=k%></td>
						<td  width="45%" ><%=CAPPOBJ.getlast_name()%></td>
						<td  width="25%"  ><%=CPOBJ.getgothram()%></td>
						<td width="20%" ><%=CAPPOBJ.getphone()%></td>
					</tr>	
					</table>
					</td></tr>
			<%}	%>
			</div>
			<% 
	}
	%>
	</div>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
<%}else{response.sendRedirect("index.jsp");} %> 
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>