<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="beans.godwanstock"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<title>Delivery Challan List</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
.search-list {
    float: left;
    margin: 36px 0 61px;
}
</style>
</head>
<script>
$(function() {
/* 	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	}); */
	
	var d = $('#param').val();
	if ($.trim(d) === "")	{
		$('#form').submit();
	}
});
</script>
<script type="text/javascript">
function myFunction()	{
	var count = $('#count').val();
	var  vendor = $('#vendor').val();
	if (vendor !== null && $.trim(vendor) !== "")	{
//		$('#dcNumber').val(vendor.split(",")[1]);
//		console.log($('#dcNumber').val());
	}
	
	for (var i = 1 ; i <= count ; i ++)	{
		var dc = $('#dc'+i).val();
		if ($.trim(vendor) !== "" && vendor != null)	{
			if (dc !== vendor.split(",")[1])	{
				$('#dd'+i).hide();
				$('#tt'+i).hide();
				$('#tp'+i).hide();
			}
			else	{
				$('#dd'+i).show();
				$('#tt'+i).show();
				$('#tp'+i).show();
			}
		} 
	}
}
function checkStockAppStatus()	{
	var dcNumber = document.getElementById('vendor').value;
	if (dcNumber.trim() !== "")	{
		$.ajax({
			url : "searchVendor.jsp",
			type : "POST",
			data : {dcNo : dcNumber.split(",")[1]},
			success : function (response)	{
				if ($.trim(response) !== "")	{
					alert($.trim(response));
					return false;
				}
				else	{
					$.ajax({
						url : "searchVendor.jsp",
						type : "POST",
						data : {dcNo : dcNumber.split(",")[1],key : "approve"},
						success : function(response)	{
							if (Number(response) >= 1)	{
								window.location.replace('<%=request.getContextPath()%>/directChallanList.jsp');
							}
							else	{
								alert("Bill can't be Generated");
							}
						}
					});
				}
			}
		});
	}
}
</script>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body onload="myFunction()">
<%if(session.getAttribute("empId") != null){ 
String value = "";
	if (request.getParameter("id") != null)	{
		value = request.getParameter("id");
}%>
<input type="hidden" id="param" name="param" value="<%=value%>">
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<form action="DCList" method="POST" id="form">
<%-- <div class="search-list">
<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals(""))
						{
							 fromdate=request.getParameter("fromDate");
							 todate=request.getParameter("toDate");
						}
						%>
 <ul>
	<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly/></li>
	<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li> 
	<li><input type="submit" class="click" name="search" value="Search"></input></li>
</ul>
</div> --%>
</form>
<%
if (request.getAttribute("dclist") != null)	{
//	SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");	
	DecimalFormat df = new DecimalFormat("#,###,##0.00");
	Map<String,List<godwanstock>> gdmap = (Map<String,List<godwanstock>>)request.getAttribute("dclist");
	Set<String> keys = (Set<String>)request.getAttribute("ids");
	Map<String,String> vendors = (Map<String,String>)request.getAttribute("vendors");
	Map<String,Double> amounts = (Map<String,Double>)request.getAttribute("amts");
	Iterator<String> itr = keys.iterator();
	Set<String> vds = vendors.keySet();
	Iterator<String> itr1 = vds.iterator();
	if (vendors.size() > 0)	{%>
	<select style="margin: 35px 45px 0;" id="vendor" onchange="myFunction()">
	<%while(itr1.hasNext())	{
		String data = vendors.get(itr1.next());%>
		<option value="<%=data.split(",")[2]+","+data.split(",")[1]%>"><%=data.split(",")[0] %></option>
	<%}%>
	</select>
	<%}else	{%>
	<h2 style="color:red;text-align: center;font-family: monospace;">No Delivery challan Bills</h2>
	<%}%>
	<!-- <input type="hidden" name="dcNumber" id="dcNumber"> -->
	<div>
	<table class="table table-bordered" style="width: 90%; margin: 0 auto;">
	<%int count = 0;		
	double amount = 0;
//	String dcNumber = "";
	while (itr.hasNext())	{
		String dc = itr.next();
		List<godwanstock> lt = gdmap.get(dc);
		Iterator<godwanstock> gdItr = lt.iterator();
		int count1 = 0;
		amount = amounts.get(dc);
		while (gdItr.hasNext())	{
		godwanstock gd = gdItr.next();
		count ++;
		count1 ++;
		%>
			<%-- <div style="display:block;" id="dd<%=count%>" title="<%=dc%>"> --%>
			<input type="hidden" name="dc" id="dc<%=count%>" value="<%=dc%>">
			<!-- <table class="table table-bordered" style="width: 80%; margin: 0 auto;"> -->
			<%if (count == 1)	{%>
			<tr class="header">
			<th>S.no</th>
			<th>DCNumber</th>
			<th>MseId</th>
			<th>Date</th>
			<th>Product</th>
			<th>MajorHead</th>
			<th>MinorHead</th>
			<th>SubHead</th>
			<th>Price</th>
			</tr>
			<%}%>
			<tr id="dd<%=count%>">
			<td style="font-size: 11px;font-weight: bold;"><%=count1%></td>
			<td style="font-size: 11px;font-weight: bold;"><%=gd.getExtra25()%></td>
			<td style="font-size: 11px;font-weight: bold;"><%=gd.getExtra1()%></td>
			<td style="font-size: 11px;font-weight: bold;"><%=gd.getDate()%></td>
			<td style="font-size: 11px;font-weight: bold;"><%=gd.getExtra29()%></td>
			<td style="font-size: 11px;font-weight: bold;"><%=gd.getExtra27()%></td>
			<td style="font-size: 11px;font-weight: bold;"><%=gd.getExtra26()%></td>
			<td style="font-size: 11px;font-weight: bold;"><%=gd.getExtra28()%></td>
			<td style="font-size: 11px;font-weight: bold;"><%=gd.getExtra24()%></td>
			</tr>
			<!-- </table> -->
			<!-- </div> -->
			<%if (!gdItr.hasNext()) {%>
			<tr id="tt<%=count%>">
				<td colspan="8" align="right" style="font-size: 11px;font-weight: bold;">TOTAL AMOUNT::&nbsp;&nbsp;&nbsp;</td>
				<td><span id="totalAmount" style="font-size: 11px;font-weight: bold;"><%=df.format(amount)%></span></td>
			</tr>
			<tr id="tp<%=count%>">
				<td colspan="9" align="center"><button class="click" onclick="checkStockAppStatus()">Generate Bill</button></td>
			</tr>
			<%}
		}
	}%>
	</table>
	</div>
<input type="hidden" id="count" value="<%=count%>">
<%}%>
</div>
<%}
else	{response.sendRedirect("index.jsp");}
%>
</body>
</html>