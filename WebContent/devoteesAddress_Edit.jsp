<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="mainClasses.customerapplicationListing"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DEVOTEES REGISTRATION LIST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(function() {
	  $( ".datepickernew" ).datepicker({ 
		  yearRange: '1940:2020',
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
		  });
	}); 
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
<style>
	.margi-rht{    margin-right: 10px;}
</style>
</head>
<body>
<jsp:useBean id="CUS" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="CAP" class="beans.customerapplication"></jsp:useBean>
<div><%@ include file="title-bar.jsp"%></div>
<div>
<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-title" style="padding:30px 30px 50px 20px;">DAILY PERFORM POOJAS CUSTOMERS LIST</div>
        <div class="vendor-list">
        <%DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat  dateFormat1= new SimpleDateFormat("yyyy-MM-dd");
        DateFormat  dateFormat2= new SimpleDateFormat("MMMM-dd");
        DateFormat  dbdate1= new SimpleDateFormat("MM-dd");
        String bid=request.getParameter("bid");
        customerpurchasesListing CUPL=new customerpurchasesListing();
        customerapplicationListing CAPL=new customerapplicationListing();
        List CP_DET=CUPL.getBillDetails(bid);
        List CAP_DET=null;
        if(CP_DET.size()>0){
        	CUS=(beans.customerpurchases)CP_DET.get(0);
        	CAP_DET=CAPL.getMcustomerDetailsBasedOnBillingId(bid);
        	if(CAP_DET.size()>0){
        		CAP=(beans.customerapplication)CAP_DET.get(0);
        	}
        }
        %>
        <form action="devoteesAddress_update.jsp" method="post">
        	<table width="60%" cellpadding="0" cellspacing="0" >
        	<tr>
        	<td>Application Ref No</td>
        	<td>Name</td>
        	<td>Gothram</td>
        	<td>Performed in the Name of</td>
        	</tr>
        	 <tr><td colspan="3" height="10"></td></tr>
        	<tr>
        	<td  style="vertical-align:top;"><input type="text" name="AppRefNo" class="margi-rht" value="<%=CAP.getphone()%>"></input></td>
        	<td  style="vertical-align:top;"><input type="text" name="customerName" class="margi-rht" value="<%=CUS.getcustomername() %>" onblur="javascript:this.value=this.value.toUpperCase();"></input></td>
        	<td  style="vertical-align:top;"><input type="text" name="gothram" class="margi-rht" value="<%=CUS.getgothram() %>" onblur="javascript:this.value=this.value.toUpperCase();"></input></td>
        	<td style="vertical-align:top;"><input type="text" name="lastname" class="margi-rht" value="<%=CAP.getlast_name() %>" onBlur="javascript:this.value=this.value.toUpperCase();"></td>
        	</tr>
        	 <tr><td colspan="3" height="10"></td></tr>
            <tr><td height="10"></td></tr>
            <tr><td height="10"></td></tr>
        	<tr>
        	<td>Address1</td>
        	<td>Address2</td>
        	<td>Address3</td>
        	<td>City</td>
        	<td>Postal Code</td>
        	<td></td>
        	</tr>
            <tr><td colspan="3" height="10"></td></tr>
           
        	<tr>
        	<td><textarea  name="address" id="address" class="margi-rht" onblur="javascript:this.value=this.value.toUpperCase();" style="width:180px; height:50px;font-size: 14px;font-family: arial;"><%=CUS.getextra6() %></textarea></td>
        	<td><textarea  name="address2" id="address2" class="margi-rht"  onblur="javascript:this.value=this.value.toUpperCase();" style="width:180px; height:50px;font-size: 14px;font-family: arial;"><%=CAP.getextra2() %></textarea></td>
        	<td><textarea  name="address3" id="address3" class="margi-rht" onblur="javascript:this.value=this.value.toUpperCase();" style="width:180px; height:50px;font-size: 14px;font-family: arial;"><%=CAP.getaddress2() %></textarea></td>
			<td style="vertical-align: top;"><input type="text" name="city" class="margi-rht" value="<%=CAP.getextra3()%>"></td>
			<td style="vertical-align: top;"> <input type="text" name="pincode" class="margi-rht" value="<%=CAP.getpincode()%>"></td>
        	</tr>
        	<tr><td colspan="3" height="10"></td></tr>
            <tr><td height="10"></td></tr>
            <tr><td height="10"></td></tr>
        	<tr>
        	<td>Receipt No</td>
        	<td>InVoice Date</td>
        	<%--  <%if(CAP.getextra1() != null && !CAP.getextra1().equals("")){ %>
        	<td>Performed Date</td>
        	<%}else if(CUS.getextra7() !=null && !CUS.getextra7().equals("")){ %>
        	<td>Special Days</td>
        	<%}else{ %>
        	<td>Performed Date</td>
        	<%} %> --%>
        	<td>Performed Date</td>
        	<td>Special Days</td>
        	<td>Phone Number</td>
        	<td>Email Id</td>
        	</tr>
        	 <tr><td colspan="3" height="10"></td></tr>
            <tr>
        	<input type="hidden" class="margi-rht" name="bid" value="<%=bid%>">
        	<td  style="vertical-align:top;"><input type="text" name="billingId" class="margi-rht" value="<%=CAP.getbillingId()%>" readonly></input></td>
        	<%-- <td  style="vertical-align:top;"><input type="text" name="date" class="margi-rht" value="<%=dateFormat1.format(dateFormat.parse(CUS.getdate()))%>" readonly></input></td> --%>
        	<td  style="vertical-align:top;"><input type="text" name="date" class="margi-rht" value="<%=CUS.getdate()%>" readonly></input></td>
        	<%-- <%if(CAP.getextra1() != null && !CAP.getextra1().equals("")){ %> --%>
        	<%if(CAP.getextra1() != null && !CAP.getextra1().trim().equals("") && !CAP.getextra1().trim().equals("-")){ %>
        	<%
        	//String Performeddate = dateFormat2.format(dbdate1.parse(CAP.getextra1()));
        	String Performeddate = CAP.getextra1();
        	//System.out.println("perform"+Performeddate);
        	String[] tokens = Performeddate.split("-");
        	%>
        	<td>
				<select name="performedMonth" style="width: 85px;">
					<%-- <option value="<%=tokens[0]%>"><%=tokens[0]%></option>
					<option value="01">JAN</option>
					<option value="02">FEB</option>
					<option value="03">MAR</option>
					<option value="04">APR</option>
					<option value="05">MAY</option>
					<option value="06">JUN</option>
					<option value="07">JUL</option>
					<option value="08">AUG</option>
					<option value="09">SEP</option>
					<option value="10">OCT</option>
					<option value="11">NOV</option>
					<option value="12">DEC</option> --%>
					<option value="">-- SELECT --</option>
					<option value="01" <% if(tokens[0].equals("01")){%>selected<% }%>>JAN</option>
					<option value="02" <% if(tokens[0].equals("02")){%>selected<% }%>>FEB</option>
					<option value="03" <% if(tokens[0].equals("03")){%>selected<% }%>>MAR</option>
					<option value="04" <% if(tokens[0].equals("04")){%>selected<% }%>>APR</option>
					<option value="05" <% if(tokens[0].equals("05")){%>selected<% }%>>MAY</option>
					<option value="06" <% if(tokens[0].equals("06")){%>selected<% }%>>JUN</option>
					<option value="07" <% if(tokens[0].equals("07")){%>selected<% }%>>JUL</option>
					<option value="08" <% if(tokens[0].equals("08")){%>selected<% }%>>AUG</option>
					<option value="09" <% if(tokens[0].equals("09")){%>selected<% }%>>SEP</option>
					<option value="10" <% if(tokens[0].equals("10")){%>selected<% }%>>OCT</option>
					<option value="11" <% if(tokens[0].equals("11")){%>selected<% }%>>NOV</option>
					<option value="12" <% if(tokens[0].equals("12")){%>selected<% }%>>DEC</option>
				</select>
				<select name="performedDay" style="width: 85px;">
					<option value="<%=tokens[1]%>"><%=tokens[1]%></option>
						<%for(int i=01;i<32;i++){ 
							if(i < 10){%>
					<option value="0<%=i%>">0<%=i%></option>
							<%}else{ %>
					<option value="<%=i%>"><%=i%></option>
						<%} }%>
					<option value="">-- SELECT --</option>	
				</select>
			</td>

			<%}else{ %>
			<td >
			<select name="performedMonth1" style="width: 85px;">
				<option value="">Month</option>
				<option value="01">JAN</option>
				<option value="02">FEB</option>
				<option value="03">MAR</option>
				<option value="04">APR</option>
				<option value="05">MAY</option>
				<option value="06">JUN</option>
				<option value="07">JUL</option>
				<option value="08">AUG</option>
				<option value="09">SEP</option>
				<option value="10">OCT</option>
				<option value="11">NOV</option>
				<option value="12">DEC</option>
			</select>
			<select name="performedDay1" style="width: 85px;">
				<option value="">Day</option>
					<%for(int i=01;i<32;i++){ 
						if(i < 10){%>
				<option value="0<%=i%>">0<%=i%></option>
						<%}else{ %>
				<option value="<%=i%>"><%=i%></option>
					<%} }%>
			</select>
			</td>
			<%} %>
			
			
			<td>
			<input type="checkbox" name="specialDays" value="Guru poornima"<%if(CUS.getextra7().contains("Guru poornima")){ %>checked<%} %>>Guru poornima
			<input type="checkbox" name="specialDays" value="Dasara"<%if(CUS.getextra7().contains("Dasara")){ %>checked<%} %>>Dasara
			<input type="checkbox" name="specialDays" value="Sri Rama navami"<%if(CUS.getextra7().contains("Sri Rama navami")){ %>checked<%} %>>Sri Rama navami<br>
			<input type="checkbox" name="specialDays" value="Datta jayanthi"<%if(CUS.getextra7().contains("Datta jayanthi")){ %>checked<%} %>>Datta jayanthi</td>
			
			<td style="vertical-align: top;"><input type="text" name="mobile" class="margi-rht" id="mobile" value="<%=CUS.getphoneno() %>"  onKeyPress="return numbersonly(this, event,true);" onblur="nameAddValidation();" placeholder="Enter mobile number" style="vertical-align:top;"></td>
        	<td  style="vertical-align:top;"><input type="text" name="emailid" class="emailid" value="<%=CAP.getemail_id() %>"></input></td>
        	</tr>
           <tr>
           <td>Amount</td>
        	<td>Date Of Birth</td>
        	<td>Date Of Anniversary</td>
        	<td>Land Line</td>
        	</tr>
        	<tr>
    <td  style="vertical-align:top;"><input type="text" name="amt" class="margi-rht" value="<%=CUS.gettotalAmmount()%>" readonly></input></td>    	
	<td><input type="text" name="dob"  class="datepickernew" value="<%=CAP.getdateof_birth()%>" readonly="readonly" style="width: 80px;">
	 </td>
	<%if(CAP.getmiddle_name() != null && !CAP.getmiddle_name().trim().equals("") && !CAP.getmiddle_name().trim().equals("-")){ %>
   	<%
   	String DOAdate = CAP.getmiddle_name();
   	String[] doatokens = DOAdate.split("-");
   	%>
   	<td>
				<select name="doamonth" style="width: 85px;">
					<option value="">-- SELECT --</option>
					<option value="01" <% if(doatokens[0].equals("01")){%>selected<% }%>>JAN</option>
					<option value="02" <% if(doatokens[0].equals("02")){%>selected<% }%>>FEB</option>
					<option value="03" <% if(doatokens[0].equals("03")){%>selected<% }%>>MAR</option>
					<option value="04" <% if(doatokens[0].equals("04")){%>selected<% }%>>APR</option>
					<option value="05" <% if(doatokens[0].equals("05")){%>selected<% }%>>MAY</option>
					<option value="06" <% if(doatokens[0].equals("06")){%>selected<% }%>>JUN</option>
					<option value="07" <% if(doatokens[0].equals("07")){%>selected<% }%>>JUL</option>
					<option value="08" <% if(doatokens[0].equals("08")){%>selected<% }%>>AUG</option>
					<option value="09" <% if(doatokens[0].equals("09")){%>selected<% }%>>SEP</option>
					<option value="10" <% if(doatokens[0].equals("10")){%>selected<% }%>>OCT</option>
					<option value="11" <% if(doatokens[0].equals("11")){%>selected<% }%>>NOV</option>
					<option value="12" <% if(doatokens[0].equals("12")){%>selected<% }%>>DEC</option>
				</select>
				<select name="doadate" style="width: 85px;">
					<option value="<%=doatokens[1]%>"><%=doatokens[1]%></option>
						<%for(int i=01;i<32;i++){ 
							if(i < 10){%>
					<option value="0<%=i%>">0<%=i%></option>
							<%}else{ %>
					<option value="<%=i%>"><%=i%></option>
						<%} }%>
					<option value="">-- SELECT --</option>	
				</select>
			</td>

			<%}else{ %>
	<td width="30">
	<select name="doamonth1" style="width: 85px;">
	<option value="">Month</option>
	<option value="01">JAN</option>
	<option value="02">FEB</option>
	<option value="03">MAR</option>
	<option value="04">APR</option>
	<option value="05">MAY</option>
	<option value="06">JUN</option>
	<option value="07">JUL</option>
	<option value="08">AUG</option>
	<option value="09">SEP</option>
	<option value="10">OCT</option>
	<option value="11">NOV</option>
	<option value="12">DEC</option>
	</select>
	<select name="doadate1" style="width: 85px;">
	<option value="">Day</option>
	<%for(int i=01;i<32;i++){ 
	if(i < 10){%>
	
	<option value="0<%=i%>">0<%=i%></option>
	<%}else{ %>
	<option value="<%=i%>"><%=i%></option>
	<%} }%>
	</select></td> <%} %>
    <td style="vertical-align: top;"><input type="text" name="landline" class="margi-rht" id="landline" value="<%=CUS.getExtra20()%>"  onKeyPress="return numbersonly(this, event,true);" placeholder="Enter Land Line" style="vertical-align:top;"></td>
     </tr>
           <tr><td height="10"></td></tr>
        	<tr>
        	<td colspan="3"><input type="submit" name="submit" value="UPDATE" class="click bordernone"></td>
        	</tr>
        	</table>
        
        
        </form>
       
        </div>
 </div>
</div>

<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>