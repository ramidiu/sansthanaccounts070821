<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>POOJA STORES GODOWN STOCK REPORT</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
	</script>
	<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<!-- main content -->
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="PRD" class="beans.products" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<div class="vendor-page">
		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<!-- <div class="search-list">
					<ul>
					<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value=""  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div> -->
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> 
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
	String currentDate=(chngDateFormat.format(c1.getTime())).toString();
	productsListing PRD_l=new productsListing ();
	headofaccountsListing HOAL=new headofaccountsListing();
	  List PRD_list=PRD_l.getMedicalStore(session.getAttribute("headAccountId").toString());
	  if(PRD_list.size()>0){%>
	  <div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td colspan="3" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%=HOAL.getHeadofAccountName(session.getAttribute("headAccountId").toString()) %></td></tr>
						<tr><td colspan="3" align="center" style="font-weight: bold;">Dilsukhnagar</td></tr>
						<tr><td colspan="1"></td><td colspan="1" align="right" style="font-weight: bold;">Godown Stock Report</td><td colspan="1" align="center">Date : <%=currentDate %></td></tr>
						<tr>
							<td class="bg" width="6%" align="center">Code</td>
							<td class="bg" width="23%">Product Name</td>
							<!-- <td class="bg" width="23%">Shop Quantity</td> -->
								<td class="bg" width="23%">Godown Quantity</td>
							<!-- <td class="bg" width="23%">Product Qty</td>
							<td class="bg" width="23%">Amount</td> -->
							</tr>
						<%for(int i=0; i < PRD_list.size(); i++ ){
							PRD=(beans.products)PRD_list.get(i); %>
						<tr>
							<td  align="center"><%=PRD.getproductId()%></td>
							<td><%=PRD.getproductName()%></td>
						<%-- <td><span><a href=""><%=PRD.getbalanceQuantityStore()%></a></span></td> --%>
						<td><span><%=PRD.getbalanceQuantityGodwan()%></span></td>
							<%-- <td><%=SALE.getquantity()%></td>
							<td> <%=SALE.gettotalAmmount()%></td> --%>
						</tr>
						<%} %>
					</table>
				</div>
					<%}else{%>
					<div align="center">
						<h1>Sorry,There are no products found in godown !..</h1>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
		<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>