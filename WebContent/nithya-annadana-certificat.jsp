<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nitya-Annadaanam Certificate</title>
</head>

<body style="margin:0;padding:0; background-repeat:no-repeat;float:left;" onload="window.print();" >
<jsp:useBean id="CSP" class="beans.customerpurchases"></jsp:useBean>
<jsp:useBean id="CAP" class="beans.customerapplication"></jsp:useBean>
<%
DateFormat  dateFormat= new SimpleDateFormat("dd-MM-yyyy");
DateFormat  dbdate= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat  dateFormat1= new SimpleDateFormat("MMM-dd");
DateFormat  dateFormat2= new SimpleDateFormat("MMMM-dd");
DateFormat  dbdate1= new SimpleDateFormat("MM-dd");
DecimalFormat df=new DecimalFormat("##.00");
mainClasses.customerpurchasesListing CUSPL=new mainClasses.customerpurchasesListing();
mainClasses.customerapplicationListing CAPL=new mainClasses.customerapplicationListing();
String billingId=request.getParameter("bid");
List TICDET=CUSPL.getBillDetails(billingId);
List CUS_AP=null;
if(TICDET.size()>0){ 
	CSP=(beans.customerpurchases)TICDET.get(0);
	 CUS_AP=CAPL.getMcustomerDetailsBasedOnBillingId(CSP.getbillingId()); 
	 if(CUS_AP.size()>0){
		 CAP=(beans.customerapplication)CUS_AP.get(0);
	 }
}
%>
<%if(request.getParameter("seva")!=null && request.getParameter("seva").equals("nitya-annadaanam")){ %>
<div style="width:952px;height:566px;">
	<div style="margin-top:328px; background:#C93">
        <div  style="width:auto;height:30px;padding-left:280px;font-size:14px;padding-bottom:0px;position:absolute;font-weight:bold; left:0;font-family: verdana;"><%=CSP.getcustomername() %></div>
        <div  style="width:auto;height:70px;padding-left:280px;font-size:12px;font-weight:bold;padding-top:0px; position:absolute; left:0px; top:355px;font-family: verdana;"><%=CSP.getextra6() %><br><%=CAP.getextra2() %><br><%=CAP.getaddress2()%><br><%=CAP.getextra3() %>-<%=CAP.getpincode() %></div>
	</div>
	<div  style="width:auto;height:30px;font-size:14px;font-weight:bold;padding-top:0px;  position:absolute; right:55px; top:325px;font-family: verdana;font-weight:bold; text-transform:uppercase;"><%if(CAP.getextra1()!=null && !CAP.getextra1().equals("")){  %> <%=dateFormat2.format(dbdate1.parse(CAP.getextra1())) %>  <%}else{ %><%=CSP.getextra7().replaceFirst(",","")%><%} %><span style="margin-left:20px;"></span></div>

<div  style="width:auto;height:auto;font-weight:bold;font-size:14px; background:#9FF; position:absolute; left:210px; top:470px;font-family: verdana;font-weight:bold;"><%=CAP.getlast_name() %></div>

<div style="width:auto;height:auto;font-size:14px; background:#F00; position:absolute; left:630px; top:470px;font-family: verdana;font-weight:bold;"><%=CSP.getgothram() %></div>



<div style="width:auto;height:auto;font-size:14px; background:#0F0; position:absolute; right:90px; top:470px;font-family: verdana;font-weight:bold;"><%=df.format(Double.parseDouble(CSP.gettotalAmmount())) %></div>
<div style="width:auto;height:auto;margin-top:0px;padding-left:10px;font-size:14px;position:absolute; right:50px; top:530px; background:#F06;font-family: verdana; font-weight:bold;"><%=CSP.getbillingId() %>/<%=dateFormat.format(dbdate.parse(CSP.getdate())) %></div>

</div>



<%}else if(request.getParameter("seva")!=null && request.getParameter("seva").equals("lifetime-archana")){  %>
		


<div style="width:952px;height:566px;">
	<div style="margin-top:328px; background:#C93">
        <div  style="width:auto;height:30px;padding-left:280px;font-size:14px;padding-bottom:0px;position:absolute;font-weight:bold; left:0;font-family: verdana;"><%=CSP.getcustomername() %></div>
        <div  style="width:auto;height:70px;padding-left:280px;font-size:12px;padding-top:0px; position:absolute; left:0px; top:355px;font-family: verdana;font-weight:bold;"><%=CSP.getextra6() %><br><%=CAP.getextra2() %><br><%=CAP.getaddress2()%><br><%=CAP.getextra3() %>-<%=CAP.getpincode() %></div>
	</div>
	<div  style="width:auto;height:30px;font-size:12px;font-weight:bold;padding-top:0px;  position:absolute; right:80px; top:340px;font-family: verdana; text-transform:uppercase;"><%if(CAP.getextra1()!=null && !CAP.getextra1().equals("")){  %> <%=dateFormat2.format(dbdate1.parse(CAP.getextra1())) %>  <%} else{ %><%=CSP.getextra7().replaceFirst(",","")%><%} %><span style="margin-left:20px;"></span></div>

<div  style="width:auto;height:auto;font-weight:bold;font-size:14px; background:#9FF; position:absolute; left:210px; top:460px;font-family: verdana;"><%=CAP.getlast_name() %></div>

<div style="width:auto;height:auto;font-size:14px; background:#F00; position:absolute; left:650px; top:460px;font-family: verdana;font-weight:bold;"><%=df.format(Double.parseDouble(CSP.getrate())) %></div>




<div style="width:auto;height:auto;font-size:13px; background:#0F0; position:absolute; right:15px; top:460px;font-family: verdana;font-weight:bold;"><%=CSP.getbillingId() %>/<%=dateFormat.format(dbdate.parse(CSP.getdate())) %></div>

<div style="width:auto;height:auto;margin-top:0px;padding-left:10px;font-size:14px;position:absolute;left:200px; top:520px; background:#F06;font-family: verdana; font-weight:bold;"><%=CSP.getgothram() %></div>

</div>
<%} %>
</body>
</html>