<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Account Department Login</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<script type="text/javascript">
function validate(){ 
$('#RequiredPassword').hide();
$('#RequiredUserName').hide();
	if($('#username').val().trim()==""){
		$('#RequiredUserName').show();
		$('#username').focus();
		return false;
	}
	
	if($('#password').val().trim()==""){
		$('#RequiredPassword').show();
		$('#password').focus();
		return false;
	}
	
}
</script>
</head>
<body class="loging_bg">
<div class="main">
<div class="logo" align="center"><div class="logo-here"><span style="color:#FFF;">SANSTHAN ACCOUNTS</span></div></div>
<div class="admin-signin">
<form action="validate.jsp" method="post" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="2"><span class="signin">Sign In</span></td>
</tr>
<tr>
<td align="right" class="correct">User Id</td>
<td><input type="text" name="username" id="username" value=""/>
<div class="plz" id="RequiredUserName" style="display: none;">Please Provide Valid Username.</div></td>
</tr>
<tr>
<td align="right" class="correct">Password</td>
<td><input type="password" name="password" id="password" value=""/>
<div class="plz" id="RequiredPassword" style="display: none;">Please Enter Password.</div>
<%if(session.getAttribute("error")!=null){%>
<div class="plz" id="RequiredPassword"><%=session.getAttribute("error") %></div>
<%session.invalidate();}%>
</td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Sign In" class="click bordernone" /></td>
</tr>
</table>
</form>
</div>
</div>
</body>
</html>