<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.List"%>
<%@ page import="beans.diagnosticissues"%>
<%@ page import="mainClasses.diagnosticissuesListing" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Day Wise Test Report</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
</head>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( ".printable" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<body>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
        <div class="clear"></div>
        <div class="vendor-list">	
        <div class="icons">
					<span><a id="print" href="javascript:void( 0 )"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print"></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel"></a></span>
					
				</div>
				<div class="search-list">
				   <%
					
					   SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
					   Calendar calendar=Calendar.getInstance();
					   String toDayDate="";
					   toDayDate=dateFormat.format(calendar.getTime());
					   
					%>
					<form action="dayWiseTestReport.jsp">
					<ul>
					
					
					
						<li><label>From Date:&nbsp;</label><input type="text" name="fromDate" id="fromDate" class="DatePicker  " value="<%=toDayDate%>"></li>
						<li><label>To Date:&nbsp;</label><input type="text" name="toDate" id="toDate" class="DatePicker " value="<%=toDayDate%>"></li> 
					     <li><input type="submit" class="click"></li>
					</ul>
					</form>
					
				</div>
				<table width="100%" cellpadding="0" cellspacing="0" id="">
<tbody><tr>
  
</tr>

<%
    String fromDate="";
    String toDate="";
    String reportFromDate="";
    String reportToDate="";
    if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){
    	reportFromDate=request.getParameter("fromDate");
    }
    if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
    	reportToDate=request.getParameter("toDate");
    }
    
    if(request.getParameter("fromDate")!=null && request.getParameter("fromDate")!=""){
    	fromDate=request.getParameter("fromDate");
    	fromDate=fromDate+" 00:00:00";
    }
    if(request.getParameter("toDate")!=null && request.getParameter("toDate")!=""){
    	toDate=request.getParameter("toDate");
    	toDate=toDate+" 23:59:59";
    }

%>

<tr><td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</td>
</tr><tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<!-- <tr><td colspan="7" align="center" style="font-weight: bold;">Receipt Doctor Test Wise Report</td></tr> -->
<tr><td colspan="7" align="center" style="font-weight: bold;">Report From&nbsp;<%=reportFromDate%>&nbsp;To&nbsp;<%=reportToDate%></td></tr>

</tbody>
</table>
				
				<div class="clear"></div>
				<div class="list-details">
				

<table width="100%" cellpadding="0" cellspacing="0" border="1" id="tblExport">

<%
diagnosticissuesListing diagnosticIssuesListing=new diagnosticissuesListing();
 List<diagnosticissues>  diagnosticList=diagnosticIssuesListing.getDayWiseTests(fromDate,toDate);
 if(diagnosticList!=null && diagnosticList.size()>0){
	 
%>

<tbody><tr>
	<td class="bg" width="15%">Test Name</td>
	<td class="bg" width="15%">Number Of Test</td>
	<td class="bg" width="15%">Patient Amount</td>
	<td class="bg" width="20%">SansthanAmount</td>
</tr>
<%
double patientAmount=0.0;
double sansthanAmount=0.0;

System.out.println("diagnosticList.size()::::"+diagnosticList.size());
for(int i=0;i<diagnosticList.size();i++){
	diagnosticissues diagnostic=diagnosticList.get(i);
	patientAmount=patientAmount+Double.parseDouble(diagnostic.getExtra3());
	sansthanAmount=sansthanAmount+Double.parseDouble(diagnostic.getExtra2());
%>
<tr>
	
	<td><%=diagnostic.getTestName()%></td>
	<td><%=diagnostic.getNumberOfTests()%></td>
	<td><%=diagnostic.getExtra3()%></td>
	<td><%=diagnostic.getExtra2()%></td>
	
</tr>
<%} %>
<tr>


<td colspan="2" align="left" style="    background-color: #bf802b;color: #fff;font-weight: bold;"><b>Total</b></td>
<td><%=patientAmount%></td>
<td><%=sansthanAmount%></td>
</tr>
</tbody>
</table>
<%
 }else{
    	%>
	 <center><span style="color:red;font-family: verdana;font-size: 250%;"><strong>No Records Are available</strong></span></center>
	<% 
}
%>
</div></div></div>
</body>
</html>