<%@page import="mainClasses.vendorsListing"%>
<%@page import="beans.medicalpurchases"%>
<%@page import="mainClasses.medicalpurchasesListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List"%>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript"
	src="js/modal-window.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">

function validate(){
	$('#oldError').hide();

	if($('#tabletName').val().trim()==""){
		$('#oldError').show();
		$('#tabletName').focus();
		return false;
	}		
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="MDCL" class="beans.medicalpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<img src="images/Arrow-down.png" />
				</div>
				<div class="search-list">
					<ul>
						<li><select>
								<option>Batch actions</option>
								<option>Email</option>
						</select></li>
						<li><select>
								<option>Sort by name</option>
								<option>Sort by company</option>
								<option>Sort by overdue balance</option>
								<option>Sort by open balance</option>
						</select></li>
						<li><input type="search" placeholder="find a vendor" /></li>
					</ul>
				</div>
				<div class="icons">
					<span><img src="images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span> <span><img
						src="images/excel.png" style="margin: 0 20px 0 0"
						title="export to excel" /></span> <span>
						<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
					<%mainClasses.medicalpurchasesListing MED = new medicalpurchasesListing();
					vendorsListing VEND = new vendorsListing();
List MD_List=MED.getmedicalpurchases();
if(MD_List.size()>0){%>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr>

							<td class="bg" width="3%">S.No.</td>
							<td class="bg" width="23%">Agencies</td>
							<td class="bg" width="23%">Date</td>
								<td class="bg" width="23%">Amount</td>
							</tr>
						<%for(int i=0; i < MD_List.size(); i++ ){
							MDCL=(medicalpurchases)MD_List.get(i); %>
						<tr>

							<td><%=i+1%></td>
							<td><span><a href="medicalpurchases_details.jsp?id=<%=MDCL.getextra1()%>"><%=VEND.getMvendorsAgenciesName(MDCL.getvendorId())%></a></span></td>
							<td><%=MDCL.getpurchaseDate()%></td>
							<td><%=MDCL.getamount()%></td>
											</tr>
						<%} %>
					</table>
					<%}else{%>
					<div align="center">
						<h1>No Tablets Added Yet</h1>
					</div>
					<%}%>


				</div>
			</div>




		</div>

	</div>
	<!-- main content -->

	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>