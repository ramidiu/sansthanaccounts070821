<%@page import="beans.medicalpurchases"%>

<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<jsp:useBean id="MDCL" class="beans.medicalpurchases" />
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%mainClasses.medicalpurchasesListing MED = new mainClasses.medicalpurchasesListing();
mainClasses.vendorsListing VEND = new mainClasses.vendorsListing();
mainClasses.tabletsListing TAB_L = new tabletsListing();
double total=0.0;
List MD_List=MED.getMmedicalpurchasesByBill(request.getParameter("id").toString());
if(MD_List.size()>0){
	MDCL=(beans.medicalpurchases)MD_List.get(0);
if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="DOC" class="beans.doctordetails"/>
<div class="vendor-page">
<form name="tablets_Update" method="post" action="medicalpurchases_Insert.jsp" onsubmit="return validate();">
<div class="vendor-box">
<div class="vendor-title">Purchase Details</div>
<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%">
<div class="warning" id="NameError" style="display: none;">Please Provide  "Vendor Name".</div>
Vendor Name*</td>
<td >
<div class="warning" id="BillError" style="display: none;">Please Provide  "Bill No".</div>
Bill No*</td>
</tr>
<tr>
<td><%=VEND.getMvendorsAgenciesName(MDCL.getvendorId())%></td>
<td><%=MDCL.getbillNumber()%></td>
</tr>
<tr>
<td>
<div class="warning" id="NarrationError" style="display: none;">Please Provide  "Narration".</div>
Narration*</td>
<td><div>Invoice No*</div></td>
</tr>
<tr>
<td><%=MDCL.getnarration()%></td>
<td><%=MDCL.getextra1()%></td>
</tr>

</table>

</div>
<div style="clear:both;"></div>



</div>

<div class="vendor-list">
<div class="clear"></div>
<div class="list-details">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>

<td class="bg" width="3%">S.No.</td>
<td class="bg" width="23%">Medicine Name</td>

<td class="bg" width="23%">Quantity</td>
<td class="bg" width="23%">Price Per Unit</td>
<td class="bg" width="23%">Price</td>
</tr>
<%for(int i=0; i <  MD_List.size(); i++ ){
	MDCL=(medicalpurchases)MD_List.get(i); 
%>
<tr>

<td><%=i+1%></td>
<td><span><%=TAB_L.getMtabletName(MDCL.gettabletId())%></span></td>

<td><%=MDCL.getquantity()%></td>
<td><%=MDCL.getdate()%></td>
<td><%=MDCL.getamount()%></td>
</tr>
<%
total=total+(Double.parseDouble(MDCL.getamount()));
} %>
<tr>
<td colspan="4" class="bg" >Total Bill Amount</td><td class="bg"><%=total %></td>
</tr>
</table>
</div>
</div>



</form>
</div>

</div>
<!-- main content -->

<%}}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>