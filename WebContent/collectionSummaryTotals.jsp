<%@page import="mainClasses.productsListing"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.headofaccountsListing,java.util.List"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<link rel="stylesheet" href=themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	$("#"+billId).slideToggle();
}
function showDetails1(billId){
	$("#"+billId).slideToggle();
	/* if(document.getElementById(billId).style.display=="none"){
		//document.getElementById(billId).style.display='block';
		$("#"+billId).slideToggle();
	}else if(document.getElementById(billId).style.display=="block"){
		//document.getElementById(billId).style.display='none';
		$("#"+billId).slideToggle();
	} */
	
}
</script>
<script type="text/javascript" lang="javascript">
	var openMyModalSj = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 450;
		modalWindow.height = 200;
		modalWindow.content = "<iframe width='450' height='200' frameborder='0' scrolling='yes' allowtransparency='false' src='pendingIndentsPopup.jsp'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
	<jsp:useBean id="HOD" class="beans.headofaccounts"></jsp:useBean>
	<jsp:useBean id="SALE" class="beans.customerpurchases" />
	<jsp:useBean id="BKR" class="beans.booking_roomsdetails"></jsp:useBean>
	<jsp:useBean id="CAN" class="beans.cancellation_details"></jsp:useBean>
	<jsp:useBean id="CU" class="beans.counter_balance_update"></jsp:useBean>
	<jsp:useBean id="SA" class="beans.customerpurchases" />
	<jsp:useBean id="BK" class="beans.bookingrooms"/>
	<jsp:useBean id="LG" class="beans.logins"></jsp:useBean>
<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down"><p style="padding-left: 500px;">TOTAL SALES REPORT</p></div>
<div class="icons">
<a id="print"><span><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></span></a>
<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>
<div class="clear"></div>
<div class="total-report">
<%headofaccountsListing HODA=new headofaccountsListing();
List HODET=HODA.getheadofaccounts();
%>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="4" class="bg-new" style="color: #F00;">*** SHRI SHIRDI SAI BABA SANSTHAN TRUST ***</td></tr>
 <form action="collectionSummaryTotals.jsp" method="post">  
 <tr>
	<td><input type="submit" name="today" value="Today" class="click bordernone"/></td>
	<td><input type="submit" name="weekly" value="Week Report" class="click bordernone"/></td>
	<td><input type="submit" name="monthly" value="Month Report" class="click bordernone"/></td>
	<td><input type="submit" name="yearly" value="Year Report" class="click bordernone"/></td>
</tr>
</form>
<form action="collectionSummaryTotals.jsp" method="post">
<tr>
<td width="20%">
<%-- <ul>
<%if(HODET.size()>0){ 
	for(int i=0;i<HODET.size();i++){
		HOD=(headofaccounts)HODET.get(i);%>
	<li><input type="checkbox" name="hoid" id="hoid" value="<%=HOD.gethead_account_id()%>"/> <%=HOD.getname() %></li>
<%}} %>
</ul> --%>
</td>
<%
String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); 
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
<td width="30%">From Date <input type="text" style="width:80px;" name="fromDate" id="fromDate" readonly="readonly" value="<%=fromdate%>"/> </td>
<td width="30%">To Date <input type="text" style="width:80px;" name="toDate" id="toDate" readonly="readonly" value="<%=todate%>"/> </td>
<td width="15%"><input type="submit" name="search" value="Search" class="click bordernone"/></td>
</tr>
</form>
</table>
<div id="tblExport">
<%
mainClasses.customerpurchasesListing SALE_L = new mainClasses.customerpurchasesListing();
mainClasses.banktransactionsListing BKTRAL=new mainClasses.banktransactionsListing();
productsListing PRDL=new productsListing();
employeesListing EMPL=new employeesListing();
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
DateFormat  onlyYear= new SimpleDateFormat("yyyy");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String currentDate=(dateFormat2.format(c1.getTime())).toString();
String fromDate=currentDate+" 00:00:01";
String toDate=currentDate+" 23:59:59";
if(request.getParameter("today")!=null && request.getParameter("today").equals("Today")){
	fromDate=currentDate+" 00:00:01";
	toDate=currentDate+" 23:59:59";
}else if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
	c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
	fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
	toDate=(dateFormat2.format(c1.getTime())).toString();
	toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
	c1.getActualMaximum(Calendar.DAY_OF_MONTH);
	int lastday = c1.getActualMaximum(Calendar.DATE);
	c1.set(Calendar.DATE, lastday);  
	toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
	c1.getActualMinimum(Calendar.DAY_OF_MONTH);
	int firstday = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday); 
	fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
	int lastday_y = c1.get(Calendar.YEAR);
	c1.set(Calendar.DATE, lastday_y);  
	c1.getActualMinimum(Calendar.DAY_OF_YEAR);
	int firstday_y = c1.getActualMinimum(Calendar.DATE);
	c1.set(Calendar.DATE, firstday_y); 
	Calendar cld = Calendar.getInstance();
	cld.set(Calendar.DAY_OF_YEAR,1); 
	fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
	cld.add(Calendar.YEAR,+1); 
	toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate")+" 00:00:01";
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate")+" 23:59:59";
	}
}
List roomtype_list=new ArrayList();
List rumtype_list_new=new ArrayList();
List rumtype_list_cancellations=new ArrayList();
List SAL_list=null;
Double totalSaleAmt=0.00;
Double totDeposit=0.00;
Double openingBal=0.00;
DecimalFormat df = new DecimalFormat("0.00");
String prevDate="";
String presentDate="";
double bankDepositTot=0.00;
double RTGS_total_amout_cancl=0.00;
double creaditSaleTot=0.00;
double cashSaleAmt=0.00;
double onlinesaleamount=0.00;
double cardSaleAmt=0.00;
double totalAmount=0.00;
double poojaStoreTotAmt=0.00;
double sansthanTotAmt=0.00;
double charityTotAmt=0.00;
double sainivasTotAmt=0.00;
double gPaySaleAmt=0.00;
double phnPeSaleAmt=0.00;
double chequeSaleAmt=0.00;

List SAL_DETAIL=null;
if(HODET.size()>0){ 
	for(int i=0;i<HODET.size();i++){
		HOD=(headofaccounts)HODET.get(i);
			if(!HOD.gethead_account_id().equals("5") && !HOD.gethead_account_id().equals("3")){
				SAL_list=SALE_L.getTotalSaleReport(HOD.gethead_account_id(),fromDate,toDate);
		
		%>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="8" class="bg-new" style="color:#F00;cursor: pointer;" onclick="showDetails('<%=HOD.gethead_account_id()%>');"><%=HOD.getname() %></td></tr>
	<%if(SAL_list.size()>0){ %>
	<tr ><td colspan="8">
	<table width="95%" cellpadding="0" cellspacing="0" class="date-wise" style="display: none;" id="<%=HOD.gethead_account_id()%>">
	<tr>
		<td style="font-weight: bold;">S.NO.</td>
		<td style="font-weight: bold;">INVOICE DATE</td>
		<td style="font-weight: bold;">INVOICE.NO.</td>
		<td style="font-weight: bold;">PAYMENT TYPE</td>
		<td style="font-weight: bold;">QTY</td>
		<td style="font-weight: bold;" align="right">DEBIT</td>
		<td style="font-weight: bold;" align="right">CREDIT</td>
		<td style="font-weight: bold;">Entered By</td>
	</tr>
	<%if(SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate)!=null && !SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate).equals("")){
		 totalSaleAmt=Double.parseDouble(SALE_L.getTotalSalesAmount(HOD.gethead_account_id(),"",fromDate));
	}
	if(BKTRAL.getEmployeeDepositAmount("",fromDate)!=null && !BKTRAL.getEmployeeDepositAmount("",fromDate).equals("")){
		 totDeposit=Double.parseDouble(BKTRAL.getEmployeeDepositAmount("",fromDate));}
	for(int s=0; s < SAL_list.size(); s++ ){ 
				SALE=(beans.customerpurchases)SAL_list.get(s);
				if(!presentDate.equals("")){
								prevDate=presentDate;	
							}
							presentDate=""+df2.format(dateFormat.parse(SALE.getdate()));
							if(SALE.getcash_type().equals("cash")){
								cashSaleAmt=cashSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							} else if(SALE.getcash_type().equals("online success")){
								onlinesaleamount=onlinesaleamount+Double.parseDouble(SALE.gettotalAmmount());
							}
							else if(SALE.getcash_type().equals("card")){
								cardSaleAmt=cardSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}
							else if(SALE.getcash_type().equals("googlepay")){
								gPaySaleAmt=gPaySaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}
							else if(SALE.getcash_type().equals("phonepe")){
								phnPeSaleAmt=phnPeSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}
							else if(SALE.getcash_type().equals("cheque")){
								chequeSaleAmt=chequeSaleAmt+Double.parseDouble(SALE.gettotalAmmount());
							}
							
							else{
								creaditSaleTot=creaditSaleTot+Double.parseDouble(SALE.gettotalAmmount());
							}%>
	<tr style="position: relative;">
			<td ><%=s+1%></td>
			<td><a href="#"><%=df2.format(dateFormat.parse(SALE.getdate()))%></a></td>
			<td><a href="#"  onclick="showDetails1('<%=SALE.getbillingId()%>')"><%=SALE.getbillingId()%></a></td>
			<td><%=SALE.getcash_type() %></td>
			<td><%=SALE.getquantity()%></td>
			<%totalAmount=totalAmount+Double.parseDouble(SALE.gettotalAmmount());%>
			<td align="right"><%=df.format(Double.parseDouble(SALE.gettotalAmmount()))%></td>
			<td></td>
			<td><%=EMPL.getMemployeesName(SALE.getemp_id()) %></td>
	</tr>
	<%SAL_DETAIL=SALE_L.getBillDetails(SALE.getbillingId());
						if(SAL_DETAIL.size()>0){ %>
						<tr>
							<td colspan="8"   >
							<div style="display: none;" id="<%=SALE.getbillingId()%>">
							<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
							<tr>
							<td width="5%"></td>
							<td width="5%">CODE</td>
							<td width="20%">PRODUCT NAME</td>
							<td width="10%">PRODUCT SALE QTY</td>
							<td width="10%">PRICE</td>
							<td width="10%">CARD LAST 4 DIGITS</td>
							<td width="10%">CARD AUTHENTICATION NO</td>
							<td width="10%">TRANSACTION MOBILE NUMBER</td>
							<td width="10%">TRANSACTION UTR NUMBER</td>
	
							<td width="10%">TOTAL AMOUNT</td>
							<td width="20%">Entered by</td>
							</tr>
						<% 
						for(int j=0;j<SAL_DETAIL.size();j++){
								
							SA=(beans.customerpurchases)SAL_DETAIL.get(j);
							System.out.println("SAL_DETAIL.size()......."+SA.getExtra29()+"''"+SA.getExtra30());
							%>
								<tr style="position: relative;">
									<td ></td>
									<td><%=SA.getproductId()%></td>
									<td><%=PRDL.getProductsNameByCat(SA.getproductId(), SA.getextra1())%></td>
									<td><%=SA.getquantity()%></td>
									<td><%=SA.getrate() %></td>
									<td><%=SA.getextra10() %></td>
									<td><%=SA.getextra14() %></td>
									<td><%=SA.getExtra29() %></td>
									<td><%=SA.getExtra30() %></td>
									<td><%=SA.gettotalAmmount()%></td>
									<td ><%=EMPL.getMemployeesName(SA.getemp_id()) %></td>
								</tr>
								<%} %>
						</table>
						</div>
						</td>
						</tr>
	<%}} %>
	</table>
	</td></tr>
	<tr>
		<td colspan="5"  style="text-align:right">Total Cash Sales Amount</td>
		<td><%=df.format(cashSaleAmt) %></td>
		<td colspan="2"><%=df.format(bankDepositTot) %></td>
			</tr>
			<%if(!HOD.getname().equals("POOJA STORES")){  %>
				<tr>
		<td colspan="5"  style="text-align:right">Total Online Sales Amount</td>
		<td><%=df.format(onlinesaleamount) %></td>
		<td colspan="2"></td>
			</tr>
			<tr>
		<td colspan="5"  style="text-align:right">Total Card Sales Amount</td>
		<td><%=df.format(cardSaleAmt) %></td>
		<td colspan="2"></td>
			</tr>
			<tr>
		<td colspan="5"  style="text-align:right">Total BharathPe Sales Amount</td>
		<td><%=df.format(gPaySaleAmt) %></td>
		<td colspan="2"></td>
			</tr>
			<tr>
		<td colspan="5"  style="text-align:right">Total PhonePe Sales Amount</td>
		<td><%=df.format(phnPeSaleAmt) %></td>
		<td colspan="2"></td>
			</tr>
		<tr>
		<td colspan="5"  style="text-align:right">Total Cheque Sales Amount</td>
		<td><%=df.format(chequeSaleAmt) %></td>
		<td colspan="2"></td>
			</tr>
			<%} %>
	<tr>
		<td colspan="5"  style="text-align:right">Total Credit Sales Amount</td>
		<td><%=df.format(creaditSaleTot) %></td>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="5"  style="text-align:right">Total Sales Amount</td>
		<%if(HOD.getname().equals("SANSTHAN")){
					sansthanTotAmt=totalAmount;
				}else if(HOD.getname().equals("CHARITY")){
					charityTotAmt=totalAmount;
				}%>
		<td><%=df.format(totalAmount)%></td>
		<td colspan="2"></td>
	</tr>
	<%
	cashSaleAmt=0.00;
	bankDepositTot=0.00;
	totalAmount=0.00;
	creaditSaleTot=0.00;
	onlinesaleamount=0.00;
	cardSaleAmt=0.00;
	gPaySaleAmt=0.00;
	phnPeSaleAmt=0.00;
	chequeSaleAmt=0.00;
	}else{ %>
	<tr><td colspan="8" style="color: #000;">No sales!</td></tr>
	<%} %>
</table>
<%} }}

%>
<table width="95%" cellpadding="0" cellspacing="0" class="date-wise">
<tr><td colspan="8" class="bg-new">Grand Total</td></tr>
<tr>
<td colspan="6"  style="text-align:right" width="75%">SANSTHAN</td>
<td colspan="2"><%=df.format(sansthanTotAmt) %></td>
</tr>
<tr>
<td colspan="6"  style="text-align:right" width="75%">CHARITY</td>
<td colspan="2"><%=df.format(charityTotAmt) %></td>
</tr>
<tr>
<td colspan="6"  style="text-align:right"><span>Grand Total</span></td>
<td colspan="2"><%=df.format(sansthanTotAmt+charityTotAmt) %></td>
</tr>
</table>
</div>
</div>
</div>
</div>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
</body>
</html>