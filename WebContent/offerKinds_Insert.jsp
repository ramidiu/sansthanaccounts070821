<%@page import="mainClasses.WtspNotification"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat,java.util.*"%>
<%@page import="beans.UniqueIdGroup" %>
<%@page import="beans.UniqueIdGroupService" %>
<%@page import="helperClasses.DevoteeGroups"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<jsp:useBean id="GOD" class="beans.godwanstockService"/>
<jsp:useBean id="GODS" class="beans.godwanstock_scService"/>
</head>
<body>
<jsp:useBean id="CSP" class="beans.customerpurchasesService"/>
<jsp:useBean id="PRD" class="beans.productsService"/>
<jsp:useBean id="NOG" class="beans.no_genaratorService"/>
<jsp:useBean id="COU" class="beans.counter_balance_updateService"/>
<jsp:useBean id="CR" class="beans.cashier_report" />
<jsp:useBean id="CSH" class="beans.cashier_reportService" />
<jsp:useBean id="CAP" class="beans.customerapplicationService" />
<jsp:useBean id="DREG" class="beans.sssst_registrationsService"></jsp:useBean>

<%
WtspNotification wtsp=new WtspNotification();
boolean productflag=false;
mainClasses.no_genaratorListing NG_l=new mainClasses.no_genaratorListing();
mainClasses.subheadListing SUBHL=new mainClasses.subheadListing();
mainClasses.counter_balance_updateListing CUBUL=new mainClasses.counter_balance_updateListing();
String billingId=NG_l.getid("offerKindsInvoice");
//String billingId=request.getParameter("receiptNo");
String minorHeadId=request.getParameter("type");
String product[]=new String[5];
if(request.getParameter("sortBy")!=null && request.getParameter("sortBy").equals("product")){
	product=request.getParameter("productId").split(" ");
	productflag=true;
}else if(request.getParameter("sortBy")!=null && request.getParameter("sortBy").equals("subhead")){
	product=request.getParameter("sub_head_id").split(" ");
}

String productId="";
if(product.length>0){
	productId=product[0];}
productsListing PROL=new productsListing();
minorheadListing MINHL=new minorheadListing();
//String HOAID=PROL.getHeadOFAccountID(minorHeadId);

DateFormat mySqlDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
UniqueIdGroupService uniqueIdGroupService=new UniqueIdGroupService();
String uniqueid = request.getParameter("uniqueid");
/* System.out.println("uniqueid===>"+uniqueid);  */
/* String devoteeGroup[]=request.getParameterValues("devoteeGroup"); */

String HOAID=request.getParameter("headId");
String Amount=request.getParameter("Amount");
String ticQty=request.getParameter("ticQty");
String totAmt=request.getParameter("totAmt");
String customerName=request.getParameter("customerName");
String mobile=request.getParameter("mobile");
String address=request.getParameter("address");
String purpose=request.getParameter("purpose");
String emailid= request.getParameter("emailid");
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");

DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat  dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String date=(dateFormat.format(c1.getTime())).toString();
 boolean x=false;
 String emp_id=session.getAttribute("empId").toString();
 DecimalFormat df2 = new DecimalFormat("###.##");
 Double totalbillAMt=0.00;
 
/*  String applictn=request.getParameter("applicatn");
 String title=request.getParameter("title");
 String lastname=request.getParameter("lastname"); */
 String gothram=request.getParameter("gothram");
/*  String emailid=request.getParameter("emailid");
 String pancardNo=request.getParameter("pancardNo");
 String dob=request.getParameter("dob");
 String pincode=request.getParameter("pincode");
 String fromdate=request.getParameter("fromdate")+" 00:00:01";
 String todate=request.getParameter("todate")+" 00:59:59"; */
 
 String fdate="";
 String time="";
 
 if(request.getParameter("fromdate")!=null && !request.getParameter("fromdate").equals(""))
 {
	 	fdate=request.getParameter("fromdate");
 }
 
 if(request.getParameter("time")!=null && !request.getParameter("time").equals(""))
 {
	 	time=request.getParameter("time");
}
 
 
%>
<jsp:setProperty name="CSP" property="date" value="<%=date%>"/>
<jsp:setProperty name="CSP" property="productId" value="<%=productId%>"/>
<jsp:setProperty name="CSP" property="quantity" value="<%=ticQty%>"/>
<jsp:setProperty name="CSP" property="rate" value="<%=Amount%>"/>
<jsp:setProperty name="CSP" property="totalAmmount" value="<%=totAmt %>"/>
<jsp:setProperty name="CSP" property="billingId" value="<%=billingId%>"/>
<jsp:setProperty name="CSP" property="vocharNumber" value=""/>
<jsp:setProperty name="CSP" property="cash_type" value="offerKind"/>
<jsp:setProperty name="CSP" property="customername" value="<%=customerName%>"/>
<jsp:setProperty name="CSP" property="phoneno" value="<%=mobile%>"/>
<jsp:setProperty name="CSP" property="emp_id" value="<%=emp_id%>"/>
<jsp:setProperty name="CSP" property="extra1" value="<%=HOAID%>"/>
<jsp:setProperty name="CSP" property="extra2" value="<%=purpose%>"/>
<jsp:setProperty name="CSP" property="extra8" value="<%=emailid%>"/>
<jsp:setProperty name="CSP" property="gothram" value="<%=gothram%>"/>
<jsp:setProperty name="CSP" property="extra6" value="<%=address%>"/>
<jsp:setProperty name="CSP" property="extra11" value="<%=fdate%>"/>
<jsp:setProperty name="CSP" property="extra13" value="<%=time%>"/>
 <%-- <%x=CSP.insert();%><%=CSP.geterror()%> --%> 

 <%
/* if(uniqueid == null || uniqueid.equals("")) */
if((uniqueid == null || uniqueid.equals("")) && !mobile.trim().equals(""))
{%>
	<jsp:setProperty property="title" name="DREG" value=""/>
	<jsp:setProperty property="first_name" name="DREG" value="<%=customerName %>"/>
	<jsp:setProperty property="middle_name" name="DREG" value=""/>
	<jsp:setProperty property="last_name" name="DREG" value=""/>
	<jsp:setProperty property="spouce_name" name="DREG" value=""/>
	<jsp:setProperty property="gender" name="DREG" value=""/>
	<jsp:setProperty property="dob" name="DREG" value=""/>
	<jsp:setProperty property="address_1" name="DREG" value="<%=address %>"/>
	<jsp:setProperty property="address_2" name="DREG" value=""/>
	<jsp:setProperty property="country" name="DREG" value=""/>
	<jsp:setProperty property="city" name="DREG" value=""/>
	<jsp:setProperty property="pincode" name="DREG" value=""/> 
	<jsp:setProperty property="mobile" name="DREG" value="<%=mobile %>"/>
	<jsp:setProperty property="idproof_1" name="DREG" value=""/>
	<jsp:setProperty property="idproof1_num" name="DREG" value=""/>
	<jsp:setProperty property="pancard_no" name="DREG" value=""/>
	<jsp:setProperty property="email_id" name="DREG" value="<%=emailid %>"/>
	<jsp:setProperty property="updates_receive_via" name="DREG" value=""/>
	<jsp:setProperty property="gothram" name="DREG" value="<%=gothram %>"/>
	<jsp:setProperty property="register_date" name="DREG" value="<%=date %>"/>
	<jsp:setProperty property="extra4" name="DREG" value=""/>
	<%=DREG.insert() %><%=DREG.geterror()%>
	
	<%String sssst_id= DREG.insertWithId(); 
	if(sssst_id !=null && !sssst_id.equals("")){
	UniqueIdGroup uig=new UniqueIdGroup();
	uig.setUniqueid(sssst_id);
	uig.setDonor("");
	uig.setTrusties("");
	uig.setNad("");
	uig.setLta("");
	uig.setRds("");
	uig.setTimeshare("");
	uig.setEmployee("");
	uig.setGeneral("");
	uig.setCreatedDate(mySqlDateFormat.format(new Date()));
	uig.setUpdatedDate(mySqlDateFormat.format(new Date()));
	uig.setExtra1("");
	uig.setExtra2("");
	uig.setExtra3("");
	uig.setExtra4("");
	uig.setExtra5("");
	uig.setExtra6("");
	uig.setExtra7("");
	uig.setExtra8("");
	uig.setExtra9("");
	uig.setExtra10("");
	uig.setExtra11("");
	uig.setExtra12("");
	uig.setExtra13("");
	uig.setExtra14("");
	uig.setExtra15("");
	
	if(productId.equals("20092")){
		uig.setNad(DevoteeGroups.NAD.toString());
	}
	if(productId.equals("20063")){
		uig.setLta(DevoteeGroups.LTA.toString());
	}
	if(!productId.equals("20092") && !productId.equals("20063")  ){
		uig.setGeneral(DevoteeGroups.GENERAL.toString());
	}
	
	uniqueIdGroupService.insert(uig);
}
%>
<jsp:setProperty name="CSP" property="extra15" value="<%=sssst_id%>"/>	
<%}else{%>
	<jsp:setProperty name="CSP" property="extra15" value="<%=uniqueid%>"/>	
<%}
%>

<%x=CSP.insert();%><%=CSP.geterror()%> 
<%

if(request.getParameter("wtsp").equals("ok") && !request.getParameter("mobile").equals("")){
	CSP.addToOptin(request.getParameter("mobile"));
	wtsp.offerKindsWtspNotification(request,response,request.getParameter("sortBy"));		
}
%>

<%System.out.println("x====>"+x); %>


<%
if(x && productflag){
	%>
	<jsp:setProperty name="GOD" property="vendorId" value="1208"/>
<jsp:setProperty name="GOD" property="productId" value="<%=productId%>"/>
<jsp:setProperty name="GOD" property="description" value=""/>
<jsp:setProperty name="GOD" property="quantity" value="<%=ticQty%>"/>
<jsp:setProperty name="GOD" property="purchaseRate" value="0.0"/>
<jsp:setProperty name="GOD" property="date" value="<%=date%>"/>
<jsp:setProperty name="GOD" property="billNo" value="<%=billingId%>"/>
<jsp:setProperty name="GOD" property="vat" value="0"/>
<jsp:setProperty name="GOD" property="emp_id" value="<%=emp_id%>"/>
<jsp:setProperty name="GOD" property="department" value="<%=HOAID%>"/>
<jsp:setProperty name="GOD" property="extra1" value="<%=billingId%>"/>
<jsp:setProperty name="GOD" property="extra2" value="<%=CSP.getpurchaseId()%>"/>
<jsp:setProperty name="GOD" property="profcharges" value="checked"/>
<jsp:setProperty name="GOD" property="extra8" value="0"/>
<jsp:setProperty name="GOD" property="extra10" value="0"/>
<jsp:setProperty name="GOD" property="extra11" value="0"/>
<jsp:setProperty name="GOD" property="checkedby" value="<%=emp_id%>"/>
<jsp:setProperty name="GOD" property="checkedtime" value="<%=date%>"/>

<jsp:setProperty name="GOD" property="po_approved_status" value="Approved" />
<jsp:setProperty name="GOD" property="po_approved_by" value="EMP1000024" />
<jsp:setProperty name="GOD" property="po_approved_date" value="<%=date%>"/>
<jsp:setProperty name="GOD" property="extra5" value="<%=ticQty%>"/>
<jsp:setProperty name="GOD" property="approval_status" value="Not-Approve" />	
	<%=GOD.insert()%><%=GOD.geterror()%>

<%
	String goadst="";
double godstock=0;

productsListing PRD_L=new productsListing();

godstock=Double.parseDouble(PRD_L.getProductsStockGodwanWithHOA(productId,HOAID));
godstock=godstock+Double.parseDouble(ticQty); 
goadst=String.valueOf(godstock);

%>	
	<jsp:setProperty name="PRD" property="productId" value="<%=productId%>"/>
<jsp:setProperty name="PRD" property="balanceQuantityGodwan" value="<%=goadst%>"/>
<jsp:setProperty name="PRD" property="head_account_id" value="<%=HOAID%>"/>
<%=PRD.updateWithHOA()%><%=PRD.geterror()%>
	<%}%>


<%-- <%if(x==true){
	mainClasses.cashier_reportListing CASH_RE=new mainClasses.cashier_reportListing();
	List cash_list=CASH_RE.getCounter(session.getAttribute("empId").toString(),"");
	for(int l=0;l<cash_list.size();l++)
	{
		CR=(beans.cashier_report)cash_list.get(l);
		if(CR.getcash3().equals(session.getAttribute("Role").toString()))
		{	
			if(CR.getlogout_time().equals(""))
			{
			String count_bal="";
			double counter_bal=0.00;
			totalbillAMt=Double.parseDouble(totAmt);
			counter_bal=(Double.parseDouble(CR.getcounter_balance().toString())+totalbillAMt);
			count_bal=""+Double.valueOf(df2.format(counter_bal));
	%>
	<jsp:setProperty name="COU" property="booking_id" value="<%=billingId%>"/>
	<jsp:setProperty name="COU" property="date" value="<%=date%>"/>
	<jsp:setProperty name="COU" property="login_id" value="<%=emp_id%>"/>
	<jsp:setProperty name="COU" property="booking_amount" value="<%=totAmt%>"/>
	<jsp:setProperty name="COU" property="opening_balance" value="<%=date%>"/>
	<jsp:setProperty name="COU" property="counter_balance" value="<%=count_bal%>"/>
	<jsp:setProperty name="COU" property="closing_balance" value=""/>
	<jsp:setProperty name="COU" property="booked_date" value="<%=date%>"/>
	<jsp:setProperty name="COU" property="counter_ex1" value=""/>
	<jsp:setProperty name="COU" property="counter_ex2" value=""/>
	<jsp:setProperty name="COU" property="counter_ex3" value=""/>
	<jsp:setProperty name="COU" property="counter_ex4" value=""/>
	<jsp:setProperty name="COU" property="counter_ex5" value=""/>
	<jsp:setProperty name="COU" property="counter_ex6" value=""/>
	<jsp:setProperty name="COU" property="counter_ex7" value=""/>
	<%=COU.insert()%><%=COU.geterror()%>
	<jsp:setProperty name="CSH" property="cashier_report_id" value="<%=CR.getcashier_report_id()%>"/>
    <jsp:setProperty name="CSH" property="counter_balance" value="<%=count_bal%>"/>
	<jsp:setProperty name="CSH" property="total_amount" value="<%=count_bal%>"/>
	<%=CSH.update() %><%=CSH.geterror() %>
<%}}
	}
} --%>
<%
/* NOG.updatInvoiceNumber(billingId,"offerKindsInvoice"); */
//response.sendRedirect("printTicket.jsp?bid="+billingId);
System.out.println("This is after the updating offerKindsInvoice table");
if(request.getParameter("saveonly")!=null && request.getParameter("saveonly").equals("SAVE-ONLY")){
	//System.out.println("save==>"+PROL.getProductsNameByCat(productId,HOAID));
	model.sendmailservice sendMail=new model.sendmailservice();
	//System.out.println("email====>"+emailid);
	if(emailid != null && !emailid.equals("")){
		//System.out.println("entered");
    String mailtemplate="<div style='box-shadow: 0px 0px 1px 0px; width:600px; margin:auto; font-family:arial; '><table width='600px' border='0' align='center' cellpadding='0' cellspacing='0' style='margin:0 auto; background:#f00'><tbody><tr><td width='600px'></td></tr></tbody> </table> <table width='600px' cellpadding='0' cellspacing='0' border='0' align='center'><tbody><tr> <td style='font-size:13px;line-height:1.4'><span style='color:#F00; text-transform:capitalize; font-size:16px; font-weight:bold;' >Welcome to Shri Shirdi Saibaba Saisansthan Trust DSNR, Thank you for booking "+PROL.getProductsNameByCat(productId,HOAID)+" Worth Rs."+totAmt+" with referenceID "+billingId+".</span>";
    		sendMail.sendmail(" saisansthan_dsnr@yahoo.co.in ", emailid, "Saisansthan", mailtemplate);
	}
	 sms.CallSmscApi SMS=new sms.CallSmscApi();
	String message="Welcome to Shri Shirdi Saibaba Saisansthan Trust DSNR, Thank you for booking "+PROL.getProductsNameByCat(productId,HOAID)+" Worth Rs."+totAmt+" with referenceID "+billingId+"."; 
	String result=SMS.SendSMS(mobile,message.replace(" ", "%20"));  
	response.sendRedirect("offerKinds-entry.jsp");
}else{
	//System.out.println("save&print====>"+PROL.getProductsNameByCat(productId,HOAID));
	model.sendmailservice sendMail=new model.sendmailservice();
	//System.out.println("email====>"+emailid);
	if(emailid != null && !emailid.equals("")){
		//System.out.println("entered");
    String mailtemplate="<div style='box-shadow: 0px 0px 1px 0px; width:600px; margin:auto; font-family:arial; '><table width='600px' border='0' align='center' cellpadding='0' cellspacing='0' style='margin:0 auto; background:#f00'><tbody><tr><td width='600px'></td></tr></tbody> </table> <table width='600px' cellpadding='0' cellspacing='0' border='0' align='center'><tbody><tr> <td style='font-size:13px;line-height:1.4'><span style='color:#F00; text-transform:capitalize; font-size:16px; font-weight:bold;' >Welcome to Shri Shirdi Saibaba Saisansthan Trust DSNR, Thank you for booking "+PROL.getProductsNameByCat(productId,HOAID)+" Worth Rs."+totAmt+" with referenceID "+billingId+".</span>";
    		sendMail.sendmail(" saisansthan_dsnr@yahoo.co.in ", emailid, "Saisansthan", mailtemplate);
	}
	 sms.CallSmscApi SMS=new sms.CallSmscApi();
	String message="Welcome to Shri Shirdi Saibaba Saisansthan Trust DSNR, Thank you for booking "+PROL.getProductsNameByCat(productId,HOAID)+" Worth Rs."+totAmt+" with referenceID "+billingId+"."; 
	String result=SMS.SendSMS(mobile,message.replace(" ", "%20"));
	response.sendRedirect("offerKinds-entry.jsp?bid="+billingId);
}

%>
</body>
</html>