<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PRODUCT-RETURN ENTRY</title>
<!--Date picker script  -->
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});		
});
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var hoid=$('#hoid').val();
var arr = [];
$.post('searchProducts.jsp',{q:data1,hoasj:hoid},function(data)
{
	var response = data.trim().split("\n");
	 var doctorNames=new Array();
	 var amount=new Array();
	 var gross=0;
	 var qntystore=new Array();
	 var sellrate=new Array();
	 var vat=new Array();
	 for(var i=0;i<response.length;i++){
		 var d=response[i].split(",");
		/* gross=Number(parseFloat(d[4])+Number(d[4])*(Number(parseFloat(d[5])/100))); */
		 arr.push({
			 label: d[0]+" "+d[1],
		        amount:d[4],
		        vat :d[5],
		        qntystore :d[6],
		        sellrate :d[7],
		        sortable: true,
		        resizeable: true
		    });
	 }
	var availableTags=data.trim().split("\n");	
	availableTags=arr;
	$('#product'+j).autocomplete({
		source: availableTags,
		focus: function(event, ui) {
			// prevent autocomplete from updating the textbox
			event.preventDefault();
			// manually update the textbox
		$(this).val(ui.item.label);
		},
		select: function(event, ui) {
			// prevent autocomplete from updating the textbox
			event.preventDefault();
			// manually update the textbox and hidden field
			$(this).val(ui.item.label);
			$('#purchaseRate'+j).val('0');
			$("#returnQty"+j).focus();
		}
		
	});
		});
} 
function Confirm(){
	 var status = confirm('Are you sure you want to submit the Journal Voucher?  \n\n Click OK to proceed');
	   if(status == false){
	  	 return false;
	   }
	   else{
	  	 return true; 
	   }
}
</script>
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
</head>
<body>
<jsp:useBean id="HOA" class="beans.headofaccounts"/>
<div><%@ include file="title-bar.jsp"%></div>
<form name="tablets_Update" method="post" action="product_return_insert.jsp" onsubmit="return Confirm();">

<div class="vendor-page">

<div class="vendor-box">
<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="4" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 10px;">(Regd.No.646/92)</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 12px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
						<tr><td colspan="4" align="center" style="font-weight: bold;font-size: 20px;">Product Return Entry</td></tr>
</table>
<div class="vender-details">
<%mainClasses.headofaccountsListing HOA_L=new mainClasses.headofaccountsListing();
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
List HA_Lists=HOA_L.getheadofaccounts();
%>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>RETURN INV NO</td>
<td>Date</td>
<td>Department</td>
</tr>
<tr>
<td><input type="text" name="billnumb" id="billnumb" value="<%=ng_LST.getid("returnProductInv")%>"  readonly="readonly"/></td>
<td><input type="text" name="date" id="date"  value="<%=currentDate%>" class="DatePicker" readonly="readonly"/></td>
<td><select name="hoid" id="hoid">
<%if(HA_Lists.size()>0){
	for(int i=0;i<HA_Lists.size();i++){
	HOA=(beans.headofaccounts)HA_Lists.get(i); %>
<option value="<%=HOA.gethead_account_id()%>" <%if(request.getParameter("hoid")!=null &&!request.getParameter("hoid").equals("")&& request.getParameter("hoid").equals(HOA.gethead_account_id()) ) {%> selected="selected" <%}else if(HOA.gethead_account_id().equals("3")){ %> selected="selected" <%} %> > <%=HOA.getname() %> </option>
<%}} %>
</select></td>
</tr>
<tr><td>MSE NUMBER *</td><td>Narration*</td>
</tr>
<tr>
<td><input type="text" name="mseID" id="billnumb" value=""   placeHolder="Enter master stock entry number" required/></td>
<td><input  name="narration" id="narration" required placeHolder="Write your narration" onkeyup="javascript:this.value=this.value.toUpperCase();"/></input></td></tr>
</table>
</div>
<div style="clear:both;"></div>
</div>

<div class="vendor-list" style="">
<div class="bgcolr"  style=" float:left;width:100%;">

<div class="sno1 bgcolr">S.NO.</div>
<div class="ven-nme1 bgcolr" style="width:250px; ">PRODUCT</div>
<div class="ven-nme1 bgcolr">RETURN-QTY</div>

</div>

<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 50; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

<div class="sno1 MT13"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1" style="width:250px; cursor: pointer;">
<input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>');tabCall('<%=i%>');" class="" value="" placeholder="Find a product here" <%if(i==0){ %> required <%} %>  autocomplete="off"/></div>
<div class="ven-nme1">&#8377;<input type="text" name="returnQty<%=i%>" id="returnQty<%=i%>"  value="1"  style="width: 150px;"></input></div>
<div class="clear"></div>
</div>
<%} %>
<div class="add-ven">
<input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/>
<span style="margin:0 80px 0 0">&nbsp;</span>
<span style="margin:0 170px 0 0">&nbsp;</span>
<input type="reset" value="Reset" class="click" style="border:none;"/>
<input type="submit" name="saveonly" value="RETURN" class="click" style="border:none; "/>

</div>
<div class="tot-ven"></div>

</div>



</div>
</form>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>