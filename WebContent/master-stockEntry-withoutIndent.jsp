<%@page import="beans.headofaccounts"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>STOCK PURCHASE ENTRY_WITH OUT PURCHASE ORDER</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 minDate: '0',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>

<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	
	var count=$('#addcount').val();
	/* if(count>0){
		for(var i=0;i<count;i++){
	if(($('#purchaseRate'+i).val().trim()=="")){
		$('#purchaseRate'+i).addClass('borderred');
		$('#purchaseRate'+i).focus();
		return false;
	}}} */
	if(($('#purchaseRate0').val().trim()=="")){
		$('#purchaseRate0').addClass('borderred');
		$('#purchaseRate0').focus();
		return false;
	}
	$.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } });
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function combochange(denom,desti,jsppage) { 
    var com_id = denom; 
    var hoaid=$("#HOA").val();
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	$.post(jsppage,{ id: com_id,hoaId: hoaid  } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	var s=document.createElement('option');
	s.text="Select PO ID";
	s.value="";
	sda1.add(s);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");
	
				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
				sda1.add(y,null);
				}
				catch(e)
				{
				sda1.add(y);
				}
	}
	}
	}); 
}
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function vendorsearch(){
	  var hoid=$('#HOA').val();
	  var data1=$('#vendorId').val();
	  var arr = [];
	  $.post('searchVendor.jsp',{q:data1,hoa : hoid},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
				 doctorIds:d[0],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		 $( '#vendorId').autocomplete({source: availableTags,
			 focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
				select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				//combochange(ui.item.doctorIds,"indentId","getIndents.jsp");
				}	 
		 
		 }); 
			});
}
function productsearch(j){
	  var hoid=$('#HOA').val();
	  var data1=$('#product'+j).val();
	  var arr = [];
	  $.post('searchProducts.jsp',{q:data1,hoa: hoid},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[2],
			        vat:d[3],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				//$('#purchaseRate'+j).val(ui.item.amount);
				//$('#vat'+j).val(ui.item.vat);
				
			}
		}); 
			});
} 
function calculate(i){
	var price=0.00;
	var Totalprice=0.00;
	var grossprice=0.00;
	var vatper=0.00;
	var qunt=0;
	var u=0;
	u=document.getElementById("addcount").value;
	
	if((document.getElementById("IGSTname"+i).value)!=null && (document.getElementById("IGSTname"+i).value)!='0' && (document.getElementById("IGSTname"+i).value)!=''){
		//alert('111111111');
		document.getElementById("CGSTname"+i).readOnly = true;
		document.getElementById("SGSTname"+i).readOnly = true;
		
	}
	else if(((document.getElementById("CGSTname"+i).value)!=null && (document.getElementById("CGSTname"+i).value)!='0' && (document.getElementById("CGSTname"+i).value)!='') || ((document.getElementById("SGSTname"+i).value)!=null && (document.getElementById("SGSTname"+i).value)!='0' && (document.getElementById("SGSTname"+i).value)!='')){
		document.getElementById("IGSTname"+i).readOnly = true;
	}
	if(((document.getElementById("GSTOption"+i).value)=='0') && ((document.getElementById("CGSTname"+i).value)!='0'|| (document.getElementById("IGSTname"+i).value)!='0' || (document.getElementById("SGSTname"+i).value)!='0')){
		alert('Please Change GST % value');
		/* document.getElementById("CGSTname"+i).readOnly = true;
		document.getElementById("SGSTname"+i).readOnly = true; */
		document.getElementById("CGSTname"+i).value='0'
		document.getElementById("SGSTname"+i).value='0'
		document.getElementById("IGSTname"+i).value='0'
		return false;
	}
	
	if(document.getElementById("CGSTname"+i).value=='0' && document.getElementById("SGSTname"+i).value=='0' && document.getElementById("IGSTname"+i).value=='0'){
		
		document.getElementById("IGSTname"+i).readOnly = false;
		document.getElementById("CGSTname"+i).readOnly = false;
		document.getElementById("SGSTname"+i).readOnly = false;
	}
	
	if(document.getElementById("product"+i).value!=""){
		if(document.getElementById("purchaseRate"+i).value!="" && document.getElementById("vat"+i).value!="")
		{
			for(var j=0;j<u;j++){
			if(document.getElementById("purchaseRate"+j).value!=""){
				/* grossprice=Number((parseFloat(document.getElementById("purchaseRate"+j).value)*parseFloat(document.getElementById("quantity"+j).value))+((parseFloat(document.getElementById("purchaseRate"+j).value)*parseFloat(document.getElementById("quantity"+j).value)))*parseFloat(document.getElementById("vat"+j).value)/100).toFixed(2); */
			grossprice=Number(((parseFloat(document.getElementById("purchaseRate"+j).value)+parseFloat(document.getElementById("CGSTname"+j).value)+parseFloat(document.getElementById("SGSTname"+j).value)+parseFloat(document.getElementById("IGSTname"+j).value)).toFixed(2)*parseFloat(document.getElementById("quantity"+j).value))+((((parseFloat(document.getElementById("purchaseRate"+j).value)+parseFloat(document.getElementById("CGSTname"+j).value)+parseFloat(document.getElementById("SGSTname"+j).value)+parseFloat(document.getElementById("IGSTname"+j).value)).toFixed(2))*parseFloat(document.getElementById("quantity"+j).value)))*parseFloat(document.getElementById("vat"+j).value)/100).toFixed(2);
				document.getElementById("grossAmt"+j).value=grossprice;
			Totalprice=Number(parseFloat(Totalprice)+parseFloat(grossprice)).toFixed(2);
	
				}
				}
			document.getElementById("total").value=Totalprice;
		}
	}
	else{
		document.getElementById('product'+i).className = 'borderred';
		document.getElementById('product'+i).placeholder  = 'Please Enter Product';
		document.getElementById("product"+i).focus();
	}
	document.getElementById("check"+i).checked = true;

}
function addOption(){
	$("#department").empty();
	//$("#vendorId").val(" ");
	var hoaid=$("#HOA").val();
	if(hoaid=="3"){ 
		$("#department").append('<option value=godown>Godown</option>');
		/* $("#department").append('<option value=shop>Shop</option>'); */
	}else if(hoaid=="4"){
		$("#department").append('<option value=santhanstores>Sansthan stores</option>');
	}else if(hoaid=="1"){
		$("#department").append('<option value=santhanstores>Sansthan stores</option>');
		$("#department").append('<option value=medicalstore>Medical store</option>');
	}else if(hoaid=="5"){
		$("#department").append('<option value=sainivasgodown>SHRI SAINIVAS GODOWN</option>');
	}
}
function getPoDetails(){
	var poid=$("#indentId").val();
	var venId=$("#vendorId").val();
	var hoaid=$("#HOA").val();
	//alert(venId);
	$("#sjpoid").val(poid);
	$("#vid").val(venId);
	$("#hoid").val(hoaid);
	$("#poidReload").submit();
	//location.reload('godwanForm.jsp?poid='+poid); 
	//$('#reload').reload(window.location+'.godwanForm.jsp?poid='+poid);
}
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<jsp:useBean id="PO" class="beans.purchaseorder"></jsp:useBean>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<form name="poidReload" id="poidReload" action="godwanForm.jsp" method="get">
<input type="hidden" name="hoid" id="hoid" value=""></input>
<input type="hidden" name="poid" id="sjpoid" value=""></input>
<input type="hidden" name="vid" id="vid" value=""></input>
</form>

<form name="tablets_Update" method="post" action="godwanInsert.jsp" onsubmit="return validate();">
<div class="vendor-page">
<input type="hidden" name="entryType" value="MasterWithOutIndent"></input>
<div class="vendor-box">
<div class="vender-details">
<table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Master Stock Entry-With Out Purchase Order</td></tr>
				</table>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>MSE-ID</td>
<td><div class="warning" id="dateError" style="display: none;">Please select date.</div>Date<span style="color: red;">*</span></td>
<td>Head account <span style="color: red;">*</span></td>
<td ><div class="warning" id="vendorError" style="display: none;">Please select vendor.</div>Vendor<span style="color: red;">*</span></td>
<td><div class="warning" id="billError" style="display: none;">Please select a bill number.</div>Bill no<span style="color: red;">*</span></td>
</tr>
<%mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
headofaccountsListing HOAL=new headofaccountsListing();
List HA_List=HOAL.getActiveHOA();
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
String mseInvId="";
if(session.getAttribute("headAccountId").toString().equals("5")){
	mseInvId=ng_LST.getidWithoutUpdatingNoGen("sainivas_mse_invId");
}else{
	mseInvId=ng_LST.getidWithoutUpdatingNoGen("invoice");
}
%>
<tr>
<td><input type="text" name="mseID" value="<%=mseInvId%>" readonly="readonly"></input></td>
<td><input type="text" name="date" id="date" value="<%=currentDate%>" style="width: 80px;" /></td>
<td><select name="HOA" id="HOA" onchange="addOption();">
<%if(session.getAttribute("headAccountId").toString().equals("5")){ %>
<option value="5"  <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals("5")){ %> selected="selected" <%} %> >SHRI SAI NIVAS</option>
<%}else{ 
for(int i=0;i<HA_List.size();i++){
	HOA=(headofaccounts)HA_List.get(i);
%>
<option value="<%=HOA.gethead_account_id() %>" <%if(request.getParameter("hoid")!=null && request.getParameter("hoid").equals(HOA.gethead_account_id())){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
	<%}} %>
</select></td>

<td>
<input type="text" name="vendorId" id="vendorId" value="" onkeyup="vendorsearch()" autocomplete="off" placeHolder="Find a vendor here" required/></td>
<td><input type="text" name="billNo" id="billNo" value="" required/></td>
</tr>
<tr>
<td>Department<span style="color: red;">*</span></td>
<td>Description<span style="color: red;">*</span></td>
<td>DC Number</td>
<td>BILL APPROVAL</td>
</tr>
<tr>
<td>
<select name="department" id="department">

</select></td>
<td><input type="text"  name="narration" id="narration" value="" required/></td>
<td><input type="text" name="dcNumber" id="dcNumber" value=""></input></td>
<td><input type="radio" name="billApprovalCon" id="billApprovalCon" value="BillApproveRequire" checked="checked"/>Required<input type="radio" name="billApprovalCon" id="billApprovalCon" value="BillApproveNotRequire"/>Not-Required</td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>
<div class="vendor-list">

<div style="clear:both;"></div>
<table width="100%" border="0" cellspacing="0"   cellpadding="0" style="" >
 <thead><tr><th style="width:1%;" class="bgcolr">S.No</th>
<th style="width:13%;" class="bgcolr">Product Name</th>
<th style="width:14%;" class="bgcolr">Description</th>
<th style="width:10%;" class="bgcolr">HSN</th>
<th style="width:14%;" class="bgcolr">Price</th>
<th style="width:8%;" class="bgcolr">GST@</th>
<th style="width:10%;" class="bgcolr">CGST</th>
<th style="width:10%;" class="bgcolr">SGST</th>
<th style="width:10%;" class="bgcolr">IGST</th>
<!-- <th style="width:10%;" class="bgcolr" align="center"><span style="margin-left:20px;">Vat</span></th> -->
<th style="width:10%;" class="bgcolr">Qty</th>
<th style="width:10%;" class="bgcolr">Gross</th>


</tr></thead>
<%for(int i=0;i<50;i++){ %>
<tr>
 <td colspan="11"  ><div id="addcolumn<%=i%>" <%if(i>4){ %>style="display: none;"<%} %> >
 <table width="100%" border="0" cellspacing="0"    >
 <tr>
<td style="width:5%; padding:5px 0 !important;" align="center"><%=i+1%></td>
<input type="hidden" name="countSj" id="addcount" value="5"/>
<input type="hidden" name="count11" id="check<%=i%>"  value="<%=i %>"/>
<td style="width:10%;padding:5px 10px 0px 0px !important;" align="center"><input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>')" autocomplete="off" class="" value="" placeHolder="Find a product Here" style="width:140px;"/></td>
<td style="width:10%;padding:5px 0 !important;" align="center"><input type="text" name="description<%=i%>" id="description<%=i%>"  value="" style="width:120px;"></input></td>
<td style="width:8%;padding:5px 0 !important;" align="center"><input type="text" name="HSENum<%=i%>" id="HSENum<%=i%>" autocomplete="off" style="width:70px;"></input></td>

<td style="width:13%;padding:5px 0 !important;" align="center">&#8377;<input type="text" name="purchaseRate<%=i%>" id="purchaseRate<%=i%>" onkeyup="calculate('<%=i %>')" value=""  autocomplete="off" style="width:120px;"></input></td>
<td style="width:5%;padding:5px 10 !important;" align="center">
<select name="GSTOption<%=i%>" id="GSTOption<%=i%>" onchange="calculate('<%=i %>')">
	<option value="0">0&#x25;</option>
	<option value="5">5&#x25;</option>
	<option value="12">12&#x25;</option>
	<option value="18">18&#x25;</option>
	<option value="28">28&#x25;</option>
</select>
</td>
<td style="width:10%;padding:5px 0 !important;" align="center"><input type="text" name="CGSTname<%=i%>" id="CGSTname<%=i%>" value="0" autocomplete="off"  onkeyup="calculate('<%=i %>')" style="width:70px;"></input></td>  
<td style="width:11%;padding:5px 0 !important;" align="center"><input type="text" name="SGSTname<%=i%>" id="SGSTname<%=i%>" value="0" autocomplete="off"  onkeyup="calculate('<%=i %>')" style="width:70px;"></input></td>
<td style="width:7%;padding:5px 0 !important;" align="center"><input type="text" name="IGSTname<%=i%>" id="IGSTname<%=i%>" value="0" autocomplete="off"  onkeyup="calculate('<%=i %>')" style="width:70px;"></input></td>
<%-- <td style="width:10%;padding:5px 0 !important;" align="center"><input type="text" name="vat<%=i%>" id="vat<%=i%>"  value="0" onkeyup="calculate('<%=i %>')" style="width:50px;padding:5px 0 !important;"/></td> --%>
<input type="hidden" name="vat<%=i%>" id="vat<%=i%>"  value="0" onkeyup="calculate('<%=i %>')" style="width:50px;padding:5px 0 !important;"/>
<td style="width:10%;padding:5px 0 !important;" align="center"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>" autocomplete="off" onkeyup="calculate('<%=i %>')" value="1" style="width:50px;"></input><span style="font-size: 12px;color: red;"></span></td>
<td style="width:10%;padding:5px 0 !important;" align="center">&#8377;<input type="text" name="grossAmt<%=i%>" id="grossAmt<%=i%>" value="0.00" readonly="readonly" style="width:50px;"></input></td>
</tr >
</table>
</div></td>


</tr></table>
<%} %>
<div class="add-ven" align="right">Total</div>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/></div>
<div class="tot-ven"> <input type="text" name="total" id="total" value="0.00" readonly="readonly" style="width:50px;"/></div>

<input type="submit" value="Submit" class="click" style="border:none; float:right; margin:0 115px 0 0"/>
</div>


<input type="hidden" name="bid" id="bid" value="<%=request.getParameter("bid")%>"/>


</div>
</form>

<script>
window.onload=function a(){
		addOption();
	};
	function print()
	{ 
		if(document.getElementById("bid").value != 'null'){
			newwindow1=window.open('billimage_form.jsp?bid='+document.getElementById("bid").value,'BillImageUpload','height=300,width=800,menubar=yes,status=yes,scrollbars=yes');
		newwindow=window.open('printReceipt.jsp?bid='+document.getElementById("bid").value+'&recName=purchaseEntry','name','height=400,width=500,menubar=yes,status=yes,scrollbars=yes');
			if (window.focus) {newwindow.focus();}
	}
	}
	window.onload=print();
	window.onload=addOption();
</script>
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>