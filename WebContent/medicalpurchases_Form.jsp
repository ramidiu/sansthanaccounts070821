
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 600;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#NameError').hide();
	$('#BillError').hide();
	$('#NarrationError').hide();

	if($('#vendorName').val().trim()==""){
		$('#NameError').show();
		$('#vendorName').focus();
		return false;
	}
	if($('#vendorBill').val().trim()==""){
		$('#BillError').show();
		$('#vendorBill').focus();
		return false;
	}
	if($('#Narration').val().trim()==""){
		$('#"NarrationError').show();
		$('#Narration').focus();
		return false;
	}
	if(($('#price0').val().trim()=="" || $('#tabletname0').val().trim()=="") || ($('#quantity0').val().trim()=="")){
		$('#tabletname0').addClass('borderred');
		$('#tabletname0').focus();
		return false;
	}
}
function calculate(i){

	var unitprice=0.0;
	var price=0.0;
	var Totalprice=0.0;
	var quantity=0;
	if(document.getElementById("tabletname"+i).value!=""){ 
		document.getElementById('tabletname'+i).className ='';
	if(document.getElementById("priceunit"+i).value=="" && document.getElementById("quantity"+i).value!="")
		{
		if(document.getElementById("price"+i).value!="")
		{
		
			price=document.getElementById("price"+i).value;
			quantity=document.getElementById("quantity"+i).value;
			unitprice=Number(parseFloat(price)/parseFloat(quantity));
			document.getElementById("priceunit"+i).value=unitprice;
			Totalprice=Number(parseFloat(document.getElementById("total").value)+parseFloat(price));
			document.getElementById("total").value=Totalprice;
			
		}
		}
	else if(document.getElementById("price"+i).value=="" && document.getElementById("quantity"+i).value!="")
		{
		if(document.getElementById("priceunit"+i).value!="")
		{
	
			unitprice=document.getElementById("priceunit"+i).value;
			quantity=document.getElementById("quantity"+i).value;
			price=Number(parseFloat(unitprice)*parseFloat(quantity));
			document.getElementById("price"+i).value=price;
			Totalprice=Number(parseFloat(document.getElementById("total").value)+parseFloat(price));
			document.getElementById("total").value=Totalprice;
		}
		}
	else if((document.getElementById("price"+i).value!="" && document.getElementById("priceunit"+i).value!="") && (document.getElementById("quantity"+i).value==""))
	
	{
	

		unitprice=document.getElementById("priceunit"+i).value;
		price=document.getElementById("price"+i).value;
		quantity=Number(parseFloat(price)/parseFloat(unitprice));
		document.getElementById("quantity"+i).value=quantity;
		Totalprice=Number(parseFloat(document.getElementById("total").value)+parseFloat(price));
		document.getElementById("total").value=Totalprice;
	
	}
	document.getElementById("check"+i).checked = true;
	}
	else{
		document.getElementById('tabletname'+i).className = 'borderred';
		document.getElementById('tabletname'+i).placeholder  = 'Tablet Name';
		document.getElementById("tabletname"+i).focus();
	}
}
</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
$(document).ready(function(){
  $("#vendorName").keyup(function(){
	  var data1=$("#vendorName").val();
	  $.post('searchVendor.jsp',{q:data1},function(data)
				 {
		 var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var doctorIds=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 doctorIds.push(d[0]);
			 doctorNames.push(d[0] +" "+ d[1]);
		 }
		 //alert(availableTags[0]);
		// alert(availableTags.length);
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=doctorNames;
		//var names=availableTags 
		 //var names[]
		 $( "#vendorName" ).autocomplete({source: availableTags}); 
			});
  });
});
</script>

<script>
function tabletsearch(j){
		  var data1=$('#tabletname'+j).val();
		  $.post('searchTablet.jsp',{q:data1},function(data)
					 {
			
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			//alert(availableTags);
 			 $( '#tabletname'+j).autocomplete({source: availableTags}); 
				});
} 
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j-5;
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
</script>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>

<%doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<jsp:useBean id="DOC" class="beans.doctordetails"/>
<form name="tablets_Update" method="post" action="medicalpurchases_Insert.jsp" onsubmit="return validate();">
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title">Purchase Entry</div>
<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="35%">
<div class="warning" id="NameError" style="display: none;">Please Provide  "Vendor Name".</div>
Vendor Name*</td>
<td >
<div class="warning" id="BillError" style="display: none;">Please Provide  "Bill No".</div>
Bill No*</td>
</tr>
<tr>
<td><input type="text" name="vendorName" id="vendorName" value="" /></td>
<td><input type="text" name="vendorBill" id="vendorBill" value="" /></td>
</tr>
<tr>
<td>
<div class="warning" id="NarrationError" style="display: none;">Please Provide  "Narration".</div>
Narration*</td>
</tr>
<tr>
<td> <textarea  name="Narration" id="Narration"   ></textarea></td>
</tr>

</table>

</div>
<div style="clear:both;"></div>
</div>

<div class="vendor-list">

<div class="sno1 bgcolr">S.No.</div>
<div class="ven-nme3 bgcolr">Medicine Name</div>
<div class="ven-nme3 bgcolr">Description</div>
<div class="ven-amt1 bgcolr">Quantity</div>
<div class="ven-nme1 bgcolr">Price Per Unit</div>
<div class="ven-nme1 bgcolr">Price</div>
<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 100; i++ ){%>
<div id="addcolumn<%=i%>" <%if(i>4){ %>style="display: none;"<%} %>>

<div class="sno1"><%=i+1%></div>
 
<div class="ven-nme3"><input type="text" name="tabletname<%=i%>" id="tabletname<%=i%>"  onkeyup="tabletsearch('<%=i %>')" class="" /></div>
<div class="ven-nme3"><input type="text" name="desc<%=i%>" id="desc<%=i%>"   /></div>
<div class="ven-amt1"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>" onKeyPress="return numbersonly(this, event,true);" onblur="calculate('<%=i%>')"style="width:50px;" /></div>
<div class="ven-nme1"><input type="text" name="priceunit<%=i%>" id="priceunit<%=i%>" onKeyPress="return numbersonly(this, event,true);" onblur="calculate('<%=i%>')" /></div>
<div class="ven-nme1"><input type="text" name="price<%=i%>" id="price<%=i%>" onKeyPress="return numbersonly(this, event,true);"  onblur="calculate('<%=i%>')"/></div>
</div>
<%}%>

 <div class="add-ven" >
<input type="button" name="button" value="Add More" onclick="addmore( )" class="click"/>
<input type="submit" value="Insert" class="click"  />
</div> 

<div class="tot-ven">
Total Bill Amount<input type="text" name="total" id="total" value="0.0" style="width:80px;"/>
</div> 

 

</div>

</div>
</form>
<!-- main content -->

<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>