<%@page import="beans.shopstock"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<jsp:useBean id="EMPLY" class="beans.employees"></jsp:useBean>
<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DAILY STOCK ISSUSES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( ".list-details" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
    </script>
    <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script language="javascript" type="text/javascript">

function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
<script>
function showstockaprve(id){
	$('#'+id).toggle();
}
</script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="SHS" class="beans.shopstock" />
		<jsp:useBean id="SHS1" class="beans.shopstock" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form action="shopTransferList.jsp" method="post">
				<div class="search-list">
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
			<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr><td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 11px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">DAILY STOCK ISSUSES REPORT</td></tr>
						<tr>
							<td  width="10%" style="font-weight: bold;">SNo</td>
							<td  colspan="2" width="21%" style="font-weight: bold;">DATE</td>
							<td  colspan="2" width="21%" style="font-weight: bold;">TRANSFER ID</td>
							<td  colspan="2" width="26%" style="font-weight: bold;">TRANSFERED BY</td>
						</tr>
					</table>
					<div id="tblExport">
	<%
	shopstockListing  SSK_L=new shopstockListing();
	productsListing PRD_L=new productsListing();
	employeesListing EMP_L=new employeesListing();
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");	
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");
	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+"";
	String toDate=currentDate+"";
	if(request.getParameter("fromDate")!=null){
		fromDate=request.getParameter("fromDate");
	}
	if(request.getParameter("toDate")!=null){
		toDate=request.getParameter("toDate");
	}
	List EMPL=EMP_L.getMemployees(session.getAttribute("empId").toString());
	if(EMPL.size()>0){
		EMPLY=(beans.employees)EMPL.get(0);
	}
	List SHOPTR_list=null;
	if(EMPLY.getextra1().equals("Sainivas StoreKeeper")){
	SHOPTR_list=SSK_L.getgShopstockTransferBasedMajorHead(fromDate,toDate,session.getAttribute("headAccountId").toString(),"81");
	} else if(EMPLY.getextra1().equals("SainivasRestaurant")) {
		SHOPTR_list=SSK_L.getgShopstockTransferBasedMajorHead(fromDate,toDate,session.getAttribute("headAccountId").toString(),"88");
	} else{
		SHOPTR_list=SSK_L.getgShopstockTransfer(fromDate,toDate,session.getAttribute("headAccountId").toString());
	}
	List TRNSList_INVBased=null;
	double totalAmount=0.00;
	DecimalFormat df = new DecimalFormat("0.00");
	if(SHOPTR_list.size()>0){%>	
		<%for(int i=0;i<SHOPTR_list.size();i++){
		SHS=(shopstock)SHOPTR_list.get(i);
		TRNSList_INVBased=SSK_L.getTransferReportBasedonStockTransaferInvoiceIdAndEmp(SHS.getextra3(),session.getAttribute("empId").toString());%>
		<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="bg" width="10%"><%=i+1 %></td>
							<td  colspan="2" width="21%" class="bg" style="font-weight: bold;"><a onclick="showstockaprve('<%=SHS.getextra3()%>')" style="cursor: pointer;" ><%=chngDateFormat.format(dateFormat2.parse(SHS.getforwardedDate()))%></a></td>
							<td colspan="2" align="left"  width="21%" class="bg" style="font-weight: bold;"><a onclick="showstockaprve('<%=SHS.getextra3()%>')" style="cursor: pointer;" ><%=SHS.getextra3()%></a></td>
							<td colspan="2" align="left" width="26%" class="bg" style="font-weight: bold;"><%=EMP_L.getMemployeesName(SHS.getemp_id()) %></td>
						</tr>
		</table>
		<div id="<%=SHS.getextra3()%>" style="display: block">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td  width="5%" style="font-weight: bold;">S.NO.</td>
				<td  width="20%" style="font-weight: bold;">Transfer NO</td>
				<td  width="20%" style="font-weight: bold;">Product Code/Name</td>
				<td  width="20%" style="font-weight: bold;">Quantity</td>
			</tr>
			<%for(int s=0;s<TRNSList_INVBased.size();s++){
				SHS1=(shopstock)TRNSList_INVBased.get(s);%>
				<tr>
					<td><%=s+1 %></td>
					<td><%=SHS1.getextra3() %></td>
					<td><%=SHS1.getproductId()%>-<%=PRD_L.getProductsNameByCat1(SHS1.getproductId(),"")%></td>
					<td><%=SHS1.getquantity()%></td>
				</tr>
			<%} %>
		</table>
				</div>
		<%} %>
			<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There are no transfer's list found!</span></div>
					</div>
			<%}%>
			</div>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>