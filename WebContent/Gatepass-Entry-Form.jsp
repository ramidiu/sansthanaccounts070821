<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GATEPASS ENTRY | SHRI SHIRDI SAI BABA SANSTHAN TRUST,DSNR</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function addOption(){
	$("#purpose").empty();
	var hoaid=$("#headAccountId").val();
	if(hoaid=="3"){
		$("#purpose").append('<option value=godown>Shop sales</option>');
	}else if(hoaid=="4" || hoaid=="1"){
		$("#purpose").append('');
		$("#purpose").append('<option value=SAIANNADANAM>SAIANNADANAM</option>');
		$("#purpose").append('<option value=SAI PRASAD>SAI PRASAD</option>');
		$("#purpose").append('<option value=MAINTENANCE>MAINTENANCE</option>');
		$("#purpose").append('<option value=TEA,BREAKFAST>TEA,BREAKFAST</option>');
		$("#purpose").append('<option value=STATIONARY&ELECTRICAL>STATIONARY&ELECTRICAL</option>');
		$("#purpose").append('<option value=SAIPOOJA ITEMS>SAIPOOJA ITEMS</option>');
		$("#purpose").append('<option value=OTHER>OTHER PURPOSE</option>');
	}
}
function productsearch(j){
	  var data1=$('#product'+j).val();
	  var hoid=$('#headAccountId'+j).val();
	  var arr = [];
	  $.post('searchProducts.jsp',{q:data1,hoa : hoid},function(data)
	{
		var response = data.trim().split("\n");
		 var doctorNames=new Array();
		 var amount=new Array();
		 var amount1=new Array();
		 var vat=new Array();
		 var qty=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 amount.push(d[2]);
			 vat.push(d[3]);
			 doctorNames.push(d[0] +" "+ d[1]);
			 arr.push({
				 label: d[0]+" "+d[1],
			        amount:d[4],
			        vat:d[5],
			        qty:d[6],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=arr;
		//alert(arr.length);
		$('#product'+j).autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				
			
			}
		}); 
			});
} 
</script>

</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
<form method="post" action="Gatepass_Insert.jsp">
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"> </div>
<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="3" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;font-size: 10px;"> (Regd.No.646/92)</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;">GATEPASS ENTRY FORM</td></tr>

<%mainClasses.employeesListing EMP_l=new mainClasses.employeesListing();
String EMP_HOA=EMP_l.getMemployeesHOA(session.getAttribute("empId").toString());
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();  %>
<tr>
<td width="35%">Date (yyyy-mm-dd)*</td>
<td width="35%">Gatepass Type*</td>
<td width="35%">Gatepass giving to*</td>
</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<tr>
<td><input type="text" name="date"  value="<%=currentDate%>" readonly="readonly"/></td>
<td><select  name="gatepass_type">
		<option value="product for repair">Product for repair</option>
	 </select></td>
	 <td><input name="gatepass_taken_by" required value="" placeHolder="Enter the gatepass receiver name" onkeyup="javascript:this.value=this.value.toUpperCase();"/></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>


<div class="vendor-list">
<div class="sno1 bgcolr">S.No.</div>
<div class="ven-nme1 bgcolr">HEAD OF ACCOUNT</div>
<div class="ven-nme1 bgcolr">PRODUCT</div>
<div class="ven-nme1 bgcolr">SUB-CATEGORY</div>
<div class="ven-amt1 bgcolr">NARRATION</div>
<div class="ven-amt1 bgcolr">QTY</div>
<div class="clear"></div>
<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 100; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">
<div class="sno1"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1">
<select name="headAccountId<%=i%>" id="headAccountId<%=i%>"  style="width:200px;"  <%if(i==0){ %> required <%} %>>
	<%if(HOA_List.size()>0){
		for(int s=0;s<HOA_List.size();s++){
		HOA=(beans.headofaccounts)HOA_List.get(s);
		%>
		<option value="<%=HOA.gethead_account_id()%>"> <%=HOA.getname() %> </option>
	<%}} %>
</select>
</div>
<div class="ven-nme1">
	<input type="text" name="product<%=i%>" id="product<%=i%>"  onkeyup="productsearch('<%=i%>')" class="" value="" autocomplete="off" <%if(i==0){ %> required <%} %>/>
</div>
<div class="ven-nme1">
	<select name="subcat<%=i%>" <%if(i==0){ %> required <%} %>>
		<option value="OUTWARD">OUTWARD</option>
		<option value="INWARD">INWARD</option>
		
	</select>
</div>
<div class="ven-amt1"><textarea name="narration<%=i%>" placeHolder="Enter your narration here" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea></div>
<div class="ven-amt1"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  onblur="calculate('<%=i %>')" value="" style="width:50px;" <%if(i==0){ %> required <%} %> onkeypress="return numbersonly(this, event,true);" ></input></div>
<div class="clear"></div>
</div>
<%} %>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/>
<input type="submit" id="submit" value="SUBMIT" class="click" style="border:none; float:right; margin:0 500px 0 0"/>
</div>

</div>
</div>
</form>
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>