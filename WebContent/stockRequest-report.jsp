<%@page import="mainClasses.stockrequestListing"%>
<%@page import="mainClasses.shopstockListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shop Stock List</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
	</script>
	<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->

<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="REQ" class="beans.stockrequest" />
		<jsp:useBean id="SHP" class="beans.shopstock" />
		<jsp:useBean id="PRD" class="beans.products" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<form action="stockRequest-report.jsp" method="post">
				<%
					String onlyFromDate = "";
					String onlyToDate = "";
					if(request.getParameter("fromDate") != null && !request.getParameter("fromDate").equals(""))
					{
						onlyFromDate = request.getParameter("fromDate");
					}
					if(request.getParameter("toDate") != null && !request.getParameter("toDate").equals(""))
					{
						onlyToDate = request.getParameter("toDate");
					}
					
				%>
				<div class="search-list">
					<ul>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=onlyFromDate%>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=onlyToDate%>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div>
				</form>
				<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span> <span>
						<ul>
							<li><img src="images/Setting-icon.png" />
								<div class="mini-menu">
									<dl>
										<dt style="color: #666; font-size: 12px; font-weight: bold;">Edit
											Colunms</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Address
										</dt>
										<dt>
											<input type="checkbox" class="edit-setting" />Email
										</dt>
									</dl>
								</div></li>
						</ul>

					</span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	shopstockListing SHPL=new shopstockListing();
	productsListing PRD_l=new productsListing ();
	stockrequestListing STOCL=new stockrequestListing();
	String fromDate=request.getParameter("fromDate");
	String toDate=request.getParameter("toDate");
	if(fromDate!=null){
		fromDate=fromDate+" 00:00:01";
	}
	if(toDate!=null){
		toDate=toDate+" 23:59:59";
	}
	List STOC_Req=STOCL.getstockrequestBasedOnHOA(session.getAttribute("headAccountId").toString(),fromDate,toDate,"");
	if(request.getParameter("fromDate") != null && !request.getParameter("fromDate").equals("") && request.getParameter("toDate") != null && !request.getParameter("toDate").equals(""))
	{
		if(STOC_Req.size()>0){ %>
      <style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:97%; 
    background:#d0e3fb; 
}

</style>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
	  				<form action="indentReport.jsp" method="post">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr><td class="yourID" colspan="7" style="padding:0px; margin:0px;"><table width="100%" cellpadding="0" cellspacing="0">
<tr>
							<%
							System.out.println(session.getAttribute("Role"));
							if(session.getAttribute("Role")!=null && session.getAttribute("Role").equals("PO")  || session.getAttribute("Role").equals("AsstPO")){%>
							<td class="bg" width="5%" style="font-weight: bold;"></td>
							<%} %>
							<td class="bg" width="7%" style="font-weight: bold;">S.NO</td>
							<td class="bg" width="23%" style="font-weight: bold;">REQUESTED DATE</td>
							<td class="bg" width="20%" style="font-weight: bold;">PRODUCT NAME.</td>
							<td class="bg" width="10%" style="font-weight: bold;">PRODUCT QTY</td>
							<td class="bg" width="15%" style="font-weight: bold;">STATUS</td>
							<td class="bg" width="20%" style="font-weight: bold;">REQUEST TYPE</td> 
							</tr>
</table></td></tr>
						<%for(int i=0; i < STOC_Req.size(); i++ ){
							REQ=(beans.stockrequest)STOC_Req.get(i); %>
						<tr>
							<%if(session.getAttribute("Role")!=null && session.getAttribute("Role").equals("PO") || session.getAttribute("Role").equals("AsstPO")){%>
							<td class="bg" width="5%" style="font-weight: bold;"><input type="checkbox" name="req_id" value="<%=REQ.getreq_id()%>"></input></td>
							<%} %>
							<td width="7%"><%=REQ.getreqinv_id()%></td>
							<td width="23%"><%=chngDateFormat.format(dbDateFormat.parse(REQ.getreq_date()))%></td>
							<td width="20%"><%=PRD_l.getProductsNameByCat1(REQ.getproduct_id(),REQ.gethead_account_id())%></td>
							<td width="10%"><span><a href=""><%=REQ.getquantity()%></a></span></td>
							<td width="15%"><%=REQ.getstatus()%></td>
							<td width="20%"> <%=REQ.getreq_type()%></td>
						</tr>
						<%} %>
						<%if(session.getAttribute("Role")!=null && session.getAttribute("Role").equals("PO")  || session.getAttribute("Role").equals("AsstPO")){%>
							<tr>
							<td colspan="7"><input type="submit" name="submit" value="Raise Indent" class="click"></input></td>
							</tr>
						<%} %>
					</table>
					</form>
					<%}else{ %>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There were no request found !</span></div>
					</div>
					<%}}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>