<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.patientsentry"%>
<%@page import="mainClasses.patientsentryListing"%>
<%@page import="mainClasses.appointmentsListing"%>
<%@page import="beans.medicineissues"%>
<%@page import="mainClasses.medicineissuesListing"%>
<%@page import="mainClasses.employeesListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="beans.banktransactions"%>
<%@page import="mainClasses.banktransactionsListing"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.customerpurchasesListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="mainClasses.appointmentsListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DailyMedicalReports</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
 <link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( ".printable" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
</head>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-list">
<div class="arrow-down"><img src="images/Arrow-down.png"></div>
<!-- <div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div> -->
<form action="DailyMedicalReportNew.jsp" method="post">
				<div class="search-list">
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li><input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly/></li>
						<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
<div class="icons">
<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span></div>
<div class="clear"></div>
<div class="list-details">
<%
/* MailBox.SendMailBean sendMail=new MailBox.SendMailBean();
sms.CallSmscApi SMS=new sms.CallSmscApi(); */
DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat  dateFormat_new= new SimpleDateFormat("dd MMM yyyy");
mainClasses.doctordetailsListing DOCT = new mainClasses.doctordetailsListing(); 
mainClasses.appointmentsListing APPL=new mainClasses.appointmentsListing();
mainClasses.medicineissuesListing MED_L=new mainClasses.medicineissuesListing();
beans.medicineissues MEDI=new beans.medicineissues();
TimeZone tz = TimeZone.getTimeZone("IST");
DecimalFormat df = new DecimalFormat("#.##");
DecimalFormat unitsformat = new DecimalFormat("#.##");
dateFormat2.setTimeZone(tz.getTimeZone("IST"));
dateFormat_new.setTimeZone(tz.getTimeZone("IST"));
Calendar c1 = Calendar.getInstance(); 
String currentDate=(dateFormat2.format(c1.getTime())).toString();
String presentdate=(dateFormat_new.format(c1.getTime())).toString();
//String fromDate=currentDate+" 00:00:01";
//String toDate=currentDate+" 23:59:59";
double totalprice,doctrwiseprice=0.0;
String message="";
/*	String fromDate="2015-05-02 00:00:01";
String toDate="2015-05-02 23:59:59";*/
int totPatients=0;
doctrwiseprice=totalprice=0.0;
beans.appointments APP=new beans.appointments();
List Medicineissue=null;
List MedicineissueList=null;
/* String fromDate="2018-03-01 00:00:00";
String toDate="2018-03-15 23:59:59"; */
SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
Calendar c2 = Calendar.getInstance(); 
TimeZone t = TimeZone.getTimeZone("IST");
DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String currentDate1=(dateFormat2.format(c1.getTime())).toString();
String fromDate=currentDate+" 00:00:01";
String toDate=currentDate+" 23:59:59";
if(request.getParameter("fromDate")!=null){
	fromDate=request.getParameter("fromDate")+" 00:00:01";
}
if(request.getParameter("toDate")!=null){
	toDate=request.getParameter("toDate")+" 23:59:59";
}
List DOCT_List=APPL.getappointmentsBasedOnDoctor(fromDate,toDate);
String s1="<table width=960 border=1 align=center cellpadding=0 cellspacing=0 style=border-left:1px solid #8b421b;border-bottom:1px solid #8b421b;font-size:13px;font-family:Arial, Helvetica, sans-serif;><tr><th style=background:#8b421b;font-size:14px;color:#FFF;height:30px;line-height:30px; colspan=3> SSSST- DAILY MEDICAL REPORT </th></tr>";
String s2="";
String html="";
int totalMedIssued=0;
if(DOCT_List.size()>0){ %>
	<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<%s2=s2;%><tr style=background:#8b421b;><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>S.NO</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;>DOCTOR NAME</th><th style=font-size:13px;font-weight:bold;border-top:1px solid #8b421b;border-right:1px solid #8b421b;> TOTAL PATIENTS VISITED </th></tr>
<%for(int i=0; i < DOCT_List.size(); i++ ){
	APP=(beans.appointments)DOCT_List.get(i);
	totPatients=totPatients+Integer.parseInt(APP.getappointmnetId());

	s2=s2;%><tr style='background:#81f7f3;font-size:12px'><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'><%=(i+1)%></td><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'><%=DOCT.getDoctorName(APP.getdoctorId())%></td><td style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'><%=APP.getappointmnetId()%></td></tr>
	<% MedicineissueList=MED_L.getDailymedicineissuesBasedonDotor(fromDate,toDate,APP.getdoctorId());
	if(MedicineissueList.size()>0){
		doctrwiseprice=0.0;
		s2=s2;%><tr><td colspan='3'><table align='center' border=1 width=100%> <tr style='background:#81f7f3;font-size:12px'><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Medicine Name</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Quantity</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Unit Price</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Total Price</th><th style='color:#8b421b;font-size:13px;font-weight:bold;border-top:1px'>Medical Hall Quantity</th>
	<% for(int k=0; k < MedicineissueList.size(); k++ ){
		MEDI=(beans.medicineissues)MedicineissueList.get(k);
		doctrwiseprice=doctrwiseprice+Double.parseDouble(MEDI.getextra3())*Double.parseDouble(MEDI.getquantity());
		s2=s2;%><tr><td style=padding:5px;><%=MEDI.getextra2()%></td><td style=padding:5px;><%=MEDI.getquantity()%></td><td style=padding:5px;><%=MEDI.getextra3()%></td><td style=padding:5px;>&#8377;<%=df.format(Double.parseDouble(MEDI.getextra3())*Double.parseDouble(MEDI.getquantity()))%>/-</td><td style=padding:5px;><%=unitsformat.format(Double.parseDouble(MEDI.getextra4()))%>Units</td></tr>
<%	}
	totalprice=doctrwiseprice+totalprice;
	s2=s2;%><tr><td style=padding:5px; colspan=3>Medicine Price</td><td style=padding:5px; colspan=1>&#8377;<%=df.format(doctrwiseprice)%>/-</td></tr>
<%	s2=s2;%></table></td></tr><%;} else{
		s2=s2;%><tr><td style='color:#8b421b;font-size:14px;font-weight:bold;border-top:1px;text-align: center;' colspan=3>No,medicine issued</td></tr>
<%		}

}

%>
<%	Medicineissue=MED_L.getDailymedicineissues(fromDate,toDate);
if(Medicineissue.size()>0){
	for(int s=0;s<Medicineissue.size();s++){
		MEDI=(beans.medicineissues)Medicineissue.get(s);
		totalMedIssued=totalMedIssued+Integer.parseInt(MEDI.getquantity());
	}
}%>
<%-- <%=message%>"Sai Sansthan Total Cost of Medicines Issued is Rs.<%=df.format(totalprice)%> and Total Quantity of Medicines issued is <%=totalMedIssued%> and Total No of Patients visited is<%=totPatients%> --%>
<%s2=s2;%><tr><td style=padding:5px; colspan=2>TOTAL NO OF PATIENTS VISITED</td><td style=padding:5px;><%=totPatients%> PATIENTS </td></tr>
<%s2=s2;%><tr><td style=padding:5px; colspan=2>TOTAL QTY OF MEDICINES ISSUED</td><td style=padding:5px;><%=totalMedIssued%> Units</td></tr>
<%s2=s2;%><tr><td style=padding:5px; colspan=2>TOTAL COST OF MEDICINES ISSUED</td><td style=padding:5px;>&#8377; <%=df.format(totalprice)%>/-</td></tr>
<%}else{
%>
	<%s2=s2;%><tr><td style=padding:5px;color:red;font-size:20px; align='center' colspan=3>Sorry,there is no data entered for today</td></tr>
<%}%>

</table>
</html>