<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 1000;
		modalWindow.height = 300;
		modalWindow.content = "<iframe width='1000' height='600' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){
	$('#PatientError ').hide();
	$('#addressError ').hide();
	$('#PhoneError ').hide();
	

	if($('#patientName').val().trim()==""){
		$('#PatientError').show();
		$('#patientName').focus();
		return false;
	}	
	if($('#address').val().trim()==""){
		$('#addressError').show();
		$('#address').focus();
		return false;
	}	
	if($('#phone').val().trim()==""){
		$('#PhoneError').show();
		$('#phone').focus();
		return false;
	}	
	
}
</script>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<!-- main content -->
<div>
<jsp:useBean id="VEN" class="beans.vendors"/>	
<jsp:useBean id="TBL" class="beans.tablets"/>

<jsp:useBean id="HOA" class="beans.headofaccounts"/>	
<div class="vendor-page">
<div class="vender-details">
<%
String vendorId=request.getParameter("id");
mainClasses.vendorsListing VEN_CL = new mainClasses.vendorsListing();
List VEN_List=VEN_CL.getMvendors("1003");
if(VEN_List.size()>0){
	VEN=(beans.vendors)VEN_List.get(0);
%>
<form method="post" action="patientsentry_Insert.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Enter Patient Information</span></td>
</tr>
<tr>

<td width="2%"></td>
<td valign="top">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3">
<div class="warning" id="PatientError" style="display: none;">"Patient Name" Required.</div>
Patient Name*</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="patientName" id="patientName" value="" /></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="addressError" style="display: none;">"Address" Required.</div>
Address*</td>
</tr>
<tr>
<td colspan="3"><textarea  name="address" id="address"></textarea></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="PhoneError" style="display: none;">"Phone No" Required.</div>
Phone No*</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="phone" id="phone" onKeyPress="return numbersonly(this, event,true);" value="" /></td>
</tr>
<tr>
<td colspan="3">
Gender*</td>
</tr>
<tr>
<td colspan="3"><input type="radio" name="gender" value="M" checked="checked" />Male</td>
<td colspan="3"><input type="radio" name="gender" value="F" />Female</td>
</tr>


</table>
</td>
</tr>
<tr>
<td colspan="3"  height="10"></td>
</tr>

<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
</table>
</form>
<%}else{%>

<%} %>
</div>
</div>

</div>
<!-- main content -->


<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>