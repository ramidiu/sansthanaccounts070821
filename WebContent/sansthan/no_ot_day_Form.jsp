<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<link href="style.css" rel="stylesheet" type="text/css" />
 <link href="js/date.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools.v1.11.js"></script>
<script type="text/javascript" src="js/DatePicker.js"></script>
<script type="text/javascript">
window.addEvent('domready', function(){
	$$('.DatePicker').each( function(el){
		new DatePicker(el);
	});
 
});</script>
<script type="text/javascript">
function validate(){
	if(document.getElementById("no_ot_date").value==""){
		alert("select Date");
		document.getElementById("no_ot_date").focus();
		return false;
	}
}
</script>
<div align="center">
<table width="98%" border="0" cellspacing="8" cellpadding="0">
<%if(request.getParameter("status")!=null && request.getParameter("status").toString().equals("false")){ %>
<tr>
<td colspan=2 bgcolor="#eeeeee" style="color: red;">Already inserted, Duplicate not possible</td>
</tr>
<%} %>
<tr>
<td colspan=2 bgcolor="#eeeeee"><strong>Create No OT Day</strong></td>
</tr>
<form name="no_ot_day_Update" method="POST" action="no_ot_day_Insert.jsp" onsubmit="return validate();">
<tr>
<th align="right">Date</th>
<td><input type="text" name="no_ot_date" id="no_ot_date" value="" class="DatePicker" readonly="readonly" /></td>
</tr>

<tr>
<td> </td>
<td ><input type="submit" name="Submit" value="CREATE" /></td>
</tr>
</form>
</table>
</div><div align="center">
<table width="98%" border="0" cellspacing="1" bgcolor="#eeeeee" cellpadding="0">

<jsp:useBean id="NOT" class="beans.no_ot_day"/>

<tr><td colspan=22> <strong>No OT Day's Listing</strong></td></tr>
<tr><th bgcolor="#fffef3">ID</th>
<th bgcolor="#fffef3">Date</th>
<th bgcolor="#fffef3">Delete</th>
</tr>
<%
	mainClasses.no_ot_dayListing NOT_CL = new mainClasses.no_ot_dayListing();
List NOT_List=NOT_CL.getno_ot_day();
for(int i=0; i < NOT_List.size(); i++ ){
	NOT=(beans.no_ot_day)NOT_List.get(i);
%><tr>
<td bgcolor="#FFF" class="attendance_text"><%=NOT.gets_id()%></td>
<td bgcolor="#FFF" class="attendance_text"><%=NOT.getno_ot_date()%></td>
<td bgcolor="#FFF" class="attendance_text"><a href="no_ot_day_Delete.jsp?s_id=<%=NOT.gets_id()%>">Delete</a></td>
</tr>
<%}%>
</table>
</div>