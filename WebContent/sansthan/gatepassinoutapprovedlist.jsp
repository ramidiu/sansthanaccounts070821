<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page import="java.util.List"%>
<%@ page import="mainClasses.GatePassService"%>
<%@ page import="beans.GatePassEntry"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@page import="mainClasses.no_ot_dayListing"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java"
	errorPage=""%>
<link href="style.css" rel="stylesheet" type="text/css" />

<link href="../css/no-more-tables.css" rel="stylesheet" />
<script src="../js/jquery-1.8.2.js"></script>
<!-- <script>
	$(function() {
	var currdate=new Date();
	$( "#datestart" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) 
		{
			$("#dateend").val(selectedDate);
		}
	});
	/* $( "#dateend" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	}); */
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	
</script> -->
<script>
	$(function() {
	var currdate=new Date();
	$( "#datestart" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#dateend" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#dateend" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#datestart" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});	
</script>
<script>
function dutychange(id){
	$('#dutyshiftcheckbox'+id).prop('checked', true);
	
}
function validatesubmit(){
	var total=$('.dutyshiftclass:checked').size();
if(Number(total)>0){
	document.getElementById("emp-attendenceconfirm").action="updateattendance.jsp";
	document.getElementById("emp-attendenceconfirm").submit();
}
else {
	alert("please change duty time atleast one employee");
	return false;
}
	}
</script>

<script type="text/javascript">function datescheck(){
var DaysDiff;

//Date1 = new Date(document.getElementById("dateend").value);
//Date2 = new Date( document.getElementById("cursdate").value);
var myDate = document.getElementById("dateend").value;
	var myDate1 = document.getElementById("cursdate").value;
	
var datestart = document.getElementById("datestart").value;

  
  if (myDate1<=datestart) {
		 alert("End Date must be lessthan the  StartDate");
          document.getElementById("datestart").focus();

return false;
}
	 // alert(document.getElementById("dateend").value);
	    if (myDate1<=myDate) {
		 alert("End Date must be lessthan the  CurrentDate");
          document.getElementById("dateend").focus();

return false;
}
}

function check1(){
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one");
	return false;} else{
	document.getElementById("createOD").action="create_OD_Insert.jsp";
	document.getElementById("createOD").submit();
	}
}


</script>
<script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>

<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css" />
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.widget.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
	<jsp:useBean id="SHT" class="beans.shift_timings" />
	<table width="98%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<form name="yearform" id="yearform"
				action="controlpanel.jsp?page=gatepassinoutapprovedlist"
				method="post">
				<td class="kws_text">From Date :</td>
				<td><input type="text" name="datestart" id="datestart"
					class="DatePicker" readonly="readonly" /></td>
				<td class="kws_text">To Date :</td>
				<td><input type="text" name="dateend" id="dateend"
					class="DatePicker" readonly="readonly" /></td>
				<td><input type="submit" name="submit" value="submit"
					class="submit_button" /></td>
			</form>
		</tr>

	</table>
	<table width="100%" border="0" cellspacing="1" bgcolor="#eeeeee"
		cellpadding="0">
		<tbody>
			<tr>


				<th bgcolor="#fffef3" class="attendance_text">Date</th>
				<th bgcolor="#fffef3" class="attendance_text">Staff ID</th>
				<th bgcolor="#fffef3" class="attendance_text">StaffName</th>
				<th bgcolor="#fffef3" class="attendance_text">Department</th>
				<th bgcolor="#fffef3" class="attendance_text">Intime</th>
				<th bgcolor="#fffef3" class="attendance_text">OutTime</th>
				<th bgcolor="#fffef3" class="attendance_text">Cl Type</th>
				<th bgcolor="#fffef3" class="attendance_text">GatePass CLS</th>
				<th bgcolor="#fffef3" class="attendance_text">GatepassType</th>
			</tr>
			<%
                    List<GatePassEntry> gatePassEntryList=null;
                    SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat dateFormat1=new SimpleDateFormat("dd-MMM-YYYY");
	           String datestart="";
	           if(request.getParameter("datestart")!=null){
	        	    datestart=request.getParameter("datestart"); 
	        	 }
		    String dateend="";
		    if(request.getParameter("dateend")!=null){
		    	dateend=request.getParameter("dateend") ;
		    }
		if(!datestart.equals("") && !dateend.equals("")){
			          String fromDate="";
			          String toDate="";
			          fromDate=datestart+" 00:00:00";
			          toDate=dateend+" 23:59:59";
				          GatePassService gatePassService=new GatePassService();
				          gatePassEntryList =gatePassService.gateUpdatedPassEntryApprovedList(fromDate, toDate);
		}
		if(gatePassEntryList!=null && gatePassEntryList.size()>0){
			for(int i=0;i<gatePassEntryList.size();i++){
				GatePassEntry gatePassEntry=gatePassEntryList.get(i);
				Date createdDate=dateFormat.parse(gatePassEntry.getCreatedDate());
				String formatedDate=dateFormat1.format(createdDate);
		%>
			<tr>
				<td><%=formatedDate%></td>
				<td><%=gatePassEntry.getStaffId()%></td>
				<td><%=gatePassEntry.getStaffName()%></td>
				<td><%=gatePassEntry.getDepartment()%></td>
				<td><%=gatePassEntry.getInTime()%></td>
				<td><%=gatePassEntry.getOuttime()%></td>
				<td><%=gatePassEntry.getClType()%></td>
				<td><%=gatePassEntry.getClnumber()%></td>
				<td><%=gatePassEntry.getGatePassType()%></td>

			</tr>
			<%}
		}
			%>

		</tbody>
	</table>
</body>
</html>