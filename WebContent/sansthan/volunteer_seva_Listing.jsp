<%@page import="beans.volunteerseva"%>
<%@page import="java.util.List"%>
<%@page import="mainClasses.volunteerSevaListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
    <script src="ui/jquery.ui.core.js"></script>
    <script src="ui/jquery.ui.datepicker.js"></script>
<link href="../js/date.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.widget.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script> 
 <script src="js/jquery-1.4.2.js"></script>  
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>  
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script> 

<script type="text/JavaScript">
function VALIDATE()
{  if(document.getElementById("date").value==""){
	alert("Please Select Date");
return false;
}
}</script>

<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
</head>
<body>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table width="980" border="0" cellspacing="0" cellpadding="0" align="center" class="normal_text view">
<tr>
<td  class="orange_heading" colspan="2"><strong>Volunteers Seva's List</strong></td>
</tr>
<tr>
<td colspan=2 height=30> </td>
</tr>
<form name="leavepermission_Update" method="POST" action="controlpanel.jsp?page=volunteer_seva_Listing" onsubmit="return VALIDATE();">
<tr>
<td>Select Date</td>
<td><input type="text" style="width: 105px;" name="date" id="date" value="" placeHolder="Select Date" readonly="readonly"></td>
</tr>
<tr>
<td> </td>
<td ><input type="submit" name="Submit" value="SUBMIT" class="submit_button"/></td>
</tr>
</form>
</table>

<%
  	if(request.getParameter("date")!=null){%>
  	
  	<table width="980" border="0" cellspacing="0" cellpadding="0" align="center" class="normal_text view">

	 <tr>   
         <th>S.No</th>
         	<th >Seva Name </th>
         	<th>SSSST ID </th>
         	<th>Date</th>
        	 <th>Description</th>
          </tr>
          
          <jsp:useBean id="VS" class="beans.volunteerseva"/>
          <%
          mainClasses.volunteerSevaListing VSL = new mainClasses.volunteerSevaListing();

/* List SFT_AllList=SFT_CL.getALLByStaffcategory(request.getParameter("id").replace("@"," ").replace("*","&"),request.getParameter("yearss")); */
List VSL_AllList=VSL.getVolunteerSevasListBasedOnDate(request.getParameter("date"));
if(VSL_AllList.size()!=0)
{
	for(int i=0; i < VSL_AllList.size(); i++ )
	{
		VS=(beans.volunteerseva)VSL_AllList.get(i);%>  
       
       <tr>
<td align="left" height=30><%= i+1 %> </td>
<td><%=VS.getSevaName() %></td>
<td><%=VS.getSssstId() %></td>
<td><%=VS.getDatee() %></td>
<td><%=VS.getExtra1() %></td>
</tr>
<%}}%>
</table>
 <%}%>

          
</body>
</html>