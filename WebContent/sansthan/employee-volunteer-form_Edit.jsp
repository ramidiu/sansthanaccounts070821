<%@page import="helperClasses.DevoteeGroups"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.uniqueIdGroupsListing"%>
<%@page import="helperClasses.DevoteeGroups"%>
<%@page import="beans.sssst_registrations" %>
<%@page import="mainClasses.sssst_registrationsListing" %>
<%@page import="beans.UniqueIdGroup" %>
<%@page import="mainClasses.uniqueIdGroupsListing" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EMPLOYEE VOLUNTEER REGISTRATION || SSSST,DSNR</title>
<!--<link href="../css/acct-style.css" type="text/css" rel="stylesheet" />-->
<script src="js/jquery-1.4.2.js"></script>
<!-- <link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" /> -->
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<link href="../js/date.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.widget.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<style>
	.mytabe td{
	text-align:left;}
	.mytable input{
	padding:7px 0px 7px 0px;}
</style>
<script>

/* $(function() {
	$( "#date" ).datepicker({
		yearRange: '1940:2020',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#date2" ).datepicker({
		yearRange: '1940:2020',
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
}); */

$(function() {
	  $( ".datepicker" ).datepicker({ 
		  yearRange: '1940:2020',
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: 'both',
			buttonImage: "../images/calendar-icon.png",
			buttonText: 'Select Date',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
		  });
	}); 
	
function block(){
	 $.blockUI({ css: { 
	        border: 'none', 
	        padding: '15px', 
	        backgroundColor: '#000', 
	        '-webkit-border-radius': '10px', 
	        '-moz-border-radius': '10px', 
	        opacity: .5, 
	        color: '#fff' 
	    } });
}

function formsumit(){ 
	document.reg.submit();}
</script>

<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/> -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
</head>
<body>
<div class="vendor-page">
<div class="vendor-box">
<jsp:useBean id="SST" class="beans.sssst_registrations"></jsp:useBean>
<jsp:useBean id="UNG" class="beans.UniqueIdGroup"></jsp:useBean>
<% String type = request.getParameter("type"); 
String sssstId = request.getParameter("bid");
sssst_registrationsListing sssst_l = new sssst_registrationsListing();
List sssst_List = sssst_l.getMsssst_registrations(sssstId); 
if(sssst_List.size() > 0){
SST = (sssst_registrations)sssst_List.get(0);
%>
<div class="vender-details">
<form  method="post" action="employee-volunteer-update.jsp" enctype="multipart/form-data" onsubmit="block();">
 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
 <tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST </td>
 <td style="padding-top:30px;"><a href="<%=request.getContextPath()%>/empOrVolImage/<%=SST.getimage() %>" target="_blank">
<img src="<%=request.getContextPath()%>/empOrVolImage/<%=SST.getimage() %>" style="height: 88px;width: 100px;" /></a></td>
 </tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<% if(request.getParameter("type") != null) {%>	
<tr><td colspan="5" align="center" style="font-weight: bold;"><%=type%> REGISTRATION</td></tr>
 <%}%>
</table>
<table width="70%" border="0" cellspacing="0" cellpadding="0" class="mytabe">
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();%>
<tr>
	<td>Sansthan Unique Id</td>
	<td>EMAIL-ID</td>
	<td colspan="1">MOBILE NO <span style="color: red;"></span></td>
</tr>
<tr>
	<td><input type="text" name="receiptNo"  style="height:30px;" value="<%=sssstId %>" readonly></td>
	<td><input type="email" name="email-id"  style="height:30px; margin-left:10px" value="<%=SST.getemail_id()%>" placeHolder="Enter email-id"></td>
	<td><input type="text" name="mobile"   style="height:30px; margin-left:10px" value="<%=SST.getmobile() %>" placeHolder="Enter mobile number"></td>
</tr>
<tr>
	<td>Title</td>
	<td>First Name<span style="color: red;"></span></td>
	<td>Middle Name</td>
	<td>Sur Name<span style="color: red;"></span></td>
	<td>Spouse Name</td>
</tr>
<tr>
	<td><select name="title">
	<option value="Mr" <%if(SST.gettitle().equals("Mr")) {%> selected="selected" <%} %>>Mr.</option>
	<option value="Miss" <%if(SST.gettitle().equals("Miss")) {%> selected="selected" <%} %>>Miss.</option>
	<option value="Mr&miss" <%if(SST.gettitle().equals("Mr&miss")) {%> selected="selected" <%} %>>Mr&miss</option>
	<option value="Master" <%if(SST.gettitle().equals("Master")) {%> selected="selected" <%} %>>Master</option>
	<option value="Baby" <%if(SST.gettitle().equals("Baby")) {%> selected="selected" <%} %>>Baby</option>
	</select></td>
	<td><input type="text" name="firstname" style="height:30px;" value="<%=SST.getfirst_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Firstname"></td>
	<td ><input type="text" name="middlename" style="height:30px; margin-left:10px" value="<%=SST.getmiddle_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Middle name"></td>
	<td ><input type="text" name="lastname" style="height:30px; margin-left:10px" value="<%=SST.getlast_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Sur name"></td>
	<td ><input type="text" name="spousename" style="height:30px; margin-left:10px" value="<%=SST.getspouce_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Spouse name"></td>
</tr>
<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
<tr>
	<td>Father Name</td>
	<td>Mother Name</td>
	<td>Mother Tongue</td>	
</tr>
<tr>
<td><input  type="text" name="fathername" style="height:30px;" value="<%=SST.getFather_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Father Name"></td>
<td><input  type="text" name="mothername" style="height:30px;" value="<%=SST.getMother_name() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Mother Name"></td>
<td><input  type="text" name="mothertongue"  style="height:30px; margin-left:10px" value="<%=SST.getMothertongue() %>" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Mother Tongue"></td>
</tr><%}%> 
<tr>
	<td>Gender</td>
	<td>Date of Birth</td>
	<td>Gothram</td>
	<!-- <td>Star</td> -->
	<td>Pancard Number</td>
</tr>
<tr>
	<td><input  type="radio" name="gender" value="male"  <%if(SST.getgender().equals("male")) {%>checked="checked" <%} %>>Male
	<input  type="radio" name="gender" value="female" <%if(SST.getgender().equals("female")){ %>checked="checked" <%} %>>Female</td>
	<td ><input  type="text" style="width: 105px;height:30px;" name="dob" id="date" class="datepicker" value="<%=SST.getdob() %>" placeHolder="Select Date of birth" readonly></td>
	<td><input  type="text" name="gothram"  style="height:30px;"  value="<%=SST.getgothram() %>" placeHolder="Enter Gothram" onkeyup="javascript:this.value=this.value.toUpperCase();"></td>
	<%-- <td><input  type="text" name="star" style="height:30px;" value="<%=SST.getextra3() %>" placeHolder="Enter Star" onkeyup="javascript:this.value=this.value.toUpperCase();"></td> --%>
	<td><input  type="text" name="pancard"  style="height:30px;" value="<%=SST.getpancard_no() %>" placeHolder="Enter pancard Number"></td>
</tr>
<tr>
	<td>Permanent Address</td>
	<td>Present Address</td>
	<td>Date Of Anniversary</td>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("VOLUNTEER")) {%>
	<td>Service Experience in Sansthan Trust</td><%} %>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
	<td>Technical Qualification</td>
	<td>Caste & Religion</td><%} %>
	
</tr>
<tr>
	<td><textarea name="permanent_address" style="height:30px; margin-left:10px" placeHolder="Enter Permanent address" onblur="javascript:this.value=this.value.toUpperCase();"/><%=SST.getPermanent_address() %></textarea></td>
	<td><textarea name="present_address" style="height:30px; margin-left:10px" placeHolder="Enter Present address" onblur="javascript:this.value=this.value.toUpperCase();"/><%=SST.getPresent_address() %></textarea></td>
	<%-- <td ><input  type="text" style="width: 105px;height:30px; margin-left:10px" name="doa" id="date2" class="datepicker" value="<%=SST.getDoa() %>" placeHolder="Select Date of Anniversary" readonly></td> --%>
	<%if(SST.getDoa() != null && !SST.getDoa().trim().equals("") && !SST.getDoa().trim().equals("-")){ %>
	<%
	String DOAnniv = SST.getDoa();
	String[] tokens = DOAnniv.split("-");
	%>
	<td>
			<select name="doamonth" style="width: 85px;">
			<option value="">-- SELECT --</option>
					<option value="01" <% if(tokens[0].equals("01")){%>selected<% }%>>JAN</option>
					<option value="02" <% if(tokens[0].equals("02")){%>selected<% }%>>FEB</option>
					<option value="03" <% if(tokens[0].equals("03")){%>selected<% }%>>MAR</option>
					<option value="04" <% if(tokens[0].equals("04")){%>selected<% }%>>APR</option>
					<option value="05" <% if(tokens[0].equals("05")){%>selected<% }%>>MAY</option>
					<option value="06" <% if(tokens[0].equals("06")){%>selected<% }%>>JUN</option>
					<option value="07" <% if(tokens[0].equals("07")){%>selected<% }%>>JUL</option>
					<option value="08" <% if(tokens[0].equals("08")){%>selected<% }%>>AUG</option>
					<option value="09" <% if(tokens[0].equals("09")){%>selected<% }%>>SEP</option>
					<option value="10" <% if(tokens[0].equals("10")){%>selected<% }%>>OCT</option>
					<option value="11" <% if(tokens[0].equals("11")){%>selected<% }%>>NOV</option>
					<option value="12" <% if(tokens[0].equals("12")){%>selected<% }%>>DEC</option>
				</select>
				<select name="doadate" style="width: 85px;">
					<option value="<%=tokens[1]%>"><%=tokens[1]%></option>
						<%for(int i=01;i<32;i++){ 
							if(i < 10){%>
					<option value="0<%=i%>">0<%=i%></option>
							<%}else{ %>
					<option value="<%=i%>"><%=i%></option>
						<%} }%>
				</select>
			</td>
			<%}else{ %>
			<td >
			<select name="doamonth1" style="width: 85px;">
				<option value="">Month</option>
				<option value="01">JAN</option>
				<option value="02">FEB</option>
				<option value="03">MAR</option>
				<option value="04">APR</option>
				<option value="05">MAY</option>
				<option value="06">JUN</option>
				<option value="07">JUL</option>
				<option value="08">AUG</option>
				<option value="09">SEP</option>
				<option value="10">OCT</option>
				<option value="11">NOV</option>
				<option value="12">DEC</option>
			</select>
			<select name="doadate1" style="width: 85px;">
				<option value="">Day</option>
					<%for(int i=01;i<32;i++){ 
						if(i < 10){%>
				<option value="0<%=i%>">0<%=i%></option>
						<%}else{ %>
				<option value="<%=i%>"><%=i%></option>
					<%} }%>
			</select>
			</td>
			<%} %>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("VOLUNTEER")) {%>
	<td><input type="text" style="height:30px;" name="service_experinece_in_sansthan_trust" value="<%=SST.getService_experinece_in_sansthan_trust() %>" placeHolder="Enter Service Experience in Sansthan Trust"/></td><%} %>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
	<td><textarea name="technical_qualification" style="height:30px; margin-left:10px" placeHolder="Enter Technical Qualification" onblur="javascript:this.value=this.value.toUpperCase();"/><%=SST.getTechnical_qualification() %></textarea></td>
	<td><input type="text" style="height:30px;margin-left:10px;" name="caste_religion" value="<%=SST.getCaste_religion() %>" placeHolder="Enter Caste & Religion" onblur="javascript:this.value=this.value.toUpperCase();"/></td><%} %>
</tr>
<tr>	
	<td>Identity Proof</td>
	<td>Identity Proof Number</td>
	<% if(request.getParameter("type") != null) {%>
	<td>Education Qualification</td>
	<td>Blood Group</td>
	<%} %>
	<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
	<td>Identification Marks</td>
	<%} %>
</tr>
<tr>
	<td><select  name="idproof1">
		<option value="<%=SST.getidproof_1()%>"><%=SST.getidproof_1()%></option>
		<option value="UID AADHAAR NO">UID AADHAAR NO</option>
		<option value="VOTE ID NO">VOTE ID NO</option>
		<option value="PAN CARD NO">PAN CARD NO</option>
		<option value="PASSPORT NO">PASSPORT NO</option>
		<option value="RATION CARD NO">RATION CARD NO</option>
		<option value="DRIVING LICENSE NO">DRIVING LICENSE NO</option>
		<option value="OTHERS">OTHERS</option>
	 </select> </td>
	 <td><input type="text" name="idproof_num" style="height:30px;" value="<%=SST.getidproof1_num() %>" placeHolder="Enter Identity proof number"></td>
	 <% if(request.getParameter("type") != null) {%>
	 <td><textarea name="education_qualification" style="height:30px; margin-left:10px" placeHolder="Enter Education Qualification" onblur="javascript:this.value=this.value.toUpperCase();"/><%=SST.getEducation_qualification()%></textarea></td>
	 <td><input type="text" name="bloodgroup" style="height:30px; margin-left:10px" value="<%=SST.getBloodgroup()%>" placeHolder="Enter Blood Group"></td>
	 <%} %>
	 <% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
	 <td><textarea name="identification_marks" style="height:30px; margin-left:10px" placeHolder="Enter Identification Marks" onblur="javascript:this.value=this.value.toUpperCase();"/><%=SST.getIdentification_marks()%></textarea></td>
	<%} %>
	<td></td>
</tr>
<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
<tr>
	<td>Previous Experience</td>
	<td>Dependents</td>
	<td>Height</td>
	<td>Weight</td>
	<td>Enclosures</td>
</tr>
<tr>
<td><textarea name="previous_experience" style="height:30px; margin-left:10px" placeHolder="Enter Previous Experience"/><%=SST.getPrevious_experience()%></textarea></td>
<td><textarea name="dependents" style="height:30px; " onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter Dependents"/><%=SST.getDependents()%></textarea></td>
<td><input type="text" name="height" style="height:30px;margin-left:10px" value="<%=SST.getHeight()%>" placeHolder="Enter Height"></td>
<td><input type="text" name="weight" style="height:30px;" value="<%=SST.getWeight()%>" placeHolder="Enter Weight"></td>
<td><textarea name="enclosures" style="height:30px; margin-left:10px" placeHolder="Enter Enclosures"/><%=SST.getEnclosures()%></textarea></td>
</tr><%} %>


<% if(request.getParameter("type") != null && request.getParameter("type").equals("VOLUNTEER")) {%>
<tr >
	<td>Occupation</td>
</tr>
<tr style="padding:0px">
<td colspan="6">

<label><input  type="radio" name="occupation"  style="height:30px;"value="Govt Employee" <%if(SST.getOccupation().equals("Govt Employee")) {%>checked="checked" <%} %>>Govt Employee </label>
<label><input  type="radio" name="occupation" style="height:30px;" value="Private Employee" <%if(SST.getOccupation().equals("Private Employee")) {%>checked="checked" <%} %>>Private Employee</label>
<label><input  type="radio" name="occupation" style="height:30px;" value="Retired Person" <%if(SST.getOccupation().equals("Retired Person")) {%>checked="checked" <%} %>>Retired Person</label>
<label><input  type="radio" name="occupation" style="height:30px;" value="House-Wife" <%if(SST.getOccupation().equals("House-Wife")) {%>checked="checked" <%} %>>House-Wife</label>
<label><input  type="radio" name="occupation" style="height:30px;" value="Student" <%if(SST.getOccupation().equals("Student")) {%>checked="checked" <%} %>>Student</label>
<label><input  type="radio" name="occupation" style="height:30px;" value="Other" <%if(SST.getOccupation().equals("Other")) {%>checked="checked" <%} %>>Other</label>

</td>

</tr>
<tr>
<%}%>

<% if(request.getParameter("type") != null && request.getParameter("type").equals("EMPLOYEE")) {%>
<tr>
	<td>Government Reservation</td>
	<td>If yes,Specify Reservation Category</td> 
	<td>Any Physical Disorder</td>
	<td>If yes,Specify % of Disorder</td>
</tr>	
<tr>	
	<td><input  type="radio" name="govt_reservation" style="margin-left:10px;" value="yes" <%if(SST.getGovt_reservation().equals("yes")) {%>checked="checked" <%} %>>Yes
	<input  type="radio" name="govt_reservation" value="no" <%if(SST.getGovt_reservation().equals("no")){ %>checked="checked" <%} %>>No</td>
	<td><input type="text" name="reservation_cat" style="height:30px;" value="<%=SST.getReservation_cat()%>" placeHolder="Enter Reservation Category"></td>
	<td><input  type="radio" name="physical_disorder" value="yes"  <%if(SST.getPhysical_disorder().equals("yes")) {%>checked="checked" <%} %>>Yes
	<input  type="radio" name="physical_disorder" value="no" <%if(SST.getPhysical_disorder().equals("no")){ %>checked="checked" <%} %>>No</td>
	<td><input type="text" name="percent_of_disorder" style="height:30px;" value="<%=SST.getPercent_of_disorder() %>" placeHolder="Enter Percent Of Disorder"></td>
</tr>
<tr>
	<td>Present Post</td> <!-- for employee designation = Present Post -->
	<td>Pay Scale Of The Post</td> <!-- for employee present pay scale = pay scale of the post -->
	<td>Date Of Appointment</td>
	<td>Basic Pay</td>
</tr>	
<tr>	
	<td><input  type="text" name="designation" style="height:30px;" value="<%=SST.getDesignation() %>" placeHolder="Present Post"></td>
	<td><input type="text" name="present_pay_scale" style="height:30px;" value="<%=SST.getPresent_pay_scale() %>" placeHolder="Enter Pay Scale Of The Post"></td>
	<td ><input  type="text" style="width: 105px;height:30px; margin-left:10px" name="date_of_appointed" class="datepicker" value="<%=SST.getDate_of_appointed() %>" placeHolder="Select Date of Appointment" readonly></td>
	<td><input type="text" name="basic_pay" style="height:30px;" value="<%=SST.getBasic_pay() %>" placeHolder="Enter Basic Pay"></td>
</tr>
<tr>
	<td>EC Resolution No</td> 
	<td>EC Resolution Date</td> 
	<td>TB Resolution No</td>
	<td>TB Resolution Date</td>
	<td>Employee Identification No</td>  
</tr>	
<tr>	
	<td><input  type="text" name="ec_resolution_num" style="height:30px;" value="<%=SST.getEc_resolution_num() %>" placeHolder="Enter EC Resolution No"></td>
	<td><input  type="text" style="width: 105px;height:30px; margin-left:10px" name="ec_resolution_date" class="datepicker" value="<%=SST.getEc_resolution_date() %>" placeHolder="Select EC Resolution Date" readonly></td>
	<td><input  type="text" name="tb_resolution_num" style="height:30px;" value="<%=SST.getTb_resolution_num() %>" placeHolder="Enter TB Resolution No"></td>
	<td><input  type="text" style="width: 105px;height:30px; margin-left:10px" name="tb_resolution_date" class="datepicker" value="<%=SST.getTb_resolution_date() %>" placeHolder="Select TB Resolution Date" readonly></td>
	<td><input type="text" name="employee_identification_num" style="height:30px;" value="<%=SST.getEmployee_identification_num() %>" placeHolder="Enter Emp Identification No"></td>
</tr>
<%} %>
<tr>
	<td>Photo Upload</td>
</tr>
<tr>
	<td><input name="empOrVolImage" id="empOrVolImage" type="file"/>
	<%=SST.getimage() %>

</td>
</tr>
<% }%>	
<tr><td></td></tr>
<tr><td></td></tr>
<tr>
<td colspan="8" padding:10px; margin-top:10px;
margin-bottom:10px;">
	<%-- <%
	List<String> list=DevoteeGroups.getAllDevotees();
	for(int i=0;i<list.size();i++){
	%>
	 <label>
      <input type="checkbox" name="devoteeGroup" value="<%=list.get(i)%>"/><%=list.get(i)%> &nbsp;&nbsp;&nbsp;
     </label>
	<%} %> --%>
     <%-- <% if(request.getParameter("type") != null) {%>
      <div style="display:none"> 
      <label>
      <input type="checkbox"  style="height:30px;" name="devoteeGroup" <%if(request.getParameter("type").equals("EMPLOYEE")) {%> checked="checked" <%} %> value="EMPLOYEE"/>EMPLOYEE &nbsp;&nbsp;&nbsp;
      </label>
      <label>
      <input type="checkbox" style="height:30px;" name="devoteeGroup" <%if(request.getParameter("type").equals("VOLUNTEER")) {%> checked="checked" <%} %> value="VOLUNTEER"/>VOLUNTEER &nbsp;&nbsp;&nbsp;
      </label>
      </div> 
     <%} %> --%>
     <input type="hidden"  style="height:30px;" name="type" value="<%=type%>"/>
</td>

</tr>
<tr><td></td></tr>
<tr> 
<td  align="center" id="save"><input type="submit" value="EDIT" class="submit_button" style="border:none;"/></td>

</tr>

</table>
</form>
</div>
</div>
</div>
</body>
</html>