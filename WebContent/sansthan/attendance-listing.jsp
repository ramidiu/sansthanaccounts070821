<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@page import="mainClasses.no_ot_dayListing"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<link href="style.css" rel="stylesheet" type="text/css" />

<link href="../css/no-more-tables.css" rel="stylesheet"/>
<script src="../js/jquery-1.8.2.js"></script>
<script>
	$(function() {
	var currdate=new Date();
	$( "#datestart" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	});
	$( "#dateend" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	});
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	
</script>
<script>
function checkAll() {
	var select_All = document.getElementById("SelectAll");

	var inputs = document.getElementsByTagName("input");
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1") {
	if(select_All.checked==true)
	inputs[i].checked = true;
	else
	inputs[i].checked = false;
	}
	}
	}
function dutychange(id){
	$('#dutyshiftcheckbox'+id).prop('checked', true);
	
}
function validatesubmit(){
	var total=$('.dutyshiftclass:checked').size();
if(Number(total)>0){
	document.getElementById("emp-attendenceconfirm").action="updateattendance.jsp";
	document.getElementById("emp-attendenceconfirm").submit();
}
else {
	alert("please change duty time atleast one employee");
	return false;
}
	}
</script>
<script>function validate(s){

if(document.getElementById("halfday"+s).checked==true){
//alert("s is halfday"+document.getElementById("halfday"+s).value);
document.getElementById("latehour"+s).value=0.5;
//alert(document.getElementById("latehour"+s).value);
document.getElementById("latetimetype"+s).value="CL";}
else if(document.getElementById("normal"+s).checked==true){
//alert("s is normal");
document.getElementById("latehour"+s).value="";
document.getElementById("latetimetype"+s).value="";}
else if(document.getElementById("hours"+s).checked==true){
//alert("s is  hours");
document.getElementById("latehour"+s).value=1;
document.getElementById("latetimetype"+s).value="hour";}
else if(document.getElementById("casuvals"+s).checked==true){
//alert("s is  casuvals");
document.getElementById("latehour"+s).value=1;
document.getElementById("latetimetype"+s).value="CL";}
else{
document.getElementById("latehour"+s).value=""; document.getElementById("latetimetype"+s).value=document.getElementById("leavetypess"+s).value;}
//if((document.getElementById("normal"+s).checked==true  || document.getElementById("hours"+s).checked==true || document.getElementById("halfday"+s).checked==true ){
 //}

if((document.getElementById("normal"+s).checked==false)&& (document.getElementById("hours"+s).checked==true || document.getElementById("casuvals"+s).checked==true || document.getElementById("halfday"+s).checked==true)){

if(document.getElementById("permisssion"+s).value==""){
alert("seelect Permission ");
document.getElementById("permisssion"+s).focus();
 return false;}}}</script>
<!--  <link href="js/date.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools.v1.11.js"></script>
<script type="text/javascript" src="js/DatePicker.js"></script>
  
<script type="text/javascript">
window.addEvent('domready', function(){
	$$('.DatePicker').each( function(el){
		new DatePicker(el);
	});

});</script> -->
<script type="text/javascript">function datescheck(){
var DaysDiff;

//Date1 = new Date(document.getElementById("dateend").value);
//Date2 = new Date( document.getElementById("cursdate").value);
var myDate = document.getElementById("dateend").value;    // end date
	var myDate1 = document.getElementById("cursdate").value;  //current date
	
var datestart = document.getElementById("datestart").value;   // start date

  
  if (myDate1 <= datestart) {
		 alert("End Date must be lessthan the  StartDate");
          document.getElementById("datestart").focus();

return false;
}
	 // alert(document.getElementById("dateend").value);
	    if (myDate1<=myDate) {
		 alert("End Date must be lessthan the  CurrentDate");
          document.getElementById("dateend").focus();

return false;
}
}
function swap_inout(intime,outtime){
	var swaper="";
	swapper=document.getElementById(intime).value;
	document.getElementById(intime).value=document.getElementById(outtime).value;
	document.getElementById(outtime).value=swapper;
}
</script>
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.widget.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<jsp:useBean id="SHT" class="beans.shift_timings"/>
<table width="100%" border="0" cellpadding="0" cellspacing="0">

<tr><td class="normal_text"><table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
  <form name="yearform" id="yearform" action="controlpanel.jsp" method="post">
  <input type="hidden" name="page" id="page" value="attendance-listing" />
  <%String cursdate="";
  SimpleDateFormat todatforms = new SimpleDateFormat("yyyy-MM-dd");
  Calendar todaycal = Calendar.getInstance();
cursdate=todatforms.format(todaycal.getTime());
  %>
  <input type="hidden" name="cursdate" id="cursdate"  value="<%=cursdate%>" />
    <td class="kws_text">Start Date :</td>
    <td><input type="text" name="datestart" id="datestart"  class="DatePicker" readonly="readonly" /></td>
    <td class="kws_text"> End Date :</td>
    <td><input type="text" name="dateend" id="dateend"  class="DatePicker" readonly="readonly" /></td>
    <td class="kws_text"> Employee Category_type :</td>
    <td><select name="categorytype" id="categorytype" style="width:150px;">
 <option value="">ALL</option>
 <option value="SANSTHAN(SH-1206)">SANSTHAN(SH-1206)</option>
 <option value="HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)">HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)</option>
 <option value="GENERAL ADMINISTRATION DEPARTMENT(SH-1382)">GENERAL ADMINISTRATION DEPARTMENT(SH-1382)</option>
      <option value="CHAPPAL STAND(SH-2784)">CHAPPAL STAND (SH-2784)</option>
      <option value="ACCOUNTS DEPARTMENT(SH-1382)">ACCOUNTS DEPARTMENT (SH-1382)</option>
      <option value="SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)">SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)</option>
 <option value="Laddu Counter(SH-1515)">Laddu Counter(SH-1515)</option>
 <option value="Pooja Stores(SH-1009)">Pooja Stores(SH-1009)</option>
 <option value="MEDICAL CENTRE(SH-1576)">MEDICAL CENTRE(SH-1576)</option>
      <option value="PUBLIC RELATION OFFICER(SH-1382)">PUBLIC RELATION OFFICER(SH-1382)</option>
	  <option value="LIBRARY(1595)">LIBRARY(1595)</option>
	  <option value="SECURITY">SECURITY</option>
	  <option value="PRIVATE SECURITY">PRIVATE SECURITY</option>
	  <option value="SANSTHAN SECURITY">SANSTHAN SECURITY</option>   
	  <option value="SERVICES">SERVICES</option>
	  <option value="SANSTHAN OUTSOURCE EMPLOYEES">SANSTHAN OUTSOURCE EMPLOYEES</option>
	  <option value="PHYSIOTHERAPY CENTRE">PHYSIOTHERAPY CENTRE</option>
	  <option value="CLEANING DEPARTMENT">CLEANING DEPARTMENT</option>
	  <option value="EDUCATION">EDUCATION</option>  
	  <option value="DOCTOR">DOCTOR</option>  
	  <option value="VEDHAPANDIT">VEDHAPANDIT</option>  
	  <option value="LADY SECURITY GAURDS">LADY SECURITY GAURDS</option>    
 </select></td>
    
    <td><input type="submit" name="submit" value="submit" class="submit_button" onClick="return datescheck();" /></td></form>
  </tr>
  
</table>
</td>

</tr>

<tr>
    <td align="left" height="50" class="orange_heading orange_heading_atendec">Employee Attendance<div align="right"><a href="attendence_excelsheet.jsp?datestart=<%=request.getParameter("datestart") %>&dateend=<%=request.getParameter("dateend") %>" ><input type="button" class="submit_button" value="Excel Sheet" /></a></div></td>
  </tr>
  <%if(session.getAttribute("UserId")!=null){
	  boolean z=false;%>
  <tr><td>
  <form name="emp-attendenceconfirm" id="emp-attendenceconfirm" action="employee_inout_confirm.jsp" method="post">
  <table width="100%" border="0" cellspacing="1" bgcolor="#eeeeee" cellpadding="0">
<tr> 
<!--<td width="4%" height="25" bgcolor="#fffef3" class="attendance_text"><input type="checkbox"/></td>-->
<th ><input type="checkbox" name="SelectAll" id="SelectAll" value="yes" onClick="checkAll();"/> </th>
<td width="7%" height="25" bgcolor="#fffef3" class="attendance_text">Id</td>
    <td width="20%" height="25" bgcolor="#fffef3" class="attendance_text">Name</td>
	<td width="16%" height="25" bgcolor="#fffef3" class="attendance_text">Designation</td>
    <td width="14%" bgcolor="#fffef3" class="attendance_text">Duty_Time</td>
    <td width="10%" bgcolor="#fffef3" class="attendance_text">In_Time</td>
    <td width="10%" bgcolor="#fffef3" class="attendance_text">Out_Time</td>
    <td width="6%" bgcolor="#fffef3" class="attendance_text"></td>
    <td width="7%" bgcolor="#fffef3" class="attendance_text">Late_Time/Leave type</td>
    <td width="8%" bgcolor="#fffef3" class="attendance_text">OT</td>
	<td width="2%" bgcolor="#fffef3" class="attendance_text">Permission</td>
	  <!--  <td width="5%" bgcolor="#fffef3" class="attendance_text">Leave type</td>
  <td width="5%" bgcolor="#fffef3" class="attendance_text">Submit</td>-->
</tr>
<jsp:useBean id="SFTT" class="beans.stafftimings"/>
<jsp:useBean id="SFTw" class="beans.stafftimings"/>
<jsp:useBean id="SFTQ" class="beans.stafftimings"/>
<jsp:useBean id="EIO" class="beans.empinout"/>
<%
	String mager_id=session.getAttribute("UserId").toString();
mainClasses.employee_attendenceListing EMA_CL = new mainClasses.employee_attendenceListing();
mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
mainClasses.leavepermissionListing LPR_CL = new mainClasses.leavepermissionListing();
mainClasses.empinoutListing EIO_CL = new mainClasses.empinoutListing();
mainClasses.shift_timingsListing SHT_CL = new mainClasses.shift_timingsListing();
SimpleDateFormat indiaformat = new SimpleDateFormat("dd-MMM-yyyy");
SimpleDateFormat fornmat =new SimpleDateFormat("yyyy");
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

//------------------for yearesdat  datesss start------------------
/*
Calendar yestday = Calendar.getInstance();  
yestday.add(Calendar.DATE, -1);
String yestdays=dateFormat.format(yestday.getTime());*/
//------------------for yearesdat  datesss  end ------------------

//----------------for datesss start ------------------
String datestart="";if(request.getParameter("datestart")!=null){datestart=request.getParameter("datestart"); }
String dateend="";if(request.getParameter("dateend")!=null){dateend=request.getParameter("dateend") ;}
//out.print("datestart"+datestart+"dateend"+dateend); 
//System.out.println("datestart=>"+datestart);
//System.out.println("dateend=>"+dateend);
if(!datestart.equals("") && !dateend.equals("")){
Calendar fromdate = Calendar.getInstance();
Calendar todate = Calendar.getInstance();
 Date date1 = dateFormat.parse(datestart);
  Date date2 = dateFormat.parse(dateend);
fromdate.setTime (date1);todate.setTime (date2);
 int timegap=(int)((todate.getTimeInMillis()-fromdate.getTimeInMillis())/(24 * 60 * 60 * 1000));       
//out.print("datestart"+datestart+"dateend"+dateend+dateFormat.format(fromdate.getTime())+"timegap"+timegap);

//-------------------end--------------------------

long totallate=0;
long totalWork = 0;
boolean x=false;
String xxin="",xxout="";
String categorytype="";if(request.getParameter("categorytype")!=null){ categorytype=request.getParameter("categorytype");}
Calendar cal = Calendar.getInstance();
String currentyears="";
//currentyears=fornmat.format(cal.getTime());
String datestartDummy[]=datestart.split("-");
if(datestartDummy.length>0){
	currentyears=datestartDummy[0];
	}
currentyears="2013";//use the above line if the timings changes
//List SFT_YaerList=SFT_CL.getStaffYearList(currentyears); //getting only year
List SFT_YaerList=SFT_CL.getStaffList(currentyears);
//List SFT_YaerList=SFT_CL.getALLStaffForAttendence(currentyears);  //forall staff

if(SFT_YaerList.size()!=0){       //GETTING YEAR
for(int aa=0; aa < SFT_YaerList.size(); aa++ ){
SFTw=(beans.stafftimings)SFT_YaerList.get(aa);
int count=0;
//from dateloop
//System.out.println("timegap=>"+timegap);
for(int p=0;p<=timegap;p++){ 
//out.print("p is"+p+"::"+timegap);
String yestdays=dateFormat.format(fromdate.getTime());
//out.print("p is"+p+"::;"+dateFormat.format(fromdate.getTime()));
%>
<tr><td colspan="15"class="kws_orange_heading"><%=indiaformat.format(fromdate.getTime())%></td></tr>
<tr><td colspan="15"class="kws_orange_heading"><input type="submit" value="Save Changes" class="submit_button" /></td></tr>
<tr><td colspan="15"class="kws_orange_heading"><input type="submit" name="button" id="button" class="submit_button" value="Change DutyTime" onclick="return validatesubmit();"></input></td></tr>

<%
	List SFT_YaerListE=null;if(request.getParameter("categorytype")!=null && !request.getParameter("categorytype").toString().equals("")){
//out.println(request.getParameter("categorytype")); // it will return categorytype 
		
 SFT_YaerListE=SFT_CL.getStaffcategoryList(request.getParameter("categorytype").toString(),SFTw.getstaff().toString());

// out.println(SFTw.getstaff().toString()); // it will return year 2013
	}
else{ SFT_YaerListE=SFT_CL.getStaffcategoryList(SFTw.getstaff().toString());}


if(SFT_YaerListE.size()!=0){             //GETTInG CATEGORY basedon year
for(int N=0; N <SFT_YaerListE.size(); N++ ){
	SFTQ=(beans.stafftimings)SFT_YaerListE.get(N);
%>
<tr><td colspan="10"class="orange_heading"><%=SFTQ.getstaff()%></td></tr>

<%
	List SFT_AllList=SFT_CL.getALLByStaffcategory(SFTQ.getstaff().toString(),SFTw.getstaff().toString());
  if(SFT_AllList.size()!=0){
  for(int Y=0; Y < SFT_AllList.size(); Y++ ){
SFTT=(beans.stafftimings)SFT_AllList.get(Y);
if(count==0)
	count=(Y+1);
else 
	count=count+1;
//out.print("count is"+count);
List EMP_conts=EIO_CL.getEmpdateexits(yestdays);  //for empinout containf dates ornot
List EMP_LIST=EIO_CL.getEmpAttendenceHours(yestdays,SFTT.getstaff_id().toString());

//out.print("EMP_LIST"+EMP_LIST.size()+"<br>");

if(EMP_conts.size()!=0){
	
if(EMP_LIST.size()!=0){ 
	String ins="",outs=""; 
	int innnnnnn=0; 
	int outttt=0;
for(int em=0; em < EMP_LIST.size(); em++ ){
EIO=(beans.empinout)EMP_LIST.get(em);
if(em==0 ){
	String dd[]=EIO.getemp_timings().toString().split(" ");
String forinnd[]=dd[1].split(":");
ins=forinnd[0]+":"+forinnd[1];
innnnnnn=Integer.parseInt(forinnd[0]); 
//out.print(EIO.getemp_timings()+"::"+ins+"<br>");
}
if(em==(EMP_LIST.size()-1)){String dd1[]=EIO.getemp_timings().toString().split(" ");
String foroutnd[]=dd1[1].split(":");
outs=foroutnd[0]+":"+foroutnd[1];
outttt=Integer.parseInt(foroutnd[0]);
//out.print(EIO.getemplo_id()+"::"+outs+"<br>");
}   }
%><jsp:useBean id="EMA" class="beans.employee_attendenceService"/>

<%
	//---------------------forchecking login statt time andd emp duty start time start---------------------------------
/*String stafsttime[]=SFTT.getstarttime().split(":");String endddttime[]=SFTT.getendtime().split(":");
int stfirst=Integer.parseInt(stafsttime[0]);int stlastst=Integer.parseInt(endddttime[0]);*/

//if(stfirstinnnnnnn)
//---------------------forchecking login statt time andd emp duty start time end---------------------------------


//if(EMA_CL.getduplicates(EIO.getemplo_id().toString(),yestdays,ins,outs,mager_id).equals(""))  //include intime andout time
if(EMA_CL.getduplicates(EIO.getemplo_id().toString(),yestdays,mager_id).equals("")) //exclude intime andout time
{
%>

<jsp:setProperty name="EMA" property="emp_atid" value="<%=EIO.getemplo_id().toString()%>"/>
<jsp:setProperty name="EMA" property="staff_id" value="<%=EIO.getemplo_id().toString()%>"/>
<jsp:setProperty name="EMA" property="staff_intime" value="<%=ins%>"/>
<jsp:setProperty name="EMA" property="staff_outtime" value="<%=outs%>"/>
<jsp:setProperty name="EMA" property="extra8" value="<%=ins%>"/>
<jsp:setProperty name="EMA" property="extra9" value="<%=outs%>"/> 
<jsp:setProperty name="EMA" property="date" value="<%=yestdays%>"/>
<jsp:setProperty name="EMA" property="mager_id" value="<%=mager_id%>"/>
<jsp:setProperty name="EMA" property="extra6" value="<%=SHT_CL.getTimings(SFTT.getstart_period().toString())%>"/>
<%
	x=EMA.insert();
//System.out.println("in Attendance-listing.jsp  OT afterInserting INTO database "+EMA.getextar7() +"++++++++++++++++++++++++++++++++++++++++++++++++++++");
%><%=EMA.geterror()%><%
	}
%>

<%
	} else{ 

//if(EMA_CL.getduplicates(SFTT.getstaff_id().toString(),yestdays,xxin,xxout,mager_id).equals(""))//include intime andout time
if(EMA_CL.getduplicates(SFTT.getstaff_id().toString(),yestdays,mager_id).equals(""))//exclude intime andout time 
{
%>
<jsp:useBean id="EMA1" class="beans.employee_attendenceService"/>
<jsp:setProperty name="EMA1" property="staff_id" value="<%=SFTT.getstaff_id().toString()%>"/>
<jsp:setProperty name="EMA1" property="staff_intime" value="<%=xxin%>"/>
<jsp:setProperty name="EMA1" property="staff_outtime" value="<%=xxout%>"/>
<jsp:setProperty name="EMA1" property="date" value="<%=yestdays%>"/>
<jsp:setProperty name="EMA1" property="mager_id" value="<%=mager_id%>"/>
<jsp:setProperty name="EMA1" property="extra6" value="<%=SHT_CL.getTimings(SFTT.getstart_period().toString())%>"/>
<%
	x=EMA1.insert();
%><%=EMA1.geterror()%><%
	}
%>
<%
	}
List SHT_List=SHT_CL.getshift_timings();
List empinout=EMA_CL.getInOut(SFTT.getstaff_id().toString(),yestdays);

%><jsp:useBean id="EMAA" class="beans.employee_attendence"/>
   <%-- <!-- <form name="emp_form<%=count%>" id="emp_form"  method="GET" action="employee_attendence_Insert.jsp" >--> --%>
<input type="hidden" name="staff_id"  id="staff_id" value="<%=SFTT.getstaff_id()%>"/>
<input type="hidden" name="datesss" id="datesss" value="<%=yestdays%>"/>

<input type="hidden" name="datestart" id="datestart" value="<%=datestart%>"/>
<input type="hidden" name="categorytype" id="categorytype" value="<%=categorytype%>"/>
<input type="hidden" name="dateend" id="dateend" value="<%=dateend%>"/>

<tr>
<!--<td width="4%" height="25" bgcolor="#FFF"  class="attendance_text"><input type="checkbox"/></td>-->
<td data-title="Select" align="center"><input type="checkbox" name="select1" id="select1<%=count%>"value="<%=SFTT.getstaff_id()%>"/><input type="hidden" name="selecttemp"id="selecttemp<%=count%>"value="<%=SFTT.getstaff_id()%>"/></td>
  <td width="7%" height="25" bgcolor="#FFF"  class="attendance_text"><input type="text" name="employee_staffid" id="employee_staffid<%=count%>" value="<%=SFTT.getstaff_id()%>" style="width: 55%" readonly="readonly" /><%
  	//=(Y+1)
  %></td>
    <td width="20%" height="25" bgcolor="#FFF"  class="attendance_text"><%=SFTT.getstaff_name()%></td>
	<td width="16%" height="25" bgcolor="#FFF"  class="attendance_text"><%=SFTT.getdesignation()%></td>
   
	<%
   		String staff_id=SFTT.getstaff_id().toString();
   		String datesss=yestdays;
   		String conform_status="pending";
   		String intime="";
   		String latehour="";
   		String permisssion="",latetimetype="";
   		String laess="";
   		long OT=0;
   		long OT1=0;
   		String Leavepermision="";
   		Leavepermision=LPR_CL.getLeavePermission(datesss,staff_id);
   		if(empinout.size()!=0){
   		for(int i=0; i < 1; i++ ){
   	         EMAA=(beans.employee_attendence)empinout.get(i); 
   			  intime=EMAA.getstaff_intime().toString();
   			  laess=EMAA.getextra5().toString();
   	%>
   	<%--  <td width="14%" bgcolor="#FFF"  class="attendance_text"><input type="text" name="inout_shift" id="inout_shift<%=count%>" value="<%=EMAA.getextra6().toString()%>" maxlength="12" style="width: 85%" readonly="readonly" /></td> --%>
   	                <td data-title="In_Time" align="center">
   	                <input type="hidden" name="inout_shift" id="inout_shift<%=count%>" value="<%=EMAA.getextra6().toString()%>" maxlength="12" style="width: 85%" readonly="readonly" />
                <select name="dutyshift<%=SFTT.getstaff_id() %>" id="dutyshift<%=SFTT.getstaff_id() %>" onchange="dutychange('<%=SFTT.getstaff_id()%>')">
                <option value=""><%=EMAA.getextra6().toString()%>
                </option>
                <%if(EMAA.getconform_status().equals("pending")){
                for(int k=0; k < SHT_List.size(); k++ ){
                	SHT=(beans.shift_timings)SHT_List.get(k); %>
                	  <option value="<%=SHT.getshift_starttitming().trim()%>To<%=SHT.getshift_endtiming().trim()%>"><%=SHT.getshift_starttitming()%> to <%=SHT.getshift_endtiming() %></option>
                	<%}}
                %>
                </select>
                <span style="display: none;">
                 <input type="checkbox" name="dutyshiftcheckbox" id="dutyshiftcheckbox<%=SFTT.getstaff_id() %>" class="dutyshiftclass" value="<%=SFTT.getstaff_id() %>" />
                 <input type="hidden" name="emp_atid<%=SFTT.getstaff_id() %>" id="emp_atid<%=SFTT.getstaff_id() %>"  value="<%=EMAA.getemp_atid()%>"/>
                </span>
                </td>
    <td width="10%" bgcolor="#FFF"  class="attendance_text"><input type="text" name="in_attdence" readonly="readonly" id="in_attdence<%=count%>" value="<%=EMAA.getstaff_intime()%>" style="width: 55%" <% if(!EMAA.getconform_status().equals("pending")) {%> readonly="readonly" <% }%> /></td>
    <td width="10%"  bgcolor="#FFF" class="attendance_text"><input type="text" name="out_attdence" readonly="readonly" id="out_attdence<%=count%>" value="<%=EMAA.getstaff_outtime()%>" style="width: 55%" <% if(!EMAA.getconform_status().equals("pending")) {%> readonly="readonly" <% }%> /></td>
       <td width="6%"  bgcolor="#FFF" class="attendance_text"><input type="button" value="swap" class="submit_button" onclick="swap_inout('in_attdence<%=count%>','out_attdence<%=count%>');" /></td>
	<%
		String stftm[]=(EMAA.getextra6().toString()).split("To");
		if( ! stftm[0].equals("") && !EMAA.getstaff_intime().toString().equals("") && !stftm[1].equals("") && !EMAA.getstaff_outtime().toString().equals("")){ 

		 String staffstart[]=stftm[0].split(":");                                                                                                                                                              
	String staffenter[]=EMAA.getstaff_intime().toString().split(":");
	 String staffend[]=stftm[1].split(":");
	String staffclose[]=EMAA.getstaff_outtime().toString().split(":");

	
//	OLD CODE (This code is for upto 17/3/2017) , from 29/1/2018 same old one is using

	long forleave=(Long.parseLong(staffend[0])-Long.parseLong(staffstart[0]));
	long lomgminutes=(Long.parseLong(staffend[1])-Long.parseLong(staffstart[1]));
	long totalwork=forleave+lomgminutes;
	long diffInHours = Long.parseLong(staffenter[0]) - Long.parseLong(staffstart[0]);
	long diffminutes = Long.parseLong(staffenter[1]) - Long.parseLong(staffstart[1]);
	totallate=(diffInHours *60)+diffminutes;
	if(totallate<-60){
		OT1=-(totallate)/60;
		totallate=0;
	}else if(totallate<=0){
		totallate=0;
	}
	diffInHours = Long.parseLong(staffclose[0]) - Long.parseLong(staffend[0]);
	diffminutes = Long.parseLong(staffclose[1]) - Long.parseLong(staffend[1]);
	OT=(diffInHours *60)+diffminutes;
	if(!intime.equals("") && !Leavepermision.equals("")){
// 		if(LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision).equals("0.5"))
// 			totallate=-270;
	}
	if(OT<0){
		totallate=totallate-(OT);
		OT=OT1;
	}else{
		OT=(OT/60)+OT1;
	} 
	 
	 /* 	 
	// NEW CODE FOR NEW OT (OT START'S FROM 11 HOURES), from 29/1/2018 new code is not using
	
	long dutyStartTimeInHoures = (Long.parseLong(staffstart[0]));
	long dutyStartTimeInMin = (Long.parseLong(staffstart[1]));
	long dutyEndTimeInHoures = (Long.parseLong(staffend[0]));
	long dutyEndTimeInMin = (Long.parseLong(staffend[1]));
	long staffEnterInHoures = (Long.parseLong(staffenter[0]));
	long staffEnterInMin = (Long.parseLong(staffenter[1]));
	long staffCloseInHoures = (Long.parseLong(staffclose[0]));
	long staffCloseInMin = (Long.parseLong(staffclose[1]));
	long totalWorkInHoures = 0;
	long totalWorkInMin = 0;
	
	long startingWorkInHoures = staffEnterInHoures - dutyStartTimeInHoures;
	long startingWorkInMin = staffEnterInMin - dutyStartTimeInMin;
	
	long startingLateInMin = (startingWorkInHoures * 60) + startingWorkInMin;
	
	if(startingLateInMin <= 0){
		startingLateInMin = 0;
	}
	
	long endingWorkInHoures = staffCloseInHoures - dutyEndTimeInHoures;
	long endingWorkInMin = staffCloseInMin - dutyEndTimeInMin;
	
	long extraWorkInMin = (endingWorkInHoures * 60) + endingWorkInMin;
	
	if(extraWorkInMin <= 0){
		totallate = startingLateInMin - extraWorkInMin;
	}
	else{
		totallate = startingLateInMin;
	}
	
	if(startingLateInMin <= 0){
		totalWorkInHoures = staffCloseInHoures - dutyStartTimeInHoures;
		totalWorkInMin = staffCloseInMin - dutyStartTimeInMin;
	}
	else{
		totalWorkInHoures = staffCloseInHoures - staffEnterInHoures;
		totalWorkInMin = staffCloseInMin - staffEnterInMin;
	}
	totalWork = (totalWorkInHoures * 60) + totalWorkInMin;
	
	totalWork = (totalWork/60);
	
	if(totalWork >= 11){
		OT = totalWork - 11;
	} */
	
	 }}	}else{
	%>
	<td width="5%" bgcolor="#FFF"  class="attendance_text"></td>
	<td width="5%" bgcolor="#FFF"  class="attendance_text"></td><%
		}
	%>
	<input type="hidden" name="intime"  id="intime" value="<%=EMAA.getstaff_intime()%>"/>
    <td width="7%" bgcolor="#FFF"  class="attendance_text">
	
    <table border="0" cellspacing="0" cellpadding="0">
 

   <tr>
    <!-- for radio checking -->
<td>
<%



if(EMAA.getconform_status().equals("confirm")){
	if(EMAA.getlate_hours().equals("") && EMAA.getextra5().equals("")){%>
		<input type="text" name="latehour" id="latehour<%=count%>" value="InTime" readonly="readonly" style="border:none; width:50%;" />
<%	}
	else{
	%>
	
	<input type="text" name="latehour" id="latehour<%=count%>" value="<%=EMAA.getlate_hours()%>" readonly="readonly" style="border:none; width:50%;" /><%=EMAA.getextra5() %>
<%
}
	}
else{
	
// 	out.println("getstaff_id ====> "+EMAA.getstaff_id()); 
// 	out.println("getextra5 ====> "+EMAA.getextra5());
// 	out.println("intime =====> "+intime);
// 	out.println("totallate =====> "+totallate);
	
	
	if((!intime.equals("")) && totallate<=15){
%>
<input type="text" name="latehour" id="latehour<%=count%>" value="InTime"readonly="readonly"style="border:none; width:50%;" /><%
	latehour="";
latetimetype = "";
//System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
	}
	else if(EMAA.getextra5().toString().equals("OD")){
	%>
	<input type="text" name="latehour" id="latehour<%=count%>" value="" readonly="readonly" style="border:none; width:50%;" /><%
	latehour="";
	 latetimetype = "";
	// System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
	}

   else if((!intime.equals("")) && totallate>15 && totallate<=60){
 %>
  <input type="text" name="latehour" id="latehour<%=count%>" value="1"readonly="readonly"style="border:none; width:50%;" />LA<%
  	latehour="1";
  latetimetype = "LA";
 // System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
  }
   else if(!intime.equals("") && EMAA.getlate_hours().toString().equals("1") && EMAA.getextra5().toString().equals("LA")){
	   %>
	    <input type="text" name="latehour" id="latehour<%=count%>" value="<%=EMAA.getlate_hours()%>"readonly="readonly" style="border:none; width:50%;"/> LA<%
	    	latehour="1";
	    latetimetype = "LA";
	//    System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
	    }
   else if((!intime.equals("")) && totallate>60 && totallate<=270){
  %>
 <input type="text" name="latehour" id="latehour<%=count%>" value="0.5"readonly="readonly"style="border:none; width:50%;" />CL<%
 	latehour="0.5";
 latetimetype = "CL";
 //System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
   } 
   else if(!intime.equals("") && EMAA.getlate_hours().toString().equals("0.5") && EMAA.getextra5().toString().equals("CL")){
	   %>
	   <input type="text" name="latehour" id="latehour<%=count%>" value="<%=EMAA.getlate_hours()%>"readonly="readonly"style="border:none; width:50%;" />CL <%
	   	latehour="0.5";
	   latetimetype = "CL";
	//   System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
	   }
  else if(!intime.equals("") && EMAA.getlate_hours().toString().equals("1") && EMAA.getextra5().toString().equals("CL")){
 %>
 <input type="text" name="latehour" id="latehour<%=count%>" value="<%=EMAA.getlate_hours()%>"readonly="readonly"style="border:none; width:50%;" />CL <%
 	latehour="1";
 latetimetype = "CL";
// System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
 }
   else if((!intime.equals("")) && totallate > 270){
 %>
  <input type="text" name="latehour" id="latehour<%=count%>" value="1"readonly="readonly"style="border:none; width:50%;" />CL<%
  	latehour="1";
  latetimetype = "CL";
 // System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
  }
    else{
  %>
  <input type="text" name="latehour" id="latehour<%=count%>" value="" readonly="readonly"style="border:none; width:50%;" /><%
  	latehour="";
  latetimetype = "";
 // System.out.println(EMAA.getstaff_id()+"   latehour --> "+latehour+"  latetimetype --> "+latetimetype);
  }
    
  //if(intime.equals("") && EMAA.getextra5().toString().equals(""))//at present
  if(intime.equals("")){
  if(EMAA.getextra5().equals("OD"))
  {
	  latehour="";
	  latetimetype = "OD";
  }
  
  else if(!Leavepermision.equals("")){
	  latetimetype=Leavepermision;
 	 latehour=LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision);
  }
   else{
  latetimetype="Absent";
  }
  %><%=latehour%> <%=latetimetype%>
  <%
 	}
  else if(!intime.equals("") && !Leavepermision.equals("")){
 	if(LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision).equals("0.5")){
 		latetimetype=Leavepermision;
 		latehour=LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision);
 	}
  }else if((!intime.equals("")) && totallate>60){
	  latetimetype="CL";
  }else if((!intime.equals("")) && totallate>15 && totallate<=60){
	  latetimetype="LA";
  } 
  else{
		  latetimetype="";
	  }
}
 %>
 </td></tr> <tr><td colspan="3" height="5"></td></tr>
</table>


    </td>
    <td width="8%" bgcolor="#FFF" class="attendance_text" >
    <%
    	no_ot_dayListing no_otOBJ= new no_ot_dayListing();
    
        List no_otOBJ_L=no_otOBJ.getNno_ot_day(yestdays);
      
      //  System.out.println("Size of th List for no_ot_dayListing "+no_otOBJ_L.size());
        
        if((no_otOBJ_L.size()!=0) || (dateFormat.parse(yestdays).getDay()==4)){
        	
        	OT=0;
       if(!Leavepermision.equals("")){
        	if(LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision).equals("1")){
        		latehour="1.5";
        		}else if(LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision).equals("0.5")){
        			latehour="1";
        			}
        } 
        }
    %>
    <%
    	if(!EMAA.getextar7().toString().equals("")){
    %><%=EMAA.getextar7()%><%
    	}else{
    %><%=OT%><%
    	}
    %>
    </td>
    <td width="2%" bgcolor="#FFF" class="attendance_text" >
	
	<%
			permisssion=EMAA.getlate_hourpermision().toString();
			if(permisssion.equals("Yes") ){
		%>
	<input type="text" name="permisssion" id="permisssion<%=count%>" value="LP"readonly="readonly"style="border:none;"/> 
	<%
 		}else{
 	%>
	<input type="text" name="permisssion" id="permisssion<%=count%>" value="" readonly="readonly"style="border:none;"/><%
		}
	%>
	</td>

	

   <!-- <td width="5%" bgcolor="#FFF" align="center"><%//if(!EMAA.getconform_status().toString().equals("confirm")){if(!EMAA.getconform_status().toString().equals("pending")){%><input type="submit" name="submit" id="submit"value="Submit" onClick="return validate(<%//=count%>);" class="submit_button"/>
   <%//}else{%><input type="submit" name="submit1"value="Edit" onClick="return validate(<%//=count%>);" class="submit_button"/><%//}}%></td>-->
</tr>

  <!-- </form>-->
	<jsp:useBean id="EMAs" class="beans.employee_attendenceService"/>



<%
//System.out.println(EMAA.getstaff_id()+" ======= >  latehour --> "+latehour+"  latetimetype --> "+latetimetype);
/* System.out.println("late hr::"+latehour); */
z=EMAs.update(staff_id,datesss,intime,latehour,permisssion,conform_status,mager_id,latetimetype,OT);%><%=EMAs.geterror()%>

<%}
}   //iff dateis notexist inempinouttabel  
}
  }} 
fromdate.add(Calendar.DATE, +1); 

}}%>
<%}}%>

</table>
</form>
   
  </td></tr>
  
        <%}else{response.sendRedirect("index.jsp");}%>
      </table>
</body></html>

