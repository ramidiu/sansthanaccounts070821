<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="mainClasses.GatePassService"%>
<%@ page import="beans.GatePassEntry"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>GatePassEntryInOut</title>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<section class="table">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				
				
				
					<table  class="table table-bordered" style="width: 750px">
						<tr>
						    <th>Date</th>
						    <th>GatePassEntryId</th>
							<th>StaffName</th>
							<th>Department</th>
							<th>Description</th>
							<th>OutTime</th>
							<th>InTime</th>
							<th>InOut(HH:MM:SS)</th>
							
						</tr>
						<%
						  SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				          GatePassService gatePassService=new GatePassService();
				          List<GatePassEntry> gatePassEntryList=gatePassService.gateUpdatedPassEntryList();
				          for(int i=0;i<gatePassEntryList.size();i++){
				        	  GatePassEntry gatePassEntry=gatePassEntryList.get(i);
				        	  Date outtime=dateFormat.parse(gatePassEntry.getCreatedDate());
				        	  Date intime=dateFormat.parse(gatePassEntry.getUpdatedDate());
				        	  long diffrennce=intime.getTime()-outtime.getTime();
				        	  long diffSeconds = diffrennce / 1000 % 60;
				  			  long diffMinutes = diffrennce / (60 * 1000) % 60;
				  			  long diffHours = diffrennce / (60 * 60 * 1000) % 24;
				         %>
				        
						<tr>
						    <td><%=gatePassEntry.getCreatedDate()%></td>
							<td><%=gatePassEntry.getGatePassEntryId()%></td>
							<td><%=gatePassEntry.getStaffName()%></td>
							<td><%=gatePassEntry.getDepartment()%></td>
							<td><%=gatePassEntry.getDescription()%></td>
							<td><%=gatePassEntry.getOuttime()%></td>
							<td><%=gatePassEntry.getInTime()%></td>
							<td>Hours:<%=diffHours%>&nbsp;Minutes:<%=diffMinutes%>&nbsp;Seconds:<%=diffSeconds%></td>
						</tr>
                  <%
				          }
                  %>
					</table>
				</div>
			</div>
		</div>
	</section>
</body>
</html>