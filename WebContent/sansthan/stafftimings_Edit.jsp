<%@page import="java.util.List"%>
<%@page import="beans.stafftimings_details"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<link href="../js/date.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script src="../js/jquery-1.8.2.js"></script>
<script>
	$(function() {
	var currdate=new Date();
	$( "#DOB" ).datepicker({	
		   yearRange: "1930:2000",
		changeMonth: true,
        changeYear: true,
		showOtherMonths: true,
        selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
        buttonImageOnly: true
		});
	$( "#joindate" ).datepicker({	
		 yearRange: "1990:2018",
		changeMonth: true,
        changeYear: true,
		showOtherMonths: true,
        selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
        buttonImageOnly: true
		});
	$( "#end_period" ).datepicker({	
		 yearRange: "1990:2018",
		changeMonth: true,
       changeYear: true,
		showOtherMonths: true,
       selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "../images/calendar-icon.png",
		buttonText: 'Select Date',
       buttonImageOnly: true
		});
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	
</script>
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.widget.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" class="view">
<jsp:useBean id="SFT" class="beans.stafftimings"/>
<jsp:useBean id="SFTD" class="beans.stafftimings_details"/>
<tr>
<td colspan=2><strong>Edit staff timings</strong></td>
</tr>
<!-- <tr>
<td colspan=2 height=30> </td>
</tr> -->
<form name="stafftimings_Update" method="POST" action="stafftimings_Update.jsp" enctype="multipart/form-data">
 <%
 	String staff_id=request.getParameter("staff_id");
 mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
 List SFT_List=SFT_CL.getMstafftimings(staff_id);
 for(int i=0; i < SFT_List.size(); i++ ){
 SFT=(beans.stafftimings)SFT_List.get(i);
 %>
 <tr>
 <td colspan="2" align="right" style="padding: 15px 15px 15px 0px">
  <img src="<%=request.getContextPath()%>/personalregistry/<%=SFT.getextra1() %>" style="height: 88px;width: 100px;" />
  <br/>
  <span style="    line-height: 0;">EMP No:<%=SFT.getStaff_nid()%></span>
 </td>
 
 </tr>
<tr>
<td>staff_id</td>
<td>
<input type="text" name="staff_ide" id="staff_ide" value="<%=SFT.getstaff_id()%>" readonly="readonly" /></td>
<input type="hidden" name="staff_id" id="staff_id" value="<%=SFT.getstaff_id()%>" /></tr>
<tr>
<td>staff_year</td>
<td><input type="text" name="staff_year" id="staff_year" value="<%=SFT.getstaff_year()%>"  readonly="readonly"/></td>
</tr>
<tr>
<td>Staff Category</td>
<td>
<select name="staff_type" id="staff_type">
<option value="<%=SFT.getstaff_type()%>"><%=SFT.getstaff_type()%></option>
 <option value="SANSTHAN(SH-1206)">SANSTHAN(SH-1206)</option>
 <option value="HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)">HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)</option>
 <option value="GENERAL ADMINISTRATION DEPARTMENT(SH-1382)">GENERAL ADMINISTRATION DEPARTMENT(SH-1382)</option>
      <option value="CHAPPAL STAND(SH-2784)">CHAPPAL STAND (SH-2784)</option>
      <option value="ACCOUNTS DEPARTMENT(SH-1382)">ACCOUNTS DEPARTMENT (SH-1382)</option>
      <option value="SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)">SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)</option>
 <option value="Laddu Counter(SH-1515)">Laddu Counter(SH-1515)</option>
 <option value="Pooja Stores(SH-1009)">Pooja Stores(SH-1009)</option>
 <option value="MEDICAL CENTRE(SH-1576)">MEDICAL CENTRE(SH-1576)</option>
      <option value="PUBLIC RELATION OFFICER(SH-1382)">PUBLIC RELATION OFFICER(SH-1382)</option>
	  <option value="LIBRARY(1595)">LIBRARY(1595)</option>
	  <option value="SHRI SAINIVAS MEGHA DHARMASALA">SHRI SAINIVAS MEGHA DHARMASALA</option>
	    <option value="SECURITY">SECURITY</option>
	   <option value="RESTAURANT">RESTAURANT</option>
	   <option value="PRIVATE SECURITY">PRIVATE SECURITY</option>
	  <option value="SANSTHAN SECURITY">SANSTHAN SECURITY</option>   
	  <option value="SERVICES">SERVICES</option>
	  <option value="SANSTHAN OUTSOURCE EMPLOYEES">SANSTHAN OUTSOURCE EMPLOYEES</option>
	  <option value="PHYSIOTHERAPY CENTRE">PHYSIOTHERAPY CENTRE</option> 
	  <option value="CLEANING DEPARTMENT">CLEANING DEPARTMENT</option>
	  <option value="EDUCATION">EDUCATION</option>  
	  <option value="DOCTOR">DOCTOR</option>  
	  <option value="VEDHAPANDIT">VEDHAPANDIT</option>  
	  <option value="LADY SECURITY GAURDS">LADY SECURITY GAURDS</option>
 </select></td>
</tr>
<tr>
<td>staff_name</td>
<td><input type="text" name="staff_name" id="staff_name" value="<%=SFT.getstaff_name()%>" /></td>
</tr>
<tr>
<td>Father's Name</td>
<td><input type="text" name="father_name" id="father_name" value="<%=SFT.getextra3()%>" /></td>
</tr>
<tr>
<td>Mother's Name</td>
<td><input type="text" name="mother_name" id="mother_name" value="<%=SFT.getextra4()%>" /></td>
</tr>
<tr>
<td>Spouse Name</td>
<td><input type="text" name="spouse_name" id="spouse_name" value="<%=SFT.getExtra5()%>" /></td>
</tr>
<tr>
<td>Mother Tongue</td>
<td><input type="text" name="mother_tongue" id="mother_tongue" value="<%=SFT.getExtra6()%>" /></td>
</tr>
<tr>
<td>designation</td>
<td>
<jsp:useBean id="STD" class="beans.staff_designation"/>
<select name="designation" id="designation" >
<option value="<%=SFT.getdesignation()%>"><%=SFT.getdesignation()%></option>
<%
	mainClasses.staff_designationListing STD_CL = new mainClasses.staff_designationListing();
List STD_List=STD_CL.getstaff_designation();
if(STD_List.size()!=0){
for(int s=0; s < STD_List.size(); s++ ){
STD=(beans.staff_designation)STD_List.get(s);
%>
<option value="<%=STD.getdesignation_name()%>"><%=STD.getdesignation_name()%> </option> <%
 	}}
 %>

</select></td>
</tr>
<tr>
<td>Staff Type</td>
<td>
<select name="status" id="status">
<option value="<%=SFT.getstatus()%>"><%=SFT.getstatus()%></option>
<%
	if(SFT.getstatus().toString().equals("permanent")){
%>
 <option value="Shift">Shift</option><%
 	}
 %>
 <%
 	if(SFT.getstatus().toString().equals("Shift")){
 %><option value="permanent">permanent</option><%
 	}
 %>
</select></td>
</tr>

<tr>
<td>Current Shift</td>
<td>
<jsp:useBean id="SHT" class="beans.shift_timings"/>
<select name="start_period" id="start_period" >
<%
	mainClasses.shift_timingsListing SHT_CL = new mainClasses.shift_timingsListing();
	List SHT_List=SHT_CL.getshift_timings();
	for(int r=0; r < SHT_List.size(); r++ )
	{
	SHT=(beans.shift_timings)SHT_List.get(r);
	%>
 		<option value="<%=SHT.getshift_starttitming()%>,<%=SHT.getshift_endtiming()%>,<%=SHT.getshift_code()%>" <%if(SFT.getstart_period().toString().equals(SHT.getshift_code().toString())){%> selected="selected"<%}%>><%=SHT.getshift_code()%></option>
 	<%}%>
 </select></td>
</tr>
<tr>
<td>Next Week Shift</td>
<td>
<jsp:useBean id="SHT1" class="beans.shift_timings"/>
<select name="start_period1" id="start_period1" >
<%
	mainClasses.shift_timingsListing SHT_CL1 = new mainClasses.shift_timingsListing();
	List SHT_List1=SHT_CL1.getshift_timings();
	for(int r=0; r < SHT_List.size(); r++ )
	{
	SHT1=(beans.shift_timings)SHT_List1.get(r);
	%>
		<%if(!SFT.getNext_week_shift().equals("")){%><option value="<%=SHT1.getshift_code()%>" <%if(SFT.getNext_week_shift().equals(SHT1.getshift_code())){%> selected="selected"<%}%>><%=SHT1.getshift_code()%></option><%}else{%>
													 <option value="<%=SHT1.getshift_code()%>" <%if(SFT.getstart_period().equals(SHT1.getshift_code())){%> selected="selected"<%}%>><%=SHT1.getshift_code()%></option><%}%>
	<%}%>
 </select></td>
</tr>

<tr>
<td>Mobile No</td>
<td>
<input type="text" name="extra2" id="mobileno"  value="<%=SFT.getMobile_no() %>"/></td>
</tr>
<tr>
<td>Permanent Address</td>
<td>
<textarea name="emergencyno" id="emergencyno"><%=SFT.getEmergency_no() %></textarea></td>
</tr>
<tr>
<td> Present Address</td>
<td>
<textarea name="extra1" id="address" ><%=SFT.getAddress() %></textarea></td>
</tr>
<tr>
<td>Blood Group</td>
<td>
<input type="text" name="bloodgroup"   value="<%=SFT.getBlood_group() %>"/></td>
</tr>
<tr>
<td>Date Of Birth</td>
<td>
<input type="text" name="extra3" id="DOB" value="<%=SFT.getDOB() %>" /></td>
</tr>
<tr>
<td>Date Of Appointment In Trust</td>
<td><input type="text" name="joindate" id="joindate" value="<%=SFT.getextra2()%>" /></td>
</tr>
<tr>
<td>Photo Upload</td>
<td>
<input name="profileImage" id="profileImage" type="file"/>
<%=SFT.getextra1() %>
<a href="<%=request.getContextPath()%>/personalregistry/<%=SFT.getextra1() %>" target="_blank"><img src="<%=request.getContextPath()%>/personalregistry/<%=SFT.getextra1() %>" style="height: 88px;width: 100px;" /></a>
</td>
</tr>
<tr>
<td>EMP No</td>
<td><input type="text" name="extra4" id="new_staff_id" value="<%=SFT.getStaff_nid()%>"  /></td>
</tr>
<tr>
<td>Re-Signed Date</td>
<td>
<input type="text" name="end_period"  id="end_period" value="<%=SFT.getend_period()%>"/></td>
</tr>

<%}%>

<%
 	String staff_ids=request.getParameter("staff_id");
 mainClasses.stafftimings_detailsListing SFTD_CL = new mainClasses.stafftimings_detailsListing();
 stafftimings_details staffDetails=SFTD_CL.getMstafftimings_details(staff_ids);
 %>
 <tr>
<td></td>
<td>
<input type="hidden" name="staff_ids" id="staff_ids" value="<%=staff_ids%>" /></tr>
<tr>
<td>Government Reservation</td>
<td>
<input type="radio" name="govern_reser" value="yes" <%if(staffDetails.getGovernment_reservation().equals("yes")){ %>checked="checked"	<%}%> >YES
<input type="radio" name="govern_reser" value="no" <%if(staffDetails.getGovernment_reservation().equals("no")){ %>checked="checked"	<%} %> >NO
</td>
</tr>
<tr>
<td>Caste & Religion</td>
<td>
<input type="text" name="caste_rel"   value="<%=staffDetails.getCaste_regligion() %>"/></td>
</tr>
<tr>
<td> Dependents</td>
<td>
<textarea name="dependent" id="dependent" ><%=staffDetails.getDependents()%></textarea></td>
</tr>
<tr>
<td>Employee Documents</td>
<td>
<input name="document1" id="document1" type="file" />
<%=staffDetails.getExtra1() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra1() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra1() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document2" id="document2" type="file" />
<%=staffDetails.getExtra2() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra2() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra2() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document3" id="document3" type="file" />
<%=staffDetails.getExtra3() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra3() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra3() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document4" id="document4" type="file" />
<%=staffDetails.getExtra4() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra4() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra4() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document5" id="document5" type="file" />
<%=staffDetails.getExtra5() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra5() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra5() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document6" id="document6" type="file" />
<%=staffDetails.getExtra6() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra6() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra6() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document7" id="document7" type="file" />
<%=staffDetails.getExtra7() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra7() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra7() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document8" id="document8" type="file" />
<%=staffDetails.getExtra8() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra8() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra8() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document9" id="document9" type="file" />
<%=staffDetails.getExtra9() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra9() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra9() %>" style="height: 30px;width: 30px;" /></a><br/>
</td>
</tr>
<tr>
<td>Educational Qualification</td>
<td>
<input type="text" name="edu_quali"   value="<%=staffDetails.getEducational_qual() %>"/></td>
</tr>
<tr>
<td>Technical Qualification</td>
<td>
<input type="text" name="tech_quali"   value="<%=staffDetails.getTechnical_qual() %>"/></td>
</tr>
<tr>
<td>Indentifications Marks</td>
<td>
<input type="text" name="ind_mark"   value="<%=staffDetails.getIdentification_marks() %>"/></td>
</tr>
<tr>
<td> Previous Experience</td>
<td>
<textarea name="pre_exper" id="pre_exper" ><%=staffDetails.getPrevious_experience() %></textarea></td>
</tr>
<tr>
<td>Height</td>
<td>
<input type="text" name="height"   value="<%=staffDetails.getHeight() %>"/></td>
</tr>
<tr>
<td> Any Physical Disorder</td>
<td>
<textarea name="phy_disorder" id="phy_disorder" ><%=staffDetails.getPhysical_disorder() %></textarea></td>
</tr>
<tr>
<td>Present Pay Scale</td>
<td>
<input type="text" name="pres_pay_scale"   value="<%=staffDetails.getPresent_pay_scale() %>"/></td>
</tr>
<tr>
<td>Basic Pay</td>
<td>
<input type="text" name="basic_pay"   value="<%=staffDetails.getBasic_pay() %>"/></td>
</tr>
<tr>
<td> Reference Of Any Two Members</td>
<td>
<textarea name="reference" id="reference" ><%=staffDetails.getReference() %></textarea></td>
</tr>
<tr>
<td>Graduvite Amount</td>
<td>
<input type="text" name="graduvite_amount"   value="<%=staffDetails.getGraduvite_amount()%>"/></td>
</tr>
<tr>
<td> </td>
<td ><input type="submit" name="Submit" value="UPDATE" class="submit_button"/></td>
</tr>
</form>
</table>