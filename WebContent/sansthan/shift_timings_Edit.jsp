<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view">

<jsp:useBean id="SHT" class="beans.shift_timings"/>

<tr>
<td colspan=2><strong>Edit shift_timings</strong></td>
</tr>
<tr>
<td colspan=2 height=30> </td>
</tr>
<form name="shift_timings_Update" method="POST" action="shift_timings_Update.jsp">
 <%
 	String shtime_id=request.getParameter("shtime_id");
 mainClasses.shift_timingsListing SHT_CL = new mainClasses.shift_timingsListing();
 List SHT_List=SHT_CL.getMshift_timings(shtime_id);
 for(int i=0; i < SHT_List.size(); i++ ){
 SHT=(beans.shift_timings)SHT_List.get(i);
 %>

<input type="hidden" name="shtime_id" id="shtime_id" value="<%=SHT.getshtime_id()%>" />

<tr>
<td>shift_name</td>
<td><input type="text" name="shift_name" id="shift_name" value="<%=SHT.getshift_name()%>" /></td>
</tr>



<tr>
<td class="normal_text">Staff_Timings</td>
<td align="left">
<table width="75%" border="0" cellspacing="0" cellpadding="0" class="noB">
  <tr>
    <td width="48%" align="center" style="border-right:1px solid #EEE;"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="noB">
  <tr>
    <td colspan="3" class="timings_text">Starting_Timings</td>
    </tr>
  <tr>
    <td width="24%" class="timings_text">HH</td>
    <td width="24%" class="timings_text">MM</td>
  
  </tr>
  <%
  	String actualstartdate[]=SHT.getshift_starttitming().toString().split(":");
  %>
  <tr>
    <td><select name="starttime" id="starttime">
	 <option value="<%=actualstartdate[0]%>"><%=actualstartdate[0]%> </option>
    <option value="00">00</option>
    <option value="01">01</option>
    <option value="02">02</option>
    <option value="03">03</option>
    <option value="04">04</option>
    <option value="05">05</option>
    <option value="06">06</option>
    <option value="07">07</option>
    <option value="08">08</option>
    <option value="09">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
	 <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option> 
    </select></td>
    <td><select name="startmin" id="startmin">
	 <option value="<%=actualstartdate[1]%>"><%=actualstartdate[1]%> </option>
    <option value="00">00</option>
    <option value="01">01</option>
    <option value="02">02</option>
    <option value="03">03</option>
    <option value="04">04</option>
    <option value="05">05</option>
    <option value="06">06</option>
    <option value="07">07</option>
    <option value="08">08</option>
    <option value="09">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option> 
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">1</option>
    <option value="32">32</option>
    <option value="33">33</option>
    <option value="34">34</option>
    <option value="35">35</option>
    <option value="36">36</option>
    <option value="37">37</option>
    <option value="38">38</option> 
    <option value="39">39</option>
    <option value="40">40</option>
    <option value="41">41</option>
    <option value="42">42</option>
    <option value="43">43</option>
    <option value="44">44</option>
    <option value="45">45</option>
    <option value="46">46</option>
    <option value="47">47</option>
    <option value="48">48</option>
    <option value="49">49</option>
    <option value="50">50</option>
    <option value="51">51</option>
    <option value="52">52</option>
    <option value="53">53</option>
    <option value="54">54</option>
    <option value="55">55</option>
    <option value="56">56</option>
    <option value="57">57</option>
    <option value="58">58</option>
    <option value="59">59</option>
  </select></td>
  
  </tr>
</table>
</td>
<td width="2%"></td>
   <td width="50%" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="noB">
  <tr>
    <td colspan="3" class="timings_text">End_Timings</td>
    </tr>
	<%
		String actualstdate[]=SHT.getshift_endtiming().toString().split(":");
	%>
  <tr>
    <td width="24%" class="timings_text">HH</td>
    <td width="24%" class="timings_text">MM</td>
  
  </tr>
  <tr>
    <td><select name="endtime" id="endtime">
	 <option value="<%=actualstdate[0]%>"><%=actualstdate[0]%> </option>
    <option value="00">00</option>
    <option value="01">01</option>
    <option value="02">02</option>
    <option value="03">03</option>
    <option value="04">04</option>
    <option value="05">05</option>
    <option value="06">06</option>
    <option value="07">07</option>
    <option value="08">08</option>
    <option value="09">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
	 <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option> 
    </select></td>
    <td><select name="endmin" id="endmin">
	 <option value="<%=actualstdate[1]%>"><%=actualstdate[1]%> </option>
    <option value="00">00</option>
    <option value="01">01</option>
    <option value="02">02</option>
    <option value="03">03</option>
    <option value="04">04</option>
    <option value="05">05</option>
    <option value="06">06</option>
    <option value="07">07</option>
    <option value="08">08</option>
    <option value="09">09</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option> 
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">1</option>
    <option value="32">32</option>
    <option value="33">33</option>
    <option value="34">34</option>
    <option value="35">35</option>
    <option value="36">36</option>
    <option value="37">37</option>
    <option value="38">38</option> 
    <option value="39">39</option>
    <option value="40">40</option>
    <option value="41">41</option>
    <option value="42">42</option>
    <option value="43">43</option>
    <option value="44">44</option>
    <option value="45">45</option>
    <option value="46">46</option>
    <option value="47">47</option>
    <option value="48">48</option>
    <option value="49">49</option>
    <option value="50">50</option>
    <option value="51">51</option>
    <option value="52">52</option>
    <option value="53">53</option>
    <option value="54">54</option>
    <option value="55">55</option>
    <option value="56">56</option>
    <option value="57">57</option>
    <option value="58">58</option>
    <option value="59">59</option>
  </select></td>
    
  </tr>
</table>
</td>
  </tr>
</table>
</td></tr>
<tr>
<td>shift_code</td>
<td><input type="text" name="shift_code" id="shift_code" value="<%=SHT.getshift_code()%>" /></td>
</tr>


<%
	}
%>
<tr>
<td> </td>
<td ><input type="submit" name="Submit" value="UPDATE"class="submit_button" /></td>
</tr>
</form>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view" style="margin-top:15px;">
<tr>

<th colspan="5" align="left"> SHIFT TIMINGS LIST</th>

</tr>
<tr><td colspan=72 height=30></td></tr>
<tr>
<td>shift Name</td>
<td>star Time</td>
<td>End Time</td>
<td>shift code</td>


<td>Edit</td>

</tr>
<jsp:useBean id="SHT5" class="beans.shift_timings"/>
<%
	mainClasses.shift_timingsListing SHT_CLl = new mainClasses.shift_timingsListing();
List SHT_Listt=SHT_CLl.getshift_timings();
for(int t=0; t < SHT_Listt.size(); t++ ){
SHT5=(beans.shift_timings)SHT_Listt.get(t);
%><tr>

<td><%=SHT5.getshift_name()%></td>
<td><%=SHT5.getshift_starttitming()%></td>
<td><%=SHT5.getshift_endtiming()%></td>
<td><%=SHT5.getshift_code()%></td>

<td><a href="controlpanel.jsp?page=shift_timings_Edit&shtime_id=<%=SHT5.getshtime_id()%>"><img src="images/edit_icon.jpg" alt="Click here to Edit" title="Click here to Edit timings of <%=SHT5.getshift_name()%>" width="15" height="26" border="0" /></a></td>

</tr>
<%}%>
</table>