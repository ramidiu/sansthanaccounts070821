<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.Date"%>  
<%@ page import="mainClasses.GatePassService"%>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Intime</title>
</head>
<body>
<%
   Calendar calendar=Calendar.getInstance();
   Date date=calendar.getTime();
   SimpleDateFormat dateDormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   String updatedDate=dateDormat.format(date);
   String intime=calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE);
   String gatePassEntryId=request.getParameter("gatePassEntryId");
   GatePassService gatePassService=new GatePassService();
   gatePassService.updateGatePassEntry(updatedDate,intime,gatePassEntryId);
   response.sendRedirect("controlpanel.jsp?page=gatepassinout");
%>
</body>
</html>