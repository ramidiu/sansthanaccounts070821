<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@page import="mainClasses.no_ot_dayListing"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<link href="style.css" rel="stylesheet" type="text/css" />

<link href="../css/no-more-tables.css" rel="stylesheet"/>
<script src="../js/jquery-1.8.2.js"></script>
<!-- <script>
	$(function() {
	var currdate=new Date();
	$( "#datestart" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) 
		{
			$("#dateend").val(selectedDate);
		}
	});
	/* $( "#dateend" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	}); */
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	
</script> -->
<script>
	$(function() {
	var currdate=new Date();
	$( "#datestart" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#dateend" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#dateend" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#datestart" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});	
</script>
<script>
function dutychange(id){
	$('#dutyshiftcheckbox'+id).prop('checked', true);
	
}
function validatesubmit(){
	var total=$('.dutyshiftclass:checked').size();
if(Number(total)>0){
	document.getElementById("emp-attendenceconfirm").action="updateattendance.jsp";
	document.getElementById("emp-attendenceconfirm").submit();
}
else {
	alert("please change duty time atleast one employee");
	return false;
}
	}
</script>

<script type="text/javascript">function datescheck(){
var DaysDiff;

//Date1 = new Date(document.getElementById("dateend").value);
//Date2 = new Date( document.getElementById("cursdate").value);
var myDate = document.getElementById("dateend").value;
	var myDate1 = document.getElementById("cursdate").value;
	
var datestart = document.getElementById("datestart").value;

  
  if (myDate1<=datestart) {
		 alert("End Date must be lessthan the  StartDate");
          document.getElementById("datestart").focus();

return false;
}
	 // alert(document.getElementById("dateend").value);
	    if (myDate1<=myDate) {
		 alert("End Date must be lessthan the  CurrentDate");
          document.getElementById("dateend").focus();

return false;
}
}

function check1(){
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one");
	return false;} else{
	document.getElementById("createOD").action="create_OD_Insert.jsp";
	document.getElementById("createOD").submit();
	}
}


</script>
<script>
$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>

<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.widget.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<jsp:useBean id="SHT" class="beans.shift_timings"/>
<table width="100%" border="0" cellpadding="0" cellspacing="0">

<tr><td class="normal_text"><table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
  <form name="yearform" id="yearform" action="controlpanel.jsp" method="post">
  <input type="hidden" name="page" id="page" value="create_OD" />
  <%String cursdate="";
  SimpleDateFormat todatforms = new SimpleDateFormat("yyyy-MM-dd");
  Calendar todaycal = Calendar.getInstance();
cursdate=todatforms.format(todaycal.getTime());
  %>
  <input type="hidden" name="cursdate" id="cursdate"  value="<%=cursdate%>" />
    <td class="kws_text">Start Date :</td>
    <td><input type="text" name="datestart" id="datestart"  class="DatePicker" readonly="readonly" /></td>
    <td class="kws_text"> End Date :</td>
    <td><input type="text" name="dateend" id="dateend"  class="DatePicker" readonly="readonly" /></td>
    <td class="kws_text"> Employee Category_type :</td>
    <td><select name="categorytype" id="categorytype" style="width:150px;">
 <option value="">ALL</option>
 <option value="SANSTHAN(SH-1206)">SANSTHAN(SH-1206)</option>
 <option value="HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)">HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)</option>
 <option value="GENERAL ADMINISTRATION DEPARTMENT(SH-1382)">GENERAL ADMINISTRATION DEPARTMENT(SH-1382)</option>
      <option value="CHAPPAL STAND(SH-2784)">CHAPPAL STAND (SH-2784)</option>
      <option value="ACCOUNTS DEPARTMENT(SH-1382)">ACCOUNTS DEPARTMENT (SH-1382)</option>
      <option value="SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)">SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)</option>
 <option value="Laddu Counter(SH-1515)">Laddu Counter(SH-1515)</option>
 <option value="Pooja Stores(SH-1009)">Pooja Stores(SH-1009)</option>
 <option value="MEDICAL CENTRE(SH-1576)">MEDICAL CENTRE(SH-1576)</option>
      <option value="PUBLIC RELATION OFFICER(SH-1382)">PUBLIC RELATION OFFICER(SH-1382)</option>
	  <option value="LIBRARY(1595)">LIBRARY(1595)</option>
	  <option value="SECURITY">SECURITY</option>
	  <option value="PRIVATE SECURITY">PRIVATE SECURITY</option>
	  <option value="SANSTHAN SECURITY">SANSTHAN SECURITY</option>   
	  <option value="SERVICES">SERVICES</option>
	  <option value="SANSTHAN OUTSOURCE EMPLOYEES">SANSTHAN OUTSOURCE EMPLOYEES</option>
	  <option value="PHYSIOTHERAPY CENTRE">PHYSIOTHERAPY CENTRE</option>
	  <option value="CLEANING DEPARTMENT">CLEANING DEPARTMENT</option>
	  <option value="EDUCATION">EDUCATION</option>  
	  <option value="DOCTOR">DOCTOR</option>  
	  <option value="VEDHAPANDIT">VEDHAPANDIT</option>  
	  <option value="LADY SECURITY GAURDS">LADY SECURITY GAURDS</option>    
 </select></td>
    
    <td><input type="submit" name="submit" value="submit" class="submit_button" /></td></form>
  </tr>
  
</table>
</td>

</tr>
<%
SimpleDateFormat indiaformat = new SimpleDateFormat("dd-MMM-yyyy");
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
%>
<tr>
    <td align="left" height="50" class="orange_heading orange_heading_atendec">Employee Attendance<div align="right"><input type="button" class="submit_button" value="Create OD" onclick="check1();"/><a href="attendence_excelsheet.jsp?datestart=<%=request.getParameter("datestart") %>&dateend=<%=request.getParameter("dateend") %>" ><input type="button" class="submit_button" value="Excel Sheet" /></a></div></td>
  </tr>
  <%if(session.getAttribute("UserId")!=null){
	  boolean z=false;%>
  <tr><td>
  <form name="createOD" id="createOD" action="create_OD_Insert.jsp" method="post">
  <table width="100%" border="0" cellspacing="1" bgcolor="#eeeeee" cellpadding="0">
<tr style="background-color:rgba(131, 151, 2, 0.78);">
<!--<td width="4%" height="25" bgcolor="#fffef3" class="attendance_text"><input type="checkbox"/></td>-->
<td width="7%" height="25" class="attendance_text orange_heading"><input type="checkbox" name="SelectAll" id="selecctall"/>Check All</td>
<td width="7%" height="25" class="attendance_text orange_heading">Id</td>
    <td width="20%" height="25"  class="attendance_text orange_heading">Name</td>
	<td width="16%" height="25"  class="attendance_text orange_heading">Designation</td>
    <td width="14%"  class="attendance_text orange_heading">Duty_Time</td>
    <td width="10%"  class="attendance_text orange_heading">In_Time</td>
    <td width="10%" class="attendance_text orange_heading">Out_Time</td>
    <td width="10%" class="attendance_text orange_heading">Late_Time/Leave type</td>
    <td width="8%" class="attendance_text orange_heading">OT</td>
</tr>
<jsp:useBean id="SFTT" class="beans.stafftimings"/>
<jsp:useBean id="SFTw" class="beans.stafftimings"/>
<jsp:useBean id="SFTQ" class="beans.stafftimings"/>
<jsp:useBean id="EIO" class="beans.empinout"/>
<%
	String mager_id=session.getAttribute("UserId").toString();
mainClasses.employee_attendenceListing EMA_CL = new mainClasses.employee_attendenceListing();
mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
mainClasses.leavepermissionListing LPR_CL = new mainClasses.leavepermissionListing();
mainClasses.empinoutListing EIO_CL = new mainClasses.empinoutListing();
mainClasses.shift_timingsListing SHT_CL = new mainClasses.shift_timingsListing();
SimpleDateFormat fornmat =new SimpleDateFormat("yyyy");

     
//out.print("datestart"+datestart+"dateend"+dateend+dateFormat.format(fromdate.getTime())+"timegap"+timegap);

//-------------------end--------------------------

String datestart="";if(request.getParameter("datestart")!=null){datestart=request.getParameter("datestart"); }
String dateend="";if(request.getParameter("dateend")!=null){dateend=request.getParameter("dateend") ;}

if(!datestart.equals("") && !dateend.equals("")){
Calendar fromdate = Calendar.getInstance();
Calendar todate = Calendar.getInstance();
 Date date1 = dateFormat.parse(datestart);
  Date date2 = dateFormat.parse(dateend);
fromdate.setTime (date1);todate.setTime (date2);
 int timegap=(int)((todate.getTimeInMillis()-fromdate.getTimeInMillis())/(24 * 60 * 60 * 1000));      

long totallate=0;
boolean x=false;
String xxin="",xxout="";
String categorytype="";if(request.getParameter("categorytype")!=null){ categorytype=request.getParameter("categorytype");}
Calendar cal = Calendar.getInstance();
String currentyears="";
//currentyears=fornmat.format(cal.getTime());
String datestartDummy[]=datestart.split("-");
if(datestartDummy.length>0){currentyears=datestartDummy[0];}
currentyears="2013";//use the above line if the timings changes
//List SFT_YaerList=SFT_CL.getStaffYearList(currentyears); //getting only year
List SFT_YaerList=SFT_CL.getStaffList(currentyears);
//List SFT_YaerList=SFT_CL.getALLStaffForAttendence(currentyears);  //forall staff

if(SFT_YaerList.size()!=0){       //GETTING YEAR
for(int aa=0; aa < SFT_YaerList.size(); aa++ ){
SFTw=(beans.stafftimings)SFT_YaerList.get(aa);
int count=0;
//from dateloop
for(int p=0;p<=timegap;p++){ 
//out.print("p is"+p+"::"+timegap);
String yestdays=dateFormat.format(fromdate.getTime());
//out.print("p is"+p+"::;"+dateFormat.format(fromdate.getTime()));

List SFT_YaerListE=null;if(request.getParameter("categorytype")!=null && !request.getParameter("categorytype").toString().equals("")){

SFT_YaerListE=SFT_CL.getStaffcategoryList(request.getParameter("categorytype").toString(),SFTw.getstaff().toString());}
else{ SFT_YaerListE=SFT_CL.getStaffcategoryList(SFTw.getstaff().toString());}

if(request.getParameter("datestart") != null && !request.getParameter("datestart").equals("")){ %><tr><td colspan="10" class="orange_heading" style="background-color:#d0b942;"><%=indiaformat.format(dateFormat.parse(yestdays))%></td></tr><%}%>

<% if(SFT_YaerListE.size()!=0){
           //GETTInG CATEGORY basedon year
for(int N=0; N <SFT_YaerListE.size(); N++ ){
	SFTQ=(beans.stafftimings)SFT_YaerListE.get(N);
%>
<tr><td colspan="10"class="orange_heading"><%=SFTQ.getstaff()%></td></tr>
<%
	List SFT_AllList=SFT_CL.getALLByStaffcategory(SFTQ.getstaff().toString(),SFTw.getstaff().toString());
  if(SFT_AllList.size()!=0){
  for(int Y=0; Y < SFT_AllList.size(); Y++ ){
SFTT=(beans.stafftimings)SFT_AllList.get(Y);
if(count==0)count=(Y+1);
else count=count+1;
//out.print("count is"+count);
List EMP_conts=EIO_CL.getEmpdateexits(yestdays);  //forr empinout containf dates ornot
List EMP_LIST=EIO_CL.getEmpAttendenceHours(yestdays,SFTT.getstaff_id().toString());
  
List SHT_List=SHT_CL.getshift_timings();
List empinout=EMA_CL.getInOut(SFTT.getstaff_id().toString(),yestdays);
%><jsp:useBean id="EMAA" class="beans.employee_attendence"/>
<jsp:useBean id="EMAA1" class="beans.employee_attendence"/>
<%
	if(empinout.size() > 0)
	{
		EMAA1=(beans.employee_attendence)empinout.get(0);
	}
%>
<%-- <!-- <form name="emp_form<%=count%>" id="emp_form"  method="GET" action="employee_attendence_Insert.jsp" >--> --%>
<input type="hidden" name="staff_id"  id="staff_id" value="<%=SFTT.getstaff_id()%>"/>
<input type="hidden" name="datesss" id="datesss" value="<%=yestdays%>"/>

<input type="hidden" name="datestart" id="datestart" value="<%=datestart%>"/>
<input type="hidden" name="categorytype" id="categorytype" value="<%=categorytype%>"/>
<input type="hidden" name="dateend" id="dateend" value="<%=dateend%>"/>

<tr>
<!--<td width="4%" height="25" bgcolor="#FFF"  class="attendance_text"><input type="checkbox"/></td>-->
<td width="7%" height="25" bgcolor="#FFF"  class="attendance_text"><%if(!EMAA1.getconform_status().equals("confirm")){%><input type="checkbox" name="select1" class="checkbox1" value = "<%=SFTT.getstaff_id()%>,<%=yestdays%>"/><%}%></td>
<td width="7%" height="25" bgcolor="#FFF"  class="attendance_text"><input type="text" name="employee_staffid" id="employee_staffid<%=count%>" value="<%=SFTT.getstaff_id()%>" style="width: 55%" readonly="readonly" /><%
  	//=(Y+1)
  %></td>
    <td width="20%" height="25" bgcolor="#FFF"  class="attendance_text"><%=SFTT.getstaff_name()%></td>
	<td width="16%" height="25" bgcolor="#FFF"  class="attendance_text"><%=SFTT.getdesignation()%></td>
   
	<%
   		String staff_id=SFTT.getstaff_id().toString();
   		String datesss=yestdays;String conform_status="pending";
   		String intime="";String latehour="";String permisssion="",latetimetype="";
   		String laess="";long OT=0;long OT1=0;
   		String Leavepermision="";
   		Leavepermision=LPR_CL.getLeavePermission(datesss,staff_id);
   		if(empinout.size()!=0){
   		for(int i=0; i < 1; i++ ){
   	         EMAA=(beans.employee_attendence)empinout.get(i);
   			  intime=EMAA.getstaff_intime().toString();laess=EMAA.getextra5().toString();
   	%>
   	<%--  <td width="14%" bgcolor="#FFF"  class="attendance_text"><input type="text" name="inout_shift" id="inout_shift<%=count%>" value="<%=EMAA.getextra6().toString()%>" maxlength="12" style="width: 85%" readonly="readonly" /></td> --%>
   	                <td data-title="In_Time" align="center">
   	                <input type="hidden" name="inout_shift" id="inout_shift<%=count%>" value="<%=EMAA.getextra6().toString()%>" maxlength="12" style="width: 85%" readonly="readonly" />
                <select name="dutyshift<%=SFTT.getstaff_id() %>" id="dutyshift<%=SFTT.getstaff_id() %>" onchange="dutychange('<%=SFTT.getstaff_id()%>')">
                <option value=""><%=EMAA.getextra6().toString()%>
                </option>
                </select>
                </td>
    <td width="10%" bgcolor="#FFF"  class="attendance_text"><input type="text" name="in_attdence" id="in_attdence<%=count%>" value="<%=EMAA.getstaff_intime()%>" style="width: 55%" readonly="readonly" /></td>
    <td width="10%"  bgcolor="#FFF" class="attendance_text"><input type="text" name="out_attdence" id="out_attdence<%=count%>" value="<%=EMAA.getstaff_outtime()%>" style="width: 55%" readonly="readonly" /></td>
	<%
		String stftm[]=(EMAA.getextra6().toString()).split("To");
		if( ! stftm[0].equals("") && !EMAA.getstaff_intime().toString().equals("") && !stftm[1].equals("") && !EMAA.getstaff_outtime().toString().equals("")){ 

		 String staffstart[]=stftm[0].split(":");
	String staffenter[]=EMAA.getstaff_intime().toString().split(":");
	 String staffend[]=stftm[1].split(":");
	String staffclose[]=EMAA.getstaff_outtime().toString().split(":");

	///fortotal  hours working employee start---------
	long forleave=(Long.parseLong(staffend[0])-Long.parseLong(staffstart[0]));
	long lomgminutes=(Long.parseLong(staffend[1])-Long.parseLong(staffstart[1]));
	long totalwork=forleave+lomgminutes;
	 //out.print("<br>::"+"forleave"+forleave+"::"+"lomgminutes"+lomgminutes+":::totalwork"+totalwork);
	 //out.print("<br>::"+SFTT.getstaff_id()+" Long.parseLong(staffenter[0]) - Long.parseLong(staffstart[0])"+staffenter[0]+"::"+staffstart[0]+"Long.parseLong(staffenter[1]) - Long.parseLong(staffstart[1]"+staffenter[1]+":::"+staffstart[1]+"<br>");
	 ///fortotal  hours working employee endd-------
	long diffInHours = Long.parseLong(staffenter[0]) - Long.parseLong(staffstart[0]);
	long diffminutes = Long.parseLong(staffenter[1]) - Long.parseLong(staffstart[1]);
	totallate=(diffInHours *60)+diffminutes;

	if(totallate<-60){
		OT1=-(totallate)/60;
		totallate=0;
	}else if(totallate<=0){
		totallate=0;
	}

	diffInHours = Long.parseLong(staffclose[0]) - Long.parseLong(staffend[0]);
	diffminutes = Long.parseLong(staffclose[1]) - Long.parseLong(staffend[1]);
	OT=(diffInHours *60)+diffminutes;

	if(!intime.equals("") && !Leavepermision.equals("")){
		if(LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision).equals("0.5"))
			totallate=-240;
	}

	if(OT<0){
		totallate=totallate-(OT);
		OT=OT1;
	}else{
		OT=(OT/60)+OT1;
	}

	//now cjhnged
	// totallate=((diffInHours - (int)diffInHours)*60)+diffminutes; //actual 
	//out.print("<br>::"+"diffInHours"+diffInHours+"::"+"Minutes"+diffminutes+"<br>"+"totallate"+totallate);
	 }}	}else{
	%>
	<td width="5%" bgcolor="#FFF"  class="attendance_text"></td>
	<td width="5%" bgcolor="#FFF"  class="attendance_text"></td><%
		}
	%>
	<input type="hidden" name="intime"  id="intime" value="<%=EMAA.getstaff_intime()%>"/>
    <td width="7%" bgcolor="#FFF"  class="attendance_text">
	
    <table border="0" cellspacing="0" cellpadding="0">
 

   <tr>
    <!-- for radio checking -->
<td>
<%
	if((!intime.equals("")) && totallate<=15){
%>
<input type="text" name="latehour" id="latehour<%=count%>" value="InTime"readonly="readonly"style="border:none; width:50%;" /><%
	latehour="";}
	else if(EMAA.getextra5().toString().equals("OD")){
	%>
	<input type="text" name="latehour" id="latehour<%=count%>" value="" readonly="readonly" style="border:none; width:50%;" /><%
	latehour="";
	}
 else if(!intime.equals("") && EMAA.getlate_hours().toString().equals("1") && EMAA.getextra5().toString().equals("LA")){
%>
 <input type="text" name="latehour" id="latehour<%=count%>" value="<%=EMAA.getlate_hours()%>"readonly="readonly"style="border:none; width:50%;"/> LA<%
 	latehour=EMAA.getlate_hours().toString();}
   else if((!intime.equals("")) && totallate>15 && totallate<=60){
 %>
  <input type="text" name="latehour" id="latehour<%=count%>" value="1"readonly="readonly"style="border:none; width:50%;" />LA<%
  	latehour="1";}
    else if(!intime.equals("") && EMAA.getlate_hours().toString().equals("0.5") && EMAA.getextra5().toString().equals("CL")){
  %>
  <input type="text" name="latehour" id="latehour<%=count%>" value="<%=EMAA.getlate_hours()%>"readonly="readonly"style="border:none; width:50%;" />CL <%
  	latehour=EMAA.getlate_hours().toString();}
   else if((!intime.equals("")) && totallate>60 && totallate<=240){
  %>
 <input type="text" name="latehour" id="latehour<%=count%>" value="0.5"readonly="readonly"style="border:none; width:50%;" />CL<%
 	latehour="0.5";} 
  else if(!intime.equals("") && EMAA.getlate_hours().toString().equals("1") && EMAA.getextra5().toString().equals("CL")){
 %>
 <input type="text" name="latehour" id="latehour<%=count%>" value="<%=EMAA.getlate_hours()%>"readonly="readonly"style="border:none; width:50%;" />CL <%
 	latehour=EMAA.getlate_hours().toString();}
   else if((!intime.equals("")) && totallate>240){
 %>
  <input type="text" name="latehour" id="latehour<%=count%>" value="1"readonly="readonly"style="border:none; width:50%;" />CL<%
  	latehour="1";}
    else{
  %>
  <input type="text" name="latehour" id="latehour<%=count%>" value="" readonly="readonly"style="border:none; width:50%;" /><%
  	latehour="";}
    
  //if(intime.equals("") && EMAA.getextra5().toString().equals(""))//at present
  if(intime.equals("")){
  if(EMAA.getextra5().equals("OD"))
  {
	  latehour="";
	  latetimetype = "OD";
  }
  
  else if(!Leavepermision.equals("")){latetimetype=Leavepermision;
  latehour=LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision);
  }
   else{
  latetimetype="Absent";}
  %><%=latehour%> <%=latetimetype%>
  <%
 	}
  else if(!intime.equals("") && !Leavepermision.equals("")){
 	if(LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision).equals("0.5")){latetimetype=Leavepermision;
 	latehour=LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision);}
  }else if((!intime.equals("")) && totallate>60){latetimetype="CL";}else if((!intime.equals("")) && totallate>15 && totallate<=60){latetimetype="LA";}else{latetimetype="";}
 %>
 </td></tr> <tr><td colspan="3" height="5"></td></tr>
</table>


    </td>
    <td width="8%" bgcolor="#FFF" class="attendance_text" >
    <%
    	no_ot_dayListing no_otOBJ= new no_ot_dayListing();
        List no_otOBJ_L=no_otOBJ.getNno_ot_day(yestdays);
        if((no_otOBJ_L.size()!=0) || (dateFormat.parse(yestdays).getDay()==4)){OT=0;
        if(!Leavepermision.equals("")){
        	if(LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision).equals("1")){
        		latehour="1.5";}else if(LPR_CL.getLeavePermission(datesss,staff_id,Leavepermision).equals("0.5")){
        			latehour="1";}
        }
        }
    
    %>
    <%
    	if(!EMAA.getextar7().toString().equals("")){
    %><%=EMAA.getextar7()%><%
    	}else{
    %><%=OT%><%
    	}
    %>
    </td>

   <!-- <td width="5%" bgcolor="#FFF" align="center"><%//if(!EMAA.getconform_status().toString().equals("confirm")){if(!EMAA.getconform_status().toString().equals("pending")){%><input type="submit" name="submit" id="submit"value="Submit" onClick="return validate(<%//=count%>);" class="submit_button"/>
   <%//}else{%><input type="submit" name="submit1"value="Edit" onClick="return validate(<%//=count%>);" class="submit_button"/><%//}}%></td>-->
</tr>

  <!-- </form>-->
	<jsp:useBean id="EMAs" class="beans.employee_attendenceService"/>



<%
}   //iff dateis notexist inempinouttabel  
}
  }} 
fromdate.add(Calendar.DATE, +1); 

}}%>
<%}}%>

</table>
</form>
   
  </td></tr>
  
        <%}else{response.sendRedirect("index.jsp");}%>
      </table>
</body>
</html>