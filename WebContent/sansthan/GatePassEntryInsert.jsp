<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="beans.GatePassEntry"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%> 
<%@ page import="java.util.Date"%>
<%@ page import="mainClasses.GatePassService"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>GatePassEntry</title>
</head>
<body>
<%
      String department=request.getParameter("department");
      String staffIdAndName=request.getParameter("staffIdAndName");
      String idAndName[]=staffIdAndName.split(",");
      String description=request.getParameter("description");
      String outtime=request.getParameter("outtime");
      String intime=request.getParameter("intime");
      String gatepasstype=request.getParameter("gatepasstype");
      GatePassEntry gatePassEntry=new GatePassEntry();
      gatePassEntry.setDepartment(department);
      gatePassEntry.setStaffId(idAndName[0]);
      gatePassEntry.setStaffName(idAndName[1].trim());
      gatePassEntry.setDescription(description);
      Calendar calendar=Calendar.getInstance();
      gatePassEntry.setOuttime(outtime);
      gatePassEntry.setInTime(intime);
      gatePassEntry.setGatePassType(gatepasstype);
      Date date=calendar.getTime();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String formatedDate=sdf.format(date);
      gatePassEntry.setCreatedDate(formatedDate);
      GatePassService gatePassService=new GatePassService();
      gatePassService.saveGatePassEntryDetails(gatePassEntry);

     response.sendRedirect("controlpanel.jsp?page=gatepassentrycreation");
%>
</body>
</html>