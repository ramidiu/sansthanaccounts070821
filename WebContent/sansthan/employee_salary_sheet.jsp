<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat,java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormat"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EMPLOYE MONTHLY ATTENDANCE REPORT</title>
<link rel="stylesheet" type="text/css" href="css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<link href="css/no-more-tables.css" rel="stylesheet"/>
<script src="../js/jquery-1.8.2.js"></script>
<script type="text/javascript">
function validate(){
	if(document.getElementById("month").value=="" || document.getElementById("year").value==""){
		alert("select month and year");
		return false;
	}
}
</script>

 <script type="text/javascript">
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css"/>
<script src="../ui/jquery.ui.core.js"></script>
<script src="../ui/jquery.ui.widget.js"></script>
<script src="../ui/jquery.ui.datepicker.js"></script>
<script src="../js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<jsp:useBean id="EMAF" class="beans.employee_attendence"/>
<jsp:useBean id="EMA" class="beans.employee_attendence"/>
<jsp:useBean id="SFT" class="beans.stafftimings"/>
<jsp:useBean id="SFTQ" class="beans.stafftimings"/>

<div>
</div> <div style="clear:both"></div>  <div class="container">
<div class="row">
	<div class="span_12_of_12 main_body " >
   <div class="span_12_of_12 middle_body"  style="padding:0% 1.5% 1.5% 1.5% ;"><div class="span_12_of_12 float_left">
  <section id="no-more-tables"><div class="table_heading span_12_of_12"><span style="margin:15px auto;width:auto; text-align:center; color:#ff5500;"> 	
EMPLOYEE SALARY SHEET</span> </div>  
<form name="yearform" id="yearform" action="controlpanel.jsp" method="post">
<div class="span_12_of_12" style="float:left;padding:5px 0px 5px 10px;"> 
          <input type="hidden" name="ty" value="<%=request.getParameter("ty")%>"></input>
           <div class="col span_3_of_12"><span class="sub_head" style="float:right;margin-right:10px;">MONTH </span></div>
           <div class="col span_2_of_12 " > 
           		<select name="month"  id="month">
           			<option value="">-month-</option>
                     <option value="0">January</option>
                     <option value="1">February</option>
                     <option value="2">March</option>
                     <option value="3">April</option>
                     <option value="4">May</option>
                     <option value="5">June</option>
                     <option value="6">July</option>
                     <option value="7">August</option>
                     <option value="8">September</option>
                      <option value="9">October</option>
                       <option value="10">November</option>
                      <option value="11">December</option>
                 </select>
                 <input type="hidden" name="page" value="employee_salary_sheet"/>
           </div><div class="col span_1_of_12"><span class="sub_head" style="float:right;margin-right:10px;">YEAR </span></div> 
            <div class="col span_2_of_12"> 
            	<select  name="year"  id="year">
    				<option value="">-year-</option>
					 <%
					 	for(int i=2013;i<2050;i++){
					 %>
							<option value="<%=i%>"><%=i%></option> 
		 				<%
 		 					}
 		 				%>			 
				 </select>
            </div><div class="col span_4_of_12">
                 
                 <input  type="submit" value="SEARCH" onClick="return validate();"/>
                 </div></div>  
              </form>
  <%
  	if(request.getParameter("year")!=null && request.getParameter("month")!=null){
  %>
  <form action="controlpanel.jsp" method="post">
                   <input type="hidden" name="page" value="employee_salary_openingbalance"/>
  <%
  	mainClasses.openingcl_balListing op_cl_bal=new mainClasses.openingcl_balListing();
    String OPB_temp_month="";
    if(request.getParameter("month").toString().equals("11")){
    	OPB_temp_month="January";
    }else if(request.getParameter("month").toString().equals("0")){
    	OPB_temp_month="February";
    }else if(request.getParameter("month").toString().equals("1")){
    	OPB_temp_month="March";
    }else if(request.getParameter("month").toString().equals("2")){
    	OPB_temp_month="April";
    }else if(request.getParameter("month").toString().equals("3")){
    	OPB_temp_month="May";
    }else if(request.getParameter("month").toString().equals("4")){
    	OPB_temp_month="June";
    }else if(request.getParameter("month").toString().equals("5")){
    	OPB_temp_month="July";
    }else if(request.getParameter("month").toString().equals("6")){
    	OPB_temp_month="August";
    }else if(request.getParameter("month").toString().equals("7")){
    	OPB_temp_month="September";
    }else if(request.getParameter("month").toString().equals("8")){
    	OPB_temp_month="October";
    }else if(request.getParameter("month").toString().equals("9")){
    	OPB_temp_month="November";
    }else if(request.getParameter("month").toString().equals("10")){
    	OPB_temp_month="December";
    }
    
    String monthName="";
    if(request.getParameter("month").toString().equals("0")){
    	monthName="January";
    }else if(request.getParameter("month").toString().equals("1")){
    	monthName="February";
    }else if(request.getParameter("month").toString().equals("2")){
    	monthName="March";
    }else if(request.getParameter("month").toString().equals("3")){
    	monthName="April";
    }else if(request.getParameter("month").toString().equals("4")){
    	monthName="May";
    }else if(request.getParameter("month").toString().equals("5")){
    	monthName="June";
    }else if(request.getParameter("month").toString().equals("6")){
    	monthName="July";
    }else if(request.getParameter("month").toString().equals("7")){
    	monthName="August";
    }else if(request.getParameter("month").toString().equals("8")){
    	monthName="September";
    }else if(request.getParameter("month").toString().equals("9")){
    	monthName="October";
    }else if(request.getParameter("month").toString().equals("10")){
    	monthName="November";
    }else if(request.getParameter("month").toString().equals("11")){
    	monthName="December";
    }

    int OPB_temp_year=Integer.parseInt(request.getParameter("year").toString());
    if(request.getParameter("month").toString().equals("11")){
    	OPB_temp_year=OPB_temp_year+1;
    }
    int op_cl_bal_month_count=op_cl_bal.getopeningcl_balMonth(OPB_temp_month,""+OPB_temp_year);
  %>
  <div class="span_12_of_12 float_left">
  <%
  	if(op_cl_bal_month_count==0){
  %>
	  <div style="float:left; display: none;" id="sal_mon_load_complete"><input type="submit" class="submit_button" value="Confirm Opening Balance for Next Month" />
	  <input type="hidden"name="OP_B_month" value="<%=OPB_temp_month%>"/>
	  <input type="hidden"name="OP_B_year" value="<%=OPB_temp_year%>"/>
	  </div>
	  <div style="float:left;" id="sal_mon_load"><strong>LOADING...</strong></div>
	  <%
	  	}else{
	  %>
  			<div class="col span_6_of_12 float_left"><span class="sub_head"><span style="color:#666666;margin-left:8px;font-weight:bold; "><img src="images/available.jpg" style="height: 15px; width:20px; vertical-align:bottom;margin-right: 10px;" /> Opening Balance For Next month Confirmed.</span></span></div>
   	  <%
   	  	}
   	    DecimalFormat df = new DecimalFormat("##.##");
   	    Calendar cal = Calendar.getInstance();
   	    Calendar calls = Calendar.getInstance(); 
   	    cal.set(Integer.parseInt(request.getParameter("year").toString()),Integer.parseInt(request.getParameter("month").toString()),1);
   	    calls.set(Integer.parseInt(request.getParameter("year").toString()),Integer.parseInt(request.getParameter("month").toString()),cal.getActualMaximum(Calendar.DAY_OF_MONTH));
   	    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
   	    SimpleDateFormat dtf=new SimpleDateFormat("dd/MM/yyyy");
   	    String fDay = dtf.format(cal.getTime());
   	    String lDay = dtf.format(calls.getTime());
   	    int timegap=(int)((calls.getTimeInMillis()-cal.getTimeInMillis())/(24*60*60*1000));
   	  %>
   <div class="col span_4_of_12 float_left" style="padding:5px 0px 5px 0px ;" >
   		<span class="sub_head">Dates :<span style="color:#666666;margin-left:8px;"> :&nbsp;&nbsp;<%=fDay%>&nbsp;&nbsp;-&nbsp;&nbsp;<%=lDay%></span></span>
    </div>
   <div class="col span_2_of_12 float_left">
        <a href="#" id="btnExport" class="my_acc_button" class="my_acc_button" style="float:right;"> Salary Excel Sheet</a>
   </div>
  </div>
 <div class="span_12_of_12 float_left " style="" id="tblExport">  
 <table width="100%" cellspacing="0" cellpadding="0">
 <tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST </td></tr>
<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">STAFF ATTENDENCE REPORT FOR THE MONTH OF <%=monthName%> - <%=request.getParameter("year")%></td></tr>
</table>
            <table width="100%" border="1" cellspacing="0"  bgcolor="#eeeeee" cellpadding="0" style="margin:0 !important;padding:0px;width:100%;">
<tr>
<th align="center" bgcolor="#fffef3" class="attendance_text">ID</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">Name</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">Designation</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">Opening Balance</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">CL's Earned</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">Total Available</th>
<td style="position:relative;padding:0px;background-color:#fffef3;">
<table border="0" cellspacing="0" bgcolor="#eeeeee" cellpadding="0" class="util_leaves" style=" width:100%; margin-bottom:0px !important;padding:0 !important;">
<tr><td colspan="4" align="center" bgcolor="#fffef3" style="background-color:#fffef3;border-bottom:1px #CCC solid;" class="attendance_text">Utilisation Of Leaves</td></tr>
<tr>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">LA</th>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">LP</th>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">CL</th>
<th style="width:65px;" bgcolor="#fffef3" class="attendance_text">Absent</th>
</tr>
</table>
</td>
<td style="padding:0 !important;margin:0 !important;background-color:#fffef3;">
<table width="100%" border="0" cellspacing="0" bgcolor="#eeeeee" cellpadding="0" style=" margin-bottom:0px !important; width:100%;">
<tr><td colspan="5" align="center" bgcolor="#fffef3" style="border-bottom:1px #CCC solid;background-color:#fffef3;" class="attendance_text">Total Utilisation (in CL)</td></tr>
<tr>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">LA</th>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">LP</th>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">CL</th>
<th style="width:65px;" bgcolor="#fffef3" class="attendance_text">Absent</th>
<th style="width:65px;" bgcolor="#fffef3" class="attendance_text">Total</th>
</tr>
</table>
</td>
<th align="center" bgcolor="#fffef3" class="attendance_text">TOTAL CL's</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">OD</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">OT (hours)</th>
</tr>

<%
	mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
mainClasses.employee_attendenceListing EMA_CL = new mainClasses.employee_attendenceListing();
int count=0;
//List SFT_YaerList=SFT_CL.getStaffDepartmentList(String.valueOf(cal.get(cal.YEAR)));
String year="";
if(request.getParameter("ty")!=null && request.getParameter("ty").equals("sansthan")){
	year="2013";
}else if(request.getParameter("ty")!=null && request.getParameter("ty").equals("sainivas")){
	year="2014";
}
List SFT_YaerList=SFT_CL.getStaffDepartmentList1(year);
//String year=String.valueOf(cal.get(cal.YEAR));

for(int aa=0; aa < SFT_YaerList.size(); aa++ ){
	//List STF_List=SFT_CL.getALLStaffForAttendence(String.valueOf(cal.get(cal.YEAR)),SFT_YaerList.get(aa).toString());
	List STF_List=SFT_CL.getALLStaffForAttendence1(year,SFT_YaerList.get(aa).toString());
%>
<tr><td colspan="11"class="orange_heading" align="center"><div class="span_12_of_12 float_left " style="font-weight:bold;color:#ff6600;text-align:center;margin:0px;" ><%=SFT_YaerList.get(aa).toString()%></div></td></tr>
<%
	for(int i=0; i < STF_List.size(); i++ ){
SFT=(beans.stafftimings)STF_List.get(i);
List STA_List=SFT_CL.getMstafftimings(SFT.getstaff_id().toString());
if(count==0)count=(i+1);
else count=count+1;
for(int G=0; G < STA_List.size(); G++ ){
SFTQ=(beans.stafftimings)STA_List.get(0);}
%>
<tr>

<td align="center" bgcolor="#FFF" class="attendance_text">
<input type="hidden"name="OP_B_select1"id="OP_B_select1<%=count%>"value="<%=SFT.getstaff_id()%>"/><%=SFTQ.getstaff_id()%>
</td>
<td align="center" bgcolor="#FFF" class="attendance_text"><%=SFTQ.getstaff_name()%></td>
<td align="center" bgcolor="#FFF" class="attendance_text"><%=SFTQ.getdesignation()%></td>

<%
	cal = Calendar.getInstance();
calls = Calendar.getInstance(); 
cal.set(Integer.parseInt(request.getParameter("year").toString()),Integer.parseInt(request.getParameter("month").toString()),1);
calls.set(Integer.parseInt(request.getParameter("year").toString()),Integer.parseInt(request.getParameter("month").toString()),cal.getActualMaximum(Calendar.DAY_OF_MONTH));
timegap=(int)((calls.getTimeInMillis()-cal.getTimeInMillis())/(24*60*60*1000));
float LA=0,LP=0,CL=0,AB=0,OT=0,OD=0,temp_CL=0;
float OP_bal=0,AT_days=0,er_CL=0,To_Cl=0,Usd_CL=0,GTo_CL=0,cal_LA=0,cal_LP=0,cal_Cl=0,fr_cal_Cl=0,cal_AB=0;

String op_cl_bal_value=op_cl_bal.getopeningcl_bal(SFTQ.getstaff_id(),request.getParameter("month"),request.getParameter("year"));
if(!op_cl_bal_value.equals("")){
	OP_bal=(float)Double.parseDouble(op_cl_bal_value);
}
for(int p=0;p<=timegap;p++){
List forlraves=EMA_CL.getEmployeeMonthLeavese(SFTQ.getstaff_id(),sdf.format(cal.getTime()),sdf.format(cal.getTime()));
ArrayList lis=new ArrayList();
String temp14="";
if(forlraves.size()>0){
for(int t=0; t < forlraves.size(); t++ ){
AT_days++;
EMAF=(beans.employee_attendence)forlraves.get(t);
if(!EMAF.getlate_hours().equals("") && !EMAF.getlate_hours().equals(null)){
	
	temp_CL=Float.parseFloat(EMAF.getlate_hours());
}else{
	temp_CL=0;
}
if(EMAF.getlate_hourpermision().equals("Yes")){LP=LP+1;} 
if(EMAF.getextra5().equals("LA")){LA=LA+1;} 
if(EMAF.getextra5().equals("CL")){CL=CL+temp_CL;}
if(EMAF.getextra5().equals("Absent")){AB=AB+1;AT_days--;}
if(EMAF.getextra5().equals("OD")){OD=OD+1;}
if(!EMAF.getextar7().equals("")){OT=OT+Float.parseFloat(EMAF.getextar7());}
}}
cal.add(Calendar.DATE,+1);} 
er_CL=AT_days/4;
/* System.out.println(er_CL+"<=er_CL=>"+AT_days+"<=AT_days=>"); */
er_CL=(float)(Math.ceil(er_CL*2)/2);
/* System.out.println(er_CL+"<=er_CL=>"+SFTQ.getstaff_id()+"<=SFTQ.getstaff_id()=>"); */
if(er_CL>=6){er_CL=6;}
To_Cl=er_CL+OP_bal;
%>
<td align="center" bgcolor="#FFF" class="attendance_text"><%=OP_bal %></td>
<td align="center" bgcolor="#FFF" class="attendance_text"><%=er_CL%></td>
<td align="center" bgcolor="#FFF" class="attendance_text"><%=To_Cl%></td>
<td style="padding:0px !important;" >
    <table  border="0" cellspacing="0" bgcolor="#eeeeee" cellpadding="0" style="width:100%;height:100%;margin:0px !important;vertical-align:top;" class="util_values">
    <tr>
    <td style="width:50px; vertical-align:top;" bgcolor="#FFF" class="attendance_text1" ><%=(int)LA %></td>
    <td style="width:50px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=(int)LP %></td>
    <td style="width:50px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=CL%></td>
    <td style="width:65px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=(int)AB %></td>
    </tr>
    </table>
</td>
<td>
<table  border="0" cellspacing="0" bgcolor="#eeeeee" cellpadding="0" style="width:100%;height:100%;margin:0px !important;vertical-align:top;" class="util_values">
   <%cal_LA=(int)(LA/3); %>
    <tr>
    <td style="width:50px; vertical-align:top;" bgcolor="#FFF" class="attendance_text1" ><%=df.format(cal_LA)%></td>
    <%cal_LP=(int)(LP/3); %>
    <td style="width:50px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=df.format(cal_LP)%></td>
    <%
Usd_CL=cal_LA+cal_LP;

 if(CL>(int)CL){
	for(int a=(int)CL;a>0;a--){
		float temp=0;
		if((int)Usd_CL>=(int)To_Cl){
			temp=(float)1.5;
		}else{
			temp=1;
		}
		cal_Cl=cal_Cl+temp;
		Usd_CL=Usd_CL+temp;
	}
	if((int)Usd_CL>=(int)To_Cl){
		fr_cal_Cl=(float)0.75;
		
	}else{
		fr_cal_Cl=(float)0.5;
	}
	cal_Cl=cal_Cl+fr_cal_Cl;
	Usd_CL=Usd_CL+fr_cal_Cl;
	
}else{
	for(int a=(int)CL;a>0;a--){
		float temp=0;
		if((int)Usd_CL>=(int)To_Cl){
			temp=(float)1.5;
		}else{
			temp=1;
		}
		cal_Cl=cal_Cl+temp;
		Usd_CL=Usd_CL+temp;
	}
	//System.out.println("number");
	 //if(SFT.getstaff_id().equals("50"))System.out.println(Usd_CL);
} 
 //if(SFT.getstaff_id().equals("50"))System.out.println(Usd_CL);
%>
    <td style="width:50px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=CL%></td>
    <%
	for(int a=(int)AB;a>0;a--){ 
		float temp=(float)1.5;
		/* float temp=0;
		if((int)Usd_CL<=(int)To_Cl){
			temp=(float)1.5;
		}else{
			temp=2;
		} */
		cal_AB=cal_AB+temp;
		Usd_CL=Usd_CL+temp;
		//if(SFT.getstaff_id().equals("50"))System.out.println(Usd_CL);
	}
%>
    <td style="width:65px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=cal_AB %></td>
    <td style="width:65px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=df.format(cal_LA+cal_LP+CL+cal_AB)%></td>
    </tr>
    </table>
</td>

<td bgcolor="#FFF" class="attendance_text"><%=df.format(To_Cl-(cal_LA+cal_LP+CL+cal_AB)) %></td>
<td bgcolor="#FFF" class="attendance_text">
	<input type="hidden" name="OP_BAL"  id="OP_BAL<%=count%>" value="<%=df.format(To_Cl-Usd_CL)%>" /><%=(int)OD %>
</td>
<td bgcolor="#FFF" class="attendance_text"><%=(int)OT %></td>
</tr>
<%} %>
<%} %>
</table>
</div>
</form>                                                     
  <%} %>                                                                                                                  
</section></div> </div> </div>
	</div>
</div>
 <div style="clear:both"></div>  
  <script>
window.onload=function a(){
document.getElementById("sal_mon_load").style.display="none";
document.getElementById("sal_mon_load_complete").style.display="block";
	};
</script>
</body>
</html>