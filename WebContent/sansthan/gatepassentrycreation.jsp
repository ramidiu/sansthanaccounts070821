<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="beans.stafftimings"%>
<%@ page import="mainClasses.GatePassService"%>
<%@ page import="beans.GatePassEntry"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>gatepassentry</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />

<!-- Plugin files below -->
<link rel="stylesheet" type="text/css" href="../js/jquery.ptTimeSelect.css" />
<script type="text/javascript" src="../js/jquery.ptTimeSelect.js"></script>


<style>
.formfield * {
	vertical-align: middle;
}
.ui-widget-content {
     border: none;
     background-color:transparent; 
    /* color: #222222; */
}
.clear {
	clear: both;
}

</style>
<script type="text/javascript">
	$(document).ready(
			function() {
				var department = $('#department').val();
				$.ajax({
					url : "getemployess.jsp",
					type : "POST",
					data : {
						department : department
					},
					success : function(response) {
						var data = response.trim().split("\n");
						$('#staffIdAndName').empty();
						if (Number(response) !== 0) {
							for (var i = 0; i < data.length; i++) {
								var record = data[i].split(",");
								$('#staffIdAndName').append(
										'<option value="'+data[i]+'">'
												+ record[1] + '</option>');
							}
						}
					}
				});

				$('#submitbtn').click(
						function() {
							var description = $('#description').val();
							if (description == "") {
								$('#description').focus().css('outline',
										'solid red 2px');
								return false;
							}
							$('#submitForm').submit();
						});
			});
	
</script>

<script type="text/javascript">
	function removeError(id) {
		$('#' + id).css('outline', '#B0C4DE');
	}
</script>
<script type="text/javascript">
	function onchangeDepartment() {
		var department = $('#department').val();
		$.ajax({
			url : "getemployess.jsp",
			type : "POST",
			data : {
				department : department
			},
			success : function(response) {
				var data = response.trim().split("\n");
				$('#staffIdAndName').empty();
				if (Number(response) !== 0) {
					for (var i = 0; i < data.length; i++) {
						var record = data[i].split(",");
						$('#staffIdAndName').append(
								'<option value="'+data[i]+'">' + record[1]
										+ '</option>');
					}
				}
			}
		});

	}
</script>

</head>
<body>
	<section class="form">

		<div class="container">
			<div class="row">
				<h1 style="text-align: center">GatePass Entry Creation</h1>
				<form id="submitForm" action="GatePassEntryInsert.jsp" method="post"
					style="text-align: center;">
					<input type="hidden" name="page" id="page"
						value="gatepassentrycreation" />
					<div class="col-md-6">
						Department <select name="department" id="department"
							onchange="onchangeDepartment();">
							<option value="SANSTHAN(SH-1206)" selected>SANSTHAN(SH-1206)</option>
							<option
								value="HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)">HOUSE
								KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)</option>
							<option value="GENERAL ADMINISTRATION DEPARTMENT(SH-1382)">GENERAL
								ADMINISTRATION DEPARTMENT(SH-1382)</option>
							<option value="CHAPPAL STAND(SH-2784)">CHAPPAL STAND
								(SH-2784)</option>
							<option value="ACCOUNTS DEPARTMENT(SH-1382)">ACCOUNTS
								DEPARTMENT (SH-1382)</option>
							<option
								value="SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)">SAI
								PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)</option>
							<option value="Laddu Counter(SH-1515)">Laddu
								Counter(SH-1515)</option>
							<option value="Pooja Stores(SH-1009)">Pooja
								Stores(SH-1009)</option>
							<option value="MEDICAL CENTRE(SH-1576)">MEDICAL
								CENTRE(SH-1576)</option>
							<option value="PUBLIC RELATION OFFICER(SH-1382)">PUBLIC
								RELATION OFFICER(SH-1382)</option>
							<option value="LIBRARY(1595)">LIBRARY(1595)</option>
							<option value="SHRI SAINIVAS MEGHA DHARMASALA">SHRI
								SAINIVAS MEGHA DHARMASALA</option>
							<option value="SULAB CLEANERS">SULAB CLEANERS</option>
							<option value="SECURITY">SECURITY</option>
							<option value="RESTAURANT">RESTAURANT</option>
							<option value="PRIVATE SECURITY">PRIVATE SECURITY</option>
							<option value="SANSTHAN SECURITY">SANSTHAN SECURITY</option>
							<option value="SERVICES">SERVICES</option>
							<option value="SANSTHAN OUTSOURCE EMPLOYEES">SANSTHAN
								OUTSOURCE EMPLOYEES</option>
							<option value="PHYSIOTHERAPY CENTRE">PHYSIOTHERAPY
								CENTRE</option>
							<option value="CLEANING DEPARTMENT">CLEANING DEPARTMENT</option>
							<option value="EDUCATION">EDUCATION</option>
							<option value="DOCTOR">DOCTOR</option>
							<option value="VEDHAPANDIT">VEDHAPANDIT</option>
							<option value="LADY SECURITY GAURDS">LADY SECURITY
								GAURDS</option>
						</select>
					</div>
					<div class="col-md-6">

						Employee <select name="staffIdAndName" id="staffIdAndName">
						</select>

					</div>
					<div class="col-md-12 formfield">
						<label>Description</label>
						<textarea rows="5" name="description" id="description"
							onkeyup="removeError('description');"></textarea>
					</div>
						
					<div id="sample1" class="ui-widget-content">
        
            <label>Outtime</label>
            <input name="outtime" value="" /> 
            <label>Intime</label>
            <input name="intime" value="" />
            
                  <div class="col-md-6">

						GatePassType <select name="gatepasstype" id="gatepasstype">
						   <option value="Personal" selected="selected">Personal</option>
						   <option value="Office Work">Office Work</option>
						   
						</select>

					</div>
        
       
    </div>
					
					<button type="button" id="submitbtn" class="submit_button"
						style="margin: 7px -43px 0 0;">Submit</button>
				</form>
			</div>
		</div>
	</section>
	<section class="table">
		<div class="container">
			<div class="row">
				<div class="col-md-12">



					<table style="width: 750px">
						<tr>
							<th>GatePassEntryId</th>
							<th>StaffName</th>
							<th>Department</th>
							<th>Description</th>
							<th>OutTime</th>
							<th>In Time</th>
							<th>GatePassTYpe</th>
						</tr>
						<%
							GatePassService gatePassService = new GatePassService();
							List<GatePassEntry> gatePassEntryList = gatePassService.gatePassEntryList();
							for (int i = 0; i < gatePassEntryList.size(); i++) {
								GatePassEntry gatePassEntry = gatePassEntryList.get(i);
						%>

						<tr>
							<td><%=gatePassEntry.getGatePassEntryId()%></td>
							<td><%=gatePassEntry.getStaffName()%></td>
							<td><%=gatePassEntry.getDepartment()%></td>
							<td><%=gatePassEntry.getDescription()%></td>
							<td><%=gatePassEntry.getOuttime()%></td>
							<td><%=gatePassEntry.getInTime()%></td>
							<td><%=gatePassEntry.getGatePassType()%></td>
						</tr>
						<%
							}
						%>
					</table>
				</div>
			</div>
		</div>
	</section>
	  
    <script type="text/javascript">
        $(document).ready(function(){
            // find the input fields and apply the time select to them.
            $('#sample1 input').ptTimeSelect();
        });
    </script>
</body>
</html>