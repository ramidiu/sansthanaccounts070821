<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function Validate(){
    var current_pass=document.change_passwordupdate.currentpassword;
	if ((current_pass.value==null)||(current_pass.value=="")){
		alert("Please Enter your Current Password")
		current_pass.focus()
		return false
	}
		

var invalid = " "; // Invalid character is a space
	var minLength = 6; // Minimum length
	var pw1=document.change_passwordupdate.newpassword.value;
	var pw2=document.change_passwordupdate.confirmpassword.value;	
	// check for minimum length

		
	var new_pass=document.change_passwordupdate.newpassword;
	if ((new_pass.value==null)||(new_pass.value=="")){
		alert("Please Enter your New Password")
		new_pass.focus()
		return false
	
	}	


	var confirm_pass=document.change_passwordupdate.confirmpassword;
	if ((confirm_pass.value==null)||(confirm_pass.value=="")){
		alert("Please Enter Confirm Password")
		confirm_pass.focus()
		return false
	}	
	//check for pass and confirm pass similarity
	if(pw1 != pw2) {
	alert('You did not enter the same new password twice. Please re-enter your password.');
	confirm_pass.focus()
	return false
	}

	return true
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <%if(request.getParameter("succ")!=null){%>
    <tr>
      <td colspan="2"  class="address_heading">Your Password has been changed.</td>
      </tr>
      <%}%>
      <%if(request.getParameter("fail")!=null){%>
      <tr>
      <td colspan="2"  class="address_heading">Your Current Password is Incorrect</td>
      </tr>
      <%}%>
  <tr>
    <td><div align="left" class="orange_heading">change password </div></td>
  </tr>
  <tr>
    <td align="center">
	<form name="change_passwordupdate" id="change_passwordupdate" action="change_passwordupdate.jsp"  onsubmit="return Validate()"><table width="60%" border="0" cellspacing="0" cellpadding="0">
      
      <tr>
        <td width="28%" height="30"><div align="left" class="normal_text">Old Password &nbsp;: </div></td>
        <td width="27%" height="30" valign="middle"><div align="left">
            <input name="currentpassword" type="password" id="currentpassword" value=""/>
        </div></td>
      </tr>
      <tr>
        <td height="30" valign="middle"><div align="left" class="normal_text">New Password : </div></td>
        <td height="30" valign="middle"><div align="left">
          <input name="newpassword" type="password" id="newpassword" value=""/>
        </div></td></tr>
        <tr>
        <td width="28%" valign="middle"><div align="left" class="normal_text">Confirm New Password : </div></td>
        <td width="27%" valign="middle"><div align="left">
          <input name="confirmpassword" type="password" id="confirmpassword" value=""/>
        </div></td>
      </tr>
      <tr>
        <td height="30" colspan="2" valign="top" align="center"><div align="center">
          <input name="Submit" type="submit" class="submit_button" value="Submit">
        </div></td>
        </tr>
    </table></form></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
