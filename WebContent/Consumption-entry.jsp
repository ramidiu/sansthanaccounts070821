<%@page import="beans.produceditems"%>
<%@page import="mainClasses.produceditemsListing"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.doctordetailsListing"%>
<%@page import="beans.tablets"%>
<%@page import="mainClasses.tabletsListing"%>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Consumption Entry | Sai sansthan</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script type='text/javascript' src='main/lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='main/lib/jquery.js'></script>
<link rel="stylesheet" type="text/css" href="main/jquery.autocomplete.css"/>
<script type='text/javascript' src='main/jquery.autocomplete.js'></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}

</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css"/>
<script>
function addmore(){
	var j=0;
	var i=0;
	j=document.getElementById("addcount").value;
	i=j;
	j=Number(Number(j)+5);
	for(i;i<j;i++){
	document.getElementById("addcolumn"+i).style.display="block";
	}
	document.getElementById("addcount").value=Number(Number(document.getElementById("addcount").value)+5);
}
function addOption(){
	$("#purpose").empty();
	var hoaid=$("#headAccountId").val();
	if(hoaid=="3"){
		$("#purpose").append('<option value=godown>Shop sales</option>');
	}else if(hoaid=="4" || hoaid=="1"){
		$("#purpose").append('');
		$("#purpose").append('<option value=SAIANNADANAM>SAIANNADANAM</option>');
		$("#purpose").append('<option value=SAI PRASAD>SAI PRASAD</option>');
		$("#purpose").append('<option value=MAINTENANCE>MAINTENANCE</option>');
		$("#purpose").append('<option value=TEA,BREAKFAST>TEA,BREAKFAST</option>');
		$("#purpose").append('<option value=STATIONARY&ELECTRICAL>STATIONARY&ELECTRICAL</option>');
		$("#purpose").append('<option value=SAIPOOJA ITEMS>SAIPOOJA ITEMS</option>');
		$("#purpose").append('<option value=OTHER>OTHER PURPOSE</option>');
	}
}

</script>
<jsp:useBean id="PRDUCD" class="beans.produceditems"></jsp:useBean>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<form method="post" action="Consumption_Insert.jsp">
<div class="vendor-page">

<div class="vendor-box">
<div class="vendor-title"> </div>
<div class="vender-details">

<table width="70%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="3" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;font-size: 10px;"> (Regd.No.646/92)</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
<tr><td colspan="3" align="center" style="font-weight: bold;">CONSUMPTION ENTRY FORM</td></tr>

<%mainClasses.employeesListing EMP_l=new mainClasses.employeesListing();
produceditemsListing PRDIL=new produceditemsListing();
String EMP_HOA=EMP_l.getMemployeesHOA(session.getAttribute("empId").toString());
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing();
List HOA_List=HOA_CL.getheadofaccounts();
List PRDKOTLIST=PRDIL.getproduceditemsKOT();
%>
<tr>
<td width="35%"><div class="warning" id="dateError" style="display: none;">Please select date.</div>Date*</td>
</tr>
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()); %>
<tr>
<td><input type="text" name="date" id="date" value="<%=currentDate%>" readonly="readonly"/></td>
</tr>
</table>
</div>
<div style="clear:both;"></div>
</div>


<div class="vendor-list">
<div class="sno1 bgcolr">S.No.</div>
<div class="ven-nme1 bgcolr">HEAD OF ACCOUNT</div>
<div class="ven-nme1 bgcolr">KOT ID</div>
<div class="ven-nme1 bgcolr">CONSUMPTION TYPE</div>
<div class="ven-nme1 bgcolr">SUB-CATEGORY</div>
<div class="ven-amt1 bgcolr">NARRATION</div>
<div class="ven-amt1 bgcolr">QTY</div>
<div class="clear"></div>
<input type="hidden" name="addcount" id="addcount" value="5"/>
<%for(int i=0; i < 100; i++ ){%>
<div <%if(i>4){ %>style="display: none;"<%} %> id="addcolumn<%=i%>">

<div class="sno1"><%=i+1%></div>
<input type="hidden" name="count" id="check<%=i%>"  value="<%=i %>"/> 
<div class="ven-nme1">
<select name="headAccountId<%=i%>"  style="width:200px;" onchange="addOption();" <%if(i==0){ %> required <%} %>>
	<%if(EMP_HOA.equals("4") || EMP_HOA.equals("1")){ %>
		<option value="4">SANSTHAN</option>
		<option value="1">CHARITY</option>
		<%}else{ %>
		<option value="<%=EMP_HOA%>"><%=HOA_CL.getHeadofAccountName(EMP_HOA)%></option><%} %>
</select>
</div>
<div class="ven-nme1">
	<select name="KOTID<%=i%>" <%if(i==0){ %> required <%} %> >
		<option value="">-Select KOT ID-</option>
		<%if(PRDKOTLIST.size()>0){
			for(int j=0;j<PRDKOTLIST.size();j++){
				PRDUCD=(produceditems)PRDKOTLIST.get(j);%>
						<option value="<%=PRDUCD.getsstid()%>"><%=PRDUCD.getsstid()%></option>
				<%}} %>
	</select>
</div>
<div class="ven-nme1">
	<select name="consumption_cat<%=i%>" <%if(i==0){ %> required <%} %> >
		<option value="">-Select Consumptiontype-</option>
		<option value="TEA,COFFE">TEA,COFFE</option>
		<option value="BREAKFAST">BREAKFAST</option>
		<option value="MILK">MILK</option>
		<option value="SAIANNADANAM">SAI ANNADANAM</option>
		<option value="PRASADAM DISTRIBUTION">PRASADAM DISTRIBUTION</option>
		<option value="PRASADAM SALE">SAI PRASADAM SALE</option>
		<option value="SAIPOOJA ITEMS">SAIPOOJA ITEMS</option>
		<option value="OTHER">OTHER PURPOSE</option>
	</select>
</div>
<div class="ven-nme1">
	<select name="consumption_subcat<%=i%>" <%if(i==0){ %> required <%} %>>
		<option value="VOLUNTEERS">VOLUNTEERS</option>
		<option value="GUESTS">GUESTS</option>
		<option value="DEVOTES GENTS">DEVOTES GENTS</option>
		<option value="DEVOTES LADIES">DEVOTES LADIES</option>
		<option value="DEVOTES CHILDERN">DEVOTES CHILDERN</option>
		<option value="STAFF">STAFF</option>
		<option value="TRUST MEMBERS">TRUST MEMBERS</option>
		<option value="ORPHANS">ORPHANS</option>
		<option value="OTHERS">OTHERS</option>
			</select>
</div>
<div class="ven-amt1"><textarea name="narration<%=i%>" placeHolder="Enter your narration here" onkeyup="javascript:this.value=this.value.toUpperCase();"></textarea></div>
<div class="ven-amt1"><input type="text" name="quantity<%=i%>" id="quantity<%=i%>"  onblur="calculate('<%=i %>')" value="" style="width:50px;" onkeypress="return numbersonly(this, event,true);"></input><span id="godQty<%=i%>" style="color: green;"></span></div>
<div class="clear"></div>
</div>
<%} %>
<div class="add-ven"><input type="button" name="button" value="Add Fields" onclick="addmore( )" class="click"/>
<input type="submit" id="submit" value="SUBMIT" class="click" style="border:none; float:right; margin:0 500px 0 0"/>
</div>

</div>
</div>
</form>
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>