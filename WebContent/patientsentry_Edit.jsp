<%@page import="mainClasses.tabletsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.util.List"   pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Password</title>
<style>
.vender-details
{
	margin:25px 5px 5px 5px; padding:0 20px;  float:left; width:95%; height:auto;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}

.vender-details table td
{
	padding:4px 5px 0 5px;
	font-family:"HelveticaNeue-Roman", "HelveticaNeue", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
}
.vender-details table td input[type="text"]
{
	padding:5px;
	
}
.vender-details table td textarea
{
	width:100%;
	height:80px;
}
.vender-details table td span
{
	font-size:22px; font-weight:bold;
}
.warning
{
	color:#900; font-weight:bold; font-size:14px; 
}

.click {
border: 1px solid #65230d;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;

padding: 5px 15px 5px 15px;
text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
font-weight: bold;
text-align: center;
color: #FFF;
background-color: #ffc579;
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffc579), color-stop(100%, #fb9d23));
background-image: -webkit-linear-gradient(top, #c88b2e, #671811);
background-image: -moz-linear-gradient(top, #c88b2e, #671811);
background-image: -ms-linear-gradient(top, #c88b2e, #671811);
background-image: -o-linear-gradient(top, #c88b2e, #671811);
background-image: linear-gradient(top, #c88b2e, #671811);
filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#c88b2e, endColorstr=#671811);
cursor: pointer;
}

.click:hover
{
	background: #742715; /* Old browsers */
background: -moz-linear-gradient(top,  #742715 0%, #bf802b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#742715), color-stop(100%,#bf802b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #742715 0%,#bf802b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #742715 0%,#bf802b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #742715 0%,#bf802b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#742715', endColorstr='#bf802b',GradientType=0 ); /* IE6-9 */
cursor: pointer;

}

</style>
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function validate(){

	$('#PatientError ').hide();
	$('#addressError ').hide();
	$('#PhoneError ').hide();
	

	if($('#patientName').val().trim()==""){
		$('#PatientError').show();
		$('#patientName').focus();
		return false;
	}	

	if($('#address').val().trim()==""){
		$('#addressError').show();
		$('#address').focus();
		return false;
	}	

	if($('#phone').val().trim()==""){
		$('#PhoneError').show();
		$('#phone').focus();
		return false;
	}	
}
</script>


</head>
<jsp:useBean id="PAT" class="beans.patientsentry"/>
<body>
<div class="vender-details">
<form method="POST" action="patientsentry_Update.jsp" onsubmit="return validate();">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3" height="50"><span>Edit Patient Details</span></td>
</tr>
 <%
String patientId=request.getParameter("ID");
mainClasses.patientsentryListing PAT_CL = new mainClasses.patientsentryListing();
List PAT_List=PAT_CL.getMpatientsentry(patientId);
for(int i=0; i < PAT_List.size(); i++ ){
PAT=(beans.patientsentry)PAT_List.get(i);%>
<input type="hidden" name="patientId" id="patientId" value="<%=PAT.getpatientId()%>" />
<tr>

<td width="2%"></td>
<td valign="top">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3">
<div class="warning" id="PatientError" style="display: none;">Please Provide  "Patient Name".</div>
Patient Name*</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="patientName" id="patientName" value="<%=PAT.getpatientName()%>" /></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="addressError" style="display: none;">Please Provide  "Address".</div>
Address*</td>
</tr>
<tr>
<td colspan="3"><textarea  name="address" id="address"><%=PAT.getaddress()%></textarea></td>
</tr>
<tr>
<td colspan="3">
<div class="warning" id="PhoneError" style="display: none;">Please Provide  "Phone Number".</div>
Phone Number*</td>
</tr>
<tr>
<td colspan="3"><input type="text" name="phone" id="phone" onKeyPress="return numbersonly(this, event,true);" value="<%=PAT.getphone()%>" /></td>
</tr>
<tr>
<td colspan="3">
Gender*</td>
</tr>
<tr>
<td colspan="3"><input type="radio" name="extra1" id="extra1" value="M" <%if(PAT.getextra1().equals("M")){%>checked="checked" <%} %>/>MALE
<input type="radio" name="extra1" id="extra1"  value="F" <%if(PAT.getextra1().equals("F")){%>checked="checked" <%} %>/>FEMALE</td>
</tr>



</table>
</td>
</tr>
<tr>
<td colspan="3" align="right"><input type="submit" value="Submit" class="click" style="border:none;"/></td>
</tr>
<%} %>
</table>
</form>
</div>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>