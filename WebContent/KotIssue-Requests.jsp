<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.stockrequestListing"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>KOT REQUEST'S</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:92%; 
    background:#d0e3fb; 
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( "#tblExport" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        //This code is used to download excel file when button is clicked
        $(document).ready(function () {
        	$("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<jsp:useBean id="SRQ" class="beans.stockrequest"></jsp:useBean>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-list">
<div class="icons">
	<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
					<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="clear"></div>
<div class="list-details">
	 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
			<tr><td colspan="7" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST</td></tr>
			<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
			<tr><td colspan="7" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
			<tr><td colspan="7" align="center" style="font-weight: bold;">KOT Request List</td></tr>
            <tbody class="yourID">
			<tr>
				<td class="bg" width="5%" style="font-weight: bold;">S.NO.</td>
				<td class="bg" width="20%" style="font-weight: bold;">DATE</td>
				<td class="bg" width="15%" style="font-weight: bold;">REQ ID</td>
				<td class="bg" width="30%" style="font-weight: bold;">NARRATION</td>
				<td class="bg" width="15%" style="font-weight: bold;">REQEST PURPOSE</td>
				<td class="bg" width="20%" style="font-weight: bold;" >ISSUE</td>
				<td class="bg" width="15%" style="font-weight: bold;" ></td>
			</tr>
			</tbody>
			<tr>
			<td colspan="7">
				<table width="100%" cellpadding="0" cellspacing="0" border='1'>
					 <%
					 SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					 SimpleDateFormat CDF=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
					 stockrequestListing STCL=new stockrequestListing();
					 List Req_Det=STCL.getPendingKots();
					 if(Req_Det.size()>0){
						 for(int i=0;i<Req_Det.size();i++){
							 SRQ=(beans.stockrequest)Req_Det.get(i);
					
					 %>
					 <tr style="position: relative;">
					 <td width="5%"><%=i+1 %></td>
					 <td width="20%"><%=CDF.format(SDF.parse(SRQ.getreq_date())) %></td>
					 <td width="15%"><%=SRQ.getreqinv_id() %></td>
					 <td width="30%"><%=SRQ.getnarration() %></td>
					 <td width="15%"><%=SRQ.getreq_type()%></td>
					 <td width="20%"><a href="shopStockTransfer.jsp?reqId=<%=SRQ.getreqinv_id()%>">ISSUE</a></td>
					 <td width="15%"></td>
					 </tr>	
					 <%}} %>
				</table>
			</td>
			</tr>
	</table>
	 </div>
</div>
</div>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
<%}else{ response.sendRedirect("index.jsp");%>

<%} %>
</body>
</html>