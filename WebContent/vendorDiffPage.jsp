<%@page import="java.text.DecimalFormat"%>
<%@page import="mainClasses.productexpensesListing"%>
<%@page import="mainClasses.paymentsListing"%>
<%@page import="beans.payments"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table width="90%" cellspacing="0" cellpadding="0" style="margin: 0 auto;" class="new-table" border="1">
						<tr><td colspan="6" align="center" style="font-weight:bold">TOTAL PAYMENTS MADE</td></tr>
						<tr>
					<!-- 	<td>S.No</td> -->
							<td width="15%" bgcolor="#CC6600">Payment Made Date</td>
							<td width="10%" bgcolor="#CC6600">Payment Mode</td>
							<td width="10%" bgcolor="#CC6600">Mse No</td>
							<td width="20%" bgcolor="#CC6600">Narration</td>
							<td width="10%" bgcolor="#CC6600">Bill Amount</td>
							<td width="15%" bgcolor="#CC6600">Actual Amount Paid</td>
						</tr>
						<%
						DecimalFormat df = new DecimalFormat("####0.00");
						double pTotal = 0.0;
						double pTotal1 = 0.0;
						double pTotal2 = 0.0;
						String expensesAmount = "";
						String mseId="";
						double lastYearPendingbal = 0.0;
						double sumValue=0.0;
						payments PList = new payments();
						productexpensesListing pel = new productexpensesListing();
						paymentsListing pl = new paymentsListing();
						List<payments> pList = pl.getpaymentsBtwDates("", "2017-04-01 00:00:01", "2018-03-31 23:59:59");
						
						
						
						
						
						if(pList.size()>0){
							for(int j=0;j<pList.size();j++){
								PList = pList.get(j);
								
								expensesAmount = pel.getExpensesInvoiceTotAmtNew(PList.getextra3());
								mseId=pel.getMseIdBasedOnExpInvId(PList.getextra3());
								//System.out.println("mseid==== >"+mseId);
								pTotal = Double.parseDouble(PList.getamount());
								pTotal2 = Double.parseDouble(expensesAmount);
								pTotal1 = pTotal+pTotal1;
								
								%>
						<tr><%
								if(!df.format(pTotal2).equals(df.format(pTotal))){
							%>
						<%-- <td><%= j+1 %></td> --%>
							<td width="15%"><%=PList.getdate()%></td>
							<td width="10%"><%=PList.getpaymentType()%><%if(PList.getpaymentType().equals("cheque")){%> (<%=PList.getchequeNo()%>)<%}%></td>
							<td width="15%"><%=mseId%></td>
							
							<td width="20%"><%=PList.getnarration()%></td>	
								
							<%-- <td width="15%"><%=df.format(pTotal2)%></td>
							<td width="10%"><%=df.format(pTotal)%></td>
							<%}else{%> --%>
								<td width="15%" bgcolor="red"><%=df.format(pTotal2)%></td>
							<td width="10%" bgcolor="red"><%=df.format(pTotal)%></td>
								
							<%} %>
						</tr>
						<%}%>
						<tr>
								<td colspan="5" align="right" style="color:orange;font-weight:bold">TOTAL AMOUNT PAID&nbsp;</td>
								<td colspan="1" style="font-weight:bold"><%=df.format(pTotal1)%></td>
							</tr>
							<%}else{%>
						
						<%}%>
					</table>
					<br/>
					<table width="90%" cellspacing="0" cellpadding="0" style="margin: 0 auto;" class="new-table" border="1">
					<%-- <tr>
						<td width="75%" align="right" style="color:orange;font-weight:bold">GRAND TOTAL&nbsp;(<%=df.format(gTotal1+lastYearPendingbal)%> - <%=df.format(pTotal1)%>)&nbsp;</td>
						<td width="15%" align="left" style="font-weight:bold"><%=df.format((gTotal1+lastYearPendingbal)-pTotal1)%></td>
					</tr> --%>
					</table>
</body>
</html>