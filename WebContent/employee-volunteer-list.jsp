<%@page import="java.util.List"%>
<%@page import="beans.sssst_registrations"%>
<%@page import="mainClasses.sssst_registrationsListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<!-- <link href="css/acct-style.css" type="text/css" rel="stylesheet" /> -->
<!-- <link href="js/date.css" rel="stylesheet" type="text/css" />
<link href="style.css" rel="stylesheet" type="text/css" /> -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/> -->
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<style>
.yourID.fixed {
    position: fixed;
    top: 0;
    left: 25px;
    z-index: 1;
    width:92%; 
    background:#d0e3fb; 
}

@media print{
   .print_table{
    width: 900px;
    border: solid 1px;
    border-collapse: collapse;
}
.print_table td{
    border-color: black;
    font-size: 12px;
    border: solid 1px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    text-align: center;
}
.print_table tr:nth-child(odd){
    background-color:#E8E8E8;
}
.print_table tr:nth-child(even){
    background-color:#ffffff;
}

	}


</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script>
function formsumit(){ 
	document.reg.submit();}
</script>

 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                            $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
                           
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<script>
$(document).ready(function () {  
  var top = $('.yourID').offset().top - parseFloat($('.yourID').css('marginTop').replace(/auto/, 100));
  $(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
      // if so, ad the fixed class
      $('.yourID').addClass('fixed');
	      
    } else {
      // otherwise remove it
      $('.yourID').removeClass('fixed');
	        
    }
  });
});
</script>
</head>
<body>
<%-- <div><%@ include file="title-bar.jsp"%></div> --%>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<% String type = request.getParameter("type"); %>
<% if (type != null) {%>
<form name="reg" id="reg" method="post" action="employee-volunteer-list.jsp">
<table style="margin-left: 50px;"><tr><td>List Type :  <select name="type" Onchange="formsumit();" style="margin-top: 25px;margin-bottom: -10px;">
<option value="EMPLOYEE" <%if(request.getParameter("type").equals("EMPLOYEE")) {%> selected="selected"<%} %>>EMPLOYEE</option>
<option value="VOLUNTEER" <%if(request.getParameter("type").equals("VOLUNTEER")) {%> selected="selected"<%} %>>VOLUNTEER</option>
</select></td></tr></table></form>
<%} %>
<div class="vendor-page">
<div class="vendor-list">
<div class="icons">
	<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
	<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
</div>
<div class="clear"></div>
<div class="list-details">
	 <div class="printable">
	 <table width="100%" cellpadding="0" cellspacing="0" id="tblExport" >
			<tr><td colspan="8" align="center" style="font-weight: bold;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;"> (Regd.No.646/92)</td></tr>
			<tr><td colspan="8" align="center" style="font-weight: bold;"><%=type %> unique Registrations List</td></tr>
         
			<tr>
			<jsp:useBean id="REG"  class="beans.sssst_registrations"></jsp:useBean>
			<td colspan="8">
				<table width="100%" cellpadding="0" cellspacing="0"  class="print_table">
					 
            <tr class="print_table" style="background-color: #e3eaf3;color: #7490ac">
				<td align="left"   width="3%" style="font-weight: bold;">S.NO.</td>
				<td align="left"  width="10%" style="font-weight: bold;">UNIQUE ID</td>
				<td align="left" width="20%" style="font-weight: bold;">NAME</td>
				<td align="left"   width="15%" style="font-weight: bold;">PHONE NO</td>
				<td align="left"   width="20%" style="font-weight: bold;">EMAIL-ID</td>
				<!--<td class="bg" width="25%" style="font-weight: bold;">PRESENT ADDRESS</td>-->
				<td align="left"   width="15%" style="font-weight: bold;">PANCARD</td>
				<td align="left"  width="17%" style="font-weight: bold;">EDIT</td>
			</tr>
			
	<jsp:useBean id="UI" class="beans.UniqueIdGroup"/>
	<jsp:useBean id="SSSST" class="beans.sssst_registrations"/>                   
<%
	mainClasses.uniqueIdGroupsListing UIGL = new mainClasses.uniqueIdGroupsListing();
	mainClasses.sssst_registrationsListing SSSSTL = new mainClasses.sssst_registrationsListing();

	List UIG_List = null;
	
	if(type.equals("VOLUNTEER"))
	{
		UIG_List=UIGL.getUniqueIdGroupsOfVolunteer();
	}
	else if(type.equals("EMPLOYEE"))
	{
		UIG_List=UIGL.getUniqueIdGroupsOfEmployees();
	}
	for(int i=0; i < UIG_List.size(); i++ )
	{
		UI=(beans.UniqueIdGroup)UIG_List.get(i);
		List SSSST_List=SSSSTL.getMsssst_registrations(UI.getUniqueid().toString());
		for(int G=0; G < SSSST_List.size(); G++ ){
		SSSST=(beans.sssst_registrations)SSSST_List.get(G);%>
		
					 <tr style="position: relative;">
					 	<td align="left" width="3%"><%=i+1 %></td>
					 	<td align="left" width="10%"><%=SSSST.getsssst_id() %></td>
					 	<td align="left" width="20%"><%=SSSST.getfirst_name() %> <%=SSSST.getlast_name() %></td>
					 	<td align="left" width="15%"><%=SSSST.getmobile() %></td>
					 	<td align="left" width="20%"><%=SSSST.getemail_id() %></td>
					 	<!--<td width="19%"><%=SSSST.getPresent_address() %></td>-->
					 	<td align="left" width="15%"><%=SSSST.getpancard_no() %></td>
					 	<td align="left" width="17%"><a href="employee-volunteer-form_Edit.jsp?bid=<%=SSSST.getsssst_id()%>&type=<%=type%>">Edit</a></td>				 	
					 </tr>	
					 <%}} %>
				</table>
			</td>
			</tr>
	</table>
	 </div>
</div>
</div>
</div>
<%-- <div><div ><jsp:include page="footer.jsp"></jsp:include></div></div> --%>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
<%}else{ response.sendRedirect("index.jsp");%>

<%} %>
</body>
</html>