<%@page import="helperClasses.DevoteeGroups"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DEVOTEE REGISTRATION || SSSST,DSNR</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		
         yearRange: '-150:+0'
			
	});
	$( "#date2" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
});
function block(){
	var uniqueId=$('#uniquid').val().trim();
	if(uniqueId != "")
	{
		alert("That Unique Id is Already Created");
		$('#uniquid').focus();
		return false;
	}
	else
		{
	$.blockUI({ css: { 
	        border: 'none', 
	        padding: '15px', 
	        backgroundColor: '#000', 
	        '-webkit-border-radius': '10px', 
	        '-moz-border-radius': '10px', 
	        opacity: .5, 
	        color: '#fff' 
	    } });
	}
}

function GetUniqueRegistrations(){
	  var mobile=$('#uniqueID').val();
		  var arr = [];
	 $.post('searchPhoneNo.jsp',{q:mobile,con : 'uniqueId'},function(data) 
	{
		var response = data.trim().split("\n");
		var sssstId = new Array();
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();
		 var address2=new Array();
		 var address3=new Array();
		 var city=new Array();
		 var pincode=new Array();
		 var pancard=new Array();
		 var email=new Array();
		 var dob=new Array();
		 for(var i=0;i<response.length;i++){
			// var d=response[i].split(",");
			 var d=response[i].split(":");
			 arr.push({
				 label: d[0],
				 sssstId :d[1],
				 phoneno:d[2],
				 customerName:d[3],
				 gothram:d[4],
				 address:d[5],
				 address2:d[6],
				 city:d[7],
				 pincode:d[8],
				 email:d[9],
				 pancard:d[10],
				 dob:d[11],
				 address3:d[12],
				 middlename:d[13],
				 lastname:d[14],
				 spousename:d[15],
				 star:d[16],
				 state:d[17],
				 idproof1:d[18],
				 idproof_num:d[19],
				 narration:d[20],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#uniqueID').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#uniquid').val(ui.item.sssstId);
				$('#mobile').val(ui.item.phoneno);
				$('#firstname').val(ui.item.customerName);
				$('#gothram').val(ui.item.gothram);
				$('#address1').val(ui.item.address);	
				$('#addressSj').val(ui.item.address);		
				$('#address2').val(ui.item.address2);
				$('#address3').val(ui.item.address3);
				$('#city').val(ui.item.city);
				$('#pincode').val(ui.item.pincode);
				$('#email-id').val(ui.item.email);
				$('#date').val(ui.item.dob);
				$('#pancard').val(ui.item.pancard);
				$('#middlename').val(ui.item.middlename);
				$('#lastname').val(ui.item.lastname);
				$('#spousename').val(ui.item.spousename);
				$('#star').val(ui.item.star);
				$('#state').val(ui.item.state);
				$('#idproof_num').val(ui.item.idproof_num);
				$('#narration').val(ui.item.narration);
				
			}
		}); 
	});
}
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>
</head>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
<div class="vendor-box">
<div class="vender-details">
<input type="hidden" name="uniqueid" id="uniqueid" value="<%=request.getParameter("uniqueid")%>"/>
<form  method="post" action="deveotee-registration-insert.jsp" onsubmit="return block();">
 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO OFFICE </td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;">DEVOTEE REGISTRATION</td></tr>
</table>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
<%String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();%>
<tr>
	<td>Date</td>
	<td>Sansthan Unique Id</td>
	<td>EMAIL-ID</td>
	<td>Get Details with Mobile</td>
	<td colspan="1">MOBILE NO <span style="color: red;">*</span></td>
	<td>Unique Id</td>
</tr>
<tr>
	<td><input type="text" name="curentdate" value="<%=currentDate%>" readonly="readonly"/></td>
	<td><input type="text" name="receiptNo" value="<%=ng_LST.getid("sssst_registrations")%>" readonly="readonly"></td>
	<td><input type="email" name="email-id" id="email-id" value="" placeHolder="Enter devotee email-id"></td>
	<td><input type="text" name="uniqueID" id="uniqueID" value="" onkeyup="GetUniqueRegistrations();" autocomplete="off"  placeHolder="Find details with MobileNumber" />
	<td><input type="text" name="mobile" id="mobile" value="" placeHolder="Enter devotee mobile number" ></td>
	<td><input type="text" name="uniquid" id="uniquid" value="" onkeyup="();" placeHolder="UniqueId Will Displayed" readonly />
</tr>
<tr>
	<td>Title</td>
	<td>First Name<span style="color: red;">*</span></td>
	<td>Middle Name</td>
	<td>Last Name<span style="color: red;">*</span></td>
	<td>Spouse Name</td>
	<td>Date Of Anniversary</td>
</tr>
<tr>
	<td><select name="title" id="title">
	<option value="Mr">Mr.</option>
	<option value="Miss">Miss.</option>
	<option value="Mr&miss">Mr&miss</option>
	</select></td>
	<td><input type="text" name="firstname" id="firstname" value=""  onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter devotee Firstname"></td>
	<td ><input type="text" name="middlename" id="middlename" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter devotee Middle name"></td>
	<td ><input type="text" name="lastname" id="lastname" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter devotee Last name"></td>
	<td ><input type="text" name="spousename" id="spousename" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeHolder="Enter devotee Spouse name"></td>
	<td width="30">
	<select name="doamonth" id="doamonth" style="width: 85px;">
	<option value="">Month</option>
	<option value="01">JAN</option>
	<option value="02">FEB</option>
	<option value="03">MAR</option>
	<option value="04">APR</option>
	<option value="05">MAY</option>
	<option value="06">JUN</option>
	<option value="07">JUL</option>
	<option value="08">AUG</option>
	<option value="09">SEP</option>
	<option value="10">OCT</option>
	<option value="11">NOV</option>
	<option value="12">DEC</option>
	</select>
	<select name="doadate" id="doadate" style="width: 85px;">
	<option value="">Day</option>
	<%for(int i=01;i<32;i++){ 
	if(i < 10){%>
	
	<option value="0<%=i%>">0<%=i%></option>
	<%}else{ %>
	<option value="<%=i%>"><%=i%></option>
	<%} }%>
	</select></td>
</tr> 
<tr>
	<td>Gender</td>
	<td>Date of Birth</td>
	<td>Gothram</td>
	<td>Star</td>
	<td>Pancard Number</td>
	<td>Updates receive By</td>
</tr>
<tr>
	<td><input  type="radio" name="gender" id="male" value="male" checked="checked">Male<input  type="radio" name="gender" id="female" value="female">FeMale</td>
	<td ><input  type="text" style="width: 105px;" name="dob" id="date" value="" placeHolder="Select Devotee Date of birth" readonly="readonly"></td>
	<td><input  type="text" name="gothram" id="gothram" value="" placeHolder="Enter Devotee Gothram" onkeyup="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input  type="text" name="star" id="star" value="" placeHolder="Enter Devotee Star" onkeyup="javascript:this.value=this.value.toUpperCase();"></td>
	<td><input  type="text" name="pancard" id="pancard" value="" placeHolder="Select Devotee pancard Number"></td>
	<td><input  type="radio" name="updates" id="email" value="email" checked="checked">Email<input  type="radio" name="updates" id="post" value="post">Post<input  type="radio" name="updates" id="mobile" value="mobile">SMS</td>
</tr>
<tr>
	<td>Address</td>
	<td>Address2</td>
	<td>Address3</td>
	<td>City</td>
	<td>State</td>
	<td>PinCode</td>
	
	
</tr>
<tr>
	<td><input type="text" name="address1" id="address1" value=""  placeHolder="Enter address" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="address2" id="address2" value=""  placeHolder="Enter address2" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="address3" id="address3" value=""  placeHolder="Enter address3" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="city" id="city" value=""  placeHolder="Enter city" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="state" id="state" value=""  placeHolder="Enter State" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
	<td><input type="text" name="pincode" id="pincode" value=""  placeHolder="Enter PinCode" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
</tr>
<tr>	
	<td>Identity Proof</td>
	<td>Identity Proof Number</td>
	<td>Narration</td>
</tr>
<tr>
	<td><select  name="idproof1" >
		<option value="">ID PROOF</option>
		<option value="UID AADHAAR NO">UID AADHAAR NO</option>
		<option value="VOTE ID NO">VOTE ID NO</option>
		<option value="PAN CARD NO">PAN CARD NO</option>
		<option value="PASSPORT NO">PASSPORT NO</option>
		<option value="RATION CARD NO">RATION CARD NO</option>
		<option value="DRIVING LICENSE NO">DRIVING LICENSE NO</option>
		<option value="OTHERS">OTHERS</option>
	 </select> </td>
	 <td><input type="text" name="idproof_num" id="idproof_num" value=""  placeHolder="Enter Identity proof number"></td>
	 <td><input  type="text" name="narration" id="narration" value="" placeHolder="Enter Narration" onblur="javascript:this.value=this.value.toUpperCase();"></td>
</tr>
<tr><td></td></tr>
<tr><td></td></tr>
<tr>
<td colspan="8" style="border:1px solid #000; padding:10px; margin-top:10px;
margin-bottom:10px;">
	<%
	List<String> list=DevoteeGroups.getAllDevotees();
	for(int i=0;i<list.size();i++){
	%>
	 <label>
      <input type="checkbox" name="devoteeGroup" value="<%=list.get(i)%>"/><%=list.get(i)%> &nbsp;&nbsp;&nbsp;
     </label>
	<%} %>
</td>

</tr>
<tr><td></td></tr>
<tr> 
<td colspan="4" align="right"><input type="reset" value="RESET" class="click" style="border:none;"/></td>
<td align="right" style="display: block;" id="save"><input type="submit" value="REGISTER" class="click"  style="border:none;"/></td>

</tr>

</table>
</form>
</div>
</div>
</div>

<script>
	function print()
	{
		
	 if(document.getElementById("uniqueid").value != 'null'){
		 newSJwindow=window.open('Devotee-Image-Capture.jsp?bid='+document.getElementById("uniqueid").value,'nameSJ','height=600,width=650,menubar=yes,status=yes,scrollbars=yes'); 		
		if (window.focus) {newSJwindow.focus();}
	}
	}
	window.onload=print();
</script>

<%}else{
	response.sendRedirect("index.jsp");
	%>

<%} %>
</body>
</html>