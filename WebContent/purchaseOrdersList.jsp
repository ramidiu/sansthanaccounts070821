<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script>
$(function(){
	$('#fromdate').datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect : function(date){
			var selectedDate = new Date(date);
			$("#todate").datepicker( "option", "minDate", selectedDate );
		}
	});
	$('#todate').datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect : function(date){
			var selectedDate = new Date(date);
			$("#fromdate").datepicker( "option", "maxDate", selectedDate );
		}
	});
	$("#fromdate").datepicker("setDate", new Date());
	$("#todate").datepicker("setDate", new Date());
});
</script>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<jsp:useBean id="PO" class="beans.purchaseorder"/>
<html>
<body>
<div><%@ include file="title-bar.jsp"%></div>

<div class="vendor-page">
<div class="vendor-list">
 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST PRO OFFICE  </td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;">PURCHASE ORDERS LIST</td></tr>
</table>
<form action="" method="get">
<input type="text" id="fromdate" name="fromdate">
<input type="text" id="todate" name="todate">
<input type="submit" value="submit" class="click">
</form>
<!-- <div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div> -->
<div class="clear"></div>
<div class="list-details">
<%
if (request.getParameter("fromdate") != null && request.getParameter("todate") != null)	{
	String fromdate = request.getParameter("fromdate");
	String todate = request.getParameter("todate");
	purchaseorderListing PUR_L = new purchaseorderListing();
	indentapprovalsListing APPR_L=new indentapprovalsListing();
	superadminListing SAD_l=new superadminListing();
	vendorsListing VEN_L=new vendorsListing();
	List PUR_List=PUR_L.getPurchaseOrderBAsedOnHOID(session.getAttribute("headAccountId").toString(),fromdate+" 00:00:00",todate+" 23:59:59");
	SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat CDF=new SimpleDateFormat("dd MMMM yyyy HH:mm");
	mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
	%>
	<%
	if(PUR_List.size()>0){%>
	<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td class="bg" width="3%">S.No.</td>
	<td class="bg" width="10%">INDENT ID</td>
	<td class="bg" width="10%"><b>PurchaseOrder Invoice</b></td>
	<td class="bg" width="15%">Company</td>
	<td class="bg" width="20%">Vendor Name</td>
	<td class="bg" width="15%">Date</td>
	<td class="bg" width="10%">Requirement</td>
	<td class="bg" width="8%">Status</td>

	</tr>
	<%List APP_Det=null;
	String approvedBy="";
	for(int i=0; i < PUR_List.size(); i++ ){
		PO=(beans.purchaseorder)PUR_List.get(i);
		 %>
	<tr>
	<td><%=i+1%></td>
	<td><%=PO.getindentinvoice_id() %></td>
	<td><a href="purchaseOrderDetailedReport.jsp?poinvid=<%=PO.getpoinv_id()%>"><b><%=PO.getpoinv_id() %></b></a></td>
	<td><%=HOA_CL.getHeadofAccountName(PO.gethead_account_id())%></td>
	<td><%=VEN_L.getMvendorsAgenciesName(PO.getvendor_id()) %></td>
	<%-- <td><ul>
	<li><span><a href="adminPannel.jsp?page=indentDetailedReport&invid=<%=INDT.getindentinvoice_id()%>"><%=INDT.getvendor_id()%>  </a></span><br /></li>
	</ul></td> --%>
	<td><%=CDF.format(SDF.parse(PO.getcreated_date()))%></td>
	<td><%=PO.getrequiremrnt()%></td>
	<td><%=PO.getstatus()%></td>

	</tr>
	<%
	} %>
	</table>
	<%}else{%>
	<div align="center"><h1 style="color: red;font-family: verdana;">Sorry,No Purchase orders Raised Yet</h1></div>
	<%}
}%>
</div>
</div>
</div>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>