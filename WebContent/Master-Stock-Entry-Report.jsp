<%@page import="mainClasses.purchaseorderListing"%>
<%@page import="mainClasses.vendorsListing"%>
<%@page import="mainClasses.godwanstockListing"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!--Date picker script  -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SHOP SALES REPORT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link rel="stylesheet" href="css/acct-style.css" type="text/css"  />
<!--Date picker script  -->
<script src="js/jquery-1.4.2.js"></script>
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
	$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});	
});
function showDetails(billId){
	if(document.getElementById(billId).style.display=="none"){
		document.getElementById(billId).style.display='block';
	}else if(document.getElementById(billId).style.display=="block"){
		document.getElementById(billId).style.display='none';
	}
	
}
</script>
  <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
                             $( "a" ).css("text-decoration","none");
                            $( ".printable" ).print();
 
                            // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<!--Date picker script  -->
<script language="javascript" type="text/javascript">
function popitup(url) {
	newwindow=window.open(url,'name','height=1000,width=500,menubar=yes,status=yes,scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
}
</script>
<%@page import="java.util.List"%>
</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
	<%if(session.getAttribute("empId")!=null){ %>
	<div><%@ include file="title-bar.jsp"%></div>
	<!-- main content -->
	<div>
		<jsp:useBean id="VEN" class="beans.vendors" />
		<jsp:useBean id="SALE" class="beans.customerpurchases" />
		<jsp:useBean id="SA" class="beans.customerpurchases" />
		<jsp:useBean id="BNK" class="beans.banktransactions" />
		<jsp:useBean id="GOD" class="beans.godwanstock" />
		<jsp:useBean id="HOA" class="beans.headofaccounts"></jsp:useBean>
		<div class="vendor-page">

		<div class="vendor-list">
				<div class="arrow-down">
					<!-- <img src="images/Arrow-down.png" /> -->
				</div>
				<div class="sj">
					  <form action="Master-Stock-Entry-Report.jsp" method="post">  
					  <select name="headID" >
					   <%
					   headofaccountsListing HOAL=new headofaccountsListing();
					   List HA_List=HOAL.getActiveHOA();
					   if(session.getAttribute("headAccountId").toString().equals("5")){ %>
						 <option value="5" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("5")){ %>selected="selected" <%} %>>SAINIVAS</option>
						 <%}else{
							 %>
							 						<option value="" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("")){ %> selected="selected" <%} %>>ALL DEPARTMENTS</option>
							 <%
							 for(int i=0;i<HA_List.size();i++){
								HOA=(beans.headofaccounts)HA_List.get(i);
								%>
								<option value="<%=HOA.gethead_account_id() %>" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals(HOA.gethead_account_id())){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
									<%}} %>
					</select>
					
					    <input type="submit" name="weekly" value="Week Report" class="click" style="border:none; "/>
					  	<input type="submit" name="monthly" value="Month Report" class="click" style="border:none; "/>
						<input type="submit" name="yearly" value="Year Report" class="click" style="border:none; "/>
					</form>
				</div>
				<form action="Master-Stock-Entry-Report.jsp" method="post">
				<div class="search-list">
					<ul>
						<%String fromdate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						String todate=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
						if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").equals("")){
							 fromdate=request.getParameter("fromDate");
						}
						if(request.getParameter("toDate")!=null && !request.getParameter("toDate").equals("")){
							todate=request.getParameter("toDate");
						}%>
						<li>
						<select name="headID" >
						 <%if(session.getAttribute("headAccountId").toString().equals("5")){ %>
						 <option value="5" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("5")){ %>selected="selected" <%} %>>SAINIVAS</option>
						 <%}else{
							 %><option value="" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals("")){ %> selected="selected" <%} %>>ALL DEPARTMENTS</option>
		 				<%
		 				for(int i=0;i<HA_List.size();i++){
						HOA=(beans.headofaccounts)HA_List.get(i);
						%>
						<option value="<%=HOA.gethead_account_id() %>" <%if(request.getParameter("headID")!=null && request.getParameter("headID").equals(HOA.gethead_account_id())){ %> selected="selected" <%} %>><%=HOA.getname() %></option>
						<%}} %>
						</select>
						</li>
						<li>
						<input type="text"  name="fromDate" id="fromDate" class="DatePicker" value="<%=fromdate %>"  readonly="readonly" /></li>
						<li><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=todate %>" readonly="readonly"/></li>
					<li><input type="submit" class="click" name="search" value="Search"></input></li>
					</ul>
				</div></form>
				<div class="icons">
					<a id="print"><span><img src="images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> 
														<%-- <a onclick="popitup('shopSalesprint.jsp?fromDate=<%=request.getParameter("fromDate")%>&toDate=<%=request.getParameter("toDate")%>')"><span><img src="../images/printer.png"
						style="margin: 0 20px 0 0" title="print" /></span></a> --%>
						<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
				<div class="clear"></div>
				<div class="list-details">
	<%SimpleDateFormat dbDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat chngDateFormat=new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat chngDF=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat time=new SimpleDateFormat("HH:mm");
	mainClasses.subheadListing SUBHL=new mainClasses.subheadListing();
	productsListing PRDL=new productsListing();
	vendorsListing VENL=new vendorsListing();
	purchaseorderListing POR=new purchaseorderListing();
	Calendar c1 = Calendar.getInstance(); 
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat df2= new SimpleDateFormat("dd-MM-yyyy");
	DateFormat  onlyYear= new SimpleDateFormat("yyyy");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String currentDate=(dateFormat2.format(c1.getTime())).toString();
	String fromDate=currentDate+" 00:00:01";
	String toDate=currentDate+" 23:59:59";
	if(request.getParameter("weekly")!=null && request.getParameter("weekly").equals("Week Report")){
		c1.set(Calendar.DAY_OF_WEEK, c1.getFirstDayOfWeek());
		fromDate=currentDate+" 00:00:00";
		c1.set(Calendar.DAY_OF_WEEK, c1.SATURDAY);
		toDate=(dateFormat2.format(c1.getTime())).toString();
		toDate=toDate+" 23:59:59";
	}else if(request.getParameter("monthly")!=null && request.getParameter("monthly").equals("Month Report")){
		c1.getActualMaximum(Calendar.DAY_OF_MONTH);
		int lastday = c1.getActualMaximum(Calendar.DATE);
		c1.set(Calendar.DATE, lastday);  
		toDate=(dateFormat2.format(c1.getTime())).toString()+" 23:59:59";
		c1.getActualMinimum(Calendar.DAY_OF_MONTH);
		int firstday = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday); 
		fromDate=(dateFormat2.format(c1.getTime())).toString()+" 00:00:00";
	}else if(request.getParameter("yearly")!=null && request.getParameter("yearly").equals("Year Report")){
		int lastday_y = c1.get(Calendar.YEAR);
		c1.set(Calendar.DATE, lastday_y);  
		c1.getActualMinimum(Calendar.DAY_OF_YEAR);
		int firstday_y = c1.getActualMinimum(Calendar.DATE);
		c1.set(Calendar.DATE, firstday_y); 
		Calendar cld = Calendar.getInstance();
		cld.set(Calendar.DAY_OF_YEAR,1); 
		fromDate=(onlyYear.format(cld.getTime())).toString()+"-04-01 00:00:00";
		cld.add(Calendar.YEAR,+1); 
		toDate=(onlyYear.format(cld.getTime())).toString()+"-03-31 23:59:59";
	}else if(request.getParameter("search")!=null && request.getParameter("search").equals("Search")){
		if(request.getParameter("fromDate")!=null){
			fromDate=request.getParameter("fromDate")+" 00:00:01";
		}
		if(request.getParameter("toDate")!=null){
			toDate=request.getParameter("toDate")+" 23:59:59";
		}
	}
	godwanstockListing GODSL=new godwanstockListing();
	String hoid="";
	if(session.getAttribute("headAccountId").toString().equals("5")){
		hoid="5";
	}
	else if(Boolean.parseBoolean(hoid="3")){
		hoid="3";
	}
	else{
		hoid="";
	}
	if(request.getParameter("headID")!=null && !request.getParameter("headID").equals("")){
		hoid=request.getParameter("headID");
	}
	List stkListDet=GODSL.getMasterStockEntryReport(hoid, fromDate, toDate);
	List SAL_DETAIL=null;
	double totalAmount=0.00; 
	DecimalFormat df = new DecimalFormat("0.00");
	String prevDate="";
	String presentDate="";
	double bankDepositTot=0.00;
	double creaditSaleTot=0.00;
	double cashSaleAmt=0.00;
	if(stkListDet.size()>0){%>
	<div class="printable">
					<table width="100%" cellpadding="0" cellspacing="0" id="tblExport">
						<tr>
						<td colspan="7" align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST <%if(HOAL.getHeadofAccountName(request.getParameter("headID"))!=null && !HOAL.getHeadofAccountName(request.getParameter("headID")).equals("")){%> <%=HOAL.getHeadofAccountName(request.getParameter("headID")) %> <%}else{ %> ALL DEPARTMENTS <%} %></td>
					</tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Master Stock Entry Report</td></tr>
					<tr><td colspan="7" align="center" style="font-weight: bold;">Report From Date <%=df2.format(dbDateFormat.parse(fromDate))%>  TO  <%=df2.format(dbDateFormat.parse(toDate)) %> </td></tr>
						<tr>
						<td colspan="7">
						<table width="100%" cellpadding="0" cellspacing="0" border="1">
						<tr>
							<td class="bg" width="2%" style="font-weight: bold;">S.NO.</td>
							<td class="bg" width="12%" style="font-weight: bold;">DATE</td>
							<td class="bg" width="6%" style="font-weight: bold;">MSE.NO.</td>
							<td class="bg" width="6%" style="font-weight: bold;">Unique No</td>
							<td class="bg" width="12%" style="font-weight: bold;">PARTY NAME</td>
							<td class="bg" width="10%" style="font-weight: bold;">BILL.NO</td>
							<td class="bg" width="12%" style="font-weight: bold;" >BILL DATE</td>
							<td class="bg" width="15%" style="font-weight: bold;" >NARRATION</td>
							<td class="bg" width="15%" style="font-weight: bold;" >PRODUCT NAME</td>
							<td class="bg" width="5%" style="font-weight: bold;">QTY</td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">AMOUNT</td>
							<td class="bg" width="20%" style="font-weight: bold;" align="right">PO.NO<!-- /DATE --></td>
							<td class="bg" width="10%" style="font-weight: bold;" align="right">SENT TO WHOM</td>
						</tr>
						<%for(int i=0; i < stkListDet.size(); i++ ){ 
							GOD=(beans.godwanstock)stkListDet.get(i);
							
							%>
						   <tr style="position: relative;">
								<td style="font-weight: bold;"><%=i+1%></td>
								<td style="font-weight: bold;"><%=df2.format(dateFormat.parse(GOD.getdate()))%>/<%=time.format(dateFormat.parse(GOD.getdate()))%></td>
								<td style="font-weight: bold;"><%=GOD.getextra1()%></td>
								<td style="font-weight: bold;"><%=GOD.getExtra9()%></td>
								<td style="font-weight: bold;"><span><%if(GOD.getvendorId()!=null && !GOD.getvendorId().equals("")){ %> <%=VENL.getMvendorsAgenciesName(GOD.getvendorId())%><%}else{ %> N/A<%} %></span></td>
								<td style="font-weight: bold;"><%=GOD.getbillNo()%></td>
								<td style="font-weight: bold;"><%=df2.format(dateFormat.parse(GOD.getdate()))%>/<%=time.format(dateFormat.parse(GOD.getdate()))%></td>
								<td style="font-weight: bold;"><%=GOD.getdescription()%></td>
								<td style="font-weight: bold;">
								<%if(GOD.getExtra7().equals("serviceBill")){ %>
								<%=GOD.getproductId()%> <%=SUBHL.getMsubheadnameWithDepartment(GOD.getproductId() ,GOD.getdepartment()) %>
								<%}else{ %>
								<%=GOD.getproductId()%>  <%=PRDL.getProductsNameByCat2(GOD.getproductId(),GOD.getdepartment())%><%} %></td>
								<td style="font-weight: bold;"><%=GOD.getquantity()%></td>
								<td style="font-weight: bold;"><%=df.format(Double.parseDouble(GOD.getpurchaseRate()))%></td>
								<td style="font-weight: bold;"> <%if(GOD.getextra2()!=null && !GOD.getextra2().equals(" ")){%><%=GOD.getextra2()%><%-- /<%=df2.format(dateFormat.parse(POR.getPurchSOrderDarw(GOD.getextra2())))%> <%=time.format(dateFormat.parse(POR.getPurchSOrderDarw(GOD.getextra2())))%> --%><%}else{ %> <%=GOD.getExtra7() %> <%} %></td>				
								<td><%if(GOD.getdepartment().equals("3")){ %>GODOWN<%}else if(GOD.getdepartment().equals("4")){ %> KITCHEN STORE  <%} %></td>
							</tr>
						<%} %>
					   </table>
						</td>
						</tr>
						<%-- <tr style="border: 1px solid #000;">
						<td colspan="5" align="right" style="font-weight: bold;">Total sales amount | </td>
						<td style="font-weight: bold;" align="right"> &#8377; <%=df.format(totalAmount)%></td>
						<td style="font-weight: bold;" align="right">&#8377;<%=df.format(bankDepositTot) %></td>
						</tr> --%>
					
					</table>
			</div>
					<%}else{%>
					<div align="center">
						<div align="center" style="padding-top: 50px;font: bold;font-size: x-large;"><span>There is no master stock entry found!</span></div>
					</div>
					<%}%>
			</div>
			</div>
</div>
</div>
	<!-- main content -->
	<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>