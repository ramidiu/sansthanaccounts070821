<%@page import="mainClasses.superadminListing"%>
<%@page import="mainClasses.indentapprovalsListing"%>
<%@page import="beans.indent"%>
<%@page import="mainClasses.indentListing"%>
<%@page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock purchase entry</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" lang="javascript" src="js/modal-window.js"></script>	
<script type="text/javascript" lang="javascript">
	var openMyModal = function(source)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = 800;
		modalWindow.height = 500;
		modalWindow.content = "<iframe width='800' height='500' frameborder='0' scrolling='yes' allowtransparency='false' src='" + source + "'></iframe>";
		modalWindow.open();
	
	};	
</script>
<script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( "#tblExport" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
</head>
<jsp:useBean id="INDT" class="beans.indent"/>
<jsp:useBean id="IAP" class="beans.indentapprovals"/>
<body>
<%if(session.getAttribute("empId")!=null){ %>
<div><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page">
 <!-- <table width="95%" cellpadding="0" cellspacing="0">
	<tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST PRO OFFICE  </td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;">INDENTS LIST</td></tr>
</table> -->
<!-- <div class="vendor-box">
<div class="vendor-title">Banks</div>
<div style="clear:both;"></div>

<div class="unpaid-box">
<div class="unpaid">Unpaid</div>
<div class="bills">
<div class="bill-unpaid">
 <ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OPEN BILL</li>
</ul>
</div>
<div class="unpaid-overdue">
<div class="overdue-amount">
<ul>
<li class="bill-amount">&#8377;0</li>
 <li>0 OVERDUE</li>
</ul>
</div>
</div>
</div>
</div>
<div class="paid-box">
<div class="unpaid">Paid</div>
<div class="bills-cash">
<div class="bill-paid">
 <ul>
<li class="bill-amount">&#8377;2000</li>
 <li>Total Bank Balance</li>
</ul>
</div>
</div>
</div>
</div> -->

<div class="vendor-list">
<div class="arrow-down"><img src="images/Arrow-down.png"></div>
<div class="search-list">
<ul>
<li><select>
<option>Batch actions</option>
<option>Email</option>
</select></li>
<li><select>
<option>Sort by name</option>
<option>Sort by company</option>
<option>Sort by overdue balance</option>
<option>Sort by open balance</option>
</select></li>
<li><input type="search" placeholder="find a vendor"/></li>
</ul>
</div>
<div class="icons">
<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
<span>
<ul>
<li><img src="images/Setting-icon.png" />
<div class="mini-menu">
<dl>
  <dt style="color:#666; font-size:12px; font-weight:bold;">Edit Colunms</dt>
   <dt><input type="checkbox" class="edit-setting">Address</dt>
  <dt><input type="checkbox" class="edit-setting">Email</dt>
</dl>
</div>
</li>
</ul>
</span></div>
<div class="clear"></div>
<div id="tblExport">
<table width="95%" cellpadding="0" cellspacing="0">
	<tr><td colspan="8" align="center" style="font-weight: bold;color: red;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST PRO OFFICE  </td></tr>
	<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="8" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
	<tr><td colspan="8" align="center" style="font-weight: bold;">INDENTS LIST</td></tr>
</table>
<div class="list-details">
<%mainClasses.indentListing IND_L = new indentListing();
indentapprovalsListing APPR_L=new indentapprovalsListing();
superadminListing SAD_l=new superadminListing();

mainClasses.employeesListing EMP_l=new mainClasses.employeesListing();
String EMP_HOA=EMP_l.getMemployeesHOA(session.getAttribute("empId").toString());
List IND_List=IND_L.getindentsbasedOnHOA(EMP_HOA);
mainClasses.headofaccountsListing HOA_CL = new mainClasses.headofaccountsListing(); 
List pendingIndents=IND_L.getPendingIndentsBasedOnHod(EMP_HOA);%>
<%
//out.println("size::::");
if(IND_List.size()>0){%>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td class="bg" width="3%"><input type="checkbox"></td>
<td class="bg" width="3%">S.No.</td>
<td class="bg" width="8%">Indent Invoice</td>
<td class="bg" width="15%">Company</td>
<!-- <td class="bg" width="20%">Vendor Name</td> -->
<td class="bg" width="15%">Date</td>
<td class="bg" width="10%">Requirement</td>
<td class="bg" width="8%">Status</td>
<td class="bg" width="18%">Approved By</td>
</tr>
<%List APP_Det=null;
String approvedBy="";
for(int i=0; i < IND_List.size(); i++ ){
	INDT=(indent)IND_List.get(i);
	APP_Det=APPR_L.getIndentDetails(INDT.getindentinvoice_id());
	if(APP_Det.size()>0){
		for(int j=0;j<APP_Det.size();j++){
			IAP=(beans.indentapprovals)APP_Det.get(j);
			if(j==0){
				approvedBy=SAD_l.getSuperadminname(IAP.getadmin_id());
			}else{
				approvedBy=approvedBy+","+SAD_l.getSuperadminname(IAP.getadmin_id());
			}
		}
	} %>
<tr>
<td><input type="checkbox"></td>
<td><%=i+1%></td>
<td><a href="indentDetailedReport.jsp?invid=<%=INDT.getindentinvoice_id()%>"><b><%=INDT.getindentinvoice_id() %></b></a></td>
<td><%=HOA_CL.getHeadofAccountName(INDT.gethead_account_id())%></td>
<%-- <td><ul>
<li><span><a href="indentDetailedReport.jsp?invid=<%=INDT.getindentinvoice_id()%>"><%=INDT.getvendor_id()%>  </a></span><br /></li>
</ul></td> --%>
<td><%=INDT.getdate()%></td>
<td><%=INDT.getrequirement()%></td>
<td><%=INDT.getstatus()%></td>
<td><%=approvedBy%></td>
</tr>
<%
approvedBy="";
} %>
</table>
<%}else{%>
<div align="center"><h1>No Indents Raised Yet</h1></div>
<%}%>
</div>
</div>
</div>
</div>
<%}else{
	response.sendRedirect("index.jsp");
} %>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>