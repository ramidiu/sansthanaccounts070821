<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<link rel="stylesheet" type="text/css" href="css/botstrap.css" />
<link href="css/no-more-tables.css" rel="stylesheet"/>
<link rel="stylesheet" href="css/tables.css" type="text/css" />	
<link rel="stylesheet" href="css/slide2.css" />
<script src="js/jquery-1.8.2.js"></script>
<!-- <script type="text/javascript" src="js/DatePicker.js"></script> -->
   <script>
	$(function() {
	var currdate=new Date();
	$( "#DOB" ).datepicker({	
		   yearRange: "1930:2000",
		changeMonth: true,
        changeYear: true,
		showOtherMonths: true,
        selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
        buttonImageOnly: true
		});
	$( "#date18" ).datepicker({	
		 yearRange: "1990:2028",
		changeMonth: true,
       changeYear: true,
		showOtherMonths: true,
       selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
       buttonImageOnly: true
		});
	$( "#joindate" ).datepicker({	
		 yearRange: "1990:2018",
		changeMonth: true,
       changeYear: true,
		showOtherMonths: true,
       selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
       buttonImageOnly: true
		});
	$(".ui-datepicker-trigger").mouseover(function() {
	    $(this).css('cursor', 'pointer');
	});	
	});
	</script> 
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="ui/jquery.ui.core.js" async></script>
<script src="ui/jquery.ui.widget.js" async></script>
<script src="ui/jquery.ui.datepicker.js" async></script>
<script type="text/javascript">
window.addEvent('domready', function(){
	$$('.DatePicker').each( function(el){
		new DatePicker(el);
	});

});</script>
<title>CREATE EMPLOYEE || SHRI SHIRDI SAI BABA SANSTHAN TRUST</title>
<script type="text/JavaScript">
function shiftdisplay() { 

if(document.getElementById("Duty_Hours").value=="9")
{  alert("9");document.getElementById("shift9").style.display="block";
document.getElementById("shift11").style.display="none";
}
if(document.getElementById("Duty_Hours").value=="11")
{ alert("11");document.getElementById("shift11").style.display="block";
document.getElementById("shift9").style.display="none";}}
</script>
<script type="text/JavaScript">
function VALIDATE() { 

	if(document.getElementById("staff_name").value=="")
	{  alert("please enter  Staff_Name");
	document.getElementById("staff_name").focus(); return false;
	}
	if(document.getElementById("staff_type").value=="")
	{  alert("please select staff type");
	document.getElementById("staff_type").focus();
	return false;
	}
	if(document.getElementById("date18").value=="")
	{ document.getElementById("date18").focus(); return false;
	}
	if(document.getElementById("designation").value=="")
	{ alert("please select designation");
	document.getElementById("designation").focus(); return false;
	}
	if(document.getElementById("status").value=="")
	{ alert("please select Staff Type");
	document.getElementById("status").focus(); return false;
	}
	if(document.getElementById("Duty_Hours").value=="")
	{  alert("please select Duty_Hours");
	document.getElementById("Duty_Hours").focus(); return false;

	}

	}
</script>


<div><jsp:include page="my-account-header.jsp"></jsp:include></div>
<div class="container">
<div class="row">

	<div class="span_12_of_12 main_body ">
	 <div class="col span_10_of_12 middle_body"  style="padding:0% 1.5% 1.5% 1.5% ;">
	 <div class="table_heading span_12_of_12"><span class="page_head">WELCOME TO GENERAL SECRETARY</span> </div> 
	 <div class="span_12_of_12 float_left leave_form1">
	<div class="table_heading span_12_of_12 "><span style="margin:15px auto;width:auto; text-align:center; float:left; color:#ff5500;">Create New staff timings</span> 
          <span style="float:right;"><a href="" class="my_acc_button">BACK</a></span>
         </div>
    <div class="col span_2_of_12">
 


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:left;"  class="view" align="left">

 <tr>
<td colspan=5 height=30> </td>
</tr>
<form name="stafftimings_Update" method="POST" action="stafftimings_Insert.jsp" onsubmit="return VALIDATE();">
<tr>
<td class="normal_text2 " align="left">Staff_ID</td>
<td align="left" colspan="4"><input type="text" name="staff_id" id="staff_id" value="" /></td>
</tr>
<tr>
<td class="normal_text2">Staff_Name</td>
<td align="left" colspan="4"><input type="text" name="staff_name" id="staff_name" value="" /></td>
</tr>
<tr>
<td class="normal_text2">Father's Name</td>
<td align="left" colspan="4"><input type="text" name="father_name" id="father_name" value="" /></td>
</tr>
<tr>
<td class="normal_text2">Mother's Name</td>
<td align="left" colspan="4"><input type="text" name="mother_name" id="mother_name" value="" /></td>
</tr>
<tr>
<td class="normal_text2">Spouse Name</td>
<td align="left" colspan="4"><input type="text" name="spouse_name" id="spouse_name" value="" /></td>
</tr>
<tr>
<td class="normal_text2">Mother Tongue</td>
<td align="left" colspan="4"><input type="text" name="mother_tongue" id="mother_tongue" value="" /></td>
</tr>
<tr>
<td class="normal_text2">Date Of Birth</td>
<td align="left" colspan="4"><input type="text" name="dob" id="DOB" value="" readonly />
<!-- <span><img src="images/calendar-icon.png" /></span> -->
</td>
</tr>
<tr>
<td class="normal_text2">Mobile No</td>
<td align="left" colspan="4"><input type="text" name="phone" id="phone" value="" /></td>
</tr>
<tr>
<td class="normal_text2">Blood Group</td>
<td align="left" colspan="4"><input type="text" name="blood_group" id="blood_group" value="" /></td>
</tr>
<tr>
<td width="204" class="normal_text2" style="width:200px;">Staff Category</td>
<td align="left" colspan="4" style="width:400px;" class=" leave_form1">
<select name="staff_type" id="staff_type" style="width:195px;">
<option value="">select</option>
 <option value="SANSTHAN(SH-1206)">SANSTHAN(SH-1206)</option>
 <option value="HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)">HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)</option>
 <option value="GENERAL ADMINISTRATION DEPARTMENT(SH-1382)">GENERAL ADMINISTRATION DEPARTMENT(SH-1382)</option>
      <option value="CHAPPAL STAND(SH-2784)">CHAPPAL STAND (SH-2784)</option>
      <option value="ACCOUNTS DEPARTMENT(SH-1382)">ACCOUNTS DEPARTMENT (SH-1382)</option>
      <option value="SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)">SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)</option>
 <option value="Laddu Counter(SH-1515)">Laddu Counter(SH-1515)</option>
 <option value="Pooja Stores(SH-1009)">Pooja Stores(SH-1009)</option>
 <option value="MEDICAL CENTRE(SH-1576)">MEDICAL CENTRE(SH-1576)</option>
      <option value="PUBLIC RELATION OFFICER(SH-1382)">PUBLIC RELATION OFFICER(SH-1382)</option>
	  <option value="LIBRARY(1595)">LIBRARY(1595)</option>
	  <option value="SHRI SAINIVAS MEGHA DHARMASALA">SHRI SAINIVAS MEGHA DHARMASALA</option>
	  <option value="SULAB CLEANERS">SULAB CLEANERS</option>
	    <option value="SECURITY">SECURITY</option>
	  <option value="RESTAURANT">RESTAURANT</option>
	  <option value="PRIVATE SECURITY">PRIVATE SECURITY</option>
	  <option value="SANSTHAN SECURITY">SANSTHAN SECURITY</option>   
	  <option value="SERVICES">SERVICES</option>
	  <option value="SANSTHAN OUTSOURCE EMPLOYEES">SANSTHAN OUTSOURCE EMPLOYEES</option>
	  <option value="PHYSIOTHERAPY CENTRE">PHYSIOTHERAPY CENTRE</option>
	  <option value="CLEANING DEPARTMENT">CLEANING DEPARTMENT</option>
	  <option value="EDUCATION">EDUCATION</option>  
	  <option value="DOCTOR">DOCTOR</option>  
	  <option value="VEDHAPANDIT">VEDHAPANDIT</option>  
	  <option value="LADY SECURITY GAURDS">LADY SECURITY GAURDS</option>
 </select></td>
</tr>
<tr>
<td class="normal_text2">Staff_Year</td>

    <td height="30" colspan="3"><div align="center">
            
                <input name="staff_year" id="date18" value="" class="DatePicker"readonly="readonly"/>
            </div></td>
            
          
</tr>


<tr>
<td class="normal_text2">Staff_Designation</td>
<td align="left" colspan="4"  class=" leave_form1">
<jsp:useBean id="STD" class="beans.staff_designation"/>
<select name="designation" id="designation" style="width:195px;">
<option value="">Select</option>
<%
	mainClasses.staff_designationListing STD_CL = new mainClasses.staff_designationListing();
List STD_List=STD_CL.getstaff_designation();
if(STD_List.size()!=0){
for(int i=0; i < STD_List.size(); i++ ){
STD=(beans.staff_designation)STD_List.get(i);
%>
<option value="<%=STD.getdesignation_name()%>"><%=STD.getdesignation_name()%> </option> <%
 	}}
 %>

					</select></td>
</tr>
<tr>
<td class="normal_text2">Date Of Appointment In Trust</td>
<td align="left" colspan="4"><input type="text" name="joindate" id="joindate" value="" readonly/></td>
</tr>

<tr>
<td class="normal_text2">Staff Type </td>
<td width="114" align="left"  class=" leave_form1">
<select name="status" id="status" style="width:195px;">
<option value="">Select</option>
 <option value="permanent">permanent</option>
 <option value="Shift">Shift</option></select>
 </td>
 </tr>
 <tr>
 <td width="193" class="normal_text2" >Staff Shift </td>
 <td align="left" colspan="2"  class=" leave_form1">
 <jsp:useBean id="SHT" class="beans.shift_timings"/>
<select name="Duty_Hours" id="Duty_Hours"  style="width:195px;">
<option value="">Select</option>
<%
	mainClasses.shift_timingsListing SHT_CL = new mainClasses.shift_timingsListing();
List SHT_List=SHT_CL.getshift_timings();
for(int i=0; i < SHT_List.size(); i++ ){
SHT=(beans.shift_timings)SHT_List.get(i);
%>
 <option value="<%=SHT.getshift_starttitming()%>,<%=SHT.getshift_endtiming()%>,<%=SHT.getshift_code()%>"><%=SHT.getshift_code()%></option>
 <%}%>
 </select>
 </td></tr>

<tr>
<td class="normal_text2">Permanent Address</td>
<td align="left" colspan="4"><textarea rows=5 cols=35  name="permt_add" id="permt_add" value=""></textarea></td>
</tr>
<tr>
<td class="normal_text2">Present Address</td>
<td align="left" colspan="4"><textarea rows=5 cols=35 name="present_add" id="present_add" value=""></textarea></td>
</tr>
<%-- <tr>
<td class="normal_text">Photo Upload</td>
<td align="left" colspan="4">
<input name="profileImage" id="profileImage" type="file" />
<img src="<%=request.getContextPath()%>/personalregistry/ %>" style="height: 88px;width: 100px;" />
</td>
</tr> --%>
<tr>

<td colspan="7" align="center"> <input type="submit" name="Submit" value="Continue" class="kws_button" /></td>

</tr>
</form>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="clear:both"></div>  
