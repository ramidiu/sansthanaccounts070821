<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="helperClasses.SaiMonths"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat,java.util.List"%>
<%@page import="java.util.Calendar"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EMPLOYE MONTHLY ATTENDANCE REPORT</title>
<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<!-- <link rel="stylesheet" type="text/css" href="css/device.css" /> -->
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<!-- <link href="css/no-more-tables.css" rel="stylesheet"/> -->
<script src="js/jquery-1.8.2.js"></script>
<script type="text/javascript">
function validate(){
	if(document.getElementById("month").value=="" || document.getElementById("year").value==""){
		alert("select month and year");
		return false;
	}
}
</script>
 <script type="text/javascript">
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<script src="js/jquery.battatech.excelexport.js"></script>
</head>
<jsp:useBean id="EMAF" class="beans.employee_attendence"/>
<jsp:useBean id="EMA" class="beans.employee_attendence"/>
<jsp:useBean id="SFT" class="beans.stafftimings"/>
<jsp:useBean id="SFTQ" class="beans.stafftimings"/>
<body>
  <% 
  
  	if(session.getAttribute("MemberId")!=null || session.getAttribute("admin_id")!=null){
  %>
  <%SaiMonths sm = new SaiMonths(); %>
<div><div ><jsp:include page="my-account-header.jsp"></jsp:include></div>
</div> <div style="clear:both"></div>  <div class="container">
<div class="row">
	<div class="span_12_of_12 main_body " >
    
    <%-- <div class="col span_2_of_12"><jsp:include page="my-account-left-nav.jsp"></jsp:include></div> --%>
    
    <div class="span_12_of_12 middle_body"  style="padding:0% 1.5% 1.5% 1.5% ;">
    <div class="table_heading span_12_of_12"><span class="page_head">WELCOME TO GENERAL SECRETARY</span> </div><div class="span_12_of_12 float_left">
    <section id="no-more-tables">
    <div class="table_heading span_12_of_12"><span style="margin:15px auto;width:auto; text-align:center; color:#ff5500;"> 	
EMPLOYE MONTHLY ATTENDANCE REPORT<%if(request.getParameter("year")!=null && request.getParameter("month")!=null){%>(<%=SaiMonths.getMonthName(Integer.parseInt(request.getParameter("month")))%>-<%=request.getParameter("year")%>)<%} %></span> </div>  
<form name="yearform" id="yearform" action="employee-attendence-Listing.jsp" method="post">
<div class="span_12_of_12" style="float:left;padding:5px 0px 5px 10px;"> 
<input type="hidden" name="ty" value="<%=request.getParameter("ty")%>"></input>
           <div class="col span_3_of_12"><span class="sub_head" style="float:right;margin-right:10px;">MONTH </span></div>
           <div class="col span_2_of_12 " >
           		<select name="month"  id="month">
           			<option value="">-month-</option>
                     <option value="0">January</option>
                     <option value="1">February</option>
                     <option value="2">March</option>
                     <option value="3">April</option>
                     <option value="4">May</option>
                     <option value="5">June</option>
                     <option value="6">July</option>
                     <option value="7">August</option>
                     <option value="8">September</option>
                      <option value="9">October</option>
                       <option value="10">November</option>
                      <option value="11">December</option>
                 </select>
           
           </div><div class="col span_1_of_12"><span class="sub_head" style="float:right;margin-right:10px;">YEAR </span></div> 
            <div class="col span_2_of_12"> 
            	<select  name="year"  id="year">
    				<option value="">-year-</option>
					 <%
					 	for(int i=2013;i<2050;i++){
					 %>
							<option value="<%=i%>"><%=i%></option> 
		 				<%
 		 					}
 		 				%>			 
				 </select>
            </div><div class="col span_4_of_12">
                 <input  type="submit" name="submit" value="SEARCH" onClick="return validate();"/>
                 </div></div>  
         </form>
        
<%
	if(request.getParameter("year")!=null && request.getParameter("month")!=null){
%>      
 <input type="hidden" name="year" id="year" value="<%=request.getParameter("year")%>"/>
  <input type="hidden" name="month" id="month" value="<%=request.getParameter("month")%>"/>
  <input type="hidden" name="ty" value="<%=request.getParameter("ty")%>"></input>
  <div class="span_12_of_12 float_left"><a href="employee-attendence-Listing-excelsheet.jsp?year=<%=request.getParameter("year")%>&amp;month=<%=request.getParameter("month")%>&amp;ty=<%=request.getParameter("ty")%>"><input type="button" name="Button" value="Monthly Excel Sheet" class=" my_acc_button" style="float:right;"/></a></div>
   <!-- <div class="span_12_of_12 float_left"><a href="#" id="btnExport"  class="my_acc_button" style="float:right;"> Monthly Excel Sheet</a></div> -->
   <%
   Calendar cal = Calendar.getInstance();
   Calendar calls = Calendar.getInstance(); 
   cal.set(Integer.parseInt(request.getParameter("year").toString()),Integer.parseInt(request.getParameter("month").toString()),1);
    calls.set(Integer.parseInt(request.getParameter("year").toString()),Integer.parseInt(request.getParameter("month").toString()),cal.getActualMaximum(Calendar.DAY_OF_MONTH));
   SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
   SimpleDateFormat dtf=new SimpleDateFormat("dd/MM/yyyy");
   String fDay = dtf.format(cal.getTime());
   String lDay = dtf.format(calls.getTime());
   int timegap=(int)((calls.getTimeInMillis()-cal.getTimeInMillis())/(24*60*60*1000));
   %>
   <div class="span_12_of_12 float_left" style="padding:5px 0px 5px 0px ;" >
      <span class="sub_head">Dates :<span style="color:#666666;margin-left:8px;"> &nbsp;&nbsp;<%=fDay%>&nbsp;&nbsp;-&nbsp;&nbsp;<%=lDay%></span></span>
     </div>             
<div class="span_12_of_12 float_left" id="tblExport">              
                  <div style="overflow: auto; width:20%; height: auto;  float: left; ">
<table width="100%" border="1" cellspacing="0" bgcolor="" cellpadding="0" style="margin:0;width:100%;border:none !important;" >
<tr>
<th align="center" bgcolor="#fffef3" class="attendance_text" height="65">ID</th>
<th align="center" bgcolor="#fffef3" class="attendance_text"  >Name</th>
</tr>
<%
	mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
mainClasses.employee_attendenceListing EMA_CL = new mainClasses.employee_attendenceListing();
//List STF_List=SFT_CL.getALLStaffForAttendence(String.valueOf(cal.get(cal.YEAR)));
String year="";
if(request.getParameter("ty")!=null && request.getParameter("ty").equals("sansthan")){
	year="2013";
}else if(request.getParameter("ty")!=null && request.getParameter("ty").equals("sainivas")){
	year="2014";
}
List STF_List=SFT_CL.getALLStaffForAttendence(year);

 for(int i=0; i < STF_List.size(); i++ ){ 
SFT=(beans.stafftimings)STF_List.get(i);
List STA_List=SFT_CL.getMstafftimings(SFT.getstaff_id().toString());
for(int G=0; G < STA_List.size(); G++ ){
SFTQ=(beans.stafftimings)STA_List.get(0);}
%>
<tr>
<td align="center" bgcolor="#FFF" class="attendance_text1" ><%=SFTQ.getstaff_id()%></td>
<td align="center" bgcolor="#FFF" class="attendance_text1"><%=SFTQ.getstaff_name()%></td>
</tr>
<%
	}
%>
</table>
</div>             
<div style="overflow: auto; width: 80%; height: auto; float: right;margin:0;padding:0;">
<table width="100%" border="1" cellspacing="0" bgcolor="#fffef3" cellpadding="0" style="margin:0 !important;"> 
<tr>
<th align="center" bgcolor="#fffef3" class="attendance_text">Type</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">Designation</th>
<%
	for(int p=0;p<=timegap;p++){
%>
 <td style="margin:0 !important;padding:0 !important;"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0 !important;padding:0 !important;">
<tr><th colspan="6" align="center" bgcolor="#fffef3" class="attendance_text"><%=dtf.format(cal.getTime())%></th></tr>
<tr>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600; ">LA</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">LP</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">CL</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">Absent</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">OD</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">OT</div></th>
</tr></table>
</td>
<%
	cal.add(Calendar.DATE,+1);}
%>
<td style="margin:0 !important;padding:0 !important;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0 !important;padding:0 !important;">
<tr><th colspan="6" align="center" bgcolor="#fffef3" class="attendance_text">Cummilative</th></tr>
<tr>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">LA</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">LP</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">CL</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">Absent</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">OD</div></th>
<th bgcolor="#fffef3" class="attendance_text"><div align="center" style="width: 50px;color:#ff6600;">OT</div></th>
</tr></table>
</td>
</tr>

<%
	//STF_List=SFT_CL.getALLStaffForAttendence(String.valueOf(cal.get(cal.YEAR)));
STF_List=SFT_CL.getALLStaffForAttendence(year);
 //String fDay = sdf.format(cal.getTime());
 //String lDay = sdf.format(calls.getTime());
 for(int i=0; i < STF_List.size(); i++ ){ 
SFT=(beans.stafftimings)STF_List.get(i);
List STA_List=SFT_CL.getMstafftimings(SFT.getstaff_id().toString());
for(int G=0; G < STA_List.size(); G++ ){
SFTQ=(beans.stafftimings)STA_List.get(0);}
%>
<tr>
<td align="center" bgcolor="#FFF" class="attendance_text1"><%=SFTQ.getstatus()%></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 350px;"><%=SFTQ.getdesignation()%></div></td>
<%
	cal = Calendar.getInstance();
calls = Calendar.getInstance(); 
cal.set(Integer.parseInt(request.getParameter("year").toString()),Integer.parseInt(request.getParameter("month").toString()),1);
calls.set(Integer.parseInt(request.getParameter("year").toString()),Integer.parseInt(request.getParameter("month").toString()),cal.getActualMaximum(Calendar.DAY_OF_MONTH));
timegap=(int)((calls.getTimeInMillis()-cal.getTimeInMillis())/(24*60*60*1000));

float LA=0,LP=0,CL=0,AB=0,OT=0,OD=0,temp_CL=0;
for(int p=0;p<=timegap;p++){
%> 

<%
 	List forlraves=EMA_CL.getEmployeeMonthLeavese(SFTQ.getstaff_id(),sdf.format(cal.getTime()),sdf.format(cal.getTime()));
 ArrayList lis=new ArrayList();
 String temp14="";
 if(forlraves.size()>0){
for(int t=0; t < forlraves.size(); t++ ){ 
 EMAF=(beans.employee_attendence)forlraves.get(t);
 if(!EMAF.getlate_hours().equals("") && !EMAF.getlate_hours().equals(null)){
 	temp_CL=Float.parseFloat(EMAF.getlate_hours());
 }else{
 	temp_CL=0;
 }
 float dem_LP=0,dem_LA=0,dem_CL=0,dem_AB=0,dem_OD=0,dem_OT=0;
 if(EMAF.getlate_hourpermision().equals("Yes")){LP=LP+1;dem_LP=dem_LP+1;} 
 if(EMAF.getextra5().equals("LA")){LA=LA+1;dem_LA=dem_LA+1;} 
 if(EMAF.getextra5().equals("CL")){CL=CL+temp_CL;dem_CL=dem_LP+temp_CL;}
 if(EMAF.getextra5().equals("Absent")){AB=AB+1;dem_AB=dem_AB+1;}
 if(EMAF.getextra5().equals("OD")){OD=OD+1;dem_OD=dem_OD+1;}
 if(!EMAF.getextar7().equals("")){OT=OT+Float.parseFloat(EMAF.getextar7());dem_OT=dem_OT+Float.parseFloat(EMAF.getextar7());}
 if(EMAF.getlate_hours().toString().equals("") && EMAF.getextra5().toString().equals("") && !EMAF.getlate_hours().toString().equals(null) && !EMAF.getextra5().toString().equals(null)){temp14="In Time";}else{temp14=EMAF.getlate_hours()+" "+EMAF.getextra5();}
 %>

<td style="padding:0px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0 !important;" class="values">
<tr>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)dem_LP %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)dem_LA %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=dem_CL %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)dem_AB %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)dem_OD %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)dem_OT %></div></td>
</tr></table>
</td>
<% }}else{%>
	<td bgcolor="#FFF" class="attendance_text1"></td> 
<%}cal.add(Calendar.DATE,+1);} %>
<td style="padding:0px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0 !important;" class="values">
<tr>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)LA %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)LP %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=CL %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)AB %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)OD %></div></td>
<td bgcolor="#FFF" class="attendance_text1"><div align="center" style="width: 50px;"><%=(int)OT %></div></td>
</tr></table>
</td>
</tr>
<%}%>
</table>
</div>
</div>
<%} %>
</section></div> </div> </div>
	</div>
</div>
 <div style="clear:both"></div>  
 <%}else{ response.sendRedirect("index.jsp");}%>
</body>
</html>