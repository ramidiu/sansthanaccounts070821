<%@page import="java.util.List"%>
<%@page import="beans.stafftimings_details"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<link rel="stylesheet" type="text/css" href="css/botstrap.css" />
<link href="css/no-more-tables.css" rel="stylesheet"/>
<link rel="stylesheet" href="css/tables.css" type="text/css" />	
<link rel="stylesheet" href="css/slide2.css" />
<script src="js/jquery-1.8.2.js"></script>
<script>
	$(function() {
	var currdate=new Date();
	$( "#DOB" ).datepicker({	
		   yearRange: "1930:2000",
		changeMonth: true,
        changeYear: true,
		showOtherMonths: true,
        selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
        buttonImageOnly: true
		});
	$( "#joindate" ).datepicker({	
		 yearRange: "1990:2018",
		changeMonth: true,
        changeYear: true,
		showOtherMonths: true,
        selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
        buttonImageOnly: true
		});
	$( "#end_period" ).datepicker({	
		 yearRange: "1990:2018",
		changeMonth: true,
       changeYear: true,
		showOtherMonths: true,
       selectOtherMonths: true,
	    dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
       buttonImageOnly: true
		});
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	
</script>
<title>EDIT EMPLOYEE DETAILS|| SHRI SHIRDI SAI BABA SANSTHAN TRUST</title>
<link rel="stylesheet" href="themes/ui-lightness/jquery.ui.all.css"/>
<script src="ui/jquery.ui.core.js" async></script>
<script src="ui/jquery.ui.widget.js" async></script>
<script src="ui/jquery.ui.datepicker.js" async></script>


<jsp:useBean id="SFT" class="beans.stafftimings"/>
<jsp:useBean id="SFTD" class="beans.stafftimings_details"/>


<div><jsp:include page="my-account-header.jsp"></jsp:include></div>
<div class="container">
<div class="row">

	<div class="span_12_of_12 main_body ">
	 <div class="col span_10_of_12 middle_body"  style="padding:0% 1.5% 1.5% 1.5% ;">
	 <div class="table_heading span_12_of_12"><span class="page_head">WELCOME TO GENERAL SECRETARY</span> </div> 
	 <div class="span_12_of_12 float_left leave_form1">
	<div class="table_heading span_12_of_12 "><span style="margin:15px auto;width:auto; text-align:center; float:left; color:#ff5500;">Edit staff timings</span> 
          <span style="float:right;"><a href="" class="my_acc_button">BACK</a></span>
         </div>
    <div class="col span_2_of_12">
    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" class="view" style="background: #fff;">
    
<form name="stafftimings_Update" method="POST" action="stafftimings_Update.jsp" enctype="multipart/form-data">
 <%
 	String staff_id=request.getParameter("staff_id");
 mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
 List SFT_List=SFT_CL.getMstafftimings(staff_id);
 for(int i=0; i < SFT_List.size(); i++ ){
 SFT=(beans.stafftimings)SFT_List.get(i);
 %>
 <tr>
<td colspan="5" height="30"> </td>
</tr>
<tr>
<td class="normal_text2 ">staff_Id</td>
<td>
<input type="text" name="staff_ide" id="staff_ide" value="<%=SFT.getstaff_id()%>" readonly="readonly" /></td>
<input type="hidden" name="staff_id" id="staff_id" value="<%=SFT.getstaff_id()%>" /></tr>
<tr>
<td class="normal_text2 ">staff_year</td>
<td><input type="text" name="staff_year" id="staff_year" value="<%=SFT.getstaff_year()%>"  readonly="readonly"/></td>
</tr>
<tr>
<td class="normal_text2 ">Staff Category</td>
<td>
<select name="staff_type" id="staff_type" style="    width: 195px;">
<option value="<%=SFT.getstaff_type()%>"><%=SFT.getstaff_type()%></option>
 <option value="SANSTHAN(SH-1206)">SANSTHAN(SH-1206)</option>
 <option value="HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)">HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)</option>
 <option value="GENERAL ADMINISTRATION DEPARTMENT(SH-1382)">GENERAL ADMINISTRATION DEPARTMENT(SH-1382)</option>
      <option value="CHAPPAL STAND(SH-2784)">CHAPPAL STAND (SH-2784)</option>
      <option value="ACCOUNTS DEPARTMENT(SH-1382)">ACCOUNTS DEPARTMENT (SH-1382)</option>
      <option value="SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)">SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)</option>
 <option value="Laddu Counter(SH-1515)">Laddu Counter(SH-1515)</option>
 <option value="Pooja Stores(SH-1009)">Pooja Stores(SH-1009)</option>
 <option value="MEDICAL CENTRE(SH-1576)">MEDICAL CENTRE(SH-1576)</option>
      <option value="PUBLIC RELATION OFFICER(SH-1382)">PUBLIC RELATION OFFICER(SH-1382)</option>
	  <option value="LIBRARY(1595)">LIBRARY(1595)</option>
	  <option value="SHRI SAINIVAS MEGHA DHARMASALA">SHRI SAINIVAS MEGHA DHARMASALA</option>
	    <option value="SECURITY">SECURITY</option>
	   <option value="RESTAURANT">RESTAURANT</option>
	   <option value="PRIVATE SECURITY">PRIVATE SECURITY</option>
	  <option value="SANSTHAN SECURITY">SANSTHAN SECURITY</option>   
	  <option value="SERVICES">SERVICES</option>
	  <option value="SANSTHAN OUTSOURCE EMPLOYEES">SANSTHAN OUTSOURCE EMPLOYEES</option>
	  <option value="PHYSIOTHERAPY CENTRE">PHYSIOTHERAPY CENTRE</option> 
	  <option value="CLEANING DEPARTMENT">CLEANING DEPARTMENT</option>
	  <option value="EDUCATION">EDUCATION</option>  
	  <option value="DOCTOR">DOCTOR</option>  
	  <option value="VEDHAPANDIT">VEDHAPANDIT</option>  
	  <option value="LADY SECURITY GAURDS">LADY SECURITY GAURDS</option>
 </select></td>
</tr>
<tr>
<td class="normal_text2 ">staff_name</td>
<td><input type="text" name="staff_name" id="staff_name" value="<%=SFT.getstaff_name()%>" /></td>
</tr>
<tr>
<td class="normal_text2 ">Father's Name</td>
<td><input type="text" name="father_name" id="father_name" value="<%=SFT.getextra3()%>" /></td>
</tr>
<tr>
<td class="normal_text2 ">Mother's Name</td>
<td><input type="text" name="mother_name" id="mother_name" value="<%=SFT.getextra4()%>" /></td>
</tr>
<tr>
<td class="normal_text2 " >Spouse Name</td>
<td><input type="text" name="spouse_name" id="spouse_name" value="<%=SFT.getExtra5()%>" /></td>
</tr>
<tr>
<td class="normal_text2 ">Mother Tongue</td>
<td><input type="text" name="mother_tongue" id="mother_tongue" value="<%=SFT.getExtra6()%>" /></td>
</tr>
<tr>
<td class="normal_text2 ">designation</td>
<td>
<jsp:useBean id="STD" class="beans.staff_designation"/>
<select name="designation" id="designation" style="    width: 195px;" >
<option value="<%=SFT.getdesignation()%>"><%=SFT.getdesignation()%></option>
<%
	mainClasses.staff_designationListing STD_CL = new mainClasses.staff_designationListing();
List STD_List=STD_CL.getstaff_designation();
if(STD_List.size()!=0){
for(int s=0; s < STD_List.size(); s++ ){
STD=(beans.staff_designation)STD_List.get(s);
%>
<option value="<%=STD.getdesignation_name()%>"><%=STD.getdesignation_name()%> </option> <%
 	}}
 %>

</select></td>
</tr>
<tr>
<td class="normal_text2 ">Staff Type</td>
<td>
<select name="status" id="status" style="    width: 195px;">
<option value="<%=SFT.getstatus()%>"><%=SFT.getstatus()%></option>
<%
	if(SFT.getstatus().toString().equals("permanent")){
%>
 <option value="Shift">Shift</option><%
 	}
 %>
 <%
 	if(SFT.getstatus().toString().equals("Shift")){
 %><option value="permanent">permanent</option><%
 	}
 %>
</select></td>
</tr>

<tr>
<td class="normal_text2 ">Shift</td>
<td>
<jsp:useBean id="SHT" class="beans.shift_timings"/>
<select name="start_period" id="start_period" style="    width: 195px;" >
<%
	mainClasses.shift_timingsListing SHT_CL = new mainClasses.shift_timingsListing();
	List SHT_List=SHT_CL.getshift_timings();
	for(int r=0; r < SHT_List.size(); r++ )
	{
	SHT=(beans.shift_timings)SHT_List.get(r);
	%>
 		<option value="<%=SHT.getshift_starttitming()%>,<%=SHT.getshift_endtiming()%>,<%=SHT.getshift_code()%>" <%if(SFT.getstart_period().toString().equals(SHT.getshift_code().toString())){%> selected="selected"<%}%>><%=SHT.getshift_code()%></option>
 	<%}%>
 </select></td>
</tr>

<tr>
<td class="normal_text2 ">Mobile No</td>
<td>
<input type="text" name="extra2" id="mobileno"  value="<%=SFT.getMobile_no() %>"/></td>
</tr>
<tr>
<td class="normal_text2 ">Permanent Address</td>
<td>
<textarea name="emergencyno" id="emergencyno" style="    width: 195px;"><%=SFT.getEmergency_no() %></textarea></td>
</tr>
<tr>
<td class="normal_text2 "> Present Address</td>
<td>
<textarea name="extra1" id="address" style="    width: 195px;" ><%=SFT.getAddress() %></textarea></td>
</tr>
<tr>
<td class="normal_text2 ">Blood Group</td>
<td>
<input type="text" name="bloodgroup"   value="<%=SFT.getBlood_group() %>"/></td>
</tr>
<tr>
<td class="normal_text2 ">Date Of Birth</td>
<td>
<input type="text" name="extra3" id="DOB" value="<%=SFT.getDOB() %>" /></td>
</tr>
<tr>
<td class="normal_text2 ">Date Of Appointment In Trust</td>
<td><input type="text" name="joindate" id="joindate" value="<%=SFT.getextra2()%>" /></td>
</tr>
<tr>
<td class="normal_text2 ">Photo Upload</td>
<td style="width: 195px"><input name="profileImage" id="profileImage" type="file"  /></td>
<td>

<img src="<%=request.getContextPath()%>/personalregistry/<%=SFT.getextra1() %>" style="height: 88px;width: 100px;" />

</td>

</tr>
<tr>
<td class="normal_text2 ">EMP No</td>
<td><input type="text" name="extra4" id="new_staff_id" value="<%=SFT.getStaff_nid()%>"  /></td>
</tr>
<tr>
<td class="normal_text2 ">Re-Signed Date</td>
<td>
<input type="text" name="end_period"  id="end_period" value="<%=SFT.getend_period()%>"/></td>
</tr>

<%}%>

<%
 	String staff_ids=request.getParameter("staff_id");
 mainClasses.stafftimings_detailsListing SFTD_CL = new mainClasses.stafftimings_detailsListing();
 stafftimings_details staffDetails=SFTD_CL.getMstafftimings_details(staff_ids);
 %>
 <tr>
<td></td>
<td>
<input type="hidden" name="staff_ids" id="staff_ids" value="<%=staff_ids%>" /></tr>
<tr>
<td class="normal_text2 ">Government Reservation</td>
<td>
<input type="radio" name="govern_reser" value="yes" <%if(staffDetails.getGovernment_reservation().equals("yes")){ %>checked="checked"	<%}%> >YES
<input type="radio" name="govern_reser" value="no" <%if(staffDetails.getGovernment_reservation().equals("no")){ %>checked="checked"	<%} %> >NO
</td>
</tr>
<tr>
<td class="normal_text2 ">Caste & Religion</td>
<td>
<input type="text" name="caste_rel"   value="<%=staffDetails.getCaste_regligion() %>"/></td>
</tr>
<tr>
<td class="normal_text2 "> Dependents</td>
<td>
<textarea name="dependent" id="dependent" style="width:195px"><%=staffDetails.getDependents()%></textarea></td>
</tr>
<tr>
<td>Employee Documents</td>
<td>
<input name="document1" id="document1" type="file" />
<%=staffDetails.getExtra1() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra1() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra1() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document2" id="document2" type="file" />
<%=staffDetails.getExtra2() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra2() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra2() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document3" id="document3" type="file" />
<%=staffDetails.getExtra3() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra3() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra3() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document4" id="document4" type="file" />
<%=staffDetails.getExtra4() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra4() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra4() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document5" id="document5" type="file" />
<%=staffDetails.getExtra5() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra5() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra5() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document6" id="document6" type="file" />
<%=staffDetails.getExtra6() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra6() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra6() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document7" id="document7" type="file" />
<%=staffDetails.getExtra7() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra7() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra7() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document8" id="document8" type="file" />
<%=staffDetails.getExtra8() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra8() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra8() %>" style="height: 30px;width: 30px;" /></a><br/>
<input name="document9" id="document9" type="file" />
<%=staffDetails.getExtra9() %>
<a href="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra9() %>" target="_blank"><img src="<%=request.getContextPath()%>/employeedocuments/<%=staffDetails.getExtra9() %>" style="height: 30px;width: 30px;" /></a><br/>
</td>
</tr>
<tr>
<td class="normal_text2 ">Educational Qualification</td>
<td>
<input type="text" name="edu_quali"   value="<%=staffDetails.getEducational_qual() %>"/></td>
</tr>
<tr>
<td class="normal_text2 ">Technical Qualification</td>
<td>
<input type="text" name="tech_quali"   value="<%=staffDetails.getTechnical_qual() %>"/></td>
</tr>
<tr>
<td class="normal_text2 ">Indentifications Marks</td>
<td>
<input type="text" name="ind_mark"   value="<%=staffDetails.getIdentification_marks() %>"/></td>
</tr>
<tr>
<td class="normal_text2 "> Previous Experience</td>
<td>
<textarea name="pre_exper" id="pre_exper" style="width:195px"><%=staffDetails.getPrevious_experience() %></textarea></td>
</tr>
<tr>
<td class="normal_text2 ">Height</td>
<td>
<input type="text" name="height"   value="<%=staffDetails.getHeight() %>"/></td>
</tr>
<tr>
<td class="normal_text2 "> Any Physical Disorder</td>
<td>
<textarea name="phy_disorder" id="phy_disorder" style="width:195px" ><%=staffDetails.getPhysical_disorder() %></textarea></td>
</tr>
<tr>
<td class="normal_text2 ">Present Pay Scale</td>
<td>
<input type="text" name="pres_pay_scale"   value="<%=staffDetails.getPresent_pay_scale() %>"/></td>
</tr>
<tr>
<td class="normal_text2 ">Basic Pay</td>
<td>
<input type="text" name="basic_pay"   value="<%=staffDetails.getBasic_pay() %>"/></td>
</tr>
<tr>
<td class="normal_text2 "> Reference Of Any Two Members</td>
<td>
<textarea name="reference" id="reference" style="width:195px"><%=staffDetails.getReference() %></textarea></td>
</tr>
<tr>
<td class="normal_text2 ">Graduvite Amount</td>
<td>
<input type="text" name="graduvite_amount"   value="<%=staffDetails.getGraduvite_amount()%>"/></td>
</tr>
<tr>
<td> </td>
<td ><input type="submit" name="Submit" value="UPDATE" class="submit_button"/></td>
</tr>
</form>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="clear:both"></div>  