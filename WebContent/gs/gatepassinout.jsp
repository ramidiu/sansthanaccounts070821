<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="mainClasses.GatePassService"%>
<%@ page import="beans.GatePassEntry"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html>
<html>
<head>
<title>gatepassinout</title>

<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<link href="css/no-more-tables.css" rel="stylesheet"/>

<script src="js/jquery-1.8.2.js"></script>
<script>
	$(function() {
	var currdate=new Date();
	
		$( "#datestart" ).datepicker({	
			changeMonth: true,
	        changeYear: true,
			showOtherMonths: true,
	        selectOtherMonths: true,
	      dateFormat: 'yy-mm-dd',
			constrainInput: true,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
	        buttonImageOnly: true,
			});
		$( "#dateend" ).datepicker({	
			changeMonth: true,
	        changeYear: true,
			showOtherMonths: true,
	        selectOtherMonths: true,
	      dateFormat: 'yy-mm-dd',
			constrainInput: true,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
	        buttonImageOnly: true,
			});
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	});
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	
</script>
<script type="text/javascript">
function datescheck(){
var DaysDiff;

//Date1 = new Date(document.getElementById("dateend").value);
//Date2 = new Date( document.getElementById("cursdate").value);
var myDate = document.getElementById("dateend").value;
	var myDate1 = document.getElementById("cursdate").value;
var datestart = document.getElementById("datestart").value;
if (myDate1<=datestart) {
		 alert("End Date must be lessthan the  StartDate");
          document.getElementById("datestart").focus();
return false;
}
	 // alert(document.getElementById("dateend").value);
	    if (myDate1<=myDate) {
		 alert("End Date must be lessthan the  CurrentDate");
          document.getElementById("dateend").focus();

return false;
}
	   
}</script>
<script type="text/javascript">
function check1(){
	
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one for appprove");
	return false;} else{
		 var ty = document.getElementById("ty").value;
		 /*var datee = document.getElementById("datee").value; */
	document.getElementById("approveForm").action="gatepassapprove.jsp?ty="+ty;
	document.getElementById("approveForm").submit();
	}
}
</script>
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<style>

.ui-datepicker-trigger{position: relative;
    top: 6px;}
</style>
</head>
<body>
<div><div ><jsp:include page="my-account-header.jsp"></jsp:include></div>
</div>
<div style="clear:both"></div> 

	<section class="abcd">
	         <div class="container" style="margin-right: auto; margin-left: auto; padding-left: 44px; padding-right: 140px;">
	              <div class="row">
	                   <div class="table_heading span_12_of_12" style="padding-top: 7%;">
	                         <span class="page_head">WELCOME TO GENERAL SECRETARY</span> 
	                   </div>
	                  <div class="table_heading span_12_of_12 float_left " style="">
                            <span style="width:auto; font-size:14px;font-weight:bold;  text-align:center; float:left; color:#ff5500;">GATEPASSENTRY</span> 
                            <div style="float:right;width:auto;height:auto;">
       	                         <a href="my-home.jsp" class="my_acc_button">Back</a>
                            </div>
                       </div>
                      <form name="yearform" id="yearform" action="gatepassinout.jsp" method="post">
                        <input type="hidden" name="page" id="page" value="gatepassinout" />
                        <input type="hidden" name="ty" id="ty"  value="<%=request.getParameter("ty")%>" />
                       <div class="span_12_of_12 " style="float:left;padding:5px 0px 5px 0px;"> 
	                         <div class="col span_3_of_12">
                                  <div class="my_acc_text_feild"> 
                                       Start Date : 
                                  </div> 
                                  <div class="my_acc_input">
                                       <input type="text" name="datestart" id="datestart" class="DatePicker " readonly="readonly" required=""> 
                                  </div>
                                  <!-- <div class="cal_icon"> 
                                      <img src="images/calendar-icon2.png">
                                  </div> -->
                             </div>
                             <div class="col span_3_of_12">
                                   <div class="my_acc_text_feild">
                                        End  Date : 
                                   </div>
                                   <div class="my_acc_input">
                                        <input type="text" name="dateend" id="dateend" class="DatePicker" readonly="readonly" required="">
                                   </div>
                                   
                             </div>
  
                              <div class="col span_1_of_12">
                                  <input type="submit" name="submit" value="SEARCH" onclick="return datescheck();">
                               </div>
                          </div>
                          </form>
                          <form id="approveForm">
                          <div class="span_12_of_12 float_left" style="padding:5px 0px 5px 0px ;  height:auto;  ">
                                 <input type="submit" name="button" id="button" class="my_acc_button" value="APPROVE" onclick="return check1();">
                          </div>
     <table class="table-bordered table-striped table-condensed" style="width:100%; margin-top:15px;" id="tblExport">
          <thead>
               <tr>   
         	      <th><input type="checkbox" name="SelectAll" id="SelectAll" value="yes" onclick="checkAll();"> </th>
              	  <th>Date</th>
         	      <th>GatePassEntryId</th>
			      <th>StaffName</th>
				  <th>Department</th>
				  <th>Description</th>
				  <th>OutTime(HH:MM)</th>
				  <th>InTime(HH:MM)</th>
				  <th>Diffrence(HH:MM:SS)</th>
				  <th>GatePassEntry CL Type</th>
				  <th>GatePassType</th>
		      </tr>
       </thead>
       <tbody>
           <%
               SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MMM-yyyy");
                SimpleDateFormat dateFormat1=new SimpleDateFormat("HH:mm");
	           String datestart="";
	           if(request.getParameter("datestart")!=null){
	        	    datestart=request.getParameter("datestart"); 
	        	 }
		    String dateend="";
		    if(request.getParameter("dateend")!=null){
		    	dateend=request.getParameter("dateend") ;
		    }
		if(!datestart.equals("") && !dateend.equals("")){
			          String fromDate="";
			          String toDate="";
			          fromDate=datestart+" 00:00:00";
			          toDate=dateend+" 23:59:59";
				          GatePassService gatePassService=new GatePassService();
				          List<GatePassEntry> gatePassEntryList=gatePassService.gateUpdatedPassEntryListBasedOnDate(fromDate, toDate);
				          for(int i=0;i<gatePassEntryList.size();i++){
				        	  GatePassEntry gatePassEntry=gatePassEntryList.get(i);
				        	  Date outtime=dateFormat1.parse(gatePassEntry.getOuttime());
				        	  Date intime=dateFormat1.parse(gatePassEntry.getInTime());
				        	  Date createdDate=dateFormat.parse(gatePassEntry.getCreatedDate());
				        	  String formatedDate=dateFormat2.format(createdDate);
				        	  
				        	  long diffrennce=intime.getTime()-outtime.getTime();
				        	  long diffSeconds = diffrennce / 1000 % 60;
				  			  long diffMinutes = diffrennce / (60 * 1000) % 60;
				  			  long diffHours = diffrennce / (60 * 60 * 1000) % 24;
				         %>
            <tr>        
               <td><input type="checkbox" name="select1" id="select1<%=i%>"value="<%=gatePassEntry.getGatePassEntryId()%>"/>
               <td><%=formatedDate%></td>
               <td><%=gatePassEntry.getGatePassEntryId()%></td>
			   <td><%=gatePassEntry.getStaffName()%></td>
			   <td><%=gatePassEntry.getDepartment()%></td>
			   <td><%=gatePassEntry.getDescription()%></td>
			   <td><%=gatePassEntry.getOuttime()%></td>
			   <td><%=gatePassEntry.getInTime()%></td>
			   <td><Strong>Hours:</strong><%=diffHours%>&nbsp;<Strong>Minutes:</strong><%=diffMinutes%>&nbsp;<strong>Seconds:</strong><%=diffSeconds%></td>
               <td>
                	<select name="cl" id="cl<%=i%>" >
                           <option value="NCL">No CL</option>
                           <option value="HCL">0.5 CL</option>
                	       <option value="FCL">1 CL</option>
                	       <option value="LA">LA</option>
                    </select>
	   		  </td>
	   		  <td><%=gatePassEntry.getGatePassType()%></td>
         </tr>
         <%} 
         }%>
      </tbody>
   </table>
   </form>
	      </div>
	      </div>
	
	</section>
	
	
    
</body>
</html>