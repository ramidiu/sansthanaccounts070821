<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.accounts.saisansthan.employeeAttendce "%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LEAVE PERMISSION FORM</title>
<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<link href="css/no-more-tables.css" rel="stylesheet"/>
<jsp:useBean id="SFTQ" class="beans.stafftimings"/>
<jsp:useBean id="EMAF" class="beans.employee_attendence"/>
<jsp:useBean id="SFT" class="beans.stafftimings"/>
<jsp:useBean id="SFTT" class="beans.stafftimings"/>
<script src="js/jquery-1.8.2.js"></script>
<script>
	$(function() {
	var currdate=new Date();
	/* $( "#leave_date" ).datepicker({	
		changeMonth: true,
        changeYear: true,
		showOtherMonths: true,
        selectOtherMonths: true,
	  minDate: 0,
      dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
        buttonImageOnly: true,
    	onSelect: function( selectedDate ) {
			$( "#to_leave_date" ).datepicker( "option", "minDate", selectedDate );
		}
		});
	$( "#to_leave_date" ).datepicker({	
		changeMonth: true,
        changeYear: true,
		showOtherMonths: true,
        selectOtherMonths: true,
	  minDate: 0,
      dateFormat: 'yy-mm-dd',
		constrainInput: true,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
        buttonImageOnly: true,
    	onSelect: function( selectedDate ) {
			$( "#leave_date" ).datepicker( "option", "maxDate", selectedDate );
		}
		}); */
		$( "#leave_date" ).datepicker({	
			changeMonth: true,
	        changeYear: true,
			showOtherMonths: true,
	        selectOtherMonths: true,
	      dateFormat: 'yy-mm-dd',
			constrainInput: true,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
	        buttonImageOnly: true,
			});
		$( "#to_leave_date" ).datepicker({	
			changeMonth: true,
	        changeYear: true,
			showOtherMonths: true,
	        selectOtherMonths: true,
	      dateFormat: 'yy-mm-dd',
			constrainInput: true,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
	        buttonImageOnly: true,
			});
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	});
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	
</script>
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>

<script  type="text/javascript">
function combos(denom,desti) { 
 
		var xx;
		var yearss=document.getElementById("currentyear").value;
        var com_id = (document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value).replace(" ","@").replace("&","*"); 
		document.getElementById(desti).options.length = 0;
		var sda1 = document.getElementById(desti);
		
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xx=new XMLHttpRequest();
  
  }
else
  {// code for IE6, IE5
  xx=new ActiveXObject("Microsoft.XMLHTTP");
  }
xx.onreadystatechange=function()
  {//alert(xx.status+"::"+xx.readyState);
 if (xx.readyState==4 && xx.status==200)
 { 
		var mytool_array=xx.responseText.split("\n");
		
	
		 for(var i=0;i<mytool_array.length;i++)
		{
		if(mytool_array[i] !="")
		{
		
		var y=document.createElement('option');
		var val_array=mytool_array[i].split(":");
		
					y.text=val_array[1];
					y.value=val_array[0];
					try
					{
					sda1.add(y,null);
					}
					catch(e)
					{
					sda1.add(y);
					}
		}
		
		}
		 Reloadpage();
		 
    }
  }
  
xx.open("GET","empcategories.jsp?id="+com_id+"&yearss="+yearss,true);
xx.send();
}
function Reloadpage(){
	if($('#employee_id').val()!=""){
	window.location.href = "leavepermission-form.jsp?empid="+$('#employee_id').val();
	}
} 
</script>
<script  type="text/javascript">
function leavevalidte(){ 
if(document.getElementById("emp_cate").value==""){alert("select Employee department");document.getElementById("emp_cate").focus();return false; }
if(document.getElementById("leave_date").value==""){ alert("select from date");document.getElementById("leave_date").focus();return false;}
if(document.getElementById("to_leave_date").value==""){ alert("select to date");document.getElementById("to_leave_date").focus();return false;}
if(document.getElementById("leave_type").value==""){ alert("select Leave Type");document.getElementById("leave_type").focus();return false;}
if(document.getElementById("leave_range").value==""){ alert("select Leave Range");document.getElementById("leave_range").focus();return false;}}</script>
</head>
<body>
</tr><jsp:useBean id="SFTw" class="beans.stafftimings"/>
<jsp:useBean id="LPW" class="beans.leavepermission"/>
<%
	if(session.getAttribute("admin_id")!=null){
%>
<div><div ><jsp:include page="my-account-header.jsp"></jsp:include></div>
</div>
<div style="clear:both"></div>  
 <div class="container">
<div class="row">
	<div class="span_12_of_12 main_body " >
      <div class="col span_10_of_12 middle_body"  style="padding:0% 1.5% 1.5% 1.5% ;">
      <div class="table_heading span_12_of_12"><span class="page_head">WELCOME TO GENERAL SECRETARY</span> </div>
 		<div class="span_12_of_12 float_left leave_form1">
        <form name="leavepermission_Update" method="post" action="leavepermission_Insert.jsp" onsubmit="return leavevalidte();">
        <section id="no-more-tables">
         <div class="table_heading span_12_of_12 "><span style="margin:15px auto;width:auto; text-align:center; float:left; color:#ff5500;">Leave Permission Form</span> 
          <span style="float:right;"><a href="" class="my_acc_button">BACK</a></span>
         </div>  
         <div class="span_12_of_12 float_left leave_form_margin" >
            <div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>
            <%List STAFFALL=null;
            	mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
            
            	SimpleDateFormat fornmat =new SimpleDateFormat("yyyy");
            	Calendar calld = Calendar.getInstance();
            	String currentyears="2013";
            	List SFT_YaerList=SFT_CL.getStaffcategoryList(currentyears);
            	if(request.getParameter("empid")!=null){ 
            		List STAFFL=SFT_CL.getMstafftimings(request.getParameter("empid").toString());
            		if(STAFFL.size()>0){
            			SFT=(beans.stafftimings)STAFFL.get(0);
            			            		}
            		STAFFALL=SFT_CL.getALLByStaffCategory(SFT.getstaff_type());
            	
            	}
            %>
			<input type="hidden" name="currentyear" id="currentyear" value="<%=currentyears%>" />
			<input type="hidden" name="type" id="type" value="sansthan" />
            <div class="col span_3_of_12 leave_feilds ">  Employee Department 	</div>
            <div class=" col span_6_of_12">
                <select name="emp_cate" id="emp_cate"  onChange=combos('emp_cate','employee_id');>
					<option value="">select</option>
					<%
						if(SFT_YaerList.size()!=0){for(int aa=0; aa < SFT_YaerList.size(); aa++ ){
									SFTw=(beans.stafftimings)SFT_YaerList.get(aa);
					%> 
  					 <option value="<%=SFTw.getstaff()%>"> <%=SFTw.getstaff()%></option>
					<%
						}}
					if(request.getParameter("empid")!=null){ 
					%>
					<option value="<%=SFT.getstaff_type()%>" selected="selected"> <%=SFT.getstaff_type()%></option>
					<%} %>
				</select>
             </div>
         </div>
  <div class="span_12_of_12 float_left leave_form_margin ">
 <div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>               
<div class="col span_3_of_12 leave_feilds">Employee Name </div>	<div class=" col span_6_of_12">
		<select name="employee_id" id="employee_id" onchange="Reloadpage()">   <option value="">--select--</option>
		<%
			if(request.getParameter("empid")!=null){
				if(STAFFALL.size()>0){
					for(int j=0;j<STAFFALL.size();j++){
        			SFTT=(beans.stafftimings)STAFFALL.get(j);
        			            		
					%>
					<option value="<%=SFTT.getstaff_id()%>" <%if(SFT.getstaff_id().equals(SFTT.getstaff_id())){%>selected="selected" <%} %>> <%=SFTT.getstaff_name()%></option>
					<%}}} %>
		</select>
</div></div>
<%if(request.getParameter("empid")!=null){ %>
  <div class="span_12_of_12 float_left leave_form_margin ">
 <div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>               


<div class="span_12_of_12"><table width="100%" border="1" cellspacing="0"  bgcolor="#eeeeee" cellpadding="0" style="margin:0 !important;padding:0px;width:100%;" id="tblExport">
<tr>
<th colspan="12">Employee CL Details</th></tr>
<tr>
<th align="center" bgcolor="#fffef3" class="attendance_text">Opening Balance</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">CL's Earned</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">Total Available</th>
<td style="position:relative;padding:0px;background-color:#fffef3;">
<table border="0" cellspacing="0" bgcolor="#eeeeee" cellpadding="0" class="util_leaves" style=" width:100%; margin-bottom:0px !important;padding:0 !important;">
<tr><td colspan="4" align="center" bgcolor="#fffef3" style="background-color:#fffef3;border-bottom:1px #CCC solid;" class="attendance_text">Utilisation Of Leaves</td></tr>
<tr>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">LA</th>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">LP</th>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">CL</th>
<th style="width:65px;" bgcolor="#fffef3" class="attendance_text">Absent</th>
</tr>
</table>
</td>
<td style="padding:0 !important;margin:0 !important;background-color:#fffef3;">
<table width="100%" border="0" cellspacing="0" bgcolor="#eeeeee" cellpadding="0" style=" margin-bottom:0px !important; width:100%;">
<tr><td colspan="5" align="center" bgcolor="#fffef3" style="border-bottom:1px #CCC solid;background-color:#fffef3;" class="attendance_text">Total Utilisation (in CL)</td></tr>
<tr>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">LA</th>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">LP</th>
<th style="width:50px;" bgcolor="#fffef3" class="attendance_text">CL</th>
<th style="width:65px;" bgcolor="#fffef3" class="attendance_text">Absent</th>
<th style="width:65px;" bgcolor="#fffef3" class="attendance_text">Total</th>
</tr>
</table>
</td>
<th align="center" bgcolor="#fffef3" class="attendance_text">TOTAL CL's</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">OD</th>
<th align="center" bgcolor="#fffef3" class="attendance_text">OT (hours)</th>
</tr>
<tr>
	<%
		int count=0;
   		DecimalFormat df = new DecimalFormat("##.##");
   		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		String date1=dateFormat.format(date);
		employeeAttendce e=new employeeAttendce();
		
		String data[]=e.calculateClOTs(request.getParameter("empid"), date1);

	%>
	<td align="center" bgcolor="#FFF" class="attendance_text"><%=data[0]%></td>
	<td align="center" bgcolor="#FFF" class="attendance_text"><%=data[1]%></td>
	<td align="center" bgcolor="#FFF" class="attendance_text"><%=data[2]%></td>
	<td style="padding:0px !important;" >
    <table  border="0" cellspacing="0" bgcolor="#eeeeee" cellpadding="0" style="width:100%;height:100%;margin:0px !important;vertical-align:top;" class="util_values">
   	 	<tr>
    		<td style="width:50px; vertical-align:top;" bgcolor="#FFF" class="attendance_text1" ><%=data[3]%></td>
    		<td style="width:50px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=data[4]%></td>
    		<td style="width:50px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=data[5]%></td>
    		<td style="width:65px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=data[6]%></td>
    	</tr>
    </table>
</td>
<td>
<table  border="0" cellspacing="0" bgcolor="#eeeeee" cellpadding="0" style="width:100%;height:100%;margin:0px !important;vertical-align:top;" class="util_values">
    <tr>
    <td style="width:50px; vertical-align:top;" bgcolor="#FFF" class="attendance_text1"><%=data[7]%></td>
    <td style="width:50px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=data[8]%></td>
    <td style="width:50px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=data[9]%></td>
    <td style="width:65px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=data[10]%></td>
    <td style="width:65px;border-left:1px #CCC solid;" bgcolor="#FFF" class="attendance_text1"><%=data[11]%></td>
    </tr>
    </table>
</td>

<td bgcolor="#FFF" class="attendance_text"><%=data[12] %></td>
<td bgcolor="#FFF" class="attendance_text">

<input type="hidden" name="OP_BAL"  id="OP_BAL<%=count%>" value="<%=data[13]%>" /><%=data[13]%>

</td>
<td bgcolor="#FFF" class="attendance_text"><%=data[14]%></td>
</tr>
	
	</table></div>
</div>
<%} %>

 <div class="span_12_of_12 float_left leave_form_margin">
  <div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>
<div class="col span_3_of_12 leave_feilds">From Leave Date </div>	
<div class=" col span_6_of_12"><input type="text" name="leave_date" id="leave_date" value="" class="DatePicker" readonly="readonly"/></div>
</div>
 <div class="span_12_of_12 float_left leave_form_margin">
  <div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>
  <div class="col span_3_of_12 leave_feilds">To Leave Date </div>	
<div class=" col span_6_of_12"><input type="text" name="to_leave_date" id="to_leave_date" value="" class="DatePicker" readonly="readonly" required/></div>
</div>

 <div class="span_12_of_12 float_left leave_form_margin" style="border:1px #FFF solid;">
 <div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>
<div class="col span_3_of_12 leave_feilds">Leave Type 	</div><div class=" col span_6_of_12">
	<select name="leave_type" id="leave_type" > 
		<option value="">-type-</option>
  		 <option value="CL"> CL</option>
	</select>
	<select name="leave_range" id="leave_range" > 
		<option value="">-range-</option>
  		 <option value="1">1</option>
   		 <option value="0.5">0.5</option>
	</select>
</div>
</div>
 <div class="span_12_of_12 float_left leave_form_margin" style="border:1px #FFF solid;">
  <div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>
<div class="col span_3_of_12" style="border:1px #FFF solid;" > </div>	
<div class=" col span_6_of_12" style="border:1px #FFF solid;">
<input type="submit" value="CREATE" class="my_acc_button" style="margin-top:10px;"></input>
</div></div></section></form>
</div>
<form method="post" action="leavepermission-form.jsp">
<div class="span_12_of_12 leave_form2 ">
 	<div class="span_12_of_12 float_left leave_form_margin">	
  	<div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>
	<div class="col span_3_of_12 leave_feilds">Leave Date</div><div class=" col span_6_of_12" style="cursor: pointer;">
	<input type="text" name="date_l" id="date" value="" readonly required/></div>
	</div>
	 <div class="span_12_of_12 float_left">
 	 <div class="col span_3_of_12 " style="border:1px #FFF solid;"></div>
	<div class="col span_3_of_12 leave_feilds"></div><div class=" col span_6_of_12"><input type="submit" name="submit" value="Search" class="my_acc_button"></input></div> </div>                   
</div>  
</form>   
<table>
	<%
		if(request.getParameter("date_l")!=null){
	%>
<tr>
<td>Leave Date</td>
<td>Leave Type</td>
<td>Employee Id</td>
<td>Employee Name</td>
<td>Leave status</td>

<td>Edit</td>

<!--<td>Delete</td> -->
</tr>
<%
	SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");
	SimpleDateFormat dateformat1 = new SimpleDateFormat("yyyy-MM-dd");
mainClasses.leavepermissionListing LPR_CL = new mainClasses.leavepermissionListing();
mainClasses.employee_attendenceListing EMP_CL = new mainClasses.employee_attendenceListing();
List LPR_List=LPR_CL.getLeavePermission(request.getParameter("date_l"));
for(int i=0; i < LPR_List.size(); i++ ){
LPW=(beans.leavepermission)LPR_List.get(i);
%><tr>

<td><%=dateformat.format(dateformat1.parse(LPW.getleave_date().toString()))%></td>
<td><%=LPW.getextra1()%> <%=LPW.getleave_type()%></td>
<td><%=SFT_CL.getstafName(LPW.getemployee_id().toString())%></td>
<td><%=LPW.getemployee_id()%></td>
<!--<td><%//=dateformat.format(dateformat1.parse(LPW.getleave_askeddate().toString()))%></td>-->
<td><%=LPW.getleave_status()%></td>
<%List LPR_PERMIList=EMP_CL.getStaffpermissionAttendence(LPW.getleave_date().toString());if(LPR_PERMIList.size()!=0){ %><td></td><%}else{%>

<td><a href="leavepermission_Edit.jsp?leavper_id=<%=LPW.getleavper_id()%>" class="submit_button" style="padding:1px 4px">Edit</a></td><%}%>
<!--<td><a href="leavepermission_Delete.jsp?leavper_id=<%//=LPW.getleavper_id()%>">Delete</a></td> -->
</tr><%}%>
<%}%>
</table>                 
   </div>
     </div>
	</div>
</div>
<div style="clear:both"></div>  
 <%}else{ response.sendRedirect("index.jsp");}%>
</body>
</html>