<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="LPR" class="beans.leavepermissionService" />
<%
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
String leave_date=request.getParameter("leave_date");
String to_leave_date=request.getParameter("to_leave_date");
String leave_type=request.getParameter("leave_type");
String employee_id=request.getParameter("employee_id");
String leave_status="yes";//request.getParameter("leave_status");
String extra1=request.getParameter("leave_range");
String extra2="";//request.getParameter("extra2");
Calendar cal=Calendar.getInstance();
Date fromdate = sdf.parse(leave_date);
Date todate = sdf.parse(to_leave_date);
while(fromdate.compareTo(todate)<=0){

	%>
<jsp:setProperty name="LPR" property="leave_date" value="<%=leave_date%>"/>
<jsp:setProperty name="LPR" property="leave_type" value="<%=leave_type%>"/>
<jsp:setProperty name="LPR" property="leave_status" value="<%=leave_status%>"/>
<jsp:setProperty name="LPR" property="employee_id" value="<%=employee_id%>"/>
<jsp:setProperty name="LPR" property="extra1" value="<%=extra1%>"/>
<jsp:setProperty name="LPR" property="extra2" value="<%=extra2%>"/>
<%=LPR.insert()%><%=LPR.geterror()%>
<%
cal.setTime(fromdate);
cal.add(Calendar.DATE, 1);
fromdate=sdf.parse(sdf.format(cal.getTime()));
leave_date=sdf.format(cal.getTime());
}
if(request.getParameter("type")!=null && request.getParameter("type").equals("sansthan")){
  response.sendRedirect("leavepermission-form.jsp?date_l="+leave_date);  
}else{
	  response.sendRedirect("sainivas_leavePermission.jsp?date_l="+leave_date);  
}

%>