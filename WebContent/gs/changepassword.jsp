<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MY ACCOUNT</title>
<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<script type="text/javascript">
function validate(){
	if(document.getElementById("new_password").value!=document.getElementById("conf_new_password").value){
		alert("Please Enter same password in confirm password");
		document.getElementById("conf_new_password").focus();
		return false;
	}
}
</script>
</head>
<body>
<div><div ><jsp:include page="my-account-header.jsp"></jsp:include></div>
</div>
<div style="clear:both"></div>  
  <div class="container">
<div class="row">
	<div class="span_12_of_12 main_body " >
      <div class="col span_10_of_12 middle_body"  style="padding:0% 1.5% 1.5% 1.5% ;">
      <div class="table_heading span_12_of_12"><span class="page_head">WELCOME TO GENERAL SECRETARY</span> </div>
      <%if(request.getParameter("q")!=null && request.getParameter("q").equals("sucess")){%>
      <div > Password updated successfully</div> <%} else if(request.getParameter("q")!=null && request.getParameter("q").equals("failure")){ %>
      <div > Password updated  failed</div>
      <%} %>
      <form action="passwordupdate.jsp" method="get" onsubmit="return validate();">
      <table>
<tr>
<td>Current Password</td>
<td><input type="password" name="cur_password" id="cur_password" value="" placeholder="Current Password" required/> </td>
</tr>
<tr>
<td>New Password</td>
<td><input type="password" name="new_password" id="new_password" value="" placeholder="New Password" required/> </td>
</tr>
<tr>
<td>Confirm New  Password</td>
<td><input type="password" name="conf_new_password" id="conf_new_password" value="" placeholder=" Confirm New Password" required/> </td>
</tr>
<tr><td colspan="2"><input type="submit" value="UPDATE" class="my_acc_button" /> </td></tr>
</table></form>
 		</div>
      </div>
	</div>
</div>
<div style="clear:both"></div>  
</body>
</html>