<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.accounts.saisansthan.employeeAttendce"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EMPLOYEE ATTENDANCE</title>
<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<jsp:useBean id="EMAF" class="beans.employee_attendence"/>
<jsp:useBean id="SHT" class="beans.shift_timings"/>
<link href="css/no-more-tables.css" rel="stylesheet"/>
<script src="js/jquery-1.8.2.js"></script>
<script>
	/*  $(function() {
	var currdate=new Date();
	$( "#datestart" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	});
	$( "#dateend" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	});
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	  */
	 
	 
	 $(function() {
			var currdate=new Date();
			$( "#datestart" ).datepicker({
				changeMonth: true,
				changeYear: true,
				numberOfMonths: 1,
				dateFormat: 'yy-mm-dd',
				onSelect: function( selectedDate ) 
				{
					$("#dateend").val(selectedDate);
				}
			});
		});	 
	
</script>
<script>
function dutychange(id){
	$('#dutyshiftcheckbox'+id).prop('checked', true);
	
}
function validatesubmit(){
	total=$('.dutyshiftclass:checked').size();
if(Number(total)>0){
	document.getElementById("emp-attendence-list").action="updateattendance.jsp";
	document.getElementById("emp-attendence-list").submit();
}
else {
	alert("please change duty time atleast one employee");
	return false;
}
	}
</script>
<script>function validate(s){

if(document.getElementById("halfday"+s).checked==true){
//alert("s is halfday"+document.getElementById("halfday"+s).value);
document.getElementById("latehour"+s).value=0.5;
//alert(document.getElementById("latehour"+s).value);
document.getElementById("latetimetype"+s).value="CL";}
else if(document.getElementById("normal"+s).checked==true){
//alert("s is normal");
document.getElementById("latehour"+s).value="";
document.getElementById("latetimetype"+s).value="";}
else if(document.getElementById("hours"+s).checked==true){
//alert("s is  hours");
document.getElementById("latehour"+s).value=1;
document.getElementById("latetimetype"+s).value="hour";}
else if(document.getElementById("casuvals"+s).checked==true){
//alert("s is  casuvals");
document.getElementById("latehour"+s).value=1;
document.getElementById("latetimetype"+s).value="CL";}
else{
document.getElementById("latehour"+s).value=""; document.getElementById("latetimetype"+s).value=document.getElementById("leavetypess"+s).value;}
//if((document.getElementById("normal"+s).checked==true  || document.getElementById("hours"+s).checked==true || document.getElementById("halfday"+s).checked==true ){
 //}

if((document.getElementById("normal"+s).checked==false)&& (document.getElementById("hours"+s).checked==true || document.getElementById("casuvals"+s).checked==true || document.getElementById("halfday"+s).checked==true)){
//alert("entere");
if(document.getElementById("permisssion"+s).value==""){
alert("seelect Permission ");
document.getElementById("permisssion"+s).focus();
 return false;}}}</script>
<script type="text/javascript">
function datescheck(){
var DaysDiff;

//Date1 = new Date(document.getElementById("dateend").value);
//Date2 = new Date( document.getElementById("cursdate").value);
var myDate = document.getElementById("dateend").value;
	var myDate1 = document.getElementById("cursdate").value;
var datestart = document.getElementById("datestart").value;
if (myDate1<=datestart) {
		 alert("End Date must be lessthan the  StartDate");
          document.getElementById("datestart").focus();
return false;
}
	 // alert(document.getElementById("dateend").value);
	    if (myDate1<=myDate) {
		 alert("End Date must be lessthan the  CurrentDate");
          document.getElementById("dateend").focus();

return false;
}
	    var cat = document.getElementById("categorytype").value;
	    if(cat == null || cat == "")
		{
			alert("Please Select Category");
			return false;
		}
}</script>
<script language = "JavaScript">

//---For check all checkboxes----

function checkAll() {
var select_All = document.getElementById("SelectAll");

var inputs = document.getElementsByTagName("input");
for (var i = 0; i < inputs.length; i++)
{
if (inputs[i].type == "checkbox" && inputs[i].name == "select1") {
if(select_All.checked==true)
inputs[i].checked = true;
else
inputs[i].checked = false;
}
}
}
//----for check atleast one check box-----
function check1(){
	
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one for appprove");
	return false;} else{
		var ty = document.getElementById("ty").value;
		var datee = document.getElementById("datee").value;
	document.getElementById("emp-attendence-list").action="employee_attendence_Insert.jsp?ty="+ty+"&datee="+datee;
	document.getElementById("emp-attendence-list").submit();
	}
}

function changeInTime(){
	
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one to change staff in time");
	return false;} else{
		var ty = document.getElementById("ty").value;
		var cursdate = document.getElementById("datestart").value;
	document.getElementById("emp-attendence-list").action="change_staff_intime.jsp?ty="+ty+"&cursdate="+cursdate;
	document.getElementById("emp-attendence-list").submit();
	}
}

function changeOutTime(){
	
	var inputs = document.getElementsByTagName("input");
	var c=0;
	for (var i = 0; i < inputs.length; i++)
	{
	if (inputs[i].type == "checkbox" && inputs[i].name == "select1"){
		if(inputs[i].checked == true)
			c++;
			}}
	if(c<1){
	alert("please check atleast one to change staff out time");
	return false;} else{
		var ty = document.getElementById("ty").value;
		var cursdate = document.getElementById("datestart").value;
	document.getElementById("emp-attendence-list").action="change_staff_outtime.jsp?ty="+ty+"&cursdate="+cursdate;
	document.getElementById("emp-attendence-list").submit();
	}
}

</script>
 <script type="text/javascript">
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<%if(session.getAttribute("MemberId")!=null || session.getAttribute("admin_id")!=null){%>
<jsp:useBean id="SFTT" class="beans.stafftimings"/>
<jsp:useBean id="SFTw" class="beans.stafftimings"/>
<jsp:useBean id="SFTQ" class="beans.stafftimings"/>
<jsp:useBean id="EIO" class="beans.empinout"/>
<jsp:useBean id="EMAA" class="beans.employee_attendence"/>
<div ><jsp:include page="my-account-header.jsp"></jsp:include></div>

<div style="clear:both"></div> 
<br> <br> <br>
<div class="container">
<div class="row">
<div class="span_12_of_12 main_body " >
     <div class="col span_10_of_12 middle_body"  style="padding:0% 1.5% 1.5% 1.5% ;">
      <div class="table_heading span_12_of_12"><span class="page_head">WELCOME TO GENERAL SECRETARY</span> </div>
 	<div class="span_12_of_12 float_left">
      <section id="no-more-tables">
      <div class="table_heading span_12_of_12 float_left " style="">
      <span style="width:auto; font-size:14px;font-weight:bold;  text-align:center; float:left; color:#ff5500;"><% if(request.getParameter("categorytype") != null && request.getParameter("categorytype").equals("PRIVATE SECURITY")){%>SANSTHAN OUTSOURCE EMPLOYEES<%}else{ %>EMPLOYEE  ATTENDANCE<%} %></span> 
       <div style="float:right;width:auto;height:auto;">
       	<%
       		if(request.getParameter("datestart")!=null && request.getParameter("dateend")!=null){
       	%><a href="attendance-listing.jsp" class="my_acc_button">Back</a><%
       		}else{
       	%><a href="my-home.jsp" class="my_acc_button">Back</a><%
       		}
       	%>
       </div>
     </div>  
    <form name="yearform" id="yearform" action="attendance-listing.jsp" method="post">
    <input type="hidden" name="page" id="page" value="attendance-listing" />
    <%
    	String cursdate="";
      		SimpleDateFormat todatforms = new SimpleDateFormat("yyyy-MM-dd");
      		Calendar todaycal = Calendar.getInstance();
    		cursdate=todatforms.format(todaycal.getTime());
    %>
		<input type="hidden" name="cursdate" id="cursdate"  value="<%=cursdate%>" />
		<input type="hidden" name="ty" id="ty"  value="<%=request.getParameter("ty")%>" />
    <div class="span_12_of_12 " style="float:left;padding:5px 0px 5px 0px;"> <div class="col span_3_of_12">
    <div class="my_acc_text_feild"> Start Date : </div> <div class="my_acc_input"><input type="text" name="datestart" id="datestart" class="DatePicker" readonly="readonly" required/></div>
     <div class="cal_icon"> <img src="images/calendar-icon2.png" /></div>
      </div>
      <div class="col span_3_of_12"><div class="my_acc_text_feild">End  Date : </div>
      <div class="my_acc_input"> <input type="text" name="dateend" id="dateend" class="DatePicker" readonly="readonly" required/></div><div class="cal_icon"> <img src="images/calendar-icon2.png" /></div></div>
     <div class="col span_5_of_12">
       <div class="my_acc_text_feild">Employee Category_type :</div>
          <div class="my_acc_input">
          		<select name="categorytype" id="categorytype" style="width:180px;">
          				<%-- <% if(request.getParameter("categorytype")==null){%>
          				<option value="">SELECT CATEGORY</option><%} %> --%>
          				<!-- <option value="WORKING EMPLOYEES">SANSTHAN EMPLOYEES</option> 
          				<option value="PRIVATE SECURITY">CONTRACT EMPLOYEES</option> -->
						<!--  <option value="">ALL</option>   -->
					
 						<%
 							if(request.getParameter("ty")!=null&&request.getParameter("ty").equals("sansthan") ){
 						%>
 						<option value="WORKING EMPLOYEES">SANSTHAN EMPLOYEES</option> 
          				<option value="PRIVATE SECURITY">CONTRACT EMPLOYEES</option>
 						<!--  <option value="SANSTHAN(SH-1206)">SANSTHAN(SH-1206)</option>
 						<option value="HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)">HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)</option>
 						<option value="GENERAL ADMINISTRATION DEPARTMENT(SH-1382)">GENERAL ADMINISTRATION DEPARTMENT(SH-1382)</option>
    					<option value="CHAPPAL STAND(SH-2784)">CHAPPAL STAND (SH-2784)</option> 
      					<option value="ACCOUNTS DEPARTMENT(SH-1382)">ACCOUNTS DEPARTMENT (SH-1382)</option>
     					<option value="SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)">SAI PRASAD - STORES, KITCHEN & PRASADAM SALES(SH-1515)</option>
 						<option value="Laddu Counter(SH-1515)">Laddu Counter(SH-1515)</option>
 						<option value="Pooja Stores(SH-1009)">Pooja Stores(SH-1009)</option>
 						<option value="MEDICAL CENTRE(SH-1576)">MEDICAL CENTRE(SH-1576)</option>
      					<option value="PUBLIC RELATION OFFICER(SH-1382)">PUBLIC RELATION OFFICER(SH-1382)</option>
						<option value="LIBRARY(1595)">LIBRARY(1595)</option> --> <%
							}
						 						else if(request.getParameter("ty")!=null&&request.getParameter("ty").equals("sainivas") ){
						%>
 							 <option value="SHRI SAINIVAS MEGHA DHARMASALA">SHRI SAINIVAS MEGHA DHARMASALA</option>
							 <option value="RESTAURANT">RESTAURANT</option>
 							<option value="HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)">HOUSE KEEPING, GARDEN AND WATER MANAGEMENT(SH-1515)</option>
      						<option value="PUBLIC RELATION OFFICER(SH-1382)">PUBLIC RELATION OFFICER(SH-1382)</option>
	 						 <option value="SECURITY">SECURITY</option>	
 						<%
	 							}
	 						%>
 				</select>
          </div>
      </div>
      <div class="col span_1_of_12"><input type="submit" name="submit" value="SEARCH"  onClick="return datescheck();"/></div>
     </div> </form>     
     <input type="hidden" name="datee" id="datee" value="<%=request.getParameter("datestart")%>" />
       <form name="emp-attendence-list" id="emp-attendence-list" method="post" >             
     <div class="span_12_of_12 float_left" style="padding:5px 0px 5px 0px ;  height:auto;  " >
          <input type="submit" name="button" id="button" class="my_acc_button" value="APPROVE" onclick="return check1();"></input>
                    <input type="submit" name="button" id="button" class="my_acc_button" value="Change DutyTime" onclick="return validatesubmit();"></input>
                    
          <a href="#" id="btnExport" class="my_acc_button" style="margin-left:5px;">Excel Sheet</a>
     </div>               
      <div class="span_12_of_12 float_left" style="">        
               
    <table class="table-bordered table-striped table-condensed" style="width:100%; margin-top:15px;" id="tblExport">
          <thead>
         <tr>   
         	<th ><input type="checkbox" name="SelectAll" id="SelectAll" value="yes" onClick="checkAll();"/> </th>
         	<th>Emp No</th>
         	<th >Id </th>
         	<th>Name </th>
         	<th> Designation</th>
        	 <th> Duty_Time</th>
        	 <th> Original In_Time</th>
        	 <th> Original Out_Time</th>
        	 <th> Updated In_Time</th>
        	 <th> Updated Out_Time</th>
        	 	 <th> Duration Of Hours</th>
        	 	  	 <th>  Total CLs</th>
        	 <th> Late Permission</th>
        	  <th> Leave type</th>
        	 <th>  OT</th>
             <th> 	Submit </th>
             <!-- <th>Photo</th> -->
          </tr>
          </thead>
       <tbody>
     		<%
     			SimpleDateFormat indiaformat = new SimpleDateFormat("dd-MMM-yyyy");
     		           mainClasses.employee_attendenceListing EMA_CL = new mainClasses.employee_attendenceListing();
     		           mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
     		           mainClasses.empinoutListing EIO_CL = new mainClasses.empinoutListing();
     		            mainClasses.shift_timingsListing SHT_CL = new mainClasses.shift_timingsListing();
     		           SimpleDateFormat fornmat =new SimpleDateFormat("yyyy");
     		           SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
     		          SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
     		           String datestart="";if(request.getParameter("datestart")!=null){datestart=request.getParameter("datestart"); }
     			String dateend="";if(request.getParameter("dateend")!=null){dateend=request.getParameter("dateend") ;}
     			if(!datestart.equals("") && !dateend.equals("")){
     			Calendar fromdate = Calendar.getInstance();
     			Calendar todate = Calendar.getInstance();
     			 Date date1 = dateFormat.parse(datestart);
     		 			 Date date2 = dateFormat.parse(dateend);
     			fromdate.setTime (date1);todate.setTime (date2);
     			 int timegap=(int)((todate.getTimeInMillis()-fromdate.getTimeInMillis())/(24 * 60 * 60 * 1000));       
     			List SHT_List=SHT_CL.getshift_timings();
     			long totallate=0;
     		long OT=0;long OT1=0;
     		boolean x=false;
     		String xxin="",xxout="";
     		String emplyCheckIn="";
     		String emplyCheckOut="";
     		long diffMinutes = 0;
			long diffHours = 0;
     		Date dateCheckIn = null;
    		Date dateCheckOut = null;
    		long difftime=0;
     		String categorytype="";
     		if(request.getParameter("categorytype")!=null){ categorytype=request.getParameter("categorytype");}
     		Calendar cal = Calendar.getInstance();
     		String currentyears="2013";
     		// currentyears=fornmat.format(cal.getTime());
     		String datestartDummy[]=datestart.split("-");
     		if(datestartDummy.length>0){currentyears=datestartDummy[0];}
     		currentyears="2013";
     		if(request.getParameter("ty")!=null && request.getParameter("ty").equals("sansthan")){
     			currentyears="2013";
     		} else if(request.getParameter("ty")!=null && request.getParameter("ty").equals("sainivas")){
     			currentyears="2014";
     		}
     		//List SFT_YaerList=SFT_CL.getStaffYearList(currentyears);
     		List SFT_YaerList=SFT_CL.getStaffList(currentyears);
     		//SFT_CL.getALLStaffForAttendence(currentyears);
     		if(SFT_YaerList.size()!=0){
     			for(int aa=0; aa < SFT_YaerList.size(); aa++ ){
     			SFTw=(beans.stafftimings)SFT_YaerList.get(aa);
     			int count=0;
     			//from dateloop
     			for(int p=0;p<=timegap;p++){ 
     			String yestdays=dateFormat.format(fromdate.getTime());
     			List SFT_YaerListE=null;
     			if(request.getParameter("categorytype")!=null && ! request.getParameter("categorytype").toString().equals("") && (request.getParameter("categorytype")).equals("WORKING EMPLOYEES"))
     			{
     				SFT_YaerListE=SFT_CL.getEmpCategoryList2(SFTw.getstaff().toString(),yestdays);
     			}
     			else if(request.getParameter("categorytype")!=null && ! request.getParameter("categorytype").toString().equals(""))
     			{
     				SFT_YaerListE=SFT_CL.getEmpCategoryList3(SFTw.getstaff().toString(),yestdays);
     			}				     		 	
     			/* else{ SFT_YaerListE=SFT_CL.getEmpCategoryList(SFTw.getstaff().toString(),yestdays);} */ 
     			
     			/* if(request.getParameter("categorytype")!=null && ! request.getParameter("categorytype").toString().equals("")){
     				//System.out.println("Category-"+request.getParameter("categorytype"));
     		 	SFT_YaerListE=SFT_CL.getEmpCategoryList(request.getParameter("categorytype").toString(),SFTw.getstaff().toString(),yestdays);}
     			else{ SFT_YaerListE=SFT_CL.getEmpCategoryList(SFTw.getstaff().toString(),yestdays);}  */
     			if(SFT_YaerListE.size()!=0){
     		%>
          <tr><td data-title="Id" colspan="16" align="left" style="font-weight: bold;"> <img src="images/calendar-icon2.png"/> <%=indiaformat.format(fromdate.getTime())%></td></tr> 
           <%
            	for(int N=0; N < SFT_YaerListE.size(); N++ ){
                    	  SFTQ=(beans.stafftimings)SFT_YaerListE.get(N);
            %>
           <tr><td data-title="Id" colspan="16" align="left" style="color: orange;font-weight: bold;"> <%=SFTQ.getstaff()%></td></tr> 
           <%
            	List SFT_AllList=SFT_CL.getALLByStaffcategory(SFTQ.getstaff().toString(),SFTw.getstaff().toString());
                         if(SFT_AllList.size()!=0){
                        	 for(int Y=0; Y < SFT_AllList.size(); Y++ ){
            		SFTT=(beans.stafftimings)SFT_AllList.get(Y);
            		if(count==0)count=(Y+1);
            			else count=count+1;  //no need  because only employee table list notfrom empinout 
            	List empinout=EMA_CL.getInOut(SFTT.getstaff_id().toString(),yestdays);
            	if(empinout.size()!=0){
            %>
          <input type="hidden" name="staff_id"  id="staff_id" value="<%=SFTT.getstaff_id()%>"/>
				<input type="hidden" name="datesss" id="datesss<%=count%>" value="<%=yestdays%>"/>
				<input type="hidden" name="datestart" id="datestart" value="<%=datestart%>"/>
				<input type="hidden" name="categorytype" id="categorytype" value="<%=categorytype%>"/>
				<input type="hidden" name="dateend" id="dateend" value="<%=dateend%>"/>
           <tr>   
               <%
                  	for(int i=0; i < 1; i++ ){
                                     EMAA=(beans.employee_attendence)empinout.get(i); 
                            		   if(count==0)count=(i+1);
                            			else count=count+1;
                  %>
                <td data-title="Select" align="center"><input type="checkbox" name="select1" id="select1<%=count%>"value="<%=SFTT.getstaff_id()%>"/><input type="hidden" name="selecttemp"id="selecttemp<%=count%>"value="<%=SFTT.getstaff_id()%>"/></td>
                <td data-title="EmpNo" align="center"><%=SFTT.getStaff_nid()%></td>
                <td data-title="Id" align="center"><%=SFTT.getstaff_id()%></td>
                <td data-title="Name" align="center"><%=SFTT.getstaff_name()%></td>
                <td data-title="Designation " align="center"><%=SFTT.getdesignation()%></td>
                <td data-title="In_Time" align="center">
                <select name="dutyshift<%=SFTT.getstaff_id() %>" id="dutyshift<%=SFTT.getstaff_id() %>" onchange="dutychange('<%=SFTT.getstaff_id()%>')">
                <option value=""><%=EMAA.getextra6().toString()%>
                </option>
                <%for(int k=0; k < SHT_List.size(); k++ ){
                	SHT=(beans.shift_timings)SHT_List.get(k); %>
                	  <option value="<%=SHT.getshift_starttitming().trim()%>To<%=SHT.getshift_endtiming().trim()%>"><%=SHT.getshift_starttitming()%> to <%=SHT.getshift_endtiming() %></option>
                	<%} %>
                </select>
                <span style="display: none;">
                 <input type="checkbox" name="dutyshiftcheckbox" id="dutyshiftcheckbox<%=SFTT.getstaff_id() %>" class="dutyshiftclass" value="<%=SFTT.getstaff_id() %>"/>
                 <input type="hidden" name="emp_atid<%=SFTT.getstaff_id() %>" id="emp_atid<%=SFTT.getstaff_id() %>"  value="<%=EMAA.getemp_atid()%>"/>
                </span>
                </td>             
                <%-- <td data-title="Out_Time" align="center"><%=EMAA.getstaff_intime()%></td>
                <td data-title="Late Permission" align="center"><%=EMAA.getstaff_outtime()%></td> --%>
                 <td data-title="Out_Time" align="center"><input type="text" style="width:40px" name="In_Time<%=SFTT.getstaff_id() %>" id="In_Time<%=SFTT.getstaff_id() %>"  value="<%=EMAA.getextra8()%>" readonly="readonly"/></td>
                <td data-title="Late Permission" align="center"><input type="text" style="width:40px" name="Out_Time<%=SFTT.getstaff_id() %>" id="Out_Time<%=SFTT.getstaff_id() %>"  value="<%=EMAA.getextra9()%>" readonly="readonly"/></td>
                <td data-title="Out_Time" align="center"><input type="text" style="width:40px" name="In_Time<%=SFTT.getstaff_id() %>" id="In_Time<%=SFTT.getstaff_id() %>"  value="<%=EMAA.getstaff_intime()%>"/></td>
                <td data-title="Late Permission" align="center"><input type="text" style="width:40px" name="Out_Time<%=SFTT.getstaff_id() %>" id="Out_Time<%=SFTT.getstaff_id() %>"  value="<%=EMAA.getstaff_outtime()%>"/></td>
                	<%
                	String stftm[]=(EMAA.getextra6().toString()).split("To");
					if( !stftm[0].equals("") && !EMAA.getstaff_intime().toString().equals("") && !stftm[1].equals("") && !EMAA.getstaff_outtime().toString().equals("")){ 
					String staffstart[]=stftm[0].split(":");
					String staffenter[]=EMAA.getstaff_intime().toString().split(":");
					 String staffend[]=stftm[1].split(":");
						String staffclose[]=EMAA.getstaff_outtime().toString().split(":");
						if(EMAA.getstaff_intime()!=null && !EMAA.getstaff_intime().equals("") && EMAA.getstaff_outtime()!=null && !EMAA.getstaff_outtime().equals("") ){
						emplyCheckIn= EMAA.getdate()+" "+EMAA.getstaff_intime();
						emplyCheckOut= EMAA.getdate()+" "+EMAA.getstaff_outtime();
						dateCheckIn = format.parse(emplyCheckIn);
						dateCheckOut = format.parse(emplyCheckOut);
						difftime=dateCheckOut.getTime()-dateCheckIn.getTime();
						diffMinutes = difftime / (60 * 1000) % 60;
					diffHours = difftime / (60 * 60 * 1000) % 24;
							}
					///fortotal  hours working employee start---------
					long forleave=(Long.parseLong(staffend[0])-Long.parseLong(staffstart[0]));
					long lomgminutes=(Long.parseLong(staffend[1])-Long.parseLong(staffstart[1]));
					long totalwork=forleave+lomgminutes;
		 			long diffInHours = Long.parseLong(staffenter[0]) - Long.parseLong(staffstart[0]);
					long diffminutes = Long.parseLong(staffenter[1]) - Long.parseLong(staffstart[1]);
					totallate=(diffInHours *60)+diffminutes;
					if(totallate<-60){
						OT1=-(totallate)/60;
						totallate=0;
					}else if(totallate<=0){
						totallate=0;}

					diffInHours = Long.parseLong(staffclose[0]) - Long.parseLong(staffend[0]);
					diffminutes = Long.parseLong(staffclose[1]) - Long.parseLong(staffend[1]);
					OT=(diffInHours *60)+diffminutes;
					if(OT<0){
						totallate=totallate-(OT);
						OT=OT1;
					}else{
						OT=(OT/60)+OT1;
					}
					 }

				
			   		DecimalFormat df = new DecimalFormat("##.##");
			   		
					DateFormat dateFormatdate = new SimpleDateFormat("yyyy/MM/dd");
					Date date = new Date();
					String Methoddate=dateFormatdate.format(dateFormat.parse(datestart));
					employeeAttendce e=new employeeAttendce();
					String data[]=e.calculateClOTs(SFTT.getstaff_id(), Methoddate);
					 
					 %> 
					 
					 <td data-title="Duration" align="center">
					 <%if(diffHours!=0){
						 %>
						 <%=diffHours %>:<%=diffMinutes %>
						 <%					 }
					 else if(!EMAA.getstaff_intime().equals("") && !EMAA.getstaff_outtime().equals("")){
					 out.println("00:00");}else{out.println("");}%>
					 
					 </td>
					 <td><%=data[12] %></td>
                <td data-title="Leave type" align="center">
                		<input type="hidden" name="totallate"  id="totallate<%=count%>" value="<%=totallate%>" />
 						<input type="hidden" name="intime"  id="intime<%=count%>" value="<%=EMAA.getstaff_intime()%>"/>
 						<input type="hidden" name="latetimetype" id="latetimetype<%=count%>" value="<%=EMAA.getextra5().toString() %>" />
						<input type="hidden" name="latehours" id="latehours<%=count%>" value="<%=EMAA.getlate_hours()%>"/>
                	<select name="permisssion" id="permisssion<%=count%>">
						<option value="Yes"<%if(EMAA.getlate_hourpermision().toString().equals("Yes")){%>selected="selected"<%}%> >LP</option>
						<option value="No"<%if(!EMAA.getlate_hourpermision().toString().equals("Yes")){%>selected="selected"<%}%>> No</option>
					</select>
                </td>
                <td data-title="OT" align="center">
                	<select name="leavetypess" id="leavetypess<%=count%>">
						<option value="">In Time</option>
						<option value="HCL"<%if(EMAA.getextra5().toString().equals("CL") && EMAA.getlate_hours().equals("0.5")){%>selected="selected"<%}%>>0.5 CL</option>
						<option value="FCL"<%if(EMAA.getextra5().toString().equals("CL") && EMAA.getlate_hours().equals("1")){%>selected="selected"<%}%>>1 CL</option>
					<option value="OHCL"<%if(EMAA.getextra5().toString().equals("CL") && EMAA.getlate_hours().equals("1.5")){%>selected="selected"<%}%>>1.5 CL</option>
						<option value="LA"<%if(EMAA.getextra5().toString().equals("LA")){%>selected="selected"<%}%>> LA</option>
						<option value="OD"<%if(EMAA.getextra5().toString().equals("OD")){%>selected="selected"<%}%>> OD</option>
						<option value="Absent"<%if(EMAA.getextra5().toString().equals("Absent")){%>selected="selected"<%}%>> Absent</option>
					</select>
                </td>
                <td data-title="Duty_Time" align="center">
                	<select name="OTpermisssion" id="OTpermisssion<%=count%>">
						<% int ot_till=Integer.parseInt(EMAA.getextar7().toString());
							for(int len=ot_till;len>0;len--){%>
							<option value="<%=len%>" <%if(EMAA.getextar7().toString().equals(""+len)){%>selected="selected"<%}%> ><%=len%> Hours</option>
						<%}%>
							<option value="0" <%if(EMAA.getextar7().toString().equals("0")){%>selected="selected"<%}%>> No OT</option>
					</select>
                </td>
                <td data-title="Submit" align="center">
                	<%if(EMAA.getconform_status().toString().equals("pending")){%><input type="text" name="submit" id="submit"value="Confirm"  class="submit_button" readonly="readonly" style="width:60px;" /> <!--onClick="return validate(<%//=count%>);" --><%}
  						if(EMAA.getconform_status().toString().equals("confirm")){ %>Confirmed<!--<input type="submit" name="submit1"value="Edit" onClick="return validate(<%//=count%>);" class="submit_button"/>--><%}%>
  					
                </td>
                
                <%-- <td data-title="Photo" align="center">
                <% if(SFTT.getextra1() != null && !SFTT.getextra1().equals("")){ %><a href="<%=request.getContextPath()%>/personalregistry/<%=SFTT.getextra1() %>" target="_blank"><img src="<%=request.getContextPath()%>/personalregistry/<%=SFTT.getextra1() %>" style="height: 30px;width: 30px;" /></a><%} %>
                	
  					
                </td>  --%>             
          <%
          diffMinutes=0;
          diffHours=0;
                  	}%>
           </tr>
        <%}}}
        }}
	fromdate.add(Calendar.DATE, +1); 
	}}}} %>
         </tbody>
         <tr>        
         <td></td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
         <td> <input type="submit" name="button" id="button" class="my_acc_button" value="Update" onclick="return changeInTime();"></input></td>
                  <td>  <input type="submit" name="button" id="button" class="my_acc_button" value="Update" onclick="return changeOutTime();"></input></td>
          <td></td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>          
                    </tr>
                    
          </table>
    </div>
        </form>
      </section>
    </div>
  </div>
  </div>
</div>
</div>
<div style="clear:both"></div>  
 <%}else{ response.sendRedirect("index.jsp");}%>
</body>
</html>