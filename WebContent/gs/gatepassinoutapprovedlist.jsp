<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="mainClasses.GatePassService"%>
<%@ page import="beans.GatePassEntry"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html>
<html>
<head>

<title>gatepassinoutapprovedlist.jsp</title>


<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<link href="css/no-more-tables.css" rel="stylesheet"/>
<jsp:useBean id="SFTQ" class="beans.stafftimings"/>
<jsp:useBean id="EMAF" class="beans.employee_attendence"/>
<jsp:useBean id="SFT" class="beans.stafftimings"/>
<jsp:useBean id="SFTT" class="beans.stafftimings"/>
<script src="js/jquery-1.8.2.js"></script>
<script>
	$(function() {
	var currdate=new Date();
	
		$( "#datestart" ).datepicker({	
			changeMonth: true,
	        changeYear: true,
			showOtherMonths: true,
	        selectOtherMonths: true,
	      dateFormat: 'yy-mm-dd',
			constrainInput: true,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
	        buttonImageOnly: true,
			});
		$( "#dateend" ).datepicker({	
			changeMonth: true,
	        changeYear: true,
			showOtherMonths: true,
	        selectOtherMonths: true,
	      dateFormat: 'yy-mm-dd',
			constrainInput: true,
			showOn: 'both',
			buttonImage: "images/calendar-icon.png",
			buttonText: 'Select Date',
	        buttonImageOnly: true,
			});
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd'
	});
		$(".ui-datepicker-trigger").mouseover(function() {
    $(this).css('cursor', 'pointer');
 });
});	
</script>
<script type="text/javascript">
function datescheck(){
var DaysDiff;

//Date1 = new Date(document.getElementById("dateend").value);
//Date2 = new Date( document.getElementById("cursdate").value);
var myDate = document.getElementById("dateend").value;
	var myDate1 = document.getElementById("cursdate").value;
var datestart = document.getElementById("datestart").value;
if (myDate1<=datestart) {
		 alert("End Date must be lessthan the  StartDate");
          document.getElementById("datestart").focus();
return false;
}
	 // alert(document.getElementById("dateend").value);
	    if (myDate1<=myDate) {
		 alert("End Date must be lessthan the  CurrentDate");
          document.getElementById("dateend").focus();

return false;
}
	   
}</script>
<link rel="stylesheet" href="../themes/ui-lightness/jquery.ui.all.css"/>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<style>

.ui-datepicker-trigger{position: relative;
    top: 6px;}
</style>
</head>
<body>
<div><div ><jsp:include page="my-account-header.jsp"></jsp:include></div>
</div>
<div style="clear:both"></div> 

	<section class="abcd">
	         <div class="container" style="margin-right: auto; margin-left: auto; padding-left: 44px; padding-right: 140px;">
	              <div class="row">
	                   <div class="table_heading span_12_of_12" style="padding-top: 7%;">
	                         <span class="page_head">WELCOME TO GENERAL SECRETARY</span> 
	                   </div> 
	                    <div class="table_heading span_12_of_12 float_left " style="">
                            <span style="width:auto; font-size:14px;font-weight:bold;  text-align:center; float:left; color:#ff5500;">GATEPASSENTRY APPROVED CLS</span> 
                            <div style="float:right;width:auto;height:auto;">
       	                         <a href="my-home.jsp" class="my_acc_button">Back</a>
                            </div>
                       </div>
	                  <form action="gatepassinoutapprovedlist.jsp" >                  
                       <div class="span_12_of_12 " style="float:left;padding:5px 0px 5px 0px;"> 
	                         <div class="col span_3_of_12">
                                  <div class="my_acc_text_feild"> 
                                       From Date : 
                                  </div> 
                                  <div class="my_acc_input">
                                       <input type="text" name="datestart" id="datestart" class="DatePicker " readonly="readonly" required=""> 
                                  </div>
                                 
                             </div>
                             <div class="col span_3_of_12">
                                   <div class="my_acc_text_feild">
                                        To  Date : 
                                   </div>
                                   <div class="my_acc_input">
                                        <input type="text" name="dateend" id="dateend" class="DatePicker" readonly="readonly" required="">
                                   </div>
                                   
                             </div>
  
                              <div class="col span_1_of_12">
                                  <input type="submit" name="submit" value="SEARCH" onclick="return datescheck();">
                               </div>
                          </div>
                          </form>
                         
     <table class="table-bordered table-striped table-condensed" style="width:100%; margin-top:15px;" id="tblExport">
          <thead>
               <tr>
                   <th>Date</th>   
         	       <th>Staff ID</th>
         	       <th>StaffName</th>
         	       <th>Department</th>
         	       <th>Intime</th>
         	       <th>OutTime</th>
         	       <th>Cl Type</th>
			       <th>GatePass  CLS</th>
			       <th>GatePassType</th>
		      </tr>
       </thead>
       <tbody>
       <%
       List<GatePassEntry> gatePassEntryList=null;
       SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       SimpleDateFormat dateFormat1=new SimpleDateFormat("dd-MMM-YYYY");
	           String datestart="";
	           if(request.getParameter("datestart")!=null){
	        	    datestart=request.getParameter("datestart"); 
	        	 }
		    String dateend="";
		    if(request.getParameter("dateend")!=null){
		    	dateend=request.getParameter("dateend") ;
		    }
		if(!datestart.equals("") && !dateend.equals("")){
			          String fromDate="";
			          String toDate="";
			          fromDate=datestart+" 00:00:00";
			          toDate=dateend+" 23:59:59";
				          GatePassService gatePassService=new GatePassService();
				          gatePassEntryList =gatePassService.gateUpdatedPassEntryApprovedList(fromDate, toDate);
		}
		if(gatePassEntryList!=null && gatePassEntryList.size()>0){
			for(int i=0;i<gatePassEntryList.size();i++){
				GatePassEntry gatePassEntry=gatePassEntryList.get(i);
				Date createdDate=dateFormat.parse(gatePassEntry.getCreatedDate());
				String formatedDate=dateFormat1.format(createdDate);
		%>
            <tr>
               <td><%=formatedDate%></td>
               <td><%=gatePassEntry.getStaffId()%></td>
               <td><%=gatePassEntry.getStaffName()%></td>
               <td><%=gatePassEntry.getDepartment()%></td>
               <td><%=gatePassEntry.getInTime()%></td>
               <td><%=gatePassEntry.getOuttime()%></td>
               <td><%=gatePassEntry.getClType()%></td>
               <td><%=gatePassEntry.getClnumber()%></td>
                <td><%=gatePassEntry.getGatePassType()%></td>
             
          </tr>
          <%}
		}
			%>

      </tbody>
   </table>
	      </div>
	      </div>
	
	</section>
	
	
    
</body>
</html>