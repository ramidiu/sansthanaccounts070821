<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@ page import="mainClasses.stafftimingsListing,beans.stafftimings"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EMPLOYEE ATTENDANCE</title>
<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<jsp:useBean id="EMAF" class="beans.employee_attendence"/>
<jsp:useBean id="SHT" class="beans.shift_timings"/>
<link href="css/no-more-tables.css" rel="stylesheet"/>
<script src="js/jquery-1.8.2.js"></script>
<script src="js/jquery-1.3.2.min.js"></script>
<script  type="text/javascript">
function combos(denom,desti) { 
 
		var xx;
		var yearss=document.getElementById("currentyear").value;
        var com_id = (document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value).replace(" ","@").replace("&","*"); 
		document.getElementById(desti).options.length = 0;
		var sda1 = document.getElementById(desti);
		
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari 
  xx=new XMLHttpRequest();
  
  }
else
  {// code for IE6, IE5 
  xx=new ActiveXObject("Microsoft.XMLHTTP");
  }
xx.onreadystatechange=function()
  {//alert(xx.status+"::"+xx.readyState); 
 if (xx.readyState==4 && xx.status==200)
 { 
		var mytool_array=xx.responseText.split("\n");
		
	
		 for(var i=0;i<mytool_array.length;i++)
		{
		if(mytool_array[i] !="")
		{
		
		var y=document.createElement('option');
		var val_array=mytool_array[i].split(":");
		
					y.text=val_array[1];
					y.value=val_array[0];
					try
					{
					sda1.add(y,null);
					}
					catch(e)
					{
					sda1.add(y);
					}
		}
		
		}
    }
  }
  
xx.open("GET","empcategories.jsp?id="+com_id+"&yearss="+yearss,true);
xx.send();
}
 

function checkCheckBox()
{
	if($("#approved:checked").length == 0)
    {
	    alert("please check at list one staff");
    	return false;
	}
}

</script>
<script type="text/javascript">
function CheckAll(chk)
{
	for (i = 0; i < chk.length; i++)
		chk[i].checked = true ;
}
function UnCheckAll(chk)
{
for (i = 0; i < chk.length; i++)
chk[i].checked = false ;
}
</script> </head>
<div><div ><jsp:include page="my-account-header.jsp"></jsp:include></div>
<jsp:useBean id="STF" class="beans.stafftimings"/>
<jsp:useBean id="STFF" class="mainClasses.stafftimingsListing"/>
<div align="center">
<table width="98%" border="0" cellspacing="1" bgcolor="#eeeeee" cellpadding="0">

<jsp:useBean id="OB" class="beans.openingcl_bal"/>

<tr><td colspan=32> <strong>Opening CL Balance for Employee Listing<%if(request.getParameter("year")!=null && request.getParameter("month")!=null){%>(<%=request.getParameter("month")%>-<%=request.getParameter("year")%>)<%} %></strong></td></tr>
  <tr>
  <form name="yearform" id="yearform" action="openingcl_bal_Form.jsp" method="post">
  <input type="hidden" name="page" id="page" value="openingcl_bal_Form" />
    <td class="kws_text">Month :</td>
    <td align="left"><select name="month"  id="month"><option value="">-month-</option>
                                                <option value="January">January</option>
                                                <option value="February">February</option>
                                                <option value="March">March</option>
                                                <option value="April">April</option>
                                                <option value="May">May</option>
                                                <option value="June">June</option>
                                                <option value="July">July</option>
                                                <option value="August">August</option>
                                                <option value="September">September</option>
                                                <option value="October">October</option>
                                                <option value="November">November</option>
                                                <option value="December">December</option>
                                                </select></td>
    <td class="kws_text"> Year :</td>
    <td align="left"><select  name="year"  id="year">
    <option value="">-year-</option>
		 <%
		 	for(int i=2013;i<2050;i++){
		 %>
			<option value="<%=i%>"><%=i%></option> 
		 <%
 		 	}
 		 %>			 
		 </select></td>
       
    <td align="left"><input type="submit" name="submit" value="submit" class="submit_button"  /></td>
    </form>
  </tr>
<tr><th bgcolor="#fffef3">Employee ID</th>
<th bgcolor="#fffef3">Name</th>
<th bgcolor="#fffef3">Month, Year</th>
<th bgcolor="#fffef3">Opening Balance</th>
<th bgcolor="#fffef3">Edit</th>
<th bgcolor="#fffef3">Delete</th>
</tr>
<%
	if(request.getParameter("year")!=null && request.getParameter("month")!=null){
%>
<%
	mainClasses.openingcl_balListing OB_CL = new mainClasses.openingcl_balListing();
List OB_List=OB_CL.getopeningcl_bal(request.getParameter("month"),request.getParameter("year"));
for(int i=0; i < OB_List.size(); i++ ){
	OB=(beans.openingcl_bal)OB_List.get(i);
%><tr>
<td class="attendance_text" bgcolor="#FFF"><%=OB.getemp_id()%></td>
<td class="attendance_text" bgcolor="#FFF"><%=STFF.getstafName(OB.getemp_id())%></td>
<td class="attendance_text" bgcolor="#FFF"><%=OB.getmon_yr().replace(" ", ", ")%></td>
<td class="attendance_text" bgcolor="#FFF"><%=OB.getvalue()%></td>
<td class="attendance_text" bgcolor="#FFF"><a href="openingcl_bal_Edit.jsp&emp_id=<%=OB.getemp_id()%>&mon_yr=<%=OB.getmon_yr()%>">Edit</a></td>
<td class="attendance_text" bgcolor="#FFF"><a href="openingcl_bal_Delete.jsp?emp_id=<%=OB.getemp_id()%>&mon_yr=<%=OB.getmon_yr()%>">Delete</a></td>
</tr>
<%}}%>

</table>
</div>
<div align="center">
<table width="98%" border="0" cellspacing="8" cellpadding="0">
<%DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
DateFormat dateFormat2 = new SimpleDateFormat("MMMM yyyy");
Calendar cal = Calendar.getInstance(); 
cal.add(Calendar.MONTH, -1); 
Date lastMonthDate=cal.getTime();
String lastMonth=dateFormat.format(lastMonthDate);
cal = Calendar.getInstance(); 
cal.add(Calendar.MONTH, -2); 
Date lastButoneMonthDate=cal.getTime(); 
String lastbutoneMonth=dateFormat.format(lastButoneMonthDate);
	if(request.getParameter("status")!=null && request.getParameter("status").toString().equals("false"))
	{
%>
<tr>
<td colspan=2 bgcolor="#eeeeee" style="color: red;">Alreday inserted, Duplicate not possible</td>
</tr>
<%
	}
%>
<tr>
<td colspan=5 bgcolor="#eeeeee" style="text-align: center;"><strong>Create Opening CL  For <%=lastMonth %></strong></td>
</tr>

<jsp:useBean id="SFTw" class="beans.stafftimings" />
<form name="openingcl_bal_Update" method="POST" action="openingcl_bal_Insert.jsp" onSubmit="return checkCheckBox();">
	<tr>
		<th align="left">sno</th>
		<th align="left">approved</th>
		<th align="left" colspan="2">Employee</th>
		<!-- <th align="left">Department</th> -->
		<th align="left">opening Balance
		<input type="hidden" name="openingdate" value="<%=lastMonth%>"/>
		</th>
	</tr>
	<tr>
<!-- 	<a href="my-home.jsp" class="my_acc_button">Back</a> -->
	<td></td><td><input type="button"  value="SelectAll" onClick="CheckAll(document.openingcl_bal_Update.approved)" class="my_acc_button">
		<input type="button"  value="UncheckAll" onClick="UnCheckAll(document.openingcl_bal_Update.approved)" class="my_acc_button"><input type="submit" id="subm" name="Submit" value="OK"  class="my_acc_button"></td></tr>
	<%
		String monthyear= dateFormat2.format(lastMonthDate);
		stafftimingsListing listing=new stafftimingsListing();
		List l=listing.getUnApprovedStaffs(monthyear);
		for(int i=0,j=1;i<l.size();i++,j++)
		{
			boolean flag=true;
			stafftimings staffs=(stafftimings)l.get(i);
			
					
			%>
	
		<tr>
			<td align="left"><%=j%></td>
			<td align="left"><input type="checkbox" id="approved" name="approved" value=<%=staffs.getstaff_id()%> ></td>
			
			<td align="left" colspan="2"><%=staffs.getstaff_id() %><input type="text" name="employee_name<%=staffs.getstaff_id()%>" value="<%=staffs.getstaff_name() %>" ></td>
			<%-- <td align="left"><input type="text" name="department_name<%=staffs.getstaff_id()%>" value=<%=staffs.getstaff_type() %> ></td> --%>
			<% 
				com.accounts.saisansthan.employeeAttendce employeeAttend=new com.accounts.saisansthan.employeeAttendce();
				String data[]=employeeAttend.calculateClOTs(staffs.getstaff_id(), lastMonth);
				//data[2] is the last month closing cl which is opening for this month
				float openingBal=0.0f;
								if(!(data[2].equals("null")))
								{
									openingBal=Float.parseFloat(data[2]);
									if(openingBal<0)
									{
										openingBal=0.0f;
									}
									
								}
			%>
			<td align="left"><input type="text" name="opening_bal<%=staffs.getstaff_id()%>" 
					value="<%=openingBal%>" ></td>
			<td><input type="hidden" name="actual_opening_cl<%=staffs.getstaff_id()%>" value=<%=data[2]%>></td>
		
			</tr>
		
	<%}//for %>
	
	
</form>
</table>
</div>
