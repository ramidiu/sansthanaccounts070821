<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>EMPLOYEE LISTING || SHRI SHIRDI SAI BABA SANSTHAN TRUST</title>
<link rel="stylesheet" type="text/css" href="gs-css/Responsive.css" />
<link rel="stylesheet" type="text/css" href="css/device.css" />
<meta name="viewport" content="width=device-width, initial-ratio=1.0" />
<link href="css/no-more-tables.css" rel="stylesheet"/>
</head>
<body>
<jsp:useBean id="SFT" class="beans.stafftimings"/>
<jsp:useBean id="SFT1" class="beans.stafftimings"/>
<jsp:useBean id="SFT9" class="beans.stafftimings"/>
<jsp:useBean id="SFT2" class="beans.stafftimings"/>

<div><div ><jsp:include page="my-account-header.jsp"></jsp:include></div>
</div>
<div style="clear:both"></div>  
<div class="container">
<div class="row">
	<div class="span_12_of_12 main_body " >
      <div class="col span_10_of_12 middle_body"  style="padding:0% 1.5% 1.5% 1.5% ;">
      <div class="table_heading span_12_of_12"><span class="page_head">WELCOME TO GENERAL SECRETARY</span> </div>
 		
 		<div class="span_12_of_12 float_left">
        <section id="no-more-tables">
          <div class="table_heading span_12_of_12"><span style="margin:15px auto;width:auto; text-align:center; color:#ff5500;">STAFF DUTIES W.E.F.<%if(request.getParameter("year")!=null){%>(<%=request.getParameter("year") %>)<%} %></span> </div>  
          <div class="span_12_of_12 " style="border:1px solid #CCC;float:left;padding:5px 0px 5px 0px;">
          <div class="col span_6_of_12"><span class="sub_head" style="float:right;margin-right:10px;">YEAR : </span></div>
          <%
	mainClasses.stafftimingsListing SFT_CL = new mainClasses.stafftimingsListing();
List SFT_List=SFT_CL.getStaffYearList();if(SFT_List.size()!=0){
%>
            <form name="stafflist" id="stafflist" action="stafftimings_Listing.jsp" method="get" >
           <div class="col span_6_of_12">
		          <select name="year"   id="year" onchange="validate();">
					<option value="">-select-</option>
					 <%
					 for(int e=(SFT_List.size()-1); e >=0; e -- ){
						 SFT9=(beans.stafftimings)SFT_List.get(e);
						 %>
							<option value="<%=SFT9.getstaff()%>" <%if(request.getParameter("year")!=null)if(request.getParameter("year").toString().equals(SFT9.getstaff().toString())){%> selected="selected" <%}%> ><%=SFT9.getstaff()%></option>
      				<%
      					}
      				%>
 				  </select>
            </div>
            </form>
            <%
	}
%>
           </div>      
          <%
	List SFT_Listww=null;if(request.getParameter("year")!=null){  SFT_Listww=SFT_CL.getStaffYearList(request.getParameter("year"));}else{
 SFT_Listww=SFT_CL.getStaffYearList();}
int count=0;
if(SFT_Listww.size()!=0){
int toasize=SFT_Listww.size();

for(int i=0; i < SFT_Listww.size(); i++ ){
SFT=(beans.stafftimings)SFT_Listww.get(0); }
%>           
<%-- <tr>
    <td align="center" height="50" class="orange_heading">STAFF DUTIES W.E.F.<%=SFT.getstaff()%></td></tr> --%>
    <%
	List SFT_YaerList=SFT_CL.getStaffcategoryList(SFT.getstaff().toString());

if(SFT_YaerList.size()!=0){
for(int j=0; j < SFT_YaerList.size(); j++ ){
SFT1=(beans.stafftimings)SFT_YaerList.get(j);
%><div class="table_heading span_12_of_12" align="center">
<span style="margin:15px auto;width:auto; color:#ff5500;"><%=SFT1.getstaff()%></span></div>
  </tr>
          <table class="table-bordered table-striped table-condensed" style="width:100%;">
           <%
  	List SFT_AllList=SFT_CL.getALLByStaffcategory(SFT1.getstaff().toString(),SFT.getstaff().toString());
    if(SFT_AllList.size()!=0){
  %>
           <thead>
              <tr>   
                    <th >S.NO. </th>
                    <th>STAFF ID </th>
                    <th >NEW STAFF ID</th>
                    <th >NAME </th>
                    <th >DESIGNATION </th>
                    <th >TIMINGS </th>
                    <th >STATUS </th>
                    <th >EDIT </th>
                     <th >Delete </th>
               </tr>
            </thead>
            <tbody>
           <%
  	for(int k=0; k < SFT_AllList.size(); k++ ){
  SFT2=(beans.stafftimings)SFT_AllList.get(k);
  if(count==0)count=count+1;
  %>
            <tr>   
         		  <td data-title="Period" align="center"><%=count%></td>
         		  <td data-title="Period" align="center"><%=SFT2.getstaff_id()%></td>
         		  <td data-title="Period" align="center"><%=SFT2.getStaff_nid()%></td>
         		  <td data-title="Period" align="center"><%=SFT2.getstaff_name()%></td>
         		  <td data-title="Period" align="center"><%=SFT2.getdesignation()%></td>
         		  <td data-title="Period" align="center"><%=SFT2.getstarttime()%> to <%=SFT2.getendtime()%></td>
         		  <td data-title="Period" align="center"><%=SFT2.getstatus()%></td>
         		  <td data-title="Period" align="center"><a href="stafftimings_Edit.jsp?staff_id=<%=SFT2.getstaff_id()%>"><img src="images/edit_icon.jpg" alt="Click here to Edit" title="Click here to Edit Shift of <%=SFT2.getstaff_name()%>" width="15" height="26" border="0" /></a></td>
         		  <td data-title="Period" align="center"><a href="stafftimings_Delete.jsp?staff_id=<%=SFT2.getstaff_id()%>"><img src="images/delete_icon.jpg" alt="Click here to delete" title="Click here to delete Shift of <%=SFT2.getstaff_name()%>" width="15" height="26" border="0" /></a></td>
         		  
         	 </tr>
         	<%if(count!=0)count=count+1;}%>
           </tbody> 
         </table>
         <% }}}%>
       </section>
       </div>
       </div>
      </div>
	</div>
</div>
<div style="clear:both"></div>  
 <%}else{ response.sendRedirect("index.jsp");}%>
<script type="text/javascript">
function validate(){
	if(document.getElementById("year").value==""){
		alert("select year");
		return false;
	}else{
		document.stafflist.action="stafftimings_Listing.jsp";
		document.stafflist.submit();
	}
	}
</script>
</body>
</html>