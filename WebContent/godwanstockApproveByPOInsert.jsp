<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@page import="mainClasses.godwanstock_scListing"%>
<%@page import="mainClasses.productsListing"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="MEP" class="beans.medicalpurchasesService"/>
<jsp:useBean id="PRD" class="beans.productsService"/>
<jsp:useBean id="inv" class="beans.invoiceService"/>
<jsp:useBean id="PR" class="beans.products"/>
<jsp:useBean id="SUPA" class="beans.superadmin"/>
<%
godwanstock_scListing GODSList = new godwanstock_scListing();
mainClasses.tabletsListing TABL= new mainClasses.tabletsListing();
mainClasses.headofaccountsListing HOFA_L=new mainClasses.headofaccountsListing();
String invoiceId=request.getParameter("invoiceId");
productsListing PROL=new productsListing();
String purchaserate="";
String extra1=TABL.getinvociceno();
productsListing PRD_L=new productsListing();
Calendar c1 = Calendar.getInstance(); 
TimeZone tz = TimeZone.getTimeZone("IST");
DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String date=(dateFormat.format(c1.getTime())).toString();
String head_account_id=request.getParameter("headAccountId");
String total = request.getParameter("total");
String discount="";
String productId="";
String quantity="";
String vat="";
String otherCharges="";
String category="";
String CGST="";
String SGST="";
String IGST="";
String extra16  = request.getParameter("total");
if(request.getParameter("category"+invoiceId)!=null){
	category=request.getParameter("category"+invoiceId).toString();
}
if(request.getParameter("otherCharges"+invoiceId)!=null){
	otherCharges=request.getParameter("otherCharges"+invoiceId).toString();
}
String gsld="";
String emp_id=session.getAttribute("empId").toString();
String department=request.getParameter("HOA");
String billApprovalStatus="Not-Approve";
if(request.getParameter("bilApprvStatus")!=null && !request.getParameter("bilApprvStatus").equals("") && request.getParameter("bilApprvStatus").equals("BillApproveNotRequire")){
	billApprovalStatus="Approved";
}
int i=0;
int j=0;

if(request.getParameter("end"+invoiceId)!=null && !request.getParameter("end"+invoiceId).toString().equals("")){
i=Integer.parseInt(request.getParameter("end"+invoiceId).toString());
}
if(request.getParameter("start"+invoiceId)!=null && !request.getParameter("start"+invoiceId).toString().equals("")){
j=Integer.parseInt(request.getParameter("start"+invoiceId).toString());
}
if(i>0){
for(int h=0;h<i;h++){
	if(request.getParameter("gsId"+j+h)!=null && request.getParameter("purchaseRate"+j+h)!=null && request.getParameter("vat"+j+h)!=null){
		productId=request.getParameter("prdid"+j+h);
		//System.out.println("prdid"+count[h]);
		//System.out.println("productId"+productId);
		purchaserate=request.getParameter("purchaseRate"+j+h);
		discount=request.getParameter("discountAmt"+j+h);
		vat=request.getParameter("vat"+j+h);
		
		 if(request.getParameter("CGSTname"+j+h)!=null && !request.getParameter("CGSTname"+j+h).equals("null") && !request.getParameter("CGSTname"+j+h).equals("")){
			 CGST=request.getParameter("CGSTname"+j+h);
		 }
		 else{
			 CGST="0"; 
		 }
		 if(request.getParameter("IGSTname"+j+h)!=null && !request.getParameter("IGSTname"+j+h).equals("null") && !request.getParameter("IGSTname"+j+h).equals("")){
			 IGST=request.getParameter("IGSTname"+j+h);
		 }
		 else{
			 IGST="0";
		 }
		 if(request.getParameter("SGSTname"+j+h)!=null && !request.getParameter("SGSTname"+j+h).equals("null") && !request.getParameter("SGSTname"+j+h).equals("")){
			 SGST=request.getParameter("SGSTname"+j+h);
		 }
		 else{
			 SGST="0";
		 }
		gsld=request.getParameter("gsId"+j+h);

 %>
<jsp:useBean id="GOD" class="beans.godwanstockService"/>
<jsp:useBean id="GODS" class="beans.godwanstock_scService"/>
<jsp:setProperty name="GOD" property="gsId" value="<%=gsld%>"/>
<jsp:setProperty name="GOD" property="vat" value="<%=vat%>"/>
<jsp:setProperty name="GOD" property="extra16" value="<%=extra16%>"/>
<jsp:setProperty name="GOD" property="extra18" value="<%=CGST%>"/>
<jsp:setProperty name="GOD" property="extra19" value="<%=SGST%>"/>
<jsp:setProperty name="GOD" property="extra20" value="<%=IGST%>"/>
<jsp:setProperty name="GOD" property="purchaseRate" value="<%=purchaserate%>"/>
<jsp:setProperty name="GOD" property="extra10" value="<%=discount %>"/>
<jsp:setProperty name="GOD" property="extra8" value="<%=otherCharges %>"/>
<%if(category.equals("serviceBill") || category.equals("dcbill")){ %>
<%-- <jsp:setProperty name="GOD" property="approval_status" value="Approved"/> --%>
<jsp:setProperty name="GOD" property="approval_status" value="<%=billApprovalStatus %>"/>
<jsp:setProperty name="GOD" property="profcharges" value="checked"/>
<%} else { %>
<jsp:setProperty name="GOD" property="profcharges" value="unchecked"/>
<%} %>
<jsp:setProperty name="GOD" property="po_approved_status" value="Approved"/>
<jsp:setProperty name="GOD" property="po_approved_by" value="<%=emp_id%>"/>
<jsp:setProperty name="GOD" property="po_approved_date" value="<%=date%>"/>
<%=GOD.update()%><%=GOD.geterror()%>


<jsp:useBean id="INV" class="beans.invoiceService" />
<jsp:setProperty name="INV" property="inv_id" value="<%=invoiceId%>" /> 
<jsp:setProperty name="INV" property="amount" value="<%=total%>" />
<jsp:setProperty name="INV" property="pendingamount" value="<%=total%>" />
<jsp:setProperty name="INV" property="extra5" value="<%=otherCharges%>" />
<%=INV.update()%><%=INV.geterror() %>

<!--this lines are written by pradeep to update latest entry in godwanstock_sc table  -->
<jsp:setProperty name="GODS" property="gsId" value="<%=gsld%>"/>
<jsp:setProperty name="GODS" property="vat" value="<%=vat%>"/>
<jsp:setProperty name="GODS" property="purchaseRate" value="<%=purchaserate%>"/>
<jsp:setProperty name="GODS" property="extra10" value="<%=discount %>"/>
<jsp:setProperty name="GODS" property="extra8" value="<%=otherCharges %>"/>
<%if(category.equals("serviceBill") || category.equals("dcbill")){ %>
<%-- <jsp:setProperty name="GOD" property="approval_status" value="Approved"/> --%>
<jsp:setProperty name="GODS" property="approval_status" value="<%=billApprovalStatus %>"/>
<jsp:setProperty name="GODS" property="profcharges" value="checked"/>
<%} else { %>
<jsp:setProperty name="GODS" property="profcharges" value="unchecked"/>
<%} %>
<jsp:setProperty name="GODS" property="po_approved_status" value="Approved"/>
<jsp:setProperty name="GODS" property="po_approved_by" value="<%=emp_id%>"/>
<jsp:setProperty name="GODS" property="po_approved_date" value="<%=date%>"/>
			<%
				String countInString = GODSList.getCountOfRecords(gsld);
				int count = Integer.parseInt(countInString);
				if(count == 1)
				{%>
					<%=GODS.update()%><%=GODS.geterror()%>
				<%}
				else if(count > 1)
				{
					String entrydate = GODSList.getMaxDateForRecord(gsld);
					%>
					<%GODS.updateBasedOnExpIdAndDate(gsld, entrydate); %><%=GODS.geterror() %>
				<% 	
				}%>

<!--END  -->

<%
purchaserate="";
discount="";
vat="";
gsld="";
}}}
   response.sendRedirect("pendingPoApproval.jsp");   
%>