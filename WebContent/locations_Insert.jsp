<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="LOC" class="beans.locationsService">
<%
String location_name=request.getParameter("locationname");
String narration=request.getParameter("narration");
String emp_id=session.getAttribute("empId").toString();
Calendar c1 = Calendar.getInstance(); 
c1.setTimeZone(TimeZone.getTimeZone("IST"));
TimeZone tz = TimeZone.getTimeZone("IST");

DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat  dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
DateFormat  dateFormat3= new SimpleDateFormat("HH:mm:ss");
dateFormat.setTimeZone(tz.getTimeZone("IST"));
String date1=(dateFormat.format(c1.getTime())).toString();
String hour=""+c1.get(Calendar.HOUR);
String min=""+c1.get(Calendar.MINUTE);
String sec=""+c1.get(Calendar.SECOND);
//String time=dateFormat3.format((c1.get(Calendar.HOUR)+":"+c1.get(Calendar.MINUTE)+":"+c1.get(Calendar.SECOND)));
String time=dateFormat3.format(dateFormat.parse(date1));
String date=request.getParameter("date")+" "+time;
String head_account_id=request.getParameter("HOA");
%>
<jsp:setProperty name="LOC" property="location_name" value="<%=location_name%>"/>
<jsp:setProperty name="LOC" property="narration" value="<%=narration%>"/>
<jsp:setProperty name="LOC" property="emp_id" value="<%=emp_id%>"/>
<jsp:setProperty name="LOC" property="date" value="<%=date%>"/>
<jsp:setProperty name="LOC" property="head_account_id" value="<%=head_account_id%>"/>
<jsp:setProperty name="LOC" property="extra1" value=""/>
<jsp:setProperty name="LOC" property="extra2" value=""/>
<jsp:setProperty name="LOC" property="extra3" value=""/>
<jsp:setProperty name="LOC" property="extra4" value=""/>
<%=LOC.insert()%><%=LOC.geterror()%>
</jsp:useBean>
<%
response.sendRedirect("Locations-Create.jsp");
%>