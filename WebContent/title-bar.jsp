<%@page import="mainClasses.headofaccountsListing"%>
<%@page import="mainClasses.counter_balance_updateListing"%>
<%@ page import="java.util.List,java.util.ArrayList" %>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<div class="vendor-name">
<%String departname="Sai Sansthan Accounts";
headofaccountsListing HOAS_L=new headofaccountsListing();
if(session.getAttribute("headAccountId")!=null){
	if(!session.getAttribute("headAccountId").toString().equals("0")){
	departname=HOAS_L.getHeadofAccountName(session.getAttribute("headAccountId").toString());
	}
}
%>

<style>
.menu {
    width: 53%!important;
    height: auto;
    padding: 0;
    float: left;
    margin: 0 -45px 0 3px;
}
.logo-here {
    width: 259px!important;
    float: left;
}
</style>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<div class="logo-here"><span style="color:#FFF;"><%=departname.trim()%></span></div>



<div class="menu">
<jsp:useBean id="PER1" class="beans.employepermissions"/>  
<ul>
<li><a href="welcome.jsp">HOME</a></li>
<%
mainClasses.employepermissionsListing PER_CL1 = new mainClasses.employepermissionsListing();
mainClasses.cashier_reportListing CASH_REP=new mainClasses.cashier_reportListing();
counter_balance_updateListing COUBL=new counter_balance_updateListing();
String perm1[]=null;
ArrayList permisions1=new ArrayList();
	List PER_List1=PER_CL1.getEmployeDetratmentpermissions(session.getAttribute("empId").toString(),session.getAttribute("headAccountId").toString());
	if(PER_List1.size()>0){
		PER1=(beans.employepermissions)PER_List1.get(0);
	}
	perm1=PER1.getemp_permissions().split(",");
	
	for(String p: perm1)
		permisions1.add(p);
  if(session.getAttribute("headAccountId").toString().equals("1") || session.getAttribute("headAccountId").toString().equals("0") ){
	if(permisions1.contains("Medical Hall"))
	{%>
		<!-- <li><a href="tablets_Form.jsp">cREATE</a></li> -->
		<li><a href="#">MEDICINE ISSUE</a>
			<ul>
				<li><a href="medicalIssue_Form.jsp">Medicines Issue</a></li>
				<li><a href="diagnosticIssue_Form.jsp">Diagnostic Test Issue</a></li>
				<li><a href="physiotherapyIssue_Form.jsp">Physiotherapy Issue</a></li>
				<li><a href="raiseIndent.jsp">Raise Indent</a></li>
			</ul>
		</li>
		<li><a href="#">STOCK APROVAL</a>
			<ul>
				<li><a href="godwanstockApprove.jsp">Stock Approval Godown</a></li>
			</ul>
		</li>
	
				<li><a href="#">REPORTS</a>
			<ul>
				<li><a href="doctorTimings-Report.jsp">Doctors Timings</a></li>
				<!-- <li><a href="reportfordiagnostic.jsp">Report For Diagnostic Test</a></li> -->
					<li><a href="ReceiptDoctorTestWiseReport.jsp">Receipt DoctorTest WiseReport</a></li>
				<li><a href="dayWiseTestReport.jsp">Day Wise Test Report</a></li>
				<li><a href="medicalSalesList.jsp">Daily Patients List</a></li>
				<li><a href="doctorWise-PatientsReport.jsp">Doctor Wise Patients</a></li>
				<li><a href="doctor-patient-MedicineIssueReport.jsp">Doctor & Patients Wise Medicine Issues</a></li>
				<li><a href="doctor-MedicineIssueReport.jsp">Doctor Wise Medicine Issues</a></li>
				<li><a href="doctor-patient-PhysiotheraphyIssueReport.jsp">Doctor & Patients Wise Physiotheraphy Issue Report</a></li>
			  	<li><a href="doctor-PhysiotheraphyIssueReport.jsp">Doctor Wise Physiotheraphy Issues</a></li>
			  	<!-- <li><a href="doctor-patient-diagnosticIssueReport.jsp">Doctor Wise Test Issues</a></li>  -->
				<li><a href="tablets_Form.jsp">Medicine Stock  </a></li>  
				<!-- <li><a href="diagnosticsReport.jsp">Doctor Wise Diagnostics Report </a></li> 
				<li><a href="diagnosticsReportDoctorPatientWise.jsp">Doctor & Patient Wise Diagnostics Report </a></li> --> 
				<li><a href="TestNameWithCode.jsp">Test Name With Code </a></li> 
			   <li><a href="DailyMedicalReportNew.jsp">MedicalDailyReports</a></li>
			   
			    <li><a href="medicineissuereport.jsp">MedicineConsumptionReport</a></li> <!-- added by madhav  -->
			</ul>
		</li>
		<%if(permisions1.contains("stockRequest"))
		{ %>
		<li><a href="#">INDENT</a>
		<ul>
			<li><a href="indentReport.jsp">Raise Indent</a></li>
			<li><a href="indent-raised-list.jsp">Raised Indent List</a></li>
			<li><a href="indentReportList.jsp">Indent's List</a></li>
			</ul>
			</li>
	<%}}if(permisions1.contains("Reception")){%>
		<li><a href="#">APPOINTMENTS & REGISTER</a>
			<ul>
			
	             <%if(session.getAttribute("Role").equals("medicalreception")){ %>		
				<li><a href="patientsentry_Listing.jsp"> Appointment's</a></li>
				<%} %>
				<%if(session.getAttribute("Role").equals("medicalhall")){ %>		
				<li><a href="physiotherapypatientsentry_Listing.jsp"> Appointment's</a></li>
				<%} %>
				
				
				<!-- <li><a href="Diagnosticpatientsentry_Listing.jsp">Diagnostic Appointment's</a></li> -->
				<li><a href="patientsList.jsp">Patients List</a></li>
				<li><a href="doctordetails_Form.jsp">Doctors</a></li>
			</ul>
		</li>
	
		<li><a href="#">REPORTS</a>
			<ul>
				<li><a href="doctorTimings-Report.jsp">Doctors Timings</a></li>
				<li><a href="medicalSalesList.jsp">Daily Patients List</a></li>
				<li><a href="doctorWise-PatientsReport.jsp">Doctor Wise Patients</a></li>
				<li><a href="doctor-patient-MedicineIssueReport.jsp">Doctor & Patients Wise Medicine Issues</a></li>
				  
			</ul>
		</li>
	<%}%>
	<%}if(session.getAttribute("headAccountId").toString().equals("3") || session.getAttribute("headAccountId").toString().equals("0") || session.getAttribute("headAccountId").toString().equals("5")){
		if(permisions1.contains("Shop"))
	{%>
		<li><a href="#">SALE</a>
		<ul>
			<li><a href="shopSale.jsp">Sales Receipt</a></li>
			<li><a href="Sale-Receipt-For-Subhead.jsp">Other Receipt-For Sub head</a></li>
			<li><a href="reprintInvoiceList.jsp">Reprint Invoice</a></li>
		</ul>
		</li>
		<li><a href="#">SALES REPORTS</a>
		<ul>
        	<li><a href="shopSalesList.jsp">Shop Sales Report</a></li>
        	<li><a href="shopSales-DetailReport.jsp">Sales Detail Report</a></li>
        	<li><a href="shopSalesList.jsp?paytype=creditsale">Credit Sales Detail Report</a></li>
        	<li><a href="ProductWise-sale-report.jsp">Product Wise Sales</a></li>
		    <li><a href="shopStockList.jsp">Shop stock Report</a></li> 
		     <li><a href="poojaStore-StockReport.jsp">Shop & Godown stock Valuation</a></li> 
		</ul>
		</li>
	<%}
		if( session.getAttribute("Role").equals("PO") || session.getAttribute("Role").equals("AsstPO")){
	
	%>
		<li><a href="#">PURCHASES ENTRY</a>
			<ul>
				<li><a href="#">Stock Entry</a>
				<ul class="level-2">
					<li><a href="godwanForm.jsp">Master Stock Entry</a></li>					
					<li><a href="directChallanEntry.jsp">DeliveryChallan Entry</a></li>
				</ul>
				</li>
				<li><a href="master-stockEntry-withoutIndent.jsp">With Out Indent MSE entry</a></li>
				<li><a href="Services-Bills-Entry.jsp">Services Entry</a></li>
				<li><a href="godwanStockReportList.jsp">M.S.E Report</a></li>
				<li><a href="Master-Stock-Entry-Report.jsp">M.S.E Detail Report</a></li>	
				<li><a href="prasadamExpenseReport.jsp">Prasadam Expenses Report</a></li>
			</ul>
		</li>
		<li><a href="#">CREATE</a>
			<ul>
			<li><a href="newVendor.jsp">New Vendor</a></li>
			<li><a href="openingBalanceProductForm.jsp">openingBalanceProductForm</a></li>
				<!-- <li><a href="Devotee-Registration.jsp?type=EMPLOYEE">Unique Id Registration</a></li>
				<li><a href="Employee-Volunteer-List.jsp">Registration List</a></li> -->
				<!-- <li><a href="Devotee-Registration.jsp?type=EMPLOYEE">Employee Registration</a></li>
				<li><a href="Devotee-Registration.jsp?type=VOLUNTEER">Volunteer Registration</a></li> -->
			</ul>
		</li>
	<%} if(permisions1.contains("stockTransfer")){%>
    	<li><a href="#">STOCK</a>
    		<ul>
				<li><a href="shopStockTransfer.jsp">Stock Transfer</a></li>
				<li><a href="poojaStockList.jsp">Godown Stock</a></li>
				<li><a href="godwanstockApprove.jsp">Stock Approval Godown</a></li>
				<li><a href="shopTransferList.jsp">Stock Transfer List</a></li>
				<%if(session.getAttribute("headAccountId").toString().equals("3")){ %>
				<li><a href="poojaStore-StockReport.jsp">Stock Valuation</a></li><%} %>
			</ul>
    	</li>
    	<li><a href="#">Report</a>
    		<ul>
				<li><a href="productReport.jsp">Product Ledger Report</a></li>
			</ul>
    	</li>
    <%}} if(session.getAttribute("headAccountId").toString().equals("4") || session.getAttribute("headAccountId").toString().equals("0")){
	if(permisions1.contains("Receipts")){%>
	<li><a href="#">RECEIPTS</a>
		<ul>
			<li><a href="ticket-application.jsp">Seva Receipts</a></li>
			<li><a href="offerKinds-entry.jsp">Kinds entry</a></li>
			<li><a href="Devotee-Registration.jsp">Devotee Registration</a></li>
			<li><a href="Pancard-update.jsp">Edit Pancard</a></li>
			<li><a href="#">Devotees Registration List</a>
				<ul class="level-2">
					<li><a href="Devotees-Registrations-Search.jsp">Search Devotees Registration List</a></li>					
					<li><a href="Devotees-Registrations-List.jsp">Devotees Registration List</a></li>
				</ul>
			</li>		
			<li><a href="#">Employee/Volunteer Registration</a>
				<ul class="level-2">
					<li><a href="employee-volunteer-form.jsp?type=EMPLOYEE">Unique ID Registration</a></li>
				   <li><a href="employee-volunteer-list.jsp?type=EMPLOYEE" >Employee-Volunteer List</a></li>
				   <li><a href="volunteer-seva-registration.jsp" >Volunteer Seva Registration</a></li>
				   <li><a href="volunteer_seva_Listing.jsp" >Volunteer Seva's List</a></li>
				</ul>
			</li>	
		</ul>
	</li>
		<li><a href="#">REPORTS</a>
			<ul>
				<li><a href="collectionSummary-Report.jsp">Collection summary</a></li>
				<li><a href="collectionSummaryTotals.jsp">Collection Summary Totals</a></li>
				<li><a href="proCollectionDetails-Report.jsp">Collection Details</a></li>
				<li><a href="proOffice-salesReport.jsp">Daily Sales Report</a></li>
				<li><a href="offerKinds-dailySalesReport.jsp">Offer Kinds DailyReport</a></li>
				<!-- <li><a href="dailyPoojasList.jsp">Daily Poojas List</a></li> -->
				<li><a href="#">Daily Poojas List</a>
				<ul class="level-2">
					<li><a href="dailyPoojasList.jsp">Daily Poojas List_NAD,LTAM</a></li>
					<li><a href="dailyPoojasList2.jsp">Daily Poojas List_NAD,LTAM (NEW)</a></li>
					<li><a href="dailyPoojasListNew.jsp">Daily Poojas List New</a></li>
					<li><a href="nityaarchanaReport.jsp">Nitya Archana Report</a></li>
					<li><a href="pratheykaMangalsnanam.jsp">Shaswatha Prathyeka Parvadina Mangalsnanam</a></li>
					<li><a href="sahasraNamarchana.jsp">Prathyeka Sahasra Namarchana</a></li>
					<li><a href="mangalasthanam-report.jsp">Mangala Snanam Report</a></li>
				</ul>
				</li>
				<li><a href="#">Devotees List</a>
				<ul class="level-2">
					<li><a href="lifetimeArchana-nityaAnnadanamDevotees-new.jsp">Devotees List</a></li>
					<li><a href="lifetimeArchana-nityaAnnadanamDevotees-search.jsp">Search Devotees List</a></li>
				</ul>
				</li>	
				<li><a href="totalsalereport.jsp">Online sales</a></li>
				<li><a href="label-print.jsp">Label Print</a></li>
				<li><a href="#">Send SMS</a>
				<ul class="level-2">
					<li><a href="sendsms.jsp">Send SMS</a></li>
					<li><a href="sendsmsnadlta.jsp">Send SMS NAD,LTA</a></li>
					<li><a href="group-list.jsp">Send SMS By Groups</a></li>
					<li><a href="group-list-nad-lta.jsp">Send SMS By Groups NAD,LTA</a></li>
					<li><a href="send-sms-special-days.jsp">Send SMS Special Days</a></li>
					<li><a href="AnniversaryBirthdayWishes.jsp">Bday,Anniv Wishes</a></li>
					<li><a href="NadLtaRemainder.jsp">NAD,LTA Remainder</a></li>
					<li><a href="automaticallySentSmsNEmail.jsp">Automatically Sent SMS And Email</a></li>
					<!-- <li><a href="#">Send SMS By Groups</a>
						<ul class="level-2">
							  <li ><a href="group-list.jsp?group=DONOR">DONOR</a></li>
						      <li ><a href="group-list.jsp?group=TRUSTEES">TRUSTEES</a></li>
						      <li ><a href="group-list.jsp?group=NAD">NAD</a></li>
						      <li ><a href="group-list.jsp?group=LTA">LTA</a></li>
						      <li ><a href="group-list.jsp?group=RDS">RDS</a></li>
						      <li ><a href="group-list.jsp?group=TIME_SHARE">TIME SHARE</a></li>
						      <li ><a href="group-list.jsp?group=EMPLOYEE">EMPLOYEE</a></li>
						      <li ><a href="group-list.jsp?group=GENERAL">GENERAL</a></li>
						      <li ><a href="group-list.jsp?group=VOLUNTEER">VOLUNTEER</a></li>
						      <li ><a href="group-list.jsp?group=online">ONLINE</a></li>
						</ul>
					</li> -->	
				</ul>
				</li>
				<li><a href="email-list.jsp">Email & Mobile List</a></li>
				<li><a href="lastHourEdit.jsp">Last Hour Edit-Receipts</a></li>
				<li><a href="lastHourOfferKindEdit.jsp">Last Hour Edit-OfferKinds</a></li>
				<li><a href="offerkindslist.jsp">OfferKinds List</a></li>
			</ul>
		</li>
   <%}}if(session.getAttribute("headAccountId").toString().equals("4") || session.getAttribute("headAccountId").toString().equals("0")){ %>
   		<%if(permisions1.contains("stockTransfer")){%>
    		<li><a href="#">STOCK</a>
    			<ul>
    				<li><a href="godwanstockApprove.jsp">Stock Approval Godown</a></li>
    				<!-- <li><a href="shopStockTransfer.jsp">Stock Issue</a></li> -->
    				<li><a href="KotIssue-Requests.jsp">KOT Issue-Requests</a></li>
        			<li><a href="producedItems-entry.jsp">Produced items entry</a></li>
        
				</ul>
    		</li>
    		<li><a href="#">REPORTS</a>
    			<ul>
    				<li><a href="Sansthan-Stores-Purchase-Report.jsp">Stock Purchased</a></li>
    				<li><a href="Daily-Stock-Issuses-Report.jsp">Stock Daily Issues</a></li>
    				<li><a href="StockIssue-InvoiceBase-Report.jsp">Invoice Base Stock Daily Issues </a></li>
    							<li><a href="ProducedItemReport.jsp">Produced Item Report</a></li>
    				<li><a href="shopTransferList.jsp">Stock Issues List</a></li>
        		    <li><a href="Stock-Statement.jsp">Stock Statement</a></li>
        		    <li><a href="poojaStore-StockReport.jsp">Kitchen Store Quantity</a></li>
					<li><a href="prasadamExpenseReport.jsp">Prasadam Expenses Report</a></li>
				</ul>
    		</li>
   		 <%}}if(session.getAttribute("Role")!=null && (session.getAttribute("Role").equals("Catering Supervisor"))){%>
   		<li><a href="#">CONSUMPTION</a>
    			<ul>
    				<li><a href="Consumption-entry.jsp">Consumption Entry</a></li>
    				<li><a href="Consumption-Report.jsp">Consumption Report</a></li>
    				<li><a href="Stock_Issue_And_ProducedItems.jsp">KOT & Produced Report</a></li>
    				<li><a href="indentReport.jsp">Raise Indent</a></li>
        			<li><a href="indentReport1.jsp">Raise Indent1</a></li>
				</ul>
    		</li>
   <%}%>
   <%if(session.getAttribute("Role")!=null && (session.getAttribute("Role").equals("PO") || session.getAttribute("Role").equals("AsstPO"))){%>
	<li><a href="#">INDENT</a>
		<ul>
			<li><a href="indentReport.jsp">Raise Indent</a></li>
			<li><a href="indentReport1.jsp">Raise Indent1</a></li>
			<%if(session.getAttribute("Role").equals("PO")){ %>
			<li><a href="pendingPoApproval.jsp">MSE Entry Approval</a></li>
			<li><a href="pendingindentReport.jsp">Indent's Approval Require</a></li>
			<%} %>
			<li><a href="indentReportList.jsp">Indent's List</a></li>
			<li><a href="purchaseOrdersList.jsp">Purchase Orders List</a></li>
			<li><a href="stockRequest-report.jsp">Stock Requests</a></li>
			<%if(session.getAttribute("Role").equals("PO"))	{%>
			<li><a href="directChallanList.jsp">DC List</a></li>
			<%}%>
		</ul>
	</li>
	<li><a href="#">INVENTORY</a>
		<ul>
			<li><a href="Locations-Create.jsp">Create Locations</a></li>
			<li><a href="Products-Inventory.jsp">Add Products Inventory Location</a></li>
			<li><a href="see_product_inventory_based_on_status.jsp">See Product list</a></li>
			
		</ul>
	</li>
	<%} %>	
	<%if(session.getAttribute("Role")!=null && !session.getAttribute("Role").equals("PO")){
		if(permisions1.contains("stockRequest") && !permisions1.contains("Medical Hall")){
	%>
		<li><a href="#">REQUEST</a>
		<ul>
			<li><a href="stockRequest.jsp">Stock request</a></li>
			<li><a href="stockRequest-report.jsp">Stock Request List</a></li>
			
		</ul>
		</li>
	<%}} if(permisions1.contains("ProductAdd")){
	%>
		<li><a href="#">Product</a>
		<ul>
			<li><a href="products.jsp">Add Product</a></li>
			</ul>
		</li>
	<%}if(permisions1.contains("returnProduct")){%>
		<li><a href="#">Return Products</a>
		<ul>
			<li><a href="Product-Return-Form.jsp">Return Products Entry</a></li>
			</ul>
		</li>
	<%}if(permisions1.contains("gatepass")){%>
		<li><a href="#">GATEPASS</a>
		<ul>
			<li><a href="Gatepass-Entry-Form.jsp">GatePass Entry</a></li>
			<li><a href="Gatepass-Entry-Report.jsp">GatePass Entry Report</a></li>
	    </ul>
		</li>
	<%}%>
</ul>
</div>
<div class="counter-bal">
<ul>
<%if(session.getAttribute("Role").equals("poojacounter") || session.getAttribute("Role").equals("procounter") || session.getAttribute("Role").equals("medicalhall")){ %>
<li><a>Counter Bal : <%=CASH_REP.getCounterBalance(session.getAttribute("empId").toString(), "") %></a><%} %></li>
</ul></div>
<div class="name">
<ul>
	<li>
    <div style="float:left; margin:0 20px 0 0;"><%=session.getAttribute("empName")%></div>
	<div class="dropmenu">
	<dl>
		<dt><a href="changePassword.jsp">Change Password</a></dt>
		<dt><a href="logout_step1.jsp">Log out</a></dt>
	</dl>
</div>
</li>
</ul></div>
<!--<dt><div class="title">Account</div></dt>-->





</div>
<script>
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});</script>
<div style=""><a href="#" class="scrollup"></a></div>