<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function validate() {
	if(document.getElementById("billimage").value==""){
		alert("please upload scanned image of the bill");
		document.getElementById("billimage").focus();
		return false;
	}
}</script>
</head>
<body>
<%String billid=request.getParameter("bid"); %>
<form name="billimage_upload" action="billimage_insert.jsp" method="post" enctype="multipart/form-data" onsubmit="return validate();">
<span> upload scanned image of the Bill Id <%=billid %></span>
<input type="hidden" name="billid" id="billid" value="<%=billid %>" />
<input type="file" name="billimage" id="billimage" value=""/>
<input type="submit" value="Upload">  
</form>
</body>
</html>