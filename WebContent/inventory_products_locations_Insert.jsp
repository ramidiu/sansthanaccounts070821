<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>

<jsp:useBean id="IPL" class="beans.inventory_products_locationsService">
<%
	String inventory_name=request.getParameter("inventory_name");
	String product[]=request.getParameter("product").split(" ");

	String quantity=request.getParameter("qty");
	String location_id=request.getParameter("location");
	String created_by=session.getAttribute("empId").toString();
	Calendar c1 = Calendar.getInstance(); 
	c1.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone tz = TimeZone.getTimeZone("IST");

	DateFormat  dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat  dateFormat2= new SimpleDateFormat("yyyy-MM-dd");
	DateFormat  dateFormat3= new SimpleDateFormat("HH:mm:ss");
	dateFormat.setTimeZone(tz.getTimeZone("IST"));
	String date1=(dateFormat.format(c1.getTime())).toString();
	String hour=""+c1.get(Calendar.HOUR);
	String min=""+c1.get(Calendar.MINUTE);
	String sec=""+c1.get(Calendar.SECOND);
	//String time=dateFormat3.format((c1.get(Calendar.HOUR)+":"+c1.get(Calendar.MINUTE)+":"+c1.get(Calendar.SECOND)));
	String time=dateFormat3.format(dateFormat.parse(date1));
	String created_date=request.getParameter("date")+" "+time;
	String updated_by="";
	String updated_date="";
	String narration=request.getParameter("narration");
	
	String head_account_id=request.getParameter("HOA");
	String image="";
	String document="";
	String rate=request.getParameter("rate");
	String depreciation=request.getParameter("depreciation");
	String amount=request.getParameter("amount");
	String warranty=request.getParameter("warranty");
	String status=request.getParameter("status");
	String damaged_amount=request.getParameter("damaged_amount");
	String extra2=request.getParameter("extra2"); //purchase date 
	//get year from purchase date  and store in extra3 .....
	String extra3=""; 
	SimpleDateFormat dtf=new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat onlyyear=new SimpleDateFormat("yyyy");
	extra3=onlyyear.format(dtf.parse(extra2)); //store purchase year only
	String extra4=request.getParameter("extra4"); //depritiation upto how many year
	
%>
<jsp:setProperty name="IPL" property="inventory_name" value=""/>
<jsp:setProperty name="IPL" property="product_id" value="<%=product[0]%>"/>
<jsp:setProperty name="IPL" property="quantity" value="<%=quantity%>"/>
<jsp:setProperty name="IPL" property="location_id" value="<%=location_id%>"/>
<jsp:setProperty name="IPL" property="created_by" value="<%=created_by%>"/>
<jsp:setProperty name="IPL" property="created_date" value="<%=created_date%>"/>
<jsp:setProperty name="IPL" property="updated_by" value="<%=updated_by%>"/>
<jsp:setProperty name="IPL" property="updated_date" value="<%=updated_date%>"/>
<jsp:setProperty name="IPL" property="narration" value="<%=narration%>"/>
<jsp:setProperty name="IPL" property="status" value="<%=status%>"/>
<jsp:setProperty name="IPL" property="head_account_id" value="<%=head_account_id%>"/>
<jsp:setProperty name="IPL" property="image" value=""/>
<jsp:setProperty name="IPL" property="document" value=""/>
<jsp:setProperty name="IPL" property="extra1" value="<%=depreciation %>"/>
<jsp:setProperty name="IPL" property="extra2" value="<%=extra2%>"/>
<jsp:setProperty name="IPL" property="extra3" value="<%=extra3%>"/>
<jsp:setProperty name="IPL" property="extra4" value="<%=extra4%>"/>
<jsp:setProperty name="IPL" property="rate" value="<%=rate %>"/>
<jsp:setProperty name="IPL" property="amount" value="<%=amount %>"/>
<jsp:setProperty name="IPL" property="warranty" value="<%=warranty %>"/>
<jsp:setProperty name="IPL" property="damaged_amount" value="<%=damaged_amount %>"/>
<%=IPL.insert()%><%=IPL.geterror()%>
</jsp:useBean>
<%
response.sendRedirect("Products-Inventory.jsp");
%>