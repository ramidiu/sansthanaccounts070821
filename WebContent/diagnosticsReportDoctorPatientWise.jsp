<%@page import="beans.patientsentry"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.subheadListing"%>
<%@page import="beans.doctordetails"%>
<%@page import="mainClasses.diagnosticissuesListing"%>
<%@page import="beans.diagnosticissues"%>
<%@page import="helperClasses.DiagnosticHelper"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>DIAGNOSTIC DOCTOR PATIENT REPORT</title>
<meta charset="utf-8">
<!-- <title>jQuery UI Datepicker - Select a Date Range</title> -->
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
 <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
<style>

.new-bg{
    background: #e3eaf3;
    font-size: 13px;
    color: #7490ac;
    padding: 5px 0 5px 5px;
    border-right: 1px solid #c0d0e4;
    border-bottom: 1px solid #c0d0e4;}
    .clicked{   
   border: 1px solid #65230d;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    padding: 5px 15px 5px 15px;
    text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
    font-weight: bold;
    text-align: center;
    color: #FFF;
    background-color: #c88b2e;
    cursor: pointer;
   }
</style>
<!-- <script>
$(function() {
  $( "#fromDate" ).datepicker({
    defaultDate: "+1w",
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    numberOfMonths: 1,
    onClose: function( selectedDate ) {
      $( "#toDate" ).datepicker( "option", "minDate", selectedDate );
    }
  });
  $( "#toDate" ).datepicker({
    defaultDate: "+1w",
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    numberOfMonths: 1,
    onClose: function( selectedDate ) {
      $( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
    }
  });
});
</script> -->
<script>
$(function() {
	$( "#fromDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#toDate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
		$( "#toDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths:1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			$( "#fromDate" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>
 <script type="text/javascript">
        // When the document is ready, initialize the link so
        // that when it is clicked, the printable area of the
        // page will print.
        $(
            function(){
 				// Hook up the print link.
                $( "#print" ).attr( "href", "javascript:void( 0 )" ).click(
                        function(){
                            // Print the DIV.
							$( "a" ).css("text-decoration","none");
							$( "#tblExport" ).print();
 							 // Cancel click event.
                            return( false );
                        });
 				});
        $(document).ready(function () {
            $("#btnExport").click(function () {
                $("#tblExport").btechco_excelexport({
                    containerid: "tblExport"
                   , datatype: $datatype.Table
                });
            });
        });
    </script>
     <script src="js/jquery.print.js"></script>
<script src="js/jquery.battatech.excelexport.js"></script>
</head>
<body>
<%
DateFormat mySqlDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
DateFormat userDateFormat=new SimpleDateFormat("dd MMM yyyy HH:mm");
String fromDate="";
String toDate="";
String submit="";
String paidFree="paid";
if(request.getParameter("fromDate")!=null && !request.getParameter("fromDate").trim().equals("") && !request.getParameter("fromDate").equals("null"))
{
	fromDate=request.getParameter("fromDate");
}
if(request.getParameter("toDate")!=null && !request.getParameter("toDate").trim().equals("") && !request.getParameter("toDate").equals("null"))
{
	toDate=request.getParameter("toDate");
}
if(request.getParameter("submitType")!=null && !request.getParameter("submitType").trim().equals("") && !request.getParameter("submitType").equals("null"))
{
	submit=request.getParameter("submitType");
}
if(request.getParameter("paidFree")!=null && !request.getParameter("paidFree").trim().equals("") && !request.getParameter("paidFree").equals("null"))
{
	paidFree=request.getParameter("paidFree");
}
%>
<div><%@ include file="title-bar.jsp"%></div>
<div class="container-fluid edit">
<form  method="POST" action="<%=request.getContextPath()%>/diagnosticsReportDoctorPatientWise.jsp">
 	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-left:20px;" class=" table-striped">
  	<tr>
    	<td>
    		<table width="50%" border="0" cellspacing="0" cellpadding="0" class="table-bordered table-striped side1">
      			<%if(request.getParameter("msg")!=null){
      			%>
  				<tr>
  				<td></td>
  					<td><strong><%=request.getParameter("msg")%></strong></td>
  				</tr>
  				<% }%>
  				
  				<tr><td><div style=" margin-top:50px;"></div></td></tr>
 	 			<tr>
 	 			
   				<!-- 	<td ><span>From Date</span></td> -->
   					<td><input type="text" name="fromDate" id="fromDate"  class="DatePicker" value="<%=fromDate%>" />
   					<!-- <img class="ui-datepicker-trigger" src="images/calendar-icon.png" alt="Select Date" title="Select Date">
   						<div id="sat1" ></div> -->
   					</td>
   					
   					<!-- <td  ><span>To Date</span></td> -->
   					<td ><input type="text" name="toDate" id="toDate"  class="DatePicker" value="<%=toDate%>" />
   					<!-- <img class="ui-datepicker-trigger" src="images/calendar-icon.png" alt="Select Date" title="Select Date">
   						<div id="sat1" ></div> -->
   					</td>
   					<td>
   					<select class="form-control"  name="paidFree" style="width:100px;">
						<option value="paid" <%if("paid".equalsIgnoreCase(paidFree)){ %>selected <%} %> >Paid</option>
						<option value="free" <%if("free".equalsIgnoreCase(paidFree)){  %>selected <%} %>>Free</option>
					</select>
   					</td>
   					<td><input type="submit" name="submitType" value="Search" class="click"></td>
  				</tr>
  				<tr>
   					
  				</tr>
  				<!-- <tr>
   					<td width="30%" class="names" ><input type="reset" value="Clear"></td>
   					<td width="50%"><input type="submit" name="submitType" value="Search"></td>
  				</tr> -->
			</table>
		</td>
	</tr>
</table>
</form>

</div>
<div class="icons">
					<span><a id="print"><img src="images/printer.png" style="margin: 0 20px 0 0" title="print" /></a></span> 
<span><a id="btnExport" href="#Export to excel"><img src="images/excel.png" style="margin: 0 20px 0 0" title="export to excel" /></a></span>
				</div>
<%-- <div class="container-fluid edit">
<form  method="POST" action="<%=request.getContextPath()%>/diagnosticsReportDoctorPatientWise.jsp">
 	<table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class=" table-striped" style="margin:0 auto;">
 	<tr><td><div style=" margin-top:20px;"></div></td></tr>
  	<tr>
    	<td width="50%">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table-bordered table-striped side1">
  				<tr>
   					<td width="25%"><input type="submit" name="submitType" value="Today" <%if("Today".equals(submit)){%>class="clicked"<%}else{%>class="click"<%}%> onclick="this.form.submit();" ></td>
   					<td width="25%"><input type="submit" name="submitType" value="This Week" <%if("This Week".equals(submit)){%>class="clicked"<%}else{%>class="click"<%}%> onclick="this.form.submit();"></td>
   					<td width="25%"><input type="submit" name="submitType" value="This Month" <%if("This Month".equals(submit)){%>class="clicked"<%}else{%>class="click"<%}%> onclick="this.form.submit();"></td>
   					<td width="25%"><input type="submit" name="submitType" value="This Financial Year" <%if("This Financial Year".equals(submit)){%>class="clicked"<%}else{%>class="click"<%}%> onclick="this.form.submit();"></td>
  				</tr>
			</table>
			
		</td>
	</tr>
</table>
</form>
</div> --%>
<%
if(request.getParameter("submitType")!=null)
{
	subheadListing listing=new subheadListing();
	DiagnosticHelper helper=new DiagnosticHelper();
	List<doctordetails> doctorIdList=helper.getDoctorId(fromDate, toDate, submit, paidFree);
	double grandTotalAmount=0.0;
%>


<div class="clearfix"></div>
	<div style="padding:20px 0px 0px 0px;">
      <div align="center" style="font-weight: bold;"> SHRI SHIRIDI SAI BABA SANSTHAN TRUST CHARITY(MEDICAL STORE)</div>
		<div align="center" style="font-weight: bold;font-size: 12px;">Regd.No.646/92,Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</div>
		<div align="center" style="font-weight: bold;">Doctor And Patient  Wise Medicine Issues Report</div>
</div>
	<div>
	<form class="form-horizontal" action="<%=request.getContextPath()%>/diagnosticsReportDoctorPatientWise.jsp" method="post" role="form" style="padding: 10px;">
		<div class="form-group">
			<div class="col-sm-6" style="text-align:center;">
				<%-- <select class="form-control"  name="paidFree" onchange='this.form.submit()' style="width: 150px;font-weight: bold;color: brown;">
					<option value="paid" <%if("paid".equalsIgnoreCase(paidFree)){ %>selected <%} %> >Paid</option>
					<option value="free" <%if("free".equalsIgnoreCase(paidFree)){  %>selected <%} %>>Free</option>
				</select> --%>
				<input type="hidden" name="fromDate" value="<%=fromDate%>"/>
				<input type="hidden" name="toDate" value="<%=toDate%>"/>
				<input type="hidden" name="submitType" value="<%=submit%>"/> 
			</div>
				<div style=" margin-top:30px;"></div>
		</div>
	</form>
	</div>
	<div style="padding:20px;" id="tblExport">
	<%
	for(int i=0;i<doctorIdList.size();i++)
	{
		doctordetails doctorIds=doctorIdList.get(i);
		double netTotalAmount=0.0;
	%>
	<div><h2 style="font:bold;color:blue; padding:5px 0px 10px 18px;"><%=doctorIds.getdoctorName()%></h2></div>
	<div class="container-fluid view">
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table-striped">
	  <tr>
	    <th class="new-bg">CODE</th>
	    <th class="new-bg">TEST NAME</th>
	    <th class="new-bg">TOTAL AMOUNT</th>
	    <th class="new-bg">% DISCOUNT</th>
	    <th class="new-bg">NET AMOUNT</th>
	  </tr>
	 
	  <% 
	  	//List<diagnosticissues> list=helper.getDiagnosticDetails(fromDate,toDate,submit,paidFree,doctorIds.getdoctorId()); 
	 	List<patientsentry> listOfPatientId=helper.getpatientIds(fromDate,toDate,submit,paidFree,doctorIds.getdoctorId()); 
	 	
		for(int j=0;j<listOfPatientId.size();j++)
		{
			patientsentry PE=listOfPatientId.get(j);
			%><tr><td colspan="5" style="color:maroon; font-size:13px; padding:5px 0px 0px 18px;"><%=PE.getpatientId()%> <%=PE.getpatientName()%></td></tr><%
			List<diagnosticissues> list=helper.getDiagnosticDetails(fromDate,toDate,submit,paidFree,doctorIds.getdoctorId(),PE.getpatientId());
			for(int k=0;k<list.size();k++)
		  	{
		  		diagnosticissues dd=list.get(k);
		  %>
		  <tr><td><div style="margin-top:15px;"></div></td></tr>
		  <tr>
		    <td align="center">
		    	<i class="clearfix"></i>
		    	<%-- <%if(dd.getDate()!=null && !dd.getDate().trim().equals("")){ %>
				<p><%=userDateFormat.format(mySqlDateFormat.parse(dd.getDate()))%></p>
				<% }%> --%>
				<p><%=dd.getTestName()%></p>
			</td>
			<td align="center">
		    	<i class="clearfix"></i>
				<p><%=listing.getSuheadName(dd.getTestName())%></p>
			</td>
			<td align="center">
		    	<i class="clearfix"></i>
				<p><%=dd.getAmount()%></p>
			</td>
			<td align="center">
		    	<i class="clearfix"></i>
				<p><%=dd.getExtra2()%></p>
			</td>
			<td align="center">
		    	<i class="clearfix"></i>
				<p><%=dd.getExtra3()%></p>
			</td>
		  </tr>
		  <% 
		  netTotalAmount=netTotalAmount+Double.parseDouble(dd.getExtra3());
		  }
		}
	  	grandTotalAmount=grandTotalAmount+netTotalAmount;
	  %>
	  </table>
	</div>
	<div align="right" style="padding-right:44px; padding-top:20px; font-weight:bold;">Total : <%=netTotalAmount%></div>
	
	<% }%>
	<div align="right" style="padding-right:44px; padding-top:20px; font-weight:bold;">Grand Total : <%=grandTotalAmount%></div>
<%} %>
 </div>
</body>
</html>