<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="mainClasses.majorheadListing"%>
<%@page import="beans.minorhead"%>
<%@page import="mainClasses.minorheadListing"%>
<%@page import="mainClasses.productsListing"%>
<%@page import="mainClasses.subheadListing"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> 
<title>PRINT RECEIPT | SHRI SHIRIDI SAI BABA SANSTHAN TRUST</title>
<link href="css/acct-style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.4.2.js"></script>
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<!--Date picker script  -->
<link rel="stylesheet" href="./admin/themes/ui-lightness/jquery.ui.all.css" />
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.datepicker.js"></script>
<script>

$(function() {
	$( "#date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#fromdate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
	$( "#todate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: 'both',
		buttonImage: "images/calendar-icon.png",
		buttonText: 'Select Date',
		 buttonImageOnly: true,
		dateFormat: 'yy-mm-dd'
	});
		
});
	</script>
<!--Date picker script  -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery.blockUI.js"></script>

<script type="text/javascript">
function numbersonly(myfield, e, dec)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789-").indexOf(keychar) > -1))
   return true;

// decimal point jump
else if (dec && (keychar == "."))
   {
   myfield.form.elements[dec].focus();
   return false;
   }
else
   return false;
}
function combochange(denom,desti,jsppage) { 
    var com_id = document.getElementById(denom).options[document.getElementById(denom).selectedIndex].value; 
	document.getElementById(desti).options.length = 0;
	var sda1 = document.getElementById(desti);
	var amt=document.getElementById("Amount");
	$.post(jsppage,{ id: com_id } ,function(data)
		{
	var where_is_mytool=data;
	//alert(where_is_mytool);
	var mytool_array=where_is_mytool.split("\n");
	var ab=mytool_array.length-5;
	//alert(mytool_array.length);
	var x=document.createElement('option');
	x.text='Select pooja name';
	x.value='';
	sda1.add(x);
	for(var i=0;i<mytool_array.length;i++)
	{
	if(mytool_array[i] !="")
	{
	//alert (mytool_array[i]);
	
	var y=document.createElement('option');
	var val_array=mytool_array[i].split(":");
				y.text=val_array[1];
				y.value=val_array[0];
				try
				{
					sda1.add(y,null);
				}
				catch(e)
				{
					sda1.add(y);
				}
	}
	}
	}); 
}
function getPoojaPrice(){
	var proId=document.getElementById("productId").value;
	var subhId=document.getElementById("type").value;
	var amt=document.getElementById("Amount");
	var qty=document.getElementById("ticQty").value;
	var totAmt=document.getElementById("totAmt").value;
	$.ajax({
		type:'post',
		url: 'getPoojaPrice.jsp', 
		data: {
		q : subhId,
		b : proId
		},
		success: function(response) { 
			var arr=response.trim().split(":");
			var price=arr[0];
			var application=arr[1];
			amt.value='0';
			document.getElementById("appl").style.display='none';
			if(application!=null && application!="" && application=="application"){
				document.getElementById("applicatn").value=arr[1];
				document.getElementById("appl").style.display='block';
			}
			amt.value=price;
			document.getElementById("totAmt").value=Number(price)*Number(qty);
		}});
}
function amountInWords(){
	  var amount=$('#totAmt').val();
		  var arr = [];
	  $.post('numberInWords.jsp',{q:amount},function(data)
	{
		var response = data.trim().split("\n");
		 $('#amountinwords').empty();
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();	 
		 			 var d=response[0].split(",");	
			 $('#amountinwords').append('('+d[0]+')');
		
	
	});
	} 

/* function calculateTotal(){
	var amt=document.getElementById("Amount").value;
	var qty=document.getElementById("ticQty").value;
	document.getElementById("totAmt").value=Number(amt)*Number(qty);
	amountInWords();
} */
function calculateTotal(){
	var tamt=document.getElementById("totAmt").value;
	var amt=document.getElementById("Amount").value;
	var qty=document.getElementById("ticQty").value;
	document.getElementById("Amount").value=Number(tamt)/Number(qty);
	amountInWords();
}
function validate(){
	$('#poojaError').hide();
	$('#amtError').hide();
	$('#nameErr').hide();
	if($('#sortBy').val().trim()=="product"){
		if(($('#productId').val().trim()=="")){
			$('#poojaError').show();
			$('#productId').addClass('borderred');
			$('#productId').focus();
			return false;
		}}else if(($('#sortBy').val().trim()=="subhead")){
			if(($('#sub_head_id').val().trim()=="")){
				$('#poojaError').show();
				$('#sub_head_id').addClass('borderred');
				$('#sub_head_id').focus();
				return false;
			}
		}
	if(($('#customerName').val().trim()=="")){
		$('#nameErr').show();
		$('#customerName').addClass('borderred');
		$('#customerName').focus();
		return false;
	}
	if(($('#Amount').val().trim()=="") || ($('#Amount').val().trim()=="0")){
		$('#amtError').show();
		$('#Amount').addClass('borderred');
		$('#Amount').focus();
		return false;
	}
	
	var totAmt=document.getElementById("totAmt").value;
	if(totAmt.indexOf(".") >= 0){
	    alert("Amount cannot contain Decimal Number");
	    $('#totAmt').focus();
	    return false;
	}
	
	 $.blockUI({ css: { 
	        border: 'none', 
	        padding: '15px', 
	        backgroundColor: '#000', 
	        '-webkit-border-radius': '10px', 
	        '-moz-border-radius': '10px', 
	        opacity: .5, 
	        color: '#fff' 
	    } });
}
function productsearch(){
	var data1=$('#productId').val();
	 /*  var headID="1"; */
	  var headID=$("#headId").val();
	  $.post('searchKindProducts.jsp',{q:data1,hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;
		$( "#productId" ).autocomplete({source: availableTags,
			select: function(event, ui) {
				//$('#customerName').focus();
				/* $('#uniqueid').focus(); */	//while using Tab the cursor will come on First Name field
				$('#uniqueID').focus();
				//$('#customerName').val(ui.item.customerName);
			}	
		}); 
	});
} 
function productsearch2(){
	var data1=$('#selectProduct').val();
	 /*  var headID="1"; */
	  var headID=$("#headId").val();
	  $.post('searchKindProducts.jsp',{q:data1,hId:headID},function(data)
	{
			 var productNames=new Array();
		var response = data.trim().split("\n");
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 productNames.push(d[0] +" "+ d[1]);
		 }
		var availableTags=data.trim().split("\n");
		var availableIds=data.trim().split("\n");
		availableTags=productNames;
		$( "#selectProduct" ).autocomplete({source: availableTags,
			select: function(event, ui) {
				//$('#customerName').focus();
				/* $('#uniqueid').focus(); */	//while using Tab the cursor will come on First Name field
				$('#uniqueID').focus();
				//$('#customerName').val(ui.item.customerName);
			}	
		}); 
	});
} 
function showdatetime()
{
	document.getElementById('dateheading').style.display = "block";
	document.getElementById('showdate').style.display = "block";
	document.getElementById('timeheading').style.display = "block";
	document.getElementById('showtime').style.display = "block";
}

$(document).ready(function(){
	$('#selectProduct').hide();
	$('#sProduct').hide();
	$("#sortBy").change(function(){
	    var sortVal=$("#sortBy").val();
	    $("#prod").hide();
	    $("#subhead").hide();
	    if(sortVal=='product'){
		    $("#prod").show();
		    $("#subhead").hide();
		    $('#productId').focus();
	    }
	    if(sortVal=='subhead'){
	    	 $("#prod").hide();
			 $("#subhead").show();
			 $('#sub_head_id').focus();
			 
	    }
	  });
	  $("#sub_head_id").keyup(function(){
		  var data1=$("#sub_head_id").val();
		  var hoid=$("#headId").val();
		  //alert('1111.....'+hoid);
		  $.post('searchSubhead.jsp',{q:data1,hoa : hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			//alert(availableIds);
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			 $( "#sub_head_id" ).autocomplete({source: availableTags,
				 select: function(event, ui) {
					 //$('#customerName').focus();
					 event.preventDefault();
				 $(this).val(ui.item.label);
				 var offerkind=ui.item.label.split(' ');
			        var offer=offerkind[0];
			        //alert(offer);
			        if(offer.trim()==="20026"){
						document.getElementById("dateheading").style.display='block';
						document.getElementById("showdate").style.display='block';
						//document.getElementById("timeheading").style.display='block';
						document.getElementById("showtime").style.display='block';
						 }
			        $("#sub_head_id").attr('readonly', true);
					/* $('#uniqueid').focus(); */   //while using Tab the cursor will come on First Name field
					$('#uniqueID').focus();
					 //$('#customerName').val(ui.item.customerName);
					}
			 }); 
			});	 
	  });
	  
	  $("#selectProduct").keyup(function(){
		  var data1=$("#selectProduct").val();
		  var hoid=$("#headId").val();
		  //alert('1111.....'+hoid);
		  $.post('searchSubhead.jsp',{q:data1,hoa : hoid},function(data)
					 {
			 var response = data.trim().split("\n");
			 var doctorNames=new Array();
			 var doctorIds=new Array();
			 for(var i=0;i<response.length;i++){
				 var d=response[i].split(",");
				 doctorIds.push(d[0]);
				 doctorNames.push(d[0] +" "+ d[1]);
			 }
			 //alert(availableTags[0]);
			// alert(availableTags.length);
			var availableTags=data.trim().split("\n");
			var availableIds=data.trim().split("\n");
			//alert(availableIds);
			availableTags=doctorNames;
			//var names=availableTags 
			 //var names[]
			 $( "#selectProduct" ).autocomplete({source: availableTags,
				 select: function(event, ui) {
					 //$('#customerName').focus();
					 event.preventDefault();
				 $(this).val(ui.item.label);
				 var offerkind=ui.item.label.split(' ');
			        var offer=offerkind[0];
			        //alert(offer);
			        if(offer.trim()==="20026"){
						document.getElementById("dateheading").style.display='block';
						document.getElementById("showdate").style.display='block';
						//document.getElementById("timeheading").style.display='block';
						document.getElementById("showtime").style.display='block';
						 }
			        $("#selectProduct").attr('readonly', true);
					/* $('#uniqueid').focus(); */   //while using Tab the cursor will come on First Name field
					$('#uniqueID').focus();
					 //$('#customerName').val(ui.item.customerName);
					}
			 }); 
			});	 
	  });
	  $("#registration1").click(function () {
			document.getElementById("uniqueID").disabled = false;
			$('#uniqueID').focus();
		});
	});
/* function GetUniqueRegistrations(){
	  var mobile=$('#uniqueID').val();
		  var arr = [];
	  $.post('searchPhoneNo.jsp',{q:mobile,con : 'uniqueId'},function(data)
	{
		var response = data.trim().split("\n");
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();
		 var address2=new Array();
		 var city=new Array();
		 var pincode=new Array();
		 var pancard=new Array();
		 var email=new Array();
		 var dob=new Array();
		 for(var i=0;i<response.length;i++){
			 var d=response[i].split(",");
			 arr.push({
				 label: d[0],
				 phoneno:d[1],
				 customerName:d[2],
				 gothram:d[3],
				 address:d[4],
				 address2:d[5],
				 city:d[6],
				 pincode:d[7],
				 email:d[8],
				 pancard:d[9],
				 dob:d[10],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#uniqueID').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#mobile').val(ui.item.phoneno);
				$('#customerName').val(ui.item.customerName);
				$('#gothram').val(ui.item.gothram);
				$('#address').val(ui.item.address);	
				$('#address2').val(ui.item.address2);
				$('#city').val(ui.item.city);
				$('#pincode').val(ui.item.pincode);
				$('#emailid').val(ui.item.email);
				$('#date').val(ui.item.dob);
				$('#pancardNo').val(ui.item.pancard);
				
			}
		}); 
	});
} */
function GetUniqueRegistrations(){
	  var mobile=$('#uniqueID').val();
		  var arr = [];
	 $.post('searchPhoneNo.jsp',{q:mobile,con : 'uniqueId'},function(data) 
	{
		var response = data.trim().split("\n");
		var sssstId = new Array();
		 var customerName=new Array();
		 var phoneno=new Array();
		 var gothram=new Array();
		 var address=new Array();
		 var address2=new Array();
		 var city=new Array();
		 var pincode=new Array();
		 var pancard=new Array();
		 var email=new Array();
		 var dob=new Array();
		 for(var i=0;i<response.length;i++){
			 //var d=response[i].split(",");
			 var d=response[i].split(":");
			 arr.push({
				 label: d[0],
				 sssstId :d[1],
				 phoneno:d[2],
				 customerName:d[3],
				 gothram:d[4],
				 address:d[5],
				 address2:d[6],
				 city:d[7],
				 pincode:d[8],
				 email:d[9],
				 pancard:d[10],
				 dob:d[11],
			        sortable: true,
			        resizeable: true
			    });
		 }
		var availableTags=data.trim().split("\n");	
		availableTags=arr;
		$('#uniqueID').autocomplete({
			source: availableTags,
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
				$(this).val(ui.item.label);
			},
			select: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox and hidden field
				$(this).val(ui.item.label);
				$('#uniqueid').val(ui.item.sssstId);
				$('#mobile').val(ui.item.phoneno);
				$('#customerName').val(ui.item.customerName);
				$('#gothram').val(ui.item.gothram);
				$('#address').val(ui.item.address);	
				$('#address2').val(ui.item.address2);
				$('#city').val(ui.item.city);
				$('#pincode').val(ui.item.pincode);
				$('#emailid').val(ui.item.email);
				$('#date').val(ui.item.dob);
				$('#pancardNo').val(ui.item.pancard);
				
			}
		}); 
	});
}
</script>
<script type="text/javascript">
function showSelectProductOption(){
	var selectedItem=$('#sortBy').val();
	if(selectedItem=='product'){
		$('#sProduct').hide();
		$('#selectProduct').hide();
	}else{
		$('#sProduct').show();
		$('#selectProduct').show();
	}	
}

</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>



</head>
<%response.setHeader("Cache-Control","no-cache"); //Forces caches to obtain a new copy of the page from the origin server
response.setHeader("Cache-Control","no-store"); //Directs caches not to store the page under any circumstance
response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
response.setHeader("Pragma","no-cache"); //HTTP 1.0 backward compatibility %>
<body>
<jsp:useBean id="SUB" class="beans.subhead"></jsp:useBean>
<jsp:useBean id="MNR" class="beans.minorhead"></jsp:useBean>
<jsp:useBean id="CSP" class="beans.customerpurchases"/>
<%if(session.getAttribute("empId")!=null){ 
String HOAID=session.getAttribute("headAccountId").toString();
%>
<div ><%@ include file="title-bar.jsp"%></div>
<div class="vendor-page"  >
<div class="vendor-box" >
 <table width="95%" cellpadding="0" cellspacing="0" id="tblExport">
	<tr><td colspan="5" align="center" style="font-weight: bold;color: red;"> SHRI SHIRDI SAI BABA SANSTHAN TRUST PRO OFFICE  </td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Regd.No.646/92</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;font-size: 10px;">Dilsukhnagar,Hyderabad,TS-500 060,Ph:24066566,24150184</td></tr>
	<tr><td colspan="5" align="center" style="font-weight: bold;">PRINT RECEIPT FOR OFFER KIND</td></tr>
</table>
<div class="clear"></div>
 
<div class="vender-details" style="">

<form name="tablets_Update" method="post" action="offerKinds_Insert.jsp" onsubmit="return validate();">
<input type="hidden" name="bid" id="bid" value="<%=request.getParameter("bid")%>"/>
<table width="70%" border="0" cellspacing="0" cellpadding="0">

<%
mainClasses.no_genaratorListing ng_LST=new mainClasses.no_genaratorListing();
String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());%>
<tr>
<td>Date</td>
<!-- <td>Receipt No</td> -->
<td>Head of account</td>
<td>Sort by</td>
<td colspan="1">
<div class="warning" id="poojaError" style="display: none;">Please select a Kind.</div>
OFFER KIND <span style="color: red;">*</span></td>
<td colspan="5" id="sProduct">
<div class="warning" id="productError" style="display: none;">Please select a Product</div>
SELECT PRODUCT <span style="color: red;">*</span></td>
<td>
<!-- <div class="warning" id="catError" style="display: none;"></div>
Category Type <span style="color: red;">*</span> --></td>
</tr>
<tr>
<td><input type="text" name="curentdate" value="<%=currentDate%>" readonly/></td>
<%-- <td><input type="text" name="receiptNo" value="<%=ng_LST.getidWithoutUpdatingNoGen("offerKindsInvoice")%>" readonly></td> --%>
<td colspan="1">
<select name="headId" id="headId"  autofocus="autofocus">
<option value="4">SANSTHAN</option>
<option value="3">POOJA STORES</option>
<option value="1">CHARITY</option>
</select>

</td>
<td>
	<select name="sortBy" id="sortBy" onchange="showSelectProductOption();">
		<option value="product">Product</option>
		<option value="subhead">Sub head</option>
	</select>
</td>
<td><div id="prod" style="display: block;"><input type="text" name="productId" id="productId" value="" onkeyup="productsearch()" placeHolder="Find a kind product here" autocomplete="off"/></div>


<div id="subhead" style="display: none;"><input type="text" name="sub_head_id" id="sub_head_id"  value="" placeholder="find a kind-sub head" autocomplete="off"/></div>
<a href="offerKinds-entry.jsp"><input type="button" name="other" value="other" class="click" style="border:none;"/></a>
</td>
<td><div id="prod2" style="display: block;"><input type="text" name="selectProduct" id="selectProduct" value="" onkeyup="productsearch2()" placeHolder="Select a kind product here" autocomplete="off"/></div></td>

<td>

<%majorheadListing MJRL=new majorheadListing();
minorheadListing MINRL=new minorheadListing();
List MHL=MINRL.getMminorheadsBasedONHOA(HOAID);
subheadListing SUBHL=new subheadListing();
List SUB_DET=SUBHL.getPoojaTypes(HOAID,"11");
%>
<%-- <select name="type" id="type" onChange="combochange('type','productId','getPoojas.jsp')">
<%if(MHL.size()>0){ 
	for(int i=0;i<MHL.size();i++){
	MNR=(minorhead)MHL.get(i);%>
<option value="<%=MNR.getminorhead_id()%>"><%=MNR.getname()%> (<%=MJRL.getmajorheadName(MNR.getmajor_head_id())%>)</option>
<%}} %>
</select> --%>
</td>

<td colspan="1"></td>
</tr>
<tr>
<td>Get Details with Mobile</td>
<td>Mobile Number</td>
<td>Unique Id</td>
</tr>
<tr>
	<!-- <td colspan="2">Have SSSST Registration ID ? </td> 
	<td><input type="radio" name="registration" id="registration" value="no" checked="checked"/>No<input type="radio" name="registration"  id="registration1" value="yes"/>Yes</td> -->
	<!-- <td colspan="2">Get the Details with Mobile Number</td>  -->
	
	<td><input type="text" name="uniqueID" id="uniqueID" onkeyup="GetUniqueRegistrations();" value="" placeHolder="Find details with Mobile Number" />
	<td><input type="text" name="mobile" id="mobile" value=""  onKeyPress="return numbersonly(this, event,true);" placeholder="Enter mobile number"></td>
<td>
<input type="radio" name="wtsp" id="wtsp" value="ok" checked="checked" />Enable 
<input type="radio" name="wtsp" id="wtsp"  value="notOk" />Disable
</td>	
	<td><input type="text" name="uniqueid" id="uniqueid" value=""  placeHolder="UniqueId Will Displayed" readonly />
</tr> 
<tr>
<!-- <td>
<div class="warning" id="QualificationError" style="display: none;">Please Provide  "Qualification".</div>
Mobile Number</td> -->
<td><div class="warning" id="nameErr" style="display: none;">Please Provide  "First name".</div>First Name <span style="color: red;">*</span></td>
<td>Gothram</td>
<td>Address</td>
<td>Email Id</td> 
</tr>
<tr>
<!-- <td><input type="text" name="mobile" id="mobile" value=""  onKeyPress="return numbersonly(this, event,true);" placeholder="Enter mobile number"></td> -->
<td><input type="text" name="customerName" id="customerName" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeholder="Enter full name"></td>
<td><input type="text" name="gothram" id="gothram"  value="" onblur="javascript:this.value=this.value.toUpperCase();" placeholder="Enter gothram"></td>
<td>
<input type="text" name="address" id="address" placeholder="Enter Your Address" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
<td><input type="text" name="emailid" id="emailid" value="" onblur="javascript:this.value=this.value.toLowerCase();" placeholder="Enter Your Email Id"></td> 
</tr>
<tr>
<td>Purpose</td>
<td>Qty</td>
<td>
<div class="warning" id="amtError" style="display: none;">Please enter amount.</div>
Approx Amount </td>

<td >Gross Amount</td>
<td id = "dateheading" style="display:none;">Date</td>
<td id = "timeheading" style="display:none;">Time</td>
</tr>

<tr>
<td><input type="text" name="purpose" id="purpose" value="" onblur="javascript:this.value=this.value.toUpperCase();" placeholder="Enter purpose"></td>
<td><input type="hidden" name="applicatn" id="applicatn" value="">
<input type="text" name="ticQty" id="ticQty" value="1" onKeyPress="return numbersonly(this, event,true);" onkeyup="calculateTotal();"></td>
<td><input type="text" name="Amount" id="Amount" value="0" /></td> 
<!-- <td><input type="radio" name="extra1" value="M" checked="checked" />Male<input type="radio" name="extra1" value="F" />Female</td> -->


<td><input type="text" name="totAmt" id="totAmt" onKeyPress="return numbersonly(this, event,true);" value="0" onkeyup="calculateTotal();">
<div id="amountinwords" style="text-align: center;font-size: 12px;color: #F00;"></div></td>

<td style="display:none;" id = "showdate"><input type="text" name="fromdate" id="fromdate" value="" style="float: left;width: 110px;" readonly="readonly"></td>
<td style="display:none;" id = "showtime">
<select name="time">
<option value="">select</option>
<option value="6 AM">6 AM</option>
<option value="12 PM">12 PM</option>
<option value="6 PM">6 PM</option>
</select></td>
</tr>
<tr> 
<td colspan="3" align="right"><input type="reset" value="RESET" class="click" style="border:none;margin-top:20px;"/></td>
<td colspan="1" align="right"><input type="submit" name="saveonly" value="SAVE-ONLY" class="click" style="border:none;"/></td>
<td align="right"><input type="submit" value="Save&Print" class="click" style="border:none;margin-top:20px;"/></td>

</tr>


</table>

</form>

</div>


</div>

</div>


<%} %>
<script>
window.onload=function a(){
	combochange('type','productId','getPoojas.jsp')
	};
	function print()
	{
	if(document.getElementById("bid").value != 'null'){
	newwindow=window.open('printReceipt.jsp?bid='+document.getElementById("bid").value+'&recName=sevaticket','name','height=600,width=500,menubar=yes,status=yes,scrollbars=yes');
	/* newwindow=window.open('Pro_print.jsp?bid='+document.getElementById("bid").value); */
	 /* newSJwindow=window.open('Devotee-Image-Capture.jsp?bid='+document.getElementById("bid").value,'nameSJ','height=600,width=650,menubar=yes,status=yes,scrollbars=yes'); */			
		if (window.focus) {newwindow.focus();}
	}
	}
	window.onload=print();
</script>
<div><div ><jsp:include page="footer.jsp"></jsp:include></div></div>
</body>
</html>